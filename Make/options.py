import multiprocessing

# Version on Visual Studio compiler
MSVC_VERSION = '10.0'

# Sets the number of parallel building processes
NUMBER_OF_JOBS = multiprocessing.cpu_count()

# Use batch builds in Visual Studio
USE_BATCH_BUILD = 0

# Implicit cache for faster builds. Disable if you like it slooow
ENABLE_IMPLICIT_CACHE = 1

# Holds variables set by user.
USER_VARIABLES = {
    'MAYASDK' : ['2013', '2014', '2015'],
    'PRMANSDK' : ['14', '15', '16', '17', '18', '19'],
}

# Pretty way for overriding environment vars
ENVIRON = {
    # example
    # 'SDK': 'c:/dev/SDK',
}
