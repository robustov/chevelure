import os

#-----------------------------------#

ULITKASDK_LIBPATH = []
ULITKASDK_CPPPATH = [
    os.path.join(env.environ['ULITKASDK'], 'inc'),
]
ULITKASDK_LIBS = []

#-----------------------------------#

OCELLARIS_LIBPATH = []
OCELLARIS_CPPPATH = [
    os.path.join(env.environ['OCELLARIS'], 'inc'),
]
OCELLARIS_LIBS = []

#-----------------------------------#

MITKASDK_LIBPATH = [os.path.join(env.environ['MITKASDK'], 'crypto/release')]
MITKASDK_CPPPATH = [
    os.path.join(env.environ['MITKASDK'], 'crypto/interfaces'),
]
MITKASDK_LIBS = ['Crypto_Dll.lib']

#-----------------------------------#
sdk_suffix = env.get_sdk_suffix()
#-----------------------------------#

MAYASDK_LIBPATH = []
MAYASDK_CPPPATH = []
MAYASDK_LIBS = []

#print env.environ['MAYA_VERSION'] + str(env)
env.environ['MAYA_SDK'] = os.path.join(env.environ['SDK'],
                                       'MAYA' + env.environ['MAYA_VERSION'] + sdk_suffix)
MAYASDK_LIBPATH = [env.environ['MAYA_SDK'] + '/lib']
MAYASDK_CPPPATH = [env.environ['MAYA_SDK'] + '/include']
MAYASDK_LIBS    = ['Foundation.lib', 'OpenMaya.lib', 'OpenMayaUI.lib',
                   'OpenMayaRender.lib', 'OpenMayaFX.lib',
                   'OpenMayaAnim.lib', 'OpenGL32.lib']

#-----------------------------------#

if env.environ.has_key('PRMAN_VERSION'):
    version = env.environ['PRMAN_VERSION']
else:
    version = env.getUserVariable('PRMANSDK')[-1]
env.environ['PRMAN_SDK'] = os.path.join(env.environ['SDK'],
                                        'PRMAN' + version + sdk_suffix)
PRMANSDK_LIBPATH = [env.environ['PRMAN_SDK'] + '/lib']
PRMANSDK_CPPPATH = [env.environ['PRMAN_SDK'] + '/include',
                    os.path.join(env.environ['SDK'], 'PRMAN_OLD', 'include'),
                    os.path.join(env.environ['SDK'], 'RAT', 'include'),]
if version < '14':
    PRMANSDK_LIBS    = ['libprmansdk.lib', 'prman.lib']
    print ' < 14 '
else:
    PRMANSDK_LIBS    = ['libprman.lib']

#-----------------------------------#

rms_path = os.path.join(env.environ['SDK'],
                        'RMS' + env.environ['MAYA_VERSION'] + sdk_suffix)
RMS_LIBPATH = [rms_path + '/lib']
RMS_CPPPATH = PRMANSDK_CPPPATH
RMS_LIBS = ['RenderMan_for_Maya.lib']

#-----------------------------------#

SYS_LIBS = [
    'Kernel32.lib','User32.lib','gdi32.lib','winspool.lib','comdlg32.lib',
    'advapi32.lib','shell32.lib','ole32.lib','oleaut32.lib','uuid.lib',
    'odbc32.lib','odbccp32.lib','winmm.lib','comctl32.lib','rpcrt4.lib',
    'wsock32.lib', 'Shlwapi.lib', 'Version.lib','Glu32.lib', 'Ws2_32.lib',
    'Netapi32.lib', 'Iphlpapi.lib',
]
