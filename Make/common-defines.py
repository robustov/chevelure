import os

CPPDEFINES = [
    'OFFSHORE_STATIC',
	'COMMON_STATIC',
	'ED_CORE_INTERNALS',
	'ED_SYNC_EXTERN=',
	'ED_CORE_EXPORT=',
	'ED_CORE_EXTERN=',
	'ED_LUA_EXTERN=',     
]

CPPPATH = [
    os.path.join(env['TOP_DIR'], 'chevelure2/external'),
    os.path.join(env['TOP_DIR'], 'chevelure2/external/osgUtils'),
]
