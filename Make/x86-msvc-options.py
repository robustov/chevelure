import os

CPPPATH_COMMON = [
]

LIBPATH_COMMON = [
]

CXXFLAGS_COMMON = ['/nologo', '/EHsc', '/GR', '/Zc:forScope', '/Zm111',
        '/Fd' + env.File(env['TARGET_NAME'] + '.pdb').abspath,
        '/W3',
]


CPPDEFINES_COMMON = [
    'WIN32', '_WINDOWS',
#    'WIN32_LEAN_AND_MEAN', 'WIN64_LEAN_AND_MEAN',
    '_USE_MATH_DEFINES', 'NOMINMAX', '_CRT_SECURE_NO_DEPRECATE'
]

#-----------------------------------#

CXXFLAGS_RELEASE = ['/MD', '/O2', '/Zi']

CPPDEFINES_RELEASE = ['NDEBUG']

#-----------------------------------#

CXXFLAGS_DEBUG = ['/MDd', '/Od', '/ZI']

CPPDEFINES_DEBUG = ['_DEBUG']

#-----------------------------------#

CPPDEFINES_COMMON_DLL = ['_USRDLL']

#-----------------------------------#

LINKFLAGS_COMMON = [
    '/STACK:16777216', '/MACHINE:X86',
    '/MANIFEST',
    '/PDB:' + env.File(env['TARGET_NAME'] + '.pdb').abspath
]

LINKFLAGS_DEBUG = ['/NODEFAULTLIB:libc', '/NODEFAULTLIB:libcmt', '/DEBUG']

LINKFLAGS_RELEASE = ['/NODEFAULTLIB:libc', '/NODEFAULTLIB:libcmt', '/DEBUG']

#-----------------------------------#
