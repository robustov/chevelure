import os

CPPPATH_COMMON = [
    os.path.join(env['TOP_DIR'], 'chevelure2/external/win32'),
    os.path.join(env['TOP_DIR'], 'chevelure2/external'),
    os.path.join(env['TOP_DIR'], 'chevelure2/external/Core'),
    os.path.join(env['TOP_DIR'], 'chevelure2/external/osgUtils'),
]

LIBPATH_COMMON = [
]

CXXFLAGS_COMMON = ['/nologo', '/EHsc', '/GR', '/Zc:forScope',
    '/Fd' + env.File(env['TARGET_NAME'] + '.pdb').abspath,
    '/W3',
]


CPPDEFINES_COMMON = [
    'WIN32', 'WIN64', '_WINDOWS',
#'WIN32_LEAN_AND_MEAN', 'WIN64_LEAN_AND_MEAN',
    '_USE_MATH_DEFINES', 'NOMINMAX', '_CRT_SECURE_NO_DEPRECATE'
]

#-----------------------------------#

CXXFLAGS_RELEASE = ['/MD', '/O2', '/Zi']

CPPDEFINES_RELEASE = ['NDEBUG']

#-----------------------------------#

CXXFLAGS_DEBUG = ['/MDd', '/Od', '/Zi']

CPPDEFINES_DEBUG = ['_DEBUG']

#-----------------------------------#

CPPDEFINES_COMMON_DLL = ['_USRDLL']

#-----------------------------------#

LINKFLAGS_COMMON = [
    '/STACK:16777216', '/MACHINE:X64',
    '/MANIFEST',
    '/LARGEADDRESSAWARE',
    '/PDB:' + env.File(env['TARGET_NAME'] + '.pdb').abspath
]

LINKFLAGS_DEBUG = ['/NODEFAULTLIB:libc', '/NODEFAULTLIB:libcmt', '/DEBUG']

LINKFLAGS_RELEASE = ['/NODEFAULTLIB:libc', '/NODEFAULTLIB:libcmt', '/DEBUG']

#-----------------------------------#
