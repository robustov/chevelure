import os, platform, re, time, SCons
from SCons.Script.SConscript import SConsEnvironment, Configure

# For every specified key ensures that this key will be present 
# in dictionary and value for this will be a list
def make_lists(params, keys):
	if params == None:
		return None
		
	for key in keys:
		if not params.has_key(key):
			params[key] = []
			
		if not isinstance(params[key], list):
			params[key] = [ params[key] ]
		
	return params

# Substle aliases   
def substitle_aliases(params, aliases):
	if params == None:
		return None

	for key, value in aliases.iteritems():
		# You specify only CPPDEFINES or cppdefines but not both at the same time
		if params.has_key(key) and params.has_key(value):
			raise SCons.Errors.UserError('both \"%s\" and \"%s\" keys specified for target' % (key, value))

		params[key] = params.pop(value, params.pop(key, []))

	return params
			
# For every specified key appends value of platform_params to value of params
def union(params, platform_params, keys):
	if platform_params == None:
		return params
			
	for key in keys:
		params[key] += platform_params[key]
		
	return params

# Substracts map m2 from m1
def sub_maps(m1, m2):
	if m1 == None or m2 == None:
		return None

	for key, value in m2.iteritems():
		if m1.has_key(key):
			m1[key] = filter(lambda x: not x in value, m1[key])

	return m1

# Special case of union() for defines 
def union_defines(params, platform_params):
	if platform_params == None or not platform_params.has_key('CPPDEFINES'):
		return params
		 
	for value in platform_params['CPPDEFINES']:
		contains = False

		for i in params['CPPDEFINES']:
			# To eleminate leading '"' in some defines
			i_offset = 0 if i[0] != '"' else 1
			value_offset = 0 if value[0] != '"' else 1

			# "DEFINE_NAME="\"DEFINE_VALUE\""" -> DEFINE_NAME
			i_name = i.split('=')[0][i_offset:]
			value_name = value.split('=')[0][value_offset:]

			contains |= i_name == value_name

		if not contains:
			params['CPPDEFINES'].append(value)
			
	return params
	
# Debug routine just to know that we do not ignore some unknown param
def validate_known_params(params):
	if params == None:
		return None
		
	valid_keys = ['target', 'include', 'header', 'source', 'project', 'resource', 'LIBS', 'LIBPATH', 'LINKFLAGS', 'CPPDEFINES', 'CCFLAGS', 'CXXFLAGS', 'CPPFLAGS', 'CPPPATH', 'FRAMEWORKS', 'not_linked_dependencies', 'external_libs', 'precompiled_header', 'SHLIBSUFFIX']
		
	for key, value in params.iteritems():
		if not key in valid_keys:
			raise SCons.Errors.UserError('Unknow key \"' + key + '\"')
			
	return params

# Filter dictionary using regex applied to keys
def filter_with_regex(unfiltered, regex):
	result = {}
	for key, value in unfiltered.iteritems():
		res = regex.search(key)
		if res != None:
			if result.has_key(res.group(1)):
				result[res.group(1)] += value
			else:
				result[res.group(1)] = value
				
	return result
	
def verbose_print(*args):
	# Print each argument separately so caller doesn't need to
	# stuff everything to be printed into a single string
	for arg in args:
		print arg,
	print
	
class EDBuildEnvironment(SConsEnvironment):

	PLATFORMS_ALIASES = {'Windows': 'win', 'Darwin': 'mac', 'Linux': 'linux'}

	def __init__(self, arguments):
		# Check that scons version is at least 2.1.0 and not an alpha release
		self.EnsureSConsVersion(2, 1)
		if SCons.__version__.find('alpha') >= 0:
			raise SCons.Errors.UserError('Alpha versions of SCons are not supported. Your SCons version is ' + SCons.__version__)

		# Gracefully hint that there is a newer version
		if not SCons.__version__.startswith('2.2.'):
			print 'There is a newer 2.2.0 SCons version. Your SCons version is ' + SCons.__version__ + '. Please update.'
			time.sleep(3)
		
		# Verbose mode
		if not arguments.has_key('edverbose'):
			global verbose_print
			verbose_print = lambda *x: None
			
		# Default environment options
		env_options = {
			'MODE' : arguments.get('mode', 'debug'),
			'ARCH' : arguments.get('arch', 'x86_64'),
			'TOOL' : arguments.get('tool', 'msvc' if platform.system() == 'Windows' else 'gcc'),
			'USE_BATCH_BUILD'       : 0, # MSVC can compile multiple files during one compiler call. Disabled by default
			'NUMBER_OF_JOBS'        : 2, # Probably everybody has at least two cores today
			'ENABLE_IMPLICIT_CACHE' : 1, # Implicit cache for faster builds
		}
 
		# Read environment options from configs
		env_options.update(self.read_config(os.path.join('Make', 'options.py')))
		
		# Generate some derived environment options
		env_options['TARGET_ARCH'] = 'amd64' if env_options['ARCH'] == 'x86_64' else 'x86'
		env_options['MSVC_BATCH']  = env_options['USE_BATCH_BUILD']
		
		# Any left console arguments override any environment options
		env_options.update(arguments)
		
		# Validate environment options
		self.validate_env_options(env_options)
		
		# Initialize environment
		env_options['tools'] = ['default'] if env_options['TOOL'] != 'mingw' else ['mingw']
		SConsEnvironment.__init__(self, **env_options)

		# TODO: check for C++ on Windows
		# Check for C++ compiler on non-Windows platforms
		if platform.system() != 'Windows':
			conf = Configure(self)
			if not conf.CheckCXX():
				raise SCons.Errors.UserError('No C++ compiler found. You should install g++ first')

		# Pretty way to override environment variables without manualy hardcodes for each of them
		self.environ = os.environ
		self.environ.update(env_options.pop('ENVIRON', {}))

		# Set some options after environment is created
		self.SetOption('num_jobs', env_options['NUMBER_OF_JOBS'])
		self.Decider('MD5-timestamp')
		self.SetOption('max_drift', 1)
		self.SetOption('diskcheck','match')
		self.SetOption('implicit_cache', env_options['ENABLE_IMPLICIT_CACHE'])
		
		# Print settings
		print "mode: %s" % self['MODE']
		print "arch: %s" % self['ARCH']
		print "tool: %s" % self['TOOL']
		
		# Setup all other settings
		self['TOP_DIR']         = os.getcwd() + '/' # we need this this slash because too many people ignore existing of os.path.join()
#		self['BUILD_DIR']       = os.path.join(self['TOP_DIR'], self.get_build_dir())
#		self['INSTALL_BIN_DIR'] = os.path.join(self['TOP_DIR'], self.get_install_bin_dir())
#		self['INSTALL_LIB_DIR'] = os.path.join(self['TOP_DIR'], self.get_install_lib_dir())
		self['PROJECT_DIR']     = os.path.join(self['TOP_DIR'], self.get_project_dir())
		
		if self['MODE'] != 'msvsproj':
			self.force_cmd_absolute_path()
			self.SConsignFile(os.path.join('Make', '.sconsign-' + self['ARCH'] + '-' + self.get_configuration_name() + '.dblite'))
		else:
			self.project_generation_mode()
			self.SConsignFile(os.path.join('Make', '.sconsign-msvsproj.dblite'))

	# Replace builder methods with corresponding project generation methods
	def project_generation_mode(self):
		self.msvsProjects = {}

		self.edBuild = self.msvsBuild
		self.edProgram = self.msvsProgram
		self.edSharedLibrary = self.msvsSharedLibrary
		self.edStaticLibrary = self.msvsStaticLibrary
		self.edLoadableModule = self.msvsLoadableModule

	# Hack to force scons pass to compiler absolute path instead of relative so
	# Visual Studio error navigation will working
	def force_cmd_absolute_path(self):
		# TODO: Maybe we need a bit more elegant solution instead of this hack
		def use_sources_abspath(x):
			return x.replace('$CHANGED_SOURCES', '$CHANGED_SOURCES_ABSPATH')

		self['CCCOM']    = use_sources_abspath(self['CCCOM'])
		self['SHCCCOM']  = use_sources_abspath(self['SHCCCOM'])
		self['CXXCOM']   = use_sources_abspath(self['CXXCOM'])
		self['SHCXXCOM'] = use_sources_abspath(self['SHCXXCOM'])

		self['CHANGED_SOURCES_ABSPATH'] = '${SOURCES.abspath}'

	# Here all environment settings validations are placed
	def validate_env_options(self, env_options):
		if env_options['MODE'] not in ['debug', 'release', 'msvsproj']:
			raise SCons.Errors.UserError("Invalid mode '" + env_options['MODE'] + "'. Must be 'debug', 'release' or 'msvsproj'")
			
		if env_options['MODE'] == 'msvsproj' and env_options['TOOL'] != 'msvc':
			raise SCons.Errors.UserError("Mode 'msvsproj' could be set only for 'msvc' tool")
	
	# Returns a dictionary of config variables
	def read_config(self, config_name):
		verbose_print("reading config '%s'" % config_name)
		
		# TODO: somehow strip imported modules
		
		# Config could be overridden by local version
		result = {}
		for name in [config_name, config_name + '.local']:
#			try:
			if os.path.isfile(name):
				temp = {}
				execfile(os.path.join(name), {'env': self}, temp)
				result.update(temp)
#			except Exception as e:
#				raise e
#				print "Error while reading config '" + name + "': " + str(e)

		return result
		
	def get_configuration_name(self):
		if self['TOOL'] == 'msvc':
			return 'vc' + self['MSVC_VERSION'].replace('.', '') + '.' + self['MODE']

		return self['TOOL'] + '.' + platform.system() + '.' + self['MODE']

	# Returns build directory path relative to top dir. Override if needed
	def get_build_dir(self):
		return os.path.join('.build', self['ARCH'], self.get_configuration_name())
		
	# Returns path to bin directory relative to top dir. Override if needed     
	def get_install_bin_dir(self):
		return os.path.join('bin', self['ARCH'], self.get_configuration_name())
		
	# Returns path to lib directory relative to top dir. Override if needed
	def get_install_lib_dir(self):
		return os.path.join('lib', self['ARCH'], self.get_configuration_name())
	
	# Returns path to project directory relative to top dir. Override if needed
	def get_project_dir(self):
		# By now we support only Visual Studio project generation
		if self['TOOL'] != 'msvc':
			return ''
			
		return os.path.join('Make', 'msvs', self['MSVC_VERSION'])

	# Get real absolute path of file or directory ignoring any variant directories
	def real_abspath(self, path):
		node = self.Entry(os.path.abspath(str(path)))

		# We need to do it in a loop because variant dir could be nested
		while node != node.srcnode():
			node = node.srcnode()

		return node.abspath

	def process_external_libs(self, external_libs):
		result = {}

		for key, value in external_libs.iteritems():
			k = re.split('_', key)
			if (len(k) == 2) or (len(k) == 3 and k[2] == self['MODE'].upper()):
				library_name = k[0].lower()
				if not result.has_key(library_name):
					result[library_name] = {}
				result[library_name][k[1]] = value

		return result
	
	def process_params(self, target_type, params):
		aliases = {'LIBS': 'libs', 'CPPDEFINES': 'cppdefines'}
		
		keys = ['include', 'source', 'LINKFLAGS', 'CCFLAGS', 'CXXFLAGS', 'CPPFLAGS', 'LIBPATH', 'CPPPATH', 'CPPDEFINES', 'LIBS', 'FRAMEWORKS', 'resource']
		
		prepare_all = lambda x: validate_known_params(make_lists(substitle_aliases(x, aliases), keys))
		union_all   = lambda x, y: union_defines(union(x, y, [x for x in keys if x != 'CPPDEFINES']), y)

		# platform specific
		platform_params = None
		platform_excludes = None
		for key, value in self.PLATFORMS_ALIASES.iteritems():
			p = params.pop(value, None)
			if p != None and platform.system() == key:
				platform_excludes = prepare_all(p.pop('exclude', None))
				platform_params = prepare_all(p)
				verbose_print('platform:     %s' % platform_params)
				verbose_print('platform_excludes:    %s' % platform_excludes)
		
		# common parameters
		excludes = prepare_all(params.pop('exclude', None))
		params = prepare_all(params)
		verbose_print('common:  %s' % params)
		verbose_print('common_excludes:  %s' % excludes)

		# union parameters
		excludes = union_all(excludes, platform_excludes)
		params = union_all(params, platform_params)
		
		config_prefix = os.path.join(self['TOP_DIR'], 'Make', self['ARCH'] + "-" + self['TOOL'])
		
		# read XXX-options.py config
		unfiltered_options = self.read_config(config_prefix + '-options.py')
		options = filter_with_regex(unfiltered_options, re.compile('(\w*)_(COMMON|' + self['MODE'].upper() + ')(|_' + target_type + ')$'))
		common_defines = self.read_config(os.path.join(self['TOP_DIR'], 'Make','common-defines.py'))
	   
		union_defines(options, common_defines)
		
		options = make_lists(options, keys)
		
		# read XXX-external_libs.py config
		external_libs = self.read_config(config_prefix + '-external_libs.py')
		external_libs = self.process_external_libs(external_libs)
		
		verbose_print('options: %s' % options)
		verbose_print('extlibs: %s' % external_libs)
		
		# Add config params
		params = union_all(params, options)

		# Add external_libs params
		if params.has_key('external_libs'):
			for lib in params['external_libs']:
				# First try external lib alias
				if external_libs.has_key(lib):
					params = union_all(params, make_lists(external_libs[lib], keys))
				else:
					# Otherwise use it as a default lib
					params['LIBS'] += [lib]
		
		verbose_print('common: %s' % params)

		# apply excludes
		params = sub_maps(params, excludes)

	def setup_precompiled_header(self, params):
		if not params.has_key('precompiled_header'):
			return

		if self['TOOL'] == 'msvc':
			# In Visual Studio for precompiled header there should be corresponding source file - "header.cpp" for "header.h" 
			precompiled_header = params['precompiled_header']
			precompiled_source = [x for x in params['source'] if str(x).endswith(precompiled_header.replace('.h', '.cpp'))][0]

			# Compile precompiled header
			pch_params = params.copy()
			pch_params.pop('target')
			pch_params.pop('source')

			pch = self.PCH(precompiled_source, **pch_params)

			# Add precompiled header to compilation params of other files
			params['source']  = [x for x in params['source'] if x != precompiled_source] + [pch[1]]
			params['PCH']     = pch[0]
			params['PCHSTOP'] = precompiled_header

		if self['TOOL'] == 'gcc':
			pass # Precompiled headers for gcc are not currently implemented
	
	# Generic builder. Do not call directly
	def edBuild(self, target_type, builder, **params):
		self['TARGET_TYPE'] = target_type
		self['TARGET_NAME'] = params['target']
		
		self.process_params(self['TARGET_TYPE'], params)
		self.setup_precompiled_header(params)
		
		# Compile resource files on Windows platform
		if platform.system() == 'Windows':
			params['LIBS'] += [ self.RES(x) for x in params.pop('resource', []) ]

		result = builder(**params)

		# Not linked dependencies
		if params.has_key('not_linked_dependencies'):
			self.Depends(result, map(self.Alias, params['not_linked_dependencies']))

		return result
	
	# Build executable program
	def edProgram(self, **params):
		target = self.edBuild('PROG', self.Program, **params)

		install = self.Install(self['INSTALL_BIN_DIR'], target)
		
		# Short alias so we can write 'scons main' instead of 'scons bin/x86_64/msvc_10.0/debug/main.exe'
		self.Alias(['install', params['target']], install)

		return target
		
	# Build static library
	def edStaticLibrary(self, **params):
		target = self.edBuild('LIB', self.StaticLibrary, **params)

		install = self.Install(self['INSTALL_LIB_DIR'], target) # .lib on windows, .a on mac and linux
		
		# Short alias so we can write 'scons shared_lib' instead of 'scons bin/x86_64/msvc_10.0/debug/shared_lib.dll'
		self.Alias(['install', params['target']], install)

		return target
		
	# Build shared library
	def edSharedLibrary(self, **params):
		target = self.edBuild('DLL', self.SharedLibrary, **params)

		install_bin = self.Install(self['INSTALL_BIN_DIR'], target[0])   # .dll on windows, .dylib on mac, .so on linux
		install_lib = self.Install(self['INSTALL_LIB_DIR'], target[1:2]) # .lib on windows, nothing on mac and linux
		self.Depends(install_lib, install_bin)
		
		# Short alias so we can write 'scons shared_lib' instead of 'scons lib/x86_64/msvc_10.0/debug/static_lib.lib'
		self.Alias(['install', params['target']], [install_bin, install_lib])

		return target

	# Build loadable module - same as shared library for Win and Linux. .bundle for mac
	def edLoadableModule(self, **params):
		target = self.edBuild('DLL', self.LoadableModule, **params)

		install_bin = self.Install(self['INSTALL_BIN_DIR'], target[0])   # .dll on windows, .bundle on mac, .so on linux
		install_lib = self.Install(self['INSTALL_LIB_DIR'], target[1:2]) # .lib on windows, nothing on mac and linux
		self.Depends(install_lib, install_bin)
		
		# Short alias so we can write 'scons shared_lib' instead of 'scons lib/x86_64/msvc_10.0/debug/static_lib.lib'
		self.Alias(['install', params['target']], [install_bin, install_lib])

		return target
		
	def msvsBuild(self, target_type, target_prefix, target_suffix, **params):
		self['TARGET_TYPE'] = target_type
		self['TARGET_NAME'] = params['target']
		self['PROJECT_NAME'], self['PROJECT_VERSION'] = (params.pop('project', params['target']) + '|').split('|')[:2]

		self.process_params(self['TARGET_TYPE'], params)
		
		modes  = [('x86', 'debug'), ('x86', 'release'), ('x86_64', 'debug'), ('x86_64', 'release')]
		
		def make_absolute(y):
			return map(lambda x: self.real_abspath(x), y)

		params['source']  = make_absolute(params['source'])
		params['srcs']    = params['source']
		if not params.has_key('include'):
		    params['include'] = []
		if not params.has_key('header'):
		    params['header'] = []
		params['incs']    = make_absolute(params['include'] + params['header']) 
		params['CPPPATH'] = make_absolute(params['CPPPATH'])
		
		params['auto_build_solution'] = 0
		params['target'] = os.path.join(self['PROJECT_DIR'], self['PROJECT_NAME'])
		params['variant'] = []
		params['buildtarget'] = map(lambda (arch, mode): 'tool=msvc\" \"arch=' + arch + '\" \"mode=' + mode + '\" \"' + self['TARGET_NAME'], modes)
		
		temp = {'ARCH': self['ARCH'], 'MODE': self['MODE']}
		def _lambda((arch, mode)):
			self._dict.update({'ARCH': arch, 'MODE': mode})
			return os.path.join(self['TOP_DIR'], self.get_install_bin_dir(), target_prefix + self['TARGET_NAME'] + target_suffix)
		
		params['runfile'] = map(_lambda, modes)
		self._dict.update(temp)

		# Store params for this version of this project   
		versions = self.msvsProjects.setdefault(self['PROJECT_NAME'], {})
		versions[ self['PROJECT_VERSION'] ] = params
	
	def msvsProgram(self, **params):
		self.msvsBuild('PROG', self['PROGPREFIX'], self['PROGSUFFIX'], **params)
		
	def msvsStaticLibrary(self, **params):
		self.msvsBuild('LIB', self['LIBPREFIX'], self['LIBSUFFIX'], **params)
		
	def msvsSharedLibrary(self, **params):
		self.msvsBuild('DLL', self['SHLIBPREFIX'], self['SHLIBSUFFIX'], **params)

	def msvsLoadableModule(self, **params):
		self.msvsBuild('DLL', self['LDMODULEPREFIX'], self['LDMODULESUFFIX'], **params)

	# Method to generate project files after we processed all sconscript files
	def generateProjects(self):
		if self['MODE'] != 'msvsproj':
			return

		for project, versions in self.msvsProjects.iteritems():
			project_params = None

			# Assemble project params from all project versions
			for version, params in versions.iteritems():
				if project_params == None:
					project_params = params
				else:
					project_params['buildtarget'] += params['buildtarget']
					project_params['runfile'] += params['runfile']

				project_params['variant'] += [x[0] + version + x[1] for x in [('Debug', '|Win32'), ('Release', '|Win32'), ('Debug', '|x64'), ('Release', '|x64')]]

			# Generate project
			target = self.MSVSProject(**project_params)
			self.Alias(['install', project_params['target']], target)
	
	# Recursive analog of Glob() function
	def RGlob(self, pattern):
		(directory, pattern) = os.path.split(pattern)
		
		node = self.Dir(directory)
		build_dir = node.abspath
		
		while node != node.srcnode():
			node = node.srcnode()
		
		result = []
		for root, dirs, files in os.walk(node.abspath):
			result += self.Glob(os.path.join(root.replace(node.abspath, build_dir), pattern))
			
		return result

	# Useful helper function
	def edSubdirs(self, subdirs):
		for subdir in subdirs:
			self.SConscript(os.path.join(subdir, 'SConscript'))

	def getUserVariable(self, name):
		if self.has_key('USER_VARIABLES') and self['USER_VARIABLES'].has_key(name):
			return self['USER_VARIABLES'][name]

		return None
