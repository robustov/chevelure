// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the SNDELAYMAYA_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// SNDELAYMAYA_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef SNDELAYMAYA_EXPORTS
#define SNDELAYMAYA_API __declspec(dllexport)
#else
#define SNDELAYMAYA_API __declspec(dllimport)
#endif

#include <maya/MDGContext.h>
#include <maya/MDagPath.h>
SNDELAYMAYA_API MDGContext& MDGContext_fsNormal(void);
SNDELAYMAYA_API MObject& MObject_kNullObject(void);
SNDELAYMAYA_API bool MDagPath_to(MObject& obj, MDagPath& path);
