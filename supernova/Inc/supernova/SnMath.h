#pragma once

#ifdef SNMATH_EXPORTS
#define SNMATH_API __declspec(dllexport)
#else
#define SNMATH_API __declspec(dllimport)
#endif

#include "ISnShaper.h"
#include "Util\STLStream.h"
#include "Util\DllProcedure.h"
class MObject;

// �������� �������
struct shaderExtern
{
	std::string type;
	std::string name;
};
template<class STREAM>
STREAM& operator >> (STREAM& out, shaderExtern& arg)
{
	out >> arg.type >> arg.name;
	return out;
}
std::string SNMATH_API mtorShaderNames(MObject obj, const char* attrname, std::vector<shaderExtern>* params=NULL);



