#pragma once

#define SN_DATAID 0xC0FFEE04

#include "Math/Math.h"
#include "Math/Polygon.h"
#include "Math/Ramp.h"
#include "MathNMaya/ParamTex.h"

#include "ISnShaper.h"
#include "Util\Stream.h"
#include "Util\MemStream.h"
#include "Util\STLStream.h"
#include "Util\DllProcedure.h"

//#include <maya/MPlug.h>
//#include <maya/MObject.h>
class MPlug;
class MObject;

namespace artist
{

	// ������� �������
	struct attr_decl
	{
		std::string name;
		sn::VALUETYPE type;
		sn::ATTRIBUTE_PLACE place;
		std::string defminmax;
		attr_decl(const char* name, sn::VALUETYPE type);
		bool operator<(const attr_decl& arg)const;
	};

	// ������ �� ����
	struct ShapeValue
	{
		// ����
		sn::ShapeShortcutPtr ssp;
//		sn::IShaper* shaper;
//		sn::IShape* shape;
		// �������� �����
		std::vector<attr_decl> attrlist;
	};

	struct IShape
	{
		virtual MObject getThis()=0;
		virtual MObject getSourceObject()=0;
		virtual MPlug getPlug(const char* name)=0;

		// ������ �������� (������� ������� � SnShape)
		virtual MObject getValue( const char* name, sn::VALUETYPE type,  sn::ATTRIBUTE_PLACE place=sn::UNIFORM_SHAPE, const char* defminmax="")=0;
		virtual bool getValue( bool& val, const char* name, sn::ATTRIBUTE_PLACE place, bool defval=false)=0;
		virtual bool getValue( int& val, const char* name, sn::ATTRIBUTE_PLACE place=sn::UNIFORM_SHAPE, int defval=0, int min=-666, int max=-666)=0;
		virtual bool getValue( float& val, const char* name, sn::ATTRIBUTE_PLACE place=sn::UNIFORM_SHAPE, float defval=0, float min=-666, float max=-666)=0;
		virtual bool getValue( Math::Vec3f& val, const char* name)=0;
		virtual bool getValue( Math::Matrix4f& val, const char* name)=0;
		virtual bool getValue( std::string& val, const char* name, sn::ATTRIBUTE_PLACE place, const char* defval="")=0;
		virtual bool getValue( ShapeValue& val, const char* name)=0;
		virtual bool getValue( std::vector<char>& data, const attr_decl& val)=0;
		virtual bool getValue( Math::Ramp& rampWidth, const char* name, float min = 0.f, float max = 1.f, float defval = 0.f)=0;
		virtual bool getValue( Math::ParamTex<unsigned char>& paramtex, const char* name, float defval = 0.f)=0;

		// ��������� �������� ���. ������� (������� ������� ������ �� ���� � ������������� �� ���� �����)
		virtual std::string mirrorAttr( MObject object, const char* attribute, int index=-1)=0;

		// ������ �������� (������� ������� � SnInstance)
		virtual sn::attr_t getAttr(const char* name, sn::VALUETYPE type, sn::ATTRIBUTE_PLACE place, const char* defminmax="")=0;
		virtual sn::attr_t getShaderAttr(const char* name, const char* type)=0;

		virtual void displayString(const char* text, ...)=0;

	};


}

inline artist::attr_decl::attr_decl(const char* name="", sn::VALUETYPE type=sn::VT_UNKNOWN)
{
	this->name = name;
	this->type = type;
}
inline bool artist::attr_decl::operator<(const artist::attr_decl& arg)const
{
	if(this->type<arg.type) return true;
	if(this->type>arg.type) return false;

//	if(this->place<arg.place) return true;
//	if(this->place<arg.place) return true;

	return strcmp( this->name.c_str(), arg.name.c_str())<0;
}
template<class STREAM>
STREAM& operator >> (STREAM& out, artist::attr_decl& arg)
{
	out >> arg.name;
	out >> *(int*)&arg.type;
	out >> *(int*)&arg.place;
	out >> arg.defminmax;
	return out;
}

template<class STREAM>
STREAM& operator >> (STREAM& out, artist::ShapeValue& arg)
{
	// ���������� ����
	out >> arg.ssp;
	out >> arg.attrlist;
	return out;
}