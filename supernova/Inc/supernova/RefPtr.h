#pragma once

template <class T>
class RefPtr
{
public:
	T* pss;
public:
	~RefPtr();
	RefPtr();
	RefPtr(T* shape);
	RefPtr(const RefPtr& arg);
	RefPtr& operator =(T* arg);
	RefPtr& operator =(const RefPtr& arg);
	const T* operator->()const;
	T* operator->();
	const T& operator*()const;
	T& operator*();
	bool isValid();
};

template<class STREAM, class T>
STREAM& operator >> (STREAM& out, RefPtr<T>& arg)
{
	if(out.isSaving())
	{
		out >> *arg.pss;
	}
	else
	{
		arg.pss = new T();
		out >> *arg.pss;
		arg.pss->addref();
	}
	return out;
}

template<class T>
inline RefPtr<T>::RefPtr()
{
	pss = 0;
}
template<class T>
inline RefPtr<T>::RefPtr(T* shape)
{
	pss = shape;
	pss->addref();
}
template<class T>
inline RefPtr<T>::RefPtr(const RefPtr<T>& arg)
{
	pss = arg.pss;
	pss->addref();
}
template<class T>
inline RefPtr<T>::~RefPtr()
{
	pss->delref();
}

template<class T>
inline RefPtr<T>& RefPtr<T>::operator =(T* arg)
{
	pss->delref();
	pss = arg;
	pss->addref();
	return *this;
}
template<class T>
inline RefPtr<T>& RefPtr<T>::operator =(const RefPtr<T>& arg)
{
	pss->delref();
	pss = arg.pss;
	pss->addref();
	return *this;
}
template<class T>
inline const T* RefPtr<T>::operator->()const
{
	return pss;
}
template<class T>
inline T* RefPtr<T>::operator->()
{
	return pss;
}
template<class T>
inline const T& RefPtr<T>::operator*()const
{
	return *pss;
}
template<class T>
inline T& RefPtr<T>::operator*()
{
	return *pss;
}

template<class T>
inline bool RefPtr<T>::isValid()
{
	if(!pss) 
		return false; 
	if(!pss->isValid())
		return false; 
	return true; 
}
