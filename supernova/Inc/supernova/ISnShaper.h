#pragma once
#include "Util\Stream.h"
#include "Util\MemStream.h"

#include "Math/Math.h"
#include "Math/Polygon.h"
#include <vector>

#include "mathNpixar\IPrman.h"
namespace cls
{
	struct IRender;
}
class MObject;
namespace artist
{
	struct IShape;
	struct ICharacter;
}

namespace sn
{
	enum VALUETYPE
	{
		VT_UNKNOWN			= 0,
		VT_BOOL				= 1,
		VT_INT				= 2,
		VT_FLOAT			= 3,
		VT_VECTOR3			= 4,
		VT_MATRIX4X4		= 5,
		VT_TRANSFORM		= 6,
		VT_STRING			= 7,
		VT_GEOMETRY			= 8,
		VT_FLOATARRAY		= 9,
		VT_VECTOR3ARRAY		= 10,
		VT_MATRIX4X4ARRAY	= 11,
		VT_SNSHAPE			= 12,
		VT_SNSHAPEARRAY		= 13,
		VT_COLOR			= 14,
	};
	enum ATTRIBUTE_PLACE
	{
		UNIFORM_SHAPE, 
		UNIFORM_PERSONAGE, 
		VARIABLE_SHAPE,
		VARIABLE_PERSONAGE,
	};
	enum STREAM_MODIFER
	{
		PER_UNKNOWN		= 0,
		PER_UNIFORM		= 1,
		PER_VERTEX		= 2,
		PER_FACE		= 3,
		PER_FACEVERTEX	= 4,
	};

	// �������
	struct attr_t
	{
		int id;
		STREAM_MODIFER modifer;
		VALUETYPE type;
		std::string name;

		attr_t(){id=-1;type=VT_UNKNOWN;modifer=PER_UNIFORM;}
		attr_t(const char* name, VALUETYPE type, STREAM_MODIFER modifer, int id=-1){this->id=id;this->modifer=modifer;this->name=name;this->type=type;};
		attr_t(const attr_t& arg){this->id=arg.id;this->modifer=arg.modifer;this->name=arg.name;this->type=arg.type;};
		attr_t& operator=(const attr_t& arg){this->id=arg.id;this->modifer=arg.modifer;this->name=arg.name;this->type=arg.type;return *this;};
		bool operator!()const {return !isValid();}
		bool isValid()const{return type!=VT_UNKNOWN;}
	};

	struct IRenderContext
	{
	};
	// �������� - ������ � ���������
	struct IPersonage
	{
		virtual void getValue(attr_t& attr, int& val){val = 0;};
		virtual void getValue(attr_t& attr, float& val){val = 0;};
		virtual void getValue(attr_t& attr, Math::Vec3f& val){val = Math::Vec3f(0, 0, 0);};
		virtual void getValue(attr_t& attr, Math::Matrix4f& val){val = Math::Matrix4f::id;};
		virtual void getValue(attr_t& attr, std::vector<float>& val){val.clear();};
		virtual void getValue(attr_t& attr, std::vector<Math::Matrix4f>& val){val.clear();};
	};
	// ������� ��� ������
	struct IShape
	{
		virtual void release()=0;
	};

	// ������� ��� ��������
	struct IShaper
	{
		virtual void release()=0;
		// �������� �����
		virtual sn::IShape* export_(
			artist::IShape& source,
			sn::IPersonage* personage
			)=0;
		// �������� �����
		virtual bool save(
			sn::IShape* snshape, 
			Util::Stream& stream
			)=0;
		// �������� �����
		virtual sn::IShape* load(
			Util::Stream& stream,
			sn::IPersonage* personage
			)=0;
		// ���������
		virtual void render(
			sn::IShape* snshape, 
			sn::IPersonage* personage, 
			const Math::Matrix4f& transform,
			IPrman* prman, 
			cls::IRender* render
			)=0;
		// ���������
		virtual void renderGL(
			sn::IShape* snshape, 
			sn::IPersonage* personage,
			const Math::Matrix4f& transform
			)=0;

		// box???? �� ������� ����!
		virtual bool getDirtyBox(
			sn::IShape* snshape, 
			const Math::Matrix4f& transform,
			Math::Box3f& box
			){return false;};
		// ���������
		virtual void renderOcs(
			sn::IShape* snshape, 
			sn::IPersonage* personage, 
			const Math::Matrix4f& transform,
			cls::IRender* render,
			int renderMode = 0xFFFFFFFF
			)=0;
	};
	typedef IShaper* (*tag_GetShaper)();

}
#include "ISnShapePtr.h"

template<class STREAM>
STREAM& operator >> (STREAM& out, sn::attr_t& arg)
{
	out >> arg.id;
	out >> arg.name;
	out >> *(int*)&arg.type;
	out >> *(int*)&arg.modifer;
	return out;
}
