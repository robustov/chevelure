#pragma once
#include "Util\DllProcedure.h"
#include "RefPtr.h"

namespace sn
{
	template <class T>
	struct Shortcut
	{
	public:
		int refcount;

		typedef T* (*tag_getproc)();

	public:
		std::string name;
		Util::DllProcedure dllproc;
		T* shaper;
		IShape* shape;
	public:
		Shortcut(){refcount=0;};
		Shortcut(Util::DllProcedure& dllproc, T* shaper, IShape* shape)
		{
			refcount=0;
			this->dllproc = dllproc;
			this->shaper  = shaper;
			this->shape   = shape;
		}
		void addref(){if(this) refcount++;};
		void delref(){if(this){ refcount--;if(refcount<=0) this->release();}};
		void release(){if(this) delete this;};
		bool isValid(){return shape!=0;}
	};
	typedef Shortcut<IShaper> ShapeShortcut;

	typedef RefPtr<ShapeShortcut> ShapeShortcutPtr;
	/*/
	class ShapeShortcutPtr
	{
	public:
		ShapeShortcut* pss;
	public:
		ShapeShortcutPtr();
		ShapeShortcutPtr(ShapeShortcut* shape);
		ShapeShortcutPtr(const ShapeShortcutPtr& arg);
		ShapeShortcutPtr& operator =(ShapeShortcut* arg);
		ShapeShortcutPtr& operator =(const ShapeShortcutPtr& arg);
		const ShapeShortcut* operator->()const;
		ShapeShortcut* operator->();
		const ShapeShortcut& operator*()const;
		ShapeShortcut& operator*();
		~ShapeShortcutPtr();
		bool isValid();
	};
	/*/
}

template<class STREAM, class T>
STREAM& operator >> (STREAM& out, sn::Shortcut<T>& arg)
{
	out>>arg.name;
	out>>arg.dllproc;
	if(out.isSaving())
	{
		arg.shaper->save(arg.shape, out);
	}
	else
	{
		sn::Shortcut<T>::tag_getproc gs = (sn::Shortcut<T>::tag_getproc)arg.dllproc.proc;
		if( gs)
		{
			arg.shaper = (*gs)();
			arg.shape = arg.shaper->load(out, NULL);
		}
	}
	return out;
}


/*/
template<class STREAM>
STREAM& operator >> (STREAM& out, sn::ShapeShortcutPtr& arg)
{
	if(out.isSaving())
	{
		out >> *arg.pss;
	}
	else
	{
		arg.pss = new sn::ShapeShortcut();
		out >> *arg.pss;
		arg.pss->addref();
	}
	return out;
}

inline sn::ShapeShortcutPtr::ShapeShortcutPtr()
{
	pss = 0;
}
inline sn::ShapeShortcutPtr::ShapeShortcutPtr(sn::ShapeShortcut* shape)
{
	pss = shape;
	pss->addref();
}
inline sn::ShapeShortcutPtr::ShapeShortcutPtr(const sn::ShapeShortcutPtr& arg)
{
	pss = arg.pss;
	pss->addref();
}
inline sn::ShapeShortcutPtr::~ShapeShortcutPtr()
{
	pss->delref();
}

inline sn::ShapeShortcutPtr& sn::ShapeShortcutPtr::operator =(sn::ShapeShortcut* arg)
{
	pss->delref();
	pss = arg;
	pss->addref();
	return *this;
}
inline sn::ShapeShortcutPtr& sn::ShapeShortcutPtr::operator =(const sn::ShapeShortcutPtr& arg)
{
	pss->delref();
	pss = arg.pss;
	pss->addref();
	return *this;
}
inline const sn::ShapeShortcut* sn::ShapeShortcutPtr::operator->()const
{
	return pss;
}
inline sn::ShapeShortcut* sn::ShapeShortcutPtr::operator->()
{
	return pss;
}
inline const sn::ShapeShortcut& sn::ShapeShortcutPtr::operator*()const
{
	return *pss;
}
inline sn::ShapeShortcut& sn::ShapeShortcutPtr::operator*()
{
	return *pss;
}

inline bool sn::ShapeShortcutPtr::isValid()
{
	if(!pss) 
		return false; 
	if(!pss->shape)
		return false; 
	return true; 
}
/*/
