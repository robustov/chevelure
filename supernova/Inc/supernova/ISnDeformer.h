#pragma once

#include "ISnShaper.h"

namespace sn
{
	struct IDeformedShape
	{
		virtual int vertsCount()=0;
		virtual int faceCount()=0;
		virtual int facevertsCount()=0;
		virtual int getVertexForFacevertex(int fv)=0;

		virtual attr_t getAttr(const char* name, VALUETYPE place)=0;
		virtual void* getStream(attr_t& attr)=0;
	};

	typedef std::map< std::string, std::vector<char> > tag_TokenParameters;

	struct IDeformer
	{
		virtual void release()=0;
		// �������� ������
		virtual sn::IShape* export_(
			artist::IShape& source,
			IDeformedShape& deformedShape,
			Math::Box3f& dirtyBox
			)=0;
		// �������� �����
		virtual bool save(
			sn::IShape* snshape, 
			Util::Stream& stream
			)=0;
		// �������� ������
		virtual sn::IShape* load(
			Util::Stream& stream,
			sn::IPersonage* personage
//			IDeformedShape& deformedShape
			)=0;
		// ����������
		virtual void filter(
			sn::IShape* snshape, 
			sn::IPersonage* personage, 
			IDeformedShape& deformedShape
			)=0;
		// �������������� ��������� �������
		virtual void getShaderParameters(
			const char* shadertype,
			sn::IShape* snshape, 
			sn::IPersonage* personage,
			tag_TokenParameters& params		// ��������� ������� :(
			){};

		// �� ���� ������ �������� ���������?
		virtual void getGeometrySource(
			artist::IShape& source,
			MObject& geom
			){};
	
	};

	typedef Shortcut<IDeformer> DeformerShortcut;
	typedef RefPtr<DeformerShortcut> DeformerShortcutPtr;

	typedef IDeformer* (*tag_GetDeformer)();

}
