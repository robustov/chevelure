#pragma once

//#include "pre.h"
#define REQUIRE_IOSTREAM
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include <maya/MIOStream.h>
#include <vector>

#include "IArtistShape.h"
#include "ISnShaper.h"
#include "SnMath.h"
#include "MathNMaya/ParamTex.h"

/*/
#ifdef SNMAYA_EXPORTS
#define SNMAYA_API __declspec(dllexport)
#else
#define SNMAYA_API __declspec(dllimport)
#endif
/*/

#pragma warning ( disable :4251)
/*/
	USING:
	{
		MFnTypedAttribute fnAttr;
		MObject newAttr = fnAttr.create( 
			"fullName", "briefName", SnData::id );
	}
/*/


class SnData : public MPxData
{
public:
	SnData();
	virtual ~SnData(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

public:
	static MTypeId id;
	static const MString typeName;

	sn::ShapeShortcutPtr ssp;
//	std::vector<char> data;
	std::vector<artist::attr_decl> attrlist;

	Math::ParamTex2d<unsigned char, Math::EJT_MUL> weight;

};
