// SnDelayMaya.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "supernova/SnDelayMaya.h"
#include <maya/MObject.h>


// This is an example of an exported function.
SNDELAYMAYA_API MDGContext& MDGContext_fsNormal(void)
{
	return MDGContext::fsNormal;
}
SNDELAYMAYA_API MObject& MObject_kNullObject(void)
{
	return MObject::kNullObj;
}
SNDELAYMAYA_API bool MDagPath_to(MObject& obj, MDagPath& path)
{
	if( !MDagPath::getAPathTo( obj, path ))
		return false;
	return true;
}



BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		{
#ifdef HAIR_TRACELOG
			char filename[512]; GetModuleFileName( (HMODULE)hModule, filename, 512); fprintf(stderr, "%s\n", filename);
#endif
			break;
		}
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
    return TRUE;
}
