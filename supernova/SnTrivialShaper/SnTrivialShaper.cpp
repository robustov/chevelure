// SnTrivialShaper.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "SnTrivialShaper.h"
#include "SnDelayMaya.h"
#include "IArtistShape.h"

#include <maya\MObject.h> 
#include <maya\MFnDependencyNode.h> 
#include <maya\MFnSkinCluster.h>
#include <maya\MItDependencyGraph.h>
#include <maya\MFnMesh.h>
#include <maya\MItMeshPolygon.h>
#include <ri.h>
#include <gl/gl.h>
#include "ISnDeformer.h"
#include "SnMath.h"

template<class STREAM>
STREAM& operator >> (STREAM& out, TrivialShaper::Shape& arg)
{
	out >> arg.surfaceShader >> arg.displaceShader;
	out >> arg.surfaceShaderParams >> arg.displaceShaderParams;

	out >> arg.facevertcount;
	out >> arg.polygons;
	out >> arg.verts;
	out >> arg.norms;
	out >> arg.u;
	out >> arg.v;
	return out;
}

extern "C" 
{
	__declspec(dllexport) sn::IShaper* getShaper();
}

sn::IShaper* getShaper()
{
	return new TrivialShaper();
}

// ���������
void TrivialShaper::render(
	sn::IShape* snshape, 
	sn::IPersonage* personage,
	IPrman* prman)
{
	Shape* shape = (Shape*)snshape;

	std::vector<Math::Vec3f>& shape_verts = shape->verts;

	int npolygons = (int)shape->polygons.size();
	int facevertcount = shape->facevertcount;

	RtInt* loops = new RtInt[npolygons];
	RtInt* nverts = new RtInt[npolygons];
	RtInt* verts = new RtInt[facevertcount];
	
	int vertscount=0;
	for(int i=0; i<npolygons; i++)
	{
		loops[i] = 1;
		nverts[i] = shape->polygons[i].size();

		for(int v=0; v<nverts[i]; v++, vertscount++)
		{
			verts[vertscount] = shape->polygons[i][v];
		}
	}

	RtToken names[] = {
		"P",
		"facevarying normal N",
		"facevarying float s", 
		"facevarying float t", 
	};
	RtPointer values[] = 
	{
		(float*)&shape_verts[0],
		(float*)&shape->norms[0],
		(float*)&shape->u[0],
		(float*)&shape->v[0],
	};

	prman->RiAttributeBegin();
	if( !shape->surfaceShader.empty())
	{
		std::vector<RtToken> tokens;
		std::vector<RtPointer> parms;
		std::vector<char> datas(1, 0);
		datas.reserve(1024);
		std::map< std::string, sn::attr_t>::iterator it = shape->surfaceShaderParams.begin();
		for( ; it!=shape->surfaceShaderParams.end(); it++)
		{
			Math::Vec3f data;
			personage->getValue(it->second, data);
			tokens.push_back( (char*)it->first.c_str());
//			tokens.push_back( "color Constant_SurfaceColor");
			parms.push_back( &datas[datas.size()]);
			datas.insert( datas.end(), (char*)&data, (char*)&data + sizeof(data));
		}
		/*/
		Math::Vec3f data(1, 2, 3);
		tokens.push_back( "color Constant_SurfaceColor");
		parms.push_back( &data);
		/*/

		prman->RiSurfaceV((char*)shape->surfaceShader.c_str(), tokens.size(), &tokens[0], &parms[0]);
	}
	if( !shape->displaceShader.empty())
		prman->RiDisplacement((char*)shape->displaceShader.c_str(), NULL);

		prman->RiPointsGeneralPolygonsV( npolygons, loops, nverts, verts, sizeof(names)/sizeof(RtToken), names, values );
	prman->RiAttributeEnd();
}

// �������� �����
sn::IShape* TrivialShaper::export(
	artist::IShape& source,
	sn::IPersonage* personage
	)
{
	TrivialShaper::Shape& shape = *(new TrivialShaper::Shape());
	MObject src = source.getSourceObject();

	// �������:
	std::vector<shaderExtern> surfaceShaderParams, displaceShaderParams;
	shape.surfaceShader = mtorShaderNames( source.getThis(), "slimSurf", &surfaceShaderParams);
	shape.displaceShader = mtorShaderNames( source.getThis(), "slimDispl", &displaceShaderParams);

	for( unsigned p=0; p<surfaceShaderParams.size(); p++)
	{
//		source.getValue(shape.surfaceShaderParams[p].name.c_str(), sn::VT_COLOR);
		sn::attr_t attr = source.getShaderAttr(surfaceShaderParams[p].name.c_str(), surfaceShaderParams[p].type.c_str());
		if( !attr) continue;
		std::string name = surfaceShaderParams[p].type + " " + surfaceShaderParams[p].name;
		shape.surfaceShaderParams[name] = attr;
	}

	std::string inMesh = source.mirrorAttr( src, "outMesh");
	MObject meshobj = source.getValue(inMesh.c_str(), sn::VT_GEOMETRY, sn::UNIFORM_SHAPE);
	if( meshobj.isNull())
		return NULL;

	MFnMesh fnMesh(meshobj);

	int numPoints = fnMesh.numVertices();
	int numNormals = fnMesh.numNormals();
	int numSTs = fnMesh.numUVs();
	int numFaces = fnMesh.numPolygons();
	int numFaceVertices = fnMesh.numFaceVertices();

	MFloatVectorArray normals;
	fnMesh.getNormals( normals );

	shape.facevertcount = 0;
	shape.polygons.resize(numFaces);
	shape.verts.resize(numPoints);
	shape.norms.resize(numFaceVertices);
	if(numSTs)
	{
		shape.u.resize(numFaceVertices);
		shape.v.resize(numFaceVertices);
	}

	MItMeshPolygon polyIt( meshobj);
	for( int face=0, faceVertex=0; polyIt.isDone() == false; polyIt.next(), face++ ) 
	{
		int count = polyIt.polygonVertexCount();
		Math::Polygon32& poly = shape.polygons[face];
		poly.resize(count);
		shape.facevertcount += count;

		for( int v=0; v<count; v++, faceVertex++) 
		{
			int vertex = polyIt.vertexIndex( v );
			poly[v] = vertex;

			MPoint point = polyIt.point( v, MSpace::kObject );
			Math::Vec3f& p = shape.verts[vertex];
			p.x = (float)point.x;
			p.y = (float)point.y;
			p.z = (float)point.z;

			int normal = polyIt.normalIndex( v );
			MVector norm = normals[normal];
			Math::Vec3f& n = shape.norms[faceVertex];
			n.x = (float)norm.x;
			n.y = (float)norm.y;
			n.z = (float)norm.z;

			if(numSTs)
			{
				float S, T;
				fnMesh.getPolygonUV( face, v, S, T );
				shape.u[faceVertex] = S;
				shape.v[faceVertex] = 1-T;
			}
		}
	}
	return &shape;
}
// �������� �����
bool TrivialShaper::save(
	sn::IShape* snshape, 
	Util::Stream& stream
	)
{
	Shape* shape = (Shape*)snshape;
	stream >> *shape;
	return true;
}

// ���������
void TrivialShaper::renderGL(
	sn::IShape* snshape, 
	sn::IPersonage* personage
	)
{
	Shape* shape = (Shape*)snshape;

	glPushAttrib(GL_POLYGON_BIT|GL_ENABLE_BIT);
	glPolygonMode(GL_FRONT, GL_LINE);
	glPolygonMode(GL_BACK, GL_LINE);
	glDisable(GL_CULL_FACE);
	for(unsigned p=0; p<shape->polygons.size(); p++)
	{
		Math::Polygon32& poly = shape->polygons[p];
		glBegin(GL_POLYGON);
		for(int v=0; v<poly.size(); v++)
		{
			int vi = poly[v];
			Math::Vec3f& vert = shape->verts[vi];
			glVertex3f( vert.x, vert.y, vert.z);
		}
		glEnd();
	}
	glPopAttrib();
}

// �������� �����
sn::IShape* TrivialShaper::load(
	Util::Stream& stream,
	sn::IPersonage* personage)
{
	Shape* shape = new Shape();
	stream >> *shape;
	return shape;
}
