#pragma once
#include "ISnShaper.h"
#include "Util\STLStream.h"
#include "SnMath.h"

struct TrivialShaper : public sn::IShaper
{
	virtual void release(){delete this;}

	// �������� �����
	virtual sn::IShape* export(
		artist::IShape& source,
		sn::IPersonage* personage
		);
	// �������� �����
	virtual bool save(
		sn::IShape* snshape, 
		Util::Stream& stream
		);
	// �������� �����
	virtual sn::IShape* load(
		Util::Stream& stream,
		sn::IPersonage* personage);

	// ���������
	virtual void render(
		sn::IShape* shape, 
		sn::IPersonage* personage,
		IPrman* prman
		);
	// ���������
	virtual void renderGL(
		sn::IShape* snshape, 
		sn::IPersonage* personage
		);

	struct Shape : public sn::IShape
	{
		// �������:
		std::string surfaceShader, displaceShader;
		std::map< std::string, sn::attr_t> surfaceShaderParams;
		std::map< std::string, sn::attr_t> displaceShaderParams;

		int facevertcount;
		std::vector<Math::Polygon32> polygons;
		std::vector<Math::Vec3f> verts;
		std::vector<Math::Vec3f> norms;
		std::vector<float> u;
		std::vector<float> v;
		virtual void release(){delete this;};
	};
};

