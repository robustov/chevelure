#include "stdafx.h"
#include "SnSkinDeformer.h"
#include "SnDelayMaya.h"

#include <maya\MObject.h> 
#include <maya\MFnDependencyNode.h> 
#include <maya\MFnSkinCluster.h>
#include <maya\MItDependencyGraph.h>
#include <maya\MFnMatrixData.h>
#include <maya\MMatrix.h>
#include <maya\MDagPathArray.h>
#include <maya\MItGeometry.h>
#include <maya\MDagPath.h>
#include <maya\MFnTransform.h>

#include "SnDelayMaya.h"
#include "IArtistShape.h"
#include <maya\MFnMesh.h>
#include <maya\MItMeshPolygon.h>
#include <ri.h>
#include <gl/gl.h>
#include "mathNmaya/mathNmaya.h"

extern "C" 
{
	__declspec(dllexport) sn::IDeformer* getDeformer();
}

sn::IDeformer* getDeformer()
{
	return new SnSkinDeformer();
}

template<class STREAM>
STREAM& operator >> (STREAM& out, SnSkinDeformer::Shape::Joint& arg)
{
	out >> arg.inv_bindpos;
	out >> arg.testpos;
	out >> arg.pos;
	out >> arg.vertexweights;
	return out;
}

template<class STREAM>
STREAM& operator >> (STREAM& out, SnSkinDeformer::Shape& arg)
{
	out >> arg.bTestMode;
	out >> arg.preTransform;
	out >> arg.joints;
	out >> arg.a_verts >> arg.a_norms;
	return out;
}

MObject SnSkinDeformer::getSkinCluster(artist::IShape& source)
{
	MStatus stat;
	MObject src = source.getSourceObject();

	MFnDependencyNode dn(src, &stat);
	if(!stat)
		return MObject_kNullObject();
	MPlug inmeshplug = dn.findPlug("inMesh", &stat);
	if(!stat)
		return MObject_kNullObject();
	MPlugArray plugarray;
	inmeshplug.connectedTo(plugarray, true, false, &stat);
	if(!stat || plugarray.length()!=1)
		return MObject_kNullObject();
	inmeshplug = plugarray[0];

	MObject skin = inmeshplug.node();
	MFnSkinCluster skinCluster(skin, &stat);
	if(!stat)
		return MObject_kNullObject();
	return skin;
}


// �� ���� ������ �������� ���������?
void SnSkinDeformer::getGeometrySource(
	artist::IShape& source,
	MObject& geom
	)
{
	MStatus stat;
	MObject skin = this->getSkinCluster(source);
	MFnSkinCluster skinCluster(skin, &stat);

	// �������� ���������
	MObjectArray inobjects;
	skinCluster.getInputGeometry(inobjects);
	if( inobjects.length()!=1)
	{
		// ������ ��� �� �...
		printf("INVALID SKIN CLUSTER: InputGeometry = %d\n", inobjects.length());
		return;
	}
	if( !inobjects[0].hasFn(MFn::kMesh))
	{
		printf("SOURCE GEOMETRY NOT MESH\n");
		return;
	}
	MObject meshobj = inobjects[0];
	if( meshobj.isNull())
		return;

	std::string meshattr = source.mirrorAttr( meshobj, "outMesh");
	meshobj = source.getValue(meshattr.c_str(), sn::VT_GEOMETRY, sn::UNIFORM_SHAPE);

	geom = meshobj;
}

// �������� ������
sn::IShape* SnSkinDeformer::export(
	artist::IShape& source,
	sn::IDeformedShape& deformedShape,
	Math::Box3f& dirtyBox
	)
{
	MStatus stat;

	MDagPath path;
	if( !MDagPath::getAPathTo(source.getSourceObject(), path))
		return NULL;

	MObject skin = this->getSkinCluster(source);
	MFnSkinCluster skinCluster(skin, &stat);

	// �������� ���������
	MObjectArray inobjects;
	skinCluster.getInputGeometry(inobjects);
	if( inobjects.length()!=1)
	{
		// ������ ��� �� �...
		printf("INVALID SKIN CLUSTER: InputGeometry = %d\n", inobjects.length());
		return NULL;
	}
	if( !inobjects[0].hasFn(MFn::kMesh))
	{
		printf("SOURCE GEOMETRY NOT MESH\n");
		return NULL;
	}
	MObject meshobj = inobjects[0];
	if( meshobj.isNull())
		return NULL;


	// �����
	MPlug bindPreMatrixArrayPlug = skinCluster.findPlug("bindPreMatrix", &stat);
	MDagPathArray infs;
	unsigned int nInfs = skinCluster.influenceObjects(infs, &stat);

	SnSkinDeformer::Shape& shape = *(new SnSkinDeformer::Shape());
	shape.a_verts = deformedShape.getAttr("P", sn::VT_VECTOR3);
	shape.a_norms = deformedShape.getAttr("N", sn::VT_VECTOR3);

	// ������������� ������� �� ������ bind
	{
		MPlug bindGeomMatrixes = skinCluster.findPlug("geomMatrix", &stat);
		MObject dataObject;
		bindGeomMatrixes.getValue( dataObject, MDGContext_fsNormal());
		MFnMatrixData matDataFn( dataObject );
		MMatrix objPreTransform = matDataFn.matrix();
		copy( shape.preTransform, objPreTransform);
	}
	source.getValue(shape.bTestMode, "TestMode", sn::UNIFORM_SHAPE, false);

	shape.joints.resize(nInfs);
	int numPoints = deformedShape.vertsCount();
	for(unsigned i = 0; i<infs.length(); i++)
	{
		MObject obj = infs[i].node();
		if( !obj.hasFn( MFn::kJoint))
			continue;

		MDagPath jointpath;
		if( !MDagPath::getAPathTo(infs[i].node(), jointpath))
			return NULL;

		MFnTransform dagNode(infs[i].node());
		std::string jointName = dagNode.fullPathName().asChar();
		Math::Matrix4f invBind, world;
		sn::attr_t pos;
		{
			// ������� �������
			std::string attrname = source.mirrorAttr(infs[i].node(), "worldMatrix", 0);
			MObject matobj = source.getValue( attrname.c_str(), sn::VT_MATRIX4X4, sn::UNIFORM_SHAPE);
			MMatrix wm = MFnMatrixData( matobj ).matrix();
			copy( world, wm);

			pos = source.getAttr(attrname.c_str(), sn::VT_MATRIX4X4, sn::UNIFORM_SHAPE);
		}

		{
			// ������� �� ������ ��������
			MPlug bindPreMatrixPlug = bindPreMatrixArrayPlug.elementByLogicalIndex(i, &stat);
			MObject dataObject;
			bindPreMatrixPlug.getValue( dataObject, MDGContext_fsNormal() );
			MFnMatrixData matDataFn ( dataObject );
			MMatrix invMat = matDataFn.matrix();
			copy( invBind, invMat);
		}

		// ���������
		Shape::Joint& dst = shape.joints[i];
		dst.vertexweights.resize(numPoints);
		dst.inv_bindpos = invBind;
		dst.testpos = world;
		dst.pos = pos;
	}

	// ���� � ������� ������
	MItGeometry gIter(meshobj);
	for ( int v=0; !gIter.isDone(); gIter.next(), v++ ) 
	{
		MObject comp = gIter.component(&stat);
		MFloatArray wts;
		unsigned int infCount;
		stat = skinCluster.getWeights( path, comp, wts, infCount);
		if(infCount != nInfs) 
		{
			return NULL;
		}
		for(unsigned i=0; i<nInfs; i++)
		{
			Shape::Joint& dst = shape.joints[i];
			dst.vertexweights[v] = wts[i];
		}
	}
	return &shape;
}

// �������� �����
bool SnSkinDeformer::save(
	sn::IShape* snshape, 
	Util::Stream& stream
	)
{
	Shape* shape = (Shape*)snshape;
	stream >> *shape;
	return true;
}

// �������� �����
sn::IShape* SnSkinDeformer::load(
	Util::Stream& stream,
	sn::IPersonage* personage)
{
	Shape* shape = new Shape();
	stream >> *shape;
	return shape;
}

// filter
void SnSkinDeformer::filter(
	sn::IShape* snshape, 
	sn::IPersonage* personage,
	sn::IDeformedShape& deformedShape
	)
{
	Shape* shape = (Shape*)snshape;

	std::vector<Math::Matrix4f> jointpos(shape->joints.size());
	for(unsigned j=0; j<shape->joints.size(); j++)
	{ 
		Shape::Joint& joint = shape->joints[j];
		if( shape->bTestMode)
			jointpos[j] = joint.testpos;
		else
			personage->getValue(joint.pos, jointpos[j]);
	}


	// ����������!
	Math::Vec3f* verts = (Math::Vec3f*)deformedShape.getStream(shape->a_verts);
	Math::Vec3f* norms = (Math::Vec3f*)deformedShape.getStream(shape->a_norms);

	if( !verts) return;
	for(int v=0; v<deformedShape.vertsCount(); v++)
	{
		Math::Vec3f& src = verts[v];

		Math::Vec3f dst(0, 0, 0);
		for(unsigned j=0; j<shape->joints.size(); j++)
		{
			Shape::Joint& joint = shape->joints[j];
			float w = joint.vertexweights[v];
			if( w<1e-4) continue;

			Math::Matrix4f m = jointpos[j]*joint.inv_bindpos;
			Math::Vec3f t = src;
			t = shape->preTransform*t;
			t = joint.inv_bindpos*t;
			t = jointpos[j]*t;
			t = w*t;
			dst = dst + t;
		}
		verts[v] = dst;

		/*/
		if(norms && shape->a_norms.modifer==sn::PER_VERTEX)
		{
			Math::Vec3f srcN = norms[v];
		}
		/*/
	}
	if( norms && shape->a_norms.modifer==sn::PER_FACEVERTEX)
	{
		for(int fv=0; fv<deformedShape.faceCount(); fv++)
		{
			int v = deformedShape.getVertexForFacevertex(fv);
			Math::Vec3f& src = norms[fv];

			Math::Vec3f dst(0, 0, 0);
			for(unsigned j=0; j<shape->joints.size(); j++)
			{
				Shape::Joint& joint = shape->joints[j];
				float w = joint.vertexweights[v];
				if( w<1e-4) continue;

				Math::Matrix4f m = jointpos[j]*joint.inv_bindpos;
				Math::Vec3f t = src;
				t = Math::Matrix4As3f(shape->preTransform)*t;
				t = Math::Matrix4As3f(joint.inv_bindpos)*t;
				t = Math::Matrix4As3f(jointpos[j])*t;
				t = w*t;
				dst = dst + t;
			}
			dst.normalize();
			norms[v] = dst;
		}
	}
}
