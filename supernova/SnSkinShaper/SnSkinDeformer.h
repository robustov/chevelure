#pragma once
#include "ISnDeformer.h"
#include "Util\STLStream.h"

struct SnSkinDeformer : public sn::IDeformer
{
	virtual void release(){delete this;}
	// �������� ������
	virtual sn::IShape* export(
		artist::IShape& source,
		sn::IDeformedShape& deformedShape,
		Math::Box3f& dirtyBox
		);
	// �������� �����
	virtual bool save(
		sn::IShape* snshape, 
		Util::Stream& stream
		);
	// �� ���� ������ �������� ���������?
	virtual void getGeometrySource(
		artist::IShape& source,
		MObject& geom
		);
	// �������� ������
	virtual sn::IShape* load(
		Util::Stream& stream,
		sn::IPersonage* personage
		);
	// ����������
	virtual void filter(
		sn::IShape* snshape, 
		sn::IPersonage* personage, 
		sn::IDeformedShape& deformedShape);

	struct Shape : public sn::IShape
	{
		bool bTestMode;	//������������ ����������� ������� 
		Math::Matrix4f preTransform;

		struct Joint
		{
			Math::Matrix4f inv_bindpos;
			Math::Matrix4f testpos;
			sn::attr_t pos;
			std::vector<float> vertexweights;
		};
		std::vector<Joint> joints;
		sn::attr_t a_verts, a_norms;
		virtual void release(){delete this;};
	};

	MObject getSkinCluster(artist::IShape& source);

};

