#pragma once
#include "ISnShaper.h"
#include "Util\STLStream.h"

struct SkinShaper : public sn::IShaper
{
	static int currentversion;

	virtual void release(){delete this;}

	// ������
	virtual int getVersion(){return currentversion;};

	// filter
	virtual void filter(
		sn::IShape* snshape, 
		sn::IPersonage* personage, 
		std::vector<Math::Vec3f>& data);

	// �������� �����
	virtual bool export(
		Util::Stream& stream,
		artist::IShape& source
		);
	// �������� �����
	virtual sn::IShape* load(
		Util::Stream& stream,
		sn::IPersonage* personage);
	// ���������
	virtual void render(
		sn::IShape* shape, 
		sn::IPersonage* personage, 
		IPrman* context);
	// ���������
	virtual void renderGL(
		sn::IShape* snshape, 
		sn::IPersonage* personage
		);

	struct Shape : public sn::IShape
	{
		int facevertcount;
		struct Joint
		{
			Math::Matrix4f inv_bindpos;
			Math::Matrix4f testpos;
			sn::attr_t pos;
			std::vector<float> vertexweights;
		};
		std::vector<Math::Polygon32> polygons;
		std::vector<Math::Vec3f> verts;
		std::vector<Math::Vec3f> norms;
		std::vector<float> u;
		std::vector<float> v;
		std::vector<Joint> joints;
		virtual void release(){delete this;};
	};

};

template<class STREAM>
STREAM& operator >> (STREAM& out, SkinShaper::Shape::Joint& arg)
{
	out >> arg.inv_bindpos;
	out >> arg.testpos;
	out >> arg.pos;
	out >> arg.vertexweights;
	return out;
}

template<class STREAM>
STREAM& operator >> (STREAM& out, SkinShaper::Shape& arg)
{
	out >> arg.facevertcount;
	out >> arg.polygons;
	out >> arg.verts;
	out >> arg.norms;
	out >> arg.u;
	out >> arg.v;
	out >> arg.joints;
	return out;
}
