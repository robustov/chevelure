// SnSkinShaper.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "SnSkinShaper.h"
#include "SnDelayMaya.h"

#include <maya\MObject.h> 
#include <maya\MFnDependencyNode.h> 
#include <maya\MFnSkinCluster.h>
#include <maya\MItDependencyGraph.h>
#include <maya\MFnMatrixData.h>
#include <maya\MMatrix.h>
#include <maya\MDagPathArray.h>
#include <maya\MItGeometry.h>
#include <maya\MDagPath.h>
#include <maya\MFnTransform.h>

#include "SnDelayMaya.h"
#include "IArtistShape.h"
#include <maya\MFnMesh.h>
#include <maya\MItMeshPolygon.h>
#include <ri.h>
#include <gl/gl.h>
#include "mathNmaya/mathNmaya.h"

int SkinShaper::currentversion;

extern "C" 
{
	__declspec(dllexport) sn::IShaper* getShaper();
}

sn::IShaper* getShaper()
{
	return new SkinShaper();
}

// filter
void SkinShaper::filter(
	sn::IShape* snshape, 
	sn::IPersonage* personage, 
	std::vector<Math::Vec3f>& data
	)
{
	Shape* shape = (Shape*)snshape;

	std::vector<Math::Matrix4f> jointpos(shape->joints.size());
	for(unsigned j=0; j<shape->joints.size(); j++)
	{ 
		Shape::Joint& joint = shape->joints[j];
//		personage->getValue(joint.pos, jointpos[j]);
		jointpos[j] = joint.testpos;
	}

	for(unsigned v=0; v<data.size(); v++)
	{
		Math::Vec3f& src = data[v];
		Math::Vec3f dst(0, 0, 0);

		for(unsigned j=0; j<shape->joints.size(); j++)
		{
			Shape::Joint& joint = shape->joints[j];

			Math::Matrix4f m = jointpos[j]*joint.inv_bindpos;
			Math::Vec3f t = src;
			t = m*t;
//			t = jointpos[j]*t;
//			t = joint.inv_bindpos*t;
			float w = joint.vertexweights[v];
			t = w*t;
			dst = dst + t;
		}
		data[v] = dst;
	}
}

// ���������
void SkinShaper::render(
	sn::IShape* snshape, 
	sn::IPersonage* personage, 
	IPrman* context)
{
	Shape* shape = (Shape*)snshape;

	int npolygons = (int)shape->polygons.size();
	int facevertcount = shape->facevertcount;

	RtInt* loops = new RtInt[npolygons];
	RtInt* nverts = new RtInt[npolygons];
	RtInt* verts = new RtInt[facevertcount];
	
	int vertscount=0;
	for(int i=0; i<npolygons; i++)
	{
		loops[i] = 1;
		nverts[i] = shape->polygons[i].size();

		for(int v=0; v<nverts[i]; v++, vertscount++)
		{
			verts[vertscount] = shape->polygons[i][v];
		}
	}

	RtToken names[] = {
		"P",
		"facevarying normal N",
		"facevarying float S", 
		"facevarying float T", 
	};
	RtPointer values[] = 
	{
		(float*)&shape->verts[0],
		(float*)&shape->norms[0],
		(float*)&shape->u[0],
		(float*)&shape->v[0],
	};

	RiPointsGeneralPolygonsV( npolygons, loops, nverts, verts, sizeof(names)/sizeof(RtToken), names, values );
}

// �������� �����
bool SkinShaper::export(
	Util::Stream& stream,
	artist::IShape& source
	)
{
	MStatus stat;
	MPlug inmeshplug = source.getPlug("inMesh");
	if( inmeshplug.node().isNull())
		return false;
	MPlugArray plugarray;
	inmeshplug.connectedTo(plugarray, true, false, &stat);
	if(!stat || plugarray.length()!=1)
		return false;
	inmeshplug = plugarray[0];

	MDagPath path;
	if( !MDagPath::getAPathTo(inmeshplug.node(), path))
		return false;

	MFnDependencyNode dn(inmeshplug.node(), &stat);
	if(!stat)
		return false;
	inmeshplug = dn.findPlug("inMesh", &stat);
	if(!stat)
		return false;
	inmeshplug.connectedTo(plugarray, true, false, &stat);
	if(!stat || plugarray.length()!=1)
		return false;
	inmeshplug = plugarray[0];

	MObject skin = inmeshplug.node();
	MFnSkinCluster skinCluster(skin, &stat);
	if(!stat)
		return false;

	// �������� ���������
	MObjectArray inobjects;
	skinCluster.getInputGeometry(inobjects);
	if( inobjects.length()!=1)
	{
		// ������ ��� �� �...
		printf("INVALID SKIN CLUSTER: InputGeometry = %d\n", inobjects.length());
		return false;
	}
	if( !inobjects[0].hasFn(MFn::kMesh))
	{
		printf("SOURCE GEOMETRY NOT MESH\n");
		return false;
	}
	MObject meshobj = inobjects[0];
	if( meshobj.isNull())
		return false;

	// ������������� ������� �� ������ bind
	MPlug bindGeomMatrixes = skinCluster.findPlug("geomMatrix", &stat);
	MObject dataObject;
	bindGeomMatrixes.getValue( dataObject, MDGContext_fsNormal());
	MFnMatrixData matDataFn( dataObject );
	MMatrix objPreTransform = matDataFn.matrix();

	// �����
	MPlug bindPreMatrixArrayPlug = skinCluster.findPlug("bindPreMatrix", &stat);
	MDagPathArray infs;
	unsigned int nInfs = skinCluster.influenceObjects(infs, &stat);

	SkinShaper::Shape shape;

	MFnMesh fnMesh(meshobj);
	int numPoints = fnMesh.numVertices();
	int numNormals = fnMesh.numNormals();
	int numSTs = fnMesh.numUVs();
	int numFaces = fnMesh.numPolygons();
	int numFaceVertices = fnMesh.numFaceVertices();
	shape.joints.resize(nInfs);
	for(unsigned i = 0; i<infs.length(); i++)
	{
		MObject obj = infs[i].node();
		if( !obj.hasFn( MFn::kJoint))
			continue;

		MDagPath jointpath;
		if( !MDagPath::getAPathTo(infs[i].node(), jointpath))
			return false;

		MFnTransform dagNode(infs[i].node());
		std::string path = dagNode.name().asChar();

		// ������� �� ������ ��������
		MPlug bindPreMatrixPlug = bindPreMatrixArrayPlug.elementByLogicalIndex(i, &stat);
		MObject dataObject;
		bindPreMatrixPlug.getValue( dataObject, MDGContext_fsNormal() );
		MFnMatrixData matDataFn ( dataObject );
		MMatrix invMat = matDataFn.matrix();
		MMatrix m = jointpath.inclusiveMatrix();

		Math::Matrix4f preTransform, invBind, world;
		copy( preTransform, objPreTransform);
		copy( invBind, invMat);
		copy( world, m);
		

		// ���������
		Shape::Joint& dst = shape.joints[i];
		dst.vertexweights.resize(numPoints);
		dst.inv_bindpos = invBind;
		dst.testpos = world;

	}

	// ���� � ������� ������
	MItGeometry gIter(meshobj);
	for ( int v=0; !gIter.isDone(); gIter.next(), v++ ) 
	{
		MObject comp = gIter.component(&stat);
		MFloatArray wts;
		unsigned int infCount;
		stat = skinCluster.getWeights( path, comp, wts, infCount);
		if(infCount != nInfs) 
		{
			return false;
		}
		for(unsigned i=0; i<nInfs; i++)
		{
			Shape::Joint& dst = shape.joints[i];
			dst.vertexweights[v] = wts[i];
		}
	}


	MFloatVectorArray normals;
	fnMesh.getNormals( normals );

	shape.facevertcount = 0;
	shape.polygons.resize(numFaces);
	shape.verts.resize(numPoints);
	shape.norms.resize(numFaceVertices);
	if(numSTs)
	{
		shape.u.resize(numFaceVertices);
		shape.v.resize(numFaceVertices);
	}

	MItMeshPolygon polyIt( meshobj);
	for( int face=0, faceVertex=0; polyIt.isDone() == false; polyIt.next(), face++ ) 
	{
		int count = polyIt.polygonVertexCount();
		Math::Polygon32& poly = shape.polygons[face];
		poly.resize(count);
		shape.facevertcount += count;

		for( int v=0; v<count; v++, faceVertex++) 
		{
			int vertex = polyIt.vertexIndex( v );
			poly[v] = vertex;

			MPoint point = polyIt.point( v, MSpace::kObject );
			Math::Vec3f& p = shape.verts[vertex];
			p.x = (float)point.x;
			p.y = (float)point.y;
			p.z = (float)point.z;

			int normal = polyIt.normalIndex( v );
			MVector norm = normals[normal];
			Math::Vec3f& n = shape.norms[faceVertex];
			n.x = (float)norm.x;
			n.y = (float)norm.y;
			n.z = (float)norm.z;

			if(numSTs)
			{
				float S, T;
				fnMesh.getPolygonUV( face, count, S, T );
				shape.u[faceVertex] = S;
				shape.v[faceVertex] = 1-T;
			}
		}
	}
	stream >> shape;
	return true;
}
// ���������
void SkinShaper::renderGL(
	sn::IShape* snshape, 
	sn::IPersonage* personage)
{
	Shape* shape = (Shape*)snshape;

	std::vector<Math::Vec3f> verts = shape->verts;
	filter(snshape, personage, verts);

	glPolygonMode(GL_FRONT, GL_LINES);
	for(unsigned p=0; p<shape->polygons.size(); p++)
	{
		Math::Polygon32& poly = shape->polygons[p];
		glBegin(GL_POLYGON);
		for(int v=0; v<poly.size(); v++)
		{
			int vi = poly[v];
			Math::Vec3f& vert = verts[vi];
			glVertex3f( vert.x, vert.y, vert.z);
		}
		glEnd();
	}
}

// �������� �����
sn::IShape* SkinShaper::load(
	Util::Stream& stream,
	sn::IPersonage* personage)
{
	Shape* shape = new Shape();
	stream >> *shape;
	return shape;
}
