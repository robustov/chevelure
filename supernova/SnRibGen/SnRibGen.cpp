#include "stdafx.h"

#include <stlport/stdio.h>
#include <stlport/stdlib.h>
#include <stlport/iostream>
#if defined (_WIN32)
#include <stlport/strstream>
#else
#include <stlport/unistd.h>
#include <stlport/strstream>
#endif
#include <stlport/math.h>
#include <sys/stat.h>
#include <stlport/string>

#include <stlport/iomanip>
using namespace std;

#include "RIBGen.h"
#include "RIBContext.h"
#include <stlport/stdexcept>

#ifdef _WIN32
#define unlink _unlink
#define stat _stat
#endif

//	����������� ����� SnRibGen

class SnRibGen : public RIBGen {
public:
    SnRibGen();
    virtual ~SnRibGen();

    virtual void Bound(RIBContext * c, RtBound b);
    virtual int GenRIB(RIBContext * c);

#ifndef RIBGEN_API
    virtual int SetArgs(int n, RtToken tokens[], RtPointer vals[]);
#else
    virtual int SetArgs( RIBContext *c,	int n, RtToken tokens[], RtPointer vals[]);
#endif
    
protected:			

	string 	furObject;
	string 	fileName;

	// �������
	bool useMotionBlur;
	bool castShadows;
	
    bool fileExists(const string & filename) const; 
};


/*---------------------------------------------------------*/
RIBGen * RIBGenCreate()
{
    return new SnRibGen();
}

void RIBGenDestroy(RIBGen * g)
{
    delete (SnRibGen *) g;
}

/*---------------------------------------------------------*/
SnRibGen::SnRibGen()
{
	useMotionBlur = true;
	castShadows = true;
}

//	������� ���������� �� ��������� ����������

SnRibGen::~SnRibGen() {
}
#ifndef RIBGEN_API
int
SnRibGen::SetArgs(int n, RtToken args[], RtPointer vals[])
#else
int
SnRibGen::SetArgs(RIBContext *, int n, RtToken args[], RtPointer vals[])
#endif
{
    int err = 0, i = 0;
    for (i = 0; i < n; i++)
	{
		if (!strcmp(args[i], "FileName") || !strcmp(args[i], "string FileName"))
		{
			fileName = *(static_cast<char **>(vals[i]));
		}
		else
		{
			err++;
		}
    }
    return err;
}

extern "C" {
    RtVoid dl_FurProceduralFree(RtPointer data)
	{
		RtString* sData = (RtString*) data;
		free(sData[0]);
		free(sData[1]);
		free(sData);
    }
}

void SnRibGen::Bound(RIBContext *, RtBound)
{
}

int SnRibGen::GenRIB(RIBContext * c)
{
    furObject = c->GetObjName();  
   	int frame = c->GetFrame(); // �������� �������� �����
    string errorString;

    if (furObject.empty())
	{
		return 0;
    }

c->ReportError(RIBContext::reInfo, "\nStart RibExport SnRibGen");

    RIBContext::RenderingPass p;
    RtBoolean decl;

    // ������ ���� �������� ������ � ��������� ��� motion blur'�    
    useMotionBlur = useMotionBlur && c->GetMotionBlur(); 
    RtFloat shutterOpen, shutterClose;
    RtFloat shutterAngle, fps;
    RtBoolean subframeMotion;
    RtBoolean blurCamera;
    RIBContext::ShutterTiming shutterTiming;
    RIBContext::ShutterConfig shutterConfig;
    
    c->GetShutter(&shutterOpen, &shutterClose);
    c->GetMotionInfo(&shutterAngle, &fps, &subframeMotion, &blurCamera, &shutterTiming, &shutterConfig);
    
    shutterClose = (float)(shutterOpen + (1.0 / fps));

    c->GetRenderingPass(&p, &decl);
    if (decl)
	{
		return 0;
    }
	else if (p == RIBContext::rpShadow)
	{
		if (!castShadows) return 0;		// �� ������� ������������ ����, ���� �������� castShadow �� off
    }

    char cmd[512];
    
    const RIBContextResult* result;


    if (fileName.empty())
	{
		c->ReportError(RIBContext::reError, "SnRibGen: must specify a filename to output to file.");
		return -1;
    }

	//	��������� ��������� � ��� ����� ���� ��� ����������
    switch (p)
	{
		case RIBContext::rpShadow:
			fileName += ".shadow";
			break;
		case RIBContext::rpReflection:
			fileName += ".reflection";
			break;
		case RIBContext::rpEnvironment:
			fileName += ".environment";
			break;
		default:
			break;
    }


    sprintf(cmd, "SnExport \"%s\" \"%s\"", furObject.c_str(), fileName.c_str());
	c->ReportError(RIBContext::reInfo, "\nSMD: %s", cmd);
    result = c->ExecuteHostCmd(cmd, errorString);
    if (result->ResultType() != RIBContextResult::kInt)
	{
		c->ReportError(RIBContext::reError, "\nSnRibGen: unexpected return type on furDataPlugin (int parameter) (%s)", errorString.c_str());
		return -1;
	}
    RtInt res;
    result->GetResult(res, &errorString);
    if (!res)
	{
		c->ReportError(RIBContext::reError, "\ndl_hairExport return FALSE");
		return -1;
	}


// ��������� ������ � ����������� ������� ��� ������ � ���  
    RtString *data = static_cast<RtString*>(malloc(sizeof(RtString) * 2));
    data[0] = strdup("SnDSO.dll");
    data[1] = static_cast<char*>(malloc(sizeof(char)*4096));
    sprintf(data[1], "%s", fileName.c_str());


	//////////////////////////////////////
	//	�������� Bound ���������� �� ���� ����� �������� ���������
    sprintf(cmd, "getAttr %s.boundingBoxMin", furObject.c_str());
    result = c->ExecuteHostCmd(cmd, errorString);    
    if (result->ResultType() != RIBContextResult::kFloatArray)
	{
		c->ReportError(RIBContext::reError, "SnRibGen: unexpected return type on getAttr boundingBoxMin (%s)", errorString.c_str());
		return -1;
    }
    vector<float> min;  
    result->GetResult(min, &errorString);

    sprintf(cmd, "getAttr %s.boundingBoxMax", furObject.c_str());
    result = c->ExecuteHostCmd(cmd, errorString);    
    if (result->ResultType() != RIBContextResult::kFloatArray)
	{
		c->ReportError(RIBContext::reError, "SnRibGen: unexpected return type on getAttr boundingBoxMax (%s)", errorString.c_str());
		return -1;
    }
    vector<float> max;
    result->GetResult(max, &errorString);

	float furBound=0;
    RtBound bound = {min[0] - furBound, max[0] + furBound, min[1] - furBound, max[1] + furBound, min[2] - furBound, max[2] + furBound};


//	����� ����������� ����� � ���
    c->Procedural(data, bound, c->GetProcSubdivFunc(RIBContext::kDynamicLoad), dl_FurProceduralFree);
    return 1;
}

bool SnRibGen::fileExists(const string & filename) const
{
    struct stat sbuf;
    return (stat(filename.c_str(), &sbuf) == 0);
}
