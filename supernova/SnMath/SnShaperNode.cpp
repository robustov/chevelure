#include "stdafx.h"

#include <maya\MObject.h> 
#include <maya\MFnDependencyNode.h> 
#include <maya\MFnSkinCluster.h>
#include <maya\MItDependencyGraph.h>
#include <maya\MFnMatrixData.h>
#include <maya\MMatrix.h>
#include <maya\MDagPathArray.h>
#include <maya\MItGeometry.h>
#include <maya\MDagPath.h>
#include <maya\MFnTransform.h>
#include <maya\MFnStringData.h>
#include <maya\MFloatPointArray.h>

#include "Math/Math.h"
#include "supernova/SnDelayMaya.h"
#include <maya\MFnMesh.h>
#include <maya\MItMeshPolygon.h>
#include <ri.h>
#include <gl/gl.h>
#include "mathNmaya/mathNmaya.h"
#include "supernova/ISnShaper.h"
#include "supernova/IArtistShape.h"

#include "supernova/SnMath.h"
#include "SnShaperNode.h"
#include "ocellaris/IRender.h"
#include "math/safevector.h"

template<class STREAM>
STREAM& operator >> (STREAM& out, ShaperNode::Shape& arg)
{
	out >> arg.surfaceShader >> arg.displaceShader;
	out >> arg.surfaceShaderParams >> arg.displaceShaderParams;

	// ��� ���. ��������
	out >> arg.showNormals >> arg.normalsLength;

	out >> arg.dirtyBox;

	out >> arg.interpolation;
	out >> arg.vertcount;
	out >> arg.facecount;
	out >> arg.facevertcount;
	out >> arg.facevert_vertindecies;

	out >> arg.polygons;
	out >> arg.verts;
	out >> arg.norms;
	out >> arg.u;
	out >> arg.v;
	out >> arg.pdeformer;

/*/
	if(save)
	{
		if( shape->deformer)
			shape->deformer->export(stream, source, shape, dirtyBox);
		stream >> dirtyBox;
	}
	else
	{
		sn::tag_GetDeformer gs = (sn::tag_GetDeformer)shape->deformerproc.proc;
		if( gs)
			shape->deformer = (*gs)();

		if( shape->deformer)
		{
			shape->deformershape = shape->deformer->load(stream, personage, *shape);
		}
		Math::Box3f dirtyBox;
		stream >> dirtyBox;
		shape->dirtyBox = dirtyBox;
	}
/*/
	return out;
}


int ShaperNode::Shape::getCount(sn::STREAM_MODIFER modifer)
{
	switch(modifer)
	{
	case sn::PER_UNIFORM: return 1;
	case sn::PER_VERTEX: return vertcount;
	case sn::PER_FACE: return facecount;
	case sn::PER_FACEVERTEX: return facevertcount;
	}
	return 0;
}

sn::attr_t ShaperNode::Shape::getAttr(const char* name, sn::VALUETYPE place)
{
	if( strcmp(name, "P")==0 )
		return sn::attr_t("P", sn::VT_VECTOR3, sn::PER_VERTEX, 0);
	if( strcmp(name, "N")==0 )
		return sn::attr_t("N", sn::VT_VECTOR3, sn::PER_FACEVERTEX, 1);
	return sn::attr_t();
}
void* ShaperNode::Shape::getStream(sn::attr_t& attr)
{
	if(attr.id==0)
		if( deformed_verts.empty())
			return std::safevector( verts);
		else
			return std::safevector( deformed_verts);
	if(attr.id==1)
		if( deformed_norms.empty())
			return std::safevector( norms);
		else
			return std::safevector( deformed_norms);
	return 0;
}



// �������� �����
sn::IShape* ShaperNode::export_(
	artist::IShape& source,
	sn::IPersonage* personage
	)
{
	ShaperNode::Shape& shape = *(new Shape());
	MObject src = source.getSourceObject();
	MObject parent = MFnDagNode(src).parent(0);

	// �������:
	std::vector<shaderExtern> surfaceShaderParams, displaceShaderParams;
	shape.surfaceShader = mtorShaderNames( source.getThis(), "slimSurf", &surfaceShaderParams);
	for( unsigned p=0; p<surfaceShaderParams.size(); p++)
	{
		sn::attr_t attr = source.getShaderAttr(surfaceShaderParams[p].name.c_str(), surfaceShaderParams[p].type.c_str());
		if( !attr) continue;
		std::string name = surfaceShaderParams[p].type + " " + surfaceShaderParams[p].name;
		shape.surfaceShaderParams[name] = attr;
	}
	shape.displaceShader = mtorShaderNames( source.getThis(), "slimDispl", &displaceShaderParams);
	for( unsigned p=0; p<displaceShaderParams.size(); p++)
	{
		sn::attr_t attr = source.getShaderAttr(displaceShaderParams[p].name.c_str(), displaceShaderParams[p].type.c_str());
		if( !attr) continue;
		std::string name = displaceShaderParams[p].type + " " + displaceShaderParams[p].name;
		shape.displaceShaderParams[name] = attr;
	}

	MObject nameobj = source.getValue("deformerDll", sn::VT_STRING, sn::UNIFORM_SHAPE, "SnHairShaper.dll");
	std::string deformername = MFnStringData(nameobj).string().asChar();

	bool mtorSubdiv = false;
	source.getValue(mtorSubdiv, "mtorSubdiv", sn::UNIFORM_SHAPE, false);

	source.getValue(shape.showNormals, "showNormals", sn::UNIFORM_SHAPE, false);
	source.getValue(shape.normalsLength, "normalsLength", sn::UNIFORM_SHAPE, 1, 0, 5);
	Math::Matrix4f matr;
	std::string meshmatrixattr = source.mirrorAttr( parent, "matrix");
	source.getValue(matr, meshmatrixattr.c_str());
	std::string meshpivotattr = source.mirrorAttr( parent, "rotatePivot");
	Math::Vec3f pivot;
	source.getValue(pivot, meshpivotattr.c_str());
	pivot = matr*pivot;
	
	sn::IDeformer* deformer = NULL;
	Util::DllProcedure dllproc(deformername.c_str(), "getDeformer");
	if( dllproc.proc)
	{
		sn::tag_GetDeformer gs = (sn::tag_GetDeformer)dllproc.proc;
		if( gs) 
		{
			deformer = (*gs)();
		}
	}
	else
	{
		fprintf(stderr, "can not load %s getDeformer\n", deformername.c_str());
		fflush(stderr);
	}

	MObject meshobj = MObject_kNullObject();
	if( deformer)
	{
		deformer->getGeometrySource(source, meshobj);
	}
	if( meshobj.isNull())
	{
		std::string meshattr = source.mirrorAttr( src, "outMesh");
		meshobj = source.getValue(meshattr.c_str(), sn::VT_GEOMETRY, sn::UNIFORM_SHAPE);
	}
	if( meshobj.isNull())
		return NULL;

	MFnMesh fnMesh(meshobj);

	int numPoints = fnMesh.numVertices();
	int numNormals = fnMesh.numNormals();
	int numSTs = fnMesh.numUVs();
	int numFaces = fnMesh.numPolygons();
	int numFaceVertices = fnMesh.numFaceVertices();

	MFloatPointArray _verts;
	MFloatArray _uArray, _vArray;
	MFloatVectorArray _normals;
	fnMesh.getPoints(_verts);
	fnMesh.getUVs(_uArray, _vArray);
	fnMesh.getNormals( _normals );

	shape.vertcount = numPoints;
	shape.facecount = numFaces;
	shape.facevertcount = numFaceVertices;

	shape.polygons.resize(numFaces);
	shape.verts.resize(numPoints);
	shape.norms.resize(numFaceVertices);
	shape.facevert_vertindecies.resize(numFaceVertices, 0);
	if(numSTs)
	{
		shape.u.resize(numFaceVertices);
		shape.v.resize(numFaceVertices);
	}

	Math::Box3f dirtyBox; bool bDirtyBox=false;
	MItMeshPolygon polyIt( meshobj);
	for( int face=0, faceVertex=0; polyIt.isDone() == false; polyIt.next(), face++ ) 
	{
		int count = polyIt.polygonVertexCount();
		Math::Polygon32& poly = shape.polygons[face];
		poly.resize(count);
		int faceFV = faceVertex;
//		shape.facevertcount += count;

		for( int v=0; v<count; v++, faceVertex++) 
		{
			/*/
			int vertex = vertices[ v];
			int nn = polyIt.normalIndex(v);
			int stind;
			polyIt.getUVIndex(v, stind);

			// V
			iverts.push_back(vertex);

			// N
			MFloatVector& n = _normals[nn];
			norms.push_back( Math::Vec3f(n.x, n.y, n.z));

			// ST
			s.push_back(_uArray[stind]);
			t.push_back(1-_vArray[stind]);
			/*/
			int vertex = polyIt.vertexIndex( v );
			int fv = faceVertex;
			if( !mtorSubdiv)
				poly[v] = vertex;
			else
			{
				poly[count-v-1] = vertex;
				fv = faceFV + count-v-1;
			}

			MPoint point = polyIt.point( v, MSpace::kObject );
			Math::Vec3f& p = shape.verts[vertex];
			p.x = (float)point.x;
			p.y = (float)point.y;
			p.z = (float)point.z;
			p = matr*p - pivot;
			// box
			if( bDirtyBox)
				dirtyBox.insert(p);
			else 
			{
				dirtyBox.set(p, p); 
				bDirtyBox=true;
			}

			int normal = polyIt.normalIndex( v );
			MVector norm = -_normals[normal];
			Math::Vec3f& n = shape.norms[fv];
			n.x = (float)norm.x;
			n.y = (float)norm.y;
			n.z = (float)norm.z;
			shape.facevert_vertindecies[fv] = vertex;

			if(numSTs)
			{
				float S, T;
				fnMesh.getPolygonUV( face, v, S, T );
				shape.u[fv] = S;
				shape.v[fv] = 1-T;
			}
			//*/
		}
	}
	if( deformer)
	{
		sn::IShape* deformershape = deformer->export_(source, shape, dirtyBox);
		if(deformershape)
		{
			shape.pdeformer = new sn::DeformerShortcut(dllproc, deformer, deformershape);
		}
	}
	shape.dirtyBox = dirtyBox;

	if( !mtorSubdiv)
		shape.interpolation = "linear";
	else
		shape.interpolation = "catmull-clark";

	return &shape;
}

// �������� �����
bool ShaperNode::save(
	sn::IShape* snshape, 
	Util::Stream& stream
	)
{
	Shape* shape = (Shape*)snshape;
	stream >> *shape;
	return true;
}

// �������� �����
sn::IShape* ShaperNode::load(
	Util::Stream& stream,
	sn::IPersonage* personage)
{
	Shape* shape = new Shape();
	stream >> *shape;
	return shape;
}
// box???? �� ������� ����!
bool ShaperNode::getDirtyBox(
	sn::IShape* snshape, 
	const Math::Matrix4f& transform,
	Math::Box3f& box
	)
{
	Shape* shape = (Shape*)snshape;
	box = shape->dirtyBox;
	box.transform(transform);

	return true;
}

// ���������
void ShaperNode::render(
	sn::IShape* snshape, 
	sn::IPersonage* personage, 
	const Math::Matrix4f& transform,
	IPrman* prman,
	cls::IRender* render
	)
{
	Shape* shape = (Shape*)snshape;

	bool bSubdiv = false;
	if( shape->interpolation != "linear")
		bSubdiv = true;

	shape->deformed_verts = shape->verts;
	shape->deformed_norms = shape->norms;
	sn::tag_TokenParameters deformerParams;
	if( shape->pdeformer.isValid())
	{
		sn::IDeformer* deformer = shape->pdeformer->shaper;
		sn::IShape* deformershape = shape->pdeformer->shape;
		deformer->filter(deformershape, personage, *shape);
		deformer->getShaderParameters("surface", deformershape, personage, deformerParams);
	}

	// transform:
	{
		int v;
		for(v=0; v<(int)shape->deformed_verts.size(); v++)
			shape->deformed_verts[v] = transform*shape->deformed_verts[v];

		Math::Matrix4As3f transformN(transform);
		for(v=0; v<(int)shape->deformed_norms.size(); v++)
			shape->deformed_norms[v] = transformN*shape->deformed_norms[v];
	}

	int npolygons = (int)shape->polygons.size();
	int facevertcount = shape->facevertcount;

	std::vector<RtInt> loops(npolygons);
	std::vector<RtInt> nverts(npolygons);
	std::vector<RtInt> verts(facevertcount);
	
	int vertscount=0;
	for(int i=0; i<npolygons; i++)
	{
		loops[i] = 1;
		nverts[i] = shape->polygons[i].size();

		for(int v=0; v<nverts[i]; v++, vertscount++)
		{
//			if( !bSubdiv)
				verts[vertscount] = shape->polygons[i][v];
//			else
//				verts[vertscount] = shape->polygons[i][nverts[i]-v-1];
		}
	}

	std::vector<RtToken> tokens;
	std::vector<RtPointer> parms;

	tokens.push_back("P");
	parms.push_back((float*)std::safevector( shape->deformed_verts));
	int vc = shape->deformed_verts.size();

	if( !bSubdiv)
	{
		tokens.push_back("facevarying normal N");
		parms.push_back((float*)std::safevector( shape->deformed_norms));
	}

	if( shape->u.size()==shape->facevert_vertindecies.size() && 
		shape->v.size()==shape->facevert_vertindecies.size() )
	{
		tokens.push_back("facevarying float s");
		parms.push_back((float*)std::safevector( shape->u));

		tokens.push_back("facevarying float t");
		parms.push_back((float*)std::safevector( shape->v));
	}

	sn::tag_TokenParameters::iterator itp = deformerParams.begin();
	for( ; itp!=deformerParams.end(); itp++)
	{
		tokens.push_back( (char*)itp->first.c_str());
		parms.push_back( std::safevector( itp->second));
	}

	IPrman::enRenderMode renderMode = IPrman::RENDERMODE_P_ONLY;
	if( prman)
		renderMode = prman->getRenderMode();
	if( renderMode&IPrman::RENDERMODE_ATTRIBUTEBEGIN)
	{
		prman->RiAttributeBegin();
	}

	/*/
	{
		Math::Box3f box = CalcDeformedBox(&shape->deformed_verts[0], shape->vertcount);
		box.set(Math::Vec3f(-10.1, -10.1, -10.1), Math::Vec3f(-10, -10, -10));
		RtBound bound;
		IPrman::copy(bound, box);
		prman->RiBound(bound);
	}
	/*/

	if( renderMode&IPrman::RENDERMODE_SHADER)
	{
		if( !shape->surfaceShader.empty())
		{
			std::vector<RtToken> tokens;
			std::vector<RtPointer> parms;
			/*/
			std::vector<char> datas(1, 0);
			datas.reserve(1024);
			std::map< std::string, sn::attr_t>::iterator it = shape->surfaceShaderParams.begin();
			for( ; it!=shape->surfaceShaderParams.end(); it++)
			{
				Math::Vec3f data;
				personage->getValue(it->second, data);
				tokens.push_back( (char*)it->first.c_str());
				parms.push_back( &datas[datas.size()]);
				datas.insert( datas.end(), (char*)&data, (char*)&data + sizeof(data));
			}
			/*/
			prman->RiSurfaceV((char*)shape->surfaceShader.c_str(), tokens.size(), std::safevector( tokens), std::safevector( parms));
		}
		if( !shape->displaceShader.empty())
		{
			std::vector<RtToken> tokens;
			std::vector<RtPointer> parms;
			/*/
			std::vector<char> datas(1, 0);
			datas.reserve(1024);
			std::map< std::string, sn::attr_t>::iterator it = shape->displaceShaderParams.begin();
			for( ; it!=shape->displaceShaderParams.end(); it++)
			{
				Math::Vec3f data;
				personage->getValue(it->second, data);
				tokens.push_back( (char*)it->first.c_str());
				parms.push_back( &datas[datas.size()]);
				datas.insert( datas.end(), (char*)&data, (char*)&data + sizeof(data));
			}
			/*/
			prman->RiDisplacementV((char*)shape->displaceShader.c_str(), tokens.size(), std::safevector( tokens), std::safevector( parms));
		}
	}

	if( renderMode&IPrman::RENDERMODE_GEOMETRY)
	{
		if( render)
		{
			cls::PA<Math::Vec3f> __Pref = render->GetParameter("__Pref");
			if( vc==__Pref.size())
			{
				tokens.push_back("vertex point __Pref");
				parms.push_back((float*)__Pref.data());
			}
		}
		
		if( !bSubdiv)
		{
			prman->RiPointsGeneralPolygonsV( npolygons, std::safevector( loops), std::safevector( nverts), std::safevector( verts), tokens.size(), std::safevector( tokens), std::safevector( parms) );
		}
		else
		{
			std::vector<RtToken> tokens_tag;
			std::vector<RtPointer> parms_tag;

			// st
			/*/
			std::vector<Math::Vec2f> st;
			st.resize(shape->u.size());
			for(unsigned i=0; i<st.size(); i++)
			{
				st[i] = Math::Vec2f(shape->u[i], shape->v[i]);
			}
			tokens.push_back("facevarying float[2] st");
			parms.push_back((float*)&st[0]);
			/*/


			int interpolateboundary[] = {0,0};
			tokens_tag.push_back("interpolateboundary");
			parms_tag.push_back(interpolateboundary);

			prman->RiSubdivisionMeshV( 
				(RtToken)shape->interpolation.c_str(), 
				npolygons, std::safevector( nverts), std::safevector( verts), 
				1, std::safevector( tokens_tag), interpolateboundary, 
				NULL, NULL, 
				tokens.size(), std::safevector( tokens), std::safevector( parms));
		}
	}

	if( renderMode&IPrman::RENDERMODE_ATTRIBUTEBEGIN)
	{
		prman->RiAttributeEnd();
	}

	if( renderMode&IPrman::RENDERMODE_P_ONLY && render)
	{
		cls::PA<Math::Vec3f> __Pref(cls::PI_VERTEX, shape->deformed_verts);
		render->Parameter("__Pref", __Pref);
	}
}
// ���������
void ShaperNode::renderOcs(
	sn::IShape* snshape, 
	sn::IPersonage* personage, 
	const Math::Matrix4f& transform,
	cls::IRender* render,
	int _renderMode
	)
{
	Shape* shape = (Shape*)snshape;

	bool bSubdiv = false;
	if( shape->interpolation != "linear")
		bSubdiv = true;

	shape->deformed_verts = shape->verts;
	shape->deformed_norms = shape->norms;
	sn::tag_TokenParameters deformerParams;
	if( shape->pdeformer.isValid())
	{
		sn::IDeformer* deformer = shape->pdeformer->shaper;
		sn::IShape* deformershape = shape->pdeformer->shape;
		deformer->filter(deformershape, personage, *shape);
		deformer->getShaderParameters("surface", deformershape, personage, deformerParams);
	}

	// transform:
	{
		int v;

		for(v=0; v<(int)shape->deformed_verts.size(); v++)
			shape->deformed_verts[v] = transform*shape->deformed_verts[v];

		Math::Matrix4As3f transformN(transform);
		for(v=0; v<(int)shape->deformed_norms.size(); v++)
			shape->deformed_norms[v] = transformN*shape->deformed_norms[v];
	}

	int npolygons = (int)shape->polygons.size();
	int facevertcount = shape->facevertcount;

	std::vector<RtInt> loops(npolygons);
	std::vector<RtInt> nverts(npolygons);
	std::vector<RtInt> verts(facevertcount);
	
	int vertscount=0;
	for(int i=0; i<npolygons; i++)
	{
		loops[i] = 1;
		nverts[i] = shape->polygons[i].size();

		for(int v=0; v<nverts[i]; v++, vertscount++)
		{
//			if( !bSubdiv)
				verts[vertscount] = shape->polygons[i][v];
//			else
//				verts[vertscount] = shape->polygons[i][nverts[i]-v-1];
		}
	}

	std::vector<RtToken> tokens;
	std::vector<RtPointer> parms;

	tokens.push_back("P");
	parms.push_back((float*)std::safevector( shape->deformed_verts));
	int vc = shape->deformed_verts.size();

	if( !bSubdiv)
	{
		tokens.push_back("facevarying normal N");
		parms.push_back((float*)std::safevector( shape->deformed_norms));
	}

	if( shape->u.size()==shape->facevert_vertindecies.size() && 
		shape->v.size()==shape->facevert_vertindecies.size() )
	{
		tokens.push_back("facevarying float s");
		parms.push_back((float*)std::safevector( shape->u));

		tokens.push_back("facevarying float t");
		parms.push_back((float*)std::safevector( shape->v));
	}

	sn::tag_TokenParameters::iterator itp = deformerParams.begin();
	for( ; itp!=deformerParams.end(); itp++)
	{
		tokens.push_back( (char*)itp->first.c_str());
		parms.push_back( std::safevector( itp->second));
	}

	IPrman::enRenderMode renderMode = (IPrman::enRenderMode)_renderMode;
	if( renderMode&IPrman::RENDERMODE_ATTRIBUTEBEGIN)
	{
		render->PushRenderAttributes();
//		prman->RiAttributeBegin();
	}

	if( renderMode&IPrman::RENDERMODE_SHADER)
	{
		if( !shape->surfaceShader.empty())
		{
//			std::vector<RtToken> tokens;
//			std::vector<RtPointer> parms;
//			prman->RiSurfaceV((char*)shape->surfaceShader.c_str(), tokens.size(), &tokens[0], &parms[0]);
			render->Shader("surface", shape->surfaceShader.c_str());
		}
		if( !shape->displaceShader.empty())
		{
//			std::vector<RtToken> tokens;
//			std::vector<RtPointer> parms;
//			prman->RiDisplacementV((char*)shape->displaceShader.c_str(), tokens.size(), &tokens[0], &parms[0]);
			render->Shader("displacement", shape->displaceShader.c_str());
		}
	}

	if( renderMode&IPrman::RENDERMODE_GEOMETRY)
	{
		/*/
		if( render)
		{
			cls::PA<Math::Vec3f> __Pref = render->GetParameter("__Pref");
			if( vc==__Pref.size())
			{
				tokens.push_back("vertex point __Pref");
				parms.push_back((float*)&__Pref[0]);
			}
		}
		
		if( !bSubdiv)
		{
			prman->RiPointsGeneralPolygonsV( npolygons, &loops[0], &nverts[0], &verts[0], tokens.size(), &tokens[0], &parms[0] );
		}
		else
		{
			std::vector<RtToken> tokens_tag;
			std::vector<RtPointer> parms_tag;

			int interpolateboundary[] = {0,0};
			tokens_tag.push_back("interpolateboundary");
			parms_tag.push_back(interpolateboundary);

			prman->RiSubdivisionMeshV( 
				(RtToken)shape->interpolation.c_str(), 
				npolygons, &nverts[0], &verts[0], 
				1, &tokens_tag[0], interpolateboundary, 
				NULL, NULL, 
				tokens.size(), &tokens[0], &parms[0]);
		}
		/*/
	}

	if( renderMode&IPrman::RENDERMODE_ATTRIBUTEBEGIN)
	{
		render->PopRenderAttributes();
//		prman->RiAttributeEnd();
	}

	if( renderMode&IPrman::RENDERMODE_P_ONLY && render)
	{
		cls::PA<Math::Vec3f> __Pref(cls::PI_VERTEX, shape->deformed_verts);
		render->Parameter("__Pref", __Pref);
	}
}


// ���������
void ShaperNode::renderGL(
	sn::IShape* snshape, 
	sn::IPersonage* personage,
	const Math::Matrix4f& transform
	)
{
	Shape* shape = (Shape*)snshape;

	shape->deformed_verts = shape->verts;
	shape->deformed_norms = shape->norms;
	if( shape->pdeformer.isValid())
	{
		sn::IDeformer* deformer = shape->pdeformer->shaper;
		sn::IShape* deformershape = shape->pdeformer->shape;
		deformer->filter(deformershape, personage, *shape);
	}
	
	// transform:
	{
		int v;

		for(v=0; v<(int)shape->deformed_verts.size(); v++)
			shape->deformed_verts[v] = transform*shape->deformed_verts[v];

		Math::Matrix4As3f transformN(transform);
		for(v=0; v<(int)shape->deformed_norms.size(); v++)
			shape->deformed_norms[v] = transformN*shape->deformed_norms[v];
	}

	glPushAttrib(GL_POLYGON_BIT|GL_ENABLE_BIT);
	glPolygonMode(GL_FRONT, GL_LINE);
	glPolygonMode(GL_BACK, GL_LINE);
	glDisable(GL_CULL_FACE);
	for(unsigned p=0; p<shape->polygons.size(); p++)
	{
		Math::Polygon32& poly = shape->polygons[p];
		glBegin(GL_POLYGON);
		for(int v=0; v<poly.size(); v++)
		{
			int vi = poly[v];
			if( vi<0 || vi>=(int)shape->deformed_verts.size()) 
				break;
			Math::Vec3f& vert = shape->deformed_verts[vi];
			glVertex3f( vert.x, vert.y, vert.z);
		}
		glEnd();
	}

	if(shape->showNormals)
	{
		float length = shape->normalsLength;
		for(int i=0; i<shape->facevertsCount(); ++i)
		{
			int v = shape->getVertexForFacevertex(i);
			Math::Vec3f v1 = shape->deformed_verts[v];
			Math::Vec3f v2 = v1+shape->deformed_norms[i]*length;

			glBegin( GL_LINES );
			glVertex3f( v1.x, v1.y, v1.z );
			glVertex3f( v2.x, v2.y, v2.z );
			glEnd();
		}
	}
	glPopAttrib();
}

Math::Box3f ShaperNode::CalcDeformedBox(Math::Vec3f* verts, int vertcount)
{
	Math::Box3f box(verts[0], verts[0]);
	for(int i=0; i<vertcount; ++i)
	{
		box.insert(verts[i]);
	}
	return box;
}


extern "C" 
{
	__declspec(dllexport) sn::IShaper* getShaper();
}

sn::IShaper* getShaper()
{
	return new ShaperNode();
}

