#pragma once
#include "supernova/ISnDeformer.h"
#include "Math/Box.h"


// ����������� sn::IShaper �� ������ ����������
struct ShaperNode : public sn::IShaper
{
	virtual void release(){delete this;}

	// �������� �����
	virtual sn::IShape* export_(
		artist::IShape& source,
		sn::IPersonage* personage
		);
	// �������� �����
	virtual bool save(
		sn::IShape* snshape, 
		Util::Stream& stream
		);
	// �������� �����
	virtual sn::IShape* load(
		Util::Stream& stream,
		sn::IPersonage* personage);
	// ���������
	virtual void render(
		sn::IShape* shape, 
		sn::IPersonage* personage, 
		const Math::Matrix4f& transform,
		IPrman* prman,
		cls::IRender* render
		);
	// ���������
	virtual void renderGL(
		sn::IShape* snshape, 
		sn::IPersonage* personage,
		const Math::Matrix4f& transform
		);
	// ���������
	virtual void renderOcs(
		sn::IShape* snshape, 
		sn::IPersonage* personage, 
		const Math::Matrix4f& transform,
		cls::IRender* render, 
		int renderMode = 0xFFFFFFFF
		);

	// box???? �� ������� ����!
	virtual bool getDirtyBox(
		sn::IShape* snshape, 
		const Math::Matrix4f& transform,
		Math::Box3f& box
		);

public:
	struct Shape : public sn::IShape, public sn::IDeformedShape
	{
		Shape()
		{ 
			interpolation = "linear";
		}
		~Shape()
		{ 
		}

		// ����������� ���������
		// �������:
		std::string surfaceShader, displaceShader;
		std::map< std::string, sn::attr_t> surfaceShaderParams;
		std::map< std::string, sn::attr_t> displaceShaderParams;

		// ��� ���. ��������
		bool showNormals;
		float normalsLength;

		Math::Box3f dirtyBox;
		/*/
		enum geometryType
		{
			MESH		= 0,
			NURBS		= 1,
			SUBDIV		= 2,
			CURVES		= 3,
			POINTS		= 4,
		};
		geometryType type;
		/*/
		std::string interpolation;

		int vertcount, facecount, facevertcount;
		std::vector<int> facevert_vertindecies;		// ������� ��������� �� ������� facevertex

		std::vector<Math::Polygon32> polygons;
		std::vector<Math::Vec3f> verts;
		std::vector<Math::Vec3f> norms;
		std::vector<float> u;						// u
		std::vector<float> v;						// v

		// ������������ ���������
		sn::DeformerShortcutPtr pdeformer;
		std::vector<Math::Vec3f> deformed_verts;
		std::vector<Math::Vec3f> deformed_norms;

		// IDeformedShape
		virtual int getCount(sn::STREAM_MODIFER modifer);
		virtual int vertsCount(){return vertcount;};
		virtual int faceCount(){return facecount;};
		virtual int facevertsCount(){return facevertcount;};

		virtual int getVertexForFacevertex(int fv){return facevert_vertindecies[fv];};

		virtual sn::attr_t getAttr(const char* name, sn::VALUETYPE place);
		virtual void* getStream(sn::attr_t& attr);

		// sn::IShape
		virtual void release(){delete this;};
	};

	Math::Box3f CalcDeformedBox(Math::Vec3f* verts, int vertcount);

};
