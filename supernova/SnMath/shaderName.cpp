#include "stdafx.h"

#include "supernova/SnMath.h"

#include <maya/MStatus.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MString.h>
#include <maya/MGlobal.h>
#include <maya/MPlug.h>
#include "supernova/SnDelayMaya.h"
#include <string.h>
#include <stdio.h>

void RecursiveFindParams(int level, const char* appid, std::vector<shaderExtern>& params);

std::string mtorShaderNames(MObject obj, const char* attrname, std::vector<shaderExtern>* params)
{
	MStatus stat;
	MStringArray res;
	char buf[1024];

	MFnDependencyNode dn(obj);
//displayString("node: %s", dn.name().asChar());

	/*/
	std::string attrname;
	if( type==ST_SURFACE)
		attrname = "slimSurf";
	else if( type==ST_DISPLACE)
		attrname = "slimDispl";
	/*/
	MPlug pl = dn.findPlug(attrname, &stat);
	if(!stat)
		return "";

	MObject o;
	pl.getValue(o, MDGContext_fsNormal());
	res = MFnStringArrayData(o).array();
	if( res.length()<1)
		return "";

	std::string id = res[0].asChar();
	if( id.empty()) return "";
//displayString("id: %s", id.c_str());

	// $appSurf = `slimcmd slim GetAppearances -id $newSurfID[0]`;
	_snprintf(buf, 1024, "slimcmd slim GetAppearances -id \"%s\"", id.c_str());
	if( !MGlobal::executeCommand(buf, res, false, false))
		return "";
	if(res.length()<1) 
		return "";
	std::string appSurf = res[0].asChar();
	if( appSurf.empty()) return "";

	// $ShdName[0] = `slimcmd $appSurf GetName`;
	_snprintf(buf, 1024, "slimcmd \"%s\" GetName", appSurf.c_str());
	if( !MGlobal::executeCommand(buf, res, false, false))
		return "";
	if(res.length()<1) 
		return "";
	std::string shaderName = res[0].asChar();
//displayString("shader: %s", shaderName.c_str());
	shaderName = "rmanshader/" + shaderName;

	// GetMaster
	bool displayEnabled = false;
	_snprintf(buf, 1024, "slimcmd \"%s\" GetMaster 1", appSurf.c_str());
	if( !MGlobal::executeCommand(buf, res, displayEnabled, false))
		return "";
	if(res.length()<1) 
		return "";
	shaderName = res[0].asChar();

	if( params)
	{
		params->clear();
		RecursiveFindParams( 0, id.c_str(), *params);
	}

	return shaderName;
}

void RecursiveFindParams(int level, const char* id, std::vector<shaderExtern>& params)
{
	if( level>10) return;
	MStatus stat;
	MStringArray res;
	char buf[1024];

	// $appSurf = `slimcmd slim GetAppearances -id $newSurfID[0]`;
	_snprintf(buf, 1024, "slimcmd slim GetAppearances -id \"%s\"", id);
	if( !MGlobal::executeCommand(buf, res, false, false))
		return;
	if(res.length()<1) 
		return;
	std::string appSurf = res[0].asChar();

	// $ShdName[0] = `slimcmd $appSurf GetName`;
	_snprintf(buf, 1024, "slimcmd \"%s\" GetName", appSurf.c_str());
	if( !MGlobal::executeCommand(buf, res, false, false))
		return;
	if(res.length()<1) 
		return;
	std::string shaderName = res[0].asChar();

	// $prop = `slimcmd $appSurf GetProperties`;
	_snprintf(buf, 1024, "slimcmd \"%s\" GetProperties", appSurf.c_str());
	if( !MGlobal::executeCommand(buf, res, false, false))
		return;
	if(res.length()<1) 
		return;

	std::vector<std::string> proplist;
	std::string prop = res[0].asChar();
	char* substr = strtok((char*)prop.c_str(), " ");
	for(; substr; substr = strtok(NULL, " "))
		proplist.push_back(substr);

	for(unsigned i=0; i<proplist.size(); i++)
	{
		std::string& substr = proplist[i];
//displayString("prop%d: %s", i, substr.c_str());
		MStringArray res2;
		// slimcmd $tm GetValueProvider == variable
		_snprintf(buf, 1024, "slimcmd \"%s\" GetValueProvider", substr.c_str());
		if( !MGlobal::executeCommand(buf, res2, false, false))
			continue;
		if(res2.length()<1) 
			continue;
		if( strcmp(res2[0].asChar(), "connection")==0)
		{
			// slimcmd $tm GetName;
			_snprintf(buf, 1024, "slimcmd \"%s\" GetConnection", substr.c_str());
			if( !MGlobal::executeCommand(buf, res2, false, false))
				continue;
			if(res2.length()<1) 
				continue;
			std::string connection = res2[0].asChar();
			if( connection.empty())
				continue;

//displayString("connection: %s", connection.c_str());
	
			RecursiveFindParams( level+1, connection.c_str(), params);
			continue;
		}
		if( strcmp(res2[0].asChar(), "variable")!=0)
			continue;

		// slimcmd $tm GetName;
		_snprintf(buf, 1024, "slimcmd \"%s\" GetName", substr.c_str());
		if( !MGlobal::executeCommand(buf, res2, false, false))
			continue;
		if(res2.length()<1) 
			continue;
		shaderExtern externParam;
		externParam.name  = res2[0].asChar();

		// slimcmd $tm GetType;
		_snprintf(buf, 1024, "slimcmd \"%s\" GetType", substr.c_str());
		if( !MGlobal::executeCommand(buf, res2, false, false))
			continue;
		if(res2.length()<1) 
			continue;
		externParam.type = res2[0].asChar();

		externParam.name = shaderName + "_" + externParam.name;
		params.push_back(externParam);
//		displayString("param: %s %s", type.c_str(), name.c_str());
	}
}


