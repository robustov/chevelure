#pragma once
#include "supernova/ISnShaper.h"

class SnMotherShaper : public sn::IShaper
{
public:
	virtual void release(){delete this;}

	virtual sn::IShape* export_(
		artist::IShape& source,
		sn::IPersonage* personage
		);
	// �������� �����
	virtual bool save(
		sn::IShape* snshape, 
		Util::Stream& stream
		);
	// �������� �����
	virtual sn::IShape* load(
		Util::Stream& stream,
		sn::IPersonage* personage);
	// ���������
	virtual void render(
		sn::IShape* shape, 
		sn::IPersonage* personage, 
		const Math::Matrix4f& transform,
		IPrman* prman,
		cls::IRender* render
		);
	// ���������
	virtual void renderGL(
		sn::IShape* snshape, 
		sn::IPersonage* personage,
		const Math::Matrix4f& transform
		);
	// ���������
	virtual void renderOcs(
		sn::IShape* snshape, 
		sn::IPersonage* personage, 
		const Math::Matrix4f& transform,
		cls::IRender* render, 
		int renderMode = 0xFFFFFFFF
		);

public:
	SnMotherShaper(void);
	~SnMotherShaper(void);

	struct Shape : public sn::IShape
	{
		virtual void release(){delete this;};

		struct Node
		{
			int transform;
			int shape;
			std::vector<int> childs;
		};
		// ����
		std::vector<Node> nodes;

		// ����������
		std::vector<Math::Matrix4f> transforms;

		// �����
		struct shape
		{
			sn::ShapeShortcutPtr ptr;
			// ������ ���������
			//...
		};
		std::vector<shape> shapes;

		// ������ �������� �� ��������
		std::map<artist::attr_decl, int> attrs;
		// ������ ��������
		std::vector<char> attrsdata;

	};
protected:
	void Recursive(MObject obj);
};
