#include "stdafx.h"
#include "SnPersonageStub.h"
#include "Util/DllProcedure.h"

extern "C" 
{
	__declspec(dllexport) sn::IShaper* getInstance();
}

sn::IShaper* getInstance()
{
	return new SnPersonageStub();
}


SnPersonageStub::Shape::Shape()
{
	bDirtyValid = false;
};
SnPersonageStub::Shape::~Shape()
{
};

void SnPersonageStub::Shape::getValue(sn::attr_t& attr, int& val)
{
	val = 0;
	std::map<artist::attr_decl, int>::iterator it = attrs.find( artist::attr_decl(attr.name.c_str(), attr.type));
	if( it==attrs.end()) return;
	int offs = it->second;
	val = *(int*)((char*)&attrsdata[0]+offs);
}
void SnPersonageStub::Shape::getValue(sn::attr_t& attr, float& val)
{
	val = 0;
	std::map<artist::attr_decl, int>::iterator it = attrs.find( artist::attr_decl(attr.name.c_str(), attr.type));
	if( it==attrs.end()) return;
	int offs = it->second;
	val = *(float*)((char*)&attrsdata[0]+offs);
}
void SnPersonageStub::Shape::getValue(sn::attr_t& attr, Math::Vec3f& val)
{
	val = Math::Vec3f(0, 0, 0);
	std::map<artist::attr_decl, int>::iterator it = attrs.find( artist::attr_decl(attr.name.c_str(), attr.type));
	if( it==attrs.end()) return;
	int offs = it->second;
	val = *(Math::Vec3f*)((char*)&attrsdata[0]+offs);
}
void SnPersonageStub::Shape::getValue(sn::attr_t& attr, Math::Matrix4f& val)
{
	val = Math::Matrix4f::id;
	std::map<artist::attr_decl, int>::iterator it = attrs.find( artist::attr_decl(attr.name.c_str(), attr.type));
	if( it==attrs.end()) return;
	val = *(Math::Matrix4f*)((char*)&attrsdata[0]+it->second);
}

template<class STREAM>
STREAM& operator >> (STREAM& out, SnPersonageStub::Shape& arg)
{
	out >> arg.bDirtyValid;
	out >> arg.box;
	out >> arg.shapeval;
	out >> arg.attrs;
	out >> arg.attrsdata;
	return out;
}


// �������� �����
sn::IShape* SnPersonageStub::export_(
	artist::IShape& source,
	sn::IPersonage* personage
	)
{
	Shape& shape = *(new Shape());
	if( !source.getValue(shape.shapeval, "shape"))
		return NULL;

	for(unsigned i=0; i<shape.shapeval.attrlist.size(); ++i)
	{
		artist::attr_decl& ad = shape.shapeval.attrlist[i];
		std::vector<char> data;
		if( !source.getValue(data, ad))
			return NULL;
		shape.attrs[ad] = shape.attrsdata.size();
		shape.attrsdata.insert(shape.attrsdata.end(), data.begin(), data.end());
	}
	shape.bDirtyValid = shape.shapeval.ssp->shaper->getDirtyBox(shape.shapeval.ssp->shape, Math::Matrix4f::id, shape.box);
	return &shape;
}
// �������� �����
bool SnPersonageStub::save(
	sn::IShape* snshape, 
	Util::Stream& stream
	)
{
	Shape* shape = (Shape*)snshape;
	stream >> *shape;
	return true;
}
// �������� �����
sn::IShape* SnPersonageStub::load(
	Util::Stream& stream,
	sn::IPersonage* personage)
{
	Shape* shape = new Shape();
	stream >> *shape;
	return shape;
}

// box???? �� ������� ����!
bool SnPersonageStub::getDirtyBox(
	sn::IShape* snshape, 
	const Math::Matrix4f& transform,
	Math::Box3f& box
	)
{
	Shape* shape = (Shape*)snshape;
	box = shape->box;
	box.transform(transform);
	return shape->bDirtyValid;
}

// ���������
void SnPersonageStub::renderGL(
	sn::IShape* snshape, 
	sn::IPersonage* personage,
	const Math::Matrix4f& transform
	)
{
	Shape* shape = (Shape*)snshape;
	sn::IShaper* shaper = shape->shapeval.ssp->shaper;
	sn::IShape* pshape  = shape->shapeval.ssp->shape;

	shaper->renderGL(pshape, shape, transform);
}
// ���������
void SnPersonageStub::render(
	sn::IShape* snshape, 
	sn::IPersonage* personage, 
	const Math::Matrix4f& transform,
	IPrman* prman,
	cls::IRender* render
	)
{
	Shape* shape = (Shape*)snshape;
	sn::IShaper* shaper = shape->shapeval.ssp->shaper;
	sn::IShape* pshape  = shape->shapeval.ssp->shape;

	shaper->render(pshape, shape, transform, prman, render);
}
// ���������
void SnPersonageStub::renderOcs(
	sn::IShape* snshape, 
	sn::IPersonage* personage, 
	const Math::Matrix4f& transform,
	cls::IRender* render, 
	int renderMode
	)
{
	Shape* shape = (Shape*)snshape;
	sn::IShaper* shaper = shape->shapeval.ssp->shaper;
	sn::IShape* pshape  = shape->shapeval.ssp->shape;

	shaper->renderOcs(pshape, shape, transform, render, renderMode);
}

/*/
bool SnPersonageStub::getSnData(Shape* shape, Util::DllProcedure& dllproc, sn::IShaper* &shaper, sn::IShape* &pshape)
{
	artist::ShapeValue& shval = shape->shapeattr;
	Util::MemStream stream;
	stream.open(&shval.data[0], shval.data.size());

	std::string dll, proc;
	stream >> dll >> proc;
	dllproc = Util::DllProcedure(dll.c_str(), proc.c_str());
	if( !dllproc.isValid())
		return false;
	sn::tag_GetShaper gs = (sn::tag_GetShaper)dllproc.proc;
	shaper = (*gs)();
	if(!shaper)
		return false;
	pshape = shaper->load(stream, NULL);
	if( !pshape)
	{
		shaper->release();
		return false;
	}
	return true;
}
/*/
