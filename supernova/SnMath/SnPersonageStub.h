#pragma once

#include "supernova/ISnShaper.h"
#include "Util\STLStream.h"
#include "Util\DllProcedure.h"
#include "supernova/IArtistShape.h"

// ����������� sn::IShaper �� ������ ����������
struct SnPersonageStub : public sn::IShaper
{
	virtual void release(){delete this;}

	// �������� �����
	virtual sn::IShape* export_(
		artist::IShape& source,
		sn::IPersonage* personage
		);
	// �������� �����
	virtual bool save(
		sn::IShape* snshape, 
		Util::Stream& stream
		);
	// �������� �����
	virtual sn::IShape* load(
		Util::Stream& stream,
		sn::IPersonage* personage);
	// ���������
	virtual void render(
		sn::IShape* snshape, 
		sn::IPersonage* personage, 
		const Math::Matrix4f& transform,
		IPrman* prman,
		cls::IRender* render
		);
	// ���������
	virtual void renderGL(
		sn::IShape* snshape, 
		sn::IPersonage* personage,
		const Math::Matrix4f& transform
		);
	// ���������
	virtual void renderOcs(
		sn::IShape* snshape, 
		sn::IPersonage* personage, 
		const Math::Matrix4f& transform,
		cls::IRender* render, 
		int renderMode = 0xFFFFFFFF
		);
	// box???? �� ������� ����!
	virtual bool getDirtyBox(
		sn::IShape* snshape, 
		const Math::Matrix4f& transform,
		Math::Box3f& box
		);

	struct Shape : public sn::IShape, public sn::IPersonage
	{
		bool bDirtyValid;
		Math::Box3f box;

		artist::ShapeValue shapeval;
		// ������ �������� �� ��������
		std::map<artist::attr_decl, int> attrs;
		// ������ ��������
		std::vector<char> attrsdata;

		Shape();
		~Shape();
		virtual void release(){delete this;};
	public:
		void getValue(sn::attr_t& attr, int& val);
		void getValue(sn::attr_t& attr, float& val);
		void getValue(sn::attr_t& attr, Math::Vec3f& val);
		void getValue(sn::attr_t& attr, Math::Matrix4f& val);

	};

protected:
//	bool getSnData(Shape* shape, Util::DllProcedure& dllproc, sn::IShaper* &shaper, sn::IShape* &pshape);
};
