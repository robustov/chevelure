#include "StdAfx.h"

#include "supernova/IArtistShape.h"
#include ".\snmothershaper.h"

#include "Math/Math.h"
#include "supernova/SnDelayMaya.h"
#include <maya\MFnMesh.h>
#include <maya\MItMeshPolygon.h>
#include <ri.h>
#include <gl/gl.h>
#include "mathNmaya/mathNmaya.h"
#include "supernova/ISnShaper.h"
#include "supernova/IArtistShape.h"
#include <maya\MFnDagNode.h>

SnMotherShaper::SnMotherShaper(void)
{
}

SnMotherShaper::~SnMotherShaper(void)
{
}

void SnMotherShaper::Recursive(MObject obj)
{
	MStatus stat;
	MFnDagNode dag(obj, &stat);
	if( !stat) return;
//	displayString( dag.fullPathName().asChar());
	for( unsigned i=0; i<dag.childCount(); i++)
	{
		MObject ch = dag.child(i, &stat);
		if(!stat) continue;

		Recursive(ch);

		MFnDependencyNode dn(ch, &stat);
		if(!stat) continue;

		MPlug plug = dn.findPlug("message", &stat);
		if(!stat) continue;

		MPlugArray plugarray;
		plug.connectedTo(plugarray, false, true, &stat);
		if(!stat) continue;
		for(unsigned p=0; p<plugarray.length(); p++)
		{
			MPlug plug = plugarray[p];
			MFnDependencyNode dn(plug.node(), &stat);
			if(!stat) continue;

			std::string type = dn.typeName().asChar();
			if( strcmp( type.c_str(), "SnShape")!=0)
				continue;

			displayString( "find %s", plug.name().asChar());
		}
	}
}

sn::IShape* SnMotherShaper::export_(
	artist::IShape& source,
	sn::IPersonage* personage
	)
{
	MStatus stat;
	MObject obj = source.getThis();
//	MDagPath path;
//	if( !MDagPath_to(obj, path))
//		return false;

//	MFnDagNode dag(path, &stat);
//	if( !stat) return NULL;

	MFnDagNode dag(obj, &stat);
	if( !stat) return NULL;
	MObject root = dag.parent(0, &stat);
	Recursive(root);
	return NULL;
}
// �������� �����
bool SnMotherShaper::save(
	sn::IShape* snshape, 
	Util::Stream& stream
	)
{
	return false;
}

// �������� �����
sn::IShape* SnMotherShaper::load(
	Util::Stream& stream,
	sn::IPersonage* personage)
{
	return 0;
}
// ���������
void SnMotherShaper::render(
	sn::IShape* shape, 
	sn::IPersonage* personage, 
	const Math::Matrix4f& transform,
	IPrman* prman,
	cls::IRender* render
	)
{
}
// ���������
void SnMotherShaper::renderGL(
	sn::IShape* snshape, 
	sn::IPersonage* personage,
	const Math::Matrix4f& transform
	)
{
}
// ���������
void SnMotherShaper::renderOcs(
	sn::IShape* snshape, 
	sn::IPersonage* personage, 
	const Math::Matrix4f& transform,
	cls::IRender* render, 
	int renderMode
	)
{
}

extern "C" 
{
	__declspec(dllexport) sn::IShaper* getMother();
}

sn::IShaper* getMother()
{
	return new SnMotherShaper();
}

