#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: SnShapeNode.h
//
// Dependency Graph Node: SnShape

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include "supernova/IArtistShape.h"
#include "supernova/ISnShaper.h"

class SnShape : public MPxNode
{
public:
	SnShape();
	virtual ~SnShape();

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	MStatus setDependentsDirty( const MPlug &plugBeingDirtied,
			MPlugArray &affectedPlugs );

	static void* creator();
	static MStatus initialize();

	bool autoReload;
public:
	static MObject i_message;
	static MObject i_autoReload;
	static MObject i_slimSurf;
	static MObject i_slimDispl;
	static MObject i_dllname;		// dllname
	static MObject i_procname;		// procname
	static MObject i_weight;
	static MObject o_output;		// Example output attribute

public:
	static MTypeId id;
};

