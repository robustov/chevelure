//
// Copyright (C) 
// File: SnSlimShaderCmd.cpp
// MEL Command: SnSlimShader

#include "SnSlimShader.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include "supernova/SnMath.h"

SnSlimShader::SnSlimShader()
{
}
SnSlimShader::~SnSlimShader()
{
}

MSyntax SnSlimShader::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.addArg(MSyntax::kString);
	return syntax;
}

MStatus SnSlimShader::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MString argsl0;
	argData.getCommandArgument(0, argsl0);

	MObject obj;
	if( !nodeFromName(argsl0, obj))
		return MS::kFailure;

	{
		std::vector<shaderExtern> displaceShaderParams;
		std::string displaceShader = mtorShaderNames( obj, "slimSurf", &displaceShaderParams);
		displayString("surface: %s", displaceShader.c_str());
		for( unsigned p=0; p<displaceShaderParams.size(); p++)
		{
			std::string name = displaceShaderParams[p].name.c_str();
			std::string type = displaceShaderParams[p].type.c_str();
			displayString("   %s %s", name.c_str(), type.c_str());
		}
	}

	{
		std::vector<shaderExtern> displaceShaderParams;
		std::string displaceShader = mtorShaderNames( obj, "slimDispl", &displaceShaderParams);
		displayString("displacement: %s", displaceShader.c_str());
		for( unsigned p=0; p<displaceShaderParams.size(); p++)
		{
			std::string name = displaceShaderParams[p].name.c_str();
			std::string type = displaceShaderParams[p].type.c_str();
			displayString("   %s %s", name.c_str(), type.c_str());
		}
	}

	return MS::kSuccess;
}

void* SnSlimShader::creator()
{
	return new SnSlimShader();
}

