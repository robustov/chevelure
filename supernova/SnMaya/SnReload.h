#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: SnReloadCmd.h
// MEL Command: SnReload

#include <maya/MPxCommand.h>

class SnReload : public MPxCommand
{
public:
	SnReload();
	virtual	~SnReload();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

