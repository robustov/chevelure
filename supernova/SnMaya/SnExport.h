#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: SnExportCmd.h
// MEL Command: SnExport

#include <maya/MPxCommand.h>

class SnExport : public MPxCommand
{
public:
	SnExport();
	virtual	~SnExport();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

