#pragma once

#include <Windows.h>

#include <maya/MIOStream.h>
#include <math.h>
#include <assert.h>

#include <maya/MFnDagNode.h>
#include <maya/MItDag.h>
#include <maya/MFnCamera.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFnNurbsSurface.h>
#include <maya/MFnTransform.h>
#include <maya/MFnMesh.h>
#include <maya/MDoubleArray.h>
#include <maya/MFloatArray.h>
#include <maya/MIntArray.h>
#include <maya/MPointArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MMatrix.h>
#include <maya/MTransformationMatrix.h>
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <maya/MPoint.h>
#include <maya/MString.h>
#include <maya/MTime.h>
#include <maya/MVector.h>
#include <maya/MFloatVector.h>
#include <maya/MArgList.h>
#include <maya/MColor.h>
#include <maya/MDagPath.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnStringArrayData.h>
#include <maya/MQuaternion.h>
#include <maya/MEulerRotation.h>
#include <maya/MItInstancer.h>
#include <maya/MPxCommand.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>

#include <maya/MPxFileTranslator.h>
#include <maya/MGlobal.h>
#include <maya/M3dView.h>
#include <maya/MItSurfaceCV.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItMeshVertex.h>
#include <maya/MDagPath.h>
#include <maya/MDagPathArray.h>
#include <maya/MSelectionList.h>
#include <maya/MAnimControl.h>
#include <maya/MFnStringData.h>
#include <maya/MFnPluginData.h>

#include <maya/MFnLight.h>
#include <maya/MFnNonAmbientLight.h>
#include <maya/MFnNonExtendedLight.h>
#include <maya/MFnAmbientLight.h>
#include <maya/MFnDirectionalLight.h>
#include <maya/MFnLightDataAttribute.h>
#include <maya/MFnPointLight.h>
#include <maya/MFnSpotLight.h>

#include <maya/MFnSet.h>
#include <maya/MFnLambertShader.h>
#include <maya/MFnBlinnShader.h>
#include <maya/MFnPhongShader.h>
#include <maya/MFnArrayAttrsData.h>

#include <math.h>
#include <maya/MIOStream.h>

#include <maya/MPxNode.h>   

#include <maya/MPxLocatorNode.h> 

#include <maya/MString.h> 
#include <maya/MTypeId.h> 
#include <maya/MPlug.h>
#include <maya/MVector.h>
#include <maya/MAngle.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MColor.h>
#include <maya/M3dView.h>
#include <maya/MDistance.h>

#include <maya/MFnUnitAttribute.h>
#include <maya/MFnNumericAttribute.h>

#include <maya/MFn.h>
#include <maya/MPxNode.h>
#include <maya/MPxManipContainer.h> 
#include <maya/MCommandResult.h> 

#include <maya/MFnCircleSweepManip.h>
#include <maya/MFnDirectionManip.h>
#include <maya/MFnDiscManip.h>
#include <maya/MFnDistanceManip.h> 
#include <maya/MFnFreePointTriadManip.h>
#include <maya/MFnStateManip.h>
#include <maya/MFnToggleManip.h>

#include <maya/MPxContext.h>
#include <maya/MPxSelectionContext.h>

#include <maya/MFnNumericData.h>
#include <maya/MManipData.h>
#include <maya/MTime.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnMatrixData.h>
#include <maya/MMatrix.h>
#include <maya/MGlobal.h>
#include <maya/MAnimMessage.h>
#include <maya/MMessage.h>
#include <maya/MPlugArray.h>
#include <maya/MFnAnimCurve.h>
#include <maya/MAnimControl.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MVectorArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MNodeMessage.h>

#include <maya/MIOStream.h>
#include <maya/MPxSurfaceShape.h>
#include <maya/MPxSurfaceShapeUI.h>
//#include <maya/MFnPlugin.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MPoint.h>
#include <maya/MPlug.h>
#include <maya/MDrawData.h>
#include <maya/MDrawRequest.h>
#include <maya/MSelectionMask.h>
#include <maya/MSelectionList.h>
#include <maya/MDagPath.h>
#include <maya/MMaterial.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFnNurbsCurveData.h>
#include <GL/glu.h>
#include <maya/MPointArray.h>
#include <maya/MDoubleArray.h>

#include <maya/MIOStream.h>
#include <maya/MPxSurfaceShape.h>
#include <maya/MPxSurfaceShapeUI.h>
#include <maya/MAttributeSpec.h>
#include <maya/MAttributeSpecArray.h>
#include <maya/MAttributeIndex.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MPoint.h>
#include <maya/MPlug.h>
#include <maya/MDrawData.h>
#include <maya/MDrawRequest.h>
#include <maya/MSelectionMask.h>
#include <maya/MSelectionList.h>
#include <maya/MDagPath.h>
#include <maya/MMaterial.h>
#include <maya/MFnSingleIndexedComponent.h>
#include <GL/glu.h>

#include <string>
#include <vector>
#include <map>
#include <list>
#include <set>
#include <algorithm>

/*/
#define e \
    if (MS::kSuccess != s) { \
		char buf[256];\
		sprintf(buf, "ERROR %d ", __LINE__);\
		MGlobal::displayError(buf);\
    }
/*/

inline void displayString(const char* text, ...)
{
	va_list argList; va_start(argList, text);
	char sbuf[256];
	_vsnprintf(sbuf, 255, text, argList);
	va_end(argList);
	MGlobal::displayInfo(sbuf);
}

inline MMatrix getMatrixForPlug(MPlug worldMatrix, MTime t)
{
	MDGContext context(t);
	MObject val;
	worldMatrix.getValue(val, context);
	MFnMatrixData matrData(val);
	MMatrix m = matrData.matrix();
	return m;
}

inline MMatrix move( const MMatrix& m, const MVector& v)
{
	MMatrix L = m;
	L(3, 0) += v[0];
	L(3, 1) += v[1];
	L(3, 2) += v[2];
//	L(3, 3) += v[3];
	return L;
}

inline void displayPoint( const MPoint& p)
{
	displayString("%g, %g, %g, %g", p[0], p[1], p[2], p[3]);
}

inline void displayMatrix( const MMatrix& L)
{
	displayString("%g, %g, %g, %g", L(0, 0), L(0, 1), L(0, 2), L(0, 3));
	displayString("%g, %g, %g, %g", L(1, 0), L(1, 1), L(1, 2), L(1, 3));
	displayString("%g, %g, %g, %g", L(2, 0), L(2, 1), L(2, 2), L(2, 3));
	displayString("%g, %g, %g, %g", L(3, 0), L(3, 1), L(3, 2), L(3, 3));
}

// Equivalence test for floats.  Equality tests are dangerous for floating      
// point values 
//
#define FLOAT_EPSILON 0.0001

inline bool equiv( float val1, float val2 )
{
    return ( fabsf( val1 - val2 ) < FLOAT_EPSILON );
}

// CMD
inline MStatus nodeFromName(MString name, MObject & obj)
{
	MSelectionList tempList;
    tempList.add( name );
    if ( tempList.length() > 0 ) 
	{
		tempList.getDependNode( 0, obj );
		return MS::kSuccess;
	}
	return MS::kFailure;
}
// CMD
inline MStatus plugFromName(MString name, MPlug & obj)
{
	MSelectionList tempList;
    tempList.add( name );
    if ( tempList.length() > 0 ) 
	{
		tempList.getPlug( 0, obj );
		return MS::kSuccess;
	}
	return MS::kFailure;
}

template <class T>
void copyBox(T dst[6], MBoundingBox& src)
{
	dst[0] = (T)src.min().x;
	dst[1] = (T)src.max().x;
	dst[2] = (T)src.min().y;
	dst[3] = (T)src.max().y;
	dst[4] = (T)src.min().z;
	dst[5] = (T)src.max().z;
}

enum aTypes
{
	atReadable		= 0x1,
	atUnReadable	= 0x10000,
	atWritable		= 0x2,
	atUnWritable	= 0x20000,
	atConnectable	= 0x4,
	atUnConnectable	= 0x40000,
	atStorable		= 0x8,
	atUnStorable	= 0x80000,
	atCached		= 0x10,
	atUnCached		= 0x100000,
	atArray			= 0x20, 
	atKeyable		= 0x40,
	atHidden		= 0x80,
	atUnHidden		= 0x800000,
	atUsedAsColor	= 0x100,
	atRenderSource	= 0x200,

	atInput			= atReadable|atWritable|atStorable|atCached,
	atOutput		= atReadable|atUnWritable|atConnectable|atUnStorable,
};

inline void addAttribute(MObject& aobj, int flags)
{
	if( aobj.isNull())
	{
		displayString("cant create attribute");
		throw MString("cant create attribute");
	}

	MFnAttribute attr(aobj);
	if(flags & atReadable)
		attr.setReadable(true);
	if(flags & atUnReadable)
		attr.setReadable(false);

	if(flags & atWritable)
		attr.setWritable(true);
	if(flags & atUnWritable)
		attr.setWritable(false);

	if(flags & atConnectable)
		attr.setConnectable(true);
	if(flags & atUnConnectable)
		attr.setConnectable(false);

	if(flags & atStorable)
		attr.setStorable(true);
	if(flags & atUnStorable)
		attr.setStorable(false);

	if(flags & atCached)
		attr.setCached(true);
	if(flags & atUnCached)
		attr.setCached(false);

	if(flags & atArray)
		attr.setArray(true);
	if(flags & atKeyable)
		attr.setKeyable(true);

	if(flags & atHidden)
		attr.setHidden(true);
	if(flags & atUnHidden)
		attr.setHidden(false);

	if(flags & atUsedAsColor)
		attr.setUsedAsColor(true);
	if(flags & atRenderSource)
		attr.setRenderSource(true);

	MStatus stat;
	stat = MPxNode::addAttribute(attr.object());
	if(!stat)
	{
		displayString("cant add attribute %s %s", attr.name().asChar(), stat.errorString().asChar());
		throw attr.name();
	}
}


inline MStatus SourceMelFromResource(HINSTANCE hModule, LPCSTR lpName, LPCSTR lpType)
{
	HRSRC res = FindResource( hModule, lpName, lpType);
	if( !res)
	{
		displayString("FindResource %s failed", lpName);
		return MS::kFailure;
	}

	HGLOBAL mem = ::LoadResource(MhInstPlugin, res);
	if(!mem)
	{
		displayString("LoadResource %s failed", lpName);
		return MS::kFailure;
	}
	DWORD size = SizeofResource(hModule, res);

	char* data = (char*)LockResource(mem);
	if(!data)
	{
		displayString("LockResource %s failed", lpName);
		return MS::kFailure;
	}

	MString str(data, size);
	MStatus stat = MGlobal::executeCommand(str);
	if(!stat)
	{
		displayString("execute %s failed", lpName);
		return MS::kFailure;
	}
#ifdef _DEBUG
	{
		FILE* file = fopen(lpName, "wt");
		if(file)
		{
			fputs(str.asChar(), file);
			fclose(file);
		}
	}
#endif
	return stat;
}
