#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: SnSlimShaderCmd.h
// MEL Command: SnSlimShader

#include <maya/MPxCommand.h>

class SnSlimShader : public MPxCommand
{
public:
	SnSlimShader();
	virtual	~SnSlimShader();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

