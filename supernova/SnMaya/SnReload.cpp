//
// Copyright (C) 
// File: SnReloadCmd.cpp
// MEL Command: SnReload

#include "SnReload.h"
#include "supernova/IArtistShape.h"
#include "supernova/ISnShaper.h"
#include "SnArtistShape2.h"
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnPluginData.h>

#include <maya/MGlobal.h>
#include "Util/STLStream.h"
#include "Util/DllProcedure.h"
#include "supernova/SnData.h"

#include <maya/MGlobal.h>

SnReload::SnReload()
{
}
SnReload::~SnReload()
{
}

MSyntax SnReload::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.addArg(MSyntax::kString);	// arg0
	return syntax;
}

MStatus SnReload::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MString argsl0;
	argData.getCommandArgument(0, argsl0);
	MPlug plug;
	if( !plugFromName(argsl0, plug))
		return MS::kFailure;
 
	displayString("SnReload %s", plug.name().asChar());
	MFnDependencyNode dn(plug.node());

	MString dllstr;
	dn.findPlug("dllname").getValue(dllstr);
	MString procstr;
	dn.findPlug("procname").getValue(procstr);

	std::string dll, proc;
	dll = dllstr.asChar();
	proc = procstr.asChar();

	Util::DllProcedure dllproc(dll.c_str(), proc.c_str());
	if( !dllproc.isValid())
	{
		displayString("procedure %s or library %s not found", proc.c_str(), dll.c_str());
		return MS::kUnknownParameter;
	}

	sn::tag_GetShaper gs = (sn::tag_GetShaper)dllproc.proc;
	sn::IShaper* shaper = (*gs)();
	if(!shaper)
	{
		displayString("procedure %s or library %s failed", proc.c_str(), dll.c_str());
		return MS::kUnknownParameter;
	}

	SnArtistShape2 artistShape( plug.node());
	sn::IShape* shape = shaper->export_( artistShape, NULL);
	if( !shape)
	{
		return MS::kFailure;
	}
	MFnPluginData fnDataCreator;
	MTypeId tmpid( SnData::id );
	MObject newDataObject = fnDataCreator.create( tmpid, &stat );
	SnData* pData = (SnData*)fnDataCreator.data( &stat );
	pData->ssp = new sn::ShapeShortcut(dllproc, shaper, shape);
	pData->attrlist = artistShape.attrlist;

	MPlug plug_x = dn.findPlug("weight");
	if( !pData->weight.load(plug_x, 128, 128, ""))
		displayString("load texture for %s FAILED", plug_x.name().asChar());

	/*/
	Util::MemStream stream;
	SnArtistShape2 artistShape( plug.node());
	stream.open();
	stream >> dll >> proc;
	shaper->export( stream, artistShape, NULL);
	stream.close();

	MFnPluginData fnDataCreator;
	MTypeId tmpid( SnData::id );
	MObject newDataObject = fnDataCreator.create( tmpid, &stat );
	SnData* pData = (SnData*)fnDataCreator.data( &stat );
	pData->data = stream.writedata;
	pData->attrlist = artistShape.attrlist;
	/*/

	plug.setValue(newDataObject);

	return MS::kSuccess;
}

void* SnReload::creator()
{
	return new SnReload();
}

