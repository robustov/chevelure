//
// Copyright (C) 
// File: SnInstancerCmd.cpp
// MEL Command: SnInstancer

#include "SnInstancer.h"

#include "supernova/SnData.h"
#include <maya/MGlobal.h>
#include "MathNMaya/MathNMaya.h"
#include "SnPersonage.h"

MTypeId     SnInstancer::id( 0xC0FFEEa0 );

MObject SnInstancer::i_shape;
MObject SnInstancer::i_id;
MObject SnInstancer::i_pos;
MObject SnInstancer::i_x;
MObject SnInstancer::i_y;
MObject SnInstancer::i_z;
MObject SnInstancer::o_output;

SnInstancer::SnInstancer()
{
	bValidBox = false;
}
SnInstancer::~SnInstancer()
{
}

MStatus SnInstancer::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;

	if( plug == o_output )
	{
		MDataHandle inputData = data.inputValue(i_shape, &stat);
		inputData.asPluginData();

		MDataHandle outputData = data.outputValue( o_output, &stat );
		outputData.set(0.0);
		data.setClean(plug);

		bValidBox = false;	
		return MS::kSuccess;
	}

	return MS::kUnknownParameter;
}

bool SnInstancer::isBounded() const
{
	return bValidBox;
}

MBoundingBox SnInstancer::boundingBox() const
{
	return box;
}

void SnInstancer::draw(
	M3dView &view, 
	const MDagPath &path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status)
{ 
	MObject thisNode = thisMObject();

	MStatus stat;
	MPlug plug(thisNode, i_shape);
	MObject val;
	plug.getValue(val);
	MFnPluginData pdata( val, &stat);
	if( !stat) return;
	SnData* sndata = (SnData*)pdata.constData(&stat);
	if( !sndata) return;

	sn::ShapeShortcutPtr shape = sndata->ssp;
	if( !shape.isValid()) return;

	MVectorArray pos;
	{
		MPlug plug(thisNode, i_pos);
		MObject val;
		plug.getValue(val);
		MFnVectorArrayData va(val);
		pos = va.array();
	}

	SnPersonage personage(thisNode);
	view.beginGL();
	for( unsigned i=0; i<pos.length(); i++)
	{
		Math::Vec3f p; copy( p, pos[i]);
		glPushMatrix();

		Math::Matrix4f m = Math::Matrix4f::id;
		m[3] = Math::Vec4f( p, 1.f);

//		float* d = m.data();
//		glMultMatrixf(d);

		shape->shaper->renderGL(shape->shape, &personage, m);

//		glPopMatrix();
	}
	view.endGL();
};


void* SnInstancer::creator()
{
	return new SnInstancer();
}

MStatus SnInstancer::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		{
			i_shape = typedAttr.create( "i_shape", "isnsh", SnData::id);
			::addAttribute(i_shape, atInput|atConnectable);
		}
		{
			i_id = typedAttr.create( "idvec", "id", MFnData::kDoubleArray);
			::addAttribute(i_id, atInput|atConnectable);
		}
		{
			i_pos = typedAttr.create( "posvec", "pos", MFnData::kVectorArray);
			::addAttribute(i_pos, atInput|atConnectable);
		}
		{
			i_x = typedAttr.create( "xRef", "xr", MFnData::kString);
			::addAttribute(i_x, atInput|atConnectable);
			i_y = typedAttr.create( "yRef", "yr", MFnData::kString);
			::addAttribute(i_y, atInput|atConnectable);
			i_z = typedAttr.create( "zRef", "zr", MFnData::kString);
			::addAttribute(i_z, atInput|atConnectable);
		}
		{
			o_output = numAttr.create( "o_output", "out", MFnNumericData::kDouble, 0.01, &stat);
			::addAttribute(o_output, atOutput);
			stat = attributeAffects( i_shape, o_output );
			stat = attributeAffects( i_id, o_output );
			stat = attributeAffects( i_pos, o_output );

			stat = attributeAffects( i_x, o_output );
			stat = attributeAffects( i_y, o_output );
			stat = attributeAffects( i_z, o_output );
		}
		if( !SourceMelFromResource(MhInstPlugin, "AESnInstancerTemplate.mel", "MEL"))
		{
			displayString("error source AESnInstancerTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}