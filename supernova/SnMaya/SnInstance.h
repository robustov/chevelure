#pragma once

#include "pre.h"
#include <maya/MPxLocatorNode.h>
#include "Util/DllProcedure.h"
#include "supernova/SnData.h"

 
class SnInstance : public MPxLocatorNode
{
public:
	Util::DllProcedure dllproc;
	sn::IShaper* instance;

	bool bValidBox;
	MBoundingBox box;

public:
	SnInstance();
	virtual ~SnInstance(); 

	MStatus setDependentsDirty( const MPlug &plugBeingDirtied,
			MPlugArray &affectedPlugs );
	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	virtual void draw(
		M3dView &view, const MDagPath &path, 
		M3dView::DisplayStyle style,
		M3dView::DisplayStatus status);

	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 

	static void* creator();
	static MStatus initialize();

	static bool getSnData(
		MObject object, 
		Util::DllProcedure& dllproc, 
		sn::IShaper* &shaper, 
		sn::IShape* &pshape);

public:
	static MObject i_shape;
	static MObject o_output;
public:
	static	MTypeId		id;
};


