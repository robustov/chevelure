//
// Copyright (C) 
// File: SnMotherCmd.cpp
// MEL Command: SnMother

#include "SnMother.h"

#include <maya/MGlobal.h>
#include "supernova/SnData.h"
#include "Util/STLStream.h"
#include "Util/DllProcedure.h"
#include "SnArtistShape2.h"

MTypeId     SnMother::id( 0xC0FFEE13 );

MObject SnMother::i_input;
MObject SnMother::o_output;
MObject SnMother::i_autoReload;

SnMother::SnMother()
{
	mother = NULL;
	dllproc = Util::DllProcedure("SnMath.dll", "getMother");
	if(dllproc.isValid())
	{
		sn::tag_GetShaper gs = (sn::tag_GetShaper)dllproc.proc;
		mother = (*gs)();
	}
}
SnMother::~SnMother()
{
	if( mother) mother->release();
}

MStatus SnMother::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;

	displayString("SnMother::compute %s", plug.name().asChar());

	if( plug == o_output )
	{
		MDataHandle inputData = data.inputValue( i_input, &stat );

		SnArtistShape2 artistShape( thisMObject());
		sn::IShape* shape = mother->export_(artistShape, NULL);
		if( !shape)
			return MS::kFailure;

		MFnPluginData fnDataCreator;
		MTypeId tmpid( SnData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		SnData* pData = (SnData*)fnDataCreator.data( &stat );
		pData->ssp = new sn::ShapeShortcut(this->dllproc, mother, shape);
		pData->attrlist = artistShape.attrlist;

		MDataHandle outputData = data.outputValue( o_output, &stat );
		outputData.set(pData);

		return MS::kSuccess;
	}

	return MS::kUnknownParameter;
}

bool SnMother::isBounded() const
{
	MObject thisNode = thisMObject();
	return false;
}

MBoundingBox SnMother::boundingBox() const
{
	MObject thisNode = thisMObject();

	MBoundingBox box;
	//...
	return box;
}

void SnMother::draw(
	M3dView &view, 
	const MDagPath &path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status)
{ 
	MObject thisNode = thisMObject();

	MPlug plug(thisNode, i_input);

	view.beginGL(); 
		glBegin(GL_LINE_STRIP);
		glVertex3f( -1, 0, -1);
		glVertex3f( -1, 0,  1);
		glVertex3f(  1, 0,  1);
		glVertex3f(  1, 0, -1);
		glVertex3f( -1, 0, -1);
		glEnd();
	view.endGL();
};


void* SnMother::creator()
{
	return new SnMother();
}

MStatus SnMother::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		{
			i_autoReload = numAttr.create( "i_autoReload", "are", MFnNumericData::kBoolean, 0, &stat ); 
			::addAttribute(i_autoReload, atInput);
		}
		{
			i_input = numAttr.create ("i_input","iin", MFnNumericData::kDouble, 0.01, &stat);
			numAttr.setMin(0.0);	
			numAttr.setMax(1.0);	
			::addAttribute(i_input, atInput);
		}
		{
			MFnTypedAttribute fnAttr;
			o_output = fnAttr.create( "o_output", "out", SnData::id );
			::addAttribute(o_output, atReadable|atWritable|atConnectable|atStorable);
			stat = attributeAffects( i_input, o_output );
			stat = attributeAffects( i_autoReload, o_output );
		}
		if( !SourceMelFromResource(MhInstPlugin, "AESnMotherTemplate.mel", "MEL"))
		{
			displayString("error source AESnMotherTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}