#include "pre.h"
#include "SnPersonage.h"
#include "mathNmaya/mathNmaya.h"

SnPersonage::SnPersonage(MObject obj)
{
	this->obj = obj;
}

void SnPersonage::getValue(sn::attr_t& attr, float& val)
{
	val = 0;
	MStatus stat;
	MFnDependencyNode dn(obj);
	MPlug pl = dn.findPlug(attr.name.c_str(), &stat);
	if(!stat) return;

	pl.getValue(val);
}
void SnPersonage::getValue(sn::attr_t& attr, Math::Vec3f& val)
{
	val = Math::Vec3f(0, 0, 0);
	/*/
	MStatus stat;
	MFnDependencyNode dn(obj);
	MPlug pl = dn.findPlug(attr.name.c_str(), &stat)
	if(!stat) return;

	pl.getValue(val);
	/*/
}
void SnPersonage::getValue(sn::attr_t& attr, Math::Matrix4f& val)
{
	val = Math::Matrix4f::id;

	MStatus stat;
	MFnDependencyNode dn(obj);
	MPlug pl = dn.findPlug(attr.name.c_str(), &stat);

	MObject dataObject;
	pl.getValue( dataObject);
	MFnMatrixData matDataFn( dataObject );
	MMatrix mm = matDataFn.matrix();
	copy( val, mm);

	/*/
	val = Math::Vec3f(0, 0, 0);
	MStatus stat;
	MFnDependencyNode dn(obj);
	MPlug pl = dn.findPlug(attr.name.c_str(), &stat)
	if(!stat) return;
	/*/
}
