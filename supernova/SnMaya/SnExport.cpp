//
// Copyright (C) 
// File: SnExportCmd.cpp
// MEL Command: SnExport

#include "SnExport.h"

#include <maya/MGlobal.h>
#include "supernova/SnData.h"
#include "SnInstance.h"
#include "Util/FileStream.h"
#include "SnArtistShape2.h"

SnExport::SnExport()
{
}
SnExport::~SnExport()
{
}

MSyntax SnExport::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.addArg(MSyntax::kString);	// object
	syntax.addArg(MSyntax::kString);	// file
	return syntax;
}

MStatus SnExport::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MString shapename;
	argData.getCommandArgument(0, shapename);
	MString filename;
	argData.getCommandArgument(1, filename);

	setResult((int)0);
	MObject obj;
	if( !nodeFromName(shapename, obj))
	{
		displayString("node %s not found", shapename.asChar());
		return MS::kFailure;
	}

	// 

	std::string dll  = "SnMath.dll";
	std::string proc = "getInstance";
	Util::DllProcedure dllproc(dll.c_str(), proc.c_str());
	if(dllproc.isValid())
	{
		sn::tag_GetShaper gs = (sn::tag_GetShaper)dllproc.proc;
		sn::IShaper* instance = (*gs)();

		SnArtistShape2 artistShape( obj);
		sn::IShape* shape = instance->export_(artistShape, NULL);

		Util::FileStream stream;
		if( shape && stream.open(filename.asChar(), true))
		{
			stream >> dll >> proc;
			instance->save(shape, stream);
		}
		if( shape) shape->release();
		instance->release();
	}


/*/
	MString attrname = "o_export";
	MFnDependencyNode dn(obj);
	MPlug plug = dn.findPlug(attrname, &stat);
	if(!stat)
	{
		displayString("not found attribute o_export", attrname.asChar());
		return MS::kFailure;
	}

	MObject val;
	if( !plug.getValue(val))
		return MS::kFailure;
	MFnPluginData pdata( val, &stat);
	if( !stat)
		return MS::kFailure;
	SnData* sndata = (SnData*)pdata.constData(&stat);
	if( !sndata)
		return MS::kFailure;

	Util::FileStream stream;
	if( stream.open(filename.asChar(), true))
	{
		stream.serialize(&sndata->data[0], sndata->data.size());
	}
//	displayString("Export to %s completed", filename.asChar());
/*/
	setResult((int)1);
	return MS::kSuccess;
}

void* SnExport::creator()
{
	return new SnExport();
}

