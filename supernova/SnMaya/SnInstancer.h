#pragma once

#include "pre.h"
#include <maya/MPxLocatorNode.h>

 
class SnInstancer : public MPxLocatorNode
{
	bool bValidBox;
	MBoundingBox box;

public:
	SnInstancer();
	virtual ~SnInstancer(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	virtual void draw(
		M3dView &view, const MDagPath &path, 
		M3dView::DisplayStyle style,
		M3dView::DisplayStatus status);

	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_shape;
	static MObject i_id;
	static MObject i_pos;
	static MObject i_x;
	static MObject i_y;
	static MObject i_z;
	static MObject o_output;

public:
	static MTypeId id;
};


