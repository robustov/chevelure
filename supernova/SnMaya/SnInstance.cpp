//
// Copyright (C) 
// File: SnInstanceCmd.cpp
// MEL Command: SnInstance

#include "SnInstance.h"

#include <maya/MGlobal.h>
#include "Util/STLStream.h"
#include "Util/STLStream.h"
#include "Util/DllProcedure.h"
#include "supernova/IArtistShape.h"
#include "supernova/ISnShaper.h"
#include "SnPersonage.h"
#include "SnArtistShape2.h"
#include "MathNMaya\MathNMaya.h"

MTypeId SnInstance::id( 0xC0FFEEf3 );

MObject SnInstance::i_shape;
MObject SnInstance::o_output;

SnInstance::SnInstance()
{
	instance = NULL;
	dllproc = Util::DllProcedure("SnMath.dll", "getInstance");
	if(dllproc.isValid())
	{
		sn::tag_GetShaper gs = (sn::tag_GetShaper)dllproc.proc;
		instance = (*gs)();
	}
	bValidBox = false;
}
SnInstance::~SnInstance()
{
	if( instance) instance->release();
}
MStatus SnInstance::setDependentsDirty( 
	const MPlug &plugBeingDirtied,
	MPlugArray &affectedPlugs )
{
	displayString("SnInstance::setDependentsDirty %s", plugBeingDirtied.name().asChar());

	MStatus stat;
	MObject thisNode = thisMObject();
	MFnDependencyNode fnThisNode( thisNode );

	return MS::kSuccess;
}

MStatus SnInstance::compute( const MPlug& plug, MDataBlock& data )
{
	displayString("SnInstance::compute %s", plug.name().asChar());
	MStatus stat;
	if( plug == o_output )
	{
		MDataHandle inputData = data.inputValue(i_shape, &stat);
		inputData.asPluginData();

		MDataHandle outputData = data.outputValue( o_output, &stat );
		outputData.set(0.0);
		data.setClean(plug);

		bValidBox = false;	
		SnArtistShape2 artistShape( thisMObject());
		sn::IShape* pshape = instance->export_(artistShape, NULL);
		if( pshape)
		{
			Math::Box3f db;
			if( instance->getDirtyBox(pshape, Math::Matrix4f::id, db))
				copy( box, db);
			pshape->release();
			bValidBox = true;
		}

		displayString("SnInstance::compute %s", plug.name().asChar());
		return MS::kSuccess;
	}
	return MS::kUnknownParameter;
}

bool SnInstance::isBounded() const
{
	return bValidBox;
}

MBoundingBox SnInstance::boundingBox() const
{
	return box;
}

void SnInstance::draw(
	M3dView &view, 
	const MDagPath &path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status)
{ 
	MStatus stat;
	MObject thisNode = thisMObject();

	double z;
	MFnDependencyNode(thisNode).findPlug(o_output).getValue(z);

	SnArtistShape2 artistShape( thisMObject());
	sn::IShape* pshape = instance->export_(artistShape, NULL);
	if( !pshape)
		return;
/*/
	Util::MemStream stream;
	SnArtistShape2 artistShape( thisMObject());
	stream.open();
	instance->export(stream, artistShape, NULL);
	stream.close();
	sn::IShape* pshape = instance->load(stream, NULL);
	if( !pshape)
		return;
/*/
	
	view.beginGL();
	instance->renderGL(pshape, NULL, Math::Matrix4f::id);
	view.endGL();

	pshape->release();
};


void* SnInstance::creator()
{
	return new SnInstance();
}
 
MStatus SnInstance::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		{
			i_shape = typedAttr.create( "i_shape", "isnsh", SnData::id);
			::addAttribute(i_shape, atInput|atConnectable);
		}
		{
			o_output = numAttr.create( "output", "out", MFnNumericData::kDouble);
			::addAttribute(o_output, atOutput);
			attributeAffects(i_shape, o_output);
		}
		if( !SourceMelFromResource(MhInstPlugin, "AESnInstanceTemplate.mel", "MEL"))
		{
			displayString("error source AEHairShapeTemplate");
		}

	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}