//
// Copyright (C) 
// File: SnDataCmd.cpp
// MEL Command: SnData

#include "supernova/SnData.h"
#include "supernova/IArtistShape.h"
#include "pre.h"

#include <maya/MGlobal.h>
#include <maya/MIOStream.h>

MTypeId SnData::id( SN_DATAID );
const MString SnData::typeName( "SnData" );

SnData::SnData()
{
//	displayString( "new SnData %x", (int)this);
}
SnData::~SnData()
{
//	if( ssp.isValid())
//	{
//		displayString( "del SnData %x: %x: ref=%d", (int)this, (int)ssp.pss, ssp->refcount);
//	}
//	else
//	{
//		displayString( "del SnData %x", (int)this);
//	}
}

MTypeId SnData::typeId() const
{
	return SnData::id;
}

MString SnData::name() const
{ 
	return SnData::typeName; 
}

void* SnData::creator()
{
	return new SnData();
}

void SnData::copy( const MPxData& other )
{
	this->ssp = ((SnData*)&other)->ssp;
//	this->data = ((SnData*)&other)->data;
	this->attrlist = ((SnData*)&other)->attrlist;
	this->weight = ((SnData*)&other)->weight;
//	this->weight.reset();
//	this->weight.defValue = ((SnData*)&other)->weight.defValue;
}

MStatus SnData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	/*/
	int argLength = args.length();
	if(argLength!=1)
		return MS::kFailure;

	MString str = args.asString(0);
	int len = str.length()/2;
	data.resize(len);
	for( int i=0; i<len; i++)
	{
		unsigned char c0 = str.asChar()[i*2+0];
		unsigned char c1 = str.asChar()[i*2+1];
		unsigned char ch = atoi(c0) + (atoi(c1)<<4);
		data[i] = ch;
	}
	/*/
	return MS::kSuccess;
}

MStatus SnData::writeASCII( 
	std::ostream& out )
{
    MStatus stat;
    return MS::kSuccess;
}

MStatus SnData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	return MS::kSuccess;
}

MStatus SnData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

    return MS::kSuccess;
}

