#include "SnShape.h"
#include <io.h>

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnPluginData.h>

#include <maya/MGlobal.h>
#include "Util/STLStream.h"
#include "Util/DllProcedure.h"
#include "Util/FileStream.h"
#include "supernova/SnData.h"
#include "SnArtistShape2.h"

/*/
global proc AESnShapeReload(string $ftn)
{
	print("AESnShapeReload: "+$ftn+"\n");
	frameLayout -borderVisible false -collapsable true -labelVisible false -collapse false characterMemberFrameLayout;
		columnLayout characterMemberLayout;
		setParent ..;
	setParent ..;
	AESnShapeReloadReplace($ftn);
}

global proc AESnShapeReloadReplace (string $ftn)
{
	print("AESnShapeReloadReplace: "+$ftn+"\n");
	setParent characterMemberLayout;
	
	// Delete the controls that are there now
	string $children[] = `columnLayout -query -childArray characterMemberLayout`;
	string $cmd = "deleteUI ";
	for ( $child in $children ) 
	{
		$cmd += $child;
		$cmd += " ";
	}
	if ( `size $children` > 0 )
	{
		// Only call evalDeferred once so the attrFieldSliderGrps don't get
		// deleted one by one.
		evalDeferred( $cmd );
	}
	setParent characterMemberLayout;
	
	rowLayout -nc 2;
		text -l $ftn;
		button -l "Reload SN data" 
			   -c ("SnReload " + $ftn) fileReloadBtn;
		setParent ..;
}
/*/

MTypeId SnShape::id( 0xC0FFEE05 );

MObject SnShape::i_message;
MObject SnShape::i_autoReload;
MObject SnShape::i_slimSurf;
MObject SnShape::i_slimDispl;
MObject SnShape::i_dllname;
MObject SnShape::i_procname;
MObject SnShape::i_weight;
MObject SnShape::o_output;

SnShape::SnShape()
{
	autoReload = false;
}
SnShape::~SnShape()
{
}
// ����� plumage
MStatus SnShape::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;
	if( plug == o_output )
	{
		MDataHandle outputData = data.outputValue( o_output, &stat );
		MFnPluginData fnDataCreator;
		MTypeId tmpid( SnData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		SnData* pData = (SnData*)fnDataCreator.data( &stat );

		MString dllstr = data.inputValue(i_dllname, &stat).asString();
		MString procstr = data.inputValue(i_procname, &stat).asString();

		MObject slimSurf = data.inputValue(i_slimSurf, &stat).data();
		MObject slimDispl = data.inputValue(i_slimDispl, &stat).data();

		bool autoReload = data.inputValue(i_autoReload, &stat).asBool();
		this->autoReload = autoReload;
		std::string dll, proc;
		dll = dllstr.asChar();
		proc = procstr.asChar();

		Util::DllProcedure dllproc(dll.c_str(), proc.c_str());
		if( !dllproc.isValid())
		{
			displayString("procedure %s or library %s not found", proc.c_str(), dll.c_str());
			outputData.set(pData);
			data.setClean(plug);
			return MS::kFailure;
		}

		sn::tag_GetShaper gs = (sn::tag_GetShaper)dllproc.proc;
		sn::IShaper* shaper = (*gs)();
		if(!shaper)
		{
			displayString("procedure %s or library %s failed", proc.c_str(), dll.c_str());
			outputData.set(pData);
			data.setClean(plug);
			return MS::kFailure;
		}
		SnArtistShape2 artistShape( thisMObject());
		sn::IShape* shape = shaper->export_( artistShape, NULL);
		if( !shape)
		{
			displayString("export failed lib=%s proc=%s", proc.c_str(), dll.c_str());
			outputData.set(pData);
			data.setClean(plug);
			return MS::kFailure;
		}
		pData->ssp = new sn::ShapeShortcut(dllproc, shaper, shape);
		pData->attrlist = artistShape.attrlist;

		MPlug plug(thisMObject(), i_weight);
		if( !pData->weight.load(plug, 128, 128, ""))
			displayString("load texture for %s FAILED", plug.name().asChar());

		outputData.set(pData);
		data.setClean(plug);
		displayString("SnShape::compute %s", plug.name().asChar());
		return MS::kSuccess;
	}
	return MS::kUnknownParameter;
}

void* SnShape::creator()
{
	return new SnShape();
}

MStatus SnShape::setDependentsDirty( const MPlug &plugBeingDirtied,
		MPlugArray &affectedPlugs )
{
	if( autoReload)
	{
		if( plugBeingDirtied.attribute() == i_message)
			return MS::kSuccess;

		displayString("SnShape::setDependentsDirty %s", plugBeingDirtied.name().asChar());
		MStatus stat;
		MObject thisNode = thisMObject();
		MFnDependencyNode fnThisNode( thisNode );

		MPlug pB = fnThisNode.findPlug( o_output, &stat );
		affectedPlugs.append( pB );
	}

	return MS::kSuccess;
}

MStatus SnShape::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		{
			i_message = numAttr.create( "i_message", "imes", MFnNumericData::kInt, 0, &stat ); 
			numAttr.setStorable(false);
			::addAttribute(i_message, atConnectable|atInput);
		}
		{
			i_weight = numAttr.create( "weight", "wei", MFnNumericData::kDouble, 1, &stat ); 
			numAttr.setMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_weight, atConnectable|atInput);
		}
		{
			i_autoReload = numAttr.create( "i_autoReload", "are", MFnNumericData::kBoolean, 0, &stat ); 
			::addAttribute(i_autoReload, atInput);
		}
		{
			MFnTypedAttribute typedAttr;
			i_slimSurf = typedAttr.create("slimSurf", "ss", MFnData::kStringArray, &stat);
			::addAttribute(i_slimSurf, atConnectable|atUnStorable|atUnCached);
		}
		{
			i_slimDispl = typedAttr.create("slimDispl", "sd", MFnData::kStringArray, &stat);
			::addAttribute(i_slimDispl, atConnectable|atUnStorable|atUnCached);
		}

		{
			MFnStringData stringData;
			i_dllname = typedAttr.create("dllname", "dll", MFnData::kString, stringData.create(MString("SnTrivialShaper.dll"), &stat), &stat);
			::addAttribute(i_dllname, atInput);
		}
		{
			MFnStringData stringData;
			i_procname = typedAttr.create("procname", "proc", MFnData::kString, stringData.create(MString("getShaper"), &stat), &stat);
			::addAttribute(i_procname, atInput);
		}
		{
			MFnTypedAttribute fnAttr;
			o_output = fnAttr.create( "o_output", "out", SnData::id );
			::addAttribute(o_output, atReadable|atWritable|atConnectable|atStorable);
			stat = attributeAffects( i_slimSurf, o_output );
			stat = attributeAffects( i_slimDispl, o_output );
			stat = attributeAffects( i_autoReload, o_output );
			stat = attributeAffects( i_weight, o_output );
//			stat = attributeAffects( i_dllname, o_output );
//			stat = attributeAffects( i_procname, o_output );
//			stat = attributeAffects( i_mesh, o_output );
		}
		if( !SourceMelFromResource(MhInstPlugin, "AESnShapeTemplate.mel", "MEL"))
		{
			displayString("error source AESnShapeTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

