#pragma once
#include "pre.h"

#include "supernova/IArtistShape.h"
#include "supernova/ISnShaper.h"
#include "supernova/SnData.h"

class SnShape;
class SnArtistShape2 : public artist::IShape
{
	MObject obj, sourceObject;
//	MDataBlock* datablock;	// ���� �������� ��������������!

public:
	SnArtistShape2(MObject& obj);//, MDataBlock* datablock=NULL);
	MObject getThis();
	MObject getSourceObject();
	MPlug getPlug(const char* name);

	MObject getValue(const char* name, sn::VALUETYPE type, sn::ATTRIBUTE_PLACE place, const char* defminmax);
	bool getValue( bool& val, const char* name, sn::ATTRIBUTE_PLACE place, bool defval=false);
	bool getValue( int& val, const char* name, sn::ATTRIBUTE_PLACE place, int defval=0, int min=-666, int max=-666);
	bool getValue( float& val, const char* name, sn::ATTRIBUTE_PLACE place, float defval=0, float min=-666, float max=-666);
	bool getValue( Math::Vec3f& val, const char* name);
	bool getValue( Math::Matrix4f& val, const char* name);
	bool getValue( std::string& val, const char* name, sn::ATTRIBUTE_PLACE place, const char* defval="");
	bool getValue( artist::ShapeValue& val, const char* name);
	bool getValue( std::vector<char>& data, const artist::attr_decl& val);
	bool getValue( Math::Ramp& rampWidth, const char* name, float min = 0.f, float max = 1.f, float defval = 0.f);
	bool getValue( Math::ParamTex<unsigned char>& paramtex, const char* name, float defval = 0.f);

	// ��������� �������� ���. ������� (������� ������� ������ �� ���� � ������������� �� ���� �����)
	std::string mirrorAttr( MObject object, const char* attribute, int index);

	// ������ ��������
	sn::attr_t getAttr(
		const char* name,
		sn::VALUETYPE type, 
		sn::ATTRIBUTE_PLACE place, const char* defminmax=NULL);
	sn::attr_t getShaderAttr(
		const char* name, 
		const char* type);

	void displayString(const char* text, ...);

protected:
	MObject findNaddAttr(
		MObject obj, 
		const char* name, sn::VALUETYPE type, sn::ATTRIBUTE_PLACE place, const char* defminmax);
public:
	std::vector<artist::attr_decl> attrlist;
//	static void addAttributes(MObject obj, std::vector<artist::attr_decl>& attrlist);

	bool getValue( bool& val, MObject attr);
	bool getValue( int& val, MObject attr);
	bool getValue( float& val, MObject attr);
	bool getValue( Math::Vec3f& val, MObject attr);
	bool getValue( Math::Matrix4f& val, MObject attr);
	bool getValue( std::string& val, MObject attr);
	bool getValue( artist::ShapeValue& val, MObject attr);
};

