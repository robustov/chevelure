#pragma once

#include "pre.h"
#include <maya/MPxLocatorNode.h>
#include "Util/DllProcedure.h"
#include "supernova/SnData.h"

 
class SnMother : public MPxLocatorNode
{
	Util::DllProcedure dllproc;
	sn::IShaper* mother;
public:
	SnMother();
	virtual ~SnMother(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	virtual void draw(
		M3dView &view, const MDagPath &path, 
		M3dView::DisplayStyle style,
		M3dView::DisplayStatus status);

	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_input;
	static MObject o_output;
	static MObject i_autoReload;

public:
	static MTypeId id;
};


