#include "pre.h"
#include "SnInstance.h"
#include "SnShape.h"
#include "supernova/SnData.h"
#include "SnMother.h"
#include "SnExport.h"
#include "SnReload.h"
#include "SnInstancer.h"
#include "SnSlimShader.h"

#include <maya/MFnPlugin.h>

MStatus uninitializePlugin( MObject obj);
MStatus initializePlugin( MObject obj )
{ 
	// ��� �������� �����
#ifdef HAIR_TRACELOG
	char filename[512]; GetModuleFileName( (HMODULE)MhInstPlugin, filename, 512); fprintf(stderr, "%s\n", filename);
#endif

	MStatus   stat;
	MFnPlugin plugin( obj, "ULITKA", "6.0", "Any");

	stat = plugin.registerCommand( "SnSlimShader", SnSlimShader::creator, SnSlimShader::newSyntax );
	if (!stat) {
		displayString("cant register node SnSlimShader");
		stat.perror("registerCommand");
		return stat;
	}

	stat = plugin.registerData( "SnData", SnData::id, SnData::creator);
	if (!stat) 
	{
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerNode( "SnInstancer", SnInstancer::id, SnInstancer::creator,
								  SnInstancer::initialize, MPxNode::kLocatorNode );
	if (!stat) 
	{
		displayString("cant register node SnInstancer");	
		stat.perror("registerNode");
		return stat;
	}


	stat = plugin.registerNode( "SnMother", SnMother::id, SnMother::creator,
								  SnMother::initialize, MPxNode::kLocatorNode );
	if (!stat) {
		stat.perror("registerNode");
		return stat;
	}

	stat = plugin.registerCommand( "SnReload", SnReload::creator, SnReload::newSyntax );
	if (!stat) {
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerCommand( "SnExport", SnExport::creator, SnExport::newSyntax );
	if (!stat) {
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}
  
	stat = plugin.registerNode( "SnShape", SnShape::id, SnShape::creator, SnShape::initialize );
	if (!stat) {
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerNode( "SnInstance", SnInstance::id, SnInstance::creator,
								  SnInstance::initialize, MPxNode::kLocatorNode );
	if (!stat) {
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	if( !SourceMelFromResource(MhInstPlugin, "SN.MEL", "MEL"))
	{
		displayString("error source SN.MEL");
	}
	return stat;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus   stat;
	MFnPlugin plugin( obj );

	stat = plugin.deregisterCommand( "SnSlimShader" );
	if (!stat) displayString("cant deregister SnSlimShader");

	stat = plugin.deregisterNode( SnInstancer::id );
	if (!stat) {
		stat.perror("deregisterNode");
		return stat;
	}

	stat = plugin.deregisterNode( SnMother::id );
	if (!stat) {
		stat.perror("deregisterNode");
		return stat;
	}

	stat = plugin.deregisterCommand( "SnReload" );
	if (!stat) {
		stat.perror("deregisterCommand");
		return stat;
	}

	stat = plugin.deregisterCommand( "SnExport" );
	if (!stat) {
		stat.perror("deregisterCommand");
		return stat;
	}

	stat = plugin.deregisterNode( SnShape::id );
	if (!stat) {
		stat.perror("deregisterNode");
		return stat;
	}

	stat = plugin.deregisterData( SnData::id );
	if (!stat) 
	{
		stat.perror("deregisterData");
		return stat;
	}

	stat = plugin.deregisterNode( SnInstance::id );
	if (!stat) {
		stat.perror("deregisterNode");
		return stat;
	}

	return stat;
}

