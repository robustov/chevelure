#pragma once

#include "supernova/ISnShaper.h"

struct SnPersonage : public sn::IPersonage
{
	MObject obj;
	SnPersonage(MObject obj);
	virtual void getValue(sn::attr_t& attr, Math::Matrix4f& val);
	virtual void getValue(sn::attr_t& attr, Math::Vec3f& val);
	virtual void getValue(sn::attr_t& attr, float& val);
};
