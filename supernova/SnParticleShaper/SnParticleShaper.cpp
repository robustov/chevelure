// SnParticleShaper.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "SnDelayMaya.h"
#include "IArtistShape.h"
#include "SnParticleShaper.h"

#include <maya\MObject.h>
#include <maya\MFnParticleSystem.h>
#include <maya\MFnVectorArrayData.h>
#include <maya\MFnDoubleArrayData.h>
#include <maya\MFnDependencyNode.h> 
#include <maya\MFnSkinCluster.h>
#include <maya\MItDependencyGraph.h>
#include <maya\MFnMesh.h>
#include <maya\MItMeshPolygon.h>
#include <maya\MAnimControl.h>
#include <maya\MTime.h>
#include <ri.h>
#include <gl/gl.h>
#include "ISnDeformer.h"
#include "SnMath.h"
#include "MathNMaya/MathNMaya.h"

extern "C" 
{
	__declspec(dllexport) sn::IShaper* getShaper();
}

sn::IShaper* getShaper()
{
	return new ParticleShaper();
}

template<class STREAM>
STREAM& operator >> (STREAM& out, ParticleShaper::Shape& arg)
{
	out >> arg.surfaceShader >> arg.displaceShader;
	out >> arg.bDynamic;
	out >> arg.renderType;

	out >> arg.bInstancer;
	out >> arg.shapeval;
	out >> arg.attrs;
	out >> arg.attrsdata;

	out >> arg.dirtybox;
	out >> arg.nonDynamicData;
	out >> arg.frames;
	out >> arg.a_frame;
	return out;
}
template<class STREAM>
STREAM& operator >> (STREAM& out, ParticleShaper::Shape::Data& arg)
{
	out >> arg.pos;
	out >> arg.width;
	out >> arg.widthPP;
	return out;
}

template<class STREAM>
STREAM& operator >> (STREAM& out, ParticleShaper::Shape::FrameShot& arg)
{
	out >> arg.prevFrameId;
	out >> arg.nextFrameId;
	out >> arg.data;
	return out;
}

// �������� �����
sn::IShape* ParticleShaper::export(
	artist::IShape& source,
	sn::IPersonage* personage
	)
{
	ParticleShaper::Shape& shape = *(new ParticleShaper::Shape());
	MObject src = source.getSourceObject();

	MStatus stat;
	MFnParticleSystem ps(src, &stat);
	if( !stat) return NULL;
	// �������:
	shape.surfaceShader = mtorShaderNames( source.getThis(), "slimSurf");
	shape.displaceShader = mtorShaderNames( source.getThis(), "slimDispl");

	if( !source.getValue( shape.bDynamic, "Dynamic", sn::UNIFORM_SHAPE, false)) 
		return NULL;

	// ���������
	if( !source.getValue( shape.bInstancer, "Instancer", sn::UNIFORM_SHAPE, false)) 
		return NULL;

	// ����
	if( !source.getValue( shape.shapeval, "i_shape"))
		if( shape.bInstancer)
			return NULL;

	// ������ �������� �� �������� (�� ���������)
	for(unsigned i=0; i<shape.shapeval.attrlist.size(); ++i)
	{
		artist::attr_decl& ad = shape.shapeval.attrlist[i];
		std::vector<char> data;
		if( !source.getValue(data, ad))
			return NULL;
		shape.attrs[ad] = shape.attrsdata.size();
		shape.attrsdata.insert(shape.attrsdata.end(), data.begin(), data.end());

		std::string PPattrRef = ad.name + "Ref";
		std::string PPattr;
		source.getValue(PPattr, PPattrRef.c_str(), sn::UNIFORM_SHAPE);
		if( PPattr.size())
		{
			std::string pp = source.mirrorAttr( src, PPattr.c_str());
			if(pp.empty())
			{
				source.displayString("PP atribute %s not found", PPattr.c_str());
			}
		}
	}
	

	std::string id = source.mirrorAttr( src, "id");
	std::string pos = source.mirrorAttr( src, "position");
	std::string rt = source.mirrorAttr( src, "particleRenderType");
	
	MObject idobj = source.getValue(id.c_str(), sn::VT_FLOATARRAY, sn::UNIFORM_SHAPE);
	if( idobj.isNull()) return false;
	MObject posobj = source.getValue(pos.c_str(), sn::VT_VECTOR3ARRAY, sn::UNIFORM_SHAPE);
	if( idobj.isNull()) return false;
	if( !source.getValue( shape.renderType, rt.c_str(), sn::UNIFORM_SHAPE)) 
		return NULL;

	shape.dirtybox.reset();
	if( !shape.bDynamic)
	{
		// width
		if( !source.getValue( shape.nonDynamicData.width, "width", sn::UNIFORM_SHAPE, 0.1f, 0, 1)) 
			return NULL;
		std::string widthPPname;
		if( !source.getValue( widthPPname, "widthPPname", sn::UNIFORM_SHAPE)) 
			return NULL;
		if(widthPPname.size())
		{
			MPlug pl = ps.findPlug(widthPPname.c_str(), &stat);
			MObject widthPPobj;
			pl.getValue(widthPPobj, MDGContext_fsNormal());
			MFnDoubleArrayData dad(widthPPobj);
			shape.nonDynamicData.widthPP.resize(dad.length());
			for( unsigned i=0; i<dad.length(); i++)
				shape.nonDynamicData.widthPP[i] = (float)dad[i];
		}
		// pos
		MFnVectorArrayData vpos(posobj);
		shape.nonDynamicData.pos.resize(vpos.length());
		for(unsigned i=0; i<vpos.length(); i++)
		{
			copy( shape.nonDynamicData.pos[i], vpos[i]);
			shape.dirtybox.insert(shape.nonDynamicData.pos[i]);
		}
	}
	else
	{
		MAnimControl ac;
		MTime curtime = ac.currentTime();

		MTime minTime = ac.minTime();//, MTime::uiUnit);
		MTime maxTime = ac.maxTime();//, MTime::uiUnit);
		MTime step(1, MTime::uiUnit());

		int frameCount=0;
		for(MTime t = minTime; t<=maxTime; t+=step, frameCount++)
		{
		}
		source.displayString("frameCount=%d", frameCount);

		shape.frames.resize( frameCount);
		std::map<int, int> lastids;		//id->index
		int frame=0;
		for( MTime t=minTime; t<=maxTime; t+=step, frame++)
		{
			Shape::FrameShot& frameShot = shape.frames[frame];

			// time
			ac.setCurrentTime(t);
			ps.evaluateDynamics(t, frame==0);

			MFnDoubleArrayData vid(idobj);
			std::map<int, int> ids;

			// prevFrameId
			frameShot.prevFrameId.resize(vid.length());
			frameShot.nextFrameId.resize(vid.length());
			for(unsigned i=0; i<vid.length(); i++)
			{
				int id = (int)vid[i];
				ids[id] = i;
				std::map<int, int>::iterator it = lastids.find(id);
				if( it==lastids.end())
					frameShot.prevFrameId[i] = -1;
				else
				{
					int lastId = it->second;
					frameShot.prevFrameId[i] = lastId;
					// ������� ����.�����
					Shape::FrameShot& prevShot = shape.frames[frame-1];
					prevShot.nextFrameId[lastId] = i;
				}
			}
			lastids = ids;

			// verts
			MFnVectorArrayData vpos(posobj);
			frameShot.data.pos.resize(vpos.length());
			for(unsigned i=0; i<vpos.length(); i++)
			{
				copy( frameShot.data.pos[i], vpos[i]);
				shape.dirtybox.insert(frameShot.data.pos[i]);
			}
		}
		ac.setCurrentTime(curtime);

		char defval[128];_snprintf(defval, 128, "%d %d %d", 0, 0, frameCount);
		shape.a_frame = source.getAttr("Frame", sn::VT_FLOAT, sn::UNIFORM_SHAPE, defval);
	}
	
	return &shape;
}
// �������� �����
bool ParticleShaper::save(
	sn::IShape* snshape, 
	Util::Stream& stream
	)
{
	Shape* shape = (Shape*)snshape;
	stream >> *shape;
	return true;
}

// box???? �� ������� ����!
bool ParticleShaper::getDirtyBox(
	sn::IShape* snshape, 
	Math::Box3f& box
	)
{
	Shape* shape = (Shape*)snshape;
	box = shape->dirtybox;
	return true;
}

ParticleShaper::Shape::Data* ParticleShaper::getDataForRender(Shape* shape, sn::IPersonage* personage)
{
	if( shape->bDynamic)
	{
		float frame;
		personage->getValue(shape->a_frame, frame);
		int f = (int)frame;
		if( f>=0 && f<(int)shape->frames.size())
		{
			Shape::FrameShot& fs = shape->frames[f];
			return &fs.data;
		}
	}
	else
	{
		return &shape->nonDynamicData;
	}
	return NULL;
}

// ���������
void ParticleShaper::render(
	sn::IShape* snshape, 
	sn::IPersonage* personage,
	IPrman* prman)
{
	Shape* shape = (Shape*)snshape;

	int npoints;
	std::vector<RtToken> names;
	std::vector<RtPointer> values;

	Shape::Data* data = getDataForRender(shape, personage);
	if(!data) return;
	if(!data->pos.size()) return;

	if( !shape->bInstancer)
	{
		npoints = data->pos.size();
		names.push_back("P");
		values.push_back(&data->pos[0]);

		if( data->widthPP.size() == npoints)
		{
			names.push_back("width");
			values.push_back(&data->widthPP[0]);
		}
		else
		{
			names.push_back("constantwidth");
			values.push_back(&data->width);
		}

		prman->RiAttributeBegin();
		if( !shape->surfaceShader.empty())
			prman->RiSurface((char*)shape->surfaceShader.c_str(), NULL);
		if( !shape->displaceShader.empty())
			prman->RiDisplacement((char*)shape->displaceShader.c_str(), NULL);

		prman->RiPointsV( npoints, names.size(), &names[0], &values[0]);
		prman->RiAttributeEnd();
	}
	else
	{
		sn::IShaper* shaper = shape->shapeval.ssp->shaper;
		sn::IShape* pshape  = shape->shapeval.ssp->shape;

		for(unsigned i=0; i<data->pos.size(); i++)
		{
			prman->RiAttributeBegin();

			Math::Matrix4f m = Math::Matrix4f::id;
			m[3] = Math::Vec4f( data->pos[i], 1.f);

			prman->RiConcatTransform(m);

			shaper->render(pshape, shape, prman);

			prman->RiAttributeEnd();
		}
	}
}

// ���������
void ParticleShaper::renderGL(
	sn::IShape* snshape, 
	sn::IPersonage* personage
	)
{
	Shape* shape = (Shape*)snshape;

	Shape::Data* data = getDataForRender(shape, personage);
	if(!data) return;
	if(!data->pos.size()) return;

	if( !shape->bInstancer)
	{
		glPushAttrib(GL_ENABLE_BIT);
		glPointSize( 4.0 );

		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer( 3, GL_FLOAT, 0, (float*)&data->pos[0]);
		glDrawArrays(GL_POINTS, 0, data->pos.size());

		glPopAttrib();
	}
	else
	{
		sn::IShaper* shaper = shape->shapeval.ssp->shaper;
		sn::IShape* pshape  = shape->shapeval.ssp->shape;

		for(unsigned i=0; i<data->pos.size(); i++)
		{
			glPushMatrix();

			Math::Matrix4f m = Math::Matrix4f::id;
			m[3] = Math::Vec4f( data->pos[i], 1.f);

			float* d = m.data();
			glMultMatrixf(d);

			shaper->renderGL(pshape, shape);

			glPopMatrix();
		}
	}
}

// �������� �����
sn::IShape* ParticleShaper::load(
	Util::Stream& stream,
	sn::IPersonage* personage)
{
	Shape* shape = new Shape();
	stream >> *shape;
	return shape;
}



void ParticleShaper::Shape::getValue(sn::attr_t& attr, int& val)
{
	val = 0;
	std::map<artist::attr_decl, int>::iterator it = attrs.find( artist::attr_decl(attr.name.c_str(), attr.type));
	if( it==attrs.end()) return;
	int offs = it->second;
	val = *(int*)((char*)&attrsdata[0]+offs);
}
void ParticleShaper::Shape::getValue(sn::attr_t& attr, float& val)
{
	val = 0;
	std::map<artist::attr_decl, int>::iterator it = attrs.find( artist::attr_decl(attr.name.c_str(), attr.type));
	if( it==attrs.end()) return;
	int offs = it->second;
	val = *(float*)((char*)&attrsdata[0]+offs);
}
void ParticleShaper::Shape::getValue(sn::attr_t& attr, Math::Vec3f& val)
{
	val = Math::Vec3f(0, 0, 0);
	std::map<artist::attr_decl, int>::iterator it = attrs.find( artist::attr_decl(attr.name.c_str(), attr.type));
	if( it==attrs.end()) return;
	int offs = it->second;
	val = *(Math::Vec3f*)((char*)&attrsdata[0]+offs);
}
void ParticleShaper::Shape::getValue(sn::attr_t& attr, Math::Matrix4f& val)
{
	val = Math::Matrix4f::id;
	std::map<artist::attr_decl, int>::iterator it = attrs.find( artist::attr_decl(attr.name.c_str(), attr.type));
	if( it==attrs.end()) return;
	val = *(Math::Matrix4f*)((char*)&attrsdata[0]+it->second);
}
