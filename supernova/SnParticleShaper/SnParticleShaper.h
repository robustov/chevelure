#pragma once
#include "ISnShaper.h"
#include "Util\STLStream.h"

struct ParticleShaper : public sn::IShaper
{
	virtual void release(){delete this;}

	// �������� �����
	virtual sn::IShape* export(
		artist::IShape& source,
		sn::IPersonage* personage
		);
	// �������� �����
	virtual bool save(
		sn::IShape* snshape, 
		Util::Stream& stream
		);
	// �������� �����
	virtual sn::IShape* load(
		Util::Stream& stream,
		sn::IPersonage* personage);

	// ���������
	virtual void render(
		sn::IShape* shape, 
		sn::IPersonage* personage,
		IPrman* prman
		);
	// ���������
	virtual void renderGL(
		sn::IShape* snshape, 
		sn::IPersonage* personage
		);
	// box???? �� ������� ����!
	virtual bool getDirtyBox(
		sn::IShape* snshape, 
		Math::Box3f& box
		);


	struct Shape : public sn::IShape, public sn::IPersonage
	{
		// �������:
		std::string surfaceShader, displaceShader;

		// ������� ��������
		bool bDynamic;
		// ��� ���������
		int renderType;
		// ���������
		bool bInstancer;
		// ����
		artist::ShapeValue shapeval;
		// ������ �������� �� �������� (�� ���������)
		std::map<artist::attr_decl, int> attrs;
		// ������ ��������� (�� ���������)
		std::vector<char> attrsdata;


		// dirty box
		Math::Box3f dirtybox;

		struct Data
		{
			// �������
			std::vector<Math::Vec3f> pos;
			// ������
			float width;
			std::vector<float> widthPP;
		};
		Data nonDynamicData;

		// ���������:
		struct FrameShot
		{
			std::vector<int> prevFrameId;	// ������� ������ �� ����.�����
			std::vector<int> nextFrameId;	// ������� ������ �� ����.�����

			Data data;
		};
		std::vector<FrameShot> frames;
		sn::attr_t a_frame;

		virtual void release(){delete this;};
	public:
		void getValue(sn::attr_t& attr, int& val);
		void getValue(sn::attr_t& attr, float& val);
		void getValue(sn::attr_t& attr, Math::Vec3f& val);
		void getValue(sn::attr_t& attr, Math::Matrix4f& val);
	};

protected:
	Shape::Data* getDataForRender(Shape* shape, sn::IPersonage* personage);
};

