#pragma once
#include "ISnShaper.h"
#include "Util\STLStream.h"

struct HairShaper : public sn::IShaper
{
//	static int currentversion;

	virtual void release(){delete this;}

	// ������
//	virtual int getVersion(){return currentversion;};

	// filter
	virtual void filter(
		sn::IShape* snshape, 
		sn::IPersonage* personage, 
		std::vector<Math::Vec3f>& verts,
		std::vector<Math::Vec3f>* norms=NULL);

	// �������� �����
	virtual bool export(
		Util::Stream& stream,
		artist::IShape& source
		);
	// �������� �����
	virtual sn::IShape* load(
		Util::Stream& stream,
		sn::IPersonage* personage);
	// ���������
	virtual void render(
		sn::IShape* shape, 
		sn::IPersonage* personage, 
		sn::IRenderContext* context);
	// ���������
	virtual void renderGL(
		sn::IShape* snshape, 
		sn::IPersonage* personage
		);

	inline int getAffectIndexForVertex(Math::Vec3f& src, int tolerance, float maxDot, float& factor)
	{
		Math::Vec3f direction(0, 1, 0);
		float dot = Math::dot(src, direction);
		dot = dot/maxDot;
		if(dot<0) dot = 0;

		int index = (int)(dot*(tolerance-1));
		if(index>=tolerance-1) index=tolerance-2;
		factor = dot*(tolerance-1) - index;
		if(factor>1) factor=1;
		return index;
	}

	struct Shape : public sn::IShape
	{
		int tolerance;
//		sn::attr_t a_point, a_direction, a_tangent;
		sn::attr_t a_polar, a_inclination, a_baseCurl, a_tipCurl, a_length;

		float maxDot;
		int facevertcount;
		std::vector<Math::Polygon32> polygons;
		std::vector<Math::Vec3f> verts;
		std::vector<Math::Vec3f> norms;
		std::vector<int> norms_vertindecies;
		std::vector<float> u;
		std::vector<float> v;

		virtual void release(){delete this;};
	};
};

template<class STREAM>
STREAM& operator >> (STREAM& out, HairShaper::Shape& arg)
{
	out >> arg.tolerance;
	out >> arg.maxDot;

//	out >> arg.a_point;
//	out >> arg.a_direction;
//	out >> arg.a_tangent;

	out >> arg.a_polar;
	out >> arg.a_inclination;
	out >> arg.a_baseCurl;
	out >> arg.a_tipCurl;
	out >> arg.a_length;

	out >> arg.facevertcount;
	out >> arg.polygons;
	out >> arg.verts;
	out >> arg.norms;
	out >> arg.norms_vertindecies;
	out >> arg.u;
	out >> arg.v;
	return out;
}
