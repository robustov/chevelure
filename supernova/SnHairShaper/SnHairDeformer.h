#pragma once
#include "supernova/ISnDeformer.h"
#include "Util\STLStream.h"

struct SnHairDeformer : public sn::IDeformer
{
	virtual void release(){delete this;}
	// �������� ������
	virtual sn::IShape* export_(
		artist::IShape& source,
		sn::IDeformedShape& deformedShape,
		Math::Box3f& dirtyBox
		);
	// �������� �����
	virtual bool save(
		sn::IShape* snshape, 
		Util::Stream& stream
		);
	// �������� ������
	virtual sn::IShape* load(
		Util::Stream& stream,
		sn::IPersonage* personage
		);
	// ����������
	virtual void filter(
		sn::IShape* snshape, 
		sn::IPersonage* personage, 
		sn::IDeformedShape& deformedShape);
	// �������������� ��������� �������
	virtual void getShaderParameters(
		const char* shadertype,
		sn::IShape* snshape, 
		sn::IPersonage* personage,
		sn::tag_TokenParameters& params		// ��������� ������� :(
		);

	struct Shape : public sn::IShape
	{
		int tolerance;
		sn::attr_t a_transforms, a_parameters;
		sn::attr_t a_polar, a_inclination, a_baseCurl, a_tipCurl;//, a_length;
		sn::attr_t a_twist, a_baseWidth, a_tipWidth; 
		sn::attr_t a_u, a_v;
		sn::attr_t a_verts, a_norms;
		sn::attr_t a_id;
		float maxDot;

		virtual void release(){delete this;};
	};


	inline int getAffectIndexForVertex(Math::Vec3f& src, std::vector<float>& parameters, float maxDot, float& factor)
	{
		Math::Vec3f direction(0, 1, 0);
		float dot = Math::dot(src, direction);
		dot = dot/maxDot;
		if(dot<0) dot = 0;

		unsigned x;
		for(x=0; x<parameters.size(); x++)
		{
			if(dot<parameters[x]) break;
		}
		if(x==parameters.size())
		{
			factor = 1;
			return parameters.size()-2;
		}
		if(x==0)
		{
			factor = 0;
			return 0;
		}
		factor = (dot-parameters[x-1])/(parameters[x]-parameters[x-1]);
		return x-1;


		/*/
		int index = (int)(dot*(tolerance-1));
		if(index>=tolerance-1) index=tolerance-2;
		factor = dot*(tolerance-1) - index;
		if(factor>1) factor=1;
		return index;
		/*/
	}
};

