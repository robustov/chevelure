#include "stdafx.h"
#include "SnHairShaper.h"
#include "SnDelayMaya.h"

#include <maya\MObject.h> 
#include <maya\MFnDependencyNode.h> 
#include <maya\MFnSkinCluster.h>
#include <maya\MItDependencyGraph.h>
#include <maya\MFnMatrixData.h>
#include <maya\MMatrix.h>
#include <maya\MDagPathArray.h>
#include <maya\MItGeometry.h>
#include <maya\MDagPath.h>
#include <maya\MFnTransform.h>

#include "SnDelayMaya.h"
#include "IArtistShape.h"
#include <maya\MFnMesh.h>
#include <maya\MItMeshPolygon.h>
#include <ri.h>
#include <gl/gl.h>
#include "mathNmaya/mathNmaya.h"
#include "Math/Rotation3.h"

//int HairShaper::currentversion;

extern "C" 
{
	__declspec(dllexport) sn::IShaper* getShaper();
}

sn::IShaper* getShaper()
{
	return new HairShaper();
}

struct CurveVertex
{
	Math::Matrix4f bind;
	Math::Matrix4f transform;

	Math::Vec3f normal;
	Math::Vec3f tangentU;
	Math::Vec3f tangentV;
	Math::Vec3f position;
	float param;
};

// filter
void HairShaper::filter(
	sn::IShape* snshape, 
	sn::IPersonage* personage, 
	std::vector<Math::Vec3f>& verts,
	std::vector<Math::Vec3f>* norms
	)
{
	Shape* shape = (Shape*)snshape;

	Math::Vec3f point(0, 0, 0);
	Math::Vec3f normal(0, 1, 0);
	Math::Vec3f tangentU(1, 0, 0);
	Math::Vec3f tangentV(0, 0, 1);
//	personage->getValue(shape->a_point, point);
//	personage->getValue(shape->a_direction, direction);
//	personage->getValue(shape->a_tangent, tangent);

	int tolerance = shape->tolerance;
	float polar, inclination, baseCurl, tipCurl, length;
	personage->getValue(shape->a_polar, polar);
	personage->getValue(shape->a_inclination, inclination);
	personage->getValue(shape->a_baseCurl, baseCurl);
	personage->getValue(shape->a_tipCurl, tipCurl);
	personage->getValue(shape->a_length, length);

	double theta = M_PI * (2 * polar - 1);
	Math::Rot3f PolarRot( (float)theta, normal );
	Math::Matrix4f obj = Math::Matrix4f::id;
	PolarRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());
	tangentU = obj*tangentU;
	normal   = obj*normal;
	tangentV = obj*tangentV;

//	normal.x = (1 - inclination) * normal.x + inclination * tangentU.x;
//	normal.y = (1 - inclination) * normal.y + inclination * tangentU.y;
//	normal.z = (1 - inclination) * normal.z + inclination * tangentU.z;
//	normal.normalize();
	double dzeta = M_PI * inclination * 0.5;
	Math::Rot3f IncRot( (float)dzeta, tangentV );
	IncRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());
	tangentU = obj*tangentU;
	normal   = obj*normal;
	tangentV = obj*tangentV;
	
	tangentU = tangentU*length;
	normal   = normal*length;
	tangentV = tangentV*length;
	
	std::vector<Math::Matrix4f> cv(tolerance);
	float maxDot = shape->maxDot;
	float segLength = maxDot/((float)tolerance-1);
	for (int _k = 0;_k < tolerance; _k++)
	{
		float param = (float)_k/((float)tolerance-1);

		Math::Matrix4f transform;
		transform[0] = Math::Vec4f(tangentU, 0);
		transform[1] = Math::Vec4f(normal, 0);
		transform[2] = Math::Vec4f(tangentV, 0);
		transform[3] = Math::Vec4f(point, 1);
		cv[_k] = transform;

		double curl = (1-param)*baseCurl + param*tipCurl;
		curl = (2*curl-1)/((float)tolerance-1);
		Math::Rot3f PolarRot( (float)M_PI*curl, tangentV );
		Math::Matrix4f obj = Math::Matrix4f::id;
		PolarRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());
		
		tangentU = obj*tangentU;
		normal   = obj*normal;
		tangentV = obj*tangentV;

		point += normal*segLength;
	}

	for(unsigned v=0; v<verts.size(); v++)
	{
		Math::Vec3f& src = verts[v];

		float factor;
		int index = getAffectIndexForVertex(src, tolerance, maxDot, factor);

		Math::Vec3f direction(0, 1, 0);
		Math::Vec3f src0 = src - direction*maxDot*index/(float)(tolerance-1);
		Math::Vec3f src1 = src - direction*maxDot*(index+1)/(float)(tolerance-1);
		Math::Matrix4f& cv0 = cv[index];
		Math::Matrix4f& cv1 = cv[index+1];

		Math::Vec3f pt0 = cv0*src0;
		Math::Vec3f pt1 = cv1*src1;
		Math::Vec3f dst = (1-factor)*pt0 + factor*pt1;
		verts[v] = dst;

	}
	if( norms)
	{
		for(unsigned fv=0; fv<(*norms).size(); fv++)
		{
			int v = shape->norms_vertindecies[fv];
			Math::Vec3f& src = verts[v];
			float factor;
			int index = getAffectIndexForVertex(src, tolerance, maxDot, factor);
			Math::Matrix4f& cv0 = cv[index];
			Math::Matrix4f& cv1 = cv[index+1];

			Math::Vec3f srcN = (*norms)[fv];
			Math::Vec3f n0 = Math::Matrix4As3f(cv0)*srcN;
			Math::Vec3f n1 = Math::Matrix4As3f(cv1)*srcN;
			Math::Vec3f dstN = (1-factor)*n0 + factor*n1;
			(*norms)[fv] = dstN.normalized();
		}
	}
}

// ���������
void HairShaper::render(
	sn::IShape* snshape, 
	sn::IPersonage* personage, 
	sn::IRenderContext* context)
{
	Shape* shape = (Shape*)snshape;

	std::vector<Math::Vec3f> shape_verts = shape->verts;
	std::vector<Math::Vec3f> shape_norms = shape->norms;
	filter(snshape, personage, shape_verts, &shape_norms);

	int npolygons = (int)shape->polygons.size();
	int facevertcount = shape->facevertcount;

	RtInt* loops = new RtInt[npolygons];
	RtInt* nverts = new RtInt[npolygons];
	RtInt* verts = new RtInt[facevertcount];
	
	int vertscount=0;
	for(int i=0; i<npolygons; i++)
	{
		loops[i] = 1;
		nverts[i] = shape->polygons[i].size();

		for(int v=0; v<nverts[i]; v++, vertscount++)
		{
			verts[vertscount] = shape->polygons[i][v];
		}
	}

	RtToken names[] = {
		"P",
		"facevarying normal N",
		"facevarying float s", 
		"facevarying float t", 
	};
	RtPointer values[] = 
	{
		(float*)&shape_verts[0],
		(float*)&shape_norms[0],
		(float*)&shape->u[0],
		(float*)&shape->v[0],
	};

	RiPointsGeneralPolygonsV( npolygons, loops, nverts, verts, sizeof(names)/sizeof(RtToken), names, values );
}

// �������� �����
bool HairShaper::export(
	Util::Stream& stream,
	artist::IShape& source
	)
{
	MObject meshobj = source.getValue("inMesh", sn::VT_GEOMETRY, sn::UNIFORM_SHAPE);
	if( meshobj.isNull())
		return false;

	MFnMesh fnMesh(meshobj);

	int numPoints = fnMesh.numVertices();
	int numNormals = fnMesh.numNormals();
	int numSTs = fnMesh.numUVs();
	int numFaces = fnMesh.numPolygons();
	int numFaceVertices = fnMesh.numFaceVertices();

	MFloatVectorArray normals;
	fnMesh.getNormals( normals );

	HairShaper::Shape shape;
	shape.facevertcount = 0;
	shape.polygons.resize(numFaces);
	shape.verts.resize(numPoints);
	shape.norms.resize(numFaceVertices);
	shape.norms_vertindecies.resize(numFaceVertices);
	if(numSTs)
	{
		shape.u.resize(numFaceVertices);
		shape.v.resize(numFaceVertices);
	}

	MItMeshPolygon polyIt( meshobj);
	for( int face=0, faceVertex=0; polyIt.isDone() == false; polyIt.next(), face++ ) 
	{
		int count = polyIt.polygonVertexCount();
		Math::Polygon32& poly = shape.polygons[face];
		poly.resize(count);
		shape.facevertcount += count;

		for( int v=0; v<count; v++, faceVertex++) 
		{
			int vertex = polyIt.vertexIndex( v );
			poly[v] = vertex;

			MPoint point = polyIt.point( v, MSpace::kObject );
			Math::Vec3f& p = shape.verts[vertex];
			p.x = (float)point.x;
			p.y = (float)point.y;
			p.z = (float)point.z;

			int normal = polyIt.normalIndex( v );
			MVector norm = normals[normal];
			Math::Vec3f& n = shape.norms[faceVertex];
			n.x = (float)norm.x;
			n.y = (float)norm.y;
			n.z = (float)norm.z;
			shape.norms_vertindecies[faceVertex] = vertex;

			if(numSTs)
			{
				float S, T;
				fnMesh.getPolygonUV( face, v, S, T );
				shape.u[faceVertex] = S;
				shape.v[faceVertex] = 1-T;
			}
		}
	}
	shape.maxDot = 0;
	Math::Vec3f direction(0, 1, 0);
	for( unsigned i=0; i<shape.verts.size(); ++i)
	{
		float dot = Math::dot(shape.verts[i], direction);
		shape.maxDot = __max(shape.maxDot, dot);
	}
	// ��������!
	shape.tolerance = 10;
	shape.a_polar.name = "polar";
	shape.a_inclination.name = "inclination";
	shape.a_baseCurl.name = "baseCurl";
	shape.a_tipCurl.name = "tipCurl";
	shape.a_length.name = "length";
	stream >> shape;
	return true;
}
// ���������
void HairShaper::renderGL(
	sn::IShape* snshape, 
	sn::IPersonage* personage)
{
	Shape* shape = (Shape*)snshape;

	std::vector<Math::Vec3f> verts = shape->verts;
	std::vector<Math::Vec3f> norms = shape->norms;
	filter(snshape, personage, verts, &norms);
	
	glPushAttrib(GL_POLYGON_BIT|GL_ENABLE_BIT);
	glPolygonMode(GL_FRONT, GL_LINE);
	glPolygonMode(GL_BACK, GL_LINE);
	glDisable(GL_CULL_FACE);
	for(unsigned p=0; p<shape->polygons.size(); p++)
	{
		Math::Polygon32& poly = shape->polygons[p];
		glBegin(GL_POLYGON);
		for(int v=0; v<poly.size(); v++)
		{
			int vi = poly[v];
			Math::Vec3f& vert = verts[vi];
			glVertex3f( vert.x, vert.y, vert.z);
		}
		glEnd();
	}
	glPopAttrib();
}

// �������� �����
sn::IShape* HairShaper::load(
	Util::Stream& stream,
	sn::IPersonage* personage)
{
	Shape* shape = new Shape();
	stream >> *shape;
	return shape;
}
