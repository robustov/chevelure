#include "stdafx.h"
#include "SnHairDeformer.h"
#include "Math/Rotation3.h"

#include <maya\MObject.h> 
#include <maya\MFnDependencyNode.h> 
#include <maya\MFnSkinCluster.h>
#include <maya\MItDependencyGraph.h>
#include <maya\MFnMatrixData.h>
#include <maya\MMatrix.h>
#include <maya\MDagPathArray.h>
#include <maya\MItGeometry.h>
#include <maya\MDagPath.h>
#include <maya\MFnTransform.h>
#include <maya\MFnNumericData.h>

#include "supernova/SnDelayMaya.h"
#include "supernova/IArtistShape.h"
#include <maya\MFnMesh.h>
#include <maya\MItMeshPolygon.h>
#include <ri.h>
#include <gl/gl.h>
#include "mathNmaya/mathNmaya.h"

extern "C" 
{
	__declspec(dllexport) sn::IDeformer* getDeformer();
}

sn::IDeformer* getDeformer()
{
	return new SnHairDeformer();
}

template<class STREAM>
STREAM& operator >> (STREAM& out, SnHairDeformer::Shape& arg)
{
	out >> arg.tolerance;
	out >> arg.maxDot;

	out >> arg.a_transforms >> arg.a_parameters;
	out >> arg.a_polar;
	out >> arg.a_twist;
	out >> arg.a_inclination;
	out >> arg.a_baseCurl;
	out >> arg.a_tipCurl;
//	out >> arg.a_length;
	out >> arg.a_baseWidth >> arg.a_tipWidth;
	out >> arg.a_u >> arg.a_v;

	out >> arg.a_verts >> arg.a_norms;
	out >> arg.a_id;
	return out;
}

// �������� ������
sn::IShape* SnHairDeformer::export_(
	artist::IShape& source,
	sn::IDeformedShape& deformedShape, 
	Math::Box3f& dirtyBox
	)
{
	SnHairDeformer::Shape& shape = *(new SnHairDeformer::Shape);
	shape.a_verts = deformedShape.getAttr("P", sn::VT_VECTOR3);
	shape.a_norms = deformedShape.getAttr("N", sn::VT_VECTOR3);

	Math::Vec3f* verts = (Math::Vec3f*)deformedShape.getStream(shape.a_verts);
	shape.maxDot = 0;
	Math::Vec3f direction(0, 1, 0);
	int i;
	for( i=0; i<deformedShape.vertsCount(); ++i)
	{
		float dot = Math::dot(verts[i], direction);
		shape.maxDot = __max(shape.maxDot, dot);
		shape.maxDot = __max(shape.maxDot, -dot);
	}
	float factor = 1/shape.maxDot;
	float m = 0;
	for( i=0; i<deformedShape.vertsCount(); ++i)
	{
		Math::Vec3f v = verts[i] = verts[i]*factor;
		m = __max( __max( __max( fabs(v.x), fabs(v.y)), fabs(v.z)), m);
	}
	dirtyBox.set( 
		Math::Vec3f(-m, -m, -m), 
		Math::Vec3f(m, m, m));
	shape.maxDot = 1;

	// ��������!
	source.getValue(shape.tolerance, "tolerance", sn::UNIFORM_SHAPE, 10, 2, 20);

	shape.a_transforms	= source.getAttr( "transforms", sn::VT_MATRIX4X4ARRAY, sn::UNIFORM_SHAPE, "0 0 0");
	shape.a_parameters  = source.getAttr( "parameters", sn::VT_FLOATARRAY, sn::UNIFORM_SHAPE, "0 0 0");

	shape.a_polar		= source.getAttr( "polar", sn::VT_FLOAT, sn::UNIFORM_SHAPE, "0 0 1");
	shape.a_twist		= source.getAttr( "twist", sn::VT_FLOAT, sn::UNIFORM_SHAPE, "0 0 2");
	shape.a_inclination = source.getAttr( "inclination", sn::VT_FLOAT, sn::UNIFORM_SHAPE, "0 0 1");
	shape.a_baseCurl	= source.getAttr( "baseCurl", sn::VT_FLOAT, sn::UNIFORM_SHAPE, "0.5 0 1");
	shape.a_tipCurl		= source.getAttr( "tipCurl", sn::VT_FLOAT, sn::UNIFORM_SHAPE, "0.5 0 1");
//	shape.a_length		= source.getAttr( "length", sn::VT_FLOAT, sn::UNIFORM_SHAPE, "1 0 5");
	shape.a_baseWidth	= source.getAttr( "baseWidth", sn::VT_FLOAT, sn::UNIFORM_SHAPE, "1 0 1");
	shape.a_tipWidth	= source.getAttr( "tipWidth", sn::VT_FLOAT, sn::UNIFORM_SHAPE, "1 0 1");
	shape.a_u			= source.getAttr( "U", sn::VT_FLOAT, sn::UNIFORM_SHAPE, "0 0 1");
	shape.a_v			= source.getAttr( "V", sn::VT_FLOAT, sn::UNIFORM_SHAPE, "0 0 1");
	shape.a_id			= source.getAttr( "id", sn::VT_FLOAT, sn::UNIFORM_SHAPE, "0 0 1");
	return &shape;
}

// �������� �����
bool SnHairDeformer::save(
	sn::IShape* snshape, 
	Util::Stream& stream
	)
{
	Shape* shape = (Shape*)snshape;
	stream >> *shape;
	return true;
}

// �������� �����
sn::IShape* SnHairDeformer::load(
	Util::Stream& stream,
	sn::IPersonage* personage)
{
	Shape* shape = new Shape();
	stream >> *shape;
	return shape;
}


// filter
void SnHairDeformer::filter(
	sn::IShape* snshape, 
	sn::IPersonage* personage,
	sn::IDeformedShape& deformedShape
	)
{
//	printf("SnHairDeformer::filter\n");
//	fflush(stdout);

	Shape* shape = (Shape*)snshape;

	Math::Vec3f point(0, 0, 0);
	Math::Vec3f normal(0, 1, 0);
	Math::Vec3f tangentU(1, 0, 0);
	Math::Vec3f tangentV(0, 0, 1);

	int tolerance = shape->tolerance;
	float polar, inclination, baseCurl, tipCurl, twist;
	float baseWidth, tipWidth;
	float u, v;
	personage->getValue(shape->a_polar, polar);
	personage->getValue(shape->a_inclination, inclination);
	personage->getValue(shape->a_baseCurl, baseCurl);
	personage->getValue(shape->a_tipCurl, tipCurl);
//	personage->getValue(shape->a_length, length);
	personage->getValue(shape->a_twist, twist);
	personage->getValue(shape->a_baseWidth, baseWidth);
	personage->getValue(shape->a_tipWidth, tipWidth);
	personage->getValue(shape->a_u, u);
	personage->getValue(shape->a_v, v);
	twist = (twist-0.5f)*2;

	std::vector<Math::Matrix4f> cv;
	std::vector<float> parameters;
	personage->getValue(shape->a_transforms, cv);
	personage->getValue(shape->a_parameters, parameters);
	float maxDot = shape->maxDot;

	if( !cv.empty())
	{
		tolerance = cv.size();
	}
	else
	{
		double theta = M_PI * (2 * polar - 1);
		Math::Rot3f PolarRot( (float)theta, normal );
		Math::Matrix4f obj = Math::Matrix4f::id;
		PolarRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());
		tangentU = obj*tangentU;
		normal   = obj*normal;
		tangentV = obj*tangentV;

		// ?????
		// orto tangentV tangentU
		{
			tangentV = Math::cross( tangentU, normal);
			tangentV.normalize();
		}

		double dzeta = M_PI * inclination * 0.5;
		Math::Rot3f IncRot( (float)dzeta, tangentV );
		IncRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());
		tangentU = obj*tangentU;
		normal   = obj*normal;
		tangentV = obj*tangentV;
		
	//	tangentU = tangentU*length;
	//	normal   = normal*length;
	//	tangentV = tangentV*length;
		
//		std::vector<Math::Matrix4f> cv(tolerance);
		cv.resize(tolerance);
		parameters.resize(tolerance);
		float segLength = maxDot/((float)tolerance-1);
		for (int _k = 0;_k < tolerance; _k++)
		{
			float param = (float)_k/((float)tolerance-1);

			float w = (1-param)*baseWidth + param*tipWidth;

			Math::Matrix4f transform;
			transform[0] = Math::Vec4f(tangentU, 0)*w;
			transform[1] = Math::Vec4f(normal, 0);
			transform[2] = Math::Vec4f(tangentV, 0)*w;
			transform[3] = Math::Vec4f(point, 1);
			cv[_k] = transform;
			parameters[_k] = param;

			// curl
			float curl = (1-param)*baseCurl + param*tipCurl;
			curl = (2*curl-1)/((float)tolerance-1);
			Math::Rot3f PolarRot( (float)(M_PI*curl), tangentV );
			Math::Matrix4f obj = Math::Matrix4f::id;
			PolarRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());
			tangentU = obj*tangentU;
			normal   = obj*normal;
			tangentV = obj*tangentV;

			// twist
			float twistAngle = twist/((float)tolerance-1);
			Math::Rot3f TwistRot( (float)(M_PI*twistAngle), Math::Vec3f(0, 1, 0) );
			TwistRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());
			tangentU = obj*tangentU;
			normal   = obj*normal;
			tangentV = obj*tangentV;

			point += normal*segLength;
		}
	}

	// ����������!
	Math::Vec3f* verts = (Math::Vec3f*)deformedShape.getStream(shape->a_verts);
	Math::Vec3f* norms = (Math::Vec3f*)deformedShape.getStream(shape->a_norms);

	if( !verts) return;
	if( norms && shape->a_norms.modifer==sn::PER_FACEVERTEX)
	{
		for(int fv=0; fv<deformedShape.facevertsCount(); fv++)
		{
			int v = deformedShape.getVertexForFacevertex(fv);
			Math::Vec3f& src = verts[v];
			float factor;
			int index = getAffectIndexForVertex(src, parameters, maxDot, factor);
			Math::Matrix4f& cv0 = cv[index];
			Math::Matrix4f& cv1 = cv[index+1];

			Math::Vec3f srcN = norms[fv];
			Math::Vec3f n0 = Math::Matrix4As3f(cv0)*srcN;
			Math::Vec3f n1 = Math::Matrix4As3f(cv1)*srcN;
			Math::Vec3f dstN = (1-factor)*n0 + factor*n1;
			norms[fv] = dstN.normalized();
		}
	}
	for(int v=0; v<deformedShape.vertsCount(); v++)
	{
		Math::Vec3f& src = verts[v];

		float factor;
		int index = getAffectIndexForVertex(src, parameters, maxDot, factor);

		Math::Vec3f direction(0, 1, 0);
		Math::Vec3f src0 = src - direction*maxDot*index/(float)(tolerance-1);
		Math::Vec3f src1 = src - direction*maxDot*(index+1)/(float)(tolerance-1);
		Math::Matrix4f& cv0 = cv[index];
		Math::Matrix4f& cv1 = cv[index+1];

		Math::Vec3f pt0 = cv0*src0;
		Math::Vec3f pt1 = cv1*src1;
		Math::Vec3f dst = (1-factor)*pt0 + factor*pt1;
		verts[v] = dst;

		if(norms && shape->a_norms.modifer==sn::PER_VERTEX)
		{
			Math::Vec3f srcN = norms[v];
			Math::Vec3f n0 = Math::Matrix4As3f(cv0)*srcN;
			Math::Vec3f n1 = Math::Matrix4As3f(cv1)*srcN;
			Math::Vec3f dstN = (1-factor)*n0 + factor*n1;
			norms[v] = dstN.normalized();
		}
	}
}

// �������������� ��������� �������
void SnHairDeformer::getShaderParameters(
	const char* shadretype,
	sn::IShape* snshape, 
	sn::IPersonage* personage,
	sn::tag_TokenParameters& params		// ��������� ������� :(
	)
{
	Shape* shape = (Shape*)snshape;
	float u, v, id;
	personage->getValue(shape->a_u, u);
	personage->getValue(shape->a_v, v);
	personage->getValue(shape->a_id, id);

	{
		std::vector<char>& vec = params["constant float SurfaceU"];
		vec.resize(sizeof(float));
		*(float*)(&vec[0]) = u;
	}
	{
		std::vector<char>& vec = params["constant float SurfaceV"];
		vec.resize(sizeof(float));
		*(float*)(&vec[0]) = v;
	}

	{
		std::vector<char>& vec = params["constant float u_surface"];
		vec.resize(sizeof(float));
		*(float*)(&vec[0]) = u;
	}
	{
		std::vector<char>& vec = params["constant float v_surface"];
		vec.resize(sizeof(float));
		*(float*)(&vec[0]) = 1-v;
	}
	{
		std::vector<char>& vec = params["constant float hairIds"];
		vec.resize(sizeof(float));
		*(float*)(&vec[0]) = id;
	}
	{
		std::vector<char>& vec = params["constant float hairId"];
		vec.resize(sizeof(float));
		*(float*)(&vec[0]) = id;
	}
};

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		{
#ifdef HAIR_TRACELOG
			char filename[512]; GetModuleFileName( (HMODULE)hModule, filename, 512); fprintf(stderr, "%s\n", filename);
#endif
			break;
		}
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
    return TRUE;
}
