// SnTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>
#include <ri.h>
#include "Util/DllProcedure.h"
#include "ISnShaper.h"
#include "Util/FileStream.h"
#include "SnDSO.h"

#include "mathNpixar\IPrman_impl.h"

int main(int argc, char* argv[])
{
//	HMODULE h = LoadLibrary("SnMath.dll");
//	void* p = GetProcAddress(h, "getShaper");

	printf("USAGE SnTest.exe snfilename [ribfilename]\n");
	if( argc<2)
		return -1;

	const char* filename = argv[1];
	std::string ribfilename = filename;
	ribfilename += ".rib";
	if( argc>=3)
		ribfilename = argv[2];

	SnDSO dso(filename);

	char* f = "dd.rib";
	RiBegin( (char*)ribfilename.c_str() );
	// format
	{
//		RtString format = "ascii";
//		RiOption(( RtToken ) "rib", ( RtToken ) "format", &format, RI_NULL);
//		RtString compression = "none";
//		RiOption(( RtToken ) "rib", ( RtToken ) "compression", &compression, RI_NULL);
	}

	Prman_Impl prman;

	dso.GenRIB(&prman);
	RiEnd( );

	return 0;
}

