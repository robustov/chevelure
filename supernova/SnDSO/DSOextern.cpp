#include "stdafx.h"

#include "SnDSO.h"
#include <ri.h>

#include "mathNpixar\IPrman_impl.h"


/**************************************************/
#ifdef _WIN32
#define export __declspec(dllexport)
#else
#define export
#endif

extern "C" {
    export RtPointer ConvertParameters(RtString paramstr);
    export RtVoid Subdivide(RtPointer data, RtFloat detail);
    export RtVoid Free(RtPointer data);
}

RtPointer ConvertParameters(RtString paramstr) 
{
	SnDSO* furData = new SnDSO(paramstr);
fprintf(stderr, "Fur: Load completed\n");

	return static_cast<RtPointer>(furData);
}

RtVoid Subdivide(RtPointer data, RtFloat detail) 
{
	if( !data) return;

fprintf(stderr, "Subdivide\n");
	SnDSO* furData = static_cast<SnDSO*>(data);

	Prman_Impl prman;
	furData->GenRIB(&prman);
}

RtVoid Free(RtPointer data) 
{
	if( !data) return;
fprintf(stderr, "Free\n");
	SnDSO* furData = static_cast<SnDSO*>(data);
	delete furData;
fprintf(stderr, "Free complete\n");
}