// dl_FurProcedural.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"

#include "SnDSO.h"
#include <ri.h>
#include "Util/DllProcedure.h"
#include "ISnShaper.h"
#include "Util/FileStream.h"

struct RiPersonage : public sn::IPersonage
{
	virtual void getValue(sn::attr_t& attr, Math::Matrix4f& val)
	{
		val = Math::Matrix4f::id;
	}
	virtual void getValue(sn::attr_t& attr, Math::Vec3f& val)
	{
		val = Math::Vec3f(0, 0, 0);
	}
	virtual void getValue(sn::attr_t& attr, float& val)
	{
		val = 0;
	}
};

SnDSO::SnDSO(const char* filename)
{
	this->filename = filename;
}

SnDSO::~SnDSO() 
{
}

typedef sn::IShaper* (*tag_GetShaper)();

bool SnDSO::GenRIB(IPrman* prman) 
{
	Util::FileStream stream;
	stream.open(filename.c_str());

	std::string dll, proc;
	stream >> dll >> proc;
	Util::DllProcedure dllproc(dll.c_str(), proc.c_str());
	if( !dllproc.isValid())
		return false;
	sn::tag_GetShaper gs = (sn::tag_GetShaper)dllproc.proc;
	sn::IShaper* shaper = (*gs)();
	if(!shaper)
		return false;

	sn::IShape* shape = shaper->load(stream, 0);
	RiPersonage personage;
	shaper->render(shape, &personage, prman);

    return true;
}

