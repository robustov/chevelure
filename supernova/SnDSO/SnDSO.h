#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <sys/types.h>
#include <sys/stat.h>
#include <string>
#include <map>
#include <vector>
#include <list>
#include <iomanip>
#include <iostream>

#include <ri.h>
	#include "mathNpixar\IPrman.h"

#if defined (_WIN32)
	#include <windows.h>
	#define kPathMax 256
#else
	#define kPathMax 256
	#include <unistd.h>
#endif


template <typename T1, typename T2>
void copy3f(T1& dst, const T2& src)
{
	(dst)[0] = src.x;
	(dst)[1] = src.y;
	(dst)[2] = src.z;
}
template <typename T1, typename T2>
void copy3fb(T1& dst, const T2& src)
{
	dst.x = (src)[0];
	dst.y = (src)[1];
	dst.z = (src)[2];
}



class SnDSO 
{
public:
	std::string filename;
public:
    SnDSO(const char* filename);
    ~SnDSO();
    bool GenRIB(IPrman* prman);
private:
};
