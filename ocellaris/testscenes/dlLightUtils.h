/* Superellipse soft clipping
 * Input:
 *   - point Q on the x-y plane
 *   - the equations of two superellipses (with major/minor axes given by
 *        a,b and A,B for the inner and outer ellipses, respectively)
 * Return value:
 *   - 0 if Q was inside the inner ellipse
 *   - 1 if Q was outside the outer ellipse
 *   - smoothly varying from 0 to 1 in between
 */
 
float clipSuperellipse (point Q;          /* Test point on the x-y plane */
		  float a, b;       /* Inner superellipse */
		  float A, B;       /* Outer superellipse */
		  float roundness;  /* Same roundness for both ellipses */
		 )
{
    float result = 0;
    float x = abs(xcomp(Q)), y = abs(ycomp(Q));
    if (x != 0 || y != 0) {  /* avoid degenerate case */
	if (roundness < 1.0e-6) {
	    /* Simpler case of a square */
	    result = 1 - (1-smoothstep(a,A,x)) * (1-smoothstep(b,B,y));
	} else if (roundness > 0.9999) {
	    /* Simple case of a circle */
	    float re = 2;   /* roundness exponent */
	    float sqr (float x) { return x*x; }
	    float q = a * b / sqrt (sqr(b*x) + sqr(a*y));
	    float r = A * B / sqrt (sqr(B*x) + sqr(A*y));
	    result = smoothstep (q, r, 1);
	} else {
	    /* Harder, rounded corner case */
	    float re = 2/roundness;   /* roundness exponent */
	    float q = a * b * pow (pow(b*x, re) + pow(a*y, re), -1/re);
	    float r = A * B * pow (pow(B*x, re) + pow(A*y, re), -1/re);
	    result = smoothstep (q, r, 1);
	}
    }
    return result;
}


/* Volumetric light shaping
 * Inputs:
 *   - the point being shaded, in the local light space
 *   - all information about the light shaping, including z smooth depth
 *     clipping, superellipse x-y shaping, and distance falloff.
 * Return value:
 *   - attenuation factor based on the falloff and shaping
 */
float
ShapeLightVolume (point PL;             /* Point in light space */
		  string lighttype;              /* what kind of light */
		  vector axis;                   /* light axis */
                  float znear, zfar;             /* z clipping */
		  float nearedge, faredge;
		  float falloff, falloffdist;    /* distance falloff */
		  float maxintensity;
		  float shearx, sheary;          /* shear the direction */
		  float width, height;           /* xy superellipse */
		  float hedge, wedge, roundness;
		  float beamdistribution;        /* angle falloff */
		  )
{	
    /* Examine the z depth of PL to apply the (possibly smooth) cuton and
     * cutoff.
     */
    float atten = 1;
    float PLlen = length(PL);
    float Pz;
    if (lighttype == "spot") {
	Pz = zcomp(PL);
    } else {
	/* For omni or area lights, use distance from the light */
	Pz = PLlen;
    }
    atten *= smoothstep (znear-nearedge, znear, Pz);
    atten *= 1 - smoothstep (zfar, zfar+faredge, Pz);
    
    /* Distance falloff */
    if (falloff != 0) {
	if (PLlen > falloffdist) {
	    atten *= pow (falloffdist/PLlen, falloff);
	} else {
	    float s = log (1/maxintensity);
	    float beta = -falloff/s;
	    atten *= (maxintensity * exp (s * pow(PLlen/falloffdist, beta)));
	}
   }

    /* Clip to superellipse */
    if (lighttype != "point" && beamdistribution > 0)
	atten *= pow (zcomp(normalize(vector PL)), beamdistribution);
    if (lighttype == "spot") {
	atten *= 1 - clipSuperellipse (point(PL/Pz-point(shearx,sheary,0)),
				       width, height,
				       width+wedge, height+hedge, roundness);
    }
    return atten;
}

/* Evaluate the occlusion between two points, P1 and P2, due to a fake
 * blocker.  Return 0 if the light is totally blocked, 1 if it totally
 * gets through.
 */
color BlockerContribution (point P1, P2; string blockercoords;  float blockerwidth, blockerheight;
		     float blockerwedge, blockerhedge; float blockerround, blockeropacity;
           )
{	
float unoccluded;
/* Get the surface and light positions in blocker coords */
point Pb1 = transform (blockercoords, P1);
point Pb2 = transform (blockercoords, P2);
color C = 1;
point Pplane;
/* Blocker works only if it's straddled by ray endpoints. */
if (zcomp(Pb2)*zcomp(Pb1) < 0)
{
vector Vlight = (Pb1 - Pb2);
Pplane = Pb1 - Vlight*(zcomp(Pb1)/zcomp(Vlight));
unoccluded = blockeropacity*(1-clipSuperellipse (Pplane, blockerwidth, blockerheight,	blockerwidth+blockerwedge,
blockerheight+blockerhedge, blockerround));
}
C *= 1-unoccluded;
return C;
}

color BlockerProjection(point P1, P2; string blockertexture, blockercoords; float blockeropacity, blockerblur,
	blockerwidth, blockerheight;)
{
float unoccluded = 1;	
point Pb1 = transform (blockercoords, P1);
point Pb2 = transform (blockercoords, P2);
color C = 0;
point Pplane;
if (zcomp(Pb2)*zcomp(Pb1) < 0)
{
vector Vlight = (Pb1 - Pb2);
Pplane = Pb1 - Vlight*(zcomp(Pb1)/zcomp(Vlight));
float xslide = 0.5*(1+xcomp(Pplane));
float yslide = 0.5*(1-ycomp(Pplane));
C += blockeropacity*(color texture (blockertexture, xslide, yslide, "blur", blockerblur));
}
color F = 1-C;
return F;
}