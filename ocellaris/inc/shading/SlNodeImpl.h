#pragma once

#include "shading/IShaderTemplate.h"
#include "shading/IShadingGraphNode.h"
#include "Util/DllProcedure.h"

#ifdef MAYA_API_VERSION
	#include <maya/MRampAttribute.h>
	#include <maya/MIntArray.h>
	#include <maya/MFloatArray.h>
#endif

//! @ingroup shading 
//! \brief SlNodeImpl
//! 
//! ����� ��� ������ � �������� SL ���������
class SlNodeImpl
{
public:
	struct InputParameter
	{
		std::string name;
		cls::enParamType type;
		cls::Param defval;

		InputParameter();
		InputParameter(const char* name, cls::enParamType type, cls::Param defval);
	};
protected:
	// ����������
	std::string slfilename;
	// ��������� ����
#ifdef MAYA_API_VERSION
		MObject node;
#endif

	// ������ � �������� ���������. ����������� � ������ InitParameters
	std::vector< InputParameter > inputs;

public:
	SlNodeImpl(void);
	~SlNodeImpl(void);
	bool Init(const char* slfilename);
	bool isValid() const;
	std::string getName();
	std::vector<std::string> getAttributeList();

	// ������ � �������� ���������. ���������. 
	void InitParameters(
		#ifdef MAYA_API_VERSION
		MObject node
		#endif
		);

	// �������� ����������
	int getParameterCount(
		);
	bool getParameter(
		#ifdef MAYA_API_VERSION
		MObject node,
		#endif
		int i, 
		std::string& name, 
		cls::Param& val);

protected:
	#ifdef MAYA_API_VERSION
	void AddAttributeToNode(
		MObject node,
		InputParameter& param
		);
	#endif
};

#include "inl/SlNodeImpl.hpp"