#ifdef MAYA_API_VERSION
#include "ocellaris/OcellarisExport.h"
ShadingGraphNodeImpl* getGraphNode(MObject node);
#endif

inline ShadingGraphNodeImpl::InputParameter::InputParameter(shn::Parameter* p)
{
	this->p=p; 
	attach=0;
	outparaminattach=0;
	defaultattach=0;
}
inline ShadingGraphNodeImpl::InputParameter::InputParameter(const InputParameter& arg)
{
	p = arg.p;
	attach = arg.attach;
	defaultattach = 0;
	outparaminattach = arg.outparaminattach;

	if( arg.defaultattach)
	{
		defaultattach = new ShadingGraphNodeImpl();
		*defaultattach = *arg.defaultattach;
		if( arg.attach == arg.defaultattach)
			attach = defaultattach;
	}
}
inline ShadingGraphNodeImpl::InputParameter::~InputParameter()
{
	if( defaultattach)
		delete defaultattach;
}


inline ShadingGraphNodeImpl::ShadingGraphNodeImpl(void)
{
	shaderTemplate = NULL;
}

inline ShadingGraphNodeImpl::~ShadingGraphNodeImpl(void)
{
}
inline bool ShadingGraphNodeImpl::Init(const char* dllNprocname)
{
	std::string buf = dllNprocname;
	char *token = strtok( (char*)buf.c_str(), "@");
	std::string dll, entrypoint;
	if( token != NULL)
	{
		dll = token;
		token = strtok( NULL, "@" );
		if( token != NULL)
			entrypoint = token;
		else
		{
			entrypoint = dll;
			dll = "ocellarisShaders.dll";
		}
	}
	return Init(dll.c_str(), entrypoint.c_str());
}

inline bool ShadingGraphNodeImpl::Init(const char* dllname, const char* proc)
{
	procTemplate.Load(dllname, proc);
	if( !procTemplate.isValid()) return false;

	shaderTemplate = (*procTemplate)();
	if(!shaderTemplate) return false;
	return true;
}
inline bool ShadingGraphNodeImpl::isValid() const
{
	return shaderTemplate!=NULL;
}
inline std::string ShadingGraphNodeImpl::getName()
{
	if(procTemplate.dll.empty()) 
		return procTemplate.entrypoint;
	return procTemplate.dll + "@" + procTemplate.entrypoint;
}
inline std::vector<std::string> ShadingGraphNodeImpl::getAttributeList()
{
	std::vector<std::string> list(inputs.size());
	for(int i=0; i<(int)inputs.size(); i++)
	{
		list[i]  = shn::pt_typestr( inputs[i].p->getType());
		list[i] += " ";
		list[i] += inputs[i].p->getName();
	}
	return list;
}

//! ��� �������� �������
inline shn::IShaderTemplate::enTemplateType ShadingGraphNodeImpl::getTemplateType()
{
	if(!shaderTemplate) return shn::IShaderTemplate::UNKNOWN;
	return shaderTemplate->GetType();
}

// ������ ��������� � ������ inputs � outputs
inline void ShadingGraphNodeImpl::InitParameters(
	#ifdef MAYA_API_VERSION
	MObject node
	#endif
	)
{
	if( !isValid()) return;

	#ifdef MAYA_API_VERSION
	this->node = node;
	#endif

	inputs.clear();
	outputs.clear();
	mainOutput = NULL;

	mainOutput = shaderTemplate->Parameters(*this);

	// 
	fulllist.push_back(this);
}
// �������� ��� ����������� �������. � ������ �������
inline void ShadingGraphNodeImpl::InputTemplates(
	std::vector<ShadingGraphNodeImpl*>& inputtemplates
	)
{
	int i;
	for(i=0; i<(int)inputs.size(); i++)
	{
		ShadingGraphNodeImpl* in = inputs[i].attach;
		if( !in) continue;
		in->InputTemplates(inputtemplates);
	}
	/////////////////////////////////
	// �������� ����
	// ��������� ��� �� �����?
	for( i=0; i<(int)inputtemplates.size(); i++)
	{
		if(inputtemplates[i]==this)
			return;
	}
	inputtemplates.push_back(this);
}

// ����������� ��� ������� ��������� � ����������� �����������
inline void ShadingGraphNodeImpl::InputParameters(
	std::vector< InputParameter >& rowinputs
	)
{
	for(int i=0; i<(int)inputs.size(); i++)
	{
		InputParameter& ip = inputs[i];
		shn::Parameter* param = ip.p;
		ShadingGraphNodeImpl* ingn = ip.attach;

		if( param->flags & shn::OPTION)
			continue;

		std::vector<shn::Parameter*> sublist;
		param->subParameters(sublist);
		if( sublist.empty())
		{
			rowinputs.push_back(InputParameter(ip));
		}
		else
		{
			if(!ingn)
			{
				for(int i=0; i<(int)sublist.size(); i++)
					rowinputs.push_back(InputParameter(sublist[i]));
			}
			else
			{
				std::vector<shn::Parameter*> attachsublist;
				ingn->mainOutput->subParameters(attachsublist);

				for(int i=0; i<(int)sublist.size(); i++)
				{
					rowinputs.push_back(InputParameter(sublist[i]));
					InputParameter& subip = rowinputs.back();

					if( i<(int)attachsublist.size())
					{
						subip.attach = ingn;
						subip.outparaminattach = attachsublist[i];
printf("attach to {0x%x : 0x%x}\n", ingn, attachsublist[i]);
					}
				}
			}
		}
	}
}
// ����������� ��� �������� ��������� � ����������� �����������
inline void ShadingGraphNodeImpl::OutputParameters(
	std::vector< shn::Parameter* >& rowoutputs
	)
{
	// �������� ���������
	for(int i=0; i<=(int)outputs.size(); i++)
	{
		shn::Parameter* param = mainOutput;
		if(i<(int)outputs.size()) 
			param = outputs[i];

		std::vector<shn::Parameter*> sublist;
		param->subParameters(sublist);
		if( sublist.empty())
		{
			rowoutputs.push_back(param);
		}
		else
		{
			for(int i=0; i<(int)sublist.size(); i++)
			{
				shn::Parameter* subp = sublist[i];
				rowoutputs.push_back(subp);
			}
		}
	}
}
// �������� �����
inline shn::Parameter* ShadingGraphNodeImpl::getMainOutput(
	)
{
	return mainOutput;
}

// ��� ���������
inline std::string ShadingGraphNodeImpl::getProcName()
{
	std::string name = procTemplate.dll + "_" + procTemplate.entrypoint;
	for(int i=0; i<(int)name.size(); i++)
	{
		if(name[i]=='.') name[i]='_';
	}
	return name;
}

// ������ ���� ���������
inline std::string ShadingGraphNodeImpl::BuildProcedure(
	std::string& includes
	)
{
	std::string procname = getProcName();
	header = "";
	procedure = "";
	procedure += "void ";
	procedure += procname + "( ";

	// ������� ���������
	std::vector< InputParameter > rowinputs;
	this->InputParameters(rowinputs);
	int i;
	for(i=0; i<(int)rowinputs.size(); i++)
	{
		InputParameter& ip = rowinputs[i];
		std::string p = ip.p->getDeclaration();
		p += "; ";
		procedure += p;
	}
	// �������� ���������
	std::vector< shn::Parameter* > rowoutputs;
	this->OutputParameters(rowoutputs);
	for(i=0; i<(int)rowoutputs.size(); i++)
	{
		shn::Parameter* param = rowoutputs[i];
		std::string p = param->getDeclaration();
		p += "; ";
		procedure += "output "+p;
	}

	procedure += ")\n";
	procedure += "{\n";

	// ���� ���������
	shaderTemplate->Build(*this);

	procedure += "}\n";

	includes = this->header;
	return procedure;
}

// ������ ����� ���������
inline std::string ShadingGraphNodeImpl::BuildProcedureCall(
	std::map<VariableKey, std::string>& variables
	)
{
	// ��������� � ���� �������
	std::string params;
	// ���������� ���. ����������
	std::string vars;

	// ����
	std::vector< InputParameter > rowinputs;
	this->InputParameters(rowinputs);
	int i;
	for(i=0; i<(int)rowinputs.size(); i++)
	{
		InputParameter& ip = rowinputs[i];
		if(!params.empty())
			params += ", ";
		params += "\n\t";

		std::string decl = ip.p->getDeclaration();
		if(ip.attach)
		{
			// ����� ���������� � variables
			std::string varname = ip.attach->FindVarName(ip.outparaminattach, variables);
			if( varname.empty())
				return "";
			params += varname;
		}
		else
		{
			// �������� ��������
			params += getValueText(*ip.p);
		}
	}
	// �����
	std::vector< shn::Parameter* > rowoutputs;
	this->OutputParameters(rowoutputs);
	for(i=0; i<(int)rowoutputs.size(); i++)
	{
		shn::Parameter* param = rowoutputs[i];
		// �������� ����������
		VariableKey key(this, param);
		char buf[256];
		sprintf(buf, "%s_%s_%d", param->getName(), this->getProcName().c_str(), variables.size());
printf("Add var %s: {0x%x : 0x%x}\n", buf, this, param);
		variables[key] = buf;

		std::string type = shn::pt_typestr( param->getType());
		vars += type;
		vars += " ";
		vars += buf;
		vars += ";\n";

		if(!params.empty())
			params += ", ";
		params += "\n\t";
		params += buf;
	}

	std::string shader;
	shader += vars;
	shader += this->getProcName();
		shader += "(";
		shader += params;
		shader += ");\n";

	return shader;
	
}
// ����� ��� ���������� � ������
inline std::string ShadingGraphNodeImpl::FindVarName(
	shn::Parameter* p, 
	std::map<VariableKey, std::string>& variables
	)
{
	// ����� ���������� � variables
	std::map<VariableKey, std::string>::iterator it = variables.find(
		VariableKey(this, p));
	if(it == variables.end())
	{
		// ������
		printf("ShaderAssembler::callProcedure cant find input variable {0x%x : 0x%x}\n", this, p);
		return "";
	}
	return it->second;
}










///////////////////////////////////////////
//
// �-��� IShadingGraphNode
inline void ShadingGraphNodeImpl::RegisterInput(shn::Parameter& param)
{
	inputs.push_back(InputParameter(&param));
	InputParameter& p = inputs.back();

	#ifdef MAYA_API_VERSION
	if( !node.isNull())
	{
		AddAttributeToNode(node, param);
	}
	#endif

	// ���������� ����:
	if( param.defaultConnectTo!="")
	{
		p.defaultattach = new ShadingGraphNodeImpl();
		p.defaultattach->Init(param.defaultConnectTo.c_str());
		p.defaultattach->InitParameters(
			#ifdef MAYA_API_VERSION
			MObject::kNullObj
			#endif
			);
		p.attach = p.defaultattach;
		p.outparaminattach = p.attach->mainOutput;
	}
	// ����� ������������ ����
	#ifdef MAYA_API_VERSION
	{
		MPlug plug = MFnDependencyNode(node).findPlug(param.name.c_str());
		MPlugArray connectedPlugs;
		bool asSrc = false;
		bool asDst = true;
		plug.connectedTo( connectedPlugs, asDst, asSrc );
		if (connectedPlugs.length() == 1)
		{
			MObject attachnode = connectedPlugs[0].node();
			ShadingGraphNodeImpl* a = getGraphNode(attachnode);
			if(a)
			{
				p.attach = a;
				p.outparaminattach = p.attach->mainOutput;
			}
		}
	}
	#endif

	//...
	// p.attach = NULL;
	if(p.attach)
	{
	}
}
inline void ShadingGraphNodeImpl::RegisterOutput(shn::Parameter& param)
{
	outputs.push_back(&param);
}

inline void ShadingGraphNodeImpl::Include(const char* format, ...)
{
	va_list argList; va_start(argList, format);
	char sbuf[1024];
	_vsnprintf(sbuf, 1023, format, argList);
	va_end(argList);

	header += "#include \"";
	header += sbuf;
	header += "\"\n";
}
inline void ShadingGraphNodeImpl::Add(const char* format, ...)
{
	va_list argList; va_start(argList, format);
	char sbuf[1024];
	_vsnprintf(sbuf, 1023, format, argList);
	va_end(argList);

	procedure += "\t";
	procedure += sbuf;
	procedure += "\n";
}
inline void ShadingGraphNodeImpl::output(const char* format, ...)
{
	va_list argList; va_start(argList, format);
	char sbuf[1024];
	_vsnprintf(sbuf, 1023, format, argList);
	va_end(argList);

	procedure += "\t";
	procedure += sbuf;
	procedure += "\n";
}

inline void ShadingGraphNodeImpl::Comment(const char* format, ...)
{
	va_list argList; va_start(argList, format);
	char sbuf[1024];
	_vsnprintf(sbuf, 1023, format, argList);
	va_end(argList);

	procedure += "//";
	procedure += sbuf;
}


// �������� ����������
inline cls::Param ShadingGraphNodeImpl::getValue(shn::Parameter& param)
{
	cls::Param val = param.getDefVal();

#ifdef MAYA_API_VERSION
	cls::Param attrval = ocExport_GetAttrValue(
		node, param.getName());
	if( !attrval.empty() && attrval->type == val->type)
		val = attrval;
#endif

	return val;
}
inline std::string ShadingGraphNodeImpl::getValueText(shn::Parameter& param)
{
	cls::Param p = getValue(param);
	return shn::pt_string(p);
}

inline void ShadingGraphNodeImpl::getValue(const shn::EnumParameter& param, int& value)
{
	value=0;
}

#ifdef MAYA_API_VERSION
inline void ShadingGraphNodeImpl::AddAttributeToNode(
	MObject node,
	shn::Parameter& param
	)
{
	ocExport_AddAttrType(
		node, 
		param.getName(), 
		param.getType(), 
		param.getDefVal());
}
#endif
