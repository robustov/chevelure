#pragma once

namespace shn
{
	//////////////////////////////////////////
	// ColorParameter	


	// ��������� ��� ����������
	inline std::string pt_typestr(cls::enParamType type)
	{
		switch(type)
		{
		case cls::PT_UNKNOWN:
			return "unknown";
		case cls::PT_BOOL:
			return "bool";
		case cls::PT_STRING:
			return "string";
		case cls::PT_FLOAT:
		case cls::PT_HALF:
		case cls::PT_DOUBLE:
			return "float";
		case cls::PT_POINT:
			return "point";
		case cls::PT_VECTOR:
			return "vector";
		case cls::PT_NORMAL:
			return "normal";
		case cls::PT_COLOR:
			return "color";
//		case cls::PT_HPOINT,		//!< float[4]
		case cls::PT_MATRIX:
			return "matrix";
		case cls::PT_INT8:
		case cls::PT_UINT8:
		case cls::PT_INT16: 
		case cls::PT_UINT16:
		case cls::PT_INT:
		case cls::PT_UINT:
			return "int";
		}
		return "";
	}

	// �������� ���������� � ������
	inline std::string pt_string(const cls::Param p)
	{
		char buf[256]="";
		switch(p->type)
		{
		case cls::PT_BOOL:
			{
				cls::P<bool> P(p);
				if( P.empty()) return "";
				_snprintf(buf, 256, "%f", P.data()?1.f:0.f);
				break;
			}
		case cls::PT_STRING:
			{
				cls::PS P(p);
				if( P.empty()) return "";
				_snprintf(buf, 256, "\"%s\"", P.data());
				break;
			}
		case cls::PT_FLOAT:
		case cls::PT_HALF:
		case cls::PT_DOUBLE:
			{
				cls::P<float> P(p);
				if( P.empty()) return "";
				_snprintf(buf, 256, "%f", P.data());
				break;
			}
		case cls::PT_POINT:
			{
				cls::P<Math::Vec3f> P(p, cls::PT_POINT);
				if( P.empty()) return "";
				Math::Vec3f& v = P.data();
				_snprintf(buf, 256, "point(%f,%f,%f)", v.x, v.y, v.z);
				break;
			}
		case cls::PT_VECTOR:
			{
				cls::P<Math::Vec3f> P(p, cls::PT_VECTOR);
				if( P.empty()) return "";
				Math::Vec3f& v = P.data();
				_snprintf(buf, 256, "vector(%f,%f,%f)", v.x, v.y, v.z);
				break;
			}
		case cls::PT_NORMAL:
			{
				cls::P<Math::Vec3f> P(p, cls::PT_NORMAL);
				if( P.empty()) return "";
				Math::Vec3f& v = P.data();
				_snprintf(buf, 256, "normal(%f,%f,%f)", v.x, v.y, v.z);
				break;
			}
		case cls::PT_COLOR:
			{
				cls::P<Math::Vec3f> P(p, cls::PT_COLOR);
				if( P.empty()) return "";
				Math::Vec3f& v = P.data();
				_snprintf(buf, 256, "color(%f,%f,%f)", v.x, v.y, v.z);
				break;
			}
//		case cls::PT_HPOINT,		//!< float[4]
//		PT_MATRIX,		//!< matrix = float[4]
		case cls::PT_INT8:
		case cls::PT_UINT8:
		case cls::PT_INT16: 
		case cls::PT_UINT16:
		case cls::PT_INT:
		case cls::PT_UINT:
			{
				cls::P<int> P(p);
				if( P.empty()) return "";
				int v = P.data();
				_snprintf(buf, 256, "%d", v);
				break;
			}
		}
		return buf;
	}

	// ��������� ��� ���������� ��� ����������
	inline std::string pt_prmantypestr(cls::enParamType type)
	{
		switch(type)
		{
		case cls::PT_BOOL:
			return "float";			// ���� bool!
		case cls::PT_STRING:
			return "string";
		case cls::PT_FLOAT:
		case cls::PT_HALF:
		case cls::PT_DOUBLE:
			return "float";
		case cls::PT_POINT:
			return "point";
		case cls::PT_VECTOR:
			return "vector";
		case cls::PT_NORMAL:
			return "normal";
		case cls::PT_COLOR:
			return "color";
//		case cls::PT_HPOINT,		//!< float[4]
		case cls::PT_MATRIX:
			return "matrix";
		case cls::PT_INT8:
		case cls::PT_UINT8:
		case cls::PT_INT16: 
		case cls::PT_UINT16:
		case cls::PT_INT:
		case cls::PT_UINT:
			return "float";			// ���� int!
		}
		return "";
	}


}