#ifdef MAYA_API_VERSION
#include "ocellaris/OcellarisExport.h"
#endif
#include "mathNpixar/ISlo.h"

inline SlNodeImpl::InputParameter::InputParameter()
{
	name = "";
	type = cls::PT_UNKNOWN;

}
inline SlNodeImpl::InputParameter::InputParameter(const char* name, cls::enParamType type, cls::Param defval)
{
	this->name = name;
	this->type = type;
	this->defval = defval;
}


inline SlNodeImpl::SlNodeImpl(void)
{
}

inline SlNodeImpl::~SlNodeImpl(void)
{
}
inline bool SlNodeImpl::Init(const char* slfilename)
{
	this->slfilename = slfilename;
	return true;
}

inline bool SlNodeImpl::isValid() const
{
	return !slfilename.empty();
}
inline std::string SlNodeImpl::getName()
{
	return slfilename;
}
inline std::vector<std::string> SlNodeImpl::getAttributeList()
{
	std::vector<std::string> list(inputs.size());
	for(int i=0; i<(int)inputs.size(); i++)
	{
		list[i]  = shn::pt_typestr( inputs[i].type);
		list[i] += " ";
		list[i] += inputs[i].name;
	}
	return list;
}

// ������ ��������� � ������ inputs � outputs
inline void SlNodeImpl::InitParameters(
	#ifdef MAYA_API_VERSION
	MObject node
	#endif
	)
{
#ifdef _DEBUG
	getISloDll SloDll("IPrman12.5D.dll", "getISlo");
#endif
#ifdef NDEBUG
	static getISloDll SloDll("IPrman12.5.dll", "getISlo");
#endif
	inputs.clear();
	if( !SloDll.isValid())
	{
		printf("IPrmanD.dll or entry point not found", slfilename.c_str());
		return;
	}

	ISlo* slo = (*SloDll)();

	int res = slo->Slo_SetShader(slfilename.c_str());
	if(res!=0)
	{
		std::string cut = slfilename;
		size_t x = cut.rfind('.');
		if(x!=std::string::npos)
			cut = cut.erase(x);
		res = slo->Slo_SetShader(cut.c_str());
	}

	if(res!=0)
	{
		printf("slo->Slo_SetShader(%s) failed", slfilename.c_str());
		return;
	}

	int n = slo->Slo_GetNArgs();
	inputs.reserve(n);
	for(int i=0; i<n; i++)
	{
		SLO_VISSYMDEF* def = slo->Slo_GetArgById(i+1);
		if( def->svd_storage == SLO_STOR_OUTPUTPARAMETER)
		{
			continue;
		}
		InputParameter* param = NULL;
		switch(def->svd_type)
		{
		case SLO_TYPE_POINT:
			{
			Math::Vec3f v(def->svd_default.pointval->xval, def->svd_default.pointval->yval, def->svd_default.pointval->zval);
			cls::P<Math::Vec3f> defval(v, cls::PT_POINT);
			inputs.push_back( InputParameter(def->svd_name, cls::PT_POINT, defval));
			param = &inputs.back();
			printf( "%s: %s = %f %f %f\n", "point", def->svd_name, v.x, v.y, v.z);
			}
			break;
		case SLO_TYPE_COLOR:
			{
			Math::Vec3f v(def->svd_default.pointval->xval, def->svd_default.pointval->yval, def->svd_default.pointval->zval);
			cls::P<Math::Vec3f> defval(v, cls::PT_COLOR);
			inputs.push_back( InputParameter(def->svd_name, cls::PT_COLOR, defval));
			param = &inputs.back();
			printf( "%s: %s = %f %f %f \n", "color", def->svd_name, v.x, v.y, v.z);
			}
			break;
		case SLO_TYPE_SCALAR:
			{
				float v = *def->svd_default.scalarval;
				cls::P<float> defval(v);
				inputs.push_back( InputParameter(def->svd_name, cls::PT_FLOAT, defval));
				param = &inputs.back();
				printf( "%s: %s = %f\n", "float", def->svd_name, v);
			}
			break;
		case SLO_TYPE_STRING:
			{
				const char* v = def->svd_default.stringval;
				cls::PS defval(v);
				inputs.push_back( InputParameter(def->svd_name, cls::PT_STRING, defval));
				param = &inputs.back();
				printf( "%s: %s = %s\n", "string", def->svd_name, v);
			}
			break;
		}
		#ifdef MAYA_API_VERSION
		if( !node.isNull() && param!=NULL)
		{
			AddAttributeToNode(node, *param);
		}
		#endif
	}
	slo->Slo_EndShader();

	#ifdef MAYA_API_VERSION
	this->node = node;
	#endif
}

// �������� ����������
inline int SlNodeImpl::getParameterCount(
	)
{
	return (int)inputs.size();
}
inline bool SlNodeImpl::getParameter(
	#ifdef MAYA_API_VERSION
	MObject node,
	#endif
	int i, 
	std::string& name, 
	cls::Param& val)
{
	if(i<0 && i>=(int)inputs.size())
		return false;
	name = inputs[i].name;

	#ifdef MAYA_API_VERSION
	val = ocExport_GetAttrValue(
		node, 
		name.c_str());
	#endif
	return true;
}

#ifdef MAYA_API_VERSION
inline void SlNodeImpl::AddAttributeToNode(
	MObject node,
	InputParameter& param
	)
{
	ocExport_AddAttrType(
		node, 
		param.name.c_str(), 
		param.type, 
		param.defval);
}
#endif
