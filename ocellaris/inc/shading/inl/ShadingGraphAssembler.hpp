inline std::string ShadingGraphAssembler::BuildGraph(
	cls::IExportContext* context,
	ShadingGraphNodeImpl& root, 
	const char* shaderName)
{
//	root.InitParameters();

	shn::IShaderTemplate::enTemplateType type = root.getTemplateType();

	// �������� ��� ����������� �������. � ������ �������
	std::vector<ShadingGraphNodeImpl*> inputtemplates;
	root.InputTemplates(inputtemplates);

	std::string procedures, text, header;

	// ��������� �������� � �������
	std::map<ShadingGraphNodeImpl::VariableKey, std::string> variables;
	for(int i=0; i<(int)inputtemplates.size(); i++)
	{
		ShadingGraphNodeImpl* gn = inputtemplates[i];
		std::string _header;
		procedures += gn->BuildProcedure(_header);
		header += _header;

		text += gn->BuildProcedureCall(variables);
	}


	std::string shader;
	shader += "// OCELLARIS SHADING NETWORK ASSEMBLER\n//\n";
	shader += header;
	switch( type)
	{
		case shn::IShaderTemplate::SURFACESHADER:
		case shn::IShaderTemplate::COLOR:
		case shn::IShaderTemplate::FLOAT:	
		case shn::IShaderTemplate::VECTOR:	
		case shn::IShaderTemplate::NORMAL:	
		case shn::IShaderTemplate::MATRIX:	
		case shn::IShaderTemplate::MANIFOLD:
			shader += "surface";
			break;
		case shn::IShaderTemplate::DISPLACESHADER:
			shader += "displace";
			break;
	}
	shader += " ";
	shader += shaderName;
	shader += "()\n";
	shader += "{\n";

	shader += procedures;

	shader += text;

	shn::Parameter* output = root.getMainOutput();


	switch(type)
	{
	case shn::IShaderTemplate::SURFACESHADER:
		{
			std::vector<shn::Parameter*> sublist;
			output->subParameters(sublist);
			if(sublist.size()==2)
			{
				shader += "Ci = ";
					shader += root.FindVarName( sublist[0], variables);
					shader += ";\n";
				shader += "Oi = ";
					shader += root.FindVarName( sublist[1], variables);
					shader += ";\n";
			}
		}
		break;
	case shn::IShaderTemplate::DISPLACESHADER:
		printf("shn::IShaderTemplate::DISPLACESHADER not implemented\n");
		break;
	case shn::IShaderTemplate::COLOR:
		{
			shader += "Ci = ";
				shader += root.FindVarName( output, variables);
				shader += ";\n";
			shader += "Oi = color(1., 1., 1.);\n";
		}
		break;
	case shn::IShaderTemplate::FLOAT:	
	case shn::IShaderTemplate::VECTOR:	
	case shn::IShaderTemplate::NORMAL:	
	case shn::IShaderTemplate::MATRIX:	
	case shn::IShaderTemplate::MANIFOLD:
		printf("shn::IShaderTemplate::FLOAT-MANIFOLD not implemented\n");
		break;
	}
	shader += "}\n";
	return shader;
}

// ������������� ������
inline std::string ShadingGraphAssembler::Compile(
	cls::IExportContext* context,
	const char* infilename,
	const char* outfilename, 
	const char* _compilestring
	)
{
	STARTUPINFO si;
	memset(&si, 0, sizeof(si));
	si.cb = sizeof(si);
	PROCESS_INFORMATION pi;
	si.hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
//	si.hStdInput = GetStdHandle(STD_INPUT_HANDLE);
	si.hStdError = GetStdHandle(STD_ERROR_HANDLE);
	si.dwFlags |= STARTF_USESHOWWINDOW;
	si.wShowWindow = SW_HIDE;
//	si.dwFlags |= STARTF_USESTDHANDLES;
	//Pipe Test Out
//	HANDLE fout = CreateFile(
//		(outfilename+std::string(".log")).c_str(), 
//		GENERIC_WRITE, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
//	if( fout != INVALID_HANDLE_VALUE)
//	{
//		si.hStdOutput = fout;
//		si.hStdError = fout;
//	}
	std::string compilestring = (_compilestring!=NULL)?_compilestring:"";
	std::string RMANTREE = getenv("RMANTREE");
	std::string RATTREE = getenv("RATTREE");
	std::string RMSTREE = getenv("RMSTREE");
	Util::changeSlashToUnixSlash(RATTREE);
	Util::changeSlashToUnixSlash(RMANTREE);
	Util::changeSlashToUnixSlash(RMSTREE);
	

	char fullPath[2048];
	std::string outfn = "";
	if( !outfilename)
		outfn = std::string(infilename)+"o";
	else
		outfn = outfilename;
	if( compilestring.empty())
	{
//		_snprintf(fullPath, 2048, "%s/bin/shader.exe -o %s %s",
		_snprintf(fullPath, 2048, "%s/bin/shader.exe -I%s/lib/slim/include -o %s %s",
			RMANTREE.c_str(),
			RATTREE.c_str(),
			outfn.c_str(),
			infilename
			);
	}
	else
	{
		size_t pos = 0;
		for(;(pos=compilestring.find("%", pos))!=std::string::npos; )
		{
			if( compilestring[pos+1]=='s' )
			{
				pos = pos+1;
				continue;
			}
			size_t pos2 = compilestring.find("%", pos+1);
			if(pos2==std::string::npos)
				break;

			std::string var( compilestring.c_str()+pos+1, compilestring.c_str()+pos2);
			// ������� env
			char buf[1024];
			DWORD res = GetEnvironmentVariable(var.c_str(), buf, 1024);
			char* _value = getenv( var.c_str());
			std::string value = _value?_value:"";
			if( value.empty())
			{
				if( context)
					value = context->getEnvironment( var.c_str());
				if( value.empty())
				{
					if( context)
						context->error("environment variable %s not fount", var.c_str());
					pos = pos2+1;
					continue;
				}
			}
			compilestring.replace(pos, pos2+1-pos, value);
			pos = pos2+1;
		}

		_snprintf(fullPath, 2048, compilestring.c_str(),
			outfn.c_str(),
			infilename
			);
	}

	{
		std::string batname = infilename;
		batname += ".bat";
		FILE* file = fopen(batname.c_str(), "wt");
		if( file)
		{
			fputs(fullPath, file);
			fputs("\npause\n", file);
			fclose(file);
		}
	}
//	if( fout != INVALID_HANDLE_VALUE)
//		CloseHandle(fout);

	BOOL res = CreateProcess( NULL, (LPSTR)fullPath, NULL, NULL, TRUE, 0, NULL, NULL, &si, &pi);

	if ( !res )
	{
		context->error("Cant start:");
		context->error(fullPath);
		return "";
	}
	else
	{
		WaitForSingleObject( pi.hProcess, INFINITE );
		DWORD exitcode;
		GetExitCodeProcess(pi.hProcess, &exitcode);
		if( exitcode==0)
		{
//			context->error("compile successed\n");
		}
		else
		{
			context->error("shader not comliled: exitcode=0x%x:", exitcode);
			context->error(fullPath);
		}
		CloseHandle(pi.hProcess);
		return outfn.c_str();
	}
}
