#pragma once

#include "shading/IShaderTemplate.h"
#include "shading/IShadingGraphNode.h"
#include "Util/DllProcedure.h"

#ifdef MAYA_API_VERSION
	#include <maya/MRampAttribute.h>
	#include <maya/MIntArray.h>
	#include <maya/MFloatArray.h>
#endif

//! @ingroup shading 
//! \brief ShadingGraphNodeImpl
//! 
//! ����� ����������� shn::IShadingGraphNode, ��������� � ���� � �������������� ����� ShadingGraphNodeManagerImpl
class ShadingGraphNodeImpl : shn::IShadingGraphNode
{
public:
	// ����� (��������. ���� ������� ���, ����� �� ��������� ����)
	struct InputParameter
	{
		shn::Parameter* p;						// ��������
		ShadingGraphNodeImpl* attach;			// ����������� ����
		ShadingGraphNodeImpl* defaultattach;	// ����������� ���� �� ���������
		shn::Parameter* outparaminattach;		// �������� ����������� ���� 

		InputParameter(shn::Parameter* p);
		InputParameter(const InputParameter& arg);
		~InputParameter();
	};

	// VariableKey. ��� ������ ����������
	struct VariableKey
	{
		ShadingGraphNodeImpl* attach;
		shn::Parameter* outparaminattach;
		VariableKey(ShadingGraphNodeImpl* attach, shn::Parameter* outparaminattach)
		{
			this->attach = attach;
			this->outparaminattach = outparaminattach;
		}
		bool operator <(const VariableKey& arg) const
		{
			if( this->attach < arg.attach) return true;
			if( this->attach > arg.attach) return false;
			return (this->outparaminattach < arg.outparaminattach);
		}
	};

protected:
	// ����������
	shn::getIShaderTemplate procTemplate;
	shn::IShaderTemplate* shaderTemplate;
	// ��������� ����
#ifdef MAYA_API_VERSION
		MObject node;
#endif

	// ������ � �������� ���������. ����������� � ������ InitParameters
	std::vector< InputParameter > inputs;
	std::vector< shn::Parameter* > outputs;
	shn::Parameter* mainOutput;


public:
	// ����� ��������� � ���� ���������
	std::string header, procedure;
public:
	// ����������� � ������ ������ graphnode
	std::vector< ShadingGraphNodeImpl* > fulllist;

public:
	ShadingGraphNodeImpl(void);
	~ShadingGraphNodeImpl(void);
	bool Init(const char* dllNprocname);
	bool Init(const char* dllname, const char* proc);
	bool isValid() const;
	std::string getName();
	std::vector<std::string> getAttributeList();

	//! ��� �������� �������
	shn::IShaderTemplate::enTemplateType getTemplateType();

	// ������ � �������� ���������. ���������. 
	void InitParameters(
		#ifdef MAYA_API_VERSION
		MObject node
		#endif
		);
	// �������� ��� ����������� �������. � ������ �������
	void InputTemplates(
		std::vector<ShadingGraphNodeImpl*>& inputtemplates
		);
	// ����������� ��� ������� ��������� � ����������� �����������
	void InputParameters(
		std::vector< InputParameter >& inputs
		);
	// ����������� ��� �������� ��������� � ����������� �����������
	void OutputParameters(
		std::vector< shn::Parameter* >& outputs
		);
	// �������� �����
	shn::Parameter* getMainOutput(
		);

	// ��� ���������
	std::string getProcName();

	// ������ ���� ���������
	std::string BuildProcedure(
		std::string& includes);

	// ������ ����� ���������
	std::string BuildProcedureCall(
		std::map<VariableKey, std::string>& variables
		);

	// ����� ��� ���������� � ������
	std::string FindVarName(
		shn::Parameter* p, 
		std::map<VariableKey, std::string>& variables
		);


// �-��� IShadingGraphNode
public:
	virtual void RegisterInput(shn::Parameter& param);
	virtual void RegisterOutput(shn::Parameter& param);
	// �������� #include ��������� � ������ �������
	virtual void Include(const char* format, ...);
	// �������� ������ � �������
	virtual void Add(const char* format, ...);
	virtual void output(const char* format, ...);
	// �������� ������ �����������
	virtual void Comment(const char* format, ...);


	// �������� ����������
	virtual cls::Param getValue(shn::Parameter& param);
	virtual std::string getValueText(shn::Parameter& param);
	virtual void getValue(const shn::EnumParameter& param, int& value);

protected:
	#ifdef MAYA_API_VERSION
	void AddAttributeToNode(
		MObject node,
		shn::Parameter& param
		);
	#endif
};

#include "inl/ShadingGraphNodeImpl.hpp"