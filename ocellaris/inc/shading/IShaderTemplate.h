#pragma once
#include "Util\DllProcedure.h"
#include "IShadingGraphNode.h"

namespace shn
{
	//! @ingroup shading 
	//! \brief IShaderTemplate
	//! 
	//! ����� ��� ���������� �������� �������� (������� shading network)
	struct IShaderTemplate
	{
		//! ��� �������
		enum enTemplateType
		{
			UNKNOWN			= -1,
			SURFACESHADER	= 0,		// ������ ���� �����: shn::SurfaceOutput
			DISPLACESHADER	= 1,		// ������ ���� �����: vector ��� float
			COLOR			= 2,		// ������ ���� �����: color
			FLOAT			= 3,		// ������ ���� �����: float
			VECTOR			= 4,		// ������ ���� �����: vector
			NORMAL			= 5,		// ������ ���� �����: normal
			MATRIX			= 6,		// ������ ���� �����: matrix
			MANIFOLD		= 7,		// ������ ���� �����: shn::ManifoldParameter
		};

		//! ��� �������
		virtual enTemplateType GetType(
			)=0;

		//! ���������� ��� ����� ���������� � ������� � �������� ����������
		//! ������ �������� �������� (����������� - ����� ��� ����������� ���������� ��������)
		virtual Parameter* Parameters(IShadingGraphNode& graphnode)=0;

		//! ���������� ���� ���������
		virtual void Build(IShadingGraphNode& graphnode)=0;
	};
	// getIShaderTemplate
	typedef IShaderTemplate* (*t_getIShaderTemplate)(void);
	typedef Util::DllProcedureTmpl< t_getIShaderTemplate> getIShaderTemplate;

	// getShaderList
	typedef char** (*t_getShaderList)(void);
	typedef Util::DllProcedureTmpl< t_getShaderList> getShaderListProc;
}