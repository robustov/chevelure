#pragma once
#include "Math/Math.h"
#include "ocellaris/IParameter.h"

namespace shn
{
	// ����� ����������
	enum enParamFlags
	{
		CONNECTABLE = 1,	// �� ���� ����� ��������� ������
		OPTION = 2,			// ��� �������� ��� ��������� � ������ ���������� �������, �� ������� � �����!!!
	};

	// ��������� ��� ����������
	inline std::string pt_typestr(cls::enParamType type);
	// ��������� ��� ���������� ��� ����������
	inline std::string pt_prmantypestr(cls::enParamType type);
	// �������� ���������� � ������
	inline std::string pt_string(const cls::Param p);

	//! @ingroup shading 
	//! \brief Parameter
	//! 
	//! ������� ����� ��� ���������� �������� �������� (������� shading network)
	struct Parameter
	{
		std::string name, description;
		std::string defaultConnectTo;
		int flags;
		virtual std::string getDeclaration()
		{
			std::string declaration = pt_prmantypestr( getType())+" "+name;
			return declaration;
		}

		virtual cls::enParamType getType()=0;
		virtual const char* getName(){return name.c_str();};
		virtual cls::Param getDefVal()=0;
		// ���������� ������� �����������
		void DefConnectTo(const char* defcon) {defaultConnectTo=defcon;};

		// ��������� ���������
		virtual void subParameters(std::vector<Parameter*>& list){};
	};

	struct ParameterStruct : public Parameter
	{
		virtual cls::Param getDefVal(){return cls::Param();};
		virtual cls::enParamType getType(){return cls::PT_UNKNOWN;};
		virtual std::string getDeclaration(){return "error!!!";};
	};

	struct BoolParameter : public Parameter
	{
		bool defVal;

		void Init(
			const char* name, 
			const char* description="", 
			bool defVal=false,
			int flags=0					// ��. enParamFlags
			)
		{
			this->name = name;
			this->description = description;
			this->defVal = defVal;
			this->flags = flags;
		}
		virtual cls::enParamType getType()
		{
			return cls::PT_BOOL;
		};
		virtual cls::Param getDefVal()
		{
			return cls::P<bool>(defVal);
		};
	};

	// �������� ColorParameter 
	struct ColorParameter : public Parameter
	{
		Math::Vec3f defVal;

		void Init(
			const char* name, 
			const char* description="", 
			Math::Vec3f defVal=Math::Vec3f(0),
			int flags=0					// ��. enParamFlags
			)
		{
			this->name = name;
			this->description = description;
			this->defVal = defVal;
			this->flags = flags;
		}
		virtual cls::enParamType getType()
		{
			return cls::PT_COLOR;
		};
		virtual cls::Param getDefVal()
		{
			return cls::P<Math::Vec3f>(defVal, cls::PT_COLOR);
		};
	};

	// �������� VectorParameter 
	struct VectorParameter : public Parameter
	{
		Math::Vec3f defVal;

		void Init(
			const char* name, 
			const char* description="", 
			Math::Vec3f defVal=Math::Vec3f(0),
			int flags=0					// ��. enParamFlags
			)
		{
			this->name = name;
			this->description = description;
			this->defVal = defVal;
			this->flags = flags;
		}
		virtual cls::enParamType getType()
		{
			return cls::PT_VECTOR;
		};
		virtual cls::Param getDefVal()
		{
			return cls::P<Math::Vec3f>(defVal, cls::PT_VECTOR);
		};
	};
	// �������� PointParameter 
	struct PointParameter : public Parameter
	{
		Math::Vec3f defVal;

		void Init(
			const char* name, 
			const char* description="", 
			Math::Vec3f defVal=Math::Vec3f(0),
			int flags=0					// ��. enParamFlags
			)
		{
			this->name = name;
			this->description = description;
			this->defVal = defVal;
			this->flags = flags;
		}
		virtual cls::enParamType getType()
		{
			return cls::PT_POINT;
		};
		virtual cls::Param getDefVal()
		{
			return cls::P<Math::Vec3f>(defVal, cls::PT_POINT);
		};
	};
	// �������� NormalParameter 
	struct NormalParameter : public Parameter
	{
		Math::Vec3f defVal;

		void Init(
			const char* name, 
			const char* description="", 
			Math::Vec3f defVal=Math::Vec3f(0),
			int flags=0					// ��. enParamFlags
			)
		{
			this->name = name;
			this->description = description;
			this->defVal = defVal;
			this->flags = flags;
		}
		virtual cls::enParamType getType()
		{
			return cls::PT_NORMAL;
		};
		virtual cls::Param getDefVal()
		{
			return cls::P<Math::Vec3f>(defVal, cls::PT_NORMAL);
		};
	};


	// �������� Float
	struct FloatParameter : public Parameter
	{
		float defVal;
		float rmin, rmax;

		FloatParameter()
		{
			rmin=rmax=-1;
		}
		void Init(
			const char* name, 
			const char* description="", 
			float defVal=0,
			int flags=0
			)
		{
			this->name = name;
			this->description = description;
			this->defVal = defVal;
			this->flags = flags;
		}
		void SetRange(
			float rmin, float rmax
			)
		{
			this->rmin = rmin;
			this->rmax = rmax;
		}
		virtual cls::enParamType getType()
		{
			return cls::PT_FLOAT;
		};

		virtual cls::Param getDefVal()
		{
			return cls::P<float>(defVal);
		};
	};

	static std::string type(int&){return "float";}
	static std::string type(std::string&){return "string";}

	// �������� Enum
	struct EnumParameter : public Parameter
	{
		int defVal;
		std::map<int, std::string> fields;

		void Init(
			const char* name, 
			const char* description="", 
			int defVal=0,
			int flags=0
			)
		{
			this->name = name;
			this->description = description;
			this->defVal = defVal;
			this->flags = flags;
		}
		void SetField(
			int id, 
			const char* text
			)
		{
			fields[id] = text;
		}
		virtual cls::enParamType getType()
		{
			return cls::PT_INT;
		};
		virtual cls::Param getDefVal()
		{
			return cls::P<int>(defVal);
		}
	};
	struct EnumStringParameter : public Parameter
	{
		std::string defVal;
		std::map<std::string, std::string> fields;

		void Init(
			const char* name, 
			const char* description="", 
			std::string defVal=0,
			int flags=0
			)
		{
			this->name = name;
			this->description = description;
			this->defVal = defVal;
			this->flags = flags;
		}
		void SetField(
			std::string id, 
			const char* text
			)
		{
			fields[id] = text;
		}
		virtual cls::enParamType getType()
		{
			return cls::PT_STRING;
		};
		virtual cls::Param getDefVal()
		{
			return cls::PS(defVal);
		}
	};

	// �������� ������
	struct StringParameter : public Parameter
	{
		std::string defVal;
		void Init(
			const char* name, 
			const char* description="", 
			const char* defVal="",
			int flags=0
			)
		{
			this->name = name;
			this->description = description;
			this->defVal = defVal;
			this->flags = flags;
		}

		virtual cls::enParamType getType()
		{
			return cls::PT_STRING;
		};
		virtual cls::Param getDefVal()
		{
			return cls::PS(defVal.c_str());
		};
	};

	// �������� ��������
	struct ManifoldParameter : public ParameterStruct
	{
		PointParameter Pt;
		VectorParameter dPdu;
		VectorParameter dPdv;

		void Init(
			const char* name, 
			const char* namePt, 
			const char* namedPdu, 
			const char* namedPdv)
		{
			this->name = name;
			Pt.Init(namePt);
			dPdu.Init(namedPdu);
			dPdv.Init(namedPdv);
		}
		// ��������� ���������
		virtual void subParameters(std::vector<Parameter*>& list)
		{
			list.resize(3);
			list[0] = &Pt;
			list[1] = &dPdu;
			list[2] = &dPdv;
		};
	};

	// �������� �������� Surface �������
	struct SurfaceOutput : public shn::ParameterStruct
	{
		shn::ColorParameter CI;
		shn::ColorParameter OI;
		void Init()
		{
			CI.Init("CI");
			OI.Init("OI");
		}
		// ��������� ���������
		virtual void subParameters(std::vector<Parameter*>& list)
		{
			list.resize(2);
			list[0] = &CI;
			list[1] = &OI;
		};
	};

	// �������� �������� Displacement �������
	struct DisplaceOutput : public shn::ParameterStruct
	{
		shn::FloatParameter displacebound;
		shn::StringParameter displacespace;

		void Init()
		{
//			displacebound.Init("CI");
//			displacespace.Init("OI");
		}
		// ��������� ���������
		virtual void subParameters(std::vector<Parameter*>& list)
		{
//			list.resize(2);
//			list[0] = &CI;
//			list[1] = &OI;
		};
	};
}
#include "inl/IShaderParameter.hpp"