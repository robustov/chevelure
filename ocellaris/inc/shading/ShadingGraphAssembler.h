#pragma once

#include "ocellaris/IExportContext.h"
#include "shading/IShaderTemplate.h"
#include "shading/IShadingGraphNode.h"
#include "Util/DllProcedure.h"
#include "Util/misc_create_directory.h"
#include "shading/ShadingGraphNodeImpl.h"

class ShadingGraphAssembler
{
public:
	// ��������� ������
	static std::string BuildGraph(
		cls::IExportContext* context,
		ShadingGraphNodeImpl& root,
		const char* shaderName
		);
	// ������������� ������
	static std::string Compile(
		cls::IExportContext* context,
		const char* infilename, 
		const char* outfilename=NULL,
		const char* compilestring=NULL
		);
};

#include "inl/ShadingGraphAssembler.hpp"