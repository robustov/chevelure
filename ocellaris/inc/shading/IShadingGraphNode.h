#pragma once
#include "IShaderParameter.h"

namespace shn
{
	//! @ingroup shading_group 
	//! \brief IShadingGraphNode
	//! 
	//! ����� ��� ���������� ��������� ����� shading network
	struct IShadingGraphNode
	{
		// ����������� �������� ���������
		// ���������� �� ���������� IShaderTemplate::Inputs()
		virtual void RegisterInput(shn::Parameter& param)=0;
		// ����������� ��������� ���������
		// ���������� �� ���������� IShaderTemplate::Inputs()
//		virtual void RegisterOutput(shn::Parameter& param)=0;

		// �������� #include ��������� � ������ �������
		virtual void Include(const char* format, ...)=0;
		// �������� ������ � �������
		virtual void Add(const char* format, ...)=0;
		virtual void output(const char* format, ...)=0;
		// �������� ������ �����������
		virtual void Comment(const char* format, ...)=0;

		// ��� �������� ���������
//		virtual const char* getVarName(shn::Parameter& param)=0;

		// �������� ����������
		virtual void getValue(const EnumParameter& param, int& value)=0;

	};
}