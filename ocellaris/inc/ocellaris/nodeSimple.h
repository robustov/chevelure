#pragma once
#include "INode.h"
#include <maya/MDagPath.h>
#include <maya/MObject.h>

namespace cls
{
	//! @ingroup impl_group 
	//! \brief proxy ���������� INode ���������� ��� ������ � ������ INode
	//! 
	struct nodeSimple : public INode
	{
		nodeSimple(
			const char* name
			)
		{
			MObject obj;
			nodeFromName(name, obj);

			this->path = MDagPath::getAPathTo(obj);
			this->obj = this->path.node();
		}

		virtual void Release()
		{
			delete this;
		}
		virtual MDagPath getPath()
		{
			return path;
		}
		virtual MObject getObject()
		{
			return obj;
		}
		virtual void setProxyObject(MObject obj)
		{
			this->obj = obj;
		}

		virtual int getId()
		{
			return *(int*)(&obj);
		}
		// �������� ���������
		virtual void AddGenerator(cls::IGenerator* gen)
		{
		}
		// ������� ����������
		virtual void Dump(FILE* file)
		{
		};

	protected:
		MDagPath path;
		MObject obj;
	};
}