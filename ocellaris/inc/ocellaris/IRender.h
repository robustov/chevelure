#pragma once

#include "Math/Math.h"
#include <limits>
#include <set>
#include "Math\Box.h"
#include "IOcellaris.h"
#include "IParameter.h"
#include "IProcedural.h"
#include "IFilter.h"

namespace cls
{
	// ������������ ����
	const float nonBlurValue = -std::numeric_limits<float>::max();

	//! @ingroup ocellaris_group 
	//! \brief IRender
	//! 
	//! ������� ����� ���������� ��������� 
	//! ���������� ��������� ����������
	struct IRender
	{
		// ��������� � ������ ������ (����������� � renderBaseCacheImpl)
		virtual void Render(IRender* render) const{}
		// ��������� ���������� �������� � ������ ������ (����������� � renderCacheImpl)
		virtual void RenderGlobalAttributes(IRender* render) const{}

		// ��� �������
		virtual const char* renderType(
			){return "";};

		// ����������� ������� ���������
		// �� �������� � ocs �����
		virtual bool RegisterProcedural(
			const char* procName, 
			IProcedural* procedural
			)=0;

		/////////////////////////////////////////////////////
		//! �����������

		//! ��������� ���������� (������ ��� ��������� �������)
		virtual void comment(
			const char* text
			)=0;

		/////////////////////////////////////////////////////
		//! ���������
		inline void Parameter(const char *name, const Param& t){ParameterV(name, t);}
		inline void Parameter(const std::string& name, const Param& t){ParameterV(name.c_str(), t);}
		inline void Parameter(const char *name, enParamType t, bool val){this->ParameterV(name, P<bool>(val));}
		inline void Parameter(const char *name, enParamType t, int val){this->ParameterV(name, P<int>(val));}
		inline void Parameter(const char *name, enParamType t, float val){this->ParameterV(name, P<float>(val));}
//		inline void Parameter(const char *name, enParamType t, double val){this->ParameterV(name, P<double>(val));}
		inline void Parameter(const char *name, enParamType t, const char *val){this->ParameterV(name, PS(val));}
		inline void Parameter(const char *name, enParamType t, const std::string& val){this->ParameterV(name, PS(val.c_str()));}
		inline void Parameter(const char *name, enParamType t, const Math::Vec3f& val){this->ParameterV(name, P<Math::Vec3f>(val, t));}
		inline void Parameter(const char *name, enParamType t, const Math::Box3f& val){this->ParameterV(name, P<Math::Box3f>(val, t));}
		inline void Parameter(const char *name, enParamType t, const Math::Matrix4f& val){this->ParameterV(name, P<Math::Matrix4f>(val, t));}

		/// �������� � �������� ����
		inline void Parameter(const char *name, enParamType type, Param& t){t->respecifyType(type);ParameterV(name, t);}
		inline void Parameter(const std::string& name, enParamType type, Param& t){t->respecifyType(type);ParameterV(name.c_str(), t);}
		inline void Parameter(const char *typedname, bool val){this->Parameter(typedname, P<bool>(val));}
		inline void Parameter(const char *typedname, int val){this->Parameter(typedname, P<int>(val));}
		inline void Parameter(const char *typedname, float val){this->Parameter(typedname, P<float>(val));}
//		inline void Parameter(const char *typedname, double val){this->Parameter(typedname, P<double>(val));}
		inline void Parameter(const char *typedname, const char *val){this->Parameter(typedname, PS(val));}
		inline void Parameter(const char *typedname, const std::string& val){this->Parameter(typedname, PS(val.c_str()));}
		inline void Parameter(const char *typedname, const Math::Vec2f& val){this->Parameter(typedname, P<Math::Vec2f>(val));}
		inline void Parameter(const char *typedname, const Math::Vec3f& val){this->Parameter(typedname, P<Math::Vec3f>(val));}
		inline void Parameter(const char *typedname, const Math::Box3f& val){this->Parameter(typedname, P<Math::Box3f>(val));}
		inline void Parameter(const char *typedname, const Math::Matrix4f& val){this->Parameter(typedname, P<Math::Matrix4f>(val));}
		inline void Parameter(const char *typedname, const std::vector<Math::Vec2f>& val, enParamInterpolation interp=PI_CONSTANT){this->Parameter(typedname, PA<Math::Vec2f>(interp, val));}
		inline void Parameter(const char *typedname, const std::vector<Math::Vec3f>& val, enParamInterpolation interp=PI_CONSTANT){this->Parameter(typedname, PA<Math::Vec3f>(interp, val));}
		inline void Parameter(const char *typedname, const std::vector<int>& val, enParamInterpolation interp=PI_CONSTANT){this->Parameter(typedname, PA<int>(interp, val));}
		inline void Parameter(const char *typedname, const std::set<int>& val, enParamInterpolation interp=PI_CONSTANT);
		inline void Parameter(const char *typedname, const std::vector<std::string>& val){this->Parameter(typedname, PSA(val));}

		/////////////////////////////////////////////////////
		//! ������ ���������
		inline Param GetParameter(const char *name){return GetParameterV(name);};
		inline bool GetParameter(const char *name, bool& val);
		inline bool GetParameter(const char *name, int& val);
		inline bool GetParameter(const char *name, float& val);
		inline bool GetParameter(const char *name, Math::Vec2f& val);
		inline bool GetParameter(const char *name, Math::Vec3f& val);
		inline bool GetParameter(const char *name, Math::Box3f& val);
		inline bool GetParameter(const char *name, const char* &val);
		inline bool GetParameter(const char *name, std::string& val);
		inline bool GetParameter(const char *name, Math::Matrix4f& val);
		inline bool GetParameter(const char *name, P<float>& val);
		inline bool GetParameter(const char *name, P<int>& val);
		inline bool GetParameter(const char *name, PS& val);
		inline bool GetParameter(const char *name, PA<Math::Vec3f>& val);
		inline bool GetParameter(const char *name, PA<float>& val);
		inline bool GetParameter(const char *name, PA<int>& val);
		inline bool GetParameter(const char *name, PA<Math::Matrix4f>& val);
		inline bool GetParameter(const char *name, std::vector<Math::Vec2f>& val);
		inline bool GetParameter(const char *name, std::vector<Math::Vec3f>& val);
		inline bool GetParameter(const char *name, PSA& val);
		inline bool GetParameter(const char *name, std::vector<std::string>& val);
		inline bool GetParameter(const char *name, std::vector<int>& val);
		inline bool GetParameter(const char *name, std::set<int>& val);


		/////////////////////////////////////////////////////
		//! ���������

		//! ��������
		virtual void ParameterV(
			const char *name,		//!< ��� ���������
			const Param& t
			)=0;
		//! �������� �� ��������
		virtual void ParameterFromAttribute(
			const char *paramname,		//!< ��� ���������
			const char *attrname		//!< ��� �������� �� �������� ����� ������
			)=0;
		//! �������� � �������
		virtual void ParameterToAttribute(
			const char *paramname,		//!< ��� ��������� �� �������� ����� ������
			const char *attrname		//!< ��� �������� 
			)=0;
		//! �������� �� ��������
		virtual void AttributeFromAttribute(
			const char *dstname,	//!< ��� �������� ���������
			const char *srcname		//!< ��� �������� �� �������� ����� ������
			)=0;
		//! �������� ���������
		virtual Param GetParameterV(
			const char *name
			)=0;
		//! ������� ��������
		virtual bool RemoveParameter(
			const char *name
			)=0;
		//! ������ ����������
		virtual void GetParameterList(
			std::vector< std::pair< std::string, Param> >& list
			)=0;


		/////////////////////////////////////////////////////
		//! ���������
		// ���� ��� �������� ���������� � global:: - �� ��������� ����������!

		//! �������
		virtual void AttributeV(
			const char *name,		//!< ��� ���������
			const Param& t
			)=0;
		//! ������ ��������
		virtual Param GetAttributeV(
			const char *name
			)=0;
		//! ������ ���������
		virtual void GetAttributeList(
			std::vector< std::pair< std::string, Param> >& list
			)=0;

		/// Set an attribute in the graphics state, with name and explicit type
		inline void Attribute(const char *name, enParamType type, Param& t){t->respecifyType(type);AttributeV(name, t);}
		inline void Attribute(const char *typedname, enParamType t, bool val) {this->Attribute(typedname, P<bool>(val, t));}
		inline void Attribute(const char *typedname, enParamType t, int val) {this->Attribute(typedname, P<int>(val, t));}
		inline void Attribute(const char *typedname, enParamType t, float val) {this->Attribute(typedname, P<float>(val, t));}
//		inline void Attribute(const char *typedname, enParamType t, double val) {this->Attribute(typedname, t, P<double>(val));}
//		inline void Attribute(const char *typedname, enParamType t, const char *val) {this->Attribute(typedname, PS(val));}
//		inline void Attribute(const char *typedname, enParamType t, const std::string &val) {this->Attribute(typedname, t, PS(val.c_str()));}
		inline void Attribute(const char *typedname, enParamType t, const Math::Vec3f& val) {this->Attribute(typedname, P<Math::Vec3f>(val, t));}
//		inline void Attribute(const char *typedname, enParamType t, Math::Box3f& val){this->Attribute(typedname, P<Math::Box3f>(val, t));}
		inline void Attribute(const char *typedname, enParamType t, const Math::Matrix4f& val){this->Attribute(typedname, P<Math::Matrix4f>(val, t));}

		/// Set an attribute with type embedded in the name
		inline void Attribute(const char *name, const Param& t){AttributeV(name,t);}
		inline void Attribute(const char *typedname, bool val) {this->AttributeV(typedname, P<bool>(val));}
		inline void Attribute(const char *typedname, int val) {this->AttributeV(typedname, P<int>(val));}
		inline void Attribute(const char *typedname, float val) {this->AttributeV(typedname, P<float>(val));}
//		inline void Attribute(const char *typedname, double val) {this->AttributeV(typedname, P<double>(val));}
		inline void Attribute(const char *typedname, const char *val) {this->AttributeV(typedname, PS(val));}
		inline void Attribute(const char *typedname, const std::string &val) {this->Attribute(typedname, PS(val.c_str()));}
		inline void Attribute(const char *typedname, const Math::Vec2f& val) {this->AttributeV(typedname, P<Math::Vec2f>(val));}
		inline void Attribute(const char *typedname, const Math::Vec3f& val) {this->AttributeV(typedname, P<Math::Vec3f>(val));}
		inline void Attribute(const char *typedname, Math::Box3f& val){this->AttributeV(typedname, P<Math::Box3f>(val));}
		inline void Attribute(const char *typedname, const Math::Matrix4f& val){this->AttributeV(typedname, P<Math::Matrix4f>(val));}

		inline Param GetAttribute(const char *name){return GetAttributeV(name);}
		inline bool GetAttribute(const char *name, bool& val);
		inline bool GetAttribute(const char *name, float &val);
		inline bool GetAttribute(const char *name, int &val);
		inline bool GetAttribute(const char *name, std::string &val);
		inline bool GetAttribute(const char *name, Math::Box3f& val);
		inline bool GetAttribute(const char *name, Math::Matrix4f& val);
		inline bool GetAttribute(const char *name, PSA& val);
		inline bool GetAttribute(const char *name, PA<Math::Matrix4f>& val);

		//! ���������� �������
		virtual void GlobalAttribute(
			const char* name,		//!< ��� ���������
			const Param& t
			)=0;

		///////////////////////////////////////////////////////////////
		/// ��������� �� �������� ��������
		virtual void IfAttribute(
			const char* name, 
			const Param& t
			)=0;
		virtual void EndIfAttribute(
			)=0;

		///////////////////////////////////////////////////////////////
		/// PushAttributes/PopAttributes
		virtual void PushAttributes(void)=0;
		virtual void PopAttributes(void)=0;

		///////////////////////////////////////////////////////////////
		/// Transformations
		virtual void PushTransform(void)=0;
		virtual void PopTransform(void)=0;
		//! ��� ����� �������� - ������� ������� �� ��������� "transform"
		virtual void SetTransform()=0;
		//! � ������ �������� - ������� ������� �� ��������� "transform"
		virtual void AppendTransform()=0;
		//! PinTransform
		virtual void PinTransform()=0;
		//! AppendTransform
		void AppendTransform(const Math::Matrix4f& M);
		void SetTransform(const Math::Matrix4f& M);
		void SetTransform(const char *spacename);

		///////////////////////////////////////////////////////////////
		/// RenderAttributes - ��� ����������: ������ Begin/End Attributes
		/// PushRenderAttributes/PopRenderAttributes
		virtual void PushRenderAttributes(void)=0;
		virtual void PopRenderAttributes(void)=0;

		///////////////////////////////////////////////////////////////
		/// Filters
		/// � IFilter ��� ��������� ������ IRender::SystemCall ����� �������������� ������ OnSystemCall
		virtual void Filter(IFilter* filter)=0;
		virtual void Filter(const char* filter)=0;				// dllname@procname


		///////////////////////////////////////////////////////////////
		/// Motion blur 
		/// time - �������� � �������� �� ������� ���. �����
		virtual void MotionBegin()=0;
		virtual void MotionEnd()=0;
		virtual void MotionPhaseBegin(float time)=0;
		virtual void MotionPhaseEnd()=0;

		// ������������ ������� ������ ����� (����� �� ���������!!!)
		// ����� �������� nonBlurValue
		virtual bool SetCurrentMotionPhase(float time)=0;


		///////////////////////////////////////////////////////////////
		/// Shaders
		virtual void Shader(const char* shadertype)=0;
		virtual void Light()=0;

		void Shader(
			const char *shadertype, 
			const char *shadername,
			const char *layername=NULL);
		void Light(
			const char *lightid, 
			const char *shadername,
			const char *layername=NULL);
/*/
		virtual void ShaderGroupBegin (void) {}
		virtual void ShaderGroupEnd (void) {}
		virtual void ConnectShaders(
			const char *srclayer, 
			const char *srcparam, 
			const char *dstlayer, 
			const char *dstparam) {}
		virtual void LightSwitch(const char *lightid, bool on) {}
/*/


		///////////////////////////////////////////////////////////////
		/// ����� ��������� ���������� ������� ������
		virtual void SystemCall(enSystemCall call)=0;
		/// ����� ��������� ���������� ������� ������
		void SystemCall(const char* callname);

		/// Geometry
		void Points(PA<Math::Vec3f>& P);
		/// 1-D prims - lines, curves, hair
		void Curves(const char *interp, const char *wrap, PA<int>& nverts);
		/// 2-D prims - rectangular patches (NURBS, bicubics, bilinears), and
		/// indexed face meshes (polys, polyhedra, subdivs)
		void Patch (const char *interp, int nu, int nv);
		void Patch (int nu, int uorder, const float *uknot,
							float umin, float umax,
							int nv, int vorder, const float *vknot,
							float vmin, float vmax);
		/*/
		/// TrimCurve
		void TrimCurve(int nloops, const int *ncurves, const int *n,
								const int *order, const float *knot,
								const float *min, const float *max,
								const float *uvw);
		/*/
		/// Mesh 
		void Mesh(
			const char *interp,		//!< ��� ������������
			PA<int>& loops,			//!< �����?
			PA<int>& nverts,		//!< ����� ��������� � �������� (size = polygoncount)
			PA<int>& verts			//!< ������� ��������� ��������� (size = facevertcount)
			);
		/// Sphere
		void Sphere(
			float radius, 
			float zmin, 
			float zmax,
			float thetamax=360);
		/// Sphere
		void Sphere (float radius);
		/// Blobby
		void Blobby(PA<Math::Matrix4f>& ellipsoids);

		/////////////////////////////////////////////////////////////////
		/// ���������

		/// ���������� ��������� (����������� ������ � renderFileBaseImpl!!!)
		virtual IRender* DeclareProcBegin(
			const char* name
			)=0;
		/// ����� ���������� ���������
		virtual void DeclareProcEnd(
			)=0;

		/// Non Render Procedural (���������������� �����)
		/// ���� ����� ������ �������� � ����!!!
		/// ��������� ����� ����� �������� ������ �-��� ��� ������/������ ���������!!!
		///                                    (��� ��� ����������� �������� �����)
		virtual void Call( 
			IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
			)=0;
		/// Non Render Procedural (���������������� �����)
		/// ���� ����� ����� �������� � ����
		/// ��������� ����� ����� �������� ������ �-��� ��� ������/������ ���������!!!
		///                                    (��� ��� ����������� �������� �����)
		virtual void Call( 
			const char*	procname			//!< ��� ��������� � DeclareProcBegin ��� dllname@procname
			)=0;

		/// Render Procedural ( � ��� ����� ���������� �����)
		/// ��� ���������� ���������� ����� IProcedural::Render()
		/// ���� ����� ������ �������� � ����!!! (��� �����???)
		/// ��������: Box, int id, bool delay
		/// !!!��������� ������ ����������� � �������� ����� ��������������!!!
		virtual void RenderCall( 
			IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
			)=0;
		/// Render Procedural ( � ��� ����� ���������� �����)
		/// ���� ����� ����� �������� � ����
		/// !!!��������� ������ ����������� � �������� ����� ��������������!!!
		virtual void RenderCall( 
			const char*	procname			//!< ��� ��������� � DeclareProcBegin ��� dllname@procname
			)=0;

		/// ���������� �����
		/// ������ �����
		virtual void Dump( 
			)=0;
	};
}
#include "serializeOcs.h"
#include "inl/IRender.hpp"

