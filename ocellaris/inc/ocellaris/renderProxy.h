#pragma once
#include "IRender.h"
#include <map>
#include <list>
#include <vector>
#include <string>


namespace cls
{
	//! @ingroup impl_group 
	//! \brief proxy ���������� IRender ���������� ��� ������ � ������ ������
	//! 
	//! ������������ ��� ������� ����� � ��������� �����������
	struct renderProxy : public IRender
	{
		renderProxy(
			IRender* render
			);
		void init(
			IRender* render
			);

		// ����������� ������� ���������
		virtual bool RegisterProcedural(
			const char* procName, 
			IProcedural* procedural
			);

		virtual void comment(
			const char* text
			);

		//!@name Set a parameter of the next shader, primitive, camera, or output
		//@{ 

		//! ��������
		virtual void ParameterV(
			const char *name,		//!< ��� ���������
			const Param& t
			);
		//! ������ ���������
		virtual Param GetParameterV(
			const char *name
			);
		virtual bool RemoveParameter(
			const char *name
			);
		//! �������� �� ��������
		void ParameterFromAttribute(
			const char *paramname,		//!< ��� ���������
			const char *attrname		//!< ��� �������� �� �������� ����� ������
			);
		//! �������� � �������
		virtual void ParameterToAttribute(
			const char *paramname,		//!< ��� ��������� �� �������� ����� ������
			const char *attrname		//!< ��� �������� 
			);
		//! ������� �� ��������
		virtual void AttributeFromAttribute(
			const char *dstname,	//!< ��� �������� ���������
			const char *srcname		//!< ��� �������� �� �������� ����� ������
			);
		virtual void GetParameterList(
			std::vector< std::pair< std::string, Param> >& list);
		//@}


		//! �������
		virtual void AttributeV(
			const char *name,		//!< ��� ���������
			const Param& t
			);
		virtual void GlobalAttribute(
			const char *name,		//!< ��� ���������
			const Param& t
			);
		//! ������ ��������
		virtual Param GetAttributeV(
			const char *name
			);
		virtual void GetAttributeList(
			std::vector< std::pair< std::string, Param> >& list
			);

		///////////////////////////////////////////////////////////////
		/// ��������� �� �������� ��������
		virtual void IfAttribute(const char* name, const Param& t);
		virtual void EndIfAttribute();

		//@{ 
		/// PushAttributes/PopAttributes
		virtual void PushAttributes (void);
		virtual void PopAttributes (void);
		virtual void SaveAttributes (const char *name, const char *attrs=NULL) {}
		virtual void RestoreAttributes(const char *name, const char *attrs=NULL) {}
		//@}

		//@{ 
		/// Transformations
		virtual void PushTransform (void);
		virtual void PopTransform (void);
		virtual void SetTransform ();
		virtual void AppendTransform ();
		virtual void PinTransform ();
		//@}

		///////////////////////////////////////////////////////////////
		/// empty implementation
		virtual void PushRenderAttributes(void);
		virtual void PopRenderAttributes(void);

		//@{ 
		/// Motion blur
		virtual void MotionBegin();
		virtual void MotionEnd();
		virtual void MotionPhaseBegin(float time);
		virtual void MotionPhaseEnd();
		// ������������ ������� ������ ����� (����� �� ���������!!!)
		virtual bool SetCurrentMotionPhase(float time);
		//@}

		virtual void Filter(IFilter* filter);
		virtual void Filter(const char* filter);

		//@{ 
		/// Shaders
		virtual void Shader(const char* shadertype);
		virtual void Light();
		//@}

		/// ����� ��������� ���������� ������� ������
		virtual void SystemCall(
			enSystemCall call
			);


		/// ���������� ���������
		virtual IRender* DeclareProcBegin(
			const char* name
			);
		virtual void DeclareProcEnd(
			);

		/// Procedural (���������������� �����)
		virtual void Call( 
			IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
			);
		virtual void Call( 
			const char*	procname			//!< ��� ��������� � DeclareProcBegin
			);


		/// Procedural (��������� �����)
		/// ��� ���������� ���������� ����� IProcedural::Render()
		/// ���� ����� ������ �������� � ����!!! (��� �����???)
		virtual void RenderCall( 
			IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
			);
		virtual void RenderCall( 
			const char*	procname			//!< ��� ��������� � DeclareProcBegin
			);

		/// ���������� �����
		/// ������ �����
		virtual void Dump( 
			);

	protected:

		IRender* render;

	};
}

namespace cls
{
	inline renderProxy::renderProxy(
		IRender* render
		)
	{
		this->render = render;
	}
	inline void renderProxy::init(
		IRender* render
		)
	{
		this->render = render;
	}

	// ����������� ������� ���������
	inline bool renderProxy::RegisterProcedural(
		const char* procName, 
		IProcedural* procedural
		)
	{
		return render->RegisterProcedural(
			procName, 
			procedural);
	}

	inline void renderProxy::comment(
		const char* text
		)
	{
		render->comment(text);
	}

	inline void renderProxy::IfAttribute(const char* name, const Param& t)
	{
		render->IfAttribute(name, t);
	}
	inline void renderProxy::EndIfAttribute()
	{
		render->EndIfAttribute();
	}

	inline void renderProxy::PushAttributes(void) 
	{
		render->PushAttributes();
	}
	inline void renderProxy::PopAttributes(void) 
	{
		render->PopAttributes();
	}

	inline void renderProxy::PushTransform (void)
	{
		render->PushTransform();
	}
	inline void renderProxy::PopTransform (void)
	{
		render->PopTransform();
	}
	inline void renderProxy::SetTransform ()
	{
		render->SetTransform();
	}
	inline void renderProxy::AppendTransform ()
	{
		render->AppendTransform();
	}
	inline void renderProxy::PinTransform()
	{
		render->PinTransform();
	}

	///////////////////////////////////////////////////////////////
	/// empty implementation
	inline void renderProxy::PushRenderAttributes(void)
	{
		render->PushRenderAttributes();
	}
	inline void renderProxy::PopRenderAttributes(void)
	{
		render->PopRenderAttributes();
	}

	//@{ 
	/// Motion blur
	inline void renderProxy::MotionBegin()
	{
		render->MotionBegin();
	}
	inline void renderProxy::MotionEnd()
	{
		render->MotionEnd();
	}
	inline void renderProxy::MotionPhaseBegin(float time)
	{
		render->MotionPhaseBegin(time);
	}
	inline void renderProxy::MotionPhaseEnd()
	{
		render->MotionPhaseEnd();
	}
	inline bool renderProxy::SetCurrentMotionPhase(float time)
	{
		return render->SetCurrentMotionPhase(time);
	}

	//@}


	//! ��������
	inline void renderProxy::ParameterV(
		const char *name,		//!< ��� ���������
		const Param& t			//!< ��������
		)
	{
		render->ParameterV(name, t);
	}
	//! ������ ���������
	inline Param renderProxy::GetParameterV(
		const char *name
		)
	{
		return render->GetParameterV(name);
	}
	inline bool renderProxy::RemoveParameter(
		const char *name
		)
	{
		return render->RemoveParameter(name);
	}
	//! �������� �� ��������
	inline void renderProxy::ParameterFromAttribute(
		const char *paramname,		//!< ��� ���������
		const char *attrname		//!< ��� �������� �� �������� ����� ������
		)
	{
		render->ParameterFromAttribute(paramname, attrname);
	}
	//! �������� �� ��������
	inline void renderProxy::ParameterToAttribute(
		const char *paramname,		//!< ��� ���������
		const char *attrname		//!< ��� �������� �� �������� ����� ������
		)
	{
		render->ParameterToAttribute(paramname, attrname);
	}
	//! ������� �� ��������
	inline void renderProxy::AttributeFromAttribute(
		const char *dstname,	//!< ��� �������� ���������
		const char *srcname		//!< ��� �������� �� �������� ����� ������
		)
	{
		render->AttributeFromAttribute(dstname, srcname);
	}

	//! ������ ����������
	inline void renderProxy::GetParameterList(
		std::vector< std::pair< std::string, Param> >& list)
	{
		render->GetParameterList(list);
	}

	//! �������
	inline void renderProxy::AttributeV(
		const char *name,		//!< ��� ���������
		const Param& t
		)
	{
		render->AttributeV(name, t);
	}
	inline void renderProxy::GlobalAttribute(
		const char *name,		//!< ��� ���������
		const Param& t
		)
	{
		render->GlobalAttribute(name, t);
	}
	//! ������ ��������
	inline Param renderProxy::GetAttributeV(
		const char *name
		)
	{
		return render->GetAttributeV(name);
	}

	inline void renderProxy::GetAttributeList(
		std::vector< std::pair< std::string, Param> >& list
		)
	{
		render->GetAttributeList(list);
	}

	inline void renderProxy::Shader(const char *shadertype)
	{
		render->Shader(shadertype);
	}

	inline void renderProxy::Light()
	{
		render->Light();
	}

	inline void renderProxy::Filter(IFilter* filter)
	{
		render->Filter(filter);
	}
	inline void renderProxy::Filter(const char* filter)
	{
		render->Filter(filter);
	}

	/// ����� ��������� ���������� ������� ������
	inline void renderProxy::SystemCall(
		enSystemCall call
		)
	{
		render->SystemCall(call);
	}

	/// ���������� ���������
	inline IRender* renderProxy::DeclareProcBegin(
		const char* name
		)
	{
		return render->DeclareProcBegin(name);
	}
	inline void renderProxy::DeclareProcEnd(
		)
	{
		render->DeclareProcEnd();
	}

	/// Call (���������������� �����)
	inline void renderProxy::Call( 
		IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
		)
	{
		render->Call(pProcedural);
	}
	/// Call (���������������� �����)
	inline void renderProxy::Call( 
		const char*	procname			//!< ��� ��������� � DeclareProcBegin
		)
	{
		render->Call(procname);
	}
	/// Procedural (���������������� �����)
	inline void renderProxy::RenderCall( 
		IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
		)
	{
		render->RenderCall(pProcedural);
	}

	/// Procedural (���������������� �����)
	inline void renderProxy::RenderCall( 
		const char*	procname			//!< ����� ������� IProcedural::Render()
		)
	{
		render->RenderCall(procname);
	}
	/// ���������� �����
	/// ������ �����
	inline void renderProxy::Dump( 
		)
	{
		render->Dump();
	}

}