#pragma once

#include "IOcellaris.h"

namespace cls
{
	//! @ingroup plugin_group
	//! \brief IShadingNodeGenerator
	//! 
	//! ����� ��� ������ �������� � Shading Network ��. IShadingNodeGenerator
	struct IShaderAssembler
	{
		// ���������� �������� ���������
		virtual bool ExternParameter(
			const char* variable, 
			cls::Param defval, 
			cls::enParamType type = cls::PT_UNKNOWN,
			cls::enParamInterpolation interp = cls::PI_VARYING
			)=0;
		// ���������� �������� ���������
		virtual bool InputParameter(
			MPlug mayaParameter, 
			const char* shaderVariable, 
			cls::enParamType type = cls::PT_UNKNOWN,
			cls::enParamInterpolation interp = cls::PI_VARYING
			)=0;
		// ���������� �������� ���������
		virtual bool InputParameter(
			const char* mayaParameter, 
			const char* shaderVariable, 
			cls::enParamType type = cls::PT_UNKNOWN,
			cls::enParamInterpolation interp = cls::PI_VARYING
			)=0;
		// ���������� �������� ���������
		bool InputParameter(
			const char* mayaNshaderVariable, 
			cls::enParamType type = cls::PT_UNKNOWN,
			cls::enParamInterpolation interp = cls::PI_VARYING
			)
		{
			return InputParameter(mayaNshaderVariable, mayaNshaderVariable, type, interp);
		}
		// ���������� �������� ���������
		bool InputParameter(
			MObject& attribute, 
			cls::enParamType type = cls::PT_UNKNOWN,
			cls::enParamInterpolation interp = cls::PI_VARYING
			)
		{
			MFnAttribute attr(attribute);
			const char* mayaNshaderVariable = attr.name().asChar();
			return InputParameter(mayaNshaderVariable, mayaNshaderVariable, type, interp);
		}
		/*/
		// ���������� �������� ���������
		virtual bool InputManifold(
			const char* mayaParameter, 
			const char* Q, 
			const char* dQu, 
			const char* dQv 
			)=0;
		/*/
		// ���������� ��������� ���������
		virtual bool OutputParameter(
			const char* shaderVariable,
			cls::enParamType type 
			)=0;
		/*/
		// ���������� ��������� ���������
		virtual bool OutputManifold(
			const char* Q, 
			const char* dQu, 
			const char* dQv 
			)=0;
		/*/

		// �������� #include ��������� � ������ �������
		virtual void Include(const char* format, ...)=0;

		// �������� ������ � �������
		virtual void Add(const char* format)=0;
		virtual void AddV(const char* format, ...)=0;
	
	};
}
