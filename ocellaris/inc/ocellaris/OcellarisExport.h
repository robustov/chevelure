#pragma once

#ifdef OCELLARISEXP_EXPORTS
#define OCELLARISEXPORT_API __declspec(dllexport)
#else
#define OCELLARISEXPORT_API __declspec(dllimport)
#endif

#include "ocellaris/IRender.h"
#include "ocellaris/IGenerator.h"
#include "ocellaris/IExportContext.h"

#include "ocellaris/IGenerator.h"
#include "ocellaris/INodeGenerator.h"
#include "ocellaris/IAttributeGenerator.h"
#include "ocellaris/IGeometryGenerator.h"
#include "ocellaris/IShaderGenerator.h"
#include "ocellaris/ITransformGenerator.h"
#include "ocellaris/INodeFilter.h"
#include "ocellaris/IDeformGenerator.h"
#include "ocellaris/IPassGenerator.h"
#include "ocellaris/IProxyGenerator.h"
#include "ocellaris/IShadingNodeGenerator.h"
#include "ocellaris/IBlurGenerator.h"
//#include "ocellaris/ChiefGenerator.h"
#include "ocellaris/INode.h"
#include "ocellaris/IOcellaris.h"

namespace cls
{
	struct IRender;
	struct IGenerator;
	struct INode;
	struct IExportContext;
	struct IGeometryGenerator;
}
struct ChiefGenerator;
class MObject;
class MTime;
class MTypeId;
class MSelectionList;

const unsigned ocellarisTypeIdBase = 0xda0c0;

//! @addtogroup plugin_group 
//! @{

// ���� � dll
OCELLARISEXPORT_API const char* ocGetPath( 
	);


//! ����� ExportContext
OCELLARISEXPORT_API cls::IExportContext* ocNewExportContext( 
	);
OCELLARISEXPORT_API void ocDeleteExportContext( 
	cls::IExportContext* context
	);


//! 
//! ���������� ������ �������������� �������� INode 
//! ��������� �������� ����
OCELLARISEXPORT_API cls::INode* ocNodeTree( 
	MDagPath path,					//<! �������� ������
	ChiefGenerator& chief,			//<! �������� ����������� �� ���������
	cls::IExportContext* context	//<! �������� ��������
	);
//! ��������� INode ��� ������ �������
OCELLARISEXPORT_API cls::INode* ocNodeTree_Single( 
	MObject node,					//<! ������
	ChiefGenerator& chief,			//<! �������� ����������� �� ���������
	cls::IExportContext* context	//<! �������� ��������
	);

//! ���������� ������ �������������� �������� INode 
//! ��������� ������ ���� set
OCELLARISEXPORT_API cls::INode* ocNodeTree_fromSet( 
	MObject set,					//<! �������� ������
	ChiefGenerator& chief,			//<! �������� ����������� �� ���������
	cls::IExportContext* context	//<! �������� ��������
	);
//! ���������� ������ �������������� �������� INode 
//! ��������� ������ ��� path
OCELLARISEXPORT_API cls::INode* ocNodeTree_fromList( 
	std::list<MDagPath>& list,				//<! ���
	ChiefGenerator& chief,					//<! �������� ����������� �� ���������
	cls::ITransformGenerator* generator,	//<! ��������� �������� �����������
	cls::IExportContext* context			//<! �������� ��������
	);
//! ���������� ������ �������������� �������� INode 
//! ��������� ������ MSelectionList
OCELLARISEXPORT_API cls::INode* ocNodeTree_fromList( 
	MSelectionList& list,					//<! ���
	ChiefGenerator& chief,					//<! �������� ����������� �� ���������
	cls::ITransformGenerator* generator,	//<! ��������� �������� �����������
	cls::IExportContext* context			//<! �������� ��������
	);

OCELLARISEXPORT_API cls::INode* ocNodeTreeType(
	int type,
	ChiefGenerator& chief,			//<! �������� ����������� �� ���������
	cls::IExportContext* context	//<! �������� ��������
	);

//! ����������� ��������� ��� ��� � ocsCache ��������� (ocGeometryGenerator)
OCELLARISEXPORT_API cls::IGeometryGenerator* ocGetGeometryGenerator();

//! ����������� ����������
void OCELLARISEXPORT_API registerGenerator(MFn::Type t, cls::IGenerator* gen);
void OCELLARISEXPORT_API registerGenerator(MTypeId, cls::IGenerator* );
//! ����������� ����������
void OCELLARISEXPORT_API unregisterGenerator(MFn::Type t, cls::IGenerator* gen);
void OCELLARISEXPORT_API unregisterGenerator(MTypeId, cls::IGenerator* );

//! ��� �����������
enum enGeneratorsType
{
	OCGT_SCENE		= 0,
	OCGT_LIGHTS		= 1,
	OCGT_SHADERS	= 2,
};
//! ���������� �� ���������
void OCELLARISEXPORT_API ocellarisDefaultGenerators(ChiefGenerator& chef, const char* options);

//! ��������!!!
void OCELLARISEXPORT_API ocellarisDefaultGenerators(enGeneratorsType gtype, ChiefGenerator& chief, cls::IExportContext* context);

// ��������!(��� ��������!!!)
//void OCELLARISEXPORT_API ocExportBlurTree(MObject obj, cls::IRender* render, Math::Box3f& box, std::vector<MTime> times, cls::IExportContext* context);

//! ��� �������� � ������� ocellaris
cls::enParamType OCELLARISEXPORT_API ocExport_GetAttrType(MObject obj, MObject attr);
//! ��� �������� � ������� ocellaris
cls::enParamType OCELLARISEXPORT_API ocExport_GetAttrArrayType(MObject obj, MObject attr);

//! ���������� �� ��������� � ocs ��������
bool OCELLARISEXPORT_API ocExport_IsCompatibleType(MObject obj, MObject attr, cls::enParamType type);

//! �������� �������
MObject OCELLARISEXPORT_API ocExport_AddAttrType(MObject obj, const char* name, cls::enParamType type, const cls::Param& defval);

//! ������ �������� ��������
void OCELLARISEXPORT_API ocExport_SetAttrValue(MObject obj, const char* attr, const cls::Param& param);
//! ������ �������� ��������
void OCELLARISEXPORT_API ocExport_SetAttrValue(MObject obj, MObject attr, const cls::Param& param);
//! ������ �������� occelaris
MObject OCELLARISEXPORT_API getExternShaderNode(MObject& obj);

//! �������� �������� ��������
cls::Param OCELLARISEXPORT_API ocExport_GetAttrValue(MObject obj, MObject attr);
cls::Param OCELLARISEXPORT_API ocExport_GetAttrValue(MObject obj, const char* attr);
cls::Param OCELLARISEXPORT_API ocExport_GetAttrValue(MPlug plug);

//! �������� �������� �������� �������
cls::Param OCELLARISEXPORT_API ocExport_GetAttrArrayValue(MObject obj, MObject attr);
//! �������� �������� �������� �������
cls::Param OCELLARISEXPORT_API ocExport_GetAttrArrayValue(MObject obj, const char* attr);

//! ��� ���. ����������
std::string OCELLARISEXPORT_API ocExport_GetCurrentDirectory();


//! ����������� �������� ��������
//! $O - ��� �������
//! $F - ����
//! %S - �����
std::string OCELLARISEXPORT_API ocExport_ApplySubParameters(const char* text);

//! @}

