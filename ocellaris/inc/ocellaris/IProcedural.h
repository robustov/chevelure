#pragma once
#include <string>

#include "IOcellaris.h"

namespace cls
{
	struct IRender;
	//! @ingroup ocellaris_group 
	//! \brief ������� ����� ��� ������� ��������. ������������ �
	//! ������� IRender::Call � IRender::RenderCall
	//! 
	//! ������� ����� ��� �������� ���������� ��� ����������� ��. IRender::Call IRender::RenderCall
	struct IProcedural
	{
		/// ����� ��� ������ � ���� (�� IRender::Call � IRender::RenderCall � )
		//! ������ ������� true ����� ������ IRender::Call ����� ��������
		//! bNeedInitialize ������������ � true ���� ������� ����� ������������� �������������!!!
		virtual bool Export(
			std::string& dllname, 
			std::string& procname, 
			bool& bNeedInitialize
			){return false;};

		// ������ ���������� ���������
		virtual void Init(
			cls::IRender* render
			){};

		/// ����� ��� ����������� (Call)
		virtual void Process(
			IRender* render
			){};
		/// ����� ��� ���������� (RenderCall)
		virtual void Render(
			IRender* render, 
			int motionBlurSamples, 
			float* motionBlurTimes
			){};


		virtual void addref()
		{
			ref++;
		}
		virtual void release()
		{
			--ref;
			if(ref < 0)
				delete this;
		}

	protected:
		int ref;

		IProcedural()
		{
			ref = 1;
		}
		virtual ~IProcedural()
		{
		}
	};

	typedef cls::IProcedural* (*PROCEDURAL_CREATOR)(cls::IRender* prman);

}
