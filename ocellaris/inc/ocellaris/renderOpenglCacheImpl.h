#pragma once
#include "IRender.h"
#include "renderImpl.h"
#include <map>
#include <list>
#include <vector>
#include <string>
#include "MathNgl/MathNgl.h"
#include "Math/Edge.h"

namespace cls
{
	//! @ingroup impl_group 
	//! \brief ���������� IRender ��� ��������� � GL �������, ������������ ����������� ���������, ��� ��������� ��������� ����������
	//! 
	//! ��� ���������� � GL � ������������ ���������
	struct renderOpenglCacheImpl : public renderImpl<TAttributeStack_minimal, TTransformStack_pure>
	{
		std::vector< Math::Vec3f> wireverts;
		std::vector< Math::Vec2i> wireinds;

		std::vector< Math::Vec3f> shadeverts;
		std::vector< Math::Vec3f> shadenorms;
		std::vector< Math::Vec2f> shadeuv;
		std::vector< int> shadeinds;

		renderOpenglCacheImpl(
			);

		// ��� �������
		virtual const char* renderType(
			){return "opengl";};

		void Render(
			bool bPoints, bool bWireframe, bool bShaded);

		void clear(
			);

		//@{ 
		/// Transformations
		virtual void PushTransform (void);
		virtual void PopTransform (void);
		virtual void SetTransform ();
		virtual void AppendTransform ();
		//@}

		/// ����� ��������� ���������� ������� ������
		virtual void SystemCall(
			enSystemCall call
			);


		/// Procedural (���������������� �����)
		virtual void Call( 
			IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
			);
		virtual void RenderCall( 
			IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
			);

		/*/
		// ������
		virtual void Filter( 
			IFilter* filter					//!< ��� ��������� � DeclareProcBegin ��� dllname@procname
			);
		virtual void Filter( 
			const char*	procname			//!< ��� ��������� � DeclareProcBegin ��� dllname@procname
			);
		/*/

		public:
			bool bPoints;
			int degeneration;

		protected:
			Math::Box3f box;
			std::list<Math::Matrix4f> transforms;

	protected:
		void addRenderCall(
			std::vector<Math::Vec3f>& outP,
			std::vector<Math::Vec2i>& vi);
		void addRenderCall(
			int fvcount,
			Math::Vec3f* outP,
			Math::Vec3f* N, 
			float* u, 
			float* v, 
			int* faces,
			int faceindexcount);


	protected:
		/// Geometry
		/// 0-D prims - point clouds
		void renderPoints (
			PA<Math::Vec3f>& P			//!< points
			);
		/// 1-D prims - lines, curves, hair
		void renderCurves (
			const char *interp, 
			const char *wrap,		//!< wrap
			PA<int>& nverts			//!< int[ncurves] ����� ��������� � ������
			);
		void renderCurves (int ncurves, int nvertspercurve, int order,
							const float *knot, float vmin, float vmax) {}
		/// 2-D prims - rectangular patches (NURBS, bicubics, bilinears), and
		/// indexed face meshes (polys, polyhedra, subdivs)
		void renderPatch (const char *interp, int nu, int nv) {}
		void renderPatch (int nu, int uorder, const float *uknot,
							float umin, float umax,
							int nv, int vorder, const float *vknot,
							float vmin, float vmax) {}
		/// TrimCurve
		void renderTrimCurve (int nloops, const int *ncurves, const int *n,
								const int *order, const float *knot,
								const float *min, const float *max,
								const float *uvw) {}
		/// Mesh 
		void renderMesh(
			const char *interp,		//!< ��� ������������
			PA<int>& loops,			//!< loops per polygon
			PA<int>& nverts,		//!< ����� ��������� � �������� (size = polygoncount)
			PA<int>& verts			//!< ������� ��������� ��������� (size = facevertcount)
			);
		/// Sphere
		void renderSphere (float radius, float zmin, float zmax,
							float thetamax=360) {}

		void renderText( 
			const char* text, 
			Math::Vec3f pos
			);
	};
}

namespace cls
{
	inline renderOpenglCacheImpl::renderOpenglCacheImpl(
		M3dView* view
		)
	{
		degeneration = 1;
		this->view = view;

		box = Math::Box3f();
		transforms.clear();
		transforms.push_back(Math::Matrix4f::id);
	}

	inline void renderOpenglCacheImpl::Render(bool bPoints, bool bWireframe, bool bShaded)
	{
		if( bPoints)
		{
			glEnableClientState(GL_VERTEX_ARRAY);
			glVertexPointer( 3, GL_FLOAT, 0, &wireverts[0]);
			glDrawArrays(GL_POINTS, 0, wireverts.size());
			glDisableClientState(GL_VERTEX_ARRAY);
		}
		if( bWireframe)
		{
			glEnableClientState(GL_VERTEX_ARRAY);
			glVertexPointer( 3, GL_FLOAT, 0, &wireverts[0]);
			glDrawElements(GL_LINES, wireinds.size()*2, GL_UNSIGNED_INT, &wireinds[0]);
			glDisableClientState(GL_VERTEX_ARRAY);
		}
		if( bShaded)
		{
			glEnableClientState(GL_NORMAL_ARRAY);
			glNormalPointer( GL_FLOAT, 0, &shadenorms[0]);
			glEnableClientState(GL_VERTEX_ARRAY);
			glVertexPointer( 3, GL_FLOAT, 0, &shadeverts[0]);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			glTexCoordPointer(2, GL_FLOAT, 0, &shadeuv[0]);

			glDrawElements(GL_TRIANGLES, shadeinds.size(), GL_UNSIGNED_INT, &shadeinds[0]);

			glDisableClientState(GL_VERTEX_ARRAY);
			glDisableClientState(GL_NORMAL_ARRAY);
		}
	}
	inline void renderOpenglCacheImpl::clear(void)
	{
		wireverts.clear();
		wireinds.clear();

		shadeverts.clear();
		shadenorms.clear();
		shadeuv.clear();
		shadeinds.clear();
	}


	inline void renderOpenglCacheImpl::PushTransform (void)
	{
		glPushMatrix();
		transforms.push_back(transforms.back());
	}
	inline void renderOpenglCacheImpl::PopTransform (void)
	{
		glPopMatrix();
		transforms.pop_back();
	}
	inline void renderOpenglCacheImpl::SetTransform ()
	{
		//
	}
	inline void renderOpenglCacheImpl::AppendTransform ()
	{
		cls::P<Math::Matrix4f> M = GetParameter("transform");
		const float* d = M.data().data();
		glMultMatrixf(d);
		Math::Matrix4f& transform = transforms.back();
		transform = M.data()*transform;
	}

	/// ����� ��������� ���������� ������� ������
	inline void renderOpenglCacheImpl::SystemCall(
		enSystemCall call
		)
	{
		bool bOk = CallFilters(call);
		if( !bOk) return;

		switch(call)
		{
		case SC_POINTS:
			{
			PA<Math::Vec3f> P;
			GetParameter("P", P);
			renderPoints(P);
			break;
			}
		case SC_CURVES:
			{
			char* interp, *wrap;
			PA<int> nverts;
			GetParameter("curves::interpolation", interp);
			GetParameter("curves::wrap", wrap);
			GetParameter("curves::nverts", nverts);
			renderCurves(interp, wrap, nverts);
			break;
			}
		case SC_PATCH:
			break;
//		case SC_TRIMCURVE:
//			break;
		case SC_MESH:
			{
			char* interp;
			PA<int> loops;
			PA<int> nverts;
			PA<int> verts;
			GetParameter("mesh::interpolation", interp);
			GetParameter("mesh::loops", loops);
			GetParameter("mesh::nverts", nverts);
			GetParameter("mesh::verts", verts);
			renderMesh(interp, loops, nverts, verts);
			break;
			}
		case SC_SPHERE:
			break;
		case SC_BLOBBY:
			break;
		case SC_TEXT:
			PS text;
			GetParameter("text", text);
			Math::Vec3f pos;
			GetParameter("position", pos);
			renderText(text.data(), pos);
			break;
		}
	}

	/// ���������� ���������
	inline IRender* renderOpenglCacheImpl::DeclareProcBegin(
		const char* name
		)
	{
		// ������ ����������� ���������
		return this;
	}
	inline void renderOpenglCacheImpl::DeclareProcEnd(
		)
	{
		// ��������� ����������� ���������
	}

	/// Call (���������������� �����)
	inline void renderOpenglCacheImpl::Call( 
		IProcedural* pProcedural
		)
	{
		pProcedural->Process(this);
	}

	/// Procedural (���������������� �����)
	inline void renderOpenglCacheImpl::RenderCall( 
		IProcedural* pProcedural
		)
	{
		pProcedural->Render(this, 0, 0);
	}

	inline void renderOpenglCacheImpl::addRenderCall(
		std::vector<Math::Vec3f>& outP,
		std::vector<Math::Vec2i>& vi)
	{
		int indexShift = wireverts.size();

		wireverts.resize(indexShift+outP.size());
		for(unsigned i=0; i<outP.size(); i++)
			wireverts[indexShift+i]	 = outP[i];

		int offset = wireinds.size();
		wireinds.resize(offset+vi.size());
		for(i=0; i<vi.size(); i++)
			wireinds[offset+i] = indexShift+vi[i];
	}
	inline void renderOpenglCacheImpl::addRenderCall(
		int fvcount,
		Math::Vec3f* outP,
		Math::Vec3f* N, 
		float* u, 
		float* v, 
		int* faces, 
		int faceindexcount
		)
	{
		int indexShift = shadeverts.size();

		shadeverts.resize(indexShift+fvcount);
		shadenorms.resize(indexShift+fvcount);
		shadeuv.resize(indexShift+fvcount);
		for(int i=0; i<fvcount; i++)
		{
			shadeverts[indexShift+i]	 = outP[i];
			shadenorms[indexShift+i]	 = Math::Vec3f(0, 1, 0);
			shadeuv[indexShift+i]		= Math::Vec2f(0, 0);
			if(N) shadenorms[indexShift+i] = -N[i];
			if( u && v)
				shadeuv[indexShift+i] = Math::Vec2f(u[i], v[i]);
		}

		int offset = shadeinds.size();
		shadeinds.resize(offset+faceindexcount);
		for(i=0; i<faceindexcount; i++)
			shadeinds[offset+i] = indexShift+faces[i];
	}




	///////////////////////////////////
	// render Implementation

	inline void renderOpenglCacheImpl::renderPoints(
		PA<Math::Vec3f>& P			//!< points
		)
	{
		/*/
		Math::Matrix4f& transform = transforms.back();

		glBegin( GL_POINTS );
		for( int i=0; i<P.size(); i++)
		{
			if((i%degeneration)==0)
				glVertex3f(P[i]);
			box.insert( transform*P[i]);
		}
		glEnd();
		/*/
	}

	inline void renderOpenglCacheImpl::renderCurves( 
		const char *interp, 
		const char *wrap,		//!< wrap
		PA<int>& nverts			//!< int[ncurves] ����� ��������� � ������
		)
	{
	}
	inline void renderOpenglCacheImpl::renderMesh(
		const char *interp,		//!< ��� ������������
		PA<int>& loops,			//!< loops per polygon
		PA<int>& nverts,		//!< ����� ��������� � �������� (size = polygoncount)
		PA<int>& verts			//!< ������� ��������� ��������� (size = facevertcount)
		)
	{
		Math::Matrix4f& transform = transforms.back();
		PA<Math::Vec3f> P(GetParameter("P"));

		// transform
		std::vector<Math::Vec3f> outP;
		outP.resize(P.size());
		for(int x=0; x<P.size(); x++)
			outP[x] = transform*P[x];

		// wireframe
		std::vector<Math::Vec2i> vi;
		{
			vi.reserve(verts.size());

			int i=0;
			std::set< Math::Edge32> edges;
			int fv = 0;
			for(int f=0; f<nverts.size(); f++)
			{
				for(int v=0; v<nverts[f]; v++, fv++)
				{
					int vi0, vi1;
					if( v==0)
						vi0 = verts[fv+nverts[f]-1];
					else
						vi0 = verts[fv-1];
					vi1 = verts[fv];
					Math::Edge32 e(vi0, vi1);
					if( edges.find(e)!=edges.end()) 
						continue;

					vi.push_back(Math::Vec2i(vi0, vi1));
				}
			}
			addRenderCall(outP, vi);
		}

		// shading
		{
			PA<Math::Vec3f> N(GetParameter("N"));
			PA<int> TR(GetParameter("triangles"));
			PA<float> s(GetParameter("s"));
			PA<float> t(GetParameter("t"));

			Math::Matrix4As3f transform33( transform);
			std::vector< Math::Vec3f> out_P;
			std::vector< Math::Vec3f> out_N;
			out_P.resize(verts.size());
			out_N.resize(verts.size());
			for(int i=0; i<verts.size(); i++)
			{
				out_P[i] = transform*P[verts[i]];
				if( N.size()==verts.size())
					out_N[i] = transform33*N[i];
				else
					out_N[i] = Math::Vec3f(0, 1, 0);
			}

			addRenderCall(verts.size(), &out_P[0], &out_N[0], 
				(s.size()==verts.size())?&s[0]:NULL,
				(t.size()==verts.size())?&t[0]:NULL,
				&TR[0], TR.size());
		}

	}
	inline void renderOpenglCacheImpl::renderText( 
		const char* text, 
		Math::Vec3f pos
		)
	{
		if( view)
		{
			MPoint pt(0, 0, 0, 1);
			pt.x = pos.x;
			pt.y = pos.y;
			pt.z = pos.z;
			view->drawText(text, MPoint(0, 0, 0, 1));
		}

	}


}

