#pragma once
#include "IRender.h"
#include "renderImpl.h"
#include <map>
#include <list>
#include <vector>
#include <string>
#include "ocellaris/renderCacheImpl.h"
#include "T/TAttributeStack_forblur.h"
#include "T/TTransformStack_minimal.h"
#include "T/TTransformStack_forblur.h"
#include "T/TBranchStack_minimal.h"

#include <mr/shader.h>
#include <mr/geoshader.h>

////////////////////////////////////////////////////////
// ��������� OUTPUT:
/*/ 
float output::shadingrate			= RiShadingRate
float output::pixelsample[2]		= RiPixelSamples
int output::filtersize[2]			
string output::filtername			= RiPixelFilter
float output::gain output::gamma	= RiExposure
int hidden::jitter					= RiHider("hidden", "int jitter"
string hidden::depthfilter			= RiHider("hidden", "string depthfilter"
string output::displayname
string output::displaytype					
string output::displaymode			= RiDisplay
string output::quantizeMode
int output::quantizeOne
int output::quantizeMax
int output::quantizeMin
float output::quantizeDither		= RiQuantize
bool output::trace					= RiOption("user", "float tracebreadthfactor", 1, "float tracedepthfactor", 1
float trace::maxdepth				= RiOption("trace", "int maxdepth"
/*/ 

namespace cls
{
	//! @ingroup impl_group 
	//! \brief ���������� IRender ��� Prman ��� ������������� �� ����� ���������� 
	//! 
	//! ��� ���������� � RenderMan
	template <class TRANSFORMSTACK = TTransformStack_pure>
	struct renderMentalRayImpl : public renderImpl<TAttributeStack_forblur, TRANSFORMSTACK, TBranchStack_minimal>
	{
		typedef renderImpl<TAttributeStack_forblur, TRANSFORMSTACK, TBranchStack_minimal> BASE;
		renderMentalRayImpl(
			miTag* result=NULL
			);
		~renderMentalRayImpl(
			);

		// ��� �������
		virtual const char* renderType(
			){return "mentalray";};

		// ���������� ������, ��������� ��������� � ��������
		void startFrom(
			const renderMentalRayImpl<TRANSFORMSTACK>& src
			);

		//@{ 
		/// PushAttributes/PopAttributes
		virtual void PushAttributes (void);
		virtual void PopAttributes (void);
		//@}

		//! �������
		virtual void AttributeV(
			const char *name,		//!< ��� ���������
			const Param& t
			);

		//@{ 
		/// Transformations
		virtual void PushTransform (void);
		virtual void PopTransform (void);
		virtual void SetTransform ();
		virtual void AppendTransform ();
		//@}

		//@{ 
		virtual void PushRenderAttributes(void);
		virtual void PopRenderAttributes(void);
		//@}

		//@{ 
		/// Motion blur
		virtual void MotionBegin();
		virtual void MotionEnd();
		virtual void MotionPhaseBegin(float time);
		virtual void MotionPhaseEnd();
		virtual bool SetCurrentMotionPhase(float time);
		//@}

		//@{ 
		/// Shaders
		virtual void Shader(const char* shadertype);
		virtual void Light ();
		//@}

		/// ����� ��������� ���������� ������� ������
		virtual void SystemCall(
			enSystemCall call
			);

		/// Procedural (���������������� �����)
		virtual void Call( 
			IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
			);
		virtual void Call( 
			const char*	procname		//!< ����� ������� IProcedural::Render()
			);


		/// Procedural (��������� �����)
		/// ��� ���������� ���������� ����� IProcedural::Render()
		/// ���� ����� ������ �������� � ����!!! (��� �����???)
		virtual void RenderCall( 
			IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
			);
		virtual void RenderCall( 
			const char*	procname		//!< ����� ������� IProcedural::Render()
			);

	protected:
		static miBoolean rpi_subdivfunc(miTag tag, miObject*, void *args);
		static miBoolean rpi_freefunc(miTag tag, miObject*, void *args);

		typedef std::vector< std::pair< std::string, Param> > paramlist_t;
		//! ��������� ����������� ����������
		struct delayRenderStruct
		{
			renderMentalRayImpl<TRANSFORMSTACK> mentalray;
			IProcedural* pDelayRender;
		};

		// scene
		miTag* result;
		//! subrender, for blur
		IRender* subrender;
		//! bBlurFinal - ��������� ����������� ���� ������
		bool bBlurFinal;

	protected:
		// ����� ������� ����� (��� ������ RiBasis)
		void singleSystemCallBeforeBlur(enSystemCall call);
		// ��������� ����� �� ������
		void singleSystemCall(enSystemCall call);

		/// Geometry
		/// 0-D prims - point clouds
		void renderPoints (
			);
		/// 1-D prims - lines, curves, hair
		void renderCurves (
			const char *interp, 
			const char *wrap,		//!< wrap
			PA<int>& nverts			//!< int[ncurves] ����� ��������� � ������
			);
		void renderCurves (int ncurves, int nvertspercurve, int order,
							const float *knot, float vmin, float vmax) {}
		/// 2-D prims - rectangular patches (NURBS, bicubics, bilinears), and
		/// indexed face meshes (polys, polyhedra, subdivs)
		void renderPatch();
		/// Mesh 
		// ������������ ���������:
		// uniform string interpolation,	//!< ��� ������������
		// int loops[]						//!< loops per polygon
		// int nverts[]						//!< ����� ��������� � �������� (size = polygoncount)
		// int verts[]						//!< ������� ��������� ��������� (size = facevertcount)

		void renderMesh(
			const char *interp,		//!< ��� ������������
			PA<int>& loops,			//!< loops per polygon
			PA<int>& nverts,		//!< ����� ��������� � �������� (size = polygoncount)
			PA<int>& verts			//!< ������� ��������� ��������� (size = facevertcount)
			);
		/// Sphere
		void renderSphere(
			float radius, float zmin, float zmax,
			float thetamax=360);
		/// Blobby
		void renderBlobby(
			PA<Math::Matrix4f>& ellipsoids);
		/// shader
		void singleShader(
			const char *shadertype);

		// RiBasis
		void renderBasis(
			);

		/// setup camera
		bool Camera(
			);
		/// setup Output
		bool Output(
			);

	protected:
		// dump
		bool bDump;

		// ��������� �����
		float blur_scenetime;
		float blur_shutterOpen;
		float blur_shutterClose;
	public:
		void setBlurParams(float blur_scenetime, float blur_shutterOpen, float blur_shutterClose);
	};
}
#include "inl/renderMentalRayImpl.hpp"
