#pragma once

#include "IOcellaris.h"

namespace cls
{
	//! @ingroup ocellaris_group 
	//! \brief IShader
	//! 
	//! ����� ��� ���������� �������� �������� (������� shading network)
	struct IShader
	{
		enum enGenType
		{
			UNKNOWN			= -1,
			SURFACESHADER	= 0,
			DISPLACESHADER	= 1,
			VOLUMESHADER	= 2,
			LIGHTSHADER		= 3
//			COLOR			= 2,
//			FLOAT			= 3,
//			VECTOR			= 4,
//			NORMAL			= 5,
//			MATRIX			= 6,
//			MANIFOLD		= 7,
		};

		// ��� �������
		virtual enGenType GetType(
			)=0;
		// ���������� ���, ������ ��� �������������
		virtual const char* GetUniqueName(
			)=0;


		//! ��� ���������������� ������� (slo/gso)
		virtual const char* name(
			)=0;

		//! ������ ���������� �������
		virtual int paramCount(
			)=0;
		// ???
		virtual void param(
			int p
			)=0;
	};
}
