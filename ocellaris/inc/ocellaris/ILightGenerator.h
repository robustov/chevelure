#pragma once

#include "ocellaris/IGenerator.h"
#include "ocellaris/INode.h"
#include "ocellaris/IRender.h"
#include "ocellaris/IExportContext.h"

namespace cls
{
	//! @ingroup plugin_group
	//! \brief ��������� ���������
	//! 
	//! ��� �������� ���������
	//! ��������� ������������ ����. �������� �� ����������� �� ��������� �������� � �����
	struct ILightGenerator : public IGenerator
	{
		//! OnGeometry
		//! 1. ��������� ���������
		//! 2. ��������� ����
		//! ���� render=NULL ������ ������� ����
		virtual bool OnLight(
			cls::INode& node,				//!< ������ 
			cls::IRender* render,			//!< render
			Math::Box3f& box,				//!< box 
			cls::IExportContext* context,	//!< context
			MObject& generatornode			//!< generator node �.� 0
			)=0;
	private:
		virtual enGenType GetType(
			){return LIGHT;};
	};
}