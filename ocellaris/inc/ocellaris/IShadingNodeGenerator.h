#pragma once

#include "ocellaris/IGenerator.h"
#include "ocellaris/INode.h"
#include "ocellaris/IRender.h"
#include "ocellaris/IShaderAssembler.h"

namespace cls
{
	struct IShaderAssembler;

	//! @ingroup plugin_group
	//! \brief ������ ��������
	//! 
	//! ��� ���������� �������� �� ���������� ���������
	struct IShadingNodeGenerator : public IGenerator
	{
		enum enShadingNodeType
		{
			UNKNOWN			= -1,
			SURFACESHADER	= 0,		// ��� return: CI OI
			DISPLACE		= 1,		// return: vector
//			DISPLACEFLOAT	= 2,		// return: float
			VOLUME			= 3,		// ???
			LIGHT			= 4,		// ��� return: Cl
			POINT			= 10,		// return: color
			COLOR			= 11,		// return: color
			FLOAT			= 12,		// return: float
			VECTOR			= 13,		// return: vector
			NORMAL			= 14,		// return: normal
			MATRIX			= 15,		// return: matrix
			MANIFOLD		= 20,		// return: shn::ManifoldParameter
		};

		// ��� ����
		virtual enShadingNodeType getShadingNodeType(
			MObject& node					//!< ������ (shader)
			)=0;
		//! ��������� ��������� 
		//! ��������� ��������� ��������� ��� �������
		virtual bool BuildShader(
			MObject& node,					//!< ������
			cls::enParamType wantedoutputtype, //!< ����� ��� ����� �� ������
			cls::IShaderAssembler* as,		//!< render
			cls::IExportContext* context,	//!< context
			MObject& generatornode			//!< generator node �.� 0
			)=0;
	private:
		virtual enGenType GetType(
			){return SHADINGNODE;};
	};

	// �� ���� ������� - ��� ���������
	cls::enParamType shaderType2paramType(IShadingNodeGenerator::enShadingNodeType);

	// �� ���� ������� - ��� ���������
	inline cls::enParamType shaderType2paramType(IShadingNodeGenerator::enShadingNodeType type)
	{
		switch(type)
		{
			case IShadingNodeGenerator::POINT:		return cls::PT_POINT;
			case IShadingNodeGenerator::COLOR:		return cls::PT_COLOR;
			case IShadingNodeGenerator::FLOAT:		return cls::PT_FLOAT;
			case IShadingNodeGenerator::VECTOR:	return cls::PT_VECTOR;
			case IShadingNodeGenerator::NORMAL:	return cls::PT_NORMAL;
			case IShadingNodeGenerator::MATRIX:	return cls::PT_MATRIX;
		}
		return cls::PT_UNKNOWN;
	}

}
