#pragma once

#include "Math/Math.h"
#include "Math/Box.h"
#include <vector>
#include <string>
#include "IOcellaris.h"

//#define OCS_MEMTEST

//! Param:
//! ����� ��������� �������� ��� ����������� ��������
//! � ���������� ���������� ���������� ����������� �� ���������� ���������� 2� IParameter
//! 
namespace cls
{
	// ������

	/*/
	��� autoexp.dat:
	cls::PS=$ADDIN(OcellarisAutoExp.dll,_Ocs_Param@28)
	cls::P<*>=$ADDIN(OcellarisAutoExp.dll,_Ocs_Param@28)
	cls::PA<*>=$ADDIN(OcellarisAutoExp.dll,_Ocs_Param@28)
	cls::Param=$ADDIN(OcellarisAutoExp.dll,_Ocs_Param@28)
	/*/

	///////////////////////////////////////////////
	// ��������� � ������� ������������� � # 
	// �� ����� ������������ � ������!!!
	

	//! @ingroup ocellaris_group 
	//! \brief ��� ���������
	//! 
	//! ��� ���������
	enum enParamType
	{
		PT_UNKNOWN		= -1,
		PT_VOID,		//!< VOID
		PT_BOOL,		//!< bool
		PT_STRING,		//!< string/string
		PT_FLOAT,		//!< float
		PT_HALF,		//!< float
		PT_DOUBLE,		//!< float
		PT_POINT,		//!< point
		PT_VECTOR,		//!< vector
		PT_NORMAL, 		//!< normal
		PT_COLOR, 		//!< color
		PT_HPOINT,		//!< float[4]
		PT_MATRIX,		//!< matrix = float[4]
		PT_INT8, PT_UINT8, PT_INT16, PT_UINT16, 
		PT_INT, 		//!< int
		PT_UINT,
		PT_BOX,			//!< Math::Box3f
		PT_VECTOR2,		// float[2]
		PT_MAX
	};
	inline const char* str_type(enParamType type);
	inline enParamType type_fromstr(const char* str);

	//! @ingroup ocellaris_group 
	//! \brief ��� ������������
	//! 
	//! ��� ������������ ��� ����������. � ������ ������� ���������������� ���������
	//! 
	enum enParamInterpolation
	{
		PI_ERROR			= -1,
		PI_UNKNOWN			= 0,
		PI_CONSTANT			= 1, //!< uniform/constant
		PI_VERTEX			= 2, //!< vertex/vertex
		PI_PRIMITIVE		= 3, //!< .../...
		PI_PRIMITIVEVERTEX	= 4, //!< facevarying/...
		PI_VARYING			= 5, //!< varying/...
		PI_LAST				= 6
	};
	inline const char* str_interpolation(enParamInterpolation type);
	inline enParamInterpolation interpolation_fromstr(const char* str);

#define VURTUAL_IN_PARAMETER 
//#define VURTUAL_IN_PARAMETER virtual

	//! @ingroup ocellaris_group 
	//! \brief IParameter (��� ����������� �������������!)
	//! 
	//! ���������� � ������ ��������� (��� ����������� �������������, ������ ���������� Param, P<T> � PA<T>)
	//! ������������� ����� ����� �������� ����������� � ��� �� ������ ��� � ��������
	struct IParameter
	{
		int refCount;				//!< ����� ������
		char* data;					//!< ������
		int stride;					//!< ������ ���������
		int size;					//!< ����� ��������� � �������
		int reserved;				//!< ����� ���������������� ��������� �������
		enParamType type;			//!< ��� ��������
		enParamInterpolation interp;	//!< ��� ������������
	public:
		//! IParameter
		IParameter();
		//! ~IParameter
		VURTUAL_IN_PARAMETER ~IParameter();
		//! �������� ���
		bool respecifyType(enParamType type);
		//! alloc ������������� ����� - ���������� ����� size*stride ����
		VURTUAL_IN_PARAMETER char* alloc(int size);
		//! ���������� 
		void free();
		//! �������� ������
		void addref();
		//! ������� ������
		VURTUAL_IN_PARAMETER void release();

#ifdef OCS_MEMTEST
		int allocsize;
		static int memory(int inm=0);
#endif
	};

	//! @ingroup ocellaris_group 
	//! \brief Param
	//! 
	//! ������ �� �������� (������ ���������� Param, P<T>, PA<T>, PS)
	struct Param
	{
		//! 
		Param();
		//! 
		Param(IParameter* body);
		//! 
		Param(const Param& arg);
		Param(Param& arg, int offset, int size=-1);
		Param( enParamType _type, enParamInterpolation interp, int size=1);
		//! 
		~Param();
		//! 
		Param& operator=(const Param& arg);
		//! 
		Param copy() const;
		//! ��������� �� ��������
		bool equal(const Param& arg) const;

		// �������� ���
		void setType(enParamType ty);
		// �������� ������������
		void setInterpolation(enParamInterpolation);
		//! 
		const IParameter* operator->() const;
		//! 
		IParameter* operator->();
		Param operator [](int i);
		//! �� �����������?
		bool empty() const;
		//! ����� ���������
		int size() const;
		//! ������ ������ � ������
		int sizebytes() const;
		//! stride
		int stride() const;
		//! data
		char* data();
		//! const data
		const char* data() const;
		const char* stringdata(int i) const;

		struct filedata
		{
			char type;		//!< ��� ��������
			char interp;	//!< ��� ������������
			short stride;	//!< ������ ���������
			int size;		//!< ����� ��������� � �������
//			char data[];	//!< ������
		};

		template< class STREAM>
		void _fwrite(STREAM& file) const;
		void fwrite(FILE* file) const;
		template< class STREAM>
		inline static Param _fread(STREAM& file);

		inline static Param fread(FILE* file);

	protected:
		IParameter* pointer;	//!< �����
		int offset;				//!< �������� � �������
		int _size;				//!< ������ � �������, ����� �� ��������� � pointer->size, ���� <0 ����� �� pointer->size
	protected:
		void init(IParameter* body, int offset=0, int size=-1);
		//! �������� ������ ��������� � ��������� ������
		void init_alloc(enParamType type, int stride, enParamInterpolation interp, int size);
		//! �������� ������ ��������� � ��������� ������
		template <class T>
		void init_alloc(const T& arg, enParamInterpolation interp, int size, enParamType _type);
		void init_allocstring(enParamInterpolation interp, const char** args, int size);
		//! init
		void copylink(const Param& arg);

		//! exit
		void exit();

		char* stringdata(int i);
	};

	//! @ingroup ocellaris_group 
	//! \brief �������� ��� �������� � IRender
	//! 
	//! ��������� ����� ��������� �� �������
	//! �������������: ocs::P<int>
	//! ������������ � �-��� IRender::Parameter(Param&)
	//! ������ ������������ ������ ����� ������ IRender::Parameter(T)
	template <class T>
	class P : public Param
	{
	public:
		//! default
		P( enParamType wanttype=PT_UNKNOWN);
		//! �� �������� ������, ����� ���� ��������� �-���� empty!!!
		P( const Param& src, enParamType wanttype=PT_UNKNOWN);
		//! �� ��������
		P(T arg, enParamType wanttype=PT_UNKNOWN);
		//!
		P<T>& operator=(T& arg);
		//! ������
		const T& data() const;
		//! ������
		T& data();
		//! ������
		bool data(T& output) const;
		//! ������
		const T& operator*() const;
		T& operator*();
		// ����� ��������?
		bool isValid() const;
		// 
		void get(T& val);
	};

	//! @ingroup ocellaris_group 
	//! \brief �������� ������ ��� �������� � IRender
	//! 
	//! ��������� ����� ��������� - �������.
	//! �������������: ocs::PA<int>.
	//! ������������ � �-��� IRender::Parameter(Param&).
	//! ����� ��������� �� ����� ��������� ������: ocs::PA<int>(pasrc, 1, 25).
	template <class T>
	class PA : public Param
	{
	public:
		//! �� �������� ������, ����� ���� ��������� �-���� empty!!!
		PA( const Param& src);
		//! default (�������� � �������� ������ ������)
		PA( enParamInterpolation interp=PI_UNKNOWN, int size=0, enParamType type=PT_UNKNOWN );
		//! �� ������� (�������� � �������� ������ ������)
		PA( enParamInterpolation interp, const T* data, int size, cls::enParamType type=PT_UNKNOWN );
		//! �� vector-������� (�������� � �������� ������ ������)
		PA( enParamInterpolation interp, const std::vector<T>& data, cls::enParamType type=PT_UNKNOWN );
		//! ������������� ������ (�� �������� � �������� ������ ������)
		PA( const PA<T>& arg, int offset=0, int size=-1);
		//! reserve
		void reserve(int s);
		// reserve_size
		int reserve_size() const;
		//! clear
		void clear();
		//! resize
		void resize(int s);
		//! push_back()
		void push_back(const T& arg = T());
		//! push_back()
		T& back();
		//! operator []
		const T& operator [](int i) const;
		//! operator []
		T& operator [](int i);
		//! ������
		const T* data() const;
		//! ������
		T* data();
		//! ������
		void data(std::vector<T>& out) const;
	};


	//! @ingroup ocellaris_group 
	//! \brief ����� ������ ��� �������� � IRender
	//! 
	//! ����� ��������� ������
	//! �������������: ocs::PS("text")
	//! ������������ � �-��� IRender::Parameter(Param&)
	//! ������ ������������ ������ ����� ������ IRender::Parameter("text")
	class PS : public Param
	{
	public:
		//! default
		PS();
		//! �� �������� ������, ����� ���� ��������� �-���� empty!!!
		PS( const Param& src);
		//! �� ��������
		PS(const char* a);
		//! �� ��������
		PS(const std::string& a);
		//!
		PS& operator=(const char* a);
		//! ���������
		bool operator==(const char* a);
		bool operator!=(const char* a);
		//! ������
		const char* data() const;
		// ����� ��������?
		bool isValid() const;
		// ������ ������?
		bool emptystring() const;
	};

	//! @ingroup ocellaris_group 
	//! \brief ����� ������ ����� ��� �������� � IRender
	//! 
	//! ����� ������ �����
	//! �������������: ocs::PSA psa(); psa.set(); psa[i]
	//! ������������ � �-��� IRender::Parameter(Param&)
	//! ��������������� �������� ������ ����� �������������!!!
	//! 
	//! ������, ������ � �������:
	//! cls::PSA psa;
	//! psa.set(arg);
	//! const char* x = psa[0];
	//! const char* y = psa[1];
	//! std::vector<const char*> out;
	//! const char** vals = psa.data( out);
	class PSA : public Param
	{
	public:
		//! default
		PSA();
		//! �� �������� ������, ����� ���� ��������� �-���� empty!!!
		PSA( const Param& src);
		//! �� �������� ������, ����� ���� ��������� �-���� empty!!!
		PSA( const char** arg, int size );
		PSA( const std::vector<const char*>& arg );
		PSA( const std::vector<std::string>& arg );

		void set( const char** arg, int size );
		void set( const std::vector<const char*>& arg );
		void set( const std::vector<std::string>& arg );
		//! operator []
		const char* operator [](int i) const;
		// ������
		const char** data( std::vector<const char*>& out) const;
		void data( std::vector<std::string>& out) const;
	};
}
#include "inl/IParameter.hpp"
