#pragma once

#include "ocellaris/IGenerator.h"
#include "ocellaris/INode.h"
#include "ocellaris/IRender.h"

namespace cls
{
	//! @ingroup plugin_group
	//! \brief ������ ��������
	//! 
	//! ��� ���������� �������� �� ���������� ���������
	struct INodeFilter : public IGenerator
	{
		//! ������ ������� false ���� ������ ����������� ���� ��������� �� ���������
		virtual bool IsRenderNode(
			cls::INode& node,				//!< ������
			cls::IRender* render,			//!< render
			cls::IExportContext* context,	//!< context
			MObject& generatornode			//!< generator node �.� 0
			)=0;
		// ���������� ��� ��������
		virtual bool OnExport(
			cls::INode& inode,				//!< ������
			cls::IRender* render,				//!< render
			cls::IExportContext* context,	//!< context
			MObject& generatornode			//!< generator node �.� 0
			)=0;

	private:
		virtual enGenType GetType(
			){return NODEFILTER;};
	};
}
