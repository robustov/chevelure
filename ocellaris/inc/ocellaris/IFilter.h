#pragma once
#include <list>
#include <Math/Math.h>
#include "IOcellaris.h"

namespace cls
{
	struct IRender;

	//! @ingroup ocellaris_group 
	//! \brief IFilter ��������� ��� ���������� ��������� ��� �������
	//! ������������ � ������ IRender::Filter
	//! 
	//! ������� ����� ��� �������� �������
	struct IFilter 
	{
		//! ����� �������
		virtual bool Begin(
			IRender* render, 
			const Math::Matrix4f& currentWorldTransform,
			int& id					// ���������� �� �������� �������
			)=0;
		//! ���������� SystemCall. ������ true ���� ���� ���������, ����� false
		virtual bool OnSystemCall(
			enSystemCall instruction,
			IRender* render,
			const Math::Matrix4f& currentWorldTransform,
			int id
			){return true;};
		//! ���������� Shader. ������ true ���� ���� ���������, ����� false
		virtual bool OnShader(
			const char* type,
			IRender* render,
			const Math::Matrix4f& currentWorldTransform,
			int id
			){return true;};
		//! ���� �������
		virtual void End(
			IRender* render, 
			int id
			)=0;
	};

	typedef std::list< std::pair<IFilter*,int> > fitrerlist_t;

	typedef cls::IFilter* (*FILTER_CREATOR)(cls::IRender* prman);
}
