#pragma once
#include "IRender.h"
//#include <ri.h>
#include <map>
#include <list>
#include <vector>
#include <string>
#include "Util/tree.h"

namespace cls
{
	//! @ingroup ocellaris_group 
	//! \brief ������������ � renderBaseCacheImpl ��� �������� ������ ����������
	//! 
	//! CacheStorageMulty ������������ ��� ������ �������� � ����� ����
	struct CacheStorageMulty
	{
		struct Instruction
		{
			enInstruction type;
			std::string name;
			Param param;
		public:
			Instruction(enInstruction type = I_UNKNOWN){this->type=type;}
			Instruction(enInstruction type, const char* name, const Param& param){this->type=type;this->name=name;this->param=param;}
		};
		typedef std::map<int, Instruction> multyinstruction_t;
		typedef std::tree<multyinstruction_t> instructiontree_t;

		instructiontree_t::iterator cur;

		instructiontree_t instructiontree;
		instructiontree_t::iterator root;

		int currentkey;

		// ��� ������
		instructiontree_t::branch_iterator readit;
		multyinstruction_t::iterator readitmi;


	public:
		void setKey(int key);
		void clear();
		void copy(const CacheStorageMulty& arg);
		void setInstruction(enInstruction type);
		void setInstruction(enInstruction type, const char* name, cls::Param p);

		bool startInstruction();
		bool nextInstruction();
		bool isEnd();

		enInstruction getInstruction();
		const char* getInstructionName();
		cls::Param getInstructionParam();

		void Dump() const;
	};
}
#include "inl/CacheStorageMulty.hpp"
