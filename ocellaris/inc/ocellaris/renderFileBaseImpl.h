#pragma once
#include "IRender.h"
#include "renderImpl.h"
#include <map>
#include <list>
#include <vector>
#include <string>
#include "Util/misc_switch_string.h"
#include "ocellaris/procedureCacheImpl.h"

namespace cls
{
	//! @ingroup impl_group 
	//! \brief ������� ���������� IRender ��� ������ � ������ �� �����
	//! 
	//! ��. renderFileImpl � renderStringImpl
	template< class STREAM>
	struct renderFileBaseImpl : public renderImpl<TAttributeStack_minimal, TTransformStack_pure>
	{
		renderFileBaseImpl(
			);
		~renderFileBaseImpl(
			);
		//! ���������
		bool Render(
			IRender* render
			);
		//! close
		bool close(
			);

		//!@name Set a parameter of the next shader, primitive, camera, or output
		//@{ 

		//! ��������� ���������� (������ ��� ��������� �������)
		virtual void comment(
			const char* text
			);

		//! ��������
		virtual void ParameterV(
			const char *name,		//!< ��� ���������
			const Param& t
			);
		virtual Param GetParameterV(
			const char *name
			);
		//! �������� �� ��������
		virtual void ParameterFromAttribute(
			const char *paramname,		//!< ��� ���������
			const char *attrname		//!< ��� �������� �� �������� ����� ������
			);
		//! �������� � �������
		virtual void ParameterToAttribute(
			const char *paramname,		//!< ��� ��������� �� �������� ����� ������
			const char *attrname		//!< ��� �������� 
			);
		//! ������� �� ��������
		virtual void AttributeFromAttribute(
			const char *dstname,	//!< ��� �������� ���������
			const char *srcname		//!< ��� �������� �� �������� ����� ������
			);

		//@}

		//! ��������
		virtual void AttributeV(
			const char *name,		//!< ��� ���������
			const Param& t
			);

		///////////////////////////////////////////////////////////////
		/// ��������� �� ����� �������
		virtual void IfAttribute(const char* name, const Param& t);
		virtual void EndIfAttribute();

		//@{ 
		/// PushAttributes/PopAttributes
		virtual void PushAttributes (void);
		virtual void PopAttributes (void);
		virtual void SaveAttributes (const char *name, const char *attrs=NULL) {}
		virtual void RestoreAttributes(const char *name, const char *attrs=NULL) {}
		//@}

		//@{ 
		/// Transformations
		virtual void PushTransform (void);
		virtual void PopTransform (void);
		virtual void SetTransform ();
		virtual void AppendTransform ();
		virtual void PinTransform();
		//@}

		//@{ 
		virtual void PushRenderAttributes(void);
		virtual void PopRenderAttributes(void);
		//@}

		//@{ 
		/// Motion blur
		virtual void MotionBegin();
		virtual void MotionEnd();
		virtual void MotionPhaseBegin(float time);
		virtual void MotionPhaseEnd();
		//@}

		//@{ 
		/// Shaders
		virtual void Shader(const char* shadertype);
		virtual void Light();
		//@}

		/// ����� ��������� ���������� ������� ������
		virtual void SystemCall(
			enSystemCall call
			);


		/// ���������� ���������
		virtual IRender* DeclareProcBegin(
			const char* name
			);
		virtual void DeclareProcEnd(
			);
		virtual void Call( 
			const char*	procname			//!< ��� ��������� � DeclareProcBegin
			);
		virtual void RenderCall( 
			const char*	procname			//!< ��� ��������� � DeclareProcBegin
			);

		// ������
		virtual void Filter( 
			IFilter* filter
			);
		virtual void Filter( 
			const char*	procname
			);

	protected:
		STREAM file;

		bool bWrite;
		bool bAscii;
		int indent;							// ��� ������ (�����.�����)
		int tell;							// ��� ������!
		bool bReadGlobals;					// ��� ������!

		// ��������� ��������������� � �����
		std::map<std::string, procedureCacheImpl<> > procedures;

		void writeindent();

		// ��� ascii ������
		std::string filename;
		int symb;				// ��� ������ 
		int line, position;		// ������� ������ ������ - �������
		bool ReadLexem(char* buf, int buflen, int& cur);
		// ������ �������
		int ParseLexem(
			IRender* &render, int level, std::list<IRender*>& procstack);

		void writeAttrOrParam(
			const char *paramname,		//!< ��� ���������
			const Param& p				//!< ��������
			);
		void writeParameter(
			const char *paramname,		//!< ��� ���������
			const Param& p			//!< ��������
			);

		Param loadAttrOrParameter(
			std::string& name
			);
		bool loadStrings(
			std::vector<std::string>& stringlist, 
			int count, 
			char* buf, int size);
		bool loadFloats(
			std::vector<float>& stringlist, 
			int count, 
			char* buf, int size);
		bool loadInts(
			std::vector<int>& stringlist, 
			int count, 
			char* buf, int size);
		bool loadInt8s(
			std::vector<unsigned char>& stringlist, 
			int count, 
			char* buf, int size);

		void binaryReadString(std::string& name);
		void binaryWriteString(const char* name);

	};
}
#include "inl/renderFileBaseImpl.hpp"
