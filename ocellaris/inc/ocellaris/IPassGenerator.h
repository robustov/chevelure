#pragma once

#include "ocellaris/IGenerator.h"
#include "ocellaris/IRender.h"
#include "ocellaris/IExportContext.h"
#include "ocellaris/INode.h"

struct ChiefGenerator;
namespace cls
{
	struct INode;
	struct IPassGenerator;
	struct IBlurGenerator;

/*/
	// ��������!!!
	struct SubGenerator
	{
		SubGenerator(){passGenerator=NULL;}
		IPassGenerator* passGenerator;
		std::string passnodename;
	};
/*/

	//! @ingroup plugin_group
	//! \brief ������� ��������� ��� PASS �����������
	//! 
	//! IPassGenerator - ������� ��������� ��� PASS �����������
	//! ������ finalpass
	struct IPassGenerator : public IGenerator
	{
		// ���� ������ ����������� ������� ������� � ������ ����� ��� ���
		// ���� ������ true - ����� ������� AddExportFrames ��� ������� ������� �����
		virtual bool IsUsedInFrame(
			cls::INode& pass, 
			float frame, 			
			cls::IExportContext* context
			)=0;

		// �������� ������ � ��������� ExportFrame ��� �������� ����� (RenderFrame)
		// �������� � ������ context->addExportTimeNode()
		// ���� ����� ���� ���������� IBlurGenerator
		virtual bool AddExportFrames(
			cls::INode& pass, 
			cls::INode& node, 
			float renderframe, 			
			cls::IExportContext* context
			)
		{
			// �� ��������� ��� �����!!!
			context->addExportTimeNode(node, renderframe, renderframe);
			return true;
		}
		// ������� ��� ����������� ���� �� ExportFrame 
		// ���� ����� ���� ���������� IBlurGenerator
		virtual bool BuildNodeFromExportFrame(
			cls::INode& pass,				// pass
			cls::INode& node,				// pass
			cls::IRender* render,			// ������
			float frame,
			Math::Matrix4f& transform,		// ����������� ����� transform
			Math::Box3f& box,				// ����������� ���� ��� �������
			cls::IExportContext* context	// �������� ��������
			)
		{
			// �� ��������� ��� �����
			// 
			cls::IRender* pcache=NULL;
			Math::Box3f* pbox = NULL;
			if( !context->getNodeExportFrameCache( frame, node, pcache, pbox))
			{
				context->error( "getNodeExportFrameCache %s %f FAILED\n", frame, node.getName());
				return false;
			}
			pcache->RenderGlobalAttributes(render);
			pcache->Render(render);
			Math::Box3f wb = *pbox;
			if( wb.isvalid())
			{
				wb.transform(transform);
				box.insert(wb);
			}
			return true;
		}


		// ���������� ����� ��������� ������ ����
		virtual bool OnExportNode(
			cls::INode& node,				//!< ������ ������� ���� �� �������
			cls::IRender* render,			//!< render
			cls::IExportContext* context,	//!< context
			cls::INode& passnode,			//!< ������ �������
			MObject& generatornode			//!< generator node �.� 0
			){return true;};

		//! ������� ������ � ��������
		virtual bool OnExport(
			cls::INode& node,				//!< ������ ���������� ������� (�.�., kNullObj)
			cls::IRender* scenecache,		//!< ��� �����
			Math::Box3f scenebox,			//!< box �����
			cls::IRender* subpasscache,		//!< ��� ����������� (�� ���� ������� OnExportParentPass)
			cls::IExportContext* context	// 
			)=0;

		// ���������� � ���� ����������� ��� �������� �������
		// ��������, ������ shadowpass ������ �������� � ������� ��� �������� ����
		virtual void OnExportParentPass( 
			cls::INode& subnode,				//!< ������ ���������� ������� (�.�., kNullObj)
			cls::IRender* render, 
			cls::IExportContext* context
			)=0;

		//! ���������� ����� �������� ���� ������
		virtual bool OnPostExport(
			cls::INode& node,				//!< ������ ���������� ������� (�.�., kNullObj)
			cls::IExportContext* context	// 
			){return true;};

	private:
		virtual enGenType GetType(
			){return PASS;};

	

	
		/*/
	// ��� ��������:
	public:
		// ������ �������� �� ������
		virtual cls::INode* getNodeTree(
			MObject& passnode,			//!< ������ ���������� ������� (�.�., kNullObj)
			ChiefGenerator* chief, 
			cls::IExportContext* context
			){return NULL;}

		//! OnInit ������� ����� ������?
		virtual bool OnInit(
			MObject& node,			//!< ������ ���������� ������� (�.�., kNullObj)
			IExportContext* context,
			std::string& passname,		// ������ ��� �������
			std::vector<cls::SubGenerator>& subGenerators	// ������ ��������������
			)=0;

		// ���������� ����� ��������� ��� ��������
		virtual bool isLazy(
			MObject& node,
			cls::IExportContext* context
			)
		{
			return false;
		}
		//! ������� ������ � ��������
		virtual bool OnExport(
			MObject& node,				//!< ������ ���������� ������� (�.�., kNullObj)
			IExportContext* context,
			IRender* render,			//!< render
			IRender* prescene,			//!< pre scene
			IRender* prenextgenerator	//!< pre next generator scene
			)=0;

		//! ������ ��������� � �������?
		virtual bool IsObjectInPass(
			MObject& node,			//!< �������� ������ ���������� � �������
			IRender* render			//!< render
			)=0;
		//! ����������� ����
		virtual bool IsBlured(
			MObject& node,			//!< �������� ������ ���������� � �������
			IExportContext* context
			){return true;}
		/*/
	};
}