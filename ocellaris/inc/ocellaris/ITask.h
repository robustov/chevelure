#pragma once
#include "IOcellaris.h"
//#include "Math/Math.h"
//#include "Math/Box.h"
#include <map>
#include <vector>
#include <set>
#include "ocellaris/IParameter.h"

namespace cls
{
	//! @ingroup plugin_group 
	//! \brief ITask ��������� ��� ������
	//! 
	//! ��������� ����������� ������
	struct ITask
	{
		virtual ~ITask(){};

		/// �������� � �������� ����
		virtual void Parameter(
			const char *name, Param& t)=0;
		// �������� ��������
		virtual Param GetParameter(
			const char *name)=0;

	};
}
