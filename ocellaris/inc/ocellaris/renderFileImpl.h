#pragma once
#include "IRender.h"
#include <map>
#include <list>
#include <vector>
#include <string>
#include "Util/misc_switch_string.h"
#include "renderFileBaseImpl.h"

namespace cls
{
	const int  header_ocellaris_len = 10;
	const char header_ocellaris[]	= "#ocellaris";
	const int  header_format_len    = 7;
	const char header_ascii[]		= "#ascii_";
	const char header_binary[]		= "#binary";
	const int  header_length		= 10+7+1;	//???


	class fileStream
	{
	public:
		fileStream()
		{
			file = NULL;
		}
		inline bool isValidForRead()
		{
			return file!=0;
		}

		inline int _ftell()
		{
			fpos_t res;
			fgetpos(file, &res);
			return (int)res;
//			return ftell(file);
		}
		inline int _fseek(int tell, int base)
		{
			fpos_t res = tell;
			return fsetpos(file, &res);
//			return fseek(file, tell, base);
		}

		inline int _fgetc()
		{
			return fgetc(file);
		}
		inline void _fgets(char* buf, int len)
		{
			fgets(buf, len, file);
		}
		inline int _feof()
		{
			return feof(file);
		}
		inline void _fputs(const char* text)
		{
			fputs(text, file);
		}
		inline void _fprintf(const char* text, ...)
		{
			va_list argList; va_start(argList, text);
			vfprintf(file, text, argList);
			va_end(argList);
		}

		int _fread( void *buffer, int size, size_t count)
		{
			return (int)fread(buffer, size, count, file);
		}
		int _fwrite( const void *buffer, int size, size_t count)
		{
			return (int)fwrite(buffer, size, count, file);
		}
	public:
		FILE* file;
	};

	//! @ingroup impl_group 
	//! \brief ���������� IRender ��� ������ � ����
	//! 
	//! ��� ������ � ����
	//! ������� Load(): ������ ����� � �������� �� ���������
	struct renderFileImpl : public renderFileBaseImpl<fileStream>
	{
		renderFileImpl(
			);
		~renderFileImpl(
			);
		//! open
		bool openForWrite(
			const char* filename, 
			bool bAscii
			);
		//! ������ ����� � �������� �� ���������
		bool openForRead(
			const char* filename,
			IRender* render=NULL	// ����� ���� ��������� render � ���������� ���� ���������� �������� - ���������� ������� ��������� ������� �� ��� ���
			);
		bool openForRead(
			FILE* file, 
			bool bAscii,
			IRender* startrender		// ����� ���� ��������� render � ���������� ���� ���������� �������� - ���������� ������� ��������� ������� �� ��� ���
			);
		//! close
		bool close(
			);
	};
}

namespace cls
{
	inline renderFileImpl::renderFileImpl()
	{
	}
	inline renderFileImpl::~renderFileImpl()
	{
		close();
	}
	inline bool renderFileImpl::openForWrite(
		const char* filename, 
		bool bAscii
		)
	{
		this->bWrite = true;
		if(!filename)
			return false;
		file.file = fopen( filename, bAscii?"wt":"wb");
		if( !file.file) return false;
		this->bAscii = bAscii;
		file._fputs( header_ocellaris);
		if( bAscii)
		{
			file._fputs( header_ascii);
			file._fputs( "\n");
		}
		else
			file._fputs( header_binary);
		this->indent = 0;
		this->filename = filename;
//		IRender::Attribute("file::box", box);
		return true;
	}
	inline bool renderFileImpl::openForRead(
		const char* filename, 
		IRender* startrender		// ����� ���� ��������� render � ���������� ���� ���������� �������� - ���������� ������� ��������� ������� �� ��� ���
		)
	{
		if( !filename || !filename[0]) return false;
		this->bWrite = false;
		file.file = fopen( filename, "rb");
		if( !file.file) return false;
		// ��������� ������
		bAscii = true;

		char buf[64];
		file._fgets(buf, header_length);
		// ocellaris file?
		if( memcmp(header_ocellaris, buf, header_ocellaris_len)!=0)
		{
			close();
			return false;
		}
		// file format
		char* format = buf + strlen(header_ocellaris);
		if( memcmp(header_ascii, format, header_format_len)==0)
			bAscii = true;
		else if( memcmp(header_binary, format, header_format_len)==0)
			bAscii = false;
		else
		{
			close();
			return false;
		}
		this->filename = filename;

		/*/
		if(bAscii)
		{
			fclose(file.file);
			file.file = fopen( filename, "rb");
			if(!file.file) return false;
			char buf[64];
			file._fread(buf, 1, header_length);
			this->tell = file._ftell();
		}
		/*/

		this->symb = -1;
		this->line = this->position = 0;
		setlocale( LC_CTYPE, "C" );

		this->tell = file._ftell();

		// ������ �������� (����� ������� �� �����������!)
		// ...
		this->bReadGlobals = true;
		Render(this);
		this->bReadGlobals = false;

		if(startrender)
		{
			// �������� ���������� �������� � ������
			std::vector< std::pair< std::string, cls::Param> > attrlist;
			this->GetAttributeList(attrlist);
			for(unsigned i=0; i<attrlist.size(); i++)
			{
				startrender->Attribute(attrlist[i].first.c_str(), attrlist[i].second);
			}
		}

		return true;
	}
	inline bool renderFileImpl::openForRead(
		FILE* f, 
		bool bAscii,
		IRender* startrender		// ����� ���� ��������� render � ���������� ���� ���������� �������� - ���������� ������� ��������� ������� �� ��� ���
		)
	{
		this->filename = "";

		this->bWrite = false;
		if( !f) return false;
		file.file = f;
		this->bAscii = bAscii;

		char buf[64];
		file._fgets(buf, header_length);

		if(bAscii)
		{
			this->symb = -1;
			this->line = this->position = 0;
			setlocale( LC_CTYPE, "C" );
		}
		this->tell = file._ftell();

		// ������ �������� (����� ������� �� �����������!)
		// ...
		this->bReadGlobals = true;
		Render(this);
		this->bReadGlobals = false;

		if(startrender)
		{
			// �������� ���������� �������� � ������
			std::vector< std::pair< std::string, cls::Param> > attrlist;
			this->GetAttributeList(attrlist);
			for(unsigned i=0; i<attrlist.size(); i++)
			{
				startrender->Attribute(attrlist[i].first.c_str(), attrlist[i].second);
			}
		}
		return true;
	}

	inline bool renderFileImpl::close()
	{
		if(file.file)
		{
			fclose(file.file);
		}
		return true;
	}


}