#pragma once

#include "Util\Error.h"

namespace cls
{
	const int  _header_ocellaris_len = 10;
	const char _header_ocellaris[] = "#ocellaris";
	const int  _header_format_len    = 7;
	const char _header_ascii[]     = "#ascii_";
	const char _header_binary[]    = "#binary";
	const int  _header_length		  = 10+7+1;	//???

	template <class STREAM>
	inline renderFileBaseImpl<STREAM>::renderFileBaseImpl()
	{
		this->tell = 0;
		this->indent = 0;
	}
	template <class STREAM>
	inline renderFileBaseImpl<STREAM>::~renderFileBaseImpl()
	{
	}

	//! ��������� ���������� (������ ��� ��������� �������)
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::comment(
		const char* text
		)
	{
		if( bAscii)
		{
			writeindent();
			file._fputs( "//");
			file._fputs( text);
			file._fputs( "\n");
		}
		else
		{
		}
	}

	///////////////////////////////////////////////////////////////
	/// ��������� �� ����� �������
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::IfAttribute(const char* name, const Param& t)
	{
		if(t.empty()) 
			return;
		if( bWrite)
		{
			if( bAscii)
			{
				writeindent();
				file._fputs( Instruction2str(I_IFATTRIBUTE));
				file._fputs( " ");
				writeAttrOrParam(name, t);
				file._fputs( ";\n");
			}
			else
			{
				enInstruction instruction = I_IFATTRIBUTE;
				file._fwrite(&instruction, sizeof(instruction), 1);
				binaryWriteString(name);
				t._fwrite(file);
			}
		}
		renderImpl<TAttributeStack_minimal, TTransformStack_pure>::IfAttribute(name, t);
	}
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::EndIfAttribute()
	{
		if( bWrite)
		{
			if( bAscii)
			{
				writeindent();
				file._fputs( Instruction2str(I_ENDIFATTRIBUTE));
				file._fputs( ";\n");
			}
			else
			{
				enInstruction instruction = I_ENDIFATTRIBUTE;
				file._fwrite(&instruction, sizeof(instruction), 1);
			}
		}
		renderImpl<TAttributeStack_minimal, TTransformStack_pure>::PushAttributes();
	}

	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::PushAttributes(void) 
	{
		if( bAscii)
		{
			writeindent();
			file._fputs( "{\n");
			++this->indent;
		}
		else
		{
			enInstruction instruction = I_ATTRIBUTE_BEGIN;
			file._fwrite(&instruction, sizeof(instruction), 1);
		}
		renderImpl<TAttributeStack_minimal, TTransformStack_pure>::PushAttributes();
	}
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::PopAttributes(void) 
	{
		renderImpl<TAttributeStack_minimal, TTransformStack_pure>::PopAttributes();
		if( bAscii)
		{
			--this->indent;
			if( this->indent<0) this->indent=0;
			writeindent();
			file._fputs( "}\n");
		}
		else
		{
			enInstruction instruction = I_ATTRIBUTE_END;
			file._fwrite(&instruction, sizeof(instruction), 1);
		}
	}

	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::PushTransform (void)
	{
		if( bAscii)
		{
			writeindent();
			file._fputs( "PushTransform;\n");
//			++this->indent;
		}
		else
		{
			enInstruction instruction = I_PUSHTRANSFORM;
			file._fwrite(&instruction, sizeof(instruction), 1);
		}
	}
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::PopTransform (void)
	{
		if( bAscii)
		{
//			--this->indent;
			writeindent();
			file._fputs( "PopTransform;\n");
		}
		else
		{
			enInstruction instruction = I_POPTRANSFORM;
			file._fwrite(&instruction, sizeof(instruction), 1);
		}
	}
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::SetTransform()
	{
		if( bAscii)
		{
			writeindent();
			file._fputs( "SetTransform;\n");
		}
		else
		{
			enInstruction instruction = I_SETTRANSFORM;
			file._fwrite(&instruction, sizeof(instruction), 1);
		}
	}
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::AppendTransform ()
	{
		if( bAscii)
		{
			writeindent();
			file._fputs( "AppendTransform;\n");
		}
		else
		{
			enInstruction instruction = I_APPENDTRANSFORM;
			file._fwrite(&instruction, sizeof(instruction), 1);
		}
	}
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::PinTransform ()
	{
		if( bAscii)
		{
			writeindent();
			file._fputs( Instruction2str(I_PINTRANSFORM));
			file._fputs( ";\n");
		}
		else
		{
			enInstruction instruction = I_PINTRANSFORM;
			file._fwrite(&instruction, sizeof(instruction), 1);
		}
	}

	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::PushRenderAttributes(void)
	{
		if( bAscii)
		{
			writeindent();
			file._fputs( Instruction2str(I_PUSHRENDERATTRIBUTE));
			file._fputs( ";\n");
			++this->indent;
		}
		else
		{
			enInstruction instruction = I_PUSHRENDERATTRIBUTE;
			file._fwrite(&instruction, sizeof(instruction), 1);
		}
	}
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::PopRenderAttributes(void)
	{
		if( bAscii)
		{
			--this->indent;
			writeindent();
			file._fputs( Instruction2str(I_POPRENDERATTRIBUTE));
			file._fputs( ";\n");
		}
		else
		{
			enInstruction instruction = I_POPRENDERATTRIBUTE;
			file._fwrite(&instruction, sizeof(instruction), 1);
		}
	}


	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::writeParameter(
		const char *paramname,		//!< ��� ���������
		const Param& p			//!< ��������
		)
	{
		if( bAscii)
		{
			writeindent();
			file._fputs( "Parameter ");
			writeAttrOrParam(paramname, p);
			file._fputs( ";\n");
		}
		else
		{
		}
	}

	//! ��������
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::ParameterV(
		const char *paramname,		//!< ��� ���������
		const Param& p			//!< ��������
		)
	{
		if(p.empty()) 
			return;
//		if( paramname[0]!='#')
//		{
			if(p->interp == PI_UNKNOWN)
			{
#ifndef SAS
				printf("renderFileBaseImpl: WARNING! parameter %s has PI_UNKNOWN interpolation type\n", paramname);
#endif
			}
			if( bAscii)
			{
				writeParameter(paramname, p);
			}
			else
			{
				enInstruction instruction = I_PARAMETER;
				file._fwrite(&instruction, sizeof(instruction), 1);
				binaryWriteString(paramname);
				p._fwrite(file);
			}
//		}
		renderImpl<TAttributeStack_minimal, TTransformStack_pure>::ParameterV(paramname, p);
	}

	//! ������ ���������
	template <class STREAM>
	inline Param renderFileBaseImpl<STREAM>::GetParameterV(
		const char *name
		)
	{
		return renderImpl<TAttributeStack_minimal, TTransformStack_pure>::GetParameterV(name);
	}
	//! �������� �� ��������
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::ParameterFromAttribute(
		const char *paramname,		//!< ��� ���������
		const char *attrname		//!< ��� �������� �� �������� ����� ������
		)
	{
		if( bAscii)
		{
			writeindent();
			file._fputs( "ParameterFromAttribute ");
			file._fputs( paramname);
			file._fputs( " ");
			file._fputs( attrname);
			file._fputs( ";\n");
		}
		else
		{
			enInstruction instruction = I_PARAMETERFROMATTR;
			file._fwrite(&instruction, sizeof(instruction), 1);
			binaryWriteString(paramname);
			binaryWriteString(attrname);
		}
	}

	//! �������� �� ��������
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::ParameterToAttribute(
		const char *paramname,		//!< ��� ���������
		const char *attrname		//!< ��� �������� �� �������� ����� ������
		)
	{
		enInstruction instruction = I_PARAMETERTOATTR;
		if( bAscii)
		{
			writeindent();
			file._fputs( Instruction2str(instruction));
			file._fputs( " ");
			file._fputs( paramname);
			file._fputs( " ");
			file._fputs( attrname);
			file._fputs( ";\n");
		}
		else
		{
			file._fwrite(&instruction, sizeof(instruction), 1);
			binaryWriteString(paramname);
			binaryWriteString(attrname);
		}
	}

	//! �������� �� ��������
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::AttributeFromAttribute(
		const char *dstname,		//!< ��� ���������
		const char *srcname		//!< ��� �������� �� �������� ����� ������
		)
	{
		if( bAscii)
		{
			writeindent();
			file._fputs( "AttributeFromAttribute ");
			file._fputs( dstname);
			file._fputs( " ");
			file._fputs( srcname);
			file._fputs( ";\n");
		}
		else
		{
			enInstruction instruction = I_ATTRIBUTEFROMATTR;
			file._fwrite(&instruction, sizeof(instruction), 1);
			binaryWriteString(dstname);
			binaryWriteString(srcname);
		}
	}

	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::AttributeV(
		const char *name,		//!< ��� ���������
		const Param& t
		)
	{
		if(t.empty()) 
			return;
		if( bWrite)
		{
			if(t->interp == PI_UNKNOWN)
				printf("renderFileBaseImpl: WARNING! attribute %s has PI_UNKNOWN interpolation type", name);
			if( bAscii)
			{
				writeindent();
				file._fputs( "Attribute ");
				writeAttrOrParam(name, t);
				file._fputs( ";\n");
			}
			else
			{
				enInstruction instruction = I_ATTRIBUTE;
				file._fwrite(&instruction, sizeof(instruction), 1);
				binaryWriteString(name);
				t._fwrite(file);
			}
		}
		renderImpl<TAttributeStack_minimal, TTransformStack_pure>::AttributeV(name, t);
	}

	/// Motion blur
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::MotionBegin()
	{
		if( bAscii)
		{
			writeindent();
			file._fprintf("MotionBegin;\n");
			++this->indent;
		}
		else
		{
			enInstruction instruction = I_MOTIONBEGIN;
			file._fwrite(&instruction, sizeof(instruction), 1);
		}
	}
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::MotionEnd()
	{
		if( bAscii)
		{
			--this->indent;
			writeindent();
			file._fprintf("MotionEnd;\n");
		}
		else
		{
			enInstruction instruction = I_MOTIONEND;
			file._fwrite(&instruction, sizeof(instruction), 1);
		}
	}
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::MotionPhaseBegin(float time)
	{
		if( bAscii)
		{
			writeindent();
			file._fprintf("MotionPhaseBegin %g;\n", time);
			++this->indent;
		}
		else
		{
			enInstruction instruction = I_MOTIONPHASEBEGIN;
			file._fwrite(&instruction, sizeof(instruction), 1);
			file._fwrite(&time, sizeof(time), 1);
		}
	}
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::MotionPhaseEnd()
	{
		if( bAscii)
		{
			--this->indent;
			writeindent();
			file._fprintf("MotionPhaseEnd;\n");
		}
		else
		{
			enInstruction instruction = I_MOTIONPHASEEND;
			file._fwrite(&instruction, sizeof(instruction), 1);
		}
	}

	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::Shader(const char* shadertype)
	{
		if( bAscii)
		{
			writeindent();
			file._fprintf("Shader \"%s\";\n", shadertype);
		}
		else
		{
			enInstruction instruction = I_SHADER;
			file._fwrite(&instruction, sizeof(instruction), 1);
			binaryWriteString(shadertype);
		}
	}
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::Light()
	{
		if( bAscii)
		{
			writeindent();
			file._fprintf("Light;\n");
		}
		else
		{
			enInstruction instruction = I_LIGHT;
			file._fwrite(&instruction, sizeof(instruction), 1);
		}
	}

	/// ����� ��������� ���������� ������� ������
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::SystemCall(
		enSystemCall call
		)
	{
		if( bAscii)
		{
			const char* callname = SystemCall2str(call);
			writeindent();
			file._fprintf("SystemCall %s;\n", callname);
		}
		else
		{
			enInstruction instruction = I_SYSTEMCALL;
			file._fwrite(&instruction, sizeof(instruction), 1);
			file._fwrite(&call, sizeof(call), 1);
		}
	}

	/// ���������� ���������
	template <class STREAM>
	inline IRender* renderFileBaseImpl<STREAM>::DeclareProcBegin(
		const char* name
		)
	{
		if( bAscii)
		{
			writeindent();
			file._fprintf("%s( \"%s\");\n", Instruction2str(I_DECLAREPROCBEGIN), name);

			++this->indent;
		}
		else
		{
			enInstruction instruction = I_DECLAREPROCBEGIN;
			file._fwrite(&instruction, sizeof(instruction), 1);
			binaryWriteString(name);
		}
		return this;
	}
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::DeclareProcEnd(
		)
	{
		if( bAscii)
		{
			--this->indent;
			writeindent();
			file._fprintf("%s;\n", Instruction2str(I_DECLAREPROCEND));
		}
		else
		{
			enInstruction instruction = I_DECLAREPROCEND;
			file._fwrite(&instruction, sizeof(instruction), 1);
		}
	}
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::Call( 
		const char*	procname			//!< ��� ��������� � DeclareProcBegin
		)
	{
		if( bAscii)
		{
			writeindent();
			file._fprintf("Call \"%s\";\n", procname);
		}
		else
		{
			enInstruction instruction = I_CALL;
			file._fwrite(&instruction, sizeof(instruction), 1);
			binaryWriteString(procname);
		}
	}
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::RenderCall( 
		const char*	procname			//!< ��� ��������� � DeclareProcBegin
		)
	{
		if( bAscii)
		{
			writeindent();
			file._fprintf("RenderCall \"%s\";\n", procname);
		}
		else
		{
			enInstruction instruction = I_RENDERCALL;
			file._fwrite(&instruction, sizeof(instruction), 1);
			binaryWriteString(procname);
		}
	};

	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::Filter( 
		IFilter* filter
		)
	{
	}
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::Filter( 
		const char*	filter			//!< ��� ��������� � DeclareProcBegin ��� dllname@procname
		)
	{
		if( bAscii)
		{
			writeindent();
			file._fprintf("Filter \"%s\";\n", filter);
		}
		else
		{
			enInstruction instruction = I_FILTER;
			file._fwrite(&instruction, sizeof(instruction), 1);
			binaryWriteString(filter);
		}
	}

/*/
	/// ���������� ���������
	template <class STREAM>
	void renderFileBaseImpl<STREAM>::ProceduralBegin(
		const char* name
		)
	{
	}
	template <class STREAM>
	void renderFileBaseImpl<STREAM>::ProceduralEnd(
		)
	{
		writeindent();
		file._fprintf("ProceduralEnd();\n");
	}

	/// Procedural (���������������� �����)
	template <class STREAM>
	void renderFileBaseImpl<STREAM>::Procedural( 
		IProcedural* pDelayRender		//!< ����� ������� IDelayRender::Render()
		)
	{
		writeindent();
		pDelayRender->Render(this);
//		file._fputs("Procedural;\n");
	}

	/// Procedural (��������� �����)
	template <class STREAM>
	void renderFileBaseImpl<STREAM>::DelayProcedural( 
		IProcedural* pDelayRender,		//!< ����� ������� IDelayRender::Render()
		int id,							//!< ��� ������ ���� ������
		const Math::Box3f& box			//!< bound
		)
	{
		writeindent();
		std::string dllname, procname;
		bool bInit;
		if( !pDelayRender->Export(dllname, procname, bInit))
		{
			file._fputs("//Call;\n");
		}
		else
		{
			file._fprintf( "Call \"%s\" \"%s\";\n", dllname.c_str(), procname.c_str());
		}
	}
/*/


	template <class STREAM>
	inline bool renderFileBaseImpl<STREAM>::Render(
		IRender* render
		)
	{
		if( !file.isValidForRead())
			return false;
		if( !render) 
			return false;

		file._fseek( this->tell, SEEK_SET);
		symb = -1;

		std::list<IRender*> procstack;

		if(bAscii)
		{
			try
			{
				// ���� ������� ������
				for(int i=0; ; i++)
				{
					tell = file._ftell();
					int res = ParseLexem(render, 0, procstack);
					if( !res)
					{
						break;
					}
				}
			}
			catch(Util::Error& e)
			{
				printf("%s\n", e.what());
				return false;
			}
		}
		else
		{
			enInstruction instruction;
			for(int i=0; ; i++)
			{
				if( file._feof()) 
					break;
				tell = file._ftell();
				bool bStop = false;
				file._fread( &instruction, sizeof(instruction), 1);
				if( file._feof()) 
					break;
 				if( this->bReadGlobals && instruction!=I_ATTRIBUTE)
					break;
				switch(instruction)
				{
					case I_ATTRIBUTE_BEGIN: 
						render->PushAttributes(); 
						break;
					case I_ATTRIBUTE_END: 
						render->PopAttributes(); 
						break;
					case I_PARAMETER: 
						{
							std::string name;
							binaryReadString(name);
							Param param = Param::_fread(file);
							render->Parameter(name.c_str(), param); 
						}
						break;
					case I_ATTRIBUTE: 
						{
							std::string name;
							binaryReadString(name);
							Param param = Param::_fread(file);
							render->Attribute(name.c_str(), param); 
						}
						break;
					case I_PARAMETERFROMATTR:
						{
							std::string name, attr;
							binaryReadString(name);
							binaryReadString(attr);
							render->ParameterFromAttribute(name.c_str(), attr.c_str()); 
						}
						break;
					case I_PARAMETERTOATTR:
						{
							std::string name, attr;
							binaryReadString(name);
							binaryReadString(attr);
							render->ParameterToAttribute(name.c_str(), attr.c_str()); 
						}
						break;
					case I_ATTRIBUTEFROMATTR:
						{
							std::string dst, src;
							binaryReadString(dst);
							binaryReadString(src);
							render->AttributeFromAttribute(dst.c_str(), src.c_str()); 
						}
						break;
					case I_IFATTRIBUTE: 
						{
							std::string name;
							binaryReadString(name);
							Param param = Param::_fread(file);
							render->IfAttribute(name.c_str(), param); 
						}
						break;
					case I_ENDIFATTRIBUTE:
						render->EndIfAttribute(); 
						break;
					case I_PUSHTRANSFORM:
						render->PushTransform(); 
						break;
					case I_POPTRANSFORM:
						render->PopTransform(); 
						break;
					case I_SETTRANSFORM:
						render->SetTransform();
						break;
					case I_APPENDTRANSFORM:
						render->AppendTransform();
						break;
					case I_PINTRANSFORM:
						render->PinTransform();
						break;

					case I_PUSHRENDERATTRIBUTE:
						render->PushRenderAttributes(); 
						break;
					case I_POPRENDERATTRIBUTE:
						render->PopRenderAttributes(); 
						break;

					case I_SHADER:
						{
							std::string type;
							binaryReadString(type);
							render->Shader(type.c_str());
							break;
						}
					case I_LIGHT:
						render->Light();
						break;
					case I_SYSTEMCALL:
						{
							enSystemCall call;
							file._fread(&call, sizeof(call), 1);
							render->SystemCall(call);
							break;
						}
					case I_CALL:
						{
							std::string procname;
							binaryReadString(procname);
							render->Call(procname.c_str());
							break;
						}
					case I_RENDERCALL:
						{
							std::string procname;
							binaryReadString(procname);
							render->RenderCall(procname.c_str());
							break;
						}
					case I_FILTER:
						{
							std::string filter;
							binaryReadString(filter);
							render->Filter(filter.c_str());
							break;
						}
					case I_MOTIONBEGIN:
						render->MotionBegin();
						break;
					case I_MOTIONEND:
						render->MotionEnd();
						break;
					case I_MOTIONPHASEBEGIN:
						{
							float t;
							file._fread(&t, sizeof(t), 1);
							render->MotionPhaseBegin(t);
							break;
						}
					case I_MOTIONPHASEEND:
						render->MotionPhaseEnd();
						break;

					case I_DECLAREPROCBEGIN:
						{
							std::string procname;
							binaryReadString(procname);

							IRender* procrender = procedures[procname].getRender();
							render->RegisterProcedural(procname.c_str(), &procedures[procname]);

							procstack.push_back(render);
							render = procrender;
							// 
							break;
						}
					case I_DECLAREPROCEND:
						{
							if( procstack.empty())
							{
								// �������� ���������� ���������
								printf("bad procedure declaration\n");
								return false;
							}
							render = procstack.back();
							procstack.pop_back();
							//
							break;
						}

					case I_ENDFILE:
						bStop = true;
						break;
					default:
						bStop = true;
						break;

				}
				if( bStop) break;
			}
		}
//		fclose(file);
		return true;
	}

	template <class STREAM>
	inline bool renderFileBaseImpl<STREAM>::ReadLexem(char* buf, int buflen, int& cur)
	{
		cur = 0;
		bool bStart = true;
		bool bNormal = false;
		int bComment = false;
		bool bString = false;
		for(;;)
		{
			if( cur>=buflen-1) 
			{
				// ������������ ������
				throw Util::Error("Buffer overfull in file %s line %d pos %d", filename.c_str(), line, position);
			}
			if( this->symb==-1)
			{
				this->symb = file._fgetc();
				if(symb==0xa)
				{
					this->line++;
					this->position = 0;
				}
				this->position++;
			}
			if( file._feof())
			{
				// ����� ����� ???
				return false;
			}
			if(bStart)
			{
				// ���������� ������
				if( symb=='/')
				{
					bStart = false;
					this->symb = file._fgetc();
					if( this->symb!='/')
					{
						// ������������ ������!!!
						throw Util::Error("Bad symbol 1(%c) in file %s line %d pos %d", this->symb, filename.c_str(), line, position);
					}
					bComment = true;
				}
				else if(symb=='"')
				{
					bString = true;
					bStart = false;
				}
				else if( isalnum(symb) || symb=='.' || symb=='#' || symb=='@' || symb=='-' || symb=='+' || symb=='_' || symb==':' || symb=='|')
				{
					bStart = false;
					bNormal = true;
					buf[cur++] = (char)symb;
				}
				else if( isspace(symb) || symb=='(' || symb==')' || symb==',' || symb==0xd || symb==0xa)
				{
					// ����������
				}
				else if( symb=='=' || symb=='[' || symb==']' || symb=='{' || symb=='}' || symb==';')
				{
					buf[cur++] = symb;
					buf[cur++] = 0;
					symb = -1;
					return true;
				}
				else 
				{
					// ������������ ������!!!
					throw Util::Error("Bad symbol 2(%c) in file %s line %d pos %d", this->symb, filename.c_str(), line, position);
				}
			}
			else
			{
				if(bComment)
				{
					if( symb==0xa)
					{
						// ����� �����������
						bComment = false;
						bStart = true;
					}
				}
				else if(bNormal)
				{
					if( isalnum(symb) || symb=='.' || symb=='#' || symb=='_' || symb=='-' || symb=='+' || symb==':'|| symb=='|')
						buf[cur++] = (char)symb;
					else
					{
						// ��������� ������
						buf[cur++] = 0;
						return true;
					}
				}
				else if(bString)
				{
					if(symb=='\\')
					{
						this->symb = file._fgetc();
						if(this->symb=='"') 
							buf[cur++] = '"';
						else 
						{
							// ������������ ������!!!
							throw Util::Error("Bad symbol 3(%c) in file %s line %d pos %d", this->symb, filename.c_str(), line, position);
						}
					}
					else if(symb=='"')
					{
						// ��������� ������
						buf[cur++] = 0;
						symb = -1;
						return true;
					}
					else
						buf[cur++] = (char)symb;
				}
			}
			symb = -1;
		}
	}

	template <class STREAM>
	inline int renderFileBaseImpl<STREAM>::ParseLexem(
		IRender* &render, int level, std::list<IRender*>& procstack )
	{
		// ������ �������
		char buf[1024];
		int len;
		bool res = ReadLexem(buf, sizeof(buf), len);
		if(!res)
		{
			// ����� �����
			return 0;
		}

		enInstruction code = str2Instruction(buf);

		if( this->bReadGlobals && code!=I_ATTRIBUTE)
		{
			// ��������� ������ ���������
			return 0;
		}

		switch(code)
		{
		case I_ATTRIBUTE_BEGIN: 
			render->PushAttributes(); 
			return 1;		// �� ��������� ;
		case I_ATTRIBUTE_END: 
			render->PopAttributes(); 
			return 1;		// �� ��������� ;
		case I_PARAMETER: 
			{
				std::string name;
				Param param = loadAttrOrParameter(name);
				render->Parameter(name.c_str(), param); 
			}
			break;
		case I_ATTRIBUTE: 
			{
				std::string name;
				Param param = loadAttrOrParameter(name);
				render->Attribute(name.c_str(), param); 
			}
			break;
		case I_PARAMETERFROMATTR:
			{
				if( !ReadLexem(buf, sizeof(buf), len)) 
					throw Util::Error("Bad lexem in file %s line %d pos %d", filename.c_str(), line, position);
				std::vector<std::string> stringlist;
				if( !loadStrings(stringlist, 1, buf, sizeof(buf)))
					return 0;
				std::string name = stringlist[0];
				stringlist.clear();

				if( !ReadLexem(buf, sizeof(buf), len)) 
					throw Util::Error("Bad lexem in file %s line %d pos %d", filename.c_str(), line, position);
				if( !loadStrings(stringlist, 1, buf, sizeof(buf)))
					return 0;
				std::string attr = stringlist[0];

				render->ParameterFromAttribute(name.c_str(), attr.c_str()); 
			}
			break;
		case I_PARAMETERTOATTR:
			{
				if( !ReadLexem(buf, sizeof(buf), len)) 
					throw Util::Error("Bad lexem in file %s line %d pos %d", filename.c_str(), line, position);
				std::vector<std::string> stringlist;
				if( !loadStrings(stringlist, 1, buf, sizeof(buf)))
					return 0;
				std::string name = stringlist[0];
				stringlist.clear();

				if( !ReadLexem(buf, sizeof(buf), len)) 
					throw Util::Error("Bad lexem in file %s line %d pos %d", filename.c_str(), line, position);
				if( !loadStrings(stringlist, 1, buf, sizeof(buf)))
					return 0;
				std::string attr = stringlist[0];

				render->ParameterToAttribute(name.c_str(), attr.c_str()); 
			}
			break;
		case I_ATTRIBUTEFROMATTR:
			{
				if( !ReadLexem(buf, sizeof(buf), len)) 
					throw Util::Error("Bad lexem in file %s line %d pos %d", filename.c_str(), line, position);
				std::vector<std::string> stringlist;
				if( !loadStrings(stringlist, 1, buf, sizeof(buf)))
					return 0;
				std::string name = stringlist[0];
				stringlist.clear();

				if( !ReadLexem(buf, sizeof(buf), len)) 
					throw Util::Error("Bad lexem in file %s line %d pos %d", filename.c_str(), line, position);
				if( !loadStrings(stringlist, 1, buf, sizeof(buf)))
					return 0;
				std::string attr = stringlist[0];

				render->AttributeFromAttribute(name.c_str(), attr.c_str()); 
			}
			break;
		case I_IFATTRIBUTE: 
			{
				std::string name;
				Param param = loadAttrOrParameter(name);
				render->IfAttribute(name.c_str(), param); 
			}
			break;
		case I_ENDIFATTRIBUTE:
			render->EndIfAttribute();
			break;
		case I_PUSHTRANSFORM:
			render->PushTransform();
			break;
		case I_POPTRANSFORM:
			render->PopTransform();
			break;
		case I_SETTRANSFORM:
			render->SetTransform();
			break;
		case I_APPENDTRANSFORM:
			render->AppendTransform();
			break;
		case I_PINTRANSFORM:
			render->PinTransform();
			break;

		case I_PUSHRENDERATTRIBUTE:
				render->PushRenderAttributes();
				break;
		case I_POPRENDERATTRIBUTE:
				render->PopRenderAttributes();
				break;

		case I_SHADER:
			{
				res = ReadLexem(buf, sizeof(buf), len);
				if(!res)
					throw Util::Error("Bad lexem in file %s line %d pos %d", filename.c_str(), line, position);
				std::string type = buf;

				render->Shader(type.c_str());
				break;
			}
		case I_LIGHT:
			{
				render->Light();
				break;
			}
		case I_SYSTEMCALL: 
			{
				res = ReadLexem(buf, sizeof(buf), len);
				if(!res)
					throw Util::Error("Bad lexem in file %s line %d pos %d", filename.c_str(), line, position);

				render->SystemCall(buf);
				break;
			}
		case I_CALL:
			{
				if( !ReadLexem(buf, sizeof(buf), len)) 
					throw Util::Error("Bad lexem in file %s line %d pos %d", filename.c_str(), line, position);
				std::vector<std::string> stringlist;
				if( !loadStrings(stringlist, 1, buf, sizeof(buf)))
					return 0;
				render->Call(stringlist[0].c_str());
			}
			break;
		case I_RENDERCALL:
			{
				if( !ReadLexem(buf, sizeof(buf), len)) 
					throw Util::Error("Bad lexem in file %s line %d pos %d", filename.c_str(), line, position);
				std::vector<std::string> stringlist;
				if( !loadStrings(stringlist, 1, buf, sizeof(buf)))
					return 0;
				render->RenderCall(stringlist[0].c_str());
			}
			break;
		case I_FILTER:
			{
				if( !ReadLexem(buf, sizeof(buf), len)) 
					throw Util::Error("Bad lexem in file %s line %d pos %d", filename.c_str(), line, position);
				std::vector<std::string> stringlist;
				if( !loadStrings(stringlist, 1, buf, sizeof(buf)))
					return 0;
				render->Filter(stringlist[0].c_str());
			}
			break;
		case I_MOTIONBEGIN:
			render->MotionBegin();
			break;
		case I_MOTIONEND:
			render->MotionEnd();
			break;
		case I_MOTIONPHASEBEGIN:
			{
				if( !ReadLexem(buf, sizeof(buf), len)) 
					throw Util::Error("Bad lexem in file %s line %d pos %d", filename.c_str(), line, position);
				std::vector<float> flt;
				if( !loadFloats(flt, 1, buf, sizeof(buf)))
					return 0;
				render->MotionPhaseBegin(flt[0]);
				break;
			}
		case I_MOTIONPHASEEND:
			render->MotionPhaseEnd();
			break;

		case I_DECLAREPROCBEGIN:
			{
				if( !ReadLexem(buf, sizeof(buf), len)) 
					throw Util::Error("Bad lexem in file %s line %d pos %d", filename.c_str(), line, position);
				std::vector<std::string> stringlist;
				if( !loadStrings(stringlist, 1, buf, sizeof(buf)))
					return 0;

				std::string procname = stringlist[0];
				IRender* procrender = procedures[procname].getRender();
				render->RegisterProcedural(procname.c_str(), &procedures[procname]);

				procstack.push_back(render);
				render = procrender;
				break;
			}
		case I_DECLAREPROCEND:
			{
				if( procstack.empty())
				{
					// �������� ���������� ���������
					throw Util::Error("bad procedure declaration %s line %d pos %d", filename.c_str(), line, position);
				}
				render = procstack.back();
				procstack.pop_back();
				break;
			}
		default:
			return 0;
		}
		// ;
		if( !ReadLexem(buf, sizeof(buf), len)) 
			throw Util::Error("Bad END lexem in file %s line %d pos %d", filename.c_str(), line, position);
		if( strcmp(buf, ";")!=0) 
			throw Util::Error("Absent ';' in file %s line %d pos %d", filename.c_str(), line, position);
		return 1;
	}

	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::writeAttrOrParam(
		const char *paramname,		//!< ��� ���������
		const Param& p			//!< ��������
		)
	{
		std::string name;
		name += str_interpolation(p->interp);
		name += " ";
		name += str_type(p->type);
		name += " ";
		name += paramname;

		file._fputs( name.c_str());
		if( p.size()!=1)
		{
			file._fprintf("[%d] = {", p.size());
		}
		else
		{
			file._fputs( " = { ");
		}
		for(int i=0; i<p.size(); i++)
		{
			if(i!=0)
				file._fputs( ", ");
			const char* data = p.data();

//			file._fprintf("%d", i);
			switch(p->type)
			{
			case PT_BOOL:
				file._fprintf( "\"%d\"", ((bool*)data)[i]?1:0); break;
			case PT_STRING:
				file._fprintf( "\"%s\"", p.stringdata(i)); break;
			case PT_FLOAT: case PT_HALF: 
				file._fprintf( "%g", ((float*)data)[i]); break;
			case PT_DOUBLE:
				file._fprintf( "%g", ((double*)data)[i]); break;
			case PT_POINT:
			case PT_VECTOR: 
			case PT_NORMAL:
			case PT_COLOR:
				file._fprintf( "%g, %g, %g", ((Math::Vec3f*)data)[i].x, ((Math::Vec3f*)data)[i].y, ((Math::Vec3f*)data)[i].z); break;
			case PT_MATRIX:
				{
					Math::Matrix4f& m = ((Math::Matrix4f*)data)[i];
					for(int x=0; x<4; x++)
						file._fprintf( "%s%g, %g, %g, %g", (x==0)?"":", ", m[x][0], m[x][1], m[x][2], m[x][3]); 
					break;
				}
			case PT_INT8: case PT_UINT8: 
				file._fprintf( "%d", ((char*)data)[i]); break;
				break;
			case PT_INT16: case PT_UINT16: 
				file._fprintf( "%d", ((short*)data)[i]); break;
				break;
			case PT_INT: case PT_UINT:
				file._fprintf( "%d", ((int*)data)[i]); break;
				break;
			case PT_BOX:
				{
					Math::Box3f& m = ((Math::Box3f*)data)[i];
					file._fprintf( "%g, %g, %g, ", m.min.x, m.min.y, m.min.z);
					file._fprintf( "%g, %g, %g", m.max.x, m.max.y, m.max.z);
					break;
				}
			case PT_HPOINT:
				file._fprintf( "%g, %g, %g, %g", ((Math::Vec4f*)data)[i].x, ((Math::Vec4f*)data)[i].y, ((Math::Vec4f*)data)[i].z, ((Math::Vec4f*)data)[i].w); break;
				break;
			case PT_VECTOR2:
				file._fprintf( "%g, %g", ((Math::Vec2f*)data)[i].x, ((Math::Vec2f*)data)[i].y); break;
				break;
			}
		}
		file._fputs( "}");
	}

	//! ��������
	template <class STREAM>
	inline Param renderFileBaseImpl<STREAM>::loadAttrOrParameter(
		std::string& name
		)
	{
		char buf[1024];
		int len;

		name = "";
		// ������������
		enParamInterpolation interp; 
		if( !ReadLexem(buf, sizeof(buf), len)) 
			throw Util::Error("Bad parameter in file %s line %d pos %d", filename.c_str(), line, position);
		interp = interpolation_fromstr(buf);
		if( interp==PI_ERROR) 
			throw Util::Error("Bad parameter interpalation %s in file %s line %d pos %d", buf, filename.c_str(), line, position);

		// ���
		enParamType type;
		if( !ReadLexem(buf, sizeof(buf), len)) 
			throw Util::Error("Bad parameter in file %s line %d pos %d", filename.c_str(), line, position);

		type = type_fromstr(buf);
		if( type==PT_UNKNOWN) 
			throw Util::Error("Bad parameter type in file %s line %d pos %d", filename.c_str(), line, position);

		// ���
		if( !ReadLexem(buf, sizeof(buf), len)) 
			throw Util::Error("Bad parameter in file %s line %d pos %d", filename.c_str(), line, position);

		name = buf;

		int size = 1;
		bool res = ReadLexem(buf, sizeof(buf), len);
		if(!res)
			throw Util::Error("Invalid parameter value in file %s line %d pos %d", filename.c_str(), line, position);

		if( strcmp( buf, "=")==0)
		{
			if( !ReadLexem(buf, sizeof(buf), len)) 
				throw Util::Error("Invalid parameter value in file %s line %d pos %d", filename.c_str(), line, position);
		}
		else if(strcmp( buf, "[")==0)
		{
			if( !ReadLexem(buf, sizeof(buf), len)) return Param();
			size = atoi(buf);
			if( !ReadLexem(buf, sizeof(buf), len)) return Param();
			if(strcmp( buf, "]")!=0) 
				throw Util::Error("Invalid parameter value in file %s line %d pos %d", filename.c_str(), line, position);

			if( !ReadLexem(buf, sizeof(buf), len)) return Param();
			if( strcmp( buf, "=")==0)
			{
				if( !ReadLexem(buf, sizeof(buf), len)) return Param();
			}
		}
		bool bBracket = false;
		if( strcmp( buf, "{")==0)
		{
			bBracket = true;
			if( !ReadLexem(buf, sizeof(buf), len)) 
				throw Util::Error("Invalid parameter value in file %s line %d pos %d", filename.c_str(), line, position);
		}

		// �������������
		int count = size;
		Param param;
		switch(type)
		{
		case PT_STRING:
			{
				std::vector<std::string> stringlist;
				if( !loadStrings( stringlist, count, buf, sizeof(buf)))
					return Param();
				param = PSA(stringlist);
				break;
			}

		case PT_FLOAT: case PT_HALF: case PT_DOUBLE:
			{
				std::vector<float> floatlist;
				if( !loadFloats( floatlist, count, buf, sizeof(buf)))
					return Param();
				param = PA<float>(interp, &floatlist[0], size, type);
				break;
			}

		case PT_POINT:
		case PT_VECTOR: 
		case PT_NORMAL:
		case PT_COLOR:
			{
				count *= 3;
				std::vector<float> floatlist;
				if( !loadFloats( floatlist, count, buf, sizeof(buf)))
					return Param();
				param = PA<Math::Vec3f>(interp, (Math::Vec3f*)&floatlist[0], size, type);
				break;
			}

		case PT_MATRIX:
			{
				count *= 16;
				std::vector<float> floatlist;
				if( !loadFloats( floatlist, count, buf, sizeof(buf)))
					return Param();
				param = PA<Math::Matrix4f>(interp, (Math::Matrix4f*)&floatlist[0], size, type);
				break;
			}
			break;
		case PT_BOOL: 
			{
				std::vector<int> intlist;
				if( !loadInts( intlist, count, buf, sizeof(buf)))
					return Param();
				std::vector<char> boollist;
				boollist.resize(intlist.size());
				for(unsigned i=0; i<boollist.size(); i++)
					boollist[i] = (intlist[i]!=0)?1:0;
				param = PA<char>(interp, &boollist[0], size, type);
				break;
			}
		case PT_INT8: case PT_UINT8: 
			{
				std::vector<unsigned char> intlist;
				if( !loadInt8s( intlist, count, buf, sizeof(buf)))
					return Param();
				param = PA<unsigned char>(interp, &intlist[0], size, type);
				break;
			}
		case PT_INT16: case PT_UINT16: case PT_INT: case PT_UINT:
			{
				std::vector<int> intlist;
				if( !loadInts( intlist, count, buf, sizeof(buf)))
					return Param();
				param = PA<int>(interp, &intlist[0], size, type);
				break;
			}
		case PT_HPOINT:
			{
				count *= 4;
				std::vector<float> floatlist;
				if( !loadFloats( floatlist, count, buf, sizeof(buf)))
					return Param();
				param = PA<Math::Vec4f>(interp, (Math::Vec4f*)&floatlist[0], size, type);
				break;
			}
		case PT_BOX:
			{
				count *= 6;
				std::vector<float> floatlist;
				if( !loadFloats( floatlist, count, buf, sizeof(buf)))
					return Param();
				param = PA<Math::Box3f>(interp, (Math::Box3f*)&floatlist[0], size, type);
				break;
			}
		case PT_VECTOR2:
			{
				count *= 2;
				std::vector<float> floatlist;
				if( !loadFloats( floatlist, count, buf, sizeof(buf)))
					return Param();
				param = PA<Math::Vec2f>(interp, (Math::Vec2f*)&floatlist[0], size, type);
				break;
			}
		}
		if(param.empty())
			throw Util::Error("Invalid parameter value in file %s line %d pos %d", filename.c_str(), line, position);

		if(bBracket)
		{
			if( count!=0)
			{
				if( !ReadLexem(buf, sizeof(buf), len)) 
					throw Util::Error("Invalid parameter value in file %s line %d pos %d", filename.c_str(), line, position);
			}
			if( strcmp( buf, "}")!=0)
				throw Util::Error("Invalid parameter value in file %s line %d pos %d", filename.c_str(), line, position);
		}
		return param;
	}
	template <class STREAM>
	inline bool renderFileBaseImpl<STREAM>::loadStrings(
		std::vector<std::string>& stringlist, int count, 
		char* buf, int size)
	{
		if( !count) return true;
		int len;
		stringlist.reserve(count);
		for(int i=0; ; )
		{
			stringlist.push_back(buf);
			i++;
			if( i>=count) break;
			if( !ReadLexem(buf, size, len)) 
				throw Util::Error("Invalid string in file %s line %d pos %d", filename.c_str(), line, position);
		}
		return true;
	}
	template <class STREAM>
	inline bool renderFileBaseImpl<STREAM>::loadFloats(
		std::vector<float>& floatlist, int count, 
		char* buf, int size)
	{
		if( !count) return true;
		int len;
		floatlist.reserve(count);

		for(int i=0; ; )
		{
			floatlist.push_back( (float)atof(buf));
			i++;
			if( i>=count) break;
			if( !ReadLexem(buf, size, len)) 
				throw Util::Error("Invalid float in file %s line %d pos %d", filename.c_str(), line, position);
		}
		return true;
	}
	template <class STREAM>
	inline bool renderFileBaseImpl<STREAM>::loadInts(
		std::vector<int>& intlist, int count, 
		char* buf, int size)
	{
		if( !count) return true;
		int len;
		intlist.reserve(count);
		for(int i=0; i<count; )
		{
			intlist.push_back(atoi(buf));
			i++;
			if( i>=count) break;
			if( !ReadLexem(buf, size, len)) 
				throw Util::Error("Invalid int in file %s line %d pos %d", filename.c_str(), line, position);
		}
		return true;
	}
	template <class STREAM>
	inline bool renderFileBaseImpl<STREAM>::loadInt8s(
		std::vector<unsigned char>& intlist, int count, 
		char* buf, int size)
	{
		if( !count) return true;
		int len;
		intlist.reserve(count);
		for(int i=0; i<count; )
		{
			intlist.push_back(atoi(buf));
			i++;
			if( i>=count) break;
			if( !ReadLexem(buf, size, len)) 
				throw Util::Error("Invalid int in file %s line %d pos %d", filename.c_str(), line, position);
		}
		return true;
	}

	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::writeindent()
	{
		for(int i=0; i<this->indent; i++)
			file._fwrite( "\t", 1, 1);
	}

	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::binaryReadString(std::string& name)
	{
		int size;
		file._fread( &size, sizeof(size), 1);
		if(!size)
			name.clear();
		else
		{
			name.resize(size, ' ');
			file._fread( (char*)name.c_str(), size, 1);
		}
	}
	template <class STREAM>
	inline void renderFileBaseImpl<STREAM>::binaryWriteString(const char* name)
	{
		int size = name?(int)strlen(name):0;
		file._fwrite( &size, sizeof(size), 1);
		if(size)
		{
			file._fwrite( name, size, 1);
		}
	}

}