#pragma once
namespace cls
{
	template <class TRANSFORMSTACK> inline
	renderGelatoImpl<TRANSFORMSTACK>::renderGelatoImpl(
		GelatoAPI* prman
		)
	{
		this->prman = prman;
		this->blur_scenetime = 0;
		this->blur_shutterOpen = 0;
		this->blur_shutterClose = 0;

		subrender = NULL;
		
		bBlurFinal = false;

		attributeStack.resetMotion();
	}
	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::setBlurParams(float blur_scenetime, float blur_shutterOpen, float blur_shutterClose)
	{
		this->blur_scenetime	= blur_scenetime;
		this->blur_shutterOpen	= blur_shutterOpen;
		this->blur_shutterClose = blur_shutterClose;
	}

	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::setGelato( 
		GelatoAPI* prman
		)
	{
		this->prman = prman;
	}

	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::PushAttributes(void) 
	{
		if( subrender) 
			subrender->PushAttributes();
		else
		{
//			blurstack.push_back(blurstackItem());
			BASE::PushAttributes();
//			prman->PushAttributes();
//			prman->PushTransform();
		}
	}
	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::PopAttributes(void) 
	{
		if( subrender) 
			subrender->PopAttributes();
		else
		{
//			if( blurstack.size()<=1)
//				throw "renderGelatoImpl<TRANSFORMSTACK>::PopAttributes ������ �����";
//			blurstack.pop_back();
			BASE::PopAttributes();
//			prman->PopAttributes();
//			prman->PopTransform();
		}
	}

	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::PushTransform (void)
	{
		if( subrender) 
			subrender->PushTransform();
		else
		{
			BASE::PushTransform();
			prman->PushAttributes();
//			prman->PushTransform();
		}
	}
	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::PopTransform (void)
	{
		if( subrender) 
			subrender->PopTransform();
		else
		{
			BASE::PopTransform();
//			prman->PopTransform();
			prman->PopAttributes();
		}
	}
	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::SetTransform ()
	{
		if( subrender) 
			subrender->SetTransform();
		else
		{
			cls::P<Math::Matrix4f> M = GetParameter("transform");
			prman->SetTransform( M.data().data());
			BASE::SetTransform();
		}
	}
	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::AppendTransform ()
	{
		if( subrender) 
			subrender->AppendTransform();
		else
		{
			// �������� ����
			if(attributeStack.getMotionSamplesCount())
			{
				// ���������
				int motionSamplesCount = attributeStack.getMotionSamplesCount();
				float* motionTimes = attributeStack.getMotionTimes();
				std::vector<float> prmantimes(motionTimes, motionTimes+motionSamplesCount);
				for(int i=0; i<motionSamplesCount; i++)
					prmantimes[i] = (blur_shutterClose-blur_shutterOpen)*prmantimes[i] + blur_scenetime;
				prman->Motion(motionSamplesCount, &prmantimes[0]);

				for(i=0; i<motionSamplesCount; i++)
				{
					float t = motionTimes[i];
					attributeStack.setMotionParamPhase( t);
//					bool bOk = CallFilters(call);
//					if( !bOk) return;
					Math::Matrix4f M = Math::Matrix4f::id;
					GetParameter("transform", M);
					prman->AppendTransform( M.data());

					// ��� �����������!!!
					BASE::AppendTransform();
				}
//				prman->RiMotionEnd();
			}
			else
			{
//				bool bOk = CallFilters(call);
//				if( !bOk) return;
				Math::Matrix4f M = Math::Matrix4f::id;
				GetParameter("transform", M);
				prman->AppendTransform( M.data());
				BASE::AppendTransform();
			}
		}
	}

	//@{ 
	/// Motion blur
	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::MotionBegin()
	{
		attributeStack.resetMotion();
		/*/
		if( !bBlurFinal)
		{
			rci.clear();
			subrender = &rci;
		}
		else
		{
			float scenetime = 0;
			this->GetAttribute("cls::scenetime", scenetime);

			// ���������
			std::vector<float> times;
			times.assign( rci.getPhases(), rci.getPhases()+rci.getPhaseCount());
			for(unsigned i=0; i<times.size(); i++)
				times[i] += scenetime;
			prman->RiMotionBeginV(rci.getPhaseCount(), &times[0]);
		}
		/*/
	}
	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::MotionEnd()
	{
		attributeStack.setMotionParamPhase(nonBlurValue);
		/*/
		if( !bBlurFinal)
		{
			subrender = NULL;
			bBlurFinal = true;
			rci.Render(this);
			bBlurFinal = false;
		}
		else
		{
			prman->RiMotionEnd();
		}
		/*/
	}
	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::MotionPhaseBegin(float time)
	{
		attributeStack.addMotionParamPhase(time);
		attributeStack.setMotionParamPhase(time);
		/*/
		if( !bBlurFinal)
		{
//			subrender = &rci;
			rci.startPhase(time);
		}
		/*/
	}
	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::MotionPhaseEnd()
	{
		attributeStack.setMotionParamPhase(nonBlurValue);
		/*/
//		subrender = NULL;
		rci.endPhase();
		/*/
	}
	template <class TRANSFORMSTACK> inline
	bool renderGelatoImpl<TRANSFORMSTACK>::SetCurrentMotionPhase(float time)
	{
		return attributeStack.setMotionParamPhase(time);
	}

	//@}


	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::Shader(const char *shadertype)
	{
		if( subrender) 
			subrender->Shader(shadertype);
		else
		{
			// �������� ����
			if(attributeStack.getMotionSamplesCount())
			{
				// ���������
				int motionSamplesCount = attributeStack.getMotionSamplesCount();
				float* motionTimes = attributeStack.getMotionTimes();

				float t = motionTimes[0];
				attributeStack.setMotionParamPhase( t);
				singleShader(shadertype);
				/*/
				std::vector<float> prmantimes(motionTimes, motionTimes+motionSamplesCount);
				for(int i=0; i<motionSamplesCount; i++)
					prmantimes[i] = (blur_shutterClose-blur_shutterOpen)*prmantimes[i] + blur_scenetime;
				prman->RiMotionBeginV(motionSamplesCount, &prmantimes[0]);

				for(int i=0; i<motionSamplesCount; i++)
				{
					float t = motionTimes[i];
					attributeStack.setMotionParamPhase( t);

					singleShader(shadertype);
				}
				prman->RiMotionEnd();
				/*/

			}
			else
			{
				singleShader(shadertype);
			}

		}
	}

	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::Light()
	{
		if( subrender) 
			subrender->Light();
		else
		{
			/*/
			cls::PS lightid = GetParameter("light::id");
			cls::PS shadername = GetParameter("light::shader");
			if( shadername.empty())
				return;

			std::vector<RtToken> tokens;
			std::vector<std::string> tokennames;
			std::vector<RtPointer> parms;

			// ����� ��� ���������
			if( !BuildTokenList(tokens, tokennames, parms, 1, 1, 1))
				return;

			if( !lightid.empty())
			{
				const char* _lightid = lightid.data();
				tokennames.push_back("__handleid");
				parms.push_back( (RtPointer)&_lightid);
				CompleteTokenList(tokens, tokennames);
			}

			prman->RiLightSourceV( (RtToken)shadername.data(), tokens.size(), &tokens[0], &parms[0]);
			/*/
		}
	}

	/// ����� ��������� ���������� ������� ������
	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::SystemCall(
		enSystemCall call
		)
	{
		if( subrender) 
		{
			subrender->SystemCall(call);
			return;
		}
		bool bVisible = true;
		GetAttribute("visible", bVisible);
		if( !bVisible) return;

		// �������� ����
		if(attributeStack.getMotionSamplesCount())
		{
			// ���������
			int motionSamplesCount = attributeStack.getMotionSamplesCount();
			float* motionTimes = attributeStack.getMotionTimes();
			std::vector<float> prmantimes(motionTimes, motionTimes+motionSamplesCount);
			for(int i=0; i<motionSamplesCount; i++)
				prmantimes[i] = (blur_shutterClose-blur_shutterOpen)*prmantimes[i] + blur_scenetime;
			prman->Motion(motionSamplesCount, &prmantimes[0]);

			for(int i=0; i<motionSamplesCount; i++)
			{
				float t = motionTimes[i];
				attributeStack.setMotionParamPhase( t);

				bool bOk = CallFilters(call);
				if( !bOk) return;
				singleSystemCall(call);
			}
//			prman->RiMotionEnd();
		}
		else
		{
			bool bOk = CallFilters(call);
			if( !bOk) return;
			singleSystemCall(call);
		}
	}

	/// Call (���������������� �����)
	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::Call( 
		IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
		)
	{
//		if( subrender) 
//			subrender->Call( pProcedural);
//		else
		{
			pProcedural->Process(this);
		}
	}
	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::Call( 
		const char*	procname
		)
	{
		IProcedural* pProcedural = findProcedure(procname);
		if(!pProcedural)
			return;
		this->Call(pProcedural);
	}
/*/
		template <class TRANSFORMSTACK> inline
		RtVoid renderGelatoImpl<TRANSFORMSTACK>::rpi_subdivfunc(RtPointer ptr, RtFloat detail)
		{
			delayRenderStruct* drs = (delayRenderStruct*)ptr;
			// �������� ���������
			drs->prman->PushAttributes();
			paramlist_t::iterator it = drs->params.begin();
			for(;it != drs->params.end(); it++)
				drs->prman->ParameterV(it->first.c_str(), it->second);
			// �����
			drs->pDelayRender->Render(
				drs->prman, 
				drs->prman->attributeStack.getMotionSamplesCount(), 
				drs->prman->attributeStack.getMotionTimes());
			drs->prman->PopAttributes();
		}
		template <class TRANSFORMSTACK> inline
		RtVoid renderGelatoImpl<TRANSFORMSTACK>::rpi_freefunc(RtPointer ptr)
		{
			delayRenderStruct* drs = (delayRenderStruct*)ptr;
			drs->pDelayRender->release();
			delete drs;
		}
		inline void prman_copy(RtBound bound, const Math::Box3f& box)
		{
			bound[0] = box.min.x; 
			bound[1] = box.max.x; 
			bound[2] = box.min.y; 
			bound[3] = box.max.y; 
			bound[4] = box.min.z; 
			bound[5] = box.max.z;
		}
/*/

	/// Procedural (���������������� �����)
	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::RenderCall( 
		IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
		)
	{
//		stackItem& si = stack.back();
		bool bDelay = false;
		this->GetParameter("delay", bDelay);
//		if( !bDelay)
		{
			pProcedural->Render(this, attributeStack.getMotionSamplesCount(), attributeStack.getMotionTimes());
		}
/*/
		else
		{
			P<Math::Box3f> box = this->GetParameter("#bound");
			RtBound bound;
			prman_copy(bound, box.data());
			delayRenderStruct* drs = new delayRenderStruct;
			// ���������� ������� ���������
			GetParameterList(drs->params);
	//		drs->params = si.params;
			drs->pDelayRender = pProcedural;
			drs->prman = this;
			pProcedural->addref();
			prman->RiProcedural(drs, bound, rpi_subdivfunc, rpi_freefunc);
		}
/*/
	}
	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::RenderCall( 
		const char*	procname			//!< eiy oeacaiiia a DeclareProcBegin
		)
	{
		// ������� ���� �� �������� � ���������
		if( strcmp(procname, "Camera")==0)
		{
			Camera();
			return;
		}
		else if( strcmp(procname, "Output")==0)
		{
			Output();
			return;
		}
		else if( strcmp(procname, "FrameBegin")==0)
			return;
		else if( strcmp(procname, "FrameEnd")==0)
			return;
		else if( strcmp(procname, "WorldBegin")==0)
		{
			prman->World();
			return;
		}
		else if( strcmp(procname, "WorldEnd")==0)
		{
			PS cameraname("Camera");
			GetParameter("render::cameraname", cameraname);
			prman->Render(cameraname.data());
			return;
		}

		IProcedural* pProcedural = findProcedure(procname);
		if(!pProcedural)
			return;
		this->RenderCall(pProcedural);
	}


	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::SetupParametersToGelato(
		)
	{
		std::vector< std::pair< std::string, Param> > params;
		GetParameterList(params);
		std::vector< std::pair< std::string, Param> >::iterator it = params.begin();
		for( ; it != params.end(); it++)
		{
			if( it->first[0]=='#') 
				continue;

			Gelato::ParamInterp detail;

			const char* name = it->first.c_str();
			Param& p = it->second;
			switch(p->interp)
			{
			case PI_CONSTANT:
				detail = Gelato::INTERP_CONSTANT;
				break;
			case PI_VERTEX:
				detail = Gelato::INTERP_VERTEX;
				break;
			case PI_PRIMITIVE:
				detail = Gelato::INTERP_PERPIECE;
				break;
			case PI_PRIMITIVEVERTEX:
				detail = Gelato::INTERP_LINEAR;
				break;
			case PI_VARYING:
				detail = Gelato::INTERP_LINEAR;
				break;
			}
			switch(p->type)
			{
			case PT_STRING:
				prman->Parameter( name, Gelato::ParamType(Gelato::PT_STRING, detail), PS(p).data());
				break;
			case PT_FLOAT: case PT_HALF: case PT_DOUBLE:
				prman->Parameter( name, Gelato::ParamType(Gelato::PT_FLOAT, detail), PA<float>(p).data());
				break;
			case PT_POINT:
				prman->Parameter( name, Gelato::ParamType(Gelato::PT_POINT, detail), PA<Math::Vec3f>(p).data());
				break;
			case PT_VECTOR: 
				prman->Parameter( name, Gelato::ParamType(Gelato::PT_VECTOR, detail), PA<Math::Vec3f>(p).data());
				break;
			case PT_NORMAL:
				prman->Parameter( name, Gelato::ParamType(Gelato::PT_NORMAL, detail), PA<Math::Vec3f>(p).data());
				break;
			case PT_COLOR:
				prman->Parameter( name, Gelato::ParamType(Gelato::PT_COLOR, detail), PA<Math::Vec3f>(p).data());
				break;
			case PT_MATRIX:
				prman->Parameter( name, Gelato::ParamType(Gelato::PT_MATRIX, detail), PA<Math::Matrix4f>(p).data());
				break;
			case PT_INT8: case PT_UINT8: case PT_INT16: case PT_UINT16: case PT_INT: case PT_UINT:
				prman->Parameter( name, Gelato::ParamType(Gelato::PT_INT, detail), PA<int>(p).data());
				break;
			case PT_HPOINT:
				break;
			}
		}
	}

	/*/
	template <class TRANSFORMSTACK> inline
	bool renderGelatoImpl<TRANSFORMSTACK>::BuildTokenList(
		std::vector<RtToken>& tokens, 
		std::vector<std::string>& tokennames,
		std::vector<RtPointer>& parms, 
		int primcount, 
		int vertcount, 
		int privvertscount)
	{
		std::vector< std::pair< std::string, Param> > params;
		GetParameterList(params);

		int rc = params.size();
		tokens.reserve(rc);
		tokennames.reserve(rc);
		parms.reserve(rc);
		static std::list<RtString> strings;
		strings.clear();
		std::vector< std::pair< std::string, Param> >::iterator it = params.begin();
		for( ; it != params.end(); it++)
		{
			if( it->first[0]=='#') 
				continue;
			std::string name;
			Param& p = it->second;
			switch(p->interp)
			{
			case PI_CONSTANT:
				name += "constant ";
				break;
			case PI_VERTEX:
				name += "vertex ";
				break;
			case PI_PRIMITIVE:
				name += "face ";
				break;
			case PI_PRIMITIVEVERTEX:
				name += "facevarying ";
				break;
			case PI_VARYING:
				name += "varying ";
				break;
			}
			switch(p->type)
			{
			case PT_STRING:
				name += "string "; break;
			case PT_FLOAT: case PT_HALF: case PT_DOUBLE:
				name += "float "; break;
			case PT_POINT:
				name += "point "; break;
			case PT_VECTOR: 
				name += "vector "; break;
			case PT_NORMAL:
				name += "normal "; break;
			case PT_COLOR:
				name += "color "; break;
			case PT_MATRIX:
				name += "matrix "; break;
			case PT_INT8: case PT_UINT8: case PT_INT16: case PT_UINT16: case PT_INT: case PT_UINT:
				name += "int "; break;
				break;
			case PT_HPOINT:
				break;
			}

			name += it->first;
			tokennames.push_back( name);
			tokens.push_back( (RtToken)tokennames.back().c_str());
			if( p->type != PT_STRING)
			{
				parms.push_back(p.data());
			}
			else
			{
				// ��� �-�� �������!!
				char* val = p.data();
				strings.push_back( val);
				RtString* str = &strings.back();
				parms.push_back(str);
			}
		}
		
		return true;
	}
	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::CompleteTokenList(
		std::vector<RtToken>& tokens, 
		const std::vector<std::string>& tokennames
		)
	{
		tokens.resize(tokennames.size());
		for(unsigned i=0; i<tokens.size(); i++)
		{
			tokens[i] = (RtToken)tokennames[i].c_str();
		}
	}
	/*/







	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::singleSystemCall(enSystemCall call)
	{
		switch(call)
		{
		case SC_POINTS:
			renderPoints();
			break;
		case SC_CURVES:
			{
			PS interp, wrap;
			PA<int> nverts;
			IRender::GetParameter("#curves::interpolation", interp);
			IRender::GetParameter("#curves::wrap", wrap);
			IRender::GetParameter("#curves::nverts", nverts);
			renderCurves(interp.data(), wrap.data(), nverts);
			break;
			}
		case SC_PATCH:
			{
				renderPatch();
                break;
			}
		case SC_MESH:
			{
			PS interp;
			PA<int> loops;
			PA<int> nverts;
			PA<int> verts;
			IRender::GetParameter("mesh::interpolation", interp);
			IRender::GetParameter("mesh::loops", loops);
			IRender::GetParameter("mesh::nverts", nverts);
			IRender::GetParameter("mesh::verts", verts);
			RemoveParameter("mesh::interpolation");
			RemoveParameter("mesh::loops");
			RemoveParameter("mesh::nverts");
			RemoveParameter("mesh::verts");
			renderMesh(interp.data(), loops, nverts, verts);
			break;
			}
		case SC_SPHERE:
			{
			float radius;
			float zmin;
			float zmax;
			float thetamax;
			IRender::GetParameter("#sphere::radius", radius);
			IRender::GetParameter("#sphere::zmin", zmin);
			IRender::GetParameter("#sphere::zmax", zmax);
			IRender::GetParameter("#sphere::thetamax", thetamax);
			renderSphere(radius, zmin, zmax, thetamax);
			break;
			}
		case SC_BLOBBY:
			{
			PA<Math::Matrix4f> ellipsoids;
			IRender::GetParameter("blobby::ellipsoids", ellipsoids);
			renderBlobby(ellipsoids);
			break;
			}
		}
	}

	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::renderPoints(
		)
	{
		/*/
		{
			PA<Math::Vec3f> P;
			IRender::GetParameter("P", P);
			RemoveParameter("P");

			int npoints = P.size();
			std::vector<RtToken> tokens;
			std::vector<std::string> tokennames;
			std::vector<RtPointer> parms;

			// ����� ��� ���������
			if( !BuildTokenList(tokens, tokennames, parms, npoints, npoints, npoints))
				return;

			tokennames.push_back("P");
			parms.push_back( P.data());
			CompleteTokenList(tokens, tokennames);

			prman->RiPointsV(npoints, tokens.size(), &tokens[0], &parms[0]);
		}
		/*/
	}

	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::renderCurves( 
		const char *interp, 
		const char *wrap,		//!< wrap
		PA<int>& nverts			//!< int[ncurves] ����� ��������� � ������
		)
	{
		if( nverts.empty()) return;

		int vertsincurve = nverts[0];
		bool bCubic = strcmp(interp, "cubic")==0;
		if( bCubic)
			interp="catmull-rom";
		int privvertscount = 0, vertcount=0;
		for(int i=0; i<nverts.size(); i++)
			privvertscount += nverts[i];

		if( bCubic)
		{
			// ����������� ���������
			cls::PA<float> _width = GetParameter("#width");
			cls::PA<float> newparam;
			newparam.reserve(nverts.size()*vertsincurve);
			int fv = 0;
			for(int i=0; i<nverts.size(); i++)
			{
				newparam.push_back(_width[fv]);
				for(int v=0; v<vertsincurve-2;v++)
					newparam.push_back(_width[fv++]);
				newparam.push_back(_width[fv-1]);
			}
			prman->Parameter("vertex float width", (float*)newparam.data());
		}
		/*/
		cls::Param _width = GetParameter("#width");
		cls::PA<float> width;
		if( !_width.empty())
		{
			if( _width.size()==1)
			{
				cls::P<float> constantwidth(_width);
				if( !constantwidth.empty())
				{
					width.resize(privvertscount);
					for(int i=0; i<width.size(); i++)
						width[i] = constantwidth.data();
				}
			}
			else
			{
				width = _width;
			}
		}
		/*/

		// ����� ��� ���������
//		SetupParametersToGelato();
		{
			cls::PA<Math::Vec3f> P = GetParameter("P");
			prman->Parameter("vertex point P", P.data());
		}
		/*/
		cls::PA<float> width = GetParameter("#width");
		if(!width.empty())
			prman->Parameter("vertex float width", (float*)width.data());
		/*/

		prman->Curves(interp, nverts.size(), vertsincurve);
	}

	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::renderPatch()
	{
		int nu;
		int uorder;
		PA<float> uknot;
		float umin;
		float umax;
		int nv;
		int vorder;
		PA<float> vknot;
		float vmin;
		float vmax;

		GetParameter( "nu", nu );
		GetParameter( "uorder", uorder );
		GetParameter( "uknots", uknot );
		GetParameter( "umin", umin );
		GetParameter( "umax", umax );
		GetParameter( "nv", nv );
		GetParameter( "vorder", vorder );
		GetParameter( "vknots", vknot );
		GetParameter( "vmin", vmin );
		GetParameter( "vmax", vmax );

		RemoveParameter( "nu" );
		RemoveParameter( "uorder" );
		RemoveParameter( "uknots" );
		RemoveParameter( "umin" );
		RemoveParameter( "umax" );
		RemoveParameter( "nv" );
		RemoveParameter( "vorder" );
		RemoveParameter( "vknots" );
		RemoveParameter( "vmin" );
		RemoveParameter( "vmax" );

		SetupParametersToGelato();

		prman->Patch( nu, uorder, uknot.data(), umin, umax, nv, vorder, vknot.data(), vmin, vmax);
	}

	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::renderTrimCurve()
	{
		int nloops;
		PA<int> ncurves;
		PA<int> order;
		PA<float> knot;
		PA<float> min;
		PA<float> max;
		PA<int> n;
		PA<float> u;
		PA<float> v;
		PA<float> w;

		GetParameter( "nloops", nloops );
		GetParameter( "ncurves", ncurves );
		GetParameter( "order", order );
		GetParameter( "knot", knot );
		GetParameter( "min", min );
		GetParameter( "max", max );
		GetParameter( "n", n );
		GetParameter( "u", u );
		GetParameter( "v", v );
		GetParameter( "w", w );
		if( u.size()!=v.size() ||
			u.size()!=w.size())
				return;

		RemoveParameter( "nloops" );
		RemoveParameter( "ncurves" );
		RemoveParameter( "order" );
		RemoveParameter( "knot" );
		RemoveParameter( "min" );
		RemoveParameter( "max" );
		RemoveParameter( "n" );
		RemoveParameter( "u" );
		RemoveParameter( "v" );
		RemoveParameter( "w" );

		std::vector<float> uvw;
		uvw.reserve(u.size()*3);
		for(int i=0; i<u.size(); i++)
		{
			uvw.push_back( u[i]);
			uvw.push_back( v[i]);
			uvw.push_back( w[i]);
		}
		prman->TrimCurve(
			nloops, ncurves.data(), n.data(), order.data(), knot.data(), min.data(), max.data(), 
			&uvw[0] );
	}

	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::renderMesh(
		const char *interp,		//!< ��� ������������
		PA<int>& loops,			//!< loops per polygon
		PA<int>& nverts,		//!< ����� ��������� � �������� (size = polygoncount)
		PA<int>& verts			//!< ������� ��������� ��������� (size = facevertcount)
		)
	{
		SetupParametersToGelato();
		prman->Mesh( interp, nverts.size(), nverts.data(), verts.data());
	}

	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::renderSphere( float radius, float zmin, float zmax, float thetamax)
	{
		prman->Sphere(radius, zmin, zmax, thetamax);
	}
	/// Blobby
	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::renderBlobby( PA<Math::Matrix4f>& ellipsoids)
	{
		/*/
		{
			int s = ellipsoids.size();
			std::vector<int> codes;
			codes.reserve(s*2 + 2 + s);
			std::vector<float> flts;
			flts.reserve(s*16);

			for(int i=0; i<s; i++)
			{
				codes.push_back(1001);
				codes.push_back( flts.size());

				Math::Matrix4f& m = ellipsoids[i];
				for(int x=0; x<4; x++)
					for(int y=0; y<4; y++)
						flts.push_back(m[x][y]);
			}
			codes.push_back(0);
			codes.push_back(s);
			for(i=0; i<s; i++)
				codes.push_back(i);

			std::vector<RtToken> tokens;
			std::vector<std::string> tokennames;
			std::vector<RtPointer> parms;

			if( !BuildTokenList(tokens, tokennames, parms, 1, 1, 1))
				return;

			prman->RiBlobbyV(s, codes.size(), &codes[0], flts.size(), &flts[0], 0, 0, tokens.size(), &tokens[0], &parms[0]);
		}
		/*/
	}
	template <class TRANSFORMSTACK> inline
	void renderGelatoImpl<TRANSFORMSTACK>::singleShader(const char *shadertype)
	{
		cls::PS shadername = GetParameter("shader::name");
		cls::PS layername = GetParameter("shader::layer");
		if( shadername.empty())
			return;

		if( strcmp(shadertype, "surface")==0)
		{
			bool ignore = false;
			IRender::GetAttribute("file::ignoreSurface", ignore);
			if( !ignore)
				prman->Shader( shadertype, shadername.data());
		}

		if( strcmp(shadertype, "displacement")==0)
		{
			bool ignore = false;
			IRender::GetAttribute("file::ignoreDisplace", ignore);
			if( !ignore)
			{
/*/
				// ��� ���� ������ �����
				P<float> displacement_bound = GetParameter("displacement::bound");
				PS displacement_space = GetParameter("displacement::space");
				const char* coordinatesystem = "shader";
				float bound = 2;
				if( !displacement_bound.empty())
					bound = (displacement_bound).data();
				if( !displacement_space.empty())
					coordinatesystem = (RtString)displacement_space.data();

				prman->Attribute("displacementbound", "sphere", &bound, "coordinatesystem", &coordinatesystem, RI_NULL);
/*/
				prman->Shader( shadertype, shadername.data());
			}
		}
	}

	template <class TRANSFORMSTACK> inline
	bool renderGelatoImpl<TRANSFORMSTACK>::Camera()
	{
		Math::Matrix4f transform;
		if( !GetParameter("camera::transform", transform))
			return false;
//		transform.invert();
		float _near, _far;
		if( !GetParameter("camera::near", _near))
			return false;
		if( !GetParameter("camera::far", _far))
			return false;

		float fov;
		PS projection;
		if( !GetParameter("camera::fov", fov))
			return false;
		if( !GetParameter("camera::projection", projection))
			return false;

		PS cameraname("Camera");
		GetParameter("camera::name", cameraname);

		PA<float> screenwindow;
		GetParameter("camera::screen", screenwindow);
		if(screenwindow.size()!=4)
			return false;

		cls::PA<int> resolution;
		if( !GetParameter("camera::resolution", resolution))
			return false;
		if( resolution.size()!=2)
			return false;

		prman->PushTransform();
		prman->SetTransform(transform.data());
		prman->Parameter( "screen", screenwindow.data());
		prman->Parameter( "resolution", resolution.data());
		prman->Parameter( "projection", projection.data());
		prman->Parameter( "fov", fov);
		prman->Parameter( "near", _near);
		prman->Parameter( "far", _far);
//			int[2] limits:bucketsize
//			string bucketorder		horizontal
//			int[2] spatialquality
//			int temporalquality

		prman->Camera(cameraname.data());
		prman->PopTransform();

		return true;
	}
	template <class TRANSFORMSTACK> inline
	bool renderGelatoImpl<TRANSFORMSTACK>::Output()
	{
		/*/
		cls::PA<float> ppixelsample;
		if( !GetParameter("output::pixelsample", ppixelsample))
			return false;
		if( ppixelsample.size()!=2)
			return false;
		prman->Parameter("pixelsample", ppixelsample.data());
		/*/

		// filterwidth
		cls::PA<int> pixelfilter;
		if( !GetParameter("output::filtersize", pixelfilter))
			return false;
		if( pixelfilter.size()!=2)
			return false;
		float filterwidth[2];
		filterwidth[0] = (float)pixelfilter[0];
		filterwidth[1] = (float)pixelfilter[1];
		prman->Parameter("filterwidth", &filterwidth);
		
		// filter
		PS filtername = "box";
		if( !GetParameter("output::filtername", filtername))
			return false;
		prman->Parameter("filter", filtername.data());
		
		PS cameraname("Camera");
		GetParameter("output::cameraname", cameraname);

		prman->Output(
			"M:/public/images/untitled.tif", 
			"iv", "rgba", 
			cameraname.data());

//Parameter ("float[4] quantize", (0, 255, 0, 255))
//Parameter ("int remote", 1)

		return true;
	}
}