#pragma once

namespace cls
{
	inline CacheStorageSimple::Instruction::Instruction()
	{
		type = I_UNKNOWN;
	}
	inline CacheStorageSimple::Instruction::Instruction(const Instruction& arg)
	{
		this->type = arg.type;
		this->name = arg.name;
//		this->param = arg.param.copy();
		this->param = arg.param;
	}

	inline void CacheStorageSimple::clear()
	{
		instructiontree.clear();
		root = instructiontree.insert( instructiontree.end(), Instruction(I_UNKNOWN));
		cur = root.end();
	}
	inline void CacheStorageSimple::copy(const CacheStorageSimple& arg)
	{
		this->instructiontree = arg.instructiontree;
		this->root = this->instructiontree.begin();
		this->cur = this->root.end();
	}
	inline void CacheStorageSimple::setInstruction(enInstruction type)
	{
		setInstruction(type, "", cls::Param());
	}
	inline void CacheStorageSimple::setInstruction(enInstruction type, const char* name, cls::Param p)
	{
		if( type==I_ATTRIBUTE_BEGIN)
		{
			cur = cur.insert( Instruction(I_ATTRIBUTE_BEGIN));
			cur = cur.end();
			return;
		}
		cur.insert( Instruction(type, name, p));
		if( type==I_ATTRIBUTE_END)
		{
			cur = cur.parent();
			cur++;
		}
	}
	inline enInstruction CacheStorageSimple::getInstruction()
	{
		return readit->type;
	}
	inline const char* CacheStorageSimple::getInstructionName()
	{
		return readit->name.c_str();
	}
	inline cls::Param CacheStorageSimple::getInstructionParam()
	{
		return readit->param;
	}
	inline bool CacheStorageSimple::startInstruction()
	{
		readit = root;
		return readit.IsEnd();
	}
	inline bool CacheStorageSimple::nextInstruction()
	{
		readit++;
		return readit.IsEnd();
	}
	inline bool CacheStorageSimple::isEnd()
	{
		return readit.IsEnd();
	}
	inline void CacheStorageSimple::Dump() const
	{
		instructiontree_t::const_branch_iterator it = instructiontree.begin();
		for(;it != instructiontree.end(); it++)
		{
			for(int i=0; i<it.level(); i++)
				printf(" ");
			printf("%d\n", it->type);
		}
	}
}
