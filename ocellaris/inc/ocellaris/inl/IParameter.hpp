#pragma once

#include "Util/misc_switch_string.h"

namespace cls
{
	//! IParameter
	inline IParameter::IParameter()
	{
		refCount = 0;
		data = 0;
		reserved = 0;
		type = PT_UNKNOWN;
		interp = PI_UNKNOWN;
	}
	//! ~IParameter
	inline IParameter::~IParameter()
	{
		free();
	}
	inline bool IParameter::respecifyType(enParamType type)
	{
		if( this->type == PT_UNKNOWN)
			{this->type = type;return true;};
		if( this->type == PT_VECTOR && 
				(type==PT_POINT||type==PT_NORMAL||type==PT_COLOR))
			{this->type = type;return true;};
		return true;
	}
#ifdef OCS_MEMTEST
	inline int IParameter::memory(int inm)
	{
		static int mem = 0;
		mem += inm;
		return mem;
	}
#endif

	//! alloc ������������� ����� - ���������� ����� size*stride ����
	inline char* IParameter::alloc(int size)
	{
		if( this->reserved < size)
			free();
		else
			return this->data;
		if( size)
		{
#ifdef OCS_MEMTEST
			this->allocsize = size*stride;
			memory( this->allocsize);
#endif
			this->data = new char[size*stride];
			this->reserved = size;
		}
		return this->data;
	}
	//! ���������� 
	inline void IParameter::free()
	{
		if( data)
		{
#ifdef OCS_MEMTEST
			memory(-this->allocsize);
			this->allocsize = 0;
#endif
			delete data;
		}
		data = NULL;
		reserved = 0;
	}
	//! �������� ������
	inline void IParameter::addref()
	{
		refCount++;
	}
	//! ������� ������
	inline void IParameter::release()
	{
		refCount--;
		if( refCount<=0) 
			delete this;
	}













	template <class T>
	int pt_stride(const T& arg){return sizeof(T);}
	inline int pt_stride(enParamType type)
	{
		switch(type)
		{
		case PT_BOOL:	return sizeof(bool);
		case PT_INT:	return sizeof(int);
		case PT_INT8:	return sizeof(unsigned char);
		case PT_FLOAT: 	return sizeof(float);
		case PT_DOUBLE: return sizeof(double);
		case PT_POINT: return sizeof(Math::Vec3f);
		case PT_VECTOR: return sizeof(Math::Vec3f);
		case PT_NORMAL: return sizeof(Math::Vec3f);
		case PT_COLOR: return sizeof(Math::Vec3f);
		case PT_HPOINT: return sizeof(Math::Vec4f);
		case PT_MATRIX:	return sizeof(Math::Matrix4f);
		case PT_BOX:	return sizeof(Math::Box3f);
		case PT_VECTOR2:	return sizeof(Math::Vec2f);
		}
		return 0;
	}

	inline enParamType pt_type(const bool&){return PT_BOOL;}
	inline enParamType pt_type(const char&){return PT_INT8;}
	inline enParamType pt_type(const unsigned char&){return PT_INT8;}
	inline enParamType pt_type(const int&){return PT_INT;}
	inline enParamType pt_type(const unsigned int&){return PT_INT;}
	inline enParamType pt_type(const float&){return PT_FLOAT;}
	inline enParamType pt_type(const double&){return PT_DOUBLE;}
	inline enParamType pt_type(const Math::Vec3f&){return PT_VECTOR;}
	inline enParamType pt_type(const Math::Vec4f&){return PT_HPOINT;}
	inline enParamType pt_type(const Math::Matrix4f&){return PT_MATRIX;}
	inline enParamType pt_type(const Math::Box3f&){return PT_BOX;}
	inline enParamType pt_type(const std::string&){return PT_STRING;}
	inline enParamType pt_type(const Math::Vec2f&){return PT_VECTOR2;}

	inline const char* str_type(enParamType type)
	{
		switch(type)
		{
		case PT_VOID:
			return "void"; break;
		case PT_BOOL:
			return "bool"; break;
		case PT_STRING:
			return "string"; break;
		case PT_FLOAT: case PT_HALF: case PT_DOUBLE:
			return "float"; break;
		case PT_POINT:
			return "point"; break;
		case PT_VECTOR: 
			return "vector"; break;
		case PT_NORMAL:
			return "normal"; break;
		case PT_COLOR:
			return "color"; break;
		case PT_MATRIX:
			return "matrix"; break;
		case PT_INT8: case PT_UINT8: 
			return "int8"; break;
		case PT_INT16: case PT_UINT16: case PT_INT: case PT_UINT:
			return "int"; break;
			break;
		case PT_HPOINT:
			return "HPOINT"; break;
			break;
		case PT_BOX:
			return "box"; break;
			break;
		case PT_VECTOR2:
			return "vector2"; break;
			break;
		}
		return "unknown";
	}
	inline enParamType type_fromstr(const char* str)
	{
		static misc::switch_string::Token typeslist[] = {
			{"bool",	PT_BOOL		}, 
			{"string",	PT_STRING	}, 
			{"float", 	PT_FLOAT	}, 
			{"point",	PT_POINT	}, 
			{"vector",	PT_VECTOR	},
			{"normal", 	PT_NORMAL	}, 
			{"color",	PT_COLOR	}, 
			{"matrix", 	PT_MATRIX	}, 
			{"box", 	PT_BOX		}, 
			{"int", 	PT_INT		}, 
			{"int8", 	PT_INT8		}, 
			{"vector2", PT_VECTOR2	}, 
			{NULL, 		-1}};
		static misc::switch_string types(typeslist);
		int r = types.match(str);
		if( r<0) 
			return PT_UNKNOWN;
		return (enParamType)r;
	}


	inline const char* str_interpolation(enParamInterpolation type)
	{
		switch(type)
		{
		case PI_CONSTANT:
			return "uniform"; break;
		case PI_VERTEX:
			return "vertex"; break;
		case PI_PRIMITIVE:
			return "face"; break;
		case PI_PRIMITIVEVERTEX:
			return "facevarying"; break;
		case PI_VARYING:
			return "varying"; break;
		}
		return "unknown";
	}
	inline enParamInterpolation interpolation_fromstr(const char* str)
	{
		static misc::switch_string::Token interpslist[] = {
			{"unknown",	PI_UNKNOWN		}, 
			{"constant",PI_CONSTANT		}, 
			{"uniform",	PI_CONSTANT		}, 
			{"vertex", 	PI_VERTEX		}, 
			{"face",	PI_PRIMITIVE	}, 
			{"facevarying",	PI_PRIMITIVEVERTEX},
			{"varying",	PI_VARYING		},
			{NULL, 		-1}};
		static misc::switch_string interps(interpslist);

		int r = interps.match(str);
		if( r<0) 
			return PI_ERROR;
		return (enParamInterpolation)r;
	}








	inline Param::Param()
	{
		init(NULL);
	}
	//! 
	inline Param::Param(IParameter* body)
	{
		init(body);
	}
	//! 
	inline Param::Param(const Param& arg)
	{
		copylink(arg);
	}
	inline Param::Param(Param& arg, int offset, int size)
	{
		init((IParameter*)arg.pointer, offset, size);
	}
	inline Param::Param( enParamType _type, enParamInterpolation interp, int size)
	{
		int _stride = pt_stride(_type);
		if( !_stride) return;
		IParameter* pointer = new IParameter;
		pointer->stride = _stride;
		pointer->type   = _type;
		pointer->interp = interp;
		pointer->alloc(size);
		pointer->size = size;
		this->init(pointer);
	}
	//! 
	inline Param::~Param()
	{
		exit();
	}
	//! 
	inline Param& Param::operator=(const Param& arg)
	{
		exit();
		copylink(arg);
		return *this;
	}
	inline Param Param::copy() const
	{
		Param p;
		if( this->empty())
			return p;
		int s = this->size();
		if( this->pointer->type == PT_STRING)
		{
			std::vector<const char*> ptrs(s);
			int i=0;
			for(i=0; i<s; i++)
				ptrs[i] = stringdata(i);

			p.init_allocstring(this->pointer->interp, ptrs.empty()?NULL:&ptrs[0], s);
		}
		else
		{
			p.init_alloc(this->pointer->type, this->pointer->stride, this->pointer->interp, s);
			int bytes = s*this->pointer->stride;
			memcpy(p.data(), this->data(), bytes);
		}
		return p;
	}
	inline bool Param::equal(const Param& arg) const
	{
		if( this->empty())
		{
			if( arg.empty())
				return true;
			return false;
		}
		if( arg.empty())
			return false;
		if( this->pointer->type != arg->type)
			return false;
		if( this->pointer->interp != arg->interp)
			return false;
		if(sizebytes()!=arg.sizebytes())
			return false;
		if( this->pointer->type == PT_STRING)
		{
			int res = strcmp( this->data(), arg.data());
			return res==0;
		}
		else
		{
			int res = memcmp(this->data(), arg.data(), arg.sizebytes());
			return res==0;
		}
	}
	
	//! 
	inline const IParameter* Param::operator->() const
	{
		return pointer;
	}
	//! 
	inline IParameter* Param::operator->()
	{
		return pointer;
	}
	inline Param Param::operator [](int i)
	{
		return Param(*this, i, 1);
	}
	inline void Param::setInterpolation(enParamInterpolation inte)
	{
		if( pointer)
			pointer->interp = inte;
	}
	inline void Param::setType(enParamType ty)
	{
		if( pointer)
		{
			if( pt_stride(ty)==pointer->stride)
				pointer->type = ty;
		}
	}

	//! �� �����������?
	inline bool Param::empty() const
	{
		if(!pointer)
			return true;
		if(this->_size>=0)
			return this->_size==0;
		return pointer->size==0;
	}
	//! ����� ���������
	inline int Param::size() const
	{
		if(!pointer) return 0;
		if( this->_size>=0)
			return this->_size;
		return pointer->size;
	}
	//! ������ ������ � ������
	inline int Param::sizebytes() const
	{
		if( !pointer) return 0;
		return size()*pointer->stride;
	}
	//! stride
	inline int Param::stride() const
	{
		if( !pointer) return 0;
		return pointer->stride;
	}
	//! data
	inline char* Param::data()
	{
		if( !pointer) return 0;
		if( !pointer->data) return 0;
		return pointer->data + pointer->stride*this->offset;
	}
	//! const data
	inline const char* Param::data() const 
	{
		if( !pointer) return 0;
		if( !pointer->data) return 0;
		return pointer->data + pointer->stride*this->offset;
	}
	inline const char* Param::stringdata(int i) const 
	{
		if( empty()) 
			return NULL;
		if( i<0) return NULL;
		if( i>=size()) return NULL;
		const char* d = data();
		if(i==0)
			return d+(pointer->size-1)*sizeof(int);
		int offset = ((int*)d)[i-1];
		return d+offset;
	}

	template< class STREAM>
	inline void Param::_fwrite(STREAM& file) const
	{
		const char* d = data();
		filedata fd;
		if( pointer)
		{
			fd.type		= (char)pointer->type;
			fd.interp	= (char)pointer->interp;
			fd.stride	= (short)pointer->stride;
			fd.size = size();
		}
		else
		{
			fd.type		= (char)PT_UNKNOWN;
			fd.interp	= (char)PI_UNKNOWN;
			fd.stride	= (short)0;
			fd.size		= 0;
		}
		file._fwrite( &fd, sizeof(fd), 1);
		if(d)
		{
			if(fd.type != PT_STRING)
			{
				file._fwrite( d, fd.size*fd.stride, 1);
			}
			else
			{
				// ��� ����������!!! ���� ��������� offset � _size!!!!
				const char* sd = pointer->data;
				int ssize = pointer->reserved;
				// 
				file._fwrite( &ssize, sizeof(ssize), 1);
				file._fwrite( d, ssize, 1);
			}

		}
	}
	inline void Param::fwrite(FILE* file) const
	{
		const char* d = data();
		filedata fd;
		if( pointer)
		{
			fd.type		= (char)pointer->type;
			fd.interp	= (char)pointer->interp;
			fd.stride	= (short)pointer->stride;
			fd.size = size();
		}
		else
		{
			fd.type		= (char)PT_UNKNOWN;
			fd.interp	= (char)PI_UNKNOWN;
			fd.stride	= (short)0;
			fd.size		= 0;
		}
		::fwrite( &fd, sizeof(fd), 1, file);
		if(d)
		{
			if(fd.type != PT_STRING)
			{
				::fwrite( d, fd.size*fd.stride, 1, file);
			}
			else
			{
				// ��� ����������!!! ���� ��������� offset � _size!!!!
				const char* sd = pointer->data;
				int ssize = pointer->reserved;
				// 
				::fwrite( &ssize, sizeof(ssize), 1, file);
				::fwrite( d, ssize, 1, file);
			}

		}
	}
	template< class STREAM>
	inline Param Param::_fread(STREAM& file)
	{
		filedata fd;
		file._fread( &fd, sizeof(fd), 1);

		if( fd.stride==0)
			return Param();
		if( fd.type	== (char)PT_UNKNOWN)
			return Param();
		if( fd.interp == (char)PI_ERROR)
			return Param();

		IParameter* pointer = new IParameter;
		pointer->stride = fd.stride;
		pointer->type   = (enParamType)fd.type;
		pointer->interp = (enParamInterpolation)fd.interp;
		pointer->size = fd.size;
		int s = fd.size*fd.stride;
		if(s)
		{
			if(fd.type != PT_STRING)
			{
				pointer->alloc(fd.size);
				file._fread( pointer->data, fd.size*fd.stride, 1);
			}
			else
			{
				int s;
				file._fread( &s, sizeof(s), 1);
				pointer->alloc((int)s);
				file._fread( pointer->data, s, 1);
			}
		}
		return Param(pointer);
	}

	inline Param Param::fread(FILE* file)
	{
		filedata fd;
		::fread( &fd, sizeof(fd), 1, file);

		if( fd.stride==0)
			return Param();
		if( fd.type	== (char)PT_UNKNOWN)
			return Param();
		if( fd.interp == (char)PI_UNKNOWN)
			return Param();

		IParameter* pointer = new IParameter;
		pointer->stride = fd.stride;
		pointer->type   = (enParamType)fd.type;
		pointer->interp = (enParamInterpolation)fd.interp;
		pointer->size = fd.size;
		int s = fd.size*fd.stride;
		if(s)
		{
			if(fd.type != PT_STRING)
			{
				pointer->alloc(fd.size);
				::fread( pointer->data, fd.size*fd.stride, 1, file);
			}
			else
			{
				int s;
				::fread( &s, sizeof(s), 1, file);
				pointer->alloc(s);
				::fread( pointer->data, s, 1, file);
			}
		}
		return Param(pointer);
	}

	inline void Param::init(IParameter* body, int offset, int size)
	{
		pointer = body;
		this->offset = offset;
		this->_size   = size;
		if(pointer)
			pointer->addref();
	}
	//! �������� ������ ��������� � ��������� ������
	inline void Param::init_alloc(enParamType type, int stride, enParamInterpolation interp, int size)
	{
		exit();
		IParameter* pointer = new IParameter;
		pointer->stride = stride;
		pointer->type   = type;
		pointer->interp = interp;
		pointer->alloc(size);
		pointer->size = size;
		this->init(pointer);
	}
	//! �������� ������ ��������� � ��������� ������
	template <class T>
	inline void Param::init_alloc(const T& arg, enParamInterpolation interp, int size, enParamType _type)
	{
		exit();
		IParameter* pointer = new IParameter;
		pointer->stride = pt_stride(arg);
		if( _type==PT_UNKNOWN)
			_type = pt_type(arg);
		pointer->type   = _type;
		pointer->interp = interp;
		pointer->alloc(size);
		pointer->size = size;
		this->init(pointer);
	}

	// ������ ������ ����� ���:
	// ���� ������ size=1: pointer->data ��������� �� char* (������ NULL ��������������)
	// ����� ����� size=S:
	//		� ������ ������ int[S-1] �������� �� ������ �����
	//		����� ���� NULL �������������� ������
	// 
	inline void Param::init_allocstring(enParamInterpolation interp, const char** args, int size)
	{
		exit();
		std::vector<int> offsets(size>0?(size-1):0);
		int s = (int)offsets.size()*sizeof(int);
		int i=0;
		for(i=0; i<size; i++)
		{
			if(i!=0) offsets[i-1] = s;
			s += (int)strlen(args[i])+1;
		}

		IParameter* pointer = new IParameter;
		pointer->stride = 1;
		pointer->type   = PT_STRING;
		pointer->interp = interp;
		pointer->alloc(s);
		pointer->size = size;
		this->init(pointer);

		for( i=1; i<size; i++)
			((int*)pointer->data)[i-1] = offsets[i-1];
		for( i=0; i<size; i++)
			strcpy( stringdata(i), args[i]);

	}

	//! init
	inline void Param::copylink(const Param& arg)
	{
		this->init( (IParameter*)arg.pointer, arg.offset, arg._size);
	}

	//! exit
	inline void Param::exit()
	{
		if(pointer)
			pointer->release();
		pointer = NULL;
	}

	inline char* Param::stringdata(int i)
	{
		char* d = data();
		if(i==0)
			return d+(pointer->size-1)*sizeof(int);
		int offset = ((int*)d)[i-1];
		return d+offset;
	}





















	//! default
	template <class T>
	P<T>::P( enParamType wanttype):Param()
	{
		T arg;
		init_alloc(arg, PI_CONSTANT, 1, wanttype);
	}
	//! �� �������� ������, ����� ���� ��������� �-���� empty!!!
	template <class T>
	P<T>::P( const Param& src, enParamType wanttype):Param()
	{
		T arg;
		if( src.empty()) return;
		if( wanttype==PT_UNKNOWN)
		{
			wanttype = pt_type(arg);
			if( src->type==PT_COLOR && wanttype==PT_VECTOR)
				wanttype=PT_COLOR;
		}
		if( src->type != wanttype) return;
		if( src.size()!=1) return;
		this->copylink(src);
//			this->init((IParameter*)src.operator->(), src.offset, src._size);
	}
	//! �� ��������
	template <class T>
	P<T>::P(T arg, enParamType wanttype):Param()
	{
		init_alloc(arg, PI_CONSTANT, 1, wanttype);
		this->data() = arg;
	}
	//!
	template <class T>
	P<T>& P<T>::operator=(T& arg)
	{
		init_alloc(arg, PI_CONSTANT, 1, PT_UNKNOWN);
		this->data() = arg;
		return *this;
	}
	//! ������
	template <class T>
	const T& P<T>::data() const
	{
		return *(const T*)(Param::data());
	}
	//! ������
	template <class T>
	bool P<T>::data(T& output) const
	{
		if(this->empty()) return false;
		output = *(const T*)(Param::data());
		return true;
	}
	//! ������
	template <class T>
	T& P<T>::data()
	{
		return *(T*)(Param::data());
	}
	//! ������
	template <class T>
	const T& P<T>::operator*() const
	{
		return this->data();
	}
	template <class T>
	T& P<T>::operator*()
	{
		return this->data();
	}
	// ����� ��������?
	template <class T>
	bool P<T>::isValid() const
	{
		if(empty()) return false;
		if(size()!=1) return false;
		return true;
	}
	// 
	template <class T>
	void P<T>::get(T& val)
	{
		if(isValid())
			val = data();
	}

















	//! �� �������� ������, ����� ���� ��������� �-���� empty!!!
	template <class T>
	PA<T>::PA( const Param& src):Param(src)
	{
//			IParameter* p = src.pointer;//(IParameter*)src.operator->()
//			if( !p) return;
//			this->init(p);
	}
	//! default (�������� � �������� ������ ������)
	template <class T>
	PA<T>::PA( enParamInterpolation interp, int size, enParamType type):Param()
	{
		T arg;
		init_alloc(arg, interp, size, type);
	}
	//! �� ������� (�������� � �������� ������ ������)
	template <class T>
	PA<T>::PA( enParamInterpolation interp, const T* data, int size, cls::enParamType type):Param()
	{
		T arg;
		init_alloc(arg, interp, size, type);
		// ����������
		if( sizebytes())
			memcpy( this->data(), data, sizebytes());
	}
	//! �� vector-������� (�������� � �������� ������ ������)
	template <class T>
	PA<T>::PA( enParamInterpolation interp, const std::vector<T>& data, cls::enParamType type):Param()
	{
		T arg;
		init_alloc(arg, interp, (int)data.size(), type);
		// ����������
		if( sizebytes())
			memcpy( this->data(), &data[0], sizebytes());
	}
	//! ������������� ������ (�� �������� � �������� ������ ������)
	template <class T>
	PA<T>::PA( const PA<T>& arg, int offset, int size):Param()
	{
		init(arg.pointer, offset, size);
	}
	//! reserve
	template <class T>
	void PA<T>::reserve(int s)
	{
		if(pointer)
		{
			pointer->alloc(s);
		}
	}
	template <class T>
	int PA<T>::reserve_size() const
	{
		if(pointer)
			return pointer->reserved;
		return 0;
	}
	//! clear
	template <class T>
	void PA<T>::clear()
	{
		enParamInterpolation interp = pointer->interp;
		cls::enParamType type = pointer->type;

		this->exit();
		T arg;
		init_alloc(arg, interp, 0, type);
	}
	//! resize
	template <class T>
	void PA<T>::resize(int s)
	{
		// ��� �������� - ����� ����� IParameter ���� ���������!!!
		pointer->alloc(s);
		pointer->size = s;
	}
	//! push_back()
	template <class T>
	void PA<T>::push_back(const T& arg)
	{
		pointer->alloc(pointer->size+1);
		pointer->size = pointer->size+1;
		(*this)[pointer->size-1] = arg;
	}
	//! back()
	template <class T>
	T& PA<T>::back()
	{
		return (*this)[pointer->size-1];
	}
	//! operator []
	template <class T>
	const T& PA<T>::operator [](int i) const 
	{
		return *(this->data() + i);
	}
	//! operator []
	template <class T>
	T& PA<T>::operator [](int i)
	{
		return *(this->data() + i);
	}
	//! ������
	template <class T>
	const T* PA<T>::data() const
	{
		return (const T*)(Param::data());
	}
	//! ������
	template <class T>
	T* PA<T>::data()
	{
		return (T*)(Param::data());
	}

	//! ������
	template <class T>
		void PA<T>::data(std::vector<T>& out) const
	{
		out.resize(this->size());
		int i=0;
		for(i=0; i<this->size(); i++)
			out[i] = (*this)[i];
	}














	//! default
	inline PS::PS():Param()
	{
		const char* arg = "";
		init_allocstring(PI_CONSTANT, &arg, 1);
	}
	//! �� �������� ������, ����� ���� ��������� �-���� empty!!!
	inline PS::PS( const Param& src):Param()
	{
		enParamType wanttype=PT_STRING;
		if( src.empty()) return;
		if( src->type != wanttype) return;
		if( src.size()!=1) return;
		this->init((IParameter*)src.operator->());
	}
	//! �� ��������
	inline PS::PS(const char* a):Param()
	{
		const char* arg = a;
		init_allocstring(PI_CONSTANT, &arg, 1);
	}
	//! �� ��������
	inline PS::PS(const std::string& a)
	{
		const char* arg = a.c_str();
		init_allocstring(PI_CONSTANT, &arg, 1);
	}

	//!
	inline PS& PS::operator=(const char* a)
	{
		const char* arg = a;
		init_allocstring(PI_CONSTANT, &arg, 1);
		return *this;
	}
	//! ���������
	inline bool PS::operator==(const char* a)
	{
		return strcmp( PS::data(), a)==0;
	}
	inline bool PS::operator!=(const char* a)
	{
		return strcmp( PS::data(), a)!=0;
	}

	//! ������
	inline const char* PS::data() const
	{
		return Param::stringdata(0);
	}
	// ����� ��������?
	inline bool PS::isValid() const
	{
		if(empty()) return false;
		return true;
	}
	// ������ ������?
	inline bool PS::emptystring() const
	{
		if(empty()) return true;
		return data()[0]==0;
	}











	//! default
	inline PSA::PSA()
	{
		init_allocstring(PI_CONSTANT, NULL, 0);
	}
	//! �� �������� ������, ����� ���� ��������� �-���� empty!!!
	inline PSA::PSA( const Param& src)
	{
		enParamType wanttype=PT_STRING;
		if( src.empty()) return;
		if( src->type != wanttype) return;
		this->init((IParameter*)src.operator->());
	}
	//! �� �������� ������, ����� ���� ��������� �-���� empty!!!
	inline PSA::PSA( const std::vector<std::string>& arg )
	{
		set(arg);
	}
	inline PSA::PSA( const char** arg, int size )
	{
		set(arg, size);
	}
	inline PSA::PSA( const std::vector<const char*>& arg )
	{
		set(arg);
	}
	inline void PSA::set( const std::vector<std::string>& _arg )
	{
		std::vector<const char*> arg(_arg.size());
		int i;
		for(i=0; i<(int)_arg.size(); i++)
			arg[i] = _arg[i].c_str();
		Param::init_allocstring(PI_CONSTANT, arg.empty()?NULL:&arg[0], (int)arg.size());
	}
	inline void PSA::set( const std::vector<const char*>& arg )
	{
		Param::init_allocstring(PI_CONSTANT, arg.empty()?NULL:(const char **)&arg[0], (int)arg.size());
	}
	inline void PSA::set( const char** arg, int size )
	{
		Param::init_allocstring(PI_CONSTANT, arg, size);
	}


	//! operator []
	inline const char* PSA::operator [](int i) const
	{
		return stringdata(i);
	}

	inline const char** PSA::data( std::vector<const char*>& out) const
	{
		out.resize(this->size());
		int i;
		for(i=0; i<(int)out.size(); i++)
			out[i] = stringdata(i);
		return out.empty()?NULL:(const char**)&out[0];
	}
	inline void PSA::data( std::vector<std::string>& out) const
	{
		out.resize(this->size());
		int i;
		for(i=0; i<(int)out.size(); i++)
			out[i] = stringdata(i);
	}
}
