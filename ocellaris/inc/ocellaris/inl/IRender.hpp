#pragma once
#include "Util/misc_switch_string.h"

namespace cls
{


///////////////////////////////////////////////////////////////
/// Shaders


	inline void IRender::Shader(
		const char *shadertype, 
		const char *shadername,
		const char *layername)
	{
		if(shadername)
			Parameter("shader::name", shadername);
		if(layername)
			Parameter("shader::layer", layername);
		this->Shader(shadertype);
	}
	inline void IRender::Light(
		const char *lightid, 
		const char *shadername,
		const char *layername)
	{
		if(lightid)
			Parameter("light::id", lightid);
		if(shadername)
			Parameter("light::shader", shadername);
		if(layername)
			Parameter("light::name", layername);
		this->Light();
	}



////////////////////////////////////////////////
/// Transformations

	inline void IRender::SetTransform(const Math::Matrix4f& M)
	{
		this->Parameter("transform", M);
		this->SetTransform();
	}
	inline void IRender::SetTransform(const char *spacename)
	{
		this->ParameterFromAttribute("transform", spacename);
		this->SetTransform();
	}



//////////////////////////////////////////
// ������������� ������� (helper)
// 
	inline void IRender::Parameter(const char *typedname, const std::set<int>& val, enParamInterpolation interp)
	{
		PA<int> param(interp);
		param.reserve((int)val.size());
		std::set<int>::const_iterator it = val.begin();
		for( ; it != val.end(); it++)
		{
			param.push_back(*it);
		}
		this->Parameter(typedname, param);
	}
	inline bool IRender::GetParameter(const char *name, std::set<int>& val)
	{
		PA<int> p = this->GetParameterV(name);
		if(p.empty()) return false;

		for(int i=0; i<p.size(); i++)
			val.insert(p[i]);
		return true;
	}


	/////////////////////////////////////////////////////
	//! ������ ���������

	inline bool IRender::GetParameter(const char *name, bool& val)
	{
		P<bool> p = this->GetParameterV(name);
		if(p.empty()) return false;
		val = p.data();
		return true;
	};
	inline bool IRender::GetParameter(const char *name, int& val)
	{
		P<int> p = this->GetParameterV(name);
		if(p.empty()) return false;
		val = p.data();
		return true;
	};
	inline bool IRender::GetParameter(const char *name, float& val)
	{
		P<float> p = this->GetParameterV(name);
		if(p.empty()) return false;
		val = p.data();
		return true;
	};
	inline bool IRender::GetParameter(const char *name, Math::Vec2f& val)
	{
		P<Math::Vec2f> p = this->GetParameterV(name);
		if(p.empty()) return false;
		val = p.data();
		return true;
	}
	inline bool IRender::GetParameter(const char *name, Math::Vec3f& val)
	{
		P<Math::Vec3f> p = this->GetParameterV(name);
		if(p.empty()) return false;
		val = p.data();
		return true;
	}
	inline bool IRender::GetParameter(const char *name, Math::Box3f& val)
	{
		P<Math::Box3f> p = this->GetParameterV(name);
		if(p.empty()) return false;
		val = p.data();
		return true;
	}
	inline bool IRender::GetParameter (const char *name, const char* &val) 
	{ 
		PS p = this->GetParameterV(name);
		if(p.empty()) return false;
		val = p.data();
		return true;
	}
	inline bool IRender::GetParameter(const char *name, std::string& val)
	{
		PS p = this->GetParameterV(name);
		if(p.empty()) return false;
		val = p.data();
		return true;
	}
	inline bool IRender::GetParameter(const char *name, PS& val)
	{
		PS p = this->GetParameterV(name);
		if(p.empty()) return false;
		val = p;
		return true;
	}
	inline bool IRender::GetParameter(const char *name, Math::Matrix4f& val)
	{
		P<Math::Matrix4f> p = this->GetParameterV(name);
		if(p.empty()) return false;
		val = p.data();
		return true;
	}
	inline bool IRender::GetParameter(const char *name, P<float>& val)
	{
		P<float> v = this->GetParameterV(name);
		if(v.empty()) return false;
		val = v;
		return true;
	}
	inline bool IRender::GetParameter(const char *name, P<int>& val)
	{
		P<int> v = this->GetParameterV(name);
		if(v.empty()) return false;
		val = v;
		return true;
	}
	inline bool IRender::GetParameter (const char *name, PA<Math::Vec3f>& val)
	{ 
		PA<Math::Vec3f> v = this->GetParameterV(name);
		if(v.empty()) return false;
		val = v;
		return true;
	}
	inline bool IRender::GetParameter (const char *name, PA<float>& val)
	{ 
		PA<float> v = this->GetParameterV(name);
		if(v.empty()) return false;
		val = v;
		return true;
	}
	inline bool IRender::GetParameter (const char *name, PA<int>& val)
	{ 
		PA<int> v = this->GetParameterV(name);
		if(v.empty()) return false;
		val = v;
		return true;
	}
	inline bool IRender::GetParameter (const char *name, PA<Math::Matrix4f>& val)
	{ 
		PA<Math::Matrix4f> v = this->GetParameterV(name);
		if(v.empty()) return false;
		val = v;
		return true;
	}
	inline bool IRender::GetParameter(const char *name, PSA& val)
	{
		PSA v = this->GetParameterV(name);
		if(v.empty()) return false;
		val = v;
		return true;
	}
	inline bool IRender::GetParameter(const char *name, std::vector<std::string>& val)
	{
		PSA v = this->GetParameterV(name);
		if(v.empty()) return false;
		v.data(val);
		return true;
	}
	inline bool IRender::GetParameter(const char *name, std::vector<int>& val)
	{
		PA<int> v = this->GetParameterV(name);
		if(v.empty()) return false;
		v.data(val);
		return true;
	}
	inline bool IRender::GetParameter(const char *name, std::vector<Math::Vec2f>& val)
	{
		PA<Math::Vec2f> v = this->GetParameterV(name);
		if(v.empty()) return false;
		v.data(val);
		return true;
	}
	inline bool IRender::GetParameter(const char *name, std::vector<Math::Vec3f>& val)
	{
		PA<Math::Vec3f> v = this->GetParameterV(name);
		if(v.empty()) return false;
		v.data(val);
		return true;
	}









	inline bool IRender::GetAttribute (const char *name, bool& val)
	{
		Param p = GetAttributeV(name);
		if(p.empty()) return false;
		if(p->type != pt_type(val)) return false;
		if(p.size() != 1) return false;
		val = (P<bool>(p).data());
		return true; 
	}
	inline bool IRender::GetAttribute (const char *name, float &val) 
	{ 
		Param p = GetAttributeV(name);
		if(p.empty()) return false;
		if(p->type != pt_type(val)) return false;
		if(p.size() != 1) return false;
		val = (P<float>(p).data());
		return true; 
	}
	inline bool IRender::GetAttribute (const char *name, int &val) 
	{ 
		Param p = GetAttributeV(name);
		if(p.empty()) return false;
		if(p->type != pt_type(val)) return false;
		if(p.size() != 1) return false;
		val = (P<int>(p).data());
		return true; 
	}
	inline bool IRender::GetAttribute (const char *name, Math::Box3f& val)
	{
		Param p = GetAttributeV(name);
		if(p.empty()) return false;
		if(p->type != pt_type(val)) return false;
		if(p.size() != 1) return false;
		val = (P<Math::Box3f>(p).data());
		return true; 
	}
	inline bool IRender::GetAttribute(const char *name, Math::Matrix4f& val)
	{
		Param p = GetAttributeV(name);
		if(p.empty()) return false;
		if(p->type != pt_type(val)) return false;
		if(p.size() != 1) return false;
		val = (P<Math::Matrix4f>(p).data());
		return true; 
	}
	inline bool IRender::GetAttribute(const char *name, std::string &val)
	{
		PS p = this->GetAttributeV(name);
		if(p.empty()) return false;
		val = p.data();
		return true;
	}

	inline bool IRender::GetAttribute(const char *name, PSA& val)
	{
		val = this->GetAttributeV(name);
		if(val.empty()) return false;
		return true;
	}
	inline bool IRender::GetAttribute(const char *name, PA<Math::Matrix4f>& val)
	{
		PA<Math::Matrix4f> v = this->GetAttributeV(name);
		if(v.empty()) return false;
		val = v;
		return true;
	}




	//! AppendTransform
	inline void IRender::AppendTransform (const Math::Matrix4f& M) 
	{
		this->Parameter("transform", M);
		AppendTransform();
	}


	/// ����� ��������� ���������� ������� ������
	inline void IRender::SystemCall(
		const char* callname
		)
	{
		enSystemCall sc = str2SystemCall(callname);
		if( sc==SC_UNKNOWN)
			return;
		SystemCall( sc);
	}


	/// Geometry
	/// 0-D prims - point clouds
	inline void IRender::Points(
		PA<Math::Vec3f>& P			//!< points
		)
	{
		Parameter("P", P);
		SystemCall(SC_POINTS);
	}
	/// 1-D prims - lines, curves, hair
	inline void IRender::Curves(
		const char *interp,		//!< ��� ������������
		const char *wrap,		//!< wrap
		PA<int>& nverts			//!< int[ncurves] ����� ��������� � ������
		)
	{
		Parameter("#curves::interpolation", interp);
		Parameter("#curves::wrap", wrap);
		Parameter("#curves::nverts", nverts);
		SystemCall(SC_CURVES);
	}
//		virtual void Curves(int ncurves, int nvertspercurve, int order,
//							const float *knot, float vmin, float vmax) {}
	/// 2-D prims - rectangular patches (NURBS, bicubics, bilinears), and
	/// indexed face meshes (polys, polyhedra, subdivs)
	inline void IRender::Patch (const char *interp, int nu, int nv) 
	{
		Parameter("patch::interpolation", interp);
		Parameter("patch::nu", nu);
		Parameter("patch::nv", nv);
		SystemCall(SC_PATCH);
	}
	inline void IRender::Patch (int nu, int uorder, const float *uknot,
						float umin, float umax,
						int nv, int vorder, const float *vknot,
						float vmin, float vmax)
	{
		// ����������
		SystemCall(SC_PATCH);
	}
	/*/
	/// TrimCurve
	inline void IRender::TrimCurve (int nloops, const int *ncurves, const int *n,
							const int *order, const float *knot,
							const float *min, const float *max,
							const float *uvw) 
	{
		// ����������
		SystemCall(SC_TRIMCURVE);
	}
	/*/
	/// Mesh 
	inline void IRender::Mesh(
		const char *interp,		//!< ��� ������������
		PA<int>& loops,			//!< �����?
		PA<int>& nverts,		//!< ����� ��������� � �������� (size = polygoncount)
		PA<int>& verts			//!< ������� ��������� ��������� (size = facevertcount)
		) 
	{
		Parameter("mesh::interpolation", interp);
		Parameter("mesh::loops", loops);
		Parameter("mesh::nverts", nverts);
		Parameter("mesh::verts", verts);
		SystemCall(SC_MESH);
	}
	/// Sphere
	inline void IRender::Sphere(
		float radius, 
		float zmin, 
		float zmax,
		float thetamax) 
	{
		Parameter("#sphere::radius", radius);
		Parameter("#sphere::zmin", zmin);
		Parameter("#sphere::zmax", zmax);
		Parameter("#sphere::thetamax", thetamax);
		SystemCall(SC_SPHERE);
	}
	/// Sphere
	inline void IRender::Sphere (float radius) 
	{ 
		Parameter("#sphere::radius", radius);
		Parameter("#sphere::zmin", -radius);
		Parameter("#sphere::zmax", radius);
		Parameter("#sphere::thetamax", 360.f);
		SystemCall(SC_SPHERE);
	}
	/// Blobby
	inline void IRender::Blobby(
		PA<Math::Matrix4f>& ellipsoids
		)
	{
		Parameter("blobby::ellipsoids", ellipsoids);
		SystemCall(SC_BLOBBY);
	}
}
