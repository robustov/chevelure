#pragma once
namespace cls
{
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::renderBaseCacheImpl(
		)
	{
		clear();
	}
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::renderBaseCacheImpl(
		const renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>& arg
		)
	{
		this->copy(arg);
	}
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::copy(
		const renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>& arg
		)
	{
		storage.copy(arg.storage);
	}
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>& renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::operator=(
		const renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>& arg)
	{
		this->copy(arg);
		return *this;
	}

	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::clear(
		)
	{
		base::clear();
		storage.clear();
	}

	//! ��������� ���������� (������ ��� ��������� �������)
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::comment(
		const char* text
		)
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_COMMENT, text, cls::Param());
		base::comment(text);
	}

	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::IfAttribute(const char* name, const Param& t)
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_IFATTRIBUTE, name, t);
		base::IfAttribute(name, t);
	}
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::EndIfAttribute()
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_ENDIFATTRIBUTE);
		base::EndIfAttribute();
	}

	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::PushAttributes(void) 
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_ATTRIBUTE_BEGIN);
		base::PushAttributes();
	}
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::PopAttributes(void) 
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_ATTRIBUTE_END);
		base::PopAttributes();
	}
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::PushTransform (void)
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_PUSHTRANSFORM);
		base::PushTransform();
	}
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::PopTransform (void)
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_POPTRANSFORM);
		base::PopTransform();
	}
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::SetTransform ()
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_SETTRANSFORM);
		base::SetTransform();
	}
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::AppendTransform ()
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_APPENDTRANSFORM);
		base::AppendTransform();
	}
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::PinTransform ()
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_PINTRANSFORM);
		base::PinTransform();
	}


	///////////////////////////////////////////////////////////////
	/// RenderAttributes - ��� ����������: ������ Begin/End Attributes
	/// PushRenderAttributes/PopRenderAttributes
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::PushRenderAttributes(void)
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_PUSHRENDERATTRIBUTE);
	}
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::PopRenderAttributes(void)
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_POPRENDERATTRIBUTE);
	}


	/// Motion blur
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::MotionBegin()
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_MOTIONBEGIN);
	}
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::MotionEnd()
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_MOTIONEND);
	}
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::MotionPhaseBegin(float time)
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_MOTIONPHASEBEGIN, "", P<float>(time));
	}
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::MotionPhaseEnd()
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_MOTIONPHASEEND);
	}

	//! ��������
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::ParameterV(
		const char *name,		//!< ��� ���������
		const Param& t			//!< ��������
		)
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_PARAMETER, name, t);
		base::ParameterV(name, t);
	}
	//! ������ ���������
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline Param renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::GetParameterV(
		const char *name
		)
	{
		return base::GetParameterV(name);
	}
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline bool renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::RemoveParameter(
		const char *name
		)
	{
		return false;
	}
	//! �������� �� ��������
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::ParameterFromAttribute(
		const char *paramname,		//!< ��� ���������
		const char *attrname		//!< ��� �������� �� �������� ����� ������
		)
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_PARAMETERFROMATTR, paramname, PS(attrname));
	}
	//! �������� �� ��������
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::ParameterToAttribute(
		const char *paramname,		//!< ��� ���������
		const char *attrname		//!< ��� �������� �� �������� ����� ������
		)
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_PARAMETERTOATTR, paramname, PS(attrname));
	}
	//! �������� �� ��������
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::AttributeFromAttribute(
		const char *dstname,		//!< ��� ���������
		const char *srcname			//!< ��� �������� �� �������� ����� ������
		)
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_ATTRIBUTEFROMATTR, dstname, PS(srcname));
	}

	//! �������
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::AttributeV(
		const char *name,		//!< ��� ���������
		const Param& t
		)
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_ATTRIBUTE, name, t);
		base::AttributeV(name, t);
	}

	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::Shader(const char *shadertype)
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_SHADER, shadertype, Param());
	}
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::Light()
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_LIGHT);
	}

	/// ����� ��������� ���������� ������� ������
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::SystemCall(
		enSystemCall call
		)
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_SYSTEMCALL, "", P<int>(call));
	}

	/// ���������� ���������
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline IRender* renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::DeclareProcBegin(
		const char* name
		)
	{
		// ������ ����������� ���������
		return this;
	}
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::DeclareProcEnd(
		)
	{
		// ��������� ����������� ���������
	}

	/// Call (���������������� �����)
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::Call( 
		IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
		)
	{
		if(!isNormalExe()) return;

//		pProcedural->Render(this);
	}
	/// Call (���������������� �����)
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::Call( 
		const char*	procname			//!< ��� ��������� � DeclareProcBegin
		)
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_CALL, procname, Param());
	}

	/// Procedural (���������������� �����)
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::RenderCall( 
		IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
		)
	{
		if(!isNormalExe()) return;

	}

	/// Procedural (���������������� �����)
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::RenderCall( 
		const char*	procname			//!< ����� ������� IProcedural::Render()
		)
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_RENDERCALL, procname, Param());
	}

	// ������
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::Filter( 
		IFilter* filter					//!< ��� ��������� � DeclareProcBegin ��� dllname@procname
		)
	{
		if(!isNormalExe()) return;

		// ��� ��� ������?
	}
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::Filter( 
		const char*	filter				//!< ��� ��������� � DeclareProcBegin ��� dllname@procname
		)
	{
		if(!isNormalExe()) return;

		storage.setInstruction(I_FILTER, filter, Param());
	}

	// ��������� ��� �� ������
	template< class STORAGE, class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::Render(
		IRender* render)const
	{
		storage.startInstruction();

		for( ;!storage.isEnd(); storage.nextInstruction())
		{
			enInstruction type = storage.getInstruction();
			switch(type)
			{
				case I_ATTRIBUTE_BEGIN:
					render->PushAttributes();
					break;
				case I_ATTRIBUTE_END:
					render->PopAttributes();
					break;
				case I_PARAMETER:
					render->ParameterV(storage.getInstructionName(), storage.getInstructionParam());
					break;
				case I_ATTRIBUTE:
					render->AttributeV(storage.getInstructionName(), storage.getInstructionParam());
					break;
				case I_PARAMETERFROMATTR:
					{
						cls::PS attrname = storage.getInstructionParam();
						render->ParameterFromAttribute(storage.getInstructionName(), attrname.data());
						break;
					}
				case I_PARAMETERTOATTR:
					{
						cls::PS attrname = storage.getInstructionParam();
						render->ParameterToAttribute(storage.getInstructionName(), attrname.data());
						break;
					}
				case I_ATTRIBUTEFROMATTR:
					{
						cls::PS attrname = storage.getInstructionParam();
						render->AttributeFromAttribute(storage.getInstructionName(), attrname.data());
						break;
					}
				case I_IFATTRIBUTE:
					render->IfAttribute(storage.getInstructionName(), storage.getInstructionParam());
					break;
				case I_ENDIFATTRIBUTE:
					render->EndIfAttribute();
					break;
				case I_PUSHTRANSFORM:
					render->PushTransform();
					break;
				case I_POPTRANSFORM:
					render->PopTransform();
					break;
				case I_SETTRANSFORM:
					render->SetTransform();
					break;
				case I_APPENDTRANSFORM:
					render->AppendTransform();
					break;
				case I_PINTRANSFORM:
					render->PinTransform();
					break;
				case I_PUSHRENDERATTRIBUTE:
					render->PushRenderAttributes();
					break;
				case I_POPRENDERATTRIBUTE:
					render->PopRenderAttributes();
					break;
				case I_SYSTEMCALL:
					{
						P<int> en(storage.getInstructionParam());
						render->SystemCall( (enSystemCall)en.data());
						break;
					}
				case I_CALL:
					{
						const char* proc = storage.getInstructionName();
						render->Call(proc);
						break;
					}
					break;
				case I_RENDERCALL:
					{
						const char* proc = storage.getInstructionName();
						render->RenderCall(proc);
						break;
					}
				case I_FILTER:
					{
						const char* proc = storage.getInstructionName();
						render->Filter(proc);
						break;
					}
					break;

				case I_MOTIONBEGIN:
					render->MotionBegin();
					break;
				case I_MOTIONEND:
					render->MotionEnd();
					break;
				case I_MOTIONPHASEBEGIN:
					{
						float time = P<float>(storage.getInstructionParam()).data();
						render->MotionPhaseBegin(time);
						break;
					}
				case I_MOTIONPHASEEND:
					render->MotionPhaseEnd();
					break;
				case I_SHADER:
					{
						const char* shadertype = storage.getInstructionName();
						render->Shader(shadertype);
					}
					break;
				case I_LIGHT:
					render->Light();
					break;
				case I_COMMENT:
					{
						const char* text = storage.getInstructionName();
						render->comment(text);
						break;
					}
			}
		}
	}
}