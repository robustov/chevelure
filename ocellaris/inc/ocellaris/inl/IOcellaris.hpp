
#include "Util\misc_switch_string.h"

namespace cls
{

	inline const char* SystemCall2str(enSystemCall type)
	{
		switch(type)
		{
		case SC_POINTS:		return "Points";
		case SC_CURVES:		return "Curves";
		case SC_PATCH:		return "Patch";
//		case SC_TRIMCURVE:	return "TrimCurve";
		case SC_MESH:		return "Mesh";
		case SC_SPHERE:		return "Sphere";
		case SC_BLOBBY:		return "Blobby";
		case SC_TEXT:		return "Text";
		}
		return "unknown";
	}
	inline enSystemCall str2SystemCall(const char* str)
	{
		static misc::switch_string::Token semslist[] = {
			{"Points",				SC_POINTS		}, 
			{"Curves",				SC_CURVES		}, 
			{"Patch",				SC_PATCH		}, 
//			{"TrimCurve",			SC_TRIMCURVE	},
			{"Mesh",				SC_MESH			},
			{"Sphere",				SC_SPHERE		},
			{"Blobby",				SC_BLOBBY		},
			{"Text",				SC_TEXT			},
			
			{NULL, 		-1}};
		static misc::switch_string sems(semslist);
		int code = sems.match(str);
		if(code==-1)
			return SC_UNKNOWN;
		return (enSystemCall)code;
	}

	inline const char* Instruction2str(enInstruction type)
	{
		switch(type)
		{
			case I_ATTRIBUTE_BEGIN:		return "{";
			case I_ATTRIBUTE_END:		return "}";				
			case I_PARAMETER:			return "Parameter";			
			case I_ATTRIBUTE:			return "Attribute";
			case I_PARAMETERFROMATTR:	return "ParameterFromAttribute";
			case I_ATTRIBUTEFROMATTR:	return "AttributeFromAttribute";
			case I_PARAMETERTOATTR:		return "ParameterToAttribute";
			
			case I_PUSHTRANSFORM:		return "PushTransform";
			case I_POPTRANSFORM:		return "PopTransform";
			case I_SETTRANSFORM:		return "SetTransform";	
			case I_APPENDTRANSFORM:		return "AppendTransform";
			case I_PINTRANSFORM:		return "PinTransform";

			case I_PUSHRENDERATTRIBUTE:	return "PushRenderAttributes";
			case I_POPRENDERATTRIBUTE:	return "PopRenderAttributes";

			case I_SYSTEMCALL:			return "SystemCall";

			case I_CALL:				return "Call";
			case I_RENDERCALL:			return "RenderCall";
			case I_FILTER:				return "Filter";			
			
			case I_MOTIONBEGIN:			return "MotionBegin";
			case I_MOTIONEND:			return "MotionEnd";	
			case I_MOTIONPHASEBEGIN:	return "MotionPhaseBegin";
			case I_MOTIONPHASEEND:		return "MotionPhaseEnd";
			
			case I_SHADER:				return "Shader";
			case I_LIGHT:				return "Light";

			case I_IFATTRIBUTE:			return "IfAttribute";
			case I_ENDIFATTRIBUTE:		return "EndIfAttribute";

			case I_DECLAREPROCBEGIN:	return "DeclareProcBegin";
			case I_DECLAREPROCEND:		return "DeclareProcEnd";

		}
		return "Unknown";
	}
	inline enInstruction str2Instruction(const char* str)
	{
		static misc::switch_string::Token semslist[] = {
			{"{",					I_ATTRIBUTE_BEGIN	}, 
			{"}", 					I_ATTRIBUTE_END		}, 
			{"Parameter",			I_PARAMETER			}, 
			{"Attribute",			I_ATTRIBUTE			},
			{"ParameterFromAttribute", I_PARAMETERFROMATTR},
			{"AttributeFromAttribute", I_ATTRIBUTEFROMATTR},
			{"ParameterToAttribute", I_PARAMETERTOATTR},

			{"PushTransform",		I_PUSHTRANSFORM		},
			{"PopTransform",		I_POPTRANSFORM		},
			{"SetTransform",		I_SETTRANSFORM		},
			{"AppendTransform",		I_APPENDTRANSFORM	},
			{"PinTransform",		I_PINTRANSFORM		},

			{"PushRenderAttributes", I_PUSHRENDERATTRIBUTE}, 
			{"PopRenderAttributes", I_POPRENDERATTRIBUTE}, 

			{"SystemCall", 			I_SYSTEMCALL		}, 
			/*/
			{"Points", 				I_POINTS			}, 
			{"Curves",				I_CURVES			}, 
			{"Mesh", 				I_MESH				}, 
			{"Sphere", 				I_SPHERE			}, 
			{"Blobby", 				I_BLOBBY			}, 
			/*/
			{"Call", 				I_CALL				}, 
			{"RenderCall",			I_RENDERCALL		}, 
			{"Filter",				I_FILTER			}, 

			{"MotionBegin",			I_MOTIONBEGIN		}, 
			{"MotionEnd",			I_MOTIONEND			}, 
			{"MotionPhaseBegin",	I_MOTIONPHASEBEGIN	}, 
			{"MotionPhaseEnd",		I_MOTIONPHASEEND	}, 

			{"Shader",				I_SHADER			}, 
			{"Light",				I_LIGHT				}, 

			{"IfAttribute",			I_IFATTRIBUTE		}, 
			{"EndIfAttribute",		I_ENDIFATTRIBUTE	}, 

			{"DeclareProcBegin",	I_DECLAREPROCBEGIN	}, 
			{"DeclareProcEnd",		I_DECLAREPROCEND	}, 

			{NULL, 		-1}};
		static misc::switch_string sems(semslist);

		int code = sems.match(str);
		if(code==-1)
			return I_UNKNOWN;
		return (enInstruction)code;
	}
}

