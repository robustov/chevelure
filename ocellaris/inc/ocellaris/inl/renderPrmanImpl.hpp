#pragma once

#include "math/safevector.h"

namespace cls
{
	template <class TRANSFORMSTACK> inline
	renderPrmanImpl<TRANSFORMSTACK>::renderPrmanImpl(
		IPrman* prman
		)
	{
//printf("new renderPrmanImpl %x\n", this);
		this->bDump = false;
		this->prman = prman;
		this->blur_scenetime = 0;
		this->blur_shutterOpen = 0;
		this->blur_shutterClose = 1;

		subrender = NULL;
		
		bBlurFinal = false;

		attributeStack.resetMotion();
	}
	template <class TRANSFORMSTACK> inline
	renderPrmanImpl<TRANSFORMSTACK>::~renderPrmanImpl(
		)
	{
//printf("delete renderPrmanImpl %x\n", this);
	}

	// ���������� ������, ��������� ��������� � ��������
	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::startFrom(
		const renderPrmanImpl<TRANSFORMSTACK>& src
		)
	{
		this->prman					= src.prman;				
		this->bDump					= src.bDump;				
		this->blur_scenetime		= src.blur_scenetime;	
		this->blur_shutterOpen		= src.blur_shutterOpen;	
		this->blur_shutterClose		= src.blur_shutterClose;	

//		paramlist_t params;
//		paramlist_t attributes;
//		renderPrmanImpl<TRANSFORMSTACK>* psrc = (renderPrmanImpl<TRANSFORMSTACK>*)&src;
//		psrc->GetParameterList(params);
//		psrc->GetAttributeList(attributes);

/*/
//		this->PushAttributes();
		{
			paramlist_t::iterator it = attributes.begin();
			for(;it != attributes.end(); it++)
				this->AttributeV(it->first.c_str(), it->second);
		}
		{
			paramlist_t::iterator it = params.begin();
			for(;it != params.end(); it++)
				this->ParameterV(it->first.c_str(), it->second);
		}
//		this->PopAttributes();
/*/

		transformStack.copyFrom( src.transformStack);
		attributeStack.copyFrom( src.attributeStack);

		proclist = src.proclist;
		filteritemlist = src.filteritemlist;
	}

	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::setBlurParams(float blur_scenetime, float blur_shutterOpen, float blur_shutterClose)
	{
		this->blur_scenetime	= blur_scenetime;
		this->blur_shutterOpen	= blur_shutterOpen;
		this->blur_shutterClose = blur_shutterClose;
	}

	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::setPrman( 
		IPrman* prman
		)
	{
		this->prman = prman;
	}

	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::PushAttributes(void) 
	{
		if(!isNormalExe()) return;

		if( subrender) 
			subrender->PushAttributes();
		else
		{
//			blurstack.push_back(blurstackItem());
			BASE::PushAttributes();
//			prman->RiAttributeBegin();
		}
	}
	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::PopAttributes(void) 
	{
		if(!isNormalExe()) return;

		if( subrender) 
			subrender->PopAttributes();
		else
		{
//			if( blurstack.size()<=1)
//				throw "renderPrmanImpl<TRANSFORMSTACK>::PopAttributes ������ �����";
//			blurstack.pop_back();
			BASE::PopAttributes();
//			prman->RiAttributeEnd();
		}
	}
	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::AttributeV(
		const char *name,		//!< ��� ���������
		const Param& t
		)
	{
		if(!isNormalExe()) return;
		if( t.empty()) return;

		BASE::AttributeV(name, t);

		const char* attrs[] = {
			"visibility::", 
			"trace::",
			"dice::",
			"photon::",
			"stochastic::",
			"derivatives::",
			"user::",
			NULL
		};
		for(int tag=0; attrs[tag]; tag++)
		{
			const char* tag__ = attrs[tag];
			const size_t tag__len = strlen(tag__);

			if( strncmp(name, tag__, tag__len)!=0)
				continue;

			std::string attrname( tag__, tag__len-2);

			// ������� ����������:
			std::string prmanname = str_type(t->type);
			prmanname += " ";
			prmanname += name+tag__len;

			// 
			if(t->type!=cls::PT_STRING)
			{
				prman->RiAttribute((char*)attrname.c_str(), prmanname.c_str(), t.data(), NULL);
			}
			else
			{
				PS val(t);
				RtString* str = (RtString*)val.data();
				prman->RiAttribute((char*)attrname.c_str(), prmanname.c_str(), &str, NULL);
			}
//printf("prman->RiAttribute %s %s\n", attrname.c_str(), prmanname.c_str());
			return;
		}

		if( strcmp(name, "sides")==0)
		{
			P<int> val(t);
			if( val.empty()) return;
			prman->RiSides(val.data());
			return;
		}
		if( strcmp(name, "shadingInterpolation")==0)
		{
			PS val(t);
			if( val.empty()) return;
			prman->RiShadingInterpolation( (char*)val.data());
			return;
		}
		if( strcmp(name, "reverseOrientation")==0)
		{
			P<int> val(t);
			if( val.empty()) return;
			if( val.data())
				prman->RiReverseOrientation();
			return;
		}
		if( strcmp(name, "name")==0)
		{
			if( t->type==cls::PT_STRING)
			{
				PS val(t);
				RtString* str = (RtString*)val.data();
				prman->RiAttribute("identifier", "string name", &str, NULL);
			}
			return;
		}
	}

	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::PushTransform (void)
	{
		if(!isNormalExe()) return;

		if( subrender) 
			subrender->PushTransform();
		else
		{
			BASE::PushTransform();
//			prman->RiAttributeBegin();
			prman->RiTransformBegin();
		}
	}
	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::PopTransform (void)
	{
		if(!isNormalExe()) return;

		if( subrender) 
			subrender->PopTransform();
		else
		{
			BASE::PopTransform();
			prman->RiTransformEnd();
//			prman->RiAttributeEnd();
		}
	}
	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::SetTransform ()
	{
		if(!isNormalExe()) return;

		if( subrender) 
			subrender->SetTransform();
		else
		{
			// �������� ����
			if(attributeStack.getMotionSamplesCount())
			{
				// ���������
				int motionSamplesCount = attributeStack.getMotionSamplesCount();
				float* motionTimes = attributeStack.getMotionTimes();
				std::vector<float> prmantimes(motionTimes, motionTimes+motionSamplesCount);
				int i;
				for(i=0; i<motionSamplesCount; i++)
					prmantimes[i] = (blur_shutterClose-blur_shutterOpen)*prmantimes[i] + blur_scenetime;
				prman->RiMotionBeginV(motionSamplesCount, std::safevector(prmantimes));

				for(i=0; i<motionSamplesCount; i++)
				{
					float t = motionTimes[i];
					attributeStack.setMotionParamPhase( t);
					transformStack.setMotionParamPhase( t);
//					bool bOk = CallFilters(call);
//					if( !bOk) return;
					Math::Matrix4f M = Math::Matrix4f::id;
					GetParameter("transform", M);
					Math::Matrix4f pintransform = transformStack.getPinTransform();
					Math::Matrix4f m = M*pintransform;
					prman->RiTransform( *(RtMatrix*)m.data());

					// ��� �� ������ �����������!!!
					if(i==0)
						BASE::SetTransform();
				}
				prman->RiMotionEnd();
				attributeStack.setMotionParamPhase(nonBlurValue);
				transformStack.setMotionParamPhase(nonBlurValue);
			}
			else
			{
				cls::P<Math::Matrix4f> M = GetParameter("transform");
				Math::Matrix4f pintransform = transformStack.getPinTransform();
				Math::Matrix4f m = M.data()*pintransform;
				if( prman)
					prman->RiTransform( *(RtMatrix*)&m);
				BASE::SetTransform();
			}
		}
	}
	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::AppendTransform ()
	{
		if(!isNormalExe()) return;

		if( subrender) 
			subrender->AppendTransform();
		else
		{
			// �������� ����
			if(attributeStack.getMotionSamplesCount())
			{
				// ���������
				int motionSamplesCount = attributeStack.getMotionSamplesCount();
				float* motionTimes = attributeStack.getMotionTimes();
				std::vector<float> prmantimes(motionTimes, motionTimes+motionSamplesCount);
				int i;
				for(i=0; i<motionSamplesCount; i++)
					prmantimes[i] = (blur_shutterClose-blur_shutterOpen)*prmantimes[i] + blur_scenetime;
				prman->RiMotionBeginV(motionSamplesCount, std::safevector(prmantimes));

				for(i=0; i<motionSamplesCount; i++)
				{
					float t = motionTimes[i];
					attributeStack.setMotionParamPhase( t);
					transformStack.setMotionParamPhase( t);
//					bool bOk = CallFilters(call);
//					if( !bOk) return;
					Math::Matrix4f M = Math::Matrix4f::id;
					GetParameter("transform", M);
					prman->RiConcatTransform( *(RtMatrix*)M.data());

					// ��� �� ������ �����������!!!
					if(i==0)
						BASE::AppendTransform();
				}
				prman->RiMotionEnd();
				attributeStack.setMotionParamPhase(nonBlurValue);
				transformStack.setMotionParamPhase(nonBlurValue);
			}
			else
			{
//				bool bOk = CallFilters(call);
//				if( !bOk) return;
				Math::Matrix4f M = Math::Matrix4f::id;
				GetParameter("transform", M);
				prman->RiConcatTransform( *(RtMatrix*)M.data());
				BASE::AppendTransform();
			}
		}
	}

	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::PushRenderAttributes(void)
	{
		if(!isNormalExe()) return;

		prman->RiAttributeBegin();
	}
	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::PopRenderAttributes(void)
	{
		if(!isNormalExe()) return;

		prman->RiAttributeEnd();
	}

	//@{ 
	/// Motion blur
	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::MotionBegin()
	{
		if(!isNormalExe()) return;

		attributeStack.resetMotion();
		/*/
		if( !bBlurFinal)
		{
			rci.clear();
			subrender = &rci;
		}
		else
		{
			float scenetime = 0;
			this->GetAttribute("cls::scenetime", scenetime);

			// ���������
			std::vector<float> times;
			times.assign( rci.getPhases(), rci.getPhases()+rci.getPhaseCount());
			for(unsigned i=0; i<times.size(); i++)
				times[i] += scenetime;
			prman->RiMotionBeginV(rci.getPhaseCount(), &times[0]);
		}
		/*/
	}
	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::MotionEnd()
	{
		if(!isNormalExe()) return;

		attributeStack.setMotionParamPhase(nonBlurValue);
		/*/
		if( !bBlurFinal)
		{
			subrender = NULL;
			bBlurFinal = true;
			rci.Render(this);
			bBlurFinal = false;
		}
		else
		{
			prman->RiMotionEnd();
		}
		/*/
	}
	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::MotionPhaseBegin(float time)
	{
		if(!isNormalExe()) return;

		attributeStack.addMotionParamPhase(time);
		attributeStack.setMotionParamPhase(time);
		/*/
		if( !bBlurFinal)
		{
//			subrender = &rci;
			rci.startPhase(time);
		}
		/*/
	}
	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::MotionPhaseEnd()
	{
		if(!isNormalExe()) return;

		attributeStack.setMotionParamPhase(nonBlurValue);
		/*/
//		subrender = NULL;
		rci.endPhase();
		/*/
	}
	template <class TRANSFORMSTACK> inline
	bool renderPrmanImpl<TRANSFORMSTACK>::SetCurrentMotionPhase(float time)
	{
		return attributeStack.setMotionParamPhase(time);
	}

	//@}


	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::Shader(const char *shadertype)
	{
		if(!isNormalExe()) return;

		if( subrender) 
			subrender->Shader(shadertype);
		else
		{
			// �������� ����
			if(attributeStack.getMotionSamplesCount())
			{
				// ���������
				int motionSamplesCount = attributeStack.getMotionSamplesCount();
				float* motionTimes = attributeStack.getMotionTimes();

				float t = motionTimes[0];
				attributeStack.setMotionParamPhase( t);
				bool bOk = CallFilters_onshader(shadertype);
				if(bOk)
					singleShader(shadertype);

			}
			else
			{
				bool bOk = CallFilters_onshader(shadertype);
				if(bOk)
					singleShader(shadertype);
			}

		}
	}

	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::Light()
	{
		if(!isNormalExe()) return;

		if( subrender) 
			subrender->Light();
		else
		{
			//cls::PS lightid = GetParameter("light::id");
			//cls::PS shadername = GetParameter("light::shader");
			cls::PS shadername = GetParameter("#lightshader");
			if( shadername.empty())
				return;

			std::vector<RtToken> tokens;
			std::vector<std::string> tokennames;
			std::vector<RtPointer> parms;
			std::list<RtString> strings;

			// ����� ��� ���������
			if( !BuildTokenList(tokens, tokennames, parms, strings, 1, 1, 1))
				return;
/*/
			if( !lightid.empty())
			{
				const char* _lightid = lightid.data();
				tokennames.push_back("__handleid");
				parms.push_back( (RtPointer)&_lightid);
				CompleteTokenList(tokens, tokennames);
			}
/*/
			prman->RiLightSourceV( (RtToken)shadername.data(), (int)tokens.size(), std::safevector(tokens), std::safevector(parms));
		}
	}

	/// ����� ��������� ���������� ������� ������
	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::SystemCall(
		enSystemCall call
		)
	{
		if(!isNormalExe()) return;

		if( subrender) 
		{
			subrender->SystemCall(call);
			return;
		}
		// visible
		bool bVisible = true;
		GetAttribute("visible", bVisible);
		if( !bVisible) return;
		GetAttribute("@visible", bVisible);
		if( !bVisible) return;

		// ��� �������
		std::string pass;
		GetAttribute("@pass", pass);
		std::string passvisible = "@visible"+pass;
		GetAttribute(passvisible.c_str(), bVisible);
		if( !bVisible) return;

		// final || shadow
		std::string passtype;
		GetAttribute("@passtype", passtype);
		passvisible = "@visible"+passtype;
		GetAttribute(passvisible.c_str(), bVisible);
		if( !bVisible) return;
		

		{
			bool bMatte = false;
			bool bSetuped = false;
			bSetuped |= this->GetAttribute( "@matte", bMatte);
			std::string passmatte = "@matte"+pass;
			bSetuped |= this->GetAttribute(passmatte.c_str(), bMatte);

			if( bSetuped)
				prman->RiMatte(bMatte?1:0);
		}

		bool bBeginAttribute = false;
		float shadingRate= 1;
		if( GetAttribute("@shadingRate", shadingRate))
		{
			if( !bBeginAttribute)
				prman->RiAttributeBegin();
			bBeginAttribute = true;

			prman->RiShadingRate(shadingRate);

			/*/
			char* opaque = "opaque";
			prman->RiAttribute( "visibility", "string transmission", &opaque, NULL);
			RtColor Opacity = {1,1,1};
			prman->RiOpacity( Opacity);
			float volumeintersectionpriority = 0;
			prman->RiAttribute( "shade", "float volumeintersectionpriority", &volumeintersectionpriority, NULL);
			int samplemotion = 0, displacements = 0;
			prman->RiAttribute( "trace", "int samplemotion", &samplemotion, "int displacements", &displacements, NULL);
			int doubleshaded = 0;
			prman->RiAttribute( "sides", "int doubleshaded", &doubleshaded, NULL);
			RtColor color = {0, .25, 1};
			prman->RiColor(color);
			/*/
		}

		singleSystemCallBeforeBlur(call);

		// �������� ����
		if(attributeStack.getMotionSamplesCount())
		{
			// ���������
			int motionSamplesCount = attributeStack.getMotionSamplesCount();
			float* motionTimes = attributeStack.getMotionTimes();
			std::vector<float> prmantimes(motionTimes, motionTimes+motionSamplesCount);
			for(int i=0; i<motionSamplesCount; i++)
				prmantimes[i] = (blur_shutterClose-blur_shutterOpen)*prmantimes[i] + blur_scenetime;
			prman->RiMotionBeginV(motionSamplesCount, std::safevector(prmantimes));

			for(int i=0; i<motionSamplesCount; i++)
			{
				float t = motionTimes[i];
				attributeStack.setMotionParamPhase( t);

				bool bOk = CallFilters(call);
				if( !bOk) return;
				singleSystemCall(call);
			}
			prman->RiMotionEnd();
		}
		else
		{
			bool bOk = CallFilters(call);
			if( !bOk) return;
			singleSystemCall(call);
		}
		if( bBeginAttribute)
		{
			prman->RiAttributeEnd();
		}
	}

	/// Call (���������������� �����)
	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::Call( 
		IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
		)
	{
		if(!isNormalExe()) return;
//		if( subrender) 
//			subrender->Call( pProcedural);
//		else
		{
			pProcedural->Process(this);
		}
	}
	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::Call( 
		const char*	procname			//!< eiy oeacaiiia a DeclareProcBegin
		)
	{
		IProcedural* pProcedural = findProcedure(procname);
		if(!pProcedural)
			return;
		this->Call(pProcedural);
	}

		template <class TRANSFORMSTACK> inline
		RtVoid renderPrmanImpl<TRANSFORMSTACK>::rpi_subdivfunc(RtPointer ptr, RtFloat detail)
		{
			delayRenderStruct* drs = (delayRenderStruct*)ptr;
			// �����
			drs->pDelayRender->Render(
				&drs->prman, 
				drs->prman.attributeStack.getMotionSamplesCount(), 
				drs->prman.attributeStack.getMotionTimes());
//			drs->prman->PopAttributes();
		}
		template <class TRANSFORMSTACK> inline
		RtVoid renderPrmanImpl<TRANSFORMSTACK>::rpi_freefunc(RtPointer ptr)
		{
			delayRenderStruct* drs = (delayRenderStruct*)ptr;
//printf("release %x.pDelayRender = %x\n", drs, drs->pDelayRender);
			drs->pDelayRender->release();
			delete drs;
		}
		inline void prman_copy(RtBound bound, const Math::Box3f& box)
		{
			bound[0] = box.min.x; 
			bound[1] = box.max.x; 
			bound[2] = box.min.y; 
			bound[3] = box.max.y; 
			bound[4] = box.min.z; 
			bound[5] = box.max.z;
		}

	/// Procedural (���������������� �����)
	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::RenderCall( 
		IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
		)
	{
		if(!isNormalExe()) return;

		bool bVisible = true;
		GetAttribute("visible", bVisible);
		if( !bVisible) return;
		GetAttribute("@visible", bVisible);
		if( !bVisible) return;

		std::string pass;
		GetAttribute("@pass", pass);
		pass = "@visible"+pass;
		GetAttribute(pass.c_str(), bVisible);
		if( !bVisible) return;

		bool bDelay = false;
		if( !this->GetParameter("@delay", bDelay))
			bDelay = false;

		if( !filteritemlist.empty())
			bDelay = false;

		if( !bDelay)
		{
			pProcedural->Render(this, attributeStack.getMotionSamplesCount(), attributeStack.getMotionTimes());
		}
		else
		{
			Math::Box3f box;
			if(!attributeStack.getMotionSamplesCount())
			{
				P<Math::Box3f> _box = this->GetParameter("#bound");
				if( !_box.empty())
					box = _box.data();
			}
			else
			{
				for( int i=0; i<attributeStack.getMotionSamplesCount(); i++)
				{
					float t = attributeStack.getMotionTimes()[i];
					attributeStack.setMotionParamPhase(t);
					P<Math::Box3f> _box = this->GetParameter("#bound");
					if( !_box.empty())
						box.insert( _box.data());
				}
			}
			RtBound bound;
			prman_copy(bound, box);
			delayRenderStruct* drs = new delayRenderStruct;
			// ���������� ������� ���������
//			GetParameterList(drs->params);
//			GetAttributeList(drs->attributes);
//			drs->params = si.params;
			drs->pDelayRender = pProcedural;
			drs->prman.startFrom(*this);
			pProcedural->addref();
//printf("addref %x.pDelayRender = %x\n", drs, pProcedural);
//	RtInt reentrant = 0;
//	prman->RiAttribute("procedural", "int reentrant", &reentrant, RI_NULL);
			prman->RiProcedural(drs, bound, rpi_subdivfunc, rpi_freefunc);
		}
	}
	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::RenderCall( 
		const char*	procname			//!< eiy oeacaiiia a DeclareProcBegin
		)
	{
		// ������� ���� �� �������� � ���������
		if( strcmp(procname, "Camera")==0)
		{
			Camera();
			return;
		}
		else if( strcmp(procname, "Output")==0)
		{
			Output();
			return;
		}

		IProcedural* pProcedural = findProcedure(procname);
		if(!pProcedural)
			return;
		this->RenderCall(pProcedural);
	}



	template <class TRANSFORMSTACK> inline
	bool renderPrmanImpl<TRANSFORMSTACK>::BuildTokenList(
		std::vector<RtToken>& tokens, 
		std::vector<std::string>& tokennames,
		std::vector<RtPointer>& parms, 
		std::list<RtString>& strings,		// ��� ����� ��������� ���������� ����� (char**)
		int primcount, 
		int vertcount, 
		int privvertscount, 
		char** interpolationTable, 
		bool bDump
		)
	{
		if(bDump)
		{
			std::string s;
			GetAttribute("name", s);
			printf("BuildTokenList --- %s\n", s.c_str());
		}
		static char* defInterpolationTable[] =
		{
			"", // PI_UNKNOWN
			"constant", // PI_CONSTANT
			"vertex", // PI_VERTEX
			"face", // PI_PRIMITIVE
			"facevarying", // PI_PRIMITIVEVERTEX
			"varying", // PI_VARYING
			"", //PI_LAST
		};
		if( !interpolationTable)
			interpolationTable = defInterpolationTable;

		std::vector< std::pair< std::string, Param> > params;
		GetParameterList(params);

		int rc = (int)params.size();
		tokens.reserve(rc);
		tokennames.reserve(rc);
		parms.reserve(rc);
		std::vector< std::pair< std::string, Param> >::iterator it = params.begin();
		for( ; it != params.end(); it++)
		{
			if( it->first[0]=='#' || it->first[0]=='@') 
				continue;
			if( it->first.find(':')!=std::string::npos)
				continue;
			std::string name;

			RtPointer data = BuildSingleParam(it->first.c_str(), it->second, interpolationTable, name, strings, bDump);
			if( !data)
				continue;

			tokennames.push_back( name);
			tokens.push_back( (RtToken)tokennames.back().c_str());
			parms.push_back( data);
		}
		
		return true;
	}
	//! ����������� ��������� �������� ��� ����������
	template <class TRANSFORMSTACK> inline
	RtPointer renderPrmanImpl<TRANSFORMSTACK>::BuildSingleParam(
		const char* paramname, 
		cls::Param& p, 
		char** interpolationTable, 
		std::string& name,
		std::list<RtString>& strings,		// ��� ����� ��������� ���������� ����� (char**)
		bool bDump 
		)
	{
		RtPointer data = NULL;
		name.reserve(50);
		if( p->interp>=0 && p->interp<PI_LAST)
		{
			name += interpolationTable[p->interp];
			name += " ";
		}
		switch(p->type)
		{
		case PT_BOOL:
			name += "float"; break;
		case PT_STRING:
			name += "string"; break;
		case PT_FLOAT: case PT_HALF: case PT_DOUBLE:
			name += "float"; break;
		case PT_POINT:
			name += "point"; break;
		case PT_VECTOR: 
			name += "vector"; break;
		case PT_NORMAL:
			name += "normal"; break;
		case PT_COLOR:
			name += "color"; break;
		case PT_MATRIX:
			name += "matrix"; break;
		case PT_INT8: case PT_UINT8: case PT_INT16: case PT_UINT16: case PT_INT: case PT_UINT:
			name += "int"; break;
			break;
		case PT_HPOINT:
			break;
		}
		if(p->interp==PI_CONSTANT && p.size()>1)
		{
			char buf[24];
			_itoa(p.size(), buf, 10);
			name += "[";
			name += buf;
			name += "]";
		}
		name += " ";

		name += paramname;
		if( bDump)
			printf("%s", name.c_str());

		if( p->type != PT_STRING)
		{
			if( bDump)
			{
				if( p->type == PT_FLOAT)
					printf("%f", *(float*)p.data());
			}
			data = p.data();
		}
		else
		{
			if( p.size()==1)
			{
				char* val = p.data();
				strings.push_back( val);
				RtString* str = &strings.back();
				data = str;
			}
			else
			{
				fprintf(stderr, "Sorry: BuildSingleParam not implemented for string array!!!\n");
				return NULL;
			}
		}
		if( bDump)
			printf("\n");
		return data;
	}

	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::CompleteTokenList(
		std::vector<RtToken>& tokens, 
		const std::vector<std::string>& tokennames
		)
	{
		tokens.resize(tokennames.size());
		for(unsigned i=0; i<tokens.size(); i++)
		{
			tokens[i] = (RtToken)tokennames[i].c_str();
		}
	}





	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::singleSystemCallBeforeBlur(enSystemCall call)
	{
		switch(call)
		{
		case SC_CURVES:
			renderBasis();
			break;
		case SC_POINTS:
		case SC_PATCH:
		case SC_MESH:
		case SC_SPHERE:
		case SC_BLOBBY:
			break;
		}
	}



	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::singleSystemCall(enSystemCall call)
	{
		switch(call)
		{
		case SC_POINTS:
			renderPoints();
			break;
		case SC_CURVES:
			{
			PS interp = IRender::GetParameter("#curves::interpolation");
			PS wrap = IRender::GetParameter("#curves::wrap");
			PA<int> nverts = IRender::GetParameter("#curves::nverts");
			renderCurves(interp.data(), wrap.data(), nverts);
			break;
			}
		case SC_PATCH:
		{
			renderPatch();
			break;
		}
//		case SC_TRIMCURVE:
//			break;
		case SC_MESH:
			{
			PS interp;
			PA<int> loops;
			PA<int> nverts;
			PA<int> verts;
			IRender::GetParameter("mesh::interpolation", interp);
			IRender::GetParameter("mesh::loops", loops);
			IRender::GetParameter("mesh::nverts", nverts);
			IRender::GetParameter("mesh::verts", verts);
//			RemoveParameter("mesh::interpolation");
//			RemoveParameter("mesh::loops");
//			RemoveParameter("mesh::nverts");
//			RemoveParameter("mesh::verts");
			renderMesh(interp.data(), loops, nverts, verts);
			break;
			}
		case SC_SPHERE:
			{
			float radius;
			float zmin;
			float zmax;
			float thetamax;
			IRender::GetParameter("#sphere::radius", radius);
			IRender::GetParameter("#sphere::zmin", zmin);
			IRender::GetParameter("#sphere::zmax", zmax);
			IRender::GetParameter("#sphere::thetamax", thetamax);
			renderSphere(radius, zmin, zmax, thetamax);
			break;
			}
		case SC_BLOBBY:
			{
			PA<Math::Matrix4f> ellipsoids;
			IRender::GetParameter("blobby::ellipsoids", ellipsoids);
			renderBlobby(ellipsoids);
			break;
			}
		}
	}

	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::renderPoints(
		)
	{
		{
			PA<Math::Vec3f> P;
			IRender::GetParameter("P", P);
			int npoints = P.size();

			cls::Param _width = GetParameter("#width");
			if( !_width.empty())
			{
				if( _width.size()==1)
				{
					cls::P<float> constantwidth(_width);
					if( !constantwidth.empty())
					{
						cls::PA<float> width(cls::PI_VARYING, npoints);
						for(int i=0; i<width.size(); i++)
							width[i] = constantwidth.data();
						Parameter("width", width);
					}
				}
				else
				{
					Parameter("width", _width);
				}
			}

			std::vector<RtToken> tokens;
			std::vector<std::string> tokennames;
			std::vector<RtPointer> parms;
			std::list<RtString> strings;

			// ����� ��� ���������
			if( !BuildTokenList(tokens, tokennames, parms, strings, npoints, npoints, npoints))
				return;

			CompleteTokenList(tokens, tokennames);

			prman->RiPointsV(npoints, (int)tokens.size(), std::safevector(tokens), std::safevector(parms));
		}
	}

	enum enStandartBasis
	{
		SB_BEZIER,	
		SB_B_SPLINE,	
		SB_CATMULL_ROM,
		SB_HERMITE,	
		SB_POWER
	};

	// �� ������ ������ ������ �����!!!
	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::renderBasis()
	{
		cls::Param basis = GetParameter("@basis");
		if( basis.empty()) return;

		if(basis->type == cls::PT_STRING)
		{
			cls::PS basisname = basis;
			static misc::switch_string::Token basislist[] = {
				{"bezier",				SB_BEZIER		}, 
				{"b-spline",			SB_B_SPLINE		}, 
				{"catmull-rom",			SB_CATMULL_ROM	}, 
				{"hermite",				SB_HERMITE		},
				{"power",				SB_POWER		},						
				{NULL, 		-1}};
			static misc::switch_string sems(basislist);
			int code = sems.match(basisname.data());

			static RtBasis BEZIERbasis = {
				-1.000000f,3.000000f,-3.000000f,1.000000f,
				3.000000f,-6.000000f,3.000000f,0.000000f,
				-3.000000f,3.000000f,0.000000f,0.000000f,
				1.000000f,0.000000f,0.000000f,0.000000f
				};
			static RtBasis BSPLINEbasis = {
				-0.166667f,0.500000f,-0.500000f,0.166667f,
				0.500000f,-1.000000f,0.500000f,0.000000f,
				-0.500000f,0.000000f,0.500000f,0.000000f,
				0.166667f,0.666667f,0.166667f,0.000000f
				};
			static RtBasis CATMULLROMbasis = {
				-0.500000f,1.500000f,-1.500000f,0.500000f,
				1.000000f,-2.500000f,2.000000f,-0.500000f,
				-0.500000f,0.000000f,0.500000f,0.000000f,
				0.000000f,1.000000f,0.000000f,0.000000f
				};
			static RtBasis HERMITEbasis = {
				2.000000f,1.000000f,-2.000000f,1.000000f,
				-3.000000f,-2.000000f,3.000000f,-1.000000f,
				0.000000f,1.000000f,0.000000f,0.000000f,
				1.000000f,0.000000f,0.000000f,0.000000f
				};
			static RtBasis POWERbasis = {
				1.000000f,0.000000f,0.000000f,0.000000f,
				0.000000f,1.000000f,0.000000f,0.000000f,
				0.000000f,0.000000f,1.000000f,0.000000f,
				0.000000f,0.000000f,0.000000f,1.000000f
				};

			switch(code)
			{
			case SB_BEZIER:
				prman->RiBasis(
					BEZIERbasis, RI_BEZIERSTEP,
					BEZIERbasis, RI_BEZIERSTEP);
				break;
			case SB_B_SPLINE:
				prman->RiBasis(
					BSPLINEbasis, RI_BSPLINESTEP,
					BSPLINEbasis, RI_BSPLINESTEP);
				break;
			case SB_CATMULL_ROM:
				prman->RiBasis(
					CATMULLROMbasis, RI_CATMULLROMSTEP,
					CATMULLROMbasis, RI_CATMULLROMSTEP);
				break;
			case SB_HERMITE:
				prman->RiBasis(
					HERMITEbasis, RI_HERMITESTEP,
					HERMITEbasis, RI_HERMITESTEP);
				break;
			case SB_POWER:	
				prman->RiBasis(
					POWERbasis, RI_POWERSTEP,
					POWERbasis, RI_POWERSTEP);
				break;
			}
		}
		else if(basis->type == cls::PT_MATRIX)
		{
			cls::P<Math::Matrix4f> basismatr = basis;
			Math::Matrix4f m = *basismatr;
			cls::P<int> basisStep = GetParameter("@basisStep");
			if( !basisStep.empty())
			{
				prman->RiBasis(
					*(RtBasis*)m.data(), basisStep.data(),
					*(RtBasis*)m.data(), basisStep.data()
					);
			}
		}
	}

	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::renderCurves( 
		const char *interp, 
		const char *wrap,		//!< wrap
		PA<int>& nverts			//!< int[ncurves] ����� ��������� � ������
		)
	{
		if( nverts.empty())
			return;
		if( interp==0)
			return;
		if( wrap==0)
			return;

		{
			bool bCubic = strcmp(interp, "cubic")==0;
			int privvertscount = 0, vertcount=0;
			for(int i=0; i<nverts.size(); i++)
				privvertscount += nverts[i];
			if( bCubic)
				privvertscount -= nverts.size()*2;


			cls::Param _width = GetParameter("#width");
			cls::PA<float> width;
			if( !_width.empty())
			{
				if( _width.size()==1)
				{
					cls::P<float> constantwidth(_width);
					if( !constantwidth.empty())
					{
//						cls::PA<float> width(cls::PI_VARYING, privvertscount);
						width.resize(privvertscount);
						for(int i=0; i<width.size(); i++)
							width[i] = constantwidth.data();
//						Parameter("width", width);
					}
				}
				else
				{
//					Parameter("width", _width);
					width = _width;
				}
			}
				
			std::vector<RtToken> tokens;
			std::vector<std::string> tokennames;
			std::vector<RtPointer> parms;
			std::list<RtString> strings;

			cls::Param _P = GetParameter("P");
			RemoveParameter( "P");

			static char* interpolationTable[] =
			{
				"", // PI_UNKNOWN
				"constant", // PI_CONSTANT
				"vertex", // PI_VERTEX
				"uniform", // PI_PRIMITIVE
				"facevarying", // PI_PRIMITIVEVERTEX
				"varying", // PI_VARYING
				"", //PI_LAST
			};

			// ����� ��� ���������
			if( !BuildTokenList(tokens, tokennames, parms, strings, nverts.size(), vertcount, privvertscount, interpolationTable))
				return;

			tokennames.push_back("P");
			parms.push_back(_P.data());
			if( !width.empty())
			{
				tokennames.push_back("width");
				parms.push_back(width.data());
			}
			
			CompleteTokenList(tokens, tokennames);

			prman->RiCurvesV((RtToken)interp, (int)nverts.size(), nverts.data(), (RtToken)wrap, (int)tokens.size(), std::safevector(tokens), std::safevector(parms));
		}
	}
	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::renderPatch()
	{
		int nloops;
		PA<int> ncurves;
		PA<int> order;
		PA<float> knot;
		PA<float> min;
		PA<float> max;
		PA<int> n;
		PA<float> u;
		PA<float> v;
		PA<float> w;

		if ( GetParameter( "nloops", nloops ) )
		{
			GetParameter( "ncurves", ncurves );
			GetParameter( "order", order );
			GetParameter( "knot", knot );
			GetParameter( "min", min );
			GetParameter( "max", max );
			GetParameter( "n", n );
			GetParameter( "u", u );
			GetParameter( "v", v );
			GetParameter( "w", w );

			RemoveParameter( "nloops" );
			RemoveParameter( "ncurves" );
			RemoveParameter( "order" );
			RemoveParameter( "knot" );
			RemoveParameter( "min" );
			RemoveParameter( "max" );
			RemoveParameter( "n" );
			RemoveParameter( "u" );
			RemoveParameter( "v" );
			RemoveParameter( "w" );

			prman->RiTrimCurve( nloops, ncurves.data(), order.data(), knot.data(), min.data(), max.data(), n.data(), u.data(), v.data(), w.data() );
        }

		int nu;
		int uorder;
		PA<float> uknot;
		float umin;
		float umax;
		int nv;
		int vorder;
		PA<float> vknot;
		float vmin;
		float vmax;

		GetParameter( "nu", nu );
		GetParameter( "uorder", uorder );
		GetParameter( "uknots", uknot );
		GetParameter( "umin", umin );
		GetParameter( "umax", umax );
		GetParameter( "nv", nv );
		GetParameter( "vorder", vorder );
		GetParameter( "vknots", vknot );
		GetParameter( "vmin", vmin );
		GetParameter( "vmax", vmax );

		RemoveParameter( "nu" );
		RemoveParameter( "uorder" );
		RemoveParameter( "uknots" );
		RemoveParameter( "umin" );
		RemoveParameter( "umax" );
		RemoveParameter( "nv" );
		RemoveParameter( "vorder" );
		RemoveParameter( "vknots" );
		RemoveParameter( "vmin" );
		RemoveParameter( "vmax" );

		std::vector<RtToken> tokens;
		std::vector<std::string> tokennames;
		std::vector<RtPointer> parms;
		std::list<RtString> strings;

		if ( !BuildTokenList( tokens, tokennames, parms, strings, 0, 0, 0) ) return;

		prman->RiNuPatchV( nu, uorder, uknot.data(), umin, umax, nv, vorder, vknot.data(), vmin, vmax, (int)tokens.size(), std::safevector(tokens), std::safevector(parms) );
	}

	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::renderMesh(
		const char *interp,		//!< ��� ������������
		PA<int>& loops,			//!< loops per polygon
		PA<int>& nverts,		//!< ����� ��������� � �������� (size = polygoncount)
		PA<int>& verts			//!< ������� ��������� ��������� (size = facevertcount)
		)
	{
		int ntags = 0;
		//cls::PSA tags;
		std::vector< char* > tags_v;
		cls::PA<int> nargs(cls::PI_PRIMITIVE);
		cls::PA<int> intargs(cls::PI_PRIMITIVE);
		cls::PA<float> floatargs(cls::PI_PRIMITIVE);

		cls::PA<int> holes( cls::PI_PRIMITIVE );
		cls::PA<int> crease_edges( cls::PI_PRIMITIVE );
		cls::PA<float> crease_hardness( cls::PI_PRIMITIVE );
		cls::PA<int> corner_vertices( cls::PI_VERTEX );
		cls::PA<float> corner_hardness( cls::PI_VERTEX );
		bool interpolateBoundary = false;

		GetParameter( "#subdiv::holes", holes );
		GetParameter( "#subdiv::crease_edges", crease_edges );
		GetParameter( "#subdiv::crease_hardness", crease_hardness );
		GetParameter( "#subdiv::corner_vertices", corner_vertices );
		GetParameter( "#subdiv::corner_hardness", corner_hardness );
		GetParameter( "#subdiv::interpolateBoundary", interpolateBoundary );

		ntags = crease_hardness.size() + (int)( !holes.empty() ) + (int)( !corner_vertices.empty() ) + (int)interpolateBoundary;

		tags_v.reserve( ntags );
		nargs.reserve( ntags*2 );
		intargs.reserve( holes.size() + crease_edges.size() + corner_vertices.size() + (int)interpolateBoundary );
		floatargs.reserve( crease_hardness.size() + corner_vertices.size() );

		if ( !holes.empty() )
		{
			tags_v.push_back( "hole" );
			nargs.push_back( holes.size() );
			nargs.push_back( 0 );

			for ( int i = 0; i < holes.size(); i++ )
			{
				intargs.push_back( holes[i] );
			}
		}

		if ( crease_edges.size() == crease_hardness.size()*2 )
		{
			if ( !crease_hardness.empty() && !crease_edges.empty() )
			{
				for ( int i = 0; i < crease_hardness.size(); i++ )
				{
					tags_v.push_back( "crease" );
					nargs.push_back( 2 );
					nargs.push_back( 1 );

					intargs.push_back( crease_edges[ i*2 ] );
					intargs.push_back( crease_edges[ i*2 + 1 ] );
					floatargs.push_back( crease_hardness[i] );
				}
			}
		}
		else
		{
			printf("warrning: crease edges array size != crease hardness array size * 2\n");
		}

		if ( corner_vertices.size() == corner_hardness.size() )
		{
			if ( !corner_vertices.empty() && !corner_hardness.empty() )
			{
				tags_v.push_back( "corner" );
				nargs.push_back( corner_vertices.size() );
				nargs.push_back( 1 );

				for ( int i = 0; i < corner_vertices.size(); i++ )
				{
					intargs.push_back( corner_vertices[i] );
					floatargs.push_back( corner_hardness[i] );
				}
			}
		}
		else
		{
			printf("warrning: corner vertices array size != corner hardness array size\n");
		}

		if ( interpolateBoundary )
		{
			tags_v.push_back( "interpolateboundary" );
			nargs.push_back( 1 );
			nargs.push_back( 0 );

			intargs.push_back( 1 );
		}

/*/
		if ( GetParameter( "ntags", ntags) )
		{
			tags = GetParameter( "tags" );
			GetParameter( "nargs", nargs );
			GetParameter( "intargs", intargs );
			GetParameter( "floatargs", floatargs );

			tags_v.reserve( tags.size() );
			for ( int i = 0; i < tags.size(); i++ )
			{
				tags_v.push_back( (RtToken)tags[i] );
			}

			RemoveParameter( "ntags" );
			RemoveParameter( "tags" );
			RemoveParameter( "nargs" );
			RemoveParameter( "intargs" );
			RemoveParameter( "floatargs" );
		}
/*/
		std::vector<RtToken> tokens;
		std::vector<std::string> tokennames;
		std::vector<RtPointer> parms;
		std::list<RtString> strings;

		int privvertscount = 0, vertcount = 0;
		for(int i=0; i<nverts.size(); i++)
			privvertscount += nverts[i];
		
		// ����� ��� ���������
		if( !BuildTokenList(tokens, tokennames, parms, strings, nverts.size(), vertcount, privvertscount))
			return;

		if( strcmp(interp, "linear")==0)
		{
			prman->RiPointsGeneralPolygonsV(
				(int)nverts.size(), 
				loops.data(), nverts.data(), verts.data(), 
				(int)tokens.size(), std::safevector(tokens), std::safevector(parms));
		}
		else
		{
			prman->RiHierarchicalSubdivisionMeshV(
//			prman->RiSubdivisionMeshV( 
				(RtToken)interp, 
				(int)nverts.size(), nverts.data(), verts.data(), 
				(int)tags_v.size(), std::safevector(tags_v), 
				nargs.data(), intargs.data(), floatargs.data(),
				0, 
				(int)tokens.size(), std::safevector(tokens), std::safevector(parms));
		}
	}

	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::renderSphere( float radius, float zmin, float zmax, float thetamax)
	{
		{
			std::vector<RtToken> tokens;
			std::vector<std::string> tokennames;
			std::vector<RtPointer> parms;
			std::list<RtString> strings;

			if( !BuildTokenList(tokens, tokennames, parms, strings, 1, 1, 1))
				return;

			prman->RiSphereV(radius, zmin, zmax, thetamax, (int)tokens.size(), std::safevector(tokens), std::safevector(parms));
		}
	}
	/// Blobby
	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::renderBlobby( PA<Math::Matrix4f>& ellipsoids)
	{
		{
			int s = ellipsoids.size();
			std::vector<int> codes;
			codes.reserve(s*2 + 2 + s);
			std::vector<float> flts;
			flts.reserve(s*16);

			int i;
			for(i=0; i<s; i++)
			{
				codes.push_back(1001);
				codes.push_back( (int)flts.size());

				Math::Matrix4f& m = ellipsoids[i];
				for(int x=0; x<4; x++)
					for(int y=0; y<4; y++)
						flts.push_back(m[x][y]);
			}
			codes.push_back(0);
			codes.push_back(s);
			for(i=0; i<s; i++)
				codes.push_back(i);

			std::vector<RtToken> tokens;
			std::vector<std::string> tokennames;
			std::vector<RtPointer> parms;
			std::list<RtString> strings;

			if( !BuildTokenList(tokens, tokennames, parms, strings, 1, 1, 1))
				return;

			prman->RiBlobbyV(s, (int)codes.size(), std::safevector(codes), (int)flts.size(), std::safevector(flts), 0, 0, (int)tokens.size(), std::safevector(tokens), std::safevector(parms));
		}
	}
	template <class TRANSFORMSTACK> inline
	void renderPrmanImpl<TRANSFORMSTACK>::singleShader(const char *shadertype)
	{
		cls::PS shadername = GetParameter("shader::name");
		cls::PS layername = GetParameter("shader::layer");
		if( shadername.empty())
			return;

		if( !layername.empty() && layername.data()[0])
		{
			cls::PS currentlayer = GetAttribute("@shader::currentlayer");
			if( !currentlayer.empty())
			{
				// ��������� �� ����������
				if( strcmp(currentlayer.data(), layername.data())!=0)
					return;
			}
			else
			{
				// currentlayer - ������, ����� ������
				return;
			}
		}
		Math::Vec3f opacity;
		if( GetParameter("@opacity", opacity))
		{
			prman->RiOpacity( &opacity[0]);
		}
		Math::Vec3f color;
		if( GetParameter("@color", color))
		{
			prman->RiColor( &color[0]);
		}

		float shadingRate= 1;
		if( GetAttribute("@shadingRate", shadingRate))
		{
			prman->RiShadingRate(shadingRate);
		}

		if( strcmp(shadertype, "surface")==0)
		{
			bool ignore = false;
			IRender::GetAttribute("shader::ignoreSurface", ignore);
			if( !ignore)
			{
				std::vector<RtToken> tokens;
				std::vector<std::string> tokennames;
				std::vector<RtPointer> parms;
				std::list<RtString> strings;

				if( !BuildTokenList(tokens, tokennames, parms, strings, 1, 1, 1, NULL, false))
					return;

				prman->RiSurfaceV( (RtToken)shadername.data(), (int)tokens.size(), std::safevector(tokens), std::safevector(parms));
			}
		}

		if( strcmp(shadertype, "displacement")==0)
		{
			bool ignore = false;
			IRender::GetAttribute("shader::ignoreDisplace", ignore);
			if( !ignore)
			{
				// ��� ���� ������ �����
				P<float> displacement_bound = GetParameter("displacement::bound");
				RemoveParameter("displacement::bound");
				PS displacement_space = GetParameter("displacement::space");
				RemoveParameter("displacement::space");
				RtString coordinatesystem = "shader";
				bool bBound = true;
				float bound = 2;
				if( !displacement_bound.empty())
					bound = (displacement_bound).data();
				else
					bBound = false;

				if( !displacement_space.empty())
					coordinatesystem = (RtString)displacement_space.data();
				else
					bBound = false;

				if( bBound)
					prman->RiAttribute("displacementbound", "sphere", &bound, "coordinatesystem", &coordinatesystem, RI_NULL);

				std::vector<RtToken> tokens;
				std::vector<std::string> tokennames;
				std::vector<RtPointer> parms;
				std::list<RtString> strings;

				if( !BuildTokenList(tokens, tokennames, parms, strings, 1, 1, 1))
					return;

				prman->RiDisplacementV( (RtToken)shadername.data(), (int)tokens.size(), std::safevector(tokens), std::safevector(parms));
			}
		}
	}
	template <class TRANSFORMSTACK> inline
	bool renderPrmanImpl<TRANSFORMSTACK>::Camera()
	{
		// RiClipping
		float _near, _far;
		if( !GetParameter("camera::near", _near))
			return false;
		if( !GetParameter("camera::far", _far))
			return false;
		prman->RiClipping( _near, _far);

		// RiFormat
		cls::PA<int> resolution;
		if( !GetParameter("camera::resolution", resolution))
			return false;
		if( resolution.size()!=2)
			return false;
		float format_ratio;
		if( !GetParameter("camera::ratio", format_ratio))
			return false;
		prman->RiFormat(resolution[0], resolution[1], format_ratio);

		// RiScreenWindow
		PA<float> screenwindow;
		GetParameter("camera::screen", screenwindow);
		if(screenwindow.size()!=4)
			return false;
		prman->RiScreenWindow(screenwindow[0], screenwindow[1], screenwindow[2], screenwindow[3]);


		// RiProjectionV
		PS projection;
		if( !GetParameter("camera::projection", projection))
			return false;

		std::vector<RtToken> tokens;
		std::vector<std::string> tokennames;
		std::vector<RtPointer> parms;

		float fov;
		if( GetParameter("camera::fov", fov))
		{
			tokennames.push_back( "fov");
			tokens.push_back( (RtToken)tokennames.back().c_str());
			parms.push_back(&fov);
		}
		CompleteTokenList(tokens, tokennames);

		prman->RiProjectionV( (char*)projection.data(), (int)tokens.size(), std::safevector(tokens), std::safevector(parms));

		// ConcatTransform
		int motionSamplesCount = attributeStack.getMotionSamplesCount();
		if( motionSamplesCount<=1)
		{
			Math::Matrix4f transform;
			if( !GetParameter("camera::transform", transform))
				return false;
			transform.invert();
			prman->RiConcatTransform(*(RtMatrix*)&transform);
		}
		else
		{
			int motionSamplesCount = attributeStack.getMotionSamplesCount();
			float* motionTimes = attributeStack.getMotionTimes();
			std::vector<float> prmantimes(motionTimes, motionTimes+motionSamplesCount);
			int i;
			for(i=0; i<motionSamplesCount; i++)
				prmantimes[i] = (blur_shutterClose-blur_shutterOpen)*prmantimes[i] + blur_scenetime;
			prman->RiMotionBeginV(motionSamplesCount, std::safevector(prmantimes));
			for(i=0; i<motionSamplesCount; i++)
			{
				float t = motionTimes[i];
				attributeStack.setMotionParamPhase( t);
				transformStack.setMotionParamPhase( t);

				Math::Matrix4f transform;
				if( !GetParameter("camera::transform", transform))
					return false;
				transform.invert();

				prman->RiConcatTransform(*(RtMatrix*)&transform);
			}
			prman->RiMotionEnd();
			attributeStack.setMotionParamPhase(nonBlurValue);
			transformStack.setMotionParamPhase(nonBlurValue);
		}

		return true;
	}
	template <class TRANSFORMSTACK> inline
	bool renderPrmanImpl<TRANSFORMSTACK>::Output()
	{
		/*/
		cls::PA<int> format;
		if( !GetParameter("output::format", format))
			return false;
		if( format.size()!=2)
			return false;
		float format_ratio;
		if( !GetParameter("output::ratio", format_ratio))
			return false;
		prman->RiFormat(format[0], format[1], format_ratio);
		
		PA<float> screenwindow;
		GetParameter("output::screenwindow", screenwindow);
		if(screenwindow.size()!=4)
			return false;
		prman->RiScreenWindow(screenwindow[0], screenwindow[1], screenwindow[2], screenwindow[3]);
		/*/
		
		float shadingrate;
		if( GetParameter("output::shadingrate", shadingrate))
		{
			prman->RiShadingRate(shadingrate);
		}

		cls::PA<float> ppixelsample;
		if( !GetParameter("output::pixelsample", ppixelsample))
			return false;
		if( ppixelsample.size()!=2)
			return false;
		prman->RiPixelSamples(ppixelsample[0], ppixelsample[1]);

		cls::P<float> gain, gamma;
		if( GetParameter("output::gain", gain))
		{
			if( GetParameter("output::gamma", gamma))
			{
				prman->RiExposure(gain.data(), gamma.data());
			}
		}

		cls::PA<int> pixelfilter;
		if( !GetParameter("output::filtersize", pixelfilter))
			return false;
		if( pixelfilter.size()!=2)
			return false;
		PS filtername;
		if( !GetParameter("output::filtername", filtername))
			return false;
		std::string filter = filtername.data();

		RtFilterFunc ff = prman->RiBoxFilter();
		if(filter=="gausian")
			ff = prman->RiGaussianFilter();
		else if(filter=="box")
			ff = prman->RiBoxFilter();
		else if(filter=="triangle")
			ff = prman->RiTriangleFilter();
		else if(filter=="catmull-rom")
			ff = prman->RiCatmullRomFilter();
		else if(filter=="separable-catmull-rom")
			ff = prman->RiSeparableCatmullRomFilter();
		else if(filter=="blackman-harris")
			ff = prman->RiBlackmanHarrisFilter();
		else if(filter=="lanczos")
			ff = prman->RiLanczosFilter();
		else if(filter=="mitchell")
			ff = prman->RiMitchellFilter();
		else if(filter=="sinc")
			ff = prman->RiSincFilter();
		else if(filter=="bessel")
			ff = prman->RiBesselFilter();
		else if(filter=="disk")
			ff = prman->RiDiskFilter();
		prman->RiPixelFilter(ff, (float)pixelfilter[0], (float)pixelfilter[1]);

		// Hider
		int hidden__jitter=0;
		if( GetParameter("hidden::jitter", hidden__jitter))
			prman->RiHider("hidden", "int jitter", &hidden__jitter, NULL);

		std::string hidden__depthfilter;
		if( GetParameter("hidden::depthfilter", hidden__depthfilter))
		{
			const char* d = hidden__depthfilter.c_str();
			prman->RiHider("hidden", "string depthfilter", &d, NULL);
		}

		bool hidden__bsigma=0;
		if( GetParameter("hidden::sigma", hidden__bsigma))
		{
			int hidden__sigma = hidden__bsigma?1:0;
			prman->RiHider("stochastic", "int sigma", &hidden__sigma, NULL);
		}
		float hidden__sigmablur=0;
		if( GetParameter("hidden::sigmablur", hidden__sigmablur))
			prman->RiHider("stochastic", "float sigmablur", &hidden__sigmablur, NULL);

		// DEBUG
		{
//			int quantize[4] = {0, 0, 0, 0};
//			float dither[1] = {0};
//			prman->RiDisplayChannel( "vector _environmentdir", "int[4] quantize", quantize, "float dither", dither, NULL);
//			prman->RiDisplayChannel( "float _occlusion", "int[4] quantize", quantize, "float dither", dither, NULL);
//DisplayChannel "color Ambient" "int[4] quantize" [0 0 0 0] "float dither" [0]
//DisplayChannel "color Backscattering" "int[4] quantize" [0 0 0 0] "float dither" [0]
		}
		{
			cls::PSA displaychannels = GetParameter("displaychannels::names");
			cls::PA<int> quantizes = GetParameter("displaychannels::quantize");
			cls::PA<float> dithers = GetParameter("displaychannels::dithers");

			for(int i=0; i<displaychannels.size(); i++)
			{
				int quantize[4] = {0, 0, 0, 0};
				float dither[1] = {0};
				if( 4*i+3 < quantizes.size())
				{
					quantize[0] = quantizes[4*i + 0];
					quantize[1] = quantizes[4*i + 1];
					quantize[2] = quantizes[4*i + 2];
					quantize[3] = quantizes[4*i + 3];
				}

				if(i<dithers.size())
					dither[0] = dithers[i];

				std::string name = displaychannels[i];

				prman->RiDisplayChannel( (char*)name.c_str(), "int[4] quantize", quantize, "float dither", dither, NULL);
			}
		}

		cls::PS displayname="foo";
		cls::PS displaytype="it";
		cls::PS displaymode="rgba";
		GetParameter("output::displayname", displayname);
		GetParameter("output::displaytype", displaytype);
		GetParameter("output::displaymode", displaymode);
		if( strcmp( displaymode.data(), "deepopacity")==0)
		{
			prman->RiDisplay("null", "null", "z", RI_NULL);
			std::string dn = displayname.data();
			dn = "+"+dn;
			prman->RiDisplay((RtToken)dn.c_str(), (RtToken)displaytype.data(), (RtToken)displaymode.data(), RI_NULL);
		}
		else
		{
			prman->RiDisplay((RtToken)displayname.data(), (RtToken)displaytype.data(), (RtToken)displaymode.data(), RI_NULL);
		}

		cls::PS quantizeMode("rgba");
		cls::P<int> quantizeOne(255);
		cls::P<int> quantizeMax(255);
		cls::P<int> quantizeMin(0);
		cls::P<float> quantizeDither(0.5f);
		if( GetParameter("output::quantizeMode", quantizeMode))
		{
			GetParameter("output::quantizeOne", quantizeOne);
			GetParameter("output::quantizeMax", quantizeMax);
			GetParameter("output::quantizeMin", quantizeMin);
			GetParameter("output::quantizeDither", quantizeDither);
			prman->RiQuantize(
				(RtToken)quantizeMode.data(), 
				(int)quantizeOne.data(), 
				(int)quantizeMin.data(), 
				(int)quantizeMax.data(), 
				(float)quantizeDither.data());
		}

		// TRACE
		float tracebreadthfactor = 0;
		this->GetParameter("output::tracebreadthfactor", tracebreadthfactor);
		prman->RiOption("user", "float tracebreadthfactor", &tracebreadthfactor, NULL);

		float tracedepthfactor = 0;
		this->GetParameter("output::tracedepthfactor", tracedepthfactor);
		prman->RiOption("user", "float tracedepthfactor", &tracedepthfactor, NULL);

		int maxdepth = 10;
		if( GetParameter("output::tracemaxdepth", maxdepth))
			prman->RiOption("trace", "int maxdepth", &maxdepth, NULL);

		int statistics_endofframe;
		if( GetParameter("statistics::endofframe", statistics_endofframe))
		{
			prman->RiOption("statistics", "int endofframe", &statistics_endofframe, NULL);
		}
		std::string statistics_xmlfilename;
		if( GetParameter("statistics::xmlfilename", statistics_xmlfilename))
		{
			RtToken val = (RtToken)statistics_xmlfilename.c_str();
			prman->RiOption("statistics", "string xmlfilename", &val, NULL);
		}

		return true;
	}

}
