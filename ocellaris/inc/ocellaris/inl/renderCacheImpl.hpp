#pragma once
namespace cls
{
	template< class STORAGE>
	inline renderCacheImpl<STORAGE>::renderCacheImpl(
		)
	{
		clear();
	}
	template< class STORAGE>
	inline renderCacheImpl<STORAGE>::renderCacheImpl(
		const renderCacheImpl& arg
		)
	{
		this->copy(arg);
	}
	template< class STORAGE>
	inline renderCacheImpl<STORAGE>& renderCacheImpl<STORAGE>::operator=(const renderCacheImpl<STORAGE>& arg)
	{
		this->copy(arg);
		return *this;
	}

	template< class STORAGE>
	inline void renderCacheImpl<STORAGE>::copy(
		const renderCacheImpl<STORAGE>& arg
		)
	{
		storage.copy(arg.storage);
	}

	//! ������ ���������� ���������
	template< class STORAGE>
	inline void renderCacheImpl<STORAGE>::GetGlobalAttributeList(
		std::vector< std::pair< std::string, Param> >& list
		)
	{
		list.clear();
		list.reserve(globalAttributes.size());
		paramlist_t::iterator it = globalAttributes.begin();
		for(; it != globalAttributes.end(); it++)
		{
			std::pair< std::string, Param> p(
				it->first.c_str(), it->second);
			list.push_back(p);
		}
	}

	template< class STORAGE>
	inline void renderCacheImpl<STORAGE>::GlobalAttribute(
		const char *name,		//!< ��� ���������
		const Param& t
		)
	{
		globalAttributes[name] = t;
	}

	// ���������� ��������
	template< class STORAGE>
	inline void renderCacheImpl<STORAGE>::RenderGlobalAttributes(
		IRender* render) const
	{
		paramlist_t::const_iterator it = globalAttributes.begin();
		for(; it != globalAttributes.end(); it++)
		{
			render->GlobalAttribute(it->first.c_str(), it->second);
		}
	}

}