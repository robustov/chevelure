#pragma once

namespace cls
{
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::renderImpl()
	{
		clear();
	}
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::clear()
	{
//		stack.clear();
//		stack.push_back(stackItem());
		branchstack.clear();
		attributeStack.clear();
		transformStack.clear();
	}
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::clear(const Math::Matrix4f& startuptransform)
	{
		branchstack.clear();
		attributeStack.clear();
		transformStack.clear();
		transformStack.SetTransform(startuptransform);
	}
	// ���������� �� ������� �������
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::init_transform(IRender* render)
	{
		Math::Matrix4f WT;
		if( render->GetParameter("worldtransform", WT))
			transformStack.SetTransform(WT);
	}
	// ���������� �� ������� �������
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::init_transform(const Math::Matrix4f& worldtransform)
	{
		transformStack.SetTransform(worldtransform);
	}
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::init_attributes(IRender* render)
	{
		std::vector< std::pair< std::string, Param> > attributes;
		render->GetAttributeList(attributes);
		for( int i=0;i<(int)attributes.size(); i++)
			attributeStack.Attribute(attributes[i].first.c_str(), attributes[i].second);
	}

	// ����������� ������� ���������
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline bool renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::RegisterProcedural(
		const char* procName, 
		IProcedural* procedural
		)
	{
		if(!isNormalExe()) return false;

		if( !procName) return false;
		ProceduralItem item;
		item.proc = procedural;
		proclist[procName] = item;
		return true;
	}


	//! ��������� ���������� (������ ��� ��������� �������)
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::comment(
		const char* text
		)
	{
	};


/////////////////////////////////////////////////////
//! ���������

	//! ��������
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::ParameterV(
		const char *paramname,		//!< ��� ���������
		const Param& p			//!< ��������
		)
	{
		if(!isNormalExe()) return;

		attributeStack.Parameter(paramname, p);
//		stackItem& si = stack.back();
//		si.params[paramname] = p;
	}

	//! ������ ���������
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline Param renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::GetParameterV(
		const char *name
		)
	{
		if(strcmp(name, "worldtransform")==0)
		{
			cls::P<Math::Matrix4f> M( transformStack.getTransform());
			return M;
		}
		return attributeStack.GetParameter(name);
//		stackItem& si = stack.back();
//		std::map<std::string, Param>::iterator it = si.params.find(name);
//		if( it == si.params.end())
//			return Param();
//		return it->second;
	}
	//! �������� �� ��������
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::ParameterFromAttribute(
		const char *paramname,		//!< ��� ���������
		const char *attrname		//!< ��� �������� �� �������� ����� ������
		)
	{
		if(!isNormalExe()) return;

		Param p = GetAttributeV(attrname);
		if( p.empty()) return;

		std::string indexname = "#indexFor_";
		indexname += paramname;
		cls::P<int> index = GetParameter(indexname.c_str());
		if( !index.empty())
		{
			int i = *index;
			if( i<0 || i>=p.size())
			{
				printf("%s out of data %s\n", indexname.c_str(), attrname);
				return;
			}
			Param pi = p[i];
			ParameterV(paramname, pi);
			return;
		}
		ParameterV(paramname, p);
	}
	//! �������� �� ��������
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::ParameterToAttribute(
		const char *paramname,		//!< ��� ���������
		const char *attrname		//!< ��� �������� �� �������� ����� ������
		)
	{
		if(!isNormalExe()) return;

		Param p = GetParameterV(paramname);
		if( p.empty()) return;

		AttributeV(attrname, p);
	}
	//! �������� �� ��������
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::AttributeFromAttribute(
		const char *dst,		//!< ��� ���������
		const char *src			//!< ��� �������� �� �������� ����� ������
		)
	{
		if(!isNormalExe()) return;
#ifdef _DEBUG
//printf("%s -> %s\n", src, dst);
#endif
		Param p = GetAttributeV(src);
		if( p.empty())
			return;
		AttributeV(dst, p);
	}

	//! ������� ��������
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline bool renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::RemoveParameter(
		const char *name
		)
	{
		if(!isNormalExe()) return false;

		return attributeStack.RemoveParameter(name);
//		return false;
	};
	//! ������ ����������
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::GetParameterList(
		std::vector< std::pair< std::string, Param> >& list)
	{
		attributeStack.GetParameterList(list);

//		stackItem& si = stack.back();
//		list.resize(si.params.size());
//		paramlist_t::iterator it = si.params.begin();
//		for(int i=0; it != si.params.end(); it++, i++)
//		{
//			list[i].first = it->first;
//			list[i].second = it->second;
//		}
	}




/////////////////////////////////////////////////////
//! ���������

	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::AttributeV(
		const char *name,		//!< ��� ���������
		const Param& t
		)
	{
		if(!isNormalExe()) return;

		attributeStack.Attribute(name, t);
//		stackItem& si = stack.back();
//		si.attrs[name] = t;
	}
	//! �������
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::GlobalAttribute(
		const char *name,		//!< ��� ���������
		const Param& t
		)
	{
		if(!isNormalExe()) return;

		Attribute(name, t);
	};


	//! ������ ���������
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline Param renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::GetAttributeV(
		const char *name
		)
	{
		return attributeStack.GetAttribute(name);

//		std::list<stackItem>::reverse_iterator sit = stack.rbegin();
//		for( ;sit != stack.rend(); )
//		{
//			stackItem& si = *sit;
//			std::map<std::string, Param>::iterator it = si.attrs.find(name);
//			if( it != si.attrs.end())
//				return it->second;
//			sit++;
//		}
//		return Param();
	}

	//! ������ ���������
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::GetAttributeList(
		std::vector< std::pair< std::string, Param> >& list)
	{
		attributeStack.GetAttributeList(list);
//		stackItem& si = stack.back();
//		list.resize(si.attrs.size());
//		paramlist_t::iterator it = si.attrs.begin();
//		for(int i=0; it != si.attrs.end(); it++, i++)
//		{
//			list[i].first = it->first;
//			list[i].second = it->second;
//		}
	}


	///////////////////////////////////////////////////////////////
	/// ��������� �� �������� ��������
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::IfAttribute(const char* name, const Param& t)
	{
		if( branchstack.isCheck())
		{
			Param p = attributeStack.GetAttribute(name);
			branchstack.If(p, t);
		}
	}
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::EndIfAttribute()
	{
		branchstack.EndIf();
	}



////////////////////////////////////////////
/// PushAttributes/PopAttributes

	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::PushAttributes(void) 
	{
		if(!isNormalExe()) return;

		attributeStack.PushAttributes();
//		stack.push_back(stackItem());
	}
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::PopAttributes(void) 
	{
		if(!isNormalExe()) return;

		attributeStack.PopAttributes(this);
		/*/
		if( stack.size()<=1)
			throw "renderPrmanImpl::PopAttributes ������ �����";

		// ��������� �������
		{
			stackItem& si = stack.back();
			fitrerlist_t::iterator it = si.filters.begin();
			for(;it != si.filters.end(); it++)
			{
				it->first->End(this, it->second);
			}
		}

		stack.pop_back();
		/*/
	}

	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::PushTransform(void) 
	{
		if(!isNormalExe()) return;

		transformStack.PushTransform();
	}
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::PopTransform(void) 
	{
		if(!isNormalExe()) return;

		transformStack.PopTransform();
	}
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::SetTransform()
	{
		if(!isNormalExe()) return;

//		transformStack.SetTransform();
	}
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::AppendTransform() 
	{
		if(!isNormalExe()) return;

		cls::P<Math::Matrix4f> M = GetParameter("transform");
		if( M.empty()) return;
		transformStack.AppendTransform(*M);
	}
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::PinTransform() 
	{
		if(!isNormalExe()) return;

		transformStack.PinTransform();
	}


	///////////////////////////////////////////////////////////////
	/// empty implementation
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::PushRenderAttributes(void)
	{
		if(!isNormalExe()) return;
	}
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::PopRenderAttributes(void)
	{
		if(!isNormalExe()) return;
	}

	///////////////////////////////////////////////////////////////
	/// Filters
	///
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::Filter(IFilter* filter)
	{
		if(!isNormalExe()) return;

		// ��������� ���. ����������
		const Math::Matrix4f& curTransform = transformStack.getTransform();
		attributeStack.Filter(filter, this, curTransform);
//		stackItem& si = stack.back();
//		int id;
//		if( filter->Begin(this, curTransform, id))
//		{
//			si.filters.push_back( std::pair<IFilter*, int>(filter, id) );
//		}
	}
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::Filter(const char* filtername)
	{
		if(!isNormalExe()) return;

		IFilter* filter = findFilter(filtername);
		if(filter)
			Filter(filter);
	}

	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline bool renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::CallFilters(enSystemCall call)
	{
		// ��������� ���. ����������
		const Math::Matrix4f& curTransform = transformStack.getTransform();

		std::list< std::pair<IFilter*, int> > filterlist;
		attributeStack.GetFilterList(filterlist);

		std::list< std::pair<IFilter*, int> >::iterator it = filterlist.begin();
		for( ; it != filterlist.end(); it++)
		{
			IFilter* filter = it->first;
			int id = it->second;
			if( !filter->OnSystemCall(call, this, curTransform, id))
				return false;
		}

		return true;
	}
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline bool renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::CallFilters_onshader(const char* type)
	{
		// ��������� ���. ����������
		const Math::Matrix4f& curTransform = transformStack.getTransform();

		std::list< std::pair<IFilter*, int> > filterlist;
		attributeStack.GetFilterList(filterlist);

		std::list< std::pair<IFilter*, int> >::iterator it = filterlist.begin();
		for( ; it != filterlist.end(); it++)
		{
			IFilter* filter = it->first;
			int id = it->second;
			if( !filter->OnShader(type, this, curTransform, id))
				return false;
		}

		return true;
	}


///////////////////////////////////////////////////////////////
/// Shaders


	/// ���������� �����
	/// ������ �����
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline void renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::Dump( 
		)
	{
		printf("\nDump\n");
		printf("attributeStack:\n");
		attributeStack.Dump();
		printf("transformStack:\n");
		transformStack.Dump();
	}


////////////////////////////////////////////////

	// ���������
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline IProcedural* renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::findProcedure(const char*	procname)
	{
		typename proclist_t::iterator it;
		it = proclist.find(procname);
		if( it==proclist.end())
		{
			Util::DllProcedure dllproc;
			if( !dllproc.LoadFormated(procname, "ocellarisProc.dll"))
				return NULL;

//printf("Laod %s in renderPrmanImpl %x\n", procname, this);

			PROCEDURAL_CREATOR pc = (PROCEDURAL_CREATOR)dllproc.proc;
			IProcedural* proc = (*pc)(this);

			if( !proc)
				return NULL;

			proclist[procname].dll = dllproc;
			proclist[procname].proc = proc;
			return proc;
		}
		return it->second.proc;
	}


	// �������
	template <class ATTRIBUTESTACK, class TRANSFORMSTACK, class BRANCHSTACK>
	inline IFilter* renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::findFilter(const char*	filtername)
	{
		typename filteritemlist_t::iterator it = filteritemlist.find(filtername);
		if( it==filteritemlist.end())
		{
			Util::DllProcedure dllproc;
			if( !dllproc.LoadFormated(filtername, "ocellarisProc"))
				return NULL;

			FILTER_CREATOR pc = (FILTER_CREATOR)dllproc.proc;
			IFilter* proc = (*pc)(this);

			if( !proc)
				return NULL;

			filteritemlist[filtername].dll = dllproc;
			filteritemlist[filtername].proc = proc;
			return proc;
		}
		return it->second.proc;
	}

//////////////////////////////////////////
// ������������� ������� (helper)
// 


}
