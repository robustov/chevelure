#pragma once

#include "math/safevector.h"

namespace cls
{
	template <class TRANSFORMSTACK> inline
	renderMentalRayImpl<TRANSFORMSTACK>::renderMentalRayImpl(
		miTag* result
		)
	{
//printf("new renderMentalRayImpl %x\n", this);
		this->result = result;
		this->bDump = false;
		this->blur_scenetime = 0;
		this->blur_shutterOpen = 0;
		this->blur_shutterClose = 1;

		subrender = NULL;
		
		bBlurFinal = false;

		attributeStack.resetMotion();
	}
	template <class TRANSFORMSTACK> inline
	renderMentalRayImpl<TRANSFORMSTACK>::~renderMentalRayImpl(
		)
	{
//printf("delete renderMentalRayImpl %x\n", this);
	}

	// ���������� ������, ��������� ��������� � ��������
	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::startFrom(
		const renderMentalRayImpl<TRANSFORMSTACK>& src
		)
	{
		this->result				= src.result;
		this->bDump					= src.bDump;				
		this->blur_scenetime		= src.blur_scenetime;	
		this->blur_shutterOpen		= src.blur_shutterOpen;	
		this->blur_shutterClose		= src.blur_shutterClose;	

//		paramlist_t params;
//		paramlist_t attributes;
//		renderMentalRayImpl<TRANSFORMSTACK>* psrc = (renderMentalRayImpl<TRANSFORMSTACK>*)&src;
//		psrc->GetParameterList(params);
//		psrc->GetAttributeList(attributes);

/*/
//		this->PushAttributes();
		{
			paramlist_t::iterator it = attributes.begin();
			for(;it != attributes.end(); it++)
				this->AttributeV(it->first.c_str(), it->second);
		}
		{
			paramlist_t::iterator it = params.begin();
			for(;it != params.end(); it++)
				this->ParameterV(it->first.c_str(), it->second);
		}
//		this->PopAttributes();
/*/

		transformStack.copyFrom( src.transformStack);
		attributeStack.copyFrom( src.attributeStack);

		proclist = src.proclist;
		filteritemlist = src.filteritemlist;
	}

	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::setBlurParams(float blur_scenetime, float blur_shutterOpen, float blur_shutterClose)
	{
		this->blur_scenetime	= blur_scenetime;
		this->blur_shutterOpen	= blur_shutterOpen;
		this->blur_shutterClose = blur_shutterClose;
	}

	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::PushAttributes(void) 
	{
		if(!isNormalExe()) return;

		if( subrender) 
			subrender->PushAttributes();
		else
		{
//			blurstack.push_back(blurstackItem());
			BASE::PushAttributes();
//			prman->RiAttributeBegin();
		}
	}
	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::PopAttributes(void) 
	{
		if(!isNormalExe()) return;

		if( subrender) 
			subrender->PopAttributes();
		else
		{
//			if( blurstack.size()<=1)
//				throw "renderMentalRayImpl<TRANSFORMSTACK>::PopAttributes ������ �����";
//			blurstack.pop_back();
			BASE::PopAttributes();
//			prman->RiAttributeEnd();
		}
	}
	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::AttributeV(
		const char *name,		//!< ��� ���������
		const Param& t
		)
	{
		if(!isNormalExe()) return;
		if( t.empty()) return;

		BASE::AttributeV(name, t);
	}

	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::PushTransform (void)
	{
		if(!isNormalExe()) return;

		if( subrender) 
			subrender->PushTransform();
		else
		{
			BASE::PushTransform();

			fprintf(stderr, "renderMentalRayImpl<> PushTransform not implementrd at all\n");
			fflush(stderr);
		}
	}
	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::PopTransform (void)
	{
		if(!isNormalExe()) return;

		if( subrender) 
			subrender->PopTransform();
		else
		{
			BASE::PopTransform();

			fprintf(stderr, "renderMentalRayImpl<> PopTransform not implementrd at all\n");
			fflush(stderr);
		}
	}
	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::SetTransform ()
	{
		if(!isNormalExe()) return;

		if( subrender) 
			subrender->SetTransform();
		else
		{
			// �������� ����
			fprintf(stderr, "renderMentalRayImpl<> SetTransform not implementrd at all\n");
			fflush(stderr);
		}
	}
	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::AppendTransform ()
	{
		if(!isNormalExe()) return;

		if( subrender) 
			subrender->AppendTransform();
		else
		{
			// �������� ����
			fprintf(stderr, "renderMentalRayImpl<> AppendTransform not implementrd at all\n");
			fflush(stderr);
		}
	}

	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::PushRenderAttributes(void)
	{
		if(!isNormalExe()) return;

	}
	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::PopRenderAttributes(void)
	{
		if(!isNormalExe()) return;

	}

	//@{ 
	/// Motion blur
	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::MotionBegin()
	{
		if(!isNormalExe()) return;

		attributeStack.resetMotion();
	}
	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::MotionEnd()
	{
		if(!isNormalExe()) return;

		attributeStack.setMotionParamPhase(nonBlurValue);
	}
	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::MotionPhaseBegin(float time)
	{
		if(!isNormalExe()) return;

		attributeStack.addMotionParamPhase(time);
		attributeStack.setMotionParamPhase(time);
	}
	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::MotionPhaseEnd()
	{
		if(!isNormalExe()) return;

		attributeStack.setMotionParamPhase(nonBlurValue);
	}
	template <class TRANSFORMSTACK> inline
	bool renderMentalRayImpl<TRANSFORMSTACK>::SetCurrentMotionPhase(float time)
	{
		return attributeStack.setMotionParamPhase(time);
	}

	//@}


	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::Shader(const char *shadertype)
	{
		if(!isNormalExe()) return;

		if( subrender) 
			subrender->Shader(shadertype);
		else
		{
			// �������� ����
			fprintf(stderr, "renderMentalRayImpl<> Shader not implementrd at all\n");
			fflush(stderr);

		}
	}

	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::Light()
	{
		if(!isNormalExe()) return;

		if( subrender) 
			subrender->Light();
		else
		{
			fprintf(stderr, "renderMentalRayImpl<> Light not implementrd at all\n");
			fflush(stderr);
		}
	}

	/// ����� ��������� ���������� ������� ������
	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::SystemCall(
		enSystemCall call
		)
	{
		if(!isNormalExe()) return;

		if( subrender) 
		{
			subrender->SystemCall(call);
			return;
		}
		// visible
		bool bVisible = true;
		GetAttribute("visible", bVisible);
		if( !bVisible) return;
		GetAttribute("@visible", bVisible);
		if( !bVisible) return;

		// ��� �������
		std::string pass;
		GetAttribute("@pass", pass);
		std::string passvisible = "@visible"+pass;
		GetAttribute(passvisible.c_str(), bVisible);
		if( !bVisible) return;

		// final || shadow
		std::string passtype;
		GetAttribute("@passtype", passtype);
		passvisible = "@visible"+passtype;
		GetAttribute(passvisible.c_str(), bVisible);
		if( !bVisible) return;


		bool bOk = CallFilters(call);
		if( !bOk) return;
		singleSystemCall(call);

	}

	/// Call (���������������� �����)
	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::Call( 
		IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
		)
	{
		if(!isNormalExe()) return;
//		if( subrender) 
//			subrender->Call( pProcedural);
//		else
		{
			pProcedural->Process(this);
		}
	}
	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::Call( 
		const char*	procname			//!< eiy oeacaiiia a DeclareProcBegin
		)
	{
		IProcedural* pProcedural = findProcedure(procname);
		if(!pProcedural)
			return;
		this->Call(pProcedural);
	}

		template <class TRANSFORMSTACK> inline
		miBoolean renderMentalRayImpl<TRANSFORMSTACK>::rpi_subdivfunc(miTag tag, miObject*, void *args)
		{
			const char* name = mi_api_tag_lookup(tag);
			mi_api_incremental(miTRUE);

			delayRenderStruct* drs = (delayRenderStruct*)args;
			// �����
			drs->mentalray.Attribute("name", name);
			drs->pDelayRender->Render(
				&drs->mentalray, 
				drs->mentalray.attributeStack.getMotionSamplesCount(), 
				drs->mentalray.attributeStack.getMotionTimes());
//			drs->prman->PopAttributes();
			return miTRUE;
		}
		template <class TRANSFORMSTACK> inline
		miBoolean renderMentalRayImpl<TRANSFORMSTACK>::rpi_freefunc(miTag tag, miObject*, void *args)
		{
			delayRenderStruct* drs = (delayRenderStruct*)args;
			delete drs;
			return miTRUE;
		}

	/// Procedural (���������������� �����)
	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::RenderCall( 
		IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
		)
	{
		if(!isNormalExe()) return;

		bool bVisible = true;
		GetAttribute("visible", bVisible);
		if( !bVisible) return;
		GetAttribute("@visible", bVisible);
		if( !bVisible) return;

		std::string pass;
		GetAttribute("@pass", pass);
		pass = "@visible"+pass;
		GetAttribute(pass.c_str(), bVisible);
		if( !bVisible) return;

		bool bDelay = false;
		if( !this->GetParameter("@delay", bDelay))
			bDelay = false;

		if( !filteritemlist.empty())
			bDelay = false;

		if( !bDelay)
		{
			pProcedural->Render(this, attributeStack.getMotionSamplesCount(), attributeStack.getMotionTimes());
		}
		else
		{
			Math::Box3f box;
			if(!attributeStack.getMotionSamplesCount())
			{
				P<Math::Box3f> _box = this->GetParameter("#bound");
				if( !_box.empty())
					box = _box.data();
			}
			else
			{
				for( int i=0; i<attributeStack.getMotionSamplesCount(); i++)
				{
					float t = attributeStack.getMotionTimes()[i];
					attributeStack.setMotionParamPhase(t);
					P<Math::Box3f> _box = this->GetParameter("#bound");
					if( !_box.empty())
						box.insert( _box.data());
				}
			}
//			RtBound bound;
//			prman_copy(bound, box);
			delayRenderStruct* drs = new delayRenderStruct;
			// ���������� ������� ���������
//			GetParameterList(drs->params);
//			GetAttributeList(drs->attributes);
//			drs->params = si.params;
			drs->pDelayRender = pProcedural;
			drs->mentalray.startFrom(*this);
			pProcedural->addref();
//printf("addref %x.pDelayRender = %x\n", drs, pProcedural);

			cls::P<int> _stindex = this->GetParameter("@startindex");
			int stindex = rand();
			if(!_stindex.empty()) stindex = _stindex.data();
			char name[128];
			sprintf(name, "RenderCall_si%d", stindex);

		    miObject* obj = mi_api_object_begin( mi_mem_strdup(name));
		    mi_api_object_placeholder_callback( rpi_subdivfunc, rpi_freefunc, drs);

			obj->visible = obj->reflection = obj->refraction = obj->shadowmap = miTRUE;
			obj->shadow  = 0x7F;

			obj->bbox_min.x = box.min.x;
			obj->bbox_min.y = box.min.y;
			obj->bbox_min.z = box.min.z;
			obj->bbox_max.x = box.max.x;
			obj->bbox_max.y = box.max.y;
			obj->bbox_max.z = box.max.z;
		    miTag tag = mi_api_object_end();

			mi_geoshader_add_result(this->result, tag);
			obj = (miObject *)mi_scene_edit(tag);
			obj->geo.placeholder_list.type = miOBJECT_HAIR;//miOBJECT_PLACEHOLDER;
			mi_scene_edit_end(tag);
		}
	}
	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::RenderCall( 
		const char*	procname			//!< eiy oeacaiiia a DeclareProcBegin
		)
	{
		// ������� ���� �� �������� � ���������
		if( strcmp(procname, "Camera")==0)
		{
			Camera();
			return;
		}
		else if( strcmp(procname, "Output")==0)
		{
			Output();
			return;
		}

		IProcedural* pProcedural = findProcedure(procname);
		if(!pProcedural)
			return;
		this->RenderCall(pProcedural);
	}






	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::singleSystemCallBeforeBlur(enSystemCall call)
	{
		switch(call)
		{
		case SC_CURVES:
			renderBasis();
			break;
		case SC_POINTS:
		case SC_PATCH:
		case SC_MESH:
		case SC_SPHERE:
		case SC_BLOBBY:
			break;
		}
	}



	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::singleSystemCall(enSystemCall call)
	{
		switch(call)
		{
		case SC_POINTS:
			renderPoints();
			break;
		case SC_CURVES:
			{
			PS interp = IRender::GetParameter("#curves::interpolation");
			PS wrap = IRender::GetParameter("#curves::wrap");
			PA<int> nverts = IRender::GetParameter("#curves::nverts");
			renderCurves(interp.data(), wrap.data(), nverts);
			break;
			}
		case SC_PATCH:
		{
			renderPatch();
			break;
		}
//		case SC_TRIMCURVE:
//			break;
		case SC_MESH:
			{
			PS interp;
			PA<int> loops;
			PA<int> nverts;
			PA<int> verts;
			IRender::GetParameter("mesh::interpolation", interp);
			IRender::GetParameter("mesh::loops", loops);
			IRender::GetParameter("mesh::nverts", nverts);
			IRender::GetParameter("mesh::verts", verts);
//			RemoveParameter("mesh::interpolation");
//			RemoveParameter("mesh::loops");
//			RemoveParameter("mesh::nverts");
//			RemoveParameter("mesh::verts");
			renderMesh(interp.data(), loops, nverts, verts);
			break;
			}
		case SC_SPHERE:
			{
			float radius;
			float zmin;
			float zmax;
			float thetamax;
			IRender::GetParameter("#sphere::radius", radius);
			IRender::GetParameter("#sphere::zmin", zmin);
			IRender::GetParameter("#sphere::zmax", zmax);
			IRender::GetParameter("#sphere::thetamax", thetamax);
			renderSphere(radius, zmin, zmax, thetamax);
			break;
			}
		case SC_BLOBBY:
			{
			PA<Math::Matrix4f> ellipsoids;
			IRender::GetParameter("blobby::ellipsoids", ellipsoids);
			renderBlobby(ellipsoids);
			break;
			}
		}
	}

	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::renderPoints(
		)
	{
		// �������� ����
		fprintf(stderr, "renderMentalRayImpl<> renderPoints not implemented\n");
		fflush(stderr);
	}

	enum enStandartBasis
	{
		SB_BEZIER,	
		SB_B_SPLINE,	
		SB_CATMULL_ROM,
		SB_HERMITE,	
		SB_POWER
	};

	// �� ������ ������ ������ �����!!!
	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::renderBasis()
	{
		// �������� ����
		fprintf(stderr, "renderMentalRayImpl<> renderBasis not implemented\n");
		fflush(stderr);
	}

	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::renderCurves( 
		const char *interp, 
		const char *wrap,		//!< wrap
		PA<int>& nverts			//!< int[ncurves] ����� ��������� � ������
		)
	{
		if( nverts.empty())
			return;
		if( interp==0)
			return;
		if( wrap==0)
			return;

		bool bCubic = strcmp(interp, "cubic")==0;
		cls::PA<float> width = GetParameter("#width");
		cls::PA<Math::Vec3f> _P = GetParameter("P");
		cls::PS name = GetAttribute("name");


		// MI
		miObject* obj = mi_api_object_begin(mi_mem_strdup(name.data()));
		obj->visible = miTRUE;
		obj->reflection = miTRUE;
		obj->refraction = miTRUE;
		obj->shadowmap = miTRUE;
		obj->shadow  = 0x7F;

	    miHair_list* hair = mi_api_hair_begin();

		if( bCubic)
			hair->degree = 3;
		else
			hair->degree = 1;
		//hair->material
		//hair->radius

		int stride = 0;
		int hairstride = 0;
		stride += 3;		// P

		// Width
		mi_api_hair_info(1, 'r', 1);
		stride += 1;

		std::vector< std::pair< std::string, Param> > params;
		GetParameterList(params);

		std::vector<Param> perhairparams, pervertparams;
		perhairparams.reserve( params.size());
		pervertparams.reserve( params.size());

		int perhairTcnt = 0, pervertTcnt = 0;
		std::vector< std::pair< std::string, Param> >::iterator it = params.begin();
		for( int i=0; it != params.end(); it++, i++)
		{
			std::string name = it->first;
			if( name[0]=='#' || name[0]=='@') 
				continue;
			if( name.find(':')!=std::string::npos)
				continue;
			if( name=="P")
				continue;
			Param& param = it->second;
			if( param.empty())
				continue;
			if( name!="u_map1" && name!="v_map1")
				continue;

			if( param->type==PT_FLOAT)
			{
				bool pervertex = param->interp == PI_VERTEX;
				if(pervertex)
				{
					pervertTcnt+=1;
					pervertparams.push_back(param);
				}
				else
				{
					perhairTcnt+=1;
					perhairparams.push_back(param);
				}
			}
		}
		if( perhairTcnt)
		{
			mi_api_hair_info(0, 't', perhairTcnt);
			hairstride+=perhairTcnt;
		}
		if( pervertTcnt)
		{
			mi_api_hair_info(1, 't', pervertTcnt);
			stride+=pervertTcnt;
		}

		// ���������
	    miGeoIndex* indecies = mi_api_hair_hairs_begin( nverts.size()+1 );
		int h;
		int privvertscount = 0;
		for(h=0; h<nverts.size(); h++)
		{
			indecies[h] = privvertscount*stride + h*hairstride;
			privvertscount += nverts[h];
		}
		int fullsize = privvertscount*stride + h*hairstride;
		indecies[h] = fullsize;
	    mi_api_hair_hairs_end();

		// ��������
		miScalar* verts = mi_api_hair_scalars_begin( fullsize);
		privvertscount = 0;
		int privvertscountNoCubic = 0;
		for(h=0; h<nverts.size(); h++)
		{
			miScalar* dsthair = verts + (privvertscount*stride + h*hairstride);

			// per hair data
			int vofs = 0;
			for(int p=0; p<(int)perhairparams.size(); p++)
			{
				Param& param = perhairparams[p];
				if( param->type == PT_FLOAT)
				{
					cls::PA<float> floatparam(param);
					float value = 0;
					if(h<param.size())
						value = floatparam[h];
					else if(!width.empty())
						value = floatparam[0];

					// �������� t ���������
					if( p & 0x1)
						value = 1-value;

					dsthair[vofs++] = value;
				}
			}

			// per vertex data
			int vertcount = nverts[h];
			for(int i=0; i<vertcount; i++)
			{
				int vertexindexP = privvertscount+i;
				int vertexindex = privvertscount+i;
				if( bCubic)
				{
					int iNoCubic = i-1;
					if(iNoCubic<0) iNoCubic=0;
					if(iNoCubic>=vertcount-2) iNoCubic--;
					vertexindex = privvertscountNoCubic+iNoCubic;
				}
				// P
				Math::Vec3f pt = _P[vertexindexP];
				dsthair[vofs++] = pt.x;
				dsthair[vofs++] = pt.y;
				dsthair[vofs++] = pt.z;
				// Width
				float w = 1;
				if(vertexindex<width.size())
					w = width[vertexindex];
				else if(!width.empty())
					w = width[0];
				dsthair[vofs++] = w;
				// ���������
				for(int p=0; p<(int)pervertparams.size(); p++)
				{
					Param& param = pervertparams[p];
					if( param->type == PT_FLOAT)
					{
						cls::PA<float> floatparam(param);
						float value = 0;
						if(vertexindex<param.size())
							value = floatparam[vertexindex];
						else if(!width.empty())
							value = floatparam[0];

						dsthair[vofs++] = value;
					}
				}
			}
			privvertscount += nverts[h];
			privvertscountNoCubic += nverts[h];
			if(bCubic)
				privvertscountNoCubic -= 2;
		}
	    mi_api_hair_scalars_end(fullsize);

		mi_api_hair_end();
		mi_api_object_end();

	}
	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::renderPatch()
	{
		// �������� ����
		fprintf(stderr, "renderMentalRayImpl<> renderPatch not implemented\n");
		fflush(stderr);
	}

	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::renderMesh(
		const char *interp,		//!< ��� ������������
		PA<int>& loops,			//!< loops per polygon
		PA<int>& nverts,		//!< ����� ��������� � �������� (size = polygoncount)
		PA<int>& verts			//!< ������� ��������� ��������� (size = facevertcount)
		)
	{
		// �������� ����
		fprintf(stderr, "renderMentalRayImpl<> renderMesh not implemented\n");
		fflush(stderr);
	}

	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::renderSphere( float radius, float zmin, float zmax, float thetamax)
	{
		// �������� ����
		fprintf(stderr, "renderMentalRayImpl<> renderSphere not implemented\n");
		fflush(stderr);
	}
	/// Blobby
	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::renderBlobby( PA<Math::Matrix4f>& ellipsoids)
	{
		// �������� ����
		fprintf(stderr, "renderMentalRayImpl<> renderBlobby not implemented\n");
		fflush(stderr);
	}
	template <class TRANSFORMSTACK> inline
	void renderMentalRayImpl<TRANSFORMSTACK>::singleShader(const char *shadertype)
	{
		// �������� ����
		fprintf(stderr, "renderMentalRayImpl<> singleShader not implemented\n");
		fflush(stderr);
	}
	template <class TRANSFORMSTACK> inline
	bool renderMentalRayImpl<TRANSFORMSTACK>::Camera()
	{
		fprintf(stderr, "renderMentalRayImpl<> Camera not implemented\n");
		fflush(stderr);
		return true;
	}
	template <class TRANSFORMSTACK> inline
	bool renderMentalRayImpl<TRANSFORMSTACK>::Output()
	{
		fprintf(stderr, "renderMentalRayImpl<> Output not implemented\n");
		fflush(stderr);
		return true;
	}

}
