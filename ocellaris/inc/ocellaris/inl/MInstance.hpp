#pragma once
#include "ocellaris/renderFileImpl.h"

inline void cls::MInstance::serializeOcs(const char* _name, cls::IRender* render, bool bSave)
{
	std::string name = _name;
	SERIALIZEOCS( filename);
}

template<class STREAM>
inline void cls::MInstance::serialise(STREAM& out)
{
	out >> bLoaded;
	out >> box;
	out >> filename;
	out >> bDrawBox;
}

template<class STREAM>
STREAM& operator >> (STREAM& out, cls::MInstance& v)
{
	v.serialise(out);
	return out;
}
namespace cls
{

	inline MInstance::MInstance(
		)
	{
		bLoaded = false;
		oc_cache.rendertype = "opengl";
	}
	inline MInstance::~MInstance(
		)
	{
		clear();
	}
	inline MInstance::MInstance(const MInstance& arg)
	{
		bLoaded = arg.bLoaded;
		box = arg.box;
		filename = arg.filename;
		attributes = arg.attributes;
	//	bUseOcCache = arg.bUseOcCache;
	//arg.oc_cache.storage.Dump();
		oc_cache = arg.oc_cache;
	//	gl_cache = arg.gl_cache;
	//oc_cache.storage.Dump();
		bDrawBox = arg.bDrawBox;
	}
	inline MInstance& MInstance::operator=(const MInstance& arg)
	{
		bLoaded = arg.bLoaded;
		box = arg.box;
		filename = arg.filename;
		attributes = arg.attributes;
	//	bUseOcCache = arg.bUseOcCache;
	//arg.oc_cache.storage.Dump();
		oc_cache = arg.oc_cache;
	//oc_cache.storage.Dump();
	//	gl_cache = arg.gl_cache;
		bDrawBox = arg.bDrawBox;
		return *this;
	}
	inline bool MInstance::LoadForRender(
		const char* filename
		)
	{
		this->filename = filename;
		cls::renderFileImpl infileimpl;
		if( !infileimpl.openForRead(this->filename.c_str()))
			return false;

		Math::Box3f box;
		if( !infileimpl.GetAttribute("file::box", box))
		{
			fprintf(stderr, "no attribute file::box in file %s\n", this->filename.c_str());
			return false;
		}
		this->box = box;

		infileimpl.Render(&oc_cache);
		bLoaded = true;
		return true;
	}
	inline IRender* MInstance::asRender()
	{
		clear();
		return &oc_cache;
	}
	inline void MInstance::setLoaded(
		Math::Box3f box, 
		bool bDrawBox
		)
	{
		this->bDrawBox = bDrawBox;
		if( box.isvalid())
			this->bLoaded = true;
		this->box = box;
	}
	inline bool MInstance::isValid(
		)const
	{
		return bLoaded;
	}
	inline Math::Box3f MInstance::getBox(
		)const
	{
		return box;
	}
	inline const char* MInstance::getFile(
		)const
	{
		if(filename.empty()) return NULL;
		return filename.c_str();
	}
	inline const std::map<std::string, Param>* MInstance::getAttributes(
		)const
	{
		return &attributes;
	}
	inline const renderCacheImpl<CacheStorageSimple>& MInstance::getCache(
		)const
	{
		return oc_cache;
	}
	inline bool MInstance::Render(
		IRender* render
		)const
	{
		oc_cache.Render(render);
		return true;
	}

	inline void MInstance::clear()
	{
		bLoaded = false;
		this->box.reset();
		filename = "";
		oc_cache.clear();
		attributes.clear();
	}
}
