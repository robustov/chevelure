#include "Util/misc_create_directory.h"

namespace cls
{

inline shaderAssemblerImpl::shaderAssemblerImpl(
	shaderProcedure_t* pprocedures
	)
{
	this->pprocedures = pprocedures;
}

// ���������� �������� ���������
inline bool shaderAssemblerImpl::ExternParameter(
	const char* variable, 
	cls::Param defval, 
	cls::enParamType type,
	cls::enParamInterpolation interp
	)
{
	if( defval.empty())
	{
		context->error("variable %s has not default value!!!", variable);
		return false;
	}

	if( type == cls::PT_UNKNOWN)
		type = defval->type;
	defval->type = type;
	std::string strtype = shn::pt_prmantypestr(type);
	std::string strinter = cls::str_interpolation(interp);
	std::string strvalue = shn::pt_string(defval);

	std::string decl = strinter+" "+strtype+" "+std::string(variable)+"="+strvalue;

	externvardeclarations[variable] = decl;
	return true;
}

// ���������� �������� ���������
inline bool shaderAssemblerImpl::InputParameter(
	const char* mayaParameter, 
	const char* shaderVariable, 
	cls::enParamType type,
	cls::enParamInterpolation interp
	)
{
	MPlug plug = MFnDependencyNode(node).findPlug(mayaParameter);
	if( plug.isNull())
	{
		printf("shaderAssembler: Attribute %s not found in node %s\n", mayaParameter, name.c_str());
		return false;
	}
	return InputParameter(plug, shaderVariable, type, interp);
}

// ���������� �������� ���������
inline bool shaderAssemblerImpl::InputParameter(
	MPlug mayaParameter, 
	const char* _shaderVariable,
	cls::enParamType type, 
	cls::enParamInterpolation interp
	)
{
	// ������� ���� � ��������� ��������� ������ - ��������� � ���� IShaderNodeGenerator::BuildShader 
	// ��� ���� ������ ������� - ������ externAttriute � ������� � ������ ����� ��������� ��������� � ������
	// ���� ��� ���������, ������������ ���������

	MPlug plug = mayaParameter;

	std::string shaderVariable = _shaderVariable;
//	Util::correctFileName(shaderVariable, '_', true);

	std::string bodyvarname = name + "_";
	bodyvarname += shaderVariable;
	// ��������� ���
	if( type == cls::PT_UNKNOWN)
	{
		type = ocExport_GetAttrType(node, plug.attribute());
	}
	else if( !ocExport_IsCompatibleType(node, plug.attribute(), type))
	{
		std::string strtype = shn::pt_prmantypestr(type);
		this->varbody += "// "+strtype+" "+bodyvarname+" - incompatible type\n";
		printf("shaderAssembler: Requered Attribute %s not compatible with type %s\n", mayaParameter, strtype.c_str());
		return false;
	}
	std::string strtype = shn::pt_prmantypestr(type);

	// ��������
	cls::Param value = ocExport_GetAttrValue(plug);
	std::string strvalue = shn::pt_string(value);
	std::string strinterp = "";
	if( interp==cls::PI_CONSTANT)
		strinterp = "uniform ";

	bool bExtern = false;

	MPlugArray connectedPlugs;
	bool asSrc = false;
	bool asDst = true;
	plug.connectedTo( connectedPlugs, asDst, asSrc );
	if (connectedPlugs.length() >= 1)
	{
		MPlug otherplug = connectedPlugs[0];
		MObject othernode = otherplug.node();
		cls::IShadingNodeGenerator* generator = context->getShadingNodeGenerator(othernode);
		if( generator)
		{
			// ����������� � �������

			// ��� ���� ����� ���������?
			bool bExist = false;
			if( this->pprocedures)
			{
				std::string procname = MFnDependencyNode(othernode).name().asChar();
				Util::correctFileName(procname, '_', true);

				shaderProcedure_t& procedures = *this->pprocedures;
				shaderProcedure_t::iterator it = procedures.find(procname);
				if( it != procedures.end())
				{
					strvalue = it->second;
					bExist = true;
				}
			}

			// ������� ���������
			if( !bExist)
			{
				std::map<std::string, cls::Param > externParams;
				shaderAssemblerImpl as(this->pprocedures);
				std::string _header, _procedure, _body;
				bool res = as.Build(
					othernode,
					type, 
					generator,
					this->context, 
					externParams,
					_header, 
					_procedure, 
					_body 
					);
				if( res)
				{
					this->header += _header;
					this->childprocedures += _procedure;
					this->childvarbody += _body;
					strvalue = as.name + "_"+as.outputVariable;

					{
						std::map<std::string, std::string>::iterator it = as.externvardeclarations.begin();
						for(;it!=as.externvardeclarations.end(); it++)
							this->externvardeclarations[it->first] = it->second;
					}
					{
						std::map<std::string, std::string>::iterator it = as.externvarplug.begin();
						for(;it!=as.externvarplug.end(); it++)
							this->externvarplug[it->first] = it->second;
					}

					if( this->pprocedures)
						(*this->pprocedures)[as.name] = strvalue;
				}
			}
		}
		else
		{
			// ����������� � ������ �������!
			// �������� �� ������� ��������
			//
			std::string decl = "uniform "+strtype+" "+bodyvarname+"="+strvalue;
			this->externvardeclarations[bodyvarname] = decl;
			this->externvarplug[bodyvarname] = mayaParameter.name().asChar();
			bExtern = true;
		}
	}
	else
	{
		// ��� ��������� - ������ ��������
		if( type == PT_COLOR)
			bExtern = true;
	}

	// �������� � externvarplug � externvardeclarations
	if(bExtern)
	{
		std::string decl = "uniform "+strtype+" "+bodyvarname+"="+strvalue;
		this->externvardeclarations[bodyvarname] = decl;
		this->externvarplug[bodyvarname] = mayaParameter.name().asChar();
	}

	if(!bExtern)
	{
		// � varbody
		std::string str = strinterp+strtype+" ";
		str += bodyvarname;
		str += " = ";
		str += strvalue;
		str += ";\n";
//		str += "// external connect\n";
		this->varbody += str;
	}

	{
		// �������� � ���������� ���������
		if(!proceduredecl.empty())
			proceduredecl += "; ";
		std::string str = strinterp+strtype+" ";
		str += shaderVariable;
		proceduredecl += str;
	}

	{
		// �������� � �����
		if(!callbody.empty())
			callbody += ", ";
		callbody += bodyvarname;
	}

	return true;
}

// ���������� ��������� ���������
inline bool shaderAssemblerImpl::OutputParameter(
	const char* shaderVariable, 
	cls::enParamType type 
	)
{
	std::string strtype = shn::pt_prmantypestr(type);
	std::string bodyvarname = name + "_";
	bodyvarname += shaderVariable;
	{
		// ��� ��������� - ������ ��������
		std::string str = strtype+" ";
		str += bodyvarname;
		str += ";\n";
		this->varbody += str;
	}
	{
		// �������� � ���������� ���������
		if(!proceduredecl.empty())
			proceduredecl += "; ";
		std::string str = strtype+" ";
		str += shaderVariable;
		proceduredecl += "output "+str;
	}
	{
		// �������� � �����
		if(!callbody.empty())
			callbody += ", ";
		callbody += bodyvarname;
	}

	this->outputVariable = shaderVariable;
	return true;
}

// �������� #include ��������� � ������ �������
inline void shaderAssemblerImpl::Include(const char* format, ...)
{
	va_list argList; va_start(argList, format);
	char sbuf[1024];
	_vsnprintf(sbuf, 1023, format, argList);
	va_end(argList);

	for(int i=0; sbuf[i]; i++)
	{
		if( sbuf[i]=='\'')
			sbuf[i]='"';
	}

//	header += "#include \"";
	header += sbuf;
	header += "\n";
}

inline void shaderAssemblerImpl::Add(const char* format)
{
	// 
	std::string sbuf = format;
	for(int i=0; sbuf[i]; i++)
	{
		if( sbuf[i]=='\'')
			sbuf[i]='"';
	}

	procedure += "\t";
	procedure += sbuf;
	procedure += "\n";
}

// �������� ������ � �������
inline void shaderAssemblerImpl::AddV(const char* format, ...)
{
	va_list argList; va_start(argList, format);
	char sbuf[1024];
	_vsnprintf(sbuf, 1023, format, argList);
	va_end(argList);

	this->Add(sbuf);
}

// ��� ����������� �������
inline bool shaderAssemblerImpl::Build(
	MObject node, 
	cls::enParamType wantedoutputtype, 
	cls::IShadingNodeGenerator* generator,
	cls::IExportContext* context, 
	std::map<std::string, cls::Param >& externParams,
	std::string& _header, 
	std::string& _procedure, 
	std::string& _body
	)
{
	this->node = node;
	MFnDependencyNode dn(node);
	this->name = dn.name().asChar();
	Util::correctFileName(this->name, '_', true);
	this->context = context;

	cls::IShadingNodeGenerator::enShadingNodeType type = generator->getShadingNodeType(node);
	std::string outputtype = "void";
	/*/
	if( this->outputVariable.empty())
		outputtype = "void";
	else
	{
		switch(type)
		{
		case cls::IShadingNodeGenerator::COLOR:
			outputtype = "color";  break;
		case cls::IShadingNodeGenerator::FLOAT:
			outputtype = "float";  break;
		}
	}
	/*/


	bool res = generator->BuildShader(node, wantedoutputtype, this, context, MObject::kNullObj);
	if( !res) return false;

	// ������� 
	{
		std::string str;

		// child
		str = childprocedures;

		// ����������
		str += outputtype + " ";
		str += this->name;
		str += "( ";
		str += this->proceduredecl;
		str += ")\n";
		str += "{\n";

		// ���� 
		str += this->procedure;

		str += "}\n";
		_procedure = str;
	}
	{
		_body = this->childvarbody;
		_body += "// "+this->name+"\n";
		_body += this->varbody;

		/*/
		if( !this->outputVariable.empty())
		{
			_body += outputtype+" ";
			_body += this->name+"_"+this->outputVariable;
			_body += " = ";
		}
		/*/
		_body += this->name;
		_body += "("+this->callbody+");\n";
	}
	{
		_header = this->header;
	}

	return true;
}

inline std::string shaderAssemblerImpl::BuildGraph(
	cls::IShadingNodeGenerator::enShadingNodeType shadertype, 
	MObject node, 
	const char* name, 
	cls::IExportContext* context, 
	std::map<std::string, cls::Param >& externParams,
	std::map<std::string, std::string>& externVarPlug
	)
{
	cls::IShadingNodeGenerator* generator = context->getShadingNodeGenerator(node);
	if( !generator)
		return "";

	// ��� ������� �� �����!!!
//	cls::IShadingNodeGenerator::enShadingNodeType type = generator->getShadingNodeType(node);

	shaderProcedure_t procedures;
	this->pprocedures = &procedures;
	std::string _header, _procedure, _body;
	bool res = Build(
		node, 
		cls::PT_COLOR, 
		generator, 
		context, 
		externParams,
		_header, 
		_procedure, 
		_body 
		);
	externVarPlug = this->externvarplug;
	this->pprocedures = NULL;
	// ������� ���
	std::string shader;
	shader += "// OCELLARIS SHADING NETWORK ASSEMBLER\n//\n";
	shader += _header;
	shader += "\n";

	switch(shadertype)
	{
		case cls::IShadingNodeGenerator::SURFACESHADER:
			{
			shader += "surface";
			shader += " ";
			shader += name;
			shader += "(";

			std::map<std::string, std::string>::iterator it = this->externvardeclarations.begin();
			for(;it!=this->externvardeclarations.end(); it++)
			{
				if(it!=this->externvardeclarations.begin())
					shader += "; ";
				shader += "\n\t";
				shader += it->second;
			}

			shader += ")\n";
			shader += "{\n";
			shader += _procedure;
			shader += _body;
			shader += "}\n";
			break;
			}
		case cls::IShadingNodeGenerator::LIGHT:
			{
			shader += "light";
			shader += " ";
			shader += name;
			shader += "(";

			std::map<std::string, std::string>::iterator it = this->externvardeclarations.begin();
			for(;it!=this->externvardeclarations.end(); it++)
			{
				if(it!=this->externvardeclarations.begin())
					shader += "; ";
				shader += "\n\t";
				shader += it->second;
			}

			shader += ")\n";
			shader += "{\n";
			shader += _procedure;
			shader += _body;
			shader += "}\n";
			break;
			}
		case cls::IShadingNodeGenerator::DISPLACE:
			{
			shader += "displace ";
			shader += " ";
			shader += name;
			shader += "(";

			std::map<std::string, std::string>::iterator it = this->externvardeclarations.begin();
			for(;it!=this->externvardeclarations.end(); it++)
			{
				if(it!=this->externvardeclarations.begin())
					shader += "; ";
				shader += "\n\t";
				shader += it->second;
			}

			shader += ")\n";
			shader += "{\n";
			shader += _procedure;
			shader += _body;
			shader += "}\n";
			break;
			}
		case cls::IShadingNodeGenerator::COLOR:
			break;
	}


	return shader;
}


/*/
// ���������� �������� ���������
bool shaderAssemblerImpl::InputManifold(
	const char* mayaParameter, 
	const char* Q, 
	const char* dQu, 
	const char* dQv 
	)
{
	MPlug plug = MFnDependencyNode(node).findPlug(mayaParameter);
	if( plug.isNull())
	{
		printf("shaderAssembler: Attribute %s not found in node %s\n", mayaParameter, name.c_str());
		return false;
	}
	// ���
	std::string strtype = "vector";
	std::string bodyvarname1 = name + "_" + Q;
	std::string bodyvarname2 = name + "_" + dQu;
	std::string bodyvarname3 = name + "_" + dQv;

	MPlugArray connectedPlugs;
	bool asSrc = false;
	bool asDst = true;
	plug.connectedTo( connectedPlugs, asDst, asSrc );
	cls::IShadingNodeGenerator* generator = NULL;
	MPlug otherplug;
	if (connectedPlugs.length() >= 1)
	{
		MPlug otherplug = connectedPlugs[0];
		generator = context->getShadingNodeGenerator(otherplug.node());
	}
	MObject othernode = otherplug.node();
	if( generator && generator->getShadingNodeType(othernode)==cls::IShadingNodeGenerator::MANIFOLD)
	{
		// ������� ���������
		std::map<std::string, cls::Param > externParams;
		shaderAssemblerImpl as;
		std::string _header, _procedure, _body;
		bool res = as.Build(
			othernode,
			generator,
			this->context, 
			externParams,
			_header, 
			_procedure, 
			_body 
			);
		if( res)
		{
			this->header += _header;
			this->childprocedures += _procedure;
			this->childvarbody += _body;
			bodyvarname1 = as.name + "_"+as.outputVariable;
			bodyvarname2 = as.name + "_"+as.outputVariable;
			bodyvarname3 = as.name + "_"+as.outputVariable;
		}
	}
	else
	{
		// �������� �� ����� - �����
		this->varbody += strtype+" "+bodyvarname1+" = vector(0, 0, 0);\n";
		this->varbody += strtype+" "+bodyvarname2+" = vector(0, 0, 0);\n";
		this->varbody += strtype+" "+bodyvarname3+" = vector(0, 0, 0);\n";
	}

	{
		// �������� � ���������� ���������
		if(!proceduredecl.empty())
			proceduredecl += "; ";
		proceduredecl += strtype+" "+std::string(Q);
		proceduredecl += "; ";
		proceduredecl += strtype+" "+std::string(dQu);
		proceduredecl += "; ";
		proceduredecl += strtype+" "+std::string(dQv);
	}

	{
		// �������� � �����
		if(!callbody.empty())
			callbody += ", ";
		callbody += bodyvarname1;
		callbody += ", ";
		callbody += bodyvarname2;
		callbody += ", ";
		callbody += bodyvarname3;
	}
	return true;
}
// ���������� ��������� ���������
bool shaderAssemblerImpl::OutputManifold(
	const char* Q, 
	const char* dQu, 
	const char* dQv 
	)
{
	std::string strtype = "vector";
	std::string bodyvarname1 = name + "_" + Q;
	std::string bodyvarname2 = name + "_" + dQu;
	std::string bodyvarname3 = name + "_" + dQv;
	{
		// ��� ��������� - ������ ��������
		this->varbody += strtype+" "+bodyvarname1+";\n";
		this->varbody += strtype+" "+bodyvarname2+";\n";
		this->varbody += strtype+" "+bodyvarname3+";\n";
	}
	{
		// �������� � ���������� ���������
		if(!proceduredecl.empty())
			proceduredecl += "; ";
		proceduredecl += "output "+strtype+" "+Q;
		proceduredecl += "; ";
		proceduredecl += "output "+strtype+" "+dQu;
		proceduredecl += "; ";
		proceduredecl += "output "+strtype+" "+dQv;
	}
	{
		// �������� � �����
		if(!callbody.empty())
			callbody += ", ";
		callbody += bodyvarname1;
		callbody += ", ";
		callbody += bodyvarname2;
		callbody += ", ";
		callbody += bodyvarname3;
	}
	return true;
}
/*/

}