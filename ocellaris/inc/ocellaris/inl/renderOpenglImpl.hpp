#pragma once
namespace cls
{
	inline renderOpenglImpl::renderOpenglImpl(
		M3dView* view
		)
	{
		bPoints = false;
		degeneration = 1;

		this->view = view;
		box = Math::Box3f();
		transforms.clear();
		transforms.push_back(Math::Matrix4f::id);
	}

	inline void renderOpenglImpl::PushTransform (void)
	{
		glPushMatrix();
		renderImpl<TAttributeStack_minimal, TTransformStack_minimal>::PushTransform();
	}
	inline void renderOpenglImpl::PopTransform (void)
	{
		glPopMatrix(); 
		renderImpl<TAttributeStack_minimal, TTransformStack_minimal>::PopTransform();
	}
	inline void renderOpenglImpl::SetTransform()
	{
		renderImpl<TAttributeStack_minimal, TTransformStack_minimal>::SetTransform();
	}
	inline void renderOpenglImpl::AppendTransform()
	{
		cls::P<Math::Matrix4f> M = GetParameter("transform");
		if( M.empty()) 
			return;
		const float* d = M.data().data();
		glMultMatrixf(d);

		renderImpl<TAttributeStack_minimal, TTransformStack_minimal>::AppendTransform();
	}

	/// Motion blur
	inline void renderOpenglImpl::MotionBegin()
	{
	}
	inline void renderOpenglImpl::MotionEnd()
	{
	}
	inline void renderOpenglImpl::MotionPhaseBegin(float time)
	{
	}
	inline void renderOpenglImpl::MotionPhaseEnd()
	{
	}


	/// ����� ��������� ���������� ������� ������
	inline void renderOpenglImpl::SystemCall(
		enSystemCall call
		)
	{
		bool bVisible = true;
		GetAttribute("visible", bVisible);
		if( !bVisible) return;
		GetAttribute("@visible", bVisible);
		if( !bVisible) return;

		bool bOk = CallFilters(call);
		if( !bOk) return;

		switch(call)
		{
		case SC_POINTS:
			{
			PA<Math::Vec3f> P;
			GetParameter("P", P);
			renderPoints(P);
			break;
			}
		case SC_CURVES:
			{
			std::string interp, wrap;
			PA<int> nverts;
			GetParameter("#curves::interpolation", interp);
			GetParameter("#curves::wrap", wrap);
			GetParameter("#curves::nverts", nverts);
			renderCurves(interp.c_str(), wrap.c_str(), nverts);
			break;
			}
		case SC_PATCH:
		{
			renderPatch();
			break;
		}
//		case SC_TRIMCURVE:
//			break;
		case SC_MESH:
			{
			std::string interp;
			PA<int> loops;
			PA<int> nverts;
			PA<int> verts;
			GetParameter("mesh::interpolation", interp);
			GetParameter("mesh::loops", loops);
			GetParameter("mesh::nverts", nverts);
			GetParameter("mesh::verts", verts);
			renderMesh(interp.c_str(), loops, nverts, verts);
			break;
			}
		case SC_SPHERE:
			{
			float radius;
			float zmin;
			float zmax;
			float thetamax;
			IRender::GetParameter("#sphere::radius", radius);
			IRender::GetParameter("#sphere::zmin", zmin);
			IRender::GetParameter("#sphere::zmax", zmax);
			IRender::GetParameter("#sphere::thetamax", thetamax);
			renderSphere(radius, zmin, zmax, thetamax);
			break;
			}
		case SC_BLOBBY:
			break;
		case SC_TEXT:
			PS text;
			GetParameter("text", text);
			Math::Vec3f pos;
			GetParameter("position", pos);
			renderText(text.data(), pos);
			break;
		}
	}

	/// Call (���������������� �����)
	inline void renderOpenglImpl::Call( 
		IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
		)
	{
		pProcedural->Process(this);
	}
	inline void renderOpenglImpl::Call( 
		const char*	procname			//!< eiy oeacaiiia a DeclareProcBegin
		)
	{
		IProcedural* pProcedural = findProcedure(procname);
		if(!pProcedural)
			return;
		this->Call(pProcedural);
	}

	/// Procedural (���������������� �����)
	inline void renderOpenglImpl::RenderCall( 
		IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
		)
	{
		pProcedural->Render(this, 0, 0);
	}
	inline void renderOpenglImpl::RenderCall( 
		const char*	procname			//!< eiy oeacaiiia a DeclareProcBegin
		)
	{
		IProcedural* pProcedural = findProcedure(procname);
		if(!pProcedural)
		{
			return;
		}
		this->RenderCall(pProcedural);
	}









	inline void renderOpenglImpl::renderPoints(
		PA<Math::Vec3f>& P			//!< points
		)
	{
		Math::Matrix4f& transform = transforms.back();

		float lastPointSize;
		glGetFloatv( GL_POINT_SIZE, &lastPointSize );
		glPointSize( 4.0 );
		glBegin( GL_POINTS );
		for( int i=0; i<P.size(); i++)
		{
			if((i%degeneration)==0)
				glVertex3f(P[i]);
			box.insert( transform*P[i]);
		}
		glEnd();
		glPointSize( lastPointSize );
	}

	inline void renderOpenglImpl::renderCurves( 
		const char *interp, 
		const char *wrap,		//!< wrap
		PA<int>& nverts			//!< int[ncurves] ����� ��������� � ������
		)
	{
		PA<Math::Vec3f> P(GetParameter("P"));

		std::vector<Math::Vec2i> vi;
		vi.reserve(P.size());

		int p=0;
		for(int i=0; i<nverts.size(); i++)
		{
			for(int v=0; v<nverts[i]-1; v++)
			{
				Math::Vec2i seg(p, p+1);
				vi.push_back(seg);
				p++;
			}
			p++;
		}

		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer( 3, GL_FLOAT, 0, &P[0]);
		glDrawElements(GL_LINES, (int)vi.size()*2, GL_UNSIGNED_INT, &vi[0]);
		glDisableClientState(GL_VERTEX_ARRAY);
	}
	inline void renderOpenglImpl::renderMesh(
		const char *interp,		//!< ��� ������������
		PA<int>& loops,			//!< loops per polygon
		PA<int>& nverts,		//!< ����� ��������� � �������� (size = polygoncount)
		PA<int>& verts			//!< ������� ��������� ��������� (size = facevertcount)
		)
	{
		PA<Math::Vec3f> P(GetParameter("P"));

		if( bPoints)
		{
			Math::Matrix4f& transform = transforms.back();
			glBegin( GL_POINTS );
			for( int i=0; i<P.size(); i++)
			{
				if(bPoints && (i%degeneration)==0)
					glVertex3f(P[i]);
				box.insert( transform*P[i]);
			}
			glEnd();
		}
		if( bWireFrame)
		{
			std::vector<Math::Vec2i> vi;
			vi.reserve(verts.size());

			int i=0;
			std::set< Math::Edge32> edges;
			int fv = 0;
			for(int f=0; f<nverts.size(); f++)
			{
				for(int v=0; v<nverts[f]; v++, fv++)
				{
					int vi0, vi1;
					if( v==0)
						vi0 = verts[fv+nverts[f]-1];
					else
						vi0 = verts[fv-1];
					vi1 = verts[fv];
					Math::Edge32 ed(vi0, vi1);
					if( edges.find(ed)!=edges.end()) 
						continue;

					vi.push_back(Math::Vec2i(vi0, vi1));
				}
			}
			glEnableClientState(GL_VERTEX_ARRAY);
			glVertexPointer( 3, GL_FLOAT, 0, &P[0]);
			glDrawElements(GL_LINES, (int)vi.size()*2, GL_UNSIGNED_INT, &vi[0]);
			glDisableClientState(GL_VERTEX_ARRAY);
		}
		if( bShaded)
		{
			Math::Matrix4f testM;
			this->GetParameter("worldtransform", testM);
			float dot = Math::dot( Math::cross( testM[0].as<3>(), testM[1].as<3>()), testM[2].as<3>());

			int shadeModel;
			glGetIntegerv( GL_SHADE_MODEL, &shadeModel);
			glShadeModel(GL_SMOOTH);

			PA<float> s = GetParameter("s");
			PA<float> t = GetParameter("t");
			PA<Math::Vec3f> N(GetParameter("N"));
			if( N.empty())
				N = GetParameter("#N");
			int fv = 0;
			for(int f=0; f<nverts.size(); f++)
			{
				glBegin( GL_POLYGON );
				for(int v=0; v<nverts[f]; v++, fv++)
				{
					int vi = verts[fv];
					Math::Vec3f p = P[vi];

					float scoord=0, tcoord=0;
					if( fv<s.size())
						scoord = s[fv];
					if( fv<s.size())
						tcoord = t[fv];
           			glTexCoord2f( scoord, tcoord);

					if(N.size()==verts.size())
					{
						Math::Vec3f n = N[fv];
						if( dot>0) 
							n = -n;
						glNormal3f( n );
					} 
					glVertex3f( p );
				}
				glEnd();
			}
			glShadeModel(shadeModel);
		}
	}
	inline void renderOpenglImpl::renderSphere(float radius, float zmin, float zmax, float thetamax)
	{
		drawSphere(radius);
	}
	inline void renderOpenglImpl::renderPatch()
	{
		PA<int> ncurves;
		PA<int> order;
		PA<float> knot;
		PA<int> n;
		PA<float> u;
		PA<float> v;

		if ( GetParameter( "ncurves", ncurves) )
		{
			GetParameter( "order", order );
			GetParameter( "knot", knot );
			GetParameter( "n", n );
			GetParameter( "u", u );
			GetParameter( "v", v );
		}

		int nu;
		int uorder;
		PA<float> uknot;
		int nv;
		int vorder;
		PA<float> vknot;
		PA<Math::Vec3f> P;

		GetParameter( "nu", nu );
		GetParameter( "uorder", uorder );
		GetParameter( "uknots", uknot );
		GetParameter( "nv", nv );
		GetParameter( "vorder", vorder );
		GetParameter( "vknots", vknot );
		GetParameter( "P", P );

		GLfloat *points = new GLfloat[ nv*nu*3 ];

		for( int i = 0; i < nu; i++ )
		{
			for( int j = 0; j < nv; j++ )
			{
				int OIndex = ( i*nv + j) * 3;
				int ocsIndex = ( j*nu + i);

				points[OIndex] = P[ocsIndex].data()[0];
				points[OIndex + 1] = P[ocsIndex].data()[1];
				points[OIndex + 2] = P[ocsIndex].data()[2];
			}
		}

		GLboolean auto_normal = glIsEnabled( GL_AUTO_NORMAL );

		if ( !auto_normal )
		{
            glEnable( GL_AUTO_NORMAL );
		}

		GLUnurbsObj *nurbs = gluNewNurbsRenderer();
		gluNurbsProperty( nurbs, GLU_SAMPLING_TOLERANCE, 25.0 );

		if ( bWireFrame )
		{
            gluNurbsProperty( nurbs, GLU_DISPLAY_MODE, GLU_OUTLINE_PATCH );
		}

		int count		= 0;
		int pointCount	= 0;
		int knotCount	= 0;

		gluBeginSurface( nurbs );
		{
			for ( int i = 0; i < ncurves.size(); i++ )
			{
				gluBeginTrim( nurbs );
				{
					for ( int j = 0; j < ncurves[i]; j++ )
					{
						int nknots = order[count] + n[count];

						GLfloat *knots = new GLfloat[ nknots ];

						for( int k = 0; k < nknots; k++, knotCount++ )
						{
							knots[k] = knot[ knotCount ];
						}

						GLfloat *points = new GLfloat[ n[count]*2 ];

						for( int k = 0; k < n[count]*2; k += 2 )
						{
							points[k] = u[ pointCount ];
							points[k + 1] = v[ pointCount ];
							pointCount++;
						}

						gluNurbsCurve( nurbs, nknots, knots, 2, points, order[ count ], GLU_MAP1_TRIM_2 );

						count++;

						delete[] points;
						delete[] knots;
					}
				}
				gluEndTrim( nurbs );
			}
			gluNurbsSurface( nurbs, uknot.size(), uknot.data(), vknot.size(), vknot.data(), nv*3, 3, points, uorder, vorder, GL_MAP2_VERTEX_3 );
		}
		gluEndSurface( nurbs );

		gluDeleteNurbsRenderer( nurbs );

		if ( !auto_normal )
		{
            glDisable( GL_AUTO_NORMAL );
		}

		delete[] points;
	}
	inline void renderOpenglImpl::renderText( 
		const char* text, 
		Math::Vec3f pos
		)
	{
		if( view)
		{
			MPoint pt(0, 0, 0, 1);
			pt.x = pos.x;
			pt.y = pos.y;
			pt.z = pos.z;
			view->drawText(text, MPoint(0, 0, 0, 1));
		}

	}
}