#pragma once

namespace cls
{
	inline void CacheStorageMulty::setKey(int key)
	{
		currentkey = key;
		readit = root.begin();
	}

	inline void CacheStorageMulty::clear()
	{
		instructiontree.clear();
		root = instructiontree.insert( instructiontree.end(), multyinstruction_t());
		cur = root.end();
	}
	inline void CacheStorageMulty::copy(const CacheStorageMulty& arg)
	{
		this->instructiontree = arg.instructiontree;
		this->root = this->instructiontree.begin();
		this->cur = this->root.end();
	}
	inline void CacheStorageMulty::setInstruction(enInstruction type)
	{
		setInstruction(type, "", cls::Param());
	}
	inline void CacheStorageMulty::setInstruction(enInstruction type, const char* name, cls::Param p)
	{
		if(currentkey==0)
		{
			if( type==I_ATTRIBUTE_BEGIN)
			{
				cur = cur.insert( multyinstruction_t());
				(*cur)[currentkey] = Instruction(I_ATTRIBUTE_BEGIN);
				cur = cur.end();
				return;
			}
			instructiontree_t::iterator it;
			it = cur.insert( multyinstruction_t());
			(*it)[currentkey] = Instruction(type, name, p);
			if( type==I_ATTRIBUTE_END)
			{
				cur = cur.parent();
				cur++;
			}
		}
		else
		{
			if( readit.IsEnd())
				return;	// ������!
			multyinstruction_t& mi = *readit;
			multyinstruction_t::iterator it = mi.begin();
			if( it->second.type != type)
				return;	// ������!

			switch(type)
			{
			case I_PARAMETER:
			case I_ATTRIBUTE:
				if(it->second.name != std::string(name))
					return;	// ������!
				mi[currentkey] = Instruction(type, name, p);
				break;
			case I_ATTRIBUTE_BEGIN:
			case I_ATTRIBUTE_END:
			case I_PARAMETERFROMATTR:
			case I_ATTRIBUTEFROMATTR:
			case I_PUSHTRANSFORM:
			case I_POPTRANSFORM:
			case I_SETTRANSFORM:
			case I_APPENDTRANSFORM:
			case I_PINTRANSFORM:

			case I_PUSHRENDERATTRIBUTE:
			case I_POPRENDERATTRIBUTE:

			case I_SYSTEMCALL:
			case I_CALL:
			case I_RENDERCALL:
			case I_FILTER:
			case I_MOTION:
			case I_MOTIONPHASE:
			case I_MOTIONBEGIN:
			case I_MOTIONEND:
			case I_MOTIONPHASEBEGIN:
			case I_MOTIONPHASEEND:
			case I_SHADER:
			case I_LIGHT:
				break;
			}
		
			readit++;
		}
	}
	inline enInstruction CacheStorageMulty::getInstruction()
	{
		return readitmi->second.type;
	}
	inline const char* CacheStorageMulty::getInstructionName()
	{
		return readitmi->second.name.c_str();
	}
	inline cls::Param CacheStorageMulty::getInstructionParam()
	{
		return readitmi->second.param;
	}
	inline bool CacheStorageMulty::startInstruction()
	{
		readit = root;
		readitmi = readit->begin();
		return readit.IsEnd();
	}
	inline bool CacheStorageMulty::nextInstruction()
	{
		bool nexttree = false;
		if( readitmi==readit->end())
			nexttree = true;
		else
		{
			readitmi++;
			if( readitmi==readit->end())
				nexttree = true;
		}
		
		if( nexttree)
		{
			readit++;
			if( readit.IsEnd())
				return true;
			readitmi = readit->begin();
		}
		return readit.IsEnd();
	}
	inline bool CacheStorageMulty::isEnd()
	{
		return readit.IsEnd();
	}
	inline void CacheStorageMulty::Dump() const
	{
		instructiontree_t::const_branch_iterator it = instructiontree.begin();
		for(;it != instructiontree.end(); it++)
		{
			for(int i=0; i<it.level(); i++)
				printf(" ");
			const multyinstruction_t& mi = *it;
			multyinstruction_t::const_iterator itx = mi.begin();
			for(;itx != mi.end(); itx++)
			{
				printf("%d(%d) ", itx->first, itx->second.type);
			}
			printf("\n");
		}
	}
}
