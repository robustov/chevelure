#pragma once
#include "IRender.h"
//#include <ri.h>
#include <map>
#include <list>
#include <vector>
#include <string>
#include "Util/DllProcedure.h"
#include "Util/tree.h"
#include "ocellaris/renderBaseCacheImpl.h"


namespace cls
{
	struct renderCacheIterator;
	//! @ingroup impl_group 
	//! \brief ���������� IRender ��� ����������� 
	//! 
	//! ��� ����������� 
	//! ���������� ����������� GlobalAttribute - ������ �� � ��������� ������, ������� ��� Render ���������� ��� RenderGlobalAttributes
	//! 
	template< class STORAGE = CacheStorageSimple>
	struct renderCacheImpl : public renderBaseCacheImpl<STORAGE, TAttributeStack_minimal, TTransformStack_pure>
	{
		typedef renderBaseCacheImpl<STORAGE, TAttributeStack_minimal, TTransformStack_pure> BASE;

		renderCacheImpl(
			);
		renderCacheImpl(
			const renderCacheImpl& arg
			);

		void copy(
			const renderCacheImpl& arg
			);

		renderCacheImpl& operator=(const renderCacheImpl& arg);
	public:
		// ���������� ��������
		void RenderGlobalAttributes(
			IRender* render)const;
		//! ������ ���������� ���������
		void GetGlobalAttributeList(
			std::vector< std::pair< std::string, Param> >& list
			);
	public:
		//! �������
		virtual void GlobalAttribute(
			const char *name,		//!< ��� ���������
			const Param& t
			);

	protected:
		
		typedef std::map<std::string, Param> paramlist_t;
		paramlist_t globalAttributes;
	};
}
#include "inl/renderCacheImpl.hpp"
