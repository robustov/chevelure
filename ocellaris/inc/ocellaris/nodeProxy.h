#pragma once
#include "INode.h"
#include <maya/MDagPath.h>
#include <maya/MObject.h>

namespace cls
{
	//! @ingroup impl_group 
	//! \brief proxy ���������� INode ���������� ��� ������ � ������ INode
	//! 
	struct nodeProxy : public INode
	{
		nodeProxy(
			INode* node
			)
		{
			this->node = node;
		}

		virtual void Release()
		{
			node->Release();
		}
		virtual MDagPath getPath()
		{
			return node->getPath();
		}
		virtual MObject getObject()
		{
			return node->getObject();
		}
		virtual void setProxyObject(MObject obj)
		{
			return node->setProxyObject(obj);
		}
		// �������� ���������
		virtual void AddGenerator(cls::IGenerator* gen)
		{
			node->AddGenerator(gen);
		}
		// ��������� ���������
		virtual cls::IGenerator* GetGenerator(
			cls::IGenerator::enGenType type, 
			MObject& genobject)
		{
			return node->GetGenerator(type, genobject);
		}
		// ������� ����������
		virtual void Dump(FILE* file)
		{
			node->Dump(file);
		};

	protected:
		INode* node;
	};
}