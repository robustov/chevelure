#pragma once

#include "IOcellaris.h"

#include <maya/MFn.h>
#include <maya/MTypeId.h>
#include <maya/MObject.h>

#include "ocellaris/IRender.h"
#include "ocellaris/IGenerator.h"
#include "ocellaris/INodeGenerator.h"
#include "ocellaris/IAttributeGenerator.h"
#include "ocellaris/IGeometryGenerator.h"
#include "ocellaris/ILightGenerator.h"
#include "ocellaris/IShaderGenerator.h"
#include "ocellaris/ITransformGenerator.h"
#include "ocellaris/INodeFilter.h"
#include "ocellaris/IDeformGenerator.h"
#include "ocellaris/IPassGenerator.h"
#include "ocellaris/IProxyGenerator.h"
#include "ocellaris/IShadingNodeGenerator.h"
#include "Util/DllProcedure.h"
#pragma warning( disable : 4251)

//! @ingroup plugin_group
//! \brief �������� ����������� 
//! 
//! ChiefGenerator - ����������� ������� � ������������
//! ������������ ������� objectset �������� ���������� � ���� � ����������� �� ���� custom ����������
//! 
//! ��� ������� MObject ���������� ���������� �������������� � ����� ����� MObject:
//! 
//! ChiefGenerator::getProxy
//! ChiefGenerator::getFilters
//! ChiefGenerator::getTransform
//! ChiefGenerator::getShader
//! ChiefGenerator::getGeometry
//! ChiefGenerator::getNode
//! ChiefGenerator::getAttribute
//! ChiefGenerator::getDeform
//! ChiefGenerator::getShadingNode
//! ChiefGenerator::getPass
//! 
//! TODO:
//! - ������� ����������� (� ����� � �� ����!!!)
//! - ��������� ������ getXXX, � �� ��� ��� ������
//! - ����� �� classname � type_t
//! - startExport/endExport ������ ������ ����� IGenerator->startExport/endExport � ���������� current dir
//!			���� ��������� � ������ �����!!!
//! - ChiefGenerator::currentDirectory - ��. ����.
//! - 
struct OCELLARISEXPORT_API ChiefGenerator
{
	static MTypeId kNullId;

	//! @ingroup plugin_group
	//! \brief ���� ��� �������� � map ������ �� ���� ���!!!
	//!
	struct type_t
	{
		MFn::Type type;			// ����������� ����
		MTypeId typeId;			// ������������� ����
		std::string classname;	// ???

		type_t( MFn::Type type = MFn::kInvalid)
		{
			this->type = type;
			this->typeId = kNullId;
			this->classname = "";
		}
		type_t( MTypeId typeId)
		{
			this->type = MFn::kInvalid;
			this->typeId = typeId;
			this->classname = "";
		}
		type_t( std::string classname)
		{
			this->type = MFn::kInvalid;
			this->typeId = kNullId;
			this->classname = classname;
		}
		void dump(FILE* file) const
		{
			if( this->type!=MFn::kInvalid)
				fprintf(file, "MFnType(%d)", (int)this->type);
			else
				fprintf(file, "MTypeId(%x)", (int)this->typeId.id());
		}
		bool operator<(const type_t& arg) const
		{
			if(this->type != arg.type)
				return this->type < arg.type;
			if(this->typeId.id() != arg.typeId.id())
				return this->typeId.id() < arg.typeId.id();
//			if(!this->classname.empty() && !arg.classname.empty())
				return this->classname < arg.classname;
//			return false;
		}
		bool operator==(const type_t& arg) const
		{
			if(	this->type == arg.type &&
				this->typeId.id() == arg.typeId.id() &&
				this->classname == arg.classname)
				return true;
			return false;
		}
	};

//////////////////////////////////////////////////////
// ������ ����������� ��� ������� cls::INode
// 
public:
	//! @ingroup plugin_group
	//! \brief ��������� ��� ���������� � ���� ������� �������� �� ��� ���������
	//!
	//! object ����� ���� = 0
	struct GeneratorNHisObject
	{
		cls::IGenerator* gen;
		MObject object;
		GeneratorNHisObject(cls::IGenerator* gen=NULL){this->gen=gen;object=MObject::kNullObj;}
		GeneratorNHisObject(cls::IGenerator* gen, const MObject& object){this->gen=gen;this->object=object;}
		bool operator <(const GeneratorNHisObject& arg)const;

		void dump(FILE* file) const
		{
			if( !this->gen)
				fprintf(file, "generator: NULL");
			else
				fprintf(file, "generator: %s", this->gen->GetUniqueName());
			if( this->object!=MObject::kNullObj)
				fprintf(file, " object: %x", *(int*)&this->object);
		}
	};

	typedef std::vector<GeneratorNHisObject> nodefilterlist_t;
	typedef std::set<GeneratorNHisObject> nodegenlist_t;			// ��� �������� ���� ���������!!!
	typedef std::vector<GeneratorNHisObject> attrgenlist_t;
	typedef std::vector<GeneratorNHisObject> deformgenlist_t;
	typedef std::vector<GeneratorNHisObject> shadergenlist_t;
	typedef std::vector<GeneratorNHisObject> transformgenlist_t;
	typedef std::vector<GeneratorNHisObject> shadingnodegenlist_t;

	typedef std::map<type_t, GeneratorNHisObject >			typesPROXYgens_t;
	typedef std::map<type_t, nodefilterlist_t >				typesFilters_t;
	typedef std::map<type_t, GeneratorNHisObject >			typesGEOMgens_t;
	typedef std::map<type_t, GeneratorNHisObject >			typesLIGHTgens_t;
	typedef std::map<type_t, nodegenlist_t >				typesNODEgens_t;
	typedef std::map<type_t, attrgenlist_t >				typesATTRgens_t;
	typedef std::map<type_t, shadergenlist_t>				typesSHADgens_t;
	typedef std::map<type_t, transformgenlist_t>			typesTRANgens_t;
	typedef std::map<type_t, deformgenlist_t >				typesDEFORMgens_t;
	typedef std::map<type_t, GeneratorNHisObject >			typesPASSgens_t;
	typedef std::map<type_t, shadingnodegenlist_t >			typesSHADINGNODEgens_t;
	typedef std::map<type_t, GeneratorNHisObject >			typesBLURgens_t;

	GeneratorNHisObject getProxy( 
		cls::INode& node);
	void getFilters( 
		cls::INode& node, ChiefGenerator::nodefilterlist_t& gens);
	GeneratorNHisObject getTransform( 
		cls::INode& node);
	void getShader( 
		cls::INode& node, shadergenlist_t& gens);
	GeneratorNHisObject getGeometry( 
		cls::INode& node);
	GeneratorNHisObject getLight( 
		cls::INode& node);
	void getNode( 
		cls::INode& node, nodegenlist_t& gens);
	void getAttribute( 
		cls::INode& node, attrgenlist_t& gens);
	void getDeform( 
		cls::INode& node, deformgenlist_t& gens);
	void getShadingNode( 
		cls::INode& node, 
		shadingnodegenlist_t& gens);
	GeneratorNHisObject getPass(
		cls::INode& node);
	GeneratorNHisObject getBlur(
		cls::INode& node);

	// clear
	void clear();
	// dump
	void dump(FILE* file) const;
	// �������� ��������� ��� ���� �������
	void addGenerator( type_t type, cls::IGenerator* gen);
	bool addGenerator( type_t type, const char* generator);
	// ������� ��������� ��� ���� �������
	void removeGenerator( type_t type, cls::IGenerator* gen);


	// ����������� ����������
	void prepareGenerators( cls::IExportContext* context);


	// ��������� �-��� ������ ��������� �� ����� � ����
	cls::IGenerator* loadGenerator(const char* generator, cls::IGenerator::enGenType type, cls::IExportContext* context);

	/*/
	// ???
	void startExport();
	void endExport();
	/*/

	ChiefGenerator();
	~ChiefGenerator();

	
protected:

	typedef std::set<cls::IGenerator* >	generators_t;

	// ����������
	typesPROXYgens_t typesPROXYgens;
	typesFilters_t  typesFilters;
	typesGEOMgens_t typesGEOMgens;
	typesLIGHTgens_t typesLIGHTgens;
	typesNODEgens_t typesNODEgens;
	typesATTRgens_t typesATTRgens;
	typesSHADgens_t typesSHADgens;
	typesTRANgens_t typesTRANgens;
	typesDEFORMgens_t typesDEFORMgens;
	typesPASSgens_t typesPASSgens;
	typesSHADINGNODEgens_t typesSHADINGNODEgens;
	typesBLURgens_t typesBLURgens;
	// ������ ������ �����������
	generators_t generators;


protected:
	void addGeneratorX( type_t type, cls::IProxyGenerator*);
	void addGeneratorX( type_t type, cls::IPassGenerator*);
	void addGeneratorX( type_t type, cls::INodeFilter*);
	void addGeneratorX( type_t type, cls::ITransformGenerator* gen);
	void addGeneratorX( type_t type, cls::INodeGenerator* gen);
	void addGeneratorX( type_t type, cls::IGeometryGenerator* gen);
	void addGeneratorX( type_t type, cls::ILightGenerator* gen);
	void addGeneratorX( type_t type, cls::IAttributeGenerator* gen);
	void addGeneratorX( type_t type, cls::IShaderGenerator* gen);
	void addGeneratorX( type_t type, cls::IDeformGenerator* gen);
	void addGeneratorX( type_t type, cls::IShadingNodeGenerator* gen);
	void addGeneratorX( type_t type, cls::IBlurGenerator* gen);

protected:
	std::string currentDirectory;

	// ������ ���������� ������� �����������
	std::map<std::string, Util::DllProcedure> externgendlls;

	// 
	//! �������� ������ ����������� ��������� ���� �� set �� MDagPath ��� MObject
	//! ����������� ������ set� ���� ocGeneratorSet
	typedef std::vector<GeneratorNHisObject > generatorlist_t;
	void findGeneratorSetList( MDagPath& path, cls::IGenerator::enGenType type, generatorlist_t& genlist);
	void findGeneratorSetList( MObject& obj, cls::IGenerator::enGenType type, generatorlist_t& genlist);
	//! ���������
	void findGeneratorSetList( MSelectionList& objects, cls::IGenerator::enGenType type, generatorlist_t& genlist);

	// ���������� ������ ���������� � ���� obj ��������� �� genlist
	GeneratorNHisObject findSingleGenerator(MObject obj, std::map<type_t, GeneratorNHisObject >& genlist);

};

// ���������� ������ ���������� � ���� obj ��������� �� genlist
inline ChiefGenerator::GeneratorNHisObject ChiefGenerator::findSingleGenerator(MObject obj, std::map<type_t, GeneratorNHisObject >& genlist)
{
	MTypeId typeId = MFnDependencyNode(obj).typeId();
	std::map<type_t, GeneratorNHisObject>::iterator it = genlist.begin();
	for(;it != genlist.end(); it++)
	{
		type_t type = it->first;
		if( type.type==MFn::kInvalid)
		{
			if( type.typeId == typeId)
				return it->second;
		}
		if( !obj.hasFn(type.type))
			continue;
		return it->second;
	}
	return NULL;
}

inline bool ChiefGenerator::GeneratorNHisObject::operator <(const GeneratorNHisObject& arg)const
{
	if(gen < arg.gen)
		return true;
	if(gen > arg.gen)
		return false;
	return *(int*)(&object) < *(int*)(&arg.object);
}

