#pragma once
#include "IRender.h"
#include <map>
#include <list>
#include <vector>
#include <string>
#include "Util/tree.h"
#include "ocellaris/T/TAttributeStack_forblur.h"
#include "ocellaris/renderCacheImpl.h"

namespace cls
{
	//! @ingroup ocellaris_group 
	//! \brief ������������ � renderBaseCacheImpl ��� �������� ������ ����������
	//! 
	//! TCacheStorageBlur ��������� ������ ��� �������� �����
	struct TCacheStorageBlur
	{
		struct Instruction
		{
			enInstruction type;
			std::string name;
			Param param;
		public:
			Instruction(enInstruction type){this->type=type;}
			Instruction(enInstruction type, const char* name, const Param& param){this->type=type;this->name=name;this->param=param;}
			void Dump(int level) const
			{
				for(int i=0; i<level; i++) printf("  ");
				printf("%s (\"%s\")", Instruction2str(type), name.c_str());
			};
			void Render(IRender* );
		};
		// ������� ����������, �� ����� ��������� ������ 1-�� ������ �� ATTRIBUTE, PARAMETER, CALL
		typedef std::list<Instruction> instructionList_t;

		// ����� - ������� ����������
		typedef std::pair<float, instructionList_t> time_chain_pair_t;

		// ������ �� ��������
		typedef std::list<time_chain_pair_t> phase_chain_t;

		enum enType
		{
			CHAIN = 0,
			PUSHATTR = 1,
			PUSHTRAN = 2,
			PUSHREND = 3,
			MOTIONNOTPARSED = 4,
			ROOT = 5,
		};
		struct ParseItem
		{
			enType type;
			// CHAIN
			phase_chain_t phasechains;
			// PUSHATTR
			int id;	// � ������ �������
			// ��� MOTIONNOTPARSED
			renderCacheImpl<>* cacheMotionInstruction;

			ParseItem(enType type, int id)
			{
				this->type = type;
				this->id = id;
				cacheMotionInstruction = NULL;
			}
			~ParseItem()
			{
				if( cacheMotionInstruction)
					delete cacheMotionInstruction;
			}
			void Dump(int level) const;
		};

		// ������ ParseItem
		typedef std::tree<ParseItem> parsetree_t;
		parsetree_t parsetree;
		parsetree_t::iterator root;

		// ����
		struct ParseStackItem
		{
			// ������� id � ������ PUSHATTR
			int currentPushAttrId;
			// ������� id � ������ CHAIN
			int currentChainId;
			// 
			parsetree_t::iterator parent;
			// ������� �������, ����� ���� NULL
			instructionList_t* currentChain;
			// �������� ������������� ���� CHAIN ������ (�������� �� ������� ������ PopAttributes)
			bool ignoreChain;

			ParseStackItem()
			{
				currentPushAttrId = 0;
				currentChainId = 0;
				currentChain = NULL;
				ignoreChain = false;
			}
		};
		typedef std::list<ParseStackItem> parseStack_t;
		parseStack_t parseStack;

		bool bDump;

		// �������� ������������� ���� ������ (�������� �� ������� ������ PopAttributes)
		int ignoreStack;
		// ���� �� NULL: �������� ������ ����������� ���� ������ (�������� �� ������� ������ MotionEnd)
		// ��� ��� ���������� � ��� �������� ������
		renderCacheImpl<>* cacheMotionInstruction;
		// 
		// ������ ���, ��������� ���� �������
		std::list<float> phases;

		float currentPhase(){if(phases.empty()) return nonBlurValue; return phases.back();};
		bool bFirstPhase(){return phases.size()==1;};		// ������ ����
	public:
		void clear();
		void copy(const TCacheStorageBlur& arg);
		void setInstruction(enInstruction type);
		void setInstruction(enInstruction type, const char* name, cls::Param p);

		bool startInstruction();
		bool nextInstruction();
		bool isEnd();

		enInstruction getInstruction();
		const char* getInstructionName();
		cls::Param getInstructionParam();

		void setCurrentPhase(float phase);

//		void parse();
		void render(IRender* render);

		void Dump() const;

	protected:
		void render(IRender* render, parsetree_t::iterator root);
//		void render(IRender* render, instructiontree_t::iterator start, instructiontree_t::iterator end);

		bool isChainInstruction(enInstruction type);

		// �������� ������������ �������� ������ Motion ������
		void extractNonBlurParameters( 
			phase_chain_t& phasechains, 
			instructionList_t& constantattrss
			);

	};
}
#include "TCacheStorageBlur.hpp"
