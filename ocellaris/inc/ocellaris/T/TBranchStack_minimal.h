#pragma once
#include "IParameter.h"
#include <list>

namespace cls
{
	//! @ingroup ocellaris_group 
	//! \brief ������������ � renderImpl ��� �������� ����� ���������
	//! 
	//! TBranchStack_minimal ������ ���� ��������� ���� ���������� isNormalExe()
	struct TBranchStack_minimal
	{
		TBranchStack_minimal()
		{
			clear();
		}
		void clear()
		{
			bNormalExecution = true;
			ifbranches.clear();
		}
		bool isCheck()const
		{
			return true;
		}
		void If(const cls::Param& arg1, const cls::Param& arg2)
		{
			bool bFlag = false;
			if( arg1.equal(arg2))
				bFlag = true;

			ifbranches.push_back(bNormalExecution);
			bNormalExecution = bFlag&bNormalExecution;
		}
		void EndIf()
		{
			if(ifbranches.empty())
			{
				printf("error in TBranchStack_minimal::EndIf. bad ifbranches stack\n");
				return;
			}
			bNormalExecution = ifbranches.back();
			ifbranches.pop_back();
		}
		bool isNormalExe() const
		{
			return bNormalExecution;
		}
	protected:
		std::list<bool> ifbranches;
		// ������� ���������
		bool bNormalExecution;
	};
}
