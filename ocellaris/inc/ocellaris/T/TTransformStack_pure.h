#pragma once

namespace cls
{
	//! @ingroup ocellaris_group 
	//! \brief ���� �����������. ��� ����������� � ������ IRender
	//! 
	//! ������ ���������� ����� ����������� - �� ������ ����������
	class TTransformStack_pure
	{
	public:
		inline void clear(){};
		inline void copyFrom(const TTransformStack_pure& ){};
		// setup ���������
		inline void PushTransform(){};
		inline void PopTransform(){};
		inline void SetTransform(const Math::Matrix4f& M){};
		inline void AppendTransform(const Math::Matrix4f& M){};
		inline void PinTransform(){};

		// ��� ��������� ���. ����������
		inline const Math::Matrix4f& getTransform()const{return Math::Matrix4f::id;};
		inline const Math::Matrix4f& getTransformInv()const{return Math::Matrix4f::id;};
		inline const Math::Matrix4f& getPinTransform() const{return Math::Matrix4f::id;};

		inline bool setMotionParamPhase(float phase)
		{
			return false;
		}

		void Dump()const
		{
			printf("no transforms:\n"); 
		}
	};
};
