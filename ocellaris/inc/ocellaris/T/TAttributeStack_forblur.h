#pragma once

#include "ocellaris/IParameter.h"
#include "ocellaris/IRender.h"
#include <list>
#include <map>
#include <string>
#include <limits>

#ifdef min
	#undef min
#endif
#ifdef max
	#undef max
#endif

namespace cls
{
	// ������������ ����
//	const float nonBlurValue = -std::numeric_limits<float>::max();

	//! @ingroup ocellaris_group 
	//! \brief ���� ���������. ��� ����������� � ������ IRender
	//! 
	//! ���������� ����� ��������� ��� �������� �������� ��������� ��� ������ ��� �����
	//! ����������� �������� resetMotion addMotionParamPhase � setMotionParamPhase
	class TAttributeStack_forblur
	{
	public:
		inline void clear()
		{
			this->currentParamPhase = nonBlurValue;
			stack.clear();
			stack.push_back(stackItem());
		}
		inline void copyFrom(const TAttributeStack_forblur& src)
		{
			this->stack = src.stack;
			this->_tempforreturn = src._tempforreturn;
			this->motionPhases = src.motionPhases;
			this->currentParamPhase = src.currentParamPhase;
		}
		inline void Parameter(
			const char *paramname,		//!< ��� ���������
			const Param& p			//!< ��������
			)
		{
			bool bNONBLUREparam = false;
			if( paramname[0]=='@')
				bNONBLUREparam = true;

			stackItem& si = stack.back();
			if( bNONBLUREparam)
			{
				paramlist_t& bpl = si.params[nonBlurValue];
				bpl[paramname] = p;
			}
			else
			{
				paramlist_t& bpl = si.params[currentParamPhase];
				bpl[paramname] = p;
			}
		}
		inline Param GetParameter(
			const char *name
			)
		{
			stackItem& si = stack.back();
			{
				paramlist_t& bpl = si.params[currentParamPhase];
				std::map<std::string, Param>::iterator it = bpl.find(name);
				if( it != bpl.end())
					return it->second;
			}
			{
				paramlist_t& bpl = si.params[nonBlurValue];
				std::map<std::string, Param>::iterator it = bpl.find(name);
				if( it != bpl.end())
					return it->second;
			}

			return Param();
		}
		inline bool RemoveParameter(
			const char *name
			)
		{
			stackItem& si = stack.back();
			paramlist_t& bpl = si.params[currentParamPhase];

			std::map<std::string, Param>::iterator it = bpl.find(name);
			if( it == bpl.end())
				return false;
			bpl.erase(it);
			return true;
		}
		//! ������ ����������
		inline void GetParameterList(
			std::vector< std::pair< std::string, Param> >& list)
		{
			list.clear();
			stackItem& si = stack.back();
			paramlist_t& bpl = si.params[currentParamPhase];
			paramlist_t& bplrest = si.params[nonBlurValue];

			list.reserve(bpl.size()+bplrest.size());

			paramlist_t::iterator it = bpl.begin();
			int i;
			for(i=0; it != bpl.end(); it++, i++)
			{
				std::pair< std::string, Param> pair;
				pair.first = it->first;
				pair.second = it->second;
				list.push_back(pair);
			}
			it = bplrest.begin();
			for(; it != bplrest.end(); it++)
			{
				// ������ ���� ������ ��� � ������ ������
				if( bpl.find(it->first)!=bpl.end()) 
					continue;
				std::pair< std::string, Param> pair;
				pair.first = it->first;
				pair.second = it->second;
				list.push_back(pair);
			}
		}
		inline void Attribute(
			const char *name,		//!< ��� ���������
			const Param& t
			)
		{
			stackItem& si = stack.back();

			bool bNONBLUREparam = false;
			if( name[0]=='@')
				bNONBLUREparam = true;

			if(bNONBLUREparam)
			{
				paramlist_t& bpl = si.attrs[nonBlurValue];
				bpl[name] = t;
			}
			else
			{
				paramlist_t& bpl = si.attrs[currentParamPhase];
				bpl[name] = t;
			}
		}
		//! ������ ���������
		inline Param GetAttribute(
			const char *name
			)
		{
			std::list<stackItem>::reverse_iterator sit = stack.rbegin();
			for( ;sit != stack.rend(); )
			{
				stackItem& si = *sit;
				if( currentParamPhase!=nonBlurValue)
				{
					paramlist_t& bpl = si.attrs[currentParamPhase];
					std::map<std::string, Param>::iterator it = bpl.find(name);
					if( it != bpl.end())
						return it->second;
				}
				{
					paramlist_t& bpl = si.attrs[nonBlurValue];
					std::map<std::string, Param>::iterator it = bpl.find(name);
					if( it != bpl.end())
						return it->second;
				}
				sit++;
			}
			return Param();
		}

		//! ������ ���������
		inline void GetAttributeList(
			std::vector< std::pair< std::string, Param> >& list)
		{
			list.clear();

			std::map<std::string, Param> allattributes;

			std::list<stackItem>::iterator it = stack.begin();
			for(int level=0; it!=stack.end(); it++, level++)
			{
				stackItem& si = *it;

				paramlist_t& bpl = si.attrs[currentParamPhase];
				paramlist_t::iterator it = bpl.begin();
				for(; it != bpl.end(); it++)
					allattributes[it->first] = it->second;
				if(currentParamPhase!=nonBlurValue)
				{
					paramlist_t& bpl = si.attrs[nonBlurValue];
					paramlist_t::iterator it = bpl.begin();
					for(; it != bpl.end(); it++)
						allattributes[it->first] = it->second;
				}
			}

			list.reserve(allattributes.size());
			std::map<std::string, Param>::iterator itaa = allattributes.begin();
			for(;itaa != allattributes.end(); itaa++)
			{
				list.push_back(*itaa);
			}



			/*/
			stackItem& si = stack.back();
			list.resize(si.attrs.size());
			paramlist_t::iterator it = si.attrs.begin();
			for(int i=0; it != si.attrs.end(); it++, i++)
			{
				list[i].first = it->first;
				list[i].second = it->second;
			}
			/*/
		}

		void PushAttributes(void) 
		{
			stack.push_back(stackItem());
		}
		void PopAttributes(IRender* pThis) 
		{
			if( stack.size()<=1)
				throw "renderPrmanImpl::PopAttributes ������ �����";

			// ��������� �������
			{
				stackItem& si = stack.back();
				fitrerlist_t::iterator it = si.filters.begin();
				for(;it != si.filters.end(); it++)
				{
					it->first->End(pThis, it->second);
				}
			}
			stack.pop_back();
		}
		inline void Filter(IFilter* filter, IRender* pThis, const Math::Matrix4f& curTransform)
		{
			stackItem& si = stack.back();
			int id;
			if( filter->Begin(pThis, curTransform, id))
			{
				si.filters.push_back( std::pair<IFilter*, int>(filter, id) );
			}
		}
		inline void GetFilterList(std::list< std::pair<IFilter*, int> >& filterlist)
		{
			filterlist.clear();
			// ��� �������
			std::list<stackItem>::reverse_iterator sit = stack.rbegin();
			for( ;sit != stack.rend(); )
			{
				stackItem& si = *sit;
				fitrerlist_t::iterator it = si.filters.begin();
				for(;it != si.filters.end(); it++)
				{
					filterlist.push_back(*it);
				}
				sit++;
			}
//			filterlist;
		}
		void Dump()
		{
			std::list<stackItem>::iterator it = stack.begin();
			for(int level=0; it!=stack.end(); it++, level++)
			{
				int i;
				for(i=0; i<level; i++) printf("  "); printf("level %d:\n", i); 
				it->Dump(i);
			}
		}

	public:
		void resetMotion()
		{
			currentParamPhase = nonBlurValue;
			motionPhases.clear();
		}
		int getMotionSamplesCount(
			)
		{
			return (int)motionPhases.size();
		};
		bool setMotionParamPhase(
			float phase)
		{
			if( phase==nonBlurValue)
			{
				currentParamPhase = phase;
				return true;
			}
			// ����� ���� ���� ����?
			if( motionPhases.find(phase)==motionPhases.end()) return false;
			currentParamPhase = phase;
			return true;
		};
		/*/
		float getMotionTime(
			int phase)
		{
			if( phase<(int)motionPhases.size())
				return motionPhases[phase];
			return 0;
		};
		/*/
		void addMotionParamPhase(float phase)
		{
			if( phase==nonBlurValue)
				return;
			motionPhases.insert(phase);
		}
		float* getMotionTimes()
		{
			if(motionPhases.empty())
				return NULL;
			_tempforreturn.resize(motionPhases.size());
			std::set<float>::iterator it = motionPhases.begin();
			int i;
			for(i=0; it != motionPhases.end(); it++, i++)
				_tempforreturn[i] = *it;
			return &_tempforreturn[0];
		};

	protected:
		//! ���� ����������
		typedef std::map<std::string, Param> paramlist_t;

		//! ���������� ����������
		typedef std::map<float, paramlist_t> blurparamlist_t;

		//! ������� ����� (���� �������� ������ ��� motionblur)
		struct stackItem
		{
			//! ������� ���������
			blurparamlist_t params;
			//! ������� ��������
			blurparamlist_t attrs;
			//! ������� �������
			fitrerlist_t filters;

			void Dump(int level)
			{
				int i;
				{
					for(i=0; i<level; i++) printf("  "); printf("Parameters:\n"); 
					blurparamlist_t::iterator it = params.begin();
					for(;it != params.end(); it++)
					{
						if( it->first != nonBlurValue)
						{
							for(i=0; i<level; i++) printf("  "); 
							printf("#%f\n", it->first);
						}
						paramlist_t& xparams = it->second;
						{
							paramlist_t::iterator itx = xparams.begin();
							for(;itx != xparams.end(); itx++)
							{
								for(i=0; i<level+1; i++) printf("  "); 
								printf("%s[%d]\n", itx->first.c_str(), itx->second.size());
							}
						}
					}
				}
				{
					for(i=0; i<level; i++) printf("  "); printf("Attributes:\n"); 
					blurparamlist_t::iterator it = attrs.begin();
					for(;it != attrs.end(); it++)
					{
						if( it->first!=nonBlurValue)
						{
							for(i=0; i<level; i++) printf("  "); 
							printf("#%f\n", it->first);
						}
						paramlist_t& xparams = it->second;
						{
							paramlist_t::iterator itx = xparams.begin();
							for(;itx != xparams.end(); itx++)
							{
								for(i=0; i<level+1; i++) printf("  "); 
								printf("%s[%d]\n", itx->first.c_str(), itx->second.size());
							}
						}
					}
				}
			}
		};
		//! ����
		std::list<stackItem> stack;
	protected:
		std::vector<float> _tempforreturn;
		// ���� �����
		std::set<float> motionPhases;
		// ������� ����
		float currentParamPhase;

	};
}
