#pragma once
#include <math\Matrix.h>
namespace cls
{
	//! @ingroup ocellaris_group 
	//! \brief ���� �����������. ��� ����������� � ������ IRender
	//! 
	//! ���������� ���������� ����� �����������. ������ ���� ���� �����������
	class TTransformStack_minimal
	{
	public:
		inline TTransformStack_minimal()
		{
			clear();
		}
		inline void clear()
		{
			transforms.clear();
			transforms.push_back( StackItem(Math::Matrix4f::id));
		}
		inline void copyFrom(const TTransformStack_minimal& src)
		{
			transforms.clear();
			transforms.push_back(
				src.transforms.back());
		}

		// setup ���������
		inline void PushTransform()
		{
			transforms.push_back(transforms.back());
		};
		inline void PopTransform()
		{
			transforms.pop_back();
		};
		inline void SetTransform(const Math::Matrix4f& M)
		{
			// SetTransform �� � worldspace!!!
			Math::Matrix4f& pintransform = transforms.back().pintransform;
			Math::Matrix4f m = M*pintransform;
			transforms.back() = M;
		};
		inline void AppendTransform(const Math::Matrix4f& M)
		{
			Math::Matrix4f& transform = transforms.back().transform;
			transform = M*transform;
		};
		// ���������� ���������, ������� ��� SetTransform
		inline void PinTransform()
		{
			Math::Matrix4f pintransform = getTransform();
			transforms.back().pintransform = pintransform;
		};

		// ��� ��������� ���. ����������
		inline const Math::Matrix4f& getTransform() const
		{
			return transforms.back().transform;
		};
		inline Math::Matrix4f getTransformInv() const
		{
			return transforms.back().transform.inverted();
		};
		inline const Math::Matrix4f& getPinTransform() const
		{
			return transforms.back().pintransform;
		};

		inline void Dump() const
		{
			printf("transforms:\n"); 
		}

		inline bool setMotionParamPhase(
			float phase)
		{
			return false;
		}

	protected:
		struct StackItem
		{
			// transform
			Math::Matrix4f transform;
			//���������, ������� ��� SetTransform
			Math::Matrix4f pintransform;

			StackItem()
			{
				transform = Math::Matrix4f::id;
				pintransform = Math::Matrix4f::id;
			}
			StackItem(const Math::Matrix4f& transform)
			{
				this->transform = transform;
				pintransform = Math::Matrix4f::id;
			}
		};
		// ���� �����������
		std::list<StackItem> transforms;
	};
};
