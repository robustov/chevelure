#pragma once

#include "IParameter.h"
#include <list>
#include <map>
#include <string>

namespace cls
{
	//! @ingroup ocellaris_group 
	//! \brief ���� ���������. ��� ����������� � ������ IRender
	//! 
	//! ������ ���������� ����� ��������� - �� ������ ��������
	class TAttributeStack_pure
	{
	public:
		inline void clear()
		{
		}
		inline void Parameter(
			const char *paramname,		//!< ��� ���������
			const Param& p			//!< ��������
			)
		{
		}
		inline Param GetParameter(
			const char *name
			)
		{
			return Param();
		}
		inline bool RemoveParameter(
			const char *name
			)
		{
			return false;
		}
		//! ������ ����������
		inline void GetParameterList(
			std::vector< std::pair< std::string, Param> >& list)
		{
			list.clear();
		}
		inline void Attribute(
			const char *name,		//!< ��� ���������
			const Param& t
			)
		{
		}
		//! ������ ���������
		inline Param GetAttribute(
			const char *name
			)
		{
			return Param();
		}

		//! ������ ���������
		inline void GetAttributeList(
			std::vector< std::pair< std::string, Param> >& list)
		{
			list.clear();
		}

		void PushAttributes(void) 
		{
		}
		void PopAttributes(IRender* pThis) 
		{
		}
		inline void Filter(IFilter* filter, IRender* pThis, const Math::Matrix4f& curTransform)
		{
		}
		inline void GetFilterList(std::list< std::pair<IFilter*, int> >& filterlist)
		{
			filterlist.clear();
		}
		inline void Dump(int level)
		{
			int i=0;
			for(i=0; i<level; i++) printf("  "); printf("No Parameters:\n"); 
			for(i=0; i<level; i++) printf("  "); printf("No Attributes:\n"); 
		}
	};
}
