#pragma once
#include <math\Matrix.h>
namespace cls
{
	//! @ingroup ocellaris_group 
	//! \brief ���� �����������. ��� ����������� � ������ IRender
	//! 
	//! ���������� ����� ����������� ��� �������� �������� ����������� �������� ��� ������ ���� �����
	//! ����������� ������� setMotionParamPhase
	class TTransformStack_forblur
	{
	protected:
		struct StackItem;
		inline StackItem& getCurrentStackItem()
		{
			// ��� ����� ���� ��������
			blurlist_t& list = transforms.back();
			blurlist_t::iterator it = list.find(currentParamPhase);
			if(it == list.end())
			{
				// ��������
				StackItem& rsi = list[currentParamPhase];
				rsi = list[nonBlurValue];
				return rsi;
			}
			else
				return it->second;
		}
		// const
		inline const StackItem& getCurrentStackItem() const
		{
			// ��� ����� ���� ��������
			const blurlist_t& list = transforms.back();
			blurlist_t::const_iterator it = list.find(currentParamPhase);
			if(it == list.end())
			{
				// nonBlurValue ������ ����!!!
				it = list.find(nonBlurValue);
				return it->second;
			}
			else
				return it->second;
		}
	public:
		inline TTransformStack_forblur()
		{
			clear();
		}
		inline void clear()
		{
			transforms.clear();
			transforms.push_back( blurlist_t());
			transforms.back()[nonBlurValue] = StackItem(Math::Matrix4f::id);
		}
		inline void copyFrom(const TTransformStack_forblur& src)
		{
			transforms.clear();
			transforms.push_back(
				src.transforms.back());
			this->currentParamPhase = src.currentParamPhase;
		}

		// setup ���������
		inline void PushTransform()
		{
			transforms.push_back(transforms.back());
		};
		inline void PopTransform()
		{
			transforms.pop_back();
		};
		inline void SetTransform(const Math::Matrix4f& M)
		{
			StackItem& si = getCurrentStackItem();

			// SetTransform �� � worldspace!!!
			Math::Matrix4f& pintransform = si.pintransform;
			Math::Matrix4f m = M*si.pintransform;
			si.transform = m;
		};
		inline void AppendTransform(const Math::Matrix4f& M)
		{
			StackItem& si = getCurrentStackItem();

			Math::Matrix4f& transform = si.transform;
			transform = M*transform;
		};
		// ���������� ���������, ������� ��� SetTransform
		inline void PinTransform()
		{
			// ��� ���� ���������
			blurlist_t& list = transforms.back();
			blurlist_t::iterator it = list.begin();
			for(;it != list.end(); it++)
			{
				StackItem& si = it->second;
				Math::Matrix4f transform = si.transform;
				si.pintransform = transform;
			}
		};

		// ��� ��������� ���. ����������
		inline const Math::Matrix4f& getTransform() const
		{
			const StackItem& si = getCurrentStackItem();
			return si.transform;
		};
		inline Math::Matrix4f getTransformInv() const
		{
			const StackItem& si = getCurrentStackItem();
			return si.transform.inverted();
		};
		inline const Math::Matrix4f& getPinTransform() const
		{
			const StackItem& si = getCurrentStackItem();
			return si.pintransform;
		};

		inline void Dump() const
		{
			printf("transforms:\n"); 
		}

		inline bool setMotionParamPhase(
			float phase)
		{
			currentParamPhase = phase;
			return true;
		}

	protected:
		// ������� ����
		float currentParamPhase;

		struct StackItem
		{
			// transform
			Math::Matrix4f transform;
			//���������, ������� ��� SetTransform
			Math::Matrix4f pintransform;

			StackItem()
			{
				transform = Math::Matrix4f::id;
				pintransform = Math::Matrix4f::id;
			}
			StackItem(const Math::Matrix4f& transform)
			{
				this->transform = transform;
				pintransform = Math::Matrix4f::id;
			}
		};
		typedef std::map<float, StackItem> blurlist_t;

		// ���� �����������
		std::list<blurlist_t> transforms;
	};
};
