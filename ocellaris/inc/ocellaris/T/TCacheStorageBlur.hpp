#pragma once
#include "ocellaris/T/TAttributeStack_forblur.h"

#pragma warning (disable : 4313)

namespace cls
{
	inline void TCacheStorageBlur::clear()
	{
		bDump = false;
		parsetree.clear();
		root = parsetree.insert( parsetree.end(), ParseItem(ROOT, 0));

		parseStack.clear();
		parseStack.push_back(ParseStackItem());
		cacheMotionInstruction = NULL;

		ignoreStack = 0;
		phases.clear();
	}
	inline void TCacheStorageBlur::setCurrentPhase(float phase)
	{
		ignoreStack = 0;
		cacheMotionInstruction = NULL;
		phases.push_back(phase);

		parseStack.clear();
		parseStack.push_back(ParseStackItem());
		ParseStackItem& stackItem = parseStack.back();
		stackItem.parent = root;
		/*/
		stackItem.currentChain = 0xFFFFFFFF;
		stackItem.currentChainId = 0xFFFFFFFF;
		stackItem.currentPushAttrId = 0xFFFFFFFF;
		/*/
	}

	inline void TCacheStorageBlur::render(IRender* render)
	{
		TCacheStorageBlur::render(render, root);
	}
	inline void TCacheStorageBlur::render(IRender* render, parsetree_t::iterator rootit)
	{
		parsetree_t::iterator itp = rootit.begin();
		for(;itp != rootit.end(); itp++)
		{
			enType type = itp->type;
			if(type==CHAIN)
			{
				if( itp->phasechains.size()!=phases.size())
					continue;	// �� ��������� ����� ��� - �������� �������

				time_chain_pair_t& first = itp->phasechains.front();
				if( first.second.size()==0)
					continue;	// ������ �������!!!
				// �� ���� ���������� ����� ���������!
				Instruction last = first.second.back();
				bool bChainInstr = isChainInstruction(last.type);

				// �������� ������������ �������� ������
				instructionList_t constantattrs;
				extractNonBlurParameters( itp->phasechains, constantattrs);
				

				////////////////////////////////
				// �������� ��������� ����������
				phase_chain_t::iterator pcit = itp->phasechains.begin();
				int sumInstructionCount = 0;
				for( ;pcit!=itp->phasechains.end();pcit++)
				{
					if( pcit->second.size()==0)
						break;	// ������ �������!!!
					Instruction& _last = pcit->second.back();
					bool _bChainInstr = isChainInstruction(last.type);
					if( _bChainInstr!=bChainInstr)
						break;	// �� ��������!!!
					if( !bChainInstr)
					{
						if(_last.type!=last.type)
							break;	// �� ��������!!!
						// ��� ���������. ������� ����.����������
						pcit->second.pop_back();
					}
					sumInstructionCount += (int)pcit->second.size();
				}
				if( pcit!=itp->phasechains.end())
					continue;		// ���� ������!!!

				//////////////////////////////////////////////
				// ��������!
				if( sumInstructionCount!=0)
				{
					render->MotionBegin();
					pcit = itp->phasechains.begin();
					for( ;pcit!=itp->phasechains.end();pcit++)
					{
						render->MotionPhaseBegin(pcit->first);
						instructionList_t::iterator iti = pcit->second.begin();
						for(;iti != pcit->second.end();iti++)
						{
							Instruction& i = *iti;
							i.Render(render);
						}
						render->MotionPhaseEnd();
					}
					render->MotionEnd();
				}
				// ��������� ����������
				if( !bChainInstr)
					last.Render(render);
			}
			else if(type==PUSHATTR)
			{
				render->Parameter("id", itp->id);
				render->PushAttributes();
				this->render(render, itp);
				render->PopAttributes();
			}
			else if(type==PUSHTRAN)
			{
				render->Parameter("id", itp->id);
				render->PushTransform();
				this->render(render, itp);
				render->PopTransform();
			}
			else if(type==PUSHREND)
			{
				render->Parameter("id", itp->id);
				render->PushRenderAttributes();
				this->render(render, itp);
				render->PopRenderAttributes();
			}
			else if(type==MOTIONNOTPARSED)
			{
				// ��������� � render ��� ��� ��������
				renderCacheImpl<>* cache = itp->cacheMotionInstruction;
				if( cache)
				{
					render->MotionBegin();
					cache->Render(render);
					render->MotionEnd();
				}
			}
		}
	}
	// �������� ������������ �������� ������ Motion ������
	inline void TCacheStorageBlur::extractNonBlurParameters( 
		phase_chain_t& phasechains, 
		instructionList_t& constantattrss
		)
	{
	}

	inline void TCacheStorageBlur::setInstruction(enInstruction type)
	{
		setInstruction(type, "", cls::Param());
	}
	inline void TCacheStorageBlur::setInstruction(enInstruction type, const char* name, cls::Param p)
	{
		if(phases.empty())
		{
			printf("TCacheStorageBlur isnt initialized!\n");
			return;
		}
		// �������� ������������� ������ �� �������� � ����
		if( ignoreStack!=0)
		{
			if( type==I_ATTRIBUTE_BEGIN || 
				type==I_PUSHTRANSFORM ||
				type==I_PUSHRENDERATTRIBUTE
				)
				ignoreStack++;
			if( type==I_ATTRIBUTE_END || 
				type==I_POPTRANSFORM ||
				type==I_POPRENDERATTRIBUTE
				)
				ignoreStack--;
			return;
		}
		ParseStackItem& stackItem = parseStack.back();

if(bDump)
	printf("setInstruction %04x: si=%x, P=%x, current chain=%x (push_id=%d, chain_id = %d)\n", (int)type, &stackItem, &*stackItem.parent, stackItem.currentChain, stackItem.currentPushAttrId, stackItem.currentChainId);

		// ��� ����������� ����!!!
		{
			if( type==I_MOTIONEND)
			{
				// ��������� ������������� ���� ������ (�������� �� ������� ������ MotionEnd)
				cacheMotionInstruction = NULL;
				return;
			}
			if( cacheMotionInstruction)
			{
				// ���������� ��� ������� � ��� ��� ���������
				cacheMotionInstruction->storage.setInstruction(type, name, p);
				return;
			}
			if( type==I_MOTIONBEGIN)
			{
				parsetree_t::iterator cur = stackItem.parent.end();
				parsetree_t::iterator newit = cur.insert( ParseItem(MOTIONNOTPARSED, 0));
				newit->cacheMotionInstruction = new renderCacheImpl<>();
				// �������� ������������� ���� ������ (�������� �� ������� ������ MotionEnd)
				cacheMotionInstruction = newit->cacheMotionInstruction;
				return;
			}
		}

		if( type==I_ATTRIBUTE_BEGIN || 
			type==I_PUSHTRANSFORM ||
			type==I_PUSHRENDERATTRIBUTE
			)
		{
			enType INTRTYPE = (type==I_ATTRIBUTE_BEGIN)?PUSHATTR:PUSHTRAN;
			if( type==I_PUSHRENDERATTRIBUTE)
				INTRTYPE = PUSHREND;
			if(bFirstPhase())
			{
				// ������� �������
				stackItem.currentChain = NULL;
				// �������� { � ������
				parsetree_t::iterator cur = stackItem.parent.end();
				parsetree_t::iterator newit = cur.insert( ParseItem(INTRTYPE, stackItem.currentPushAttrId));
				// ���� id
				stackItem.currentPushAttrId++;
				// �������� stackItem
				parseStack.push_back(ParseStackItem());
				ParseStackItem& stackItem = parseStack.back();
				stackItem.parent = newit;
				if(bDump)
					printf("START. new P = %x. current chain=%x\n", &*stackItem.parent, stackItem.currentChain);
			}
			else
			{
				// ����� ������ PUSHATTR �� id
				if(bDump)
					printf("FINDING currentPushAttrId=%d in %x\n", stackItem.currentPushAttrId, &*stackItem.parent);
				parsetree_t::iterator it = stackItem.parent.begin();
				for( ; it!=stackItem.parent.end(); it++)
				{
					if(it->type != INTRTYPE) continue;
					if(it->id!=stackItem.currentPushAttrId) continue;
					// �����!
					// ������� �������
					stackItem.currentChain = NULL;
					// ���� id
					stackItem.currentPushAttrId++;
					// �������� stackItem
					parseStack.push_back( ParseStackItem());
					ParseStackItem& stackItem = parseStack.back();
					stackItem.parent = it;
					if(bDump)
						printf("  FIND P=%x\n", &*stackItem.parent);
//					if(bDump)
//						printf("  ADD CHAIN %x\n", &stackItem);
					return;
				}
				// �� ����� ������ ��������
				// ����� ������������ ��� ��������� ������ �� ������� I_ATTRIBUTE_END
				ignoreStack = 1;
				if(bDump)
					printf("START ignoreStack\n");
				return;
			}
			return;
		}

		if( type==I_ATTRIBUTE_END || 
			type==I_POPTRANSFORM || 
			type==I_POPRENDERATTRIBUTE
			)
		{
			if( parseStack.size()==1)
			{
				stackItem.ignoreChain = true;
				ignoreStack = 1;
				printf("error in TCacheStorageBlur::setInstruction. Bad call I_ATTRIBUTE_END or I_POPTRANSFORM!!!\n");
				return;
			}
			parseStack.pop_back();
			ParseStackItem& stackItem = parseStack.back();
			if(bDump)
				printf("END. P=%x. current chain=%x\n", &*stackItem.parent, stackItem.currentChain);
			return;
		}
		if( type==I_PARAMETER)
		{
			// ���� ��� id - ��������� ��� currentPushAttrId! � �������� ���������� ���� �� ���������
			if(strcmp(name, "id")==0)
			{
				P<int> pid = p;
				if(!pid.empty())
				{
					stackItem.currentPushAttrId = *pid;
					return;
				}
			}
		}
		// �������� ������������� ���� CHAIN ������ (�������� �� ������� ������ PopAttributes)
		if( stackItem.ignoreChain)
			return;
		// ���� ��� ������� - ������ �����
		if( !stackItem.currentChain)
		{
			if(bDump)
				printf("need new chain:\n");
			if( bFirstPhase())
			{
				// ������� CHAIN � ������
				parsetree_t::iterator cur = stackItem.parent.end();
				parsetree_t::iterator newit = cur.insert( ParseItem(CHAIN, stackItem.currentChainId));
				stackItem.currentChainId++;
				// ����� �������
				newit->phasechains.push_back(
					time_chain_pair_t(currentPhase(), instructionList_t()));
				stackItem.currentChain = &newit->phasechains.back().second;
				if(bDump)
					printf("  new P=%x. new cain = %x\n", &*newit, stackItem.currentChain);
			}
			else
			{
				// ����� ������� �� currentChainId
				parsetree_t::iterator it = stackItem.parent.begin();
				for( ; it!=stackItem.parent.end(); it++)
				{
					if(it->type != CHAIN) continue;
					if(it->id!=stackItem.currentChainId) continue;
					break;
				}
				if( it!=stackItem.parent.end())
				{
					// �����!
					// ���� id
					stackItem.currentChainId++;
					// �������� �������
					it->phasechains.push_back(
						time_chain_pair_t(currentPhase(), instructionList_t()));
					stackItem.currentChain = &it->phasechains.back().second;
					if(bDump)
						printf("  find P=%x\n", &*it, stackItem.currentChain);
				}
				else
				{
					if(bDump)
						printf("  ignoreChain\n");
					// �� �����!
					// �������� ������������� ���� CHAIN ������
					stackItem.ignoreChain = true;
					return;
				}
			}
		}

		switch(type)
		{
			// ������� �� ����������� �������
			case I_PARAMETER:
			case I_ATTRIBUTE:
			case I_PARAMETERFROMATTR:
			case I_PARAMETERTOATTR:
			case I_ATTRIBUTEFROMATTR:
			case I_IFATTRIBUTE:
			case I_ENDIFATTRIBUTE:
			case I_CALL:
				{
					if(bDump)
						printf("error in TCacheStorageBlur::setInstruction PUT INSTRUCTION. current chain = %x\n", stackItem.currentChain);
					// �������� ������� � �������
					stackItem.currentChain->push_back(
						Instruction(type, name, p)
						);
					break;
				}
			// ������� ����������� �������
//			case I_PUSHTRANSFORM:
//			case I_POPTRANSFORM:
			case I_SETTRANSFORM:
			case I_APPENDTRANSFORM:
//			case I_PINTRANSFORM:
			case I_SYSTEMCALL:
			case I_RENDERCALL:
			case I_FILTER:
			case I_SHADER:
			case I_LIGHT:
				{
					// �������� ������� � ������� � �������� �������
					stackItem.currentChain->push_back(
						Instruction(type, name, p)
						);
					stackItem.currentChain = NULL;
					if(bDump)
						printf("error in TCacheStorageBlur::setInstruction PUT & STOP INSTRUCTION. current chain = %x\n", stackItem.currentChain);
					break;
				}
			case I_MOTION:
			case I_MOTIONPHASE:
			case I_MOTIONBEGIN:
			case I_MOTIONEND:
			case I_MOTIONPHASEBEGIN:
			case I_MOTIONPHASEEND:
				// ��� ��� ������
				printf("error in TCacheStorageBlur::setInstruction. instructions I_MOTION... are ignored!!!\n");
				return;
		}
		// ��! ����� ��� ���!!!
	}
	inline bool TCacheStorageBlur::isChainInstruction(enInstruction type)
	{
		switch(type)
		{
			// ������� �� ����������� �������
			case I_PARAMETER:
			case I_ATTRIBUTE:
			case I_PARAMETERFROMATTR:
			case I_PARAMETERTOATTR:
			case I_ATTRIBUTEFROMATTR:
			case I_CALL:
				return true;
			// ������� ����������� �������
			case I_PUSHTRANSFORM:
			case I_POPTRANSFORM:
			case I_SETTRANSFORM:
			case I_APPENDTRANSFORM:
			case I_SYSTEMCALL:
			case I_RENDERCALL:
			case I_FILTER:
			case I_SHADER:
			case I_LIGHT:
				return false;
			case I_MOTION:
			case I_MOTIONPHASE:
			case I_MOTIONBEGIN:
			case I_MOTIONEND:
			case I_MOTIONPHASEBEGIN:
			case I_MOTIONPHASEEND:
				// ��� ��� ������
				return false;
		}
		return false;
	}

	inline enInstruction TCacheStorageBlur::getInstruction()
	{
//		return readit->type;
		return I_UNKNOWN;
	}
	inline const char* TCacheStorageBlur::getInstructionName()
	{
//		return readit->name.c_str();
		return "";
	}
	inline cls::Param TCacheStorageBlur::getInstructionParam()
	{
//		return readit->param;
		return Param();
	}
	inline bool TCacheStorageBlur::startInstruction()
	{
		return false;
//		readit = root;
//		return readit.IsEnd();
	}
	inline bool TCacheStorageBlur::nextInstruction()
	{
		return false;
//		readit++;
//		return readit.IsEnd();
	}
	inline bool TCacheStorageBlur::isEnd()
	{
		return true;
//		return readit.IsEnd();
	}

	inline void TCacheStorageBlur::Dump() const
	{
		parsetree_t::const_branch_iterator it = parsetree.begin();
		for(;it != parsetree.end(); it++)
		{
			for(int i=0; i<it.level(); i++) printf("  ");
			it->Dump(it.level());
			printf("");
		}
	}

	inline void TCacheStorageBlur::ParseItem::Dump(int level) const
	{
		printf("%x: ", this);
		if(type==CHAIN)
		{
			printf("CHAIN: #%d\n", id);
			phase_chain_t::const_iterator it = phasechains.begin();
			for(;it != phasechains.end(); it++)
			{
				for(int i=0; i<level+1; i++) printf("  ");
				const instructionList_t& il = it->second;
				printf("T=%f chain=%x\n", it->first, &il);
				instructionList_t::const_iterator itin = il.begin();
				for(;itin != il.end(); itin++)
				{
					itin->Dump(level+2);
					printf("\n");
				}

			}
		}
		if(type==PUSHATTR)
		{
			printf("PUSHATTR: id=%d\n", id);
		}
		if(type==PUSHTRAN)
		{
			printf("PUSHTRAN: id=%d\n", id);
		}
		if(type==PUSHREND)
		{
			printf("PUSHTRAN: id=%d\n", id);
		}
		if(type==ROOT)
		{
			printf("ROOT\n");
		}
	}
	inline void TCacheStorageBlur::Instruction::Render(IRender* render)
	{
		switch(type)
		{
			case I_ATTRIBUTE_BEGIN:
				render->PushAttributes();
				break;
			case I_ATTRIBUTE_END:
				render->PopAttributes();
				break;
			case I_PARAMETER:
				render->ParameterV(name.c_str(), param);
				break;
			case I_ATTRIBUTE:
				render->AttributeV(name.c_str(), param);
				break;
			case I_PARAMETERFROMATTR:
				{
				cls::PS attrname = param;
				render->ParameterFromAttribute(name.c_str(), attrname.data());
				break;
				}
			case I_PARAMETERTOATTR:
				{
				cls::PS attrname = param;
				render->ParameterToAttribute(name.c_str(), attrname.data());
				break;
				}
			case I_ATTRIBUTEFROMATTR:
				{
				cls::PS attrname = param;
				render->AttributeFromAttribute(name.c_str(), attrname.data());
				break;
				}
			case I_PUSHTRANSFORM:
				render->PushTransform();
				break;
			case I_POPTRANSFORM:
				render->PopTransform();
				break;
			case I_SETTRANSFORM:
				render->SetTransform();
				break;
			case I_APPENDTRANSFORM:
				render->AppendTransform();
				break;
			case I_PINTRANSFORM:
				render->PinTransform();
				break;
			case I_PUSHRENDERATTRIBUTE:
				render->PushRenderAttributes();
				break;
			case I_POPRENDERATTRIBUTE:
				render->PopRenderAttributes();
				break;
			case I_SYSTEMCALL:
				{
					P<int> en(param);
					render->SystemCall( (enSystemCall)en.data());
					break;
				}
			case I_CALL:
				{
					const char* proc = name.c_str();
					render->Call(proc);
					break;
				}
				break;
			case I_RENDERCALL:
				{
					const char* proc = name.c_str();
					render->RenderCall(proc);
					break;
				}
			case I_FILTER:
				{
					const char* proc = name.c_str();
					render->Filter(proc);
					break;
				}
				break;

			case I_MOTIONBEGIN:
				render->MotionBegin();
				break;
			case I_MOTIONEND:
				render->MotionEnd();
				break;
			case I_MOTIONPHASEBEGIN:
				{
					float time = P<float>(param).data();
					render->MotionPhaseBegin(time);
					break;
				}
			case I_MOTIONPHASEEND:
				render->MotionPhaseEnd();
				break;
			case I_SHADER:
				{
					const char* shadertype = name.c_str();
					render->Shader(shadertype);
				}
				break;
			case I_LIGHT:
				render->Light();
				break;
		}
	}

	inline void TCacheStorageBlur::copy(const TCacheStorageBlur& arg)
	{
		clear();
	}
}
