#pragma once
#include "ocellaris/T/TAttributeStack_forblur.h"

namespace cls
{
	inline void TCacheStorageBlur::clear()
	{
		instructiontree.clear();
		root = instructiontree.insert( instructiontree.end(), Instruction(nonBlurValue, I_UNKNOWN));
		cur = root.end();
		ignoreStack = 0;
		phases.clear();
	}
	inline void TCacheStorageBlur::setCurrentPhase(float phase)
	{
		phases.push_back(phase);
		root->currentid = 0;
		cur = root.begin();
	}
	inline void TCacheStorageBlur::parse()
	{
		// �������� I_ATTRIBUTE_BEGIN � �������� ���-��� ���������
		instructiontree_t::branch_iterator it = instructiontree.begin();
		for(;it != instructiontree.end(); )
		{
			if( it->type==I_ATTRIBUTE_BEGIN && 
				it->blurPhase != phases.size())
			{
				instructiontree_t::iterator delit = it;
				it = it.scipchild();
				instructiontree.erase(delit);
			}
			else
			{
				it++;
			}
		}
	}
	inline void TCacheStorageBlur::render(IRender* render)
	{
		TCacheStorageBlur::render(render, root);
	}
	inline void TCacheStorageBlur::render(IRender* render, instructiontree_t::iterator rootit)
	{
		instructiontree_t::iterator it = rootit.begin();
		instructiontree_t::iterator start = it, end = it;
		for(;it != rootit.end(); it++)
		{
			// �� ������ I_ATTRIBUTE_BEGIN
			if(it->type==I_ATTRIBUTE_BEGIN)
			{
				if(start != end)
				{
					this->render(render, start, end);
					end = it;
				}
				render->Parameter("id", it->blurPhase); 
				render->PushAttributes();
				this->render(render, it);
				render->PopAttributes();
				start = it;
				start++;
				end = start;
			}
			else
				end = it;
		}
		if(start != end)
		{
			this->render(render, start, end);
		}
	}
	inline void TCacheStorageBlur::render(IRender* render, instructiontree_t::iterator start, instructiontree_t::iterator end)
	{
		instructiontree_t::iterator operation = end;
		render->MotionBegin();
		for(int i=0; i<(int)phases.size(); i++)
		{
			render->MotionPhaseBegin(phases[i]);

			instructiontree_t::iterator it = start;
			for(;it != end; it++)
			{	
				if( it->type==I_ATTRIBUTE || 
					it->type==I_PARAMETER || 
					it->type==I_CALL||
					it->type==I_PARAMETERFROMATTR
					)
				{
					if(it->blurPhase==phases[i])
					{
						if( it->type ==I_ATTRIBUTE)
							render->AttributeV(it->name.c_str(), it->param);
						else if( it->type ==I_PARAMETER)
							render->ParameterV(it->name.c_str(), it->param);
						else if( it->type ==I_CALL)
							render->Call(it->name.c_str());
						else if( it->type ==I_PARAMETERFROMATTR)
						{
							cls::PS attrname = it->param;
							render->ParameterFromAttribute(it->name.c_str(), attrname.data());
						}
					}
				}
				else
				{
					operation = it;
				}
			}
			render->MotionPhaseEnd();
		}
		render->MotionEnd();

		if(operation!=end)
		{
			enInstruction type = operation->type;
			switch(type)
			{
				case I_ATTRIBUTE_BEGIN:
				case I_ATTRIBUTE_END:
					printf("TCacheStorageBlur::render error!!!");
					break;
				case I_CALL:
				case I_PARAMETER:
				case I_ATTRIBUTE:
				case I_PARAMETERFROMATTR:
					printf("TCacheStorageBlur::render error!!!");
					break;
				case I_PUSHTRANSFORM:
					render->PushTransform();
					break;
				case I_POPTRANSFORM:
					render->PopTransform();
					break;
				case I_SETTRANSFORM:
					render->SetTransform();
					break;
				case I_APPENDTRANSFORM:
					render->AppendTransform();
					break;
				case I_SYSTEMCALL:
					{
						P<int> en(operation->param);
						render->SystemCall( (enSystemCall)en.data());
						break;
					}
				case I_RENDERCALL:
					{
						const char* proc = operation->name.c_str();
						render->RenderCall(proc);
						break;
					}
				case I_FILTER:
					{
						const char* proc = operation->name.c_str();
						render->Filter(proc);
						break;
					}
					break;
				case I_SHADER:
					{
						const char* shadertype = operation->name.c_str();
						render->Shader(shadertype);
					}
					break;
				case I_LIGHT:
					render->Light();
					break;
			}
		}
	}

	inline void TCacheStorageBlur::setInstruction(enInstruction type)
	{
		setInstruction(type, "", cls::Param());
	}
	inline void TCacheStorageBlur::setInstruction(enInstruction type, const char* name, cls::Param p)
	{
		// �������� ������������� ������ �� �������� � ����
		if( ignoreStack!=0)
		{
			if( type==I_ATTRIBUTE_BEGIN)
				ignoreStack++;
			if( type==I_ATTRIBUTE_END)
				ignoreStack--;
			return;
		}

		if( type==I_ATTRIBUTE_BEGIN)
		{
			instructiontree_t::iterator parent = cur.parent();
			if(bFirstPhase())
			{
				// id
				int id = parent->currentid;
				instructiontree_t::iterator newit = cur.insert( Instruction(1, I_ATTRIBUTE_BEGIN, "", P<int>(id)));
				newit = newit.end();

				parent->currentid++;
parent->Dump();
				cur = newit;
				cur.parent()->currentid=0;
				return;
			}
			else
			{
				// ����� id � ����� ������
				instructiontree_t::iterator it = cur.parent().begin();
				instructiontree_t::iterator end = cur.parent().end();
cur->Dump();
				int currentid = parent->currentid;
				for( ; it != end; it++ )
				{
					if( it->type == I_ATTRIBUTE_BEGIN)
					{
						P<int> id =	it->param;
						if( !id.empty())
							if(*id == currentid)
							{
								// �����!!!
								it->blurPhase += 1;
								parent->currentid++;
								cur = it.begin();
								cur.parent()->currentid = 0;
								return;
							}
					}
				}
				// �� ����� ������ ��������
				// ����� ������������ ��� ��������� ������ �� I_ATTRIBUTE_END
				ignoreStack = 1;
				return;
			}
			return;
		}
		if( type==I_ATTRIBUTE_END)
		{
			// ������ �� �����!!!
//			cur.insert( Instruction(currentPhase, type, name, p));
			cur = cur.parent();
			cur++;
			return;
		}
		if( type==I_PARAMETER)
		{
			// id ���������!
			if(strcmp(name, "id")==0)
			{
				P<int> pid = p;
				if(!p.empty())
				{
					cur.parent()->currentid = *pid;
					return;
				}
			}
		}
		cur.insert( Instruction(currentPhase(), type, name, p));
	}
	inline enInstruction TCacheStorageBlur::getInstruction()
	{
		return readit->type;
	}
	inline const char* TCacheStorageBlur::getInstructionName()
	{
		return readit->name.c_str();
	}
	inline cls::Param TCacheStorageBlur::getInstructionParam()
	{
		return readit->param;
	}
	inline bool TCacheStorageBlur::startInstruction()
	{
		readit = root;
		return readit.IsEnd();
	}
	inline bool TCacheStorageBlur::nextInstruction()
	{
		readit++;
		return readit.IsEnd();
	}
	inline bool TCacheStorageBlur::isEnd()
	{
		return readit.IsEnd();
	}

	inline void TCacheStorageBlur::Dump() const
	{
		instructiontree_t::const_branch_iterator it = instructiontree.begin();
		for(;it != instructiontree.end(); it++)
		{
			for(int i=0; i<it.level(); i++)
				printf(" ");
			it->Dump();
			printf("\n");
		}
	}


	/*/
	inline void TCacheStorageBlur::copy(const TCacheStorageBlur& arg)
	{
		this->instructiontree = arg.instructiontree;
		this->root = this->instructiontree.begin();
		this->cur = this->root.end();
	}
	/*/
}
