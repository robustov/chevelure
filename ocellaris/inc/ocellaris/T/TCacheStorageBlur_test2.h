#pragma once
#include "IRender.h"
#include <map>
#include <list>
#include <vector>
#include <string>
#include "Util/tree.h"
#include "ocellaris/T/TAttributeStack_forblur.h"

namespace cls
{
	//! @ingroup ocellaris_group 
	//! \brief ������������ � renderBaseCacheImpl ��� �������� ������ ����������
	//! 
	//! TCacheStorageBlur ��������� ������ ��� �������� �����
	struct TCacheStorageBlur
	{
		struct Instruction
		{
			int currentid;
			float blurPhase;		// ���� �����. ��� ������ I_ATTRIBUTE_BEGIN ��� �������� ����� ��� �������� � ������.
			enInstruction type;
			std::string name;
			Param param;
		public:
			Instruction(float blurPhase, enInstruction type){this->blurPhase=blurPhase;this->type=type;currentid=0;}
			Instruction(float blurPhase, enInstruction type, const char* name, const Param& param){this->blurPhase=blurPhase;this->type=type;this->name=name;this->param=param;currentid=0;}
			void Dump()
			{
				if(type==I_ATTRIBUTE_BEGIN)
					printf("%f %s %d - %d", blurPhase, Instruction2str(type), P<int>(param).data(), currentid);
				else
					printf("%f %s (\"%s\") - %d", blurPhase, Instruction2str(type), name.c_str(), currentid);
			};
		};
		typedef std::tree<Instruction> instructiontree_t;

		instructiontree_t::iterator cur;

		instructiontree_t::branch_iterator readit;

		instructiontree_t instructiontree;
		instructiontree_t::iterator root;

		int ignoreStack;		// �������� ������������� ������ �� �������� � ����
		std::vector<float> phases;

		float currentPhase(){if(phases.empty()) return nonBlurValue; return phases.back();};
		bool bFirstPhase(){return phases.size()==1;};		// ������ ����
	public:
		void clear();
//		void copy(const TCacheStorageBlur& arg);
		void setInstruction(enInstruction type);
		void setInstruction(enInstruction type, const char* name, cls::Param p);

		bool startInstruction();
		bool nextInstruction();
		bool isEnd();

		enInstruction getInstruction();
		const char* getInstructionName();
		cls::Param getInstructionParam();

		void setCurrentPhase(float phase);

		void parse();
		void render(IRender* render);

		void Dump() const;

	protected:
		void render(IRender* render, instructiontree_t::iterator root);
		void render(IRender* render, instructiontree_t::iterator start, instructiontree_t::iterator end);

	};
}
#include "TCacheStorageBlur.hpp"
