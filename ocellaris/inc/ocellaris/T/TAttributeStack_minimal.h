#pragma once

#include "IParameter.h"
#include <list>
#include <map>
#include <string>

namespace cls
{
	//! @ingroup ocellaris_group 
	//! \brief ���� ���������. ��� ����������� � ������ IRender
	//! 
	//! ���������� ���������� ����� ��������� (�� ������ blur ������)
	class TAttributeStack_minimal
	{
	public:
		inline void clear()
		{
			stack.clear();
			stack.push_back(stackItem());
		}
		inline void Parameter(
			const char *paramname,		//!< ��� ���������
			const Param& p			//!< ��������
			)
		{
			stackItem& si = stack.back();
			si.params[paramname] = p;
		}
		inline Param GetParameter(
			const char *name
			)
		{
			stackItem& si = stack.back();
			std::map<std::string, Param>::iterator it = si.params.find(name);
			if( it == si.params.end())
				return Param();
			return it->second;
		}
		inline bool RemoveParameter(
			const char *name
			)
		{
			return false;
		}
		//! ������ ����������
		inline void GetParameterList(
			std::vector< std::pair< std::string, Param> >& list)
		{
			stackItem& si = stack.back();
			list.resize(si.params.size());
			paramlist_t::iterator it = si.params.begin();
			int i=0;
			for(i=0; it != si.params.end(); it++, i++)
			{
				list[i].first = it->first;
				list[i].second = it->second;
			}
		}
		inline void Attribute(
			const char *name,		//!< ��� ���������
			const Param& t
			)
		{
			stackItem& si = stack.back();
			si.attrs[name] = t;
		}
		//! ������ ���������
		inline Param GetAttribute(
			const char *name
			)
		{
			std::list<stackItem>::reverse_iterator sit = stack.rbegin();
			for( ;sit != stack.rend(); )
			{
				stackItem& si = *sit;
				std::map<std::string, Param>::iterator it = si.attrs.find(name);
				if( it != si.attrs.end())
					return it->second;
				sit++;
			}
			return Param();
		}

		//! ������ ���������
		inline void GetAttributeList(
			std::vector< std::pair< std::string, Param> >& list)
		{
			stackItem& si = stack.back();
			list.resize(si.attrs.size());
			paramlist_t::iterator it = si.attrs.begin();
			int i=0;
			for(i=0; it != si.attrs.end(); it++, i++)
			{
				list[i].first = it->first;
				list[i].second = it->second;
			}
		}

		void PushAttributes(void) 
		{
			stack.push_back(stackItem());
		}
		void PopAttributes(IRender* pThis) 
		{
			if( stack.size()<=1)
				throw "renderPrmanImpl::PopAttributes ������ �����";

			// ��������� �������
			{
				stackItem& si = stack.back();
				fitrerlist_t::iterator it = si.filters.begin();
				for(;it != si.filters.end(); it++)
				{
					it->first->End(pThis, it->second);
				}
			}
			stack.pop_back();
		}
		inline void Filter(IFilter* filter, IRender* pThis, const Math::Matrix4f& curTransform)
		{
			stackItem& si = stack.back();
			int id;
			if( filter->Begin(pThis, curTransform, id))
			{
				si.filters.push_back( std::pair<IFilter*, int>(filter, id) );
			}
		}
		inline void GetFilterList(std::list< std::pair<IFilter*, int> >& filterlist)
		{
			filterlist.clear();
			// ��� �������
			std::list<stackItem>::reverse_iterator sit = stack.rbegin();
			for( ;sit != stack.rend(); )
			{
				stackItem& si = *sit;
				fitrerlist_t::iterator it = si.filters.begin();
				for(;it != si.filters.end(); it++)
				{
					filterlist.push_back(*it);
				}
				sit++;
			}
			filterlist;
		}

		int getMotionSamplesCount()
		{
			return 0;
		}
		float* getMotionTimes()
		{
			return 0;
		}
		void Dump()
		{
			std::list<stackItem>::iterator it = stack.begin();
			int level=0;
			for(level=0; it!=stack.end(); it++, level++)
			{
				int i=0;
				for(i=0; i<level; i++) printf("  "); printf("level %d:\n", i); 
				it->Dump(i);
			}
		}

	protected:
		//! ���� ����������
		typedef std::map<std::string, Param> paramlist_t;
		//! ������� ����� (���� �������� ������ ��� motionblur)
		struct stackItem
		{
			//! ������� ��������� (������������)
			paramlist_t params;
			//! ������� ��������
			paramlist_t attrs;
			//! ������� �������
			fitrerlist_t filters;

			void Dump(int level)
			{
				int i=0;
				{
					for(i=0; i<level; i++) printf("  "); printf("Parameters:\n"); 
					paramlist_t::iterator it = params.begin();
					for(;it != params.end(); it++)
					{
						for(i=0; i<level; i++) printf("  "); 
						printf("%s[%d]\n", it->first.c_str(), it->second.size());
					}
				}
				{
					for(i=0; i<level; i++) printf("  "); printf("Attributes:\n"); 
					paramlist_t::iterator it = attrs.begin();
					for(;it != attrs.end(); it++)
					{
						for(i=0; i<level; i++) printf("  "); 
						printf("%s[%d]\n", it->first.c_str(), it->second.size());
					}
				}
				{
					for(i=0; i<level; i++) printf("  "); 
					printf("Filters:\n"); 
					fitrerlist_t::iterator it = filters.begin();
					for(;it != filters.end(); it++)
					{
						for(i=0; i<level; i++) printf("  "); 
						printf("0x%x(%d)\n", *(int*)&it->first, it->second);
					}
				}
			}
		};
		//! ����
		std::list<stackItem> stack;
	};
}
