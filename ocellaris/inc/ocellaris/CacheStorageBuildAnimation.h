#pragma once
#include "ocellaris\CacheStorageMulty.h"

namespace cls
{
	struct CacheStorageBuildAnimation : public CacheStorageMulty
	{
		void Parse()
		{
			instructiontree_t::branch_iterator it = instructiontree.begin();
			for(;it != instructiontree.end(); it++)
			{
				multyinstruction_t& mi = *it;
				multyinstruction_t::iterator itx = mi.begin();
				if( itx == mi.end()) continue;

				Instruction startinst = itx->second;
				if( startinst.type==I_PARAMETER ||
					startinst.type==I_ATTRIBUTE)
				{
					// ������� �������� ������
					bool beq = true;
					for( itx++; itx != mi.end(); itx++)
					{
						bool res = startinst.param.equal(itx->second.param);
						if(res) continue;
						beq = false;
						break;
					}
					if( beq)
					{
						// ������� ��� ����� �������
						mi.erase(++mi.begin(), mi.end());
					}
					else
					{
						// ������� � ���� ������
						int s = 0;
						itx = mi.begin();
						for( ; itx != mi.end(); itx++)
						{
							s += itx->second.param.size();
						}
						cls::PA<int> offsets(PI_CONSTANT, mi.size());
						cls::Param fullparam(startinst.param->type, startinst.param->interp, s);
						itx = mi.begin();
						for( int c=0, i=0; itx != mi.end(); itx++, i++)
						{
							offsets[i] = c;
							char* dst = fullparam.data() + c*fullparam.stride();
							int s = itx->second.param.size();
							if( s==0) continue;
							const char* src = itx->second.param.data();
							memcpy(dst, src, s*fullparam.stride());
							c+=s;
						}
						mi.erase(mi.begin(), mi.end());
						mi[0] = Instruction(startinst.type, startinst.name.c_str(), fullparam);
						if(startinst.type==I_PARAMETER)
							mi[1] = Instruction(I_PARAMETER, "animation::type", PS("PARAMETER"));
						else
							mi[1] = Instruction(I_PARAMETER, "animation::type", PS("ATTRIBUTE"));

						mi[2] = Instruction(I_PARAMETER, "animation::parameter", PS(startinst.name.c_str()));
						mi[3] = Instruction(I_PARAMETER, "animation::offset", offsets);
						mi[4] = Instruction(I_PARAMETERFROMATTR, "animation::phase", PS("global::phase"));
						mi[5] = Instruction(I_PARAMETERFROMATTR, "animation::cycling", PS("global::cycling"));
						mi[6] = Instruction(I_CALL, "SimpleAnimation", Param());
					}
				}
			}
		}
	};
}
