#pragma once

#include "ocellaris/IGenerator.h"
#include "ocellaris/INode.h"
#include "ocellaris/IRender.h"
#include "ocellaris/IExportContext.h"

namespace cls
{
	//! @ingroup plugin_group
	//! \brief ��������� �����
	//! 
	//! 1. ��� �������� ��������� �������� � ����� ����� ��������� ������� �������
	//! 2. ��� ������ �����
	struct IBlurGenerator : public IGenerator
	{
		//! OnFrame
		//! ������ ������� ��������� �������� � ����� ����� ��������� ������� �������
		//! context->addExportTime()
		virtual bool OnAddFrame(
			cls::INode& pass,				//!< pass
			float frame,					//!< RenderFrame
			cls::INode& node,				//!< ������ 
			cls::IExportContext* context,	//!< context
			MObject& generatornode			//!< generator node �.� 0
			)=0;
		// ���������� ��� ������ �����
		virtual bool OnBuildBlur(
			cls::INode& pass,				//!< pass
			cls::INode& node,				//!< ������ 
			cls::IRender* render,			//!< render
			Math::Matrix4f& transform,		// ����������� ����� transform
			Math::Box3f& box,				// ����������� ���� ��� �������
			float frame,					//!< ����� ���� ��������
			cls::IExportContext* context,	//!< context
			MObject& generatornode			//!< generator node �.� 0
			)=0;

	private:
		virtual enGenType GetType(
			){return IGenerator::BLUR;};
	};
}