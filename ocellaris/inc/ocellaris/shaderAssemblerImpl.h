#pragma once
#include "IShaderAssembler.h"
#include "shading/IShaderParameter.h"
#include "ocellaris/IExportContext.h"

namespace cls
{
	//! @ingroup plugin_group
	//! \brief IShadingNodeGenerator
	//! 
	//! ����� ��� ������ �������� � Shading Network ��. IShadingNodeGenerator
	struct shaderAssemblerImpl : public IShaderAssembler
	{

		// ���������� �������� ���������
		virtual bool ExternParameter(
			const char* variable, 
			cls::Param defval, 
			cls::enParamType type,
			cls::enParamInterpolation interp
			);
		// ���������� �������� ���������
		virtual bool InputParameter(
			const char* mayaParameter, 
			const char* shaderVariable, 
			cls::enParamType type = cls::PT_UNKNOWN,
			cls::enParamInterpolation interp = cls::PI_VARYING
			);
		// ���������� �������� ���������
		virtual bool InputParameter(
			MPlug mayaParameter, 
			const char* shaderVariable, 
			cls::enParamType type,
			cls::enParamInterpolation interp
			);

		/*/
		// ���������� �������� ���������
		virtual bool InputManifold(
			const char* mayaParameter, 
			const char* Q, 
			const char* dQu, 
			const char* dQv 
			);
		/*/
		// ���������� ��������� ���������
		virtual bool OutputParameter(
			const char* shaderVariable,
			cls::enParamType type 
			);
		/*/
		// ���������� ��������� ���������
		virtual bool OutputManifold(
			const char* Q, 
			const char* dQu, 
			const char* dQv 
			);
		/*/

		// �������� #include ��������� � ������ �������
		virtual void Include(const char* format, ...);

		// �������� ������ � �������
		virtual void Add(const char* format);
		virtual void AddV(const char* format, ...);
	
	public:
		// ���������� �������
		std::string BuildGraph(
			cls::IShadingNodeGenerator::enShadingNodeType shadertype, 
			MObject node, 
			const char* name, 
			cls::IExportContext* context, 
			std::map<std::string, cls::Param >& externParams, 
			std::map<std::string, std::string>& externVarPlug
			);

	protected:

		// ��� ����������� �������
		bool Build(
			MObject node, 
			cls::enParamType wantedoutputtype, 
			cls::IShadingNodeGenerator* generator,
			cls::IExportContext* context, 
			std::map<std::string, cls::Param >& externParams,
			std::string& header, 
			std::string& procedure, 
			std::string& body
			);

	protected:
		// ��� �������� ��������� (���� ��������� ����������)
		// ��� SN ���� -> ��� output ����������
		typedef std::map<std::string, std::string> shaderProcedure_t;

		shaderProcedure_t* pprocedures;

		// current
		cls::IExportContext* context;
		MObject node;
		std::string name;
		std::string header;

		std::string childprocedures;
		std::string proceduredecl;
		std::string procedure;

		std::string childvarbody;
		std::string varbody;
		std::string callbody;

		// ��� � ���������� �������� ��������
		std::map<std::string, std::string> externvardeclarations;

		// ������� ������� ��������� ������ �� ���� (��� -> plug)
		std::map<std::string, std::string> externvarplug;

		std::map<std::string, cls::Param > inputs;
		std::string outputVariable;

	public:
		shaderAssemblerImpl(shaderProcedure_t* pprocedures=NULL);
	};
}

#include "inl/shaderAssemblerImpl.hpp"
