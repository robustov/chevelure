#pragma once

#include "ocellaris/IGenerator.h"
#include "ocellaris/INode.h"
#include "ocellaris/IExportContext.h"
#include "ocellaris/IRender.h"
//#include <maya/MObject.h>
class MObject;

namespace cls
{
	//! @ingroup plugin_group
	//! \brief ��������� ��� ������ �����������
	//! 
	//! ������ ��������� ��������� ����������� �������������� ������ (MObject) �� ������
	//! ���������� ��� ���������� ������ �������������� �������� ��.INode
	struct IProxyGenerator : public IGenerator
	{
		// ��� ����������
		virtual enGenType GetType(
			){return PROXY;}
		//! OnProcess
		virtual MObject OnProcess(
			cls::INode* node, 
			cls::IExportContext* context,
			MObject& generatornode
			)=0;//{return node->getObject();};
	};
}