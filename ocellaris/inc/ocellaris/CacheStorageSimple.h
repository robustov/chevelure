#pragma once
#include "IRender.h"
//#include <ri.h>
#include <map>
#include <list>
#include <vector>
#include <string>
#include "Util/tree.h"

namespace cls
{
	//! @ingroup ocellaris_group 
	//! \brief ������������ � renderBaseCacheImpl ��� �������� ������ ����������
	//! 
	//! CacheStorageSimple ������� �������� ������
	struct CacheStorageSimple
	{
		struct Instruction
		{
			enInstruction type;
			std::string name;
			Param param;
		public:
			Instruction();
			Instruction(const Instruction& arg);
			Instruction(enInstruction type){this->type=type;}
			Instruction(enInstruction type, const char* name, const Param& param){this->type=type;this->name=name;this->param=param;}
		};
		typedef std::tree<Instruction> instructiontree_t;

		instructiontree_t::iterator cur;

		instructiontree_t::branch_iterator readit;

		instructiontree_t instructiontree;
		instructiontree_t::iterator root;
	public:
		void clear();
		void copy(const CacheStorageSimple& arg);
		void setInstruction(enInstruction type);
		void setInstruction(enInstruction type, const char* name, cls::Param p);

		bool startInstruction();
		bool nextInstruction();
		bool isEnd();

		enInstruction getInstruction();
		const char* getInstructionName();
		cls::Param getInstructionParam();

		void Dump() const;
	};
}
#include "inl/CacheStorageSimple.hpp"
