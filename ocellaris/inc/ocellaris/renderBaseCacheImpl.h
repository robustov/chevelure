#pragma once
#include "IRender.h"

//#include <ri.h>
#include <map>
#include <list>
#include <vector>
#include <string>
#include "Util/DllProcedure.h"
#include "Util/tree.h"
#include "CacheStorageSimple.h"
#include "renderImpl.h"

namespace cls
{
	//! @ingroup impl_group 
	//! \brief ������� ���������� IRender ��� ����������� 
	//! 
	//! ��������� �� renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK>
	//! STORAGE - ����� ��� �������� ���� ������
	//! 
	//! STORAGE:
	//! CacheStorageSimple	- ������� �������� ������
	//! TCacheStorageBlur	- ��������� ������ ��� �������� �����
	//! CacheStorageBuildAnimation
	//! CacheStorageMulty
	//! 
	template< class STORAGE = CacheStorageSimple, class ATTRIBUTESTACK = TAttributeStack_minimal, class TRANSFORMSTACK = TTransformStack_pure, class BRANCHSTACK = TBranchStack_pure>
	struct renderBaseCacheImpl : public renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>
	{
		typedef renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK> base;

		renderBaseCacheImpl(
			);
		renderBaseCacheImpl(
			const renderBaseCacheImpl& arg
			);
		void copy(
			const renderBaseCacheImpl& arg
			);
		renderBaseCacheImpl& operator=(
			const renderBaseCacheImpl& arg);
		// clear
		void clear(
			);
	public:
		std::string rendertype;
		// ��� �������
		virtual const char* renderType(
			){return rendertype.c_str();};

		//! ��������� ���������� (������ ��� ��������� �������)
		virtual void comment(
			const char* text
			);

		//!@name Set a parameter of the next shader, primitive, camera, or output
		//@{ 

		//! ��������
		virtual void ParameterV(
			const char *name,		//!< ��� ���������
			const Param& t
			);

		//! ������ ���������
		virtual Param GetParameterV(
			const char *name
			);
		virtual bool RemoveParameter(
			const char *name
			);
		//! �������� �� ��������
		virtual void ParameterFromAttribute(
			const char *paramname,		//!< ��� ���������
			const char *attrname		//!< ��� �������� �� �������� ����� ������
			);
		//! �������� � �������
		virtual void ParameterToAttribute(
			const char *paramname,		//!< ��� ��������� �� �������� ����� ������
			const char *attrname		//!< ��� �������� 
			);
		//! ������� �� ��������
		virtual void AttributeFromAttribute(
			const char *dstname,	//!< ��� �������� ���������
			const char *srcname		//!< ��� �������� �� �������� ����� ������
			);
		//@}

		virtual void AttributeV(
			const char *name,		//!< ��� ���������
			const Param& t
			);

		///////////////////////////////////////////////////////////////
		/// ��������� �� �������� ��������
		virtual void IfAttribute(const char* name, const Param& t);
		virtual void EndIfAttribute();

		//@{ 
		/// PushAttributes/PopAttributes
		virtual void PushAttributes (void);
		virtual void PopAttributes (void);
		virtual void SaveAttributes (const char *name, const char *attrs=NULL) {}
		virtual void RestoreAttributes(const char *name, const char *attrs=NULL) {}
		//@}

		//@{ 
		/// Transformations
		virtual void PushTransform (void);
		virtual void PopTransform (void);
		virtual void SetTransform ();
		virtual void AppendTransform();
		virtual void PinTransform();
		//@}

		///////////////////////////////////////////////////////////////
		/// RenderAttributes - ��� ����������: ������ Begin/End Attributes
		/// PushRenderAttributes/PopRenderAttributes
		virtual void PushRenderAttributes(void);
		virtual void PopRenderAttributes(void);

		//@{ 
		/// Motion blur
		virtual void MotionBegin();
		virtual void MotionEnd();
		virtual void MotionPhaseBegin(float time);
		virtual void MotionPhaseEnd();
		//@}

		//@{ 
		/// Shaders
		virtual void Shader(const char* shadertype);
		virtual void Light();
		//@}

		///////////////////////////////////////////////////////////////
		/// Filters
		virtual void Filter(IFilter* filter);
		virtual void Filter(const char* filter);

		/// ����� ��������� ���������� ������� ������
		virtual void SystemCall(
			enSystemCall call
			);

		/// ���������� ���������
		virtual IRender* DeclareProcBegin(
			const char* name
			);
		virtual void DeclareProcEnd(
			);

		/// Procedural (���������������� �����)
		virtual void Call( 
			IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
			);
		virtual void Call( 
			const char*	procname			//!< ��� ��������� � DeclareProcBegin
			);


		///! Procedural (��������� �����)
		///! ��� ���������� ���������� ����� IProcedural::Render()
		///! ���� ����� ������ �������� � ����!!! (��� �����???)
		virtual void RenderCall( 
			IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
			);
		virtual void RenderCall( 
			const char*	procname			//!< ��� ��������� � DeclareProcBegin
			);

		/*/
		// ������
		virtual void Filter( 
			IFilter* filter				//!< ����� ������� IFilter::Begin(),IFilter::End(),IFilter::Command()
			);
		virtual void Filter( 
			const char*	filter			//!< ��� ��������� � DeclareProcBegin ��� dllname@procname
			);
		/*/

	public:
/*/
		struct Instruction
		{
			enInstruction type;
			std::string name;
			Param param;
		public:
			Instruction(enInstruction type){this->type=type;}
			Instruction(enInstruction type, const char* name, const Param& param){this->type=type;this->name=name;this->param=param;}
		};
		typedef std::tree<Instruction> instructiontree_t;
	protected:
		instructiontree_t::iterator cur;

		virtual void Render(
			instructiontree_t::iterator& root, 
			IRender* render);
/*/
		virtual void Render(IRender* render)const;

		mutable STORAGE storage;
	};
}
#include "inl/renderBaseCacheImpl.hpp"
