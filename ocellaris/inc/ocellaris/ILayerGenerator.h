#pragma once

namespace cls
{
	//! @ingroup plugin_group 
	//! \brief ��������� ����
	//! 
	//! ��� ���������� �������������� ���� ����������
	//! ���� �� ������������! 
	struct ILayerGenerator : public IGenerator
	{
		//! ��� ����
		virtual const char* name(
			)=0;
		//! ��� ����
		virtual const char* type(
			)=0;
		//! OnLayer
		//! ���������� ��� ��������� ����.
		//! ��� ���� ����� ����� �����
		virtual void OnLayer(
			INode* node,			//!< ������ �� ������� �������� ILayerGenerator
			IExportContext* context	//!< context
			)=0;
	private:
		virtual enGenType GetType(
			){return LAYER;};
	};
}