#pragma once

#include "ocellaris/IGenerator.h"
#include "ocellaris/INode.h"
#include "ocellaris/IRender.h"

namespace cls
{
	//! @ingroup plugin_group
	//! \brief ��������� �������
	//! 
	//! ��� ���������� ������ � �������
	struct IShaderGenerator : public IGenerator
	{
		//! OnShader
		//! render ����� ���� = NULL!
		virtual bool OnSurface(
			cls::INode& node,			//!< ������ ��� �������� ����������� �������
			cls::IRender* render,			//!< render
			cls::IExportContext* context,
			MObject& generatornode
			)=0;
		virtual bool OnDisplacement(
			cls::INode& node,			//!< ������ ��� �������� ����������� �������
			cls::IRender* render,			//!< render
			cls::IExportContext* context,
			MObject& generatornode
			)=0;
	private:
		virtual enGenType GetType(
			){return SHADER;};
	};
}