#pragma once
#include "IOcellaris.h"
#include "Math/Math.h"
#include "Math/Box.h"
#include <map>
#include <vector>
#include <set>
//#include "ocellaris/IExportContext.h"

class MObject;
class MDagPath;
class MTime;

namespace cls
{
	struct ILayerGenerator;
	struct INodeGenerator;
	struct IGeometryGenerator;
	struct IAttributeGenerator;
	struct IBlurGenerator;
	struct IShader;
	struct IRender;
	struct IExportContext;

	//! @ingroup plugin_group 
	//! \brief INode ��������� ��� ������� � maya �������
	//! 
	//! ������ � maya ������� �� ����� ��������
	//! ������������ ������� ocNodeTree
	//! ������ INode �������� ���������� ��������������� ���������� �������
	struct INode
	{
		virtual ~INode(){};
		virtual void Release()=0;

		// ������ �������. ������ ������ ���� ����
		virtual MObject getObject()const=0;
		// ���� � ����. ������������ ����� - ������ ���� ���������� ��������. 
		// ������ ��� ���� ����� ���������� Proxy ������
		virtual MDagPath getPath()const=0;
		// 
		virtual void setProxyObject(MObject obj)=0;
		// ��� 
		virtual const char* getName()const=0;
		// ���� �� ����� ���� � ������ ������ ������
		virtual bool isMasterPass(cls::INode*) const=0;

		// �������� ���������
		virtual void AddGenerator(
			cls::IGenerator* gen
			)=0;

		// ��������� ���� �� �������������� ���� 
		// ���������� �� NodePool::addnode
		virtual bool IsExported(
			cls::IExportContext* context
			)=0;

		// ��������� ���������
		virtual cls::IGenerator* GetGenerator(
			cls::IGenerator::enGenType type, 
			MObject& genobject)=0;

		// ������� ����������
		virtual void Dump(FILE* file)const{};

		/*/
		// ���������� � �������� �����
		virtual void AddFrame(
			cls::INode& pass,			// pass
			float frame,				// ����
			cls::IGenerator* passgen,	// ������
			cls::IBlurGenerator* defaultbg,	// ���� ��������� �� ���������
			cls::IExportContext* context	// �������� ��������
			)=0;
		/*/

		// Prepare Node �������� OnPrepare � �����������
		virtual bool PrepareNode(
			cls::IExportContext* context
			)=0;

		// Export:

		// �������
		virtual bool Export(
			cls::IRender* render,				// ������
			Math::Box3f& box,					// ���� � ��������� ����������� (�����������!!!)
			cls::IExportContext* context,		// ��������
			int whatRender = WR_ALL				// ����� �� enWhatRender
			)=0;
		// ����� ����� � ������
		virtual bool PushNode(
			cls::IRender* render,				// ������
			Math::Matrix4f& transform,		// ����������� ����� transform
			Math::Box3f& box,					// ����������� ���� ��� ������� (�������������)
			cls::IExportContext* context,	// �������� ��������
			int whatRender = WR_ALL				// ����� �� enWhatRender
			)=0;
/*/
		// ������� ����
		virtual bool BuildFrame(
			cls::INode& pass,				// pass
			cls::IRender* render,			// ������
			float frame,
			cls::IBlurGenerator* defaultbg,	// ���� ��������� �� ���������
			Math::Matrix4f& transform,		// ����������� ����� transform
			Math::Box3f& box,				// ����������� ���� ��� �������
			cls::IExportContext* context	// �������� ��������
			)=0;
/*/
		// ����� ���� � �� �����
		virtual bool PopNode(
			cls::IRender* render,				// ������
			cls::IExportContext* context,
			int whatRender = WR_ALL				// ����� �� enWhatRender
			)=0;


		// Render


		enum enWhatRender
		{
			WR_FILTERS					= 0x01,
			WR_ATTR_BEFORE_TRANSFORM	= 0x02,
			WR_TRANSFORM				= 0x04,
			WR_ATTR_BEFORE_NODE			= 0x08,
			WR_NODE						= 0x10,
			WR_ATTR_BEFORE_SHADER		= 0x20,
			WR_SHADER					= 0x40,
			WR_ATTR_BEFORE_GEOMETRY		= 0x80,
			WR_GEOMETRY					= 0x100,
			WR_CHILDS					= 0x4000,
			WR_COMMENTS					= 0x8000,
			WR_ALL						= 0xFFFF
		};

		// ��������!!!

		// ���������������� ������
		virtual void Render(
			cls::IRender* render, 
			Math::Box3f& box, 
			Math::Matrix4f trans, 
			cls::IExportContext* context,
			int whatRender = WR_ALL					// ����� �� enWhatRender
			){};

		// ���������� � ��������. ������� ��� ������� ����� ���������� INode::PrepareFrame
		// ������ ���� ����� � ����� ����� ��� ����� ���� ������� �������
		// ����������� ������ used_timestamps
		virtual void PrepareFrame(
			float t, 
			float shutterOpenFrame, 
			float shutterCloseFrame,
			std::map<float, std::set<float> >& used_timestamps	// ��������� ������ ���� ������� � �����. �� ������
			){};
		// ������� �� ���������� ���.
		virtual void Export(
			float time,
			cls::IExportContext* context
			){};

		// FinalRender ��������� ������ ����� �� ����� ���������� ������� Export
		virtual void FinalRender(
			cls::IRender* render, 
			Math::Box3f& box, 
			Math::Matrix4f trans, 
			cls::IExportContext* context
			){};
	};
}
