#pragma once

#include "ocellaris/IGenerator.h"
#include "ocellaris/INode.h"
#include "ocellaris/IRender.h"

namespace cls
{
	//! @ingroup plugin_group
	//! \brief ��������� ��������
	//! 
	//! ��� ���������� �������������� �������� � ���������
	struct IAttributeGenerator : public IGenerator
	{
		enum enCallType
		{
			CT_BEFORE_TRANSFORM	= 0x01, 
			CT_BEFORE_NODE		= 0x02, 
			CT_BEFORE_SHADER	= 0x04, 
			CT_BEFORE_GEOMETRY	= 0x08, 
		};
		// � ����� ������ ��������
		virtual int GetCallType(
			MObject& generatornode)=0;
		/// OnAttribute
		//! render ����� ���� = NULL!
		virtual bool OnAttribute(
			cls::INode& node,				//!< ������ ��� �������� ����������� �������
			cls::IRender* render,			//!< render
			cls::IExportContext* context, 
			MObject& generatornode
			)=0;
	private:
		virtual enGenType GetType(
			){return ATTRIBUTE;};
	};
}