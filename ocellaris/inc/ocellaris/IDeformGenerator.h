#pragma once

#include "ocellaris/IGenerator.h"
#include "ocellaris/INode.h"
#include "ocellaris/IRender.h"

namespace cls
{
	//! @ingroup plugin_group
	//! \brief ��������� ����������
	//!
	//! ������������� ���������, �������� ��������� � ������� �����
	//! ���������� ����� �������� ���������, �� ����� ������� � ���� ���������� ������ (Points, Mesh, Sphere � �.�.)
	//! 
	struct IDeformGenerator : public IGenerator
	{
		enum enDeformCallType
		{
			CT_BEFORE_TRANSFORM	= 0x01, 
//			CT_BEFORE_NODE		= 0x02, 
			CT_BEFORE_SHADER	= 0x04, 
			CT_BEFORE_GEOMETRY	= 0x08, 
		};
		// � ����� ������ ��������
		virtual int GetDeformCallType(
			MObject& generatornode)=0;

		//! ��������� ����� ���������� �������� ���� ������� ����� �������������� �������� ���������
		virtual void OnSetupNode(
			cls::INode& node, 
			IExportContext* context,
			MObject& generatornode			//!< generator node �.� 0
			){};
		//! ���������� ����� ��������� ��������� �������� SystemCall
		//! ��������� ��������� �������� ��������� ��� �������� Call �����
		//! render ����� ���� = NULL
		virtual void OnGeometry(
			cls::INode& inode,				//!< ������ 
			cls::IRender* render,				//!< render
			cls::enSystemCall call,				//!< ��� ������ ��������� ��������: I_POINTS, I_CURVES, I_MESH ...
			cls::IExportContext* context, 
			MObject& generatornode			//!< generator node �.� 0
			){}
		// ���������� � AppendTransform � SetTransform
		virtual void OnTransform(
			cls::INode& inode,				//!< ������ 
			cls::IRender* render,			//!< render
			bool bAppend,					//!< 
			cls::IExportContext* context, 
			MObject& generatornode			//!< generator node �.� 0
			){}
		//
		virtual void OnShader(
			cls::INode& inode,				//!< ������ 
			cls::IRender* render,			//!< render
			const char* shadertype,			//!< 
			cls::IExportContext* context, 
			MObject& generatornode			//!< generator node �.� 0
			){}

		private:
			virtual enGenType GetType(
				){return DEFORM;};
	};
}