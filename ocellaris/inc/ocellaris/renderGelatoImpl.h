#pragma once
#include "IRender.h"
#include "renderImpl.h"
#include <gelatoapi.h>
#include <map>
#include <list>
#include <vector>
#include <string>
#include "ocellaris/renderCacheImpl.h"
#include "T/TAttributeStack_forblur.h"
#include "T/TTransformStack_minimal.h"

namespace cls
{
	//! @ingroup impl_group 
	//! \brief ���������� IRender ��� Gelato ��� ������������� �� ����� ���������� 
	//! 
	//! ��� ���������� � Gelato
	template <class TRANSFORMSTACK = TTransformStack_pure>
	struct renderGelatoImpl : public renderImpl<TAttributeStack_forblur, TRANSFORMSTACK>
	{
		typedef renderImpl<TAttributeStack_forblur, TRANSFORMSTACK> BASE;

		renderGelatoImpl(
			GelatoAPI* prman=NULL
			);
		void setGelato( 
			GelatoAPI* prman
			);

		//@{ 
		/// PushAttributes/PopAttributes
		virtual void PushAttributes (void);
		virtual void PopAttributes (void);
		//@}

		//@{ 
		/// Transformations
		virtual void PushTransform (void);
		virtual void PopTransform (void);
		virtual void SetTransform ();
		virtual void AppendTransform ();
		//@}

		//@{ 
		/// Motion blur
		virtual void MotionBegin();
		virtual void MotionEnd();
		virtual void MotionPhaseBegin(float time);
		virtual void MotionPhaseEnd();
		virtual bool SetCurrentMotionPhase(float time);
		//@}

		//@{ 
		/// Shaders
		virtual void Shader(const char* shadertype);
		virtual void Light ();
		//@}

		/// ����� ��������� ���������� ������� ������
		virtual void SystemCall(
			enSystemCall call
			);

		/// Procedural (���������������� �����)
		virtual void Call( 
			IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
			);
		virtual void Call( 
			const char*	procname		//!< ����� ������� IProcedural::Render()
			);


		/// Procedural (��������� �����)
		/// ��� ���������� ���������� ����� IProcedural::Render()
		/// ���� ����� ������ �������� � ����!!! (��� �����???)
		virtual void RenderCall( 
			IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
			);
		virtual void RenderCall( 
			const char*	procname		//!< ����� ������� IProcedural::Render()
			);
	protected:

		void SetupParametersToGelato(
			);

		typedef std::vector< std::pair< std::string, Param> > paramlist_t;
		//! ��������� ����������� ����������
		struct delayRenderStruct
		{
			renderGelatoImpl<TRANSFORMSTACK>* prman;
			IProcedural* pDelayRender;
			paramlist_t params;
			paramlist_t attributes;
		};

		//! subrender, for blur
		IRender* subrender;
		//! bBlurFinal - ��������� ����������� ���� ������
		bool bBlurFinal;
		//! blur parser
//		renderBlurParser rci;

	protected:
		void singleSystemCall(enSystemCall call);

		/// Geometry
		/// 0-D prims - point clouds
		void renderPoints (
			);
		/// 1-D prims - lines, curves, hair
		void renderCurves (
			const char *interp, 
			const char *wrap,		//!< wrap
			PA<int>& nverts			//!< int[ncurves] ����� ��������� � ������
			);
		void renderCurves (int ncurves, int nvertspercurve, int order,
							const float *knot, float vmin, float vmax) {}
		/// 2-D prims - rectangular patches (NURBS, bicubics, bilinears), and
		/// indexed face meshes (polys, polyhedra, subdivs)
		void renderPatch();
		/// TrimCurve
		void renderTrimCurve();
		/// Mesh 
		// ������������ ���������:
		// uniform string interpolation,	//!< ��� ������������
		// int loops[]						//!< loops per polygon
		// int nverts[]						//!< ����� ��������� � �������� (size = polygoncount)
		// int verts[]						//!< ������� ��������� ��������� (size = facevertcount)

		void renderMesh(
			const char *interp,		//!< ��� ������������
			PA<int>& loops,			//!< loops per polygon
			PA<int>& nverts,		//!< ����� ��������� � �������� (size = polygoncount)
			PA<int>& verts			//!< ������� ��������� ��������� (size = facevertcount)
			);
		/// Sphere
		void renderSphere(
			float radius, float zmin, float zmax,
			float thetamax=360);
		/// Blobby
		void renderBlobby(
			PA<Math::Matrix4f>& ellipsoids);
		void singleShader(
			const char *shadertype);

		/// setup camera
		bool Camera(
			);
		/// setup Output
		bool Output(
			);

	protected:
		GelatoAPI* prman;
		// ��������� �����
		float blur_scenetime;
		float blur_shutterOpen;
		float blur_shutterClose;
	public:
		void setBlurParams(float blur_scenetime, float blur_shutterOpen, float blur_shutterClose);
	};
}
#include "inl/renderGelatoImpl.hpp"
