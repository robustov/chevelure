#pragma once

#include "IRender.h"

#include <map>
#include <list>
#include <string>
#include "Util/DllProcedure.h"
#include "T/TTransformStack_pure.h"
#include "T/TAttributeStack_minimal.h"
#include "T/TBranchStack_pure.h"

namespace cls
{
	//! @ingroup ocellaris_group 
	//! \brief ������� ���������� IRender ��� ������ � ���������� � ������������
	//! 
	//! ������� ���������� IRender 
	//! ���������� ��������� ��������:
	//! 
	//! ���������� ����������� ����������:
	//! ATTRIBUTESTACK - ����� ��� ����������� ����� ���������
	//! TRANSFORMSTACK - ����� ��� ����������� ����� �����������
	//! BRANCHSTACK    - ����� ��� ����������� ����� ���������
	//! 
	//! ��������: ����� isNormalExe - ������ ���������� � ������ ���������� ���� ������� IRender � ��������
	//!	
	//! ATTRIBUTESTACK:
	//! TAttributeStack_pure - ������ ���������� ����� ��������� - ������ �� ������
	//! TAttributeStack_minimal - ���������� ���������� ����� ��������� (�� ������ blur ������)
	//! TAttributeStack_forblur - ���������� ����� ��������� ��� �������� �������� ��������� ��� ������ ��� �����
	//!								����������� �������� resetMotion addMotionParamPhase � setMotionParamPhase
	//!
	//! TRANSFORMSTACK:
	//! TTransformStack_pure - ������ ���������� ����� ����������� - �� ������ ����������
	//! TTransformStack_minimal - ���������� ���������� ����� �����������. ������ ���� ���� �����������
	//! TTransformStack_forblur - ���������� ����� ����������� ��� �������� �������� ����������� �������� ��� ������ ���� �����
	//!								����������� ������� setMotionParamPhase
	//! BRANCHSTACK:
	//! TBranchStack_pure	 - ���������� ���������
	//! TBranchStack_minimal - ������ ���� ��������� ���� ���������� isNormalExe()
	//! 
	template <class ATTRIBUTESTACK = TAttributeStack_minimal, class TRANSFORMSTACK = TTransformStack_pure, class BRANCHSTACK = TBranchStack_pure>
	struct renderImpl : public IRender
	{
		renderImpl();
		// ������� �����
		inline void clear();
		inline void clear(const Math::Matrix4f& startuptransform);

		// ���������� �� ������� �������
		void init_transform(IRender* render);
		void init_transform(const Math::Matrix4f& worldtransform);
		void init_attributes(IRender* render);

		// ���������� ������, ��������� ��������� � ��������
		/*/
		void startFrom( const renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>& src)
		{
			transformStack.copyFrom( src.transformStack);
			attributeStack.copyFrom( src.attributeStack);
		}
		/*/
		// ����������� ������� ���������
		virtual bool RegisterProcedural(
			const char* procName, 
			IProcedural* procedural
			);

		/////////////////////////////////////////////////////
		//! �����������

		//! ��������� ���������� (������ ��� ��������� �������)
		virtual void comment(
			const char* text
			);

		/////////////////////////////////////////////////////
		//! ���������

		//! ��������
		virtual void ParameterV(
			const char *name,		//!< ��� ���������
			const Param& t
			);
		//! �������� �� ��������
		virtual void ParameterFromAttribute(
			const char *paramname,		//!< ��� ���������
			const char *attrname		//!< ��� �������� �� �������� ����� ������
			);
		//! �������� � �������
		virtual void ParameterToAttribute(
			const char *paramname,		//!< ��� ��������� �� �������� ����� ������
			const char *attrname		//!< ��� �������� 
			);
		//! ������� �� ��������
		virtual void AttributeFromAttribute(
			const char *dstname,	//!< ��� �������� ���������
			const char *srcname		//!< ��� �������� �� �������� ����� ������
			);
		//! �������� ���������
		virtual Param GetParameterV(
			const char *name
			);
		//! ������� ��������
		virtual bool RemoveParameter(
			const char *name
			);
		//! ������ ����������
		virtual void GetParameterList(
			std::vector< std::pair< std::string, Param> >& list);




		/////////////////////////////////////////////////////
		//! ���������
		// ���� ��� �������� ���������� � global:: - �� ��������� ����������!

		//! �������
		virtual void AttributeV(
			const char *name,		//!< ��� ���������
			const Param& t
			);
		//! �������
		virtual void GlobalAttribute(
			const char *name,		//!< ��� ���������
			const Param& t
			);
		//! ������ ��������
		virtual Param GetAttributeV(
			const char *name
			);
		//! ������ ���������
		virtual void GetAttributeList(
			std::vector< std::pair< std::string, Param> >& list
			);

		///////////////////////////////////////////////////////////////
		/// ��������� �� �������� ��������
		virtual void IfAttribute(const char* name, const Param& t);
		virtual void EndIfAttribute();

		///////////////////////////////////////////////////////////////
		/// PushAttributes/PopAttributes
		virtual void PushAttributes(void);
		virtual void PopAttributes(void);

		///////////////////////////////////////////////////////////////
		/// Transformations
		virtual void PushTransform(void);
		virtual void PopTransform(void);
		//! ��� ����� �������� - ������� ������� �� ��������� "transform"
		void SetTransform();
		//! � ������ �������� - ������� ������� �� ��������� "transform"
		virtual void AppendTransform();
		//! PinTransform
		virtual void PinTransform();

		///////////////////////////////////////////////////////////////
		/// empty implementation
		virtual void PushRenderAttributes(void);
		virtual void PopRenderAttributes(void);

		///////////////////////////////////////////////////////////////
		/// Filters
		///
		virtual void Filter(IFilter* filter);
		virtual void Filter(const char* filter);

		///////////////////////////////////////////////////////////////
		/// Motion blur 
		/// time - �������� � �������� �� ������� ���. �����
		virtual void MotionBegin(){};
		virtual void MotionEnd(){};
		virtual void MotionPhaseBegin(float time){};
		virtual void MotionPhaseEnd(){};
		// ������������ ������� ������ ����� (����� �� ���������!!!)
		virtual bool SetCurrentMotionPhase(float time){return false;};


		///////////////////////////////////////////////////////////////
		/// Shaders
		virtual void Shader(const char* shadertype){}
		virtual void Light(){}

		///////////////////////////////////////////////////////////////
		/// ����� ��������� ���������� ������� ������
		virtual void SystemCall(enSystemCall call){}
		/// ����� ��������� ���������� ������� ������
		void SystemCall(const char* callname);

		/////////////////////////////////////////////////////////////////
		/// ���������

		/// ���������� ���������
		virtual IRender* DeclareProcBegin(
			const char* name
			){return this;};
		virtual void DeclareProcEnd(
			){};

		/// Procedural (���������������� �����)
		/// ���� ����� ������ �������� � ����!!!
		virtual void Call( 
			IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
			){};
		/// Procedural (���������������� �����)
		/// ���� ����� ����� �������� � ����
		virtual void Call( 
			const char*	procname			//!< ��� ��������� � DeclareProcBegin ��� dllname@procname
			){};

		/// Procedural (��������� �����)
		/// ��� ���������� ���������� ����� IProcedural::Render()
		/// ���� ����� ������ �������� � ����!!! (��� �����???)
		/// ��������: Box, id
		virtual void RenderCall( 
			IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
			){};
		/// Procedural (��������� �����)
		/// ���� ����� ����� �������� � ����
		virtual void RenderCall( 
			const char*	procname			//!< ��� ��������� � DeclareProcBegin ��� dllname@procname
			){};

		/// ���������� �����
		/// ������ �����
		virtual void Dump( 
			);

	// ��� ����������
	protected:
		ATTRIBUTESTACK attributeStack;

	// ���������
	protected:
		struct ProceduralItem
		{
			Util::DllProcedure dll;
			IProcedural* proc;
		};
		typedef std::map<std::string, ProceduralItem> proclist_t;
		//! ������ ��������
		proclist_t proclist;

		//! ����� ���������
		IProcedural* findProcedure(const char* procname);

	// �������
	protected:
		struct FilterItem
		{
			Util::DllProcedure dll;
			IFilter* proc;
		};
		typedef std::map<std::string, FilterItem> filteritemlist_t;
		//! ������ ��������
		filteritemlist_t filteritemlist;

		//! ����� ���������
		IFilter* findFilter(const char* filtername);

		//! ��������� �������
		bool CallFilters(enSystemCall call);
		bool CallFilters_onshader(const char* type);
	// ����������
	protected:
		// ���������� ����� �����������
		TRANSFORMSTACK transformStack;
	// ���������
	private:
		// ���� ���������
		BRANCHSTACK branchstack;
	protected:
		// ��� �������� ��������� ���������� - ������ ���������� � ������ ���������� IRender � ��������
		bool isNormalExe()const{return branchstack.isNormalExe();};
	};
}
#include "inl/renderImpl.hpp"

