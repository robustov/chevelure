#pragma once

#define OCELLARIS

namespace cls
{
	struct IProcedural;

	//! @ingroup ocellaris_group 
	//! \brief ���� �������� ���������� ������� ������
	//! 
	//! ���� �������� ���������� ������� ������
	enum enSystemCall
	{
		SC_POINTS = 0,
		SC_CURVES = 1,
		SC_PATCH = 2,
//		SC_TRIMCURVE = 3,
		SC_MESH = 4,
		SC_SPHERE = 5,
		SC_BLOBBY = 6,
		SC_TEXT = 7,
		SC_UNKNOWN = -1,
	};
	inline const char* SystemCall2str(enSystemCall type);
	inline enSystemCall str2SystemCall(const char* str);

	//! @ingroup ocellaris_group 
	//! \brief ���� ������ ���������
	//! 
	//! ������������ �� ������ ������ ������� ������� ����
	enum enInstruction
	{
		I_UNKNOWN				= 0x00, 

		I_ATTRIBUTE_BEGIN		= 0x01, 
		I_ATTRIBUTE_END			= 0x02, 
		I_PARAMETER				= 0x03, 
		I_ATTRIBUTE				= 0x04, 
		I_PARAMETERFROMATTR		= 0x05, 
		I_ATTRIBUTEFROMATTR		= 0x06, 
		I_PARAMETERTOATTR		= 0x07, 

		I_PUSHTRANSFORM			= 0X10, 
		I_POPTRANSFORM			= 0X11, 
		I_SETTRANSFORM			= 0X12, 
		I_APPENDTRANSFORM		= 0X14, 
		I_PINTRANSFORM			= 0X15, 

		I_SYSTEMCALL			= 0x20, 
		I_PUSHRENDERATTRIBUTE	= 0x21, 
		I_POPRENDERATTRIBUTE	= 0x22, 

		I_CALL					= 0x30, 
		I_RENDERCALL			= 0x31,
		I_FILTER				= 0x32,

		I_MOTION				= 0x40, 
		I_MOTIONPHASE			= 0x41, 
		I_MOTIONBEGIN			= 0x42, 	
		I_MOTIONEND				= 0x43, 	
		I_MOTIONPHASEBEGIN		= 0x44, 	
		I_MOTIONPHASEEND		= 0x45, 	

		I_SHADER				= 0x50, 
		I_LIGHT					= 0x51, 

		I_IFATTRIBUTE			= 0x60, 
		I_ENDIFATTRIBUTE		= 0x61, 

		I_DECLAREPROCBEGIN		= 0x70, 
		I_DECLAREPROCEND		= 0x71, 

		//========================
		I_COMMENT				= 0xEE,
		I_ENDFILE				= 0xFF
	};
	inline const char* Instruction2str(enInstruction type);
	inline enInstruction str2Instruction(const char* str);
}


/** @defgroup plugin_group ����������, ������ � ������ ��� ��������
\brief �� ������ ������ ������� �������� ���������� � OcellarisExport.cpp(OcellarisExport.dll)

��� ������� ������� ����������� ��������� ��������:

1. ��� ������� ������ ������ �������� (���������� ������ cls::INodeFilter) � ���� ���� ���� �� ������� 
cls::INodeFilter::IsRenderNode() ������ false ������ ������ �� ��������������.
����������� ����������: StandartNodeFilter

2. ��������� ��������� ���������� (cls::ITransformGenerator) �������������� � ��������
���� ����� ���� � ���� ���������� ����� cls::ITransformGenerator::OnTransformStart().
����������� ����������: TransformGenerator

3. ��������� ������ ����������� ������� (cls::INodeGenerator)
� ������� ���������� ����� cls::INodeGenerator::OnNodeStart().

4-6. ����� ����������� ���������, ������� � ����������.
���� ��� ������� ������ ��������� ��������� (cls::IGeometryGenerator), 
�� ��� ����� ������� ����������� ��������� ���������:
- ��������� ��������� ������� (cls::IShaderGenerator) �������������� � �������� 
� ���� �� ������ ���������� ����� cls::IShaderGenerator::OnShader();
- ��������� ������ ����������� ��������� (cls::IAttributeGenerator) � ��� ������� 
���������� cls::IAttributeGenerator::OnAttribute();
- ���������� ����� cls::IGeometryGenerator::OnGeometry();
����������� ���������� ���������� ���������: MeshGenerator, InstancerGenerator, NuCurveGenerator.
����������� ���������� ���������� �������: SlimShaderGenerator.
����������� ���������� ���������� ���������: NameAttributeGenerator, ParticleAttributeGenerator.

7. ������������ ����� ��������� ������ ��������

8. � ���� ����������� ������� (cls::INodeGenerator) ���������� ����� cls::INodeGenerator::OnNodeEnd().

9. � ���������� ���������� ���������� cls::ITransformGenerator::OnTransformEnd()

����������: 

1. � ���� �� ���� ���������� � ���������� cls::INodeGenerator � ����� ����
���� ������ ������ ��� � ����.

2. �������� ��������� ��� �� ����� IDeformationGenerator ������������ � 
������ ������ draw ������ ���������� IRender (cls::IRender::Points(), cls::IRender::Mesh() � �.�. )
����� ���� ������ ��� �������� ��� ���� ��������� ������������ �� ���������.

3. �������������� OcellarisExport ����� ������������ ������ ���������� cls::IRender ������� �������� 
���������� ������, � �� ������� ����� � ����.

4. ���� ����� ��������� � ��������� ���������� � ���������� ���������, �������� ������ �����������.

��������:
 - �������� � ������������ ���������, ������ ��� ������ � render ��������
 @{
 */

/** @page plugin_group_page1 ���������� ����������� �������������
 *  ���������� ����������� ������������� ��� ��������� ��������
 */

/** @} */ // end of plugin_group




/** @defgroup ocellaris_group ���������� ����������� � ocellaris
 *  ���������� ����������� � ocellaris
 *  @{
 */

/** @page ocellaris_group_page1 ������ ����������� � ocellaris
 *  ���������� ����������� � ocellaris
 */

/** @} */ // end of ocellaris_group




/** @defgroup implement_group ����������� ���������� ocellaris
����������� ���������� ocellaris
@{
 */
/** @page implement_group_page1 ����������� ���������� ocellaris
����������� ���������� ocellaris
*/
/** @} */ // end of ocellaris_group




/** @defgroup core_group ���������� ���������� ����������� ocellaris
���������� ���������� ����������� ocellaris
@{
 */
/** @page core_group_page1 ���������� ���������� ����������� ocellaris

*/
/** @} */ // end of core_group


//! cls - Ocellaris namespace
namespace cls
{
}

#include "inl/IOcellaris.hpp"
