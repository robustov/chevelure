#pragma once

namespace cls
{
template <class T>
inline void serializeOcsEnum(const char* name, cls::IRender* render, bool bSave, T& value)
{
	if( bSave) 
		render->Parameter( name, (int)value); 
	else 
	{
		int res;
		render->GetParameter( name, res);
		value = (T)res;
	}
}
}

// 
// ������������ ����� IRender
// ������ ���� �������������: 
// IRender* render, bool bSave, std::string name
// 

// SERIALIZEOCS
#define SERIALIZEOCS(ARG) \
	if( bSave) \
		render->Parameter( (name+"::"#ARG).c_str(),this->##ARG); \
	else \
		render->GetParameter( (name+"::"#ARG).c_str(),this->##ARG);

// SERIALIZEOCSPROC
#define SERIALIZEOCSPROC(ARG) \
	this->##ARG.serializeOcs( (name+"::"#ARG).c_str(), render, bSave);

// SERIALIZEOCSPROCARRAY
#define SERIALIZEOCSPROCARRAY(ARG) \
{\
	if(bSave)\
	{\
		render->Parameter( (name+"::"#ARG).c_str(),(int)this->##ARG.size()); \
	}\
	else\
	{\
		int count=0;\
		render->GetParameter( (name+"::"#ARG).c_str(), count);\
		this->##ARG.resize(count);\
	}\
	for(int i=0; i<(int)this->##ARG.size(); i++)\
	{\
		char buf[256];\
		sprintf(buf, "%s::"#ARG"%d", _name, i);\
		this->##ARG[i].serializeOcs(buf, render, bSave);\
	}\
}

// SERIALIZEOCSENUM
#define SERIALIZEOCSENUM(ARG) \
	cls::serializeOcsEnum((name+"::"#ARG).c_str(), render, bSave, this->##ARG);

// SERIALIZEOCS2
#define SERIALIZEOCS2(ARG1, ARG2) \
	if( bSave) \
	{ \
		render->Parameter( (name+"::"#ARG1).c_str(),this->##ARG1); \
		render->Parameter( (name+"::"#ARG2).c_str(),this->##ARG2); \
	} \
	else \
	{ \
		render->GetParameter( (name+"::"#ARG1).c_str(),this->##ARG1); \
		render->GetParameter( (name+"::"#ARG2).c_str(),this->##ARG2); \
	};

// SERIALIZEOCSTEX
#define SERIALIZEOCSTEX(ARG1) \
	if( bSave) \
	{ \
		cls::PA<int> res(cls::PI_CONSTANT, 2); \
		res[0] = this->##ARG1.sizeX(); \
		res[1] = this->##ARG1.sizeX(); \
		cls::PA<float> param(cls::PI_CONSTANT, this->##ARG1.data(), this->##ARG1.fullsize()); \
		render->Parameter( (name+"::"#ARG1"res").c_str(), res);  \
		render->Parameter( (name+"::"#ARG1).c_str(), param);  \
	} \
	else \
	{ \
		cls::PA<int> res = render->GetParameter( (name+"::"#ARG1"res").c_str()); \
		if(res.size()==2) \
		{  \
			cls::PA<float> param = render->GetParameter( (name+"::"#ARG1).c_str()); \
			if( param.size()==res[0]*res[1]) \
			{ \
				this->##ARG1.init(res[0], res[1]); \
				memcpy( this->##ARG1.data(), param.data(), this->##ARG1.fullsize()*sizeof(float)); \
			} \
		} \
	}

