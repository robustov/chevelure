#pragma once

#include "ocellaris/ChiefGenerator.h"
#include "../ocellarisExport/Node.h"

struct MayaExportContextImpl : public cls::IExportContext
{
	std::string IGNOREOVERRIDE;

	virtual const char* getEnvironment(
		const char* env
		)const
	{
		MayaExportContextImpl* pThis = (MayaExportContextImpl*)this;
		if( _strcmpi(env, "WORKSPACEDIR")==0)
			return projectDir.c_str();
		if( _strcmpi(env, "SHADERDIR")==0)
			return pThis->getShaderDirectory();
		if( _strcmpi(env, "OUTPUTDIR")==0)
			return pThis->getOutputDirectory();
		if( _strcmpi(env, "OCSDIR")==0)
			return pThis->getOcsDirectory();
		if( _strcmpi(env, "IGNOREOVERRIDE")==0)
			return IGNOREOVERRIDE.c_str();
		return "";
	}
	// ����������
	virtual void setEnvironment(
		const char* env, 
		const char* value
		)
	{
		if( _strcmpi(env, "WORKSPACEDIR")==0)
			projectDir = value;
		if( _strcmpi(env, "SHADERDIR")==0)
			shaderDir = value;
		if( _strcmpi(env, "OUTPUTDIR")==0)
			outputDir = value;
		if( _strcmpi(env, "OCSDIR")==0)
			ocsDir = value;
		if( _strcmpi(env, "IGNOREOVERRIDE")==0)
			IGNOREOVERRIDE = value;
	}
	virtual ChiefGenerator* getGenerators(
		)
	{
		return &chief;
	}
	virtual void addRenderFramePass(
		cls::INode& node,				//!< ������ pass
		float frametime					//!< � ������ ����� ���������
		)
	{
	}
	virtual void addExportTimeNode(
		cls::INode& node,				//!< ������ 
		float frametime,				//!< � ������ ����� ���������
		float exportTime				//!< ������ ����� ������ ���� ���������� �������
		)
	{
	}

	enum enTaskType
	{
		PER_JOB		= 1,
		PER_FRAME	= 2,
	};


	virtual void OnNextFrame()
	{
//		cls::IExportContext::OnNextFrame();
		crop = -1;
		outputfilename = nocropoutputfilename= "";
	}
	//! ������� �����
	virtual int frame(
		)
	{
		return _frame;
	}
	//! ������� pass
	virtual const char* pass(
		)
	{
		return currentPass.c_str();
	}

	// ����������
	virtual const char* getOcsDirectory()
	{
		if( !ocsDir.empty())
			return ocsDir.c_str();
		ocsDir = projectDir + "/ocs/";
		MString ribdir;
		MGlobal::executeCommand("workspace -rte rib", ribdir);
		if( ribdir.length()!=0)
			ocsDir += ribdir.asChar();
		else
			ocsDir += "rib";
		ocsDir += "/";
		
		return ocsDir.c_str();
	}
	virtual const char* getAlfredTaskName()
	{
		if( alfredTaskName.empty())
		{
			alfredTaskName = scenename;
			alfredTaskName += "(";
			alfredTaskName += taskname;
			alfredTaskName += ")";
		}
		return alfredTaskName.c_str();
	}
	virtual const char* getOutputDirectory()
	{
		if( outputDir.empty())
		{
			outputDir = projectDir +"/";
			if( scenename.size()!=0)
				outputDir += scenename;
			else
				outputDir += "rmanpix";
			outputDir += "/";
			outputDir += taskname;
			outputDir += "/";
		}
		return outputDir.c_str();
	}
	const char* getShaderDirectory()
	{
		return shaderDir.c_str();
	}

	std::map<int, cls::INode* > nodeById;

	// ������ ����� ���������� �����
	virtual void getGlobalLights(
		std::vector<cls::INode* >& lights
		)
	{
	}

	// ������� ���� �������� ��� �������
	virtual cls::INode* getNodeForObject(
		MObject& obj
		)
	{
		int id = *(int*)(&obj);
		// �������� ����� � ������
		std::map<int, cls::INode* >::iterator it = nodeById.find(id);
		if( it!=nodeById.end())
			return it->second;

		//! ��������� INode ��� ������ �������
		cls::INode* inode = ocNodeTree_Single( 
			obj,					//<! ������
			chief,			//<! �������� ����������� �� ���������
			this			//<! �������� ��������
			);

		nodeById[id] = inode;
		return inode;
	}

	// ������� ��������� ��� �������� ���� ��������
	virtual cls::IShadingNodeGenerator* getShadingNodeGenerator(
		MObject& obj
		)
	{
		ChiefGenerator::shadingnodegenlist_t gens;
		Node node;
		node.set(obj);
		chief.getShadingNode( node, gens);
		if(gens.empty()) return NULL;
		return (cls::IShadingNodeGenerator*)gens[0].gen;
	};

	virtual float getCurrentFrame(
		)
	{
		return 1e12f;
	}

	struct genDataKey
	{
		int id;	// object
		cls::IGenerator* gen;
		bool operator<(const genDataKey& arg)const 
		{
			if(id<arg.id) return true;
			if(id>arg.id) return false;
			return (int)gen < (int)arg.gen;
		}
	};
	std::map<genDataKey, cls::renderCacheImpl<> > generatorexportcontext;

	// ������ ��� � ���� ���������� ��� �������� ���� 
	virtual bool getNodeExportFrameCache(
		float frame, 
		cls::INode& node,				// ������
		cls::IRender* &pcache,
		Math::Box3f* &pbox
		)
	{
		return false;
	};

	// ��������� ������, �������� � �������� ��������
	virtual cls::IRender* objectContext(
		MObject& obj, 
		cls::IGenerator* generator
		)
	{
		genDataKey key;
		key.gen = generator;
		key.id = *(int*)&obj;
		cls::renderCacheImpl<>& render = generatorexportcontext[key];
		return &render;
	}

	// �������� ������
	virtual cls::ITask* addTask(
		cls::ITask* parent			// �.�. NULL - ����� ����������� � ����� ������
		)
	{
		return NULL;
	}
	// ������ ����������� ����� ����� �������
	virtual cls::ITask* getPreTask(
		)
	{
		return NULL;
	}

	// �������������� ������ ����� ����������� 
	// �������� txmake
	virtual void addTask(
		enTaskType type,		// ���
		const char* title,		// ���������
		const char* cmd			// �������
		)
	{
		system(cmd);
	}
/*/
	// ��������!!!
	void setObjectData(
		MObject& obj, 
		cls::IGenerator* generator, 
		void* data)
	{
		genDataKey key;
		key.gen = generator;
		key.id = *(int*)&obj;
		generatordatas[key] = data;
	}
	// ��������!!!
	void* getObjectData(
		MObject& obj, 
		cls::IGenerator* generator 
		)
	{
		genDataKey key;
		key.gen = generator;
		key.id = *(int*)&obj;
		std::map<genDataKey, void*>::iterator it = generatordatas.find(key);
		if( it == generatordatas.end()) return 0;
		return it->second;
	}
/*/
	// crop
	int crop;
	virtual void setCrop(int crop)
	{
		this->crop = crop;
	};
	virtual int getCrop()
	{
		return this->crop;
	};
	// output file
	std::string outputfilename, nocropoutputfilename;
	virtual void setOutputFile(const char* outputfilename, bool bCrop=true)
	{
		if(bCrop)
			this->outputfilename = outputfilename;
		else
			this->nocropoutputfilename = outputfilename;
	};
	virtual const char* getOutputFile(bool bCrop=true)
	{
		if(bCrop)
			return outputfilename.c_str();
		else
			return nocropoutputfilename.c_str();
	};

public:
	// �������� ��� �����
	std::string scenename;
	// ���������� �������
	std::string projectDir;
	//! ������� �����
	int _frame;
	//! ��� ������ (� ����������� ���������)
	std::string taskname;
	// ������ ��� ������
	std::string alfredTaskName;
	// ��� ���� � ���������
	std::string rmappixpath;

	//! ��������� ����������
	std::string ocsDir, outputDir, shaderDir, currentPass;

	// ��� ����������
	ChiefGenerator chief;

	ChiefGenerator& getChiefGenerator()
	{
		return chief;
	}

	MayaExportContextImpl(const char* taskname)
	{
		crop = -1;
		_frame = 0;
		this->taskname = taskname;
		projectDir = "";

		MString resdir;
		MGlobal::executeCommand("workspace -rte renderScenes", resdir);
		rmappixpath = resdir.asChar();

		MStringArray _scenename;
		MGlobal::executeCommand("file -q -shn -l", _scenename);
		if( _scenename.length()>0)
			scenename = _scenename[0].asChar();
	}
	void InitChiefGenerator(enGeneratorsType chiefGenType)
	{
		ocellarisDefaultGenerators(OCGT_SCENE, chief, this);
	}
	~MayaExportContextImpl()
	{
		std::map<int, cls::INode* >::iterator it = nodeById.begin();
		for(;it != nodeById.end(); it++)
		{
			if( it->second)
				it->second->Release();
		}
	}

	void setProjectDirectory(const char* rootdir)
	{
		projectDir = rootdir;
	}
	void setOutputDirectory(const char* dir)
	{
		outputDir = dir;
	}
	void setOcsDir(const char* dir)
	{
		ocsDir = dir;
	}
	void setShaderDir(const char* dir)
	{
		shaderDir = dir;
	}
	void setPass(const char* dir)
	{
		currentPass = dir;
	}
	

	void setFrame(int _frame)
	{
		this->_frame = _frame;
	}
};
