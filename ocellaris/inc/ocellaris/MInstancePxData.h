#pragma once

//#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include "Util/Stream.h"
#include "ocellaris\OcellarisExport.h"
#include "MInstance.h"

namespace cls
{
	class OCELLARISEXPORT_API MInstancePxData : public MPxData
	{
	public:
		MInstancePxData();
		virtual ~MInstancePxData(); 

		// Override methods in MPxData.
		virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
		virtual MStatus readBinary( std::istream& in, unsigned length );
		virtual MStatus writeASCII( std::ostream& out );
		virtual MStatus writeBinary( std::ostream& out );

		// Custom methods.
		virtual void copy( const MPxData& ); 

		MTypeId typeId() const; 
		MString name() const;
		static void* creator();

	public:
		static MTypeId id;
		static const MString typeName;

		MInstance drawcache;

	/*/
		bool renderPoints, drawOnlyBox;
		int renderDegradation;
		std::map<int, listdata> dlists;

	//	GLuint redMat;

		void clear();
		GLuint newlist(int frame);
		void setbox( int frame, Math::Box3f box);
		void setfile( int frame, const char* filename){dlists[frame].filename=filename;}

	//	GLuint get(int frame);
		bool isValid(int frame);
		void draw(int frame, bool bPoints=false, bool bWireframe=true, bool bShaded=false);
		Math::Box3f getbox( int frame);
		const char* getfile( int frame);
		std::map<std::string, cls::Param>* getAttributes(int frame);
		void UpdateAttrList(int frame, MObject thisNode, bool bString=false);
		void SetAttrToRender(int frame, MObject thisNode, cls::IRender* render);
	/*/

	protected:
		void serialize(Util::Stream& stream);
	};
}