#pragma once

#include "ocellaris/renderExecutiveCacheImpl.h"
#include "NxPhysics.h"

class renderImplPhysX : public cls::renderExecutiveCacheImpl
{
public:
	// ��� �������
	virtual const char* renderType(
		){return "PhysX";};

public:
	typedef cls::renderExecutiveCacheImpl base;

	renderImplPhysX()
	{
		this->gScene = NULL;
		this->current = NULL;
		this->scale = 1;
		this->bKinematic = false;
		this->subSteps = 8;
	}
	virtual void init(
		NxScene* gScene,
		float scale
		)
	{
		this->gScene = gScene;
		this->current = NULL;
		this->scale = 1/scale;
	}
	virtual void setKinematicMode(bool bKinematic=true)
	{
		this->bKinematic = bKinematic;
	}
public:
	
	NxScene* gScene;
	int subSteps;
	bool bKinematic;

	std::string currentname;
	Math::Matrix4f currentpos;
	NxActorDesc* current;
	std::list<NxShapeDesc*> currentshapes;
	float scale;
	Math::Matrix4f cameraMatrix;

	// ����� ������� � � ������ ������ ��������� ����
	std::map<std::string, NxActor*> actors;
	std::map<std::string, std::string> shapesInActor;

	std::map<std::string, NxCloth*> cloths;

	struct Thread
	{
		cls::PA<Math::Vec3f> P;
		cls::PA<Math::Vec3f> Pfreeze;
		cls::PA<Math::Vec3f> N;
		cls::PA<int> numVerticesInThreads;
	};
	std::map<std::string, Thread> threads;

	struct ForceField
	{
		cls::P<Math::Vec3f> Constant;
		cls::P<Math::Vec3f> Noise;
	};
	ForceField forceField;

	// �������
	struct JointTask
	{
		NxJointDesc* joint;
		Math::Vec3f globalAncor;
		Math::Vec3f globalAxis;
		std::string shape0, shape1;
	};
	std::list<JointTask> jointtasks;
	std::map<std::string, NxJoint*> joints;

	// ��������
	struct Ballancer
	{
		float maxMotorSpeed;
		float maxMotorForce;
		float frequency;
		float phase;
		float offset;
		NxRevoluteJoint* joint;
	};
	std::list<Ballancer> ballancers;
	std::map<std::string, Ballancer*> ballancers_by_name;

	virtual void ClearActorList()
	{
		currentshapes.clear();
		actors.clear();
		shapesInActor.clear();
		ballancers_by_name.clear();
	}

	virtual bool BeginActor(const char* name)
	{
		if( current!=0) 
			printf("actor already started!!!\n");

		//GetAttribute("name", currentname);
		GetAttribute("fullName", currentname);
		std::map<std::string, NxActor*>::iterator ita = actors.find(currentname);
		if( ita != actors.end())
		{
			currentpos = this->transformStack.getTransform();
			return true;
		}

		shapesInActor[currentname] = currentname;

		currentpos = this->transformStack.getTransform();

		current = new NxActorDesc();
		current->globalPose = Mat44toNxMat34(currentpos);
//		current->globalPose.t = NxVec3(0,0,0);

		printf("Begin Actor %s\n", currentname.c_str());
		return true;
	}
	virtual bool EndActor()
	{
		std::map<std::string, NxActor*>::iterator ita = actors.find(currentname);
		if( ita != actors.end())
		{
			NxActor* actor = ita->second;
			actor->raiseBodyFlag(NX_BF_KINEMATIC);
//			actor->setGlobalPose( Mat44toNxMat34(currentpos));
			actor->moveGlobalPose( Mat44toNxMat34(currentpos));
			printf("Update Actor %s\n", currentname.c_str());
			return true;
		}
		if( !current)
		{
			printf("EndActor invalid: actor dont started!!!\n");
			return false;
		}

		NxBodyDesc bodyDesc;
		float density = 1;

		if (density)
		{
			current->body = &bodyDesc;
			current->density = density;
		}
		else
		{
			current->body = NULL;
		}

		bool kinematic = false;
		this->GetParameter("kinematic", kinematic);
		if( kinematic )
		{
			bodyDesc.flags |= NX_BF_KINEMATIC;
			printf("kinematic\n");
		}

		std::string externname;
		this->GetParameter("externTransformName", externname);

		current->name = strdup(currentname.c_str());
//		current->name = strdup(externname.c_str());

		NxActor* actor = gScene->createActor(*current);
		if( !actor)
		{
			printf("actor %s not created\n", currentname.c_str());
		}
		actors[currentname] = actor;
		printf("End Actor %s (extern:%s)\n\n", currentname.c_str(), externname.c_str());

		// CLEAR
		delete current;
		current = NULL;
		currentname = "";

		std::list<NxShapeDesc*>::iterator it = currentshapes.begin();
		for(;it != currentshapes.end(); it++)
		{
			delete *it;
		}
		currentshapes.clear();

		return true;
	}
	virtual float getScale()
	{
		return scale;
	}

	virtual Math::Matrix4f getGlobalTransform()
	{
		Math::Matrix4f pos = this->transformStack.getTransform();
		return pos;
	}
	virtual Math::Matrix4f getLocalTransform()
	{
		Math::Matrix4f pos = this->transformStack.getTransform();
		if( !current)
		{
			printf("getLocalTransform invald: actor dont started!!!\n");
			return pos;
		}

		pos = currentpos.inverted()*pos;
		return pos;
	}

	virtual bool AddShape(NxShapeDesc* shape)
	{
		if( bKinematic)
		{
			delete shape;
			return true;
		}
		if( !current)
		{
			delete shape;
			printf("AddShape invalid: actor dont started!!!\n");
			return false;
		}
		if( shape)
		{
			current->shapes.pushBack(shape);
			currentshapes.push_back(shape);
		}

		std::string name;
		GetAttribute("name", name);
		shapesInActor[name] = currentname;

		printf("Shape %s\n", name.c_str());

		return true;
	}

	virtual bool AddCloth( NxClothDesc* clothDesc )
	{
		clothDesc->globalPose = Mat44toNxMat34( this->transformStack.getTransform() );
		NxCloth* cloth = gScene->createCloth( *clothDesc );
		delete clothDesc;

		std::string name;
		GetAttribute("name", name);
		const char* clothName = strdup(name.c_str());
		cloth->setName( clothName );
		cloths[clothName] = cloth;

		cls::PA<float> points;
		GetParameter("physX::points", points);
		int numPoints = points.size()/3;
		cls::PA<Math::Vec3f> Pfreeze;
		Pfreeze.reserve(numPoints);
		for( int i = 0; i < numPoints; i++ )
		{
			Pfreeze.push_back( Math::Vec3f( points[i*3], points[i*3 + 1], points[i*3 + 2] ) );
		}

		cls::PA<int> numVerticesInThreads;
		GetParameter("numVerticesInThreads", numVerticesInThreads);

		//threads[clothName].P = GetParameter("P");
		threads[clothName].Pfreeze = Pfreeze;//GetParameter("#Pfreeze");
		//threads[clothName].N = GetParameter("#N");
		threads[clothName].numVerticesInThreads = numVerticesInThreads;

		bool glueStart = true;
		GetParameter( "glueStart", glueStart );
		bool glueEnd = true;
		GetParameter( "glueEnd", glueEnd );

		NxVec3 *positions = new NxVec3[ cloth->getNumberOfParticles() ];
		cloth->getPositions(positions);

		if( glueStart )
		{
			cloth->attachVertexToGlobalPosition( 1, positions[1] );
			cloth->attachVertexToGlobalPosition( 2, positions[2] );
		}

		if( glueEnd )
		{
			cloth->attachVertexToGlobalPosition( 0, positions[0] );
			cloth->attachVertexToGlobalPosition( 3, positions[3] );
		}

		cls::PS glueVertices("");
        GetParameter("glueVertices",glueVertices);

		char* glueVerticesStr = strdup(glueVertices.data());
		char seps[] = " ,";
		char* token = NULL;
		std::vector<int> glue;

		token = strtok( glueVerticesStr, seps );

		while( token != NULL )
		{
			//printf("_%s\n",token);
			glue.push_back(atoi(token));
			token = strtok( NULL, seps );
		}

		for( unsigned int i = 0; i < glue.size(); i++ )
		{
			if( glue[i] >= 0 && glue[i] < (int)cloth->getNumberOfParticles() )
			{
				cloth->attachVertexToGlobalPosition( glue[i], positions[glue[i]] );
			}
		}

		delete[] positions;

		return true;
	}

	virtual bool AddJoint(NxJointDesc* joint, Math::Vec3f& globalAncor, Math::Vec3f& globalAxis, const char* shape0, const char* shape1)
	{
		if( bKinematic)
		{
			delete joint;
			return true;
		}

		jointtasks.push_back(JointTask());
		JointTask& jt = jointtasks.back();
		jt.joint = joint;
		jt.globalAncor = globalAncor;

		jt.globalAxis = globalAxis;
		jt.shape0 = shape0;
		jt.shape1 = shape1;

		std::string name;
		GetAttribute("name", name);
		jt.joint->name  = strdup(name.c_str());

		bool ballancer = false;
		this->GetParameter("physX::ballancerEnable", ballancer);
		if(ballancer)
		{
			ballancers.push_back(Ballancer());
			Ballancer& b = ballancers.back();
			this->GetParameter("physX::ballancerFrequency", b.frequency);
			this->GetParameter("physX::ballancerPhase", b.phase);
			this->GetParameter("physX::ballancerOffset", b.offset);
			this->GetParameter("physX::maxMotorSpeed", b.maxMotorSpeed);
			this->GetParameter("physX::maxMotorForce", b.maxMotorForce);
			b.joint = NULL;
			ballancers_by_name[name] = &b;
		}

		return true;
	}

	virtual NxActor* FindActorByShapeName(const char* shape)
	{
		std::map<std::string, std::string>::iterator is = shapesInActor.find(shape);
		if( is == shapesInActor.end())
		{
			printf("cant find shape \"%s\"\n", shape);
			return 0;
		}

		std::map<std::string, NxActor*>::iterator ia = actors.find(is->second);
		if( ia == actors.end())
		{
			printf("cant find actor \"%s\"\n", is->second.c_str());
			return 0;
		}
		return ia->second;
	}
	virtual void CompleteJoints()
	{
		std::list<JointTask>::iterator it = jointtasks.begin();
		for( ; it != jointtasks.end(); it++)
		{
			JointTask& jt = *it;

			NxActor* a0 = FindActorByShapeName( jt.shape0.c_str());
			NxActor* a1 = FindActorByShapeName( jt.shape1.c_str());
			Math::Vec3f globalAncor = jt.globalAncor;

			if( a0==0 || a1==0) 
			{
				printf("cant create joint ");
				continue;
			}
			jt.joint->actor[0] = a0;
			jt.joint->actor[1] = a1;

			jt.joint->setGlobalAnchor(NxVec3(globalAncor.x, globalAncor.y, globalAncor.z));

			if( jt.globalAxis!=Math::Vec3f(0))
				jt.joint->setGlobalAxis(NxVec3(jt.globalAxis.x, jt.globalAxis.y, jt.globalAxis.z));

			NxJoint* res = gScene->createJoint(*jt.joint);

			if( ballancers_by_name.find(jt.joint->name)!=ballancers_by_name.end())
			{
				if( res->getType()==NX_JOINT_REVOLUTE)
					ballancers_by_name[jt.joint->name]->joint = (NxRevoluteJoint*)res;
			}

			joints[jt.joint->name] = res;
			delete jt.joint;
			printf("Create Joint %s-%s\n", jt.shape0.c_str(), jt.shape1.c_str());
		}
		jointtasks.clear();
	}

	virtual NxMat34 Mat44toNxMat34(Math::Matrix4f m)
	{
		m[0].normalize();
		m[1].normalize();
		m[2].normalize();

		if(true)
		{
			std::swap( m[1][0], m[0][1]);
			std::swap( m[2][0], m[0][2]);
			std::swap( m[2][1], m[1][2]);
		}

		NxMat34 res;
		res.setRowMajor44(m.data());
		res.t = NxVec3(m[3][0]*scale, m[3][1]*scale, m[3][2]*scale);
		return res;
	}

	virtual Math::Matrix4f NxMat34toMat44(NxMat34 m)
	{
		Math::Matrix4f res;

		m.getColumnMajor44( res.data() );

		res[0].normalize();
		res[1].normalize();
		res[2].normalize();

		res[3][0] = m.t.x*scale;
		res[3][1] = m.t.y*scale;
		res[3][2] = m.t.z*scale;

		return res;
	}

	virtual void PushTransform(void) 
	{
		transformStack.PushTransform();
		base::PushTransform(); 
	}
	virtual void PopTransform(void) 
	{
		transformStack.PopTransform();
		base::PopTransform();
	}
	virtual void SetTransform()
	{
		cls::P<Math::Matrix4f> M = GetParameter("transform");
		transformStack.SetTransform(*M);
		base::SetTransform();
	}
	virtual void AppendTransform() 
	{
		if( current)
		{
			std::string name;
			GetAttribute("name", name);
			if( shapesInActor.find(name)==shapesInActor.end())
			{
				shapesInActor[name] = currentname;
				printf("%s in %s\n", name.c_str(), currentname.c_str());
			}
		}

		cls::P<Math::Matrix4f> M = GetParameter("transform");
		transformStack.AppendTransform(*M);
		base::AppendTransform();
	}

	//! �������� �� ��������
	virtual void ParameterFromAttribute(
		const char *paramname,		//!< ��� ���������
		const char *attrname		//!< ��� �������� �� �������� ����� ������
		)
	{
		base::ParameterFromAttribute(paramname, attrname);
	}
	//! �������� �� ��������
	virtual void ParameterToAttribute(
		const char *paramname,		//!< ��� ���������
		const char *attrname		//!< ��� �������� �� �������� ����� ������
		)
	{
		base::ParameterToAttribute(paramname, attrname);
	}
	//! ������� �� ��������
	virtual void AttributeFromAttribute(
		const char *dstname,	//!< ��� �������� ���������
		const char *srcname		//!< ��� �������� �� �������� ����� ������
		)
	{
		base::AttributeFromAttribute(dstname, srcname);
	}

};