#pragma once

#include "ocellaris/IGenerator.h"
#include "ocellaris/INode.h"
#include "ocellaris/IRender.h"

namespace cls
{
	//! @ingroup plugin_group
	//! \brief ��������� �������
	//!
	//! ������������ ��������� ������
	struct INodeGenerator : public IGenerator
	{
		//! render ����� ���� = NULL
		virtual bool OnNodeStart(
			cls::INode& node,				//!< ������
			cls::IRender* render,			//!< render
			cls::IExportContext* context,	//!< context 
			MObject& generatornode			//!< generator node �.� 0
			)=0;
		//! render ����� ���� = NULL
		virtual void OnNodeEnd(
			cls::INode& node,				//!< ������
			cls::IRender* render,			//!< render
			cls::IExportContext* context,	//!< context 
			MObject& generatornode			//!< generator node �.� 0
			)=0;
	private:
		virtual enGenType GetType(
			){return NODE;};
	};
}