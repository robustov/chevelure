#pragma once
#include "ocellaris\renderBaseCacheImpl.h"
#include "ocellaris\renderImpl.h"
#include "ocellaris/CacheStorageSimple.h"
#include "ocellaris/T/TAttributeStack_forblur.h"
#include "ocellaris/T/TTransformStack_minimal.h"
#include "ocellaris/T/TBranchStack_minimal.h"

namespace cls
{
	//! @ingroup impl_group 
	//! \brief ��� � ��������������� ����������� ������� Call, RenderCall � Filter
	//! 
	//! ������������ ��� ������� 
	struct renderExecutiveCacheImpl : public renderBaseCacheImpl<CacheStorageSimple, TAttributeStack_forblur, TTransformStack_minimal, TBranchStack_minimal>
	{
		typedef TAttributeStack_forblur ATTRIBUTESTACK;
		typedef TTransformStack_minimal TRANSFORMSTACK;
		typedef TBranchStack_minimal BRANCHSTACK;
		typedef renderBaseCacheImpl<CacheStorageSimple, ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK> base;
		typedef renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK> base_renderImpl;
		renderExecutiveCacheImpl(
			)
		{
			clear();
		}
		// ���������� worldtransform � �������� �� IRender
		renderExecutiveCacheImpl(
			IRender* src
			)
		{
			clear();

			Math::Matrix4f WT;
			if( src->GetParameter("worldtransform", WT))
				transformStack.SetTransform(WT);

			std::vector< std::pair< std::string, Param> > attributes;
			src->GetAttributeList(attributes);
			for( int i=0;i<(int)attributes.size(); i++)
				attributeStack.Attribute(attributes[i].first.c_str(), attributes[i].second);
		}

		//! �������� �� ��������
		void ParameterFromAttribute(
			const char *paramname,		//!< ��� ���������
			const char *attrname		//!< ��� �������� �� �������� ����� ������
			)
		{
			if(!isNormalExe()) return;

			Param p = GetAttributeV(attrname);
			if(p.empty()) return;
			ParameterV(paramname, p);
		}
		//! �������� � �������
		virtual void ParameterToAttribute(
			const char *paramname,		//!< ��� ��������� �� �������� ����� ������
			const char *attrname		//!< ��� �������� 
			)
		{
			if(!isNormalExe()) return;

			Param p = GetParameterV(paramname);
			if(p.empty()) return;
			AttributeV(attrname, p);
		}
		//! ������� �� ��������
		virtual void AttributeFromAttribute(
			const char *dstname,	//!< ��� �������� ���������
			const char *srcname		//!< ��� �������� �� �������� ����� ������
			)
		{
			if(!isNormalExe()) return;

			Param p = GetAttributeV(srcname);
			if(p.empty()) return;
			AttributeV(dstname, p);
		}
		///////////////////////////////////////////////////////////////
		/// ��������� �� �������� ��������
		virtual void IfAttribute(const char* name, const Param& t)
		{
			renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::IfAttribute(name, t);
		}
		virtual void EndIfAttribute()
		{
			renderImpl<ATTRIBUTESTACK, TRANSFORMSTACK, BRANCHSTACK>::EndIfAttribute();
		}

		/// ����� ��������� ���������� ������� ������
		// ���������� �������
		virtual void SystemCall(
			enSystemCall call
			)
		{
			CallFilters(call);
			base::SystemCall(call);
		}

		/// Procedural (���������������� �����)
		virtual void Call( 
			IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
			)
		{
			if(!isNormalExe()) return;

			pProcedural->Process(this);
		}
		virtual void Call( 
			const char*	procname		//!< ����� ������� IProcedural::Render()
			)
		{
			IProcedural* pProcedural = findProcedure(procname);
			if(!pProcedural)
				return;
			this->Call(pProcedural);
		}

		/// Procedural (��������� �����)
		/// ��� ���������� ���������� ����� IProcedural::Render()
		/// ���� ����� ������ �������� � ����!!! (��� �����???)
		virtual void RenderCall( 
			IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
			)
		{
			if(!isNormalExe()) return;

			pProcedural->Render(this, attributeStack.getMotionSamplesCount(), attributeStack.getMotionTimes());
		}
		virtual void RenderCall( 
			const char*	procname		//!< ����� ������� IProcedural::Render()
			)
		{
			IProcedural* pProcedural = findProcedure(procname);
			if(!pProcedural)
			{
				fprintf(stderr, "renderExecutiveCacheImpl::RenderCall! Procedure %s not found\n", procname);
				return;
			}
			this->RenderCall(pProcedural);
		}
		void Filter(IFilter* filter)
		{
			if(!isNormalExe()) return;

			base_renderImpl::Filter(filter);
		}

		void Filter( 
			const char*	filter				//!< ��� ��������� � DeclareProcBegin ��� dllname@procname
			)
		{
			if(!isNormalExe()) return;

			base_renderImpl::Filter(filter);
		}

		void MotionBegin()
		{
			if(!isNormalExe()) return;

			attributeStack.resetMotion();
			base::MotionBegin();
		}
		void MotionEnd()
		{
			if(!isNormalExe()) return;

			attributeStack.setMotionParamPhase(nonBlurValue);
			base::MotionEnd();
		}
		void MotionPhaseBegin(float time)
		{
			if(!isNormalExe()) return;

			attributeStack.addMotionParamPhase(time);
			attributeStack.setMotionParamPhase(time);
			base::MotionPhaseBegin(time);
		}
		void MotionPhaseEnd()
		{
			if(!isNormalExe()) return;

			attributeStack.setMotionParamPhase(nonBlurValue);
			base::MotionPhaseEnd();
		}
		bool SetCurrentMotionPhase(float time)
		{
			return attributeStack.setMotionParamPhase(time);
		}
	};
};
