#pragma once
#include "IRender.h"
//#include <ri.h>
#include <map>
#include <list>
#include <vector>
#include <string>
#include "Util/DllProcedure.h"
#include "Util/tree.h"
#include "ocellaris/renderBaseCacheImpl.h"
#include "ocellaris/T/TCacheStorageBlur.h"
#include "ocellaris/T/TAttributeStack_pure.h"

namespace cls
{
	//! @ingroup impl_group 
	//! \brief ��� ������������ ������ ������ ��� ���������� renderCacheImpl 
	//! 
	//! ��� ����������
	//! �������� ��� ������ ��. procedures\ReadFile.cpp
	template< class ATTRIBUTESTACK = TAttributeStack_minimal, class TRANSFORMSTACK = TTransformStack_pure>
	struct renderBlurParser : public renderBaseCacheImpl<TCacheStorageBlur, ATTRIBUTESTACK, TRANSFORMSTACK>
	{
		typedef renderBaseCacheImpl<TCacheStorageBlur, ATTRIBUTESTACK, TRANSFORMSTACK> BASE;

		renderBlurParser(
			);
		// �� ��������� �����������
		void SetCurrentPhase(
			float phase
			);
		// ������
		void Render(
			IRender* render
			);
	};
}
#include "inl/renderBlurParser.hpp"
