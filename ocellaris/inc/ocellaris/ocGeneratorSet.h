#pragma once

#ifdef OCELLARISEXP_EXPORTS
#define OCELLARISEXPORT_API __declspec(dllexport)
#else
#define OCELLARISEXPORT_API __declspec(dllimport)
#endif

#include <maya/MPxObjectSet.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 

namespace cls
{
	class OCELLARISEXPORT_API ocGeneratorSet : public MPxObjectSet
	{
	public:
		ocGeneratorSet();
		virtual ~ocGeneratorSet(); 

		virtual bool setInternalValue( 
			const MPlug&,
			const MDataHandle&);

		virtual MStatus compute( 
			const MPlug& plug, 
			MDataBlock& data );

		static void* creator();
		static MStatus initialize();

	public:
		static MObject i_type;				// ���
		static MObject i_generator;			// ��� ����������
		static MObject i_calltypeAttribute;	// � ����� ������ �������� (��� ������� ����������)

	public:
		static MTypeId id;
		static const MString typeName;
	};
}
