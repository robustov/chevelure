#pragma once

//#include "ocellaris/IExportContext.h"
class MObject;
namespace cls
{
	struct IExportContext;
	struct INode;

	//! @ingroup plugin_group
	//! \brief ������� ��������� ��� ���� �����������
	//! 
	//! IGenerator - ������� ��������� ��� ���� �����������
	struct IGenerator
	{
		enum enGenType
		{
			UNKNOWN			= -1,
			ATTRIBUTE		= 0,	// ��� ���������� �������������� �������� ��� ��������
									// ����� ���������� � ������ ������� ��������: CT_BEFORE_TRANSFORM CT_BEFORE_NODE CT_BEFORE_SHADER CT_BEFORE_GEOMETRY

			DEFORM			= 1,	// ������������� ���������, �������� ��������� � ������� �����
									// ���������� ����� �������� ���������, �� ����� ������� � ���� ���������� ������ (Points, Mesh, Sphere � �.�.)

			GEOMETRY		= 2,	// ��� �������� ���������
									// ��������� ������������ ����. �������� �� ����������� �� ��������� �������� � �����

			LAYER			= 3,	// �� ������������!!!
									//

			NODEFILTER		= 4,	// ����������� ���������, 
									// ��� ������������ �������� �� ���������� ���������

			NODE			= 5,	// ������������ ��������� ������???
									// ���������� ��������� � ����������� �������� � �����

			PASS			= 6,	// IPassGenerator - ������� ��������� ��� PASS �����������
									// �������� ������ - ������� ������ � �������� ����������

			SHADER			= 7,	// ��� ���������� ������ � �������
									//
			TRANSFORM		= 8,	// ������� ����������
									// ������ ������ ��������� ������������� �� ����� �������� ��� ������� bounding box

			PROXY			= 9,	// ������ ��������� ��������� ����������� �������������� ������ (MObject) �� ������
									// ���������� ��� ���������� ������ �������������� �������� ��.INode

			SHADINGNODE		= 10,	// ��������� ��� ���� Shading Network, 
									// ��� ��������

			BLUR			= 11,	// ��������� ����� ��. IBlurGenerator 
									// ��� ��������

			LIGHT			= 12,	// ��������� ��
		};
		// ��� ����������
		virtual enGenType GetType(
			)=0;
		// ���������� ���, ������ ��� �������������
		virtual const char* GetUniqueName(
			)=0;

		// ��� �������� � ����
		virtual void OnLoadToMaya(
			MObject& generatornode
			){};
		// ����������� ��������� (���������� 1 ��� ����� ������� ��������)
		virtual bool OnPrepare(
			cls::IExportContext* context
			){return true;};
		//! ����������� ���� � �������� 
		virtual bool OnPrepareNode(
			cls::INode& node, 
			cls::IExportContext* context,
			MObject& generatornode
			){return true;};

		virtual ~IGenerator(){};
	};
	typedef cls::IGenerator* (*GENERATOR_CREATOR)();
}