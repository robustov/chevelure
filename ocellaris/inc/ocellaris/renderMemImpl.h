#pragma once
#include "IRender.h"
//#include <ri.h>
#include <map>
#include <list>
#include <vector>
#include <string>
#include "Util/misc_switch_string.h"
#include "renderFileBaseImpl.h"

namespace cls
{
	struct memStream
	{
		memStream()
		{
			this->cur = 0;
		}
		inline bool isValidForRead()
		{
			return !writebuf.empty();
		}

		inline int _ftell()
		{
			return this->cur;
		}
		inline int _fseek(int tell, int base)
		{
			if( base==SEEK_SET)
			{
				this->cur = tell;
				return 1;
			}
			return 0;
		}

		inline int _fgetc()
		{
			fprintf(stderr, "text functions in memStream not implemented");
			return 0;
		}
		inline void _fgets(char* b, int len)
		{
			fprintf(stderr, "text functions in memStream not implemented");
		}
		inline int _feof()
		{
			return ( this->cur >= (int)writebuf.size())?1:0;
		}
		inline void _fputs(const char* text)
		{
			fprintf(stderr, "text functions in memStream not implemented");
		}
		inline void _fprintf(const char* text, ...)
		{
			fprintf(stderr, "text functions in memStream not implemented");
		}

		int _fread( void *buffer, int size, size_t count)
		{
			char* data = &writebuf[0];
			char* b = (char*)buffer;
			for(size_t i=0; i<count; i++)
			{
				for(size_t c=0; c<size; c++)
				{
					if( this->cur >= (int)writebuf.size()) return 0;
					*(b++) = *(data+cur);
					cur++;
				}
			}
			return size*count;
		}
		int _fwrite( const void *buffer, int size, size_t count)
		{
			for(size_t i=0; i<count; i++)
			{
				writebuf.insert(writebuf.end(), (const char*)buffer, (const char*)buffer+size);
			}
			this->cur = writebuf.size();
			return size*count;
		}
	public:
		int cur;
		// ��� ������
		std::vector<char> writebuf;
	};
	//! @ingroup impl_group 
	//! \brief ���������� IRender ��� ������ � ����
	//! 
	//! ��� ������ � ����
	//! ������� Load(): ������ ����� � �������� �� ���������
	struct renderMemImpl : public renderFileBaseImpl<memStream>
	{
		renderMemImpl(
			);
		~renderMemImpl(
			);
		bool openForWrite(
			);
		//! ������ ����� � �������� �� ���������
		bool openForRead(
			const char* buffer, int buflen
			);
		std::vector< char>& getWriteBuffer(
			);
	public:
		void comment(const char* text){};

	};
}

namespace cls
{
	inline renderMemImpl::renderMemImpl()
	{
	}
	inline std::vector< char>& renderMemImpl::getWriteBuffer()
	{
		return file.writebuf;
	}
	inline renderMemImpl::~renderMemImpl()
	{
	}
	inline bool renderMemImpl::openForWrite(
		)
	{
		this->bWrite = true;
		this->bAscii = false;
		this->indent = 0;

		return true;
	}
	inline bool renderMemImpl::openForRead(
		const char* buffer, int buflen
		)
	{
		file.writebuf.assign(buffer, buffer+buflen);
		file.cur = 0;

		this->tell = file._ftell();
		this->bReadGlobals = false;
		this->bWrite = false;
		this->bAscii = false;
		this->symb = -1;
		this->line = this->position = 0;
		return true;
	}
}