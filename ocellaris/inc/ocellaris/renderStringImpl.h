#pragma once
#include "IRender.h"
//#include <ri.h>
#include <map>
#include <list>
#include <vector>
#include <string>
#include "Util/misc_switch_string.h"
#include "renderFileBaseImpl.h"
#include "IExportContext.h"

namespace cls
{
	struct stringStream
	{
		stringStream()
		{
			buffer = NULL;
			cur = NULL;
			fulllen = 0;
		}
		inline bool isValidForRead()
		{
			return buffer!=0;
		}

		inline int _ftell()
		{
			return (int)(cur - buffer);
		}
		inline int _fseek(int tell, int base)
		{
			if( base==SEEK_SET)
			{
				cur = buffer+tell;
				return 1;
			}
			return 0;
		}

		inline int _fgetc()
		{
			if( !(*cur)) return ' ';
			return *(cur++);
		}
		inline void _fgets(char* b, int len)
		{
			for(int c=0; c<len; c++)
			{
				if( !(*cur)) 
					break;
				*(b++) = *(cur++);
			}
			*(b++) = 0;
		}
		inline int _feof()
		{
			return ( *cur == 0)?1:0;
		}
		inline void _fputs(const char* text)
		{
			writebuf.push_back(text);
			fulllen += (int)writebuf.back().size();
		}
		inline void _fprintf(const char* text, ...)
		{
			va_list argList; va_start(argList, text);
			char sbuf[256];
			_vsnprintf(sbuf, 255, text, argList);
			va_end(argList);
			writebuf.push_back(sbuf);
			fulllen += (int)writebuf.back().size();
		}

		int _fread( void *buffer, int size, size_t count)
		{
			char* b = (char*)buffer;
			for(size_t i=0; i<count; i++)
			{
				for(size_t c=0; c<(size_t)size; c++)
				{
					if( !(*cur)) return 0;
					*(b++) = *(cur++);
				}
			}
			return (int)(size*count);
		}
		int _fwrite( const void *buffer, int size, size_t count)
		{
			return 0;
		}
	public:
		const char* buffer;
		const char* cur;
		// ��� ������
		std::list< std::string> writebuf;
		int fulllen;

	};
	//! @ingroup impl_group 
	//! \brief ���������� IRender ��� ������ � ����
	//! 
	//! ��� ������ � ����
	//! ������� Load(): ������ ����� � �������� �� ���������
	struct renderStringImpl : public renderFileBaseImpl<stringStream>
	{
		renderStringImpl(
			);
		~renderStringImpl(
			);
		bool openForWrite(
			);
		//! ������ ����� � �������� �� ���������
		bool openForRead(
			const char* buffer
			);
		std::string getWriteBuffer(
			);
	public:
		void comment(const char* text){};
	
	public:
		// ������� �����
		static bool RenderText(
			const char* text, 
			cls::IRender* render,
			cls::IExportContext* context = NULL
			);

	};
}

namespace cls
{
	inline renderStringImpl::renderStringImpl()
	{
	}
	inline std::string renderStringImpl::getWriteBuffer()
	{
		std::string str;
		str.reserve(file.fulllen+100);
		std::list< std::string>::iterator it = file.writebuf.begin();
		for(;it != file.writebuf.end(); it++)
			str += *it;
		return str;
	}
	inline renderStringImpl::~renderStringImpl()
	{
	}
	inline bool renderStringImpl::openForWrite(
		)
	{
		this->bWrite = true;
		this->bAscii = true;
		this->indent = 0;
//		file._fputs( header_ocellaris);
//		file._fputs( header_ascii);
//		file._fputs( "\n");

		return true;
	}
	inline bool renderStringImpl::openForRead(
		const char* buffer
		)
	{
		file.buffer = buffer;
		file.cur = buffer;

//		for( int i=0; i<cls::_header_length; i++)
//			file._fgetc();

		this->tell = file._ftell();
		this->bReadGlobals = false;
		this->bWrite = false;
		this->bAscii = true;
		this->symb = -1;
		this->line = this->position = 0;
		return true;
	}
	// ������� �����
	inline bool renderStringImpl::RenderText(
		const char* text, 
		cls::IRender* render,
		cls::IExportContext* context
		)
	{
		try
		{
			cls::renderStringImpl fileimpl;
			std::string customdata = text;
			customdata += "\n";
			if( fileimpl.openForRead(customdata.c_str()))
			{
				fileimpl.Render(render);
			}
		}
		catch(Util::Error& error)
		{
			if( context)
				context->error(error.what());
			return false;
		}
		catch(...)
		{
			if( context)
				context->error("Error parse addString");
			return false;
		}
		return true;
	}

}