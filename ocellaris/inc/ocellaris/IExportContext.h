#pragma once
#include <set>
#include "IOcellaris.h"

#include "ocellaris/IGenerator.h"
#include "ocellaris/ITask.h"
class MObject;
class MDagPath;
class MayaProgress;
struct ChiefGenerator;

namespace cls
{
	struct INode;
	struct IGenerator;
	struct IShadingNodeGenerator;
	struct IRender;
	struct ITask;
	struct IPassGenerator;
	//! @ingroup ocellaris_group 
	//! \brief IExportContext �������� ��������
	//! 
	//! ������������ ���� ������� ������� ��������� �������� � �������:
	//! - �������� �����������
	//! - ��� ���
	//! - �������� ��������
	//! - �������� ������
	//!	- �������� ��������
	//! - �������� �����
	//! - �������
	//! - ������ �����
	//!
	//! ���������� �������� � IGeometryGenerator::OnGeometry()
	//!
	//! 
	struct IExportContext
	{
		IExportContext();
		virtual ~IExportContext();

	// ���������
	public:
		// message
		virtual void printf(const char* text, ...){};
		// ������
		virtual void error(const char* text, ...){};

	// ����������
	public:
		//! ����������
		//! ����� ���� ����� �������������� ������ � 
		//! getShadowDirectory getOcsDirectory � �.�.
		//! 
		//! ������ ����������� ����������:
		//! JOBNAME   - ��� ������
		//! WORKSPACEDIR - ��� ��� ����� ��������� workspace
		//! SHADERDIR	- ���� ��������� ���������� �������
		//! SHADERCOMPILER	- "%RMANTREE%/bin/shader.exe -o %s -I%RATTREE%/lib/slim/include %s"
		//! OUTPUTDIR - ���������� ��� �������� ������ ����������
		//! RIBDIR    - ���������� ��� ������ �������� 
		//! STATDIR		- ���������� ��� ������ ����������
		//! TEXDIR		- ���������� ��� ��������������� ������ �������
		//! SEARCHDIR	- ����������� � ���� � searchpath
		//! OCSDIR		- ��� ���� �� ����
		//! DISPLAYTYPE	- ���� ������ � final pass ������������ ��� ��������
		//! RENDER		- ��� ������� ("prman" "netrender")
		//! RENDERCOMMAND - ������� ������� ������� ( ������������ � final pass)
		//! RENDERSHADOWCOMMAND - ������� ������� ������� ( ������������ � shadow pass)
		//! OCSCOMMENT  - ���������� � �������������� �����
		//! 
		//! EXPORTTEMPLATED	- 0 ��� 1 (���� 1 �� ������������ template ���� (������������ � �������) )
		//! IGNOREOVERRIDE	- 0 ��� 1 (���� 1 �� ������� overrideVisibility �� �����������)
		//! FRAMENUMBER - (����� �����) ������������ ������������� � ExportContextImpl::doexport
		//! 

		//! SimpleExportPass:
		//! OUTPUTFILENAME - ��� ��������� ����� (����� ��������� %s ��� FRAMENUMBER)
		//! FRAMENUMBER - ����� �����
		//! OUTPUTFORMAT - ������ (ascii, binary)
		//! SRCFILENAME - ��� ��������� ����� ����� (�������������)
		//! SRCHOSTNAME	- ��� � ����� ��������� (�������������)
		//! BLUR		- (int)
		//! BLURSHUTTEROPENFRAME  (��������)
		//! BLURSHUTTERCLOSEFRAME (��������)
		//! BOUNDINGBOX - string ("extbox.min.x, extbox.min.y, extbox.min.z, extbox.max.x, extbox.max.y, extbox.max.z")

		//!SlimShaderGenerator
		//! SLIMDUMP	("0" "1") - ��� �������� ������� ������
		//! SLIMCOPYSHADERS ("0" "1") - �������� �������

		//! ExportToPrman (���������� �� RibGen)
		//! PASSNAME - ��� ������� (������)
		//! 
		//! PAUSERENDER - ������������ ��� ������������ ������� �������
		//! 

		//! Instancer Generator
		//! LAZYINSTANCERGENERATOR - 0 (default) ��� 1 - �� ��������� ����� �������������� ��������

		virtual const char* getEnvironment(
			const char* env
			)const=0;
		// ����������
		virtual void setEnvironment(
			const char* env, 
			const char* value
			)=0;

	// �������� �����������:
	public:
		virtual ChiefGenerator* getGenerators(
			)=0;

	// ���������� �� ���� �����������:
	public:
		// �������� ������ � �������
		virtual cls::INode* addNode(
			MObject& obj,					//!< ������
			cls::INode* masterpassnode=0	//!< ���� ������ ����������� ����������� �������� � �� ������ �������� � �������� �������
			)=0;
		// �������� ������ ����
		virtual cls::INode* addPass(
			MObject& obj				//!< ������
			)=0;
		// ������������ RenderFrame ��� �������
		// �������� pass � ������ RenderFrame ��� ��������� �����
		virtual void addRenderFramePass(
			cls::INode& node,				//!< ������ pass
			float frametime					//!< � ������ ����� ���������
			)=0;
		// ������������ ExportFrame
		// �������� ������ � ������ ExportFrame ��� ��������� RenderFrame
		// ���������� �� ���� �����������:
		virtual void addExportTimeNode(
			cls::INode& node,				//!< ������ 
			float frametime,				//!< � ������ ����� ���������
			float exportTime				//!< ������ ����� ������ ���� ���������� �������
			)=0;

	// ���������� �� �����������:
	public:
		// ������ ������� ����� ������� �����������
		// ������� ������ � ������ do export - ����� ������� OnExport � pass
		virtual float getCurrentFrame(
			)=0;

	// ���������� �� �����������:
	public:
		// ������ ����� ���������� �����
		virtual void getGlobalLights(
			std::vector<cls::INode* >& lights
			)=0;
		// ������� ���� �������� ��� �������
		virtual cls::INode* getNodeForObject(
			MObject& obj
			)=0;
		// ������� ��������� ��� �������� ���� �������� ???
		virtual cls::IShadingNodeGenerator* getShadingNodeGenerator(
			MObject& obj
			)=0;
		// ��������� ������, �������� � �������� ��������
		virtual cls::IRender* objectContext(
			MObject& obj,				// ����� ���� INode* ???
			cls::IGenerator* generator
			)=0;
		// ������ ��� � ���� ���������� ��� �������� ���� 
		virtual bool getNodeExportFrameCache(
			float frame, 
			cls::INode& node,				// ������
			IRender* &pcache,
			Math::Box3f* &pbox
			)=0;
	// ���������� ��������:
	public:
		// �������� ������
		virtual cls::ITask* addTask(
			cls::ITask* parent			// �.�. NULL - ����� ����������� � ����� ������
			)=0;
		// ����� ������ �� �����
		virtual cls::ITask* findTask(
			const char* taskname, 
			bool bTopLevelOnly=true
			)=0;
		// ���������� ����� ����� ��������
		virtual bool addRelation(
			cls::ITask* task, 
			cls::ITask* dependon 
			)=0;
		// ������ ����������� ����� ����� �������
		virtual cls::ITask* getPreTask(
			)=0;


	// ��� ������ �� ��������
	public:

		// ������ env �� ������� � �����
		virtual void clear(){};
		// �������� � env 
		virtual bool setenv(const char* env, const char* val){return false;};
		virtual const char* getenv(const char* env) const{return NULL;};

	// ����������
		// ������ ����������� �� ���������
		virtual bool addDefaultGenerators(const char* options){return false;};

		// �������� ����������� ��������� ��� ������� ����
		virtual bool addGeneratorForType(const char* type, const char* generator){return false;};

		// ��������� �-��� ������ ��������� �� ����� � ����
		virtual cls::IGenerator* loadGenerator(const char* generator, cls::IGenerator::enGenType type){return 0;};

		// ����������� ����������
		virtual bool prepareGenerators(){return false;};


		// ������ ���, �������� � �����������
		virtual cls::INode* addnodes(const char* root, const char* options){return 0;};
		virtual cls::INode* addnodes(MDagPath& path, const char* options){return 0;};

		// ������ �������
		virtual int addpass(const char* passnode, const char* options){return 0;};
		virtual int addpass(cls::IPassGenerator* passgen, const char* options){return 0;};
		// ����� ��������
		virtual int connectPass(const char* mainpass, const char* subpass, const char* options){return 0;};

		// ��
		virtual int addlight(const char* lightnode, const char* options){return 0;};

		// �������� ���� � ��������� ������� ����� (��� ���� ���������� �� ���������)
		virtual int addframe(double frame){return 0;};

		// ���������� � �������� (�����)
		// ��������� export frames
		virtual int preparenodes(){return 0;};

		// ���������� �������
		virtual int doexport(const char* dumpoption, const char* options, MayaProgress* progress){return 0;};

		// �������� �� ������
		virtual int render(const char* renderoption){return 0;};

		// dump
		virtual void dump(const char* options) const{return;}

	};

	inline IExportContext::IExportContext()
	{
	}
	inline IExportContext::~IExportContext()
	{
	}

	/*/
	struct IExportContext
	{
		IExportContext();
		virtual ~IExportContext();

	public:
		//! ����������
		//! ����� ���� ����� �������������� ������ � 
		//! getShadowDirectory getOcsDirectory � �.�.
		//! ������ ����������� ����������:
		//! WORKSPACEDIR - 
		//! 
		virtual const char* getEnvironment(
			const char* env
			)=0;
		// ����������
		virtual void setEnvironment(
			const char* env, 
			const char* value
			)=0;




		////////////////////////////////////////////////
		// ��� ��� ��������!!!
		// 
		typedef enum enShutterTiming
		{
			kShutterOpenOnFrame=0,
			kShutterCenterOnFrame,
			kShutterCloseOnFrame
		};
		virtual void OnNextFrame()
		{
		}

		//! ������� �����
		virtual int frame(
			){return 0;}
		//! FPS
		virtual float fps(
			){return 25.f;}
		//! ������� pass
		virtual const char* pass(
			){return "";}
		// ��� �������� ����
		virtual const char* layertype(
			){return "";}
		//! ������� ����
		virtual const char* layer(
			){return "";}
		//! MotionBlur
		virtual bool motionBlur(
			){return false;}
		//! ��������� �����
		virtual enShutterTiming shutterTiming(
			){return kShutterOpenOnFrame;}
		//! Shutter
		virtual void shutter( 
			float& open, 
			float& close,
			bool& subframeMotion
			){open=0; close=0; subframeMotion=false;}
		//! ������ true ���� ������ ��� �� �������������
		//! ������� ������ � ������ 
		virtual bool setStamp(int id);

		virtual bool IsIgnoreOverride(){return bIgnoreOverride;}

		// ����������
		virtual const char* getShadowDirectory(){return "";}
		virtual const char* getOutputDirectory(){return "";}
		virtual const char* getOcsDirectory(){return "";}
		virtual const char* getShaderDirectory(){return "";}

		// crop
		virtual void setCrop(int crop){};
		virtual int getCrop(){return 1;};

		// output file
		virtual void setOutputFile(const char* fn, bool bCrop=true){};
		virtual const char* getOutputFile(bool bCrop=true){return "";};

	// ���������� �� �����������:
	public:
		// ������� ���� �������� ��� �������
		virtual INode* getNodeForObject(
			MObject& obj
			){return NULL;};

		// ������� ��������� ��� �������� ���� ��������
		virtual IShadingNodeGenerator* getShadingNodeGenerator(
			MObject& obj
			){return NULL;};

		// ��������� ������, �������� � �������� ��������
		virtual cls::IRender* objectContext(
			MObject& obj,				// ����� ���� INode* ???
			IGenerator* generator
			)=0;

	// ���������� ������� �����:
	public:
		// �������������� ������ ����� ����������� 
		// �������� txmake
		enum enTaskType
		{
			PER_JOB		= 1,
			PER_FRAME	= 2,
		};
		virtual void addTask(
			enTaskType type,		// ���
			const char* title,		// ���������
			const char* cmd			// �������
			)=0;


	protected:
		// ������ ����������� ���
		std::set<int> processed;

		struct genDataKey
		{
			int id;	// object
			IGenerator* gen;
			bool operator<(const genDataKey& arg)const 
			{
				if(id<arg.id) return true;
				if(id>arg.id) return false;
				return (int)gen < (int)arg.gen;
			}
		};
//		std::map<genDataKey, void*> generatordatas;

	public:
		bool bIgnoreOverride;
	};
	inline IExportContext::IExportContext()
	{
		bIgnoreOverride = false;
	}
	inline IExportContext::~IExportContext()
	{
	}

	// ������ true ���� ����� ������ ��� ���������
	inline bool IExportContext::setStamp(int id)
	{
		if( processed.find(id)!=processed.end())
			return false;
		processed.insert(id);
		return true;
	}
/*/
}
