#pragma once
#include "IProcedural.h"
#include <map>
#include <list>
#include <vector>
#include <string>
#include "renderCacheImpl.h"

namespace cls
{
	//! @ingroup impl_group 
	//! \brief ���������� IProcedural ��� DeclareProcBegin � DeclareProcEnd
	//! 
	//! �������� ����� cache
	template< class STORAGE = CacheStorageSimple, class ATTRIBUTESTACK = TAttributeStack_minimal, class TRANSFORMSTACK = TTransformStack_pure>
	struct procedureCacheImpl : public IProcedural
	{
		procedureCacheImpl(
			)
		{
			cache.clear();
		}

		/// ����� ��� ����������� (Call)
		virtual void Process(
			IRender* render
			)
		{
			cache.Render(render);
		}
		/// ����� ��� ���������� (RenderCall)
		virtual void Render(
			IRender* render, 
			int motionBlurSamples, 
			float* motionBlurTimes
			)
		{
			cache.Render(render);
		}

	public:
		// ������ ��������� ��� ����������
		IRender* getRender()
		{
			return &cache;
		}

	protected:
		renderBaseCacheImpl<STORAGE, ATTRIBUTESTACK, TRANSFORMSTACK> cache;
//		renderCacheImpl<STORAGE> cache;
	};
}