#pragma once
#include "IRender.h"
#include "renderImpl.h"
#include <map>
#include <list>
#include <vector>
#include <string>
#include <set>
#include "MathNgl/MathNgl.h"
#include "Math/Edge.h"
#include "T/TTransformStack_minimal.h"
#include <maya/M3dView.h>

namespace cls
{
	//! @ingroup impl_group 
	//! \brief ���������� IRender ��� ��������� � GL �������
	//! 
	//! ��� ���������� � GL
	struct renderOpenglImpl : public renderImpl<TAttributeStack_minimal, TTransformStack_minimal>
	{
		M3dView* view;

		renderOpenglImpl(
			M3dView* view=NULL
			);
		// box
		Math::Box3f getBox(){return box;};


		// ��� �������
		virtual const char* renderType(
			){return "opengl";};

		//!@name Set a parameter of the next shader, primitive, camera, or output
		//@{ 


		//@{ 
		/// PushAttributes/PopAttributes
//		virtual void PushAttributes (void);
//		virtual void PopAttributes (void);
		virtual void SaveAttributes (const char *name, const char *attrs=NULL) {}
		virtual void RestoreAttributes(const char *name, const char *attrs=NULL) {}
		//@}

		//@{ 
		/// Transformations
		virtual void PushTransform (void);
		virtual void PopTransform (void);
		virtual void SetTransform ();
		virtual void AppendTransform ();
		//@}

		//@{ 
		/// Motion blur
		virtual void MotionBegin();
		virtual void MotionEnd();
		virtual void MotionPhaseBegin(float time);
		virtual void MotionPhaseEnd();
		//@}

		/// ����� ��������� ���������� ������� ������
		virtual void SystemCall(
			enSystemCall call
			);

		/// Procedural (���������������� �����)
		virtual void Call( 
			IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
			);
		virtual void Call( 
			const char*	procname		//!< ����� ������� IProcedural::Render()
			);

		/// Procedural (��������� �����)
		/// ��� ���������� ���������� ����� IProcedural::Render()
		/// ���� ����� ������ �������� � ����!!! (��� �����???)
		virtual void RenderCall( 
			IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
			);
		virtual void RenderCall( 
			const char*	procname		//!< ����� ������� IProcedural::Render()
			);

		/*/
		// ������
		virtual void Filter( 
			IFilter* filter					//!< ��� ��������� � DeclareProcBegin ��� dllname@procname
			);
		virtual void Filter( 
			const char*	procname			//!< ��� ��������� � DeclareProcBegin ��� dllname@procname
			);
		/*/

		public:
			bool bPoints;
			bool bShaded;
			bool bWireFrame;
			int degeneration;

		protected:
			Math::Box3f box;
			std::list<Math::Matrix4f> transforms;

		/// Geometry
		/// 0-D prims - point clouds
		void renderPoints (
			PA<Math::Vec3f>& P			//!< points
			);
		/// 1-D prims - lines, curves, hair
		void renderCurves (
			const char *interp, 
			const char *wrap,		//!< wrap
			PA<int>& nverts			//!< int[ncurves] ����� ��������� � ������
			);
		void renderCurves (int ncurves, int nvertspercurve, int order,
							const float *knot, float vmin, float vmax) {}
		/// 2-D prims - rectangular patches (NURBS, bicubics, bilinears), and
		/// indexed face meshes (polys, polyhedra, subdivs)
		void renderPatch();
		/// Mesh 
		void renderMesh(
			const char *interp,		//!< ��� ������������
			PA<int>& loops,			//!< loops per polygon
			PA<int>& nverts,		//!< ����� ��������� � �������� (size = polygoncount)
			PA<int>& verts			//!< ������� ��������� ��������� (size = facevertcount)
			);
		/// Sphere
		void renderSphere (float radius, float zmin, float zmax,
							float thetamax=360);

		void renderText( 
			const char* text, 
			Math::Vec3f pos
			);
	};
}
#include "inl/renderOpenglImpl.hpp"
