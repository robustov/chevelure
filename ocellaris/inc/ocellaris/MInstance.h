#pragma once

#ifdef OCELLARISEXP_EXPORTS
#define OCELLARISEXPORT_API __declspec(dllexport)
#else
#define OCELLARISEXPORT_API __declspec(dllimport)
#endif

#include "ocellaris/IRender.h"
#include "ocellaris\renderCacheImpl.h"
#include "ocellaris\IParameter.h"
#include "Math\box.h"
#include <string>
#include <map>

#pragma warning (disable:4251)

//#include <maya/M3dView.h>
class M3dView;

namespace cls
{
	enum enMInstanceMode
	{
		ODM_as_viewport = 0,
		ODM_wireframe = 1,
		ODM_points = 2,
		ODM_only_box = 3
	};
	//! @ingroup impl_group 
	//! \brief MInstance ������ �������� ������ ��� �������� � �����
	//! 
	//! ����� ����� ������ �� ocs �����. ����� �� ����
	//! 
	struct MInstance
	{
	protected:
		// ��������?
		bool bLoaded;
		Math::Box3f box;
		std::string filename;
		bool bDrawBox;

		// oc Cache
		cls::renderCacheImpl<cls::CacheStorageSimple> oc_cache;

//		bool bUseOcCache;
		// Render Cache
//		cls::renderOpenglCacheImpl gl_cache;

		// external attributes
		std::map<std::string, cls::Param> attributes;

	public:
		MInstance(
			);
		~MInstance(
			);
		// ��������� ������ �� filename � ���
		bool LoadForRender(
			const char* filename
			);
		/*/
		bool LoadMayaFile(
			const char* mayafile, 
			const char* mayaobject, 
			enMInstanceMode drawMode, 
			bool bDrawBox, 
			bool bUseOcCache
			);
		/*/
		OCELLARISEXPORT_API bool LoadOcs(
			const char* ocsfile, 
			const char* proxyocsfile, 
			enMInstanceMode drawMode, 
			bool bDrawBox, 
			bool readglobals, 
			bool bViewSubFiles,//=true
			bool brenderProxy
			);
		cls::IRender* asRender(
			);
		void clear(
			);
		void setLoaded(
			Math::Box3f box, 
			bool bDrawBox=true
			);

		bool isValid(
			)const;
		Math::Box3f getBox(
			)const;
		const char* getFile(
			)const;
		const std::map<std::string, cls::Param>* getAttributes(
			)const;
		const cls::renderCacheImpl<cls::CacheStorageSimple>& getCache(
			)const;

		OCELLARISEXPORT_API void UpdateAttrList(
			MObject thisNode, 
			bool bString = false);
		OCELLARISEXPORT_API void SetAttrToRender(
			MObject thisNode, 
			cls::IRender* render)const;
		bool Render(
			cls::IRender* render
			)const;
		OCELLARISEXPORT_API bool RenderToMaya(
			MObject thisNode,
			M3dView* view,
			bool bPoints=false, bool bWireframe=true, bool bShaded=false
			)const;
		OCELLARISEXPORT_API bool RenderToMaya(
			MObject thisNode,
			cls::IRender* render
			)const;
	public:
		MInstance& operator=(const MInstance& arg);
		MInstance(const MInstance& arg);

		template<class STREAM>
		void serialise(STREAM& out);

		void serializeOcs(const char* _name, cls::IRender* render, bool bSave);

	protected:
		std::string getFullFilename(const char* fn);
	};
}
#include "inl/MInstance.hpp"
