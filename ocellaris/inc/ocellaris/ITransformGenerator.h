#pragma once

#include "ocellaris/IGenerator.h"
#include "ocellaris/INode.h"
#include "ocellaris/IRender.h"

namespace cls
{
	//! @ingroup plugin_group
	//! \brief ��������� �������������
	//!
	//! ������� ����������
	//! ������ ������ ��������� ������������� �� ����� �������� ��� ������� bounding box
	//! 
	struct ITransformGenerator : public IGenerator
	{
		//! ��������� ����� render->PushTransform(); 
		//! ������� ����� �������
		virtual void OnTransformStart(
			cls::INode& node,					//!< ������
			cls::IRender* render,				//!< render
			const Math::Matrix4f& curtransform,	//!< ������� ���������
			Math::Matrix4f& newtransform,		//!< ����� ���������
			cls::IExportContext* context,		
			MObject& generatornode				//!< generator node �.� 0
			)=0;
		//! ��������� ����� { AppendTransform ��� SetTransform }
		virtual void OnTransform(
			cls::INode& node,					//!< ������
			cls::IRender* render,				//!< render
			cls::IExportContext* context,
			MObject& generatornode				//!< generator node �.� 0
			)=0;
		//! ��������� ����� render->PopAttributes()
		virtual void OnTransformEnd(
			cls::INode& node,					//!< ������
			cls::IRender* render,				//!< render
			cls::IExportContext* context,
			MObject& generatornode				//!< generator node �.� 0
			)=0;
	private:
		virtual enGenType GetType(
			){return TRANSFORM;};
	};
}