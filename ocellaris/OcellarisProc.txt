������ ����������� �������� (OcellarisProc)

----
Call "Dump" �������� ����� IRender->Dump

----
RenderCall "Box" ������ ������� �����
- box "#box" �����
- float #width ������ �����

RenderCall "SolidBox" ���
- box "#box" �����

----
Filter "HairFilter" - ������ ��� �����
matrix[] "HairFilter::transforms"	- ������� ������
float[] "HairFilter::parameters"	- �������� �������� ������������� �������
float "HairFilter::maxDot"			- ��� ���������������� ������
float "HairFilter::u"				- ����.�����.�����������
float "HairFilter::v"				- ����.�����.�����������
int "HairFilter::id"				- �� ������

----
RenderCall "ReadOCS" - ������ �����
string "filename"	- ��� �����
bool readglobals	- ������ �� ���������� ��������? (�� ��������� false)
� ����� ����� ����:
attribute string file::box - ����
attribute string file::ignoreBlur - ������������ ������� �����

�������� ������� ����� ���� ������� �� ������� �����:
box "file::box" - ����
bool "file::ignoreBlur" - �� ��������� ������� ����� ��� ������ �����

----
"ApplyVelocity"

----
"SimpleAnimation"
string "animation::parameter"			- ��� �������������� ���������
type[] PARAMETER						- �������� ���������
Attribute float[] "animation::phases"	- ������ ������ (�����)
float "animation::phase"				- ������� ����
int[] "animation::offset"				- �������� � ������� PARAMETER ��� ������ ����
