// shadingAsm.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "shading/ShadingGraphNodeImpl.h"
#include "shaderassembler.h"

int _tmain(int argc, _TCHAR* argv[])
{
	if(argc<2)
	{
		printf("no args\nformat: dllname@procname\nYou can set 2nd argument with filename for shader");
		return 0;
	}
	std::string outfilename = "";
	if(argc>=3)
	{
		outfilename = argv[2];
	}
	std::string dllNproc = argv[1];

	ShaderAssembler assembler;

	assembler.root.Init(dllNproc.c_str());
	if( !assembler.root.isValid())
	{
		printf("error: dll or entrypoint not found!!!");
		return 1;
	}

	std::string text = assembler.Build();
	puts(text.c_str());

	if( outfilename!="")
	{
		FILE* file = fopen(outfilename.c_str(), "wt");
		if(!file)
		{
			printf("cant open file %s", outfilename.c_str());
			return 1;
		}
		fputs(text.c_str(), file);
		fclose(file);

		ShadingGraphAssembler::Compile(outfilename.c_str());
	}
	// �� 
	return 0;
}

