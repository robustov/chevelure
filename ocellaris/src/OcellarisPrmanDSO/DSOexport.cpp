#include "stdafx.h"
#include <ri.h>
#include <string>

/**************************************************/
#ifdef WIN32
#define export __declspec(dllexport)
#else
#define export
#endif

#include "Util/misc_create_directory.h"

#include "ocellaris\renderStringImpl.h"
#include "ocellaris\renderPrmanImpl.h"
#include "ocellaris\renderFileImpl.h"
#include "ocellaris\renderCacheImpl.h"
#include "ocellaris\T\TTransformStack_minimal.h"

//#include "mathNpixar\IPrman_impl.h"

//Prman_Impl prman;

getIPrmanDll prmanproc;
IPrman* prman = NULL;

struct ocellarisDSO
{
	std::string parameters;
	float scenetime;
	float shutterOpen;
	float shutterClose;
	cls::renderCacheImpl<cls::CacheStorageSimple> cache;

//	getIPrmanDll prmanproc;
//	IPrman* prman;

	ocellarisDSO()
	{
	}
	/*/
	ocellarisDSO():
	#ifdef _DEBUG
		prmanproc("IPrmanDSOD.dll", "getIPrman")
	#else
		prmanproc("IPrmanDSO.dll", "getIPrman")
	#endif
	{
		prman = NULL;
		if( prmanproc.isValid())
			prman = (*prmanproc)();
	}
	/*/
};
extern "C" {
    export RtPointer ConvertParameters(RtString paramstr);
    export RtVoid Subdivide(RtPointer data, RtFloat detail);
    export RtVoid Free(RtPointer data);
}

RtPointer ConvertParameters(RtString paramstr) 
{
	if( !prman)
	{
		#ifdef _DEBUG
			prmanproc.Load("IPrmanDSOD.dll", "getIPrman");
		#else
			prmanproc.Load("IPrmanDSO.dll", "getIPrman");
		#endif
		if( prmanproc.isValid())
			prman = (*prmanproc)();
	}

//fprintf(stderr, "OcellarisPrmanDSO: %s\n", paramstr);

	ocellarisDSO* pData = new ocellarisDSO;

	std::string x = paramstr;
	char* a1 = strtok((char*)x.c_str(), "$");
	if( a1 == NULL )
		return NULL;
	pData->parameters = a1;
	pData->scenetime = 0;

	// scenetime
	char* a2 = strtok(NULL, "$");
	if( a2) 
	{
		pData->scenetime = (float)atof(a2);
		a2 = strtok(NULL, "$");
	}
	// shutterOpen
	pData->shutterOpen = pData->scenetime;
	if( a2) 
	{
		pData->shutterOpen = (float)atof(a2);
		a2 = strtok(NULL, "$");
	}
	// shutterClose
	pData->shutterClose = pData->shutterOpen;
	if( a2) 
	{
		pData->shutterClose = (float)atof(a2);
		a2 = strtok(NULL, "$");
	}
	// attributes
	if( a2) 
	{
		if( a2 && a2[0])
		{
//fprintf(stderr, "OcellarisPrmanDSO: %s\n", a2);
			cls::renderStringImpl fileimpl;
			if( !fileimpl.openForRead(a2))
			{
				fprintf(stderr, "OcellarisPrmanDSO cant read this: %s\n", a2);
				return NULL;
			}
			if( !fileimpl.Render(&pData->cache))
			{
				fprintf(stderr, "OcellarisPrmanDSO cant read this: %s\n", a2);
				return NULL;
			}
			a2 = strtok(NULL, "$");
		}
	}


	return static_cast<RtPointer>(pData);
}

std::map< std::string, cls::renderCacheImpl<cls::CacheStorageSimple> > files;

RtVoid Subdivide(RtPointer data, RtFloat detail) 
{
	if( !data) 
		return;
	ocellarisDSO* pData = (ocellarisDSO*)data;
	if( !prman)
		return;

	RtString str;
	RxInfoType_t restype;
	int rescount;
	prman->RxOption("user:pass_class", &str, sizeof(str), &restype, &rescount);
	if( rescount)
	{
	}
	prman->RiAttributeBegin();

	cls::renderPrmanImpl<cls::TTransformStack_minimal> thePrman(prman);


	cls::renderImpl<> implx;
	pData->cache.Render(&implx);
	Math::Matrix4f worldtransform;
	if( implx.GetAttribute("@dso::worldtransform", worldtransform))
	{
#ifndef SAS
		printf("i have worldtransform\n");
#endif
		thePrman.clear(worldtransform);
	}
	else
	{
#ifndef SAS
		printf("i dont have worldtransform\n");
#endif
		thePrman.clear();
	}

	thePrman.setBlurParams(pData->scenetime, pData->shutterOpen, pData->shutterClose);
	cls::IRender* render = &thePrman;

	render->PushAttributes();
	{
		pData->cache.Render(render);
		render->Parameter("filename", pData->parameters.c_str());
		render->Parameter("readglobals", true);

		render->RenderCall("ReadOCS");
	}
	render->PopAttributes();

	prman->RiAttributeEnd();

//	cache->Render(render);

//	render->Attribute( "blur::scenetime", pData->scenetime);
//	bool bMotionBlur = (pData->shutterOpen!=pData->shutterClose);
//	render->Attribute( "blur::motionBlur", bMotionBlur);
//	render->Attribute( "blur::shutterOpen", pData->shutterOpen);
//	render->Attribute( "blur::shutterClose", pData->shutterClose);

	/*/
	cls::renderCacheImpl<cls::CacheStorageSimple>* cache = NULL;
	std::map< std::string, cls::renderCacheImpl<cls::CacheStorageSimple> >::iterator it = files.find(pData->parameters);
	if( it != files.end())
	{
		cache = &it->second;
	}
	else
	{
		cls::renderFileImpl fileimpl;
		if( !fileimpl.openForRead(pData->parameters.c_str()))
		{
			fprintf(stderr, "Some error during opening %s\n", pData->parameters.c_str());
			return;
		}
		cls::renderCacheImpl<cls::CacheStorageSimple>& xcache = files[pData->parameters];
		fileimpl.Render(&xcache);
		cache = &xcache;
	}
	/*/
}

RtVoid Free(RtPointer data) 
{
	if( !data) return;
	ocellarisDSO* pData = (ocellarisDSO*)data;
	delete pData;
}

HMODULE HairDSO = 0;
HMODULE OcellarisDSO = 0;

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
//	fprintf(stdout, "Fur: DllMain %d\n", ul_reason_for_call);
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		{
			// ��� �������� �����
			char filename[512]; 
			GetModuleFileName( (HMODULE)hModule, filename, 512); 
			fprintf(stderr, "load %s\n", filename);
			fflush(stderr);

#ifdef SAS
			{
				std::string path = filename;
				path = Util::extract_foldername(path.c_str());
				char buf[10000];
				_snprintf(buf, 10000, "PATH=%s;%s", path.c_str(), getenv("PATH"));
				_putenv(buf);
				printf("PATH+=%s\n", path.c_str());

				OcellarisDSO = ::LoadLibrary("ocellarisProc.dll");
				OcellarisDSO = ::LoadLibrary("chevelureDSO.dll");
			}
#endif
#ifndef SAS
			{
				OcellarisDSO = ::LoadLibrary("ocellarisProc.dll");
//				HairDSO = ::LoadLibrary("HairDSO.dll");
	//			thePrman.clear();
			}
#endif
		}
		break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		{
			// ��� �������� �����
			char filename[512]; 
			GetModuleFileName( (HMODULE)hModule, filename, 512); 
			fprintf(stderr, "unload %s\n", filename);
			fflush(stderr);
		}
		{
			if( HairDSO)
				::FreeLibrary(HairDSO);
			if( OcellarisDSO)
				::FreeLibrary(OcellarisDSO);
		}
		break;
	}
    return TRUE;
}
