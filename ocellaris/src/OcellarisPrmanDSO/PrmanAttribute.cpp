#include "stdafx.h"
#include "ocellaris/IProcedural.h"
#include "ocellaris/IRender.h"
#include "ocellaris\renderPrmanImpl.h"

struct PrmanAttributeProc : public cls::IProcedural
{
	// секция рендеринга
	virtual void Render(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		);
};
PrmanAttributeProc prmanattributeproc;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IProcedural* __cdecl PrmanAttribute(cls::IRender* prman)
	{
		return &prmanattributeproc;
	}
}

// секция рендеринга
void PrmanAttributeProc::Render(
	cls::IRender* render, 
	int motionBlurSamples, 
	float* motionBlurTimes
	)
{
	if( strcmp(render->renderType(), "prman")!=0)
		return;

	cls::PS attribute = render->GetParameter("@attribute");
	cls::Param value      = render->GetParameter("@value");

	cls::renderPrmanImpl<cls::TTransformStack_minimal>* prman = (cls::renderPrmanImpl<cls::TTransformStack_minimal>*)render;
//	prman->prman->RiAttribute();
}
