// OCELLARIS SHADING NETWORK ASSEMBLER
//
#include "bake3dNormal.h"
#include "LambertOcs.h"

surface bake3dNormal()
{
void bake3dNormalRSL( output color result)
{
	normal n;
bake3dNormal("M:/server2_mitka/develop-ocellaris/rmantex/normal.0001.ptc", n);
result = color(xcomp(n)*0.5+0.5, ycomp(n)*0.5+0.5, zcomp(n)*0.5+0.5);

}
void bake3dNormal( color SurfaceColor; color SurfaceOpacity; color ambientColor; color incandescence; float diffuse; float translucence; float translucenceDepth; float translucenceFocus)
{
		LambertOcs(SurfaceColor, SurfaceOpacity, ambientColor, incandescence, diffuse, translucence, translucenceDepth, translucenceFocus, Oi, Ci);
}
// bake3dNormalRSL
color bake3dNormalRSL_result;
bake3dNormalRSL(bake3dNormalRSL_result);
Ci = bake3dNormalRSL_result;
Oi = color(0.1, 0.1, 0.1);
}
