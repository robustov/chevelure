#ifndef LambertOcs_h
#define LambertOcs_h

void
LambertOcs(
	color SurfaceColor; 
	color SurfaceOpacity; 
	color ambientColor;
	color incandescence; 
	float diffuse;
	float translucence;
	float translucenceDepth;
	float translucenceFocus;
	output color Oi;
	output color Ci;
	)
{
	vector V;														
	color Ia, Id, Itr;												
	color opacity = SurfaceOpacity;								
	normal Nf = faceforward( N, I, N );									
	Nf = normalize(Nf);
	V = -normalize(I);												
	Ia = ambientColor;												
	Id = 0;															
	Itr = 0;														
	illuminance(P, Nf, PI/2)										
	{																
		float dot = normalize(L).Nf;								
		if(dot>0) 
			Itr += Cl*dot;									
	}																
	Itr *= diffuse;													
	Oi = opacity;													
	Ci = opacity * ( SurfaceColor * (Ia + Id + Itr) + incandescence);
}

#endif
