#ifndef OcclusionOcs_h
#define OcclusionOcs_h

#include "ul_pxslRayUtil.h"

void
OcclusionOcs(
	uniform float Samples;
	uniform float MaxDistance;
	uniform float MaxVariation;
	output color result;
	)
{

  	void
  	pxslShadingNormal(varying vector incident;
  			  varying normal n;
  			  output normal ns;)
  	{
  	    uniform float depth;
  	    rayinfo("depth", depth);
  	    if(depth > 0)
  		ns = faceforward(n, incident, n);
  	    else
  	    {
  		uniform float sides = 2;
  		attribute("Sides", sides);
  		if(sides == 2)
  		    ns = faceforward(n, incident, n);
  		else
  		    ns = n;
  	    }
  	}
      
  
  		void pxslCurrentPoint (
  		    uniform float frequency;
  		    output varying point Q; 
  		    output varying vector duQ;
  		    output varying vector dvQ;
  		) 
  		{
  		    extern point P, Ps;
  		    extern float du, dv;
  #if SLIM_SHADERTYPEID == SLIM_TYPEID_light
  		    Q = frequency * Ps;
  #else
  		    Q = frequency * P;
  #endif
  		    duQ = Du(Q)*du;
  		    dvQ = Dv(Q)*dv;
  		}
  	    
  
  	void
  	pxslIncidentDirection(uniform float normalize;
  			    uniform float flip;
  			    output vector V;)
  	{
  	    extern vector I;
  	    if(normalize == 0)
  		V = I;
  	    else
  		V = normalize(I);
  	    if(flip != 0)
  		V = -V;
  	}
      
  
  	void
  	pxslSurfaceNormal(uniform float normalize;
  			output normal n;)
  	{
  	    extern normal N;
  	    if(normalize == 0)
  		n = N;
  	    else
  		n = normalize(N);
  	}

  	    void
  	    pxslOcclusion(
  		uniform float invert;
  		uniform float samples;
  		uniform string subset;
  		uniform float maxdist;
  		uniform float maxvar;
  		point Q;
  		vector dQu;
  		vector dQv;
  		normal n;
  		output float result;
  	    )
  	    {
  		uniform float xsamples = ul_pxslGetRaySamples(samples);
  		if( xsamples > 0 )
  		{
  		    result = occlusion(Q, n, xsamples,
  				"maxdist", maxdist,
  				"maxvariation", maxvar,
  				"subset", subset
  				);
  		}
  		else
  		    result = 0;
  		if(invert != 0)
  		    result = 1 - result;
  	    }

  normal tmp1;
  normal tmp5;
  float tmp6;
  color tmp7;
  vector tmp0;
  point tmp2;
  color tmp9;
  color tmp8;
  vector tmp3;
  vector tmp4;

  pxslIncidentDirection ( /* IncidentDirection */
    1, /* Normalize */
    0, /* Flip */
    tmp0  /* result */ 
    );
  pxslSurfaceNormal ( /* SurfaceNormal */
    1, /* Normalize */
    tmp1  /* result */ 
    );
  pxslCurrentPoint ( /* CurrentPoint */
    1, /* Frequency */
    tmp2, /* Q */ 
    tmp3, /* dQu */ 
    tmp4  /* dQv */ 
    );
  pxslShadingNormal ( /* ShadingNormal */
    tmp0, /* Forward */ 
    tmp1, /* Normal */ 
    tmp5  /* result */ 
    );

  pxslOcclusion ( /* Occlusion */
    1, /* Invert */
    Samples, /* Samples */
    "", /* Trace Subset */
    MaxDistance, /* Max Distance */
    MaxVariation, /* Max Variation */
    tmp2, /* Q */ 
    tmp3, /* dQu */ 
    tmp4, /* dQv */ 
    tmp5, /* normal */ 
    tmp6  /* result */ 
    );


result = color(tmp6, tmp6, tmp6);
}