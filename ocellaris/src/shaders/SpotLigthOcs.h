#ifndef SpotLigthOcs_h
#define SpotLigthOcs_h

void SpotLigthShadowMapOcs(
	string filename;
	float samples;
	float blur;
	float bias;
	output color result; 
	)
{
	extern point Ps;
	if(filename == "")
	{
		result = 0;
	}
	else
	{
		if(bias != 0)
		{
			result = color shadow(filename, Ps,
				"samples", samples,
				"blur", blur,
				"bias", bias);
		}
		else
		{
			result = color shadow(filename, Ps,
				"samples", samples,
				"blur", blur);
		}
	}
}

void SpotLigthOcs(
//    string  pixar_PhotonMapCaustic_PhotonMap = "";
    uniform float   ConeAngle;
    uniform float   PenumbraAngle;
	color   Color;
	float kL;
	output varying color   outColor;
//	output varying float   __nondiffuse;
//	output varying float   __nonspecular;
//	output varying color   _shadow;
//	output varying color   _cl_noshadow;
//	output varying float   _contribRim;
//	output varying float   _contribThinTranslucence;
//	output varying float   _contribBackScattering;
//	output varying float   _contribSubsurfaceScattering;
	)
{

	point from = point "shader"(0,0,0);
	vector axis = normalize(vector "shader"(0,0,1));
	uniform float angle = radians(ConeAngle)*0.5;
	uniform float penumbra = -radians(PenumbraAngle);
	if(penumbra<0)
	{
		penumbra = -penumbra;
		angle = angle+penumbra;
		
	}
	uniform float cosoutside = cos(angle);
	uniform float cosinside = cos(angle-penumbra);

	illuminate(from, axis, angle) 
	{
		varying float atten, cosangle;
		cosangle = L.axis / length(L);
		atten = smoothstep( cosoutside, cosinside, cosangle );
		atten *= 1 / pow(length(L), 0);
//		__nondiffuse = 1 - 1;
//		__nonspecular = 1 - 1;
//		_contribRim = 1;
//		_contribThinTranslucence = 1;
//		_contribBackScattering = 1;
//		_contribSubsurfaceScattering = 1;
		outColor = atten * kL * Color;
//		_cl_noshadow = Cl;
//		outColor = pxslCmix(Cl, color (0,0,0), _shadow);
	}
}

#endif
