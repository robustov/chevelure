#ifndef PhongOcs_h
#define PhongOcs_h

void
PhongOcs(
	color SurfaceColor; 
	color SurfaceOpacity; 
	color ambientColor;
	color incandescence; 
	float diffuse;
	float translucence;
	float translucenceDepth;
	float translucenceFocus;
	float roughness; 
	float highlightSize;
	color whiteness;
	color specularColor;
	output color Oi;
	output color Ci;
	)
{
	vector V;
	color Ia, Id, Itr, Ispec;
	color opacity = 1 - SurfaceOpacity;
	normal Nf = faceforward( N, I, N );
	Nf = normalize(Nf);
	V = -normalize(I);
	vector R = reflect( -normalize(V), normalize(N) );

	Ia = ambientColor;												
	Id = 0;
	Itr = 0;
	Ispec = 0;
	illuminance(P, Nf, PI)										
	{	
		vector Ln = normalize(L);
		// diffuse
		float dot = Ln.Nf;								
		if(dot>0) 
			Itr += Cl*dot;

		Ispec += Cl * pow(max(0.0,R.Ln), 1.0/roughness);

		// specular
		//
		//vector H = normalize(Ln + V);
		// whiteness???
		//Ispec += pow( (H.Nf)*highlightSize, 1.0/roughness) * Cl;
	}																
	Itr *= diffuse;													
	Oi = opacity;													
	Ci = opacity * ( SurfaceColor * (Ia + Id + Itr) + specularColor*Ispec + incandescence);
}

#endif
