#define ENVPARAMS \
        envname, envspace, envrad

#define DECLARE_ENVPARAMS                           \
        string envname, envspace;                   \
         float envrad

#define DECLARE_DEFAULTED_ENVPARAMS                                 \
        string envname = "", envspace = "world";                    \
        float envrad = 100

#define PAR_PLAST   Ktex, Kd, Ktd, Ka, Ksp, roughness, Kr, SpecSharpness, ior, blur, envrad , \
                                            basecolor, ambcolor, speccolor,  Mask , \
                                           plastOn, texname, glossyOn, envname, envspace


#define DECLARE_PLAST  float Ktex, Kd, Ktd, Ka, Ksp, roughness, Kr, SpecSharpness, ior, blur;  uniform float envrad ; \
                                          color  basecolor, ambcolor, speccolor; varying color Mask ;  \
                                          string plastOn, texname, glossyOn, envname, envspace

#define DEFAULT_DECLARE_PLAST  float Ktex = 0, Kd =1, Ktd=1, Ka=1, Ksp=1, roughness=0.2, Kr=1, SpecSharpness=1, ior=1, blur=0.05;  uniform float envrad =100 ; \
                                          color  basecolor =1, ambcolor =1, speccolor =1; varying color Mask =1 ;  \
                                          string plastOn="", texname="", glossyOn="", envname = "", envspace="world"


/* raysphere - calculate the intersection of ray (E,I) with a sphere
 * centered at the origin and with radius r.  We return the number of
 * intersections found (0, 1, or 2), and place the distances to the
 * intersections in t0, t1 (always with t0 <= t1).  Ignore any hits
 * closer than eps.
 */
float
raysphere (point E; vector I;   /* Origin and unit direction of the ray */
           float r;             /* radius of sphere */
	   float eps;           /* epsilon - ignore closer hits */
	   output float t0, t1; /* distances to intersection */
    )
{
    /* Set up a quadratic equation -- note that a==1 if I is normalized */
    float b = 2 * ((vector E) . I);
    float c = ((vector E) . (vector E)) - r*r;
    float discrim = b*b - 4*c;
    float solutions;
    if (discrim > 0) {           /* Two solutions */
	discrim = sqrt(discrim);
	t0 = (-discrim - b) / 2;
	if (t0 > eps) {
	    t1 = (discrim - b) / 2;
	    solutions = 2;
	} else {
	    t0 = (discrim - b) / 2;
	    solutions = (t0 > eps) ? 1 : 0;
	}
    } else if (discrim == 0) {  /* One solution on the edge! */
	t0 = -b/2;
	solutions = (t0 > eps) ? 1 : 0;
    } else {                    /* Imaginary solution -> no intersection */
	solutions = 0;
    }
    return solutions;
}

/* Environment() - A replacement for ordinary environment() lookups, this
 * function ray traces against an environment sphere of known, finite
 * radius.  Inputs are:
 *    envname - filename of environment map
 *    envspace - name of space environment map was made in
 *    envrad - approximate supposed radius of environment sphere
 *    P, R - position and direction of traced ray
 *    blur - amount of additional blur to add to environment map
 * Outputs are:
 *    return value - the color of incoming environment light
 *    alpha - opacity of environment map lookup in the direction R.
 * Warning -  the environment call itself takes derivatives, causing
 * trouble if called inside a loop or varying conditional!  Be cautious.
 */
color Environment ( DECLARE_ENVPARAMS ;
                    point P;  vector R;  float blur; output float alpha;)
{
    /* Transform to the space of the environment map */
    point Psp = transform (envspace, P);
    vector Rsp = normalize (vtransform (envspace, R));
    float r2 = envrad * envrad;
    /* Clamp the position to be *inside* the environment sphere */
    if ((vector Psp).(vector Psp) > r2)
        Psp = point (envrad * normalize (vector Psp));
    float t0, t1;
    if (raysphere (Psp, Rsp, envrad, 1.0e-4, t0, t1) > 0)
	Rsp = vector (Psp + t0 * Rsp);
    alpha = float environment (envname[3], Rsp, "blur", blur, "fill", 1);
    return color environment (envname, Rsp, "blur", blur);
}



         color ReflMap(string reflname; point P; float blur; output float alpha;)
{
point Pndc = transform("NDC", P);
float x = xcomp(Pndc), y = ycomp(Pndc);
alpha = float texture(reflname[3], x, y, "blur", blur, "fill", 1);
return color texture(reflname, x, y, "blur", blur);
}




         color SampleEnvironmet (point P; vector R; float Kr, blur; DECLARE_ENVPARAMS; )
{
color Ce = 0;
float alpha;
 if(envname != "") {
       if(envspace == "NDC")
             Ce = ReflMap(envname, P, blur, alpha);
      else Ce = Environment(ENVPARAMS, P, R, blur, alpha );
                              }
return Kr*Ce;

}

/* pixar LocIllumGlossy */
         color LocIllumGlossy ( normal N;  vector V;  float roughness, sharpness )
{
    color C = 0;
    float w = .18 * (1-sharpness);
    extern point P;
    illuminance ("-environment", P, N, PI/2) {
	/* Must declare extern L & Cl because we're in a function */
	extern vector L;  extern color Cl;
	float nonspec = 0;
	lightsource ("__nonspecular", nonspec);
	if (nonspec < 1) {
	    vector H = normalize(normalize(L)+V);
	    C += Cl * ((1-nonspec) *
		       smoothstep (.72-w, .72+w,
				   pow(max(0,N.H), 1/roughness)));
	}
    }

    return C;
}

float linestep(float min, max, x)
{
	float f;
	if (x < min) f = 0;
	if (x >= max) f = 1;
	f = (x-min)/(max-min);
	return f;
}
/* */

color
LocIllumOrenNayar (normal N;  vector V;  float ONroughness;)
{
    /* Surface roughness coefficients for Oren/Nayar's formula */
    float sigma2 = ONroughness * ONroughness;
    float A = 1 - 0.5 * sigma2 / (sigma2 + 0.33);
    float B = 0.45 * sigma2 / (sigma2 + 0.09);
    /* Useful precomputed quantities */
    float  theta_r = acos (V . N);        /* Angle between V and N */
    vector V_perp_N = normalize(V-N*(V.N)); /* Part of V perpendicular to N */

    /* Accumulate incoming radiance from lights in C */
    color  C = 0;
    extern point P;
    illuminance ("-environment", P, N, PI/2) {
	/* Must declare extern L & Cl because we're in a function */
	extern vector L;  extern color Cl;
	float nondiff = 0;
	lightsource ("__nondiffuse", nondiff);
	if (nondiff < 1) {
	    vector LN = normalize(L);
	    float cos_theta_i = LN . N;
	    float cos_phi_diff = V_perp_N . normalize(LN - N*cos_theta_i);
	    float theta_i = acos (cos_theta_i);
	    float alpha = max (theta_i, theta_r);
	    float beta = min (theta_i, theta_r);
	    C += (1-nondiff) * Cl * cos_theta_i *
		(A + B * max(0,cos_phi_diff) * sin(alpha) * tan(beta));
	}
    }
    return C;
}



/* Diffuse Ambient components */
void
ambientDiffuseFunc(normal Ns;
                         normal N;
                         vector V;
                     float Ka;
                     color ambC;
                     float Kd;
                     color diffuseColor;
                     float roughnessOrenN;
                     float enableA;
                     float enableD;
					uniform float lightTypeD;
					output color pDiffColor;
					output color pDiffDirect;
					output color pDiffDirectShadow;
					output color pOcclusionDirect;
					output color pDiffIndirect;
					output color pDiffEnvironment;
					output color pOcclusionIndirect;
					output color ambD;
					output color D;
					output color pShadowPass;)
{
extern point P;
color inshadow, Cl_noshd, Cl_diff;
pShadowPass = 0;

		    // illumination from direct lights
		    // here we:
		    // - add to our total diffuse contribution
		    // - tally direct occlusion (shadow)
		    // - tally (unoccluded) direct illumination

	if (lightTypeD <= 0)
	{
	illuminance("-environment", P, Ns, 1.57,  "lightcache", "reuse")
		{
			    float nondiff = 0;
			    lightsource("__nondiffuse", nondiff);
			    if (nondiff < 1)
			   {
			   float k = (1-nondiff) * Ns.normalize(L);
				if( 0 != lightsource("_shadow", inshadow) )
				{
				    pOcclusionDirect += inshadow;
					pShadowPass += inshadow;
				}
				if( 0 == lightsource("_cl_noshadow", Cl_noshd) )
				    Cl_noshd = Cl;
				Cl_diff = Cl_noshd - Cl;
				pDiffDirect += k * Cl_noshd;
				pDiffDirectShadow += k * Cl_diff;
				color pDiffEnvironment = 0;

			    }
		}
	}
		    // illumination from environments
		    // this will include both environment and indirect illum
		    if (lightTypeD >= 0)
		    {
			float nondiff = 0;
			float occl = 0;
			color indiff = 0;
			color envCol = 0;
			illuminance ("environment", P, Ns, 1.57, "lightcache", "refresh", "send:light:__surfacearea", -10,
			    "send:light:__coneangle", 1.57,
			    "send:light:__coneaxis", Ns,
			    "light:__nondiffuse", nondiff,
			    "light:_indirectdiffuse", indiff,
			    "light:_occlusion", occl
			    )
			{
			    if (nondiff < 1)
			    {
				envCol = (1-nondiff) * Cl;
				pDiffIndirect += indiff - envCol;
				// subtract out indirect component of env
				pDiffEnvironment += envCol;
				// accumulate occlusion
				pOcclusionIndirect += max(color(occl), pOcclusionIndirect);
			    }
			}
		    }
		    color kd = Kd * diffuseColor;
		    pDiffDirect *= kd;
		    pDiffDirectShadow *= kd;
		    pDiffIndirect *= kd;
		    pDiffEnvironment *= kd;



color Amb = Ka * ambient() * ambC;
D = 0;


 if (roughnessOrenN > 0)
 	{
 		if (lightTypeD <= 0)
 		{
 			D = kd * LocIllumOrenNayar (N, V, roughnessOrenN);
			D += pDiffIndirect + pDiffEnvironment;
		}
 	}
 else
 	{
		D = pDiffDirect;
		D += pDiffIndirect + pDiffEnvironment - pDiffDirectShadow;
	}



ambD =1;
ambD = Amb + D - Amb * D;

if((enableA * Ka) == 0)
 {
 ambD = D;
 }
if ((enableD * Kd) == 0)
 {
 ambD = Amb;
 }
if(((enableD * Kd) == 0)&&((enableA * Ka) == 0))
 {
 ambD =1;
 }
}


/*SpecularComponent*/
void
specularFunc(	normal Nf;
             	vector V;
            	float Ksp;
            	float roughnessSpec;
            	color specularColor;
				float useBlinn;
				uniform float lightTypeS;
				output color pSpecularColor;
				output color pOcclusionDirect;
				output color pSpecularDirect;
				output color pSpecularDirectShadow;
				output color pSpecularEnvironment;
				output color specSum;
                  )
{

//Blinn variables
    float E;
    vector H, Ln, Nn;
    float NH, NH2, NHSQ, Dd, Gg, VN, VH, LN, Ff, tmp;
    float nondiff, nonspec;
    color ks;
	color k;
    color inshadow, Cl_noshd, Cl_diff;
    extern vector L, I;
    extern color Cl;
	extern point P;


		    // direct illumination from lights
		    // here we:
		    // - add to our total specular contribution
		    // - tally direct occlusion (shadow)
		    // - tally (unoccluded) direct illumination
		    if (lightTypeS <= 0)
		    {
				if (useBlinn ==1)
				{
					if(roughnessSpec != 1)
						E = 1 / (roughnessSpec * roughnessSpec - 1);
			   		else
						E = -1e5;
			    	VN = V.Nf;
			    	illuminance("-environment", P, Nf, 1.57079632679)
			    	{
						if( 0 == lightsource("__nonspecular", nonspec) )
				    		nonspec = 0;
						if( nonspec < 1 )
							{
						    Ln = normalize(L);
						    H = normalize(Ln+V);
						    NH = Nf.H;
						    NHSQ = NH*NH;
						    NH2 = NH * 2;
						    Dd = (E+1) / (NHSQ + E);
						    Dd *= Dd;
						    VH = V.H;
						    LN = Ln.Nf;
						    if( VN < LN )
						    {
							if( VN * NH2 < VH )
							    Gg = NH2 / VH;
							else
							    Gg = 1 / VN;
						    }
				    	else
				    		{
						if( LN * NH2 < VH )
						    Gg = (LN * NH2) / (VH * VN);
						else
						    Gg = 1 / VN;
					    }
				    /* poor man's Fresnel */
				    tmp = pow((1 - VH), 3);
				    Ff = tmp + (1 - tmp) * Ksp;
				    k = Dd * Gg * Ff * specularColor;

				    if( 0 == lightsource("_cl_noshadow", Cl_noshd) )
					Cl_noshd = Cl;
				    Cl_diff = Cl_noshd - Cl;
				    pSpecularDirect += k * Cl_noshd;
				    pSpecularDirectShadow += k * Cl_diff;
				}
			    }
				}
			else {
			illuminance("-environment", P, Nf, 1.57,
			    "lightcache", "reuse")
			{
			    float nonspec = 0;
			    color inshadow = 0, Cl_noshd = 0, Cl_diff;
			    lightsource("__nonspecular", nonspec);
			    if (nonspec < 1)
			    {
					k = (1-nonspec) *
				    specularbrdf(normalize(L), Nf, V, roughnessSpec);
				if( 0 != lightsource("_shadow", inshadow) )
				    pOcclusionDirect += inshadow;
				if( 0 == lightsource("_cl_noshadow", Cl_noshd) )
				    Cl_noshd = Cl;
				Cl_diff = Cl_noshd - Cl;
				pSpecularDirect += k * Cl_noshd;
				pSpecularDirectShadow += k * Cl_diff;
			    }
				}
			}
		    }

		    // illumination from environments
		    // unlike diffuse, this (currently) does not contain any
		    // object-based calculations
		    if (lightTypeS >= 0)
		    {
			float nonspec = 0;
			color envCol = 0;
			float cone = 1.55 * pow(roughnessSpec,2);
			illuminance ("environment", P, Nf, 1.57,
			    "lightcache", "refresh",
			    "send:light:__surfacearea", -10,
			    "send:light:__coneangle", cone,
			    "send:light:__coneaxis", reflect(-V, Nf),
			    "light:__nonspecular", nonspec
			    )
			{
			    if (nonspec < 1)
			    {
				pSpecularEnvironment += (1-nonspec) * Cl;
			    }
			}
		    }

		    // scale by Ks
		    ks = specularColor * Ksp;
		    pSpecularDirect *= ks;
		    pSpecularDirectShadow *= ks;
		    pSpecularEnvironment *= ks;

		    specSum = pSpecularDirect - pSpecularDirectShadow + pSpecularEnvironment;
		    pSpecularColor = specularColor;

}



/*RimComponent*/
color rimFunc(normal Ns;
           vector V;
           float Krim;
           color colorationRim;
           float edginessRim;
           float sharpnessRim;
           uniform float lightType;
           )
{
extern point P;
color Rim = 0;


// calculate edginess
float cosine = clamp( Ns.V, 0, 1 );
float sine = sqrt(1 - cosine*cosine);
float edge = pow(sine, edginessRim*10);

// run through smoothstep
float w = .5 * (1-sharpnessRim);
float ss = smoothstep(.5-w, .5+w, edge);
		    edge *= ss;

		    if (lightType <= 0)
		    {
			illuminance("-environment", P, Ns, 1.57,
			    "lightcache", "reuse")
			{
			    float krim = 1;
			    lightsource("_contribRim", krim);
			    if (krim > 0)
			    {
				Rim += Cl * krim * normalize(L).Ns;
			    }


			}
		    }
		    if (lightType >= 0)
		    {
			/* this is diffuse-y */
			illuminance ("environment", P, Ns, 1.57,
			    "send:light:__surfacearea", -10,
			    "send:light:__coneangle", 1.57,
			    "send:light:__coneaxis", Ns,
			    "lightcache", "refresh")
			{
			    float krim = 1;
			    lightsource("_contribRim", krim);
			    if (krim > 0)
			    {
				Rim += krim * Cl;
			    }
			}
		    }
		    Rim *= edge * Krim * colorationRim;

		   return Rim;
		}

color
translucenceFunc( normal Ns; float c; )
{
    color C = 0;
    extern point P;
    extern color Cl;
    float nondiff;

    if( c != 0 )
    {
	illuminance(P, Ns, PI)
	{
	    if( 0 == lightsource("__nondiffuse", nondiff) )
		nondiff = 0;
	    C += (1 - nondiff) * Cl;
	}
	C *= c;
    }
    return C;
}






