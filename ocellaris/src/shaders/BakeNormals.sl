#include "pxslRayUtil.h"

surface OcclOccelaris(
	float lightwarp = 1; 
	string envmap = ""; 
	string envspace = ""; 
	float maxpixdist = 100;
	string distr = "cosin"; 
	float bias = -1; 
	string hitmode = "default"; 
	float falloffmode = 0; 
	float falloff = 0;
	float samples = 64; 
	string subset = ""; 
	float maxdist = 50, coneangle = 75; 
	uniform string sides = "front";
	float maxvar = 0.15;)
{
		float invert = 1;
		extern normal N;
		extern point P;
		float result = 0;
		float angle = radians(coneangle);
		uniform float xsamples = pxslGetRaySamples(samples);
		if( xsamples > 0)
		{
		    result = occlusion(P, N, xsamples, "coneangle", angle, "hitsides", sides, "brightnesswarp", lightwarp, "bias", bias,
		    "environmentmap", envmap, "environmentspace", envspace, "maxpixeldist", maxpixdist, "distribution", distr, "hitmode", hitmode,
		    "falloff", falloff, "falloffmode", falloffmode, "maxdist", maxdist,	"maxvariation", maxvar,"subset", subset);
		}
		else result = 0;
		if(invert != 0) result = 1 - result;
		Ci = result*color(1);
		Oi = color(1);
}