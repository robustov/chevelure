#ifndef bake3dNormal_h
#define bake3dNormal_h

void
bake3dNormal(
			string PtcFile; 
			output normal n;
			)
{
	normal Nn = normalize(N);
//	normal Nf = faceforward( Nn, I, Nn );
	normal Nf = Nn;
	color c = color(xcomp(Nf)*0.5+0.5, ycomp(Nf)*0.5+0.5, zcomp(Nf)*0.5+0.5);

	uniform float interpolate = 1;
	uniform float radius = 1; //
	Nn = 0;
	bake3d( PtcFile, "_color", P, Nn, "radius", radius, "interpolate", interpolate, "_color", c);
	n = Nf;
}

#endif
