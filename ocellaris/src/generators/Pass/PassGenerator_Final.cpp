#include "stdafx.h"
#include "ocellaris/OcellarisExport.h"
#include "ocellaris/renderFileImpl.h"
#include "ocellaris/ILightGenerator.h"
#include "MathNMaya/MathNMaya.h"

#include "PassGenerator_Final.h"

#include "ocellaris\renderStringImpl.h"
#include "ocellaris/renderPrmanImpl.h"
#include "Util/misc_create_directory.h"
#include <string>
#include <time.h>
#include "mathNpixar/IPrman.h"
#include "mathNpixar/IPrman_ascii.h"


PassGenerator_Final passgenerator_final;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl PassFinal()
	{
		return &passgenerator_final;
	}
}

// ����������� ��������� (���������� 1 ��� ����� ������� ��������)
bool PassGenerator_Final::OnPrepare(
	cls::IExportContext* context
	)
{
	setforpass.clear();
	return true;
}

//! ����������� ���� � �������� 
bool PassGenerator_Final::OnPrepareNode(
	cls::INode& _node, 
	cls::IExportContext* context,
	MObject& generatornode
	)
{
	MStatus stat;
	// set
	MObject node = _node.getObject();
	cls::PS setnode = ocExport_GetAttrValue(node, "set");
	if( !setnode.empty() && setnode.data()[0])
	{
		MObject setobj;
		if( nodeFromName(setnode.data(), setobj))
		{
			MFnSet setfn(setobj, &stat);
			if( stat)
			{
				// ���������!
				setforpass[&_node] = setobj;
			}
		}
	}
	// ������!!!
	MObject camnode = FindCamera(node, context);
	context->addNode(camnode);
	return true;
}

// ���������� ����� ��������� ������ ����
bool PassGenerator_Final::OnExportNode(
	cls::INode& node,				//!< ������ ������� ���� �� �������
	cls::IRender* render,			//!< render
	cls::IExportContext* context,	//!< context
	cls::INode& passnode,			//!< ������ �������
	MObject& generatornode			//!< generator node �.� 0
	)
{
	// ���� ������������ set - ��������� ����� �� ��������� ����
	std::map<cls::INode*, MObject>::iterator it = setforpass.find(&passnode);
	if( it!=setforpass.end())
	{
		MStatus stat;
		MObject setobj = it->second;
		MFnSet setfn(setobj, &stat);

		bool inset = IsNodeInSet(node, setfn);

		if( inset)
		{
			std::string passname = passnode.getName();
			render->IfAttribute("@passname", cls::PS(passname));
			render->Attribute("@visible", true);
			render->EndIfAttribute();
		}
	}
	return true;
}

bool PassGenerator_Final::AddExportFrames(
	cls::INode& pass, 
	cls::INode& node, 
	float renderframe, 			
	cls::IExportContext* context
	)
{
	cls::P<bool> blur = ocExport_GetAttrValue(pass.getObject(), "ocsBlur");
	if( blur.empty() || !blur.data())
	{
		return cls::IPassGenerator::AddExportFrames(
			pass, 
			node, 
			renderframe, 			
			context);
	}

	int blurcount = 2;
	bool bSubFrame = false;
	float shutterAngle = 90;

	for(int i=0; i<blurcount; i++)
	{
		float exportTime = renderframe;
		if(i!=0) exportTime = renderframe + i/(blurcount-1.f);

		context->addExportTimeNode(node, renderframe, exportTime);
	}
	return true;
}

// ������� ��� ����������� ���� �� ExportFrame 
// ���� ����� ���� ���������� IBlurGenerator
bool PassGenerator_Final::BuildNodeFromExportFrame(
	cls::INode& pass,				// pass
	cls::INode& node,				// pass
	cls::IRender* render,			// ������
	float frame,
	Math::Matrix4f& transform,		// ����������� ����� transform
	Math::Box3f& box,				// ����������� ���� ��� �������
	cls::IExportContext* context	// �������� ��������
	)
{
	cls::P<bool> blur = ocExport_GetAttrValue(pass.getObject(), "ocsBlur");
	if( blur.empty() || !blur.data())
	{
		return cls::IPassGenerator::BuildNodeFromExportFrame(
			pass,
			node,
			render,
			frame,
			transform,
			box,	
			context);
	}

	int blurcount = 2;
	bool bSubFrame = false;
	float shutterAngle = 90;

	cls::renderBlurParser<> blurParser;
	for(int i=0; i<blurcount; i++)
	{
		float exportTime = frame;
		if(i!=0) exportTime = frame + i/(blurcount-1.f);
		float blurphase = i/(blurcount-1.f);

		cls::IRender* pcache=NULL;
		Math::Box3f* pbox = NULL;
		if( !context->getNodeExportFrameCache( exportTime, node, pcache, pbox))
		{
			context->error( "getNodeExportFrameCache %s %f FAILED\n", frame, node.getName());
			return false;
		}
		pcache->RenderGlobalAttributes(render);

		blurParser.SetCurrentPhase(blurphase);
		pcache->Render(&blurParser);
		Math::Box3f wb = *pbox;
		if( wb.isvalid())
		{
			wb.transform(transform);
			box.insert(wb);
		}
	}
	blurParser.Render(render);

	return true;

}


//! ������� ������ � �������� � ���������� ������� ��������
bool PassGenerator_Final::OnExport(
	cls::INode& _node,				//!< ������ ���������� ������� (�.�., kNullObj)
	cls::IRender* scenecache,		//!< ����������� � ��� �����
	Math::Box3f scenebox,			//!< box �����
	cls::IRender* subpasscache,		//!< ��� ����������� (�� ���� ������� OnExportParentPass)
	cls::IExportContext* context	// 
	)
{
	return OnExport(
		_node.getName(), 
		_node,				//!< ������ ���������� ������� (�.�., kNullObj)
		scenecache,			//!< ����������� � ��� �����
		scenebox,			//!< box �����
		subpasscache,		//!< ��� ����������� (�� ���� ������� OnExportParentPass)
		context	// 
		);
}

//! ������� ������ � ��������
bool PassGenerator_Final::OnExport(
	const char* _passname,			// ��� �������
	cls::INode& _node,				//!< ������ ���������� ������� (�.�., kNullObj)
	cls::IRender* scenecache,		//!< ����������� � ��� �����
	Math::Box3f scenebox,			//!< box �����
	cls::IRender* subpasscache,		//!< ��� ����������� (�� ���� ������� OnExportParentPass)
	cls::IExportContext* context	// 
	)
{
	MObject node = _node.getObject();
	MObject camnode = FindCamera(node, context);

	std::string displayname = _passname;

	cls::PA<int> resolution		= ocExport_GetAttrValue(node, "resolution");
	cls::P<float> format_ratio	= ocExport_GetAttrValue(node, "format_ratio");
	cls::P<float> dspyGain		= ocExport_GetAttrValue(node, "dspyGain");
	cls::P<float> dspyGamma		= ocExport_GetAttrValue(node, "dspyGamma");
	cls::PA<float> pixelsample	= ocExport_GetAttrValue(node, "pixelsample");
	cls::PA<int> pixelfilter	= ocExport_GetAttrValue(node, "pixelfilter");
	cls::P<float> shadingrate	= ocExport_GetAttrValue(node, "shadingrate");
	cls::P<bool> ocsAscii		= ocExport_GetAttrValue(node, "ocsAscii");
	
	cls::PS filtername			= ocExport_GetAttrValue(node, "filtername");

//	cls::PS displaytype			= ocExport_GetAttrValue(node, "displaytype");
	std::string displaytype		= context->getEnvironment("DISPLAYTYPE");
	cls::PS displaymode			= ocExport_GetAttrValue(node, "displaymode");

	cls::PS quantizeMode		= ocExport_GetAttrValue(node, "quantizeMode");
	cls::P<int> quantizeOne		= ocExport_GetAttrValue(node, "quantizeOne");
	cls::P<int> quantizeMax		= ocExport_GetAttrValue(node, "quantizeMax");
	cls::P<int> quantizeMin		= ocExport_GetAttrValue(node, "quantizeMin");
	cls::P<float> quantizeDither= ocExport_GetAttrValue(node, "quantizeDither");

	cls::P<bool> sigma			= ocExport_GetAttrValue(node, "sigma");
	cls::P<float> sigmablur		= ocExport_GetAttrValue(node, "sigmaBlur");


	cls::PS prescenestring		= ocExport_GetAttrValue(node, "addPreScene");
	cls::PS outputOptions		= ocExport_GetAttrValue(node, "frameOptions");
	cls::PS beforeScene			= ocExport_GetAttrValue(node, "beforeScene");

	cls::P<bool> blur = ocExport_GetAttrValue(node, "ocsBlur");
	bool bBlur = (!blur.empty() && blur.data());

	cls::P<bool> trace = ocExport_GetAttrValue(node, "trace");
	bool bTrace = (!trace.empty() && trace.data());
	

	std::string passname = _passname;

	Util::correctFileName(passname, '_');
	std::string workspacedir = context->getEnvironment("WORKSPACEDIR");
	std::string outputdir = context->getEnvironment("OUTPUTDIR");
	std::string ribdir = context->getEnvironment("RIBDIR");
	std::string statdir = context->getEnvironment("STATDIR");
	std::string framenumber = context->getEnvironment("FRAMENUMBER");
	int frame = atoi(framenumber.c_str());
	framenumber = "_"+framenumber;

	std::string outfilename = ribdir+"\\"+passname+framenumber+".ocs";
	std::string outstatname = statdir+"\\"+passname+framenumber+".xml";
	std::string outfilenamescene = ribdir+"\\"+passname+framenumber+"_scene.ocs";
	std::string outfilenamelights = ribdir+"\\"+passname+framenumber+"_lights.ocs";
	std::string outfilenamerib = ribdir+"\\"+passname+framenumber+".rib";
	std::string outfilename_bat = ribdir+"\\"+passname+framenumber+".bat";
	std::string taskname = passname+"."+framenumber;
	std::string previewcmd;

	Util::changeSlashToUnixSlash(outfilename);
	Util::changeSlashToUnixSlash(outstatname);
	Util::changeSlashToUnixSlash(outfilenamescene);
	Util::changeSlashToUnixSlash(outfilenamelights);
	Util::changeSlashToUnixSlash(outfilenamerib);
	Util::changeSlashToUnixSlash(outfilename_bat);

	bool bAscii = true;
	if( !ocsAscii.empty())
		bAscii = ocsAscii.data();

	// displayname
	{
		std::string filename;
		filename = context->getEnvironment("FILENAME");
		if( filename.empty())
			filename = context->getEnvironment("JOBNAME");
		if( !filename.empty())
			displayname = filename;

		char* extenion="";
		if( strcmp(displaytype.c_str(),"tiff")==0)
			extenion = ".tif";
		if( strcmp(displaytype.c_str(),"exr")==0)
			extenion = ".exr";
		if( strcmp(displaytype.c_str(),"targa")==0)
			extenion = ".tga";

		if( strcmp(displaytype.c_str(),"it")==0)
		{
			displayname += " "+framenumber;
		}
		else
		{
			displayname = outputdir+"/"+displayname+framenumber+extenion;
			previewcmd = "sho "+displayname;
			Util::create_directory_for_file(displayname.c_str());
		}
		Util::changeSlashToUnixSlash(displayname);
		Util::changeSlashToUnixSlash(previewcmd);
	}

	Math::Matrix4f mview;
	{
		// ������� ������ � ��������:
		Util::create_directory_for_file(outfilename.c_str());
		cls::renderFileImpl filerender;
		if( !filerender.openForWrite(outfilename.c_str(), bAscii))
		{
			context->error("Export: renderFileImpl cant create \"%s\" file!!!", outfilename.c_str());
			return false;
		}
		cls::IRender* render = &filerender;

		// CAMERA
		{
			render->PushAttributes();

			cls::INode* camexportnode = context->getNodeForObject(camnode);
			Math::Matrix4f cammatrix1, cammatrix2;
			if( camexportnode)
			{
				float frame = context->getCurrentFrame();
				cls::P<bool> blur = ocExport_GetAttrValue(_node.getObject(), "ocsBlur");
				cls::P<bool> cameraBlur = ocExport_GetAttrValue(_node.getObject(), "cameraBlur");
				if( blur.empty() || !blur.data() || cameraBlur.empty() || !cameraBlur.data())
				{
					// ��� �����
					cls::IRender* pcache=NULL;
					Math::Box3f* pbox = NULL;
					if( context->getNodeExportFrameCache( frame, *camexportnode, pcache, pbox))
					{
						pcache->GetParameter("camera::transform", cammatrix1);
						render->Parameter("camera::transform", cammatrix1);
					}
				}
				else
				{
					// ����
					int blurcount = 2;
					render->MotionBegin();
					for(int i=0; i<blurcount; i++)
					{
						float exportTime = frame;
						if(i!=0) exportTime = frame + i/(blurcount-1.f);
						float blurphase = i/(blurcount-1.f);

						cls::IRender* pcache=NULL;
						Math::Box3f* pbox = NULL;
						if( context->getNodeExportFrameCache( exportTime, *camexportnode, pcache, pbox))
						{
							render->MotionPhaseBegin(blurphase);
							if(i==0)
							{
								pcache->GetParameter("camera::transform", cammatrix1);
								render->Parameter("camera::transform", cammatrix1);
							}
							else
							{
								pcache->GetParameter("camera::transform", cammatrix2);
								render->Parameter("camera::transform", cammatrix2);
							}	
							render->MotionPhaseEnd();
						}
					}
					render->MotionEnd();
				}
			}
			mview = cammatrix1;

			this->CameraAttributes(camnode, render);
//			render->GetParameter("camera::transform", mview);

			if( camnode.hasFn(MFn::kCamera))
			{
				MFnCamera fnCamera(camnode);
				render->Parameter("camera::resolution", resolution);
				render->Parameter("camera::ratio", format_ratio);

				cls::PS projection;
				render->GetParameter("camera::projection", projection);
				
				if( strcmp(projection.data(), "perspective")==0)
				{
					// screen
					float sw_x = 1;
					float sw_y = resolution[1]/(float)resolution[0];
					cls::PA<float> screen(cls::PI_CONSTANT, 4);
					screen[0] = -sw_x;
					screen[1] = sw_x;
					screen[2] = -sw_y;
					screen[3] = sw_y;
					render->Parameter("camera::screen", screen);
				}
				if( strcmp(projection.data(), "orthographic")==0)
				{
					cls::PA<float> screen;
					render->GetParameter("camera::screen", screen);
					float sw_x = 1;
					float sw_y = resolution[1]/(float)resolution[0];
					screen[2] = screen[0]*sw_y;
					screen[3] = screen[1]*sw_y;
					render->Parameter("camera::screen", screen);
				}
			}
			else if(node.hasFn(MFn::kSpotLight))
			{
			}
			render->RenderCall("Camera");
			render->PopAttributes();
		}

		// OPTIONS
		{
			render->PushAttributes();
			std::string cameraname = MFnDependencyNode(camnode).name().asChar();
			render->Parameter("output::cameraname", cameraname.c_str());

			render->Parameter("output::shadingrate", shadingrate);

			render->Parameter("output::gain", dspyGain);
			render->Parameter("output::gamma", dspyGamma);

			render->Parameter("output::pixelsample", pixelsample);

			render->Parameter("output::filtersize", pixelfilter);
			render->Parameter("output::filtername", filtername);

			render->Parameter("output::displayname", displayname.c_str());
			render->Parameter("output::displaytype", displaytype.c_str());
			render->Parameter("output::displaymode", displaymode);

			render->Parameter("output::quantizeMode", quantizeMode);
			render->Parameter("output::quantizeOne",  quantizeOne);
			render->Parameter("output::quantizeMax",  quantizeMax);
			render->Parameter("output::quantizeMin",  quantizeMin);
			render->Parameter("output::quantizeDither", quantizeDither);

			render->Parameter("hidden::sigma",		sigma);
			render->Parameter("hidden::sigmablur",	sigmablur);

			if( !statdir.empty())
			{
				render->Parameter("statistics::endofframe", (int)1);
				render->Parameter("statistics::xmlfilename", outstatname);
				Util::create_directory_for_file(outstatname.c_str());
			}

			if(bTrace)
			{
				render->Parameter("output::tracebreadthfactor",	ocExport_GetAttrValue(node, "traceBreadthFactor"));
				render->Parameter("output::tracedepthfactor",	ocExport_GetAttrValue(node, "traceDepthFactor"));
				render->Parameter("output::tracemaxdepth",		ocExport_GetAttrValue(node, "traceMaxDepth"));
			}

			// ������� ���������������� �����
			if( !outputOptions.empty())
			{
				cls::renderStringImpl::RenderText(outputOptions.data(), render, context);
			}
			render->RenderCall("Output");
			render->PopAttributes();
		}
	}

	// ������� ����� � ��������� ����
	{
		Util::create_directory_for_file(outfilenamescene.c_str());
		cls::renderFileImpl filerender;
		if( !filerender.openForWrite(outfilenamescene.c_str(), bAscii))
		{
			context->error("Export: renderFileImpl cant create \"%s\" file!!!", outfilenamescene.c_str());
			return false;
		}
		filerender.Attribute("@pass", "final");
		filerender.Attribute("@passname", passname.c_str());
		filerender.Attribute("@camera::worldpos", mview);
		filerender.Attribute("shadingInterpolation", "smooth");

		// ���� ������������ set - ��������� visible = false
		std::map<cls::INode*, MObject>::iterator it = setforpass.find(&_node);
		if( it!=setforpass.end())
		{
			filerender.Attribute("@visible", false);
		}

		// ������� beforeScene
		if( !beforeScene.empty())
		{
			cls::renderStringImpl::RenderText(beforeScene.data(), &filerender, context);
		}

		subpasscache->Render(&filerender);
		scenecache->RenderGlobalAttributes(&filerender);
		scenecache->Render(&filerender);
	}


	// ��������� �����
	{
		Util::create_directory_for_file(outfilenamelights.c_str());
		cls::renderFileImpl filerender;
		if( !filerender.openForWrite(outfilenamelights.c_str(), bAscii))
		{
			context->error("Export: renderFileImpl cant create \"%s\" file!!!", outfilenamelights.c_str());
			return false;
		}
		filerender.PushAttributes();
		subpasscache->Render(&filerender);

		std::vector<cls::INode* > lights;
		context->getGlobalLights(lights);
		for(int ll=0; ll<(int)lights.size(); ll++)
		{
			cls::INode* node = lights[ll];

//			cls::IRender* pcache;
			Math::Box3f pbox;

			MObject genobj;
			cls::ILightGenerator* gen = (cls::ILightGenerator*)node->GetGenerator(cls::IGenerator::LIGHT, genobj);
			if( !gen) 
				continue;

			gen->OnLight(*node, &filerender, pbox, context, genobj);
			
			/*/
			context->getNodeExportFrameCache(
				context->getCurrentFrame(), 
				*lights[ll],				// ������
				pcache,
				pbox
				);

			if( pcache)
				pcache->Render(&filerender);
			/*/
		}
		filerender.PopAttributes();
	}

	// �������������� � ���!
	{
		/*/
#ifdef _DEBUG
	  	getIPrmanDll dllproc("IPrmanD.dll", "getIPrman");
#else
	  	getIPrmanDll dllproc("IPrman.dll", "getIPrman");
#endif
		if( !dllproc.isValid()) 
		{
			context->error("cant fount IPrman.dll or IPrmanD.dll");
			return false;
		}
		IPrman* prman = (*dllproc)();
		prman->RiBegin( (char*)outfilenamerib.c_str() );
		{
			RtString format = "ascii";
			prman->RiOption(( RtToken ) "rib", ( RtToken ) "format", &format, RI_NULL);
			RtString compression = "none";
			prman->RiOption(( RtToken ) "rib", ( RtToken ) "compression", &compression, RI_NULL);
		}
		/*/

		Prman_ascii thePrman;
		thePrman.Open(outfilenamerib.c_str());
		IPrman* prman = &thePrman;

		std::string seachpath = this->getSearchPath(context, "");

		RtString text = (RtString)seachpath.c_str();
		prman->RiOption( (RtToken )"searchpath", (RtToken )"shader", ( RtPointer )&text, RI_NULL);
		prman->RiOption( (RtToken )"searchpath", (RtToken )"texture", ( RtPointer )&text, RI_NULL);
		prman->RiOption( (RtToken )"searchpath", (RtToken )"procedural", ( RtPointer )&text, RI_NULL);

		prman->RiFrameBegin(frame);

		// blur !!!
		float frameTime = 0.f;
		float shutterOpenFrame = frameTime+0;
		float shutterCloseFrame = frameTime+1;
		if( bBlur)
		{
			cls::P<float> shutterAngle = ocExport_GetAttrValue(node, "shutterAngle");
			cls::P<bool> subframeMotion = ocExport_GetAttrValue(node, "subframeMotion");
			
			if( !shutterAngle.empty())
				shutterCloseFrame = shutterOpenFrame + shutterAngle.data()/360.f;
		
			prman->RiShutter(shutterOpenFrame, shutterCloseFrame);
		}

		// ������ ������ � ���������
		cls::renderPrmanImpl<> renderprman(prman);
		renderprman.setBlurParams(shutterOpenFrame, shutterOpenFrame, shutterCloseFrame);
		cls::renderFileImpl filerender;
		if( !filerender.openForRead(outfilename.c_str()))
		{
			context->error("Export: renderFileImpl cant open \"%s\" file!!!", outfilename.c_str());
			return false;
		}
		filerender.Render( &renderprman);

		prman->RiWorldBegin();
		prman->RiTransformBegin();
			prman->RiRotate( 90, 0, 1, 0);
			prman->RiCoordinateSystem( "_XTOZ");
		prman->RiTransformEnd();
		prman->RiTransformBegin();
			prman->RiRotate( -90, 1, 0, 0);
			prman->RiCoordinateSystem( "_YTOZ");
		prman->RiTransformEnd();
		prman->RiTransformBegin();
			prman->RiScale( -1, 1, 1);
			prman->RiCoordinateSystem( "_world_lefthanded");
		prman->RiTransformEnd();
		prman->RiTransformBegin();
			prman->RiCoordinateSystem( "_YTOZ");
			prman->RiCoordinateSystem( "worldspace");
			prman->RiCoordinateSystem( "_environment");
		prman->RiTransformEnd();

		// ��������� �����
		{
			cls::renderFileImpl filerender;
			if( !filerender.openForRead(outfilenamelights.c_str()))
			{
				context->error("Export: renderFileImpl cant open \"%s\" file!!!", outfilenamelights.c_str());
				return false;
			}
			filerender.Render( &renderprman);
		}

		// ����� �������
		{
			cls::IRender* render = &renderprman;
			render->Attribute("visibility::camera", 1);
			if(bTrace)
			{
				render->Attribute("visibility::trace", 1);
				render->Attribute("visibility::photon", 0);
				render->Attribute("visibility::transmission", 1);

				render->Attribute("trace::maxspeculardepth",	ocExport_GetAttrValue(node, "traceMaxSpecularDepth"));
				render->Attribute("trace::maxdiffusedepth",		ocExport_GetAttrValue(node, "traceMaxDiffuseDepth"));
				render->Attribute("trace::bias",				ocExport_GetAttrValue(node, "traceBias"));
				render->Attribute("trace::displacements",		ocExport_GetAttrValue(node, "traceDisplacements"));
				render->Attribute("trace::samplemotion",		ocExport_GetAttrValue(node, "traceSampleMotion"));

				render->Attribute("photon::shadingmodel", "matte");
				render->Attribute("photon::estimator", 50);
			}
		}

		// RiProcedural
		{
			char buf[2048];
			// ���������� � ������
//			cls::renderStringImpl stringrender;
//			stringrender.openForWrite();
//			subpasscache->Render(&stringrender);
//			stringrender.Attribute("@pass", "final");
//			std::string addstream = stringrender.getWriteBuffer();
			std::string addstream = "";

			const char* ocs = outfilenamescene.c_str();
			_snprintf(buf, 1224, "%s$%f$%f$%f$%s", 
				ocs, 
				frameTime, frameTime, frameTime+1, "");

			addstream = buf+addstream;

			RtBound bound;
			IPrman::copy(bound, scenebox);
			RtString *data = static_cast<RtString*>(malloc(sizeof(RtString) * 3));
			data[0] = _strdup("ocellarisPrmanDSO.dll");
			data[1] = _strdup(addstream.c_str());
			data[2] = 0;
			prman->RiProcedural(data, bound, prman->RiProcDynamicLoad(), dl_FurProceduralFree);
		}

		prman->RiWorldEnd();
		prman->RiFrameEnd();

		prman->RiEnd();
	}

	// ������� ��� �������:
	std::string rendername = context->getEnvironment( "RENDER");
	std::string localcmd;
	{

		localcmd = context->getEnvironment( "RENDERCOMMAND");
		if(localcmd.empty())
			localcmd = "prman.exe -p:1 -Progress";
		localcmd += " ";
		localcmd += "\""+outfilenamerib+"\"";
		FILE* file = fopen(outfilename_bat.c_str(), "wt");
		if( file)
		{
			std::string path_env = context->getEnvironment("PATH");
			if( !path_env.empty())
				fprintf(file, "set PATH=%s;%%PATH%%\n", path_env.c_str());

			fputs( localcmd.c_str(), file);
			fputs( "\n", file);

			fprintf(file, "ocellarisTest.exe \"%s\" \"%s.ocs\" -ocs -exe\n", outfilenamescene.c_str(), outfilenamescene.c_str());
			fprintf(file, "ocellarisTest.exe \"%s\" \"%s.rib\" -rib -exe\n", outfilenamescene.c_str(), outfilenamescene.c_str());
			fprintf(file, "pause\n");
			fclose(file);
		}
	}

	// ������!
	{
		int atmost = 1;
		if( rendername=="netrender")
		{
			cls::P<int> maxProcessors = ocExport_GetAttrValue(node, "maxProcessors");
			maxProcessors.data(atmost);
		}

		cls::ITask* task = context->addTask(NULL);
		task->Parameter("name", cls::PS(taskname));
		task->Parameter("cmd", cls::PS(localcmd));
		task->Parameter("atmost", cls::P<int>(atmost));
		if( !previewcmd.empty())
			task->Parameter("preview", cls::PS(previewcmd.c_str()));

		if( displaytype!="it")
		{
			cls::IRender* objectContext = context->objectContext(_node.getObject(), this);

			std::vector<std::string> files;
			objectContext->GetParameter("filelist", files);
			files.push_back(displayname);
			objectContext->Parameter("filelist", files);

			std::vector<std::string> tasks;
			objectContext->GetParameter("tasklist", tasks);
			tasks.push_back(taskname);
			objectContext->Parameter("tasklist", tasks);

		}
	}
	return true;
}

// ���������� � ���� ����������� ��� �������� �������
// ��������, ������ shadowpass ������ �������� � ������� ��� �������� ����
void PassGenerator_Final::OnExportParentPass( 
	cls::INode& _node,				//!< ������ ���������� ������� (�.�., kNullObj)
	cls::IRender* render, 
	cls::IExportContext* context
	)
{
	MObject node = _node.getObject();

	/*/
	if( !prescenestring.empty())
	{
		try
		{
			cls::renderStringImpl fileimpl;
			std::string customdata = prescenestring.data();
			customdata += "\n";
			if( fileimpl.openForRead(customdata.c_str()))
			{
				fileimpl.Render(render);
			}
		}
		catch(...)
		{
			displayString("Error parse addString");
		}
	}
	/*/
}
//! ���������� ����� �������� ���� ������
bool PassGenerator_Final::OnPostExport(
	cls::INode& node,				//!< ������ ���������� ������� (�.�., kNullObj)
	cls::IExportContext* context	// 
	)
{
	bool bMakeMov = true;
	cls::P<bool> _bMakeMov = ocExport_GetAttrValue(node.getObject(), "makeMov");
	if( !_bMakeMov.empty())
		bMakeMov = _bMakeMov.data();

	if( bMakeMov)
	{
		cls::IRender* objectContext = context->objectContext(node.getObject(), this);

		std::vector<std::string> files;
		objectContext->GetParameter("filelist", files);
		std::vector<std::string> tasks;
		objectContext->GetParameter("tasklist", tasks);

		std::string outputdir = context->getEnvironment("OUTPUTDIR");
		Util::removeBackSlash(outputdir);

		std::string displayname;
		displayname = context->getEnvironment("FILENAME");
		if( displayname.empty())
			displayname = context->getEnvironment("JOBNAME");

		std::string movname = outputdir;
		if( !displayname.empty())
		{
			outputdir = Util::extract_foldername(outputdir.c_str());
			movname = outputdir+displayname;
		}

		time_t t = time(NULL);
		struct tm* newTime = localtime( &t);
		movname+=asctime(newTime);
		movname = movname+".mov";
		Util::correctFileName(movname, '_', true);
		Util::changeSlashToUnixSlash(movname);

		std::string localcmd;
		localcmd += "ocellarisCropAsm.exe -mov ";
		localcmd += '\"'+movname+'\"';
		int i=0;
		for(i=0;i<(int)files.size(); i++)
		{
			localcmd += " ";
			localcmd += '\"'+files[i]+'\"';
		}
		std::string previewcmd = "ocellarisCmd.exe -openfile -select \""+movname+"\"";
		Util::changeSlashToUnixSlash(previewcmd);

		cls::ITask* task = context->addTask(NULL);
		task->Parameter("name", cls::PS("build mov"));
		task->Parameter("cmd", cls::PS(localcmd));
		task->Parameter("service", cls::PS("quicktime"));
	//	task->Parameter("atmost", cls::P<int>(atmost));
		if( !previewcmd.empty())
			task->Parameter("preview", cls::PS(previewcmd.c_str()));

		for(i=0;i<(int)tasks.size(); i++)
		{
			cls::ITask* dependon = context->findTask(tasks[i].c_str());
			if(!dependon) continue;

			context->addRelation( task, dependon);
		}
	}
	return true;
}


MObject PassGenerator_Final::FindCamera(
	MObject& node,
	cls::IExportContext* context
	)
{
	cls::PS camera = ocExport_GetAttrValue(node, "camera");
	MObject camnode = MObject::kNullObj;
	nodeFromName(camera.data(), camnode);

	if(camnode.isNull())
	{
		context->error("Cant find camera %s\n", camera.data());
		return MObject::kNullObj;
	}
	if( !camnode.hasFn(MFn::kCamera) && 
		!camnode.hasFn(MFn::kSpotLight))
	{
		context->error("Object %s is not Camera!!!\n", camera.data());
		return MObject::kNullObj;
	}
	return camnode;
}


// ��������� ��������� �� ���� � ����
bool PassGenerator_Final::IsNodeInSet(cls::INode& node, const MFnSet& setfn)
{
	MStatus stat;
	MFnDagNode dan( node.getPath().node() );
	if( dan.object().isNull())
		return false;

	bool res = setfn.isMember( dan.object(), &stat);
	if( stat) 
		return res;

	return false;

	/*/
	{
		bool res = false;
		for(;;)
		{
			bool _res = setfn.isMember( dan.object(), &stat);
			if( stat) 
			{
				res = _res;
				if(res) break;
			}
			MObject parent = dan.parent(0);
			if( parent.isNull())
				break;
			if( parent.hasFn(MFn::kWorld))
				break;
			dan.setObject(parent);
		}
		return res;
	}
	return false;
	/*/
}
