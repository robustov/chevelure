#pragma once

#include "ocellaris/OcellarisExport.h"
#include "ocellaris/IPassGenerator.h"
#include "PassGenerator_Base.h"

//! @ingroup implement_group
//! \brief ��������� ��� occlusion ������� 
//! 
struct PassGenerator_Occ : public PassGenerator_Base
{
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "PassGenerator_Occ";};

	//! OnInit ������� ����� ������?
	OCELLARISEXPORT_API virtual bool OnInit(
		MObject& node,			//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IExportContext* context,
		std::string& passname,
		std::vector<cls::SubGenerator>& subGenerators	// ������ ��������������
		);
	//! ������� ������ � ��������
	OCELLARISEXPORT_API virtual bool OnExport(
		MObject& node,			//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IExportContext* context,
		cls::IRender* render,			//!< render
		cls::IRender* prescene,			//!< pre scene
		cls::IRender* prenextgenerator	//!< pre next generator scene
		);
	/*/
	//! ���������� � ������ World ����� ������
	OCELLARISEXPORT_API virtual void OnPreScene(
		MObject& node,			//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IRender* render,		//!< render
		cls::IExportContext* context
		);
	/*/
	//! ������ ��������� � �������?
	OCELLARISEXPORT_API virtual bool IsObjectInPass(
		MObject& node,			//!< �������� ������ ���������� � �������
		cls::IRender* render			//!< render
		);

	//! ����� ������, node - ocOcclusionPass
	MObject FindCamera(
		MObject& node			//!< ������ ���������� ������� (�.�., kNullObj)
		);
};
