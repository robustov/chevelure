#include "stdafx.h"
#include "ocellaris/OcellarisExport.h"
#include "ocellaris/renderFileImpl.h"
#include "MathNMaya/MathNMaya.h"

#include "PassGenerator_Custom.h"

#include "ocellaris\renderStringImpl.h"
#include "ocellaris/renderPrmanImpl.h"
#include "Util/misc_create_directory.h"
#include <string>
#include "mathNpixar/IPrman.h"


PassGenerator_Custom passgenerator_custom;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl PassCustom()
	{
		return &passgenerator_custom;
	}
}


//! ������� ������ � �������� � ���������� ������� ��������
bool PassGenerator_Custom::OnExport(
	cls::INode& _node,				//!< ������ ���������� ������� (�.�., kNullObj)
	cls::IRender* scenecache,		//!< ����������� � ��� �����
	Math::Box3f scenebox,			//!< box �����
	cls::IRender* subpasscache,		//!< ��� ����������� (�� ���� ������� OnExportParentPass)
	cls::IExportContext* context	// 
	)
{
	const char* _passname = _node.getName();
	MObject node = _node.getObject();
	MObject camnode = FindCamera(node, context);

	std::string displayname = _passname;

	/*/
	cls::PA<int> resolution		= ocExport_GetAttrValue(node, "resolution");
	cls::P<float> format_ratio	= ocExport_GetAttrValue(node, "format_ratio");
	cls::P<float> dspyGain		= ocExport_GetAttrValue(node, "dspyGain");
	cls::P<float> dspyGamma		= ocExport_GetAttrValue(node, "dspyGamma");
	cls::PA<float> pixelsample	= ocExport_GetAttrValue(node, "pixelsample");
	cls::PA<int> pixelfilter	= ocExport_GetAttrValue(node, "pixelfilter");
	cls::P<float> shadingrate	= ocExport_GetAttrValue(node, "shadingrate");
	
	cls::PS filtername			= ocExport_GetAttrValue(node, "filtername");

	cls::PS displaytype			= ocExport_GetAttrValue(node, "displaytype");
	cls::PS displaymode			= ocExport_GetAttrValue(node, "displaymode");

	cls::PS quantizeMode		= ocExport_GetAttrValue(node, "quantizeMode");
	cls::P<int> quantizeOne		= ocExport_GetAttrValue(node, "quantizeOne");
	cls::P<int> quantizeMax		= ocExport_GetAttrValue(node, "quantizeMax");
	cls::P<int> quantizeMin		= ocExport_GetAttrValue(node, "quantizeMin");
	cls::P<float> quantizeDither= ocExport_GetAttrValue(node, "quantizeDither");

	cls::PS prescenestring		= ocExport_GetAttrValue(node, "addPreScene");
	/*/

	cls::PS cameraname			= ocExport_GetAttrValue(node, "camera");
	cls::PA<int> resolution		= ocExport_GetAttrValue(node, "resolution");
	cls::P<float> format_ratio	= ocExport_GetAttrValue(node, "format_ratio");

	cls::PS outputOptions		= ocExport_GetAttrValue(node, "outputOptions");
	cls::PS beforeScene			= ocExport_GetAttrValue(node, "beforeScene");
	cls::PS forChildPasses		= ocExport_GetAttrValue(node, "forChildPasses");
	cls::PS postRenderTask		= ocExport_GetAttrValue(node, "postRenderTask");
	

	cls::P<bool> ocsAscii		= ocExport_GetAttrValue(node, "ocsAscii");


	std::string passname = _passname;
	Util::correctFileName(passname, '_');
	std::string workspacedir = context->getEnvironment("WORKSPACEDIR");
	std::string outputdir = context->getEnvironment("OUTPUTDIR");
	std::string ribdir = context->getEnvironment("RIBDIR");
	std::string framenumber = context->getEnvironment("FRAMENUMBER");
	int frame = atoi(framenumber.c_str());
	framenumber = "_"+framenumber;

	std::string outfilename = ribdir+"\\"+passname+framenumber+".ocs";
	std::string outfilenamescene = ribdir+"\\"+passname+framenumber+"_scene.ocs";
	std::string outfilenamelights = ribdir+"\\"+passname+framenumber+"_lights.ocs";
	std::string outfilenamerib = ribdir+"\\"+passname+framenumber+".rib";
	std::string outfilename_bat = ribdir+"\\"+passname+framenumber+".bat";
	std::string taskname = passname+"."+framenumber;
	std::string previewcmd;

	Util::changeSlashToUnixSlash(outfilename);
	Util::changeSlashToUnixSlash(outfilenamescene);
	Util::changeSlashToUnixSlash(outfilenamelights);
	Util::changeSlashToUnixSlash(outfilenamerib);
	Util::changeSlashToUnixSlash(outfilename_bat);

	bool bAscii = true;
	if( !ocsAscii.empty())
		bAscii = ocsAscii.data();

context->error("ToDo DisplayChannel!");
	// ������� ����� � ��������� ����
	{
		Util::create_directory_for_file(outfilenamescene.c_str());
		cls::renderFileImpl filerender;
		if( !filerender.openForWrite(outfilenamescene.c_str(), bAscii))
		{
			context->error("Export: renderFileImpl cant create \"%s\" file!!!", outfilenamescene.c_str());
			return false;
		}
		filerender.Attribute("@pass", "final");
		filerender.Attribute("@passname", passname.c_str());

		// ������� beforeScene
		if( !beforeScene.empty())
		{
			cls::renderStringImpl::RenderText(beforeScene.data(), &filerender, context);
		}

		subpasscache->Render(&filerender);
		scenecache->Render(&filerender);
	}

	// displayname
	/*/
	{
		char* extenion="";
		if( strcmp(displaytype.data(),"tiff")==0)
			extenion = ".tif";
		if( strcmp(displaytype.data(),"exr")==0)
			extenion = ".exr";
		if( strcmp(displaytype.data(),"targa")==0)
			extenion = ".tga";

		if( strcmp(displaytype.data(),"it")==0)
		{
			displayname += " "+framenumber;
		}
		else
		{
			displayname = outputdir+"/"+displayname+framenumber+extenion;
			previewcmd = "sho "+displayname;
			Util::create_directory_for_file(displayname.c_str());
		}
		Util::changeSlashToUnixSlash(displayname);
		Util::changeSlashToUnixSlash(previewcmd);
	}
	/*/

	{
		// ������� ������ � ��������:
		Util::create_directory_for_file(outfilename.c_str());
		cls::renderFileImpl filerender;
		if( !filerender.openForWrite(outfilename.c_str(), bAscii))
		{
			context->error("Export: renderFileImpl cant create \"%s\" file!!!", outfilename.c_str());
			return false;
		}
		cls::IRender* render = &filerender;

		// CAMERA
		Math::Matrix4f mview;
		{
			render->PushAttributes();
			this->CameraAttributes(camnode, render);
			render->GetParameter("camera::transform", mview);

			if( camnode.hasFn(MFn::kCamera))
			{
				MFnCamera fnCamera(camnode);
				render->Parameter("camera::resolution", resolution);
				render->Parameter("camera::ratio", format_ratio);

				cls::PS projection;
				render->GetParameter("camera::projection", projection);
				
				if( strcmp(projection.data(), "perspective")==0)
				{
					// screen
					float sw_x = 1;
					float sw_y = resolution[1]/(float)resolution[0];
					cls::PA<float> screen(cls::PI_CONSTANT, 4);
					screen[0] = -sw_x;
					screen[1] = sw_x;
					screen[2] = -sw_y;
					screen[3] = sw_y;
					render->Parameter("camera::screen", screen);
				}
				if( strcmp(projection.data(), "orthographic")==0)
				{
					cls::PA<float> screen;
					render->GetParameter("camera::screen", screen);
					float sw_x = 1;
					float sw_y = resolution[1]/(float)resolution[0];
					screen[2] = screen[0]*sw_y;
					screen[3] = screen[1]*sw_y;
					render->Parameter("camera::screen", screen);
				}
			}
			else if(node.hasFn(MFn::kSpotLight))
			{
			}
			render->RenderCall("Camera");
			render->PopAttributes();
		}

		// OPTIONS
		{
			render->PushAttributes();

			std::string cameraname = MFnDependencyNode(camnode).name().asChar();
			render->Parameter("output::cameraname", cameraname.c_str());

			// ������� outputOptions
			if( !outputOptions.empty())
			{
				cls::renderStringImpl::RenderText(outputOptions.data(), &filerender, context);
			}

			render->RenderCall("Output");
			render->PopAttributes();
		}
	}

	// �������������� � ���!
	{
#ifdef _DEBUG
	  	getIPrmanDll dllproc("IPrmanD.dll", "getIPrman");
#else
	  	getIPrmanDll dllproc("IPrman.dll", "getIPrman");
#endif
		if( !dllproc.isValid()) 
		{
			context->error("cant fount IPrman.dll or IPrmanD.dll");
			return false;
		}

		IPrman* prman = (*dllproc)();
		prman->RiBegin( (char*)outfilenamerib.c_str() );
		{
			RtString format = "ascii";
			prman->RiOption(( RtToken ) "rib", ( RtToken ) "format", &format, RI_NULL);
			RtString compression = "none";
			prman->RiOption(( RtToken ) "rib", ( RtToken ) "compression", &compression, RI_NULL);
		}
		std::string buf;
		std::string seachpath; 
		buf = workspacedir; Util::changeSlashToTclSlash(buf);
		seachpath += buf; seachpath += ":";
		seachpath += buf+"/3D"; seachpath += ":";

		if( getenv("RMANTREE"))
		{
			buf = getenv("RMANTREE"); Util::changeSlashToTclSlash(buf);
			seachpath += buf+"/lib"; seachpath += ":";
			seachpath += buf+"/lib/shaders"; seachpath += ":";
		}

		if( getenv("RATTREE"))
		{
			buf = getenv("RATTREE"); Util::changeSlashToTclSlash(buf);
			seachpath += buf+"/lib/shaders"; seachpath += ":";
		}

		buf = context->getEnvironment("SEARCHDIR");
		if( !buf.empty())
		{
			Util::changeSlashToTclSlash(buf);
			seachpath += buf;		 seachpath += ":";
			seachpath += buf+"/bin"; seachpath += ":";
			seachpath += buf+"/dso"; seachpath += ":";
		}
		buf = ocGetPath();
		if( !buf.empty())
		{
			Util::changeSlashToTclSlash(buf);
			seachpath += buf;		 seachpath += ":";
		}

//		buf = ocellarisInstallDir; Util::changeSlashToTclSlash(buf);
//		seachpath += buf;seachpath += ":";
		seachpath += ".";seachpath += ":";
		seachpath += "@";seachpath += ":";
		Util::changeSlashToSlash(seachpath);

		RtString text = (RtString)seachpath.c_str();
		prman->RiOption( (RtToken )"searchpath", (RtToken )"shader", ( RtPointer )&text, RI_NULL);
		prman->RiOption( (RtToken )"searchpath", (RtToken )"texture", ( RtPointer )&text, RI_NULL);
		prman->RiOption( (RtToken )"searchpath", (RtToken )"procedural", ( RtPointer )&text, RI_NULL);

		prman->RiFrameBegin(frame);

		// ������ ������ � ���������
		cls::renderPrmanImpl<> renderprman(prman);
		cls::renderFileImpl filerender;
		if( !filerender.openForRead(outfilename.c_str()))
		{
			context->error("Export: renderFileImpl cant open \"%s\" file!!!", outfilename.c_str());
			return false;
		}
		filerender.Render( &renderprman);

		prman->RiWorldBegin();

		// before scene
		{
			// ������� beforeScene
			if( !beforeScene.empty())
			{
				cls::renderStringImpl::RenderText(beforeScene.data(), &renderprman, context);
			}
		}


		// RiProcedural
		{
			char buf[2048];
			std::string addstream = "";

			float frameTime = 0.f;
			const char* ocs = outfilenamescene.c_str();
			_snprintf(buf, 1224, "%s$%f$%f$%f$%s", 
				ocs, 
				frameTime, frameTime, frameTime+1, "");

			addstream = buf+addstream;

			RtBound bound;
			IPrman::copy(bound, scenebox);
			RtString *data = static_cast<RtString*>(malloc(sizeof(RtString) * 3));
			data[0] = _strdup("ocellarisPrmanDSO.dll");
			data[1] = _strdup(addstream.c_str());
			data[2] = 0;
			prman->RiProcedural(data, bound, prman->RiProcDynamicLoad(), dl_FurProceduralFree);
		}

		prman->RiWorldEnd();
		prman->RiFrameEnd();

		prman->RiEnd();
	}

	// ������� ��� �������:
	std::string localcmd;
	{
		localcmd += "prman.exe -p:1 -Progress ";
		localcmd += "\""+outfilenamerib+"\"";
		FILE* file = fopen(outfilename_bat.c_str(), "wt");
		if( file)
		{
			std::string path_env = context->getEnvironment("PATH");
			if( !path_env.empty())
				fprintf(file, "set PATH=%%PATH%%;%s\n", path_env.c_str());

			fputs( localcmd.c_str(), file);
			fputs( "\n", file);

			fprintf(file, "rem ocellarisTest.exe \"%s\" \"%s.ocs\" -ocs -exe\n", outfilenamescene.c_str(), outfilenamescene.c_str());
			fprintf(file, "rem ocellarisTest.exe \"%s\" \"%s.rib\" -rib -exe\n", outfilenamescene.c_str(), outfilenamescene.c_str());
			fclose(file);
		}
	}

	// ������!
	{
		cls::ITask* task = context->addTask(NULL);
		task->Parameter("name", cls::PS(taskname));
		task->Parameter("cmd", cls::PS(localcmd));
		if( !previewcmd.empty())
			task->Parameter("preview", cls::PS(previewcmd.c_str()));

context->error("ToDo postRenderTask!");
	}
	// postRenderTask
	{
		if( !postRenderTask.empty())
		{
			std::string cmd = postRenderTask.data();
			cls::ITask* task = context->addTask(NULL);
			task->Parameter("name", cls::PS(taskname+"_postRenderTask"));
			task->Parameter("cmd", cls::PS(cmd));
		}
	}
	return true;
}

// ���������� � ���� ����������� ��� �������� �������
// ��������, ������ shadowpass ������ �������� � ������� ��� �������� ����
void PassGenerator_Custom::OnExportParentPass( 
	cls::INode& _node,				//!< ������ ���������� ������� (�.�., kNullObj)
	cls::IRender* render, 
	cls::IExportContext* context
	)
{
	MObject node = _node.getObject();

	/*/
	if( !prescenestring.empty())
	{
		try
		{
			cls::renderStringImpl fileimpl;
			std::string customdata = prescenestring.data();
			customdata += "\n";
			if( fileimpl.openForRead(customdata.c_str()))
			{
				fileimpl.Render(render);
			}
		}
		catch(...)
		{
			displayString("Error parse addString");
		}
	}
	/*/
}


MObject PassGenerator_Custom::FindCamera(
	MObject& node,
	cls::IExportContext* context
	)
{
	cls::PS camera = ocExport_GetAttrValue(node, "camera");
	MObject camnode = MObject::kNullObj;
	nodeFromName(camera.data(), camnode);

	if(camnode.isNull())
	{
		context->error("Cant find camera %s\n", camera.data());
		return MObject::kNullObj;
	}
	if( !camnode.hasFn(MFn::kCamera) && 
		!camnode.hasFn(MFn::kSpotLight))
	{
		context->error("Object %s is not Camera!!!\n", camera.data());
		return MObject::kNullObj;
	}
	return camnode;
}


