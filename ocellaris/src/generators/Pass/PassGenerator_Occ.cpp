#include "stdafx.h"
#include "PassGenerator_Occ.h"

//! ����� ������, node - ocOcclusionPass
MObject PassGenerator_Occ::FindCamera(
	MObject& node			//!< ������ ���������� ������� (�.�., kNullObj)
	)
{
	cls::PS camera = ocExport_GetAttrValue(node, "camera");
	MObject camobj = MObject::kNullObj;
	if( camera.isValid())
	{
		nodeFromName(camera.data(), camobj);
	}
	return camobj;
}


//! OnInit ������� ����� ������?
bool PassGenerator_Occ::OnInit(
	MObject& node,
	cls::IExportContext* context,
	std::string& passname,
	std::vector<cls::SubGenerator>& subGenerators	// ������ ��������������
	)
{
	// node ������ ���� ���� ocOcclusionPass
	MString typemstr = MFnDependencyNode( node).typeName();
	const char* typestr = typemstr.asChar();
	if( strcmp( typestr, "ocOcclusionPass")!=0)
		return false;

	cls::P<bool> renderable		= ocExport_GetAttrValue(node, "renderable");
	if( renderable.isValid())
		if( !*renderable)
			return false;

	MObject camnode = FindCamera(node);
	if(camnode.isNull()) 
		return false;
	std::string cameraname = MFnDependencyNode(camnode).name().asChar();

	passname = "occlusion";
	return true;
}
//! ������� ������ � ��������
bool PassGenerator_Occ::OnExport(
	MObject& node,			//!< ������ (�.�., kNullObj)
	cls::IExportContext* context,
	cls::IRender* render,			//!< render
	cls::IRender* prescene,			//!< pre scene
	cls::IRender* prenextgenerator	//!< pre next generator scene
	)
{
	cls::P<float> scale = ocExport_GetAttrValue(node, "scale");
	float scalef = 1;
	if(scale.isValid())
		scalef = *scale;

	MObject camnode = FindCamera(node);
	if(camnode.isNull()) return false;
	std::string cameraname = MFnDependencyNode(camnode).name().asChar();
	
	Camera(camnode, render, scalef);
	Options(camnode, render);

	{
		// ���������� ������������ ������
		prescene->PushAttributes();

		cls::P<float> lightwarp		= ocExport_GetAttrValue(node, "lightwarp");
		cls::PS envmap				= ocExport_GetAttrValue(node, "envmap");
		cls::PS envspace			= ocExport_GetAttrValue(node, "envspace");
		cls::P<float> maxpixdist	= ocExport_GetAttrValue(node, "maxpixdist");
		cls::P<int> distr			= ocExport_GetAttrValue(node, "distr");
		cls::P<float> bias			= ocExport_GetAttrValue(node, "bias");
		cls::P<int> hitmode			= ocExport_GetAttrValue(node, "hitmode");
		cls::P<bool> falloffmode	= ocExport_GetAttrValue(node, "falloffmode");
		cls::P<float> falloff		= ocExport_GetAttrValue(node, "falloff");
		cls::P<int>	samples			= ocExport_GetAttrValue(node, "samples");
		cls::P<float> maxdist		= ocExport_GetAttrValue(node, "maxdist");
		cls::P<float> coneangle		= ocExport_GetAttrValue(node, "coneangle");
		cls::P<int>	sides			= ocExport_GetAttrValue(node, "sides");
		
		std::string distrname = "";
		switch(*distr)
		{
		case 0: distrname = "cosin"; break;
		case 1: distrname = "uniform"; break;
		}
		std::string sidesname = "";
		switch(*sides)
		{
		case 0: sidesname = "front"; break;
		case 1: sidesname = "back"; break;
		case 2: sidesname = "both"; break;
		}
		
		std::string hitmodename = "";
		switch(*sides)
		{
		case 0: hitmodename = "default"; break;
	//	case 1: hitmodename = "back"; break;
	//	case 2: hitmodename = "both"; break;
		}

		prescene->Parameter("lightwarp", lightwarp);
		prescene->Parameter("envmap", envmap);
		prescene->Parameter("envspace", envspace);
		prescene->Parameter("maxpixdist", maxpixdist);
		prescene->Parameter("distr", distrname.c_str());
		prescene->Parameter("bias", bias);
		prescene->Parameter("falloffmode", falloffmode);
		prescene->Parameter("falloff", falloff);
		prescene->Parameter("samples", samples);
		prescene->Parameter("maxdist", maxdist);
		prescene->Parameter("coneangle", coneangle);
		prescene->Parameter("sides", sidesname.c_str());
		prescene->Shader("surface", "OcclOccelaris");
		/*/
		std::string format = "invcolor";
		float maxsamples = 256;
		float maxdist = 50.f;
		float maxerror = 0.5f; 
		float maxpixeldist = 100.f;
		std::string distribution = "cosine";
		float bias = -1.f;
		float coneangle = 1.309f;

		render->Parameter("format", format.c_str());
		render->Parameter("maxsamples", maxsamples);
		render->Parameter("maxdist", maxdist);
		render->Parameter("maxerror", maxerror);
		render->Parameter("maxpixeldist", maxpixeldist);
		render->Parameter("distribution", distribution.c_str());
		render->Parameter("bias", bias);
		render->Parameter("coneangle", coneangle);
		render->Shader("surface", "ratOcclusion");
		/*/
		prescene->PopAttributes();

		prescene->Attribute("shader::ignoreSurface", true);

		prescene->Attribute( "visibility::camera", 1);
		prescene->Attribute( "visibility::trace", 1);
		prescene->Attribute( "visibility::photon", 1);
	//	prescene->Attribute( "string transmission" ["opaque"]
	}
	return true;
}
/*/
//! ���������� � ������ World ����� ������
void PassGenerator_Occ::OnPreScene(
	MObject& node,			//!< �������� ������ ���������� � �������
	cls::IRender* render,		//!< render
	cls::IExportContext* context
	)
{
}
/*/
//! ������ ��������� � �������?
bool PassGenerator_Occ::IsObjectInPass(
	MObject& node,			//!< �������� ������ ���������� � �������
	cls::IRender* render			//!< render
	)
{
	return true;
}
