#include "stdafx.h"

#include "Util/misc_create_directory.h"
#include "ocellaris/IPassGenerator.h"
#include "ocellaris/renderFileImpl.h"

//! pass ��������� ��� �������� ��������
//! ������������ � ocTranslator
//! OUTPUTFILENAME
//! OUTPUTFORMAT
//! SRCFILENAME
//! SRCHOSTNAME
//! 
struct AnimationExport_PassGenerator : public cls::IPassGenerator
{
public:
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "AnimationExport_PassGenerator";};

	// ���� ������ ����������� ������� ������� � ������ ����� ��� ���
	virtual bool IsUsedInFrame(
		cls::INode& node, 
		float frame, 			
		cls::IExportContext* context
		){return true;}
	/*/
	// ������ ������� BlurGenerator ������������ �� ���������
	// ����� ������� NULL
	virtual cls::IBlurGenerator* GetDefaultBlurGenerator( 
		cls::INode& node, 
		cls::IExportContext* context
		){return NULL;}
	/*/
	//! ������� ������ � ��������
	virtual bool OnExport(
		cls::INode& node,				//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IRender* scenecache,		//!< ��� �����
		Math::Box3f scenebox,			//!< box �����
		cls::IRender* subpasscache,		//!< ��� ����������� (�� ���� ������� OnExportParentPass)
		cls::IExportContext* context	// 
		);

	// ���������� � ���� ����������� ��� �������� �������
	// ��������, ������ shadowpass ������ �������� � ������� ��� �������� ����
	virtual void OnExportParentPass( 
		cls::INode& subnode,				//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IRender* render, 
		cls::IExportContext* context
		){}
};

AnimationExport_PassGenerator animationexport_passgenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl AnimationExportPass()
	{
		return &animationexport_passgenerator;
	}
}


bool AnimationExport_PassGenerator::OnExport(
	cls::INode& node,				//!< ������ ���������� ������� (�.�., kNullObj)
	cls::IRender* scenecache,		//!< ��� �����
	Math::Box3f scenebox,			//!< box �����
	cls::IRender* subpasscache,		//!< ��� ����������� (�� ���� ������� OnExportParentPass)
	cls::IExportContext* context	// 
	)
{
	std::string filename = context->getEnvironment("OUTPUTFILENAME");
	cls::IRender* ctx = context->objectContext(node.getObject(), this);
	if( ctx)
	{
		ctx->GetAttribute("filename", filename);
	}

	std::string format   = context->getEnvironment("OUTPUTFORMAT");
	std::string comment1 = context->getEnvironment("SRCFILENAME");
	std::string comment2 = context->getEnvironment("SRCHOSTNAME");
	std::string framenum = context->getEnvironment("FRAMENUMBER");
	bool bAscii = true;
	if(format=="ascii")
		bAscii = true;
	if(format=="binary")
		bAscii = false;

	char buf[512];
	_snprintf(buf, sizeof(buf), filename.c_str(), framenum);
	filename = buf;

	// �������� �������:
	Util::create_directory_for_file(filename.c_str());
	cls::renderFileImpl filerender;
	if( !filerender.openForWrite(filename.c_str(), bAscii))
	{
		context->error("Export: renderFileImpl cant create \"%s\" file!!!", filename.c_str());
		return false;
	}
	cls::IRender* render = &filerender;

	render->comment(comment1.c_str());
	render->comment(comment2.c_str());
	render->Attribute("file::box", scenebox);
//		scenecache->RenderGlobalAttributes(&_render);
	render->PushAttributes();
	scenecache->Render(render);
	render->PopAttributes();

	return true;
}
