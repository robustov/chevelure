#pragma once

#include "ocellaris/OcellarisExport.h"
#include "ocellaris/IPassGenerator.h"
class MFnCamera;
//! @ingroup implement_group
//! \brief ����������� pass ��������� ��� ���������� �������
//! 
struct PassGenerator_Base : public cls::IPassGenerator
{

protected:
	// �������� ��������� ������ � render
	OCELLARISEXPORT_API bool CameraAttributes(
		MObject& node,				//!< ������ (�.�., kNullObj)
		cls::IRender* render		//!< render
		);

	// SearchPath
	std::string getSearchPath(
		cls::IExportContext* context, 
		const char* type);

protected:
	std::string getMtorValue(const char* name);

	void getCameraInfo( MFnCamera& cam, int cam_width, int cam_height, double fov_ratio );

	void portFieldOfView( 
		int port_width, int port_height,
		double& horizontal,
		double& vertical,
		MFnCamera& fnCamera );

	void computeViewingFrustum ( 
		double     window_aspect,
		double&    left,
		double&    right,
		double&    bottom,
		double&    top,
		MFnCamera& cam );

	static float MM_TO_INCH;

	// ��� RiProcedural
	static void dl_FurProceduralFree(
		void* data
		);
};
