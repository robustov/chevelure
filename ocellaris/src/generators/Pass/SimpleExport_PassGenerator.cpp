#include "stdafx.h"

#include "Util/misc_create_directory.h"
#include "ocellaris/IPassGenerator.h"
#include "ocellaris/renderFileImpl.h"
#include "ocellaris/renderBlurParser.h"

//! pass ��������� ��� �������� ��������
//! ������������ � ocTranslator
//! OUTPUTFILENAME
//! OUTPUTFORMAT
//! SRCFILENAME
//! SRCHOSTNAME
//! 
struct SimpleExport_PassGenerator : public cls::IPassGenerator
{
public:
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "SimpleExport_PassGenerator";};

	// ���� ������ ����������� ������� ������� � ������ ����� ��� ���
	virtual bool IsUsedInFrame(
		cls::INode& node, 
		float frame, 			
		cls::IExportContext* context
		){return true;}

	virtual bool AddExportFrames(
		cls::INode& pass, 
		cls::INode& node, 
		float renderframe, 			
		cls::IExportContext* context
		);

	// ������� ��� ����������� ���� �� ExportFrame 
	// ���� ����� ���� ���������� IBlurGenerator
	bool BuildNodeFromExportFrame(
		cls::INode& pass,				// pass
		cls::INode& node,				// pass
		cls::IRender* render,			// ������
		float frame,
		Math::Matrix4f& transform,		// ����������� ����� transform
		Math::Box3f& box,				// ����������� ���� ��� �������
		cls::IExportContext* context	// �������� ��������
		);

	//! ������� ������ � ��������
	virtual bool OnExport(
		cls::INode& node,				//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IRender* scenecache,		//!< ��� �����
		Math::Box3f scenebox,			//!< box �����
		cls::IRender* subpasscache,		//!< ��� ����������� (�� ���� ������� OnExportParentPass)
		cls::IExportContext* context	// 
		);

	// ���������� � ���� ����������� ��� �������� �������
	// ��������, ������ shadowpass ������ �������� � ������� ��� �������� ����
	virtual void OnExportParentPass( 
		cls::INode& subnode,				//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IRender* render, 
		cls::IExportContext* context
		){}
};

SimpleExport_PassGenerator simpleexport_passgenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl SimpleExportPass()
	{
		return &simpleexport_passgenerator;
	}
}

bool SimpleExport_PassGenerator::AddExportFrames(
	cls::INode& pass, 
	cls::INode& node, 
	float renderframe, 			
	cls::IExportContext* context
	)
{
	const char* blur = context->getEnvironment("BLUR");
	const char* open = context->getEnvironment("BLURSHUTTEROPENFRAME");
	const char* close = context->getEnvironment("BLURSHUTTERCLOSEFRAME");

	if( !blur[0] || !open[0] || !close[0] || blur[0]=='0')
	{
		return cls::IPassGenerator::AddExportFrames(
			pass, 
			node, 
			renderframe, 			
			context
			);
	}
	else
	{
		double openframe = atof(open);
		double closeframe = atof(close);

		context->addExportTimeNode(node, renderframe, (float)openframe);
		context->addExportTimeNode(node, renderframe, (float)closeframe);
	}
	return true;
}

// ������� ��� ����������� ���� �� ExportFrame 
// ���� ����� ���� ���������� IBlurGenerator
bool SimpleExport_PassGenerator::BuildNodeFromExportFrame(
	cls::INode& pass,				// pass
	cls::INode& node,				// pass
	cls::IRender* render,			// ������
	float frame,
	Math::Matrix4f& transform,		// ����������� ����� transform
	Math::Box3f& box,				// ����������� ���� ��� �������
	cls::IExportContext* context	// �������� ��������
	)
{
	const char* blur = context->getEnvironment("BLUR");
	const char* open = context->getEnvironment("BLURSHUTTEROPENFRAME");
	const char* close = context->getEnvironment("BLURSHUTTERCLOSEFRAME");

	if( !blur[0] || !open[0] || !close[0] || blur[0]=='0')
	{
		return cls::IPassGenerator::BuildNodeFromExportFrame(
			pass,
			node,
			render,
			frame,
			transform,
			box,	
			context);
	}

	int blurcount = 2;
	bool bSubFrame = false;
	float shutterAngle = 90;

	cls::renderBlurParser<> blurParser;
	for(int i=0; i<blurcount; i++)
	{
		float exportTime = frame;
		if(i!=0) exportTime = frame + i/(blurcount-1.f);
		float blurphase = i/(blurcount-1.f);

		cls::IRender* pcache=NULL;
		Math::Box3f* pbox = NULL;
		if( !context->getNodeExportFrameCache( exportTime, node, pcache, pbox))
		{
			context->error( "getNodeExportFrameCache %s %f FAILED\n", frame, node.getName());
			return false;
		}
		pcache->RenderGlobalAttributes(render);

		blurParser.SetCurrentPhase(blurphase);
		pcache->Render(&blurParser);
		Math::Box3f wb = *pbox;
		if( wb.isvalid())
		{
			wb.transform(transform);
			box.insert(wb);
		}
	}
	blurParser.Render(render);

	return true;

}

bool SimpleExport_PassGenerator::OnExport(
	cls::INode& node,				//!< ������ ���������� ������� (�.�., kNullObj)
	cls::IRender* scenecache,		//!< ��� �����
	Math::Box3f scenebox,			//!< box �����
	cls::IRender* subpasscache,		//!< ��� ����������� (�� ���� ������� OnExportParentPass)
	cls::IExportContext* context	// 
	)
{
	// overwrite box from Environment variable "boundingbox"
	std::string boxstring = context->getEnvironment("BOUNDINGBOX");
	if( !boxstring.empty())
	{
		// parse
		Math::Box3f extbox;
		int res = sscanf(boxstring.c_str(), "%f,%f,%f,%f,%f,%f", &extbox.min.x, &extbox.min.y, &extbox.min.z, &extbox.max.x, &extbox.max.y, &extbox.max.z);
		if(res==6)
		{
			scenebox = extbox;
			context->printf("OVERWRITE BOUNDING BOX TO: min={%f %f %f} max={%f %f %f}\n", 
				extbox.min.x, extbox.min.y, extbox.min.z, 
				extbox.max.x, extbox.max.y, extbox.max.z);
		}
	}

	std::string filename = context->getEnvironment("OUTPUTFILENAME");
	cls::IRender* ctx = context->objectContext(node.getObject(), this);
	if( ctx)
	{
		ctx->GetAttribute("filename", filename);
		ctx->Attribute("box", scenebox);
	}
	
	std::string format   = context->getEnvironment("OUTPUTFORMAT");
	std::string comment0 = context->getEnvironment("OCSCOMMENT");
	std::string comment1 = context->getEnvironment("SRCFILENAME");
	std::string comment2 = context->getEnvironment("SRCHOSTNAME");
	std::string framenum = context->getEnvironment("FRAMENUMBER");
	bool bAscii = true;
	if(format=="ascii")
		bAscii = true;
	if(format=="binary")
		bAscii = false;

	int frame = atoi( framenum.c_str());
	char buf[512];
	_snprintf(buf, sizeof(buf), filename.c_str(), frame);
	filename = buf;

	// �������� �������:
	Util::create_directory_for_file(filename.c_str());
	cls::renderFileImpl filerender;
	if( !filerender.openForWrite(filename.c_str(), bAscii))
	{
		context->error("Export: renderFileImpl cant create \"%s\" file!!!", filename.c_str());
		return false;
	}
	cls::IRender* render = &filerender;

	if( !comment0.empty())
		render->comment(comment0.c_str());
	if( !comment1.empty())
		render->comment(comment1.c_str());
	if( !comment2.empty())
		render->comment(comment2.c_str());
	render->Attribute("file::box", scenebox);
	scenecache->RenderGlobalAttributes(render);
	render->PushAttributes();
	scenecache->Render(render);
	render->PopAttributes();

	return true;
}
