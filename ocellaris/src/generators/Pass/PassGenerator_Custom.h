#pragma once

#include "ocellaris/OcellarisExport.h"
#include "ocellaris/IPassGenerator.h"
#include "PassGenerator_Base.h"
#include "../generators/blurgenerator/FinalPassBlurGenerator.h"

//! @ingroup implement_group
//! \brief ����������� pass ��������� ��� ���������� �������
//! 
struct PassGenerator_Custom : public PassGenerator_Base
{
public: 
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "PassGenerator_Custom";};

	// ���� ������ ����������� ������� ������� � ������ ����� ��� ���
	virtual bool IsUsedInFrame(
		cls::INode& node, 
		float frame, 			
		cls::IExportContext* context
		)
	{
		// ������������ �� ���� ������
		return true;
	}

	//! ������� ������ � ��������
	virtual bool OnExport(
		cls::INode& node,				//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IRender* scenecache,		//!< ��� �����
		Math::Box3f scenebox,			//!< box �����
		cls::IRender* subpasscache,		//!< ��� ����������� (�� ���� ������� OnExportParentPass)
		cls::IExportContext* context		// 
		);

	// ���������� � ���� ����������� ��� �������� �������
	// ��������, ������ shadowpass ������ �������� � ������� ��� �������� ����
	// ������������� ���������� ����� OnExport()
	virtual void OnExportParentPass( 
		cls::INode& subnode,				//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IRender* render, 
		cls::IExportContext* context
		);

protected:
	MObject FindCamera(
		MObject& node,
		cls::IExportContext* context
		);
};
