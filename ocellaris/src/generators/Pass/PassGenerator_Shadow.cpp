#include "stdafx.h"
#include "ocellaris/IPassGenerator.h"
#include "PassGenerator_Base.h"
#include "ocellaris/OcellarisExport.h"
#include "ocellaris/IBlurGenerator.h"
#include "ocellaris/OcellarisExport.h"
#include "ocellaris/renderFileImpl.h"
#include "MathNMaya/MathNMaya.h"

#include <maya/MFnFluid.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MItCurveCV.h>
#include <maya/MPlug.h>
#include <maya/MPoint.h>
#include <maya/MPointArray.h>
#include <maya/MGlobal.h>
#include <maya/MDagPath.h>
#include <maya/MMatrix.h>
#include "mathNpixar/IPrman.h"
#include "mathNpixar/IPrman_ascii.h"
#include "ocellaris\renderStringImpl.h"
#include "ocellaris/renderPrmanImpl.h"
#include "Util/misc_create_directory.h"


#include "Util/misc_create_directory.h"

#include <string>

//! @ingroup implement_group
//! \brief ����������� pass ��������� ��� �����
//! 
struct PassGenerator_Shadow : public PassGenerator_Base
{
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "PassGenerator_Shadow";};

	// ���� ������ ����������� ������� ������� � ������ ����� ��� ���
	virtual bool IsUsedInFrame(
		cls::INode& node, 
		float frame, 			
		cls::IExportContext* context
		)
	{
		return true;
	}
	// ����������� ���������
	bool OnPrepare(
		cls::IExportContext* context
		);
	//! ����������� ���� � �������� 
	bool OnPrepareNode(
		cls::INode& node, 
		cls::IExportContext* context,
		MObject& generatornode
		);
	/*/
	// ������ ������� BlurGenerator ������������ �� ���������
	// ����� ������� NULL
	virtual cls::IBlurGenerator* GetDefaultBlurGenerator( 
		cls::INode& node, 
		cls::IExportContext* context
		)
	{
		return NULL;
	}
	/*/

	//! ������� ������ � ��������
	virtual bool OnExport(
		cls::INode& node,				//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IRender* scenecache,		//!< ��� �����
		Math::Box3f scenebox,			//!< box �����
		cls::IRender* subpasscache,		//!< ��� ����������� (�� ���� ������� OnExportParentPass)
		cls::IExportContext* context		// 
		);

	// ���������� � ���� ����������� ��� �������� �������
	// ��������, ������ shadowpass ������ �������� � ������� ��� �������� ����
	// ������������� ���������� ����� OnExport()
	virtual void OnExportParentPass( 
		cls::INode& subnode,				//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IRender* render, 
		cls::IExportContext* context
		);
};


PassGenerator_Shadow passshadow;

extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl PassShadow()
	{
		return &passshadow;
	}
}



// ����������� ���������
bool PassGenerator_Shadow::OnPrepare(
	cls::IExportContext* context
	)
{
	return true;
}
//! ����������� ���� � �������� 
bool PassGenerator_Shadow::OnPrepareNode(
	cls::INode& node, 
	cls::IExportContext* context,
	MObject& generatornode
	)
{
	ocExport_AddAttrType(node.getObject(), "ocsAscii", cls::PT_BOOL, cls::P<bool>(1));
	ocExport_AddAttrType(node.getObject(), "deepShadows", cls::PT_BOOL, cls::P<bool>(false));
	ocExport_AddAttrType(node.getObject(), "lazyCompute", cls::PT_BOOL, cls::P<bool>(false));
	ocExport_AddAttrType(node.getObject(), "lazyCompute", cls::PT_BOOL, cls::P<bool>(false));
	ocExport_AddAttrType(node.getObject(), "pixelSamples", cls::PT_FLOAT, cls::P<float>(1));

	// ��������� �������� �� ����
	cls::P<bool> useDepthMapShadows = ocExport_GetAttrValue(node.getObject(), "useDepthMapShadows");
	if( useDepthMapShadows.empty() || !useDepthMapShadows.data())
		return false;

	MObject camnode = node.getObject();
	context->addNode(camnode);

	return true;
}
//! ������� ������ � ��������
bool PassGenerator_Shadow::OnExport(
	cls::INode& _node,				//!< ������ ���������� ������� (�.�., kNullObj)
	cls::IRender* scenecache,		//!< ��� �����
	Math::Box3f scenebox,			//!< box �����
	cls::IRender* subpasscache,		//!< ��� ����������� (�� ���� ������� OnExportParentPass)
	cls::IExportContext* context		// 
	)
{
	MObject node = _node.getObject();
	MObject camnode = _node.getObject();

	std::string passname = _node.getName();
	Util::correctFileName(passname, '_');
	std::string outputdir = context->getEnvironment("SHADOWDIR");
	std::string ribdir = context->getEnvironment("RIBDIR");
	std::string statdir = context->getEnvironment("STATDIR");
	std::string framenumber = context->getEnvironment("FRAMENUMBER");
	int frame = atoi(framenumber.c_str());
	framenumber = "_"+framenumber;

	std::string workspacedir = context->getEnvironment("WORKSPACEDIR");
	std::string outfilename = ribdir+"\\"+passname+framenumber+".ocs";
	std::string outstatname = statdir+"\\"+passname+framenumber+".xml";
	std::string outfilenamescene = ribdir+"\\"+passname+framenumber+"_scene.ocs";
	std::string outfilenamerib = ribdir+"\\"+passname+framenumber+".rib";
	std::string outfilename_bat = ribdir+"\\"+passname+framenumber+".bat";
	std::string taskname = passname+"."+framenumber;
	std::string extenion = ".tex";
	std::string displayname = passname;
	displayname = outputdir+"/"+displayname+framenumber+extenion;
	std::string previewcmd = "sho "+displayname;

	Util::changeSlashToUnixSlash(outfilename);
	Util::changeSlashToUnixSlash(outstatname);
	Util::changeSlashToUnixSlash(outfilenamescene);
	Util::changeSlashToUnixSlash(outfilenamerib);
	Util::changeSlashToUnixSlash(outfilename_bat);
	Util::changeSlashToUnixSlash(displayname);
	Util::changeSlashToUnixSlash(previewcmd);

	cls::P<bool> ocsAscii		= ocExport_GetAttrValue(node, "ocsAscii");
	cls::P<bool> lazy_			= ocExport_GetAttrValue(node, "lazyCompute");

	bool bAscii = true;
	ocsAscii.data(bAscii);
	bool bLazy = false;
	lazy_.data(bLazy);

	cls::PA<int> resolution(cls::PI_CONSTANT, 2);
	cls::PA<float> pixelsample(cls::PI_CONSTANT, 2);
	cls::PA<int> pixelfilter(cls::PI_CONSTANT, 2);
	cls::P<float> format_ratio(1);
	cls::P<bool> deepShadows(0);
	cls::PS filtername("box");
	if( camnode==node)
	{
		cls::P<float> ps = ocExport_GetAttrValue(node, "pixelSamples");

		cls::P<int> dmapResolution	= ocExport_GetAttrValue(node, "dmapResolution");
		resolution[0] = dmapResolution.data();
		resolution[1] = dmapResolution.data();
		pixelsample[0] = ps.data();
		pixelsample[1] = ps.data();
		cls::P<int> dmapFilterSize	= ocExport_GetAttrValue(node, "dmapFilterSize");
		pixelfilter[0] = dmapFilterSize.data();
		pixelfilter[1] = dmapFilterSize.data();
		*format_ratio = 1;
	}
	else
	{
		resolution = ocExport_GetAttrValue(node, "resolution");
		resolution[1]=resolution[0];
		format_ratio	= ocExport_GetAttrValue(node, "format_ratio");
//		dspyGain		= ocExport_GetAttrValue(node, "dspyGain");
//		dspyGamma		= ocExport_GetAttrValue(node, "dspyGamma");
		pixelsample	= ocExport_GetAttrValue(node, "pixelsample");
		pixelfilter	= ocExport_GetAttrValue(node, "pixelfilter");
//		shadingrate	= ocExport_GetAttrValue(node, "shadingrate");
		filtername		= ocExport_GetAttrValue(node, "filtername");
		deepShadows		= ocExport_GetAttrValue(node, "deepShadows");
	}


	MObject attr;
	attr = ocExport_AddAttrType(camnode, "near", cls::PT_FLOAT, cls::P<float>(0.001f));
	cls::P<float> fnear = ocExport_GetAttrValue(camnode, attr);
	attr = ocExport_AddAttrType(camnode, "far", cls::PT_FLOAT, cls::P<float>(10000.f));
	cls::P<float> ffar = ocExport_GetAttrValue(camnode, attr);

//	attr = ocExport_AddAttrType(camnode, "deepshadow", cls::PT_BOOL, cls::P<bool>(false));
	cls::P<bool> deepshadow = ocExport_GetAttrValue(camnode, "deepShadows");

	std::string displaytype	= "shadow";
	std::string displaymode	= "z";

//Display "null" "null" "z" 
//Display "+M:/server2_mitka/develop-ocellaris/rmantex/shd/untitled.spotLightShape1.dsh.0001.tex" "deepshad" "deepopacity" 
	if(!deepshadow.empty() && *deepshadow)
	{
		displaytype	= "deepshad";
		displaymode	= "deepopacity";
	}

	// displayname
	{
		std::string outputfolder = context->getEnvironment("OUTPUTDIR");
		outputfolder += displayname+"/";
		Util::create_directory_for_file(displayname.c_str());

		cls::IRender* data = context->objectContext(_node.getObject(), this);
		data->Attribute("displayname", displayname.c_str());
	}

	// ������ ��� �����
	Math::Matrix4f mview;
	{
		// ������� ������ � ��������:
		Util::create_directory_for_file(outfilename.c_str());
		cls::renderFileImpl filerender;
		if( !filerender.openForWrite(outfilename.c_str(), bAscii))
		{
			context->error("Export: renderFileImpl cant create \"%s\" file!!!", outfilename.c_str());
			return false;
		}
		cls::IRender* render = &filerender;

		// CAMERA
		{
			render->PushAttributes();

			Math::Matrix4f cammatrix1;
			cls::INode* camexportnode = context->getNodeForObject(camnode);
			{
				// ��� �����
				cls::IRender* pcache=NULL;
				Math::Box3f* pbox = NULL;
				if( context->getNodeExportFrameCache( (float)frame, *camexportnode, pcache, pbox))
				{
					pcache->GetParameter("camera::transform", cammatrix1);
					render->Parameter("camera::transform", cammatrix1);
				}
			}
			mview = cammatrix1;

			this->CameraAttributes(camnode, render);

//			render->GetParameter("camera::transform", mview);

			render->Parameter("camera::near", fnear);
			render->Parameter("camera::far", ffar);

			cls::PA<float> screen(cls::PI_CONSTANT, 4);
			screen[0] = -1;
			screen[1] = 1;
			screen[2] = -1;
			screen[3] = 1;
			render->Parameter("camera::screen", screen);

			render->Parameter("camera::resolution", resolution);
			render->Parameter("camera::ratio", format_ratio);

			if( camnode.hasFn(MFn::kCamera))
			{
			}
			else if(node.hasFn(MFn::kSpotLight))
			{
			}
			render->RenderCall("Camera");
			render->PopAttributes();
		}

		// OPTIONS
		{
			render->PushAttributes();
			std::string cameraname = MFnDependencyNode(camnode).name().asChar();
			render->Parameter("output::cameraname", cameraname.c_str());

	//		render->Parameter("output::shadingrate", shadingrate);

	//		render->Parameter("output::gain", dspyGain);
	//		render->Parameter("output::gamma", dspyGamma);

			render->Parameter("output::pixelsample", pixelsample);

			render->Parameter("output::filtersize", pixelfilter);
			render->Parameter("output::filtername", filtername);

			render->Parameter("output::displayname", displayname.c_str());
			render->Parameter("output::displaytype", displaytype.c_str());
			render->Parameter("output::displaymode", displaymode.c_str());

			render->Parameter("hidden::jitter", int(0));
			if(!deepshadow.empty() && *deepshadow)
				render->Parameter("hidden::jitter", int(1));

			render->Parameter("hidden::depthfilter", "min");

			if( !statdir.empty())
			{
				render->Parameter("statistics::endofframe", (int)1);
				render->Parameter("statistics::xmlfilename", outstatname);
				Util::create_directory_for_file(outstatname.c_str());
			}

	//		render->Parameter("output::quantizeMode", quantizeMode);
	//		render->Parameter("output::quantizeOne",  quantizeOne);
	//		render->Parameter("output::quantizeMax",  quantizeMax);
	//		render->Parameter("output::quantizeMin",  quantizeMin);
	//		render->Parameter("output::quantizeDither", quantizeDither);

			render->RenderCall("Output");
			render->PopAttributes();
		}
	}

	// ������� ����� � ��������� ����
	{
		Util::create_directory_for_file(outfilenamescene.c_str());
		cls::renderFileImpl filerender;
		if( !filerender.openForWrite(outfilenamescene.c_str(), bAscii))
		{
			context->error("Export: renderFileImpl cant create \"%s\" file!!!", outfilenamescene.c_str());
			return false;
		}
		filerender.Attribute("@pass", "shadow");
		filerender.Attribute("@passname", passname.c_str());
		filerender.Attribute("@camera::worldpos", mview);

		subpasscache->Render(&filerender);
		scenecache->RenderGlobalAttributes(&filerender);
		scenecache->Render(&filerender);
	}

	if( bLazy && Util::is_file_exist( displayname.c_str()) )
		return true;

	// �������������� � ���!
	{
		/*/
#ifdef _DEBUG
	  	getIPrmanDll dllproc("IPrmanD.dll", "getIPrman");
#else
	  	getIPrmanDll dllproc("IPrman.dll", "getIPrman");
#endif
		if( !dllproc.isValid()) 
		{
			context->error("cant fount IPrman.dll or IPrmanD.dll");
			return false;
		}

		IPrman* prman = (*dllproc)();
		prman->RiBegin( (char*)outfilenamerib.c_str() );
		{
			RtString format = "ascii";
			prman->RiOption(( RtToken ) "rib", ( RtToken ) "format", &format, RI_NULL);
			RtString compression = "none";
			prman->RiOption(( RtToken ) "rib", ( RtToken ) "compression", &compression, RI_NULL);
		}
		/*/
		Prman_ascii thePrman;
		thePrman.Open(outfilenamerib.c_str());
		IPrman* prman = &thePrman;

		std::string seachpath = this->getSearchPath(context, "");

		RtString text = (RtString)seachpath.c_str();
		prman->RiOption( (RtToken )"searchpath", (RtToken )"shader", ( RtPointer )&text, RI_NULL);
		prman->RiOption( (RtToken )"searchpath", (RtToken )"texture", ( RtPointer )&text, RI_NULL);
		prman->RiOption( (RtToken )"searchpath", (RtToken )"procedural", ( RtPointer )&text, RI_NULL);

		prman->RiFrameBegin(frame);

		// ������ ������ � ���������
		cls::renderPrmanImpl<> renderprman(prman);
		cls::renderFileImpl filerender;
		if( !filerender.openForRead(outfilename.c_str()))
		{
			context->error("Export: renderFileImpl cant open \"%s\" file!!!", outfilename.c_str());
			return false;
		}
		filerender.Render( &renderprman);

		prman->RiWorldBegin();
		// RiProcedural
		{
			char buf[2048];
			// ���������� � ������
			cls::renderStringImpl stringrender;
			stringrender.openForWrite();
			subpasscache->Render(&stringrender);
			std::string addstream = stringrender.getWriteBuffer();

			float shutterOpenFrame = 1/24.f;
			float shutterCloseFrame = 1/24.f;
			const char* ocs = outfilenamescene.c_str();
			_snprintf(buf, 1224, "%s$%f$%f$%f$%s", 
				ocs, 
				(float)frame, shutterOpenFrame, shutterCloseFrame, "");

			addstream = buf+addstream;

			RtBound bound;
			IPrman::copy(bound, scenebox);
			RtString *data = static_cast<RtString*>(malloc(sizeof(RtString) * 3));
			data[0] = _strdup("ocellarisPrmanDSO.dll");
			data[1] = _strdup(addstream.c_str());
			data[2] = 0;
			prman->RiProcedural(data, bound, prman->RiProcDynamicLoad(), dl_FurProceduralFree);
		}

		prman->RiWorldEnd();
		prman->RiFrameEnd();

		prman->RiEnd();
	}

	// ������� ��� �������:
	std::string localcmd;
	{
		localcmd = context->getEnvironment( "RENDERSHADOWCOMMAND");
		if(localcmd.empty())
			localcmd = "prman.exe -p:1 -Progress";
		localcmd += " ";
		localcmd += "\""+outfilenamerib+"\"";
		FILE* file = fopen(outfilename_bat.c_str(), "wt");
		if( file)
		{
			std::string path_env = context->getEnvironment("PATH");
			if( !path_env.empty())
				fprintf(file, "set PATH=%%PATH%%;%s\n", path_env.c_str());

			fputs( localcmd.c_str(), file);
			fputs( "\n", file);

			fprintf(file, "ocellarisTest.exe \"%s\" \"%s.ocs\" -ocs -exe\n", outfilenamescene.c_str(), outfilenamescene.c_str());
			fprintf(file, "ocellarisTest.exe \"%s\" \"%s.rib\" -rib -exe\n", outfilenamescene.c_str(), outfilenamescene.c_str());
			fprintf(file, "pause\n");

			fclose(file);
		}
	}

	// ������!
	{
		cls::ITask* task = context->addTask(NULL);
		task->Parameter("name", cls::PS(taskname));
		task->Parameter("cmd", cls::PS(localcmd));
		if( !previewcmd.empty())
			task->Parameter("preview", cls::PS(previewcmd.c_str()));
	}

	return true;
}

// ���������� � ���� ����������� ��� �������� �������
// ��������, ������ shadowpass ������ �������� � ������� ��� �������� ����
// ������������� ���������� ����� OnExport()
void PassGenerator_Shadow::OnExportParentPass( 
	cls::INode& subnode,				//!< ������ ���������� ������� (�.�., kNullObj)
	cls::IRender* render, 
	cls::IExportContext* context
	)
{
	// ��� �������� ����������� � light ��������
	cls::IRender* data = context->objectContext(subnode.getObject(), this);
	std::string displayname;
	data->GetAttribute("displayname", displayname);

	// ��� �������� � ��� �������� ���� ���� ��������
	std::string light_name = subnode.getName();
	Util::correctFileName(light_name, '_');
	std::string attrname = light_name;
	attrname += "_shadowName";

	render->Attribute(attrname.c_str(), displayname.c_str());
}























/*/

//! OnInit
bool PassGenerator_Shadow::OnInit(
	MObject& node,
	cls::IExportContext* context,
	std::string& passname,
	std::vector<cls::SubGenerator>& subGenerators	// ������ ��������������
	)
{
	MObject camnode = FindCamera(node);
	if(camnode.isNull())
		return false;

	if( node==camnode)
	{
		cls::P<bool> useDepthMapShadows = ocExport_GetAttrValue(node, "useDepthMapShadows");
		if( useDepthMapShadows.empty() || !useDepthMapShadows.data())
		{
			return false;
		}
	}
	passname = "shadow";
	return true;
}
// ���������� ����� ��������� ��� ��������
bool PassGenerator_Shadow::isLazy(
	MObject& node,
	cls::IExportContext* context
	)
{
	std::string displayname = MFnDependencyNode(node).name().asChar();

	MObject attr;
	attr = ocExport_AddAttrType(node, "lazy", cls::PT_BOOL, cls::P<bool>(false));
	cls::P<bool> bLazy = ocExport_GetAttrValue(node, attr);
	if( bLazy.empty() || !bLazy.data())
		return false;

	std::string outputfolder = context->getEnvironment("OUTPUTDIR");
	outputfolder += displayname+"/";

	return true;
}

//! ������� ������ � ��������
bool PassGenerator_Shadow::OnExport(
	MObject& node,			//!< ������ (�.�., kNullObj)
	cls::IExportContext* context,
	cls::IRender* render,			//!< render
	cls::IRender* prescene,			//!< pre scene
	cls::IRender* prenextgenerator	//!< pre next generator scene
	)
{
	std::string displayname = MFnDependencyNode(node).name().asChar();
	std::string displayname_nocrop;

	MObject camnode = FindCamera(node);

	cls::PA<int> resolution(cls::PI_CONSTANT, 2);
	cls::PA<float> pixelsample(cls::PI_CONSTANT, 2);
	cls::PA<int> pixelfilter(cls::PI_CONSTANT, 2);
	cls::P<float> format_ratio(1);
	cls::PS filtername("box");
	if( camnode==node)
	{
		cls::P<int> dmapResolution	= ocExport_GetAttrValue(node, "dmapResolution");
		resolution[0] = dmapResolution.data();
		resolution[1] = dmapResolution.data();
		pixelsample[0] = 1;
		pixelsample[1] = 1;
		cls::P<int> dmapFilterSize	= ocExport_GetAttrValue(node, "dmapFilterSize");
		pixelfilter[0] = dmapFilterSize.data();
		pixelfilter[1] = dmapFilterSize.data();
		*format_ratio = 1;
	}
	else
	{
		resolution = ocExport_GetAttrValue(node, "resolution");
		resolution[1]=resolution[0];
		format_ratio	= ocExport_GetAttrValue(node, "format_ratio");
//		dspyGain		= ocExport_GetAttrValue(node, "dspyGain");
//		dspyGamma		= ocExport_GetAttrValue(node, "dspyGamma");
		pixelsample	= ocExport_GetAttrValue(node, "pixelsample");
		pixelfilter	= ocExport_GetAttrValue(node, "pixelfilter");
//		shadingrate	= ocExport_GetAttrValue(node, "shadingrate");
		filtername			= ocExport_GetAttrValue(node, "filtername");
	}


	MObject attr;
	attr = ocExport_AddAttrType(camnode, "near", cls::PT_FLOAT, cls::P<float>(0.001f));
	cls::P<float> fnear = ocExport_GetAttrValue(camnode, attr);
	attr = ocExport_AddAttrType(camnode, "far", cls::PT_FLOAT, cls::P<float>(10000.f));
	cls::P<float> ffar = ocExport_GetAttrValue(camnode, attr);

	attr = ocExport_AddAttrType(camnode, "deepshadow", cls::PT_BOOL, cls::P<bool>(false));
	cls::P<bool> deepshadow = ocExport_GetAttrValue(camnode, attr);

//	cls::P<float> dspyGain		= ocExport_GetAttrValue(node, "dspyGain");
//	cls::P<float> dspyGamma		= ocExport_GetAttrValue(node, "dspyGamma");

//	std::string filtername = "box";
	std::string displaytype	= "shadow";
	std::string displaymode	= "z";

	if(!deepshadow.empty() && *deepshadow)
	{
		displaytype	= "deepshad";
		displaymode	= "deepopacity";
	}

	// displayname
	{
		std::string outputfolder = context->getEnvironment("OUTPUTDIR");
		outputfolder += displayname+"/";
	}

	// CAMERA
	Math::Matrix4f mview;
	{
		render->PushAttributes();
		this->CameraAttributes(camnode, render);
		render->GetParameter("camera::transform", mview);

		render->Parameter("camera::near", fnear);
		render->Parameter("camera::far", ffar);

		cls::PA<float> screen(cls::PI_CONSTANT, 4);
		screen[0] = -1;
		screen[1] = 1;
		screen[2] = -1;
		screen[3] = 1;
		render->Parameter("camera::screen", screen);

		render->Parameter("camera::resolution", resolution);
		render->Parameter("camera::ratio", format_ratio);

		if( camnode.hasFn(MFn::kCamera))
		{
		}
		else if(node.hasFn(MFn::kSpotLight))
		{
		}
		render->RenderCall("Camera");
		render->PopAttributes();
	}

	// OPTIONS
	{
		render->PushAttributes();
		std::string cameraname = MFnDependencyNode(camnode).name().asChar();
		render->Parameter("output::cameraname", cameraname.c_str());

//		render->Parameter("output::shadingrate", shadingrate);

//		render->Parameter("output::gain", dspyGain);
//		render->Parameter("output::gamma", dspyGamma);

		render->Parameter("output::pixelsample", pixelsample);

		render->Parameter("output::filtersize", pixelfilter);
		render->Parameter("output::filtername", filtername);

		render->Parameter("output::displayname", displayname.c_str());
		render->Parameter("output::displaytype", displaytype.c_str());
		render->Parameter("output::displaymode", displaymode.c_str());

		render->Parameter("hidden::jitter", int(0));
		render->Parameter("hidden::depthfilter", "min");

//		render->Parameter("output::quantizeMode", quantizeMode);
//		render->Parameter("output::quantizeOne",  quantizeOne);
//		render->Parameter("output::quantizeMax",  quantizeMax);
//		render->Parameter("output::quantizeMin",  quantizeMin);
//		render->Parameter("output::quantizeDither", quantizeDither);

		render->RenderCall("Output");
		render->PopAttributes();
	}

	// PRE SCENE
	{
		prescene->Shader("surface", "null");
		prescene->Attribute("shader::ignoreSurface", true);
		prescene->Attribute("@passtype", "shadow");
		prescene->Attribute("@pass", displayname.c_str());
		prescene->Attribute("@camera::worldpos", mview);
	}

	// NEXT GEN
	{
		MFnLight light(camnode);
		MString light_name = light.name();
		std::string attrname = light_name.asChar();
		attrname += "_shadowName";
		prenextgenerator->Attribute(attrname.c_str(), displayname_nocrop.c_str());
	}

	return true;
}
// ���������� � sub �����������, ��� OnPreScene � �������� �����
void PassGenerator_Shadow::OnPostGeneratorScene(
	MObject& node,			//!< ������ ���������� ������� (�.�., kNullObj)
	cls::IRender* render,		//!< render
	cls::IExportContext* context
	)
{
	MObject camnode = FindCamera(node);

	const char* outfile = (const char*)context->getObjectData(node, this);
	if( !outfile) return;
	MFnLight light(camnode);
	MString light_name = light.name();
	std::string attrname = light_name.asChar();
	attrname += "_shadowName";

	render->Attribute(attrname.c_str(), outfile);
}
	/*/


