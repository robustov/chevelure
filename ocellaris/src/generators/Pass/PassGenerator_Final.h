#pragma once

#include "ocellaris/OcellarisExport.h"
#include "ocellaris/IPassGenerator.h"
#include "PassGenerator_Base.h"
#include "../generators/blurgenerator/FinalPassBlurGenerator.h"

//! @ingroup implement_group
//! \brief ����������� pass ��������� ��� ���������� �������
//! 
struct PassGenerator_Final : public PassGenerator_Base
{
	// ������ ���� ���� -> set
	std::map<cls::INode*, MObject> setforpass;

public: 
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "PassGenerator_Final";};

	// ����������� ��������� (���������� 1 ��� ����� ������� ��������)
	virtual bool OnPrepare(
		cls::IExportContext* context
		);

	// ���� ������ ����������� ������� ������� � ������ ����� ��� ���
	virtual bool IsUsedInFrame(
		cls::INode& node, 
		float frame, 			
		cls::IExportContext* context
		)
	{
		// ������������ �� ���� ������
		return true;
	}
	//! ����������� ���� � �������� 
	virtual bool OnPrepareNode(
		cls::INode& node, 
		cls::IExportContext* context,
		MObject& generatornode
		);

	// ���������� ����� ��������� ������ ����
	virtual bool OnExportNode(
		cls::INode& node,				//!< ������ ������� ���� �� �������
		cls::IRender* render,			//!< render
		cls::IExportContext* context,	//!< context
		cls::INode& passnode,			//!< ������ �������
		MObject& generatornode			//!< generator node �.� 0
		);

	virtual bool AddExportFrames(
		cls::INode& pass, 
		cls::INode& node, 
		float renderframe, 			
		cls::IExportContext* context
		);
	// ������� ��� ����������� ���� �� ExportFrame 
	// ���� ����� ���� ���������� IBlurGenerator
	virtual bool BuildNodeFromExportFrame(
		cls::INode& pass,				// pass
		cls::INode& node,				// pass
		cls::IRender* render,			// ������
		float frame,
		Math::Matrix4f& transform,		// ����������� ����� transform
		Math::Box3f& box,				// ����������� ���� ��� �������
		cls::IExportContext* context	// �������� ��������
		);
	/*/
	// ������ ������� BlurGenerator ������������ �� ���������
	// ����� ������� NULL
	virtual cls::IBlurGenerator* GetDefaultBlurGenerator( 
		cls::INode& node, 
		cls::IExportContext* context
		);
	/*/

	//! ������� ������ � ��������
	virtual bool OnExport(
		cls::INode& node,				//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IRender* scenecache,		//!< ��� �����
		Math::Box3f scenebox,			//!< box �����
		cls::IRender* subpasscache,		//!< ��� ����������� (�� ���� ������� OnExportParentPass)
		cls::IExportContext* context		// 
		);

	// ���������� � ���� ����������� ��� �������� �������
	// ��������, ������ shadowpass ������ �������� � ������� ��� �������� ����
	// ������������� ���������� ����� OnExport()
	virtual void OnExportParentPass( 
		cls::INode& subnode,				//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IRender* render, 
		cls::IExportContext* context
		);
	//! ���������� ����� �������� ���� ������
	virtual bool OnPostExport(
		cls::INode& node,				//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IExportContext* context	// 
		);

// �������:
public:
	MObject FindCamera(
		MObject& node,
		cls::IExportContext* context
		);

	//! ������� ������ � ��������
	//! ����������
	bool OnExport(
		const char* _passname,			// ��� �������
		cls::INode& _node,				//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IRender* scenecache,		//!< ����������� � ��� �����
		Math::Box3f scenebox,			//!< box �����
		cls::IRender* subpasscache,		//!< ��� ����������� (�� ���� ������� OnExportParentPass)
		cls::IExportContext* context	// 
		);

	// ��������� ��������� �� ���� � ����
	bool IsNodeInSet(cls::INode& node, const MFnSet& setfn);
};
