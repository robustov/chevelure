#include "stdafx.h"
//#include "FluidGenerator.h"
#include "ocellaris/OcellarisExport.h"
#include "ocellaris/renderFileImpl.h"
#include "MathNMaya/MathNMaya.h"

#include <maya/MFnFluid.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MItCurveCV.h>
#include <maya/MPlug.h>
#include <maya/MPoint.h>
#include <maya/MPointArray.h>
#include <maya/MGlobal.h>
#include <maya/MDagPath.h>
#include <maya/MMatrix.h>
#include "PassGenerator_MtorFinal.h"
#include <string>

//! ����� ������
MObject PassGenerator_MtorFinal::FindCamera(
	MObject& node			//!< ������ (�.�., kNullObj)
	)
{
	// ����� ������
	MObject camnode;
	std::string cameraname = getMtorValue("camName");

	if( !node.isNull() && node.hasFn(MFn::kCamera))
	{
		camnode = node;
		cameraname = MFnDependencyNode(camnode).name().asChar();
	}
	else
	{
		nodeFromName(cameraname.c_str(), camnode);
	}
	if(camnode.isNull())
		return MObject::kNullObj;

	return camnode;
}

//! OnInit
bool PassGenerator_MtorFinal::OnInit(
	MObject& node,
	cls::IExportContext* context,
	std::string& passname,
	std::vector<cls::SubGenerator>& subGenerators	// ������ ��������������
	)
{
	{
		MFnDependencyNode dn(node);
		printf("%s\n", node.apiTypeStr());
		std::string cameraname = dn.name().asChar();
		printf("%s\n", cameraname.c_str());
	}

	MObject camnode = FindCamera(node);
	if(camnode.isNull()) return false;
	std::string cameraname = MFnDependencyNode(camnode).name().asChar();

	passname = "final";
	return true;
}
//! ������� ������ � ��������
bool PassGenerator_MtorFinal::OnExport(
	MObject& node,			//!< ������ (�.�., kNullObj)
	cls::IExportContext* context,
	cls::IRender* render,			//!< render
	cls::IRender* prescene,			//!< pre scene
	cls::IRender* prenextgenerator	//!< pre next generator scene
	)
{
	MObject camnode = FindCamera(node);
	if(camnode.isNull()) return false;
	std::string cameraname = MFnDependencyNode(camnode).name().asChar();
	
	Camera(camnode, render);
	Options(camnode, render);

	return true;
}
/*/
//! ���������� � ������ World ����� ������
void PassGenerator_MtorFinal::OnPreScene(
	MObject& node,			//!< �������� ������ ���������� � �������
	cls::IRender* render,		//!< render
	cls::IExportContext* context
	)
{
}
/*/
//! ������ ��������� � �������?
bool PassGenerator_MtorFinal::IsObjectInPass(
	MObject& node,			//!< �������� ������ ���������� � �������
	cls::IRender* render			//!< render
	)
{
	// ��� �����!!!
	return true;
}


//! ������� ������
bool PassGenerator_MtorFinal::Camera(
	MObject& node,			//!< ������ (�.�., kNullObj)
	cls::IRender* render,			//!< render
	float scaleresolution
	)
{
	if( !node.hasFn(MFn::kCamera) && 
		!node.hasFn(MFn::kSpotLight))
	{
		return false;
	}

	render->PushAttributes();
	this->CameraAttributes(node, render);

	if( node.hasFn(MFn::kCamera))
	{
		// resolution
		cls::PA<int> resolution(cls::PI_CONSTANT, 2);
		resolution[0] = atoi( getMtorValue("dspyFormatX").c_str());
		resolution[1] = atoi( getMtorValue("dspyFormatY").c_str());
		resolution[0] = (int)(resolution[0]*scaleresolution);
		resolution[1] = (int)(resolution[1]*scaleresolution);
		render->Parameter("camera::resolution", resolution);

		float format_ratio = (float)atof( getMtorValue("pixelRatio").c_str());
		render->Parameter("camera::ratio", format_ratio);

		// screen
		float sw_x = 1;
		float sw_y = resolution[1]/(float)resolution[0];
		cls::PA<float> screen(cls::PI_CONSTANT, 4);
		screen[0] = -sw_x;
		screen[1] = sw_x;
		screen[2] = -sw_y;
		screen[3] = sw_y;
		render->Parameter("camera::screen", screen);

	}
	else if(node.hasFn(MFn::kSpotLight))
	{
	}
	render->RenderCall("Camera");
	render->PopAttributes();
	return true;
}
//! ������� �������� ��� ��� ��� ��������
bool PassGenerator_MtorFinal::Options(
	MObject& node,			//!< ������ (�.�., kNullObj)
	cls::IRender* render			//!< render
	)
{

	render->PushAttributes();
	std::string cameraname = MFnDependencyNode(node).name().asChar();
	render->Parameter("output::cameraname", cameraname.c_str());

	// Exposure
	float dspyGain = (float)atof( getMtorValue("dspyGain").c_str());
	float dspyGamma = (float)atof( getMtorValue("dspyGamma").c_str());

	// RiPixelSamples
	cls::PA<float> pixelsample(cls::PI_CONSTANT, 2);
	pixelsample[0] = (float)atof( getMtorValue("pixelSamplesX").c_str());
	pixelsample[1] = (float)atof( getMtorValue("pixelSamplesY").c_str());

	//
	cls::PA<int> pixelfilter(cls::PI_CONSTANT, 2);
	pixelfilter[0] = atoi( getMtorValue("filterWidthX").c_str());
	pixelfilter[1] = atoi( getMtorValue("filterWidthY").c_str());
	std::string filter = getMtorValue("pixelFilter");

	float shadingrate = (float)atof( getMtorValue("shadingRate").c_str());

//	render->PushAttributes();

	render->Parameter("output::shadingrate", shadingrate);

	render->Parameter("output::gain", dspyGain);
	render->Parameter("output::gamma", dspyGamma);

	render->Parameter("output::pixelsample", pixelsample);

	render->Parameter("output::filtersize", pixelfilter);
	render->Parameter("output::filtername", filter.c_str());

	// quantize
	std::string dspyQuantizeMode = getMtorValue("dspyQuantizeMode");
	int dspyQuantizeOne = (int)atoi( getMtorValue("dspyQuantizeOne").c_str());
	int dspyQuantizeMin = (int)atoi( getMtorValue("dspyQuantizeMin").c_str());
	int dspyQuantizeMax = (int)atoi( getMtorValue("dspyQuantizeMax").c_str());
	float dspyQuantizeDither = (float)atof( getMtorValue("dspyQuantizeDither").c_str());
	render->Parameter("output::quantizeMode", dspyQuantizeMode.c_str());
	render->Parameter("output::quantizeOne", dspyQuantizeOne);
	render->Parameter("output::quantizeMin", dspyQuantizeMin);
	render->Parameter("output::quantizeMax", dspyQuantizeMax);
	render->Parameter("output::quantizeDither", dspyQuantizeDither);

	// display
	std::string dspyServer = getMtorValue("dspyServer");
	std::string dspyServerMode = getMtorValue("dspyServerMode");
	std::string displayname = "foo";
	render->Parameter("output::displayname", displayname.c_str());
	render->Parameter("output::displaytype", dspyServer.c_str());
	render->Parameter("output::displaymode", dspyServerMode.c_str());

	render->RenderCall("Output");
	render->PopAttributes();

	return true;
}
