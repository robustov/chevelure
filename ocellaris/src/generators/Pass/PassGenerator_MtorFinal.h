#pragma once

#include "ocellaris/IPassGenerator.h"
#include "PassGenerator_Base.h"

//! @ingroup implement_group
//! \brief ����������� pass ��������� ��� ���������� ������� ������������ ������ �� mtor
//! 
struct PassGenerator_MtorFinal : public PassGenerator_Base
{
	//! ����� ������
	MObject FindCamera(
		MObject& node			//!< ������ (�.�., kNullObj)
		);
	//! ������� ������
	OCELLARISEXPORT_API bool Camera(
		MObject& node,			//!< ������ (�.�., kNullObj)
		cls::IRender* render,	//!< render
		float scaleresolution=1.f
		);
	//! ������� �������� ��� ��� ��� ��������
	OCELLARISEXPORT_API bool Options(
		MObject& node,			//!< ������ (�.�., kNullObj)
		cls::IRender* render			//!< render
		);
public:
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "PassGenerator_Final";};

	//! OnInit ������� ����� ������?
	OCELLARISEXPORT_API virtual bool OnInit(
		MObject& node,			//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IExportContext* context,
		std::string& passname,
		std::vector<cls::SubGenerator>& subGenerators	// ������ ��������������
		);
	//! ������� ������ � ��������
	OCELLARISEXPORT_API virtual bool OnExport(
		MObject& node,			//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IExportContext* context,
		cls::IRender* render,			//!< render
		cls::IRender* prescene,			//!< pre scene
		cls::IRender* prenextgenerator	//!< pre next generator scene
		);
	/*/
	//! ���������� � ������ World ����� ������
	OCELLARISEXPORT_API virtual void OnPreScene(
		MObject& node,			//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IRender* render,		//!< render
		cls::IExportContext* context
		);
	/*/
	//! ������ ��������� � �������?
	OCELLARISEXPORT_API virtual bool IsObjectInPass(
		MObject& node,			//!< �������� ������ ���������� � �������
		cls::IRender* render			//!< render
		);
};
