#include "stdafx.h"
#include "ocellaris/OcellarisExport.h"
#include "ocellaris/renderFileImpl.h"
#include "MathNMaya/MathNMaya.h"
#include <Maya/MFnRenderLayer.h>

#include "PassGenerator_Final.h"

//! @ingroup implement_group
//! \brief ����������� pass ��������� ��� ������� renderLayer
//! 
struct PassGenerator_RenderLayer : public PassGenerator_Final
{
public: 
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "PassGenerator_RenderLayer";};

	//! ����������� ���� � �������� 
	virtual bool OnPrepareNode(
		cls::INode& node, 
		cls::IExportContext* context,
		MObject& generatornode
		);

	// ���� ������ ����������� ������� ������� � ������ ����� ��� ���
	virtual bool IsUsedInFrame(
		cls::INode& node, 
		float frame, 			
		cls::IExportContext* context
		);
	/*/
	// ������ ������� BlurGenerator ������������ �� ���������
	// ����� ������� NULL
	virtual cls::IBlurGenerator* GetDefaultBlurGenerator( 
		cls::INode& node, 
		cls::IExportContext* context
		);
	/*/

	// ���������� ����� ��������� ������ ����
	virtual bool OnExportNode(
		cls::INode& node,				//!< ������ ������� ���� �� �������
		cls::IRender* render,			//!< render
		cls::IExportContext* context,	//!< context
		cls::INode& passnode,			//!< ������ �������
		MObject& generatornode			//!< generator node �.� 0
		);

	//! ������� ������ � ��������
	virtual bool OnExport(
		cls::INode& node,				//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IRender* scenecache,		//!< ��� �����
		Math::Box3f scenebox,			//!< box �����
		cls::IRender* subpasscache,		//!< ��� ����������� (�� ���� ������� OnExportParentPass)
		cls::IExportContext* context		// 
		);

	// ���������� � ���� ����������� ��� �������� �������
	// ��������, ������ shadowpass ������ �������� � ������� ��� �������� ����
	// ������������� ���������� ����� OnExport()
	virtual void OnExportParentPass( 
		cls::INode& subnode,				//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IRender* render, 
		cls::IExportContext* context
		);
protected:
	// ����� renderPass ������
	cls::INode* getRenderPassNode(cls::IExportContext* context);

};


PassGenerator_RenderLayer passgenerator_renderlayer;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl PassRenderLayer()
	{
		return &passgenerator_renderlayer;
	}
}

// ����� renderPass ������
cls::INode* PassGenerator_RenderLayer::getRenderPassNode(cls::IExportContext* context)
{
	// ����� renderPass
	std::string renderpassname = context->getEnvironment("RENDERPASSNODE");
	MObject passobj;
	if( !nodeFromName(renderpassname.c_str(), passobj))
		return NULL;
	cls::INode* passnode = context->getNodeForObject(passobj);
	if( !passnode)
		return NULL;
	return passnode;
}

//! ����������� ���� � �������� 
bool PassGenerator_RenderLayer::OnPrepareNode(
	cls::INode& node, 
	cls::IExportContext* context,
	MObject& generatornode
	)
{
	cls::INode* passnode = getRenderPassNode(context);
	if( !passnode) return false;

	cls::P<bool> renderable = ocExport_GetAttrValue(node.getObject(), "renderable");
	if( renderable.empty() || !renderable.data())
		return false;
	
	return true;
}

// ���� ������ ����������� ������� ������� � ������ ����� ��� ���
bool PassGenerator_RenderLayer::IsUsedInFrame(
	cls::INode& node, 
	float frame, 			
	cls::IExportContext* context
	)
{
	return true;
}
/*/
// ������ ������� BlurGenerator ������������ �� ���������
// ����� ������� NULL
cls::IBlurGenerator* PassGenerator_RenderLayer::GetDefaultBlurGenerator( 
	cls::INode& node, 
	cls::IExportContext* context
	)
{
	cls::INode* passnode = getRenderPassNode(context);
	if( !passnode) return NULL;

	return PassGenerator_Final::GetDefaultBlurGenerator(*passnode, context);
}
/*/

// ���������� ����� ��������� ������ ����
bool PassGenerator_RenderLayer::OnExportNode(
	cls::INode& node,				//!< ������ ������� ���� �� �������
	cls::IRender* render,			//!< render
	cls::IExportContext* context,	//!< context
	cls::INode& passnode,			//!< ������ �������
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MFnRenderLayer rl(passnode.getObject());
	if( node.getObject().hasFn(MFn::kWorld))
		return true;
	
	MStatus stat;
	MFnDagNode dan( node.getPath().node() );
	if( dan.object().isNull())
		return true;

	bool res = false;
	for(;;)
	{
		bool _res = rl.inLayer(dan.object(), &stat);
		if( stat) 
		{
			res = _res;
			if(res) break;
		}

		MObject parent = dan.parent(0);
		if( parent.isNull())
			break;
		if( parent.hasFn(MFn::kWorld))
			break;
		dan.setObject(parent);
	}

//context->error("%s is %d", node.getName(), (int)res);
	if( !res)
	{
		std::string passname = passnode.getName();
		render->IfAttribute("@passname", cls::PS(passname));
		render->Attribute("@visible", false);
		render->EndIfAttribute();
	}
	return true;
}

//! ������� ������ � ��������
bool PassGenerator_RenderLayer::OnExport(
	cls::INode& _node,				//!< ������ ���������� ������� (�.�., kNullObj)
	cls::IRender* scenecache,		//!< ����������� � ��� �����
	Math::Box3f scenebox,			//!< box �����
	cls::IRender* subpasscache,		//!< ��� ����������� (�� ���� ������� OnExportParentPass)
	cls::IExportContext* context	// 
	)
{
	cls::INode* passnode = getRenderPassNode(context);
	if( !passnode) return false;

	std::string passname = _node.getName();

	return PassGenerator_Final::OnExport(
		passname.c_str(), 
		*passnode,
		scenecache,
		scenebox,
		subpasscache,
		context
		);
}

// ���������� � ���� ����������� ��� �������� �������
// ��������, ������ shadowpass ������ �������� � ������� ��� �������� ����
void PassGenerator_RenderLayer::OnExportParentPass( 
	cls::INode& _node,				//!< ������ ���������� ������� (�.�., kNullObj)
	cls::IRender* render, 
	cls::IExportContext* context
	)
{
	cls::INode* passnode = getRenderPassNode(context);
	if( !passnode) return;

	return PassGenerator_Final::OnExportParentPass(
		*passnode,
		render,
		context
		);
}
