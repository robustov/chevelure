#include "stdafx.h"
#include "MayaTransformGenerator.h"
#include <maya/MFnMesh.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MPlug.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MIntArray.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnTransform.h>
#include <maya/MTransformationMatrix.h>
#include <string>
#include <mathNmaya/mathNmaya.h>
#include "math/Matrix.h"

MayaTransformGenerator mayaTransformGenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl GetMayaTransformGenerator()
	{
		return &mayaTransformGenerator;
	}
}

MayaTransformGenerator::MayaTransformGenerator()
{
}

//! render ����� ���� = NULL
void MayaTransformGenerator::OnTransformStart(
	cls::INode& node,					//!< ������
	cls::IRender* render,				//!< render
	const Math::Matrix4f& curtransform,	//!< ������� ���������
	Math::Matrix4f& newtransform,		//!< ����� ���������
	cls::IExportContext* context,		
	MObject& generatornode				//!< generator node �.� 0
	)
{
	MObject obj = node.getObject();
	if( !obj.hasFn(MFn::kTransform))
	{
		if( render)
			render->PushAttributes();
		newtransform = curtransform;
		return;
	}

	MFnDependencyNode dn(obj);

	MFnTransform transform(obj);
	MTransformationMatrix tm = transform.transformation();
	Math::Matrix4f m;
	copy( m, tm.asMatrix());
	newtransform = m*curtransform;

	if( render)
	{
		render->PushTransform();
	}
}

//! ��������� ����� { AppendTransform ��� SetTransform }
void MayaTransformGenerator::OnTransform(
	cls::INode& node,					//!< ������
	cls::IRender* render,				//!< render
	cls::IExportContext* context,
	MObject& generatornode				//!< generator node �.� 0
	)
{
	MObject obj = node.getObject();
	if( !obj.hasFn(MFn::kTransform))
		return;

	MFnDependencyNode dn(obj);

	MFnTransform transform(obj);
	MTransformationMatrix tm = transform.transformation();
	Math::Matrix4f m;
	copy( m, tm.asMatrix());

	if( render)
	{
		render->PushAttributes();
		{

			Math::Vec3f translate;
			Math::Vec3f rotate;
			Math::Vec3f scale;
			Math::Vec3f shear; 
			int rotateOrder;
			Math::Vec3f scalePivot;
			Math::Vec3f scalePivotTranslate;
			Math::Vec3f rotatePivot;
			Math::Vec3f rotatePivotTranslate;
			Math::Vec3f rotateAxis;

			MVector mTranslate( transform.getTranslation( MSpace::kTransform ) );

			double mRotation[3], mScale[3], mShear[3];

			MTransformationMatrix::RotationOrder mRotationOrder( MTransformationMatrix::kXYZ );
			transform.getRotation( mRotation,  mRotationOrder );
			mRotationOrder = transform.rotationOrder();

			transform.getScale( mScale );
			transform.getShear( mShear );

			MQuaternion mRotateOrientation( transform.rotateOrientation( MSpace::kTransform ) );

			MPoint mRotatePivot( transform.rotatePivot( MSpace::kTransform ) );
			MVector mRotatePivotTranslation( transform.rotatePivotTranslation( MSpace::kTransform ) );

			MPoint mScalePivot( transform.scalePivot( MSpace::kTransform ) );
			MVector mScalePivotTranslation( transform.scalePivotTranslation( MSpace::kTransform ) );

			for ( int i = 0; i < 3; i++ )
			{
				translate[i] = (float)mTranslate[i];
				rotate[i] = (float)mRotation[i];
				scale[i] = (float)mScale[i];
				shear[i] = (float)mShear[i];

				scalePivot[i] = (float)mScalePivot[i];
				scalePivotTranslate[i] = (float)mScalePivotTranslation[i];
				rotatePivot[i] = (float)mRotatePivot[i];
				rotatePivotTranslate[i] = (float)mRotatePivotTranslation[i];

				rotateAxis[i] = (float)mRotateOrientation.asEulerRotation()[i];

			}

			rotateOrder = (int)mRotationOrder-(int)MTransformationMatrix::kXYZ;

			render->Parameter("MayaTransform::translate", translate);
			render->Parameter("MayaTransform::rotate", rotate);
			render->Parameter("MayaTransform::scale", scale);
			render->Parameter("MayaTransform::shear", shear);
			render->Parameter("MayaTransform::rotateOrder", rotateOrder);
			render->Parameter("MayaTransform::scalePivot", scalePivot);
			render->Parameter("MayaTransform::scalePivotTranslate", scalePivotTranslate);
			render->Parameter("MayaTransform::rotatePivot", rotatePivot);
			render->Parameter("MayaTransform::rotatePivotTranslate", rotatePivotTranslate);
			render->Parameter("MayaTransform::rotateAxis", rotateAxis);

//			render->RenderCall("MayaTransform");
			render->Call("MayaTransform");
			render->AppendTransform();
		}
		render->PopAttributes();
	}
}

void MayaTransformGenerator::OnTransformEnd(
	cls::INode& node,					//!< ������
	cls::IRender* render,				//!< render
	cls::IExportContext* context,
	MObject& generatornode				//!< generator node �.� 0
	)
{
	MObject obj = node.getObject();
	if( !obj.hasFn(MFn::kTransform))
	{
		if( render)
			render->PopAttributes();
		return;
	}

	if( render)
	{
		render->PopTransform();
	}
}

