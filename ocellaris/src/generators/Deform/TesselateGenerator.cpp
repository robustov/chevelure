#include "stdafx.h"
#include "TesselateGenerator.h"
#include <maya/MFnMesh.h>
#include <maya/MFloatArray.h>
#include <maya/MAnimControl.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatVector.h>
#include <maya/MPlug.h>
#include <maya/MFnSkinCluster.h>
#include <maya/MItGeometry.h>
#include "mathNmaya/mathNmaya.h"
#include <maya/MItDependencyGraph.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MFnSingleIndexedComponent.h>
#include <maya/MItMeshPolygon.h>

TesselateGenerator tesselategenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl Tesselate()
	{
		return &tesselategenerator;
	}
}

// ��� �������� � ����
void TesselateGenerator::OnLoadToMaya(
	MObject& generatornode
	)
{
	MObject attr;
//	attr = ocExport_AddAttrType(generatornode, "parameterName", cls::PT_STRING, cls::PS("shaderLayer"));
}

//! ��������� ���������� �������� �������� ���� ������� ����� �������������� ��������� ��������� 
void TesselateGenerator::OnSetupNode(
	cls::INode& inode, 
	cls::IExportContext* context,
	MObject& generatornode			//!< generator node �.� 0
	)
{
}

//! render ����� ���� = NULL
void TesselateGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,				//!< render
	cls::enSystemCall call,				//!< ��� ������ ��������� ��������: I_POINTS, I_CURVES, I_MESH ...
	cls::IExportContext* context, 
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();

	MStatus stat;
	MFnMesh mesh(node, &stat);
	if( !stat) 
		return;

/*/
	cls::PS parameterName = ocExport_GetAttrValue(generatornode, "parameterName");
	if( parameterName.empty() || !parameterName.data()[0])
		return;

	int fvc = mesh.numFaceVertices();
	cls::PA<float> shaderLayer(cls::PI_PRIMITIVEVERTEX);
	shaderLayer.reserve(fvc);

	cls::PA<int> data = ocExport_GetAttrArrayValue(node, parameterName.data());

	bool bReverse = true;
	int faceVertex = 0;
	for ( MItMeshPolygon polyIt ( node ); !polyIt.isDone(); polyIt.next() ) 
	{
		int polyIndex = polyIt.index();
		MIntArray vertices;
		polyIt.getVertices( vertices);
		int count = (int)vertices.length();
		for(int xv=0; xv<count; xv++, faceVertex++)
		{
			int v = bReverse?(count-xv-1):(v);

			float sl = -1;
			if( polyIndex < data.size())
			{
				sl = (float)data[polyIndex];
			}
			shaderLayer.push_back(sl);
		}
	}
	if( render)
	{
		render->Parameter("shaderLayer", shaderLayer);
	}
/*/
}
