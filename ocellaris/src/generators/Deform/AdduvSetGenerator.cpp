#include "stdafx.h"
#include "AdduvSetGenerator.h"
#include <maya/MFnMesh.h>
#include <maya/MFloatArray.h>
#include <maya/MAnimControl.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatVector.h>
#include <maya/MPlug.h>
#include <maya/MFnSkinCluster.h>
#include <maya/MItGeometry.h>
#include "mathNmaya/mathNmaya.h"
#include <maya/MItDependencyGraph.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MFnSingleIndexedComponent.h>
#include <maya/MItMeshPolygon.h>

AdduvSetGenerator adduvsetgenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl AdduvSet()
	{
		return &adduvsetgenerator;
	}
}

// ��� �������� � ����
void AdduvSetGenerator::OnLoadToMaya(
	MObject& generatornode
	)
{
	MObject attr;
	attr = ocExport_AddAttrType(generatornode, "uvSetName", cls::PT_STRING, cls::PS("uvSetName"));
}

//! ��������� ���������� �������� �������� ���� ������� ����� �������������� ��������� ��������� 
void AdduvSetGenerator::OnSetupNode(
	cls::INode& inode, 
	cls::IExportContext* context,
	MObject& generatornode			//!< generator node �.� 0
	)
{
}

//! render ����� ���� = NULL
void AdduvSetGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,				//!< render
	cls::enSystemCall call,				//!< ��� ������ ��������� ��������: I_POINTS, I_CURVES, I_MESH ...
	cls::IExportContext* context, 
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();

	MStatus stat;
	MFnMesh mesh(node, &stat);
	if( !stat) 
		return;

	cls::PS uvSetName = ocExport_GetAttrValue(generatornode, "uvSetName");
	if( uvSetName.empty() || !uvSetName.data()[0])
		return;

	std::string name = uvSetName.data();
	MFloatArray _uArray, _vArray;
	MString mname = name.c_str();
	stat = mesh.getUVs(_uArray, _vArray, &mname);
	if(!stat)
	{
		displayString("AdduvSetGenerator: Cant find uvSet %s", name.c_str());
	}

	int fvc = mesh.numFaceVertices();

	cls::PA<float> s(cls::PI_PRIMITIVEVERTEX), t(cls::PI_PRIMITIVEVERTEX);
	s.reserve(fvc);
	t.reserve(fvc);

	bool bReverse = true;
	int faceVertex = 0;
	for ( MItMeshPolygon polyIt ( node ); !polyIt.isDone(); polyIt.next() ) 
	{
		MIntArray vertices;
		polyIt.getVertices( vertices);
		int count = (int)vertices.length();
		for(int xv=0; xv<count; xv++, faceVertex++)
		{
			int v = bReverse?(count-xv-1):(v);

			int stind = _uArray.length()+1;
			float _u, _v;
			if( !polyIt.getUVIndex(v, stind, _u, _v, &mname))
				stind = _uArray.length()+1;

			if( stind>=(int)_uArray.length())
				s.push_back(0.f);
			else
				s.push_back(_uArray[stind]);

			if( stind>=(int)_vArray.length())
				t.push_back(0);
			else
				t.push_back(1-_vArray[stind]);
		}
	}
	if( render)
	{
		std::string uParam = "u_"+name;
		std::string vParam = "v_"+name;
		render->Parameter(uParam.c_str(), s);
		render->Parameter(vParam.c_str(), t);
	}
}
