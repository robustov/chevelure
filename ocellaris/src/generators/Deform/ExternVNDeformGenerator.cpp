#include "stdafx.h"
#include "ExternVNDeformGenerator.h"
#include <maya/MFnMesh.h>
#include <maya/MFloatArray.h>
#include <maya/MAnimControl.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatVector.h>
#include <maya/MPlug.h>
#include <maya/MFnSkinCluster.h>
#include <maya/MItGeometry.h>
#include "mathNmaya/mathNmaya.h"
#include <maya/MItDependencyGraph.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MFnSingleIndexedComponent.h>

ExternVNDeformGenerator externvndeformgenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl ExternVNDeform()
	{
		return &externvndeformgenerator;
	}
}

//! ��������� ���������� �������� �������� ���� ������� ����� �������������� ��������� ��������� 
void ExternVNDeformGenerator::OnSetupNode(
	cls::INode& inode, 
	cls::IExportContext* context,
	MObject& generatornode			//!< generator node �.� 0
	)
{
}

//! render ����� ���� = NULL
void ExternVNDeformGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,				//!< render
	cls::enSystemCall call,				//!< ��� ������ ��������� ��������: I_POINTS, I_CURVES, I_MESH ...
	cls::IExportContext* context, 
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MStatus stat;
	// ��� ����
	MObject node = inode.getObject();
	MFnDependencyNode dn(node, &stat);
	std::string name;
	if(stat) name = dn.name().asChar();

	// �������� ��������
	size_t n = name.find(':');
	if( n!=name.npos) name = name.substr(n+1);

	cls::PA<Math::Vec3f> P;
	render->GetParameter("P", P);
	if(!P.empty())
	{
		std::string attrname = "ExternGeometry::P.";
		attrname += name;
		render->GlobalAttribute(attrname.c_str(), P);
		render->ParameterFromAttribute("P", attrname.c_str());
	}

	cls::PA<Math::Vec3f> N;
	render->GetParameter("N", N);
	if(!N.empty())
	{
		std::string attrname = "ExternGeometry::N.";
		attrname += name;
		render->GlobalAttribute(attrname.c_str(), N);
		render->ParameterFromAttribute("N", attrname.c_str());
	}
	else
	{
		render->GetParameter("#N", N);
		if(!N.empty())
		{
			std::string attrname = "ExternGeometry::N.";
			attrname += name;
			render->GlobalAttribute(attrname.c_str(), N);
			render->ParameterFromAttribute("#N", attrname.c_str());
		}
	}
}
