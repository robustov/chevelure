#include "stdafx.h"
#include "LightGenerator.h"
#include <maya/MFnLight.h>
#include <maya/MDagPath.h>
#include <maya/MMatrix.h>
#include "mathNmaya/mathNmaya.h"
#include "ocellaris/OcellarisExport.h"
#include "ocellaris/renderCacheImpl.h"
#include "Util/misc_create_directory.h"
#include "./SlimCommands.h"

#include <maya/MAngle.h>//
#include <maya/MPlugArray.h>//

LightGenerator lightgenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl Light()
	{
		return &lightgenerator;
	}
}

bool LightGenerator::OnLight(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();
	MDagPath path;
	MDagPath::getAPathTo(node, path);

//	MMatrix _matr = path.inclusiveMatrix();
	MTransformationMatrix xform( path.inclusiveMatrix()	);
	double scale[] = { 1, 1, -1 };
	xform.setScale(	scale, MSpace::kTransform );
	MMatrix _matr = xform.asMatrix();

	Math::Matrix4f matr; ::copy(matr, _matr);

	MFnLight light(node);
//	MString light_name = light.name();
	std::string light_name = inode.getName();
	Util::correctFileName(light_name, '_');

	MObject attr;
	attr = ocExport_AddAttrType(node, "useSlimShader", cls::PT_BOOL, cls::P<bool>(false));
	cls::P<bool> useSlimShader = ocExport_GetAttrValue(node, attr);
	if( !useSlimShader.empty() && useSlimShader.data())
	{
		cls::renderCacheImpl<> cache;
		Slim::paramlist_t lightShaderParams;
		Slim::shaderlist_t shaderlist;

		std::string workspacedir;
		{
			MString _workspacedir;
			MGlobal::executeCommand("workspace -fn", _workspacedir);
			workspacedir = _workspacedir.asChar();
			Util::pushBackSlash(workspacedir);
		}
		MObject externnode = node;

		std::string ligthShader = Slim::readShaderNameNparameters( &cache, node, "slimLight", lightShaderParams, externnode, "oc_light",  "ocellarisSurfaceShaderAttrs", shaderlist, workspacedir.c_str(), 
			false);
		if( render && !ligthShader.empty())
		{
			render->PushTransform();
			render->PushAttributes();
			render->SetTransform(matr);
			render->PopAttributes();

			render->PushAttributes();
			cache.Render(render);


			cls::Param LightColor = ocExport_GetAttrValue(node, "color");
			render->Parameter("LightColor", cls::PT_COLOR, LightColor);

			cls::P<float> intensity = ocExport_GetAttrValue(node, "intensity");
			render->Parameter("Kl", intensity);

			cls::P<float> dropoff = ocExport_GetAttrValue(node, "dropoff");
			render->Parameter("Dropoff", dropoff);

			cls::P<float> coneAngle = ocExport_GetAttrValue(node, "coneAngle");
			render->Parameter("ConeAngle", coneAngle);

			cls::P<float> penumbraAngle = ocExport_GetAttrValue(node, "penumbraAngle");
			render->Parameter("PenumbraAngle", penumbraAngle);

			cls::P<float> ulPenumbraLinear = ocExport_GetAttrValue(node, "ulPenumbraLinear");
			render->Parameter("penumbraLinear", ulPenumbraLinear);

			cls::P<float> ulMayaPenumbra = ocExport_GetAttrValue(node, "ulMayaPenumbra");
			render->Parameter("mayaPenumbra", ulMayaPenumbra);

			cls::P<Math::Vec3f> ShadowColor = ocExport_GetAttrValue(node, "shadowColor");
			render->Parameter("ShadowColor", ShadowColor);

			cls::P<float> tkShDecay = ocExport_GetAttrValue(node, "tkShDecay");
			render->Parameter("ShadowDecay", tkShDecay);
		
			// Falloff		
			cls::P<float> decayRate = ocExport_GetAttrValue(node, "decayRate");
			render->Parameter("Falloff", decayRate);

			cls::P<float> ulFalloffStyle = ocExport_GetAttrValue(node, "ulFalloffStyle");
			render->Parameter("FalloffStyle", ulFalloffStyle);

			cls::P<float> tkStartDist = ocExport_GetAttrValue(node, "tkStartDist");
			render->Parameter("StartDist", tkStartDist);

			cls::P<float> tkEndDist = ocExport_GetAttrValue(node, "tkEndDist");
			render->Parameter("FalloffDist", tkEndDist);

			cls::P<float> tkEndPersent = ocExport_GetAttrValue(node, "tkEndPersent");
			render->Parameter("EndPersent", tkEndPersent);

			cls::P<float> ulBoundlessAngle = ocExport_GetAttrValue(node, "ulBoundlessAngle");
			render->Parameter("BoundlessAngle", ulBoundlessAngle);

			// Special Spot Parametrs
			cls::PS tkCategory = ocExport_GetAttrValue(node, "tkCategory");
			render->Parameter("category", tkCategory);

			cls::P<float> emitDiffuse = ocExport_GetAttrValue(node, "emitDiffuse");
			render->Parameter("diffuseContribution", emitDiffuse);

			cls::P<float> emitSpecular = ocExport_GetAttrValue(node, "emitSpecular");
			render->Parameter("specularContribution", emitSpecular);

//Parameter uniform string pixar_SurfPtLight_CoordinateSystem = { "current"};
//Parameter uniform float pixar_SurfPtLight_UsePref = { 0.000000};

			// shadow map
			std::string shname = "ulShadowMSH_1_";
			Slim::shaderlist_t::iterator it = shaderlist.begin();
			for(;it != shaderlist.end(); it++)
			{
				Slim::ShaderDeclare& sd = it->second;
				printf("%s: %s\n", it->first.c_str(), sd.shadername.c_str());
				Slim::paramlist_t::iterator itp = sd.paramlist.begin();
				for(;itp != sd.paramlist.end(); itp++)
				{
					std::string n = itp->first;
					printf("\t%s\n", n.c_str());
					size_t x = n.find("BlurScale");
					if(x != n.npos)
						shname = n.substr(0, x);
				}
			}
			printf("SHADER PREFIX = %s\n", shname.c_str());

			// Commom Parametrs
			render->Parameter((shname+"ConeAngle").c_str(), coneAngle);
			render->Parameter((shname+"PenumbraAngle").c_str(), penumbraAngle);

			cls::P<bool> useDepthMapShadows = ocExport_GetAttrValue(node, "useDepthMapShadows");
			render->Parameter((shname+"useShadow").c_str(), useDepthMapShadows);

			cls::P<float> tkBlurScale = ocExport_GetAttrValue(node, "tkBlurScale");
			render->Parameter((shname+"BlurScale").c_str(), tkBlurScale);

			cls::P<float> ulBiasAtten = ocExport_GetAttrValue(node, "ulBiasAtten");
			render->Parameter((shname+"biasAtten").c_str(), ulBiasAtten);

			cls::P<float> weightSh = ocExport_GetAttrValue(node, "weightSh");
			render->Parameter((shname+"weightSh").c_str(), weightSh);

			cls::P<float> samplesMSH = ocExport_GetAttrValue(node, "samplesMSH");
			render->Parameter((shname+"samplesSh").c_str(), samplesMSH);

			cls::P<float> sizeSh = ocExport_GetAttrValue(node, "sizeSh");
			render->Parameter((shname+"sizeSh").c_str(), sizeSh);

			cls::P<float> weightMSH = ocExport_GetAttrValue(node, "weightMSH");
			render->Parameter((shname+"weightMSH").c_str(), weightMSH);

//			cls::P<float> samplesMSH = ocExport_GetAttrValue(node, "samplesMSH");
			render->Parameter((shname+"samplesMSH").c_str(), samplesMSH);

			cls::P<float> sizeMSHMin = ocExport_GetAttrValue(node, "sizeMSHMin");
			render->Parameter((shname+"sizeMSHMin").c_str(), sizeMSHMin);

			cls::P<float> sizeMSHMax = ocExport_GetAttrValue(node, "sizeMSHMax");
			render->Parameter((shname+"sizeMSHMax").c_str(), sizeMSHMax);

			cls::P<float> LayersMSH = ocExport_GetAttrValue(node, "LayersMSH");
			render->Parameter((shname+"LayersMSH").c_str(), LayersMSH);

			cls::P<float> DimensionMSH = ocExport_GetAttrValue(node, "DimensionMSH");
			render->Parameter((shname+"DimensionMSH").c_str(), DimensionMSH);


			// Smart Transform
			cls::P<float> tkDoShTrans = ocExport_GetAttrValue(node, "tkDoShTrans");
			render->Parameter((shname+"doTrans").c_str(), tkDoShTrans);

			cls::P<float> tkShCoordSys = ocExport_GetAttrValue(node, "tkShCoordSys");
			render->Parameter((shname+"coordsys").c_str(), tkShCoordSys);

			render->Parameter( (shname+"File").c_str(), "3D/DATA/shd/RAT-200_v1_mitka/RAT-200_v1_mitka.MainLightShape.dsh.0105.tex");
			std::string attrname = light_name;
			attrname += "_shadowName";
			render->ParameterFromAttribute( (shname+"File").c_str(), attrname.c_str());
			
			render->Parameter("#lightshader", ligthShader.c_str());
			render->Light();
			render->PopAttributes();
			render->PopTransform();
			return true;
		}
	}

	//std::string shadername = "spotlight";
	if ( render )
	{
		render->PushTransform();
		render->PushAttributes();
		render->SetTransform(matr);
		render->PopAttributes();

		render->PushAttributes();
		switch ( node.apiType() )
		{
			case MFn::kDirectionalLight: render->Parameter("#lightshader", "mtorDirectionalLight" ); break;
			case MFn::kPointLight: render->Parameter("#lightshader", "mtorPointLight" ); break;
			case MFn::kSpotLight: render->Parameter("#lightshader", "mtorSpotLight" ); break;
			case MFn::kAmbientLight: render->Parameter("#lightshader", "mtorAmbientLight" ); break;
			default:
			{
				printf("Light type - \"%s\" are not implemented. Using \"pointLight\" type.\n", node.apiTypeStr());
				render->Parameter("#lightshader", "mtorPointLight" );
			}
		}
		//render->Parameter("#lightshader", shadername.c_str() );//

		float intensity;
		Math::Vec3f lightcolor;

		light.findPlug( "colorR" ).getValue( lightcolor[0] );
		light.findPlug( "colorG" ).getValue( lightcolor[1] );
		light.findPlug( "colorB" ).getValue( lightcolor[2] );
		light.findPlug( "intensity" ).getValue( intensity );	
		render->Parameter( "intensity", intensity );
		render->Parameter( "lightcolor", cls::PT_COLOR, lightcolor );

		switch ( node.apiType() )
		{
			case MFn::kPointLight:
			{
				double decayRate;
				light.findPlug( "decayRate" ).getValue( decayRate );
				render->Parameter( "decayRate", (float)decayRate );
				
				if ( light.findPlug( "lightGlow" ).isConnected() )
				{
					int glowStyle;
					Math::Vec3f glowColor;
					float glowIntensity;
					float glowSpread;
					float glowStarLevel;
					float glowRadialNoise;
					float glowStarPoints;
					float glowRotation;
					float glowNoiseFreq;

					MPlugArray plugs;
					light.findPlug( "lightGlow" ).connectedTo( plugs, 1, 0 );
					MObject opticalFXnode = plugs[0].node();
					MFnDependencyNode opticalFX( opticalFXnode );
					opticalFX.findPlug( "glowType" ).getValue( glowStyle );
					opticalFX.findPlug( "glowColorR" ).getValue( glowColor[0] );
					opticalFX.findPlug( "glowColorG" ).getValue( glowColor[1] );
					opticalFX.findPlug( "glowColorB" ).getValue( glowColor[2] );
					opticalFX.findPlug( "glowIntensity" ).getValue( glowIntensity );
					opticalFX.findPlug( "glowSpread" ).getValue( glowSpread );
					opticalFX.findPlug( "glowStarLevel" ).getValue( glowStarLevel );
					opticalFX.findPlug( "glowRadialNoise" ).getValue( glowRadialNoise );
					opticalFX.findPlug( "starPoints" ).getValue( glowStarPoints );
					opticalFX.findPlug( "rotation" ).getValue( glowRotation );
					opticalFX.findPlug( "radialFrequency" ).getValue( glowNoiseFreq );

					render->Parameter( "glowStyle", (float)glowStyle );
					render->Parameter( "glowColor", cls::PT_COLOR, glowColor );
					render->Parameter( "glowIntensity", glowIntensity );
					render->Parameter( "glowSpread", glowSpread );
					render->Parameter( "glowStarLevel", glowStarLevel );
					render->Parameter( "glowRadialNoise", glowRadialNoise );
					render->Parameter( "glowStarPoints", glowStarPoints );
					render->Parameter( "glowRotation", glowRotation );
					render->Parameter( "glowNoiseFreq", glowNoiseFreq );
				}
				break;
			}

			case MFn::kSpotLight:
			{
				double decayRate, dropoff;
				MAngle coneAngle, penumbraAngle;
				light.findPlug( "decayRate" ).getValue( decayRate );
				light.findPlug( "coneAngle" ).getValue( coneAngle );
				light.findPlug( "penumbraAngle" ).getValue( penumbraAngle );	
				light.findPlug( "dropoff" ).getValue( dropoff );

				render->Parameter( "decayRate", (float)decayRate );
				render->Parameter( "coneAngle", (float)( 0.5 * coneAngle.asRadians() ) );
				render->Parameter( "penumbraAngle", (float)( penumbraAngle.asRadians() ) );
				render->Parameter( "dropOff", (float)dropoff );

				bool useBarnDoors, useDecayRegions;

				light.findPlug( "barnDoors" ).getValue( useBarnDoors );

				if ( useBarnDoors )
				{
					MAngle _barnDoors[4];
					cls::PA<float> barnDoors( cls::PI_CONSTANT );
					barnDoors.reserve(4);

					light.findPlug( "leftBarnDoor" ).getValue( _barnDoors[0] );
					light.findPlug( "rightBarnDoor" ).getValue( _barnDoors[1] );
					light.findPlug( "topBarnDoor" ).getValue( _barnDoors[2] );
					light.findPlug( "bottomBarnDoor" ).getValue( _barnDoors[3] );

					for ( int i = 0; i < 4; i++ )
					{
						barnDoors.push_back( (float)_barnDoors[i].asRadians() );
					}

					render->Parameter( "useBarnDoors", (float)useBarnDoors );
					render->Parameter( "barnDoors", barnDoors );
				}

				light.findPlug( "useDecayRegions" ).getValue( useDecayRegions );

				if ( useDecayRegions )
				{
					MDistance _decayRegions[6];
					cls::PA<float> decayRegions( cls::PI_CONSTANT );
					decayRegions.reserve(6);

					light.findPlug( "startDistance1" ).getValue( _decayRegions[0] );
					light.findPlug( "endDistance1" ).getValue( _decayRegions[1] );
					light.findPlug( "startDistance2" ).getValue( _decayRegions[2] );
					light.findPlug( "endDistance2" ).getValue( _decayRegions[3] );
					light.findPlug( "startDistance3" ).getValue( _decayRegions[4] );
					light.findPlug( "endDistance3" ).getValue( _decayRegions[5] );

					for ( int i = 0; i < 6; i++ )
					{
						decayRegions.push_back( (float)_decayRegions[i].asCentimeters() );
					}

					render->Parameter( "useDecayRegions", (float)useDecayRegions );
					render->Parameter( "decayRegions", decayRegions );
				}

				if ( light.findPlug( "lightGlow" ).isConnected() )
				{
					int glowStyle;
					Math::Vec3f glowColor;
					float glowIntensity;
					float glowSpread;
					float glowStarLevel;
					float glowRadialNoise;
					float glowStarPoints;
					float glowRotation;
					float glowNoiseFreq;

					MPlugArray plugs;
					light.findPlug( "lightGlow" ).connectedTo( plugs, 1, 0 );
					MObject opticalFXnode = plugs[0].node();
					MFnDependencyNode opticalFX( opticalFXnode );
					opticalFX.findPlug( "glowType" ).getValue( glowStyle );
					opticalFX.findPlug( "glowColorR" ).getValue( glowColor[0] );
					opticalFX.findPlug( "glowColorG" ).getValue( glowColor[1] );
					opticalFX.findPlug( "glowColorB" ).getValue( glowColor[2] );
					opticalFX.findPlug( "glowIntensity" ).getValue( glowIntensity );
					opticalFX.findPlug( "glowSpread" ).getValue( glowSpread );
					opticalFX.findPlug( "glowStarLevel" ).getValue( glowStarLevel );
					opticalFX.findPlug( "glowRadialNoise" ).getValue( glowRadialNoise );
					opticalFX.findPlug( "starPoints" ).getValue( glowStarPoints );
					opticalFX.findPlug( "rotation" ).getValue( glowRotation );
					opticalFX.findPlug( "radialFrequency" ).getValue( glowNoiseFreq );

					render->Parameter( "glowStyle", (float)glowStyle );
					render->Parameter( "glowColor", cls::PT_COLOR, glowColor );
					render->Parameter( "glowIntensity", glowIntensity );
					render->Parameter( "glowSpread", glowSpread );
					render->Parameter( "glowStarLevel", glowStarLevel );
					render->Parameter( "glowRadialNoise", glowRadialNoise );
					render->Parameter( "glowStarPoints", glowStarPoints );
					render->Parameter( "glowRotation", glowRotation );
					render->Parameter( "glowNoiseFreq", glowNoiseFreq );
				}
				break;
			}

			case MFn::kAmbientLight:
			{
				float ambientShade;
				light.findPlug( "ambientShade" ).getValue( ambientShade );
				render->Parameter( "ambientShade", ambientShade );
				break;
			}
		}

		cls::P<bool> useDepthMapShadows = ocExport_GetAttrValue(node, "useDepthMapShadows");
		if( !useDepthMapShadows.empty() && useDepthMapShadows.data())
		{
			std::string attrname = light_name;
			attrname += "_shadowName";
			render->ParameterFromAttribute( "shadowname", attrname.c_str());

			cls::Param shadowColor = ocExport_GetAttrValue(node, "shadowColor");
			render->Parameter( "shadowcolor", shadowColor);
		}

		render->Light();
		render->PopAttributes();
		render->PopTransform();
	}
	//render->Parameter("intensity", 100.f);
	//render->Parameter("color", cls::PT_COLOR, Math::Vec3f(1,1,1));
//areaLight
//volumeLight

	box.min = Math::Vec3f(-1, -1, -1);
	box.max = Math::Vec3f( 1,  1,  1);

	//render->Parameter("intensity", 100.f);
	//render->Parameter("lightcolor", cls::PT_COLOR, Math::Vec3f(1,1,1));
//	render->Parameter("decayRate", 0.f); 
//	render->Parameter("coneAngle", 0.349066f); 
//	render->Parameter("penumbraAngle", 0.f); 
//	render->Parameter("dropOff", 0.f); 
//	render->Parameter("useBarnDoors", 0.f); 

//	float _barnDoors[4] = {0.349066f, 0.349066f, 0.349066f, 0.349066f};
//	cls::PA<float> barnDoors(cls::PI_CONSTANT, _barnDoors, 4);
//	render->Parameter("barnDoors", barnDoors);

//	render->Parameter("useDecayRegions", 0.f); 
//	float _decayRegions[6] = {1, 2, 3, 6, 8, 10,};
//	cls::PA<float> decayRegions(cls::PI_CONSTANT, _decayRegions, 6);
//	render->Parameter("decayRegions", decayRegions);

	//render->Light( light_name.asChar(), shadername.c_str());
//	render->Light();//
//	LightSource "mtorSpotLight" "spotLightShape1" "float intensity" [1] "color lightcolor" [1 1 1] "float decayRate" [0] "float coneAngle" [0.349066] "float penumbraAngle" [0] "float dropOff" [0] "float useBarnDoors" [0] "float[4] barnDoors" [0.349066 0.349066 0.349066 0.349066] 
//	"float useDecayRegions" [0] "float[6] decayRegions" [1 2 3 6 8 10]
//	render->PopTransform();

	return true;
}
