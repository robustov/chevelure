#pragma once
#include "ocellaris/OcellarisExport.h"
#include "ocellaris/renderBlurParser.h"

struct FinalPassBlurGenerator : public cls::IBlurGenerator
{
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "FinalPassBlurGenerator";};

	//! OnFrame
	//! ������ ������� ��������� �������� � ����� ����� ��������� ������� �������
	//! context->addExportTime()
	virtual bool OnAddFrame(
		cls::INode& pass,				//!< pass
		float frame,					//!< RenderFrame
		cls::INode& node,				//!< ������ 
		cls::IExportContext* context,	//!< context
		MObject& generatornode			//!< generator node �.� 0
		)
	{
		int blurcount = 2;
		bool bSubFrame = false;
		float shutterAngle = 90;

		for(int i=0; i<blurcount; i++)
		{
			float exportTime = frame;
			if(i!=0) exportTime = frame + i/(blurcount-1.f);

			context->addExportTimeNode(node, frame, exportTime);
		}
		return true;
	}
	// ���������� ��� ������ �����
	virtual bool OnBuildBlur(
		cls::INode& pass,				//!< pass
		cls::INode& node,				//!< ������ 
		cls::IRender* render,			//!< render
		Math::Matrix4f& transform,		// ����������� ����� transform
		Math::Box3f& box,				// ����������� ���� ��� �������
		float frame,					//!< ����� ���� ��������
		cls::IExportContext* context,	//!< context
		MObject& generatornode			//!< generator node �.� 0
		)
	{
		int blurcount = 2;
		bool bSubFrame = false;
		float shutterAngle = 90;

		if( blurcount==1)
		{
			cls::IRender* pcache=NULL;
			Math::Box3f* pbox = NULL;
			if( !context->getNodeExportFrameCache( frame, node, pcache, pbox))
			{
				context->error( "getNodeExportFrameCache %s %f FAILED\n", frame, node.getName());
				return false;
			}
			pcache->RenderGlobalAttributes(render);
			pcache->Render(render);
			Math::Box3f wb = *pbox;
			if( wb.isvalid())
			{
				wb.transform(transform);
				box.insert(wb);
			}
			return true;
		}

		cls::renderBlurParser<> blurParser;
//		render->MotionBegin();
		for(int i=0; i<blurcount; i++)
		{
			float exportTime = frame;
			if(i!=0) exportTime = frame + i/(blurcount-1.f);
			float blurphase = i/(blurcount-1.f);

			cls::IRender* pcache=NULL;
			Math::Box3f* pbox = NULL;
			if( !context->getNodeExportFrameCache( exportTime, node, pcache, pbox))
			{
				context->error( "getNodeExportFrameCache %s %f FAILED\n", frame, node.getName());
				return false;
			}
			pcache->RenderGlobalAttributes(render);

			blurParser.SetCurrentPhase(blurphase);
//			render->MotionPhaseBegin(blurphase);
			pcache->Render(&blurParser);
//			render->MotionPhaseEnd();
			Math::Box3f wb = *pbox;
			if( wb.isvalid())
			{
				wb.transform(transform);
				box.insert(wb);
			}
		}
//		render->MotionEnd();
		blurParser.Render(render);

		return true;
	}
};
