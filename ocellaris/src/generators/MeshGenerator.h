#pragma once

#include "ocellaris/OcellarisExport.h"
#pragma warning ( disable:4275)
#pragma warning ( disable:4251)

#include "ocellaris/IGeometryGenerator.h"

//! @ingroup implement_group
//! \brief ����������� ��������� ��������� ��� �������� ���� Mesh � Subdiv
//! 
struct OCELLARISEXPORT_API MeshGenerator : public cls::IGeometryGenerator
{
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "MeshGenerator";};

	virtual bool OnGeometry(
		cls::INode& node,				//!< ������ 
		cls::IRender* render,			//!< render
		Math::Box3f& box,				//!< box 
		cls::IExportContext* context,	//!< context
		MObject& generatornode			//!< generator node �.� 0
		);
protected:
	bool MeshToOcs(
		cls::INode& node,				//!< ������ 
		cls::IRender* render,			//!< render
		Math::Box3f& box,				//!< box 
		cls::IExportContext* context,	//!< context
		bool usingSubdiv
		);
};
