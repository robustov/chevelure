#pragma once

#include "ocellaris/ITransformGenerator.h"
//! @ingroup implement_group
//! \brief ����������� ��������� �������������
//! 
struct TransformGenerator : public cls::ITransformGenerator
{
//	int level;
//	Math::Matrix4f trans;

	TransformGenerator();
public:
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "TransformGenerator";};

	//! render ����� ���� = NULL
	virtual void OnTransformStart(
		cls::INode& node,					//!< ������
		cls::IRender* render,				//!< render
		const Math::Matrix4f& curtransform,	//!< ������� ���������
		Math::Matrix4f& newtransform,		//!< ����� ���������
		cls::IExportContext* context,		
		MObject& generatornode				//!< generator node �.� 0
		);
	//! ��������� ����� { AppendTransform ��� SetTransform }
	virtual void OnTransform(
		cls::INode& node,					//!< ������
		cls::IRender* render,				//!< render
		cls::IExportContext* context,
		MObject& generatornode				//!< generator node �.� 0
		);
	//! render ����� ���� = NULL
	virtual void OnTransformEnd(
		cls::INode& node,					//!< ������
		cls::IRender* render,				//!< render
		cls::IExportContext* context,
		MObject& generatornode				//!< generator node �.� 0
		);
};
