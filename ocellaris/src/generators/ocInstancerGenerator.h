#pragma once
#include "ocellaris/IGeometryGenerator.h"

//! @ingroup implement_group
//! \brief ����� ��� ��� �� �����, �������� ����
//! 
struct ocInstancerGenerator : public cls::IGeometryGenerator
{
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "ocInstancerGenerator";};

	bool OnGeometry(MObject& node, cls::IRender* render, Math::Box3f& box, cls::IExportContext* context);
};
