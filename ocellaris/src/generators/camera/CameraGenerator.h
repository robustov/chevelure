#pragma once

#include "ocellaris/IAttributeGenerator.h"


//! @ingroup implement_group
//! \brief ����������� ��������� ��������� ��� ������
//! 
struct CameraGenerator : public cls::IAttributeGenerator
{
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "CameraGenerator";};

	// � ����� ������ ��������
	int GetCallType(
		MObject& generatornode
		){return CT_BEFORE_TRANSFORM;}

	//! render ����� ���� = NULL!
	virtual bool OnAttribute(
		cls::INode& node,				//!< ������ ��� �������� ����������� �������
		cls::IRender* render,			//!< render
		cls::IExportContext* context, 
		MObject& generatornode
		);
protected:
	void getCameraInfo( MFnCamera& cam, int cam_width, int cam_height, double fov_ratio );

	void portFieldOfView( 
		int port_width, int port_height,
		double& horizontal,
		double& vertical,
		MFnCamera& fnCamera );

	void computeViewingFrustum ( 
		double     window_aspect,
		double&    left,
		double&    right,
		double&    bottom,
		double&    top,
		MFnCamera& cam );

};
