#include "stdafx.h"
#include "CameraGenerator.h"
#include <maya/MFnCamera.h>
#include <string>
#include "ocellaris/OcellarisExport.h"
#include "MathNMaya/MathNMaya.h"

CameraGenerator cameragenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl Camera()
	{
		return &cameragenerator;
	}
}

/*/
// ��� �������� � ����
void CameraGenerator::OnLoadToMaya(
	MObject& generatornode
	)
{
}
/*/

//! render ����� ���� = NULL!
bool CameraGenerator::OnAttribute(
	cls::INode& inode,				//!< ������ ��� �������� ����������� �������
	cls::IRender* render,			//!< render
	cls::IExportContext* context, 
	MObject& generatornode
	)
/*/
bool CameraGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
/*/
{
	MObject node = inode.getObject();
	// camera::transform
	MDagPath path;
	MFnDagNode(node).getPath(path);
	MTransformationMatrix xform( path.inclusiveMatrix()	);
	double scale[] = { 1, 1, -1	};
	xform.setScale(	scale, MSpace::kTransform );
	Math::Matrix4f mview;
//	copy(mview, xform.asMatrixInverse());
	copy(mview, xform.asMatrix());
	render->Parameter("camera::transform", mview);

	MString name = MFnDagNode(node).name();
	render->Parameter("camera::name", name.asChar());

	if( node.hasFn(MFn::kCamera))
	{
		MFnCamera fnCamera(node);
		// camera::near
		// camera::far
		double neardb	   = 0.001;	   // TODO:	these values are duplicated	elsewhere in this file
		double fardb	  =	250000.0; // TODO: these values	are	duplicated elsewhere in	this file
		if ( fnCamera.isClippingPlanes() ) 
		{
			neardb = fnCamera.nearClippingPlane();
			fardb =	fnCamera.farClippingPlane();
		}
		render->Parameter("camera::near", (float)neardb);
		render->Parameter("camera::far", (float)fardb);

		// Projection
		int cam_width = 640;
		int cam_height = 480;
		float aspectRatio = 1;
		double fov_ratio = 1;
		getCameraInfo(fnCamera, cam_width, cam_height, fov_ratio );

		bool isOrtho = fnCamera.isOrtho();
		double hFOV =	fnCamera.horizontalFieldOfView()/fov_ratio;
		double orthoWidth	= fnCamera.orthoWidth();
		double orthoHeight = fnCamera.orthoWidth() * ((float)cam_height / (float)cam_width);
	//	bool motionBlur	= fnCamera.isMotionBlur();
		double focalLength = fnCamera.focalLength();
		double focalDistance = fnCamera.focusDistance();
		double fStop = fnCamera.fStop();
		if ( isOrtho ) 
		{
			/*/
			// ��� ����� �� ��!!!
			MDistance unitHelper;
			MDistance unit( 1, unitHelper.uiUnit() );
			double frameWidth, frameHeight;
			// the whole frame width has to be scaled according to the UI Unit
			frameWidth  = orthoWidth  * 5 * unit.asMeters();
			frameHeight = orthoHeight * 5 * unit.asMeters();
			/*/

			render->Parameter("camera::projection", "orthographic");

			cls::PA<float> screen(cls::PI_CONSTANT, 4);
			screen[0] = (float)-orthoWidth/2;
			screen[1] = (float)orthoWidth/2;
			screen[2] = (float)-orthoHeight/2;
			screen[3] = (float)orthoHeight/2;
			render->Parameter("camera::screen", screen);
		} 
		else 
		{
			float fieldOfView = (float)(hFOV * 180.0 / M_PI);
			render->Parameter("camera::projection", "perspective");
			render->Parameter("camera::fov", fieldOfView);
		}
	}
	else if(node.hasFn(MFn::kSpotLight))
	{
		MFnSpotLight fnSpot(node);

		float coneAngle = (float)fnSpot.coneAngle();
		float penumbraAngle = (float)fnSpot.penumbraAngle();
		if( penumbraAngle<0) penumbraAngle = 0;
		float hFOV = coneAngle + penumbraAngle*2;
		float fieldOfView = (float)(hFOV * 180.0 / M_PI);

		render->Parameter("camera::projection", "perspective");
		render->Parameter("camera::fov", fieldOfView);
	}
	return true;
}

void CameraGenerator::getCameraInfo( MFnCamera& cam, int cam_width, int cam_height, double fov_ratio )
//
//  Description:
//      Get information about the given camera
//
{
	// Resoultion can change if camera film-gate clips image
	// so we must keep camera width/height separate from render
	// globals width/height.
	//

	M3dView m_activeView = M3dView::active3dView();
	int width        = m_activeView.portWidth();
	int height       = m_activeView.portHeight();
	bool ignoreFilmGate = true;

	cam_width  = width;
	cam_height = height;

	// If we are using a film-gate then we may need to
	// adjust the resolution to simulate the 'letter-boxed'
	// effect.
	if ( cam.filmFit() == MFnCamera::kHorizontalFilmFit ) 
	{
		if ( !ignoreFilmGate ) 
		{
			double new_height = cam_width /
				( cam.horizontalFilmAperture() /
				cam.verticalFilmAperture() );

			if ( new_height < cam_height ) 
			{
				cam_height = ( int )new_height;
			}
		}

		double hfov, vfov;
		portFieldOfView( cam_width, cam_height, hfov, vfov, cam );
		fov_ratio = hfov / vfov;
	}
	else if ( cam.filmFit() == MFnCamera::kVerticalFilmFit ) 
	{
		double new_width = cam_height /
			( cam.verticalFilmAperture() /
			cam.horizontalFilmAperture() );

		double hfov, vfov;

		// case 1 : film-gate smaller than resolution
		//         film-gate on
		if ( ( new_width < cam_width ) && ( !ignoreFilmGate ) ) 
		{
			cam_width = ( int )new_width;
			fov_ratio = 1.0;
		}

		// case 2 : film-gate smaller than resolution
		//         film-gate off
		else if ( ( new_width < cam_width ) && ( ignoreFilmGate ) ) 
		{
			portFieldOfView( ( int )new_width, cam_height, hfov, vfov, cam );
			fov_ratio = hfov / vfov;
		}

		// case 3 : film-gate larger than resolution
		//         film-gate on
		else if ( !ignoreFilmGate ) 
		{
			portFieldOfView( ( int )new_width, cam_height, hfov, vfov, cam );
			fov_ratio = hfov / vfov;
		}

		// case 4 : film-gate larger than resolution
		//         film-gate off
		else if ( ignoreFilmGate ) 
		{
			portFieldOfView( ( int )new_width, cam_height, hfov, vfov, cam );
			fov_ratio = hfov / vfov;
		}

	}
	else if ( cam.filmFit() == MFnCamera::kOverscanFilmFit ) 
	{
		double new_height = cam_width /
			( cam.horizontalFilmAperture() /
			cam.verticalFilmAperture() );
		double new_width = cam_height /
			( cam.verticalFilmAperture() /
			cam.horizontalFilmAperture() );

		if ( new_width < cam_width ) 
		{
			if ( !ignoreFilmGate ) 
			{
				cam_width = ( int ) new_width;
				fov_ratio = 1.0;
			}
			else 
			{
				double hfov, vfov;
				portFieldOfView( ( int )new_width, cam_height, hfov, vfov, cam );
				fov_ratio = hfov / vfov;
			}
		}
		else {
			if ( !ignoreFilmGate )
				cam_height = ( int ) new_height;

			double hfov, vfov;
			portFieldOfView( cam_width, cam_height, hfov, vfov, cam );
			fov_ratio = hfov / vfov;
		}
	}
	else if ( cam.filmFit() == MFnCamera::kFillFilmFit ) 
	{
		double new_width = cam_height /
			( cam.verticalFilmAperture() /
			cam.horizontalFilmAperture() );
		double hfov, vfov;

		if ( new_width >= cam_width ) 
		{
			portFieldOfView( ( int )new_width, cam_height, hfov, vfov, cam );
			fov_ratio = hfov / vfov;
		}
		else 
		{
			portFieldOfView( cam_width, cam_height, hfov, vfov, cam );
			fov_ratio = hfov / vfov;
		}
	}
}


void CameraGenerator::portFieldOfView( 
	int port_width, int port_height,
	double& horizontal,
	double& vertical,
	MFnCamera& fnCamera )
{
	double left, right, bottom, top;
	double aspect = (double) port_width / port_height;
	computeViewingFrustum(aspect,left,right,bottom,top,fnCamera);

	double neardb = fnCamera.nearClippingPlane();
	horizontal = atan( ( ( right - left ) * 0.5 ) / neardb ) * 2.0;
	vertical = atan( ( ( top - bottom ) * 0.5 ) / neardb ) * 2.0;
}

const double MM_TO_INCH = 0.03937;

void CameraGenerator::computeViewingFrustum ( 
	double     window_aspect,
	double&    left,
	double&    right,
	double&    bottom,
	double&    top,
	MFnCamera& cam )
{
	double film_aspect   = cam.aspectRatio();
	double aperture_x    = cam.horizontalFilmAperture();
	double aperture_y    = cam.verticalFilmAperture();
	double offset_x      = cam.horizontalFilmOffset();
	double offset_y      = cam.verticalFilmOffset();
	double focal_to_near = cam.nearClippingPlane() / (cam.focalLength() * MM_TO_INCH);

	focal_to_near *= cam.cameraScale();

	double scale_x = 1.0;
	double scale_y = 1.0;
	double translate_x = 0.0;
	double translate_y = 0.0;

	switch ( cam.filmFit() ) 
	{
	case MFnCamera::kFillFilmFit:
		if ( window_aspect < film_aspect ) 
		{
			scale_x = window_aspect / film_aspect;
		}
		else 
		{
			scale_y = film_aspect / window_aspect;
		}
		break;
	case MFnCamera::kHorizontalFilmFit:
		scale_y = film_aspect / window_aspect;
		if ( scale_y > 1.0 ) 
		{
			translate_y = cam.filmFitOffset() *
				( aperture_y - ( aperture_y * scale_y ) ) / 2.0;
		}
		break;
	case MFnCamera::kVerticalFilmFit:
		scale_x = window_aspect / film_aspect;
		if (scale_x > 1.0 ) 
		{
			translate_x = cam.filmFitOffset() *
				( aperture_x - ( aperture_x * scale_x ) ) / 2.0;
		}
		break;
	case MFnCamera::kOverscanFilmFit:
		if ( window_aspect < film_aspect ) 
		{
			scale_y = film_aspect / window_aspect;
		}
		else 
		{
			scale_x = window_aspect / film_aspect;
		}
		break;
	case MFnCamera::kInvalid:
		break;
	}

	left   = focal_to_near * (-.5 * aperture_x * scale_x + offset_x + translate_x );
	right  = focal_to_near * ( .5 * aperture_x * scale_x + offset_x + translate_x );
	bottom = focal_to_near * (-.5 * aperture_y * scale_y + offset_y + translate_y );
	top    = focal_to_near * ( .5 * aperture_y * scale_y + offset_y + translate_y );
}
