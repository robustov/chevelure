#include "stdafx.h"
#include "CameraGenerator.h"
#include "MathNMaya/MathNMaya.h"
#include <maya/MFnCamera.h>
#include <maya/M3dView.h>
#include <maya/MDagPath.h>
#include <maya/MTransformationMatrix.h>
#include <maya/MMatrix.h>
#include <maya/MDistance.h>
#include <maya/MAnimControl.h>
#include <maya/MFnSpotLight.h>

#define MM_TO_INCH 0.03937

inline std::string getMtorValue(const char* name)
{
	char buf[512];
	_snprintf(buf, 512, "mtor control getvalue -rg %s", name);
	MString res;
	if( !MGlobal::executeCommand(buf, res))
		return "";
	return res.asChar();
}

bool CameraGenerator::OnGeometry(
	MObject& node, 
	cls::IRender* render, 
	Math::Box3f& box, 
	cls::IExportContext* context)
{
	if( !node.hasFn(MFn::kCamera) && 
		!node.hasFn(MFn::kSpotLight))
	{
		return false;
	}

//	render->PushAttributes();
	// camera::transform
	MDagPath path;
	MFnDagNode(node).getPath(path);
	MTransformationMatrix xform( path.inclusiveMatrix()	);
	double scale[] = { 1, 1, -1	};
	xform.setScale(	scale, MSpace::kTransform );
	Math::Matrix4f mview;
//	copy(mview, xform.asMatrixInverse());
	copy(mview, xform.asMatrix());
	render->Parameter("camera::transform", mview);

	if( node.hasFn(MFn::kCamera))
	{
		MFnCamera fnCamera(node);

		MString name = MFnDagNode(node).name();
		render->Parameter("camera::name", name.asChar());

		// resolution
		cls::PA<int> resolution(cls::PI_CONSTANT, 2);
		resolution[0] = atoi( getMtorValue("dspyFormatX").c_str());
		resolution[1] = atoi( getMtorValue("dspyFormatY").c_str());
		render->Parameter("camera::resolution", resolution);

		float format_ratio = (float)atof( getMtorValue("pixelRatio").c_str());
		render->Parameter("camera::ratio", format_ratio);

		// screen
		float sw_x = 1;
		float sw_y = resolution[1]/(float)resolution[0];
		cls::PA<float> screen(cls::PI_CONSTANT, 4);
		screen[0] = -sw_x;
		screen[1] = sw_x;
		screen[2] = -sw_y;
		screen[3] = sw_y;
		render->Parameter("camera::screen", screen);

		// camera::near
		// camera::far
		double neardb	   = 0.001;	   // TODO:	these values are duplicated	elsewhere in this file
		double fardb	  =	250000.0; // TODO: these values	are	duplicated elsewhere in	this file
		if ( fnCamera.isClippingPlanes() ) 
		{
			neardb = fnCamera.nearClippingPlane();
			fardb =	fnCamera.farClippingPlane();
		}
		render->Parameter("camera::near", (float)neardb);
		render->Parameter("camera::far", (float)fardb);

		// Projection
		int cam_width = 640;
		int cam_height = 480;
		float aspectRatio = 1;
		double fov_ratio = 1;
		getCameraInfo(fnCamera, cam_width, cam_height, fov_ratio );

		bool isOrtho = fnCamera.isOrtho();
		double hFOV =	fnCamera.horizontalFieldOfView()/fov_ratio;
		double orthoWidth	= fnCamera.orthoWidth();
		double orthoHeight = fnCamera.orthoWidth() * ((float)cam_height / (float)cam_width);
	//	bool motionBlur	= fnCamera.isMotionBlur();
		double focalLength = fnCamera.focalLength();
		double focalDistance = fnCamera.focusDistance();
		double fStop = fnCamera.fStop();
		if ( isOrtho ) 
		{
			// ��� ����� �� ��!!!
			MDistance unitHelper;
			MDistance unit( 1, unitHelper.uiUnit() );
			double frameWidth, frameHeight;
			// the whole frame width has to be scaled according to the UI Unit
			frameWidth  = orthoWidth  * 5 * unit.asMeters();
			frameHeight = orthoHeight * 5 * unit.asMeters();
			render->Parameter("camera::projection", "orthographic");
		} 
		else 
		{
			float fieldOfView = (float)(hFOV * 180.0 / M_PI);
			render->Parameter("camera::projection", "perspective");
			render->Parameter("camera::fov", fieldOfView);
		}
	}
	else if(node.hasFn(MFn::kSpotLight))
	{
		MFnSpotLight fnSpot(node);

		render->Parameter("camera::near", 0.001f);
		render->Parameter("camera::far", 10000.f);

		float coneAngle = (float)fnSpot.coneAngle();
		float penumbraAngle = (float)fnSpot.penumbraAngle();
		if( penumbraAngle<0) penumbraAngle = 0;
		float hFOV = coneAngle + penumbraAngle*2;
		float fieldOfView = (float)(hFOV * 180.0 / M_PI);

		render->Parameter("camera::projection", "perspective");
		render->Parameter("camera::fov", fieldOfView);
	}
	render->RenderCall("Camera");
//	render->PopAttributes();
	return true;
}


void CameraGenerator::computeViewingFrustum ( 
	double     window_aspect,
	double&    left,
	double&    right,
	double&    bottom,
	double&    top,
	MFnCamera& cam )
{
	double film_aspect   = cam.aspectRatio();
	double aperture_x    = cam.horizontalFilmAperture();
	double aperture_y    = cam.verticalFilmAperture();
	double offset_x      = cam.horizontalFilmOffset();
	double offset_y      = cam.verticalFilmOffset();
	double focal_to_near = cam.nearClippingPlane() / (cam.focalLength() * MM_TO_INCH);

	focal_to_near *= cam.cameraScale();

	double scale_x = 1.0;
	double scale_y = 1.0;
	double translate_x = 0.0;
	double translate_y = 0.0;

	switch ( cam.filmFit() ) 
	{
	case MFnCamera::kFillFilmFit:
		if ( window_aspect < film_aspect ) 
		{
			scale_x = window_aspect / film_aspect;
		}
		else 
		{
			scale_y = film_aspect / window_aspect;
		}
		break;
	case MFnCamera::kHorizontalFilmFit:
		scale_y = film_aspect / window_aspect;
		if ( scale_y > 1.0 ) 
		{
			translate_y = cam.filmFitOffset() *
				( aperture_y - ( aperture_y * scale_y ) ) / 2.0;
		}
		break;
	case MFnCamera::kVerticalFilmFit:
		scale_x = window_aspect / film_aspect;
		if (scale_x > 1.0 ) 
		{
			translate_x = cam.filmFitOffset() *
				( aperture_x - ( aperture_x * scale_x ) ) / 2.0;
		}
		break;
	case MFnCamera::kOverscanFilmFit:
		if ( window_aspect < film_aspect ) 
		{
			scale_y = film_aspect / window_aspect;
		}
		else 
		{
			scale_x = window_aspect / film_aspect;
		}
		break;
	case MFnCamera::kInvalid:
		break;
	}

	left   = focal_to_near * (-.5 * aperture_x * scale_x + offset_x + translate_x );
	right  = focal_to_near * ( .5 * aperture_x * scale_x + offset_x + translate_x );
	bottom = focal_to_near * (-.5 * aperture_y * scale_y + offset_y + translate_y );
	top    = focal_to_near * ( .5 * aperture_y * scale_y + offset_y + translate_y );
}
void CameraGenerator::portFieldOfView( 
	int port_width, int port_height,
	double& horizontal,
	double& vertical,
	MFnCamera& fnCamera )
{
	double left, right, bottom, top;
	double aspect = (double) port_width / port_height;
	computeViewingFrustum(aspect,left,right,bottom,top,fnCamera);

	double neardb = fnCamera.nearClippingPlane();
	horizontal = atan( ( ( right - left ) * 0.5 ) / neardb ) * 2.0;
	vertical = atan( ( ( top - bottom ) * 0.5 ) / neardb ) * 2.0;
}

void CameraGenerator::getCameraInfo( MFnCamera& cam, int cam_width, int cam_height, double fov_ratio )
//
//  Description:
//      Get information about the given camera
//
{
	// Resoultion can change if camera film-gate clips image
	// so we must keep camera width/height separate from render
	// globals width/height.
	//

	M3dView m_activeView = M3dView::active3dView();
	int width        = m_activeView.portWidth();
	int height       = m_activeView.portHeight();
	bool ignoreFilmGate = true;

	cam_width  = width;
	cam_height = height;

	// If we are using a film-gate then we may need to
	// adjust the resolution to simulate the 'letter-boxed'
	// effect.
	if ( cam.filmFit() == MFnCamera::kHorizontalFilmFit ) 
	{
		if ( !ignoreFilmGate ) 
		{
			double new_height = cam_width /
				( cam.horizontalFilmAperture() /
				cam.verticalFilmAperture() );

			if ( new_height < cam_height ) 
			{
				cam_height = ( int )new_height;
			}
		}

		double hfov, vfov;
		portFieldOfView( cam_width, cam_height, hfov, vfov, cam );
		fov_ratio = hfov / vfov;
	}
	else if ( cam.filmFit() == MFnCamera::kVerticalFilmFit ) 
	{
		double new_width = cam_height /
			( cam.verticalFilmAperture() /
			cam.horizontalFilmAperture() );

		double hfov, vfov;

		// case 1 : film-gate smaller than resolution
		//         film-gate on
		if ( ( new_width < cam_width ) && ( !ignoreFilmGate ) ) 
		{
			cam_width = ( int )new_width;
			fov_ratio = 1.0;
		}

		// case 2 : film-gate smaller than resolution
		//         film-gate off
		else if ( ( new_width < cam_width ) && ( ignoreFilmGate ) ) 
		{
			portFieldOfView( ( int )new_width, cam_height, hfov, vfov, cam );
			fov_ratio = hfov / vfov;
		}

		// case 3 : film-gate larger than resolution
		//         film-gate on
		else if ( !ignoreFilmGate ) 
		{
			portFieldOfView( ( int )new_width, cam_height, hfov, vfov, cam );
			fov_ratio = hfov / vfov;
		}

		// case 4 : film-gate larger than resolution
		//         film-gate off
		else if ( ignoreFilmGate ) 
		{
			portFieldOfView( ( int )new_width, cam_height, hfov, vfov, cam );
			fov_ratio = hfov / vfov;
		}

	}
	else if ( cam.filmFit() == MFnCamera::kOverscanFilmFit ) 
	{
		double new_height = cam_width /
			( cam.horizontalFilmAperture() /
			cam.verticalFilmAperture() );
		double new_width = cam_height /
			( cam.verticalFilmAperture() /
			cam.horizontalFilmAperture() );

		if ( new_width < cam_width ) 
		{
			if ( !ignoreFilmGate ) 
			{
				cam_width = ( int ) new_width;
				fov_ratio = 1.0;
			}
			else 
			{
				double hfov, vfov;
				portFieldOfView( ( int )new_width, cam_height, hfov, vfov, cam );
				fov_ratio = hfov / vfov;
			}
		}
		else {
			if ( !ignoreFilmGate )
				cam_height = ( int ) new_height;

			double hfov, vfov;
			portFieldOfView( cam_width, cam_height, hfov, vfov, cam );
			fov_ratio = hfov / vfov;
		}
	}
	else if ( cam.filmFit() == MFnCamera::kFillFilmFit ) 
	{
		double new_width = cam_height /
			( cam.verticalFilmAperture() /
			cam.horizontalFilmAperture() );
		double hfov, vfov;

		if ( new_width >= cam_width ) 
		{
			portFieldOfView( ( int )new_width, cam_height, hfov, vfov, cam );
			fov_ratio = hfov / vfov;
		}
		else 
		{
			portFieldOfView( cam_width, cam_height, hfov, vfov, cam );
			fov_ratio = hfov / vfov;
		}
	}
}























































bool CameraGenerator_test::Generate(
							   MObject&	node,
							   IPrman* prman
							   )
{
	MFnCamera fnCamera(	node );
	//	  iter->gotJobOptions =	false;

	MStatus	status;
	status.clear();
	//	MPlug cPlug	= fnCamera.findPlug( MString( "ribOptions" ), &status );
	//	if ( status	== MS::kSuccess	) {
	//		cPlug.getValue(	iter->jobOptions );
	//		iter->gotJobOptions	= true;
	//	}
	// Resoultion can change if camera film-gate clips image
	// so we must keep camera width/height separate from render
	// globals width/height.
	//

	int cam_width = 640;
	int cam_height = 480;
	float aspectRatio = 1;
	double fov_ratio = 1;
	getCameraInfo(fnCamera, cam_width, cam_height, fov_ratio );

//	iter->width	= cam_width;
//	iter->height = cam_height;
	// Renderman specifies shutter by time open
	// so we need to convert shutterAngle to time.
	// To do this convert shutterAngle to degrees and
	// divide by 360.
	//
	double shutter = fnCamera.shutterAngle() * 0.5 / M_PI;
//	double liqglo_shutterTime = iter->camera[sample].shutter;
	double orthoWidth	= fnCamera.orthoWidth();
	double orthoHeight = fnCamera.orthoWidth() * ((float)cam_height /	(float)cam_width);
	bool motionBlur	= fnCamera.isMotionBlur();
	double focalLength = fnCamera.focalLength();
	double focalDistance = fnCamera.focusDistance();
	double fStop = fnCamera.fStop();
	MDagPath path;
	fnCamera.getPath(path);
	MTransformationMatrix xform( path.inclusiveMatrix()	);
	double scale[] = { 1, 1, -1	};
	//xform.addScale( scale, MSpace::kTransform	);
	xform.setScale(	scale, MSpace::kTransform );

	MMatrix mat = xform.asMatrixInverse();

	double neardb	   = 0.001;	   // TODO:	these values are duplicated	elsewhere in this file
	double fardb	  =	250000.0; // TODO: these values	are	duplicated elsewhere in	this file

	if ( fnCamera.isClippingPlanes() ) 
	{
		neardb	   = fnCamera.nearClippingPlane();
		fardb	  =	fnCamera.farClippingPlane();
	}
	bool isOrtho = fnCamera.isOrtho();

	// The camera's	fov	may	not	match the rendered image in	Maya
	// if a	film-fit is	used. 'fov_ratio' is used to account for
	// this.
	//
	double hFOV =	fnCamera.horizontalFieldOfView()/fov_ratio;
//	double aspectRatio =	aspectRatio;


	{
		double quantValue = 0;
		float m_rgain=1.f, m_rgamma=1.f;

		MTime curtime = MAnimControl::currentTime();
		float oframe = (float)curtime.as(MTime::kSeconds);
		curtime += MTime( 1, MTime::uiUnit());
		float cframe = (float)curtime.as(MTime::kSeconds);

		{
			// Smooth Shading
			prman->RiShadingInterpolation( "smooth" );
			// Quantization
			/*/
			if ( quantValue != 0 ) 
			{
				int whiteValue = (int) pow( 2.0, quantValue ) - 1;
				prman->RiQuantize( prman->RI_RGBA(), whiteValue, 0, whiteValue, 0.5 );
			} 
			else 
			{
				prman->RiQuantize( prman->RI_RGBA(), 0, 0, 0, 0 );
			}
			/*/
			if ( m_rgain != 1.0 || m_rgamma != 1.0 ) 
			{
				prman->RiExposure( m_rgain, m_rgamma );
			}
			prman->RiPixelSamples( 3, 3);
			prman->RiPixelFilter( prman->RiBoxFilter(), 2, 2);

		}
		// Display driver
	    prman->RiDisplay("foo", "it", prman->RI_RGBA(), RI_NULL);

		// Setting Resolution\n
	    prman->RiFormat( cam_width, cam_height, aspectRatio );

		if ( isOrtho ) 
		{
			MDistance unitHelper;
			MDistance unit( 1, unitHelper.uiUnit() );
			double frameWidth, frameHeight;
			// the whole frame width has to be scaled according to the UI Unit
			frameWidth  = orthoWidth  * 5 * unit.asMeters();
			frameHeight = orthoHeight * 5 * unit.asMeters();
			prman->RiProjection( "orthographic", RI_NULL );
			prman->RiScreenWindow( -(RtFloat)frameWidth, (RtFloat)frameWidth, -(RtFloat)frameHeight, (RtFloat)frameHeight );
		} 
		else 
		{
			RtFloat fieldOfView = (RtFloat)(hFOV * 180.0 / M_PI);
			prman->RiProjection( "perspective", prman->RI_FOV(), &fieldOfView, RI_NULL );
		}
	    prman->RiClipping( (RtFloat)neardb, (RtFloat)fardb );
		if( false)
		{
			prman->RiDepthOfField( (RtFloat)fStop, (RtFloat)focalLength, (RtFloat)focalDistance );
		}
		if( true)
		{
		    prman->RiShutter( oframe, cframe );
		}

		{
			RtMatrix cameraMatrix;
			mat.get( cameraMatrix );
			prman->RiTransform( cameraMatrix );
		}
	}

	return true;
}

void CameraGenerator_test::portFieldOfView( 
	int port_width, int port_height,
	double& horizontal,
	double& vertical,
	MFnCamera& fnCamera )
{
	double left, right, bottom, top;
	double aspect = (double) port_width / port_height;
	computeViewingFrustum(aspect,left,right,bottom,top,fnCamera);

	double neardb = fnCamera.nearClippingPlane();
	horizontal = atan( ( ( right - left ) * 0.5 ) / neardb ) * 2.0;
	vertical = atan( ( ( top - bottom ) * 0.5 ) / neardb ) * 2.0;
}

void CameraGenerator_test::computeViewingFrustum ( 
	double     window_aspect,
	double&    left,
	double&    right,
	double&    bottom,
	double&    top,
	MFnCamera& cam )
{
	double film_aspect   = cam.aspectRatio();
	double aperture_x    = cam.horizontalFilmAperture();
	double aperture_y    = cam.verticalFilmAperture();
	double offset_x      = cam.horizontalFilmOffset();
	double offset_y      = cam.verticalFilmOffset();
	double focal_to_near = cam.nearClippingPlane() / (cam.focalLength() * MM_TO_INCH);

	focal_to_near *= cam.cameraScale();

	double scale_x = 1.0;
	double scale_y = 1.0;
	double translate_x = 0.0;
	double translate_y = 0.0;

	switch ( cam.filmFit() ) 
	{
	case MFnCamera::kFillFilmFit:
		if ( window_aspect < film_aspect ) 
		{
			scale_x = window_aspect / film_aspect;
		}
		else 
		{
			scale_y = film_aspect / window_aspect;
		}
		break;
	case MFnCamera::kHorizontalFilmFit:
		scale_y = film_aspect / window_aspect;
		if ( scale_y > 1.0 ) 
		{
			translate_y = cam.filmFitOffset() *
				( aperture_y - ( aperture_y * scale_y ) ) / 2.0;
		}
		break;
	case MFnCamera::kVerticalFilmFit:
		scale_x = window_aspect / film_aspect;
		if (scale_x > 1.0 ) 
		{
			translate_x = cam.filmFitOffset() *
				( aperture_x - ( aperture_x * scale_x ) ) / 2.0;
		}
		break;
	case MFnCamera::kOverscanFilmFit:
		if ( window_aspect < film_aspect ) 
		{
			scale_y = film_aspect / window_aspect;
		}
		else 
		{
			scale_x = window_aspect / film_aspect;
		}
		break;
	case MFnCamera::kInvalid:
		break;
	}

	left   = focal_to_near * (-.5 * aperture_x * scale_x + offset_x + translate_x );
	right  = focal_to_near * ( .5 * aperture_x * scale_x + offset_x + translate_x );
	bottom = focal_to_near * (-.5 * aperture_y * scale_y + offset_y + translate_y );
	top    = focal_to_near * ( .5 * aperture_y * scale_y + offset_y + translate_y );
}

void CameraGenerator_test::getCameraInfo( MFnCamera& cam, int cam_width, int cam_height, double fov_ratio )
//
//  Description:
//      Get information about the given camera
//
{
	// Resoultion can change if camera film-gate clips image
	// so we must keep camera width/height separate from render
	// globals width/height.
	//

	M3dView m_activeView = M3dView::active3dView();
	int width        = m_activeView.portWidth();
	int height       = m_activeView.portHeight();
	bool ignoreFilmGate = true;

	cam_width  = width;
	cam_height = height;

	// If we are using a film-gate then we may need to
	// adjust the resolution to simulate the 'letter-boxed'
	// effect.
	if ( cam.filmFit() == MFnCamera::kHorizontalFilmFit ) 
	{
		if ( !ignoreFilmGate ) 
		{
			double new_height = cam_width /
				( cam.horizontalFilmAperture() /
				cam.verticalFilmAperture() );

			if ( new_height < cam_height ) 
			{
				cam_height = ( int )new_height;
			}
		}

		double hfov, vfov;
		portFieldOfView( cam_width, cam_height, hfov, vfov, cam );
		fov_ratio = hfov / vfov;
	}
	else if ( cam.filmFit() == MFnCamera::kVerticalFilmFit ) 
	{
		double new_width = cam_height /
			( cam.verticalFilmAperture() /
			cam.horizontalFilmAperture() );

		double hfov, vfov;

		// case 1 : film-gate smaller than resolution
		//         film-gate on
		if ( ( new_width < cam_width ) && ( !ignoreFilmGate ) ) 
		{
			cam_width = ( int )new_width;
			fov_ratio = 1.0;
		}

		// case 2 : film-gate smaller than resolution
		//         film-gate off
		else if ( ( new_width < cam_width ) && ( ignoreFilmGate ) ) 
		{
			portFieldOfView( ( int )new_width, cam_height, hfov, vfov, cam );
			fov_ratio = hfov / vfov;
		}

		// case 3 : film-gate larger than resolution
		//         film-gate on
		else if ( !ignoreFilmGate ) 
		{
			portFieldOfView( ( int )new_width, cam_height, hfov, vfov, cam );
			fov_ratio = hfov / vfov;
		}

		// case 4 : film-gate larger than resolution
		//         film-gate off
		else if ( ignoreFilmGate ) 
		{
			portFieldOfView( ( int )new_width, cam_height, hfov, vfov, cam );
			fov_ratio = hfov / vfov;
		}

	}
	else if ( cam.filmFit() == MFnCamera::kOverscanFilmFit ) 
	{
		double new_height = cam_width /
			( cam.horizontalFilmAperture() /
			cam.verticalFilmAperture() );
		double new_width = cam_height /
			( cam.verticalFilmAperture() /
			cam.horizontalFilmAperture() );

		if ( new_width < cam_width ) 
		{
			if ( !ignoreFilmGate ) 
			{
				cam_width = ( int ) new_width;
				fov_ratio = 1.0;
			}
			else 
			{
				double hfov, vfov;
				portFieldOfView( ( int )new_width, cam_height, hfov, vfov, cam );
				fov_ratio = hfov / vfov;
			}
		}
		else {
			if ( !ignoreFilmGate )
				cam_height = ( int ) new_height;

			double hfov, vfov;
			portFieldOfView( cam_width, cam_height, hfov, vfov, cam );
			fov_ratio = hfov / vfov;
		}
	}
	else if ( cam.filmFit() == MFnCamera::kFillFilmFit ) 
	{
		double new_width = cam_height /
			( cam.verticalFilmAperture() /
			cam.horizontalFilmAperture() );
		double hfov, vfov;

		if ( new_width >= cam_width ) 
		{
			portFieldOfView( ( int )new_width, cam_height, hfov, vfov, cam );
			fov_ratio = hfov / vfov;
		}
		else 
		{
			portFieldOfView( cam_width, cam_height, hfov, vfov, cam );
			fov_ratio = hfov / vfov;
		}
	}
}
