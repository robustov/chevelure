#pragma once

#include "ocellaris/IAttributeGenerator.h"

//! @ingroup implement_group
//! \brief ����������� ��������� �������� "��� �������"
//! ������������� ������ ���� � ������� name
//! 
struct NameAttributeGenerator : public cls::IAttributeGenerator
{
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "NameAttributeGenerator";};

	// � ����� ������ ��������
	int GetCallType(
		MObject& generatornode
		){return CT_BEFORE_TRANSFORM;}

	/// OnAttribute
	virtual bool OnAttribute(
		cls::INode& node,			//!< ������ ��� �������� ����������� �������
		cls::IRender* render,	//!< render
		cls::IExportContext* context, 
//		const char* attrName,	//!< ��� ��������
//		cls::enParamType type,
		MObject& generatornode
		);
};
