#pragma once

#include "ocellaris/ITransformGenerator.h"
//! @ingroup implement_group
//! \brief ����������� ��������� ������������� ��� ��������� MayaTransform. ������ �� ������� � ��������� ��������� ���� ����������
//! 

/*/
//! vector MayaTransform::translate 
//! vector MayaTransform::rotate
//! vector MayaTransform::scale
//! vector MayaTransform::shear 
//! int    MayaTransform::rotateOrder (0=xyz, 1=yzx, 2=zxy, 3=xzy, 4=yxz, 5=zyx)
//! vector MayaTransform::scalePivot 
//! vector MayaTransform::scalePivotTranslate 
//! vector MayaTransform::rotatePivot 
//! vector MayaTransform::rotatePivotTranslate 
//! vector MayaTransform::rotateAxis 
/*/

struct MayaTransformGenerator : public cls::ITransformGenerator
{
//	int level;
//	Math::Matrix4f trans;

	MayaTransformGenerator();
public:
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "MayaTransformGenerator";};

	//! ��������� ����� render->PushTransform(); 
	//! ������� ����� �������
	virtual void OnTransformStart(
		cls::INode& node,					//!< ������
		cls::IRender* render,				//!< render
		const Math::Matrix4f& curtransform,	//!< ������� ���������
		Math::Matrix4f& newtransform,		//!< ����� ���������
		cls::IExportContext* context,		
		MObject& generatornode				//!< generator node �.� 0
		);
	//! ��������� ����� { AppendTransform ��� SetTransform }
	virtual void OnTransform(
		cls::INode& node,					//!< ������
		cls::IRender* render,				//!< render
		cls::IExportContext* context,
		MObject& generatornode				//!< generator node �.� 0
		);
	//! ��������� ����� render->PopAttributes()
	virtual void OnTransformEnd(
		cls::INode& node,					//!< ������
		cls::IRender* render,				//!< render
		cls::IExportContext* context,
		MObject& generatornode				//!< generator node �.� 0
		);
};
