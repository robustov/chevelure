#pragma once

#include "ocellaris/INode.h"
#include "mathNpixar/IPrman.h"
#include "ocellaris/OcellarisExport.h"

class MFnCamera;

#pragma warning ( disable : 4275)

//! @ingroup implement_group
//! \brief ���������� ������ ��� ����� � ������
//! 
//! ���������� ���������:
//! camera::resolution
//! camera::ratio
//! camera::screen
//! camera::projection
//! camera::fov
//! camera::near
//! camera::far
//! camera::transform		- �� ������������� ������� ������������ ������

struct OCELLARISEXPORT_API CameraGenerator : public cls::IGeometryGenerator
{
	bool OnGeometry(
		MObject& node, 
		cls::IRender* render, 
		Math::Box3f& box, 
		cls::IExportContext* context);

protected:
	void getCameraInfo( MFnCamera& cam, int cam_width, int cam_height, double fov_ratio );

	void portFieldOfView( 
		int port_width, int port_height,
		double& horizontal,
		double& vertical,
		MFnCamera& fnCamera );

	void computeViewingFrustum ( 
		double     window_aspect,
		double&    left,
		double&    right,
		double&    bottom,
		double&    top,
		MFnCamera& cam );

};


//! @ingroup implement_group
//! \brief ��������! ������������� ��������� ��������� ����������! �����!
//! 
struct OCELLARISEXPORT_API CameraGenerator_test
{
	virtual bool Generate(
		MObject& node,
		IPrman* prman
		);

protected:
	void getCameraInfo( MFnCamera& cam, int cam_width, int cam_height, double fov_ratio );

	void portFieldOfView( 
		int port_width, int port_height,
		double& horizontal,
		double& vertical,
		MFnCamera& fnCamera );

	void computeViewingFrustum ( 
		double     window_aspect,
		double&    left,
		double&    right,
		double&    bottom,
		double&    top,
		MFnCamera& cam );

};
