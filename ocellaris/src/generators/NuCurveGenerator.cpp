#include "stdafx.h"
#include "NuCurveGenerator.h"
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MItCurveCV.h>
#include <maya/MPlug.h>
#include <maya/MPoint.h>
#include <maya/MPointArray.h>
#include <string>
#include "ocellaris/OcellarisExport.h"

NuCurveGenerator nucurvegenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl NuCurveGenerator()
	{
		return &nucurvegenerator;
	}
}

// ��� �������� � ����
void NuCurveGenerator::OnLoadToMaya(
	MObject& generatornode
	)
{
	MObject attr;
	attr = ocExport_AddAttrType(generatornode, "baseWidth",   cls::PT_FLOAT, cls::P<float>(1));
	attr = ocExport_AddAttrType(generatornode, "tipWidth",    cls::PT_FLOAT, cls::P<float>(1));
	attr = ocExport_AddAttrType(generatornode, "renderWidth", cls::PT_FLOAT, cls::P<float>(0.1f));
}

bool NuCurveGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	float baseWidth = 1;
	float tipWidth = 1;   
	float renderWidth = 0.1f;
	if( !generatornode.isNull())
	{
		cls::P<float> _baseWidth   = ocExport_GetAttrValue(generatornode, "baseWidth");
		cls::P<float> _tipWidth    = ocExport_GetAttrValue(generatornode, "tipWidth");
		cls::P<float> _renderWidth = ocExport_GetAttrValue(generatornode, "renderWidth");
		if( !_baseWidth.empty()) baseWidth = *_baseWidth;
		if( !_tipWidth.empty()) tipWidth = *_tipWidth;
		if( !_renderWidth.empty()) renderWidth = *_renderWidth;
	}
	
	MObject node = inode.getObject();
	MStatus stat;
	MFnNurbsCurve nurbs( node, &stat );
	if( !stat) 
		return false;

	int ncurves = 1;
	int cvc = nurbs.numCVs();
	int degree = nurbs.degree();
	MFnNurbsCurve::Form form = nurbs.form();

	MPointArray array;
	nurbs.getCVs( array);
	int l = array.length();

	std::string interp;
	std::string wrap;
	cls::PA<Math::Vec3f> CVs(cls::PI_VERTEX, 0, cls::PT_POINT);
	cls::PA<float> W( cls::PI_VARYING);

	if( degree==1)
	{
		interp = "linear";
		wrap = "nonperiodic";
		CVs.resize(l);
		W.resize(l);

		MPoint p;
		for(int i=0; i<l; i++) 
		{
			p = array[i];
			CVs[i] = Math::Vec3f((float)p.x, (float)p.y, (float)p.z);
			float f = i/(float)(l-1);
			W[i] = (1-f)*baseWidth + f*tipWidth;
			W[i] *= renderWidth;
			box.insert(CVs[i]);
		}
	}
	else if(degree==3)
	{
		interp = "cubic";
		if( form == MFnNurbsCurve::kPeriodic)
		{
			wrap = "periodic";
			W.reserve(l);
			CVs.reserve(l);
		}
		else
		{
			wrap = "nonperiodic";
			CVs.reserve(l+4);
			W.reserve(l+2);
		}

		MPoint p;
		if( form != MFnNurbsCurve::kPeriodic)
		{
			p = array[0];
			Math::Vec3f v((float)p.x, (float)p.y, (float)p.z);
			CVs.push_back(v);
			CVs.push_back(v);
			W.push_back(baseWidth*renderWidth);
		}

		for(int i=0; i<l; i++) 
		{
			p = array[i];
			Math::Vec3f v((float)p.x, (float)p.y, (float)p.z);
			CVs.push_back(v);
			box.insert(v);

			float f = i/(float)(l-1);
			float w = (1-f)*baseWidth + f*tipWidth;
			w *= renderWidth;
			W.push_back(w);
		}
		if( form != MFnNurbsCurve::kPeriodic)
		{
			p = array[l-1];
			Math::Vec3f v((float)p.x, (float)p.y, (float)p.z);
			CVs.push_back(v);
			CVs.push_back(v);
			W.push_back(tipWidth*renderWidth);
		}

		if( render)
		{
			render->Parameter("@basis", "b-spline");
		}
	}
	cls::PA<int> nvert(cls::PI_PRIMITIVE, 1); 
	nvert[0] = CVs.size();

	if( render)
	{
		render->Parameter("#width", W);	// constantwidth ???
		render->Parameter("P", cls::PT_POINT, CVs);
		render->Curves( interp.c_str(), wrap.c_str(), nvert);
	}

	return true;
}
