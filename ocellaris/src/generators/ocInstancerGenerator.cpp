#include "stdafx.h"
#include "ocInstancerGenerator.h"
#include "ocInstance.h"
#include "ocellaris/MInstancePxData.h"
#include <maya/MFnDependencyNode.h>
#include <maya/MFnParticleSystem.h> 
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MPlug.h>
#include <maya/MVector.h>
#include <maya/MPxLocatorNode.h>
#include <maya/MPxLocatorNode.h>
#include <maya/MFnPluginData.h>
#include "mathNmaya/mathNmaya.h"
#include "util/misc_create_directory.h"

// �� ������������ ��. ocInstancerProxyGenerator
bool ocInstancerGenerator::OnGeometry(MObject& node, cls::IRender* render, Math::Box3f& box, cls::IExportContext* context)
{
	MFnDependencyNode dn(node);
	ocInstance* ocinstance = (ocInstance*)dn.userNode();
	if( !ocinstance) return false;
	ocInstance2Geometry* geom = ocinstance->getGeometry();
	if( !geom) return false;

	MStatus stat;

	MPoint min, max;
	MObject val;
	MPlug(node, MPxLocatorNode::nodeBoundingBoxMin).getValue(val);
	MFnNumericData(val).getData(min.x, min.y, min.z);
	MPlug(node, MPxLocatorNode::nodeBoundingBoxMax).getValue(val);
	MFnNumericData(val).getData(max.x, max.y, max.z);

	copy( box.min, min);
	copy( box.max, max);

	cls::MInstancePxData* cache = geom->cache;
	if( !cache) 
		return false;

	const char* filename = cache->drawcache.getFile();
	if( !filename) 
		return false;
//	MPlug plug = MFnDependencyNode(node).findPlug("filename");
//	MString filename;
//	plug.getValue(filename);

	if(render)
	{
		std::string fn = filename;
		Util::changeSlashToSlash(fn);
		if( geom->matricies.empty())
		{
			cache->drawcache.SetAttrToRender(node, render);
			render->Parameter("filename", fn.c_str());
			render->Parameter("#bound", box);
			render->Parameter("id", (int)0);
			render->Parameter("@delay", true);
			render->RenderCall("ReadOCS");
		}
		else
		{
			for(int i=0; i<(int)geom->matricies.size(); i++)
			{
				render->PushTransform();
				Math::Matrix4f pos = geom->matricies[i];
				render->AppendTransform(pos);
//				geom->cache->drawcache.RenderToMaya(obj, false, true, false);
				render->Parameter("filename", fn.c_str());
				render->Parameter("#bound", box);
				render->Parameter("id", (int)0);
				render->Parameter("@delay", true);
				render->RenderCall("ReadOCS");
				render->PopTransform();
			}
		}
	}
	return true;
}
