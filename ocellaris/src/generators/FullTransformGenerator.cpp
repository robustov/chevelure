#include "stdafx.h"
#include "FullTransformGenerator.h"
#include <maya/MFnMesh.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MPlug.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MIntArray.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnTransform.h>
#include <maya/MTransformationMatrix.h>
#include <string>
#include <mathNmaya/mathNmaya.h>
#include "math/Matrix.h"

FullTransformGenerator fulltransformgenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl FullTransform()
	{
		return &fulltransformgenerator;
	}
}

FullTransformGenerator::FullTransformGenerator()
{
}

//! render ����� ���� = NULL
void FullTransformGenerator::OnTransformStart(
	cls::INode& node,					//!< ������
	cls::IRender* render,				//!< render
	const Math::Matrix4f& curtransform,	//!< ������� ���������
	Math::Matrix4f& newtransform,		//!< ����� ���������
	cls::IExportContext* context,		
	MObject& generatornode				//!< generator node �.� 0
	)
{
	MDagPath path = node.getPath();
	MMatrix tm = path.exclusiveMatrix();
	Math::Matrix4f m;
	copy( m, tm);

	newtransform = m*curtransform;

	if( render)
	{
		render->PushTransform();
	}
}

//! ��������� ����� { AppendTransform ��� SetTransform }
void FullTransformGenerator::OnTransform(
	cls::INode& node,					//!< ������
	cls::IRender* render,				//!< render
	cls::IExportContext* context,
	MObject& generatornode				//!< generator node �.� 0
	)
{
	MDagPath path = node.getPath();
	MMatrix tm = path.exclusiveMatrix();
	Math::Matrix4f m;
	copy( m, tm);

	if( render)
	{
		render->PushAttributes();
		render->AppendTransform(m);
		render->PopAttributes();
	}
}

void FullTransformGenerator::OnTransformEnd(
	cls::INode& node,					//!< ������
	cls::IRender* render,				//!< render
	cls::IExportContext* context,
	MObject& generatornode				//!< generator node �.� 0
	)
{
	if( render)
	{
		render->PopTransform();
	}
}

