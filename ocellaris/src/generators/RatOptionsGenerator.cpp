#include "stdafx.h"
#include "RatOptionsGenerator.h"
#include "MathNMaya/MathNMaya.h"
#include <maya/MFnCamera.h>
#include <maya/M3dView.h>
#include <maya/MDagPath.h>
#include <maya/MTransformationMatrix.h>
#include <maya/MMatrix.h>
#include <maya/MDistance.h>
#include <maya/MAnimControl.h>
#include <maya/MFnSpotLight.h>

#define MM_TO_INCH 0.03937

inline std::string getMtorValue(const char* name)
{
	char buf[512];
	_snprintf(buf, 512, "mtor control getvalue -rg %s", name);
	MString res;
	if( !MGlobal::executeCommand(buf, res))
		return "";
	return res.asChar();
}

bool RatOptionsGenerator::OnOptions(
	cls::IRender* render, 
	cls::IExportContext* context)
{
	/*/
	cls::PA<int> format(cls::PI_CONSTANT, 2);
	format[0] = atoi( getMtorValue("dspyFormatX").c_str());
	format[1] = atoi( getMtorValue("dspyFormatY").c_str());
	float format_ratio = (float)atof( getMtorValue("pixelRatio").c_str());
	/*/

	// Exposure
	float dspyGain = (float)atof( getMtorValue("dspyGain").c_str());
	float dspyGamma = (float)atof( getMtorValue("dspyGamma").c_str());

	// RiPixelSamples
	cls::PA<float> pixelsample(cls::PI_CONSTANT, 2);
	pixelsample[0] = (float)atof( getMtorValue("pixelSamplesX").c_str());
	pixelsample[1] = (float)atof( getMtorValue("pixelSamplesY").c_str());

	//
	cls::PA<int> pixelfilter(cls::PI_CONSTANT, 2);
	pixelfilter[0] = atoi( getMtorValue("filterWidthX").c_str());
	pixelfilter[1] = atoi( getMtorValue("filterWidthY").c_str());
	std::string filter = getMtorValue("pixelFilter");

	/*/
Identity 
Attribute "dice" "string strategy" ["planarprojection"]
Attribute "dice" "string referencecamera" ["worldcamera"]
Attribute "dice" "int rasterorient" [1]
Option "user" "float tracebreadthfactor" [0] "float tracedepthfactor" [0]
PixelSamples 3 3
Exposure 1 1
Hider "hidden" "jitter" [1]
PixelFilter "separable-catmull-rom" 2 2
#Shutter 1 1.25
Shutter 0.0416667 0.0520833
Format 640 480 1
Display "D:/public/New_Project/rmanpix/untitled.0001" "it" "rgba" "int merge" [0] "string dspyParams" ["dspyRender -context D:/public/New_Project/x.mb -time 1 -renderer 2 -crop 0 1 0 1 -port 1920 -workspace D:/public/New_Project/;"]
ScreenWindow -1 1 -0.75 0.75
	/*/

//	render->PushAttributes();
	/*/
	render->Parameter("output::format", format);
	render->Parameter("output::ratio", format_ratio);

	cls::PA<float> screenwindow(cls::PI_CONSTANT, 4);
	screenwindow[0] = -sw_x;
	screenwindow[1] = sw_x;
	screenwindow[2] = -sw_y;
	screenwindow[3] = sw_y;
	render->Parameter("output::screenwindow", screenwindow);
	/*/

	render->Parameter("output::gain", dspyGain);
	render->Parameter("output::gamma", dspyGamma);

	render->Parameter("output::pixelsample", pixelsample);

	render->Parameter("output::filtersize", pixelfilter);
	render->Parameter("output::filtername", filter.c_str());

	render->RenderCall("Output");
//	render->PopAttributes();

	return true;
}

char* gulAllControls[] = {
// Dysplay
	"dspyName",
	"camName",
	"frameCamName",
	"contextString",
	"dspyFormatX",
	"dspyFormatY",
		"dspyUnique",
	"pixelRatio",
	"dspyServer",
	"dspyServerMode",
	"dspyQuantizeDither",
	"dspyQuantizeMax",
	"dspyQuantizeMin",
	"dspyQuantizeMode",
	"dspyQuantizeOne",
		"dspyGain",
		"dspyGamma",
	"dspyPrimaryOnly",

// CAMERA
	"blurCamera",
	"blurSubframe",
	"shutterAngle",
	"shutterConfig",
	"shutterTiming",

	"frontPlane",
	"backPlane",
	"doMotionBlur",
	"dofUseLookat",

// REYES
	"motionFactor",
	"filterWidthX",
	"filterWidthY",
	"pixelFilter",
	"pixelSamplesX",
	"pixelSamplesY",
	"shadingInterp",
	"shadingRate",
	"diceCamera",
	"diceStrategy",
	"shadowBias",
	"rasterOrient",
	"dspyBucketX",
	"dspyBucketY",
	"dspyGrid",

// RAYS
	"rayTrace",
	"traceBias",
	"traceBreadthSampleFactor",
	"traceDepthSampleFactor",
	"traceDisplacements",
	"traceMaxDepth",
	"traceMaxDiffuseDepth",
	"traceMaxSpecularDepth",
	"traceSampleMotion",
	"traceSpecularThreshold",

// RAYS::IRRADIANCE
	"irradianceFileMode",
	"irradianceHandle",
	"irradianceMaxError",
	"irradianceMaxPixelDist",
// RAYS::PHOTON
	"photonCausticMap",
	"photonEstimator",
	"photonGlobalMap",
	"photonShadingModel",

// ACCEL
"lazyCompute",

// SPOOL
// SPOOL:FRAMES
	"doAnim",
	"animFPS",
	"postFrameScript",
	"preFrameScript",
	"statisticsLevel",
	"sequenceStart",
	"sequenceStop",
	"referenceFrame",
	
// SPOOL:ALFRED
	"alfCrews",
	"alfEnvKey",
	"alfJobOptions",
	"alfJobPriority",
	"alfMetaData",
	"alfNRMMax",
	"alfNRMMin",
	"alfPause",
	"alfRendererArgs",
	"alfSvc",
	"alfTag",
	"alfWhenDoneCmd",
	"alfWhenErrorCmd",

// SPOOL:JOBSETUP
	"RIBFormat",
	"RIBGen",
	"RIBStyle",
	"renderer",
	"superframeCount",
	"jobChunkSize",
	"jobCleanup",

// SPOOL:CUSTOM
	"customImageMetrics",
	"customImageSvc",
	"customImageTag",
	"customImager",
	"customJobSettings",
	"customPreRenderCmd",
	"customPreRenderMetrics",
	"customPreRenderSvc",
	"customPreRenderTag",
	"customRenderMetrics",
	"customRenderSvc",
	"customRenderTag",
	"customRenderer",


// REST
"computeBy",
"computeLocation",
"computeOrder",
"computeStart",
"computeStop",
"computedMaps",
"convertTextures",
"cropWinXMax",
"cropWinXMin",
"cropWinYMax",
"cropWinYMin",
"doCrop",
"doDOF",

"hider",
"imager",
"jitter",
"lightsOnly",

"objectsOnly",
"selectedSet",

"zealousCaching"
};
