#include "stdafx.h"
#include "NurbsGenerator.h"
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MItCurveCV.h>
#include <maya/MPlug.h>
#include <maya/MPoint.h>
#include <maya/MPointArray.h>
#include <maya/MFnNurbsSurface.h>
#include <string>

bool NurbsGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();
	MStatus stat;

	MFnNurbsSurface nurbs( node, &stat );
	if( !stat )
	{
		return false;
	}

	if ( nurbs.isTrimmedSurface() && render )
	{
		int nloops = 0;

		cls::PA<int> ncurves( cls::PI_PRIMITIVE );	
		int ncurves_size = 0;
		cls::PA<int> order( cls::PI_PRIMITIVE );	
		int curve_size = 0;
		cls::PA<float> knot( cls::PI_VARYING );		
		int knot_size = 0;
		cls::PA<float> min( cls::PI_PRIMITIVE );
		cls::PA<float> max( cls::PI_PRIMITIVE );
		cls::PA<int> n( cls::PI_PRIMITIVE );
		cls::PA<float> u( cls::PI_VERTEX );			
		int cvs_size = 0;
		cls::PA<float> v( cls::PI_VERTEX );
		cls::PA<float> w( cls::PI_VERTEX );

		MPointArray M_CVs;

		for ( unsigned int i = 0; i< nurbs.numRegions(); i++ )
		{
			for ( unsigned int j = 0; j < nurbs.numBoundaries(i); j++ )
			{
				ncurves_size++;
				for ( unsigned int k = 0; k < nurbs.numEdges(i,j); k++ )
				{
					MObjectArray edges = nurbs.edge( i, j, k, true );
					for ( unsigned int l = 0; l < edges.length(); l++ )
					{
						MFnNurbsCurve edge( edges[l] );
						edge.getCVs( M_CVs );
						cvs_size += M_CVs.length();
						curve_size++;
						knot_size += edge.numKnots() + 2;
					}
				}
			}
		}

		ncurves.reserve( ncurves_size );
		order.reserve( curve_size );
		knot.reserve( knot_size );
		min.reserve( curve_size );
		max.reserve( curve_size );
		n.reserve( curve_size );
		u.reserve( cvs_size );
		v.reserve( cvs_size );
		w.reserve( cvs_size );

		for ( unsigned int i = 0; i< nurbs.numRegions(); i++ )
		{
			for ( unsigned int j = 0; j < nurbs.numBoundaries(i); j++ )
			{
				ncurves.push_back(0);
				for ( unsigned int k = 0; k < nurbs.numEdges(i,j); k++ )
				{
					MObjectArray edges = nurbs.edge( i, j, k, true );
					for ( unsigned int l = 0; l < edges.length(); l++ )
					{
						ncurves[ nloops + j] += 1;
						MFnNurbsCurve edge( edges[l] );
						edge.getCVs( M_CVs );

						for ( unsigned int m = 0; m < M_CVs.length(); m++ )
						{
							u.push_back( (float)M_CVs[m].x );
							v.push_back( (float)M_CVs[m].y );
							w.push_back( 1.0f );
						}

                        double min_d, max_d;
						edge.getKnotDomain( min_d, max_d );
						min.push_back( (float)min_d );
						max.push_back( (float)max_d );

						order.push_back( edge.degree()+1 );

						n.push_back( edge.numCVs() );

						knot.push_back( (float)edge.knot(0) );
						for ( int n = 0; n < edge.numKnots(); n++ )
						{
							knot.push_back( (float)edge.knot(n) );
						}
						knot.push_back( (float)edge.knot( edge.numKnots()-1) );
					}
				}
			}

			nloops += nurbs.numBoundaries(i);
		}

		render->Parameter( "nloops", nloops );
		render->Parameter( "ncurves", ncurves );
		render->Parameter( "order", order );
		render->Parameter( "knot", knot );
		render->Parameter( "min", min );
		render->Parameter( "max", max );
		render->Parameter( "n", n );
		render->Parameter( "u", u );
		render->Parameter( "v", v );
		render->Parameter( "w", w );
	}

	int nu;
	int uorder;
	cls::PA<float> uknot( cls::PI_VARYING );
	double umin;
	double umax;

	int nv;
	int vorder;
	cls::PA<float> vknot( cls::PI_VARYING );
	double vmin;
	double vmax;

	cls::PA<Math::Vec3f> CVs( cls::PI_VERTEX );

	MPointArray M_CVs;
	nurbs.getCVs( M_CVs );

	if ( render )
	{
		nu = nurbs.numCVsInU();
		nv = nurbs.numCVsInV();
		uorder = nurbs.degreeU() + 1;
		vorder = nurbs.degreeV() + 1;
		nurbs.getKnotDomain( umin, umax, vmin, vmax );

		CVs.reserve( M_CVs.length() );
		uknot.reserve( nurbs.numKnotsInU() + 2 );
		vknot.reserve( nurbs.numKnotsInV() + 2 );
	}

	for( int i = 0; i < nurbs.numCVsInV(); i++ )
	{
		for( int j = 0; j < nurbs.numCVsInU(); j++ )
		{
			int MIndex = j*nurbs.numCVsInV() + i;

			MPoint point = M_CVs[MIndex];
			Math::Vec3f p( (float)point.x, (float)point.y, (float)point.z );
			box.insert( p );

			if ( render )
			{
				CVs.push_back( p );
			}
		}
	}

	if ( render )
	{
		uknot.push_back( (float)nurbs.knotInU(0) );
		for( int i = 0; i < nurbs.numKnotsInU(); i++ )
		{
			uknot.push_back( (float)nurbs.knotInU(i) );
		}
		uknot.push_back( (float)nurbs.knotInU( nurbs.numKnotsInU()-1) );

		vknot.push_back( (float)nurbs.knotInV(0) );
		for( int i = 0; i < nurbs.numKnotsInV(); i++ )
		{
			vknot.push_back( (float)nurbs.knotInV(i) );
		}
		vknot.push_back( (float)nurbs.knotInV( nurbs.numKnotsInV()-1) );

		render->Parameter( "nu", nu );
		render->Parameter( "uorder", uorder );
		render->Parameter( "uknots", uknot );
		render->Parameter( "umin", (float)umin );
		render->Parameter( "umax", (float)umax );

		render->Parameter( "nv", nv );
		render->Parameter( "vorder", vorder );
		render->Parameter( "vknots", vknot );
		render->Parameter( "vmin", (float)vmin );
		render->Parameter( "vmax", (float)vmax );

		render->Parameter( "P", cls::PT_POINT, CVs );

		render->SystemCall( cls::SC_PATCH );
	}

	return true;
}
