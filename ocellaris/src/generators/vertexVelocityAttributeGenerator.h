
#pragma once

#include "ocellaris/OcellarisExport.h"
#include "ocellaris/IDeformGenerator.h"

#pragma warning ( disable:4275)
#pragma warning ( disable:4251)

//! ������� �������������� ������� "velocity" ������� ������ ������ �������� ������� �������� �� ����
//!
struct OCELLARISEXPORT_API VertexVelocityAttributeGenerator : public cls::IDeformGenerator
{
	// � ����� ������ ��������
	virtual int GetDeformCallType(
		MObject& generatornode){return cls::IDeformGenerator::CT_BEFORE_GEOMETRY;};

	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "VertexVelocityAttributeGenerator";};

	//! render ����� ���� = NULL
	virtual void OnGeometry(
		cls::INode& inode,				//!< ������ 
		cls::IRender* render,				//!< render
		cls::enSystemCall call,				//!< ��� ������ ��������� ��������: I_POINTS, I_CURVES, I_MESH ...
		cls::IExportContext* context, 
		MObject& generatornode			//!< generator node �.� 0
		);
};
