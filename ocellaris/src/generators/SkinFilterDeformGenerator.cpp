#include "stdafx.h"
#include "SkinFilterDeformGenerator.h"
#include <maya/MFnMesh.h>
#include <maya/MFloatArray.h>
#include <maya/MAnimControl.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatVector.h>
#include <maya/MPlug.h>
#include <maya/MFnSkinCluster.h>
#include <maya/MItGeometry.h>
#include "mathNmaya/mathNmaya.h"
#include <maya/MItDependencyGraph.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MFnSingleIndexedComponent.h>

SkinFilterDeformGenerator skinfilterdeformgenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl SkinFilterDeform()
	{
		return &skinfilterdeformgenerator;
	}
}

MObject getSkinCluster(MObject src)
{
	MStatus stat;

	MFnDependencyNode dn(src, &stat);
	if(!stat)
		return MObject::kNullObj;
	MPlug inmeshplug = dn.findPlug("inMesh", &stat);
	if(!stat)
		return MObject::kNullObj;
	MPlugArray plugarray;
	inmeshplug.connectedTo(plugarray, true, false, &stat);
	if(!stat || plugarray.length()!=1)
		return MObject::kNullObj;
	inmeshplug = plugarray[0];

	MObject skin = inmeshplug.node();
	MFnSkinCluster skinCluster(skin, &stat);
	if(!stat)
		return MObject::kNullObj;
	return skin;
}

// �� ���� ������ �������� ���������?
MObject getGeometrySource(
	MObject source
	)
{
	MStatus stat;
	MObject skin = getSkinCluster(source);
	MFnSkinCluster skinCluster(skin, &stat);

	// �������� ���������
	MObjectArray inobjects;
	skinCluster.getInputGeometry(inobjects);
	if( inobjects.length()!=1)
	{
		// ������ ��� �� �...
		printf("SkinFilterDeformGenerator: INVALID SKIN CLUSTER: InputGeometry = %d\n", inobjects.length());
		return MObject::kNullObj;
	}
	if( !inobjects[0].hasFn(MFn::kMesh))
	{
		printf("SkinFilterDeformGenerator: SOURCE GEOMETRY NOT MESH\n");
		return MObject::kNullObj;
	}
	MObject meshobj = inobjects[0];
	if( meshobj.isNull())
	{
		printf("SkinFilterDeformGenerator: meshobj.isNull()\n");
		return MObject::kNullObj;
	}
	return meshobj;
}
//! ��������� ���������� �������� �������� ���� ������� ����� �������������� ��������� ��������� 
void SkinFilterDeformGenerator::OnSetupNode(
	cls::INode& inode, 
	cls::IExportContext* context,
	MObject& generatornode			//!< generator node �.� 0
	)
{
	this->lastnode = NULL;

	MObject node = inode.getObject();
	if( node.hasFn(MFn::kMesh))
	{
		// �������� ���������
		MObject meshobj = getGeometrySource(node);
		if( meshobj.isNull())
		{
			MFnDependencyNode dn(node);
			displayString( "%s don have skinCluster deformer", dn.name().asChar());
			return;
		}
 		MFnDependencyNode dn(meshobj);
		printf( "source geometry is %s\n", dn.name().asChar());

		inode.setProxyObject(meshobj);
		this->lastnode = &inode;
		this->lastnodeobject = node;
	}
}

//! render ����� ���� = NULL
void SkinFilterDeformGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,				//!< render
	cls::enSystemCall call,				//!< ��� ������ ��������� ��������: I_POINTS, I_CURVES, I_MESH ...
	cls::IExportContext* context, 
	MObject& generatornode			//!< generator node �.� 0
	)
{
	if( this->lastnode != &inode)
	{
		displayString( "SkinFilterDeformGenerator: last node is missed");
		return;
	}
	MObject node = this->lastnodeobject;
//	MObject node = inode.getObject();
	MStatus stat;

	if( !render) 
		return;

	MDagPath path;
	if( !MDagPath::getAPathTo(node, path))
		return;

	if( node.hasFn(MFn::kMesh))
	{
		MItDependencyNodes it(MFn::kJoint);
		/*/
		std::vector<std::string> all_joints_name;
		while(it.isDone())
		{
			MObject obj = it.thisNode();
			MFnDagNode dgn(obj);
			MString name = dgn.fullPathName();
			all_joints_name.push_back(name.asChar());
			it.next();
		}
		std::sort(all_joints_name.begin(), all_joints_name.end());
		/*/
		// ����
		MObject skin = getSkinCluster(node);
		MFnSkinCluster skinCluster(skin, &stat);
		// �������� ���������
		MObject meshobj = getGeometrySource(node);
		if( skin.isNull() || meshobj.isNull())
			return;

		// �����
		MPlug bindPreMatrixArrayPlug = skinCluster.findPlug("bindPreMatrix", &stat);
		MDagPathArray infs;
		unsigned int nInfs = skinCluster.influenceObjects(infs, &stat);

		cls::PA<Math::Vec3f> P = render->GetParameter("P");
		// ������������� ������� �� ������ bind
		Math::Matrix4f preTransform;
		{
			MPlug bindGeomMatrixes = skinCluster.findPlug("geomMatrix", &stat);
			MObject dataObject;
			bindGeomMatrixes.getValue( dataObject, MDGContext::fsNormal);
			MFnMatrixData matDataFn( dataObject );
			MMatrix objPreTransform = matDataFn.matrix();
			copy( preTransform, objPreTransform);

			for(int i=0; i<P.size(); i++)
			{
				P[i] = preTransform*P[i];
			}
		}
		cls::PA<Math::Matrix4f> joints_pos(cls::PI_CONSTANT), joints_invbindpos(cls::PI_CONSTANT);
		std::vector<std::string> joints_name(nInfs);

		joints_pos.resize(nInfs);
		joints_invbindpos.resize(nInfs);
		int numPoints = P.size();
		for(unsigned i = 0; i<infs.length(); i++)
		{
			MDagPath jointpath = infs[i];
			MObject obj = infs[i].node();
			if( !obj.hasFn( MFn::kJoint))
			{
				printf("SkinFilterDeformGenerator: NODE %s isn't joint\n", jointpath.fullPathName().asChar());
				continue;
			}

			MFnTransform dagNode(infs[i].node());
			std::string jointName = dagNode.partialPathName().asChar();
			size_t reference = jointName.find(':');
			if(reference!=jointName.npos)
			{
				jointName = jointName.substr(reference+1);
			}
			Math::Matrix4f invBind, world;
			{
				// ������� �������
				MMatrix wm = jointpath.inclusiveMatrix();
				copy( world, wm);
			}

			{
				int index = skinCluster.indexForInfluenceObject(jointpath, &stat);

				// ������� �� ������ ��������
				MPlug bindPreMatrixPlug = bindPreMatrixArrayPlug.elementByLogicalIndex(index, &stat);
//				MPlug bindPreMatrixPlug = bindPreMatrixArrayPlug[i];
				MObject dataObject;
				bindPreMatrixPlug.getValue( dataObject);
				MFnMatrixData matDataFn ( dataObject );
				MMatrix invMat = matDataFn.matrix();
				copy( invBind, invMat);
			}

			// ���������
			joints_name[i] = jointName;
			joints_pos[i] = world;
			joints_invbindpos[i] = invBind;
		}
		// ���� � ������� ������
		cls::PA<float> W;
		W.resize(P.size()*nInfs);

		MItGeometry gIter(meshobj);
		for ( int v=0; !gIter.isDone(); gIter.next(), v++ ) 
		{
			MObject comp = gIter.component(&stat);
			MFnSingleIndexedComponent sic(comp);
			int cnt = sic.elementCount();
			int index = sic.element(0);

			MFloatArray wts;
			unsigned int infCount;
			stat = skinCluster.getWeights( path, comp, wts, infCount);
			if(infCount != nInfs) 
			{
				printf("SkinFilterDeformGenerator: infCount != nInfs\n");
				return;
			}
			bool bTest = false;
			if(bTest)
			{
				MFnMesh srcmesh(meshobj);
				MFnMesh dstmesh(node);

				MPoint pt;
				srcmesh.getPoint(v, pt);
				Math::Vec3f src;
				::copy( src, pt);
				Math::Vec3f dst(0, 0, 0);
				for(unsigned i=0; i<nInfs; i++)
				{
					if( wts[i]==0) continue;
					float w = wts[i];

					printf("%s %f\n", joints_name[i].c_str(), wts[i]);

					Math::Matrix4f ib = joints_invbindpos[i];
					Math::Matrix4f m = joints_pos[i];
					m = ib*m;

					Math::Vec3f t = src;
					t = m*t;
					t = w*t;
					dst = dst + t;
				}
				dstmesh.getPoint(v, pt);
				::copy( src, pt);
				float error = (src-dst).length();
				if(error>0.001)
				{
					printf("e");
				}
			}
			for(unsigned i=0; i<nInfs; i++)
			{
				W[v*nInfs + i] = wts[i];
			}
		}

		render->Attribute("SkinFilter::preTransform", preTransform);
		render->Attribute("SkinFilter::inv_bindpos", joints_invbindpos);
		render->GlobalAttribute("SkinFilter::pos", joints_pos);
		cls::PSA pjoints_name(joints_name);
		render->GlobalAttribute("SkinFilter::jointnames", pjoints_name);
//		cls::PSA palljoints_name(all_joints_name);
//		render->GlobalAttribute("SkinFilter::alljointnames", pjoints_name);

//		render->Parameter("SkinFilter::JointNameIndex", W);
		render->Parameter("SkinFilter::Weights", W);
		render->Call("Skin");

	}
}
