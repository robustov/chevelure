#pragma once

#include "ocellaris/OcellarisExport.h"
#include "ocellaris/IShaderGenerator.h"

//////////////////////////////////////////////////////////////////////
// ����������� ��������� ������ ����������� �� �����������
// ����������� �� ���� ������ IShaderNodeGenerator � �������� ������
// ���
class MayaShaderGenerator : public cls::IShaderGenerator
{
public:
	MayaShaderGenerator(void);
	~MayaShaderGenerator(void);

	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "MayaShaderGenerator";};

	virtual bool OnPrepareNode(
		cls::INode& node, 
		cls::IExportContext* context,
		MObject& generatornode
		);

	// ��� �������� � ����
	virtual void OnLoadToMaya(
		MObject& generatornode
		);

	//! 
	bool OnSurface(
		cls::INode& node, 
		cls::IRender* render,
		cls::IExportContext* context,
		MObject& generatornode
		);
	virtual bool OnDisplacement(
		cls::INode& node,			//!< ������ ��� �������� ����������� �������
		cls::IRender* render,			//!< render
		cls::IExportContext* context,
		MObject& generatornode
		);

protected:
	MObject findShader( MObject& setNode, const char* plugName );

	// ������������� ������ ���� ��� �� ������������
	bool BuildShader(
		MObject& surfaceShaderNode, 
		MObject& node, 
		cls::IRender* render,
		cls::IExportContext* context,
		cls::IShadingNodeGenerator::enShadingNodeType shtype
		);

};
