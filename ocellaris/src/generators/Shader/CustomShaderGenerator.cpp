#include "StdAfx.h"
#include "customshadergenerator.h"

CustomShaderGenerator customshadergenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl CustomShader()
	{
		return &customshadergenerator;
	}
}

CustomShaderGenerator::CustomShaderGenerator(void)
{
}

CustomShaderGenerator::~CustomShaderGenerator(void)
{
}

// ��� �������� � ����
void CustomShaderGenerator::OnLoadToMaya(
	MObject& generatornode
	)
{
	MObject attr;
	attr = ocExport_AddAttrType(generatornode, "shadertype", cls::PT_STRING, cls::PS("surface"));
	attr = ocExport_AddAttrType(generatornode, "shadername", cls::PT_STRING, cls::PS(""));
	attr = ocExport_AddAttrType(generatornode, "layername", cls::PT_STRING, cls::PS(""));
}

//! 
bool CustomShaderGenerator::OnSurface(
	cls::INode& node, 
	cls::IRender* render,
	cls::IExportContext* context,
	MObject& generatornode
	)
{
	cls::PS shadertype = ocExport_GetAttrValue(generatornode, "shadertype");
	cls::PS shadername = ocExport_GetAttrValue(generatornode, "shadername");
	cls::PS layername = ocExport_GetAttrValue(generatornode, "layername");

	if( shadertype.empty() ||shadername.empty())
		return false;
	if( !shadertype.data()[0] || !shadername.data()[0])
		return false;

	if(render)
	{
		if(layername.empty())
			render->Shader(shadertype.data(), shadername.data());
		else
			render->Shader(shadertype.data(), shadername.data(), layername.data());
	}
	return true;
}
