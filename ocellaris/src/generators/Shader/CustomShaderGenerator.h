#pragma once

#include "ocellaris/OcellarisExport.h"
#include "ocellaris/IShaderGenerator.h"

class CustomShaderGenerator : public cls::IShaderGenerator
{
public:
	CustomShaderGenerator(void);
	~CustomShaderGenerator(void);

	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "CustomShaderGenerator";};

	virtual bool OnPrepare(
		cls::INode& node, 
		cls::IExportContext* context
		){return true;}

	// ��� �������� � ����
	virtual void OnLoadToMaya(
		MObject& generatornode
		);

	//! 
	bool OnSurface(
		cls::INode& node, 
		cls::IRender* render,
		cls::IExportContext* context,
		MObject& generatornode
		);
	virtual bool OnDisplacement(
		cls::INode& node,			//!< ������ ��� �������� ����������� �������
		cls::IRender* render,			//!< render
		cls::IExportContext* context,
		MObject& generatornode
		){return false;};

};
