#include "StdAfx.h"
#include "MayaShaderGenerator.h"
#include "ocellaris/shaderAssemblerImpl.h"
#include "Util/misc_create_directory.h"
#include "shading/ShadingGraphAssembler.h"

MayaShaderGenerator mayashadergenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl MayaShader()
	{
		return &mayashadergenerator;
	}
}

MayaShaderGenerator::MayaShaderGenerator(void)
{
}

MayaShaderGenerator::~MayaShaderGenerator(void)
{
}

// ��� �������� � ����
void MayaShaderGenerator::OnLoadToMaya(
	MObject& generatornode
	)
{
//	MObject attr;
//	attr = ocExport_AddAttrType(generatornode, "shadertype", cls::PT_STRING, cls::PS("surface"));
//	attr = ocExport_AddAttrType(generatornode, "shadername", cls::PT_STRING, cls::PS(""));
//	attr = ocExport_AddAttrType(generatornode, "layername", cls::PT_STRING, cls::PS(""));
}

bool MayaShaderGenerator::OnPrepareNode(
	cls::INode& node, 
	cls::IExportContext* context,
	MObject& generatornode
	)
{
	MObject obj = node.getPath().node();
	MStatus stat;

	MObject shadingGroup = MObject::kNullObj;

	// ����� SG ��� �������
	if( obj.hasFn(MFn::kMesh))
	{
		// ��� Mesh ����� getConnectedSetsAndMembers
		MFnMesh mesh(obj);
		// 
		int instanceNum=0;
		MObjectArray sets, comps; 
		// set ��� SG
		// comp - ��� ������ ���������(���������)
		if( !mesh.getConnectedSetsAndMembers(instanceNum, sets, comps, true)) 
		{
			return false;
		}
		for ( unsigned i=0; i<sets.length(); i++ ) 
		{
			MObject set = sets[i];
			MObject comp = comps[i];

			MFnSet fnSet( set );
			if ( fnSet.restriction(&stat) == MFnSet::kRenderableOnly)
			{
				shadingGroup = set;
				break;
			}
		}
	}
	else
	{
		// ��� ��������� ����� getAssociatedSets
		MSelectionList objects;
		objects.add( obj );
		MObjectArray setArray;

		// Get all of the sets that this object belongs to
		//
		MGlobal::getAssociatedSets( objects, setArray );

		// Look for a set that is a "shading group"
		for ( int i=0; i<(int)setArray.length(); i++ )
		{
			MObject mobj = setArray[i];
			MFnSet fnSet( mobj );
			if ( fnSet.restriction(&stat) == MFnSet::kRenderableOnly)
			{
				shadingGroup = mobj;
				break;
			}
		}
		if( shadingGroup.isNull())
		{
			MStringArray setArray;
			MString cmd = "listSets -o ";
			cmd+=MFnDependencyNode(obj).name();
			MGlobal::executeCommand(cmd, setArray);
			for ( int i=0; i<(int)setArray.length(); i++ )
			{
				MObject mobj;
				nodeFromName( setArray[i], mobj);
				if( mobj.isNull()) continue;
				MFnSet fnSet( mobj );
				if ( fnSet.restriction(&stat) == MFnSet::kRenderableOnly)
				{
					shadingGroup = mobj;
					break;
				}
			}
		}
	}
	if( shadingGroup.isNull())
	{
		// �� ������� SG
//		printf(
//			"shadingGroup for %s not found", 
//			MFnDependencyNode(obj).name().asChar());
		return false;
	}


	MObject surfaceShaderNode = findShader(shadingGroup, "surfaceShader");
	MObject displaceShaderNode = findShader(shadingGroup, "displacementShader");
	MObject shadowShaderNode = findShader(shadingGroup, "miShadowShader");

	if( surfaceShaderNode.isNull() &&
		displaceShaderNode.isNull() &&
		shadowShaderNode.isNull())
	{
		return false;
	}

	
	cls::IRender* contextstream = context->objectContext(obj, this);
	if( contextstream)
	{
		if( !surfaceShaderNode.isNull())
		{
			contextstream->Parameter(
				"surfaceShader", 
				MFnDependencyNode( surfaceShaderNode).name().asChar());

			// ������������� ������ � ����� Shader
			BuildShader(
				surfaceShaderNode, 
				obj, 
				NULL, 
				context, 
				cls::IShadingNodeGenerator::SURFACESHADER
				);

		}
		if( !displaceShaderNode.isNull())
		{
			contextstream->Parameter(
				"displaceShader", 
				MFnDependencyNode( displaceShaderNode).name().asChar());

			// ������������� ������ � ����� Shader
			BuildShader(
				surfaceShaderNode, 
				obj, 
				NULL, 
				context, 
				cls::IShadingNodeGenerator::DISPLACE
				);
		}
		if( !shadowShaderNode.isNull())
		{
			contextstream->Parameter(
				"shadowShader", 
				MFnDependencyNode( shadowShaderNode).name().asChar());

			// ������������� ������ � ����� Shader
			BuildShader(
				shadowShaderNode, 
				obj, 
				NULL, 
				context, 
				cls::IShadingNodeGenerator::SURFACESHADER
				);
		}
	}
	return true;
}

//! 
bool MayaShaderGenerator::OnSurface(
	cls::INode& node, 
	cls::IRender* render,
	cls::IExportContext* context,
	MObject& generatornode
	)
{
	MObject obj = node.getPath().node();

	cls::IRender* contextstream = context->objectContext(obj, this);
	if( !contextstream)
		return false;

	std::string surfaceShader;
	contextstream->GetParameter("surfaceShader", surfaceShader);

	if( !surfaceShader.empty())
	{
		MObject surfaceShaderNode;
		nodeFromName(surfaceShader.c_str(), surfaceShaderNode);
		if( !surfaceShaderNode.isNull())
		{
			render->IfAttribute("@pass", cls::PS("final"));
			// ������������� ������ � ����� Shader
			BuildShader(
				surfaceShaderNode, 
				obj, 
				render, 
				context, 
				cls::IShadingNodeGenerator::SURFACESHADER
				);
			render->EndIfAttribute();
		}
	}

	std::string shadowShader;
	contextstream->GetParameter("shadowShader", shadowShader);
	if( !shadowShader.empty())
	{
		MObject shadowShaderNode;
		nodeFromName(shadowShader.c_str(), shadowShaderNode);
		if( !shadowShaderNode.isNull())
		{
			render->IfAttribute("@pass", cls::PS("shadow"));
			// ������������� ������ � ����� Shader
			BuildShader(
				shadowShaderNode, 
				obj, 
				render, 
				context, 
				cls::IShadingNodeGenerator::SURFACESHADER
				);
			render->EndIfAttribute();
		}
	}
	return true;
}
bool MayaShaderGenerator::OnDisplacement(
	cls::INode& node,			//!< ������ ��� �������� ����������� �������
	cls::IRender* render,			//!< render
	cls::IExportContext* context,
	MObject& generatornode
	)
{
	MObject obj = node.getPath().node();

	cls::IRender* contextstream = context->objectContext(obj, this);
	if( !contextstream)
		return false;

	std::string surfaceShader;
	if( !contextstream->GetParameter("displaceShader", surfaceShader))
		return false;
	if( surfaceShader.empty())
		return false;

	MObject surfaceShaderNode;
	nodeFromName(surfaceShader.c_str(), surfaceShaderNode);
	if( surfaceShaderNode.isNull())
		return false;

	// ������������� ������ � ����� Shader
	BuildShader(
		surfaceShaderNode, 
		obj, 
		render, 
		context, 
		cls::IShadingNodeGenerator::DISPLACE
		);

	return true;
}

// Find the shading node for the given shading group set node.
MObject MayaShaderGenerator::findShader( MObject& setNode, const char* plugName )
{
	MFnDependencyNode fnNode(setNode);
	MPlug shaderPlug = fnNode.findPlug(plugName);
			
	if (!shaderPlug.isNull()) 
	{
		MPlugArray connectedPlugs;
		bool asSrc = false;
		bool asDst = true;
		shaderPlug.connectedTo( connectedPlugs, asDst, asSrc );

		if (connectedPlugs.length() == 1)
			return connectedPlugs[0].node();
	}			
	return MObject::kNullObj;
}

// ������������� ������ ���� ��� �� ������������
bool MayaShaderGenerator::BuildShader(
	MObject& surfaceShaderNode, 
	MObject& node, 
	cls::IRender* render,
	cls::IExportContext* context,
	cls::IShadingNodeGenerator::enShadingNodeType shtype
	)
{
	// ����� ��� �������������?
	cls::IRender* contextstream = context->objectContext(surfaceShaderNode, this);
	if(contextstream)
	{
		std::string shaderfile;
		if( contextstream->GetParameter("shaderfile", shaderfile))
		{
			if( *shaderfile.data()==0)
				return false;
			if( render)
			{
				// �������� ��������
				std::vector< std::string > externvariables;
				contextstream->GetParameter("externvariables", externvariables);
				for(int i=0; i<(int)externvariables.size()/2; i++)
				{
					std::string name = externvariables[i*2+0];
					std::string attr = externvariables[i*2+1];
					MPlug plug;
					plugFromName(attr.c_str(), plug);
					cls::Param p = ocExport_GetAttrValue(plug);
					if( p.empty())
						context->error( "MayaShaderGenerator::BuildShader cant read attribute %s", attr.c_str());
					else
						render->Parameter(name.c_str(), p);
				}

				if( shtype==cls::IShadingNodeGenerator::SURFACESHADER)
					render->Shader("surface", shaderfile.data());
				if( shtype==cls::IShadingNodeGenerator::DISPLACE)
					render->Shader("displacement", shaderfile.data());
			}
			return true;
		}
	}


	// ��������� ������ IShaderNodeGenerator

	// ������ IShadingNodeGenerator ��� �������
	// ��������� BuildShader
	// IShaderNodeGenerator::BuildShader ������ ��������� ����� ����������� ���������, 
	// ������� ���� � ������� ��������� ������ ������ - ��������� � ���� IShaderNodeGenerator::BuildShader 
	// ��� ���� ������ ������� - ������ externAttriute � ������� � ������ ����� ��������� ��������� � ������
	// (� ��������� � ���������)
//	IShadingNodeGenerator

	std::string name = MFnDependencyNode(surfaceShaderNode).name().asChar();
	Util::correctFileName( name, '_', true);
	cls::shaderAssemblerImpl as;

	std::map<std::string, cls::Param > externParams;
	std::map<std::string, std::string > externvarplug;
	std::string text = as.BuildGraph(shtype, surfaceShaderNode, name.c_str(), context, externParams, externvarplug);
	if( text.empty())
	{
		if(contextstream)
			contextstream->Parameter("shaderfile", "");
		return false;
	}

	// ������ ���������� ������� ���� ��������
	std::vector< std::string > externvariables;
	{
		externvariables.reserve(externvarplug.size()*2);
		std::map<std::string, std::string>::iterator it = externvarplug.begin();
		for(;it!=externvarplug.end(); it++)
		{
			externvariables.push_back(it->first);
			externvariables.push_back(it->second);
		}
	}

	std::string outfilename = "uncompiled";
	{
		const char* sd = context->getEnvironment("SHADERDIR");
		if( !sd || !*sd)
		{
			MString _workspacedir;
			MGlobal::executeCommand("workspace -q -rd", _workspacedir);
			std::string workspacedir = _workspacedir.asChar();
			outfilename = workspacedir + "ocs/";
		}
		else
			outfilename = sd;

		Util::pushBackSlash(outfilename);
		outfilename += name;
		outfilename += ".sl";
		Util::create_directory_for_file(outfilename.c_str());

//		displayString(
//			"computeShaderFile for %s", 
//			MFnDependencyNode(surfaceShaderNode).name().asChar());
		{
			FILE* file = fopen( outfilename.c_str(), "wt");
			if(!file)
			{
				printf("cant open file %s", outfilename.c_str());
				if(contextstream)
					contextstream->Parameter("shaderfile", "unsaved");
				return false;
			}
			fputs(text.c_str(), file);
			fclose(file);
		}
		// �������� slo
		{
			std::string slo = outfilename + "o";
			remove(slo.c_str());
		}

		std::string shadercompiler = context->getEnvironment("SHADERCOMPILER");
		if( ShadingGraphAssembler::Compile(context, outfilename.c_str(), NULL, shadercompiler.c_str())=="")
		{
			if(contextstream)
				contextstream->Parameter("shaderfile", "");
			return false;
		};
		Util::changeSlashToSlash(outfilename);

		// �������� ".slo" � �����
		size_t x = outfilename.rfind('.');
		if(x!=std::string::npos)
			outfilename = outfilename.erase(x);
//		data.outputValue(plug).set( MString(outfilename.c_str()));
	}
	if( render)
	{
		if( shtype==cls::IShadingNodeGenerator::SURFACESHADER)
			render->Shader("surface", outfilename.c_str());
		if( shtype==cls::IShadingNodeGenerator::DISPLACE)
			render->Shader("displacement", outfilename.c_str());
	}
	if( contextstream)
	{
		contextstream->Parameter("shaderfile", outfilename.c_str());
		contextstream->Parameter("externvariables", externvariables);
	}
	return true;
}
