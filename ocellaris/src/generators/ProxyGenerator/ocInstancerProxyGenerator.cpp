#include "stdafx.h"
#include "ocInstancerProxyGenerator.h"
#include "ocInstance.h"
#include "ocellaris/MInstancePxData.h"
#include "../generators/AttributeGenerator/ocInstancerAttributeGenerator.h"

ocInstancerAttributeGenerator ocinstancerattributegenerator;

//! OnProcess
MObject ocInstancerProxyGenerator::OnProcess(
	cls::INode* node, 
	cls::IExportContext* context,
	MObject& generatornode
	)
{
	MFnDependencyNode dn(node->getObject());
	MPlug plug(node->getObject(), ocInstance::i_drawCache);
	
	MPlugArray array;
	plug.connectedTo( array, true, false);
	if(array.length()==0)
		return node->getObject();

	plug = array[0];

	/*/
	ocInstance* ocinstance = (ocInstance*)dn.userNode();
	if( !ocinstance) return false;
	ocInstance2Geometry* geom = ocinstance->getGeometry();
	if( !geom) return false;
	cls::MInstancePxData* cache = geom->cache;
	cache->drawcache.SetAttrToRender(node, render);
	/*/

	node->AddGenerator(&ocinstancerattributegenerator);
	dn.setObject(plug.node());
	return plug.node();
}
