#pragma once
#include "ocellaris/IProxyGenerator.h"

//! @ingroup implement_group
//! \brief ������ ��������� ��� ����� ocInstancer
//! ���������� ������ ������� ���������� ������ ��� ���� ocInstancer
struct ocInstancerProxyGenerator : public cls::IProxyGenerator
{
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "ocInstancerProxyGenerator";};

	//! OnProcess
	virtual MObject OnProcess(
		cls::INode* node,
		cls::IExportContext* context,
		MObject& generatornode
		);
};
