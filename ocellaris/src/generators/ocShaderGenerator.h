#pragma once

#include "ocellaris/OcellarisExport.h"
#include "ocellaris/IShaderGenerator.h"

#pragma warning ( disable:4275)
#pragma warning ( disable:4251)

//! @ingroup implement_group
//! \brief ����������� ��������� ������� 
//! ������� �������������� ������ ���� ocShaderParameter ����������� � �������� �������
//! ������ ������� ocellaris ����������� ����� ����������� ��������� shading group � �������
//!
struct ocShaderGenerator : public cls::IShaderGenerator
{
public:
	ocShaderGenerator();

	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "ocShaderGenerator";};

	//! ����������� ���� � �������� 
	virtual bool OnPrepareNode(
		cls::INode& node, 
		cls::IExportContext* context,
		MObject& generatornode
		);

	//! 
	bool OnSurface(
		cls::INode& node, 
		cls::IRender* render,
		cls::IExportContext* context,
		MObject& generatornode
		);
	virtual bool OnDisplacement(
		cls::INode& node,			//!< ������ ��� �������� ����������� �������
		cls::IRender* render,			//!< render
		cls::IExportContext* context,
		MObject& generatornode
		);

protected:
	MObject findShader( MObject& setNode, const char* plugName );
};
