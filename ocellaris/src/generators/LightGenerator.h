#pragma once

#include "ocellaris/ILightGenerator.h"

//! @ingroup implement_group
//! \brief ����������� ��������� ��� ��������� �����
//! ���� ������� �� �����
struct LightGenerator : public cls::ILightGenerator
{
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "LightGenerator";};

	virtual bool OnLight(
		cls::INode& node,				//!< ������ 
		cls::IRender* render,			//!< render
		Math::Box3f& box,				//!< box 
		cls::IExportContext* context,	//!< context
		MObject& generatornode			//!< generator node �.� 0
		);
};
