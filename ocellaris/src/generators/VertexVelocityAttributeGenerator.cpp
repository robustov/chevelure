#include "stdafx.h"
#include "VertexVelocityAttributeGenerator.h"
#include <maya/MFnMesh.h>
#include <maya/MFloatArray.h>
#include <maya/MAnimControl.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatVector.h>
#include <maya/MPlug.h>
#include "mathNmaya/mathNmaya.h"

VertexVelocityAttributeGenerator vertexvelocityattributegenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl VertexVelocityDeform()
	{
		return &vertexvelocityattributegenerator;
	}
}


//! render ����� ���� = NULL
void VertexVelocityAttributeGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,				//!< render
	cls::enSystemCall call,				//!< ��� ������ ��������� ��������: I_POINTS, I_CURVES, I_MESH ...
	cls::IExportContext* context, 
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();
	MStatus stat;

	if( !render) 
		return;

	if( node.hasFn(MFn::kMesh))
	{
		MFnDependencyNode dn(node);
		MPlug plug = dn.findPlug("outMesh", &stat);

		MTime t0 = MAnimControl::currentTime();
		MTime t1 = t0+MTime(0.5, MTime::uiUnit());

		MDGContext context(t1);
		MObject obj;
		stat = plug.getValue( obj, context);

		MFnMesh mesh(obj, &stat);
		MFloatPointArray verts1;
		mesh.getPoints(verts1);

		/*/
		MTime t0 = MAnimControl::currentTime();
		MTime t1 = t0+MTime(0.5, MTime::uiUnit());

		MAnimControl::setCurrentTime(t1);
		MFnMesh mesh(node, &stat);
		MFloatPointArray verts1;
		mesh.getPoints(verts1);
		MAnimControl::setCurrentTime(t0);
		/*/

		cls::PA<Math::Vec3f> P0 = render->GetParameter("P");
		if( P0.size()!=verts1.length())
		{
			printf("VertexVelocityAttributeGenerator: P0.size(%d)!=verts1.lenght(%d)\n", P0.size(), verts1.length());
			return;
		}
		cls::PA<Math::Vec3f> vel(cls::PI_VERTEX, P0.size());
		for(int i=0; i<(int)P0.size(); i++)
		{
			Math::Vec3f p1;
			::copy(p1, verts1[i]);
			Math::Vec3f p = p1 - P0[i];
			vel[i] = p*2;
		}
		render->Parameter("velocity", vel);
		render->Call("ApplyVelocity");
	}
}
