#include "stdafx.h"
#include "TransformGenerator.h"
#include <maya/MFnMesh.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MPlug.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MIntArray.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnTransform.h>
#include <maya/MTransformationMatrix.h>
#include <string>
#include <mathNmaya/mathNmaya.h>
#include "math/Matrix.h"

TransformGenerator::TransformGenerator()
{
}

//! render ����� ���� = NULL
void TransformGenerator::OnTransformStart(
	cls::INode& node,					//!< ������
	cls::IRender* render,				//!< render
	const Math::Matrix4f& curtransform,	//!< ������� ���������
	Math::Matrix4f& newtransform,		//!< ����� ���������
	cls::IExportContext* context,		
	MObject& generatornode				//!< generator node �.� 0
	)
{
	MObject obj = node.getObject();
	if( !obj.hasFn(MFn::kTransform))
	{
		if( render)
			render->PushAttributes();
		newtransform = curtransform;
		return;
	}

	MFnDependencyNode dn(obj);
	bool inheritsTransform = true;
	MPlug plug = dn.findPlug("inheritsTransform");
	if( !plug.isNull())
		plug.getValue(inheritsTransform);

	MFnTransform transform(obj);
	MTransformationMatrix tm = transform.transformation();
	Math::Matrix4f m;
	copy( m, tm.asMatrix());
	if( inheritsTransform)
		newtransform = m*curtransform;
	else
		newtransform = m;

	if( render)
		render->PushTransform();
}
//! ��������� ����� { AppendTransform ��� SetTransform }
void TransformGenerator::OnTransform(
	cls::INode& node,					//!< ������
	cls::IRender* render,				//!< render
	cls::IExportContext* context,
	MObject& generatornode				//!< generator node �.� 0
	)
{
	MObject obj = node.getObject();
	if( !obj.hasFn(MFn::kTransform))
		return;

	MFnDependencyNode dn(obj);
	bool inheritsTransform = true;
	MPlug plug = dn.findPlug("inheritsTransform");
	if( !plug.isNull())
		plug.getValue(inheritsTransform);

	MFnTransform transform(obj);
	MTransformationMatrix tm = transform.transformation();
	Math::Matrix4f m;
	copy( m, tm.asMatrix());

	if( render)
	{
//render->PushAttributes();
		render->PushAttributes();
		if( inheritsTransform)
			render->AppendTransform(m);
		else
			render->SetTransform(m);
		render->PopAttributes();
	}
}

void TransformGenerator::OnTransformEnd(
	cls::INode& node,					//!< ������
	cls::IRender* render,				//!< render
	cls::IExportContext* context,
	MObject& generatornode				//!< generator node �.� 0
	)
{
	MObject obj = node.getObject();
	if( !obj.hasFn(MFn::kTransform))
	{
		if( render)
			render->PopAttributes();
		return;
	}

	if( render)
	{
		render->PopTransform();
	}
}

