#pragma once

#include "ocellaris/IAttributeGenerator.h"

//! @ingroup implement_group
//! \brief ��������� ��� ���������� ���������� ������� � ��������� ������
//! ����� ����� ���������� ������� � ���������� ������� ������ ��������������� ����������� 
//! �������� ���� ocParticleVectorAttribute � ocParticleFloatAttribute
//! 
struct ParticleAttributeGenerator : public cls::IAttributeGenerator
{
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "ParticleAttributeGenerator";};

	/// OnAttribute
	virtual bool OnAttribute(
		cls::INode& node,			//!< ������ ��� �������� ����������� �������
		cls::IRender* render,	//!< render
		cls::IExportContext* context, 
//		const char* attrName,	//!< ��� ��������
//		cls::enParamType type, 
		MObject& generatornode
		);
};
