#pragma once

#include "ocellaris/OcellarisExport.h"
#include "ocellaris/IShaderGenerator.h"

#pragma warning ( disable:4275)
#pragma warning ( disable:4251)

//! @ingroup implement_group
//! \brief ����������� ��������� ������� 
//! ������� �������������� ������ ���� ocShaderParameter ����������� � �������� �������
//! ����� ������ ����� �������� �� mtor�
//! ������� ��������� �������� ����������� � ������ ocShaderParameter � ����� ���� ��������/������������ � ����
//!
struct OCELLARISEXPORT_API SlimShaderGenerator : public cls::IShaderGenerator
{
public:
	bool bLazy;
	static bool bDontUseMtor;
//	static bool bClearShaderWhenStart;
	static bool bPutExternShaderParameter;
//	struct ShaderParameter
//	{
//		std::string type;
//	};
	typedef std::map<std::string, cls::Param> paramlist_t;
	struct ShaderDeclare
	{
		std::string shadername;
		paramlist_t paramlist;
	};
	typedef std::map<std::string, ShaderDeclare> shaderlist_t;
	shaderlist_t shaderlist;

	std::string workspacedir;
public:
	std::string copyShader(
		const char* shader, 
		cls::IExportContext* context 
		);
	//! ������ ��� �������, ��������� ������ ����������, ������ ���������
	std::string readShaderNameNparameters(
		cls::IRender* render, 
		MObject obj, 
		const char* attrname, 
		paramlist_t& params, 
		MObject externnode, 
		const char* externnodeattr,
		const char* externnodeListattr, 
		cls::IExportContext* context 
		);
	//! ������ ��� ������� �� mtor
	std::string mtorShaderNames(
		MObject obj, 
		const char* attrname, 
		paramlist_t& paramlist, 
		cls::IExportContext* context, 
		bool displayEnabled = false, 
		bool paramIterations = false
		);
	//! ������ ���������� ������� ��������������� �� slo �����
	void sloReadShaderParams(
		const char* shaderName, 
		paramlist_t& params, 
		cls::IExportContext* context);

	//! ��������! ������ ���������� ������� �� mtor
	void RecursiveFindParams(
		bool displayEnabled,
		int level, 
		const char* id, 
		paramlist_t& params);
	//! displaceBound
	bool mtorGetDisplaceBound(
		MObject obj, 
		const char* attrname, 
		float& displaceBound);

public:
	SlimShaderGenerator();

	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "SlimShaderGenerator";};

	bool OnPrepare(
		cls::IExportContext* context
		);

	bool OnPrepareNode(
		cls::INode& node, 
		cls::IExportContext* context,
		MObject& generatornode
		);

	//! 
	bool OnSurface(
		cls::INode& node, 
		cls::IRender* render,
		cls::IExportContext* context,
		MObject& generatornode
		);
	bool OnDisplacement(
		cls::INode& node, 
		cls::IRender* render,
		cls::IExportContext* context,
		MObject& generatornode
		);
};
