#include "StdAfx.h"
#include "ocellaris/IDeformGenerator.h"
#include "ocellaris/OcellarisExport.h"

class ParamFromAttributeGenerator : public cls::IDeformGenerator
{
public:
	ParamFromAttributeGenerator(void);
	~ParamFromAttributeGenerator(void);

	// ��� �������� � ����
	virtual void OnLoadToMaya(
		MObject& generatornode
		);

	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "ParamFromAttributeGenerator";};

	// � ����� ������ ��������
	virtual int GetDeformCallType(
		MObject& generatornode
		);

	//
	virtual void OnShader(
		cls::INode& inode,				//!< ������ 
		cls::IRender* render,			//!< render
		const char* shadertype,			//!< 
		cls::IExportContext* context, 
		MObject& generatornode			//!< generator node �.� 0
		);
/*/
	/// OnAttribute
	virtual bool OnAttribute(
		cls::INode& node,				//!< ������ ��� �������� ����������� �������
		cls::IRender* render,			//!< render
		cls::IExportContext* context, 
		MObject& generatornode
		);
/*/
};

ParamFromAttributeGenerator paramfromattributegenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl ParamFromAttribute()
	{
		return &paramfromattributegenerator;
	}
}

ParamFromAttributeGenerator::ParamFromAttributeGenerator(void)
{
}

ParamFromAttributeGenerator::~ParamFromAttributeGenerator(void)
{
}

// � ����� ������ ��������
int ParamFromAttributeGenerator::GetDeformCallType(
	MObject& generatornode
	)
{
	cls::P<int> type = ocExport_GetAttrValue(generatornode, "calltypeAttribute");
	if(!type.empty())
	{
		cls::IAttributeGenerator::enCallType ct = (cls::IAttributeGenerator::enCallType)type.data();
		return ct;
	}
	return CT_BEFORE_TRANSFORM;
}

// ��� �������� � ����
void ParamFromAttributeGenerator::OnLoadToMaya(
	MObject& generatornode
	)
{
	MObject attr;
	attr = ocExport_AddAttrType(generatornode, "FromAttribute", cls::PT_STRING, cls::PS("inputAttribute"));
	attr = ocExport_AddAttrType(generatornode, "ToParameter",   cls::PT_STRING, cls::PS("outputParameter"));
	attr = ocExport_AddAttrType(generatornode, "MakeGlobal",   cls::PT_BOOL, cls::P<bool>(false));
}

void ParamFromAttributeGenerator::OnShader(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	const char* shadertype,			//!< 
	cls::IExportContext* context, 
	MObject& generatornode			//!< generator node �.� 0
	)
{
//	MDagPath path = inode.getPath();
//	MString name = path.partialPathName();

	cls::PS from = ocExport_GetAttrValue(generatornode, "FromAttribute");
	cls::PS to   = ocExport_GetAttrValue(generatornode, "ToParameter");
	cls::P<bool> MakeGlobal = ocExport_GetAttrValue(generatornode, "MakeGlobal");

	if( render && !from.emptystring() && !to.emptystring())
	{
		cls::Param p = render->GetParameter(to.data());
		if( !p.empty())
		{
			if( !MakeGlobal.empty() && MakeGlobal.data())
				render->GlobalAttribute(from.data(), p);
		}
		render->ParameterFromAttribute(to.data(), from.data());
	}
}

/*/
//! 
bool ParamFromAttributeGenerator::OnAttribute(
	cls::INode& node,				//!< ������ ��� �������� ����������� �������
	cls::IRender* render,			//!< render
	cls::IExportContext* context, 
	MObject& generatornode
	)
{
	MDagPath path = node.getPath();
	MString name = path.partialPathName();

	cls::PS from = ocExport_GetAttrValue(generatornode, "FromAttribute");
	cls::PS to   = ocExport_GetAttrValue(generatornode, "ToParameter");

	if( render && !from.emptystring() && !to.emptystring())
	{
//		render->GlobalAttribute(from.data(), );
		render->ParameterFromAttribute(to.data(), from.data());
	}
	return true;
}
/*/
