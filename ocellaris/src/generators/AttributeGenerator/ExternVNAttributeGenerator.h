#pragma once

#include "ocellaris/IAttributeGenerator.h"

class ExternVNAttributeGenerator : public cls::IAttributeGenerator
{
public:
	ExternVNAttributeGenerator(void);
	~ExternVNAttributeGenerator(void);

	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "ExternVNAttributeGenerator";};

	// � ����� ������ ��������
	int GetCallType(
		MObject& generatornode
		){return CT_BEFORE_GEOMETRY;}

	// ��� �������� � ����
	void OnLoadToMaya(
		MObject& generatornode);

	/// OnAttribute
	virtual bool OnAttribute(
		cls::INode& node,				//!< ������ ��� �������� ����������� �������
		cls::IRender* render,			//!< render
		cls::IExportContext* context, 
		MObject& generatornode
		);
};
