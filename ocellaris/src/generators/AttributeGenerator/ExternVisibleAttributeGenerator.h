#pragma once

#include "ocellaris/IAttributeGenerator.h"

class ExternVisibleAttributeGenerator : public cls::IAttributeGenerator
{
public:
	ExternVisibleAttributeGenerator(void);
	~ExternVisibleAttributeGenerator(void);

	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "ExternVisibleAttributeGenerator";};

	// � ����� ������ ��������
	int GetCallType(
		MObject& generatornode
		){return CT_BEFORE_TRANSFORM;}

	/// OnAttribute
	virtual bool OnAttribute(
		cls::INode& node,				//!< ������ ��� �������� ����������� �������
		cls::IRender* render,			//!< render
		cls::IExportContext* context, 
//		const char* attrName,	//!< ��� �������� (����������� �� ������)
//		cls::enParamType type,	//!< ��� �������� (����������� �� ������)
		MObject& generatornode
		);
};
