#pragma once
#include "ocellaris/IAttributeGenerator.h"

//! @ingroup implement_group
//! \brief ����� ��� ��� �� �����, �������� ����
//! 
struct ocInstancerAttributeGenerator : public cls::IAttributeGenerator
{
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "ocInstancerAttributeGenerator";};
	// � ����� ������ ��������
	virtual int GetCallType(
		MObject& generatornode
		){return CT_BEFORE_GEOMETRY;}

		/// OnAttribute
	virtual bool OnAttribute(
		cls::INode& node,				//!< ������ ��� �������� ����������� �������
		cls::IRender* render,		//!< render
		cls::IExportContext* context, 
//		const char* attrName,		//!< ��� �������� (����������� �� ������)
//		cls::enParamType type,		//!< ��� �������� (����������� �� ������)
		MObject& generatornode
		);
};
