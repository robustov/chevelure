#include "StdAfx.h"
#include ".\customattributegenerator.h"
#include "ocellaris\renderStringImpl.h"

CustomAttributeGenerator customattributegenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl CustomAttribute()
	{
		return &customattributegenerator;
	}
}

CustomAttributeGenerator::CustomAttributeGenerator(void)
{
}

CustomAttributeGenerator::~CustomAttributeGenerator(void)
{
}

// ��� �������� � ����
void CustomAttributeGenerator::OnLoadToMaya(
	MObject& generatornode
	)
{
	MObject attr;
	attr = ocExport_AddAttrType(generatornode, "Parameter", cls::PT_BOOL, cls::P<bool>(false));
	attr = ocExport_AddAttrType(generatornode, "type", cls::PT_STRING, cls::PS("string"));
	attr = ocExport_AddAttrType(generatornode, "name", cls::PT_STRING, cls::PS(""));
	attr = ocExport_AddAttrType(generatornode, "value", cls::PT_STRING, cls::PS(""));
	attr = ocExport_AddAttrType(generatornode, "custom", cls::PT_STRING, cls::PS(""));
}
// � ����� ������ ��������
int CustomAttributeGenerator::GetCallType(
	MObject& generatornode
	)
{
	cls::P<int> type = ocExport_GetAttrValue(generatornode, "calltypeAttribute");
	if(!type.empty())
	{
		cls::IAttributeGenerator::enCallType ct = (cls::IAttributeGenerator::enCallType)type.data();
		return ct;
	}
	return CT_BEFORE_TRANSFORM;
}

//! 
bool CustomAttributeGenerator::OnAttribute(
	cls::INode& node,				//!< ������ ��� �������� ����������� �������
	cls::IRender* render,			//!< render
	cls::IExportContext* context, 
//	const char* attrName,	//!< ��� �������� (����������� �� ������)
//	cls::enParamType _type,	//!< ��� �������� (����������� �� ������)
	MObject& generatornode
	)
{
	cls::P<bool> bParam = ocExport_GetAttrValue(generatornode, "Parameter");
	cls::PS type = ocExport_GetAttrValue(generatornode, "type");
	cls::PS name = ocExport_GetAttrValue(generatornode, "name");
	cls::PS value = ocExport_GetAttrValue(generatornode, "value");

	cls::PS custom = ocExport_GetAttrValue(generatornode, "custom");
	if( !custom.empty() && custom.data()[0])
	{
		cls::renderStringImpl fileimpl;
		std::string customdata = custom.data();
		customdata += "\n";
		if( fileimpl.openForRead(customdata.c_str()))
		{
			if(render)
			{
				if( !fileimpl.Render(render))
					displayString("failed parse: %s", custom.data());
			}
		}
		else
		{
			displayString("failed parse: %s", custom.data());
		}
	}

	if( type.empty() ||name.empty() || value.empty() ||
		!type.data()[0] || !name.data()[0] || !value.data()[0])
	{
		return false;
	}

	std::string cmd="";
	if( bParam.empty() || !bParam.data())
		cmd += "Attribute";
	else
		cmd += "Parameter";
	cmd += " uniform ";
	cmd += type.data();
	cmd += " ";
	cmd += name.data();
	cmd += " = {";
	cmd += value.data();
	cmd += "};\n";

	if( render)
	{
		cls::renderStringImpl fileimpl;
		if( fileimpl.openForRead(cmd.c_str()))
		{
			fileimpl.Render(render);
		}
	}
	return true;
}
