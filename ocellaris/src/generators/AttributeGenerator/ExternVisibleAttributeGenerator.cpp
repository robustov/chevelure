#include "StdAfx.h"
#include "ExternVisibleAttributeGenerator.h"

ExternVisibleAttributeGenerator externvisibleattributegenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl ExternVisibleAttribute()
	{
		return &externvisibleattributegenerator;
	}
}

ExternVisibleAttributeGenerator::ExternVisibleAttributeGenerator(void)
{
}

ExternVisibleAttributeGenerator::~ExternVisibleAttributeGenerator(void)
{
}

//! 
bool ExternVisibleAttributeGenerator::OnAttribute(
	cls::INode& node,				//!< ������ ��� �������� ����������� �������
	cls::IRender* render,			//!< render
	cls::IExportContext* context, 
//	const char* attrName,	//!< ��� �������� (����������� �� ������)
//	cls::enParamType _type,	//!< ��� �������� (����������� �� ������)
	MObject& generatornode
	)
{
	MDagPath path = node.getPath();
	MString name = path.partialPathName();

	if( render)
	{
		std::string globalattr = name.asChar();
		size_t n = globalattr.find(':');
		if( n!=globalattr.npos) globalattr = globalattr.substr(n+1);
		globalattr = "ExternVisible::"+globalattr;

		render->GlobalAttribute(globalattr.c_str(), cls::P<bool>(true));
		render->AttributeFromAttribute("@visible", globalattr.c_str());
	}
	return true;
}
