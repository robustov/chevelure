#include "stdafx.h"
#include "ocInstancerAttributeGenerator.h"
#include "ocInstance.h"
#include "ocellaris/MInstancePxData.h"
#include <maya/MFnDependencyNode.h>
#include <maya/MFnParticleSystem.h> 
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MPlug.h>
#include <maya/MVector.h>
#include <maya/MPxLocatorNode.h>
#include <maya/MPxLocatorNode.h>
#include <maya/MFnPluginData.h>
#include "mathNmaya/mathNmaya.h"
#include "util/misc_create_directory.h"

// �� ������������ ��. ocInstancerProxyGenerator
bool ocInstancerAttributeGenerator::OnAttribute(
	cls::INode& node,				//!< ������ ��� �������� ����������� �������
	cls::IRender* render,		//!< render
	cls::IExportContext* context, 
//	const char* attrName,		//!< ��� �������� (����������� �� ������)
//	cls::enParamType type,		//!< ��� �������� (����������� �� ������)
	MObject& generatornode
	)
{
	MObject srcnode = node.getPath().node();
	MFnDependencyNode dn(srcnode);
	if( dn.typeId() != ocInstance::id)
		return false;
	ocInstance* ocinstance = (ocInstance*)dn.userNode();
	if( !ocinstance) return false;
	ocInstance2Geometry* geom = ocinstance->getGeometry();
	if( !geom) return false;

	cls::MInstancePxData* cache = geom->cache;
	if( !cache) 
		return false;

	if(render)
	{
		cache->drawcache.SetAttrToRender(srcnode, render);
	}
	return true;
}
