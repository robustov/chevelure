#pragma once

#include "ocellaris/IAttributeGenerator.h"
#include "ocellaris/OcellarisExport.h"

class CustomAttributeGenerator : public cls::IAttributeGenerator
{
public:
	CustomAttributeGenerator(void);
	~CustomAttributeGenerator(void);

	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "CustomAttributeGenerator";};

	// ��� �������� � ����
	virtual void OnLoadToMaya(
		MObject& generatornode
		);

	// � ����� ������ ��������
	int GetCallType(
		MObject& generatornode
		);

	/// OnAttribute
	virtual bool OnAttribute(
		cls::INode& node,				//!< ������ ��� �������� ����������� �������
		cls::IRender* render,			//!< render
		cls::IExportContext* context, 
//		const char* attrName,	//!< ��� �������� (����������� �� ������)
//		cls::enParamType type,	//!< ��� �������� (����������� �� ������)
		MObject& generatornode
		);
};
