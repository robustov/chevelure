#include "StdAfx.h"
#include "ExternVNAttributeGenerator.h"
#include "ocellaris/nodeSimple.h"
#include "ocellaris/renderCacheImpl.h"
#include "ocellaris/renderProxy.h"
#include "ocellaris/OcellarisExport.h"

ExternVNAttributeGenerator externvnattributegenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl ExternVNAttribute()
	{
		return &externvnattributegenerator;
	}
}

ExternVNAttributeGenerator::ExternVNAttributeGenerator(void)
{
}

ExternVNAttributeGenerator::~ExternVNAttributeGenerator(void)
{
}

// ��� �������� � ����
void ExternVNAttributeGenerator::OnLoadToMaya(
	MObject& generatornode
	)
{
	MObject attr;
	attr = ocExport_AddAttrType(generatornode, "inputGeometry", cls::PT_STRING, cls::PS(""));
	attr = ocExport_AddAttrType(generatornode, "nodeName", cls::PT_STRING, cls::PS(""));
}

struct renderProxySkipRenderCall : public cls::renderProxy
{
	renderProxySkipRenderCall(IRender* render):cls::renderProxy(render){}
	/// ����� ��������� ���������� ������� ������
	virtual void SystemCall(
		cls::enSystemCall call
		){};
	virtual void RenderCall( 
		cls::IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
		){};
};

//! 
bool ExternVNAttributeGenerator::OnAttribute(
	cls::INode& inode,				//!< ������ ��� �������� ����������� �������
	cls::IRender* render,			//!< render
	cls::IExportContext* context, 
	MObject& generatornode
	)
{
	// ��� ������� ���� - �� ��� ������� ���������
	cls::PS inputGeometry = ocExport_GetAttrValue(generatornode, "inputGeometry");
	if( inputGeometry.empty() || !inputGeometry.data()[0])
		return false;

	// ��� ������� ���� (�� ��� ����������� ��� ��������)
	cls::PS nodeName = ocExport_GetAttrValue(generatornode, "nodeName");
	if( nodeName.empty() || !nodeName.data()[0])
		return false;

	if( render)
	{
		MObject meshnode;
		nodeFromName(inputGeometry.data(), meshnode);
		if( meshnode.isNull()) 
			return false;

		// ������� ���������
		cls::INode* geomnode = context->getNodeForObject(meshnode);
		// ������ ���������
		Math::Box3f lb;
		cls::renderCacheImpl<> cache;
		geomnode->Render( &cache, lb, Math::Matrix4f::id, context, cls::INode::WR_GEOMETRY);

		// ��������� ��� SystemCall � RenderCall 
		renderProxySkipRenderCall skipRender(render);
		cache.RenderGlobalAttributes(&skipRender);
		cache.Render(&skipRender);

		{
			std::string attrname = std::string( nodeName.data())+"::P";
			render->ParameterToAttribute("P", attrname.c_str());
		}
		{
			std::string attrname = std::string( nodeName.data())+"::N";
			render->ParameterToAttribute("N", attrname.c_str());
		}

/*/
		MObject node = inode.getObject();
		Util::DllProcedure dllproc;
		dllproc.LoadFormated("Mesh", "OcellarisExport");

		cls::GENERATOR_CREATOR pc = (cls::GENERATOR_CREATOR)dllproc.proc;
		cls::IGenerator* gen = (*pc)();
		cls::IGeometryGenerator* defgen = (cls::IGeometryGenerator*)gen;
		
		cls::nodeSimple meshnode(inputGeometry.data());


		Math::Box3f lb;
		cls::renderCacheImpl<> cache;
		defgen->OnGeometry( meshnode, &cache, lb, context, MObject::kNullObj);

		cls::PA< Math::Vec3f> P;
		cache.GetParameter("P", P);
		cls::PA< Math::Vec3f> N;
		cache.GetParameter("N", N);

		if( !P.empty())
		{
			std::string attrname = std::string( nodeName.data())+"::P";
			render->Attribute(attrname.c_str(), P);
		}
		if( !N.empty())
		{
			std::string attrname = std::string( nodeName.data())+"::N";
			render->Attribute(attrname.c_str(), N);
		}
/*/
	}
	return true;
}
