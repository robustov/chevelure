#pragma once

#include "ocellaris/IAttributeGenerator.h"
#include "ocellaris/OcellarisExport.h"

class PassAttributeGenerator : public cls::IAttributeGenerator
{
public:
	PassAttributeGenerator(void);
	~PassAttributeGenerator(void);

	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "PassAttributeGenerator";};

	// ��� �������� � ����
	virtual void OnLoadToMaya(
		MObject& generatornode
		);

	// � ����� ������ ��������
	virtual int GetCallType(
		MObject& generatornode
		);

	/// OnAttribute
	virtual bool OnAttribute(
		cls::INode& node,				//!< ������ ��� �������� ����������� �������
		cls::IRender* render,			//!< render
		cls::IExportContext* context, 
//		const char* attrName,	//!< ��� �������� (����������� �� ������)
//		cls::enParamType type,	//!< ��� �������� (����������� �� ������)
		MObject& generatornode
		);
};
