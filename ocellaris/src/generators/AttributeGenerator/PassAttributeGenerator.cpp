#include "StdAfx.h"
#include ".\PassAttributeGenerator.h"
#include "ocellaris\renderStringImpl.h"

PassAttributeGenerator passattributegenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl PassAttribute()
	{
		return &passattributegenerator;
	}
}

PassAttributeGenerator::PassAttributeGenerator(void)
{
}

PassAttributeGenerator::~PassAttributeGenerator(void)
{
}

// ��� �������� � ����
void PassAttributeGenerator::OnLoadToMaya(
	MObject& generatornode
	)
{
	MObject attr;
	attr = ocExport_AddAttrType(generatornode, "allPassString", cls::PT_STRING, cls::PS(""));
	attr = ocExport_AddAttrType(generatornode, "finalPassString", cls::PT_STRING, cls::PS(""));
	attr = ocExport_AddAttrType(generatornode, "shadowPassString", cls::PT_STRING, cls::PS(""));
}
// � ����� ������ ��������
int PassAttributeGenerator::GetCallType(
	MObject& generatornode
	)
{
	cls::P<int> type = ocExport_GetAttrValue(generatornode, "calltypeAttribute");
	if(!type.empty())
	{
		cls::IAttributeGenerator::enCallType ct = (cls::IAttributeGenerator::enCallType)type.data();
		return ct;
	}
	return CT_BEFORE_TRANSFORM;
}

//! 
bool PassAttributeGenerator::OnAttribute(
	cls::INode& node,				//!< ������ ��� �������� ����������� �������
	cls::IRender* render,			//!< render
	cls::IExportContext* context, 
//	const char* attrName,	//!< ��� �������� (����������� �� ������)
//	cls::enParamType _type,	//!< ��� �������� (����������� �� ������)
	MObject& generatornode
	)
{
	cls::PS allPassString    = ocExport_GetAttrValue(generatornode, "allPassString");
	cls::PS finalPassString  = ocExport_GetAttrValue(generatornode, "finalPassString");
	cls::PS shadowPassString = ocExport_GetAttrValue(generatornode, "shadowPassString");

	// ALL PASS
	if( !allPassString.empty() && allPassString.data()[0])
	{
		cls::renderStringImpl fileimpl;

		std::string customdata = allPassString.data();
		customdata += "\n";

		if( fileimpl.openForRead(customdata.c_str()))
			fileimpl.Render(render);
		else
			displayString("read addStringAllpass FAILURE");
	}

	// FINAL PASS
	if( !finalPassString.empty() && finalPassString.data()[0])
	{
//		if( strcmp( context->pass(), "final")==0)
		{
			render->IfAttribute("@pass", cls::PS("final"));
			cls::renderStringImpl fileimpl;
			std::string customdata = finalPassString.data();
			customdata += "\n";
			if( fileimpl.openForRead(customdata.c_str()))
				fileimpl.Render(render);
			else
				displayString("read addStringFinalpass FAILURE");
			render->EndIfAttribute();
		}
	}
	
	// SHADOW PASS
	if( !shadowPassString.empty() && shadowPassString.data()[0])
	{
//		if( strcmp( context->pass(), "shadow")==0)
		{
			render->IfAttribute("@pass", cls::PS("shadow"));
			cls::renderStringImpl fileimpl;
			std::string customdata = shadowPassString.data();
			customdata += "\n";
			if( fileimpl.openForRead(customdata.c_str()))
				fileimpl.Render(render);
			else
				displayString("read addStringShadowpass FAILURE");
			render->EndIfAttribute();
		}
	}
	return true;
}
