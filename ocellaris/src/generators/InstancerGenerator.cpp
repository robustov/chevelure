#include "stdafx.h"
#include "InstancerGenerator.h"
#include "ocellaris/OcellarisExport.h"
#include <maya/MFnDependencyNode.h>
#include <maya/MFnParticleSystem.h> 
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include "mathNmaya/mathNmaya.h"
#include <maya/MPlug.h>
#include <maya/MVector.h>
#include <maya/MFnInstancer.h>

#include "ocellaris/renderFileImpl.h"
#include "ocellaris/renderCacheImpl.h"
#include "Util/misc_create_directory.h"

InstancerGenerator instancerGenerator;
InstancerPassGenerator instancerPassGenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl GetInstancerGenerator()
	{
		return &instancerGenerator;
	}
	__declspec(dllexport) cls::IGenerator* __cdecl GetInstancerPassGenerator()
	{
		return &instancerPassGenerator;
	}
	
}

//! @ingroup implement_group
//! \brief ����������� ��������� ��� Instancer
//!

// ��� �������� � ����
void InstancerGenerator::OnLoadToMaya(
	MObject& generatornode
	)
{
	MObject attr;
	attr = ocExport_AddAttrType(generatornode, "ocsFile",	cls::PT_STRING, cls::PS(""));
	attr = ocExport_AddAttrType(generatornode, "from1",		cls::PT_STRING, cls::PS(""));
	attr = ocExport_AddAttrType(generatornode, "to1",		cls::PT_STRING, cls::PS(""));
	attr = ocExport_AddAttrType(generatornode, "from2",		cls::PT_STRING, cls::PS(""));
	attr = ocExport_AddAttrType(generatornode, "to2",		cls::PT_STRING, cls::PS(""));
	attr = ocExport_AddAttrType(generatornode, "PPAttrs",	cls::PT_STRING, cls::PS("") );
}

// ����������� ��������� (���������� 1 ��� ����� ������� ��������)
bool InstancerGenerator::OnPrepare(
	cls::IExportContext* context
	)
{
	return true;
}

std::map<cls::INode*, InstancerPassGenerator::ObjectList> InstancerPassGenerator::data;

//! ����������� ���� � �������� 
bool InstancerGenerator::OnPrepareNode(
	cls::INode& node, 
	cls::IExportContext* context,
	MObject& generatornode
	)
{
	MObject obj = node.getObject();

	MStatus stat;
	MFnInstancer instancer(obj, &stat);
	if( !stat) 
	{
		context->error("InstancerGenerator: object %s isnot instancer", node.getName());
		return false;
	}

	cls::INode* instancerpassnode = context->addPass(obj);
	if( !instancerpassnode)
	{
		context->error("InstancerGenerator: add pass failed");
		return false;
	}

	MPlug plug = instancer.findPlug("inputHierarchy");

	InstancerPassGenerator::ObjectList& data = InstancerPassGenerator::data[&node];
	data.reserve(plug.numConnectedElements());

	std::string LAZYINSTANCERGENERATOR = context->getEnvironment("LAZYINSTANCERGENERATOR");
	bool lazy = (LAZYINSTANCERGENERATOR=="1");

	for(int e=0; e<(int)plug.numConnectedElements(); e++)
	{
		MPlug pe = plug.connectionByPhysicalIndex(e);
		MObject peobj = pe.node();

		MPlugArray array;
		pe.connectedTo(array, true, false);

		for(int i=0; i<(int)array.length(); i++)
		{
			MPlug objplug = array[i];
			MObject obj = objplug.node();

			// �������� ������
			if( !lazy)
			{
				cls::INode* node = context->addNode(obj, instancerpassnode);
				if( !node) continue;
				data.push_back(InstancerPassGenerator::Data());
				InstancerPassGenerator::Data& item = data.back();
				item.root = node;
				// �������� � ������ ��������
				InstancerPassGenerator::recAdd(*node, item, context, instancerpassnode);
			}
			else
			{
				data.push_back(InstancerPassGenerator::Data());
				InstancerPassGenerator::Data& item = data.back();
				cls::INode* node = context->addNode(obj, node);	// ��� ����������� ��� ��������� - ���� ������� �� ���������� �������
				if( !node) continue;
				item.root = node;
				InstancerPassGenerator::recAdd(*node, item, context, node);// ��� ����������� ��� ��������� - ���� ������� �� ���������� �������
			}
		}
	}

	return true;
}

bool InstancerGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	bool bInstanceRenderProc = true;

	MObject node = inode.getObject();
	MDagPath path = inode.getPath();

	MStatus stat;
	MFnInstancer instancer(path, &stat);
	if( !stat) 
	{
		context->error("InstancerGenerator: object %s not instancer", inode.getName());
		return false;
	}

	// ������ ��������
	InstancerPassGenerator::ObjectList& data = InstancerPassGenerator::data[&inode];

	MObject gennode = generatornode;
	if(gennode.isNull())
	{
		gennode = inode.getObject();
		this->OnLoadToMaya(gennode);
	}
	cls::PS from[2];
	cls::PS to[2];
	cls::PS ocsFile = ocExport_GetAttrValue(gennode, "ocsFile");
	from[0]   = ocExport_GetAttrValue(gennode, "from1");
	to[0]     = ocExport_GetAttrValue(gennode, "to1");
	from[1]   = ocExport_GetAttrValue(gennode, "from2");
	to[1]     = ocExport_GetAttrValue(gennode, "to2");

	cls::PS PPAttrs = ocExport_GetAttrValue( gennode, "PPAttrs" );
	std::vector< std::string > PPAttrs_str;
	char p_PPAttrs[128];
	if( !PPAttrs.empty() )
	{
		strcpy( p_PPAttrs, PPAttrs.data() );
		char seps[] = " ";
		char *token;
		token = strtok( p_PPAttrs, seps );

		while( token != NULL )
		{
			PPAttrs_str.push_back( token );
			token = strtok( NULL, seps );
		}
	}

	if( node.isNull()) 
	{
		context->error("InstancerGenerator: object %s failed", inode.getName());
		return false;
	}
	std::string tmpdir = context->getEnvironment("RIBDIR");

	MFnDependencyNode dn(node);

	// ����� particle
	MObject psobj;
	MObject instancerobj = node;
	int instancerId = *(int*)(&instancerobj);
	{
		MPlug inputPoints = dn.findPlug("inputPoints", &stat);
		if( !stat)
		{
			context->error("InstancerGenerator: object %s !findPlug inputPoints", inode.getName());
			return false;
		}

		MPlugArray array;
		inputPoints.connectedTo(array, true, false);
		if(array.length()==0)
		{
			context->error("InstancerGenerator: object %s !findPlug1 inputPoints", inode.getName());
			return false;
		}
		psobj = array[0].node();
	}
	MFnParticleSystem ps(psobj);
//	ps.evaluateDynamics(MAnimControl::currentTime(), false);
	cls::PA<float> ids = ocExport_GetAttrArrayValue(psobj, "id");
	// ���: id -> index in ids
	std::map<int, int> id2index;
	for(int i=0; i<ids.size(); i++)
	{
		int id = (int)ids[i];
		id2index[id] = i;
	}
	cls::P<float> currentTime = ocExport_GetAttrValue(psobj, "currentTime");
	render->Parameter("currentTime", currentTime);
	

//	MPlug template_plug = dn.findPlug("template");
//	bool template_val = false;
//	template_plug.getValue( template_val);
//	template_plug.setValue( false);

	{
		cls::PA<int> _id(cls::PI_VERTEX, ids.size());
		cls::PA<Math::Matrix4f> _position(cls::PI_VERTEX, ids.size());
		cls::PA<int> _fileindex(cls::PI_VERTEX, ids.size());
		
		std::vector<std::string> filenames(data.size());
		std::vector<Math::Box3f> fileboxes(data.size());

		for(int x=0; x<(int)data.size(); x++ )
		{
			InstancerPassGenerator::Data& obj = data[x];
			obj.getFileAndBox( filenames[x], fileboxes[x], context);
		}

		int pc = (int)instancer.particleCount();
		for(int p=0; p<pc; p++)
		{
			int findex = -1;
			// ����� ������
			MMatrix instancerMatrix;
			MDagPathArray paths;
			instancer.instancesForParticle(p, paths, instancerMatrix, &stat);
			Math::Box3f instancebox;
			for(int i=0; i<(int)paths.length(); i++)
			{
				// ����� ����� path � data
				for(int x=0; x<(int)data.size(); x++ )
				{
					InstancerPassGenerator::Data& item = data[x];
					if( item.isMyPath(paths[i]))
					{
						findex = x;
						instancebox = item.box;
						break;
					}
				}
				if(findex>=0)
					break;
			}
			Math::Matrix4f m; 
			::copy(m, instancerMatrix);

//			std::map<int, int>::iterator it = id2index.find(id);
//			int index = i;
//			if( it!=id2index.end())
//				index = it->second;
			int index = p;
			int id = (int)ids[p];
			_id[index] = id;
			_position[index] = m;
			_fileindex[index] = findex;

			// box
			instancebox.transform(m);
			box.insert(instancebox);
		}

		// ����������� ���������
		cls::PA<float> age = ocExport_GetAttrArrayValue(psobj, "age");
		cls::PA<Math::Vec3f> velocity = ocExport_GetAttrArrayValue(psobj, "velocity");
		cls::PA<Math::Vec3f> rgbPP = ocExport_GetAttrArrayValue(psobj, "rgbPP");
		cls::PA<Math::Vec3f> opacityPP = ocExport_GetAttrArrayValue(psobj, "opacityPP");

		age.setInterpolation( cls::PI_CONSTANT);
		velocity.setInterpolation( cls::PI_CONSTANT);
		rgbPP.setInterpolation( cls::PI_CONSTANT);
		rgbPP.setType( cls::PT_COLOR);
		opacityPP.setInterpolation( cls::PI_CONSTANT);
		opacityPP.setType( cls::PT_COLOR);

		render->Parameter("#particleid", _id);
		render->Parameter("#M", _position);
		render->Parameter("#fileindex", _fileindex);
		render->Parameter("#filenames", cls::PSA(filenames));
		render->Parameter("age", age);
		render->Parameter("velocity", velocity);
		render->Parameter("rgbPP", rgbPP);
		render->Parameter("opacityPP", opacityPP);

		// ������������� ���������
		for(int p=0; p<2; p++)
		{
			if( !from[p].emptystring())
			{
				cls::Param addp = ocExport_GetAttrArrayValue(psobj, from[p].data());
				std::string name = from[p].data();
				if( !to[p].emptystring())
				{
					name = to[p].data();
					// ���� ���� ������ - ������ ���
					std::string buf = to[p].data();
					char *token = strtok( (char*)buf.c_str(), " \t");
					if( token != NULL)
					{
						cls::enParamType type = cls::type_fromstr(token);
						token = strtok( NULL, " \t" );
						if( token != NULL && type != cls::PT_UNKNOWN)
						{
							name = token;
							addp.setType( type);
						}
					}
				}

				addp.setInterpolation( cls::PI_CONSTANT);
				render->Parameter(name.c_str(), addp);
			}
		}

//		std::string ocsPPAttrs;
		for( int k = 0; k < PPAttrs_str.size(); k++ )
		{
			const char* name = PPAttrs_str[k].c_str();
			cls::Param param = ocExport_GetAttrArrayValue( psobj, name );

			if( !param.empty() )
			{
				if( param->type == cls::PT_VECTOR )
				{
					cls::PA<Math::Vec3f> ocsPP( param );
					ocsPP.setType( cls::PT_COLOR );
					ocsPP.setInterpolation( cls::PI_CONSTANT );

					render->Parameter( "user::"+std::string(name), ocsPP );

//					ocsPPAttrs.append( std::string(name) );
//					ocsPPAttrs.append( " " );
				}
				else if( param->type == cls::PT_FLOAT )
				{
					cls::PA<float> ocsPP( param );
					ocsPP.setType( cls::PT_FLOAT );
					ocsPP.setInterpolation( cls::PI_CONSTANT );

					render->Parameter( "user::"+std::string(name), ocsPP );

//					ocsPPAttrs.append( name );
//					ocsPPAttrs.append( " " );
				}
			}
		}
//		if( !ocsPPAttrs.empty() )
//		{
//			render->Parameter( "#PPAttrs", cls::PSA(PPAttrs_str) );
//		}

		render->RenderCall("Instance");
	}

	/*/
	if( bInstanceRenderProc)
	{
		cls::PA<int> _id(cls::PI_VERTEX, ids.size());
		cls::PA<Math::Matrix4f> _position(cls::PI_VERTEX, ids.size());
		cls::PA<int> _fileindex(cls::PI_VERTEX, ids.size());

		std::vector<std::string> filenames;
		std::vector<Math::Box3f> fileboxes;
		std::map<std::string, int> filenames_index;

		bool bocsFile = false;
		if( !ocsFile.emptystring())
		{
			cls::renderFileImpl infileimpl;
			if( infileimpl.openForRead(ocsFile.data()))
			{
				Math::Box3f b;
				if( infileimpl.GetAttribute("file::box", b))
				{
					// box & filename
					filenames.push_back(ocsFile.data());
					fileboxes.push_back(b);
					bocsFile = true;
				}
			}
		}

		MItInstancer it;
		int lastid = -1;
		int i=0;
		while( ! it.isDone() ) 
		{
			if( it.instancerId() != instancerId)
//			if( !(it.instancerPath() == path))
			{
				it.nextInstancer();
				continue;
			}
			int id = it.particleId();
			MMatrix	matrix = it.matrix();
			Math::Matrix4f m; ::copy(m, matrix);
			int pathid = it.pathId();
			MDagPath path = it.path();

			if( lastid!=id)
			{
				int findex = 0;
				if( !bocsFile)
				{
					// ������ �� �����
					std::string pathkey = path.fullPathName().asChar();
					std::map<std::string, int>::iterator iit = filenames_index.find(pathkey);
					if( iit==filenames_index.end())
					{
						std::string filename = pathkey;
						Util::correctFileName((char*)filename.c_str());
						filename = tmpdir + "/" + filename + ".ocs";

						Math::Box3f box;
						if( !exportShape(inode, path, box, filename.c_str(), context))
						{
							filename="";
							box = Math::Box3f(0);
						}

						filenames.push_back( filename);
						fileboxes.push_back( box);
						findex = filenames.size()-1;
						filenames_index[pathkey] = findex;
					}
					else
					{
						findex = iit->second;
					}
				}
				else
				{
					MMatrix matrix = path.inclusiveMatrix();
					Math::Matrix4f ebuch; ::copy(ebuch, matrix);
 
					m = ebuch*m;
				}

				Math::Box3f b = fileboxes[findex];
				b.transform(m);
				box.insert(b);

				std::map<int, int>::iterator it = id2index.find(id);
				int index = i;
				if( it!=id2index.end())
					index = it->second;
				_id[index] = id;
				_position[index] = m;
				_fileindex[index] = findex;

				lastid = id;
				i++;
			}

			it.next(); 
			if( i>=ids.size()) break;
		}

		// ����������� ���������
		cls::PA<float> age = ocExport_GetAttrArrayValue(psobj, "age");
		cls::PA<Math::Vec3f> velocity = ocExport_GetAttrArrayValue(psobj, "velocity");
		cls::PA<Math::Vec3f> rgbPP = ocExport_GetAttrArrayValue(psobj, "rgbPP");
		cls::PA<Math::Vec3f> opacityPP = ocExport_GetAttrArrayValue(psobj, "opacityPP");
		age.setInterpolation( cls::PI_CONSTANT);
		velocity.setInterpolation( cls::PI_CONSTANT);
		rgbPP.setInterpolation( cls::PI_CONSTANT);
		rgbPP.setType( cls::PT_COLOR);
		opacityPP.setInterpolation( cls::PI_CONSTANT);
		opacityPP.setType( cls::PT_COLOR);

		render->Parameter("#particleid", _id);
		render->Parameter("#M", _position);
		render->Parameter("#fileindex", _fileindex);
		render->Parameter("#filenames", cls::PSA(filenames));
		render->Parameter("age", age);
		render->Parameter("velocity", velocity);
		render->Parameter("rgbPP", rgbPP);
		render->Parameter("opacityPP", opacityPP);

		// ������������� ���������
		for(int p=0; p<2; p++)
		{
			if( !from[p].emptystring())
			{
				cls::Param addp = ocExport_GetAttrArrayValue(psobj, from[p].data());
				std::string name = from[p].data();
				if( !to[p].emptystring())
				{
					name = to[p].data();
					// ���� ���� ������ - ������ ���
					std::string buf = to[p].data();
					char *token = strtok( (char*)buf.c_str(), " \t");
					if( token != NULL)
					{
						cls::enParamType type = cls::type_fromstr(token);
						token = strtok( NULL, " \t" );
						if( token != NULL && type != cls::PT_UNKNOWN)
						{
							name = token;
							addp.setType( type);
						}
					}
				}

				addp.setInterpolation( cls::PI_CONSTANT);
				render->Parameter(name.c_str(), addp);
			}
		}

		render->RenderCall("Instance");
	}
	else
	{
		std::map<std::string, IG_Instance> instances;
		std::map<std::string, cls::Param> additionalattrs;

		MItInstancer it;
		int lastid = -1;
		while( ! it.isDone() ) 
		{
			if( it.instancerId() != instancerId)
			{
				it.nextInstancer();
				continue;
			}
			int id = it.particleId();
			MMatrix	matrix = it.matrix();
			int pathid = it.pathId();
			MDagPath path = it.path();
			Math::Matrix4f m; ::copy(m, matrix);
			IG_Instance inst;

			std::string pathkey = path.fullPathName().asChar();
			std::map<std::string, IG_Instance>::iterator iit = instances.find(pathkey);
			if( iit==instances.end())
			{
				inst.filename = pathkey;
				Util::correctFileName((char*)inst.filename.c_str());
				inst.filename = tmpdir + "/" + inst.filename + ".ocs";

				exportShape(path, inst, context);
				// �������� �������� ���������
				for(int ai=0; ai<(int)inst.externalattrs.size(); ai++)
				{
					std::string attrName = inst.externalattrs[ai].first;
					if( additionalattrs.find(attrName)!=additionalattrs.end()) continue;
					// �������� �������
					cls::Param defval = inst.externalattrs[ai].second;
					additionalattrs[attrName] = defval;
					std::string paramname = attrName.substr(strlen("global::"));
					cls::Param val = ocExport_GetAttrValue(psobj, paramname.c_str());
					if( !val.empty())
						additionalattrs[attrName] = val;
					else
					{
						val = ocExport_GetAttrArrayValue(psobj, paramname.c_str());
						val->type = defval->type;
						val->interp = cls::PI_CONSTANT;
						additionalattrs[attrName] = val;
					}
				}
				instances[pathkey.c_str()] = inst;
			}
			else
			{
				inst = iit->second;
			}

			if( lastid!=id)
			{
				if( lastid!=-1)
				{
					render->PopTransform();
					render->PopAttributes();
				}

				lastid = id;
				render->PushAttributes();
				render->Parameter("id", id);
				render->PushTransform();

				render->PushAttributes();
				render->AppendTransform(m);
				render->PopAttributes();
			}
			render->PushAttributes();
				render->Parameter("pathid", pathid);
				render->Parameter("path", path.fullPathName().asChar());
	//			float radius = 1.f;
	//			render->Sphere(radius);
				if( inst.bValid)
				{
					Math::Box3f b = inst.box;
					b.transform(m);
					box.insert(b);

					int indexinids = id2index[id];
	//				render->Parameter("#box", inst.box);
	//				float w = (inst.box.max-inst.box.min).length()/15;
	//				render->Parameter("#width", w);
	//				render->RenderCall("Box");

					// �������� ���������
					for(int ai=0; ai<(int)inst.externalattrs.size(); ai++)
					{
						std::string attrName = inst.externalattrs[ai].first;
						cls::Param val = additionalattrs[attrName];
						if( val.empty())
							continue;
						else if( val.size()==1)
						{
							render->Attribute(attrName.c_str(), val);
						}
						else
						{
							// �� id ����� ����� ��-��
							if( indexinids>=val.size())
								continue;
							cls::Param x = val[indexinids];
							render->Attribute(attrName.c_str(), x);
						}
					}

	//				render->PushAttributes();
	//					render->Attribute("global::testColor", cls::PT_COLOR, Math::Vec3f(1, 1, 0));
						render->Parameter("filename", inst.filename.c_str());
						render->RenderCall("ReadOCS");
	//				render->PopAttributes();
				}
			render->PopAttributes();

			it.next(); 
		}
		if( lastid!=-1)
		{
			render->PopTransform();
			render->PopAttributes();
		}
	}
	/*/

//	template_plug.setValue( template_val);




	/*/
	// id
	MPlug paramplug = dn.findPlug("id", &stat);
	if( !stat) return false;
	MObject idobj;
	if( !paramplug.getValue( idobj)) return false;
	MFnDoubleArrayData _id0(idobj);
	cls::PA<int> ids(cls::PI_VERTEX, _id0.length());
	for(z=0; z<(int)_id0.length(); ++z) 
		ids[z] = (int)_id0[z];

	// particleRenderType
	enParticleRenderType type;
	dn.findPlug("particleRenderType", &stat).getValue( *(int*)&type);

	// position
	MPlug plug_position = dn.findPlug("position", &stat);
	MObject obj_position;
	plug_position.getValue(obj_position);
	MFnVectorArrayData position(obj_position);
	cls::PA<Math::Vec3f> verts(cls::PI_VERTEX, ids.size());
	for(z=0; z<ids.size(); ++z) 
	{
		verts[z].x = (float)position[z].x;
		verts[z].y = (float)position[z].y;
		verts[z].z = (float)position[z].z;
		box.insert(verts[z]);
	}

	// RadiusPP
	MPlug plug_radiusPP = dn.findPlug("radiusPP", &stat);
	if(stat)
	{
		MObject idobj;
		plug_radiusPP.getValue(idobj);
		MFnDoubleArrayData _id0(idobj);
		cls::PA<float> radiusPP(cls::PI_VERTEX, ids.size());
		for(z=0; z<ids.size(); ++z) 
		{
			radiusPP[z] = (float)_id0[z];
		}
		render->Parameter("radius", radiusPP);
	}
	else
	{
		MPlug plug_radius = dn.findPlug("radius", &stat);
		if( stat)
		{
			double r; plug_radius.getValue(r);
			cls::P<float> radius((float)r);
			if( render)
				render->Parameter("radius", radius);
		}
	}

	if( render)
	{
		switch(type)
		{
		case spheres:
			{
				std::vector< std::pair< std::string, cls::Param> > params;
				render->GetParameterList(params);

				float maxradius = 0;
				for(int i=0; i<ids.size(); i++)
				{
					Math::Matrix4f m = Math::Matrix4f::id;
					m[3] = Math::Vec4f( verts[i], 1);
					render->PushAttributes();
					render->PushTransform();
					render->AppendTransform(m);
					float radius = 0.1f;

					for(unsigned z=0; z<params.size(); z++)
					{
						cls::Param& p = params[z].second;
						int s = p.size();
						std::string& name = params[z].first;

						// radius
						if( name=="radius")
						{
							if( s==ids.size() && p->interp == cls::PI_VERTEX)
								radius = cls::P<float>(p[i]).data();
							else
								radius = cls::P<float>(p[0]).data();
							continue;
						}

						// ���������
						if( s==ids.size() && p->interp == cls::PI_VERTEX)
						{
							cls::Param ps = p[i].copy();
							ps->interp = cls::PI_CONSTANT;
							render->Parameter(name.c_str(), ps);
						}
						else
							render->Parameter(name.c_str(), p);
					}
					maxradius = __max(maxradius, radius);

					render->Parameter("id", ids[i]);
					render->Sphere(radius);
					render->PopTransform();
					render->PopAttributes();
					box.expand( maxradius);
				}
				break;
			}
		case blobby:
			{
				cls::PA<float> radius = render->GetParameter("radius");
				cls::PA<Math::Matrix4f> ellipsoids(cls::PI_VERTEX, ids.size());

				float maxradius = 0;
				for(int i=0; i<ids.size(); i++)
				{
					float r = 1;
					if( radius.size()==ids.size() && radius->interp == cls::PI_VERTEX)
						r = radius[i];
					else if(radius.size()>=1)
						r = radius[0];

					maxradius = __max(maxradius, r);
					Math::Matrix4f m = Math::Matrix4f::id;
					float d = r*2.46698f;
					m.scale( Math::Vec4f(d, d, d, 1));

					m[3] = Math::Vec4f( verts[i], 1);

					ellipsoids[i] = m;
				}
				render->Blobby(ellipsoids);
				box.expand( maxradius);
				break;
			}
		default:
			{
				box.expand( 0.1f);
				render->Parameter("constantwidth", 0.1f);
				render->Parameter("id", ids);
				render->Points(verts);
				break;
			}
		}
	}
	/*/
	return true;
}


// ����������� ��������� (���������� 1 ��� ����� ������� ��������)
bool InstancerPassGenerator::OnPrepare(
	cls::IExportContext* context
	)
{
	data.clear();
	return true;
};

// ���� ������ ����������� ������� ������� � ������ ����� ��� ���
bool InstancerPassGenerator::IsUsedInFrame(
	cls::INode& pass, 
	float renderframe, 			
	cls::IExportContext* context
	)
{
	std::string LAZYINSTANCERGENERATOR = context->getEnvironment("LAZYINSTANCERGENERATOR");
	bool lazy = (LAZYINSTANCERGENERATOR=="1");
	if( lazy)
		return false;

	ObjectList& instanceobjects = data[&pass];
	for(int i=0; i<(int)instanceobjects.size(); i++)
	{
		Data& data = instanceobjects[i];
		std::list<cls::INode*>::iterator it = data.nodes.begin();
		for( ; it != data.nodes.end(); it++)
		{
			cls::INode* node = *it;
			// ��������� ��� renderframe � 0�� �����
			context->addExportTimeNode(*node, renderframe, 0);
		}
	}
	return false;
}

// �������� ������ � ��������� ExportFrame ��� �������� ����� (RenderFrame)
// �������� � ������ context->addExportTimeNode()
// ���� ����� ���� ���������� IBlurGenerator
bool InstancerPassGenerator::AddExportFrames(
	cls::INode& pass, 
	cls::INode& node, 
	float renderframe, 			
	cls::IExportContext* context
	)
{
	return true;
}
// ������� ��� ����������� ���� �� ExportFrame 
// ���� ����� ���� ���������� IBlurGenerator
bool InstancerPassGenerator::BuildNodeFromExportFrame(
	cls::INode& pass,				// pass
	cls::INode& node,				// pass
	cls::IRender* render,			// ������
	float frame,
	Math::Matrix4f& transform,		// ����������� ����� transform
	Math::Box3f& box,				// ����������� ���� ��� �������
	cls::IExportContext* context	// �������� ��������
	)
{
	return false;
}

//! ������� ������ � ��������
bool InstancerPassGenerator::OnExport(
	cls::INode& node,				//!< ������ ���������� ������� (�.�., kNullObj)
	cls::IRender* scenecache,		//!< ��� �����
	Math::Box3f scenebox,			//!< box �����
	cls::IRender* subpasscache,		//!< ��� ����������� (�� ���� ������� OnExportParentPass)
	cls::IExportContext* context	// 
	)
{
	return false;
}

// ���������� � ���� ����������� ��� �������� �������
// ��������, ������ shadowpass ������ �������� � ������� ��� �������� ����
void InstancerPassGenerator::OnExportParentPass( 
	cls::INode& subnode,				//!< ������ ���������� ������� (�.�., kNullObj)
	cls::IRender* render, 
	cls::IExportContext* context
	)
{
}
//! ���������� ����� �������� ���� ������
bool InstancerPassGenerator::OnPostExport(
	cls::INode& node,				//!< ������ ���������� ������� (�.�., kNullObj)
	cls::IExportContext* context	// 
	)
{
	data.clear();
	return true;
};

void InstancerPassGenerator::recAdd(
	cls::INode& root, 
	InstancerPassGenerator::Data& data, 
	cls::IExportContext* context, 
	cls::INode* instancerpassnode
	)
{
	data.nodes.push_back(&root);

	MDagPath path = root.getPath();
	for( unsigned int i=0; i<path.childCount(); i++)
	{
		MObject obj = path.child(i);

		cls::INode* node = context->addNode(obj, instancerpassnode);
		if( !node) continue;

		recAdd(*node, data, context, instancerpassnode);
	}
}

bool InstancerPassGenerator::Data::getFileAndBox(
	std::string& filename, 
	Math::Box3f& box, 
	cls::IExportContext* context)
{
	if(this->filename.empty())
	{
		std::string LAZYINSTANCERGENERATOR = context->getEnvironment("LAZYINSTANCERGENERATOR");
		bool lazy = (LAZYINSTANCERGENERATOR=="1");

		// filename
		std::string pathkey = root->getPath().fullPathName().asChar();
		Util::correctFileName((char*)pathkey.c_str());
		std::string tmpdir = context->getEnvironment("RIBDIR");
		std::string filename = tmpdir + "/" + pathkey + ".ocs";
		Util::changeSlashToUnixSlash(this->filename);

		if(lazy)
		{
			this->filename = filename;
			this->box = Math::Box3f(Math::Vec3f(0, 0, 0), 1);

			cls::renderFileImpl infileimpl;
			if( infileimpl.openForRead(filename.c_str()))
			{
				Math::Box3f box;
				if( infileimpl.GetAttribute("file::box", box))
				{
					this->filename = filename;
					this->box = box;
				}
			}
			else
			{
				context->error("InstancerGenerator: file %s not found", filename.c_str());
			}
		}
		else
		{
			bool bAscii = true;
			std::string format = context->getEnvironment("OUTPUTFORMAT");
			if(format=="ascii")
				bAscii = true;
			if(format=="binary")
				bAscii = false;

			cls::INode* root = this->root;

			cls::renderCacheImpl<cls::CacheStorageSimple> _cache;
			cls::IRender* render = &_cache;

			// �� ����� �����
			Math::Matrix4f m = Math::Matrix4f::id;
		
			render->PushTransform();
			render->PushAttributes();
			render->AppendTransform(m);
			render->PopAttributes();

			Math::Matrix4f transform = Math::Matrix4f::id;
			float exportFrame = 0;
			Math::Box3f localbox;
			recExport(
				exportFrame, 
				*root, 
				transform, 
				localbox, 
				render, 
				context, 
				0
				);

			render->PopTransform();

			// save to file
			Util::create_directory_for_file(filename.c_str());
			cls::renderFileImpl _render;
			if( !_render.openForWrite(filename.c_str(), bAscii))
			{
				context->error("InstancerGenerator: file %s not saved", filename.c_str());
				return false;
			}

			_render.Attribute("file::box", localbox);
			_cache.RenderGlobalAttributes(&_render);
			_cache.Render(&_render);

			this->filename = filename;
			this->box = localbox;
		}
	}
	filename = this->filename;
	box = this->box;
	return true;
}
// ���� ���� � ���� ������?
bool InstancerPassGenerator::Data::isMyPath(MDagPath path)
{
	std::list<cls::INode*>::iterator it = this->nodes.begin();		// ��� �������
	for( ;it != this->nodes.end(); it++)
	{
		cls::INode* node = *it;
		if( node->getPath() == path)
			return true;
	}
	return false;
}
void InstancerPassGenerator::Data::recExport(
	float exportFrame, 
	cls::INode& root, 
	Math::Matrix4f transform, 
	Math::Box3f& box, 
	cls::IRender* render, 
	cls::IExportContext* context, 
	int level
	)
{
	int whatRender = cls::INode::WR_ALL;
	Math::Matrix4f m = transform;
//	if( level!=0)
		root.PushNode(render, m, box, context, whatRender);

	// �������
	cls::IRender* cache=NULL;
	Math::Box3f* thisbox=NULL;
	if( context->getNodeExportFrameCache(exportFrame, root, cache, thisbox))
	{
		cache->RenderGlobalAttributes(render);
		cache->Render(render);
		Math::Box3f localbox = *thisbox;
		localbox.transform(m);
		box.insert(localbox);
	}

	MDagPath path = root.getPath();
	for( unsigned int i=0; i<path.childCount(); i++)
	{
		MObject obj = path.child(i);

		cls::INode* node = context->addNode(obj);
		if( !node) continue;

		recExport(exportFrame, *node, m, box, render, context, level+1);
	}

//	if( level!=0)
		root.PopNode(render, context, whatRender);
}


