#include "stdafx.h"
#include "MeshGenerator.h"
#include <maya/MFnMesh.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MPlug.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MIntArray.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MPointArray.h>
#include <string>

#include <maya/MFnSet.h>
#include <maya/MFnSingleIndexedComponent.h>
#include <maya/MItMeshEdge.h>
#include <vector>

MeshGenerator meshgenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl Mesh()
	{
		return &meshgenerator;
	}
}

bool MeshGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();
	MStatus stat;
	MString minterpolation;
	std::string interpolation = "linear";
	bool bDoubleSide = true;
    bool usingSubdivMtor = false;

	MFnMesh mesh(node, &stat);
	if( !stat) 
		return false;
	mesh.findPlug("doubleSided").getValue(bDoubleSide);
	mesh.findPlug("interpolation").getValue(minterpolation);
    mesh.findPlug("mtorSubdiv").getValue(usingSubdivMtor);
	if( minterpolation.length())
		interpolation = minterpolation.asChar();
	else
		if( usingSubdivMtor) 
			interpolation = "catmull-clark";

	if( !MeshToOcs(
		inode,				//!< ������ 
		render,			//!< render
		box,				//!< box 
		context,	//!< context
		interpolation!="linear"
		))
		return true;

	cls::PA<Math::Vec3f> P = render->GetParameter("P");
	MPlug plug;
	plug = mesh.findPlug("rmanP__Pref");
	if( !plug.isNull())
	{
		MObject obj;
		plug.getValue(obj);
		MFnVectorArrayData va(obj);
		if( va.length() == P.size())
		{
			cls::PA<Math::Vec3f> __Pref(cls::PI_VERTEX); 
			__Pref.resize(P.size());
			for(int v=0; v<P.size(); v++)
			{
				__Pref[v] = Math::Vec3f( (float)va[v].x, (float)va[v].y, (float)va[v].z);
			}
			render->Parameter("__Pref", cls::PT_POINT, __Pref);
		}
	}
	render->Parameter("DoubleSide", bDoubleSide?1:0);

	render->Parameter("mesh::interpolation", interpolation.c_str());
	render->SystemCall(cls::SC_MESH);
//	render->Mesh(interpolation.c_str(), loops, nverts, iverts);
	return true;
}

bool MeshGenerator::MeshToOcs(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	bool usingSubdiv
	)
{
	MStatus stat;
	MObject node = inode.getObject();

	MFnMesh mesh(node, &stat);
	if( !stat) 
		return false;

	if( usingSubdiv && render )
	{
		MString info;
		MObjectArray sets, components;
		mesh.getConnectedSetsAndMembers( 0, sets, components, 0 );

		std::vector< std::pair<int, short> > corner_v;
		std::vector< std::pair<int, short> > crease_v;
		std::vector< int > hole_v;

		for( unsigned int i = 0; i < sets.length(); i++ )
		{
			MIntArray componentElements;
			short corner = 0, crease = 0;
			bool hole = false;

			MObject setObj = sets[i];
			MObject componentObj = components[i];
			MFnSingleIndexedComponent component( componentObj );
			MFnSet set(setObj);
			
			switch ( set.restriction() )
			{
				case MFnSet::kVerticesOnly:
				{
					if ( set.findPlug( "mtorSubdivCorner" ).getValue( corner ) && corner != 0 )
					{
						component.getElements( componentElements );
						corner_v.reserve( componentElements.length() );
						for ( unsigned int k = 0; k < componentElements.length(); k++ )
						{
							corner_v.push_back( std::pair<int, short>( componentElements[k], corner) );
						}
					}
					break;
				}
				case MFnSet::kEdgesOnly:
				{
					if ( set.findPlug( "mtorSubdivCrease" ).getValue( crease ) && crease != 0)
					{
						component.getElements( componentElements );
						crease_v.reserve( componentElements.length() );
						for ( unsigned int k = 0; k < componentElements.length(); k++ )
						{
							crease_v.push_back( std::pair<int, short>( componentElements[k], crease) );
						}
					}
					break;
				}
				case MFnSet::kFacetsOnly:
				{
					if ( set.findPlug( "mtorSubdivHole" ).getValue( hole ) && hole != false )
					{
						component.getElements( componentElements );
						hole_v.reserve( componentElements.length() );
						for ( unsigned int k = 0; k < componentElements.length(); k++ )
						{
							hole_v.push_back( componentElements[k] );
						}
					}
					break;
				}
			}
		}

		bool interpolateBoundary = false;
		bool ignoreEdges = true;
		mesh.findPlug("mtorSubdivInterp").getValue( interpolateBoundary );
		mesh.findPlug("mtorSubdivIgnoreEdges").getValue( ignoreEdges );

		std::vector< int > hardEdges_v;
		if ( !ignoreEdges )
		{
			MItMeshEdge iter( node );
			hardEdges_v.reserve( iter.count() );

			for ( ; !iter.isDone(); iter.next() )
			{
				if ( !iter.isSmooth() )
				{
					hardEdges_v.push_back( iter.index() );
				}
			}
		}

		cls::PA<int> holes( cls::PI_PRIMITIVE, hole_v );
		cls::PA<int> crease_edges( cls::PI_PRIMITIVE );
		cls::PA<float> crease_hardness( cls::PI_PRIMITIVE );
		cls::PA<int> corner_vertices( cls::PI_VERTEX );
		cls::PA<float> corner_hardness( cls::PI_VERTEX );
	
		crease_edges.reserve( (int)crease_v.size()*2 + (int)hardEdges_v.size()*2 );
		crease_hardness.reserve( (int)crease_v.size() + (int)hardEdges_v.size() );
		for ( std::vector< std::pair<int, short> >::size_type i = 0; i < crease_v.size(); i++ )
		{
			int vertex_2[2];
			mesh.getEdgeVertices( crease_v[i].first, vertex_2 );
			crease_edges.push_back( vertex_2[0] );
			crease_edges.push_back( vertex_2[1] );
			crease_hardness.push_back( (float)crease_v[i].second );
		}

		for ( std::vector< int >::size_type i = 0; i < hardEdges_v.size(); i++ )
		{
			int vertex_2[2];
			mesh.getEdgeVertices( hardEdges_v[i], vertex_2 );
			crease_edges.push_back( vertex_2[0] );
			crease_edges.push_back( vertex_2[1] );
			crease_hardness.push_back( 32767.0f );
		}

		corner_vertices.reserve( corner_v.size() );
		corner_hardness.reserve( corner_v.size() );
		for ( std::vector< std::pair<int, short> >::size_type i = 0; i < corner_v.size(); i++ )
		{
			corner_vertices.push_back( corner_v[i].first );
			corner_hardness.push_back( (float)corner_v[i].second );
		}

		if ( !holes.empty() )
		{
			render->Parameter( "#subdiv::holes", holes );
		}

		if ( !crease_edges.empty() )
		{
			render->Parameter( "#subdiv::crease_edges", crease_edges );
			render->Parameter( "#subdiv::crease_hardness", crease_hardness );
		}

		if ( !corner_vertices.empty() )
		{
			render->Parameter( "#subdiv::corner_vertices", corner_vertices );
			render->Parameter( "#subdiv::corner_hardness", corner_hardness );
		}

		if ( interpolateBoundary )
		{
			render->Parameter( "#subdiv::interpolateBoundary", interpolateBoundary );
		}
	}

	int fc = mesh.numPolygons();
	int nc = mesh.numNormals();
	int vc = mesh.numVertices();
	int fvc = mesh.numFaceVertices();

	if( !fc || !vc)
		return false;

	MFloatPointArray _verts;
	MFloatArray _uArray, _vArray;
	MFloatVectorArray _normals;
	mesh.getPoints(_verts);
	mesh.getUVs(_uArray, _vArray);
	mesh.getNormals( _normals );

	cls::PA<int> loops(cls::PI_PRIMITIVE);
	cls::PA<int> nverts(cls::PI_PRIMITIVE);
	cls::PA<int> iverts(cls::PI_PRIMITIVEVERTEX);
	cls::PA<Math::Vec3f> verts(cls::PI_VERTEX);
	cls::PA<Math::Vec3f> norms(cls::PI_PRIMITIVEVERTEX);
	cls::PA<float> s(cls::PI_PRIMITIVEVERTEX), t(cls::PI_PRIMITIVEVERTEX);
	cls::PA<int> faces(cls::PI_PRIMITIVEVERTEX);

	bool bUV = false;
	MStringArray setNames;
	if( mesh.getUVSetNames( setNames))
		if( setNames.length()!=0)
			bUV = true;
 
	if( render)
	{
		loops.reserve(fc);
		nverts.reserve(fc);
		iverts.reserve(fvc);
		verts.reserve(vc);
		norms.reserve(fvc);
		if( bUV)
		{
			s.reserve(fvc);
			t.reserve(fvc);
		}
		faces.reserve(fvc*3);
	}

	for(unsigned v=0; v<_verts.length(); v++)
	{
		MFloatPoint& pt = _verts[v];
		Math::Vec3f p(pt.x, pt.y, pt.z);
		box.insert(p);
		if( render)
			verts.push_back(p);
	}

	if( render)
	{
		bool bReverse = true;

		// ������ ��������
		{
			int faceVertex = 0;
			for ( MItMeshPolygon polyIt ( node ); !polyIt.isDone(); polyIt.next() ) 
			{
				MIntArray vertices;
				polyIt.getVertices( vertices);
				int count = (int)vertices.length();
				nverts.push_back(count);
				loops.push_back(1);
//				for(int v=count-1; v>=0; v--, faceVertex++)
				int startFV = faceVertex;
				for(int xv=0; xv<count; xv++, faceVertex++)
				{
					int v = bReverse?(count-xv-1):(v);
					int vertex = vertices[ v];
					int nn = polyIt.normalIndex(v);
					int stind;
					float _u, _v;
					if( !polyIt.getUVIndex(v, stind, _u, _v))
						bUV = false;

					// V
					iverts.push_back(vertex);

					// N
					MFloatVector& n = _normals[nn];
					norms.push_back( Math::Vec3f(n.x, n.y, n.z));

					// ST
					if( bUV)
					{
						s.push_back(_uArray[stind]);
						t.push_back(1-_vArray[stind]);
					}
				}
				if( polyIt.hasValidTriangulation())
				{
					int tngCount = 0;
					polyIt.numTriangles(tngCount);
					for(int hh = 0; hh<tngCount; hh++)
					{
						MPointArray FaceVertex;
						MIntArray FaceVtxTrngl;
						polyIt.getTriangle(hh, FaceVertex, FaceVtxTrngl);

						Math::Vec3i f( FaceVtxTrngl[0], FaceVtxTrngl[1], FaceVtxTrngl[2]);

						// ������ ��������� ��������
						Math::Vec3i inds;
						for(unsigned vi=0; vi<FaceVtxTrngl.length(); vi++)
							for(unsigned d=0; d<vertices.length(); d++)
								if( vertices[d]==FaceVtxTrngl[vi])
									inds[vi] = startFV+d;
						faces.push_back(inds[0]);
						faces.push_back(inds[1]);
						faces.push_back(inds[2]);
					}
				}
			}
		}

		render->Parameter("P", cls::PT_POINT, verts);
		if( !usingSubdiv)
			render->Parameter("N", cls::PT_NORMAL, norms);
		else
			render->Parameter("#N", cls::PT_NORMAL, norms);
		if( bUV)
		{
			render->Parameter("s", s);
			render->Parameter("t", t);
		}
		render->Parameter("gl::triangles", faces);

		render->Parameter("mesh::loops", loops);
		render->Parameter("mesh::nverts", nverts);
		render->Parameter("mesh::verts", iverts);

//fprintf(stderr, "fvc=%d, n=%d iv=%d\n", fvc, norms.size(), iverts.size());
	}
	return true;
}

