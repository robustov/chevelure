#include "stdafx.h"
#include "ParticleAttributeGenerator.h"
#include <maya/MFnDependencyNode.h>
#include <maya/MFloatArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MPlug.h>
#include <maya/MVector.h>

bool ParticleAttributeGenerator::OnAttribute(
	cls::INode& node,				//!< ������ ��� �������� ����������� �������
	cls::IRender* render,		//!< render
	cls::IExportContext* context, 
//	const char* attrName,		//!< ��� ��������
//	cls::enParamType type,
	MObject& generatornode
	)
{
const char* attrName = NULL;
cls::enParamType type = cls::PT_UNKNOWN;

	std::string name;
	MStatus stat;

	MFnDependencyNode dn(node.getObject(), &stat);
	MString stypename = dn.typeName();

	MPlug plug;
	plug = dn.findPlug("i_input");
	if( !plug.isNull())
	{
		MObject obj_input;
		plug.getValue(obj_input);

		if(stypename=="ocParticleVectorAttribute")
		{
			MFnVectorArrayData _data(obj_input, &stat);
			if( stat)
			{
				cls::PA<Math::Vec3f> data(cls::PI_VERTEX, _data.length());
				unsigned z;
				for(z=0; z<(int)_data.length(); ++z) 
				{
					data[z].x = (float)_data[z].x;
					data[z].y = (float)_data[z].y;
					data[z].z = (float)_data[z].z;
				}

				if( render && !data.empty())
					if( attrName)
						render->Parameter(attrName, type, data);
					else
						render->Parameter("NULL", type, data);
			}
			return true;
		}
		if(stypename=="ocParticleFloatAttribute")
		{
			MFnDoubleArrayData _data(obj_input, &stat);
			if( stat)
			{
				cls::PA<float> data(cls::PI_VERTEX, _data.length());
				unsigned z;
				for(z=0; z<(int)_data.length(); ++z) 
					data[z] = (float)_data[z];

				if( render)
					if( attrName)
						render->Parameter(attrName, type, data);
					else
						render->Parameter("NULL", type, data);
			}
			return true;
		}
	}
	return false;
}
