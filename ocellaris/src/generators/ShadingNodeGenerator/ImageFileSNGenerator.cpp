#include "StdAfx.h"
#include "ImageFileSNGenerator.h"
#include "ocellaris/IShaderAssembler.h"
#include "Util/misc_create_directory.h"
#include <maya/MRenderUtil.h>

ImageFileSNGenerator::ImageFileSNGenerator(void)
{
}

ImageFileSNGenerator::~ImageFileSNGenerator(void)
{
}

#define output(STRING) (as->Add(STRING))

//! ������ ������� false ���� ������ ����������� ���� ��������� �� ���������
bool ImageFileSNGenerator::BuildShader(
	MObject& node,					//!< ������
	cls::enParamType wantedoutputtype, //!< ����� ��� ����� �� ������
	cls::IShaderAssembler* as,		//!< assembler
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	bool bColor = (wantedoutputtype!=cls::PT_FLOAT);

	if( bColor)
		as->OutputParameter("result", cls::PT_COLOR);
	else
		as->OutputParameter("floatresult", cls::PT_FLOAT);


	MString texturePath;
	MRenderUtil::exactFileTextureName(node, texturePath);
//context->error(texturePath.asChar());

	cls::PS imagefile = ocExport_GetAttrValue(node, "fileTextureName");
	if( !bColor)
		as->Add( "color result=color(1, 1, 1);");
	if( imagefile.empty())
	{
		as->Add( "result=color(1, 1, 1);");
	}
	else
	{
	}
	std::string filename = imagefile.data();
	std::string outfilename = filename;
	// �������� ���������� � ��� �����
	{
		std::string extention = Util::extract_extention(filename.c_str());
		std::string shortfilename = Util::extract_filename(filename.c_str());
		int pos = (int)shortfilename.rfind('.');
		if(pos!=std::string::npos)
			shortfilename = shortfilename.substr(0, pos);

		if(extention=="tex")
		{
			// ���� �� ������!!!
		}
		else
		{
			// ������ ���������������
			std::string texdir = "";
			if( context) 
			{
				const char* sd = context->getEnvironment("TEXDIR");
				if( sd)
					texdir = sd;
			}
			if( texdir.empty())
			{
				MString _workspacedir;
				MGlobal::executeCommand("workspace -q -rd", _workspacedir);
				std::string workspacedir = _workspacedir.asChar();
				texdir = workspacedir + "rmantex/";
			}
			Util::pushBackSlash(texdir);

			cls::P<bool> wrapU = ocExport_GetAttrValue(node, "wrapU");
			cls::P<bool> wrapV = ocExport_GetAttrValue(node, "wrapV");
			std::string smode = "black";
			if( !wrapU.empty() && *wrapU)
				smode = "periodic";
			std::string tmode = "black";
			if( !wrapV.empty() && *wrapV)
				tmode = "periodic";
//			std::string newer = "-newer";
			std::string newer = "";

			outfilename = texdir+shortfilename+".tex";
			std::string previewcmd = "sho \""+outfilename+"\"";

			{
				Util::create_directory(texdir.c_str());
				// txmake
				std::string RMANTREE = getenv("RMANTREE");
				Util::changeSlashToUnixSlash(RMANTREE);
				std::string cmd;
//				cmd = RMANTREE;
				cmd += "txmake.exe -smode "+smode+" -tmode "+tmode+" -filter catmull-rom -sfilterwidth 3 -tfilterwidth 3 -resize up "+newer+" ";
				cmd += "\""+filename+"\" ";
				cmd += "\""+outfilename+"\"";
				if(context)
				{
context->error(cmd.c_str());
					cls::ITask* task = context->addTask( NULL);
					task->Parameter("name", cls::PS(filename));
					task->Parameter("cmd", cls::PS(cmd));
					if( !previewcmd.empty())
						task->Parameter("preview", cls::PS(previewcmd.c_str()));
				}
			}
		}
	}

	//
//	as->InputManifold("uvCoord", "Pt", "dPtu", "dPtv");
//	as->InputParameter("fileTextureName", "File", cls::PT_STRING);
	as->InputParameter("colorGain", "ColorGain", cls::PT_COLOR);
	as->InputParameter("colorOffset", "ColorBias", cls::PT_COLOR);

	cls::P<float> rotateUV= ocExport_GetAttrValue(node, "rotateUV");
	cls::P<float> offsetU = ocExport_GetAttrValue(node, "offsetU");
	cls::P<float> offsetV = ocExport_GetAttrValue(node, "offsetV");
	cls::P<float> repeatU = ocExport_GetAttrValue(node, "repeatU");
	cls::P<float> repeatV = ocExport_GetAttrValue(node, "repeatV");

	output("	// manifold");
	output("	extern float s, t, du, dv;");
	as->AddV( "	uniform float repeatS = %f;", repeatU.empty()?0:repeatU.data());
	as->AddV( "	uniform float repeatT = %f;", repeatV.empty()?0:repeatV.data());
	as->AddV( "	uniform float offsetS = %f;", offsetU.empty()?0:offsetU.data());
	as->AddV( "	uniform float offsetT = %f;", offsetV.empty()?0:offsetV.data());
	as->AddV( "	uniform float angle   = %f;", rotateUV.empty()?0:rotateUV.data());

	output("	point Pt;");
	output("	setxcomp(Pt, repeatS * s + offsetS);");
	output("	setycomp(Pt, repeatT * t + offsetT);");
	output("	setzcomp(Pt, 0);");
	output("	if(angle != 0)");
	output("	    Pt = rotate(Pt, radians(angle), point(0,0,0), point(0,0,1));");
	output("	vector dPtu = vector Du(Pt)*du;");
	output("	vector dPtv = vector Dv(Pt)*dv;");

	output("	// texture");
	output("	uniform float SFilt=1;");
	output("	uniform float TFilt=1;");
	output("	uniform float lerp=0;");
	output("	uniform string filter = 'box';");

	as->AddV( "	uniform string File = '%s';", outfilename.c_str());
	output("	if (File != '') ");
	output("	{");
	output("		result = color texture(File, ");
	output("		xcomp(Pt),");
	output("		ycomp(Pt),");
	output("		'swidth', SFilt,");
	output("		'twidth', TFilt,");
//	output("		'filter', filter,");
	output("		'lerp', lerp);");
	output("	} ");
	output("	else");
	output("	{");
	output("	    result = color(1,1,1);");
	output("	}");

	as->Include( "#include 'ul_pxslRemap.h'");
	output("	result = ul_pxslColorBias_stuped(ColorBias, result);");
	output("	result = ul_pxslColorGain_stuped(ColorGain, result);");

	if( !bColor)
	{
		as->Add( "	floatresult = (comp(result, 0) + comp(result, 1) + comp(result, 2)) / 3;");
	}

//	output("	result = ul_pxslColorBias(ColorBias-color(0.5, 0.5, 0.5), result);");
//	output("	result = ul_pxslColorGain(ColorGain+color(0.5, 0.5, 0.5), result);");
	return true;
}
