#pragma once

#include "ocellaris/OcellarisExport.h"
#include "ocellaris/IShadingNodeGenerator.h"

//////////////////////////////////////////////////////////////////////
// ����������� ��������� ������ ����������� �� �����������
// ����������� �� ���� ������ IShaderNodeGenerator � �������� ������
// ���
class place2dTextureSNGenerator : public cls::IShadingNodeGenerator
{
public:
	place2dTextureSNGenerator(void);
	~place2dTextureSNGenerator(void);

	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "place2dTextureSNGenerator";};

	// ��� ����
	virtual enShadingNodeType getShadingNodeType(
		MObject& node					//!< ������ (shader)
		){return MANIFOLD;};

	//! ��������� ��������� 
	//! ��������� ��������� ��������� ��� �������
	virtual bool BuildShader(
		MObject& node,				//!< ������
		cls::IShaderAssembler* as,		//!< render
		cls::IExportContext* context,	//!< context
		MObject& generatornode			//!< generator node �.� 0
		);
};
