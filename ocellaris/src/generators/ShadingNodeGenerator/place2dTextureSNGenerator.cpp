#include "StdAfx.h"
#include "place2dTextureSNGenerator.h"
#include "ocellaris/IShaderAssembler.h"

place2dTextureSNGenerator::place2dTextureSNGenerator(void)
{
}

place2dTextureSNGenerator::~place2dTextureSNGenerator(void)
{
}

//! ������ ������� false ���� ������ ����������� ���� ��������� �� ���������
bool place2dTextureSNGenerator::BuildShader(
	MObject& node,				//!< ������
	cls::IShaderAssembler* as,		//!< assembler
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	//
	as->InputParameter("Angle", "Angle", cls::PT_FLOAT);
	as->InputParameter("repeatS", "repeatS", cls::PT_FLOAT);
	as->InputParameter("repeatT", "repeatT", cls::PT_FLOAT);
	as->InputParameter("offsetS", "offsetS", cls::PT_FLOAT);
	as->InputParameter("offsetT", "offsetT", cls::PT_FLOAT);
	as->OutputManifold("Q", "dQu", "dQv");
	
	as->Add("extern float s, t, du, dv;");
	as->Add("setxcomp(Q, repeatS * s + offsetS);");
	as->Add("setycomp(Q, repeatT * t + offsetT);");
	as->Add("setzcomp(Q, 0);");
	as->Add("if(angle != 0)");
	as->Add("    Q = rotate(Q, radians(angle), point(0,0,0), point(0,0,1));");
	as->Add("dQu = vector Du(Q)*du;");
	as->Add("dQv = vector Dv(Q)*dv;");

	return true;
}
