#include "StdAfx.h"
#include "PhongSNGenerator.h"
#include "ocellaris/IShaderAssembler.h"

PhongSNGenerator::PhongSNGenerator(void)
{
}

PhongSNGenerator::~PhongSNGenerator(void)
{
}

#define output(STRING) (as->Add(STRING))

//! ������ ������� false ���� ������ ����������� ���� ��������� �� ���������
bool PhongSNGenerator::BuildShader(
	MObject& node,				//!< ������
	cls::enParamType wantedoutputtype, //!< ����� ��� ����� �� ������
	cls::IShaderAssembler* as,		//!< assembler
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	//
	as->InputParameter("color", "SurfaceColor", cls::PT_COLOR);
	as->InputParameter("transparency", "SurfaceOpacity", cls::PT_COLOR);
	as->InputParameter("ambientColor", "ambientColor", cls::PT_COLOR);
	as->InputParameter("incandescence", "incandescence", cls::PT_COLOR);

	as->InputParameter("diffuse", "diffuse", cls::PT_FLOAT);
	as->InputParameter("translucence", "translucence", cls::PT_FLOAT);
	as->InputParameter("translucenceDepth", "translucenceDepth", cls::PT_FLOAT);
	as->InputParameter("translucenceFocus", "translucenceFocus", cls::PT_FLOAT);

	as->InputParameter("roughness",		"roughness", cls::PT_FLOAT);
	as->InputParameter("highlightSize", "highlightSize", cls::PT_FLOAT);
	as->InputParameter("whiteness",		"whiteness", cls::PT_COLOR);
	as->InputParameter("specularColor", "specularColor", cls::PT_COLOR);

	//output("	Ci = SurfaceColor * SurfaceOpacity;");
	//output("	Oi = SurfaceOpacity;");
	as->Include( "#include 'PhongOcs.h'");

	output("	PhongOcs(SurfaceColor, SurfaceOpacity, ambientColor, incandescence, diffuse, translucence, translucenceDepth, translucenceFocus, ");
	output("		roughness, highlightSize, whiteness, specularColor, Oi, Ci);");

	/*/
	output("	normal Nf;														");
	output("	vector V;														");
	output("	color Ia, Id, Itr;												");
	output("	color opacity = 1 - SurfaceOpacity;								");
	output("	normal Nn = normalize(N);										");
	output("	Nf = faceforward( Nn, I, Nn );									");
	output("	V = -normalize(I);												");
	output("	Ia = ambientColor;												");
	output("	Id = 0;															");
	output("	Itr = 0;														");
	output("	illuminance(P, Nf, PI/2)										");
	output("	{																");
	output("		float dot = normalize(L).Nf;								");
	output("		if(dot>0) Itr += Cl*dot;									");
	output("	}																");
	output("	Itr *= diffuse;													");
	output("	Oi = opacity;													");
	output("	Ci = opacity * SurfaceColor * (Ia + Id + Itr + incandescence);	");
	/*/

	return true;
}
