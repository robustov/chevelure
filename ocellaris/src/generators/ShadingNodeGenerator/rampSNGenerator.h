#pragma once

#include "ocellaris/OcellarisExport.h"
#include "ocellaris/IShadingNodeGenerator.h"

//////////////////////////////////////////////////////////////////////
// ����������� ��������� ������ ����������� �� �����������
// ����������� �� ���� ������ IShaderNodeGenerator � �������� ������
// ���
class RampSNGenerator : public cls::IShadingNodeGenerator
{
public:
	RampSNGenerator(void);
	~RampSNGenerator(void);

	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "RampSNGenerator";};

	// ��� ����
	virtual enShadingNodeType getShadingNodeType(
		MObject& node					//!< ������ (shader)
		){return COLOR;};

	bool OnPrepareNode(
		cls::INode& node, 
		cls::IExportContext* context,
		MObject& generatornode
		);

	//! ��������� ��������� 
	//! ��������� ��������� ��������� ��� �������
	virtual bool BuildShader(
		MObject& node,				//!< ������
		cls::enParamType wantedoutputtype, //!< ����� ��� ����� �� ������
		cls::IShaderAssembler* as,		//!< render
		cls::IExportContext* context,	//!< context
		MObject& generatornode			//!< generator node �.� 0
		);
};
