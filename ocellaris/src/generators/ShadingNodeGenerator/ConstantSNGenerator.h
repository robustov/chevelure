#pragma once

#include "ocellaris/OcellarisExport.h"
#include "ocellaris/IShadingNodeGenerator.h"

//////////////////////////////////////////////////////////////////////
// ����������� ��������� ������ ����������� �� �����������
// ����������� �� ���� ������ IShaderNodeGenerator � �������� ������
// ���
class ConstantSNGenerator : public cls::IShadingNodeGenerator
{
public:
	ConstantSNGenerator(void);
	~ConstantSNGenerator(void);

	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "ConstantSNGenerator";};

	// ��� ����
	virtual enShadingNodeType getShadingNodeType(
		MObject& node					//!< ������ (shader)
		){return SURFACESHADER;};

	//! ��������� ��������� 
	//! ��������� ��������� ��������� ��� �������
	virtual bool BuildShader(
		MObject& node,				//!< ������
		cls::IShaderAssembler* as,		//!< render
		cls::IExportContext* context,	//!< context
		MObject& generatornode			//!< generator node �.� 0
		);
};
