#include "StdAfx.h"
#include "ConstantSNGenerator.h"
#include "ocellaris/IShaderAssembler.h"

ConstantSNGenerator::ConstantSNGenerator(void)
{
}

ConstantSNGenerator::~ConstantSNGenerator(void)
{
}

#define output(STRING) (as->Add(STRING))

//! ������ ������� false ���� ������ ����������� ���� ��������� �� ���������
bool ConstantSNGenerator::BuildShader(
	MObject& node,				//!< ������
	cls::IShaderAssembler* as,		//!< assembler
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	//
	as->InputParameter("color", "SurfaceColor", cls::PT_COLOR);
	as->InputParameter("transparency", "SurfaceOpacity", cls::PT_COLOR);
	
	output("	Ci = SurfaceColor * SurfaceOpacity;				");
	output("	Oi = SurfaceOpacity;							");

	return true;
}
