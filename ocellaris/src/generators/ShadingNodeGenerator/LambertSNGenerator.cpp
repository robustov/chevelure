#include "StdAfx.h"
#include "LambertSNGenerator.h"
#include "ocellaris/IShaderAssembler.h"

LambertSNGenerator::LambertSNGenerator(void)
{
}

LambertSNGenerator::~LambertSNGenerator(void)
{
}

#define output(STRING) (as->Add(STRING))

//! ������ ������� false ���� ������ ����������� ���� ��������� �� ���������
bool LambertSNGenerator::BuildShader(
	MObject& node,				//!< ������
	cls::enParamType wantedoutputtype, //!< ����� ��� ����� �� ������
	cls::IShaderAssembler* as,		//!< assembler
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	//
	as->InputParameter("color", "SurfaceColor", cls::PT_COLOR);
	as->InputParameter("transparency", "SurfaceTransparency", cls::PT_COLOR);
	as->InputParameter("ambientColor", "ambientColor", cls::PT_COLOR);
	as->InputParameter("incandescence", "incandescence", cls::PT_COLOR);

	as->InputParameter("diffuse", "diffuse", cls::PT_FLOAT);
	as->InputParameter("translucence", "translucence", cls::PT_FLOAT);
	as->InputParameter("translucenceDepth", "translucenceDepth", cls::PT_FLOAT);
	as->InputParameter("translucenceFocus", "translucenceFocus", cls::PT_FLOAT);

	//output("	Ci = SurfaceColor * SurfaceOpacity;");
	//output("	Oi = SurfaceOpacity;");
	as->Include( "#include 'LambertOcs.h'");

	output("	LambertOcs(SurfaceColor, (1-SurfaceTransparency)*Os, ambientColor, incandescence, diffuse, translucence, translucenceDepth, translucenceFocus, Oi, Ci);");

	/*/
	output("	normal Nf;														");
	output("	vector V;														");
	output("	color Ia, Id, Itr;												");
	output("	color opacity = 1 - SurfaceOpacity;								");
	output("	normal Nn = normalize(N);										");
	output("	Nf = faceforward( Nn, I, Nn );									");
	output("	V = -normalize(I);												");
	output("	Ia = ambientColor;												");
	output("	Id = 0;															");
	output("	Itr = 0;														");
	output("	illuminance(P, Nf, PI/2)										");
	output("	{																");
	output("		float dot = normalize(L).Nf;								");
	output("		if(dot>0) Itr += Cl*dot;									");
	output("	}																");
	output("	Itr *= diffuse;													");
	output("	Oi = opacity;													");
	output("	Ci = opacity * SurfaceColor * (Ia + Id + Itr + incandescence);	");
	/*/

	return true;
}
