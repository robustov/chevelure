#include "StdAfx.h"
#include "SpotLightSNGenerator.h"
#include "ocellaris/IShaderAssembler.h"

SpotLightSNGenerator::SpotLightSNGenerator(void)
{
}

SpotLightSNGenerator::~SpotLightSNGenerator(void)
{
}

#define output(STRING) (as->Add(STRING))

//! ����������� ���� � �������� 
bool SpotLightSNGenerator::OnPrepareNode(
	cls::INode& node, 
	cls::IExportContext* context,
	MObject& generatornode
	)
{
	return true;
};

//! ������ ������� false ���� ������ ����������� ���� ��������� �� ���������
bool SpotLightSNGenerator::BuildShader(
	MObject& node,				//!< ������
	cls::enParamType wantedoutputtype, //!< ����� ��� ����� �� ������
	cls::IShaderAssembler* as,		//!< assembler
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	//
	as->InputParameter("coneAngle", "ConeAngle", cls::PT_FLOAT, cls::PI_CONSTANT);
	as->InputParameter("penumbraAngle", "PenumbraAngle", cls::PT_FLOAT, cls::PI_CONSTANT);
	as->InputParameter("color", "Color", cls::PT_COLOR);
	as->InputParameter("intensity", "kL", cls::PT_FLOAT);

	as->Include( "#include 'SpotLigthOcs.h'");
	as->Include( "#include 'ul_pxslUtil.h'");

	output("	color Cout=0;");
	output("	SpotLigthOcs(ConeAngle, PenumbraAngle, Color, kL, Cout);");

	cls::P<bool> useDepthMapShadows = ocExport_GetAttrValue(node, "useDepthMapShadows");
	if( !useDepthMapShadows.empty() && useDepthMapShadows.data())
	{
		ocExport_AddAttrType(node, "shadowSamples", cls::PT_FLOAT, cls::P<float>(16));
		ocExport_AddAttrType(node, "shadowBlur", cls::PT_BOOL, cls::P<bool>(false));

		as->ExternParameter( "shadowname", cls::PS(""), cls::PT_STRING, cls::PI_CONSTANT);
		as->InputParameter("shadowSamples", "ShadowSamples", cls::PT_FLOAT, cls::PI_CONSTANT);
		as->InputParameter("shadowBlur", "ShadowBlur", cls::PT_BOOL, cls::PI_CONSTANT);
		as->InputParameter("dmapBias", "ShadowBias", cls::PT_FLOAT, cls::PI_CONSTANT);
		as->InputParameter("shadowColor", "ShadowColor", cls::PT_COLOR);

		output("	color _shadow=1;");
		output("	SpotLigthShadowMapOcs(shadowname, ShadowSamples, ShadowBlur, ShadowBias, _shadow);");
		output("	Cout = ul_pxslCmix(Cout, ShadowColor, _shadow);");
	}

	output("	Cl=Cout;");

	return true;
}
