#include "StdAfx.h"
#include "NoiseSNGenerator.h"
#include "ocellaris/IShaderAssembler.h"
//#include <multimap>

NoiseSNGenerator::NoiseSNGenerator(void)
{
}

NoiseSNGenerator::~NoiseSNGenerator(void)
{
}

#define output(STRING) (as->Add(STRING))

//! ����������� ���� � �������� 
bool NoiseSNGenerator::OnPrepareNode(
	cls::INode& node, 
	cls::IExportContext* context,
	MObject& generatornode
	)
{
	return true;
};

struct RampKey
{
	std::string keyname;
	std::string valname;
};

//! ������ ������� false ���� ������ ����������� ���� ��������� �� ���������
bool NoiseSNGenerator::BuildShader(
	MObject& node,				//!< ������
	cls::enParamType wantedoutputtype, //!< ����� ��� ����� �� ������
	cls::IShaderAssembler* as,		//!< assembler
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	as->OutputParameter("result", cls::PT_COLOR);

	cls::P<int> type = ocExport_GetAttrValue(node, "type");
	if( type.empty()) 
		return false;
	cls::P<int> interpolation = ocExport_GetAttrValue(node, "interpolation");
	if( interpolation.empty()) 
		return false;

	if( type.data()!=0 &&
		type.data()!=1 )
		return false;

	MStatus stat;
	MFnDependencyNode dn( node);
	MPlug plug = dn.findPlug("colorEntryList");

	std::multimap< float, RampKey> rampkeys;
	for( int s=0; s<(int)plug.numElements(); s++)
	{
		MPlug elem = plug.elementByPhysicalIndex(s);

		MPlug elem1 = elem.child(0);
		double position;
		elem1.getValue(position);
		MPlug elem2 = elem.child(1);

		char buf[24];
		RampKey rk;
		rk.keyname = MFnAttribute( elem1.attribute()).name().asChar();
		rk.keyname += _itoa(s, buf, 10);
		rk.valname = MFnAttribute( elem2.attribute()).name().asChar();
		rk.valname += _itoa(s, buf, 10);

		as->InputParameter(elem1, rk.keyname.c_str(), cls::PT_FLOAT, cls::PI_CONSTANT);
		as->InputParameter(elem2, rk.valname.c_str(), cls::PT_COLOR, cls::PI_CONSTANT);

		cls::P<float> value = ocExport_GetAttrValue(elem1);
		if( value.empty())
			continue;

		rampkeys.insert( std::pair<float, RampKey>(value.data(), rk) );
	}


	ocExport_AddAttrType(node, "uvSetName", cls::PT_STRING, cls::PS(""));
	ocExport_AddAttrType(node, "uvScale", cls::PT_FLOAT, cls::P<float>(1));
	ocExport_AddAttrType(node, "tile", cls::PT_BOOL, cls::P<bool>(1));

	cls::PS uvSetName_ = ocExport_GetAttrValue(node, "uvSetName");
	std::string uvSetName = uvSetName_.data();
	output("	point Q;");
	if(uvSetName.empty() || uvSetName=="st")
	{
		output("	Q = point(s, t, 0);");
	}
	else
	{
		as->ExternParameter( ("u_"+uvSetName).c_str(), cls::P<float>(0), cls::PT_FLOAT);
		as->ExternParameter( ("v_"+uvSetName).c_str(), cls::P<float>(0), cls::PT_FLOAT);
		as->AddV("	extern float u_%s, v_%s;", uvSetName.c_str(), uvSetName.c_str());
		as->AddV("	Q = point(u_%s, v_%s, 0);", uvSetName.c_str(), uvSetName.c_str());
	}

	if( type.data()==0)
		output("	float param = 1-ycomp(Q);");
	if( type.data()==1)
		output("	float param = xcomp(Q);");

	as->InputParameter("uvScale", "uvScale", cls::PT_FLOAT, cls::PI_CONSTANT);
	output("	param = param*uvScale;");

	cls::P<bool> bTile = ocExport_GetAttrValue(node, "tile");
	if( !bTile.empty() && bTile.data())
	{
		output("	param = param - floor(param);");
	}
	output("	param = clamp(param, 0, 1);");

	if( interpolation.data()==0)
	{
		// ��������
		std::multimap< float, RampKey>::iterator it = rampkeys.begin();
		for( ; it != rampkeys.end(); it++)
		{
			RampKey& rk = it->second;
			// ��� ������������
			if( it == rampkeys.begin())
				as->AddV("	result = %s;", rk.valname.c_str());
			else
			{
				as->AddV("	if(param>%s)", rk.keyname.c_str());
				as->AddV("		result = %s;", rk.valname.c_str());
			}
		}
	}
	else
	{
		std::string keys, values;
		std::multimap< float, RampKey>::iterator it = rampkeys.begin();
		for( ; it != rampkeys.end(); it++)
		{
			RampKey& rk = it->second;
			// � ������ � � �����
			if( it == rampkeys.begin())
			{
				keys += "0, 0";
				values += rk.valname+", "+rk.valname;
			}

			keys += ", ";
			values += ", ";
			keys += rk.keyname;
			values += rk.valname;

			// � ������ � � �����
			std::multimap< float, RampKey>::iterator ittest = it; ittest++;
			if( ittest==rampkeys.end())
			{
				keys += ", 1, 1";
				values += ", "+rk.valname+", "+rk.valname;
			}
		}

		std::string interp = "catrom";
		if(interpolation.data()==1)
			interp = "linear";
		output("	float k;");
		as->AddV("	k = float spline( \"solve%s\", param, ", interp.c_str());
		as->AddV("		%s);", keys.c_str());
		as->AddV("	result = color spline( \"%s\", k, ", interp.c_str());
		as->AddV("		%s);", values.c_str());
	}




/*/
/*/



/*/
	//
	as->InputParameter("coneAngle", "ConeAngle", cls::PT_FLOAT, cls::PI_CONSTANT);
	as->InputParameter("penumbraAngle", "PenumbraAngle", cls::PT_FLOAT, cls::PI_CONSTANT);
	as->InputParameter("color", "Color", cls::PT_COLOR);
	as->InputParameter("intensity", "kL", cls::PT_FLOAT);

	as->Include( "#include 'SpotLigthOcs.h'");
//	as->Include( "#include 'pxslUtil.h'");

	output("	color Cout=0;");
	output("	SpotLigthOcs(ConeAngle, PenumbraAngle, Color, kL, Cout);");

	cls::P<bool> useDepthMapShadows = ocExport_GetAttrValue(node, "useDepthMapShadows");
	if( !useDepthMapShadows.empty() && useDepthMapShadows.data())
	{
		ocExport_AddAttrType(node, "shadowSamples", cls::PT_FLOAT, cls::P<float>(1));
		ocExport_AddAttrType(node, "shadowBlur", cls::PT_BOOL, cls::P<bool>(false));

		as->ExternParameter( "shadowname", cls::PS(""), cls::PT_STRING, cls::PI_CONSTANT);
		as->InputParameter("shadowSamples", "ShadowSamples", cls::PT_FLOAT, cls::PI_CONSTANT);
		as->InputParameter("shadowBlur", "ShadowBlur", cls::PT_BOOL, cls::PI_CONSTANT);
		as->InputParameter("dmapBias", "ShadowBias", cls::PT_FLOAT, cls::PI_CONSTANT);
		as->InputParameter("shadowColor", "ShadowColor", cls::PT_COLOR);

		output("	color _shadow=1;");
		output("	SpotLigthShadowMapOcs(shadowname, ShadowSamples, ShadowBlur, ShadowBias, _shadow);");
		output("	Cout = pxslCmix(Cout, ShadowColor, _shadow);");
	}

	output("	Cl=Cout;");

/*/
	return true;
}
