#pragma once

#include "ocellaris/OcellarisExport.h"
#include "ocellaris/IShadingNodeGenerator.h"

//////////////////////////////////////////////////////////////////////
// ����������� ��������� ������ ����������� �� �����������
// ����������� �� ���� ������ IShaderNodeGenerator � �������� ������
// ���
class LambertSNGenerator : public cls::IShadingNodeGenerator
{
public:
	LambertSNGenerator(void);
	~LambertSNGenerator(void);

	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "LambertSNGenerator";};

	// ��� ����
	virtual enShadingNodeType getShadingNodeType(
		MObject& node					//!< ������ (shader)
		){return SURFACESHADER;};

	//! ��������� ��������� 
	//! ��������� ��������� ��������� ��� �������
	virtual bool BuildShader(
		MObject& node,				//!< ������
		cls::enParamType wantedoutputtype, //!< ����� ��� ����� �� ������
		cls::IShaderAssembler* as,		//!< render
		cls::IExportContext* context,	//!< context
		MObject& generatornode			//!< generator node �.� 0
		);
};
