#pragma once

#include "ocellaris/OcellarisExport.h"
#include "ocellaris/IShadingNodeGenerator.h"

//////////////////////////////////////////////////////////////////////
// ����������� ��������� ������ ����������� �� �����������
// ����������� �� ���� ������ IShaderNodeGenerator � �������� ������
// ���
class PhongSNGenerator : public cls::IShadingNodeGenerator
{
public:
	PhongSNGenerator(void);
	~PhongSNGenerator(void);

	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "PhongSNGenerator";};

	// ��� ����
	virtual enShadingNodeType getShadingNodeType(
		MObject& node					//!< ������ (shader)
		){return SURFACESHADER;};

	//! ��������� ��������� 
	//! ��������� ��������� ��������� ��� �������
	virtual bool BuildShader(
		MObject& node,				//!< ������
		cls::enParamType wantedoutputtype, //!< ����� ��� ����� �� ������
		cls::IShaderAssembler* as,		//!< render
		cls::IExportContext* context,	//!< context
		MObject& generatornode			//!< generator node �.� 0
		);
};
