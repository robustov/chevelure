#include "stdafx.h"
#include "SkipTransformGenerator.h"
#include <maya/MFnMesh.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MPlug.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MIntArray.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnTransform.h>
#include <maya/MTransformationMatrix.h>
#include <string>
#include <mathNmaya/mathNmaya.h>
#include "math/Matrix.h"

SkipTransformGenerator skiptransformgenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl SkipTransform()
	{
		return &skiptransformgenerator;
	}
}

SkipTransformGenerator::SkipTransformGenerator()
{
}

//! render ����� ���� = NULL
void SkipTransformGenerator::OnTransformStart(
	cls::INode& node,					//!< ������
	cls::IRender* render,				//!< render
	const Math::Matrix4f& curtransform,	//!< ������� ���������
	Math::Matrix4f& newtransform,		//!< ����� ���������
	cls::IExportContext* context,		
	MObject& generatornode				//!< generator node �.� 0
	)
{
	newtransform = curtransform;
}

//! ��������� ����� { AppendTransform ��� SetTransform }
void SkipTransformGenerator::OnTransform(
	cls::INode& node,					//!< ������
	cls::IRender* render,				//!< render
	cls::IExportContext* context,
	MObject& generatornode				//!< generator node �.� 0
	)
{
}

void SkipTransformGenerator::OnTransformEnd(
	cls::INode& node,					//!< ������
	cls::IRender* render,				//!< render
	cls::IExportContext* context,
	MObject& generatornode				//!< generator node �.� 0
	)
{
}

