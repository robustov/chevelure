#pragma once

#include "ocellaris/ITransformGenerator.h"
//! @ingroup implement_group
//! \brief ����������� ��������� ������������� ����� ������� �� exclusiveMatrix
//! 
struct SkipTransformGenerator : public cls::ITransformGenerator
{
//	int level;
//	Math::Matrix4f trans;

	SkipTransformGenerator();
public:
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "SkipTransformGenerator";};

	//! ��������� ����� render->PushTransform(); 
	//! ������� ����� �������
	virtual void OnTransformStart(
		cls::INode& node,					//!< ������
		cls::IRender* render,				//!< render
		const Math::Matrix4f& curtransform,	//!< ������� ���������
		Math::Matrix4f& newtransform,		//!< ����� ���������
		cls::IExportContext* context,		
		MObject& generatornode				//!< generator node �.� 0
		);
	//! ��������� ����� { AppendTransform ��� SetTransform }
	virtual void OnTransform(
		cls::INode& node,					//!< ������
		cls::IRender* render,				//!< render
		cls::IExportContext* context,
		MObject& generatornode				//!< generator node �.� 0
		);
	//! ��������� ����� render->PopAttributes()
	virtual void OnTransformEnd(
		cls::INode& node,					//!< ������
		cls::IRender* render,				//!< render
		cls::IExportContext* context,
		MObject& generatornode				//!< generator node �.� 0
		);
};
