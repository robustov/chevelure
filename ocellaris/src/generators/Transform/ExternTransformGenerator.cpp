#include "stdafx.h"
#include "ExternTransformGenerator.h"
#include <maya/MFnMesh.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MPlug.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MIntArray.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnTransform.h>
#include <maya/MTransformationMatrix.h>
#include <string>
#include <mathNmaya/mathNmaya.h>
#include "math/Matrix.h"
#include "ocellaris/OcellarisExport.h"

ExternTransformGenerator externtransformgenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl ExternTransform()
	{
		return &externtransformgenerator;
	}
}

ExternTransformGenerator::ExternTransformGenerator()
{
}

// ��� �������� � ����
void ExternTransformGenerator::OnLoadToMaya(
	MObject& generatornode
	)
{
	MObject attr;
	attr = ocExport_AddAttrType(generatornode, "worldTransform", cls::PT_BOOL, cls::P<bool>(true));
}
//! render ����� ���� = NULL
void ExternTransformGenerator::OnTransformStart(
	cls::INode& node,					//!< ������
	cls::IRender* render,				//!< render
	const Math::Matrix4f& curtransform,	//!< ������� ���������
	Math::Matrix4f& newtransform,		//!< ����� ���������
	cls::IExportContext* context,		
	MObject& generatornode				//!< generator node �.� 0
	)
{
	MDagPath path = node.getPath();

	cls::P<bool> _bWorld = ocExport_GetAttrValue(generatornode, "worldTransform");
	bool bWorld = true;
	if( !_bWorld.empty())
		bWorld = _bWorld.data();

	Math::Matrix4f m;
	if( bWorld)
	{
		// WORLD
		MMatrix tm = path.inclusiveMatrix();
		copy( m, tm);
		newtransform = curtransform;
	}
	else
	{
		MObject obj = node.getObject();
		MFnTransform transform(obj);
		MTransformationMatrix tm = transform.transformation();
		copy( m, tm.asMatrix());
		newtransform = m*curtransform;
	}

	MString name = path.partialPathName();
	if( render)
	{
		render->PushTransform();
	}
}

//! ��������� ����� { AppendTransform ��� SetTransform }
void ExternTransformGenerator::OnTransform(
	cls::INode& node,					//!< ������
	cls::IRender* render,				//!< render
	cls::IExportContext* context,
	MObject& generatornode				//!< generator node �.� 0
	)
{
	MDagPath path = node.getPath();

	cls::P<bool> _bWorld = ocExport_GetAttrValue(generatornode, "worldTransform");
	bool bWorld = true;
	if( !_bWorld.empty())
		bWorld = _bWorld.data();

	Math::Matrix4f m;
	if( bWorld)
	{
		// WORLD
		MMatrix tm = path.inclusiveMatrix();
		copy( m, tm);
	}
	else
	{
		MObject obj = node.getObject();
		MFnTransform transform(obj);
		MTransformationMatrix tm = transform.transformation();
		copy( m, tm.asMatrix());
	}

	MString name = path.partialPathName();
	if( render)
	{
		std::string globalattr = name.asChar();
		size_t n = globalattr.find(':');
		if( n!=globalattr.npos) globalattr = globalattr.substr(n+1);
		globalattr = "ExternTransform::"+globalattr;

		render->GlobalAttribute(globalattr.c_str(), cls::P<Math::Matrix4f>(m));
		
//render->PushAttributes();
		render->PushAttributes();
		render->ParameterFromAttribute("transform", globalattr.c_str());
		if( bWorld)
			render->SetTransform();
		else
			render->AppendTransform();
		render->PopAttributes();
	}
}

void ExternTransformGenerator::OnTransformEnd(
	cls::INode& node,					//!< ������
	cls::IRender* render,				//!< render
	cls::IExportContext* context,
	MObject& generatornode				//!< generator node �.� 0
	)
{
	if( render)
	{
		render->PopTransform();
//render->PopAttributes();
	}
}

