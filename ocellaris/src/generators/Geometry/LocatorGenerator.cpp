#include "stdafx.h"
#include "LocatorGenerator.h"
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MItCurveCV.h>
#include <maya/MPlug.h>
#include <maya/MPoint.h>
#include <maya/MPointArray.h>
#include <maya/MFnNurbsSurface.h>
#include <string>

LocatorGenerator locatorGenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl GetLocatorGenerator()
	{
		return &locatorGenerator;
	}
}

bool LocatorGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();
	MStatus stat;

	if( render)
	{
		render->Sphere(1);
	}

	return true;
}
