#pragma once
#include "ocellaris/IGeometryGenerator.h"

//! @ingroup implement_group
//! \brief ����������� ����� ������ � ���������� ��������
//! 
class SkinClusterGenerator : public cls::IGeometryGenerator
{
public:
	SkinClusterGenerator(void);
	~SkinClusterGenerator(void);

	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "SkinClusterGenerator";};

	virtual bool OnGeometry(
		cls::INode& node,				//!< ������ 
		cls::IRender* render,			//!< render
		Math::Box3f& box,				//!< box 
		cls::IExportContext* context,	//!< context
		MObject& generatornode			//!< generator node �.� 0
		);
};
