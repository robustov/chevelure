#pragma once

#include "ocellaris/ILightGenerator.h"

#include "ocellaris/OcellarisExport.h"

//! @ingroup implement_group
//! \brief ����������� ��������� ��� ��������� �����
//! ���� ������� �� �����
struct SpotLightGenerator : public cls::ILightGenerator
{
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "SpotLightGenerator";};

	// ��� �������� � ����
	virtual void OnLoadToMaya(
		MObject& generatornode
		);

	virtual bool OnPrepareNode(
		cls::INode& node, 
		cls::IExportContext* context,
		MObject& generatornode
		);

	virtual bool OnLight(
		cls::INode& node,				//!< ������ 
		cls::IRender* render,			//!< render
		Math::Box3f& box,				//!< box 
		cls::IExportContext* context,	//!< context
		MObject& generatornode			//!< generator node �.� 0
		);

public:
	// ������������� ������ ���� ��� �� ������������
	std::string BuildShader(
		MObject& node, 
		cls::IRender* render,
		cls::IExportContext* context,
		cls::IShadingNodeGenerator::enShadingNodeType shtype,
		// ������ ���������� ������� ���� ��������
		std::vector< std::string >& externvariables
		);

};
