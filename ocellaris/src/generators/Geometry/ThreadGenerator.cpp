#include "stdafx.h"
#include "ThreadGenerator.h"
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MItCurveCV.h>
#include <maya/MPlug.h>
#include <maya/MPoint.h>
#include <maya/MPointArray.h>
#include <maya/MFnPointArrayData.h>
#include <string>
#include "ocellaris/OcellarisExport.h"
#include "mathNmaya/mathNmaya.h"
#include "math/perlinNoise.h"

int setIndexes( int start, int end, int *indexes, int currentIndex );

ThreadGenerator threadgenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl ThreadGenerator()
	{
		return &threadgenerator;
	}
}

// ��� �������� � ����
void ThreadGenerator::OnLoadToMaya(
	MObject& generatornode
	)
{
	MObject attr;
//	attr = ocExport_AddAttrType(generatornode, "baseWidth",   cls::PT_FLOAT, cls::P<float>(1));
//	attr = ocExport_AddAttrType(generatornode, "tipWidth",    cls::PT_FLOAT, cls::P<float>(1));
	// ������ ����
	attr = ocExport_AddAttrType(generatornode, "width",		cls::PT_FLOAT, cls::P<float>(0.04f));
	attr = ocExport_AddAttrType(generatornode, "widthJitter", cls::PT_FLOAT, cls::P<float>(1.f));
	attr = ocExport_AddAttrType(generatornode, "widthJitterStep", cls::PT_FLOAT, cls::P<float>(0.5f));
	// ������ �������
	attr = ocExport_AddAttrType(generatornode, "fiberWidth", cls::PT_FLOAT, cls::P<float>(0.1f));
//	attr = ocExport_AddAttrType(generatornode, "fiberWidthJitter", cls::PT_FLOAT, cls::P<float>(0.1f));
//	attr = ocExport_AddAttrType(generatornode, "fiberWidthFreq", cls::PT_FLOAT, cls::P<float>(0.5f));
	// ��� ������
	attr = ocExport_AddAttrType(generatornode, "twistStep", cls::PT_FLOAT, cls::P<float>(0.7f));
	attr = ocExport_AddAttrType(generatornode, "vertsPerTwistStep", cls::PT_INT, cls::P<int>(10));
	// fiberCount
	attr = ocExport_AddAttrType(generatornode, "fiberCount", cls::PT_INT, cls::P<int>(4));
	attr = ocExport_AddAttrType(generatornode, "fiberTwist", cls::PT_FLOAT, cls::P<float>(1));
//	attr = ocExport_AddAttrType(generatornode, "subdive",    cls::PT_INT, cls::P<int>(30));


	attr = ocExport_AddAttrType(generatornode, "hairsCount", cls::PT_INT, cls::P<int>(70));
	attr = ocExport_AddAttrType(generatornode, "hairVerts", cls::PT_INT, cls::P<int>(4));
	attr = ocExport_AddAttrType(generatornode, "hairLenght", cls::PT_FLOAT, cls::P<float>(0.1f));
	attr = ocExport_AddAttrType(generatornode, "hairScraggle", cls::PT_FLOAT, cls::P<float>(1.f));
	attr = ocExport_AddAttrType(generatornode, "hairWidth", cls::PT_FLOAT, cls::P<float>(0.004f));
	attr = ocExport_AddAttrType(generatornode, "hairLenghtJitter", cls::PT_FLOAT, cls::P<float>(0.f));

	attr = ocExport_AddAttrType(generatornode, "scale", cls::PT_FLOAT, cls::P<float>(1));
}

bool ThreadGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MStatus stat;
	float width = 0.1f;
	float fiberWidth = 0.03f;
	float widthJitter = 1;
	float widthJitterStep = 0.5;
//		float fiberWidthJitter = 0.5;
//		float fiberWidthFreq = 1;
	float twistStep = 0.7f;
	int fiberCount = 4;
//	int subdive = 10;

	int seed = rand();
	MObject attr = ocExport_AddAttrType(inode.getObject(), "thread_seed", cls::PT_INT, cls::P<int>(seed));
	cls::P<int> _seed = ocExport_GetAttrValue(inode.getObject(), "thread_seed");
	render->Parameter("@Thread::id", _seed);

	float box_offset = width+fiberWidth;
	if( !generatornode.isNull())
	{
		cls::P<float> _width = ocExport_GetAttrValue(generatornode, "width");
		cls::P<float> _fiberWidth = ocExport_GetAttrValue(generatornode, "fiberWidth");
		cls::P<float> _widthJitter = ocExport_GetAttrValue(generatornode, "widthJitter");
		cls::P<float> _widthJitterStep = ocExport_GetAttrValue(generatornode, "widthJitterStep");
//			cls::P<float> _fiberWidthJitter = ocExport_GetAttrValue(generatornode, "fiberWidthJitter");
//			cls::P<float> _fiberWidthFreq = ocExport_GetAttrValue(generatornode, "fiberWidthFreq");
		cls::P<float> _twistStep = ocExport_GetAttrValue(generatornode, "twistStep");
		cls::P<int> _fiberCount = ocExport_GetAttrValue(generatornode, "fiberCount");
//		cls::P<int> _subdive = ocExport_GetAttrValue(generatornode, "subdive");
		cls::P<float> _fiberTwist = ocExport_GetAttrValue(generatornode, "fiberTwist");

		cls::P<int> vertsPerTwistStep = ocExport_GetAttrValue(generatornode, "vertsPerTwistStep");
		cls::P<float> scale = ocExport_GetAttrValue(generatornode, "scale");


		
		render->Parameter("Thread::scale", scale);
		render->Parameter("Thread::width", _width);
		render->Parameter("Thread::fiberWidth", _fiberWidth);
		render->Parameter("Thread::widthJitter", _widthJitter);
		render->Parameter("Thread::widthJitterStep", _widthJitterStep);
		render->Parameter("Thread::twistStep", _twistStep);
		render->Parameter("Thread::fiberCount", _fiberCount);
//		render->Parameter("Thread::subdive", _subdive);
		render->Parameter("Thread::fiberTwist", _fiberTwist);
		render->Parameter("@Thread::vertpertwist", vertsPerTwistStep);

		cls::P<int> hairsCount = ocExport_GetAttrValue(generatornode, "hairsCount");
		cls::P<int> hairVerts = ocExport_GetAttrValue(generatornode, "hairVerts");
		cls::P<float> hairLenght = ocExport_GetAttrValue(generatornode, "hairLenght");
		cls::P<float> hairScraggle = ocExport_GetAttrValue(generatornode, "hairScraggle");
		cls::P<float> hairWidth = ocExport_GetAttrValue(generatornode, "hairWidth");
		cls::P<float> hairLenghtJitter = ocExport_GetAttrValue(generatornode, "hairLenghtJitter");

		render->Parameter("Thread::hairsCount", hairsCount);
		render->Parameter("Thread::hairVerts", hairVerts);
		render->Parameter("Thread::hairLenght", hairLenght);
		render->Parameter("Thread::hairScraggle", hairScraggle);
		render->Parameter("Thread::hairWidth", hairWidth);
		render->Parameter("Thread::hairLenghtJitter", hairLenghtJitter);

		box_offset = 0;
		if( !_width.empty())
			box_offset += _width.data();
		if( !_fiberWidth.empty())
			box_offset += _fiberWidth.data();
		if( !hairLenght.empty())
			box_offset += hairLenght.data();
			
	}

	MObject node = inode.getObject();
	MFnNurbsCurve nurbs( node, &stat );
	if( stat) 
	{
		MDoubleArray knots;
		nurbs.getKnots( knots);
		MPointArray array;
		nurbs.getCVs( array);
		int l = array.length();
		cls::PA<Math::Vec3f> CVs(cls::PI_VERTEX, l, cls::PT_POINT);
		cls::PA<Math::Vec3f> N(cls::PI_VERTEX, l, cls::PT_NORMAL);
		for(int i=0; i<l; i++) 
		{
			MPoint p = array[i];
			CVs[i] = Math::Vec3f((float)p.x, (float)p.y, (float)p.z);
			box.insert( CVs[i], box_offset);
			MVector n = nurbs.normal(knots[i], MSpace::kObject, &stat);
			if(!stat) n = MVector(1, 0, 0);
			n.normalize();
			::copy( N[i], n);
		}

		// freeze
		{
			MObject freezeval;
			nurbs.findPlug("Pfreeze").getValue(freezeval);
			MFnPointArrayData data(freezeval);
			if( data.length() == array.length())
			{
				cls::PA<Math::Vec3f> CVsfreeze(cls::PI_VERTEX, l, cls::PT_POINT);
				for(int i=0; i<l; i++) 
				{
					MPoint p = data[i];
					CVsfreeze[i] = Math::Vec3f((float)p.x, (float)p.y, (float)p.z);
				}
				render->Parameter("#Pfreeze", cls::PT_POINT, CVsfreeze);
			}
		}

		render->Parameter("P", cls::PT_POINT, CVs);
	//		render->Parameter("#N", N);
		render->RenderCall("Thread");
	}

	MFnMesh mesh( node, &stat );
	if( stat) 
	{
		MPointArray points;
		MFnPointArrayData pointsfreeze;
		mesh.getPoints( points);

		// freeze
		{
			MObject freezeval;
			mesh.findPlug("Pfreeze").getValue(freezeval);
			pointsfreeze.setObject(freezeval);
		}

		cls::PA< Math::Vec3f> P, Pfreeze;
		P.reserve(points.length());
		Pfreeze.reserve(points.length());
//		cls::PA< Math::Vec3f> PolyN, N;
//		N.reserve(points.length());
//		PolyN.reserve(points.length());
		int startX = 0;

		int numPoints = points.length()/2;
		int numSegments = numPoints - 1;
		std::vector<int> indexes(numSegments+1);
		int gi = 4;
		setIndexes( 0, numSegments, &indexes[0], gi );
		indexes[0] = 0;
		indexes[numSegments] = 1;

		for( int i = 0; i < numPoints; i++ )
		{
			int in = indexes[i];
			P.push_back( Math::Vec3f( (float)points[in].x, (float)points[in].y, (float)points[in].z ) );
			box.insert( P.back(), box_offset);
			Pfreeze.push_back(P.back());
			if( in<(int)pointsfreeze.length())
				::copy( Pfreeze.back(), pointsfreeze[in]);
		}

		render->Parameter("P", cls::PT_POINT, P);
		render->Parameter("#Pfreeze", cls::PT_POINT, Pfreeze);
//		render->Parameter("#N", N);
		render->RenderCall("Thread");
	}


	return true;
}

int setIndexes( int start, int end, int *indexes, int currentIndex )
{
	int delta = end - start;
	int index = start + delta/2;

	indexes[ index ] = currentIndex;
	currentIndex += 2;

	if ( delta  == 3 )
	{
		indexes[ index+1 ] = currentIndex;
		currentIndex += 2;

		return currentIndex;
	}

	if ( delta  == 2 ) return currentIndex;

	currentIndex = setIndexes( start, index, indexes, currentIndex );
	currentIndex = setIndexes( index, end, indexes, currentIndex );

	return currentIndex;
}
