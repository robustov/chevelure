#include "stdafx.h"
#include "SpotLightGenerator.h"
#include <maya/MFnLight.h>

#include "ocellaris/shaderAssemblerImpl.h"
#include "Util/misc_create_directory.h"
#include "shading/ShadingGraphAssembler.h"
#include "mathNmaya/mathNmaya.h"

/*/
#include <maya/MDagPath.h>
#include <maya/MMatrix.h>
#include "mathNmaya/mathNmaya.h"
#include "ocellaris/OcellarisExport.h"
#include "ocellaris/renderCacheImpl.h"
#include "Util/misc_create_directory.h"
#include "./SlimCommands.h"

#include <maya/MAngle.h>//
#include <maya/MPlugArray.h>//
/*/

SpotLightGenerator spotlightgenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl SpotLight()
	{
		return &spotlightgenerator;
	}
}

// ��� �������� � ����
void SpotLightGenerator::OnLoadToMaya(
	MObject& generatornode
	)
{
}

//! ����������� ���� � �������� 
bool SpotLightGenerator::OnPrepareNode(
	cls::INode& node, 
	cls::IExportContext* context,
	MObject& generatornode
	)
{
	MObject obj = node.getObject();

	std::vector< std::string > externvariables;

	// ������������� ������ � ����� Shader
	std::string shadername = BuildShader(
		obj, 
		NULL, 
		context, 
		cls::IShadingNodeGenerator::LIGHT, 
		externvariables
		);

	cls::IRender* contextstream = context->objectContext(obj, this);
	contextstream->Parameter("shadername", shadername);
	contextstream->Parameter("externvariables", externvariables);

	return true;
};

bool SpotLightGenerator::OnLight(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject obj = inode.getObject();

	MDagPath path = inode.getPath();
	MTransformationMatrix xform( path.inclusiveMatrix()	);
	double scale[] = { 1, 1, -1 };
	xform.setScale(	scale, MSpace::kTransform );
	MMatrix _matr = xform.asMatrix();

	Math::Matrix4f matr; ::copy(matr, _matr);


	cls::IRender* contextstream = context->objectContext(obj, this);
	if( !contextstream)
		return false;

	std::string shadername;
	if( !contextstream->GetParameter("shadername", shadername))
		return false;
	if( shadername.empty())
		return false;
	
	render->PushTransform();
	render->PushAttributes();
	render->SetTransform(matr);
	render->PopAttributes();

	render->PushAttributes();

	cls::P<bool> useDepthMapShadows = ocExport_GetAttrValue(obj, "useDepthMapShadows");
	if( !useDepthMapShadows.empty() && useDepthMapShadows.data())
	{
		// ��� �������� � ��� �������� ���� ���� ��������
		std::string light_name = inode.getName();
		Util::correctFileName(light_name, '_');

		std::string attrname = light_name;
		attrname += "_shadowName";
		render->ParameterFromAttribute( "shadowname", attrname.c_str());

//		cls::Param shadowColor = ocExport_GetAttrValue(node, "shadowColor");
//		render->Parameter( "shadowcolor", shadowColor);
	}

	// �������� ��������
	std::vector< std::string > externvariables;
	contextstream->GetParameter("externvariables", externvariables);
	for(int i=0; i<(int)externvariables.size()/2; i++)
	{
		std::string name = externvariables[i*2+0];
		MPlug plug;
		plugFromName(externvariables[i*2+1].c_str(), plug);
		cls::Param p = ocExport_GetAttrValue(plug);
		render->Parameter(name.c_str(), p);
	}

	render->Parameter("#lightshader", shadername.c_str());
	render->Light();
	render->PopAttributes();
	render->PopTransform();

	return true;
}

// ������������� ������ ���� ��� �� ������������
std::string SpotLightGenerator::BuildShader(
	MObject& node, 
	cls::IRender* render,
	cls::IExportContext* context,
	cls::IShadingNodeGenerator::enShadingNodeType shtype,
	// ������ ���������� ������� ���� ��������
	std::vector< std::string >& externvariables
	)
{
	// ��������� ������ IShaderNodeGenerator

	// ������ IShadingNodeGenerator ��� �������
	// ��������� BuildShader
	// IShaderNodeGenerator::BuildShader ������ ��������� ����� ����������� ���������, 
	// ������� ���� � ������� ��������� ������ ������ - ��������� � ���� IShaderNodeGenerator::BuildShader 
	// ��� ���� ������ ������� - ������ externAttriute � ������� � ������ ����� ��������� ��������� � ������
	// (� ��������� � ���������)
//	IShadingNodeGenerator

	std::string name = MFnDependencyNode(node).name().asChar();
	Util::correctFileName( name, '_', true);
	cls::shaderAssemblerImpl as;

	std::map<std::string, cls::Param > externParams;
	std::map<std::string, std::string > externvarplug;
	std::string text = as.BuildGraph(cls::IShadingNodeGenerator::LIGHT, node, name.c_str(), context, externParams, externvarplug);
	if( text.empty())
	{
		return "";
	}

	// ������ ���������� ������� ���� ��������
	{
		externvariables.clear();
		externvariables.reserve(externvarplug.size()*2);
		std::map<std::string, std::string>::iterator it = externvarplug.begin();
		for(;it!=externvarplug.end(); it++)
		{
			externvariables.push_back(it->first);
			externvariables.push_back(it->second);
		}
	}

	std::string outfilename = "uncompiled";
	{
		const char* sd = context->getEnvironment("SHADERDIR");
		if( !sd || !*sd)
		{
			MString _workspacedir;
			MGlobal::executeCommand("workspace -q -rd", _workspacedir);
			std::string workspacedir = _workspacedir.asChar();
			outfilename = workspacedir + "ocs/";
		}
		else
			outfilename = sd;

		Util::pushBackSlash(outfilename);
		outfilename += name;
		outfilename += ".sl";
		Util::create_directory_for_file(outfilename.c_str());

		displayString(
			"computeShaderFile for %s", 
			MFnDependencyNode(node).name().asChar());
		{
			FILE* file = fopen( outfilename.c_str(), "wt");
			if(!file)
			{
				printf("cant open file %s", outfilename.c_str());
				return "";
			}
			fputs(text.c_str(), file);
			fclose(file);
		}
		// �������� slo
		{
			std::string slo = outfilename + "o";
			remove(slo.c_str());
		}

		std::string shadercompiler = context->getEnvironment("SHADERCOMPILER");
		if( ShadingGraphAssembler::Compile(context, outfilename.c_str(), NULL, shadercompiler.c_str())=="")
		{
			return "";
		};
		Util::changeSlashToSlash(outfilename);

		// �������� ".slo" � �����
		size_t x = outfilename.rfind('.');
		if(x!=std::string::npos)
			outfilename = outfilename.erase(x);
//		data.outputValue(plug).set( MString(outfilename.c_str()));
	}
	return outfilename;
}
