#pragma once

#include "ocellaris/IGeometryGenerator.h"
#include "../MeshGenerator.h"

//! @ingroup implement_group
//! \brief ����������� ��������� ��������� ��� ������
//! 
struct UvCurveGenerator : public MeshGenerator
{
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "UvCurveGenerator";};

	// ��� �������� � ����
	virtual void OnLoadToMaya(
		MObject& generatornode
		);

	virtual bool OnGeometry(
		cls::INode& node,				//!< ������ 
		cls::IRender* render,			//!< render
		Math::Box3f& box,				//!< box 
		cls::IExportContext* context,	//!< context
		MObject& generatornode			//!< generator node �.� 0
		);
};
