#pragma once
#include <maya/MObject.h>
#include "ocellaris/IGeometryGenerator.h"

//! @ingroup implement_group
//! \brief ������� ocFile
//! 
struct ocFileGenerator : public cls::IGeometryGenerator
{
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "ocFileGenerator";};

	virtual bool OnGeometry(
		cls::INode& node,				//!< ������ 
		cls::IRender* render,			//!< render
		Math::Box3f& box,				//!< box 
		cls::IExportContext* context,	//!< context
		MObject& generatornode			//!< generator node �.� 0
		);
};
