#include "StdAfx.h"
#include ".\skinclustergenerator.h"
#include <maya/MFnMesh.h>
#include <maya/MFloatArray.h>
#include <maya/MAnimControl.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatVector.h>
#include <maya/MPlug.h>
#include <maya/MFnSkinCluster.h>
#include <maya/MItGeometry.h>
#include "mathNmaya/mathNmaya.h"
#include <maya/MItDependencyGraph.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MFnSingleIndexedComponent.h>

SkinClusterGenerator skinclustergenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl SkinCluster()
	{
		return &skinclustergenerator;
	}
}

SkinClusterGenerator::SkinClusterGenerator(void)
{
}

SkinClusterGenerator::~SkinClusterGenerator(void)
{
}

bool SkinClusterGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MStatus stat;
	MObject node = inode.getObject();

	MObject skin = node;
	MFnSkinCluster skinCluster(skin, &stat);

	MDagPathArray infs;
	unsigned int nInfs = skinCluster.influenceObjects(infs, &stat);

	cls::PA<Math::Matrix4f> joints_pos(cls::PI_CONSTANT), joints_invbindpos(cls::PI_CONSTANT);
	joints_pos.resize(nInfs);
	std::vector<std::string> joints_name(nInfs);

	for(unsigned i = 0; i<infs.length(); i++)
	{
		MDagPath jointpath = infs[i];
		MObject obj = infs[i].node();
		if( !obj.hasFn( MFn::kJoint))
		{
			printf("SkinFilterDeformGenerator: NODE %s isn't joint\n", jointpath.fullPathName().asChar());
			continue;
		}

		MFnTransform dagNode(infs[i].node());
		std::string jointName = dagNode.partialPathName().asChar();
		size_t reference = jointName.find(':');
		if(reference!=jointName.npos)
		{
			jointName = jointName.substr(reference+1);
		}
		Math::Matrix4f world;
		{
			// ������� �������
			MMatrix wm = jointpath.inclusiveMatrix();
			copy( world, wm);
		}
		joints_name[i] = jointName;
		joints_pos[i] = world;
	}

	render->comment("  GlobalAttribute SkinFilter::pos");
	render->GlobalAttribute("SkinFilter::pos", joints_pos);
	cls::PSA pjoints_name(joints_name);
	render->comment("  GlobalAttribute SkinFilter::jointnames");
	render->GlobalAttribute("SkinFilter::jointnames", pjoints_name);

	return true;
}

