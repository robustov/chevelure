#include "stdafx.h"

#include "ocFile.h"
#include "ocFileGenerator.h"
#include "ocellaris/MInstancePxData.h"
#include <maya/MFnDependencyNode.h>
#include <maya/MFnParticleSystem.h> 
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MPlug.h>
#include <maya/MVector.h>
#include <maya/MPxLocatorNode.h>
#include <maya/MPxLocatorNode.h>
#include <maya/MFnPluginData.h>
#include "mathNmaya/mathNmaya.h"
#include "util/misc_create_directory.h"
#include "ocellaris\renderStringImpl.h"

bool ocFileGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();
	MFnDependencyNode dn(node);

	bool brenderProxy = false;
	MPlug(node, ocFile::i_renderProxy).getValue(brenderProxy);

	const char* filenameattr = "filename";
	if(brenderProxy) 
		filenameattr = "proxyfilename";
	MPlug plug = MFnDependencyNode(node).findPlug(filenameattr);
	MString filename;
	plug.getValue(filename);

	bool readglobals = false;
	MPlug(node, ocFile::i_readglobals).getValue(readglobals);
	

	Math::Box3f thisbox(1.f);
	plug = MFnDependencyNode(node).findPlug("box");
	MObject boxval;
	plug.getValue(boxval);
	MFnVectorArrayData vad(boxval);
	if( vad.length()>=2)
	{
		::copy( thisbox.min, vad[0]);
		::copy( thisbox.max, vad[1]);
	}
	box.insert(thisbox);

	plug = MFnDependencyNode(node).findPlug("delay");
	bool bDelay = false;
	plug.getValue(bDelay);

	bool bMatte = false;
	MPlug(node, ocFile::i_matte).getValue(bMatte);

	bool bIgnoreShaders = false;
	MPlug(node, ocFile::i_ignoreShaders).getValue(bIgnoreShaders);

	MString addStringAllpass;
	MPlug(node, ocFile::i_addStringAllpass).getValue(addStringAllpass);

	MString addStringFinalpass;
	MPlug(node, ocFile::i_addStringFinalpass).getValue(addStringFinalpass);

	MString addStringShadowpass;
	MPlug(node, ocFile::i_addStringShadowpass).getValue(addStringShadowpass);

	if(render)
	{
		// ALL PASS
		{
			cls::renderStringImpl fileimpl;

			std::string customdata = addStringAllpass.asChar();
			customdata += "\n";

			if( fileimpl.openForRead(customdata.c_str()))
				fileimpl.Render(render);
			else
				displayString("read addStringAllpass FAILURE");
		}

		// FINAL PASS
//		if( strcmp( context->pass(), "final")==0)
		{
			render->IfAttribute("@pass", cls::PS("final"));
			cls::renderStringImpl fileimpl;
			std::string customdata = addStringFinalpass.asChar();
			customdata += "\n";
			if( fileimpl.openForRead(customdata.c_str()))
				fileimpl.Render(render);
			else
				displayString("read addStringFinalpass FAILURE");
			render->EndIfAttribute();
		}
		
		// SHADOW PASS
//		if( strcmp( context->pass(), "shadow")==0)
		{
			render->IfAttribute("@pass", cls::PS("shadow"));
			cls::renderStringImpl fileimpl;
			std::string customdata = addStringShadowpass.asChar();
			customdata += "\n";
			if( fileimpl.openForRead(customdata.c_str()))
				fileimpl.Render(render);
			else
				displayString("read addStringShadowpass FAILURE");
			render->EndIfAttribute();
		}

		std::string fn = filename.asChar();
		Util::changeSlashToSlash(fn);
		render->Parameter("filename", fn.c_str());

		if(readglobals)
		{
			cls::renderFileImpl infileimpl;
			infileimpl.openForRead(fn.c_str(), render);
		}

		if( bMatte)
			render->Attribute("@matte", true);

		if( bIgnoreShaders)
		{
			render->Attribute("shader::ignoreSurface", true);
			render->Attribute("shader::ignoreDisplace", true);
		}

		render->Parameter("#bound", thisbox);
		render->Parameter("id", (int)0);
		render->Parameter("@delay", bDelay);
		render->RenderCall("ReadOCS");
	}
	return true;
}
