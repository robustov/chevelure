#include "stdafx.h"
#include "UvCurveGenerator.h"
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MItCurveCV.h>
#include <maya/MPlug.h>
#include <maya/MPoint.h>
#include <maya/MPointArray.h>
#include <maya/MFnPointArrayData.h>
#include <string>
#include "ocellaris/OcellarisExport.h"
#include "mathNmaya/mathNmaya.h"
#include "math/perlinNoise.h"

UvCurveGenerator uvcurvegenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl UvCurveGenerator()
	{
		return &uvcurvegenerator;
	}
}

// ��� �������� � ����
void UvCurveGenerator::OnLoadToMaya(
	MObject& generatornode
	)
{
	MObject attr;
//	attr = ocExport_AddAttrType(generatornode, "baseWidth",   cls::PT_FLOAT, cls::P<float>(1));
//	attr = ocExport_AddAttrType(generatornode, "tipWidth",    cls::PT_FLOAT, cls::P<float>(1));
	// ������ ����
	attr = ocExport_AddAttrType(generatornode, "width",		cls::PT_FLOAT, cls::P<float>(0.04f));
	attr = ocExport_AddAttrType(generatornode, "widthJitter", cls::PT_FLOAT, cls::P<float>(1.f));
	attr = ocExport_AddAttrType(generatornode, "widthJitterStep", cls::PT_FLOAT, cls::P<float>(0.5f));
	// ������ �������
	attr = ocExport_AddAttrType(generatornode, "fiberWidth", cls::PT_FLOAT, cls::P<float>(0.1f));
//	attr = ocExport_AddAttrType(generatornode, "fiberWidthJitter", cls::PT_FLOAT, cls::P<float>(0.1f));
//	attr = ocExport_AddAttrType(generatornode, "fiberWidthFreq", cls::PT_FLOAT, cls::P<float>(0.5f));
	// ��� ������
	attr = ocExport_AddAttrType(generatornode, "twistStep", cls::PT_FLOAT, cls::P<float>(0.7f));
	attr = ocExport_AddAttrType(generatornode, "vertsPerTwistStep", cls::PT_INT, cls::P<int>(10));
	// fiberCount
	attr = ocExport_AddAttrType(generatornode, "fiberCount", cls::PT_INT, cls::P<int>(4));
	attr = ocExport_AddAttrType(generatornode, "fiberTwist", cls::PT_FLOAT, cls::P<float>(1));
//	attr = ocExport_AddAttrType(generatornode, "subdive",    cls::PT_INT, cls::P<int>(30));


	attr = ocExport_AddAttrType(generatornode, "hairsCount", cls::PT_INT, cls::P<int>(70));
	attr = ocExport_AddAttrType(generatornode, "hairVerts", cls::PT_INT, cls::P<int>(4));
	attr = ocExport_AddAttrType(generatornode, "hairLenght", cls::PT_FLOAT, cls::P<float>(0.1f));
	attr = ocExport_AddAttrType(generatornode, "hairScraggle", cls::PT_FLOAT, cls::P<float>(1.f));
	attr = ocExport_AddAttrType(generatornode, "hairWidth", cls::PT_FLOAT, cls::P<float>(0.004f));
	attr = ocExport_AddAttrType(generatornode, "hairLenghtJitter", cls::PT_FLOAT, cls::P<float>(0.f));

	attr = ocExport_AddAttrType(generatornode, "scale", cls::PT_FLOAT, cls::P<float>(1));
}

bool UvCurveGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MStatus stat;
	float width = 0.1f;
	float fiberWidth = 0.03f;
	float widthJitter = 1;
	float widthJitterStep = 0.5;
//		float fiberWidthJitter = 0.5;
//		float fiberWidthFreq = 1;
	float twistStep = 0.7f;
	int fiberCount = 4;
//	int subdive = 10;

	int seed = rand();
	MObject attr = ocExport_AddAttrType(inode.getObject(), "thread_seed", cls::PT_INT, cls::P<int>(seed));
	cls::P<int> _seed = ocExport_GetAttrValue(inode.getObject(), "thread_seed");
	render->Parameter("@Thread::id", _seed);

	float box_offset = width+fiberWidth;
	if( !generatornode.isNull())
	{
		cls::P<float> _width = ocExport_GetAttrValue(generatornode, "width");
		cls::P<float> _fiberWidth = ocExport_GetAttrValue(generatornode, "fiberWidth");
		cls::P<float> _widthJitter = ocExport_GetAttrValue(generatornode, "widthJitter");
		cls::P<float> _widthJitterStep = ocExport_GetAttrValue(generatornode, "widthJitterStep");
//			cls::P<float> _fiberWidthJitter = ocExport_GetAttrValue(generatornode, "fiberWidthJitter");
//			cls::P<float> _fiberWidthFreq = ocExport_GetAttrValue(generatornode, "fiberWidthFreq");
		cls::P<float> _twistStep = ocExport_GetAttrValue(generatornode, "twistStep");
		cls::P<int> _fiberCount = ocExport_GetAttrValue(generatornode, "fiberCount");
//		cls::P<int> _subdive = ocExport_GetAttrValue(generatornode, "subdive");
		cls::P<float> _fiberTwist = ocExport_GetAttrValue(generatornode, "fiberTwist");

		cls::P<int> vertsPerTwistStep = ocExport_GetAttrValue(generatornode, "vertsPerTwistStep");
		cls::P<float> scale = ocExport_GetAttrValue(generatornode, "scale");


		
		render->Parameter("Thread::scale", scale);
		render->Parameter("Thread::width", _width);
		render->Parameter("Thread::fiberWidth", _fiberWidth);
		render->Parameter("Thread::widthJitter", _widthJitter);
		render->Parameter("Thread::widthJitterStep", _widthJitterStep);
		render->Parameter("Thread::twistStep", _twistStep);
		render->Parameter("Thread::fiberCount", _fiberCount);
//		render->Parameter("Thread::subdive", _subdive);
		render->Parameter("Thread::fiberTwist", _fiberTwist);
		render->Parameter("@Thread::vertpertwist", vertsPerTwistStep);

		cls::P<int> hairsCount = ocExport_GetAttrValue(generatornode, "hairsCount");
		cls::P<int> hairVerts = ocExport_GetAttrValue(generatornode, "hairVerts");
		cls::P<float> hairLenght = ocExport_GetAttrValue(generatornode, "hairLenght");
		cls::P<float> hairScraggle = ocExport_GetAttrValue(generatornode, "hairScraggle");
		cls::P<float> hairWidth = ocExport_GetAttrValue(generatornode, "hairWidth");
		cls::P<float> hairLenghtJitter = ocExport_GetAttrValue(generatornode, "hairLenghtJitter");

		render->Parameter("Thread::hairsCount", hairsCount);
		render->Parameter("Thread::hairVerts", hairVerts);
		render->Parameter("Thread::hairLenght", hairLenght);
		render->Parameter("Thread::hairScraggle", hairScraggle);
		render->Parameter("Thread::hairWidth", hairWidth);
		render->Parameter("Thread::hairLenghtJitter", hairLenghtJitter);

		box_offset = 0;
		if( !_width.empty())
			box_offset += _width.data();
		if( !_fiberWidth.empty())
			box_offset += _fiberWidth.data();
		if( !hairLenght.empty())
			box_offset += hairLenght.data();
			
	}

	bool res = MeshGenerator::MeshToOcs(
		inode,				//!< ������ 
		render,			//!< render
		box,				//!< box 
		context,	//!< context
		false
		);
	if(!res)
		return false;

	render->RenderCall("UvCurve");

	return true;
}

