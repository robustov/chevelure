#include "stdafx.h"
#include "ocShaderGenerator.h"
#include <maya/MFnSet.h>
#include <maya/MFnMesh.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MPlug.h>
#include <maya/MFnStringArrayData.h>
#include <maya/MGlobal.h>
#include <maya/MFnNumericData.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnStringData.h>
#include <maya/MStringArray.h>
#include <maya/MFnStringArrayData.h>
#include <maya/MSelectionList.h>
#include <string>
#include "Util/misc_switch_string.h"
#include "ocellaris/OcellarisExport.h"
#include <maya/MPlugArray.h>
#include <maya/MDGModifier.h>
#include "mathNpixar/ISlo.h"
#include "Util/misc_create_directory.h"
#include "ocSurfaceShader.h"

ocShaderGenerator::ocShaderGenerator()
{
}


struct ocShaderGeneratorData
{
	MObject surfaceShader;
	std::string shaderfile;
	MObject displaceShader;
	std::string displaceshaderfile;
};
bool ocShaderGenerator::OnPrepareNode(
	cls::INode& node, 
	cls::IExportContext* context,
	MObject& generatornode
	)
{
	MObject obj = node.getPath().node();
	MStatus stat;

	MObject shadingGroup = MObject::kNullObj;

	// ����� SG ��� �������
	if( obj.hasFn(MFn::kMesh))
	{
		// ��� Mesh ����� getConnectedSetsAndMembers
		MFnMesh mesh(obj);
		// 
		int instanceNum=0;
		MObjectArray sets, comps; 
		// set ��� SG
		// comp - ��� ������ ���������(���������)
		if( !mesh.getConnectedSetsAndMembers(instanceNum, sets, comps, true)) 
		{
			return false;
		}
		for ( unsigned i=0; i<sets.length(); i++ ) 
		{
			MObject set = sets[i];
			MObject comp = comps[i];

			MFnSet fnSet( set );
			if ( fnSet.restriction(&stat) == MFnSet::kRenderableOnly)
			{
				shadingGroup = set;
				break;
			}
		}
	}
	else
	{
		// ��� ��������� ����� getAssociatedSets
		MSelectionList objects;
		objects.add( obj );
		MObjectArray setArray;

		// Get all of the sets that this object belongs to
		//
		MGlobal::getAssociatedSets( objects, setArray );

		// Look for a set that is a "shading group"
		for ( int i=0; i<(int)setArray.length(); i++ )
		{
			MObject mobj = setArray[i];
			MFnSet fnSet( mobj );
			if ( fnSet.restriction(&stat) == MFnSet::kRenderableOnly)
			{
				shadingGroup = mobj;
				break;
			}
		}
		if( shadingGroup.isNull())
		{
			MStringArray setArray;
			MString cmd = "listSets -o ";
			cmd+=MFnDependencyNode(obj).name();
			MGlobal::executeCommand(cmd, setArray);
			for ( int i=0; i<(int)setArray.length(); i++ )
			{
				MObject mobj;
				nodeFromName( setArray[i], mobj);
				if( mobj.isNull()) continue;
				MFnSet fnSet( mobj );
				if ( fnSet.restriction(&stat) == MFnSet::kRenderableOnly)
				{
					shadingGroup = mobj;
					break;
				}
			}
		}
	}
	if( shadingGroup.isNull())
	{
		// �� ������� SG
//		displayStringD(
//			"shadingGroup for %s not found", 
//			MFnDependencyNode(obj).name().asChar());
		return false;
	}


	MObject surfaceShaderNode = findShader(shadingGroup, "surfaceShader");
	MObject displaceShaderNode = findShader(shadingGroup, "displacementShader");

	if( surfaceShaderNode.isNull() &&
		displaceShaderNode.isNull())
	{
		return false;
	}

//	ocShaderGeneratorData* data = new ocShaderGeneratorData;
	bool bValid = false;
	if( !surfaceShaderNode.isNull())
	{
		// ������ ���� ocSurfaceShader?
		std::string shadername;
		// ����� ������
		// �������� ���������
		// ShaderFilename()
		MFnDependencyNode fnNode(surfaceShaderNode);
		MPlug shaderPlug = fnNode.findPlug("shaderFile");
		MString _shadername;
		if( shaderPlug.getValue(_shadername)) 
		{
			shadername = _shadername.asChar();

			cls::IRender* generatorcontect;
			generatorcontect = context->objectContext(surfaceShaderNode, this);
			generatorcontect->Parameter("file", shadername.c_str());

			generatorcontect = context->objectContext(obj, this);
			generatorcontect->Parameter("surfaceShader", shadername.c_str());
			generatorcontect->Parameter("surfaceShaderNode", fnNode.name().asChar());
			bValid = true;
		}
	}
	if( !displaceShaderNode.isNull())
	{
		// ������ ���� ocSurfaceShader?
		std::string shadername;

		// ����� ������
		// �������� ���������
		// ShaderFilename()
		MFnDependencyNode fnNode(displaceShaderNode);
		MPlug shaderPlug = fnNode.findPlug("shaderFile");
		MString _shadername;
		if( shaderPlug.getValue(_shadername)) 
		{
			// �����
			shadername = _shadername.asChar();

			cls::IRender* generatorcontect;
			generatorcontect = context->objectContext(displaceShaderNode, this);
			generatorcontect->Parameter("file", shadername.c_str());

			generatorcontect = context->objectContext(obj, this);
			generatorcontect->Parameter("displaceShader", shadername.c_str());
			generatorcontect->Parameter("displaceShaderNode", fnNode.name().asChar());
			bValid = true;
		}
	}
	if( !bValid)
		return false;

	return true;
}
bool ocShaderGenerator::OnSurface(
	cls::INode& node, 
	cls::IRender* render, 
	cls::IExportContext* context,
	MObject& generatornode
	)
{
	MObject obj = node.getPath().node();

	cls::IRender* generatorcontect;
	generatorcontect = context->objectContext(obj, this);
	if( !generatorcontect)
		return true;

	std::string shaderfile, surfaceShaderNode; 
	if( !generatorcontect->GetParameter("surfaceShader", shaderfile))
		return true;
	if( !generatorcontect->GetParameter("surfaceShaderNode", surfaceShaderNode))
		return true;
	MObject surfaceShader;
	nodeFromName(surfaceShaderNode.c_str(), surfaceShader);

	if( surfaceShader.isNull())
		return true;
	if( shaderfile.empty())
		return true;

	MStatus stat;

	// ����� ������
	// �������� ���������
	// ShaderFilename()
	MFnDependencyNode fnNode(surfaceShader);

	ocSurfaceShader* ocsurfaceshader = (ocSurfaceShader*)fnNode.userNode();
	if( render)
	{
//		render->PushAttributes();
		// ���������
//		ocsurfaceshader->slNode.getAttributeList();
		;

		for(int i=0; i<ocsurfaceshader->getParameterCount(); i++)
		{
			std::string name;
			cls::Param val;
			if( ocsurfaceshader->getParameter(i, name, val))
			{
				render->Parameter(name, val);
			}
		}

		render->Shader("surface", shaderfile.c_str());
//		render->PopAttributes();

		MPlug plug;
		plug = MPlug(surfaceShader, ocSurfaceShader::i_ignoreLastShader);
		bool bIgnore = false;
		plug.getValue(bIgnore);

		if( bIgnore)
		{
displayString("shader::ignoreSurface dont work correctly!!!");
			render->Attribute( "shader::ignoreSurface", true);
		}
	}

	return true;
}

bool ocShaderGenerator::OnDisplacement(
	cls::INode& node,			//!< ������ ��� �������� ����������� �������
	cls::IRender* render,			//!< render
	cls::IExportContext* context,
	MObject& generatornode
	)
{
	MObject obj = node.getPath().node();

	cls::IRender* generatorcontect;
	generatorcontect = context->objectContext(obj, this);
	if( !generatorcontect)
		return true;

	std::string displaceshaderfile, displaceShaderNode; 
	if( !generatorcontect->GetParameter("displaceShader", displaceshaderfile))
		return true;
	if( !generatorcontect->GetParameter("displaceShaderNode", displaceShaderNode))
		return true;
	MObject displaceShader;
	nodeFromName(displaceShaderNode.c_str(), displaceShader);

	if( displaceShader.isNull() || 
		displaceshaderfile.empty())
	{
//		render->Shader("displacement", "null");
		return true;
	}

	MStatus stat;

	// ����� ������
	// �������� ���������
	// ShaderFilename()
	MFnDependencyNode fnNode(displaceShader);

	ocSurfaceShader* ocsurfaceshader = (ocSurfaceShader*)fnNode.userNode();
	if( render)
	{
		for(int i=0; i<ocsurfaceshader->getParameterCount(); i++)
		{
			std::string name;
			cls::Param val;
			if( ocsurfaceshader->getParameter(i, name, val))
			{
				render->Parameter(name, val);
			}
		}
		cls::P<float> displacementBound = ocExport_GetAttrValue(displaceShader, "displacementBound");
		cls::PS displacementSpace = ocExport_GetAttrValue(displaceShader, "displacementSpace");

		render->Parameter("displacement::bound", displacementBound);
		render->Parameter("displacement::space", displacementSpace);

		render->Shader("displacement", displaceshaderfile.c_str());

		MPlug plug;
		plug = MPlug(displaceShader, ocSurfaceShader::i_ignoreLastShader);
		bool bIgnore = false;
		plug.getValue(bIgnore);

		if( bIgnore)
		{
displayString("shader::ignoreSurface dont work correctly!!!");
			render->Attribute( "shader::ignoreSurface", true);
		}
	}
	return true;
}


// Find the shading node for the given shading group set node.
MObject ocShaderGenerator::findShader( MObject& setNode, const char* plugName )
{
	MFnDependencyNode fnNode(setNode);
	MPlug shaderPlug = fnNode.findPlug(plugName);
			
	if (!shaderPlug.isNull()) 
	{
		MPlugArray connectedPlugs;
		bool asSrc = false;
		bool asDst = true;
		shaderPlug.connectedTo( connectedPlugs, asDst, asSrc );

		if (connectedPlugs.length() == 1)
			return connectedPlugs[0].node();
	}			
	return MObject::kNullObj;
}
