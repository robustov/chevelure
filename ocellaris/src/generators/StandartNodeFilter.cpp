#include "stdafx.h"
#include "StandartNodeFilter.h"
#include "ocellaris/IExportContext.h"
#include <maya/MFnDependencyNode.h>
#include <maya/MPlug.h>

// ������ ������� true ���� ������ �������� ���������
// ��������� �����
// visibility
// intermediateObject
// mtorInvis
// overrideEnabled->overrideVisibility

bool StandartNodeFilter::IsRenderNode(
	cls::INode& inode,				//!< ������
	cls::IRender* render,				//!< render
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	// ����� �� ������ � �� ���� ���� ���������
	MObject node = inode.getPath().node();
	if( node.hasFn( MFn::kGroundPlane))
		return false;
	if( node.hasFn( MFn::kManipulator))
		return false;

	bool flag;
	MFnDependencyNode dn(node);
	const char* etstr = context->getEnvironment("EXPORTTEMPLATED");
	bool bExportTemplated = false;
	if(etstr[0]=='1')
		bExportTemplated = true;

	MPlug plug;
	plug = dn.findPlug("intermediateObject");
	if( !plug.isNull())
	{
		plug.getValue(flag);
		if(flag) return false;
	}
	if( !bExportTemplated)
	{
		plug = dn.findPlug("template");
		if( !plug.isNull())
		{
			plug.getValue(flag);
			if(flag) return false;
		}
	}
	const char* iostr = context->getEnvironment("IGNOREOVERRIDE");
	bool bIgnoreOverride = false;
	if(iostr[0]=='1')
		bIgnoreOverride = true;
	if( !bIgnoreOverride)
	{
		plug = dn.findPlug( "overrideEnabled");
		if( !plug.isNull())
		{
			bool isOver = false;
			plug.getValue(isOver);
			if( isOver)
			{
				plug = dn.findPlug("overrideVisibility");
				if( !plug.isNull())
				{
					plug.getValue(flag);
					if(!flag) return false;
				}
			}
		}
	}

	plug = dn.findPlug("mtorInvis");
	if( !plug.isNull())
	{
		plug.getValue(flag);
		if(flag) return false;
	}

	plug = dn.findPlug("visibility");
	if( plug.isConnected())
		return true;
	if( !plug.isNull())
	{
		plug.getValue(flag);
		if(!flag) return false;
	}

	return true;
}

bool StandartNodeFilter::OnExport(
	cls::INode& inode,				//!< ������
	cls::IRender* render,				//!< render
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	bool bVisible = true;
	MPlug plug;
	MFnDependencyNode dn(inode.getObject());

	bool flag;
	plug = dn.findPlug("visibility");
	if( !plug.isNull())
	{
		plug.getValue(flag);
		if(!flag) bVisible=false;
	}
	plug = dn.findPlug("mtorInvis");
	if( !plug.isNull())
	{
		plug.getValue(flag);
		if(flag) bVisible=false;
	}
	if( !bVisible)
	{
		render->Attribute("@visible", bVisible);
	}
	return true;
}
