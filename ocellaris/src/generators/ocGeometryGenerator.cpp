#include "stdafx.h"
#include "ocGeometryGenerator.h"
#include <maya/MFnDependencyNode.h>
#include <maya/MFnParticleSystem.h> 
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MPlug.h>
#include <maya/MVector.h>
#include <maya/MPxLocatorNode.h>
#include <maya/MFnPluginData.h>
#include "mathNmaya/mathNmaya.h"
#include "ocellaris/MInstancePxData.h"


bool ocGeometryGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();
	MPlug plug = MFnDependencyNode(node).findPlug("ocsCache");
	MObject obj;
	plug.getValue(obj);

	MFnPluginData pd(obj);
	cls::MInstancePxData* cache = (cls::MInstancePxData*)pd.data();
	if( !cache) 
		return false;

	cls::MInstance& data = cache->drawcache;
	const char* ocsfile = data.getFile();
	if( !ocsfile) 
	{
		data.Render(render);
		box = data.getBox();
	}
	return true;
}
