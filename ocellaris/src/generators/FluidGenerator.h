#pragma once

#include "ocellaris/IGeometryGenerator.h"


//! @ingroup implement_group
//! \brief ��������� ��������� �������, (����)
//! 
struct FluidGenerator : public cls::IGeometryGenerator
{
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "FluidGenerator";};

	std::string workspacedir;

	virtual bool OnGeometry(
		cls::INode& node,				//!< ������ 
		cls::IRender* render,			//!< render
		Math::Box3f& box,				//!< box 
		cls::IExportContext* context,	//!< context
		MObject& generatornode			//!< generator node �.� 0
		);
};
