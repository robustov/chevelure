#include "stdafx.h"
#include "NameAttributeGenerator.h"
#include <maya/MFnMesh.h>
#include <maya/MFloatArray.h>

bool NameAttributeGenerator::OnAttribute(
	cls::INode& clsnode,			//!< ������ ��� �������� ����������� �������
	cls::IRender* render,	//!< render
	cls::IExportContext* context, 
//	const char* attrName,	//!< ��� ��������
//	cls::enParamType type, 
	MObject& generatornode
	)
{
	MObject node = clsnode.getObject();
	std::string name;
	MStatus stat;

const char* attrName = "name";
cls::enParamType type = cls::PT_STRING; 

	const char* _typename = node.apiTypeStr();
	

	MFnDependencyNode dn(node, &stat);
	if(stat)
		name = dn.name().asChar();
	/*/
	// � ������� ������� �����
	MFnDagNode dan(node, &stat);
	if(stat)
		name = dan.fullPathName().asChar();
	/*/

	if( render)
	{
		if( attrName)
			render->Attribute(attrName, name.c_str());
		else
			render->Attribute("name", name.c_str());

		render->Attribute("mayatype", _typename);
	}
	return true;
}
