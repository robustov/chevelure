#include "stdafx.h"
#include "FluidGenerator.h"
#include "ocellaris/OcellarisExport.h"
#include "ocellaris/renderFileImpl.h"
#include "MathNMaya/MathNMaya.h"

#include <maya/MFnFluid.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MItCurveCV.h>
#include <maya/MPlug.h>
#include <maya/MPoint.h>
#include <maya/MPointArray.h>
#include <maya/MGlobal.h>
#include <maya/MDagPath.h>
#include <maya/MMatrix.h>
#include <string>

#include "Util/misc_create_directory.h"

#include <maya/MRenderUtil.h>
#include <maya/MFloatMatrix.h>

// �������
bool FluidGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();
	MFnFluid fluid(node);

	cls::PA<int> res(cls::PI_CONSTANT, 3);
	unsigned int _res[3];
	fluid.getResolution(_res[0], _res[1], _res[2]);
	res[0] = _res[0]; res[1] = _res[1]; res[2] = _res[2];

	double x, y, z;
	fluid.getDimensions(x, y, z);
	cls::PA<float> dim(cls::PI_CONSTANT, 3);
	dim[0] = (float)x; dim[1] = (float)y; dim[2] = (float)z;

	render->Parameter("#resolution", res);
	render->Parameter("#dimentions", dim);
//	render->Parameter("world", world);

	int s = res[0]*res[1]*res[2];
	int gs = fluid.gridSize();

	float ds = 0;
	cls::PA<float> density(cls::PI_CONSTANT, s);

	float* densityGrid = fluid.density();
	if( densityGrid)
	{
		for(int z = 0; z<res[2]; z++)
			for(int y = 0; y<res[1]; y++)
				for(int x = 0; x<res[0]; x++)
				{
					int find = fluid.index(x, y, z);
					int myind = x + y*res[0] + z*res[0]*res[1];
					if(find>=0 && find<gs)
					{
						density[myind] = densityGrid[find];
						ds += density[myind];
					}
					else
						density[myind] = 0;
				}
		ds = ds/s;
	}
	else
	{
		ds = 1.f;
		for(int i = 0; i<s; i++)
			density[i] = ds;
	}

	// ��������
	/*/
	MPlug plug = fluid.findPlug("outColor");
//	MPlug plug = fluid.findPlug("outTransparency");
	if( !plug.isNull())
	{
		MFloatPointArray refpoints(s);
		for(int z = 0; z<res[2]; z++)
			for(int y = 0; y<res[1]; y++)
				for(int x = 0; x<res[0]; x++)
				{
					int myind = x + y*res[0] + z*res[0]*res[1];
					MFloatPoint pt(
						(x+0.5f)/(float)res[0], 
						(y+0.5f)/(float)res[1], 
						(z+0.5f)/(float)res[2]);
					pt.x *= dim[0];
					pt.y *= dim[1];
					pt.z *= dim[2];

//					pt.x = x;
//					pt.y = y;
//					pt.z = z;
					refpoints[myind] = pt;
				}

		MFloatMatrix cameraMat;
		MFloatVectorArray colors, transps;

		bool result;
		result = MRenderUtil::sampleShadingNetwork( 
				plug.name(), 
				s,
				false, false, cameraMat,
				&refpoints,
//				NULL, 
				NULL, NULL,
				NULL, 
				NULL, 
//				&refpoints, 
				NULL, NULL, NULL,
				colors, transps );
		for(int z = 0; z<res[2]; z++)
			for(int y = 0; y<res[1]; y++)
				for(int x = 0; x<res[0]; x++)
				{
					int myind = x + y*res[0] + z*res[0]*res[1];
					if( myind<(int)colors.length())
					{
						float c = 1-colors[myind].x;
						float t = transps[myind].x;
						density[myind] *= c;
					}
				}

	}
	/*/
	render->Parameter("#densitySum", ds);
	render->Parameter("#density", density);
	

	float *rGrid;
	float *gGrid;
	float *bGrid;
	fluid.getColors(rGrid, gGrid, bGrid);
	if(rGrid && gGrid && bGrid) 
	{
		cls::PA<Math::Vec3f> color(cls::PI_CONSTANT, s);

		for(int z = 0; z<res[2]; z++)
			for(int y = 0; y<res[1]; y++)
				for(int x = 0; x<res[0]; x++)
				{
					int find = fluid.index(x, y, z);
					int myind = x + y*res[0] + z*res[0]*res[1];
					if(find>=0 && find<gs)
						color[myind] = Math::Vec3f(rGrid[find], gGrid[find], bGrid[find]);
					else
						color[myind] = Math::Vec3f(0, 0, 0);
				
				}
		render->Parameter("color", color);
	}

	ocExport_AddAttrType(node, "particleRadius", cls::PT_FLOAT, cls::P<float>(0.1f));
	cls::P<float> radius = ocExport_GetAttrValue(node, "particleRadius");
	render->Parameter("#radius", radius);

	ocExport_AddAttrType(node, "particteCount", cls::PT_INT, cls::P<int>(10));
	cls::P<int> particteCount = ocExport_GetAttrValue(node, "particteCount");
	render->Parameter("#particteCount", particteCount);

	render->RenderCall("Fluid");

	box.insert(Math::Vec3f(-dim[0]/2, -dim[1]/2, -dim[2]/2));
	box.insert(Math::Vec3f( dim[0]/2,  dim[1]/2,  dim[2]/2));
	return true;
}

/*/

// �������� ������� � ��������!!!

void Planes( 
	cls::PA<int>& nverts, cls::PA<int>& loops, cls::PA<int>& iverts, 
	cls::PA<Math::Vec3f>& P, int N, int startN, Math::Vec3f v00, Math::Vec3f v01, Math::Vec3f v10, Math::Vec3f v11, Math::Vec3f dv)
{
	for(int x=0; x<N; x++)
	{
		Math::Vec3f v = dv*(x+0.5f);

		int i = x + startN;
		nverts[i] = 4;
		loops[i] = 1;
		iverts[i*4 + 0] = i*4 + 0;
		iverts[i*4 + 1] = i*4 + 1;
		iverts[i*4 + 2] = i*4 + 2;
		iverts[i*4 + 3] = i*4 + 3;

		P[i*4 + 0] = v00 + v;
		P[i*4 + 1] = v01 + v;
		P[i*4 + 2] = v11 + v;
		P[i*4 + 3] = v10 + v;
	}
}

bool FluidGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();
	MFnFluid fluid(node);

	MDagPath path = MDagPath::getAPathTo(node);
	Math::Matrix4f world;
	copy(world, path.inclusiveMatrix());
	MObject ocshading = getExternShaderNode(node);

	cls::PS defval("rmantmp\\Fluid.$S.$O.$F.ocs");
	MObject attr1 = ocExport_AddAttrType(ocshading, "fluidCacheFile", cls::PT_STRING, defval);
	cls::PS _cache = ocExport_GetAttrValue(ocshading, attr1);
	std::string cachefile = ocExport_GetCurrentDirectory()+ocExport_ApplySubParameters(_cache.data());
	std::string cachefilename = cachefile;
	Util::changeSlashToSlash(cachefile);

	MObject attr2 = ocExport_AddAttrType(ocshading, "fluidCacheFileAscii", cls::PT_BOOL, cls::P<bool>(false));
	cls::P<bool> bAscii = ocExport_GetAttrValue(ocshading, attr2);
	
	cls::renderFileImpl file;
	if( !file.openForWrite(cachefilename.c_str(), bAscii.data()))
	{
		fprintf(stderr, "FluidGenerator: file %s can't be created\n", cachefilename.c_str());
		return false;
	}
	cls::IRender* filerender = &file;

	cls::PA<unsigned int> res(cls::PI_CONSTANT, 3);
	fluid.getResolution(res[0], res[1], res[2]);
	double x, y, z;
	fluid.getDimensions(x, y, z);
	cls::PA<float> dim(cls::PI_CONSTANT, 3);
	dim[0] = (float)x; dim[1] = (float)y; dim[2] = (float)z;

	filerender->Parameter("resolution", res);
	filerender->Parameter("dimentions", dim);
	filerender->Parameter("world", world);

	int s = res[0]*res[1]*res[2];
	int gs = fluid.gridSize();

	float* densityGrid = fluid.density();
	if( densityGrid)
	{
		float ds = 0;
		cls::PA<float> density(cls::PI_CONSTANT, s);

		for(unsigned int z = 0; z<res[2]; z++)
			for(unsigned int y = 0; y<res[1]; y++)
				for(unsigned int x = 0; x<res[0]; x++)
				{
					int find = fluid.index(x, y, z);
					int myind = x + y*res[0] + z*res[0]*res[1];
					if(find>=0 && find<gs)
					{
						density[myind] = densityGrid[find];
						ds += density[myind];
					}
					else
						density[myind] = 0;
				}

		filerender->Parameter("densitySum", ds);
		filerender->Parameter("density", density);
	}

	float *rGrid;
	float *gGrid;
	float *bGrid;
	fluid.getColors(rGrid, gGrid, bGrid);
	if(rGrid && gGrid && bGrid) 
	{
		cls::PA<Math::Vec3f> color(cls::PI_CONSTANT, s);

		for(unsigned int z = 0; z<res[2]; z++)
			for(unsigned int y = 0; y<res[1]; y++)
				for(unsigned int x = 0; x<res[0]; x++)
				{
					int find = fluid.index(x, y, z);
					int myind = x + y*res[0] + z*res[0]*res[1];
					if(find>=0 && find<gs)
						color[myind] = Math::Vec3f(rGrid[find], gGrid[find], bGrid[find]);
					else
						color[myind] = Math::Vec3f(0, 0, 0);
				
				}
		filerender->Parameter("color", color);
	}

	// TEST
	{
		cls::PA<int> nverts(cls::PI_PRIMITIVE, res[0]+res[1]+res[2]);
		cls::PA<int> loops(cls::PI_PRIMITIVE, (res[0]+res[1]+res[2]));
		cls::PA<int> iverts(cls::PI_PRIMITIVEVERTEX, (res[0]+res[1]+res[2])*4);
		cls::PA<Math::Vec3f> P(cls::PI_VERTEX, (res[0]+res[1]+res[2])*4);

		Planes( nverts, loops, iverts, P, res[2], 
			0, 
			Math::Vec3f( -dim[0]/2, -dim[1]/2, -dim[2]/2),
			Math::Vec3f( -dim[0]/2,  dim[1]/2, -dim[2]/2),
			Math::Vec3f(  dim[0]/2, -dim[1]/2, -dim[2]/2),
			Math::Vec3f(  dim[0]/2,  dim[1]/2, -dim[2]/2),
			Math::Vec3f(0, 0, dim[2]/res[2]));

		Planes( nverts, loops, iverts, P, res[1], 
			res[2], 
			Math::Vec3f( -dim[0]/2, -dim[1]/2, -dim[2]/2),
			Math::Vec3f( -dim[0]/2, -dim[1]/2, dim[2]/2),
			Math::Vec3f(  dim[0]/2, -dim[1]/2, -dim[2]/2),
			Math::Vec3f(  dim[0]/2, -dim[1]/2, dim[2]/2),
			Math::Vec3f(0, dim[1]/res[1], 0));

		Planes( nverts, loops, iverts, P, res[0], 
			res[2]+res[1], 
			Math::Vec3f( -dim[0]/2, -dim[1]/2, -dim[2]/2),
			Math::Vec3f( -dim[0]/2, -dim[1]/2, dim[2]/2),
			Math::Vec3f( -dim[0]/2, dim[1]/2, -dim[2]/2),
			Math::Vec3f( -dim[0]/2, dim[1]/2, dim[2]/2),
			Math::Vec3f(dim[0]/res[0], 0, 0));

		render->Parameter("FluidFileName", cachefile.c_str());
		render->Parameter("P", P);
		render->Mesh("linear", loops, nverts, iverts);
	}

//	render->Parameter("fluidCache", cache);
//	render->Call("renderfluid");
	box.min = Math::Vec3f(-dim[0]/2, -dim[1]/2, -dim[2]/2);
	box.max = Math::Vec3f( dim[0]/2,  dim[1]/2,  dim[2]/2);
	return true;
}
/*/