#include "ocellaris/IGeometryGenerator.h"
#include "ocellaris/IPassGenerator.h"

//! @ingroup implement_group
//! \brief ��������� ��������� ��� ParticleSystem
//! ������ ����� �������������� Points, Spheres � Blobby (� ������� ��������)
//! 
struct InstancerGenerator : public cls::IGeometryGenerator
{
	enum enParticleRenderType
	{
		multiPoints = 0,
		multyStreak = 1,
		numeric = 2,
		points = 3,
		spheres = 4,
		sprites = 5,
		streak = 6,
		blobby = 7,
		cloud = 8,
		tube = 9
	};

	// ��� �������� � ����
	virtual void OnLoadToMaya(
		MObject& generatornode
		);

	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "InstancerGenerator";};

	// ����������� ��������� (���������� 1 ��� ����� ������� ��������)
	virtual bool OnPrepare(
		cls::IExportContext* context
		);
	//! ����������� ���� � �������� 
	bool InstancerGenerator::OnPrepareNode(
		cls::INode& node, 
		cls::IExportContext* context,
		MObject& generatornode
		);

	// �������
	virtual bool OnGeometry(
		cls::INode& node,				//!< ������ 
		cls::IRender* render,			//!< render
		Math::Box3f& box,				//!< box 
		cls::IExportContext* context,	//!< context
		MObject& generatornode			//!< generator node �.� 0
		);
/*/
protected:
	struct IG_Instance
	{
		std::string filename;
		Math::Box3f box;
		bool bValid;
		std::vector< std::pair< std::string, cls::Param> > externalattrs;
		IG_Instance(){bValid=false;}
	};
	bool exportShape(
		MDagPath& path, 
		IG_Instance& igi,
		cls::IExportContext* context	//!< context
		);
	bool exportShape(
		cls::INode& instancernode,				//!< ������ 
		MDagPath& path, 
		Math::Box3f& box, 
		const char* filename,
		cls::IExportContext* context	//!< context
		);
/*/
};


// ��� �������� �������������� ��������
struct InstancerPassGenerator : public cls::IPassGenerator
{
	// ������ -> ��� �����
	struct Data
	{
		cls::INode* root;					// �������� ����
		std::list<cls::INode*> nodes;		// ��� �������
		// ���� ����������
		std::string filename;
		Math::Box3f box;

		// ��� ����� � ���� (��������� ���� ����)
		bool getFileAndBox(std::string& filename, Math::Box3f& box, cls::IExportContext* context);
		// ���� ���� � ���� ������?
		bool isMyPath(MDagPath path);

		// ����������� �������
		void recExport(
			float exportFrame, 
			cls::INode& root, 
			Math::Matrix4f transform, 
			Math::Box3f& box, 
			cls::IRender* render, 
			cls::IExportContext* context, 
			int level
			);

	};
	typedef std::vector<Data> ObjectList;

	// instancer -> ObjectList
	static std::map<cls::INode*, ObjectList> data;

	static void recAdd(
		cls::INode& root, 
		InstancerPassGenerator::Data& data, 
		cls::IExportContext* context,
		cls::INode* instancerpassnode
		);

	// ����������� ��������� (���������� 1 ��� ����� ������� ��������)
	virtual bool OnPrepare(
		cls::IExportContext* context
		);

	// ���� ������ ����������� ������� ������� � ������ ����� ��� ���
	virtual bool IsUsedInFrame(
		cls::INode& pass, 
		float renderframe, 			
		cls::IExportContext* context
		);

	// �������� ������ � ��������� ExportFrame ��� �������� ����� (RenderFrame)
	// �������� � ������ context->addExportTimeNode()
	// ���� ����� ���� ���������� IBlurGenerator
	virtual bool AddExportFrames(
		cls::INode& pass, 
		cls::INode& node, 
		float renderframe, 			
		cls::IExportContext* context
		);

	// ������� ��� ����������� ���� �� ExportFrame 
	// ���� ����� ���� ���������� IBlurGenerator
	virtual bool BuildNodeFromExportFrame(
		cls::INode& pass,				// pass
		cls::INode& node,				// pass
		cls::IRender* render,			// ������
		float frame,
		Math::Matrix4f& transform,		// ����������� ����� transform
		Math::Box3f& box,				// ����������� ���� ��� �������
		cls::IExportContext* context	// �������� ��������
		);

	//! ������� ������ � ��������
	virtual bool OnExport(
		cls::INode& node,				//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IRender* scenecache,		//!< ��� �����
		Math::Box3f scenebox,			//!< box �����
		cls::IRender* subpasscache,		//!< ��� ����������� (�� ���� ������� OnExportParentPass)
		cls::IExportContext* context	// 
		);

	// ���������� � ���� ����������� ��� �������� �������
	// ��������, ������ shadowpass ������ �������� � ������� ��� �������� ����
	virtual void OnExportParentPass( 
		cls::INode& subnode,				//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IRender* render, 
		cls::IExportContext* context
		);

	//! ���������� ����� �������� ���� ������
	virtual bool OnPostExport(
		cls::INode& node,				//!< ������ ���������� ������� (�.�., kNullObj)
		cls::IExportContext* context	// 
		);

	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "InstancerPassGenerator";};
};
