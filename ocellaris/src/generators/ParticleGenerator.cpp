#include "stdafx.h"
#include "ParticleGenerator.h"
#include <maya/MFnDependencyNode.h>
#include <maya/MFnParticleSystem.h> 
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MPlug.h>
#include <maya/MVector.h>


//! @ingroup implement_group
//! \brief ����������� ��������� ��� kParticle (particleSystem)
//! 

// ���� ���������� ��� ����
bool ParticleGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();
	int z;
	MStatus stat;
	MFnParticleSystem ps(node);
	MFnDependencyNode dn(node);

	MPlug instanceplug = dn.findPlug("instanceData", &stat);
	int nInst = instanceplug.numElements();
	if(nInst!=0)
	{
		for( int i=0; i<nInst; i++)
		{
			MPlug p = instanceplug.connectionByPhysicalIndex(i);
			if(render)
				render->comment(p.name().asChar());
			if(!p.isNull())
			{
				// �� ����������
				if(render)
					render->comment("size instanceData != 0");
				return false;
			}
		}
	}


	// id
	MPlug paramplug = dn.findPlug("id", &stat);
	if( !stat) return false;
	MObject idobj;
	if( !paramplug.getValue( idobj)) return false;
	MFnDoubleArrayData _id0(idobj);
	cls::PA<int> ids(cls::PI_VERTEX, _id0.length());
	int minid=0, maxid=0;
	for(z=0; z<(int)_id0.length(); ++z) 
	{
		ids[z] = (int)_id0[z];
		if(z==0)
			minid = maxid = ids[z];
		minid = __min(minid, ids[z]);
		maxid = __max(maxid, ids[z]);
	}

	// particleRenderType
	enParticleRenderType type;
	dn.findPlug("particleRenderType", &stat).getValue( *(int*)&type);

	// position
	MPlug plug_position = dn.findPlug("position", &stat);
	MObject obj_position;
	plug_position.getValue(obj_position);
	MFnVectorArrayData position(obj_position);
	cls::PA<Math::Vec3f> verts(cls::PI_VERTEX, ids.size(), cls::PT_POINT);
	for(z=0; z<ids.size(); ++z) 
	{
		verts[z].x = (float)position[z].x;
		verts[z].y = (float)position[z].y;
		verts[z].z = (float)position[z].z;
		box.insert(verts[z]);
	}

	// RadiusPP
	float maxRadius = 0;
	MPlug plug_radiusPP = dn.findPlug("radiusPP", &stat);
	if(stat)
	{
		MObject idobj;
		plug_radiusPP.getValue(idobj);
		MFnDoubleArrayData _id0(idobj);
		cls::PA<float> radiusPP(cls::PI_VERTEX, ids.size());
		for(z=0; z<ids.size(); ++z) 
		{
			radiusPP[z] = (float)_id0[z];
			maxRadius = __max(maxRadius, radiusPP[z]);
		}
		if( render)
			render->Parameter("radius", radiusPP);
	}
	else
	{
		MPlug plug_radius = dn.findPlug("radius", &stat);
		if( stat)
		{
			double r; plug_radius.getValue(r);
			cls::P<float> radius((float)r);
			maxRadius = __max(maxRadius, (float)r);
			if( render)
				render->Parameter("radius", radius);
		}
	}

	render->Parameter("#id", ids);
	render->Parameter("P", verts);

	if( render)
	{
		switch(type)
		{
		case spheres:
			{
				render->Parameter("@type", "spheres");
				render->RenderCall("Particles");
				box.expand( maxRadius);
				break;
			}
		case blobby:
			{
				render->Parameter("@type", "blobby");
				render->RenderCall("Particles");
				box.expand( maxRadius);
				break;
/*/
				cls::PA<float> radius = render->GetParameter("radius");
				cls::PA<Math::Matrix4f> ellipsoids(cls::PI_VERTEX, ids.size());

				float maxradius = 0;
				for(int i=0; i<ids.size(); i++)
				{
					float r = 1;
					if( radius.size()==ids.size() && radius->interp == cls::PI_VERTEX)
						r = radius[i];
					else if(radius.size()>=1)
						r = radius[0];

					maxradius = __max(maxradius, r);
					Math::Matrix4f m = Math::Matrix4f::id;
					float d = r*2.46698f;
					m.scale( Math::Vec4f(d, d, d, 1));

					m[3] = Math::Vec4f( verts[i], 1);

					ellipsoids[i] = m;
				}
				render->Blobby(ellipsoids);
				box.expand( maxradius);
				break;
/*/
			}
		default:
			{
				/*/
				cls::PA<float> radius;
				radius = render->GetParameter("radius");
				if( radius.size()==ids.size())
				{
					box.expand( maxRadius);
					render->Parameter("#width", radius);
				}
				else if(radius.size()==1)
				{
					box.expand( maxRadius);
					render->Parameter("#width", maxRadius);
				}
				else
				{
					box.expand( 0.1f);
					render->Parameter("#width", 0.1f);
				}
				/*/

//				render->Parameter("#minid", minid);
//				render->Parameter("#maxid", maxid);
//				render->Points(verts);
				render->Parameter("@type", "points");
				render->RenderCall("Particles");
				box.expand( maxRadius);
				break;
			}
		}
	}

	return true;
}
