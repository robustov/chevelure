#pragma once

#include "ocellaris/INodeFilter.h"

#pragma warning ( disable:4275)
#pragma warning ( disable:4251)

//! @ingroup implement_group
//! \brief ����������� ������ ��������. 
//! �� ��������� ��������� ��������� (visible==0) � intermediate ��������
//! 
struct StandartNodeFilter : public cls::INodeFilter
{
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "StandartNodeFilter";};

	// ������ ������� true ���� ������ �������� ���������
	virtual bool IsRenderNode(
		cls::INode& inode,				//!< ������
		cls::IRender* render,				//!< render
		cls::IExportContext* context,	//!< context
		MObject& generatornode			//!< generator node �.� 0
		);
	// ���������� ��� ��������
	bool OnExport(
		cls::INode& inode,				//!< ������
		cls::IRender* render,				//!< render
		cls::IExportContext* context,	//!< context
		MObject& generatornode			//!< generator node �.� 0
		);
};