#include "stdafx.h"
#include "OnceNodeFilter.h"
#include <maya/MFnDependencyNode.h>
#include <maya/MPlug.h>

OnceNodeFilter::OnceNodeFilter()
{
}

// ������ ������� true ���� ������ �������� ���������
bool OnceNodeFilter::IsRenderNode(
	cls::INode& inode,				//!< ������
	cls::IRender* render,				//!< render
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();
	int id = *(int*)(&node);
	if( processed.find(id)!=processed.end())
		return false;

	processed.insert(id);
	return true;
}

