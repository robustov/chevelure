#pragma once
#include "ocellaris/IGeometryGenerator.h"

//! @ingroup implement_group
//! \brief ��������� ��������� ��� ParticleSystem
//! ������ ����� �������������� Points, Spheres � Blobby (� ������� ��������)
//! , ����� ����� �� InstancerGenerator ���� ������� �� ��� �������
struct ParticleGenerator : public cls::IGeometryGenerator
{
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "ParticleGenerator";};

	enum enParticleRenderType
	{
		multiPoints = 0,
		multyStreak = 1,
		numeric = 2,
		points = 3,
		spheres = 4,
		sprites = 5,
		streak = 6,
		blobby = 7,
		cloud = 8,
		tube = 9
	};

	virtual bool OnGeometry(
		cls::INode& node,				//!< ������ 
		cls::IRender* render,			//!< render
		Math::Box3f& box,				//!< box 
		cls::IExportContext* context,	//!< context
		MObject& generatornode			//!< generator node �.� 0
		);
};
