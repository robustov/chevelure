#pragma once

#include "ocellaris/INodeFilter.h"
#include <set>

#include "ocellaris/OcellarisExport.h"
#pragma warning ( disable:4275)
#pragma warning ( disable:4251)

//! @ingroup implement_group
//! \brief Once Node Filter
//! �� ��������� ��������� ��������� ��������
//! 
struct OCELLARISEXPORT_API OnceNodeFilter : public cls::INodeFilter
{
	std::set<int> processed;

	OnceNodeFilter();

	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "OnceNodeFilter";};

	// ������ ������� true ���� ������ �������� ���������
	virtual bool IsRenderNode(
		cls::INode& inode,				//!< ������
		cls::IRender* render,				//!< render
		cls::IExportContext* context,	//!< context
		MObject& generatornode			//!< generator node �.� 0
		);
};