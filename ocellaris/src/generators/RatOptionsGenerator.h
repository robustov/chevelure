#pragma once

#include "ocellaris/INode.h"
#include "mathNpixar/IPrman.h"
#include "ocellaris/OcellarisExport.h"

#pragma warning ( disable : 4275)

//! @ingroup implement_group
//! \brief ���������� ��������� ���������� ������ �� �������� R��
//! 
//! ���������� ���������:
//! float output::gain
//! float output::gamma
//! float[2] output::pixelsample
//! int[2] output::filtersize
//! string output::filtername
//! 
struct OCELLARISEXPORT_API RatOptionsGenerator
{
	bool OnOptions(
		cls::IRender* render, 
		cls::IExportContext* context);

};
