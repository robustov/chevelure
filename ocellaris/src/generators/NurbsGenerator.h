#pragma once

#include "ocellaris/IGeometryGenerator.h"


//! @ingroup implement_group
//! \brief ����������� ��������� ��������� ��� nurbs ������������
//! 
struct NurbsGenerator : public cls::IGeometryGenerator
{
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "NurbsGenerator";};

	virtual bool OnGeometry(
		cls::INode& node,				//!< ������ 
		cls::IRender* render,			//!< render
		Math::Box3f& box,				//!< box 
		cls::IExportContext* context,	//!< context
		MObject& generatornode			//!< generator node �.� 0
		);
};
