#include "stdafx.h"
#include "EdgesMeshGenerator.h"
#include "mathNMaya/mathNMaya.h"
#include <maya/MFnMesh.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MPlug.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MIntArray.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MPointArray.h>
#include <string>

EdgesMeshGenerator edgesmeshgenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl EdgesMesh()
	{
		return &edgesmeshgenerator;
	}
}

// ��� �������� � ����
void EdgesMeshGenerator::OnLoadToMaya(
	MObject& generatornode
	)
{
	MObject attr;
	attr = ocExport_AddAttrType(generatornode, "renderWidth", cls::PT_FLOAT, cls::P<float>(0.1f));
}

bool EdgesMeshGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	cls::P<float> _renderWidth = ocExport_GetAttrValue(generatornode, "renderWidth");
	float renderWidth = 0.1f;
	if( !_renderWidth.empty()) renderWidth = *_renderWidth;

	MObject node = inode.getObject();
	MStatus stat;
//	std::string interpolation = "linear";

	MFnMesh mesh(node, &stat);
	if( !stat) 
		return false;

	int fc = mesh.numPolygons();
	int nc = mesh.numNormals();
	int vc = mesh.numVertices();
	int fvc = mesh.numFaceVertices();

	if( !fc || !vc)
		return false;

	int ec = mesh.numEdges();

	MFloatPointArray _verts;
	mesh.getPoints(_verts);
//	MFloatVectorArray _normals;
//	mesh.getNormals( _normals );

	cls::PA<int> nverts(cls::PI_PRIMITIVE, ec);
	cls::PA<Math::Vec3f> verts(cls::PI_VERTEX, ec*2);
//	cls::PA<Math::Vec3f> norms(cls::PI_VERTEX, ec*2);


	for(int i=0; i<ec; i++)
	{
		nverts[i] = 2;

		int2 evt;
		mesh.getEdgeVertices(i, evt);

		MFloatPoint v0 = _verts[evt[0]];
		::copy(verts[i*2+0], v0);
		MFloatPoint v1 = _verts[evt[1]];
		::copy(verts[i*2+1], v1);

		box.insert(verts[i*2+0]);
		box.insert(verts[i*2+1]);

	}

	if(render)
	{
		render->Parameter("#width", renderWidth);
		render->Parameter("P", cls::PT_POINT, verts);
		render->Curves("linear", "nonperiodic", nverts);
	}
	return true;
}

