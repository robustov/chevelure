#include "stdafx.h"
#include "RendAttrNodeGenerator.h"
#include <maya/MFnMesh.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MPlug.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MIntArray.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnTransform.h>
#include <maya/MTransformationMatrix.h>
#include <string>
#include <mathNmaya/mathNmaya.h>
#include "math/Matrix.h"

RendAttrNodeGenerator::RendAttrNodeGenerator()
{
}

bool RendAttrNodeGenerator::OnNodeStart(
	cls::INode& node,				//!< ������ 
	cls::IRender* render,			//!< render
	cls::IExportContext* context,	//!< context 
	MObject& generatornode			//!< generator node �.� 0
	)
{
	bool bLight = node.getObject().hasFn(MFn::kLight);
	if( render)
	{
		if( !bLight)
			render->PushRenderAttributes();

		MObject obj = node.getObject();
		MObject obj2 = node.getPath().node();
		MFnDependencyNode dn(obj2);
		MPlug plug;

		bool primaryVisibility = true;
		plug = dn.findPlug( "primaryVisibility");
		plug.getValue( primaryVisibility);
		render->Attribute("@visiblefinal", primaryVisibility);

		bool castsShadows = true;
		plug = dn.findPlug( "castsShadows");
		plug.getValue( castsShadows);
		render->Attribute("@visibleshadow", castsShadows);
		
	}
	return true;
}
void RendAttrNodeGenerator::OnNodeEnd(
	cls::INode& node,				//!< ������ 
	cls::IRender* render,				//!< render
	cls::IExportContext* context,	//!< context 
	MObject& generatornode			//!< generator node �.� 0
	)
{
	bool bLight = node.getObject().hasFn(MFn::kLight);
	if( render)
	{
		if( !bLight)
			render->PopRenderAttributes();
	}
}

