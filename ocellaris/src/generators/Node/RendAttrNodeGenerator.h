#pragma once

#include "ocellaris/INodeGenerator.h"
//! @ingroup implement_group
//! \brief ������ ������ PushRenderAttribute/PopRenderAttribute ��� ����
//! 
struct RendAttrNodeGenerator : public cls::INodeGenerator
{
	RendAttrNodeGenerator();
public:
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "RendAttrNodeGenerator";};

	virtual bool OnNodeStart(
		cls::INode& node,				//!< ������ 
		cls::IRender* render,			//!< render
		cls::IExportContext* context,	//!< context 
		MObject& generatornode			//!< generator node �.� 0
		);
	virtual void OnNodeEnd(
		cls::INode& node,				//!< ������ 
		cls::IRender* render,				//!< render
		cls::IExportContext* context,	//!< context 
		MObject& generatornode			//!< generator node �.� 0
		);
};
