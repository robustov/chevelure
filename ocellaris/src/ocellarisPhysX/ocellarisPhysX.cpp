// ocellarisPhysX.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "ocellaris/renderImplPhysX.h"
#include "NxCooking.h"
#include "Stream.h"

#define render_getbool(attr) bool attr = false;\
						render->GetParameter( #attr, attr );

#define render_getfloat(attr) float attr = 0.0f;\
						render->GetParameter( #attr, attr );

#define render_getint(attr) int attr = 0;\
						render->GetParameter( #attr, attr );

//struct SkinFilter : public cls::IFilter
struct PhysXJoint : public cls::IProcedural
{
	virtual void Render(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		)
	{
		if( strcmp( render->renderType(), "PhysX")!=0)
			return;

		cls::PS type;
		if( !render->GetParameter("physX::type", type))
		{
			printf("PhysXPrimitive cant find parameter physX::type\n");
			return;
		}
		cls::PS actor0, actor1;
		if( !render->GetParameter("physX::actor0", actor0))
		{
			printf("PhysXPrimitive cant find parameter physX::actor0\n");
			return;
		}
		if( !render->GetParameter("physX::actor1", actor1))
		{
			printf("PhysXPrimitive cant find parameter physX::actor1\n");
			return;
		}

		renderImplPhysX* phisX = (renderImplPhysX*)render;

		Math::Matrix4f pos = phisX->getGlobalTransform();
		pos[3]*=phisX->getScale();
		pos[3][3] = 1;

		bool enableMotor = false;
		float maxMotorSpeed=0, maxMotorForce=0;
		bool freeSpin=true;
		render->GetParameter("physX::enableMotor", enableMotor);
		render->GetParameter("physX::maxMotorSpeed", maxMotorSpeed);
		render->GetParameter("physX::maxMotorForce", maxMotorForce);
		render->GetParameter("physX::freeMotorSpin", freeSpin);

		bool springEnable = false;
		float springValue=0, springDamper=0, springTarget=0;
		render->GetParameter("physX::springEnable", springEnable);
		render->GetParameter("physX::springValue",  springValue);
		render->GetParameter("physX::springDamper", springDamper);
		render->GetParameter("physX::springTarget", springTarget);

		bool limitEnable = false;
		float limitMin=0, limitMax=0;
		render->GetParameter("physX::limitEnable", limitEnable);
		render->GetParameter("physX::limitMin",  limitMin);
		render->GetParameter("physX::limitMax", limitMax);

		Math::Vec3f axis(pos[2][0], pos[2][1], pos[2][2]);

		if( strcmp(type.data(), "Revolute")==0)
		{
			NxRevoluteJointDesc* desc = new NxRevoluteJointDesc;

			if(enableMotor)
			{
				NxMotorDesc motorDesc;
				motorDesc.velTarget = maxMotorSpeed;
				motorDesc.maxForce = maxMotorForce;
				motorDesc.freeSpin = freeSpin;
				desc->motor = motorDesc;
				desc->flags |= NX_RJF_MOTOR_ENABLED;
			}

			if( springEnable)
			{
				desc->spring = NxSpringDesc(springValue, springDamper, springTarget);
				desc->flags |= NX_RJF_SPRING_ENABLED;
			}
			if( limitEnable)
			{
				desc->limit.low.value = limitMin;
				desc->limit.high.value = limitMax;
				desc->flags |= NX_SJF_TWIST_LIMIT_ENABLED;
			}

			phisX->AddJoint(desc, Math::Vec3f(pos[3][0], pos[3][1], pos[3][2]), axis, actor0.data(), actor1.data());
		}
		if( strcmp(type.data(), "Spherical")==0)
		{
			NxSphericalJointDesc* desc = new NxSphericalJointDesc;

			// swingSpring
			bool springEnable = false;
			float springValue=0, springDamper=0, springTarget=0;
			render->GetParameter("physX::springEnable", springEnable);
			render->GetParameter("physX::springValue",  springValue);
			render->GetParameter("physX::springDamper", springDamper);
			render->GetParameter("physX::springTarget", springTarget);
			if( springEnable)
			{
				desc->swingSpring = NxSpringDesc(springValue, springDamper, springTarget);
				desc->flags |= NX_SJF_SWING_SPRING_ENABLED;
//				desc->twistSpring = NxSpringDesc(springValue, springDamper, springTarget);
//				desc->flags |= NX_SJF_TWIST_SPRING_ENABLED;
			}
			if( limitEnable)
			{
				desc->swingLimit.value = limitMax;
//				desc->swingLimit.high.value = limitMax;
				desc->flags |= NX_SJF_SWING_LIMIT_ENABLED;
			}

			{
				desc->twistLimit.low.value = -0.01f;
				desc->twistLimit.high.value = 0.01f;
				desc->flags |= NX_SJF_TWIST_LIMIT_ENABLED;
			}


//			desc->setGlobalAnchor(NxVec3(pos[3][0], pos[3][1], pos[3][2]));
//			desc->setGlobalAxis(NxVec3(0, 0, 1));
			phisX->AddJoint(desc, Math::Vec3f(pos[3][0], pos[3][1], pos[3][2]), axis, actor0.data(), actor1.data());
		}
	}
};

//struct SkinFilter : public cls::IFilter
struct PhysXShape : public cls::IProcedural
{
	virtual void Render(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		)
	{
		if( strcmp( render->renderType(), "PhysX")!=0)
			return;

		cls::PS type;
		if( !render->GetParameter("physX::type", type))
		{
			printf("PhysXPrimitive cant find parameter physX::type\n");
			return;
		}
		renderImplPhysX* phisX = (renderImplPhysX*)render;

		bool collidable = true;
		render->GetParameter("physX::collidable", collidable);
		float density = 1;
		render->GetParameter("physX::density", density);

		// ��� ���������
		Math::Matrix4f pos = phisX->getLocalTransform();

		float scale = phisX->getScale();

		std::string name;
		render->GetAttribute("name", name);

		if( strcmp(type.data(), "Box")==0)
		{
			Math::Vec3f _boxDim;
			if( !render->GetParameter("physX::boxDim", _boxDim))
			{
				printf("PhysXPrimitive cant find parameter physX::boxDim\n");
				return;
			}
			
			NxVec3 boxDim(_boxDim.data());

			// The actor has one shape, a box
			NxBoxShapeDesc* boxDesc = new NxBoxShapeDesc;

			if( !collidable)
				boxDesc->shapeFlags |= NX_SF_DISABLE_COLLISION;
			boxDesc->density = density;

			boxDim.x *= pos[0].length()*scale; 
			boxDim.y *= pos[1].length()*scale; 
			boxDim.z *= pos[2].length()*scale; 
			boxDesc->dimensions.set(boxDim.x,boxDim.y,boxDim.z);
			boxDesc->localPose = phisX->Mat44toNxMat34(pos);

			boxDesc->name = strdup(name.c_str());

//			printf("boxDim = %f, %f, %f scale=%f\n", _boxDim.x, _boxDim.y, _boxDim.z, scale);
			phisX->AddShape(boxDesc);
		}
		if( strcmp(type.data(), "Capsule")==0)
		{
			float height, radius;
			if( !render->GetParameter("physX::height", height))
			{
				printf("PhysXPrimitive cant find parameter physX::height\n");
				return;
			}
			if( !render->GetParameter("physX::radius", radius))
			{
				printf("PhysXPrimitive cant find parameter physX::radius\n");
				return;
			}

			NxCapsuleShapeDesc* capsuleDesc = new NxCapsuleShapeDesc;

			if( !collidable)
				capsuleDesc->shapeFlags |= NX_SF_DISABLE_COLLISION;
			capsuleDesc->density = density;

			capsuleDesc->height = height*scale;
			capsuleDesc->radius = radius*scale;

			capsuleDesc->radius *= (pos[0].length()+pos[2].length())/2; 
			capsuleDesc->height *= pos[1].length(); 
			capsuleDesc->localPose = phisX->Mat44toNxMat34(pos);
			phisX->AddShape(capsuleDesc);
		}
		if( strcmp(type.data(), "Mesh")==0)
		{
            bool convex = true;
			render->GetParameter("physX::convex", convex);

			cls::PA<float> points;
			render->GetParameter("physX::points", points);
			int numPoints = points.size()/3;

			cls::PA<int> faces;
			render->GetParameter("physX::faces", faces);
			int numFaces = faces.size()/3;

			for( int i = 0; i < points.size(); i++ )
			{
				points[i] *= scale;
			}

			NxInitCooking();

			if( convex )
			{
				NxConvexMeshDesc convexDesc;
				convexDesc.numVertices			= numPoints;
				convexDesc.pointStrideBytes		= sizeof(float)*3;
				convexDesc.points				= &points[0];
				convexDesc.flags				= NX_CF_COMPUTE_CONVEX;

				NxConvexShapeDesc* convexShapeDesc = new NxConvexShapeDesc;

				// Cooking from memory
				MemoryWriteBuffer buf;
				bool status = NxCookConvexMesh(convexDesc, buf);
				convexShapeDesc->meshData = phisX->gScene->getPhysicsSDK().createConvexMesh(MemoryReadBuffer(buf.data));

				phisX->AddShape(convexShapeDesc);
			}
			else
			{
				NxTriangleMeshDesc triangleDesc;
				triangleDesc.numVertices = numPoints;
				triangleDesc.numTriangles = numFaces;
				triangleDesc.pointStrideBytes = sizeof(float)*3;
				triangleDesc.triangleStrideBytes = sizeof(int)*3;
				triangleDesc.points = &points[0];
				triangleDesc.triangles = &faces[0];
				triangleDesc.flags = 0;

				NxTriangleMeshShapeDesc* triangleMeshShapeDesc = new NxTriangleMeshShapeDesc;

				// Cooking from memory
				MemoryWriteBuffer buf;
				bool status = NxCookTriangleMesh(triangleDesc, buf);
				triangleMeshShapeDesc->meshData = phisX->gScene->getPhysicsSDK().createTriangleMesh(MemoryReadBuffer(buf.data));

				phisX->AddShape(triangleMeshShapeDesc);
			}
		}
		if( strcmp(type.data(), "Void")==0)
		{
			NxBoxShapeDesc* boxDesc = new NxBoxShapeDesc;

			NxVec3 boxDim(0.0001f, 0.0001f, 0.0001f);
			boxDesc->dimensions.set(boxDim.x,boxDim.y,boxDim.z);
			boxDesc->localPose = phisX->Mat44toNxMat34(pos);
			boxDesc->shapeFlags |= NX_SF_DISABLE_COLLISION;
			phisX->AddShape(boxDesc);
		}
	}
};

//struct SkinFilter : public cls::IFilter
struct PhysXActor : public cls::IProcedural
{
	bool bStart;
	PhysXActor(bool bStart)
	{
		this->bStart = bStart;
	}
	virtual void Render(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		)
	{
		if( strcmp( render->renderType(), "PhysX")!=0)
			return;

		renderImplPhysX* phisX = (renderImplPhysX*)render;

		phisX->GetAttribute( "PhysX::Camera", phisX->cameraMatrix );

		if( bStart)
		{
			// ������ �������� ������
			phisX->BeginActor("");
		}
		else
		{
			phisX->EndActor();
		}	
	}
};

struct PhysXCloth : public cls::IProcedural
{
	virtual void Render(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		)
	{
		if( strcmp( render->renderType(), "PhysX")!=0)
			return;

		renderImplPhysX* phisX = (renderImplPhysX*)render;

		phisX->GetAttribute( "PhysX::Camera", phisX->cameraMatrix );
		phisX->GetAttribute( "PhysX::subSteps", phisX->subSteps );

		cls::P<Math::Vec3f> Constant;
		cls::P<Math::Vec3f> Noise;
		Constant = phisX->GetAttribute( "PhysX::fieldForce");
		Noise = phisX->GetAttribute( "PhysX::fieldNoise");
		if(!Constant.empty())
		{
			NxForceFieldDesc fieldDesc;
			fieldDesc.coordinates = NX_FFC_CARTESIAN;
			fieldDesc.constant = NxVec3(Constant.data()[0],Constant.data()[1],Constant.data()[2]);
			fieldDesc.noise = NxVec3(Noise.data()[0],Noise.data()[1],Noise.data()[2]);
			NxForceField *pForceField; 
			pForceField = phisX->gScene->createForceField(fieldDesc);
			NxBoxForceFieldShapeDesc boxDesc;
			boxDesc.dimensions = NxVec3(1000000,1000000,1000000);
			pForceField->createShape(boxDesc);
		}

		cls::PA<float> points;
		render->GetParameter("physX::points", points);
		int numPoints = points.size()/3;

		cls::PA<int> faces;
		render->GetParameter("physX::faces", faces);
		int numFaces = faces.size()/3;

		for( int i = 0; i < points.size(); i++ )
		{
			points[i] *= phisX->scale;
		}

		NxClothMeshDesc clothMeshDesc;
		clothMeshDesc.numVertices = numPoints;
		clothMeshDesc.numTriangles = numFaces;
		clothMeshDesc.pointStrideBytes = sizeof(float)*3;
		clothMeshDesc.triangleStrideBytes = sizeof(int)*3;
		clothMeshDesc.points = &points[0];
		clothMeshDesc.triangles = &faces[0];
		clothMeshDesc.flags = 0;

		render_getfloat(thickness);
		render_getfloat(density);
		render_getfloat(bendingStiffness);
		render_getfloat(stretchingStiffness);
		render_getfloat(dampingCoefficient);
		render_getfloat(friction);
		render_getfloat(pressure);
		render_getfloat(tearFactor);
		render_getfloat(collisionResponseCoefficient);
		render_getfloat(attachmentResponseCoefficient);
		render_getfloat(attachmentTearFactor);
		render_getint(solverIterations);
		render_getfloat(sleepLinearVelocity);
		render_getfloat(relativeGridSpacing);

		render_getbool( pressureFlag );
		render_getbool( staticFlag );
		render_getbool( disableCollision );
		render_getbool( selfcollision );
		render_getbool( gravity );
		render_getbool( bending );
		render_getbool( bendingOrtho );
		render_getbool( damping );
		render_getbool( collisionTwoway );
		render_getbool( tearable );
		render_getbool( hardware );
		render_getbool( comdamping );

		NxClothDesc* clothDesc = new NxClothDesc;
		clothDesc->thickness = thickness;
		clothDesc->density = density;
		clothDesc->bendingStiffness = bendingStiffness;
		clothDesc->stretchingStiffness = stretchingStiffness;
		clothDesc->dampingCoefficient = dampingCoefficient;
		clothDesc->friction = friction;
		clothDesc->pressure = pressure;
		clothDesc->tearFactor = tearFactor;
		clothDesc->collisionResponseCoefficient = collisionResponseCoefficient;
		clothDesc->attachmentResponseCoefficient = attachmentResponseCoefficient;
		clothDesc->attachmentTearFactor = attachmentTearFactor;
		clothDesc->solverIterations = solverIterations;
		clothDesc->sleepLinearVelocity = sleepLinearVelocity;
		clothDesc->relativeGridSpacing = relativeGridSpacing;
		clothDesc->flags = 0;

		if ( pressureFlag ) clothDesc->flags |= NX_CLF_PRESSURE;
		if ( staticFlag ) clothDesc->flags |= NX_CLF_STATIC;
		if ( disableCollision ) clothDesc->flags |= NX_CLF_DISABLE_COLLISION;
		if ( selfcollision ) clothDesc->flags |= NX_CLF_SELFCOLLISION;
		//clothDesc->flags |= NX_CLF_VISUALIZATION;
		if ( gravity ) clothDesc->flags |= NX_CLF_GRAVITY;
		if ( bending ) clothDesc->flags |= NX_CLF_BENDING;
		if ( bendingOrtho ) clothDesc->flags |= NX_CLF_BENDING_ORTHO;
		if ( damping ) clothDesc->flags |= NX_CLF_DAMPING;
		if ( collisionTwoway ) clothDesc->flags |= NX_CLF_COLLISION_TWOWAY;
		//clothDesc->flags |= NX_CLF_TRIANGLE_COLLISION;
		if ( tearable ) clothDesc->flags |= NX_CLF_TEARABLE;
		if ( hardware ) clothDesc->flags |= NX_CLF_HARDWARE;
		if ( comdamping ) clothDesc->flags |= NX_CLF_COMDAMPING;
		//clothDesc->flags |= NX_CLF_VALIDBOUNDS;
		//clothDesc->flags |= NX_CLF_FLUID_COLLISION;

		NxMeshData meshData;
		NxU32 maxVertices = 2 * numPoints;
		meshData.verticesPosBegin = (NxVec3*)malloc(sizeof(NxVec3)*maxVertices);
		meshData.verticesNormalBegin = (NxVec3*)malloc(sizeof(NxVec3)*maxVertices);
		meshData.verticesPosByteStride = sizeof(NxVec3);
		meshData.verticesNormalByteStride = sizeof(NxVec3);
		meshData.maxVertices = maxVertices;
		meshData.numVerticesPtr = (NxU32*)malloc(sizeof(NxU32));

		NxU32 maxIndices = 3 * numFaces;
		meshData.indicesBegin = (NxU32*)malloc(sizeof(NxU32)*maxIndices);
		meshData.indicesByteStride = sizeof(NxU32);
		meshData.maxIndices = maxIndices;
		meshData.numIndicesPtr = (NxU32*)malloc(sizeof(NxU32));

		*meshData.numVerticesPtr = 0;
		*meshData.numIndicesPtr = 0;

		NxInitCooking();

		MemoryWriteBuffer buf;
		bool status = NxCookClothMesh(clothMeshDesc, buf);
		clothDesc->clothMesh = phisX->gScene->getPhysicsSDK().createClothMesh(MemoryReadBuffer(buf.data));
		clothDesc->meshData = meshData;

		phisX->AddCloth( clothDesc );
	}
};

PhysXShape physxshape;
PhysXCloth physxcloth;
PhysXJoint physxjoint;
PhysXActor physxactor_begin(true);
PhysXActor physxactor_end(false);

extern "C"
{
	__declspec(dllexport) cls::IProcedural* __cdecl Shape(cls::IRender* prman)
	{
		return &physxshape;
	}
	__declspec(dllexport) cls::IProcedural* __cdecl Cloth(cls::IRender* prman)
	{
		return &physxcloth;
	}
	__declspec(dllexport) cls::IProcedural* __cdecl BeginActor(cls::IRender* prman)
	{
		return &physxactor_begin;
	}
	__declspec(dllexport) cls::IProcedural* __cdecl EndActor(cls::IRender* prman)
	{
		return &physxactor_end;
	}
	__declspec(dllexport) cls::IProcedural* __cdecl Joint(cls::IRender* prman)
	{
		return &physxjoint;
	}
}

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    return TRUE;
}

