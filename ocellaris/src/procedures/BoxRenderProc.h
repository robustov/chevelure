#pragma once
#include "ocellaris/IProcedural.h"
#include "ocellaris/IRender.h"

//! @ingroup implement_group
//! \brief ��������� BoxRenderProc
//! 
//! ������ ������ ������ Box
//! ������� ��������� � ��������:
//! box #box
//! float #width
struct BoxRenderProc : public cls::IProcedural
{
	// ������ ����������
	virtual void Render(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		);

public:
	bool singleBox(cls::IRender* render);
	bool singleBox(cls::IRender* render, Math::Box3f& box);
};
