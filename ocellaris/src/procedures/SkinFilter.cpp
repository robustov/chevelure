#include "SkinFilter.h"
#include "ocellaris/renderFileImpl.h"
#include "ocellaris/renderCacheImpl.h"

SkinFilter skinFilter;

extern "C"
{
	// Call
	__declspec(dllexport) cls::IProcedural* __cdecl Skin(cls::IRender* prman)
	{
		return &skinFilter;
	}
}

// ������ ����������
void SkinFilter::Process(cls::IRender* render)
{
//printf("SkinFilter\n");
	Math::Matrix4f preTransform;
	cls::PA<Math::Matrix4f> inv_bindpos;
	cls::PA<Math::Matrix4f> pos;
	if( !render->GetAttribute("SkinFilter::preTransform", preTransform))
	{
		printf("SkinFilter cant find attribute SkinFilter::preTransform\n");
		return;
	}
	if( !render->GetAttribute("SkinFilter::inv_bindpos", inv_bindpos))
	{
		printf("SkinFilter cant find attribute SkinFilter::inv_bindpos\n");
		return;
	}
	if( !render->GetAttribute("SkinFilter::pos", pos))
	{
		printf("SkinFilter cant find attribute SkinFilter::pos\n");
		return;
	}
	if( pos.empty())
	{
		printf("SkinFilter cant find attribute SkinFilter::pos\n");
		return;
	}
	if( inv_bindpos.size() != pos.size())
	{
		printf("SkinFilter inv_bindpos.size(%d) != pos.size(%d)\n", inv_bindpos.size(), pos.size());
		return;
	}

	SkinFilterInstanceData ind;
	ind.preTransform = preTransform;
	ind.inv_bindpos = inv_bindpos;
	ind.pos = pos;

	// �������� ������� ����� ������
	cls::PA<float> W = render->GetParameter("SkinFilter::Weights");
	cls::PA<Math::Vec3f> P = render->GetParameter("P");
	if(P.empty())
		return;
	int jointcount = ind.pos.size();
	std::vector<Math::Matrix4f> joints(jointcount);
	for(int j=0; j<jointcount; j++)
	{
		Math::Matrix4f m = ind.inv_bindpos[j]*ind.pos[j];
		joints[j] = m;
	}
	if(W.size()!=P.size()*jointcount)
	{
		printf( "SkinFilter error data\n");
		return;
	}

	// newP
	cls::PA<Math::Vec3f> newP(cls::PI_VERTEX, P.size(), cls::PT_POINT);
	for(int v=0; v<P.size(); v++)
	{
		Math::Vec3f src = P[v];
		Math::Vec3f dst(0, 0, 0);

		for(int j=0; j<jointcount; j++)
		{
			float w = W[v*jointcount+j];
			if(w==0) continue;

			Math::Vec3f t = src;
			t = joints[j]*t;
			t = w*t;
			dst = dst + t;
		}
		newP[v] = dst;
	}
	render->Parameter("P", newP);
//	render->RemoveParameter("SkinFilter::Weights");


	cls::PA<Math::Vec3f> N = render->GetParameter("N");
	cls::PA<int> verts = render->GetParameter("mesh::verts");
	if(N.size()==verts.size())
	{
		cls::PA<Math::Vec3f> newN(cls::PI_PRIMITIVEVERTEX, N.size(), cls::PT_NORMAL);
		for(int fv=0; fv<verts.size(); fv++)
		{
			int v = verts[fv];
			Math::Vec3f src = N[fv];
			Math::Vec3f dst(0, 0, 0);
			for(int j=0; j<jointcount; j++)
			{
				float w = W[v*jointcount+j];
				if(w==0) continue;
				Math::Vec3f t = src;
				t = Math::Matrix4As3f(joints[j])*t;
				t = w*t;
				dst = dst + t;
			}
//			dst = -dst.normalized();
			newN[fv] = dst;
		}
		render->Parameter("N", newN);
	}
}

