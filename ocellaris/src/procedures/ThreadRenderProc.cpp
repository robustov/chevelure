#include "ocellaris/IProcedural.h"
#include "ocellaris/IRender.h"
#include "math/spline.h"
#include "math/rotation3.h"
#include "math/perlinNoise.h"

struct ThreadRenderData
{
	ThreadRenderData();
	ThreadRenderData(const ThreadRenderData& arg);

	cls::PA<Math::Vec3f> meshP;
	cls::PA<Math::Vec3f> meshPfreeze;
	cls::PA<int> meshnverts;
	cls::PA<int> meshloops;
	cls::PA<int> meshiverts;
	cls::PA<float> meshs, mesht;

	cls::PA<Math::Vec3f> curveP;
	cls::PA<int> curvenverts;
	float curveW;

	bool RenderMesh(
		cls::IRender* render
		);
	bool RenderHairs(
		cls::IRender* render
		);
};

//! @ingroup implement_group
//! \brief ��������� ThreadRenderProc
//! 
//! ������ ����
//! ������� ��������� � ��������:
//! P - �������
//! N - �������
//! T - tangent
struct ThreadRenderProc : public cls::IProcedural
{
	// ������ ����������
	virtual void Render(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		);

protected:


	bool TestRender(
		cls::IRender* render
		);
	bool BuildThread(
		cls::IRender* render, 
		ThreadRenderData& data
		);

	void buildSpline( 
		Math::Spline3f& spline, 
		cls::PA<Math::Vec3f>& P
		);

	float buildVerts(
		Math::Spline3f& spline, 
		float step, 
		std::vector<float>& params, 
		std::vector<float>& lenghts);

//	bool single(cls::IRender* render);
};

ThreadRenderProc threadrenderproc;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IProcedural* __cdecl Thread(cls::IRender* prman)
	{
		return &threadrenderproc;
	}
}

inline float rnd()
{
	return 2*rand()/(float)RAND_MAX - 1;
}

ThreadRenderData::ThreadRenderData():
	meshP(cls::PI_VERTEX, 0, cls::PT_POINT),
	meshPfreeze(cls::PI_VERTEX, 0, cls::PT_POINT),
	meshs(cls::PI_PRIMITIVEVERTEX),
	mesht(cls::PI_PRIMITIVEVERTEX),
	meshnverts(cls::PI_PRIMITIVE),
	meshloops(cls::PI_PRIMITIVE),
	meshiverts(cls::PI_PRIMITIVEVERTEX),
	curveP(cls::PI_VERTEX, 0, cls::PT_POINT),
	curvenverts(cls::PI_PRIMITIVE)
{
};

ThreadRenderData::ThreadRenderData(const ThreadRenderData& arg):
	meshP(cls::PI_VERTEX, 0, cls::PT_POINT),
	meshPfreeze(cls::PI_VERTEX, 0, cls::PT_POINT),
	meshs(cls::PI_PRIMITIVEVERTEX),
	mesht(cls::PI_PRIMITIVEVERTEX),
	meshnverts(cls::PI_PRIMITIVE),
	meshloops(cls::PI_PRIMITIVE),
	meshiverts(cls::PI_PRIMITIVEVERTEX),
	curveP(cls::PI_VERTEX, 0, cls::PT_POINT),
	curvenverts(cls::PI_PRIMITIVE)
{
}

// ������ ����������
void ThreadRenderProc::Render(
	cls::IRender* render, 
	int motionBlurSamples, 
	float* motionBlurTimes
	)
{
	if( strcmp(render->renderType(), "opengl")==0)
	{
		// ��� ��������
		cls::PA<int> nverts(cls::PI_PRIMITIVE, 1);
		cls::PA<Math::Vec3f> P = render->GetParameter("P");
		if( P.empty()) return;
		nverts[0] = P.size();

		render->Parameter("#width", 0.1f);	// constantwidth ???

		render->Parameter("#curves::interpolation", "linear");
		render->Parameter("#curves::wrap", "nonperiodic");
		render->Parameter("#curves::nverts", nverts);
		render->SystemCall(cls::SC_CURVES);
		return;
	}
	bool bTest = false;
	render->GetParameter("@Thread::test", bTest);
	if(bTest)
	{
		if( !motionBlurSamples)
		{
			TestRender(render);
			render->SystemCall(cls::SC_CURVES);
		}
		else
		{
			std::vector<ThreadRenderData> data(motionBlurSamples);
			for(int i=0; i<motionBlurSamples; i++)
			{
				render->SetCurrentMotionPhase(motionBlurTimes[i]);
				if( !TestRender(render))
				{
					render->SetCurrentMotionPhase(cls::nonBlurValue);
					return;
				}
			}
			render->SetCurrentMotionPhase(cls::nonBlurValue);
			render->SystemCall(cls::SC_CURVES);
		}
		return;
	}

	bool bHairs = true, bFibres = true;
	render->GetParameter("@Hairs", bHairs);
	render->GetParameter("@Fibres", bFibres);

	if( !motionBlurSamples)
	{
		ThreadRenderData data;
		if( !BuildThread(render, data))
			return;

		if( bFibres)
		{
			render->PushAttributes();
			data.RenderMesh(render);
			render->SystemCall(cls::SC_MESH);
			render->PopAttributes();
		}
		if( bHairs && !data.curveP.empty())
		{
			render->PushAttributes();
			data.RenderHairs(render);
			render->SystemCall(cls::SC_CURVES);
			render->PopAttributes();
		}
	}
	else
	{
		std::vector<ThreadRenderData> data(motionBlurSamples);
		for(int i=0; i<motionBlurSamples; i++)
		{
			render->SetCurrentMotionPhase(motionBlurTimes[i]);
			if( !BuildThread(render, data[i]))
			{
				render->SetCurrentMotionPhase(cls::nonBlurValue);
				return;
			}
		}
		render->SetCurrentMotionPhase(cls::nonBlurValue);

		if( bFibres)
		{
			render->PushAttributes();
			render->MotionBegin();
			for(int i=0; i<motionBlurSamples; i++)
			{
				render->MotionPhaseBegin( motionBlurTimes[i]);
				if( !data[i].RenderMesh(render))
					return;
				render->MotionPhaseEnd();
			}
			render->SystemCall(cls::SC_MESH);
			render->MotionEnd();
			render->PopAttributes();
		}
		if( bHairs && !data[0].curveP.empty())
		{
//			render->Parameter("@basis", "b-spline");
			
			render->PushAttributes();
			render->MotionBegin();
			for(int i=0; i<motionBlurSamples; i++)
			{
				render->MotionPhaseBegin( motionBlurTimes[i]);
				if( !data[i].RenderHairs(render))
					return;
				render->MotionPhaseEnd();
			}
			render->SystemCall(cls::SC_CURVES);
			render->MotionEnd();
			render->PopAttributes();
		}
	}

	/*/
	if( !motionBlurSamples)
	{
		if( !Hairs(render))
			return;
		render->SystemCall(cls::SC_CURVES);
	}
	else
	{
		for(int i=0; i<motionBlurSamples; i++)
		{
			render->MotionPhaseBegin( motionBlurTimes[i]);
			if( !Hairs(render))
				return;
			render->MotionPhaseEnd();
		}
		render->SystemCall(cls::SC_CURVES);
	}
	//*/
}


// ��������� ������
void ThreadRenderProc::buildSpline(Math::Spline3f& spline, cls::PA<Math::Vec3f>& P)
{
	std::vector<Math::Vec3f> knots;
	knots.resize(P.size()+4);
	int k=0;
	knots[k++] = knots[k++] = P[0];
	for(int i=0; i<(int)P.size(); i++)
		knots[k++] = P[i];
	knots[k++] = knots[k++] = P[P.size()-1];

	spline.setBasis(spline.BSpline);
	spline.setKnots(&knots[0], knots.size(), false);
}

float ThreadRenderProc::buildVerts(Math::Spline3f& spline, float step, std::vector<float>& params, std::vector<float>& lenghts)
{
	double fulllenght = 0;
	{
		// ����� ��������� �� ���� ������
		int divides = 10;
		int vc = (spline.knots.size()-1)*divides + 1;

		Math::Vec3f prev_pt(0);
		for(int v=0; v<vc; v++ )
		{
			float param = v/(float)(vc+1);
			Math::Vec3f pt = spline.getValue(param);
			if(v!=0)
			{
				fulllenght+=(prev_pt-pt).length();
			}
			prev_pt = pt;
		}
	}
	float count = (float)(fulllenght/step + 1);
	count *= 2;
	params.reserve((int)count);
	lenghts.reserve((int)count);

	float paramstep = 1/count;
	paramstep *= 0.1f;

	fulllenght = 0;
	float param = 0;
	Math::Vec3f prev_pt = spline.getValue(param);
	params.push_back(param);
	lenghts.push_back((float)fulllenght);

	for(;;)
	{
		double dl = 0;
		for( ; param<1; )
		{
			// ��������� ���� dl<step;
			param += paramstep;
			Math::Vec3f pt = spline.getValue( (float)param);
			double l = (prev_pt-pt).length();
			dl += l;
			fulllenght += l;
			prev_pt = pt;
			if( dl>=step)
				break;
		}
		if(param>1) param=1;
		params.push_back(param);
		lenghts.push_back((float)fulllenght);
//printf("%d: %f, %f\n", params.size()-1, params.back(), lenghts.back());
		if( param==1)
		{
			spline.getValue(param);
			spline.getTangent(param);
			break;
		}
	}
	return (float)fulllenght;
}

// ������ ����������
bool ThreadRenderProc::TestRender(
	cls::IRender* render
	)
{
	cls::PA<Math::Vec3f> P = render->GetParameter("P");
	if(P.empty())
		return false;

	float twistStep = 0.7f;
	render->GetParameter("Thread::twistStep", twistStep);

	Math::Spline3f spline;
	this->buildSpline(spline, P);
	std::vector<float> params, lenght;
	float length = (float)this->buildVerts(spline, twistStep/4, params, lenght);


	int vc = params.size();
//	printf("verts %d\n", vc);

	cls::PA<Math::Vec3f> _P(cls::PI_VERTEX, vc, cls::PT_POINT);
	for( int i=0; i<vc; i++)
	{
		float param = params[i];
		Math::Vec3f p = spline.getValue(param);
		_P[i] = p;
	}
	float W = 0.1f;
	render->Parameter("#width", W);	// constantwidth ???
	render->Parameter("P", cls::PT_POINT, _P);

	cls::PA<int> nvert(cls::PI_PRIMITIVE, 1); 
	nvert[0] = _P.size();
	render->Parameter("#curves::interpolation", "linear");
	render->Parameter("#curves::wrap", "nonperiodic");
	render->Parameter("#curves::nverts", nvert);
	return true;
}

bool ThreadRenderProc::BuildThread(
	cls::IRender* render, 
	ThreadRenderData& data
	)
{
	float width = 0.1f;
	float fiberWidth = 0.03f;
	float widthJitter = 1;
	float widthJitterStep = 0.5;
//	float fiberWidthJitter = 0.5;
//	float fiberWidthFreq = 1;
	float twistStep = 0.7f;
	float fiberTwist = 1;
	int fiberCount = 6;
//	int divides = 10;
	int seed = 0;
	data.curveW = 0.01f;
	int id=0;

	// HAIRS
	int hairsCount = 70;
	int vertsInHair = 4;
	float hairlenght = 0.1f;
	float scragle = 0.1f;
	float hairLenghtJitter = 0;
	int vertpertwist = 10;
	float scale = 1;

	render->GetParameter("@Thread::id", id);

	render->GetParameter("Thread::scale", scale);
	render->GetParameter("Thread::hairsCount", hairsCount);
	render->GetParameter("Thread::hairVerts", vertsInHair);
	render->GetParameter("Thread::hairLenght", hairlenght);
	render->GetParameter("Thread::hairScraggle", scragle);
	render->GetParameter("Thread::hairWidth", data.curveW);
	render->GetParameter("Thread::hairLenghtJitter", hairLenghtJitter);

	render->GetParameter("Thread::width", width);
	render->GetParameter("Thread::fiberWidth", fiberWidth);
	render->GetParameter("Thread::fiberTwist", fiberTwist);
	render->GetParameter("Thread::widthJitter", widthJitter);
	render->GetParameter("Thread::widthJitterStep", widthJitterStep);
	render->GetParameter("Thread::twistStep", twistStep);
	render->GetParameter("Thread::fiberCount", fiberCount);
//	render->GetParameter("Thread::subdive", divides);
	render->GetParameter("@Thread::vertpertwist", vertpertwist);
	render->GetParameter("@Thread::seed", seed);

	hairlenght	*= scale;
	data.curveW	*= scale;
	width		*= scale;
	fiberWidth  *= scale;
	twistStep	*= scale;

	scragle *= hairlenght;
	fiberWidth *= 0.5f;
	width *= 0.5f;

	cls::PA<Math::Vec3f> P = render->GetParameter("P");
	if(P.empty())
		return false;

	cls::PA<Math::Vec3f> Pfreeze = render->GetParameter("#Pfreeze");
	cls::PA<Math::Vec3f> N = render->GetParameter("#N");

	Math::Spline3f spline;
	buildSpline(spline, P);

	Math::Spline3f splineFreeze;
	if(!Pfreeze.empty())
		buildSpline(splineFreeze, Pfreeze);
	else
		printf("ThreadRenderProc: Pfreeze empty!!!\n");
		

	std::vector<float> params, lenghts;
	if(!Pfreeze.empty())
		this->buildVerts(splineFreeze, twistStep/vertpertwist, params, lenghts);
	else
		this->buildVerts(spline, twistStep/vertpertwist, params, lenghts);

//	printf("ThreadRenderProc: %d verts\n", params.size());


	Math::Spline3f splineN;
	if(!N.empty())
		buildSpline(splineN, N);

	int vc = params.size();
	int seg = vc - 1;
//	int perSegment = hairsCount/vc;
//	cls::PA<Math::Vec3f> _P(cls::PI_VERTEX, 0, cls::PT_POINT);
	data.meshP.reserve( vc*4*fiberCount);

	if(Pfreeze.size()==P.size())
	{
		data.meshPfreeze.reserve( vc*4*fiberCount);
	}
	if( id==0)
	{
		// ��������� id �� Pfreeze
		for( int i=0; i<P.size(); i++)
		{
			if(Pfreeze.size()==P.size())
				id+=*(int*)&Pfreeze[i];
			else
				id+=*(int*)&P[i];
		}
	}

	int nfaces = 4*(vc-1);
//	cls::PA<int> nverts(cls::PI_PRIMITIVE);
	data.meshnverts.reserve(nfaces*fiberCount);

//	cls::PA<int> loops(cls::PI_PRIMITIVE);
	data.meshloops.reserve(nfaces*fiberCount);

//	cls::PA<int> iverts(cls::PI_PRIMITIVEVERTEX);
	data.meshiverts.reserve(nfaces*4*fiberCount);

	data.meshs.reserve(nfaces*4*fiberCount);
	data.mesht.reserve(nfaces*4*fiberCount);

	// ����� ����� �� ��
	float hairpercm = (float)hairsCount;

	int fullHairCount=0;
	float fulllenght = 0;
	// ��������� ����� �����
	{
//		float lenghtfreeze = 0;
		Math::Vec3f prev_ptfreeze(0);
		for(int v=0; v<vc; v++ )
		{
			float param = params[v];
//			Math::Vec3f pt = spline.getValue(param);
//			Math::Vec3f ptfreeze = pt;
//			if(!Pfreeze.empty())
//				ptfreeze = splineFreeze.getValue(param);

			// �����
			float lenghtfreeze = lenghts[v];

			if(v!=0)
			{
				int last_count = fullHairCount;
				int count = (int)(lenghtfreeze*hairpercm);
				int perSegment = count-last_count;
				fullHairCount += perSegment;
			}
//			prev_ptfreeze = ptfreeze;
		}
		fulllenght = lenghts.back();
	}

	if( fullHairCount!=0)
	{
	//	cls::PA<Math::Vec3f> _P(cls::PI_VERTEX, 0, cls::PT_POINT);
		data.curveP.reserve( fiberCount*fullHairCount*vertsInHair);

	//	cls::PA<int> nverts(cls::PI_PRIMITIVE);
		data.curvenverts.reserve(fullHairCount*fiberCount);
	}

	float fiberAngleStep = (float)( 2*M_PI/(float)fiberCount);

	srand(seed+id);
//	printf("id=%d\n", id);
	for(int fiber=0; fiber<fiberCount; fiber++)
	{
		rand();rand();
		Math::PerlinNoise noise;
		noise.Init(rand());

		float fiberAngleShift = fiber*fiberAngleStep;

		float lenght = 0;
//		float lenghtfreeze = 0;

		int last_count = 0;
		int lastvert = data.meshP.size();
		Math::Vec3f prev_pt(0);
		Math::Vec3f prev_point(0);
		Math::Vec3f prev_ptfreeze(0);
		Math::Vec3f prev_normal(1, 0, 0);
		for(int v=0; v<vc; v++ )
		{
			float param = params[v];
			Math::Vec3f pt = spline.getValue(param);
			Math::Vec3f tangent = spline.getTangent(param);
			if(tangent==Math::Vec3f(0))
				tangent = Math::Vec3f(0, 1, 0);
			tangent.normalize();

			Math::Vec3f normal = prev_normal;
			if(!N.empty())
				normal = splineN.getValue(param);
			// ������� ������ ���� ������������ ��������
			normal = Math::cross( tangent, normal);
			normal = -Math::cross( tangent, normal).normalized();
			prev_normal = normal;

			Math::Vec3f ptfreeze = pt;
			if(!Pfreeze.empty())
				ptfreeze = splineFreeze.getValue(param);

			// �����
			if(v!=0)
			{
				lenght += (prev_pt - pt).length();
			}
			float lenghtfreeze = lenghts[v];

			// ����
			// ��� ??? ���� ����� ����� � ��������� ������ ���������
			// 
			float angle = lenghtfreeze/twistStep;
			angle = angle-floor(angle);
			angle *= (float)(2*M_PI);
			angle += fiberAngleShift;
			Math::Rot3f quaternion(angle, tangent);
			Math::Rot3f qtwist(angle*fiberTwist, tangent);
			Math::Vec3f offset = Math::rotate(quaternion, normal);

			// R
//			float fw = rand()/(float)RAND_MAX - 0.5f;
			float noisep = lenghtfreeze/widthJitterStep;
			float fw = noise.noise(id*0.3333f+(float)M_PI*fiber+noisep*0.1f, id*0.3333f+noisep, 0.57777f-noisep*0.088f);
			fw *= widthJitter;
			fw = __max(0.01f-1, fw);
			fw =  width*(1+fw);

			// offset
			offset = offset*fw;
			Math::Vec3f point = pt + offset;
			Math::Vec3f pointfreeze = ptfreeze + offset;

			// �������� (� ��������. �������)
//			Math::Rot3f qtwist = (fiberTwist>0)?quaternion:quaternion.inverted();
//			qtwist *= fabs(fiberTwist);
//			qtwist.normalize();
			Math::Vec3f normal1 = Math::rotate(qtwist, normal);
			normal1.normalize();
//			Math::Vec3f normal1 = normal;
			Math::Vec3f normal2 = Math::cross( tangent, normal1).normalized();

			// ������
			float w = fiberWidth;

			// MESH
			{
				normal1 *= w;
				normal2 *= w;

				// P
				data.meshP.push_back(point + normal1);
				data.meshP.push_back(point + normal2);
				data.meshP.push_back(point - normal1);
				data.meshP.push_back(point - normal2);

				if( Pfreeze.size()==P.size())
				{
					data.meshPfreeze.push_back(pointfreeze + normal1);
					data.meshPfreeze.push_back(pointfreeze + normal2);
					data.meshPfreeze.push_back(pointfreeze - normal1);
					data.meshPfreeze.push_back(pointfreeze - normal2);
				}
			}

			// HAIRS
			if(v!=0 && fullHairCount!=0)
			{
				int count = (int)(lenghtfreeze*hairpercm);
				int perSegment = count-last_count;
				for(int h=0; h<perSegment; h++)
				{
					float factor = rand()/(float)RAND_MAX;
					float tangent_a = (float)( 2*M_PI*rand()/(float)RAND_MAX);
					float inclination = 2*rand()/(float)RAND_MAX - 1;

					Math::Vec3f p = prev_point*factor + (1-factor)*point;
					Math::Rot3f quaternion(tangent_a, tangent);
					Math::Vec3f dir = Math::rotate(quaternion, normal);

					dir += inclination*tangent;
					dir.normalize();
					float thisHairLength = (1 - rnd()*hairLenghtJitter);
					if( thisHairLength<0.002f) thisHairLength=0.002f;
					thisHairLength *= hairlenght;
					dir *= thisHairLength;

					// ������� �����
					float segInv = 1/(float)(vertsInHair-1);
					for(int vih=0; vih<vertsInHair; vih++)
					{
						data.curveP.push_back(p);

						p += dir*segInv;
						dir.x += rnd()*scragle;
						dir.y += rnd()*scragle;
						dir.z += rnd()*scragle;
						dir.normalize();
						dir *= thisHairLength;
					}
					data.curvenverts.push_back(vertsInHair);
					last_count++;
				}
			}

			prev_pt = pt;
			prev_ptfreeze = ptfreeze;
			prev_point = point;
		}

		bool bInvert = true;
		for(int f=0; f<nfaces; f++ )
		{
			data.meshloops.push_back(1);
			data.meshnverts.push_back(4);
			if( (f&0x3) != 0x3)
			{
				if( !bInvert)
				{
					data.meshiverts.push_back( lastvert+f+0);
					data.meshiverts.push_back( lastvert+f+1);
					data.meshiverts.push_back( lastvert+f+5);
					data.meshiverts.push_back( lastvert+f+4);
				}
				else
				{
					data.meshiverts.push_back( lastvert+f+4);
					data.meshiverts.push_back( lastvert+f+5);
					data.meshiverts.push_back( lastvert+f+1);
					data.meshiverts.push_back( lastvert+f+0);
				}
			}
			else
			{
				if( !bInvert)
				{
					data.meshiverts.push_back( lastvert+f+0);
					data.meshiverts.push_back( lastvert+f+1-4);
					data.meshiverts.push_back( lastvert+f+5-4);
					data.meshiverts.push_back( lastvert+f+4);
				}
				else
				{
					data.meshiverts.push_back( lastvert+f+4);
					data.meshiverts.push_back( lastvert+f+5-4);
					data.meshiverts.push_back( lastvert+f+1-4);
					data.meshiverts.push_back( lastvert+f+0);
				}
			}
			if( data.meshs.reserve_size()>0 && data.mesht.reserve_size()>0)
			{
				int v = f/4;
				float t0 = v/(float)(vc-1);
				float t1 = (v+1)/(float)(vc-1);
				float s0 = (f&0x3)/(float)4;
				float s1 = s0 + 0.25f;
				data.meshs.push_back(s0);
				data.meshs.push_back(s1);
				data.meshs.push_back(s1);
				data.meshs.push_back(s0);
				data.mesht.push_back(1-t0);
				data.mesht.push_back(1-t0);
				data.mesht.push_back(1-t1);
				data.mesht.push_back(1-t1);
			}
		}
	}

	printf("ThreadRenderProc: %d mesh %d hairs\n", data.meshP.size(), data.curveP.size());

/*/
	render->Parameter("P", _P);
//			if( !N.empty())
//				render->Parameter("N", N);
//			if( !s.empty())
//				render->Parameter("s", s);
//			if( !t.empty())
//				render->Parameter("t", t);

	bool bCubic = true;
	if( bCubic)
	{
		render->Parameter("#subdiv::interpolateBoundary", true);
	}
	render->Parameter("mesh::interpolation", bCubic?"catmull-clark":"linear");
	render->Parameter("mesh::loops", loops);
	render->Parameter("mesh::nverts", nverts);
	render->Parameter("mesh::verts", iverts);
//	render->SystemCall(SC_MESH);
/*/

	return true;
}

/*/
bool ThreadRenderProc::Hairs(
	cls::IRender* render
	)
{
	float width = 0.1f;
	float fiberWidth = 0.03f;
	float widthJitter = 1;
	float widthJitterStep = 0.5;
//	float fiberWidthJitter = 0.5;
//	float fiberWidthFreq = 1;
	float twistStep = 0.7f;
	int fiberCount = 6;
	int divides = 10;
	int seed = 0;

	render->GetParameter("Thread::width", width);
	render->GetParameter("Thread::fiberWidth", fiberWidth);
	render->GetParameter("Thread::widthJitter", widthJitter);
	render->GetParameter("Thread::widthJitterStep", widthJitterStep);
	render->GetParameter("Thread::twistStep", twistStep);
	render->GetParameter("Thread::fiberCount", fiberCount);
	render->GetParameter("Thread::subdive", divides);
	render->GetParameter("Thread::seed", seed);
	fiberWidth *= 2;
	width *= 2;
	

	// HAIRS
	int perSegment = 10;
	int vertsInHair = 4;
	float hairlenght = 0.1;
	float scragle = 0.1*hairlenght;

	cls::PA<Math::Vec3f> P = render->GetParameter("P");
	if(P.empty())
		return false;

	cls::PA<Math::Vec3f> N = render->GetParameter("N");

	Math::Spline3f spline;
	buildSpline(spline, P);

	Math::Spline3f splineN;
	if(!N.empty())
		buildSpline(splineN, N);

	int vc = (P.size()-1)*divides + 1;
	int seg = vc - 1;
	cls::PA<Math::Vec3f> _P(cls::PI_VERTEX, 0, cls::PT_POINT);
	_P.reserve( fiberCount*seg*perSegment*vertsInHair);

	cls::PA<int> nverts(cls::PI_PRIMITIVE);
	nverts.reserve(seg*perSegment*fiberCount);

	float fiberAngleStep = (float)( 2*M_PI/(float)fiberCount);

	srand(seed);
	for(int fiber=0; fiber<fiberCount; fiber++)
	{
		rand();rand();
		Math::PerlinNoise noise;
		noise.Init(rand());

		float fiberAngleShift = fiber*fiberAngleStep;

		float lenght = 0;

		int lastvert = _P.size();
		Math::Vec3f prev_pt(0);
		Math::Vec3f prev_point(0);
		for(int v=0; v<vc; v++ )
		{
			float param = v/(float)(vc+1);
			Math::Vec3f pt = spline.getValue(param);
			Math::Vec3f tangent = spline.getTangent(param);

			Math::Vec3f normal = Math::cross( tangent, Math::Vec3f(1, 0, 0)).normalized();
			if(!N.empty())
				normal = splineN.getValue(param).normalized();

			// ����
			// ��� ??? ���� ����� ����� � ��������� ������ ���������
			// 
			float angle = lenght/twistStep;
			angle = angle-floor(angle);
			angle *= (float)(2*M_PI);
			angle += fiberAngleShift;
			Math::Rot3f quaternion(angle, tangent);

			// R
//			float fw = rand()/(float)RAND_MAX - 0.5f;
			float noisep = lenght/widthJitterStep;
			float fw = noise.noise(M_PI*fiber+noisep*0.1f, noisep, 0.57777f-noisep*0.088);
			fw *= widthJitter;
			fw = __max(0.01-1, fw);
			fw =  width*(1+fw);

			// offset
			Math::Vec3f offset = Math::rotate(quaternion, normal);
			offset = offset*fw;
			Math::Vec3f point = pt + offset;

			if(v!=0)
			{
				for(int h=0; h<perSegment; h++)
				{
					float factor = rand()/(float)RAND_MAX;
					float tangent_a = 2*M_PI*rand()/(float)RAND_MAX;
					float inclination = 2*rand()/(float)RAND_MAX - 1;

					Math::Vec3f p = prev_point*factor + (1-factor)*point;
					Math::Rot3f quaternion(tangent_a, tangent);
					Math::Vec3f dir = Math::rotate(quaternion, normal);

					dir += inclination*tangent;
					dir.normalize();
					dir *= hairlenght;

					// ������� �����
					float segInv = 1/(float)(vertsInHair-1);
					for(int vih=0; vih<vertsInHair; vih++)
					{
						_P.push_back(p);

						p += dir*segInv;
						dir.x += rnd()*scragle;
						dir.y += rnd()*scragle;
						dir.z += rnd()*scragle;
					}
					nverts.push_back(vertsInHair);
				}
			}

			// �����
			lenght += (prev_pt-pt).length();
			prev_pt = pt;
			prev_point = point;
		}
	}

	render->Parameter("P", _P);

	float W = 0.1f;
	render->Parameter("#width", W);	// constantwidth ???

	render->Parameter("#curves::interpolation", "linear");
	render->Parameter("#curves::wrap", "nonperiodic");
	render->Parameter("#curves::nverts", nverts);
	
//	render->SystemCall(SC_CURVE);

	return true;
}
/*/

bool ThreadRenderData::RenderMesh(
	cls::IRender* render
	)
{
	render->Parameter("P", meshP);
	if( meshPfreeze.size()==meshP.size())
		render->Parameter("__Pref", meshPfreeze);
	
//			if( !N.empty())
//				render->Parameter("N", N);
	if( !meshs.empty())
		render->Parameter("s", meshs);
	if( !mesht.empty())
		render->Parameter("t", mesht);

	bool bCubic = true;
	if( bCubic)
	{
		render->Parameter("#subdiv::interpolateBoundary", true);
	}
	render->Parameter("mesh::interpolation", bCubic?"catmull-clark":"linear");
	render->Parameter("mesh::loops", meshloops);
	render->Parameter("mesh::nverts", meshnverts);
	render->Parameter("mesh::verts", meshiverts);

	return true;
//	render->SystemCall(SC_MESH);
}

bool ThreadRenderData::RenderHairs(
	cls::IRender* render
	)
{
	render->Parameter("P", curveP);

	render->Parameter("#width", curveW);	// constantwidth ???

	render->Parameter("#curves::interpolation", "linear");
	render->Parameter("#curves::wrap", "nonperiodic");
	render->Parameter("#curves::nverts", curvenverts);
	return true;
}
