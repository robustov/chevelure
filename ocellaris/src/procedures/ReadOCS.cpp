#include "ocellaris/IProcedural.h"
#include "ocellaris/renderFileImpl.h"
#include "ocellaris/renderCacheImpl.h"
#include "ocellaris/renderBlurParser.h"
#include "Util/misc_create_directory.h"

//! @ingroup implement_group
//! \brief ��������� ReadFile
//! 
//! �������� � ��������� ocs ����
//! ������� ��������� � ��������:
//! string filename - ��� ocs �����
//! global::ignoreBlur (���������� ������� ocs �����), ���� true �� ����� ��������� ���������� ���� 
//! 
/*/
// ������ 1:
MotionBegin;
	MotionPhaseBegin 0;
		Parameter filename = "file1.ocs";
	MotionPhaseEnd;
	MotionPhaseBegin 1;
		Parameter filename = "file2.ocs";
	MotionPhaseEnd;
	ReadFile;
MotionEnd;

// ������ 2:
MotionBegin;
	MotionPhaseBegin 0;
		Attribute global::animation = 0.1;
	MotionPhaseEnd;
	MotionPhaseBegin 1;
		Attribute global::animation = 0.2;
	MotionPhaseEnd;
	Parameter filename = "file.ocs";
	ReadFile;
MotionEnd;
/*/

struct ReadOCSProc : public cls::IProcedural
{
	/// ����� ��� ����������� (Call)
	virtual void Process(
		cls::IRender* render
		);

	// ������ ����������
	virtual void Render(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		);

};


ReadOCSProc readOCSProc;
extern "C"
{
	// RenderCall
	__declspec(dllexport) cls::IProcedural* __cdecl ReadOCS(cls::IRender* prman)
	{
		return &readOCSProc;
	}
}

struct cacheItem
{
//	Math::Box3f box;
	bool ignoreBlur;
	cls::renderCacheImpl<cls::CacheStorageSimple> cache;
	time_t filetime;
	cacheItem()
	{
		ignoreBlur = false;
	}
};

typedef std::map <std::string, cacheItem> files_t;

files_t files;

cacheItem* getFileCache(
	cls::IRender* render)
{
	{
		bool bRenderFiles = false;
		render->GetAttribute("viewport::bRenderFiles", bRenderFiles);
		if( !bRenderFiles)
		{
			const char* renderType = render->renderType();
			if( strcmp(renderType, "opengl")==0)
			{
				return NULL;
			}
		}
	}

	cls::Param src = render->GetParameter("filename");
	cls::PS filename = src;
	if( filename.empty())
		return NULL;

	bool bReadGlobals = false;
	render->GetParameter("readglobals", bReadGlobals);

	time_t filetime = Util::getFileTime(filename.data());

	files_t::iterator it = files.find(filename.data());
	if( it==files.end() || it->second.filetime != filetime)
	{
		if( it==files.end())
			it = files.insert( files_t::value_type(filename.data(), cacheItem())).first;

		cacheItem& cacheItem = it->second;
		cacheItem.filetime = filetime;
		cacheItem.cache.clear();
		cls::IRender* startrender = NULL;
		if(bReadGlobals) startrender = &cacheItem.cache;
		
		cls::renderFileImpl in;
		if( !in.openForRead(filename.data(), startrender))
		{
			std::string seachpath;
			render->GetAttribute("file::seachpath", seachpath);
			seachpath += "/";
			seachpath += filename.data();
			if( !in.openForRead(seachpath.data(), startrender))
			{
				fprintf(stderr, "ReadOCSProc: Cant read %s\n", filename.data());
				return NULL;
			}
		}

//		in.GetAttribute("file::box", cacheItem.box);
		in.GetAttribute("file::ignoreBlur", cacheItem.ignoreBlur);
		in.Render( &cacheItem.cache);
printf("ReadOCS %s\n", filename.data());
	}
	return &it->second;
}

/// ����� ��� ����������� (Call)
void ReadOCSProc::Process(
	cls::IRender* render
	)
{
	// ���� ������ ����
	cacheItem* cache = getFileCache(render);
	if(cache) 
		cache->cache.Render(render);
}

// ������ ����������
// ���� ����������� � �������� �����
void ReadOCSProc::Render(
	cls::IRender* render, 
	int motionBlurSamples, 
	float* motionBlurTimes
	)
{
printf(".");
	render->PinTransform();

	if( !motionBlurSamples)
	{
		// ��� �����
		cacheItem* cache = getFileCache(render);
		if(cache) 
			cache->cache.Render(render);
		return;
	}
	else
	{
		// �������
		cacheItem* samecache = NULL;
		for(int i=0; i<motionBlurSamples; i++)
		{
			render->SetCurrentMotionPhase(motionBlurTimes[i]);

			cacheItem* cache = getFileCache(render);
			if( !cache)
			{
				// ������
				render->SetCurrentMotionPhase(cls::nonBlurValue);
				return;
			}
			// ���� � ����� ������ ���� file::ignoreBlur, �� ������� ������
			if(cache->ignoreBlur)
			{
				render->SetCurrentMotionPhase(cls::nonBlurValue);
				render->MotionBegin();
				render->MotionEnd();
				cache->cache.Render(render);
				return;
			}
			if( !samecache)
				samecache = cache;
			else
			{
				if( cache!=samecache)
					samecache = (cacheItem*)0xFFFFFFF;
			}
		}
		/*/
		if( samecache != (cacheItem*)0xFFFFFFF)
		{
			// �� ���� ����� (���� ��� ������)
			render->SetCurrentMotionPhase(cls::nonBlurValue);
			render->MotionBegin();
			render->MotionEnd();
			samecache->cache.Render(render);
			return;
		}
		/*/

		// ���������
		{
			// ������� ����
			cls::renderBlurParser<> blurParser;
			for(int i=0; i<motionBlurSamples; i++)
			{
				render->SetCurrentMotionPhase(motionBlurTimes[i]);
				cacheItem* cache = getFileCache(render);
				if( !cache) return;

				blurParser.SetCurrentPhase(motionBlurTimes[i]);
				cache->cache.Render(&blurParser);
			}
			render->SetCurrentMotionPhase(cls::nonBlurValue);
//			blurParser.storage.Dump();
			blurParser.Render(render);
		}
	}
}

