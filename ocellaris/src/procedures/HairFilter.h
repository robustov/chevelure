#pragma once
#include "ocellaris/IFilter.h"
#include "ocellaris/IRender.h"
#include <map>

struct HairFilterInstanceData
{
	Math::Matrix4f startTransform;
	cls::PA<Math::Matrix4f> transforms;
	cls::PA<float> parameters;
	float maxDot;
	cls::P<float> u, v;
	cls::P<int> hairid;
};

//! @ingroup implement_group
//! \brief ������ HairFilter
//! ���������� ��� � �������
//! 
struct Hair_Filter : public cls::IFilter
{
	Hair_Filter();

	//! ����� �������
	virtual bool Begin(
		cls::IRender* render, 
		const Math::Matrix4f& currentWorldTransform,
		int& id					// ���������� �� �������� �������
		);
	//! ������� ������ true ���� ���� ���������, ����� false
	virtual bool OnSystemCall(
		cls::enSystemCall instruction,
		cls::IRender* render,
		const Math::Matrix4f& currentWorldTransform,
		int id
		);
	//! ���� �������
	virtual void End(
		cls::IRender* render, 
		int id
		);

protected:
	int curid;
	std::map<int, HairFilterInstanceData> HairFilters;

	int getAffectIndexForVertex(
		const Math::Vec3f& src, 
		const HairFilterInstanceData& data, 
		float& factor);
};
