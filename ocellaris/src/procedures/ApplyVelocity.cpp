#include "ApplyVelocity.h"
#include "ocellaris/renderFileImpl.h"
#include "ocellaris/renderCacheImpl.h"

ApplyVelocityProc applyVelocityProc;

extern "C"
{
	// Call
	__declspec(dllexport) cls::IProcedural* __cdecl ApplyVelocity(cls::IRender* prman)
	{
		return &applyVelocityProc;
	}
}

// секция рендеринга
void ApplyVelocityProc::Process(cls::IRender* render)
{
	cls::PA<Math::Vec3f> P = render->GetParameter("P");
	cls::PA<Math::Vec3f> V = render->GetParameter("velocity");
	cls::P<float> phase = render->GetAttribute("#phase");

	if( phase.empty()) 
	{
//		fprintf(stderr, "ApplyVelocityProc. Phase attribute not found!\n");
		return;
	}
	if(P.size()==0)
	{
//		fprintf(stderr, "ApplyVelocityProc. P parameter not found!\n");
		return;
	}

	if(P.size()!=V.size())
	{
//		fprintf(stderr, "ApplyVelocityProc. V parameter empty or invalide size!\n");
		return;
	}

	for(int i=0; i<P.size(); i++)
	{
		P[i] += V[i]*phase.data();
	}
}
