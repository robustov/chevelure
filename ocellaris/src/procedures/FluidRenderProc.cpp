#include "ocellaris/IProcedural.h"
#include "ocellaris/IRender.h"
#include "ocellaris/renderCacheImpl.h"

struct FluidRenderProc : public cls::IProcedural
{
	// ������ ����������
	virtual void Render(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		);

public:
	bool singleRender(cls::IRender* render, cls::IRender* renderout);
};

FluidRenderProc fluidrenderproc;

inline float _rnd()
{
	float r = rand()/(float)RAND_MAX;
	return r;
}

extern "C"
{
	// Box
	__declspec(dllexport) cls::IProcedural* __cdecl Fluid(cls::IRender* prman)
	{
		return &fluidrenderproc;
	}
}

// ��� ������������
float calc_factor(
	int& nei_x, 
	float v_x, 
	int res_x)
{
	float faactor_x = 1;
	if( v_x<0.5f)
	{
//		if(nei_x>0)
		{
			nei_x += -1;
			faactor_x = v_x+0.5f;
		}
	}
	else
	{
//		if(nei_x<res_x-1)
		{
			nei_x += 1;
			faactor_x = -v_x+1.5f;
		}
	}
	return faactor_x;
}
/*/
float getArray3Val(float* array, Math::Vec3i& xyz, int* res)
{
	int myind = xyz.x + xyz.y*res[0] + xyz.z*res[0]*res[1];
	return array[myind];
}
/*/
float getArray3Val(float* array, int x, int y, int z, int* res)
{
	if(x<0 || x>=res[0])
		return 0;
	if(y<0 || y>=res[1])
		return 0;
	if(z<0 || z>=res[2])
		return 0;
	int myind = x + y*res[0] + z*res[0]*res[1];
	return array[myind];
}

// ������ ����������
// ���� ����������� � �������� �����
void FluidRenderProc::Render(
	cls::IRender* render, 
	int motionBlurSamples, 
	float* motionBlurTimes
	)
{
	if( !motionBlurSamples)
	{
		cls::renderCacheImpl<> renderout;
		if( !singleRender(render, &renderout))
			return;

		renderout.Render(render);
	}
	else
	{
		cls::renderCacheImpl<> renderout;
		for(int i=0; i<motionBlurSamples; i++)
		{
			render->SetCurrentMotionPhase(motionBlurTimes[i]);
			if( !singleRender(render, &renderout))
				return;
			break;
		}
		render->SetCurrentMotionPhase(cls::nonBlurValue);

		renderout.Render(render);
	}
}

bool FluidRenderProc::singleRender(
	cls::IRender* render, 
	cls::IRender* renderout
	)
{
	cls::PA<int> res   = render->GetParameter("#resolution");
	if( res.size()!=3)
		return false;
	int s = res[0]*res[1]*res[2];

	cls::PA<float> dim = render->GetParameter("#dimentions");
	if( dim.size()!=3)
		return false;

	float ds = 1;
	render->GetParameter("#densitySum", ds);

	cls::PA<float> density = render->GetParameter("#density");
	if( density.size()!=s)
		return false;

	float radius = 0.1f;
	render->GetParameter("#radius", radius);
	int particteCount = 10;
	render->GetParameter("#particteCount", particteCount);
	int seed = 1231;

	cls::PA<Math::Vec3f> P(cls::PI_VERTEX);
	P.reserve(s*particteCount);

	// ����������� ������
	Math::Vec3f celldim(dim[0]/res[0], dim[1]/res[1], dim[2]/res[2]);
	// ��������� ���������
	srand(seed);
	for( int z = 0; z<res[2]; z++)
	{
		for( int y = 0; y<res[1]; y++)
		{
			for( int x = 0; x<res[0]; x++)
			{
				int myind = x + y*res[0] + z*res[0]*res[1];
				float d = density[myind];

				Math::Vec3f corner(
					dim[0]*(x/(float)res[0]-0.5f), 
					dim[1]*(y/(float)res[1]-0.5f), 
					dim[2]*(z/(float)res[2]-0.5f) 
					);

				seed = rand();
				// ������� ��������?
				for( int p=0; p<particteCount; p++)
				{
					Math::Vec3f v(_rnd(), _rnd(), _rnd());

					// ������������ ���������
					Math::Vec3f faactor(1, 1, 1);
					Math::Vec3i nei(x, y, z);

					faactor.x = calc_factor(nei.x, v.x, res[0]);
					faactor.y = calc_factor(nei.y, v.y, res[1]);
					faactor.z = calc_factor(nei.z, v.z, res[2]);

					float dx = getArray3Val(density.data(), nei.x, y, z, res.data());
					float dy = getArray3Val(density.data(), x, nei.y, z, res.data());
					float dz = getArray3Val(density.data(), x, y, nei.z, res.data());
					float dd = 
						faactor.x*d + (1-faactor.x)*dx + 
						faactor.y*d + (1-faactor.y)*dy + 
						faactor.z*d + (1-faactor.z)*dz;
					dd = dd/3;

					float r = _rnd();
					if( r>dd) 
						continue;

					v.x *= celldim.x;
					v.y *= celldim.y;
					v.z *= celldim.z;

					P.push_back(corner+v);
				}
				srand(seed);
			}
		}
	}

	renderout->Parameter("#width", radius);
	renderout->Points(P);

	return true;
}
