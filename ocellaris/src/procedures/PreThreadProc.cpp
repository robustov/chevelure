#include "ocellaris/IProcedural.h"
#include "ocellaris/IRender.h"

// �� ����� ��� �����
struct PreThreadProc : public cls::IProcedural
{
	// ������ ����������
	virtual void Process(
		cls::IRender* render
		);
};

PreThreadProc prethreadproc;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IProcedural* __cdecl PreThread(cls::IRender* prman)
	{
		return &prethreadproc;
	}
}

inline float rnd()
{
	return 2*rand()/(float)RAND_MAX - 1;
}

void PreThreadProc::Process(
	cls::IRender* render
	)
{
}

