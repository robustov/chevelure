#pragma once
#include "ocellaris/IFilter.h"
#include "ocellaris/IRender.h"
#include <map>

struct SkinFilterInstanceData
{
	Math::Matrix4f preTransform;
	// �����
	cls::PA<Math::Matrix4f> inv_bindpos;
	cls::PA<Math::Matrix4f> pos;
};

//! @ingroup implement_group
//! \brief ������ SkinFilter
//! ���������� �� ������
//! 

//struct SkinFilter : public cls::IFilter
struct SkinFilter : public cls::IProcedural
{
	// ������ ����������
	virtual void Process(cls::IRender* render);

/*/
	virtual void Render(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		);
/*/

};
