#pragma once
#include "ocellaris/IProcedural.h"

//! @ingroup implement_group
//! \brief ��������� ������ ��������� �������� ��������
//! 
//! �������� P �� ������� velocity � ������� phase
//! ������� ��������� � ��������:
//! * vector[] P
//! * vector[] velocity
//! * attribute phase
//! 
//! 
struct ApplyVelocityProc : public cls::IProcedural
{
	// ������ ����������
	virtual void Process(cls::IRender* render);
};
