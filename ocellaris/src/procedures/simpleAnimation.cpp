#pragma once
#include "simpleAnimation.h"
#include "ocellaris/renderFileImpl.h"
#include "ocellaris/renderCacheImpl.h"
#include <math.h>

SimpleAnimationProc simpleAnimationProc;

extern "C"
{
	// Call
	__declspec(dllexport) cls::IProcedural* __cdecl SimpleAnimation(cls::IRender* prman)
	{
		return &simpleAnimationProc;
	}
}

// ������ ����������
void SimpleAnimationProc::Process(cls::IRender* render)
{
//	cls::PSA<Math::Vec3f> P = render->GetParameter("P");

	cls::PS parametername = render->GetParameter("animation::parameter");
	if( parametername.empty())
		return;
	cls::PS parametertype = render->GetParameter("animation::type");
	if( parametertype.empty())
		return;
	bool bAttribute = false;
	if( parametertype=="ATTRIBUTE")
		bAttribute = true;

	cls::Param p;
	if( bAttribute)
		p = render->GetAttribute(parametername.data());
	else
		p = render->GetParameter(parametername.data());
	if( p.empty())
		return;

	cls::PA<float> phases = render->GetAttribute("animation::phases");
	if( phases.empty())
		return;

	// phase
	float phase = 0;
	cls::P<float> _phase = render->GetParameter("animation::phase");
	if( !_phase.empty())
		phase = *_phase;

	cls::PA<int> offset = render->GetParameter("animation::offset");
	if( offset.empty())
		return;
	
//printf( "phase=%f\n", phase);
	bool bCycling = true;
//	cls::P<bool> _bCycling = render->GetAttribute("animation::cycling");
	cls::P<bool> _bCycling = render->GetParameter("animation::cycling");
	if(!_bCycling.empty())
		bCycling = _bCycling.data();

	if( bCycling)
	{
		// ���������
		float duration = phases[phases.size()-1];
		phase = duration*(phase/duration - floor(phase/duration));
	}
	else
	{
		if(phase<phases[0]) phase=phases[0];
		if(phase>=phases[phases.size()-1]) phase=phases[phases.size()-1];
	}

	// �������� ����
	int i;
	for(i=0; i<phases.size(); i++)
	{
		if( phases[i]>=phase) 
			break;
	}
	int phase1 = i, phase0=i-1;
	if( i==0)
		phase0=i;
	if( i==phases.size())
		phase1=i-1;
	float factor = 0;
	if( phase0!=phase1)
		factor = (phase-phases[phase0])/(phases[phase1]-phases[phase0]);

#ifdef _DEBUG
//	printf("SimpleAnimationProc: %d(%f) %d(%f)\n", phase0, (1-factor), phase1, factor);
#endif

	// �������� � ��������
	int offset0 = offset[phase0];
	int offset1 = offset[phase1];
	int size0 = p.size()-offset0;
	int size1 = p.size()-offset1;
	if(phase0+1<phases.size())
		size0 = offset[phase0+1]-offset0;
	if(phase1+1<phases.size())
		size1 = offset[phase1+1]-offset1;

	// �������
	int fullsize = p.size();
	char* data0 = p.data()+p.stride()*offset0;
	char* data1 = p.data()+p.stride()*offset1;

	cls::Param neo(p->type, p->interp, size0);
	char* dst = neo.data();
	if(size0!=size1 || factor==0.f)
	{
		// ��� ������������
		memcpy( dst, data0, size0*neo.stride());
	}
	else
	{
		// ������������
		switch(p->type)
		{
			case cls::PT_BOOL:	
			case cls::PT_INT:
				// �� ���������������!
				if( factor<0.5f) 
					memcpy( dst, data0, size0*neo.stride());
				else
					memcpy( dst, data1, size1*neo.stride());
				break;
			case cls::PT_DOUBLE: 
				// �����
				break;
			case cls::PT_FLOAT: 	
			case cls::PT_POINT:
			case cls::PT_VECTOR: 
			case cls::PT_NORMAL: 
			case cls::PT_COLOR:
			case cls::PT_HPOINT: 
			case cls::PT_MATRIX:	
			case cls::PT_BOX:	
				{
					float* fdst = (float*)dst;
					float* fsrc0 = (float*)data0;
					float* fsrc1 = (float*)data1;
					int s = neo.sizebytes()/sizeof(float);
					for(int i=0; i<s; i++)
						fdst[i] = (1-factor)*fsrc0[i] + factor*fsrc1[i];
				}
				break;
		}
	}
	if( bAttribute)
		render->Attribute(parametername.data(), neo);
	else
		render->Parameter(parametername.data(), neo);
}
