#include "DumpProc.h"
#include "ocellaris/renderFileImpl.h"
#include "ocellaris/renderCacheImpl.h"

DumpProc dumpProc;

extern "C"
{
	// Dump
	__declspec(dllexport) cls::IProcedural* __cdecl Dump(cls::IRender* prman)
	{
		return &dumpProc;
	}
}

// секция рендеринга
void DumpProc::Process(cls::IRender* render)
{
	render->Dump();
}
