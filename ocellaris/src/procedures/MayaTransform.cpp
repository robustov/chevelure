#include "MayaTransform.h"
#include "ocellaris/renderFileImpl.h"
#include "ocellaris/renderCacheImpl.h"
#include "ocellaris/renderBlurParser.h"

MayaTransformProc mayaTransform;
extern "C"
{
	__declspec(dllexport) cls::IProcedural* __cdecl MayaTransform(cls::IRender* prman)
	{
		return &mayaTransform;
	}
}

struct TransformData
{
	Math::Vec3f translate;
	Math::Vec3f rotate;
	Math::Vec3f scale;
	Math::Vec3f shear; 
	int    rotateOrder;			// (0=xyz, 1=yzx, 2=zxy, 3=xzy, 4=yxz, 5=zyx)
	Math::Vec3f scalePivot;
	Math::Vec3f scalePivotTranslate;
	Math::Vec3f rotatePivot;
	Math::Vec3f rotatePivotTranslate;
	Math::Vec3f rotateAxis;
};
void getTransformData( cls::IRender* render, TransformData& td)
{
	if( !render->GetParameter("MayaTransform::translate", td.translate))
		td.translate = Math::Vec3f(0.f);
	if( !render->GetParameter("MayaTransform::rotate", td.rotate))
		td.rotate = Math::Vec3f(0.f);
	if( !render->GetParameter("MayaTransform::scale", td.scale))
		td.scale = Math::Vec3f(1.f);
	if( !render->GetParameter("MayaTransform::shear", td.shear))
		td.shear = Math::Vec3f(0.f);
	if( !render->GetParameter("MayaTransform::rotateOrder", td.rotateOrder))
		td.rotateOrder = 1;
	if( !render->GetParameter("MayaTransform::scalePivot", td.scalePivot))
		td.scalePivot = Math::Vec3f(0.f);
	if( !render->GetParameter("MayaTransform::scalePivotTranslate", td.scalePivotTranslate))
		td.scalePivotTranslate = Math::Vec3f(0.f);
	if( !render->GetParameter("MayaTransform::rotatePivot", td.rotatePivot))
		td.rotatePivot = Math::Vec3f(0.f);
	if( !render->GetParameter("MayaTransform::rotatePivotTranslate", td.rotatePivotTranslate))
		td.rotatePivotTranslate = Math::Vec3f(0.f);
	if( !render->GetParameter("MayaTransform::rotateAxis", td.rotateAxis))
		td.rotateAxis = Math::Vec3f(0.f);
}

void renderTransformData( cls::IRender* render, TransformData& td)
{
	Math::Matrix4f m = Math::Matrix4f::id;

	float tx = td.translate[0], ty = td.translate[1], tz = td.translate[2];
	float rx = td.rotate[0], ry = td.rotate[1], rz = td.rotate[2];
	float sx = td.scale[0], sy = td.scale[1], sz = td.scale[2];
	float shxy = td.shear[0], shxz = td.shear[1], shyz = td.shear[2];
	
	float spx = td.scalePivot[0], spy = td.scalePivot[1], spz = td.scalePivot[2];
	float sptx = td.scalePivotTranslate[0], spty = td.scalePivotTranslate[1], sptz = td.scalePivotTranslate[2];
	float rpx = td.rotatePivot[0], rpy = td.rotatePivot[1], rpz = td.rotatePivot[2];
	float rptx = td.rotatePivotTranslate[0], rpty = td.rotatePivotTranslate[1], rptz = td.rotatePivotTranslate[2];

	float rax = td.rotateAxis[0], ray = td.rotateAxis[1], raz = td.rotateAxis[2];

	int rotateOrder = td.rotateOrder;

	float cx, cy, cz;

	float SP[16] = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, spx, spy, spz, 1 };
	float ST[16] = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, sptx, spty, sptz, 1 };
	float RP[16] = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, rpx, rpy, rpz, 1 } ;
	float RT[16] = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, rptx, rpty, rptz, 1 };
	float S[16]  = { sx, 0, 0, 0, 0, sy, 0, 0, 0, 0, sz, 0, 0, 0, 0, 1 };
	float SH[16] = { 1, 0, 0, 0, shxy, 1, 0, 0, shxz, shyz, 1, 0, 0, 0, 0, 1 };

	sx = sin(rax); cx = cos(rax);
	sy = sin(ray); cy = cos(ray);
	sz = sin(raz); cz = cos(raz);

	float AX[16] = { 1, 0, 0, 0, 0, cx, sx, 0, 0, -sx, cx, 0, 0, 0, 0, 1 };
	float AY[16] = { cy, 0, -sy, 0, 0, 1, 0, 0, sy, 0, cy, 0, 0, 0, 0, 1 };
	float AZ[16] = { cz, sz, 0, 0, -sz, cz, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };

	sx = sin(rx); cx = cos(rx);
	sy = sin(ry); cy = cos(ry);
	sz = sin(rz); cz = cos(rz);

	float RX[16] = { 1, 0, 0, 0, 0, cx, sx, 0, 0, -sx, cx, 0, 0, 0, 0, 1 };
	float RY[16] = { cy, 0, -sy, 0, 0, 1, 0, 0, sy, 0, cy, 0, 0, 0, 0, 1 };
	float RZ[16] = { cz, sz, 0, 0, -sz, cz, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };

	float T[16]  = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, tx, ty, tz, 1 };

	Math::Matrix4f mSP(SP);
	Math::Matrix4f mSPi(SP);
	mSPi.invert();
	Math::Matrix4f mS(S);
	Math::Matrix4f mSH(SH);
	Math::Matrix4f mST(ST);
	Math::Matrix4f mRP(RP);
	Math::Matrix4f mRPi(RP);
	mRPi.invert();
	
	Math::Matrix4f mAX(AX);
	Math::Matrix4f mAY(AY);
	Math::Matrix4f mAZ(AZ);

	Math::Matrix4f mR;
	Math::Matrix4f mRX(RX);
	Math::Matrix4f mRY(RY);
	Math::Matrix4f mRZ(RZ);

	Math::Matrix4f mRT(RT);
	Math::Matrix4f mT(T);

	switch ( rotateOrder )	// ( 0 = xyz, 1 = yzx, 2 = zxy, 3 = xzy, 4 = yxz, 5 = zyx )
	{
		case 0: mR = mRX * mRY * mRZ; break;
		case 1: mR = mRY * mRZ * mRX; break;
		case 2: mR = mRZ * mRX * mRY; break;
		case 3: mR = mRX * mRZ * mRY; break;
		case 4: mR = mRY * mRX * mRZ; break;
		case 5: mR = mRZ * mRY * mRX; break;
	}

	m = mSPi * mS * mSH * mSP * mST * mRPi * ( mAX * mAY * mAZ ) * mR * mRP * mRT * mT;

	render->Parameter("transform", m);
//	render->AppendTransform(m);
}

void MayaTransformProc::Process(cls::IRender* render)
{
	TransformData td;
	getTransformData(render, td);
	renderTransformData(render, td);
}

/*/
// ������ ����������
// ���� ����������� � �������� �����
void MayaTransformProc::Render(
	cls::IRender* render, 
	int motionBlurSamples, 
	float* motionBlurTimes
	)
{
	if( !motionBlurSamples)
	{
		// ��� �����
		TransformData td;
		getTransformData(render, td);
		renderTransformData(render, td);
		return;
	}
	else
	{
		// �������
		cls::renderBlurParser<> blurParser;
		for(int i=0; i<motionBlurSamples; i++)
		{
			render->SetCurrentMotionPhase(motionBlurTimes[i]);

			TransformData td;
			getTransformData(render, td);

			blurParser.SetCurrentPhase(motionBlurTimes[i]);
			renderTransformData(&blurParser, td);
		}
		render->SetCurrentMotionPhase(cls::nonBlurValue);
		blurParser.Render(render);
	}
}
/*/

