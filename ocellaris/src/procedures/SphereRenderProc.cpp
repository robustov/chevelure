#include "ocellaris/IProcedural.h"
#include "ocellaris/IRender.h"

struct SphereRenderProc : public cls::IProcedural
{
	// ������ ����������
	virtual void Render(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		);

public:
	bool singleSphere(cls::IRender* render);
	bool singleSphere(cls::IRender* render, Math::Box3f& box);
};

SphereRenderProc sphererenderproc;

extern "C"
{
	// Box
	__declspec(dllexport) cls::IProcedural* __cdecl Sphere(cls::IRender* prman)
	{
		return &sphererenderproc;
	}
}

// ������ ����������
// ���� ����������� � �������� �����
void SphereRenderProc::Render(
	cls::IRender* render, 
	int motionBlurSamples, 
	float* motionBlurTimes
	)
{
	if( !motionBlurSamples)
	{
		if( !singleSphere(render))
			return;
		render->SystemCall(cls::SC_CURVES);
	}
	else
	{
//		render->MotionBegin();
		for(int i=0; i<motionBlurSamples; i++)
		{
			render->MotionPhaseBegin( motionBlurTimes[i]);
			if( !singleSphere(render))
				return;
			render->MotionPhaseEnd();
		}
		render->SystemCall(cls::SC_CURVES);
//		render->MotionEnd();
	}
}

bool SphereRenderProc::singleSphere(
	cls::IRender* render
	)
{
	cls::P<Math::Box3f> box = render->GetParameter("#box");
	if(box.empty()) return false;

	return singleSphere( render, *box);
}

bool SphereRenderProc::singleSphere(cls::IRender* render, Math::Box3f& box)
{
	cls::PA<int> nverts(cls::PI_PRIMITIVE, 6);
	nverts[0] = 5;
	nverts[1] = 5;
	nverts[2] = 2;
	nverts[3] = 2;
	nverts[4] = 2;
	nverts[5] = 2;
	render->Parameter("#curves::nverts", nverts);

	int i=0;
	cls::PA<Math::Vec3f> verts(cls::PI_VERTEX, 18);
	verts[i++] = box.corner(0);
	verts[i++] = box.corner(1);
	verts[i++] = box.corner(3);
	verts[i++] = box.corner(2);
	verts[i++] = box.corner(0);

	verts[i++] = box.corner(4);
	verts[i++] = box.corner(5);
	verts[i++] = box.corner(7);
	verts[i++] = box.corner(6);
	verts[i++] = box.corner(4);

	verts[i++] = box.corner(0);
	verts[i++] = box.corner(0+4);
	verts[i++] = box.corner(1);
	verts[i++] = box.corner(1+4);
	verts[i++] = box.corner(2);
	verts[i++] = box.corner(2+4);
	verts[i++] = box.corner(3);
	verts[i++] = box.corner(3+4);
	render->Parameter("P", verts);

	render->Parameter("#curves::interpolation", "linear");
	render->Parameter("#curves::wrap", "nonperiodic");
	return true;
}
