#include "ocellaris/IProcedural.h"
#include "ocellaris/IRender.h"
#include "ocellaris/renderFileImpl.h"
#include "ocellaris/renderCacheImpl.h"
#include "ocellaris/renderBlurParser.h"
#include "Util/misc_create_directory.h"

//! @ingroup implement_group
//! \brief ��������� InstanceRenderProc
//! 
//! ��� ��������������� ��������
//!
struct InstanceRenderProc : public cls::IProcedural
{

	// ������ ����������
	virtual void Render(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		);

protected:
	void tochache(
		std::string name, 
		cls::IRender* render,
		cls::PA<int>& _id,
		cls::PA<Math::Matrix4f>& _position,
		cls::PSA& _filenames,
		cls::PA<int>& _fileindex,
		std::map<std::string, cls::Param>& list0
		);
	bool tochache_single(
		int i,
		std::string name, 
		cls::IRender* render,
		int _id,
		Math::Matrix4f& m,
		cls::PSA& _filenames,
		int _fileindex,
		std::map<std::string, cls::Param>& list0
		);

};

InstanceRenderProc instancerenderproc;

extern "C"
{
	// Dump
	__declspec(dllexport) cls::IProcedural* __cdecl Instance(cls::IRender* prman)
	{
		return &instancerenderproc;
	}
}


// ������ ����������
// ���� ����������� � �������� �����
void InstanceRenderProc::Render(
	cls::IRender* render, 
	int motionBlurSamples, 
	float* motionBlurTimes
	)
{
	bool bBlur = motionBlurSamples!=0;

	// � ���
	if(!bBlur)
	{
		cls::PA<int> _id				= render->GetParameter("#particleid");
		cls::PA<Math::Matrix4f> _position = render->GetParameter("#M");
		cls::PSA _filenames				= render->GetParameter("#filenames");
		cls::PA<int> _fileindex			= render->GetParameter("#fileindex");

		// id -> index
		std::map<int, int> id_map;
		for(int i=0; i<_id.size(); i++)
		{
			int id = _id[i];
			id_map[id] = i;
		}

		// ������ ��� ��������� ���������, �������� ������
		std::vector< std::pair< std::string, cls::Param> > list;
		render->GetParameterList(list);
		std::map<std::string, cls::Param> list0;
		for(int p=0; p<(int)list.size(); p++)
		{
			if(list[p].first[0]!='#')
				list0[list[p].first] = list[p].second;
		}

		std::string name;
		render->GetAttribute("name", name);

		render->PushRenderAttributes();
		tochache( name, render, _id, _position, _filenames, _fileindex, list0);
		render->PopRenderAttributes();
	}
	else
	{
		/////////////////////////////////////
		// ���� ������� ������ ��� �������
		std::map<int, cls::renderBlurParser<> > caches;
		std::map<int, int > check;

		for(int b=0; b<motionBlurSamples; b++)
		{
			render->SetCurrentMotionPhase(motionBlurTimes[b]);

			std::string name;
			render->GetAttribute("name", name);

			// 
			cls::PA<int> _idX						= render->GetParameter("#particleid");
			cls::PA<Math::Matrix4f> _positionX		= render->GetParameter("#M");
			cls::PA<int> _fileindexX				= render->GetParameter("#fileindex");
			cls::PSA _filenamesX					= render->GetParameter("#filenames");

			// ������ ��� ��������� ���������, �������� ������
			std::vector< std::pair< std::string, cls::Param> > list;
			render->GetParameterList(list);
			std::map<std::string, cls::Param> list0;
			for(int p=0; p<(int)list.size(); p++)
			{
				if(list[p].first[0]!='#')
					list0[list[p].first] = list[p].second;
			}

			for( int i=0; i<_idX.size(); i++)
			{
				int id = _idX[i];
				if(b==0)
					check[id] = 0;
				cls::renderBlurParser<>& blurParser = caches[id];
//				cls::renderBlurParser<> blurParser;

				blurParser.SetCurrentPhase(motionBlurTimes[b]);
				
				if(i>=_positionX.size()) 
					continue;

				if( i>=_fileindexX.size()) 
					continue;

				tochache_single( i, name, &blurParser, _idX[i], _positionX[i], _filenamesX, _fileindexX[i], list0);
				check[id]++;
			}
		}
		render->SetCurrentMotionPhase(cls::nonBlurValue);

		// ���������
		std::map<int, cls::renderBlurParser<> >::iterator it = caches.begin();
		for(;it != caches.end(); it++)
		{
			int id = it->first;
			if( check[id]!=motionBlurSamples)
				continue;

			cls::renderBlurParser<>& blurParser = caches[id];
			blurParser.Render(render);
		}
	}
}



void InstanceRenderProc::tochache(
	std::string name, 
	cls::IRender* render,
	cls::PA<int>& _id,
	cls::PA<Math::Matrix4f>& _position,
	cls::PSA& _filenames,
	cls::PA<int>& _fileindex,
	std::map<std::string, cls::Param>& list0
	)
{
	/*/
	// ������ �������� ( PPAttrs ) � ������ �� �� ������� ��� user �������� ( vector ������ �� ������ ��� color )
	// �� � ������������� ���� �� ��������, �.�. ���� �������� � tochache_single
	cls::PSA PPAttrs = render->GetParameter( "PPAttrs" );

	std::vector<cls::Param> src_params(PPAttrs.size());
	for( int k = 0; k < PPAttrs.size(); k++ )
	{
		src_params[k] = render->GetParameter( PPAttrs[k] );
	}
	/*/

	for(int i=0;i<_id.size(); i++)
	{
		Math::Matrix4f m = Math::Matrix4f::id;
		if(i<_position.size()) m = _position[i];

		int findex = -1;
		if( i<_fileindex.size()) 
			findex = _fileindex[i];

		tochache_single(
			i, 
			name, 
			render,
			_id[i], 
			m, 
			_filenames, 
			findex, 
			list0
			);
	}
}

bool InstanceRenderProc::tochache_single(
	int i,
	std::string name, 
	cls::IRender* render,
	int _id,
	Math::Matrix4f& m,
	cls::PSA& _filenames,
	int findex,
	std::map<std::string, cls::Param>& list0
	)
{
	char buf[256];
	_snprintf(buf, 256, "%s[%d] id=%d", name.c_str(), i, _id);


	if( Math::length(m[0])<=0.0001 || 
		Math::length(m[1])<=0.0001 || 
		Math::length(m[2])<=0.0001
		)
		return false;

	if( findex<0 || findex>=_filenames.size()) 
		return false;

	render->PushAttributes();
	render->Attribute("name", buf);
	render->PushTransform();

	render->PushAttributes();
	render->AppendTransform(m);
	render->PopAttributes();
		
	render->PushRenderAttributes();

	// ���������
	std::map<std::string, cls::Param>::iterator it = list0.begin();
	for( ;it != list0.end(); it++)
	{
		cls::Param& p = it->second;
		if(p.size()==1) 
			render->Attribute(it->first.c_str(), p);
		else
		{
			if(i<p.size())
				render->Attribute(it->first.c_str(), p[i]);
		}
	}

//	render->Attribute( "user::test", cls::PT_FLOAT, 10.f );

	render->Parameter("filename", _filenames[findex]);
	render->RenderCall("ReadOCS");
	render->PopRenderAttributes();

	render->PopTransform();
	render->PopAttributes();
	return true;
}
