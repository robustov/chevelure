#pragma once
#include "ocellaris/IProcedural.h"
#include "ocellaris/IRender.h"

//! @ingroup implement_group
//! \brief ��������� SolidBoxRenderProc
//! 
//! ������ ����� Box
//! ������� ��������� � ��������:
//! box #box
struct SolidBoxRenderProc : public cls::IProcedural
{
	// ������ ����������
	virtual void Render(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		);

protected:
	bool singleBox(cls::IRender* render);
};
