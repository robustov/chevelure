#include "SolidBoxRenderProc.h"

SolidBoxRenderProc solidBoxRenderProc;

extern "C"
{
	// SolidBox
	__declspec(dllexport) cls::IProcedural* __cdecl SolidBox(cls::IRender* prman)
	{
		return &solidBoxRenderProc;
	}
}

// ������ ����������
// ���� ����������� � �������� �����
void SolidBoxRenderProc::Render(
	cls::IRender* render, 
	int motionBlurSamples, 
	float* motionBlurTimes
	)
{
	if( !motionBlurSamples)
	{
		if( !singleBox(render))
			return;
		render->SystemCall(cls::SC_MESH);
	}
	else
	{
		render->MotionBegin();
		for(int i=0; i<motionBlurSamples; i++)
		{
			render->MotionPhaseBegin( motionBlurTimes[i]);
			if( !singleBox(render))
				return;
			render->MotionPhaseEnd();
		}
		render->SystemCall(cls::SC_MESH);
		render->MotionEnd();
	}
}

bool SolidBoxRenderProc::singleBox(
	cls::IRender* render
	)
{
	cls::P<Math::Box3f> box = render->GetParameter("#box");
	if(box.empty()) return false;

	cls::PA<int> nverts(cls::PI_PRIMITIVE, 6);
	cls::PA<int> nloops(cls::PI_PRIMITIVE, 6);
	int i=0;
	for(i=0; i<6; i++)
	{
		nverts[i] = 4;
		nloops[i] = 1;
	}

	cls::PA<Math::Vec3f> P(cls::PI_VERTEX, 8);
	for(i=0; i<8; i++)
	{
		P[i] = (*box).corner(i);
	}

	int mesh_verts[24] = {
		2, 3, 1, 0, 
		4, 5, 7, 6, 

		2, 3, 7, 6, 
		4, 5, 1, 0, 

		0, 2, 6, 4, 
		1, 3, 7, 5, 

//		6, 7, 5, 4, 
//		0, 1, 7, 6, 
//		3, 5, 7, 1, 
//		4, 2, 0, 6
	};

	cls::PA<int> verts(cls::PI_PRIMITIVEVERTEX, 24);
	for(i=0; i<24; i++)
		verts[i] = mesh_verts[i];

	render->Parameter("P", P);

	render->Parameter("mesh::interpolation", "linear");
	render->Parameter("mesh::loops", nloops);
	render->Parameter("mesh::nverts", nverts);
	render->Parameter("mesh::verts", verts);

	return true;
}
