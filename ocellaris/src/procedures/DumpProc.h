#pragma once
#include "ocellaris/IProcedural.h"

//! @ingroup implement_group
struct DumpProc : public cls::IProcedural
{
	// секция рендеринга
	virtual void Process(cls::IRender* render);
};
