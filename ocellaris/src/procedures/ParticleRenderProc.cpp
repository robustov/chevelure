#include "ocellaris/IProcedural.h"
#include "ocellaris/IRender.h"
#include "ocellaris/renderFileImpl.h"
#include "ocellaris/renderCacheImpl.h"
#include "ocellaris/renderBlurParser.h"
#include "Util/misc_create_directory.h"

//! @ingroup implement_group
//! \brief ��������� ParticleRenderProc
//! 
//! ��� ��������������� ��������
//!
struct ParticleRenderProc : public cls::IProcedural
{

	// ������ ����������
	virtual void Render(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		);

protected:
/*/
	void tochache(
		std::string name, 
		cls::IRender* render,
		cls::PA<int>& _id,
		std::map<std::string, cls::Param>& list0
		);
/*/
	bool tocache_single(
		cls::PS type, 
		int i,
		std::string name, 
		cls::IRender* render,
		int _id,
		Math::Vec3f& pt,
		std::map<std::string, cls::Param>& list0
		);

	bool tocache_singleArray(
		cls::PS type, 
		std::string name, 
		cls::IRender* render,
		cls::PA<float> ids,
		cls::PA<float> radius, 
		cls::PA<Math::Vec3f> P
		);

};

ParticleRenderProc particlerenderproc;

extern "C"
{
	// Dump
	__declspec(dllexport) cls::IProcedural* __cdecl Particles(cls::IRender* prman)
	{
		return &particlerenderproc;
	}
}


// ������ ����������
// ���� ����������� � �������� �����
void ParticleRenderProc::Render(
	cls::IRender* render, 
	int motionBlurSamples, 
	float* motionBlurTimes
	)
{
	bool bBlur = motionBlurSamples!=0;

	// � ���
	if(!bBlur)
	{
		std::string name;
		render->GetAttribute("name", name);

		cls::PS type = render->GetParameter("@type");
		cls::PA<float> radius = render->GetParameter("radius");
		cls::PA<int> ids = render->GetParameter("#id");
		cls::PA<Math::Vec3f> P = render->GetParameter("P");
		if( ids.empty())
			return;
		if( ids.size()!=P.size())
			return;
		
	
		// ������ ��� ��������� ���������, �������� ������
		std::vector< std::pair< std::string, cls::Param> > list;
		render->GetParameterList(list);
		std::map<std::string, cls::Param> list0;
		for(int p=0; p<(int)list.size(); p++)
		{
			printf( "%s\n", list[p].first.c_str());
			if(list[p].first[0]!='#')
				list0[list[p].first] = list[p].second;
		}
		render->PushRenderAttributes();
		if( type=="points" )
		{
			tocache_singleArray(type, name, render, ids, radius, P);
			render->SystemCall(cls::SC_POINTS);
		}
		if(type=="spheres")
		{
			float maxradius = 0;
			for(int i=0; i<ids.size(); i++)
			{
				tocache_single( type, i, name, render, ids[i], P[i], list0);
			}
		}
		if(type=="blobby")
		{
			tocache_singleArray(type, name, render, ids, radius, P);
			render->SystemCall(cls::SC_BLOBBY);
		}

//		tocache( name, render, _id, list0);
		render->PopRenderAttributes();
	}
	else
	{
		std::string name;
		render->GetAttribute("name", name);
		cls::PS type = render->GetParameter("@type");

		if(type=="spheres")
		{
			/////////////////////////////////////
			// ���� ������� ������ ��� �������
			std::map<int, cls::renderBlurParser<> > caches;
			std::map<int, int > check;

			for(int b=0; b<motionBlurSamples; b++)
			{
				render->SetCurrentMotionPhase(motionBlurTimes[b]);

				cls::PA<float> radius = render->GetParameter("radius");
				cls::PA<int> ids = render->GetParameter("#id");
				cls::PA<Math::Vec3f> P = render->GetParameter("P");
				if( ids.empty())
					return;
				if( ids.size()!=P.size())
					return;

				// ������ ��� ��������� ���������, �������� ������
				std::vector< std::pair< std::string, cls::Param> > list;
				render->GetParameterList(list);
				std::map<std::string, cls::Param> list0;
				for(int p=0; p<(int)list.size(); p++)
				{
					if(list[p].first[0]!='#')
						list0[list[p].first] = list[p].second;
				}

				for( int i=0; i<ids.size(); i++)
				{
					int id = ids[i];
					if(b==0)
						check[id] = 0;
					cls::renderBlurParser<>& blurParser = caches[id];

					blurParser.SetCurrentPhase(motionBlurTimes[b]);
					
					if(i>=P.size()) 
						continue;

					tocache_single( type, i, name, &blurParser, id, P[i], list0);
					check[id]++;
				}
			}
			render->SetCurrentMotionPhase(cls::nonBlurValue);

			// ���������
			std::map<int, cls::renderBlurParser<> >::iterator it = caches.begin();
			for(;it != caches.end(); it++)
			{
				int id = it->first;
				if( check[id]!=motionBlurSamples)
					continue;

				cls::renderBlurParser<>& blurParser = caches[id];
				blurParser.Render(render);
			}
		}
		else
		{
			// ������� ���������� �������!
			std::map<int, int > check;
			for(int b=0; b<motionBlurSamples; b++)
			{
				render->SetCurrentMotionPhase(motionBlurTimes[b]);

				cls::PA<int> ids = render->GetParameter("#id");
				for(int i=0; i<ids.size(); i++)
				{
					int id = ids[i];
					if(b==0)
						check[id] = 0;
					check[id]++;
				}
			}
			render->SetCurrentMotionPhase(cls::nonBlurValue);

			// ���������� ������ - ���������� ������� � �������
			std::map<int, int>::iterator it = check.begin();
			int i=0;
			for(;it != check.end(); it++)
			{
				int id = it->first;
				if( it->second!=motionBlurSamples)
				{
					it->second = -1;
					continue;
				}
				else
				{
					it->second = i;
					i++;
				}
			}
			int fullcount = i;

			// ��������� �������
			cls::renderBlurParser<> blurparser;
			for(int b=0; b<motionBlurSamples; b++)
			{
				render->SetCurrentMotionPhase(motionBlurTimes[b]);

				cls::PA<float> radius = render->GetParameter("radius");
				cls::PA<int> ids = render->GetParameter("#id");
				cls::PA<Math::Vec3f> P = render->GetParameter("P");

				cls::PA<float> _radius(cls::PI_VERTEX, fullcount);
				cls::PA<int> _ids( cls::PI_VERTEX, fullcount);
				cls::PA<Math::Vec3f> _P( cls::PI_VERTEX, fullcount, cls::PT_POINT);

				for(int i=0; i<ids.size(); i++)
				{
					int id = ids[i];
					int index = check[ids[i]];
					if(index<0) continue;
					_ids[index] = id;
					_P[index] = P[i];

					if( i<radius.size())
						_radius[index] = radius[i];
					else
						_radius[index] = radius[0];
				}
				// to parser
				blurparser.SetCurrentPhase(motionBlurTimes[b]);
				tocache_singleArray(type, name, &blurparser, _ids, _radius, _P);
			}
			render->SetCurrentMotionPhase(cls::nonBlurValue);

			render->PushAttributes();
			blurparser.Render(render);
			if( type=="points" )
				render->SystemCall(cls::SC_POINTS);
			if(type=="blobby")
				render->SystemCall(cls::SC_BLOBBY);
			render->PopAttributes();
		}
	}
}


/*/

void ParticleRenderProc::tochache(
	std::string name, 
	cls::IRender* render,
	cls::PA<int>& _id,
	cls::PA<Math::Matrix4f>& _position,
	cls::PSA& _filenames,
	cls::PA<int>& _fileindex,
	std::map<std::string, cls::Param>& list0
	)
{
	for(int i=0;i<_id.size(); i++)
	{
		Math::Matrix4f m = Math::Matrix4f::id;
		if(i<_position.size()) m = _position[i];

		int findex = -1;
		if( i<_fileindex.size()) 
			findex = _fileindex[i];

		tochache_single(
			i, 
			name, 
			render,
			_id[i], 
			m, 
			_filenames, 
			findex, 
			list0
			);
	}
}
/*/

bool ParticleRenderProc::tocache_single(
	cls::PS type, 
	int i,
	std::string name, 
	cls::IRender* render,
	int _id,
	Math::Vec3f& pt,
	std::map<std::string, cls::Param>& list0
	)
{
	Math::Matrix4f m = Math::Matrix4f::id;
	m[3] = Math::Vec4f( pt, 1);

	render->PushAttributes();

	render->Parameter("id", _id);

	render->PushTransform();

	render->PushAttributes();
	render->AppendTransform(m);
	render->PopAttributes();

	float radius = 0.1f;

	render->PushAttributes();
	std::map<std::string, cls::Param>::iterator it = list0.begin();
//				for(unsigned z=0; z<params.size(); z++)
	for( ; it != list0.end(); it++)
	{
		cls::Param& p = it->second;
		int s = p.size();
		const std::string& name = it->first;

		// radius
		if( name=="radius")
		{
			if( i<s && p->interp == cls::PI_VERTEX)
				radius = cls::P<float>(p[i]).data();
			else
				radius = cls::P<float>(p[0]).data();
			continue;
		}

		// ���������
		if( i<s && p->interp == cls::PI_VERTEX)
		{
			cls::Param ps = p[i].copy();
			ps->interp = cls::PI_CONSTANT;
			render->Parameter(name.c_str(), ps);
		}
		else if(s==1)
		{
			render->Parameter(name.c_str(), p);
		}
	}

	render->Sphere(radius);
	render->PopAttributes();

	render->PopTransform();
	render->PopAttributes();
	return true;
}

bool ParticleRenderProc::tocache_singleArray(
	cls::PS type, 
	std::string name, 
	cls::IRender* render,
	cls::PA<float> ids,
	cls::PA<float> radius, 
	cls::PA<Math::Vec3f> P
	)
{
	if( type=="points")
	{
		if( radius.size()==ids.size())
		{
			render->Parameter("#width", radius);
		}
		else if(radius.size()==1)
		{
			render->Parameter("#width", radius[0]);
		}
		else
		{
			render->Parameter("#width", 0.1f);
		}
		render->Parameter("P", P);
	}

	if(type=="blobby")
	{
		cls::PA<Math::Matrix4f> ellipsoids(cls::PI_VERTEX, ids.size());

		float maxradius = 0;
		for(int i=0; i<ids.size(); i++)
		{
			float r = 1;
			if( radius.size()==ids.size() && radius->interp == cls::PI_VERTEX)
				r = radius[i];
			else if(radius.size()>=1)
				r = radius[0];

			maxradius = __max(maxradius, r);
			Math::Matrix4f m = Math::Matrix4f::id;
			float d = r*2.46698f;
			m.scale( Math::Vec4f(d, d, d, 1));

			m[3] = Math::Vec4f( P[i], 1);

			ellipsoids[i] = m;
		}
		render->Parameter("blobby::ellipsoids", ellipsoids);
	}
	return true;
}
