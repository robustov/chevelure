#include "HairFilter.h"
#include "ocellaris/renderFileImpl.h"
#include "ocellaris/renderCacheImpl.h"

Hair_Filter hairFilter;

extern "C"
{
	// Filter
	__declspec(dllexport) cls::IFilter* __cdecl HairFilter(cls::IRender* prman)
	{
		return &hairFilter;
	}
}

Hair_Filter::Hair_Filter()
{
	curid = 0;
}

//! ����� �������
bool Hair_Filter::Begin(
	cls::IRender* render, 
	const Math::Matrix4f& currentWorldTransform,
	int& id					// ���������� �� �������� �������
	)
{
	cls::PA<Math::Matrix4f> transforms = render->GetParameter("HairFilter::transforms");
	if( transforms.empty()) return false;
	cls::PA<float> parameters = render->GetParameter("HairFilter::parameters");
	if( parameters.empty()) return false;
	cls::P<float> maxDot = render->GetParameter("HairFilter::maxDot");
	if( maxDot.empty()) return false;
	cls::P<float> u = render->GetParameter("HairFilter::u");
	cls::P<float> v = render->GetParameter("HairFilter::v");
	cls::P<int> hairid = render->GetParameter("HairFilter::id");

	if( parameters.size()!=transforms.size())
		return false;

	id = curid++;
	HairFilterInstanceData& hid = HairFilters[id];
	hid.transforms = transforms;
	hid.parameters = parameters;
	hid.maxDot = maxDot.data();
	hid.u = u;
	hid.v = v;
	hid.hairid = hairid;

	hid.startTransform = currentWorldTransform;

	for(int i=0; i<transforms.size(); i++)
	{
		float y = i*(*maxDot)/(transforms.size()-1);

		Math::Matrix4f B = Math::Matrix4f::id;
		B.scale(Math::Vec4f(*maxDot));
		B[3] = Math::Vec4f(0, y, 0, 1);
		Math::Matrix4f T = B.inverted()*transforms[i];
		transforms[i] = T;
	}

	if(false)
	{
		printf("id=%d maxDot = %f\n", id, maxDot);
		for(int i=0; i<parameters.size(); i++)
		{
			float y = i*(*maxDot)/transforms.size();
			Math::Vec3f v(0, y, 0);
			v = transforms[i]*v;
			printf("%f: %f %f %f\n", parameters[i], v.x, v.y, v.z);
		}
	}
	return true;
}
//! ������� ������ true ���� ���� ���������, ����� false
bool Hair_Filter::OnSystemCall(
	cls::enSystemCall instruction,
	cls::IRender* render,
	const Math::Matrix4f& currentWorldTransform,
	int id
	)
{
	if( HairFilters.find(id)==HairFilters.end())
		return false;
	HairFilterInstanceData& hid = HairFilters[id];
	cls::PA<Math::Vec3f> P = render->GetParameter("P");
	if(P.empty())
		return true;

	// preTransform
	Math::Matrix4f preTransform = currentWorldTransform*hid.startTransform.inverted();
	Math::Matrix4f postTransform = preTransform.inverted();

	cls::PA<Math::Vec3f> N = render->GetParameter("N");
	// ��������� ���������:
	cls::PA<int> verts;
	switch( instruction)
	{
	case cls::SC_MESH: 
		verts = render->GetParameter("mesh::verts");
	}
	if( !N.empty() && N->interp == cls::PI_PRIMITIVEVERTEX && verts.size()==N.size())
	{
		cls::PA<Math::Vec3f> newN = N.copy();
		for(int fv=0; fv<N.size(); fv++)
		{
			int v = verts[fv];
			Math::Vec3f src = P[v];
			src = preTransform*src;
			float factor;
			int index = getAffectIndexForVertex(src, hid, factor);
			Math::Matrix4f& cv0 = hid.transforms[index];
			Math::Matrix4f& cv1 = hid.transforms[index+1];

			Math::Vec3f srcN = N[fv];
			srcN = Math::Matrix4As3f(preTransform)*srcN;
			Math::Vec3f n0 = Math::Matrix4As3f(cv0)*srcN;
			Math::Vec3f n1 = Math::Matrix4As3f(cv1)*srcN;
			Math::Vec3f dstN = (1-factor)*n0 + factor*n1;
			dstN = Math::Matrix4As3f(postTransform)*dstN;
			newN[fv] = dstN.normalized();
		}
		render->Parameter("N", newN);
	}
	if( !N.empty() && N->interp == cls::PI_VERTEX && P.size()==N.size())
	{
	}
	{
		cls::PA<Math::Vec3f> newP = P.copy();

//		printf("id=%d P=%d newP=%d\n", id, P->data, newP->data);

		float tolerance = 1.f / hid.transforms.size();
		for(int v=0; v<P.size(); v++)
		{
			Math::Vec3f src = P[v];
			src = preTransform*src;

			float factor;
			int index = getAffectIndexForVertex(src, hid, factor);

			Math::Vec3f direction(0, 1, 0);
			Math::Matrix4f& cv0 = hid.transforms[index];
			Math::Matrix4f& cv1 = hid.transforms[index+1];

			Math::Vec3f pt0 = cv0*src;
			Math::Vec3f pt1 = cv1*src;
			Math::Vec3f dst = (1-factor)*pt0 + factor*pt1;
			dst = postTransform*dst;
			newP[v] = dst;
//printf("%d: {%f %f %f} -> {%f %f %f}\n", v, src.x, src.y, src.z, dst.x, dst.y, dst.z);
		}
		render->Parameter("P", newP);
	}
	render->Parameter("SurfaceU", hid.u);
	render->Parameter("SurfaceV", hid.v);
	render->Parameter("u_surface", hid.u);
	render->Parameter("v_surface", hid.v);
	render->Parameter("hairId", hid.hairid);

	return true;
}
//! ���� �������
void Hair_Filter::End(
	cls::IRender* render, 
	int id
	)
{
	HairFilters.erase(id);
}

int Hair_Filter::getAffectIndexForVertex(
	const Math::Vec3f& src, 
	const HairFilterInstanceData& data, 
	float& factor)
{
	Math::Vec3f direction(0, 1, 0);
	float dot = Math::dot(src, direction);
	dot = dot / data.maxDot;
	if(dot<0) dot = 0;

	int x;
	for(x=0; x<data.parameters.size(); x++)
	{
		if(dot<data.parameters[x]) break;
	}
	if(x==data.parameters.size())
	{
		factor = 1;
		return data.parameters.size()-2;
	}
	if(x==0)
	{
		factor = 0;
		return 0;
	}
	factor = (dot-data.parameters[x-1])/(data.parameters[x]-data.parameters[x-1]);
	return x-1;


	/*/
	int index = (int)(dot*(tolerance-1));
	if(index>=tolerance-1) index=tolerance-2;
	factor = dot*(tolerance-1) - index;
	if(factor>1) factor=1;
	return index;
	/*/
}
