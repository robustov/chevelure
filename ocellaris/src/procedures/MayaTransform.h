#pragma once
#include "ocellaris/IProcedural.h"

//! @ingroup implement_group
//! \brief ��������� MayaTransform
//! 
//! �������� ������� ���������� ��� � ����
//! ������� ��������� � ��������:
//! vector MayaTransform::translate 
//! vector MayaTransform::rotate
//! vector MayaTransform::scale
//! vector MayaTransform::shear 
//! int    MayaTransform::rotateOrder (0=xyz, 1=yzx, 2=zxy, 3=xzy, 4=yxz, 5=zyx)
//! vector MayaTransform::scalePivot 
//! vector MayaTransform::scalePivotTranslate 
//! vector MayaTransform::rotatePivot 
//! vector MayaTransform::rotatePivotTranslate 
//! vector MayaTransform::rotateAxis 
//! 
/*/
              -1                      -1
   matrix = SP * S * SH * SP * ST * RP * RA * R * RP * RT * T

   SP = |  1    0    0    0 |     ST = |  1    0    0    0 |
        |  0    1    0    0 |          |  0    1    0    0 |
        |  0    0    1    0 |          |  0    0    1    0 |
        | spx  spy  spz   1 |          | sptx spty sptz  1 |

   S  = |  sx   0    0    0 |     SH = |  1    0    0    0 |
        |  0    sy   0    0 |          | shxy  1    0    0 |
        |  0    0    sz   0 |          | shxz shyz  1    0 |
        |  0    0    0    1 |          |  0    0    0    1 |

   RP = |  1    0    0    0 |     RT = |  1    0    0    0 |
        |  0    1    0    0 |          |  0    1    0    0 |
        |  0    0    1    0 |          |  0    0    1    0 |
        | rpx  rpy  rpz   1 |          | rptx rpty rptz  1 |

   RA = AX * AY * AZ

   AX = |  1    0    0    0 |     AY = |  cy   0   -sy   0 |
        |  0    cx   sx   0 |          |  0    1    0    0 |
        |  0   -sx   cx   0 |          |  sy   0    cy   0 |
        |  0    0    0    1 |          |  0    0    0    1 |

   AZ = |  cz   sz   0    0 |     sx = sin(rax), cx = cos(rax)
        | -sz   cz   0    0 |     sy = sin(ray), cx = cos(ray)
        |  0    0    1    0 |     sz = sin(raz), cz = cos(raz)
        |  0    0    0    1 |

   Rotate:
		If the rotationInterpolation attribute specifies quaternion
		interpolation, use the following OpenMaya API calls to construct
		the matrix:
			Mquaternion q( rx, ry, rz, rw )
			R  = q.asMatrix()

		Otherwise, for Euler-angle rotation use:
			R  = RX * RY * RZ  (Note: order is determined by rotateOrder)

			RX = |  1    0    0    0 |     RY = |  cy   0   -sy   0 |
				 |  0    cx   sx   0 |          |  0    1    0    0 |
				 |  0   -sx   cx   0 |          |  sy   0    cy   0 |
				 |  0    0    0    1 |          |  0    0    0    1 |

			RZ = |  cz   sz   0    0 |     sx = sin(rx), cx = cos(rx)
				 | -sz   cz   0    0 |     sy = sin(ry), cx = cos(ry)
				 |  0    0    1    0 |     sz = sin(rz), cz = cos(rz)
				 |  0    0    0    1 |

   T  = |  1    0    0    0 |
        |  0    1    0    0 |
        |  0    0    1    0 |
        |  tx   ty   tz   1 |
 
/*/

struct MayaTransformProc : public cls::IProcedural
{
	/// ����� ��� ����������� (Call)
	virtual void Process(cls::IRender* render);
	/*/
	// ������ ����������
	virtual void Render(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		);
	/*/

};
