// ocellarisMayaCmd.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Util/misc_create_directory.h"
#include "Util/misc_create_directory.h"
#include <time.h>

int openfile(int argc, _TCHAR* argv[]);
int readJob( const char* filename);
int waitJob( const char* filename);
// 0 -maketask
// 1 -render
int maketask( int argc, _TCHAR* argv[], int cmd);


int _tmain(int argc, _TCHAR* argv[])
{
	if(argc>2)
	{
		if(strcmp(argv[1], "-maketask")==0)
		{
			return maketask( argc-2, argv+2, 0);
		}
		if(strcmp(argv[1], "-render")==0)
		{
			/*/
			std::string alffile = "C:/temp/alfspool";
			{
				struct tm   *newTime;
				time_t      szClock;
				time( &szClock );
				newTime = localtime( &szClock );
				char buf[100];
				alffile += itoa(szClock, buf, 10);
				alffile += ".alf";
				Util::correctFileName((char*)alffile.c_str(), true);
			}
			/*/
			return maketask( argc-2, argv+2, 1);
		}
	}
	return 0;
}
