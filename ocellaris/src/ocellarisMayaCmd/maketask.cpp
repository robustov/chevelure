#include "stdafx.h"

#define NT_APP
#define MLIBRARY_DONTUSE_MFC_MANIFEST

#include <maya/MIOStream.h>
#include <maya/MLibrary.h>
#include <maya/MGlobal.h>
#include <maya/MFileIO.h>
#include <list>
#include <string>
#include "Util/misc_create_directory.h"

// �����
// -workspace ...
// -passname
// -sequence
// -remote
// -render
// -displaytype
// -renderLayers
// -pause
// -bDump

int maketask( int argc, _TCHAR* argv[], int icmd)
{
	printf("init Maya\n");
	MStatus status, stat;

	// ��� ������ ������ ���� ���������:
	// MAYA_LOCATION=C:\AW\Maya2008
	// PYTHONPATH=C:\AW\Maya2008\Python\lib\site-packages
	putenv("MAYA_PLUG_IN_PATH=M:\\ulitkabin\\bin");
//	putenv("PYTHONPATH=M:\\ulitkabin\\python");

	status = MLibrary::initialize( true, "", true);
	if ( !status ) 
	{
		status.perror("MLibrary::initialize");
		return (1);
	}
	std::string workspace="";
	std::string passname="";
	std::string sequence="1";
	std::string remote="alfred";
	std::string render="prman";
	std::string displaytype="tiff";
	std::string renderLayers="0";
	std::string pause="0";
	std::string bDump="0";
	std::string alffile="";
	std::string scenename="";

	std::list< std::string> files;
	for( ; argc>0; )
	{
		if(argv[0][0]=='-')
		{
			std::string key = argv[0];
			argc--, argv++;
			if(argc<=0) 
				break;
			if( strcmp(key.c_str(), "-workspace")==0)
				workspace = argv[0];
			if( strcmp(key.c_str(), "-passname")==0)
				passname = argv[0];
			if( strcmp(key.c_str(), "-sequence")==0)
				sequence = argv[0];
			if( strcmp(key.c_str(), "-remote")==0)
				remote = argv[0];
			if( strcmp(key.c_str(), "-render")==0)
				render = argv[0];
			if( strcmp(key.c_str(), "-displaytype")==0)
				displaytype = argv[0];
			if( strcmp(key.c_str(), "-renderLayers")==0)
				renderLayers = argv[0];
			if( strcmp(key.c_str(), "-pause")==0)
				pause = argv[0];
			if( strcmp(key.c_str(), "-bDump")==0)
				bDump = argv[0];
			if( strcmp(key.c_str(), "-alffile")==0)
				alffile = argv[0];
			if( strcmp(key.c_str(), "-scenename")==0)
				scenename = argv[0];
		}
		else
		{
			files.push_back(argv[0]);
		}
		argc--, argv++;
	}

	if( files.empty())
	{
		fprintf(stderr, "Error: No files specified\n");
		return -1;
	}

	std::string cmd;

	if( !MGlobal::executePythonCommand("import ocsRender"))
	{
		fprintf(stderr, "import ocsRender FAILED!!!\n\n");
		return -1;
	}
	if( !MGlobal::sourceFile("ocellarisMayaMenu.mel"))
	{
		fprintf(stderr, "source FAILED ocellarisMayaMenu.mel\n\n");
		return -1;
	}

	std::list< std::string>::iterator it = files.begin();

	std::string lastworkspace = "";
	for( ; it != files.end(); it++)
	{
		std::string mayafile = *it;
		Util::changeSlashToUnixSlash(mayafile);

		std::string cur_workspace = workspace;
		if(cur_workspace.empty())
		{
			// ����� � ��������:

		}
		if(!cur_workspace.empty())
		{
			Util::changeSlashToUnixSlash(cur_workspace);
			cmd = "workspace -o ";
			cmd += '\"'+cur_workspace+'\"';
			puts(cmd.c_str());
			bool res = false;
			if( !MGlobal::executeCommand(cmd.c_str()))
			{
				fprintf(stderr, "FAILED!!!\n\n");
			}
		}

//		MFileIO::newFile(true);
		stat = MFileIO::open(mayafile.c_str());
		if ( !stat ) 
		{
			fprintf(stderr, "File %s not opened", mayafile.c_str());
			continue;
		}

		MStringArray nodelist;
		if( MGlobal::executeCommand("ls -type HairShape", nodelist))
		{
			for(int i=0; i<(int)nodelist.length(); i++)
			{
				std::string cmd;
				cmd += "HairUtilCmd buildcache -s ";
				cmd += nodelist[i].asChar();
				puts(cmd.c_str());
				puts("\n");

				MGlobal::executeCommand(cmd.c_str());
			}
		}


		std::string pythoncmd;
		pythoncmd += "ocsRender.render(";
		pythoncmd += "  passname=\""	+ passname+"\"";
		pythoncmd += ", sequence="		+ sequence;
		pythoncmd += ", remote=\""		+ remote+"\"";
		pythoncmd += ", render=\""		+ render+"\"";
		pythoncmd += ", displaytype=\"" + displaytype+"\"";
		pythoncmd += ", renderLayers="	+ renderLayers;
		pythoncmd += ", pause="			+ pause;
		pythoncmd += ", bDump="			+ bDump;
		pythoncmd += ", alffile=\""		+ alffile+"\"";
		pythoncmd += ", scenename=\""	+ scenename+"\"";
		pythoncmd += ")";

		puts(pythoncmd.c_str());
		puts("\n");
		
		if( !MGlobal::executePythonCommand(pythoncmd.c_str()))
		{
			fprintf(stderr, "FAILED!!!\n\n");
			continue;
		}
	}

	MLibrary::cleanup();
	return 0;
}
