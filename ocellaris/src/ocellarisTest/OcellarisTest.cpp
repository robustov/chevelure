// SnTest.cpp : Defines the entry point for the console application.
//

#include <windows.h>
#include <ri.h>

#include "ocellaris\renderCacheImpl.h"
#include "ocellaris\renderPrmanImpl.h"
#include "ocellaris\renderFileImpl.h"
#include "ocellaris\renderExecutiveCacheImpl.h"
#include "ocellaris\renderBlurParser.h"
#include "mathNpixar\IPrman.h"
#include "mathNpixar/IPrman_ascii.h"

bool Test(cls::IRender*);
bool Test();

//#include "ocellaris\renderFileImpl.h"
//#include "ocellaris\renderCacheImpl.h"
//#include "ocellaris\renderBlurParser.h"
//#include "ocellaris\IProcedural.h"

// -blur (������ ����� ���� ���)

int main(int argc, char* argv[])
{
	printf("OcellarisTest usage\n");
	printf("OcellarisTest input.ocs [outputfile] [format] [render]\n");
	printf("  default format -rib\n");
	printf("  default output file name = input.ocs.rib\n");
	printf("Formats:\n");
	printf(" -rib (pixar renderman ascii noncompressed)\n");
	printf(" -ocs (ocellaris ascii)\n");
	printf("Render:\n");
	printf(" -exe execute Call()\n");
	printf(" -blur execute blur\n");
	printf(" -copy dont execute\n");
	
	if(	argc<2) 
	{
		Test();
		return -1;
	}

	std::string infilename = argv[1];

	bool bBlur = false;
	bool bExecute = false;
	if(argc>=5)
	{
		bBlur = strcmp( argv[4], "-blur")==0;
		if( strcmp( argv[4], "-copy")==0)
			bExecute = false;
		if( strcmp( argv[4], "-exe")==0)
			bExecute = true;
	}

	std::string format = "-rib";
	if(argc>=4)
	{
		format = argv[3];
	}

	std::string outfilename = infilename + ".rib";
	if(argc>=3)
	{
		outfilename = argv[2];
	}


	cls::IRender* render = NULL;

	//*/
#ifdef _DEBUG
  	getIPrmanDll dllproc("IPrmanD.dll", "getIPrman");
#else
  	getIPrmanDll dllproc("IPrman.dll", "getIPrman");
#endif
	if( !dllproc.isValid()) 
	{
		printf("cant fount IPrman.dll or IPrmanD.dll");
		return 0;
	}
	IPrman* prman = (*dllproc)();
	/*/
	Prman_ascii thePrman;
	IPrman* prman = &thePrman;
	/*/

	cls::renderFileImpl ocsfileimpl;
//	Prman_Impl prman;
	cls::renderPrmanImpl<cls::TTransformStack_minimal> prmanimpl(prman);
//	cls::renderPrmanImpl<cls::TTransformStack_forblur> prmanimpl(&prman);
	

	if(format=="-ocs")
	{
		ocsfileimpl.openForWrite(outfilename.c_str(), true);
		render = &ocsfileimpl;
	}
	else if( format=="-rib")
	{
//		thePrman.Open(outfilename.c_str());
		//*/
		prman->RiBegin( (char*) outfilename.c_str());
		// format
		RtString format = "ascii";
		prman->RiOption(( RtToken ) "rib", ( RtToken ) "format", &format, RI_NULL);
		RtString compression = "none";
		prman->RiOption(( RtToken ) "rib", ( RtToken ) "compression", &compression, RI_NULL);
		//*/
		render = &prmanimpl;
		prmanimpl.setBlurParams(0, 0, 1);
	}
	else
	{
		printf("unknown format %s\n", format.c_str());
	}

	cls::renderExecutiveCacheImpl cache;
	cls::renderFileImpl infileimpl;

	try
	{
		Math::Box3f box;
		if( bBlur)
		{
			printf("use Blur Parser\n");
			cls::renderBlurParser<> blurParser;
			blurParser.SetCurrentMotionPhase(cls::nonBlurValue);

			cls::renderFileImpl infileimpl;
			if( !infileimpl.openForRead(infilename.c_str()))
				throw "Loading error";
			if( !infileimpl.Render( &blurParser))
				throw "Render error";

//			blurParser.Parse();
			blurParser.Render(render);
		}
		else
		{
			if( bExecute)
			{

				cls::renderFileImpl infileimpl;
				if( !infileimpl.openForRead(infilename.c_str(), &cache))
					throw "Loading error";
				if( !infileimpl.Render( &cache))
					throw "Render error";

				cache.Render(render);
			}
			else
			{
				if( !infileimpl.openForRead(infilename.c_str(), render))
					throw "Loading error";
				if( !infileimpl.Render( render))
					throw "Render error";
			}
		}
		printf("Loading successefully\n");
	}
	catch(char* text)
	{
		printf("Error: %s\n", text);
	}
	catch(...)
	{
		printf("Unknown Exception \n");
	}

	if( format=="-rib")
	{
		prman->RiEnd();
		printf("Save to %s\n", outfilename.c_str());
		
	}

	return 0;
}



