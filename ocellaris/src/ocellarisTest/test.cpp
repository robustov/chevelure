#include <windows.h>
#include "ocellaris\IParameter.h"
#include "ocellaris/renderFileImpl.h"
#include "shading/IShaderTemplate.h"
#include "shading/IShadingGraphNode.h"
#include "Util/DllProcedure.h"
#include "ocellaris/renderBlurParser.h"
#include "ocellaris/renderCacheImpl.h"
#include <Util/misc_directory_processing.h>
#include "ocellaris\renderExecutiveCacheImpl.h"


bool Test()
{
	{
		cls::renderCacheImpl<> cache;
//		cache.comment("sdsds");
		std::vector<std::string> s;
		s.push_back("11111");
		s.push_back("2222");
		s.push_back("3333");
		cache.Parameter("name", s);

		cls::renderFileImpl ocsfileimpl_in1;
		ocsfileimpl_in1.openForWrite("M:/temp/test.ocs", true);
		
		cache.Render(&ocsfileimpl_in1);
	}

	/*/
	{
		cls::renderFileImpl ocsfileimpl_in1;
		ocsfileimpl_in1.openForWrite("M:/temp/test.ocs", true);

		cls::PA<unsigned char> c(cls::PI_CONSTANT, 4);
		c[0] = 0;
		c[1] = 1;
		c[2] = 2;
		c[3] = 3;
		ocsfileimpl_in1.Parameter("f", c);

		Math::Vec2f g(1, 314);
		ocsfileimpl_in1.Parameter("g", g);

		std::vector<Math::Vec2f> u;
		u.push_back(Math::Vec2f(2, 3));
		u.push_back(Math::Vec2f(4, 5));
		u.push_back(Math::Vec2f(6, 7));
		ocsfileimpl_in1.Parameter("u", u, cls::PI_CONSTANT);
	}
	/*/
	/*/
	{
		cls::renderCacheImpl<> cache;
		cls::renderFileImpl ocsfileimpl_in1;
		ocsfileimpl_in1.openForRead("M:/temp/test.ocs", &cache);
		ocsfileimpl_in1.Render( &cache);

		Math::Vec2f g;
		cache.GetParameter("g", g);

		cls::PA<unsigned char> f = cache.GetParameter("f");

		std::vector<Math::Vec2f> u;
		cache.GetParameter("u", u);

		printf("%d\n", u.size());
	}

	{
		cls::renderFileImpl ocsfileimpl_in1;
		ocsfileimpl_in1.openForWrite("M:/temp/declproc.ocs", true);
		
		ocsfileimpl_in1.comment("sdsds");
		ocsfileimpl_in1.DeclareProcBegin("Proc1");
			ocsfileimpl_in1.PushAttributes();
			ocsfileimpl_in1.ParameterFromAttribute("fromextern", "ext");
			ocsfileimpl_in1.Parameter("intennal", 11);
			ocsfileimpl_in1.IfAttribute("ext", cls::P<int>(2));
				ocsfileimpl_in1.Parameter("final", 22);
			ocsfileimpl_in1.EndIfAttribute();
			ocsfileimpl_in1.PopAttributes();
		ocsfileimpl_in1.DeclareProcEnd();

		ocsfileimpl_in1.PushAttributes();
		ocsfileimpl_in1.Attribute("ext", 1);
		ocsfileimpl_in1.Call("Proc1");
		ocsfileimpl_in1.Attribute("ext", 2);
		ocsfileimpl_in1.Call("Proc1");
		ocsfileimpl_in1.Attribute("ext", 3);
		ocsfileimpl_in1.Call("Proc1");
		ocsfileimpl_in1.PopAttributes();
	}

	{
		cls::renderExecutiveCacheImpl cache;
		cls::renderFileImpl ocsfileimpl_in1;
		ocsfileimpl_in1.openForRead("M:/temp/declproc.ocs", &cache);
		ocsfileimpl_in1.Render( &cache);

		cls::renderFileImpl ocsfileimpl_in2;
		ocsfileimpl_in2.openForWrite("M:/temp/declproc_exe.ocs", true);
		cache.Render(&ocsfileimpl_in2);
	}
	/*/

	/*/
	{
		cls::renderFileImpl ocsfileimpl_in1;
		ocsfileimpl_in1.openForRead("C:/temp/blur1.ocs");
		cls::renderFileImpl ocsfileimpl_in2;
		ocsfileimpl_in2.openForRead("C:/temp/blur2.ocs");

		cls::renderBlurParser<> blurparser;

		blurparser.storage.bDump = true;

			blurparser.SetCurrentPhase(0.f);
			ocsfileimpl_in1.Render(&blurparser);

		blurparser.storage.bDump = true;
printf("\nDUMP:\n");
		blurparser.storage.Dump();

printf("\n\n");

			blurparser.SetCurrentPhase(1.f);
			ocsfileimpl_in2.Render(&blurparser);

		cls::renderFileImpl outfileimpl;
		if( !outfileimpl.openForWrite("C:/temp/blur.ocs.ocs", true))
			throw "Loading error";
		
printf("\nDUMP:\n");
		blurparser.storage.Dump();
		blurparser.Render(&outfileimpl);
	}
	/*/
	return true;
}
