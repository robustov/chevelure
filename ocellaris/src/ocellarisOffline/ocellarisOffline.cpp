// ocellarisOffline.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <maya/MLibrary.h>
#include <maya/MIOStream.h>
#include <maya/MGlobal.h>
#include <maya/MAnimControl.h>
#include <maya/MFileIO.h>
#include "ocellaris/OcellarisExport.h"

#include "ocellaris\renderFileImpl.h"
#include "ocellaris\renderCacheImpl.h"
#include "ocellaris\ChiefGenerator.h"
#include "..\generators\SlimShaderGenerator.h"
#include "..\generators\VertexVelocityAttributeGenerator.h"
#include "mathNmaya/mathNmaya.h"

#include "Util/misc_create_directory.h"

// ocellarisOffline filename.mb objectname outfilename.ocs [deltatime] [-ascii]
// ocellarisOffline //Server/Projects/Nightwatch2/KMB370_383/3D/floor_flight/scenes/Floors/floor_01_test.mb floor_01 d:/temp/temp/%04d.ocs 1

int _tmain(int argc, _TCHAR* argv[])
{
	MStatus status, stat;
	std::string workspacedir = "//server/Projects/Nightwatch2/KMB370_383/3D/floor_flight";

	char* outfilename;
	outfilename = "d:/temp/test/xx%04d.ocs";

	bool bAscii = 0;
	double deltaTime = 1;
	char* filename = "//server/Projects/Nightwatch2/KMB370_383/3D/floor_flight/scenes/Floors/floor_01_test.mb";
	char* objname = "floor_01";
	
	argc--;argv++;

	if(argc>=1)
	{
		workspacedir = argv[0];
		argc--;argv++;
	}
	if(argc>=1)
	{
		filename = argv[0];
		argc--;argv++;
	}
	if(argc>=1)
	{
		objname = argv[0];
		argc--;argv++;
	}
	if(argc>=1)
	{
		outfilename = argv[0];
		argc--;argv++;
	}
	if(argc>=1)
	{
		deltaTime = atoi(argv[0]);
		argc--;argv++;
	}
	if(argc>=1)
	{
		bAscii = strcmp(argv[0], "-ascii")==0;
		argc--;argv++;
	}
	
	size_t sz = strlen(outfilename);
	for(size_t c=0; c<sz; c++)
		if(outfilename[c]=='/')
			outfilename[c]='\\';

	status = MLibrary::initialize (true, "", true);
	if ( !status ) {
		status.perror("MLibrary::initialize");
		return (1);
	}

	MGlobal::executeCommand("loadPlugin mtor_maya6.mll");

	workspacedir = "workspace -o \"" + workspacedir + '"';
	MGlobal::executeCommand(workspacedir.c_str());

	MFileIO::newFile(true);
	stat = MFileIO::open(filename);
	if ( !stat ) 
	{
		fprintf(stderr, "File %s not opened", filename);
		return -1;
	}
	fflush(stderr);
	fflush(stdout);

	MObject sourceobj;
	if( !nodeFromName(objname, sourceobj))
		return MS::kFailure;

	MDagPath dag;
	stat = MDagPath::getAPathTo( sourceobj, dag );

	Math::Box3f box;
	MFnDependencyNode dn(sourceobj);

	cls::IExportContext context;

	ChiefGenerator chief;
	ocellarisDefaultGenerators(OCGT_SCENE, chief, &context);
	SlimShaderGenerator::bDontUseMtor = true;
	SlimShaderGenerator::bClearShaderWhenStart = false;

	VertexVelocityAttributeGenerator vvg;
	chief.addGenerator(MFn::kMesh, &vvg);
	SlimShaderGenerator::bClearShaderWhenStart = true;

//	VertexVelocityAttributeGenerator vvg;
//	chief.addGenerator(MFn::kMesh, &vvg);

	MTime dt(deltaTime, MTime::uiUnit());
	MTime t = MAnimControl::minTime();
	double pers = MAnimControl::maxTime().as(MTime::uiUnit()) - t.as(MTime::uiUnit());
	pers = pers/deltaTime;
	for( int i=0; t<=MAnimControl::maxTime(); t+=dt, i++)
	{
		MAnimControl::setCurrentTime(t);
		float ft = (float)t.as(MTime::uiUnit());
//		printf("%f:\n", ft);
		int time = i;

		MMatrix m = dag.exclusiveMatrix();
		Math::Matrix4f transform;
		::copy(transform, m);

		cls::renderCacheImpl<cls::CacheStorageSimple> cache;
		Math::Box3f box;

		cache.PushTransform();
		cache.PushAttributes();
		((cls::IRender*)&cache)->AppendTransform(transform);
		cache.PopAttributes();
		ocExportTree(sourceobj, chief, &cache, box, transform, &context);
		cache.PopTransform();
		
		std::string outfilenameform = outfilename + std::string("//%04d.ocs");
		char buf[512];
		sprintf(buf, outfilenameform.c_str(), time);
		cls::renderFileImpl _render;
		Util::create_directory_for_file(buf);
//		displayString("{%f, %f, %f}-{%f, %f, %f}", box.min.x, box.min.y, box.min.z, box.max.x, box.max.y, box.max.z);
		_render.openForWrite(buf, bAscii);
		_render.cls::IRender::Attribute("file::box", box);
		cls::IRender* rende = &_render;
		rende->Attribute("scene::time", ft);
//printf("%s\n", buf);

		cache.Render(&_render);

//		printf("R90000   %02d%%\n", i);
		int pr = (int)(100*i/pers);
		fprintf(stderr, "R90000   %02d%%\n", pr);
		fflush(stderr);
	}


	MLibrary::cleanup();

	return 0;
}

