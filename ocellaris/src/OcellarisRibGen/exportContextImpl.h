#pragma once
#include <ri.h>
#include <vector>
#include <string>
#include "ocellaris/IExportContext.h"

//! @ingroup ocellaris_group 
//! \brief ���������� IExportContext
//! 
struct ExportContextImpl : public cls::IExportContext
{
	//! ������� �����
	virtual int frame(
		){return currentframe;}
	//! FPS
	virtual float fps(
		){return ffps;}
	// ��� �������� ����
	virtual const char* layertype(
		){return slayertype.c_str();}
	//! ������� ����
	virtual const char* layer(
		){return slayer.c_str();}
	//! ������� pass
	virtual const char* pass(
		){return passname.c_str();}
	//! MotionBlur
	virtual bool motionBlur(
		){return bMotionBlur;}
	//! ��������� �����
	virtual enShutterTiming shutterTiming(
		){return eshutterTiming;}
	//! Shutter
	virtual void shutter( 
		float& open, 
		float& close, 
		bool& subframeMotion
		)
	{
		open = shutterOpen;
		close = shutterClose;
		subframeMotion = this->subframeMotion;
	}
	// ����������
	virtual const char* getOcsDirectory()
	{
		ocsDir = projectDir + "/ocs/";
		return ocsDir.c_str();
	}
	virtual const char* getOutputDirectory()
	{
		outputDir = projectDir + "/rmanpix/" + taskname + "/";
		return outputDir.c_str();
	}
	// ��������� ������, �������� � �������� ��������
	virtual cls::IRender* objectContext(
		MObject& obj, 
		cls::IGenerator* generator
		)
	{
		return NULL;
	};
	virtual void addTask(
		enTaskType type,		// ���
		const char* title,		// ���������
		const char* cmd			// �������
		)
	{
	}

/*/
	void setObjectData(
		MObject& obj, 
		cls::IGenerator* generator, 
		void* data)
	{
		genDataKey key;
		key.gen = generator;
		key.id = *(int*)&obj;
		generatordatas[key] = data;
	}
	void* getObjectData(
		MObject& obj, 
		cls::IGenerator* generator 
		)
	{
		genDataKey key;
		key.gen = generator;
		key.id = *(int*)&obj;
		std::map<genDataKey, void*>::iterator it = generatordatas.find(key);
		if( it == generatordatas.end()) return 0;
		return it->second;
	}
/*/

	/*/
	//! IRender � ������� �������
	virtual IRender* render(
		)=0;
	//! ���������� �� INodeGenerator::OnNode.
	//! ��������� ��������� ������� ��������� method ������� nodegenerator � ��������� ����� scenetime.
	//! � ����������� ����������� ������� ����� ���� �� �������� ���������� � �������� ����� getValue( DGContext(time)),
	//! �� ��� ��������� - ��� ����������, �������� ��� ������������� ��������
	virtual void delayExport( 
		double scenetime, 
		INodeGenerator* nodegenerator,
		void (INodeGenerator::*method)(INode* node, IExportContext* context)
		)=0;
	/*/

public:
	//! ������� �����
	std::string slayertype;
	std::string slayer;
	std::string passname;
	int currentframe;
	float ffps;

	bool bMotionBlur;
	enShutterTiming eshutterTiming;
	bool subframeMotion;
	float shutterOpen;
	float shutterClose;
	float shutterAngle;

	// ���������� �������
	std::string projectDir;
	std::string taskname;
	std::string ocsDir, outputDir;

	ExportContextImpl()
	{
		projectDir = "";
	}
	void setProjectDirectory(const char* filename)
	{
		projectDir = filename;
	}
	void setTaskName(const char* taskname)
	{
		this->taskname = taskname;
	}

	// ��������� ����������� �� ���� �������
	void set(RIBContext* context)
	{
		RIBContext::RenderingPass pass;
		RtBoolean declarePhase;
		context->GetRenderingPass( &pass, &declarePhase);
		switch(pass)
		{
		case RIBContext::rpFinal:
			slayertype = "final"; break;
		case RIBContext::rpShadow:
			slayertype = "shadow"; break;
		case RIBContext::rpReflection:
			slayertype = "reflection"; break;
		}
		passname = slayertype;
		bMotionBlur = context->GetMotionBlur()?true:false;
		currentframe = context->GetFrame();

		bool blurCamera;
		RIBContext::ShutterConfig shutterConfig;
		short ssubframeMotion, sblurCamera;
		context->GetMotionInfo( 
			&shutterAngle,
			&ffps,
			&ssubframeMotion,
			&sblurCamera,
			(RIBContext::ShutterTiming*)&eshutterTiming,
			&shutterConfig);

		subframeMotion = ssubframeMotion?true:false;
		blurCamera = sblurCamera?true:false;
		context->GetShutter( 
			&shutterOpen, 
			&shutterClose);

		if( !ssubframeMotion)
		{
			shutterClose = (float)(shutterOpen + (1.0 / ffps));
		}
	}

};
