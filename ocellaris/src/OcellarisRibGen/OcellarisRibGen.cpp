//#include "stdafx.h"
#include <sys/types.h>
#include <sys/stat.h>

#ifndef MSVC2003
#include <stlport/stdio.h>
#include <stlport/stdlib.h>
#include <stlport/iostream>
#if defined (_WIN32)
#include <stlport/strstream>
#else
#include <stlport/unistd.h>
#include <stlport/strstream>
#endif
#include <stlport/math.h>
#include <sys/stat.h>
#include <stlport/string>
#include <stlport/limit>

#include <stlport/iomanip>
using namespace std;
#include <stlport/stdexcept>
#endif

#include <windows.h>

#include "RIBGen.h"
#include "RIBContext.h"
#include "mathNpixar/IRIBContext_Impl.h"

#include "ocellaris/IExportContext.h"
#include "Util/misc_create_directory.h"
//#include "ocellaris/OcellarisExport.h"



#ifdef _WIN32
#define unlink _unlink
#define stat _stat
#endif

typedef bool (*EXPORT_OC_NODE)(
	const char* argsl0,
	const char* filename,
	const char* passname,
	float* camera,
	bool bTree,
	bool blur,
	double shutterOpen, 
	double shutterClose, 
	double shutterOpenFrame, 
	double shutterCloseFrame,
	double time,
	bool bAscii,
	bool bView, 
	bool bDump,
	IPrman* prman, 
	cls::IExportContext* context,
	const char* additionalString,
	bool DoExportToThisFile
	);

typedef void* (*CREATECACHEFRAMEOPENSTATE)(
	const char* argsl0,
	const char* filename,
	const char* passname,
	bool bTree,
	bool blur,
	double shutterOpen, 
	double shutterClose, 
	double shutterOpenFrame, 
	double shutterCloseFrame,
	bool bAscii,
	bool bView,
	int number
	);
typedef void (*FREECACHEFRAMEOPENSTATE)(void* a);

const char* dllname = "ocellarisMaya.mll";
const char* pn_EXPORT_CONTEXT = "getExportContextForRibGen";
const char* pn_EXPORT_TO_PRMAN = "ExportToPrman";

typedef cls::IExportContext* (*EXPORT_CONTEXT)(
	const char* options
	);

typedef bool (*EXPORT_TO_PRMAN)(
	IPrman*, 
	const char* node, 
	double time, 
	bool useMotionBlur, 
	double shutterOpen, 
	double shutterClose, 
//	double shutterOpenFrame, 
//	double shutterCloseFrame,
	double fps, 
	const float* camera, 
	bool bMayaShaders, 
	bool bSlimShaders, 
	const char* options
	);




const char* pn_CREATECACHEFRAMEOPENSTATE = "CreateCacheFrameOpenState";
const char* pn_FREECACHEFRAMEOPENSTATE = "FreeCacheFrameOpenState";

//	����������� ����� OcellarisRibGen

class OcellarisRibGen : public RIBGen 
{
	HMODULE module;
	std::vector<void*> cachedata;
public:
    OcellarisRibGen();
    virtual ~OcellarisRibGen();

    // Optionally cache internal motion-blur state at FrameOpen.
    //  This call allows certain types of RIB Generator to
    //  handle deforming motion blur.  Ostensibly the cached
    //  state will be used to compose the RIB representation
    //  of your deforming object. 
    virtual int CacheFrameOpenState( RIBContext * );

    virtual void Bound(RIBContext * c, RtBound b);
    virtual int GenRIB(RIBContext * c);

#ifndef RIBGEN_API
    virtual int SetArgs(int n, RtToken tokens[], RtPointer vals[]);
#else
    virtual int SetArgs( RIBContext *c,	int n, RtToken tokens[], RtPointer vals[]);
#endif
    
protected:			
	bool LoadRIBContext( RIBContext *c);
	void close();

	bool DoExportToThisFile;
	int frame;
	std::string furObject;
	std::string FileName;
	std::string ProjectPath;
	std::string JobName;

	bool bAscii;
	bool bDump;
	bool bIgnoreSurfaceShaders;
	bool bIgnoreDisplacementShaders;
	bool bRayTrace;
	bool bMayaShaders; 
	bool bSlimShaders;

    RtFloat shutterOpen, shutterClose;
    RtFloat shutterAngle, fps;
	RtFloat time;
    RtBoolean subframeMotion;
    RtBoolean blurCamera;
    RIBContext::ShutterTiming shutterTiming;
    RIBContext::ShutterConfig shutterConfig;
    RIBContext::RenderingPass pass;
	std::string passname;
	std::string fullFileName;
	bool bTree;

	// �������
	bool useMotionBlur;
	bool castShadows;
	
protected:
    bool fileExists(const std::string & filename) const; 
};


extern "C"
{

/*---------------------------------------------------------*/
__declspec(dllexport) RIBGen * __cdecl RIBGenCreate()
{
//printf("RIBGenCreate\n");
    return new OcellarisRibGen();
}

__declspec(dllexport) void __cdecl RIBGenDestroy(RIBGen * g)
{
    delete (OcellarisRibGen *) g;
}
}

/*---------------------------------------------------------*/
OcellarisRibGen::OcellarisRibGen()
{
//printf("OcellarisRibGen()\n");
	bRayTrace = false;
	bAscii = false;
	bIgnoreSurfaceShaders = false;
	useMotionBlur = true;
	castShadows = true;
	bDump = false;
	DoExportToThisFile = false;
	bMayaShaders = false;
	bSlimShaders = false;

	module = NULL;


}

//	������� ���������� �� ��������� ����������

OcellarisRibGen::~OcellarisRibGen() 
{
	close();
}

void OcellarisRibGen::close()
{
//printf("OcellarisRibGen::close\n");
	if( module)
	{
		FARPROC proc = GetProcAddress(module, pn_FREECACHEFRAMEOPENSTATE);
		if( proc)
		{
			FREECACHEFRAMEOPENSTATE exportproc = (FREECACHEFRAMEOPENSTATE)proc;
			for(unsigned i=0; i<cachedata.size(); i++)
				(*exportproc)(cachedata[i]);
			cachedata.clear();
		}
		FreeLibrary(module);
		module = 0;
	}
}
#ifndef RIBGEN_API
int
OcellarisRibGen::SetArgs(int n, RtToken args[], RtPointer vals[])
#else
int
OcellarisRibGen::SetArgs(RIBContext* c, int n, RtToken args[], RtPointer vals[])
#endif
{
//c->ReportError(RIBContext::reInfo, "OcellarisRibGen::SetArgs\n");

    int err = 0, i = 0;
    for (i = 0; i < n; i++)
	{
		if (!strcmp(args[i], "FileName") || !strcmp(args[i], "string FileName"))
		{
			FileName = *(static_cast<char **>(vals[i]));
			Util::correctFileName(FileName);
		}
		else if (!strcmp(args[i], "ProjectPath") || !strcmp(args[i], "string ProjectPath"))
		{
			ProjectPath = *(static_cast<char **>(vals[i]));
		}
		else if (!strcmp(args[i], "JobName") || !strcmp(args[i], "string JobName"))
		{
			JobName = *(static_cast<char **>(vals[i]));
		}
		else if (!strcmp(args[i], "Ascii") || !strcmp(args[i], "float Ascii"))
		{
			bAscii = *(RtFloat *) (vals[i]) != 0;
		}
		else if (!strcmp(args[i], "Dump") || !strcmp(args[i], "float Dump"))
		{
			bDump = *(RtFloat *) (vals[i]) != 0;
		}
		else if (!strcmp(args[i], "IgnoreSurfaceShaders") || !strcmp(args[i], "float IgnoreSurfaceShaders"))
		{
			bIgnoreSurfaceShaders = *(RtFloat *) (vals[i]) != 0;
		}
		else if (!strcmp(args[i], "IgnoreDisplacementShaders") || !strcmp(args[i], "float IgnoreDisplacementShaders"))
		{
			bIgnoreDisplacementShaders = *(RtFloat *) (vals[i]) != 0;
		}
		else if (!strcmp(args[i], "RayTrace") || !strcmp(args[i], "float RayTrace"))
		{
			bRayTrace = *(RtFloat *) (vals[i]) != 0;
		}
		else if (!strcmp(args[i], "DoExportToThisFile") || !strcmp(args[i], "float DoExportToThisFile"))
		{
			bRayTrace = *(RtFloat *) (vals[i]) != 0;
		}
		else if (!strcmp(args[i], "MayaShaders") || !strcmp(args[i], "float MayaShaders"))
		{
			bMayaShaders = *(RtFloat *) (vals[i]) != 0;
		}
		else if (!strcmp(args[i], "SlimShaders") || !strcmp(args[i], "float SlimShaders"))
		{
			bSlimShaders = *(RtFloat *) (vals[i]) != 0;
		}
		else
		{
//			err++;
		}
    }
    return err;
}

extern "C" {
    RtVoid dl_FurProceduralFree(RtPointer data)
	{
		RtString* sData = (RtString*) data;
		free(sData[0]);
		free(sData[1]);
		free(sData);
    }
}

void OcellarisRibGen::Bound(RIBContext* c, RtBound)
{
//c->ReportError(RIBContext::reInfo, ">>> 4. GenRIB 0x%x", this);
}



int OcellarisRibGen::CacheFrameOpenState( RIBContext * c)
{
//c->ReportError(RIBContext::reInfo, ">>> 5. GenRIB 0x%x", this);
	/*/
	c->ReportError(RIBContext::reInfo, ">>> CacheFrameOpenState 0x%x", this);
	if( !LoadRIBContext(c))
	{
		return -1;
	}
	if( !useMotionBlur)
		return 0;

	if( module)
	{
		FARPROC proc = GetProcAddress(module, pn_CREATECACHEFRAMEOPENSTATE);
		if( proc)
		{
			CREATECACHEFRAMEOPENSTATE exportproc = (CREATECACHEFRAMEOPENSTATE)proc;
			void* cd = (*exportproc)(
				furObject.c_str(), fileName.c_str(), passname.c_str(), bTree, 
				useMotionBlur,
				shutterOpen, shutterClose, shutterOpen*fps, shutterClose*fps, 
				bAscii, false, this->cachedata.size());
			if(cd)
				this->cachedata.push_back(cd);
		}
		else
		{
			c->ReportError(RIBContext::reInfo, "\nCant Find ExportOcNode");
			return -1;
		}
	}
	else
	{
		c->ReportError(RIBContext::reInfo, "\nCant Load OcellarisMaya.mll");
		return -1;
	}
	/*/
	return 0;
}

int OcellarisRibGen::GenRIB(RIBContext * c)
{
	if( !LoadRIBContext(c))
	{
		return -1;
	}

    std::string errorString;
	if( bDump)
		c->ReportError(RIBContext::reInfo, "\nStart RibExport OcellarisRibGen");

	RtMatrix camera;
	c->GetCamBasis( camera, 1);

	passname = "";
    switch (pass)
	{
		case RIBContext::rpInvalid:
			passname = "invalid";
			break;
		case RIBContext::rpTraverseOnly:
			passname = "TraverseOnly";
			break;
		case RIBContext::rpFinal:
			passname = "final";
			break;
		case RIBContext::rpShadow:
			passname = "shadow";
			break;
		case RIBContext::rpReflection:
			passname = "reflection";
			break;
		case RIBContext::rpEnvironment:
			passname = "environment";
			break;
		case RIBContext::rpArchive:
			passname = "archive";
			break;
			
		default:
			break;
    }
	if( bDump)
		c->ReportError(RIBContext::reInfo, "\npassname=%s", passname.c_str());


	// ��������. ��� ���������
	if(bRayTrace)
	{
		int camera = 1;
		int trace = 1;
		int photon = 1;
		c->Attribute("visibility", "int camera", &camera, "int trace", &trace, "int photon", &photon, RI_NULL);
//		c->Attribute( "visibility" "int camera" [1] "int trace" [1] "int photon" [1]
	}

	this->module = LoadLibrary(dllname);
	if( module)
	{
		/*/
		if( !useMotionBlur && this->cachedata.size()==0)
		{
			FARPROC proc = GetProcAddress(module, pn_CREATECACHEFRAMEOPENSTATE);
			if( proc)
			{
				CREATECACHEFRAMEOPENSTATE exportproc = (CREATECACHEFRAMEOPENSTATE)proc;
				void* cd = (*exportproc)(
					furObject.c_str(), fileName.c_str(), passname.c_str(), bTree, 
					useMotionBlur,
					shutterOpen, shutterClose, shutterOpen*fps, shutterClose*fps, time, 
					bAscii, false, this->cachedata.size());
				if(cd)
					this->cachedata.push_back(cd);
			}
			else
			{
//				close();
				c->ReportError(RIBContext::reInfo, ">>> Cant Find ExportOcNode");
				return -1;
			}
		}
		/*/

		std::string additionalString = "";
		if(bIgnoreSurfaceShaders)
			additionalString += "Attribute uniform bool file::ignoreSurface = {1};";
		if(bIgnoreDisplacementShaders)
			additionalString += "Attribute uniform bool file::ignoreDisplace = {1};";

		const float* mview = &camera[0][0];

		FARPROC proc1 = GetProcAddress(module, pn_EXPORT_CONTEXT);
		FARPROC proc2 = GetProcAddress(module, pn_EXPORT_TO_PRMAN);
		if( proc1 && proc2)
		{
			EXPORT_CONTEXT  exportcontext = (EXPORT_CONTEXT)proc1;
			EXPORT_TO_PRMAN exporttoprman = (EXPORT_TO_PRMAN)proc2;
			cls::IExportContext* context = (*exportcontext)("");

			
			context->setEnvironment( "PROJECTDIR",	ProjectPath.c_str());
			context->setEnvironment( "JOBNAME",		JobName.c_str());
			context->setEnvironment( "OUTPUTFILENAME", FileName.c_str());
			if( bAscii)
				context->setEnvironment( "OUTPUTFORMAT", "ascii");
			else
				context->setEnvironment( "OUTPUTFORMAT", "binary");
			context->setEnvironment( "PASSNAME", passname.c_str());
			
//			shutterOpen, shutterClose, shutterOpen*fps, shutterClose*fps, time, 
//			bTree
//			useMotionBlur,
//			shutterOpen, shutterClose, shutterOpen*fps, shutterClose*fps, time, 
//			this->cachedata.size());
			
			RibContext_Impl prman(c);
			const float* mview = &camera[0][0];
			bool res = (*exporttoprman)(
				&prman, 
				furObject.c_str(),
				time*fps, 
				useMotionBlur, 
				shutterOpen, shutterClose, 
//				shutterOpen*fps, shutterClose*fps,
				fps, 
				mview, 
				bMayaShaders, 
				bSlimShaders, 
				""
				);
			if(!res)
			{
				c->ReportError(RIBContext::reInfo, ">>> ExportOcNode FAILED!!!");
				return -1;
			}
			std::string fn = context->getEnvironment("OUTPUTFILENAME");
			FileName = fn;
		}
		else
		{
			close();
			c->ReportError(RIBContext::reInfo, ">>> %s: Cant Find %s or %s", furObject.c_str(), pn_EXPORT_CONTEXT, pn_EXPORT_TO_PRMAN);
			return -1;
		}
	}
	else
	{
		close();
		c->ReportError(RIBContext::reInfo, ">>> %s: Cant Load %s", furObject.c_str(), dllname);
		return -1;
	}

/*/
// ��������� ������ � ����������� ������� ��� ������ � ���  
    RtString *data = static_cast<RtString*>(malloc(sizeof(RtString) * 2));
#if defined (_WIN32)
    data[0] = strdup("OcellarisPrmanDSO.dll");
#else
    data[0] = strdup("OcellarisDSO.so");
#endif
	int textsize = 10000;
    data[1] = static_cast<char*>(malloc(sizeof(char)*(textsize+2)));
	if( !useMotionBlur) 
		shutterOpen = shutterClose = time;
    _snprintf(data[1], textsize, "%s$%f$%f$%f$%s", fileName.c_str(), time, shutterOpen, shutterClose, attributes);


//	����� ����������� ����� � ���
#if RIBCONTEXT_API == 2
    c->Procedural(data, bound, c->ProcDynamicLoad, dl_FurProceduralFree);
#elif RIBCONTEXT_API > 2
    c->Procedural(data, bound, c->GetProcSubdivFunc(RIBContext::kDynamicLoad), dl_FurProceduralFree);
#else
    Error - OcellarisRibGen requires a more modern implementation of the RIB Context interface.
#endif

/*/
	close();
    return 1;
}

bool OcellarisRibGen::fileExists(const std::string & filename) const
{
    struct stat sbuf;
	return (stat(filename.c_str(), &sbuf) == 0);
}


bool OcellarisRibGen::LoadRIBContext( RIBContext *c)
{
    furObject = c->GetObjName();  
   	frame = c->GetFrame(); // �������� �������� �����
    if (furObject.empty())
		return false;


    // ������ ���� �������� ������ � ��������� ��� motion blur'�    
    useMotionBlur = c->GetMotionBlur()?1:0; 

    c->GetShutter(&shutterOpen, &shutterClose);
    c->GetMotionInfo(&shutterAngle, &fps, &subframeMotion, &blurCamera, &shutterTiming, &shutterConfig);
    
	if( shutterOpen==shutterClose) 
		useMotionBlur = 0;

	if( !subframeMotion)
	{
	    shutterClose = (float)(shutterOpen + (1.0 / fps));
		//c->ReportError(RIBContext::reInfo, "\nsubframeMotion");
	}
	if( bDump)
		c->ReportError(RIBContext::reInfo, "\nshutterOpen=%.5f shutterClose=%.5f shutterAngle=%.5f fps=%.5f, subframeMotion=%d, blurCamera=%d, shutterTiming=%d, shutterConfig=%d", 
			shutterOpen, shutterClose, 
			shutterAngle, fps, (int)subframeMotion, (int)blurCamera, (int)shutterTiming, (int)shutterConfig);

    RtBoolean decl;
    c->GetRenderingPass(&pass, &decl);
    if( decl)
	{
		c->ReportError(RIBContext::reError, ">>> OcellarisRibGen: GetRenderingPass decl!=0.");
		return false;
    }
//	else if (pass == RIBContext::rpShadow)
//	{
//		if (!castShadows) 
//			return false;		// �� ������� ������������ ����, ���� �������� castShadow �� off
  //  }

    if( FileName.empty())
	{
		c->ReportError(RIBContext::reError, ">>> OcellarisRibGen: must specify a filename to output to file.");
		return false;
    }
	//	��������� ��������� � ��� ����� ���� ��� ����������
	std::string passname = "";
    switch (pass)
	{
		case RIBContext::rpInvalid:
			passname = "invalid";
			break;
		case RIBContext::rpTraverseOnly:
			passname = "TraverseOnly";
			break;
		case RIBContext::rpFinal:
			passname = "final";
			break;
		case RIBContext::rpShadow:
			passname = "shadow";
			break;
		case RIBContext::rpReflection:
			passname = "reflection";
			break;
		case RIBContext::rpEnvironment:
			passname = "environment";
			break;
		case RIBContext::rpArchive:
			passname = "archive";
			break;
			
		default:
			break;
    }
	fullFileName = FileName + "." + passname;

	time = frame/fps;

	bTree = false;

	return true;
}
