#include "stdafx.h"
#include "ocExport_InstancerData.h"


#include <maya/MGlobal.h>
#include <maya/MAnimControl.h>
#include <maya/MFnParticleSystem.h>

#include "ocInstance.h"
#include "ocParticleInstancer.h"
#include "Util/misc_create_directory.h"
#include "mathNmaya/mathNmaya.h"

#include "ocellaris/OcellarisExport.h"
#include "ocellaris/renderStringImpl.h"

extern "C" {
    RtVoid dl_FurProceduralFree(RtPointer data);
}

bool swapIdNNumber = true;
MStatus InstancerData::LoadParticles(
	MObject psobj,
//	MObject instancerobj, 
	bool bDump
	)
{
	this->psobj = psobj;
	MObject instancerobj;
	instancerobj = MObject::kNullObj;

	{
		MDagPath path;
		MDagPath::getAPathTo(instancerobj, path);
		MMatrix instancer_transform = path.inclusiveMatrix();
	}

	MStatus stat;
	{
		MFnDependencyNode dn(psobj);
		// ������� ���������
		MPlug inputPoints = dn.findPlug("instanceData", &stat);
		if( !stat)
		{
			displayString("!findPlug instanceData");
			return MS::kFailure;
		}
		for(int i=0; i<(int)inputPoints.numElements(); i++)
		{
			MPlug pp = inputPoints[i];

			int ch = 1;
			pp = pp.child(ch);

			MPlugArray array;
			pp.connectedTo(array, false, true);
			if(array.length()==0)
				continue;

			instancerobj = array[0].node();
//			displayString("find %s instancer", MFnDependencyNode(instancerobj).name().asChar());
			break;
		}
		if( instancerobj.isNull())
		{
			displayString("cant find instancer");
			return MS::kFailure;
		}
	}

	MFnDependencyNode dn(instancerobj);

	phases.LoadPhases(instancerobj, psobj, bDump);

	int particlescount = 0;
	// position + id + phase
	int instancerId = *(int*)(&instancerobj);

	// ID
	std::map<int, int> idIndexId_Num;
	std::map<int, int> idIndexNum_Id;
	{
		MFnParticleSystem ps(psobj);

		unsigned i;
		/*/
		MIntArray particleIds;
		ps.particleIds(particleIds);
		for(i=0; i<particleIds.length(); i++)
		{
			int id = particleIds[i];
			idIndexparticleIds_Num[id] = i;
			if(bDump)
				displayString("particleIds = %d, ind=%d", id, i);
		}
		/*/

		MPlug paramplug = ps.findPlug("id", &stat);
		if( !stat)
		{
			displayString("!findPlug2+");
			return MS::kFailure;
		}
		MObject idobj;
		if( !paramplug.getValue( idobj))
		{
			displayString("!findPlug2++");
			return MS::kFailure;
		}
		MFnDoubleArrayData id(idobj, &stat);
		if( !stat)
		{
			displayString("!findPlugX");
			return MS::kFailure;
		}
		for(i=0; i<id.length(); i++)
		{
			int value = (int)id[i];
			if(bDump)
				displayString("id = %d, ind=%d", value, i);
			// ��� ����� ���������
			idIndexId_Num[ value] = i;
			idIndexNum_Id[ i] = value;
		}
	}

	// READ instancer
	int errors = 0;
	int maxPhasesCount = 1;
	MItInstancer it;
	// ����� ������� � ����������
	int instancerParticleId = -1, lastParticleId = -1;
	while( ! it.isDone() ) 
	{
		int id = it.particleId();
		MMatrix	matrix = it.matrix();
		int pathid = it.pathId();

		if( it.instancerId() != instancerId)
		{
			if(bDump)
				displayString("id=%d != %d", it.instancerId(), instancerId);
			it.nextInstancer();
			continue;
		}

		// ����� ������� � ����������
		if( id != lastParticleId)
		{
			instancerParticleId++;
			lastParticleId = id;
		}

		MDagPath phasepath = it.path();
		std::string phasename = phasepath.fullPathName().asChar();
		int p = phases.findPhase(phasename);
		if( p<0)
		{
			if(bDump)
				displayString("can't find phase number for %s", phasename.c_str());
			errors++;
			it.next();
			continue;
		}
		PhaseData& phaseData = phases.getPhase(p);

		if(bDump)
			displayString("id = %d, pathid=%d", id, pathid);

		MMatrix m = phaseData.matrix*matrix;

		int indexInPS = 0;
		if( id == instancerParticleId)
		{
			indexInPS = idIndexNum_Id[id];
			std::swap(indexInPS, id);
		}
		else if( idIndexId_Num.find(id)!=idIndexId_Num.end())
		{
			indexInPS = idIndexId_Num[id];
		}
		else
		{
			// ������ �������
			if(bDump)
				displayString("idIndexMap.find(%d)==idIndexMap.end()", id);
			errors++;
			it.next(); 
			continue;
		}

		// Box
		MBoundingBox particleBox;
		for(int c=0; c<8; c++)
		{
			MPoint pt = phaseData.getBoxCorner(c);
			MVector pos = pt*m;
			particleBox.expand(pos);
		}
		this->box.expand(particleBox);

		// particleIds
		std::map<int, particleIdData>::iterator itid = particleIds.find(id);
		if( itid == particleIds.end())
		{
			itid = particleIds.insert(std::map<int, particleIdData>::value_type(id, particleIdData())).first;

			itid->second.id = id;
			itid->second.indexInPS = indexInPS;
			itid->second.particles.reserve( maxPhasesCount);
		}
		particleIdData& pid = itid->second;
		pid.box.expand(particleBox);

		// particleData
		particleData pd;
		pd.id = id;
		pd.pathid = pathid;
		pd.phase = p;
		pd.matrix = m;
		pid.particles.push_back(pd);
		maxPhasesCount = __max( (int)itid->second.particles.size(), maxPhasesCount);

		particlescount++;

		it.next(); 
	}	

	if( errors)
	{
		displayString("LoadParticles(...) %d ERRORs", errors);
	}

	return MS::kSuccess;
}

bool InstancerData::Export(
	MObject psobj,
	IPrman* prman,
	InstancerData* instancedatas, 
	int count, 
	double shutterOpen, double shutterClose, 
	const char* additionalString,
	bool bDump
	)
{
	std::vector<int> ids;
	{
		MStatus stat;
		MFnDependencyNode ps(psobj);
		MPlug paramplug = ps.findPlug("id", &stat);
		if( !stat) return false;
		MObject idobj;
		if( !paramplug.getValue( idobj)) return false;
		MFnDoubleArrayData id(idobj, &stat);
		ids.resize(id.length());
		for(unsigned i=0; i<id.length(); i++)
		{
			ids[i] = (int)id[i];
		}
	}

	attributes_t attributeslist;
	std::vector<particleData*> blur;
	std::vector<float> times;
	for(int b=0; b<count; b++)
	{
		times.push_back( instancedatas[b].getTime(count, shutterOpen, shutterClose));
	}

	RtMatrix m0;
	instancedatas[0].instancer_transform.get(m0);
	prman->RiTransformBegin();
	prman->RiTransform(m0);

	InstancerData* instancedata = &instancedatas[0];
	std::map<int, particleIdData>::iterator itid = instancedata->particleIds.begin();
	for(int i=0; itid != instancedata->particleIds.end(); itid++, i++)
	{
		particleIdData& pid = itid->second;
		if( pid.id<0) continue;

		prman->RiAttributeBegin();

		RtBound bound;
		copyBox(bound, pid.box);
		prman->RiBound(bound);

		for(int p=0; p< (int)pid.particles.size(); p++)
		{
			particleData& pd = pid.particles[p];

			if(count>1)
			{
				// ����� � ��������� ����� id
				blur.clear();
				for(int b=1; b<count; b++)
				{
					particleData* pd1 = instancedatas[b].getParticleData(pid.id, p);
					if( !pd1) break;
					blur.push_back(pd1);
				}
				if(blur.size()!=count-1) continue;
			}


			PhaseData& phaseData = instancedata->phases.getPhase(pd.phase);
			if( !phaseData.cache)
				continue;

			std::string filename = "xxx";

			prman->RiAttributeBegin();

			RtMatrix m0;
			pd.matrix.get(m0);

			prman->RiTransformBegin();
			if(count>1)
			{
				prman->RiMotionBeginV(count, &times[0]);
				prman->RiConcatTransform(m0);
				for(int b=0; b<(int)blur.size(); b++)
				{
					RtMatrix m;
					blur[b]->matrix.get(m);
					prman->RiConcatTransform(m);
				}
				prman->RiMotionEnd();

			}
			else
			{
				prman->RiConcatTransform(m0);
			}
			std::string ocsfilename = phaseData.cache->drawcache.getFile();


			std::string attributes = "";
			cls::renderStringImpl attrrender;
			if( attrrender.openForWrite())
			{
				attrrender.Attribute("global::id", cls::P<float>( (float)pid.id));

				const std::map<std::string, cls::Param>* attrs = phaseData.cache->drawcache.getAttributes();
				std::map<std::string, cls::Param>::const_iterator it = attrs->begin();
				for(;it != attrs->end(); it++)
				{
					std::string attrname = it->first;
					attributes_t::iterator x = attributeslist.find(attrname);
					if(x==attributeslist.end())
					{
						// ������ �������
						readAttribute( psobj, ids, attrname, attributeslist[attrname]);
						x = attributeslist.find(attrname);
					}
					param_pp_t& idparams = x->second;
					param_pp_t::iterator pit = idparams.find(pid.id);
					if(pit==idparams.end()) 
					{
						if(bDump)
							displayString("readAttributes for %d failed", pid.id);
						continue;
					}

					attrname = "global::"+attrname;
					attrrender.Attribute(attrname.c_str(), pit->second);
				}
				attributes = attrrender.getWriteBuffer();
				attributes += additionalString;
			}
			else
			{
				displayStringError("ExportOc: renderFileImpl cant create temporary file!!!");
			}

//			std::string fileName = "M:\\public\\fruits\\apple.ocs";
			RtString *data = static_cast<RtString*>(malloc(sizeof(RtString) * 3));
			data[0] = strdup("OcellarisPrmanDSO.dll");
			int textsize = 10000;
			data[1] = static_cast<char*>(malloc(sizeof(char)*(textsize+2)));
			_snprintf(data[1], textsize, "%s$%f$%f$%f$%s", ocsfilename.c_str(), 0.f, 0.f, 0.f, attributes.c_str());
//			_snprintf(data[1], textsize, "%s$%f$%f$%f$%s", ocsfilename.c_str(), 0.f, 0.f, 0.f, "");
//			data[1][0] = 0;
			data[2] = 0;

			RtBound bound;
			copyBox(bound, phaseData.box);
			prman->RiProcedural(data, bound, prman->RiProcDynamicLoad(), dl_FurProceduralFree);
			
			prman->RiTransformEnd();
			prman->RiAttributeEnd();
		}
		prman->RiAttributeEnd();
	}
	prman->RiTransformEnd();
	return true;
}
void InstancerData::readAttribute(MObject psobj, std::vector<int>& ids, std::string name, param_pp_t& params)
{
	MFnDependencyNode dn(psobj);
	MPlug plug = dn.findPlug( ("cls_"+name).c_str());
	if( plug.isNull()) 
		return;
	MString attrname;
	plug.getValue(attrname);
	if( attrname.length()==0)
		return;
	
	plug = dn.findPlug(attrname);
	if( plug.isNull())
	{
		displayString("Plug %s not found in %s", attrname.asChar(), dn.name().asChar());
		return;
	}

	MObject valobj;
	plug.getValue(valobj);
	MFn::Type t = valobj.apiType();
	if( t == MFn::kVectorArrayData)
	{
		MFnVectorArrayData scaleV(valobj);
		if( ids.size()!=scaleV.length()) return;
		for(unsigned i=0; i<scaleV.length(); i++)
		{
			Math::Vec3f scale;
			::copy( scale, scaleV[i]);
			params[ ids[i] ] = cls::P<Math::Vec3f>(scale);
		}
	}
	else if( t == MFn::kDoubleArrayData)
	{
		MFnDoubleArrayData scaleS(valobj);
		if( ids.size()!=scaleS.length()) return;
		for(unsigned i=0; i<scaleS.length(); i++)
			params[ ids[i] ] = cls::P<float>( (float)scaleS[i]);
	}
}

float InstancerData::getTime(int fullcount, double shutterOpen, double shutterClose)
{
	if(fullcount<=1) return 0;

	double time = shutterOpen + num*(shutterClose-shutterOpen)/(fullcount-1);
	return (float)time;
}

InstancerData::particleData* InstancerData::getParticleData(
	int id, int index)
{
	if( particleIds.find(id)==particleIds.end())
		return NULL;
	particleIdData& pid = particleIds[id];
	if( index<0 || index >= (int)pid.particles.size())
		return NULL;

	particleData& pd = pid.particles[index];

	return &pd;
}









