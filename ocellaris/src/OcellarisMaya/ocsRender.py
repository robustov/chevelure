import ocsCommands
#from ctypes import *
from time import *
import maya.mel as mel
import maya.cmds as cmds
import maya.OpenMaya as OpenMaya
import maya.OpenMayaAnim as anim
import os.path

# remote="local" - render localy
# remote="alfred" - make rib localy render remote
# remote="make alfred" - make rib localy render remote dont start render
# remote="fast alfred" - make rib in maya (fastDistStart) render remote
# remote="dist alfred" - make rib remote render remote

def render(
		passname="", 
		sequence=0, 
		remote="local", 
		render="prman", 
		displaytype="", 
		renderLayers=0, 
		pause=0, 
		bDump = False, 
		alffile="", 
		scenename="", 
		useMtor=0
		):
	reload (ocsCommands)

	alfred=0
	fastDistStart = False
	startrender = True

	if bDump:
		print "clear:"
	ocsCommands.clear()

	if scenename!="":
		ocsCommands.setenv("SCENENAME", scenename);
	
	if remote=="local":
		alfred=0
		fastDistStart = False
	elif remote=="alfred":
		alfred=1
		fastDistStart = False
	elif remote=="make alfred":
		alfred=1
		fastDistStart = False
		startrender = False
	elif remote=="fast alfred":
		alfred=1
		fastDistStart = True
	elif remote=="dist alfred":
		setupEnv(passname);
		jobname = ocsCommands.getenv("JOBNAME");
		
		scene = ""
		curscene = mel.eval("file -q -sceneName")
		print curscene
		if curscene=="":
			print "Save scene before"
			return;
		scdir = os.path.dirname(curscene);
		extention = mel.eval( "fileExtension( \"" + curscene + "\")" )
		unknownnodes = mel.eval( "ls -type unknown")
		
		if (extention == "ma") and (unknownnodes.size()!=0):
			scene = scdir+"/"+ocsCommands.getenv("UNINAME")+".ma"
			cmds.file( scene, f=True, op="v=0", type="mayaAscii", pr=True, ea=True)
		else:
			scene = scdir+"/"+ocsCommands.getenv("UNINAME")+".mb"
			cmds.file( scene, f=True, op="v=0", type="mayaBinary", pr=True, ea=True)
		workspace = mel.eval( "workspace -fn");

		options = ""		
		options += " -passname "	+ '"'+passname+'"';
		options += " -sequence "	+ repr(sequence);
		options += " -remote "		+ '"make alfred"';
		options += " -render "		+ '"'+render+'"';
		options += " -displaytype " + '"'+displaytype+'"';
		options += " -renderLayers " + repr(renderLayers);
		options += " -pause "		+ repr(pause);
		options += " -bDump "		+ repr(bDump);
		options += " -scenename "		+ '"'+ocsCommands.getenv("SCENENAME")+'"';
		print options
		print scene
		print workspace
		
		#start alfed task
		ocsCommands.distalfred(options, scene, workspace);
		return

	#print ("ocsRender.render("+passname+", "+sequence+", "+alfred+", "+renderLayers+", "+pause+")")

	#-------------------------------------------
	# SETUP ENVIRIONMENTS
	if passname=="":
		passname = mel.eval("Ocellaris_SelectFinalPass")

	if alffile!="":
		ocsCommands.setenv("ALFFILE", alffile)
	
	if pause==1:
		ocsCommands.setenv("PAUSERENDER", "1")
		print ("pauserender is 1")
	#ocsCommands.setenv("PATH", "m:/ulitkabin/bin");
	#ocsCommands.setenv("SLIMDUMP", "1");
	ocsCommands.setenv("SLIMCOPYSHADERS", "1");
	if displaytype=="":
		displaytype="it";
	ocsCommands.setenv("DISPLAYTYPE", displaytype);

	ocsCommands.setenv("RENDER", render);
	if render=="prman":
		ocsCommands.setenv("RENDERCOMMAND", "prman.exe -p:1 -Progress")
		ocsCommands.setenv("RENDERSHADOWCOMMAND", "prman.exe -p:1 -Progress")
	if render=="netrender":
		#ocsCommands.setenv("RENDERCOMMAND", "netrender.exe -f -Progress %H")
		ocsCommands.setenv("RENDERCOMMAND", "netrender.exe -f -Progress")
		ocsCommands.setenv("RENDERSHADOWCOMMAND", "netrender.exe -f -Progress")
		alfred = True

	# envinronment
	if bDump:
		print "envinronments:"
	setupEnv(passname);

	if not alfred:
		fastDistStart = False


	#-------------------------------------------
	# fast Dist Start
	if sequence and fastDistStart:
		if bDump:
			print "fast dist start:"
		ocsCommands.fastdiststart("begin");

		secStartFrame = mel.eval( "getAttr "+passname+".secStartFrame")
		secEndFrame   = mel.eval( "getAttr "+passname+".secEndFrame")
		secEachFrame  = mel.eval( "getAttr "+passname+".secEachFrame")
		for i in range(secStartFrame, secEndFrame+1):
			ocsCommands.fastdiststart_addframe(i)
			
		ocsCommands.fastdiststart("end");


	#-------------------------------------------
	# generators
	if bDump:
		print "generators:"
	ocsCommands.addDefaultGenerators("")
	ocsCommands.addGeneratorForType("shape", "MayaShader")
	if useMtor:
		if mel.eval("exists mtor"):
			ocsCommands.addGeneratorForType("shape", "SlimShader")
	ocsCommands.addGeneratorForType("spotLight", "PassShadow")
	ocsCommands.addGeneratorForType("renderLayer", "PassRenderLayer")
	#ocsCommands.addGeneratorForType("light", "Light");
	ocsCommands.addGeneratorForType("light", "SpotLight");
	ocsCommands.preparegenerators()

	if bDump:
		ocsCommands.dump("-generators")

	#-------------------------------------------
	# add root node
	if bDump:
		print "addnodes:"
	ocsCommands.addnodes("")
	if bDump:
		ocsCommands.dump("-nodes")

	#-------------------------------------------
	# passes
	if bDump:
		print "add pass:"
	passes = list()
	if renderLayers==0:
		ocsCommands.addpass(passname, "")
		passes.append( passname)
	else:
		ocsCommands.setenv( "RENDERPASSNODE", passname)
		dgIterator = OpenMaya.MItDependencyNodes(OpenMaya.MFn.kRenderLayer)
		while not dgIterator.isDone():
			obj = dgIterator.thisNode()
			dn = OpenMaya.MFnDependencyNode(obj)
			name = dn.name()
			passes.append( name)
			ocsCommands.addpass(name, "")
			dgIterator.next()
		
	if bDump:
		ocsCommands.dump("-passes")

	#-------------------------------------------
	# lights
	if bDump:
		print "add lights:"
	dagIterator = OpenMaya.MItDag(OpenMaya.MItDag.kDepthFirst, OpenMaya.MFn.kLight)
	while not dagIterator.isDone():
		path = OpenMaya.MDagPath()
		dagIterator.getPath(path)
		name = path.fullPathName()
		ocsCommands.addlight(name)
		if bDump:
			print ("add pass: "+name)
		ocsCommands.addpass(name)
		if bDump:
			print ("connect passes: "+passname+"->"+name)
		for i in range(len(passes)):
			ocsCommands.connectpass(passes[i], name)
		dagIterator.next()
		
	if bDump:
		ocsCommands.dump("-lights")

	#-------------------------------------------
	# prepare frames
	if bDump:
		print "prepare frames:"
	if sequence:
		secStartFrame = mel.eval( "getAttr "+passname+".secStartFrame")
		secEndFrame   = mel.eval( "getAttr "+passname+".secEndFrame")
		secEachFrame  = mel.eval( "getAttr "+passname+".secEachFrame")
		for i in range(secStartFrame, secEndFrame+1):
			ocsCommands.addframe(i)
	else:
		animControl = anim.MAnimControl
		t = animControl.currentTime()
		frame = t.asUnits(t.uiUnit())
		ocsCommands.addframe(frame)
		
		
	if bDump:
		ocsCommands.dump("-frames")

	#-------------------------------------------
	# prepare nodes
	if bDump:
		print "prepare nodes:"
	ocsCommands.preparenodes("ocellaris", "prepare nodes")

	#-------------------------------------------
	# export
	if bDump:
		print "export:"
	dumpoptions = "";
	if bDump:
		dumpoptions = "-dump";
		#dumpoptions = "-dumptree";
	options = "";
	if fastDistStart:
		options += "-fastdiststart ";
	res = ocsCommands.export(dumpoptions, options, "ocellaris", "export")
	if res==0:
		# user cancel
		return

	#-------------------------------------------
	# start render
	if not fastDistStart:
		renderoptions = ""
		if alfred!=0:
			renderoptions += "-alfred "
		if startrender!=0:
			renderoptions += "-start "
			
		if bDump:
			print "render:"
		ocsCommands.render(renderoptions)
	#

def getDirectory(name, passname):
	reload (ocsCommands)
	# envinronment
	print "envinronments:"
	setupEnv(passname);
	
	val = ocsCommands.getenv(name);
	return val;
	
def isUNC(str):
	if (len(str)>1) and (str[0]=='\\') and (str[1]=='\\'):
		return True
	if (len(str)>1) and (str[0]=='/') and (str[1]=='/'):
		return True
	return False

def setupEnv(passname):
	# WORKSPACEDIR
	wd = mel.eval( "workspace -fn");
	ocsCommands.setenv("WORKSPACEDIR", wd)
	
	# SCENENAME
	scenename = ocsCommands.getenv("SCENENAME")
	if scenename=="":
		scenenames = mel.eval( "file -q -shn -l");
		scenename = scenenames[0]
	ocsCommands.setenv("SCENENAME", scenename)

	# JOBNAME = scenename+passname
	jobname = scenename+"_"+passname
	ocsCommands.setenv("JOBNAME", jobname)

	# RIBDIR
	ribDir = mel.eval( "workspace -q -fre RIB");
	if ribDir=="":
		ribDir="DATA/rib";
		mel.eval( "workspace -fr RIB \""+ribDir+"\"");

	ribDir = ribDir+"/"+scenename+"/"+passname
	if not isUNC(ribDir):
		ribDir = wd+"/"+ribDir
	ocsCommands.setenv("RIBDIR", ribDir)

	# STATDIR
	statdir = ribDir + "/.statistic"
	ocsCommands.setenv("STATDIR", statdir)

	# UNEQUAL FILENAME
	t = time();
	uniname = jobname+repr(t);
	ocsCommands.setenv("UNINAME", uniname)

	# SHADOWDIR
	shdDir = mel.eval( "workspace -q -fre shadow");
	if shdDir=="":
		shdDir="DATA/shd";
		mel.eval( "workspace -fr shadow \""+shdDir+"\"");

	shdDir = shdDir+"/"+scenename+"/"+passname
	if not isUNC(shdDir):
		shdDir = wd+"/"+shdDir
	ocsCommands.setenv("SHADOWDIR", shdDir);

	# OUTPUTDIR
	outputDir = mel.eval( "workspace -q -rte images");
	if outputDir=="":
		outputDir="DATA/output";
	outputDir = outputDir+"/"+scenename+"/"+passname
	if not isUNC(outputDir):
		outputDir = wd+"/"+outputDir
	ocsCommands.setenv("OUTPUTDIR", outputDir);

	# SHADERDIR
	shaders = ribDir+"/shaders";
	ocsCommands.setenv("SHADERDIR", shaders);

	# TEXDIR
	texdir = ribDir+"/tex";
	ocsCommands.setenv("TEXDIR", texdir);

	#SHADERCOMPILER
	#compiler = "%RMANTREE%/bin/shader.exe -I%RATTREE%/lib/slim/include -o %s %s"

	RATTREE = mel.eval( "getenv(\"RATTREE\")")
	RMSTREE = mel.eval( "getenv(\"RMSTREE\")")
	RMANTREE = mel.eval( "getenv(\"RMANTREE\")")
	#SEARCHPATH = mel.eval( "getenv(\"ULITKABIN\")")
	SEARCHPATH = ""

	#compiler = RMSTREE+"/rmantree/bin/shader.exe -autoplug -C -I"+RATTREE+"/lib/slim/include -I%ULITKABIN%/include -o %s %s"
	compiler = RMANTREE+"/bin/shader.exe -autoplug -C \"-I%ULITKABIN%/include\" -o \"%s\" \"%s\""
	#compiler = RMANTREE+"/bin/shader.exe \"-I%ULITKABIN%/include\" -o \"%s\" \"%s\""
	ocsCommands.setenv("SHADERCOMPILER", compiler)
	ocsCommands.setenv("SEARCHDIR", SEARCHPATH)

	# 
	# OCSDIR
	# OUTPUTFILE
	# IGNOREOVERRIDE

