#include "stdafx.h"
#include "ocInstance.h"

#ifdef USE_LOCATOR_INSTANCE

#include <maya/MGlobal.h>
#include <maya/MFnPluginData.h>
#include <maya/MFnUnitAttribute.h>
#include "ocellaris\renderFileImpl.h"
#include "ocellaris\renderOpenglImpl.h"
#include "mathNgl\mathNgl.h"
#include "mathNmaya\mathNmaya.h"
#include "Util/misc_directory_processing.h"
#include "ocellaris/ocellarisExport.h"

MObject ocInstance::i_time;
MObject ocInstance::i_startTime;
MObject ocInstance::i_drawCache;
MObject ocInstance::i_drawBox;
MObject ocInstance::i_motionBlurEnable;
MObject ocInstance::o_filename;

ocInstance::ocInstance()
{
//	bValidBox = false;
}
ocInstance::~ocInstance()
{
}

MDataBlock ocInstance::getData()
{
	MDataBlock data = ((ocInstance*)(this))->forceCache();
	return data;
}

MStatus ocInstance::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;
	if( plug==o_filename)
	{
		cls::MInstancePxData* cache = (cls::MInstancePxData*)data.inputValue( i_drawCache, &stat ).asPluginData();
		MTime t = data.inputValue( i_time, &stat ).asTime();
		MTime st = data.inputValue( i_startTime, &stat ).asTime();
		t -= st;
		double dt = t.as(MTime::uiUnit());
		int time = (int)(dt+0.5);
		if( time<0) time=0;

		if( !cache) 
			return MS::kSuccess;
		if( cache->dlists.empty()) 
			return MS::kSuccess;

		if( cache->dlists.size()==1)
			time = cache->dlists.begin()->first;

		MDataHandle out = data.outputValue(o_filename, &stat);
		out.set( MString(""));
		data.setClean(o_filename);


		MString file = cache->getfile(time);
		out.set( file);
		return MS::kSuccess;
	}
	return MS::kUnknownParameter;
}

bool ocInstance::isBounded() const
{
	/*/
	MObject thisNode = thisMObject();
	MObject cacheval;
	if( getCache(thisNode, cacheval))
		return true;
	else 
		return false;
	/*/
	return true;
}

MBoundingBox ocInstance::boundingBox() const
{
	MObject thisNode = thisMObject();
	MObject cacheval;
	MDataBlock data = ((ocInstance*)(this))->forceCache();
	cls::MInstancePxData* cache = getCache(data, cacheval);

	MBoundingBox box( MPoint(-0.5, -0.5, -0.5), MPoint(0.5, 0.5, 0.5));
	if( !cache) 
		return box;

	int frame = getFrame(data, thisNode);
	bool bis = cache->isValid(frame);
	if( bis)
	{
		Math::Box3f mbox = cache->getbox( frame);
		copy(box, mbox);
	}
	return box;
}

void ocInstance::draw(
	M3dView &view, 
	const MDagPath &path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status)
{ 
	MDataBlock data = forceCache();
	MObject thisNode = thisMObject();

	bool bDrawBox = false;
	MPlug plug(thisNode, i_drawBox);
	plug.getValue(bDrawBox);
	
	Math::Box3f defbox;
	defbox.min = -Math::Vec3f(0.5f, 0.5f, 0.5f);
	defbox.max = Math::Vec3f(0.5f, 0.5f, 0.5f);

	MObject cacheval;
	cls::MInstancePxData* cache = getCache(data, cacheval);

	view.beginGL(); 

	if( !cache) 
	{
		drawBox(defbox);
	}
	else
	{
		int frame = getFrame(data, thisNode);
//		GLuint redMat = cache->get(frame);
//		GLboolean bis = glIsList(redMat);
		if(cache->isValid(frame))
		{
			cache->UpdateAttrList(frame, thisNode);

			if( bDrawBox)
			{
				Math::Box3f box = cache->getbox(frame);
				drawBox(box);
			}
			cache->draw(frame);
		}
		else
		{
			drawBox(defbox);
		}
	}

	view.endGL();
};

/*/
cls::MInstancePxData* ocInstance::getCache(MObject thisNode, MObject& obj_drawCache) const
{
	MPlug plug(thisNode, i_drawCache);
	plug.getValue(obj_drawCache);
	MFnPluginData pd(obj_drawCache);
	cls::MInstancePxData* cache = (cls::MInstancePxData*)pd.data();
	if( !cache) 
		return NULL;
	if( cache->dlists.empty()) 
		return NULL;
	return cache;
}
int ocInstance::getFrame(MObject thisNode) const
{
	MObject cacheval;
	cls::MInstancePxData* cache = getCache(thisNode, cacheval);
	if( !cache) return 0;

	if( cache->dlists.size()==1)
		return cache->dlists.begin()->first;

	MTime t;
	MPlug plug(thisNode, i_time);
	plug.getValue(t);

	MTime st;
	plug = MPlug(thisNode, i_startTime);
	plug.getValue(st);
	
	t -= st;
	double dt = t.as(MTime::uiUnit());
	int time = (int)(dt+0.5);
	if( time<0) time=0;
	return time;
}
/*/

cls::MInstancePxData* ocInstance::getCache(MDataBlock data, MObject& obj_drawCache) const
{
	MPxData* pxdata = data.inputValue(i_drawCache).asPluginData();
	cls::MInstancePxData* cache = (cls::MInstancePxData*)pxdata;
	if( !cache) 
		return NULL;
	if( cache->dlists.empty()) 
		return NULL;
	return cache;
}
int ocInstance::getFrame(MDataBlock data, MObject thisNode) const
{
	MObject cacheval;
	cls::MInstancePxData* cache = getCache(data, cacheval);
	if( !cache) return 0;

	if( cache->dlists.size()==1)
		return cache->dlists.begin()->first;

	MTime t;
	MPlug plug(thisNode, i_time);
	plug.getValue(t);

	MTime st;
	plug = MPlug(thisNode, i_startTime);
	plug.getValue(st);
	
	t -= st;
	double dt = t.as(MTime::uiUnit());
	int time = (int)(dt+0.5);
	if( time<0) time=0;
	return time;
}

void* ocInstance::creator()
{
	return new ocInstance();
}

MStatus ocInstance::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnUnitAttribute	unitAttr;
	MStatus				stat;

	try
	{
		{
			i_time = unitAttr.create( "time", "t", MFnUnitAttribute::kTime);
			::addAttribute(i_time, atInput|atKeyable);
		}
		{
			i_startTime = unitAttr.create( "startTime", "st", MFnUnitAttribute::kTime);
			::addAttribute(i_startTime, atInput|atKeyable);
		}
		{
			i_drawBox = numAttr.create( "drawBox","db", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_drawBox, atInput|atKeyable);
		}
		{
			i_motionBlurEnable = numAttr.create( "motionBlurEnable","mbe", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_motionBlurEnable, atInput|atKeyable);
		}
		{
			i_matrixArray = typedAttr.create( "matrixArray", "ma", MFnData::kDoubleArray, &stat);
			::addAttribute(i_matrixArray, atReadable|atStorable|atUnCached|atHidden);
		}
		{
			i_drawCache = typedAttr.create( "drawCache", "dch", cls::MInstancePxData::id, MObject::kNullObj, &stat);
			::addAttribute(i_drawCache, atReadable|atUnStorable|atUnCached);
		}
		
		{
			o_filename = typedAttr.create ("filename","fn", MFnData::kString, &stat);
			::addAttribute(o_filename, atOutput);
			stat = attributeAffects( i_time, o_filename);
			stat = attributeAffects( i_startTime, o_filename);
			stat = attributeAffects( i_drawCache, o_filename);

			stat = attributeAffects( i_drawCache, nodeBoundingBox);
		}
		if( !MGlobal::sourceFile("AEocInstanceTemplate.mel"))
		{
			displayString("error source AEocInstanceTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

#endif