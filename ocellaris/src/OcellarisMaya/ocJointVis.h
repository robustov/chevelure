#pragma once

#include "pre.h"
#include <maya/MPxLocatorNode.h>

#include "ocellaris/IGeometryGenerator.h"
#include "ocellaris/OcellarisExport.h"

/*/
struct ocJointVisGenerator : public cls::IGeometryGenerator
{
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "ocJointVisGenerator";};

	bool OnGeometry(
		MObject& node, 
		cls::IRender* render, 
		Math::Box3f& box, 
		cls::IExportContext* context
		);
};
/*/

// ������� "ocsCache"
// ������������ ��������� ocGeometryGenerator

class ocJointVis : public MPxLocatorNode
{
public:
	ocJointVis();
	virtual ~ocJointVis(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	virtual void draw(
		M3dView &view, const MDagPath &path, 
		M3dView::DisplayStyle style,
		M3dView::DisplayStatus status);

	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_nodename;
	static MObject i_time;
	static MObject o_ocsCache;

public:
	static MTypeId id;
	static const MString typeName;
};


