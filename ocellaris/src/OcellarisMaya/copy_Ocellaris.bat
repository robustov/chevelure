set BIN_DIR=.\out\ocellaris
set BIN_DIR=\\server\bin

if "%DSTPATH%"=="" (set BIN_DIR=%BIN_DIR%) else (set BIN_DIR=%DSTPATH%)

rem SN
mkdir %BIN_DIR%
mkdir %BIN_DIR%\bin
mkdir %BIN_DIR%\mel
mkdir %BIN_DIR%\python
mkdir %BIN_DIR%\include

rem copy BinRelease\OcellarisAutoExp.dll "C:\Program Files\Microsoft Visual Studio .NET 2003\Common7\IDE"

cd /D %ULITKABIN%
copy BinRelease\ocellarisExport.dll				%BIN_DIR%\bin
copy BinRelease\ocellarisGelatoDSO.dll				%BIN_DIR%\bin
copy BinRelease\ocellarisMaya.mll					%BIN_DIR%\bin
copy BinRelease\ocellarisOffline.exe				%BIN_DIR%\bin
copy BinRelease\ocellarisPrmanDSO.dll				%BIN_DIR%\bin
copy BinRelease\ocellarisProc.dll					%BIN_DIR%\bin
copy BinRelease\ocellarisShaders.dll				%BIN_DIR%\bin
copy BinRelease\ocellarisRibGen.dll				%BIN_DIR%\bin
copy BinRelease\ocellarisRibGen.slim				%BIN_DIR%\bin
copy BinRelease\ocellarisTest.exe					%BIN_DIR%\bin
copy BinRelease\ocellarisCropAsm.exe				%BIN_DIR%\bin
copy BinRelease\IPrman12.5.dll						%BIN_DIR%\bin\
copy BinRelease\IPrman13.5.dll						%BIN_DIR%\bin\IPrman.dll
copy BinRelease\IPrmanDSO13.5.dll					%BIN_DIR%\bin\IPrmanDSO.dll

copy Mel\AEocRootTransformTemplate.mel		%BIN_DIR%\mel
copy Mel\AEocOcclusionPassTemplate.mel		%BIN_DIR%\mel
copy Mel\AEocShaderParameterTemplate.mel	%BIN_DIR%\mel
copy Mel\AEocFileTemplate.mel				%BIN_DIR%\mel
copy Mel\AEocInstanceTemplate.mel			%BIN_DIR%\mel
copy Mel\ocellarisMayaMenu.Mel				%BIN_DIR%\mel
copy Mel\ocTranslatorOptions.mel			%BIN_DIR%\mel
copy Mel\sloTranslatorOptions.mel			%BIN_DIR%\mel
copy Mel\AEocGeneratorSetTemplate.mel		%BIN_DIR%\mel
copy Mel\AEocJointVisTemplate.mel			%BIN_DIR%\mel
copy Mel\AEocRenderPassTemplate.mel			%BIN_DIR%\mel
copy Mel\AEocLightTemplate.mel				%BIN_DIR%\mel
copy Mel\AEocSequenceFileTemplate.mel		%BIN_DIR%\mel
copy Mel\AEocGeneratorProxyTemplate.mel		%BIN_DIR%\mel

// PHYSX
copy BinRelease\ocellarisPhysXMaya.mll				%BIN_DIR%\bin
copy BinRelease\ocellarisPhysX.dll					%BIN_DIR%\bin
copy BinRelease\ragdoll.exe						%BIN_DIR%\bin
copy Mel\AEocPhysXActorTemplate.mel			%BIN_DIR%\mel
copy Mel\AEocPhysXShapeTemplate.mel			%BIN_DIR%\mel
copy Mel\ocellarisPhysXMayaMenu.mel			%BIN_DIR%\mel
copy Mel\AEocPhysXJointTemplate.mel			%BIN_DIR%\mel

pause