#pragma once

#include "pre.h"
#include <maya/MPxSurfaceShapeUI.h> 
#include "ocInstance2Shape.h"

class ocInstance2UI : public MPxSurfaceShapeUI
{
public:
	ocInstance2UI();
	virtual ~ocInstance2UI(); 

	/////////////////////////////////////////////////////////////////////
	//
	// Overrides
	//
	/////////////////////////////////////////////////////////////////////

	// Puts draw request on the draw queue
	//
	virtual void getDrawRequests( 
		const MDrawInfo & info,
		bool objectAndActiveOnly,
		MDrawRequestQueue & requests 
		);

	// Main draw routine. Gets called by maya with draw requests.
	//
	virtual void draw( 
		const MDrawRequest & request,
		M3dView & view 
		) const;

	// Main selection routine
	//
	virtual bool select( 
		MSelectInfo &selectInfo,
		MSelectionList &selectionList,
		MPointArray &worldSpaceSelectPts 
		) const;

	/////////////////////////////////////////////////////////////////////
	// Helper routines
protected:
	void drawBoundingBox( 
		const MDrawRequest & request,
		M3dView & view 
		) const;
	void drawWireframe( 
		const MDrawRequest & request, 
		M3dView & view ) const;
	void drawShaded( 
		const MDrawRequest & request, 
		M3dView & view ) const;
	void drawVertices( 
		const MDrawRequest & request, 
		M3dView & view ) const;

	bool selectVertices( 
		MSelectInfo &selectInfo,
		MSelectionList &selectionList,
		MPointArray &worldSpaceSelectPts 
		) const;

public:
	static void * creator();

private:
	/*/
	// Draw Tokens
	//
	enum {
		kDrawVertices, // component token
		kDrawWireframe,
		kDrawWireframeOnShaded,
		kDrawSmoothShaded,
		kDrawFlatShaded,
		kLastToken
	};
	/*/
};
