#include "stdafx.h"
#include "ocCluster.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>

#include <maya/MGlobal.h>

MObject ocCluster::i_input;
MObject ocCluster::o_output;

ocCluster::ocCluster()
{
}
ocCluster::~ocCluster()
{
}

MStatus ocCluster::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;

	if( plug == o_output )
	{
		MDataHandle inputData = data.inputValue( i_input, &stat );

		MDataHandle outputData = data.outputValue( plug, &stat );

		data.setClean(plug);
		return MS::kSuccess;
	}
	return MS::kUnknownParameter;
}

void* ocCluster::creator()
{
	return new ocCluster();
}

MStatus ocCluster::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		{
			i_input = numAttr.create ("i_input","iin", MFnNumericData::kDouble, 0.01, &stat);
			numAttr.setMin(0.0);	
			numAttr.setMax(1.0);	
			::addAttribute(i_input, atInput);
		}
		{
			o_output = numAttr.create( "o_output", "out", MFnNumericData::kDouble, 0.01, &stat);
			::addAttribute(o_output, atOutput);
			stat = attributeAffects( i_input, o_output );
		}
		if( !MGlobal::sourceFile("AEocClusterTemplate.mel"))
		{
			displayString("error source AEocClusterTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

