#include "stdafx.h"
#include "ocellaris/OcellarisExport.h"
#include "ocellaris/renderFileImpl.h"
#include "MathNMaya/MathNMaya.h"

#include <maya/MFnFluid.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MItCurveCV.h>
#include <maya/MPlug.h>
#include <maya/MPoint.h>
#include <maya/MPointArray.h>
#include <maya/MGlobal.h>
#include <maya/MDagPath.h>
#include <maya/MMatrix.h>
#include <string>

#include "Util/misc_create_directory.h"


extern "C" {
    __declspec(dllexport) bool ocExportFluid(
		const char* objectname, 
		const char* filename, 
		bool bAscii
		);
}

bool ocExportFluid(
	const char* objectname, 
	const char* filename, 
	bool bAscii
	)
{
	MObject node;
	if( !nodeFromName(objectname, node))
		return false;

	if( node.apiType()!=MFn::kFluid)
	{
		MFnDagNode dag(node);
		MObject obj = dag.parent(0);
		if(obj.isNull()) return false;
		dag.setObject(obj);

		for(int c=0; c<(int)dag.childCount(); c++)
		{
			node = dag.child(c);
			if( node.apiType()==MFn::kFluid)
				goto found;
		}
		return false;
	}
found:
	MFnFluid fluid(node);

	MDagPath path = MDagPath::getAPathTo(node);
	Math::Matrix4f world;
	copy(world, path.inclusiveMatrix());
	MObject ocshading = getExternShaderNode(node);

	std::string cachefile = filename;
	Util::changeSlashToSlash(cachefile);

	cls::renderFileImpl file;
	if( !file.openForWrite(cachefile.c_str(), bAscii))
	{
		fprintf(stderr, "FluidGenerator: file %s can't be created\n", cachefile.c_str());
		return false;
	}
	cls::IRender* filerender = &file;

	cls::PA<unsigned int> res(cls::PI_CONSTANT, 3);
	fluid.getResolution(res[0], res[1], res[2]);
	double x, y, z;
	fluid.getDimensions(x, y, z);
	cls::PA<float> dim(cls::PI_CONSTANT, 3);
	dim[0] = (float)x; dim[1] = (float)y; dim[2] = (float)z;

	filerender->Parameter("resolution", res);
	filerender->Parameter("dimentions", dim);
	filerender->Parameter("world", world);

	int s = res[0]*res[1]*res[2];
	int gs = fluid.gridSize();

	float* densityGrid = fluid.density();
	if( densityGrid)
	{
		float ds = 0;
		cls::PA<float> density(cls::PI_CONSTANT, s);

		for(unsigned int z = 0; z<res[2]; z++)
			for(unsigned int y = 0; y<res[1]; y++)
				for(unsigned int x = 0; x<res[0]; x++)
				{
					int find = fluid.index(x, y, z);
					int myind = x + y*res[0] + z*res[0]*res[1];
					if(find>=0 && find<gs)
					{
						density[myind] = densityGrid[find];
						ds += density[myind];
					}
					else
						density[myind] = 0;
				}

		filerender->Parameter("densitySum", ds);
		filerender->Parameter("density", density);
	}

	float *rGrid;
	float *gGrid;
	float *bGrid;
	fluid.getColors(rGrid, gGrid, bGrid);
	if(rGrid && gGrid && bGrid) 
	{
		cls::PA<Math::Vec3f> color(cls::PI_CONSTANT, s);

		for(unsigned int z = 0; z<res[2]; z++)
			for(unsigned int y = 0; y<res[1]; y++)
				for(unsigned int x = 0; x<res[0]; x++)
				{
					int find = fluid.index(x, y, z);
					int myind = x + y*res[0] + z*res[0]*res[1];
					if(find>=0 && find<gs)
						color[myind] = Math::Vec3f(rGrid[find], gGrid[find], bGrid[find]);
					else
						color[myind] = Math::Vec3f(0, 0, 0);
				
				}
		filerender->Parameter("color", color);
	}


//	render->Parameter("fluidCache", cache);
//	render->Call("renderfluid");
	Math::Box3f box;
	box.min = Math::Vec3f(-dim[0]/2, -dim[1]/2, -dim[2]/2);
	box.max = Math::Vec3f( dim[0]/2,  dim[1]/2,  dim[2]/2);

	return true;
}
