#include "stdafx.h"
//
// Copyright (C) 
// File: ocExportAnimationCmd.cpp
// MEL Command: ocExportAnimation

#include "ocExportAnimation.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MAnimControl.h>
#include <maya/MFileIO.h>
#include <time.h>

#include "Util/misc_create_directory.h"
#include "Util/currentHostName.h"

#include "ocellaris/OcellarisExport.h"
#include "ocellaris\renderFileImpl.h"
#include "ocellaris\renderCacheImpl.h"
#include "ocellaris\ChiefGenerator.h"
#include "ocellaris\CacheStorageMulty.h"
#include "../ocellarisExport/ExportContextImpl.h"
#include "ocellaris\CacheStorageBuildAnimation.h"

#include "mathNmaya/mathNmaya.h"
#include "MathNMaya/MayaProgress.h"

const MString ocExportAnimation::typeName( "ocExportAnimation" );

// ocExportAnimation pCube -o "d:/temp/cube/" -dt 1 -a 1
// ocExportAnimation pCube -o "d:/temp/cube/" -dt 1 -a 0

// ocExportAnimation particle_for_trush -o "d:/temp/particle_for_trush/%04d.ocs" -dt 1 -a 0
// ocExportAnimation floor_01 -o "d:/temp/floor_01/%04d.ocs" -dt 1 -a 0
// ocExportAnimation pTorus1 -o "m:/temp/xxx/%%04d.ocs" -a 1 -alf 1


ocExportAnimation::ocExportAnimation()
{
}
ocExportAnimation::~ocExportAnimation()
{
}

MSyntax ocExportAnimation::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.addArg(MSyntax::kString);						// arg0
	syntax.addFlag("alf", "alfred", MSyntax::kBoolean);		// ����� �������
	syntax.addFlag("o", "output", MSyntax::kString);		// ���� �� ������
	syntax.addFlag("dt", "deltaTime", MSyntax::kDouble);	// dt
	syntax.addFlag("a", "ascii", MSyntax::kBoolean);		// ascii
	syntax.addFlag("sf", "singleFile", MSyntax::kBoolean);	// singleFile
	syntax.addFlag("du", "dump", MSyntax::kBoolean);		// dump
	syntax.addFlag("rt", "roottransform", MSyntax::kString);		// roottransform
	syntax.addFlag("st", "startTime", MSyntax::kTime);
	syntax.addFlag("et", "endTime", MSyntax::kTime);
	syntax.addFlag("rtg", "roottransformgenerator", MSyntax::kString);	// roottransform
	syntax.addFlag("msh", "mayashaders", MSyntax::kBoolean);	// mayashaders
	return syntax;
}

MStatus ocExportAnimation::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MString argsl0;
	argData.getCommandArgument(0, argsl0);

	MString filename;
	if( !argData.getFlagArgument("o", 0, filename))
		filename = "";

	double deltaTime;
	if( !argData.getFlagArgument("deltaTime", 0, deltaTime))
		deltaTime = 1;

	bool bAscii;
	if( !argData.getFlagArgument("ascii", 0, bAscii))
		bAscii = 1;

	bool bAlfred = false;
	if( !argData.getFlagArgument("alf", 0, bAlfred))
		bAlfred = false;

	bool bSingleFile = false;
	if( !argData.getFlagArgument("singleFile", 0, bSingleFile))
		bSingleFile = false;

	bool bMayaShaders = false;
	if( !argData.getFlagArgument("msh", 0, bMayaShaders))
		bMayaShaders = false;

	bool bDump = false;
	if( !argData.getFlagArgument("du", 0, bDump))
		bDump = false;

	MString roottransform;
	if( !argData.getFlagArgument("rt", 0, roottransform))
		roottransform = "";

	MString roottransformgen;
	if( !argData.getFlagArgument("rtg", 0, roottransformgen))
		roottransformgen = "";

	MTime dt(deltaTime, MTime::uiUnit());
	MTime minT = MAnimControl::minTime();
	MTime maxT = MAnimControl::maxTime();

	MTime t;
	if( argData.getFlagArgument("startTime", 0, t))
		minT = t;
	if( argData.getFlagArgument("endTime", 0, t))
		maxT = t;

	bool bPaused = false;
	if( bAlfred)
	{
		return MS::kFailure;
	}

	MObject sourceobj;
	if( !nodeFromName(argsl0, sourceobj))
		return MS::kFailure;

	MObject roottransformobj;
	nodeFromName(roottransform, roottransformobj);

	MDagPath roottransformdag;
	if( !roottransformobj.isNull())
		stat = MDagPath::getAPathTo( roottransformobj, roottransformdag );

	MDagPath dag;
	stat = MDagPath::getAPathTo( sourceobj, dag );

	Math::Box3f box;
	MFnDependencyNode dn(sourceobj);

	std::string projectDir;
	{
		MString workspacedir;
		MGlobal::executeCommand("workspace -fn", workspacedir);
		projectDir = workspacedir.asChar();
	}
	// folder
	std::string folder = Util::extract_foldername(filename.asChar());
	Util::create_directory(folder.c_str());

	bool bMotionVelocity = false;

	ExportContextImpl context;
	context.setEnvironment( "WORKSPACEDIR", projectDir.c_str());
	context.setEnvironment( "OUTPUTDIR", folder.c_str());
	context.setEnvironment( "SHADERDIR", folder.c_str());
	context.setEnvironment( "OCSDIR", folder.c_str());
	context.setEnvironment( "RIBDIR", folder.c_str());

	context.setEnvironment("OUTPUTFILENAME", filename.asChar());
	context.setEnvironment("OUTPUTFORMAT", (bAscii)?("ascii"):("binary"));
	context.setEnvironment("SRCFILENAME", MFileIO::currentFile().asChar());
	context.setEnvironment("SRCHOSTNAME", Util::currentHostName().c_str());

	/////////////////////////////////////////
	// ���������� !!!
	context.addDefaultGenerators("");
	if( bMayaShaders)
		context.addGeneratorForType("shader", "MayaShader");

	// roottransformgen
	// ���� ����� �������� �������!!!
	if( roottransformgen.length())
		context.addGeneratorForType("roottransform", roottransformgen.asChar());

	if(bMotionVelocity)
		context.addGeneratorForType("mesh", "VertexVelocityAttribute");

	cls::IGenerator* pass = NULL;
	if(!bSingleFile)
	{
		pass = context.loadGenerator("SimpleExportPass", cls::IGenerator::PASS);
	}
	else
	{
		pass = context.loadGenerator("AnimationExportPass", cls::IGenerator::PASS);
	}
	context.prepareGenerators();


	if( sourceobj.hasFn(MFn::kSet))
	{
		MFnSet set(sourceobj);
		MSelectionList sl;
		set.getMembers(sl, true);
		for( int i=0; i<(int)sl.length(); i++)
		{
			MDagPath dag;
			if( !sl.getDagPath(i, dag)) continue;
			context.addnodes(dag, "");
		}
	}
	else
	{
		context.addnodes(dag, "");
	}
	if( bDump) context.dump("-nodes");
	if( bDump) return MS::kSuccess;


//	MayaExportContextImpl context("exportAnimation");
//	context.InitChiefGenerator(OCGT_SCENE);

	MayaProgress mp("Ocellaris", "Export animation");

	if(!bSingleFile)
	{
		std::string outfilenameform = filename.asChar();
		outfilenameform += std::string("//%04d.ocs");
		context.setEnvironment("OUTPUTFILENAME", outfilenameform.c_str());

		std::string passoptions = "Attribute uniform string filename=\""+outfilenameform+"\";\n";

		context.addpass( (cls::IPassGenerator*)pass, passoptions.c_str());

			/*/
				MMatrix m = dag.exclusiveMatrix();
				Math::Matrix4f transform;
				::copy(transform, m);
				transform = transform*rootmatrix_inv;

				cache->PushTransform();
				cache->PushAttributes();
				((cls::IRender*)cache)->AppendTransform(transform);
				cache->PopAttributes();
				pNode->Render(cache, box, transform, &context);
	//			ocExportTree(sourceobj, chief, cache, box, transform, &context);
				cache->PopTransform();
			/*/

	}
	else
	{
		std::string outfilenameform = filename.asChar();
		std::string passoptions = "Attribute uniform string filename=\""+outfilenameform+"\";\n";

		context.addpass( (cls::IPassGenerator*)pass, passoptions.c_str());


		/*/
		MayaProgress mp("Ocellaris", "Exsport");
		cls::renderCacheImpl<cls::CacheStorageBuildAnimation> cache;

		Math::Box3f box;
		std::vector<float> phases;

		MTime t = minT;
		for( int i=0; t<=maxT; t+=dt, i++)
		{
			cache.storage.setKey(i);
			MAnimControl::setCurrentTime(t);
			double perc = (t-minT).as(MTime::kSeconds)/(maxT-minT).as(MTime::kSeconds);
			if( !mp.OnProgress( (float)perc))
				return MS::kSuccess;
			int time = i;

			Math::Matrix4f rootmatrix_inv = Math::Matrix4f::id;
			if( !roottransformobj.isNull())
			{
				MMatrix m = roottransformdag.inclusiveMatrixInverse();
				::copy( rootmatrix_inv, m);
			}

			cls::IRender* render = ((cls::IRender*)&cache);
			for(int i=0; i<(int)nodes.size(); i++)
			{
				cls::INode* pNode = nodes[i];
				MDagPath dag = pNode->getPath();

				MMatrix m = dag.exclusiveMatrix();
				Math::Matrix4f transform;
				::copy(transform, m);
				transform = transform*rootmatrix_inv;

				render->PushTransform();
				render->PushAttributes();
				render->AppendTransform(transform);
				render->PopAttributes();
				Math::Box3f framebox;
				pNode->Render(&cache, framebox, transform, &context);
				box.insert(framebox);
				render->PopTransform();
			}

			SlimShaderGenerator::bClearShaderWhenStart = false;
			SlimShaderGenerator::bDontUseMtor = true;

			phases.push_back( (float)(t-minT).as(MTime::kSeconds));
		}

		cache.storage.Parse();
		{
			std::string outfilenameform = filename.asChar();
//			outfilenameform += std::string("//animation.ocs");
			cls::renderFileImpl _render;
			Util::create_directory_for_file(outfilenameform.c_str());
			displayString("{%f, %f, %f}-{%f, %f, %f}", box.min.x, box.min.y, box.min.z, box.max.x, box.max.y, box.max.z);
			if( _render.openForWrite(outfilenameform.c_str(), bAscii))
			{
				cls::IRender* r = &_render;
				r->Attribute("file::box", box);
				r->Attribute("animation::phase_max", phases.back());
//				r->Attribute("animation::phase_maxnc", phases[phases.size()-2]);
				r->Attribute("animation::phase_min", phases.front());
				r->Attribute("global::phase", 0.f);
				r->Attribute("global::cycling", (bool)true);
				r->PushAttributes();
					r->Attribute("animation::phases", cls::PA<float>(cls::PI_CONSTANT, phases) );
					cache.Render(&_render);
				r->PopAttributes();
			}
		}
		/*/
	}

	{
		MTime t = minT;
		for( int i=0; t<=maxT; t+=dt, i++)
		{
			context.addframe( (float)t.as(MTime::uiUnit()));
		}
	}

	context.preparenodes();

	context.doexport("", "", &mp);

	return MS::kSuccess;
}

void* ocExportAnimation::creator()
{
	return new ocExportAnimation();
}

