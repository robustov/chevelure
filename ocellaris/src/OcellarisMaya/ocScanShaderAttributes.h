#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ocScanShaderAttributesCmd.h
// MEL Command: ocScanShaderAttributes

#include <maya/MPxCommand.h>

class ocScanShaderAttributes : public MPxCommand
{
public:
	static const MString typeName;
	ocScanShaderAttributes();
	virtual	~ocScanShaderAttributes();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

