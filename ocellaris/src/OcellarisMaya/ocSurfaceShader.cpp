#include "stdafx.h"
#ifdef WIN32
#pragma warning( disable : 4786 )		// Disable STL warnings.
#endif

#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MString.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFloatVector.h>
#include <maya/MFnStringData.h>
//#include <maya/MFnPlugin.h>
#include <maya/MSceneMessage.h>

#include <maya/MHWShaderSwatchGenerator.h>
#include <maya/MHardwareRenderer.h>
#include <maya/MGeometryData.h>
#include <maya/MImage.h>

#include <GL/gl.h>
#include <GL/glu.h>

#include "ocSurfaceShader.h"
#include "shading/ShadingGraphAssembler.h"
#include "Util/misc_create_directory.h"
//#include "ocSurfaceShaderBehavior.h"

MObject ocSurfaceShader::i_SLfilename;
MObject ocSurfaceShader::i_templateName;
MObject ocSurfaceShader::i_shaderAttrsList;
MObject ocSurfaceShader::i_ignoreLastShader;
MObject ocSurfaceShader::o_shaderFile;

MObject ocSurfaceShader::i_displacementBound;
MObject ocSurfaceShader::i_displacementSpace;
MObject ocSurfaceShader::o_displacement;

ShadingGraphNodeImpl* getGraphNode(MObject node)
{
	MFnDependencyNode dn(node);
	if( dn.typeId() != ocSurfaceShader::id) return NULL;

	ocSurfaceShader* attachShader = (ocSurfaceShader*)dn.userNode();
	return &attachShader->graphNode;
}


ocSurfaceShader::ocSurfaceShader()
{

	// �� ���
	attachSceneCallbacks();

	mAmbientColor[0] = mAmbientColor[1] = mAmbientColor[2] = 0.1f;
	mDiffuseColor[0] = mDiffuseColor[1] = mDiffuseColor[2] = 0.5f;
	mSpecularColor[0] = mSpecularColor[1] = mSpecularColor[2] = 0.5f;
	mShininess[0] = mShininess[1] = mShininess[2] = 100.0f;
	mAttributesChanged = false;

	phong_map_id = 0;
	mGeometryShape = 0;
}

ocSurfaceShader::~ocSurfaceShader()
{
	// �� ���
	detachSceneCallbacks();
}

void ocSurfaceShader::postConstructor( )
{
	setMPSafe(false);
}

void * ocSurfaceShader::creator()
{
    return new ocSurfaceShader();
}

MStatus ocSurfaceShader::computeShaderFile( const MPlug& plug, MDataBlock& data)
{
	if( !slNode.isValid())
	{
		graphNode.InitParameters(thisMObject());
		std::string name = MFnDependencyNode(thisMObject()).name().asChar();

		std::string text = ShadingGraphAssembler::BuildGraph(NULL, graphNode, name.c_str());

		MString _workspacedir;
		MGlobal::executeCommand("workspace -q -rd", _workspacedir);
		std::string workspacedir = _workspacedir.asChar();
		std::string outfilename = workspacedir + "ocs/" + name;
		outfilename += ".sl";
		Util::create_directory_for_file(outfilename.c_str());

		displayStringD(
			"computeShaderFile for %s", 
			MFnDependencyNode(thisMObject()).name().asChar());
		{
			FILE* file = fopen( outfilename.c_str(), "wt");
			if(!file)
			{
				printf("cant open file %s", outfilename.c_str());
				return MS::kFailure;
			}
			fputs(text.c_str(), file);
			fclose(file);
		}

		ShadingGraphAssembler::Compile(NULL, outfilename.c_str());
		Util::changeSlashToSlash(outfilename);

		// �������� ".slo" � �����
		size_t x = outfilename.rfind('.');
		if(x!=std::string::npos)
			outfilename = outfilename.erase(x);
		data.outputValue(plug).set( MString(outfilename.c_str()));
	}
	else
	{
		slNode.InitParameters(thisMObject());

		std::string outfilename = slNode.getName();
//		outfilename = ShadingGraphAssembler::Compile(outfilename.c_str());
		Util::changeSlashToSlash(outfilename);
		size_t x = outfilename.rfind('.');
		if(x!=std::string::npos)
			outfilename = outfilename.erase(x);
		data.outputValue(plug).set( MString(outfilename.c_str()));
	}

	
	return MS::kSuccess;
}

int ocSurfaceShader::getParameterCount()
{
	if( !slNode.isValid())
	{
		return 0;
	}
	else
	{
		return slNode.getParameterCount();
	}
}
bool ocSurfaceShader::getParameter(int i, std::string& name, cls::Param& val)
{
	if( !slNode.isValid())
	{
		return false;
	}
	else
	{
		return slNode.getParameter(thisMObject(), i, name, val);
	}
}

MStatus ocSurfaceShader::setDependentsDirty( const MPlug& plug, MPlugArray& pa)
{
	pa.append( MPlug(thisMObject(), o_shaderFile));
	return MS::kSuccess;
}

bool ocSurfaceShader::getSlFilenameName( const MPlug& plug, MDataHandle& handle, MDGContext& )
{
	std::string name = slNode.getName();
	handle.set( MString(name.c_str()));
	return true;
}

bool ocSurfaceShader::getShaderName( const MPlug& plug, MDataHandle& handle, MDGContext& )
{
	std::string name = graphNode.getName();
	handle.set( MString(name.c_str()));
	return true;
}

bool ocSurfaceShader::getShaderAttrsList( const MPlug& plug, MDataHandle& handle, MDGContext& )
{
	std::vector<std::string> list = graphNode.getAttributeList();
	bool bUseSl = !slNode.getName().empty();
	if( bUseSl)
	{
		slNode.InitParameters(thisMObject());
		list = slNode.getAttributeList();
	}
	else
	{
		graphNode.InitParameters(thisMObject());
		list = graphNode.getAttributeList();
	}

	MStringArray attrList;
	attrList.setLength(list.size());
	for(int i=0; i<(int)list.size(); i++)
	{
		attrList[i] = list[i].c_str();
	}

	MFnStringArrayData saData;
	handle.set(saData.create(attrList));
	return true;
}

bool ocSurfaceShader::setSlFilenameName( const MPlug& plug, const MDataHandle& handle, MDGContext& )
{
	MString str = handle.asString();
	slNode.Init(str.asChar());
	std::string cmd = "updateAE ";
	cmd += MFnDependencyNode(thisMObject()).name().asChar();
	MGlobal::executeCommand(cmd.c_str());
	
	return true;
}
bool ocSurfaceShader::setShaderName( const MPlug& plug, const MDataHandle& handle, MDGContext& )
{
	MString str = handle.asString();
	graphNode.Init(str.asChar());
	std::string cmd = "updateAE ";
	cmd += MFnDependencyNode(thisMObject()).name().asChar();
	MGlobal::executeCommand(cmd.c_str());
	
	return true;
}

bool ocSurfaceShader::setShaderAttrsList( const MPlug& plug, const MDataHandle& handle, MDGContext& )
{
	return true;
}


void ocSurfaceShader::BuildShader()
{
	if( !graphNode.isValid())
		return;
//	ShaderAssembler assembler;
//	assembler.root = &graphNode;
//	std::string shader = assembler.Build();



	/*/
	// ����� i_templateName
	std::string buf = procname;
	char *token = strtok( (char*)buf.c_str(), "@");
	Util::DllProcedure dllproc;
	if( token != NULL)
	{
		dllproc.dll = token;
		token = strtok( NULL, "@" );
		if( token != NULL)
			dllproc.entrypoint = token;
		else
		{
			dllproc.entrypoint = dllproc.dll;
			dllproc.dll = "ocellarisShaders";
		}
	}
	dllproc.Load();
	if( !dllproc.isValid())
		return NULL;

	PROCEDURAL_CREATOR pc = (PROCEDURAL_CREATOR)dllproc.proc;
	IShader* shader = (*pc)(this);

	if( !proc)
		return NULL;

	proclist[procname].dll = dllproc;
	proclist[procname].proc = proc;


	// ��� ���� ������� ������
	shader->Build

	cls::ShadingNodeImpl shader;
	shader.Parameter("OC")

	Build()
	/*/
}

/*/
void ocSurfaceShader::InitGraphNode(const char* name)
{
	graphNode.Init(name);
	if( graphNode.isValid())
		graphNode.InitParameters(thisMObject());
}
/*/

MStatus ocSurfaceShader::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnEnumAttribute	enumAttr;
	MFnStringData stringData;
	MStatus				stat;

	try
	{
		{
			i_SLfilename = typedAttr.create( "SLfilename", "SLfilename",
				MFnData::kString, stringData.create("", &stat), &stat );
			typedAttr.setInternal(true);
			::addAttribute(i_SLfilename, atInput);
		}
		{
			i_templateName = typedAttr.create( "templateName", "templateName",
				MFnData::kString, stringData.create("Constant", &stat), &stat );
			typedAttr.setInternal(true);
			::addAttribute(i_templateName, atInput);
		}
		{
			i_shaderAttrsList = typedAttr.create( "shaderAttrsList", "shaderAttrsList", MFnData::kStringArray, &stat);
			typedAttr.setInternal(true);
			::addAttribute(i_shaderAttrsList, atInput|atHidden);
		}
		
		{
			i_ignoreLastShader = numAttr.create( "ignoreLastShader", "ils", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_ignoreLastShader, atInput);
		}
		{
			o_shaderFile = typedAttr.create( "shaderFile", "shaderFile", MFnData::kString, &stat);
			typedAttr.setInternal(true);
			::addAttribute(o_shaderFile, atOutput|atHidden|atUnCached);

			{
				MObject isAffected = o_shaderFile;
				attributeAffects(i_SLfilename, isAffected);
				attributeAffects(i_templateName, isAffected);
				attributeAffects(i_shaderAttrsList, isAffected);
			}
		}


		{
			i_displacementBound = numAttr.create( "displacementBound", "dispBo", MFnNumericData::kDouble, 0.1);
			numAttr.setMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_displacementBound, atInput);

			i_displacementSpace = typedAttr.create( "displacementSpace", "dispSp",
				MFnData::kString, stringData.create("shader", &stat), &stat );
			::addAttribute(i_displacementSpace, atInput);

			o_displacement = numAttr.create( "displacement", "d", MFnNumericData::kFloat, 0);
			numAttr.setInternal( true );
			::addAttribute(o_displacement, atOutput);
		}

		if( !MGlobal::sourceFile("AEocSurfaceShaderTemplate.mel"))
		{
			displayString("error source AEocSurfaceShaderTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}

	{
		// Make sure that all attributes are cached internal for
		// optimal performance !

		MFnNumericAttribute nAttr; 

	
		// Create input attributes
		aColor = nAttr.createColor( "color", "c");
		nAttr.setStorable(true);
		nAttr.setKeyable(true);
		nAttr.setDefault(0.1f, 0.1f, 0.1f);
		nAttr.setCached( true );
		nAttr.setInternal( true );

		aDiffuseColor = nAttr.createColor( "diffuseColor", "dc" );
		nAttr.setStorable(true);
		nAttr.setKeyable(true);
		nAttr.setDefault(1.f, 0.5f, 0.5f);
		nAttr.setCached( true );
		nAttr.setInternal( true );

		aSpecularColor = nAttr.createColor( "specularColor", "sc" );
		nAttr.setStorable(true);
		nAttr.setKeyable(true);
		nAttr.setDefault(0.5f, 0.5f, 0.5f);
		nAttr.setCached( true );
		nAttr.setInternal( true );

		// This is defined as a point, so that users can easily enter
		// values beyond 1.
		aShininess = nAttr.createPoint( "shininess", "sh" );
		nAttr.setStorable(true);
		nAttr.setKeyable(true);
		nAttr.setDefault(100.0f, 100.0f, 100.0f);
		nAttr.setCached( true );
		nAttr.setInternal( true );

		aGeometryShape = nAttr.create( "geometryShape", "gs", MFnNumericData::kInt );
		nAttr.setStorable(true);
		nAttr.setKeyable(true);
		nAttr.setDefault(0);
		nAttr.setCached( true );
		nAttr.setInternal( true );


		// create output attributes here
		// outColor is the only output attribute and it is inherited
		// so we do not need to create or add it.
		//

		// Add the attributes here

		addAttribute(o_displacement);
		addAttribute(aColor);
		addAttribute(aDiffuseColor);
		addAttribute(aSpecularColor);
		addAttribute(aShininess);
		addAttribute(aGeometryShape);

		attributeAffects (aColor,			outColor);
		attributeAffects (aDiffuseColor,	outColor);
		attributeAffects (aSpecularColor,	outColor);
		attributeAffects (aShininess,		outColor);
	}

    return MS::kSuccess;
}

















//////////////////////////////////////////////////
//
// ��� �� ���:

#undef ENABLE_TRACE_API_CALLS
#ifdef ENABLE_TRACE_API_CALLS
#define TRACE_API_CALLS(x) cerr << "ocSurfaceShader: "<<(x)<<"\n"
#else
#define TRACE_API_CALLS(x) 
#endif

#ifndef GL_EXT_texture_cube_map
# define GL_NORMAL_MAP_EXT                   0x8511
# define GL_REFLECTION_MAP_EXT               0x8512
# define GL_TEXTURE_CUBE_MAP_EXT             0x8513
# define GL_TEXTURE_BINDING_CUBE_MAP_EXT     0x8514
# define GL_TEXTURE_CUBE_MAP_POSITIVE_X_EXT  0x8515
# define GL_TEXTURE_CUBE_MAP_NEGATIVE_X_EXT  0x8516
# define GL_TEXTURE_CUBE_MAP_POSITIVE_Y_EXT  0x8517
# define GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_EXT  0x8518
# define GL_TEXTURE_CUBE_MAP_POSITIVE_Z_EXT  0x8519
# define GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_EXT  0x851A
# define GL_PROXY_TEXTURE_CUBE_MAP_EXT       0x851B
# define GL_MAX_CUBE_MAP_TEXTURE_SIZE_EXT    0x851C
#endif


MObject  ocSurfaceShader::aColor;
MObject  ocSurfaceShader::aDiffuseColor;
MObject  ocSurfaceShader::aSpecularColor;
MObject  ocSurfaceShader::aShininess;
MObject  ocSurfaceShader::aGeometryShape;

//
// DESCRIPTION:
///////////////////////////////////////////////////////

void ocSurfaceShader::printGlError( const char *call )
{
    GLenum error;

	while( (error = glGetError()) != GL_NO_ERROR ) {
	    cerr << call << ":" << error << " is " << (const char *)gluErrorString( error ) << "\n";
	}
}


MFloatVector ocSurfaceShader::Phong ( double cos_a )
{
	MFloatVector p;

	if ( cos_a < 0.0 ) cos_a = 0.0;

	p[0] = (float)(mSpecularColor[0]*pow(cos_a,double(mShininess[0])) +
				   mDiffuseColor[0]*cos_a + mAmbientColor[0]);
	p[1] = (float)(mSpecularColor[1]*pow(cos_a,double(mShininess[1])) + 
				   mDiffuseColor[1]*cos_a + mAmbientColor[1]);
	p[2] = (float)(mSpecularColor[2]*pow(cos_a,double(mShininess[2])) + 
				   mDiffuseColor[2]*cos_a + mAmbientColor[2]);

	if ( p[0] > 1.0f ) p[0] = 1.0f;
	if ( p[1] > 1.0f ) p[1] = 1.0f;
	if ( p[2] > 1.0f ) p[2] = 1.0f;

	return p;
}

#define PHONG_TEXTURE_RES 256

static const GLenum faceTarget[6] =
{
	GL_TEXTURE_CUBE_MAP_POSITIVE_X_EXT,
	GL_TEXTURE_CUBE_MAP_NEGATIVE_X_EXT,
	GL_TEXTURE_CUBE_MAP_POSITIVE_Y_EXT,
	GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_EXT,
	GL_TEXTURE_CUBE_MAP_POSITIVE_Z_EXT,
	GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_EXT
};

static void cubeToDir ( int face, double s, double t,
					  double &x, double &y, double &z )
//
// Description:
//		Map uv to cube direction
{
	switch ( face )
	{
		case 0:
			x = 1;
			y = -t;
			z = -s;
			break;
		case 1:
			x = -1;
			y = -t;
			z = s;
			break;
		case 2:
			x = s;
			y = 1;
			z = t;
			break;
		case 3:
			x = s;
			y = -1;
			z = -t;
			break;
		case 4:
			x = s;
			y = -t;
			z = 1;
			break;
		case 5:
			x = -s;
			y = -t;
			z = -1;
			break;
	}

	double invLen = 1.0 / sqrt ( x*x + y*y + z*z );
	x *= invLen;
	y *= invLen;
	z *= invLen;
}


void ocSurfaceShader::init_Phong_texture ( void )
//
// Description:
//		Set up a cube map for Phong lookup
//
{
	GLubyte * texture_data;

	// Always release the old texture id before getting a
	// new one.
	if (phong_map_id != 0)
		glDeleteTextures( 1, &phong_map_id );
	glGenTextures ( 1, &phong_map_id );

	glEnable ( GL_TEXTURE_CUBE_MAP_EXT );
	glPixelStorei ( GL_UNPACK_ALIGNMENT, 1 );
	glPixelStorei ( GL_UNPACK_ROW_LENGTH, 0 );
	glBindTexture ( GL_TEXTURE_CUBE_MAP_EXT, phong_map_id );

	texture_data = new GLubyte[3*PHONG_TEXTURE_RES*PHONG_TEXTURE_RES];

	for ( int face=0 ; face<6 ; face++ )
	{	
		int index = 0;
		for ( int j=0 ; j<PHONG_TEXTURE_RES ; j++ )
		{
			double t = 2*double(j)/(PHONG_TEXTURE_RES - 1) - 1;	// -1 to 1
			for ( int i=0 ; i<PHONG_TEXTURE_RES ; i++ )
			{
				double s = 2*double(i)/(PHONG_TEXTURE_RES - 1) - 1;	// -1 to 1
				double x, y, z;
				cubeToDir ( face, s, t, x, y, z );

				MFloatVector intensity = Phong ( z );

				texture_data[index++] = (int)(255*intensity[0]);
				texture_data[index++] = (int)(255*intensity[1]);
				texture_data[index++] = (int)(255*intensity[2]);
			}
		}

		glTexImage2D ( faceTarget[face], 0, GL_RGB, PHONG_TEXTURE_RES, PHONG_TEXTURE_RES,
			0, GL_RGB, GL_UNSIGNED_BYTE, texture_data );

	}

	glDisable ( GL_TEXTURE_CUBE_MAP_EXT );

	delete [] texture_data;

	// Make sure to mark attributes "clean".
	mAttributesChanged = false;
}



void ocSurfaceShader::releaseEverything()
{
    if (phong_map_id != 0) {
		M3dView view = M3dView::active3dView();

        // The M3dView class doesn't return the correct status if there isn't
        // an active 3D view, so we rely on the success of beginGL() which
        // will make the context current.
        //
        if (view.beginGL()) {
		    glDeleteTextures( 1, &phong_map_id );
        
            phong_map_id = 0;
        }

        view.endGL();
    }
}

void ocSurfaceShader::attachSceneCallbacks()
{
	fBeforeNewCB  = MSceneMessage::addCallback(MSceneMessage::kBeforeNew,  releaseCallback, this);
	fBeforeOpenCB = MSceneMessage::addCallback(MSceneMessage::kBeforeOpen, releaseCallback, this);
	fBeforeRemoveReferenceCB = MSceneMessage::addCallback(MSceneMessage::kBeforeRemoveReference, 
														  releaseCallback, this);
	fMayaExitingCB = MSceneMessage::addCallback(MSceneMessage::kMayaExiting, releaseCallback, this);
}

/*static*/
void ocSurfaceShader::releaseCallback(void* clientData)
{
	ocSurfaceShader *pThis = (ocSurfaceShader*) clientData;
	pThis->releaseEverything();
}

void ocSurfaceShader::detachSceneCallbacks()
{
	if (fBeforeNewCB)
		MMessage::removeCallback(fBeforeNewCB);
	if (fBeforeOpenCB)
		MMessage::removeCallback(fBeforeOpenCB);
	if (fBeforeRemoveReferenceCB)
		MMessage::removeCallback(fBeforeRemoveReferenceCB);
	if (fMayaExitingCB)
		MMessage::removeCallback(fMayaExitingCB);

	fBeforeNewCB = 0;
	fBeforeOpenCB = 0;
	fBeforeRemoveReferenceCB = 0;
	fMayaExitingCB = 0;
}

/*/
MStatus initializePlugin( MObject obj )
{
	TRACE_API_CALLS("initializePlugin");
	MStatus   status;
	
	const MString& swatchName =	MHWShaderSwatchGenerator::initialize();
	const MString UserClassify( "shader/surface/utility/:swatch/"+swatchName );

	MFnPlugin plugin( obj, "Alias", "4.5", "Any");
	status = plugin.registerNode( "ocSurfaceShader", ocSurfaceShader::id, 
			                      ocSurfaceShader::creator, ocSurfaceShader::initialize,
								  MPxNode::kHwShaderNode, &UserClassify );
	if (!status) {
		status.perror("registerNode");
		return status;
	}

	plugin.registerDragAndDropBehavior("ocSurfaceShaderBehavior", 
									   ocSurfaceShaderBehavior::creator);

	return MS::kSuccess;
}

MStatus uninitializePlugin( MObject obj )
{
	TRACE_API_CALLS("uninitializePlugin");
	MStatus   status;
	
	MFnPlugin plugin( obj );

	// Unregister all chamelion shader nodes
	plugin.deregisterNode( ocSurfaceShader::id );
	if (!status) {
		status.perror("deregisterNode");
		return status;
	}

	plugin.deregisterDragAndDropBehavior("ocSurfaceShaderBehavior");

	return MS::kSuccess;
}
/*/



// DESCRIPTION:
//
MStatus ocSurfaceShader::compute(
	const MPlug& plug,
    MDataBlock& block 
	) 
{ 
	if( plug==o_shaderFile)
	{
		return computeShaderFile(plug, block);
	}

    if ((plug != outColor) && (plug.parent() != outColor))
		return MS::kUnknownParameter;

	MFloatVector & color  = block.inputValue( aDiffuseColor ).asFloatVector();

    // set output color attribute
    MDataHandle outColorHandle = block.outputValue( outColor );
    MFloatVector& outColor = outColorHandle.asFloatVector();
	outColor = color;

    outColorHandle.setClean();
    return MS::kSuccess;
}

/* virtual */
bool ocSurfaceShader::setInternalValueInContext( const MPlug &plug, 
											   const MDataHandle &handle,
											   MDGContext & ctx)
{
	bool handledAttribute = false;

	if (plug == i_SLfilename)
	{
		return setSlFilenameName( plug, handle, ctx);
	}
	if (plug == i_templateName)
	{
		return setShaderName( plug, handle, ctx);
	}
	if (plug == i_shaderAttrsList)
	{
		return setShaderAttrsList( plug, handle, ctx);
	}

	if (plug == aColor)
	{
		handledAttribute = true;
		float3 & val = handle.asFloat3();
		if (val[0] != mAmbientColor[0] || 
			val[1] != mAmbientColor[1] || 
			val[2] != mAmbientColor[2])
		{
			mAmbientColor[0] = val[0];
			mAmbientColor[1] = val[1];
			mAmbientColor[2] = val[2];
			mAttributesChanged = true;
		}
	}
	else if (plug == aDiffuseColor)
	{
		handledAttribute = true;
		float3 & val = handle.asFloat3();
		if (val[0] != mDiffuseColor[0] || 
			val[1] != mDiffuseColor[1] || 
			val[2] != mDiffuseColor[2])
		{
			mDiffuseColor[0] = val[0];
			mDiffuseColor[1] = val[1];
			mDiffuseColor[2] = val[2];
			mAttributesChanged = true;
		}
	}
	else if (plug == aSpecularColor)
	{
		handledAttribute = true;
		float3 & val = handle.asFloat3();
		if (val[0] != mSpecularColor[0] || 
			val[1] != mSpecularColor[1] || 
			val[2] != mSpecularColor[2])
		{
			mSpecularColor[0] = val[0];
			mSpecularColor[1] = val[1];
			mSpecularColor[2] = val[2];
			mAttributesChanged = true;
		}
	}
	else if (plug == aShininess)
	{
		handledAttribute = true;
		float3 & val = handle.asFloat3();
		if (val[0] != mShininess[0] || 
			val[1] != mShininess[1] || 
			val[2] != mShininess[2])
		{
			mShininess[0] = val[0];
			mShininess[1] = val[1];
			mShininess[2] = val[2];
			mAttributesChanged = true;
		}
	}
	else if (plug == aGeometryShape)
	{
		handledAttribute = true;
		mGeometryShape = handle.asInt();
	}

	return handledAttribute;
}

/* virtual */
bool
ocSurfaceShader::getInternalValueInContext( const MPlug& plug,
										 MDataHandle& handle,
										 MDGContext& ctx)
{
	bool handledAttribute = false;

	if (plug == i_SLfilename)
	{
		return getSlFilenameName( plug, handle, ctx);
	}
	if (plug == i_templateName)
	{
		return getShaderName( plug, handle, ctx);
	}
	if (plug == i_shaderAttrsList)
	{
		return getShaderAttrsList( plug, handle, ctx);
	}

	if (plug == aColor)
	{
		handledAttribute = true;
		handle.set( mAmbientColor[0], mAmbientColor[1], mAmbientColor[2] );
	}
	else if (plug == aDiffuseColor)
	{
		handledAttribute = true;
		handle.set( mDiffuseColor[0], mDiffuseColor[1], mDiffuseColor[2] );
	}
	else if (plug == aSpecularColor)
	{
		handledAttribute = true;
		handle.set( mSpecularColor[0], mSpecularColor[1], mSpecularColor[2] );
	}
	else if (plug == aShininess)
	{
		handledAttribute = true;
		handle.set( mShininess[0], mShininess[1], mShininess[2] );
	}
	else if (plug == aGeometryShape)
	{
		handledAttribute = true;
		handle.set( (int) mGeometryShape );
	}
	return handledAttribute;
}

// #define _MAKE_HWSHADER_NODE_STATICLY_EVALUATE_

/* virtual */
MStatus	ocSurfaceShader::bind(const MDrawRequest& request, M3dView& view)

{
	TRACE_API_CALLS("bind");

	if (mAttributesChanged || (phong_map_id == 0))
	{
		init_Phong_texture ();
	}

	return MS::kSuccess;
}


/* virtual */
MStatus	ocSurfaceShader::glBind(const MDagPath&)
{
	TRACE_API_CALLS("glBind");

	if ( mAttributesChanged || (phong_map_id == 0))
	{
		init_Phong_texture ();
	}

	return MS::kSuccess;
}


/* virtual */
MStatus	ocSurfaceShader::unbind(const MDrawRequest& request, M3dView& view)
{
	TRACE_API_CALLS("unbind");

    // The texture may have been allocated by the draw; it's kept
    // around for use again. When scene new or open is performed this
    // texture will be released in releaseEverything().

	return MS::kSuccess;
}

/* virtual */
MStatus	ocSurfaceShader::glUnbind(const MDagPath&)
{
	TRACE_API_CALLS("glUnbind");

    // The texture may have been allocated by the draw; it's kept
    // around for use again. When scene new or open is performed this
    // texture will be released in releaseEverything().

	return MS::kSuccess;
}

/* virtual */
MStatus	ocSurfaceShader::geometry( const MDrawRequest& request,
							    M3dView& view,
                                int prim,
								unsigned int writable,
								int indexCount,
								const unsigned int * indexArray,
								int vertexCount,
								const int * vertexIDs,
								const float * vertexArray,
								int normalCount,
								const float ** normalArrays,
								int colorCount,
								const float ** colorArrays,
								int texCoordCount,
								const float ** texCoordArrays)
{
	TRACE_API_CALLS("geometry");
	MStatus stat = MStatus::kSuccess;

	if (mGeometryShape != 0)
		drawDefaultGeometry();
	else
		stat = draw( prim, writable, indexCount, indexArray, vertexCount,
				vertexIDs, vertexArray, normalCount, normalArrays, colorCount, 
				colorArrays, texCoordCount, texCoordArrays);
	return stat;
}

/* virtual */
MStatus	ocSurfaceShader::glGeometry(const MDagPath & path,
                                int prim,
								unsigned int writable,
								int indexCount,
								const unsigned int * indexArray,
								int vertexCount,
								const int * vertexIDs,
								const float * vertexArray,
								int normalCount,
								const float ** normalArrays,
								int colorCount,
								const float ** colorArrays,
								int texCoordCount,
								const float ** texCoordArrays)
{
	TRACE_API_CALLS("glGeometry");
	MStatus stat = MStatus::kSuccess;

	if (mGeometryShape != 0)
		drawDefaultGeometry();
	else
		stat = draw( prim, writable, indexCount, indexArray, vertexCount,
					vertexIDs, vertexArray, normalCount, normalArrays, colorCount, 
					colorArrays, texCoordCount, texCoordArrays);
	return stat;
}

/* virtual */
MStatus 
ocSurfaceShader::renderSwatchImage( MImage & outImage )
{
	unsigned int width, height;
	outImage.getSize( width, height );
	displayStringD("ocSurfaceShader::renderSwatchImage %d %d " , width, height);

	return outImage.readFromFile("M:/public/images/untitled/ocRenderPass1_64/ocRenderPass1_64_0001.tif");
//	return outImage.readFromFile("M:/public/images/untitled/ocRenderPass1/ocRenderPass1_0001.tif");
	

	MStatus status = MStatus::kFailure;

	// Get the hardware renderer utility class
	MHardwareRenderer *pRenderer = MHardwareRenderer::theRenderer();
	if (pRenderer)
	{
		const MString& backEndStr = pRenderer->backEndString();

		// Get geometry
		// ============
		unsigned int* pIndexing = 0;
		unsigned int  numberOfData = 0;
		unsigned int  indexCount = 0;

		MHardwareRenderer::GeometricShape gshape = MHardwareRenderer::kDefaultSphere;
		if (mGeometryShape == 2)
		{
			gshape = MHardwareRenderer::kDefaultCube;
		}
		else if (mGeometryShape == 3)
		{
			gshape = MHardwareRenderer::kDefaultPlane;
		}

		MGeometryData* pGeomData =
			pRenderer->referenceDefaultGeometry( gshape, numberOfData, pIndexing, indexCount );
		if( !pGeomData )
		{
			return MStatus::kFailure;
		}

		// Make the swatch context current
		// ===============================
		//
		unsigned int width, height;
		outImage.getSize( width, height );
		unsigned int origWidth = width;
		unsigned int origHeight = height;

		MStatus status2 = pRenderer->makeSwatchContextCurrent( backEndStr, width, height );

		if( status2 == MS::kSuccess )
		{
			glPushAttrib ( GL_ALL_ATTRIB_BITS );

			// Get camera
			// ==========
			{
				// Get the camera frustum from the API
				double l, r, b, t, n, f;
				pRenderer->getSwatchOrthoCameraSetting( l, r, b, t, n, f );

				glMatrixMode(GL_PROJECTION);
				glLoadIdentity();
				glOrtho( l, r, b, t, n, f );

				glMatrixMode(GL_MODELVIEW);
				glLoadIdentity();
				// Rotate the cube a bit so we don't see it head on
				if (gshape ==  MHardwareRenderer::kDefaultCube)
					glRotatef( 45, 1.0, 1.0, 1.0 );
				else if (gshape == MHardwareRenderer::kDefaultPlane)
					glScalef( 1.5, 1.5, 1.5 );
				else
					glScalef( 1.0, 1.0, 1.0 );
			}

			// Draw The Swatch
			// ===============
			drawTheSwatch( pGeomData, pIndexing, numberOfData, indexCount );

			// Read pixels back from swatch context to MImage
			// ==============================================
			pRenderer->readSwatchContextPixels( backEndStr, outImage );

			// Double check the outing going image size as image resizing
			// was required to properly read from the swatch context
			outImage.getSize( width, height );
			if (width != origWidth || height != origHeight)
			{
				status = MStatus::kFailure;
			}
			else
			{
				status = MStatus::kSuccess;
			}

			glPopAttrib();
		}
		else
		{
			pRenderer->dereferenceGeometry( pGeomData, numberOfData );
		}
	}
	return status;
}
	
void ocSurfaceShader::drawTheSwatch( 
	MGeometryData* pGeomData,
	unsigned int* pIndexing,
	unsigned int  numberOfData,
	unsigned int  indexCount )
{
	TRACE_API_CALLS("drwaTheSwatch");

	MHardwareRenderer *pRenderer = MHardwareRenderer::theRenderer();
	if( !pRenderer )	return;

	if ( mAttributesChanged || (phong_map_id == 0))
	{
		init_Phong_texture ();
	}


	// Get the default background color
	float r, g, b, a;
	MHWShaderSwatchGenerator::getSwatchBackgroundColor( r, g, b, a );
	glClearColor( r, g, b, a );

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);


	glDisable ( GL_LIGHTING );
	glDisable ( GL_TEXTURE_1D );
	glDisable ( GL_TEXTURE_2D );
	{
		glEnable ( GL_TEXTURE_CUBE_MAP_EXT );
		glBindTexture ( GL_TEXTURE_CUBE_MAP_EXT, phong_map_id );
		glEnable ( GL_TEXTURE_GEN_S );
		glEnable ( GL_TEXTURE_GEN_T );
		glEnable ( GL_TEXTURE_GEN_R );
		glTexGeni ( GL_S, GL_TEXTURE_GEN_MODE, GL_NORMAL_MAP_EXT );
		glTexGeni ( GL_T, GL_TEXTURE_GEN_MODE, GL_NORMAL_MAP_EXT );
		glTexGeni ( GL_R, GL_TEXTURE_GEN_MODE, GL_NORMAL_MAP_EXT );
		glTexParameteri(GL_TEXTURE_CUBE_MAP_EXT, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_CUBE_MAP_EXT, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_CUBE_MAP_EXT, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP_EXT, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		glTexEnvi ( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );

		// Could modify the texture matrix here to do light tracking...
		glMatrixMode ( GL_TEXTURE );
		glPushMatrix ();
		glLoadIdentity ();
		glRotatef( 5.0, -1.0, 0.0, 0.0 );
		glRotatef( 10.0, 0.0, 1.0, 0.0 );
		glMatrixMode ( GL_MODELVIEW );
	}

	// Draw default geometry
	{
		if (pGeomData)
		{
			glPushClientAttrib ( GL_CLIENT_VERTEX_ARRAY_BIT );

			float *vertexData = (float *)( pGeomData[0].data() );
			if (vertexData)
			{
				glEnableClientState( GL_VERTEX_ARRAY );
				glVertexPointer ( 3, GL_FLOAT, 0, vertexData );
			}

			float *normalData = (float *)( pGeomData[1].data() );
			if (normalData)
			{
				glEnableClientState( GL_NORMAL_ARRAY );
				glNormalPointer (    GL_FLOAT, 0, normalData );
			}

			if (vertexData && normalData && pIndexing )
				glDrawElements ( GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, pIndexing );

			glPopClientAttrib();

			// Release data references
			pRenderer->dereferenceGeometry( pGeomData, numberOfData );
		}
	}

	{
		glMatrixMode ( GL_TEXTURE );
		glPopMatrix ();
		glMatrixMode ( GL_MODELVIEW );

		glDisable ( GL_TEXTURE_CUBE_MAP_EXT );
		glDisable ( GL_TEXTURE_GEN_S );
		glDisable ( GL_TEXTURE_GEN_T );
		glDisable ( GL_TEXTURE_GEN_R );

	}
}

void ocSurfaceShader::drawDefaultGeometry()
{
	TRACE_API_CALLS("drawDefaultGeometry");

	MHardwareRenderer *pRenderer = MHardwareRenderer::theRenderer();
	if (!pRenderer)
		return;


	glPushAttrib ( GL_ENABLE_BIT );

	glDisable ( GL_LIGHTING );
	glDisable ( GL_TEXTURE_1D );
	glDisable ( GL_TEXTURE_2D );

	{
		glEnable ( GL_TEXTURE_CUBE_MAP_EXT );
		glBindTexture ( GL_TEXTURE_CUBE_MAP_EXT, phong_map_id );
		glEnable ( GL_TEXTURE_GEN_S );
		glEnable ( GL_TEXTURE_GEN_T );
		glEnable ( GL_TEXTURE_GEN_R );
		glTexGeni ( GL_S, GL_TEXTURE_GEN_MODE, GL_NORMAL_MAP_EXT );
		glTexGeni ( GL_T, GL_TEXTURE_GEN_MODE, GL_NORMAL_MAP_EXT );
		glTexGeni ( GL_R, GL_TEXTURE_GEN_MODE, GL_NORMAL_MAP_EXT );
		glTexParameteri(GL_TEXTURE_CUBE_MAP_EXT, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_CUBE_MAP_EXT, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_CUBE_MAP_EXT, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP_EXT, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		glTexEnvi ( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );

		glMatrixMode ( GL_TEXTURE );
		glPushMatrix ();
		glLoadIdentity ();
		glMatrixMode ( GL_MODELVIEW );
	}

	// Get default geometry
	{
		unsigned int numberOfData = 0;
		unsigned int *pIndexing = 0;
		unsigned int indexCount = 0;

		MHardwareRenderer::GeometricShape gshape = MHardwareRenderer::kDefaultSphere;
		if (mGeometryShape == 2)
		{
			gshape = MHardwareRenderer::kDefaultCube;
		}
		else if (mGeometryShape == 3)
		{
			gshape = MHardwareRenderer::kDefaultPlane;
		}

		// Get data references
		MGeometryData * pGeomData =
			pRenderer->referenceDefaultGeometry( gshape, numberOfData, pIndexing, indexCount);

		if (pGeomData)
		{
			glPushClientAttrib ( GL_CLIENT_VERTEX_ARRAY_BIT );

			float *vertexData = (float *)( pGeomData[0].data() );
			if (vertexData)
			{
				glEnableClientState( GL_VERTEX_ARRAY );
				glVertexPointer ( 3, GL_FLOAT, 0, vertexData );
			}

			float *normalData = (float *)( pGeomData[1].data() );
			if (normalData)
			{
				glEnableClientState( GL_NORMAL_ARRAY );
				glNormalPointer (    GL_FLOAT, 0, normalData );
			}

			if (vertexData && normalData && pIndexing )
				glDrawElements ( GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, pIndexing );

			glPopClientAttrib();

			// Release data references
			pRenderer->dereferenceGeometry( pGeomData, numberOfData );
		}
	}


	{
		glMatrixMode ( GL_TEXTURE );
		glPopMatrix ();
		glMatrixMode ( GL_MODELVIEW );

		glDisable ( GL_TEXTURE_CUBE_MAP_EXT );
		glDisable ( GL_TEXTURE_GEN_S );
		glDisable ( GL_TEXTURE_GEN_T );
		glDisable ( GL_TEXTURE_GEN_R );

		glPopAttrib();
	}
}

MStatus	ocSurfaceShader::draw(int prim,
							unsigned int writable,
							int indexCount,
							const unsigned int * indexArray,
							int vertexCount,
							const int * vertexIDs,
							const float * vertexArray,
							int normalCount,
							const float ** normalArrays,
							int colorCount,
							const float ** colorArrays,
							int texCoordCount,
							const float ** texCoordArrays)
{
	TRACE_API_CALLS("draw");


#if !defined(_MAKE_HWSHADER_NODE_STATICLY_EVALUATE_)
	if ( prim != GL_TRIANGLES && prim != GL_TRIANGLE_STRIP)	{
        return MS::kFailure;
    }

    {
		// glPushAttrib ( GL_ALL_ATTRIB_BITS ); // This is overkill
		// Even this is too much as matrices and texture state are restored.
		// glPushAttrib ( GL_ENABLE_BIT | GL_TEXTURE_BIT | GL_TRANSFORM_BIT );
		glPushAttrib ( GL_ENABLE_BIT );

		glDisable ( GL_LIGHTING );
		glDisable ( GL_TEXTURE_1D );
		glDisable ( GL_TEXTURE_2D );

		// Setup cube map generation
		glEnable ( GL_TEXTURE_CUBE_MAP_EXT );
		glBindTexture ( GL_TEXTURE_CUBE_MAP_EXT, phong_map_id );
		glEnable ( GL_TEXTURE_GEN_S );
		glEnable ( GL_TEXTURE_GEN_T );
		glEnable ( GL_TEXTURE_GEN_R );
		glTexGeni ( GL_S, GL_TEXTURE_GEN_MODE, GL_NORMAL_MAP_EXT );
		glTexGeni ( GL_T, GL_TEXTURE_GEN_MODE, GL_NORMAL_MAP_EXT );
		glTexGeni ( GL_R, GL_TEXTURE_GEN_MODE, GL_NORMAL_MAP_EXT );
		glTexParameteri(GL_TEXTURE_CUBE_MAP_EXT, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_CUBE_MAP_EXT, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_CUBE_MAP_EXT, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP_EXT, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		glTexEnvi ( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );

		// Could modify the texture matrix here to do light tracking...
		glMatrixMode ( GL_TEXTURE );
		glPushMatrix ();
		glLoadIdentity ();
		glMatrixMode ( GL_MODELVIEW );
    }
#endif
	// Draw the surface.
	//
    {
		glPushClientAttrib ( GL_CLIENT_VERTEX_ARRAY_BIT );
		// GL_VERTEX_ARRAY does not necessarily need to be
		// enabled, as it should be enabled before this routine
		// is valled.
		glEnableClientState( GL_VERTEX_ARRAY );
		glEnableClientState( GL_NORMAL_ARRAY );

		glVertexPointer ( 3, GL_FLOAT, 0, &vertexArray[0] );
		glNormalPointer (    GL_FLOAT, 0, &normalArrays[0][0] );
		
		glDrawElements ( prim, indexCount, GL_UNSIGNED_INT, indexArray );
		
		// The client attribute is already being popped. You
		// don't need to reset state here.
		//glDisableClientState( GL_NORMAL_ARRAY );
		//glDisableClientState( GL_VERTEX_ARRAY );
		glPopClientAttrib();
    }
#if !defined(_MAKE_HWSHADER_NODE_STATICLY_EVALUATE_)
	{
		glMatrixMode ( GL_TEXTURE );
		glPopMatrix ();
		glMatrixMode ( GL_MODELVIEW );

		glDisable ( GL_TEXTURE_CUBE_MAP_EXT );
		glDisable ( GL_TEXTURE_GEN_S );
		glDisable ( GL_TEXTURE_GEN_T );
		glDisable ( GL_TEXTURE_GEN_R );

		glPopAttrib();
	}
#endif
	return MS::kSuccess;
}

/* virtual */
int	ocSurfaceShader::normalsPerVertex()
{
	TRACE_API_CALLS("normalsPerVertex");
#if defined(_TEST_CYCLING_NORMALS_PER_VERTEX_)
	static int normCnt = 1;
	if (normCnt == 3)
		normCnt = 1;
	else
		normCnt++;
	return normCnt;
#else
	return 1;
#endif
}

/* virtual */
int	ocSurfaceShader::texCoordsPerVertex()
{
	TRACE_API_CALLS("texCoordsPerVertex");
#if defined(_TEST_CYCLING_UVSETS_PER_VERTEX_)
	static int uvCnt = 1;
	if (uvCnt == 3)
		uvCnt = 1;
	else
		uvCnt++;
	return uvCnt;
#else
	return 0;
#endif
}

/* virtual */ 
int	ocSurfaceShader::getTexCoordSetNames(MStringArray& names)
{
	return 0;
}
