#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ocParticleAttributeNode.h
//
// Dependency Graph Node: ocParticleAttribute

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 

class ocParticleFloatAttribute : public MPxNode
{
public:
	ocParticleFloatAttribute();
	virtual ~ocParticleFloatAttribute(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_input;
	static MObject o_output;

public:
	static MTypeId id;
	static const MString typeName;
};

class ocParticleVectorAttribute : public MPxNode
{
public:
	ocParticleVectorAttribute();
	virtual ~ocParticleVectorAttribute(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_input;
	static MObject o_output;

public:
	static MTypeId id;
	static const MString typeName;
};

