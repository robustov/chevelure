#include "stdafx.h"
//
// Copyright (C) 
// File: ocParticleInstancerCmd.cpp
// MEL Command: ocParticleInstancer

#include "ocParticleInstancer.h"

#include <maya/MGlobal.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnStringData.h>
#include <maya/MFnPluginData.h>
#include "mathNmaya/mathNmaya.h"
#include "math/Rotation3.h"
#include "ocellaris\renderOpenglImpl.h"

MObject ocParticleInstancer::i_id;
MObject ocParticleInstancer::i_position;

MObject ocParticleInstancer::i_rotationType;
	MObject ocParticleInstancer::i_dirvector, ocParticleInstancer::i_upvector;
	MObject ocParticleInstancer::i_axis, ocParticleInstancer::i_angle;

MObject ocParticleInstancer::i_scale;
MObject ocParticleInstancer::i_phase;

MObject ocParticleInstancer::i_drawCache;

MObject ocParticleInstancer::o_ocsCache;

// display
MObject ocParticleInstancer::i_viewParticles;

ocParticleInstancer::ocParticleInstancer()
{
}
ocParticleInstancer::~ocParticleInstancer()
{
}

MObject ocParticleInstancer::getParticle()
{
	MPlugArray pa;
	MPlug plug(thisMObject(), i_id);
	plug.connectedTo(pa, true, false);
	MObject particle = pa[0].node();
	return particle;
}

bool ocParticleInstancer::Save(
	cls::IRender* render, 
	Math::Box3f& box, 
	cls::IExportContext* context)
{
	MObject thisNode = thisMObject();

	std::vector<float> frame;
	cls::PA<int> res_id;
	cls::PA<Math::Matrix4f> res_pos;
	process(thisNode, res_id, res_pos, frame);

	MDataBlock data = ((ocParticleInstancer*)(this))->forceCache();
	MObject cacheval;
	cls::MInstancePxData* cache = getCache(data, cacheval);

	// particle
	MObject particle = getParticle();
	if( particle.isNull()) return MS::kFailure;

	// ���������� ������ ������� ���������
	std::map<std::string, cls::Param> attributes;
	attributes = *cache->drawcache.getAttributes();

	if( !cache) 
		return false;

	// ��������� ���������
	std::map<std::string, cls::Param>::iterator it = attributes.begin();
	for(;it != attributes.end(); it++)
	{
		std::string globalAttr = it->first;
		displayStringD(it->first.c_str());

		std::string plugname = "cls_"+globalAttr;
		MPlug XXX = getDataPlug(thisNode, particle, plugname.c_str());

		// ��������
		cls::Param val = ocExport_GetAttrArrayValue(XXX.node(), XXX.attribute());
		if( val.size() < res_pos.size())
			continue;

		if(it->second->type == cls::PT_COLOR)
			val->type = cls::PT_COLOR;
		it->second = val;
	}

	for(int i=0; i<res_pos.size(); i++)
	{
		int f = (int)frame[i];
		float Phase = frame[i] - f;
		std::string filename = cache->drawcache.getFile();
		if( filename.empty()) continue;

		Math::Box3f b = cache->drawcache.getBox();

		render->Parameter("id", res_id[i]);
		render->PushAttributes();
		render->PushTransform();

		render->PushAttributes();
		render->AppendTransform(res_pos[i]);
		render->PopAttributes();

		render->PushAttributes();
		render->Parameter("filename", filename.c_str());
		render->Parameter("#bound", b);
//		render->Parameter("id", res_id[i]);

		std::map<std::string, cls::Param>::iterator it = attributes.begin();
		for(;it != attributes.end(); it++)
		{
			std::string globalAttr = it->first;
			std::string plugname = "global::"+globalAttr;
			cls::Param& p = it->second;
			if(p->type == cls::PT_FLOAT)
			{
				cls::PA<float> pa = p;
				render->Attribute(plugname.c_str(), pa->type, pa[i]);
			}
			else if(p->type == cls::PT_VECTOR || p->type == cls::PT_COLOR)
			{
				cls::PA<Math::Vec3f> pa = p;
				render->Attribute(plugname.c_str(), pa->type, pa[i]);
			}

		}
//				render->DelayCall("ReadOCS");
		render->RenderCall("ReadOCS");
		render->PopAttributes();

		render->PopTransform();
		render->PopAttributes();

		b.transform(res_pos[i]);
		box.insert(b);
	}
	return true;
}

MStatus ocParticleInstancer::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;

	if( plug == o_ocsCache )
	{
		MObject thisNode = thisMObject();

		cls::MInstancePxData* pData = NULL;
		bool bNewData = false;
		MDataHandle outputData = data.outputValue( o_ocsCache, &stat );
		MPxData* userdata = outputData.asPluginData();
		pData = static_cast<cls::MInstancePxData*>(userdata);
		MFnPluginData fnDataCreator;
		if( !pData)
		{
			MTypeId tmpid( cls::MInstancePxData::id );
			MObject newDataObject = fnDataCreator.create( tmpid, &stat );
			pData = static_cast<cls::MInstancePxData*>(fnDataCreator.data( &stat ));
			bNewData = true;
		}
//		pData->data.clear();
//		pData->box = Math::Box3f();
		cls::IRender* render = pData->drawcache.asRender();

		std::vector<float> frame;
		cls::PA<int> res_id;
		cls::PA<Math::Matrix4f> res_pos;
		process(thisNode, res_id, res_pos, frame);

		MObject cacheval;
		cls::MInstancePxData* cache = getCache(data, cacheval);

		// particle
		MObject particle = getParticle();
		if( particle.isNull()) return MS::kFailure;

		// ���������� ������ ������� ���������
		std::map<std::string, cls::Param> attributes;
		attributes = *cache->drawcache.getAttributes();

		if( cache) 
		{
			// ��������� ���������
			std::map<std::string, cls::Param>::iterator it = attributes.begin();
			for(;it != attributes.end(); it++)
			{
				std::string globalAttr = it->first;
				displayStringD(it->first.c_str());

				std::string plugname = "cls_"+globalAttr;
				MPlug XXX = getDataPlug(thisNode, particle, plugname.c_str());

				// ��������
				cls::Param val = ocExport_GetAttrArrayValue(XXX.node(), XXX.attribute());
				if( val.size() < res_pos.size())
					continue;

				if(it->second->type == cls::PT_COLOR)
					val->type = cls::PT_COLOR;
				it->second = val;
			}

			Math::Box3f cbox;
			for(int i=0; i<res_pos.size(); i++)
			{
				int f = (int)frame[i];
				float Phase = frame[i] - f;
				std::string filename = cache->drawcache.getFile();
				if( filename.empty()) continue;

				Math::Box3f b = cache->drawcache.getBox();

				render->PushAttributes();
				render->PushTransform();
				render->AppendTransform(res_pos[i]);
//				render->Attribute("Phase", Phase);
				render->Parameter("filename", filename.c_str());
				render->Parameter("#bound", b);
				render->Parameter("id", res_id[i]);

				std::map<std::string, cls::Param>::iterator it = attributes.begin();
				for(;it != attributes.end(); it++)
				{
					std::string globalAttr = it->first;
					std::string plugname = "global::"+globalAttr;
					cls::Param& p = it->second;
					if(p->type == cls::PT_FLOAT)
					{
						cls::PA<float> pa = p;
						render->Attribute(plugname.c_str(), pa->type, pa[i]);
					}
					else if(p->type == cls::PT_VECTOR || p->type == cls::PT_COLOR)
					{
						cls::PA<Math::Vec3f> pa = p;
						render->Attribute(plugname.c_str(), pa->type, pa[i]);
					}

				}
//				render->DelayCall("ReadOCS");
				render->Call("ReadOCS");
				render->PopTransform();
				render->PopAttributes();

				b.transform(res_pos[i]);
				cbox.insert(b);
			}
			pData->drawcache.setLoaded(cbox);
//			displayString("ocParticleInstancer::compute: {%f, %f, %f}-{%f, %f, %f}", cbox.min.x, cbox.min.y, cbox.min.z, cbox.max.x, cbox.max.y, cbox.max.z);
		}

		if( bNewData)
			outputData.set(pData);
		data.setClean(plug);
		return MS::kSuccess;
	}

	return MS::kUnknownParameter;
}

bool ocParticleInstancer::isBounded() const
{
	MObject thisNode = thisMObject();
	return true;
}

MBoundingBox ocParticleInstancer::boundingBox() const
{
	MDataBlock data = ((ocParticleInstancer*)(this))->forceCache();
	MObject thisNode = thisMObject();

	MBoundingBox box( MPoint(-0.5, -0.5, -0.5), MPoint(0.5, 0.5, 0.5));

	std::vector<float> frame;
	cls::PA<int> res_id;
	cls::PA<Math::Matrix4f> res_pos;
	process(thisNode, res_id, res_pos, frame);

	MObject cacheval;
	cls::MInstancePxData* cache = getCache(data, cacheval);

	if( !cache) 
		return box;

	Math::Box3f cbox;
	int real = 0;
	for(int i=0; i<res_pos.size(); i++)
	{
		int f = (int)frame[i];

		bool bis = cache->drawcache.isValid();
		if( !bis)
			continue;

		Math::Box3f b = cache->drawcache.getBox();

		real++;
		b.transform(res_pos[i]);
		cbox.insert(b);
	}
//	displayString("full: %d, real: %d", res_pos.size(), real);
//	displayString("ocParticleInstancer::boundingBox: {%f, %f, %f}-{%f, %f, %f}", cbox.min.x, cbox.min.y, cbox.min.z, cbox.max.x, cbox.max.y, cbox.max.z);

	copy(box, cbox);
	return box;
}

void ocParticleInstancer::draw(
	M3dView &view, 
	const MDagPath &path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status)
{ 
	MDataBlock data = forceCache();
	MObject thisNode = thisMObject();

	std::vector<float> frame;
	cls::PA<int> res_id;
	cls::PA<Math::Matrix4f> res_pos;
	process(thisNode, res_id, res_pos, frame);

	MObject cacheval;
	cls::MInstancePxData* cache = getCache(data, cacheval);

	if( !cache) 
		return;

	MObject particle = getParticle();
	if( particle.isNull()) 
		return;

	//////////////////////////////////////////////////
	// ���������� ������ ������� ���������
	std::map<std::string, cls::Param> attributes;
	attributes = *cache->drawcache.getAttributes();
	// ��������� ���������
	std::map<std::string, cls::Param>::iterator it = attributes.begin();
	for(;it != attributes.end(); it++)
	{
		std::string globalAttr = it->first;
//		displayStringD(it->first.c_str());

		std::string plugname = "cls_"+globalAttr;
		MPlug XXX = getDataPlug(thisNode, particle, plugname.c_str());

		// ��������
		cls::Param val = ocExport_GetAttrArrayValue(XXX.node(), XXX.attribute());
		if( val.empty()) 
			continue;
		if( val.size() < res_pos.size())
			continue;
 
		if(it->second->type == cls::PT_COLOR)
			val->type = cls::PT_COLOR;
		it->second = val;
	}


	bool bWireframe = (style!=M3dView::kGouraudShaded);
	bool bShaded = (style==M3dView::kGouraudShaded);
	if(status!=M3dView::kDormant && status!=M3dView::kInvisible && status!=M3dView::kTemplate)
		bWireframe = true;

	view.beginGL(); 

	cls::renderOpenglImpl glrender;
	glrender.bPoints = false;
	glrender.bWireFrame = bWireframe;
	glrender.bShaded = bShaded;
	cls::IRender* render = &glrender;
	/////////////////////////////////////////////////////
	// ���������
	for(int i=0; i<res_pos.size(); i++)
	{
		int f = (int)frame[i];
		bool bis = cache->drawcache.isValid();
		cache->drawcache.UpdateAttrList(thisNode, true);
		if(bis)
		{
			render->PushTransform();
			render->PushAttributes();
//			glPushMatrix();
//			float* d = res_pos[i].data();
//			glMultMatrixf(d);
			render->AppendTransform(res_pos[i]);

			// ��������
			std::map<std::string, cls::Param>::iterator it = attributes.begin();
			for(;it != attributes.end(); it++)
			{
				std::string globalAttr = it->first;
				std::string plugname = "global::"+globalAttr;
				cls::Param& p = it->second;
				if(p->type == cls::PT_FLOAT)
				{
					cls::PA<float> pa = p;
					render->Attribute(plugname.c_str(), pa->type, pa[i]);
				}
				else if(p->type == cls::PT_VECTOR || p->type == cls::PT_COLOR)
				{
					cls::PA<Math::Vec3f> pa = p;
					render->Attribute(plugname.c_str(), pa->type, pa[i]);
				}
			}

			cache->drawcache.RenderToMaya(thisMObject(), render);

//			glPopMatrix();
			render->PopAttributes();
			render->PopTransform();
		}
	}

	view.endGL();
};

/*/
MPlug ocParticleInstancer::getDataPlug(MObject thisNode, MObject particle, const char* attibute)const
{
	MStatus stat;
	MFnDependencyNode thisdn(thisNode);
	MPlug plug = thisdn.findPlug(attibute);
	if( plug.isNull())
	{
		MFnStringData stringData;
		MFnTypedAttribute typedAttr;
		MObject attr = typedAttr.create(attibute, attibute, MFnData::kString, stringData.create(""), &stat);
		if( !stat) return MPlug();
		typedAttr.setConnectable(true);
		typedAttr.setStorable(true);
		typedAttr.setWritable(true);
		typedAttr.setReadable(true);
		typedAttr.setHidden(false);
		thisdn.addAttribute(attr);
		plug = thisdn.findPlug(attibute);
	}
	MString attrname;
	plug.getValue(attrname);
	if( attrname.length()==0) return MPlug();

	MFnDependencyNode dn(particle);
	plug = dn.findPlug(attrname);
	return plug;
}
/*/

void ocParticleInstancer::process(
	MObject thisNode, 
	cls::PA<int>& res_id, 
	cls::PA<Math::Matrix4f>& res_pos, 
	std::vector<float>& res_frame) const
{
	std::list<MObject> vals;
	MPlug plug;

	int rotationType;
	MPlug(thisNode, i_rotationType).getValue(rotationType);
	
	// particle
	MPlugArray pa;
	plug = MPlug(thisNode, i_id);
	plug.connectedTo(pa, true, false);
	MObject particle = pa[0].node();

	// ���������
	vals.push_back(MObject());
	plug = getDataPlug(thisNode, particle, "pos");
	plug.getValue(vals.back());
	MFnVectorArrayData pos(vals.back());

		// dir
		vals.push_back(MObject());
		plug = getDataPlug(thisNode, particle, "dir");
		plug.getValue(vals.back());
		MFnVectorArrayData dir(vals.back());

		// up
		vals.push_back(MObject());
		plug = getDataPlug(thisNode, particle, "up");
		plug.getValue(vals.back());
		MFnVectorArrayData up(vals.back());

		// axis
		vals.push_back(MObject());
		plug = getDataPlug(thisNode, particle, "axis");
		plug.getValue(vals.back());
		MFnVectorArrayData axis(vals.back());

		// angle
		vals.push_back(MObject());
		plug = getDataPlug(thisNode, particle, "angle");
		plug.getValue(vals.back());
		MFnDoubleArrayData angle(vals.back());


	vals.push_back(MObject());
	plug = getDataPlug(thisNode, particle, "scale");
	plug.getValue(vals.back());
	std::vector< Math::Vec3f> scale;

	MFn::Type t = vals.back().apiType();
	if( vals.back().apiType() == MFn::kVectorArrayData)
	{
		MFnVectorArrayData scaleV(vals.back());
		scale.resize(scaleV.length());
		for(unsigned i=0; i<scaleV.length(); i++)
			copy( scale[i], scaleV[i]);
	}
	else if( vals.back().apiType() == MFn::kDoubleArrayData)
	{
		MFnDoubleArrayData scaleS(vals.back());
		scale.resize(scaleS.length());
		for(unsigned i=0; i<scaleS.length(); i++)
			scale[i] = Math::Vec3f( (float)scaleS[i], (float)scaleS[i], (float)scaleS[i]);
	}
	else if( vals.back().apiType() == MFn::kData3Double)
	{
		MFnNumericData nd(vals.back());
		MVector v;
		nd.getData(v.x, v.y, v.z);

		scale.resize(pos.length());
		for(unsigned i=0; i<pos.length(); i++)
			copy( scale[i], v);
	}
	else if( vals.back().apiType() == MFn::kInvalid)
	{
		double val;
		if( plug.getValue(val))
		{
			scale.resize(pos.length());
			for(unsigned i=0; i<pos.length(); i++)
				scale[i] = Math::Vec3f( (float)val, (float)val, (float)val);
		}
	}
	if(false)
	{
		double val;
		plug.getValue(val);
		scale.resize(pos.length());
		for(unsigned i=0; i<pos.length(); i++)
			scale[i] = Math::Vec3f( (float)val, (float)val, (float)val);
	}

//	if( scale.length()<size) return;

	// ��
	plug = pa[0];
	vals.push_back(MObject());
	plug.getValue(vals.back());
	MFnDoubleArrayData ids(vals.back());
	unsigned size = ids.length();

	vals.push_back(MObject());
	plug = getDataPlug(thisNode, particle, "phase");
	plug.getValue(vals.back());
	MFnDoubleArrayData frame(vals.back());

	if( pos.length()<size) return;

	// ���������������� �������
	res_id.resize(size);
	res_pos.resize(size);
	res_frame.resize(size, 0);

	// ������
	for(unsigned i=0; i<size; i++)
	{
		res_id[i] = (int)ids[i];
		res_frame[i] = (float)frame[i];
//fprintf(stderr, "%d, ", res_id[i]);
		Math::Vec3f p;
		copy( p, pos[i]);
		Math::Matrix4f m = Math::Matrix4f::id;

		Math::Vec3f x(1, 0, 0);
		Math::Vec3f y(0, 1, 0);
		Math::Vec3f z(0, 0, 1);
		Math::Vec3f s(1, 1, 1);
		if(rotationType==0)
		{
			if( i<dir.length())
			{
//				Math::Vec3f x0 = x;
				copy( x, dir[i]);
//				Math::Rot3f rot(x0, x);
//				Math::Matrix4f m;
//				rot.getMatrix(m);
//				y = m*y;
//				z = m*y;
			}
			if( i<up.length())
			{
				copy( y, up[i]);
			}
		}
		else
		{
			if( i<axis.length() && i<angle.length())
			{
				Math::Vec3f ax;
				copy( ax, axis[i]);

				Math::Rot3f rot( (float)angle[i], ax);
				Math::Matrix4f m;
				rot.getMatrix(m);
				x = m*x;
				y = m*y;
				z = m*z;
			}
		}
		if( i<scale.size())
			s = scale[i];
		
		x.normalize();
		y.normalize();
		z = Math::cross(x, y);
		z.normalize();
		y = Math::cross(z, x);
		y.normalize();
//		x = x;

		m[0] = Math::Vec4f( x, 0)*s.x;
		m[1] = Math::Vec4f( y, 0)*s.y;
		m[2] = Math::Vec4f( z, 0)*s.z;
		m[3] = Math::Vec4f( p, 1);
		res_pos[i] = m;

	}
}

/*/
cls::MInstancePxData* ocParticleInstancer::getCache(MObject thisNode, MObject& obj_drawCache) const
{
	MPlug plug(thisNode, i_drawCache);
//	MObject obj_drawCache;
	plug.getValue(obj_drawCache);
	MFnPluginData pd(obj_drawCache);
	cls::MInstancePxData* cache = (cls::MInstancePxData*)pd.data();
	if( !cache) 
		return NULL;
	if( cache->dlists.empty()) 
		return NULL;
	return cache;
}
/*/

cls::MInstancePxData* ocParticleInstancer::getCache(MDataBlock data, MObject& obj_drawCache) const
{
	MPxData* pxdata = data.inputValue(i_drawCache).asPluginData();
	cls::MInstancePxData* cache = (cls::MInstancePxData*)pxdata;
	if( !cache) 
		return NULL;
	return cache;
}





void* ocParticleInstancer::creator()
{
	return new ocParticleInstancer();
}
MStatus ocParticleInstancer::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnEnumAttribute enumAttr;
	MStatus				stat;
	MFnStringData stringData;

	try
	{
		{
			i_id = typedAttr.create( "pid","id", MFnData::kDoubleArray, &stat);
			::addAttribute(i_id, atReadable|atWritable|atUnStorable|atUnCached);

			i_position = typedAttr.create( "pos","ps", MFnData::kString, stringData.create("position"), &stat);
			::addAttribute(i_position, atInput);

			i_phase = typedAttr.create( "phase","ph", MFnData::kString, stringData.create(""), &stat);
			::addAttribute(i_phase, atInput);

			i_rotationType = enumAttr.create ("rotationType", "rty");
			enumAttr.addField("basis (X+Y)", 0);
			enumAttr.addField("axis (Axis+Angle)", 1);
			::addAttribute(i_rotationType, atInput);

			{
				i_dirvector = typedAttr.create( "dir","dv", MFnData::kString, stringData.create(""), &stat);
				::addAttribute(i_dirvector, atInput);
			
				i_upvector = typedAttr.create( "up", "uv", MFnData::kString, stringData.create(""), &stat);
				::addAttribute(i_upvector, atInput);
			}
			{
				i_axis = typedAttr.create( "axis","ax", MFnData::kString, stringData.create(""), &stat);
				::addAttribute(i_axis, atInput);
			
				i_angle = typedAttr.create( "angle", "ang", MFnData::kString, stringData.create(""), &stat);
				::addAttribute(i_angle, atInput);
			}

			i_scale = typedAttr.create( "scale", "sv", MFnData::kString, stringData.create(""), &stat);
			::addAttribute(i_scale, atInput);
		}
		{
			i_drawCache = typedAttr.create( "drawCache", "dch", cls::MInstancePxData::id, MObject::kNullObj, &stat);
			::addAttribute(i_drawCache, atReadable|atUnStorable|atUnCached);
		}

		{
			o_ocsCache = typedAttr.create( "ocsCache", "occh", cls::MInstancePxData::id, MObject::kNullObj, &stat);
			::addAttribute(o_ocsCache, atWritable|atUnStorable|atUnCached);
			stat = attributeAffects( i_id, o_ocsCache );
			stat = attributeAffects( i_position, o_ocsCache );
			stat = attributeAffects( i_phase, o_ocsCache );
			stat = attributeAffects( i_dirvector, o_ocsCache );
			stat = attributeAffects( i_upvector, o_ocsCache );
			stat = attributeAffects( i_scale, o_ocsCache );
			stat = attributeAffects( i_drawCache, o_ocsCache );
		}
		// display
		{
			i_viewParticles = numAttr.create( "viewParticles","vp", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_viewParticles, atInput);
		}
		if( !MGlobal::sourceFile("AEocParticleInstancerTemplate.mel"))
		{
			displayString("error source AEocParticleInstancerTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

MPlug ocParticleInstancer::getDataPlug(MObject thisNode, MObject particle, const char* attibute) const 
{
	MStatus stat;
	MFnDependencyNode thisdn(thisNode);
	MPlug plug = thisdn.findPlug(attibute);
	if( plug.isNull())
	{
		MFnStringData stringData;
		MFnTypedAttribute typedAttr;
		MObject aobj = typedAttr.create( attibute, attibute, MFnData::kString, stringData.create(""), &stat);
		thisdn.addAttribute(aobj);
		displayString("Plug %s not found in %s", attibute, thisdn.name().asChar());
		return plug;
	}
	MString attrname;
	plug.getValue(attrname);
	if( attrname.length()==0)
	{
		return MPlug();
	}

	MFnDependencyNode dn(particle);
	plug = dn.findPlug(attrname);
	if( plug.isNull())
	{
		displayString("Plug %s not found in %s", attrname.asChar(), dn.name().asChar());
	}
	return plug;
}
