#pragma once
#include "pre.h"

#include <maya/MPxHwShaderNode.h>
#include <maya/MPoint.h>

// For swatch rendering
#include <maya/MHardwareRenderer.h>
#include <maya/MHWShaderSwatchGenerator.h>
#include <maya/MGeometryData.h>

#include "shading/ShadingGraphNodeImpl.h"
#include "shading/SlNodeImpl.h"

struct ocShaderGenerator;
class ocSurfaceShader : public MPxHwShaderNode
{
	friend struct ocShaderGenerator;

public:
	ocSurfaceShader();
	virtual ~ocSurfaceShader();

	virtual void postConstructor();
	virtual MStatus compute( const MPlug&, MDataBlock& );
	virtual MStatus computeShaderFile( const MPlug&, MDataBlock& );

	virtual MStatus setDependentsDirty( const MPlug& plug, MPlugArray& pa);

	static  MTypeId id;
	static const MString typeName;

public:
	// Attributes
	static MObject i_SLfilename;		// ��� �������� �������
	static MObject i_templateName;		// ��� ������� ( dllname@procname)
	static MObject i_shaderAttrsList;	// ������ ���������� ��������
	static MObject i_ignoreLastShader;	// ������������ ������ �������
	static MObject o_shaderFile;		// ��� ����� ���������������� �������

	static MObject i_displacementBound;
	static MObject i_displacementSpace;
	static MObject o_displacement;

	// Internal

	// ������
	SlNodeImpl slNode;
	ShadingGraphNodeImpl graphNode;

	// ������ ���������
	int getParameterCount();
	bool getParameter(int i, std::string& name, cls::Param& val);

public:
	void BuildShader();


//	void InitGraphNode(const char* name);


	bool getSlFilenameName( const MPlug& plug, MDataHandle& handle, MDGContext& );
	bool getShaderName( const MPlug& plug, MDataHandle& handle, MDGContext& );
	bool getShaderAttrsList( const MPlug& plug, MDataHandle& handle, MDGContext& );

	bool setSlFilenameName( const MPlug& plug, const MDataHandle& handle, MDGContext& );
	bool setShaderName( const MPlug& plug, const MDataHandle& handle, MDGContext& );
	bool setShaderAttrsList( const MPlug& plug, const MDataHandle& handle, MDGContext& );




public:
// ��� �� ���:
	void			releaseEverything();


	// Internally cached attribute handling routines
	virtual bool getInternalValueInContext( const MPlug&,
		MDataHandle&,
		MDGContext&);
	virtual bool setInternalValueInContext( const MPlug&,
		const MDataHandle&,
		MDGContext&);

	// Interactive overrides
	virtual MStatus		bind( const MDrawRequest& request,
		M3dView& view );
	virtual MStatus		unbind( const MDrawRequest& request,
		M3dView& view );
	virtual MStatus		geometry( const MDrawRequest& request,
		M3dView& view,
		int prim,
		unsigned int writable,
		int indexCount,
		const unsigned int * indexArray,
		int vertexCount,
		const int * vertexIDs,
		const float * vertexArray,
		int normalCount,
		const float ** normalArrays,
		int colorCount,
		const float ** colorArrays,
		int texCoordCount,
		const float ** texCoordArrays);

	// Batch overrides
	virtual MStatus	glBind(const MDagPath& shapePath);
	virtual MStatus	glUnbind(const MDagPath& shapePath);
	virtual MStatus	glGeometry( const MDagPath& shapePath,
		int prim,
		unsigned int writable,
		int indexCount,
		const unsigned int * indexArray,
		int vertexCount,
		const int * vertexIDs,
		const float * vertexArray,
		int normalCount,
		const float ** normalArrays,
		int colorCount,
		const float ** colorArrays,
		int texCoordCount,
		const float ** texCoordArrays);

	MStatus			draw(
		int prim,
		unsigned int writable,
		int indexCount,
		const unsigned int * indexArray,
		int vertexCount,
		const int * vertexIDs,
		const float * vertexArray,
		int normalCount,
		const float ** normalArrays,
		int colorCount,
		const float ** colorArrays,
		int texCoordCount,
		const float ** texCoordArrays);

	void			drawDefaultGeometry();

	// Overridden to draw an image for swatch rendering.
	///
	virtual MStatus renderSwatchImage( MImage & image );
	void			drawTheSwatch( MGeometryData* pGeomData,
		unsigned int* pIndexing,
		unsigned int  numberOfData,
		unsigned int  indexCount );

	virtual int		normalsPerVertex();
	virtual int		texCoordsPerVertex();
	virtual int		getTexCoordSetNames(MStringArray& names);

	static  void *  creator();
	static  MStatus initialize();

	MFloatVector	Phong ( double cos_a );
	void			init_Phong_texture ( void );
	void			printGlError( const char *call );

protected:

	void attachSceneCallbacks();
	void detachSceneCallbacks();

	static void releaseCallback(void* clientData);

private:

	// Attributes
	static MObject  aColor;
	static MObject  aDiffuseColor;
	static MObject  aSpecularColor;
	static MObject  aShininess;
	static MObject	aGeometryShape;

public:
	// Internal data
	GLuint			phong_map_id;

	MPoint			cameraPosWS;

	float3			mAmbientColor;
	float3			mDiffuseColor;
	float3			mSpecularColor;
	float3			mShininess;
	unsigned int	mGeometryShape;
	bool			mAttributesChanged; // Keep track if any attributes changed

	// Callbacks that we monitor so we can release OpenGL-dependant
	// resources before their context gets destroyed.
	MCallbackId fBeforeNewCB;
	MCallbackId fBeforeOpenCB;
	MCallbackId fBeforeRemoveReferenceCB;
	MCallbackId fMayaExitingCB;
};

