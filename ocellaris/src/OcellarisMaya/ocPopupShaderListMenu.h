#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ocPopupShaderListMenuCmd.h
// MEL Command: ocPopupShaderListMenu

#include <maya/MPxCommand.h>

class ocPopupShaderListMenu : public MPxCommand
{
public:
	static const MString typeName;
	ocPopupShaderListMenu();
	virtual	~ocPopupShaderListMenu();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

