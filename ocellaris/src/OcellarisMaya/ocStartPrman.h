#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ocStartPrmanCmd.h
// MEL Command: ocStartPrman

#include <maya/MPxCommand.h>
#include "ocellaris/IRender.h"
#include "ocellaris/IPassGenerator.h"
#include "Alfred/Alfred.h"

namespace cls
{
	struct IPassGenerator;
	struct IExportContext;
};
#include "ocellaris/renderCacheImpl.h"

class ocStartPrman : public MPxCommand
{
public:
	ocStartPrman();
	virtual	~ocStartPrman();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();

	struct SubGenerator
	{
		cls::IPassGenerator* passGenerator;
		MObject passnode;
		cls::renderCacheImpl<> cache_prescene;
		cls::renderCacheImpl<> cache_prescene_nextgen;
		SubGenerator()
		{
			passGenerator = NULL;
		}
	};
public:
	bool GenRib(
		cls::IPassGenerator* passGenerator,
//		cls::renderCacheImpl<>& passexportcache,
		MObject passnode, 
		std::vector<SubGenerator>& subGenerators,
		cls::IExportContext* context,
		int frame,
		Math::Box3f box, 
		std::string filename,
		std::string ocsfilename,
		std::string ocsfilename_noblur,
		std::string ligthsfilename,
		std::string projectDir,
		bool bBlur, 
		float shutterOpenFrame, 
		float shutterCloseFrame, 
		std::string& cmd, 
		std::string& debugcmd, 
		Alfred::Task* alfredtask
		);

};

