#include "stdafx.h"
//
// Copyright (C) 
// File: ocPopupShaderListMenuCmd.cpp
// MEL Command: ocPopupShaderListMenu

#include "ocPopupShaderListMenu.h"
#include "shading/ShadingGraphNodeImpl.h"
#include "Util\tree.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>

ocPopupShaderListMenu::ocPopupShaderListMenu()
{
}
ocPopupShaderListMenu::~ocPopupShaderListMenu()
{
}

struct MenuItem
{
	std::string menuname;
	std::string shadername;
	MenuItem(const char* menuname, const char* shadername="")
	{
		this->menuname = menuname;
		this->shadername = shadername;
	}
};

// ������� ��������� ����
void createMenuRec(std::tree<MenuItem>::iterator root, const char* command)
{
	std::tree<MenuItem>::iterator it = root.begin();
	for(;it != root.end(); it++)
	{
		char cmd[1024];
		if(it->shadername.empty())
		{
			_snprintf(cmd, 1024, "menuItem -l \"%s\" -sm true -to true -aob true", 
				it->menuname.c_str());
		}
		else
		{
			char menucmd[1024];
			std::string shadername = "\\\""+it->shadername+"\\\"";
			_snprintf(menucmd, 1024, command, shadername.c_str());

			_snprintf(cmd, 1024, "menuItem -l \"%s\" -c (\"%s\")", 
				it->menuname.c_str(), 
				menucmd
				);
		}
		MGlobal::executeCommand( cmd);
		createMenuRec(it, command);
		if(it->shadername.empty())
		{
			MGlobal::executeCommand( "setParent -menu ..");

		}
	}
}

MSyntax ocPopupShaderListMenu::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

//	syntax.addArg(MSyntax::kString);	// arg0
	syntax.addFlag("c", "command", MSyntax::kString);
	syntax.addFlag("t", "type", MSyntax::kString);
	return syntax;
}


MStatus ocPopupShaderListMenu::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MString argsl0;
	argData.getFlagArgument("c", 0, argsl0);
	MString argsl1;
	argData.getFlagArgument("t", 0, argsl1);

	std::tree<MenuItem> menu;
	std::tree<MenuItem>::iterator root = menu.insert( menu.end(), MenuItem(""));

	{
		std::string dllname = "ocellarisShaders.dll";
		shn::getShaderListProc proc(dllname.c_str(), "GetShaderList");
		if( proc.isValid()) 
		{
			char** list = (*proc)();
			for(int i=0; list[i]; i++)
			{
				std::string proc = list[i];
				i++;
				if(!list[i]) break;
				std::string menuname = list[i];
				menuname += "/"+proc;
				i++;
				if(!list[i]) break;
				std::string type = list[i];

				// ������
				if( argsl1.length()!=0 && type != argsl1.asChar()) 
					continue;

				// ������� ����
				MStringArray ar;
				MString(menuname.c_str()).split('/', ar);

				std::tree<MenuItem>::iterator menuitem = root;
				for(int pm=0; pm<(int)ar.length(); pm++)
				{
					const char* mn = ar[pm].asChar();
					
					// ����� ��� ��������
					std::tree<MenuItem>::iterator it = menuitem.begin();
					for(;it != menuitem.end(); it++)
					{
						if(it->menuname == mn) break;
					}
					if( it==menuitem.end())
						it = menu.insert( menuitem.end(), MenuItem(mn));
					menuitem = it;
				}
				menuitem->shadername = dllname+"@"+proc;
			}
			
		}
	}

	// ������� ��������� ����
	createMenuRec(root, argsl0.asChar());

	return MS::kSuccess;
}

void* ocPopupShaderListMenu::creator()
{
	return new ocPopupShaderListMenu();
}

