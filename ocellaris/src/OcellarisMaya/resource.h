//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by OcellarisMaya.rc
//
#define AEocOcclusionPassTemplate_mel   103
#define AEocOccPassNodeTemplate_mel     104
#define AEocRenderPassTemplate_mel      105
#define AEocJointVisTemplate_mel        106
#define AEocGeneratorSetTemplate_mel    107
#define AEocSequenceFileTemplate_mel    108
#define AEocGeneratorProxyTemplate_mel  109
#define AEocRSLshaderTemplate_mel       110

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        111
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           111
#endif
#endif
