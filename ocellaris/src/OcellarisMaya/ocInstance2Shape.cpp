#include "stdafx.h"
#include "ocInstance.h"

#ifndef USE_LOCATOR_INSTANCE

#include <maya/MFnSingleIndexedComponent.h>
#include <maya/MAttributeSpecArray.h>
#include <maya/MAttributeSpec.h>
#include <maya/MAttributeIndex.h>
#include <maya/MSelectionMask.h>
#include <maya/MObjectArray.h>
#include <maya/MDagPath.h>
#include <maya/MFnPluginData.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnStringData.h>
#include "mathNmaya\mathNmaya.h"
#include "Util/misc_directory_processing.h"

MObject ocInstance::i_time;
MObject ocInstance::i_startTime;
MObject ocInstance::i_drawCache;
MObject ocInstance::i_drawBox;
MObject ocInstance::i_matrixArray;
MObject ocInstance::i_motionBlurEnable;
MObject ocInstance::o_filename;

ocInstance::ocInstance() 
{
}
MDataBlock ocInstance::getData()
{
	MDataBlock data = ((ocInstance*)(this))->forceCache();
	return data;
}

void ocInstance::postConstructor()
{
	MStatus stat;
	setRenderable( true);

	// for RFM
	{
		MObject obj = thisMObject();
		MFnDependencyNode dn(obj);
		MFnStringData stringData;
		MFnTypedAttribute typedAttr;
		MObject attr = typedAttr.create( "rman__torattr___preShapeScript", "", MFnData::kString, stringData.create("ocellarisRFMprocedure export", &stat), &stat );
		typedAttr.setConnectable(true);
		typedAttr.setStorable(true);
		typedAttr.setWritable(true);
		typedAttr.setReadable(true);
		typedAttr.setHidden(false);
		typedAttr.setKeyable(true);
		dn.addAttribute(attr);
	}
}
ocInstance::~ocInstance() 
{
}
void* ocInstance::creator()
{
	return new ocInstance();
}


MStatus ocInstance::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;
	if( plug==o_filename)
	{
		cls::MInstancePxData* cache = (cls::MInstancePxData*)data.inputValue( i_drawCache, &stat ).asPluginData();
		MTime t = data.inputValue( i_time, &stat ).asTime();
		MTime st = data.inputValue( i_startTime, &stat ).asTime();
		t -= st;
		double dt = t.as(MTime::uiUnit());
		int time = (int)(dt+0.5);
		if( time<0) time=0;

		if( !cache) 
			return MS::kSuccess;
		if( !cache->drawcache.isValid()) 
			return MS::kSuccess;

		MDataHandle out = data.outputValue(o_filename, &stat);
		out.set( MString(""));
		data.setClean(o_filename);

		MString file = cache->drawcache.getFile();
		out.set( file);
		return MS::kSuccess;
	}
	return MS::kUnknownParameter;
}



bool ocInstance::isBounded() const 
{
	return true; 
}

MBoundingBox ocInstance::boundingBox() const
{
	MObject thisNode = thisMObject();
//	MObject cacheval;
//	MDataBlock data = ((ocInstance*)(this))->forceCache();
//	cls::MInstancePxData* cache = getCache(data, cacheval);

	ocInstance2Geometry* geom = ((ocInstance*)(this))->getGeometry();

	MBoundingBox box( MPoint(-0.5, -0.5, -0.5), MPoint(0.5, 0.5, 0.5));
	if( !geom->cache) 
		return box;

//	int frame = getFrame(data, thisNode);
	bool bis = geom->cache->drawcache.isValid();
	if( bis)
	{
		if( geom->matricies.empty())
		{
			Math::Box3f mbox = geom->cache->drawcache.getBox();
			copy(box, mbox);
		}
		else
		{
			Math::Box3f mbox;
			for(int i=0; i<(int)geom->matricies.size(); i++)
			{
				Math::Matrix4f pos = geom->matricies[i];

				Math::Box3f box = geom->cache->drawcache.getBox();
				box.transform(pos);
				mbox.insert(box);
			}
			copy(box, mbox);
		}
	}
	return box;
}















ocInstance2Geometry* ocInstance::getGeometry()
{
	MStatus stat;
	MDataBlock data = forceCache();
	MObject thisNode = thisMObject();

	theGeometry.frame = -1;
	theGeometry.bDrawBox = false;
	theGeometry.bDrawBox = data.inputValue(i_drawBox).asBool();
	
	/*/
	MObject obj = data.inputValue(i_matrixArray).data();
	MFnDoubleArrayData dad(obj);
	int count = dad.length()/16;
	theGeometry.matricies.clear();
	if( count)
		theGeometry.matricies.resize(count);
	for(int i=0, x=0; i<count; i++)
	{
		Math::Matrix4f& m = theGeometry.matricies[i];
		float* da = m.data();
		for(int e=0; e<16; e++)
			da[e] = (float)dad[x++];
	}
	/*/

	MObject cacheval;
	theGeometry.cache = getCache(data, cacheval);
	if( theGeometry.cache) 
	{
		theGeometry.frame = getFrame(data, thisNode);
		if(theGeometry.cache->drawcache.isValid())
		{
			theGeometry.cache->drawcache.UpdateAttrList(thisNode);
		}
	}
	return &theGeometry;
}

MStatus ocInstance::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnUnitAttribute	unitAttr;
	MStatus stat;

	try
	{
		{
			i_time = unitAttr.create( "time", "t", MFnUnitAttribute::kTime);
			::addAttribute(i_time, atInput|atKeyable);
		}
		{
			i_startTime = unitAttr.create( "startTime", "st", MFnUnitAttribute::kTime);
			::addAttribute(i_startTime, atInput|atKeyable);
		}
		{
			i_drawBox = numAttr.create( "drawBox","db", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_drawBox, atInput|atKeyable);
		}
		{
			i_motionBlurEnable = numAttr.create( "motionBlurEnable","mbe", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_motionBlurEnable, atInput|atKeyable);
		}
		{
			i_matrixArray = typedAttr.create( "matrixArray", "ma", MFnData::kDoubleArray, &stat);
			::addAttribute(i_matrixArray, atReadable|atStorable|atUnCached|atHidden);
		}
		{
			i_drawCache = typedAttr.create( "drawCache", "dch", cls::MInstancePxData::id, MObject::kNullObj, &stat);
			::addAttribute(i_drawCache, atReadable|atUnStorable|atUnCached);
		}
		
		{
			o_filename = typedAttr.create ("filename","fn", MFnData::kString, &stat);
			::addAttribute(o_filename, atOutput);
			stat = attributeAffects( i_time, o_filename);
			stat = attributeAffects( i_startTime, o_filename);
			stat = attributeAffects( i_matrixArray, o_filename);
			stat = attributeAffects( i_drawCache, o_filename);
		}
		if( !MGlobal::sourceFile("AEocInstanceTemplate.mel"))
		{
			displayString("error source AEocInstanceTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

cls::MInstancePxData* ocInstance::getCache(MDataBlock data, MObject& obj_drawCache) const
{
	MPxData* pxdata = data.inputValue(i_drawCache).asPluginData();
	cls::MInstancePxData* cache = (cls::MInstancePxData*)pxdata;
	if( !cache) 
		return NULL;
	return cache;
}
int ocInstance::getFrame(MDataBlock data, MObject thisNode) const
{
	return 0;
}

#endif