#include "stdafx.h"
//
// Copyright (C) 
// File: ocPolyShaderLayerCmd.cpp
// MEL Command: ocPolyShaderLayer

#include "ocPolyShaderLayer.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MFnMesh.h>
#include <Maya/MIntArray.h>
#include <Maya/MFnIntArrayData.h>
#include <Maya/MFnSingleIndexedComponent.h>

ocPolyShaderLayer::ocPolyShaderLayer()
{
}
ocPolyShaderLayer::~ocPolyShaderLayer()
{
}

MSyntax ocPolyShaderLayer::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.addArg(MSyntax::kLong);	// ������ ����
	return syntax;
}

MStatus ocPolyShaderLayer::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	int sl = -1;
	argData.getCommandArgument(0, sl);

	MSelectionList list;
	MGlobal::getActiveSelectionList(list);
	for( int i=0; i<(int)list.length(); i++)
	{
		MDagPath dagPath;
		MObject component;
		list.getDagPath( i, dagPath, component);
		MFnMesh mesh(dagPath.node(), &stat);
		if( !stat)
			continue;

		int pc = mesh.numPolygons();
		MIntArray data(pc, -1);

		std::string attrName = "shaderLayer";
		// ���������� ������
		MPlug plug = mesh.findPlug(attrName.c_str());
		if( !plug.isNull())
		{
			MObject val;
			plug.getValue(val);
			MFnIntArrayData intdata(val, &stat);
			if( stat && intdata.length()==pc)
			{
				data = intdata.array();
			}
			else
			{
				// ��������!!!
				mesh.removeAttribute(plug.attribute());
				plug = MPlug();
			}
		}

		if( component.isNull())
		{
			// ��� �������� ��������
			data = MIntArray(pc, (int)sl);
		}
		else
		{
			// �������� ���������
			MFnSingleIndexedComponent single(component);
			if( component.apiType() != MFn::kMeshPolygonComponent)
				continue;

			MIntArray selectedP;
			single.getElements( selectedP );
			for(int i=0; i<(int)selectedP.length(); i++)
			{
				int pi = selectedP[i];
				data[pi] = sl;
			}
		}
		MFnIntArrayData intdata;
		MObject val = intdata.create(data, &stat);

		// ��������
		if( plug.isNull())
		{
			// ������� �������
			MFnTypedAttribute typedAttr;
			MObject i_attr = typedAttr.create(attrName.c_str(), attrName.c_str(), MFnData::kIntArray, val);
			typedAttr.setConnectable(true);
			typedAttr.setStorable(true);
			typedAttr.setWritable(true);
			typedAttr.setReadable(true);
			typedAttr.setHidden(false);
			typedAttr.setKeyable(false);
			stat = mesh.addAttribute(i_attr);
		}
		else
		{
			// ��� ����
			stat = plug.setValue(val);
		}
	}

	return MS::kSuccess;
}

void* ocPolyShaderLayer::creator()
{
	return new ocPolyShaderLayer();
}

