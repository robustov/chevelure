#include "stdafx.h"
//
// Copyright (C) 
// File: ocFreezeVertsCmd.cpp
// MEL Command: ocFreezeVerts

#include "ocFreezeVerts.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFnPointArrayData.h>

ocFreezeVerts::ocFreezeVerts()
{
}
ocFreezeVerts::~ocFreezeVerts()
{
}

MSyntax ocFreezeVerts::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

//	syntax.addArg(MSyntax::kString);	// arg0
//	syntax.addFlag("f", "format", MSyntax::kString);
	syntax.useSelectionAsDefault(true);
	syntax.setObjectType( MSyntax::kSelectionList, 1 );

	return syntax;
}

MStatus ocFreezeVerts::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

//	MString argsl0;
//	argData.getCommandArgument(0, argsl0);

	MSelectionList list;
	argData.getObjects( list );

	for(int i=0; i<(int)list.length(); i++)
	{
		MObject component;
		MDagPath dagPath;
		if( !list.getDagPath( i, dagPath))
			continue;

		MFnNurbsCurve nurbs( dagPath.node(), &stat );
		if( stat) 
		{
			MPlug plug = nurbs.findPlug("Pfreeze");
			if(plug.isNull())
			{
				MFnTypedAttribute typedAttr;
				MObject a_freeze = typedAttr.create( "Pfreeze", "pfre", MFnData::kPointArray, &stat);
				nurbs.addAttribute(a_freeze);
				plug = MPlug(dagPath.node(), a_freeze);
			}
			MPointArray array;
			nurbs.getCVs(array);
			MFnPointArrayData data;
			MObject val = data.create(array);
			plug.setValue(val);
			continue;
		}
		MFnMesh mesh( dagPath.node(), &stat );
		if( stat) 
		{
			MPlug plug = mesh.findPlug("Pfreeze");
			if(plug.isNull())
			{
				MFnTypedAttribute typedAttr;
				MObject a_freeze = typedAttr.create( "Pfreeze", "pfre", MFnData::kPointArray, &stat);
				mesh.addAttribute(a_freeze);
				plug = MPlug(dagPath.node(), a_freeze);
			}

			MPointArray array;
			mesh.getPoints( array);
			MFnPointArrayData data;
			MObject val = data.create(array);
			plug.setValue(val);

			continue;
		}
	}
	return MS::kSuccess;
}

void* ocFreezeVerts::creator()
{
	return new ocFreezeVerts();
}

