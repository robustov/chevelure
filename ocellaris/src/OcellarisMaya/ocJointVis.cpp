#include "stdafx.h"
//
// Copyright (C) 
// File: ocJointVisCmd.cpp
// MEL Command: ocJointVis

#include "ocJointVis.h"
#include "ocellaris/MInstancePxData.h"
#include "ocellaris\renderOpenglImpl.h"

#include <maya/MGlobal.h>
#include <maya/MFnPluginData.h>
#include <maya/MFnUnitAttribute.h>

#include "mathNmaya/mathNmaya.h"
#include "math/Rotation3.h"

MObject ocJointVis::i_nodename;
MObject ocJointVis::i_time;
MObject ocJointVis::o_ocsCache;

ocJointVis::ocJointVis()
{
}
ocJointVis::~ocJointVis()
{
}

// �������� ��� �����
struct Bone
{
	MPoint pt0;
	MPoint pt1;
};
struct Joint
{
	Math::Matrix4f pos;
	std::string name;
};
void BuildListBoneRec(MDagPath dag, std::vector<Bone>& bones, std::vector<Joint>& joints, int level)
{
	MStatus stat;
	MMatrix m0 = dag.inclusiveMatrix();
	if( dag.node().hasFn(MFn::kJoint))
	{
		Joint j;
		::copy( j.pos, m0);
		j.name = dag.fullPathName().asChar();
		joints.push_back(j);
	}

	MPoint pt0 = MPoint(0,0,0)*m0;
	for( int i=0; i<(int)dag.childCount(); i++)
	{
		MObject obj = dag.child(i);
		MDagPath dagch;
		MDagPath::getAPathTo(obj, dagch);

		MMatrix m1 = dagch.inclusiveMatrix();
		MPoint pt1 = MPoint(0,0,0)*m1;
	
		if( obj.hasFn(MFn::kJoint) && dag.node().hasFn(MFn::kJoint))
		{
			// � ������ ������
			Bone bone;
			bone.pt0 = pt0;
			bone.pt1 = pt1;
			bones.push_back(bone);
//			displayStringD( dag.fullPathName().asChar());
		}
		// ������
		BuildListBoneRec(dagch, bones, joints, level+1);
	}
}

MStatus ocJointVis::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;

	if( plug == o_ocsCache )
	{
		MObject thisNode = thisMObject();

		data.inputValue( i_time).asTime();
		MString nodename = data.inputValue( i_nodename).asString();

		cls::MInstancePxData* pData = NULL;
		bool bNewData = false;
		MDataHandle outputData = data.outputValue( o_ocsCache, &stat );
		MPxData* userdata = outputData.asPluginData();
		pData = static_cast<cls::MInstancePxData*>(userdata);
		MFnPluginData fnDataCreator;
		if( !pData)
		{
			MTypeId tmpid( cls::MInstancePxData::id );
			MObject newDataObject = fnDataCreator.create( tmpid, &stat );
			pData = static_cast<cls::MInstancePxData*>(fnDataCreator.data( &stat ));
			bNewData = true;
		}
		cls::IRender* render = pData->drawcache.asRender();

		// �������� ��� �����
		std::vector<Bone> bones;
		std::vector<Joint> joints;
	
		MObject rootobj;
		nodeFromName(nodename, rootobj);

		MDagPath dag;
		MDagPath::getAPathTo(rootobj, dag);
		BuildListBoneRec(dag, bones, joints, 0);

		cls::PA<Math::Matrix4f> joint_pos(cls::PI_CONSTANT, joints.size());
		std::vector<std::string> joint_sname;
		int i;
		for(i=0; i<(int)joints.size(); i++)
		{
			Joint& j = joints[i];
			joint_pos[i] = j.pos;
			joint_sname.push_back(j.name);
		}
		cls::PSA joint_name(joint_sname);
		render->Attribute("JointPos", joint_pos);
		render->Attribute("JointName", joint_name);


		Math::Box3f cbox;
			
		cls::PA<Math::Vec3f> P(cls::PI_VERTEX, bones.size()*2, cls::PT_POINT);
		cls::PA<int> I(cls::PI_PRIMITIVE, bones.size());
		for(i=0; i<(int)bones.size(); i++)
		{
			Math::Vec3f p0, p1;
			::copy( p0, bones[i].pt0);
			::copy( p1, bones[i].pt1);
			cbox.insert(p0);
			cbox.insert(p1);

			P[i*2+0] = p0;
			P[i*2+1] = p1;
			I[i] = 2;
		}
		render->Parameter("P", P);
		render->Curves("linear", "nonperiodic", I);
		pData->drawcache.setLoaded(cbox);

		if( bNewData)
			outputData.set(pData);
		data.setClean(plug);
		return MS::kSuccess;
	}

	return MS::kUnknownParameter;
}

bool ocJointVis::isBounded() const
{
	MObject thisNode = thisMObject();
	return true;
}

MBoundingBox ocJointVis::boundingBox() const
{
	MObject thisNode = thisMObject();

	MPlug plug(thisNode, o_ocsCache);
	MObject val;
	plug.getValue(val);

	MBoundingBox box;

	MFnPluginData pd(val);
	cls::MInstancePxData* cache = (cls::MInstancePxData*)pd.data();
	if( !cache) 
		return box;

	Math::Box3f b = cache->drawcache.getBox();
	::copy(box, b);
	return box;
}

void ocJointVis::draw(
	M3dView &view, 
	const MDagPath &path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status)
{ 
	MObject thisNode = thisMObject();

	MPlug plug(thisNode, o_ocsCache);
	MObject val;
	plug.getValue(val);

	MFnPluginData pd(val);
	cls::MInstancePxData* cache = (cls::MInstancePxData*)pd.data();
	if( !cache) 
		return;


	view.beginGL(); 

//	cls::renderOpenglImpl glrender;
//	glrender.bPoints = false;
//	glrender.bWireFrame = true;
//	glrender.bShaded = false;
//	cls::IRender* render = &glrender;

	cache->drawcache.RenderToMaya(thisNode, &view, false, true, false);

	view.endGL();
};


void* ocJointVis::creator()
{
	return new ocJointVis();
}

MStatus ocJointVis::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnUnitAttribute	unitAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		{
			i_nodename = typedAttr.create( "nodename", "inn", MFnData::kString, &stat);
			::addAttribute(i_nodename, atInput);
		}
		{
			i_time = unitAttr.create( "time", "it", MTime(0.0), &stat);
			::addAttribute(i_time, atInput);
		}
		{
			o_ocsCache = typedAttr.create( "ocsCache", "occh", cls::MInstancePxData::id, MObject::kNullObj, &stat);
			::addAttribute(o_ocsCache, atWritable|atUnStorable|atCached);
			stat = attributeAffects( i_nodename, o_ocsCache );
			stat = attributeAffects( i_time, o_ocsCache );
		}
		if( !MGlobal::sourceFile("AEocJointVisTemplate.mel"))
		{
			displayString("error source AEocJointVisTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}