#include "stdafx.h"
#include "ocRSLshader.h"
#include "ocellaris/OcellarisExport.h"
#include "ocellaris/shaderAssemblerImpl.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnStringData.h>

#include <maya/MGlobal.h>

MObject ocRSLshader::i_type;
MObject ocRSLshader::i_outputParamName;
MObject ocRSLshader::i_headerText;
MObject ocRSLshader::i_bodyText;
MObject ocRSLshader::i_shaderAttrsList;

MObject ocRSLshader::i_input;
MObject ocRSLshader::i_UVCoord;
MObject ocRSLshader::o_color;
MObject ocRSLshader::o_alpha;

ocRSLshader::ocRSLshader()
{
}
ocRSLshader::~ocRSLshader()
{
}

MStatus ocRSLshader::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;

	if( plug == o_color || plug.parent() == o_color )
	{
		MDataHandle inputData = data.inputValue( i_input, &stat );

		float2& uv = data.inputValue( i_UVCoord ).asFloat2();

		MFloatVector resultColor(0, 0, 0);
		resultColor.x = uv[0] - floor( uv[0]);
		resultColor.y = uv[1] - floor( uv[1]);

		MDataHandle outputData = data.outputValue( o_color, &stat );
	    MFloatVector& outColor = outputData.asFloatVector();
	    outColor = resultColor;
	    outputData.setClean();

		return MS::kSuccess;
	}
	return MS::kUnknownParameter;
}

void* ocRSLshader::creator()
{
	return new ocRSLshader();
}

MStatus ocRSLshader::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnEnumAttribute	enumAttr;
	MStatus				stat;
	MFnStringData stringData;

	try
	{
		{
			i_type = enumAttr.create("type", "tp", (int)cls::IShadingNodeGenerator::COLOR);
			enumAttr.addField("SURFACESHADER", 	cls::IShadingNodeGenerator::SURFACESHADER); 	
//			enumAttr.addField("DISPLACEVECTOR", cls::IShadingNodeGenerator::DISPLACEVECTOR); 
			enumAttr.addField("DISPLACE",		cls::IShadingNodeGenerator::DISPLACE);
			enumAttr.addField("VOLUME",			cls::IShadingNodeGenerator::VOLUME);
			enumAttr.addField("LIGHT",			cls::IShadingNodeGenerator::LIGHT);
			enumAttr.addField("COLOR",			cls::IShadingNodeGenerator::COLOR); 
			enumAttr.addField("FLOAT",			cls::IShadingNodeGenerator::FLOAT); 
			enumAttr.addField("VECTOR",			cls::IShadingNodeGenerator::VECTOR); 
			enumAttr.addField("NORMAL",			cls::IShadingNodeGenerator::NORMAL); 
			enumAttr.addField("MATRIX",			cls::IShadingNodeGenerator::MATRIX); 
			enumAttr.addField("MANIFOLD",		cls::IShadingNodeGenerator::MANIFOLD); 
			::addAttribute(i_type, atInput);
		}
		{
			i_outputParamName = typedAttr.create( "outputParamName", "opn",
				MFnData::kString, stringData.create("result", &stat), &stat );
			::addAttribute(i_outputParamName, atInput);
		}
		{
			i_headerText = typedAttr.create( "headerText", "ht",
				MFnData::kString, stringData.create("", &stat), &stat );
			::addAttribute(i_headerText, atInput|atHidden);
		}
		{
			i_bodyText = typedAttr.create( "bodyText", "bt",
				MFnData::kString, stringData.create("result = color(1, 1, 1);", &stat), &stat );
			::addAttribute(i_bodyText, atInput|atHidden);
		}
		{
			i_shaderAttrsList = typedAttr.create( "shaderAttrsList", "shaderAttrsList", MFnData::kStringArray, &stat);
//			typedAttr.setInternal(true);
			::addAttribute(i_shaderAttrsList, atInput|atHidden);
		}

		{
			i_input = numAttr.create ("i_input","iin", MFnNumericData::kDouble, 0.01, &stat);
			numAttr.setMin(0.0);	
			numAttr.setMax(1.0);	
			::addAttribute(i_input, atInput|atHidden);
		}
		{
			MObject child1 = numAttr.create( "uCoord", "u", MFnNumericData::kFloat);
			MObject child2 = numAttr.create( "vCoord", "v", MFnNumericData::kFloat);
			i_UVCoord = numAttr.create( "uvCoord", "uv", child1, child2);
			::addAttribute(i_UVCoord, atInput|atHidden);
		}
		{
			o_color = numAttr.createColor( "outColor", "oc");
			numAttr.setDisconnectBehavior(MFnAttribute::kReset);
			numAttr.setUsedAsColor(false);
			::addAttribute(o_color, atReadable|atUnWritable|atConnectable|atUnStorable|atUnCached);

			stat = attributeAffects( i_input, o_color );
			stat = attributeAffects( i_UVCoord, o_color );
		}
		{
			o_alpha = numAttr.create( "outAlpha", "oa", MFnNumericData::kDouble, 0);
			::addAttribute(o_alpha, atReadable|atUnWritable|atConnectable|atUnStorable|atUnCached);

			stat = attributeAffects( i_input, o_alpha);
			stat = attributeAffects( i_UVCoord, o_alpha);
		}
		if( !MGlobal::sourceFile("AEocRSLshaderTemplate.mel"))
		{
			displayString("error source AEocRSLshaderTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

void ocRSLshader::updateShader(MObject node)
{
	cls::shaderAssemblerImpl as;
	ocRSLshader::generator.BuildShader(node, cls::PT_UNKNOWN, &as, NULL, MObject::kNullObj);
}

ocRSLshader::Generator ocRSLshader::generator;

// ��� ����
cls::IShadingNodeGenerator::enShadingNodeType ocRSLshader::Generator::getShadingNodeType(
	MObject& node					//!< ������ (shader)
	)
{
	int type;
	MPlug(node, ocRSLshader::i_type).getValue(type);
	return (cls::IShadingNodeGenerator::enShadingNodeType)type;
}

// �������� ��������� �������� �� ������
// ���� [ (interp) type attrName(=value)] (�������� [ uniform float kDiffuse])
// ������ ���������� �����
std::string ParseAttributes(
	MObject node, 
	cls::IShaderAssembler* as,		//!< render
	cls::IExportContext* context,	//!< context
	const char* text, 
	MStringArray& inputAttributes
	)
{
	std::string body;
	int length = strlen(text);
	body.reserve(length);

	// ������ [float attrName] ([type attrName])
	bool insidebraket = false;
	std::string currentAttr;
	for(int i=0, k=0; i<length; i++)
	{
		char C = text[i];
		if(!insidebraket)
		{
			if( C == '[')
			{
				insidebraket = true;
				currentAttr.clear();
			}
			else
			{
				body+=C;
			}
		}
		else
		{
			// ������ �������
			if( C == ']')
			{
				// ������� currentAttr
				std::list<std::string> list;
				std::string prop = currentAttr;
				char* substr = strtok((char*)prop.c_str(), " \t=");
				for(; substr; substr = strtok(NULL, " \t="))
					list.push_back(substr);

				if( list.empty())
				{
					if( context)
						context->error("ocRSLshader:: PARSE attribute error \"%s\"\n", currentAttr.c_str());
					continue;
				}
				// Extern
				//////////////////////////////////////
				bool bExtern = false;
				if(list.front()=="extern")
				{
					bExtern = true;
					list.pop_front();
				}


				if( list.empty())
				{
					if( context)
						context->error("ocRSLshader:: PARSE attribute error \"%s\"\n", currentAttr.c_str());
					continue;
				}
				//////////////////////////////////////
				// Interpolation
				cls::enParamInterpolation interp = cls::PI_VARYING;
				{
					interp = cls::interpolation_fromstr(list.front().c_str());
					if( interp!=cls::PI_ERROR)
						list.pop_front();
					else
						interp = cls::PI_VARYING;
				}

				//////////////////////////////////////
				// Type
				if( list.empty())
				{
					if( context)
						context->error("ocRSLshader:: PARSE attribute error \"%s\"\n", currentAttr.c_str());
					continue;
				}
				cls::enParamType type = cls::PT_FLOAT;
				{
					type = cls::type_fromstr(list.front().c_str());
					if( type==cls::PT_UNKNOWN)
					{
						if( context)
							context->error("ocRSLshader:: PARSE attribute error undefined type \"%s\"\n", currentAttr.c_str());
						continue;
					}
					list.pop_front();
				}

				//////////////////////////////////////
				// name
				std::string name = "";
				{
					if( list.empty())
					{
						if( context)
							context->error("ocRSLshader:: PARSE attribute error \"%s\"\n", currentAttr.c_str());
						continue;
					}
					name = list.front();
					list.pop_front();
				}

				//////////////////////////////////////
				// 
				cls::Param defval;
				if( !list.empty())
				{
					float v = (float)atof( list.front().c_str());
					defval = cls::P<float>(v);
					list.pop_front();
				}


				body+=name;

				if( bExtern)
				{
					as->ExternParameter( name.c_str(), defval, type, interp);
				}
				else
				{
					ocExport_AddAttrType(
						node, 
						name.c_str(), 
						type, 
						defval
						);
					as->InputParameter( name.c_str(), name.c_str(), type, interp);

					inputAttributes.append(cls::str_type(type));
					inputAttributes.append(name.c_str());
				}
				insidebraket = false;
			}
			else
			{
				currentAttr += C;
			}
		}
	}
int bs = body.size();
int bl = strlen( body.c_str());
	return body;
}

//! ��������� ��������� 
//! ��������� ��������� ��������� ��� �������
bool ocRSLshader::Generator::BuildShader(
	MObject& node,					//!< ������
	cls::enParamType wantedoutputtype, //!< ����� ��� ����� �� ������
	cls::IShaderAssembler* as,		//!< render
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MFnDependencyNode dn(node);
	int _type;
	MString outputParamName;
	MString headerText;
	MString bodyText;
	MStringArray inputAttributes;

	MPlug(node, ocRSLshader::i_type).getValue(_type);
	MPlug(node, ocRSLshader::i_outputParamName).getValue(outputParamName);
	MPlug(node, ocRSLshader::i_headerText).getValue(headerText);
	MPlug(node, ocRSLshader::i_bodyText).getValue(bodyText);
	cls::IShadingNodeGenerator::enShadingNodeType type = (cls::IShadingNodeGenerator::enShadingNodeType)_type;

	cls::enParamType paramType = cls::shaderType2paramType(type);

	// �������� ��������� �������� �� bodyText
	std::string body = ParseAttributes(node, as, context, bodyText.asChar(), inputAttributes);

	as->OutputParameter(outputParamName.asChar(), paramType);

	as->Include(headerText.asChar());
	as->Add(body.c_str());

	MFnStringArrayData sad;
	MObject alval = sad.create(inputAttributes);
	MPlug(node, ocRSLshader::i_shaderAttrsList).setValue(alval);
	return true;
}


