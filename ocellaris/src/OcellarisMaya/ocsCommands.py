#import ocsCommands
#reload (ocsCommands)
#print ocsCommands.clear()
#reload (ocsCommands)
#print ocsCommands.getenv("sasas")
#print ocsCommands.getenv("sasas")

from ctypes import *
from ctypes.wintypes import *
from _ctypes import LoadLibrary, FreeLibrary
import maya.cmds as cmd 
import maya.OpenMaya as OpenMaya

def clear():
 cmd.loadPlugin("ocellarisMaya.mll", quiet=True);
 handle = cdll.LoadLibrary('ocellarisMaya.mll');
 try:
  prototype = CFUNCTYPE(c_int)
  ocs_clear = prototype(("ocs_clear", handle))
  result = ocs_clear()
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle);

def getenv(env):
 cmd.loadPlugin("ocellarisMaya.mll", quiet=True);
 handle = cdll.LoadLibrary('ocellarisMaya.mll');
 try:
  prototype1 = CFUNCTYPE(c_char_p, c_char_p)
  ocs_getenv = prototype1(("ocs_getenv", handle))
  result = ocs_getenv(env)
  resulttype = c_char_p(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle)

def setenv(env, val):
 cmd.loadPlugin("ocellarisMaya.mll", quiet=True);
 handle = cdll.LoadLibrary('ocellarisMaya.mll');
 try:
  prototype1 = CFUNCTYPE(c_int, c_char_p, c_char_p)
  ocs_setenv = prototype1(("ocs_setenv", handle))
  result = ocs_setenv(env, val)
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle)
  
def addDefaultGenerators(options=""):
 cmd.loadPlugin("ocellarisMaya.mll", quiet=True);
 handle = cdll.LoadLibrary('ocellarisMaya.mll');
 try:
  prototype = CFUNCTYPE(c_int, c_char_p)
  ocs_addDefaultGenerators = prototype(("ocs_addDefaultGenerators", handle))
  result = ocs_addDefaultGenerators(options)
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle)
  
def addGeneratorForType(type, generator):
 cmd.loadPlugin("ocellarisMaya.mll", quiet=True);
 handle = cdll.LoadLibrary('ocellarisMaya.mll');
 try:
  prototype = CFUNCTYPE(c_int, c_char_p, c_char_p)
  ocs_addGeneratorForType = prototype(("ocs_addGeneratorForType", handle))
  result = ocs_addGeneratorForType(type, generator)
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle)
  
def addnodes(node="", options=""):
 cmd.loadPlugin("ocellarisMaya.mll", quiet=True);
 handle = cdll.LoadLibrary('ocellarisMaya.mll');
 try:
  prototype = CFUNCTYPE(c_int, c_char_p, c_char_p)
  ocs_addnodes = prototype(("ocs_addnodes", handle))
  result = ocs_addnodes(node, options)
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle)

def addpass(node="", options=""):
 cmd.loadPlugin("ocellarisMaya.mll", quiet=True);
 handle = cdll.LoadLibrary('ocellarisMaya.mll');
 try:
  prototype = CFUNCTYPE(c_int, c_char_p, c_char_p)
  ocs_addpass = prototype(("ocs_addpass", handle))
  result = ocs_addpass(node, options)
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle)

def addpassgenerator(gen="", options=""):
 cmd.loadPlugin("ocellarisMaya.mll", quiet=True);
 handle = cdll.LoadLibrary('ocellarisMaya.mll');
 try:
  prototype = CFUNCTYPE(c_int, c_char_p, c_char_p)
  ocs_addpassgenerator = prototype(("ocs_addpassgenerator", handle))
  result = ocs_addpassgenerator(gen, options)
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle)

def connectpass( mainpass, subpass, options=""):
 cmd.loadPlugin("ocellarisMaya.mll", quiet=True);
 handle = cdll.LoadLibrary('ocellarisMaya.mll');
 try:
  prototype = CFUNCTYPE(c_int, c_char_p, c_char_p, c_char_p)
  ocs_connectpass = prototype(("ocs_connectpass", handle))
  result = ocs_connectpass(mainpass, subpass, options)
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle)

def addlight(node="", options=""):
 cmd.loadPlugin("ocellarisMaya.mll", quiet=True);
 handle = cdll.LoadLibrary('ocellarisMaya.mll');
 try:
  prototype = CFUNCTYPE(c_int, c_char_p, c_char_p)
  ocs_addlight = prototype(("ocs_addlight", handle))
  result = ocs_addlight(node, options)
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle)

def addframe(time):
 cmd.loadPlugin("ocellarisMaya.mll", quiet=True);
 handle = cdll.LoadLibrary('ocellarisMaya.mll');
 try:
  prototype = CFUNCTYPE(c_int, c_float)
  ocs_addframe = prototype(("ocs_addframe", handle))
  result = ocs_addframe(time)
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle)


def preparegenerators():
 cmd.loadPlugin("ocellarisMaya.mll", quiet=True);
 handle = cdll.LoadLibrary('ocellarisMaya.mll');
 try:
  prototype = CFUNCTYPE(c_int)
  ocs_preparegenerators = prototype(("ocs_preparegenerators", handle))
  result = ocs_preparegenerators()
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle)

def preparenodes(progresstitle="", progresstext=""):
 cmd.loadPlugin("ocellarisMaya.mll", quiet=True);
 handle = cdll.LoadLibrary('ocellarisMaya.mll');
 try:
  prototype = CFUNCTYPE(c_int, c_char_p, c_char_p)
  ocs_preparenodes = prototype(("ocs_preparenodes", handle))
  result = ocs_preparenodes(progresstitle, progresstext)
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle)

def export(dumpoption="", options="", progresstitle="", progresstext=""):
 cmd.loadPlugin("ocellarisMaya.mll", quiet=True);
 handle = cdll.LoadLibrary('ocellarisMaya.mll');
 try:
  prototype = CFUNCTYPE(c_int, c_char_p, c_char_p, c_char_p, c_char_p)
  ocs_export = prototype(("ocs_export", handle))
  result = ocs_export(dumpoption, options, progresstitle, progresstext)
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle)

def render(dumpoption=""):
 cmd.loadPlugin("ocellarisMaya.mll", quiet=True);
 handle = cdll.LoadLibrary('ocellarisMaya.mll');
 try:
  prototype = CFUNCTYPE(c_int, c_char_p)
  ocs_render = prototype(("ocs_render", handle))
  result = ocs_render(dumpoption)
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle)

def distalfred(options, mayafilename, workspace):
 cmd.loadPlugin("ocellarisMaya.mll", quiet=True);
 handle = cdll.LoadLibrary('ocellarisMaya.mll');
 try:
  prototype = CFUNCTYPE(c_int, c_char_p, c_char_p, c_char_p)
  ocs_distalfred = prototype(("ocs_distalfred", handle))
  result = ocs_distalfred(options, mayafilename, workspace)
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle)

def fastdiststart(option):
 cmd.loadPlugin("ocellarisMaya.mll", quiet=True);
 handle = cdll.LoadLibrary('ocellarisMaya.mll');
 try:
  prototype = CFUNCTYPE(c_int, c_char_p)
  ocs_proc = prototype(("ocs_fastdiststart", handle))
  result = ocs_proc(option)
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle)

def fastdiststart_addframe(frame):
 cmd.loadPlugin("ocellarisMaya.mll", quiet=True);
 handle = cdll.LoadLibrary('ocellarisMaya.mll');
 try:
  prototype = CFUNCTYPE(c_int, c_float)
  ocs_proc = prototype(("ocs_fastdiststart_addframe", handle))
  result = ocs_proc(frame)
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle)


def dump(options=""):
 cmd.loadPlugin("ocellarisMaya.mll", quiet=True);
 handle = cdll.LoadLibrary('ocellarisMaya.mll');
 try:
  prototype = CFUNCTYPE(c_int, c_char_p)
  ocs_dump = prototype(("ocs_dump", handle))
  result = ocs_dump(options)
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle)




def test(path):
 cmd.loadPlugin("ocellarisMaya.mll", quiet=True);
 handle = cdll.LoadLibrary('ocellarisMaya.mll');
 try:
  result = handle.ocs_test(path)
#  prototype1 = CFUNCTYPE(c_int, POINTER(OpenMaya.MDagPath))
#  ocs_test = prototype1(("ocs_test", handle))
#  result = ocs_test(POINTER(path))
  resulttype = c_int(result)
  return resulttype.value;
 finally:
#  print "FreeLibrary"
  FreeLibrary(handle._handle)

