#pragma once
#include "pre.h"
#include "ocellaris/MInstancePxData.h"

// ��� - ��������� (min,max)
struct interval
{
	double min, max;
	interval(double min=-1e38, double max=1e38)
	{
		this->min = min;
		this->max = max;
	}
	interval(const interval& arg)
	{
		this->min = arg.min;
		this->max = arg.max;
	}
	interval& operator=(const interval& arg)
	{
		this->min = arg.min;
		this->max = arg.max;
		return *this;
	}
};

struct PhaseData
{
	MObject phase;
	std::string name;
	interval interval;
	MMatrix matrix;
	MBoundingBox box;

	PhaseData(){cache=0; frame=0;}

	cls::MInstancePxData* cache;
	int frame;

	MPoint getBoxCorner(int i);
};
struct InstancerPhases
{
//	std::string defaultShader;
	MObject psobj;

	std::vector<PhaseData> phases;
	std::map<std::string, int> phasenames;

	MStatus LoadPhases(
		MObject instancer, 
		MObject psobj, 
		bool bDump);

	PhaseData& getPhase(int index){return phases[index];}
	int findPhase(std::string& name);

protected:
	void findPhasesHierarchy(
		MDagPath& path, 
		interval inter);

};

