#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ocCustomPassNode.h
//
// Dependency Graph Node: ocCustomPass

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 

class ocCustomPass : public MPxNode
{
public:
	ocCustomPass();
	virtual ~ocCustomPass(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_parent;
//	static MObject i_passgenerator;//string 

	// set
	static MObject i_setname;//string 
	// ��� ������
	static MObject i_cameraname;//string 
	static MObject i_resolution;//int [2]
	static MObject i_format_ratio;// double "pixelRatio"

	// �����
//	static MObject i_dspyGain;// float
//	static MObject i_dspyGamma;// float
//	static MObject i_pixelsample;// float 2
//	static MObject i_pixelfilter;// int[2]
//	static MObject i_shadingrate;// float
//	static MObject i_filtername;
//	static MObject i_outputfilename;

	// display
//	static MObject i_displayname;
//	static MObject i_displaytype;
//	static MObject i_displaymode;
	// quantize
//	static MObject i_quantizeMode;
//	static MObject i_quantizeOne;
//	static MObject i_quantizeMax;		
//	static MObject i_quantizeMin;		
//	static MObject i_quantizeDither;

	// ���������
//	static MObject i_secStartFrame;
//	static MObject i_secEndFrame;
//	static MObject i_secEachFrame;

	// blur
//	static MObject i_ocsBlur;
//	static MObject i_ocsShutterAngle;
//	static MObject i_ocsShutterTiming;
//	static MObject i_subframeMotion;
	// ...
	// crop
//	static MObject i_crop;
//	static MObject i_cropXY;

	// ����
//	static MObject i_autoshadows;

	// trace
//	static MObject i_trace[10];

	// ���������
	static MObject i_ocsAscii;
	static MObject i_outputOptions;
	static MObject i_beforeScene;
	static MObject i_forChildPasses;
	static MObject i_postRenderTask;

public:
	static MTypeId id;
	static const MString typeName;
};

