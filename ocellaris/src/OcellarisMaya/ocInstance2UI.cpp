#include "stdafx.h"
#include "ocInstance.h"

#ifndef USE_LOCATOR_INSTANCE

#include "ocInstance2UI.h"

#include <maya/MIOStream.h>
#include <maya/MMaterial.h>
#include <maya/MDrawData.h>
#include <maya/MDrawRequest.h>
#include <maya/M3dView.h>
#include <maya/MMatrix.h>
#include <maya/MFnSingleIndexedComponent.h>
#include <maya/MObjectArray.h>
#include <maya/MDagPath.h>
#include "mathNgl\mathNgl.h"

// Object and component color defines
//
#define LEAD_COLOR				18	// green
#define ACTIVE_COLOR			15	// white
#define ACTIVE_AFFECTED_COLOR	8	// purple
#define DORMANT_COLOR			4	// blue
#define HILITE_COLOR			17	// pale blue
#define DORMANT_VERTEX_COLOR	8	// purple
#define ACTIVE_VERTEX_COLOR		16	// yellow

// Vertex point size
//
#define POINT_SIZE				2.0	

////////////////////////////////////////////////////////////////////////////////
//
// UI implementation
//
////////////////////////////////////////////////////////////////////////////////

ocInstance2UI::ocInstance2UI() {}
ocInstance2UI::~ocInstance2UI() {}

void* ocInstance2UI::creator()
{
	return new ocInstance2UI();
}

///////////////////////////////////////////////////////////////////////////////
//
// Overrides
//
///////////////////////////////////////////////////////////////////////////////

void ocInstance2UI::getDrawRequests( 
	const MDrawInfo & info,
	bool objectAndActiveOnly,
	MDrawRequestQueue & queue 
	)
{
//	displayStringD("ocInstance2UI::getDrawRequests()");

	// Get the data necessary to draw the shape
	//
	MDrawData data;
	ocInstance* shapeNode = (ocInstance*)surfaceShape();
	ocInstance2Geometry* geom = shapeNode->getGeometry();

	MDrawRequest request = info.getPrototype( *this );
	getDrawData( geom, data );
	request.setDrawData( data );

	M3dView::DisplayStyle  appearance    = info.displayStyle();
	M3dView::DisplayStatus displayStatus = info.displayStatus();
	request.setDisplayStyle( appearance);
	switch ( appearance )
	{
		case M3dView::kPoints:
		case M3dView::kBoundingBox:
		case M3dView::kWireFrame:
		{
//			request.setToken( appearance );

			M3dView::ColorTable activeColorTable = M3dView::kActiveColors;
			M3dView::ColorTable dormantColorTable = M3dView::kDormantColors;

			switch ( displayStatus )
			{
				case M3dView::kLead :
					request.setColor( LEAD_COLOR, activeColorTable );
					break;
				case M3dView::kActive :
					request.setColor( ACTIVE_COLOR, activeColorTable );
					break;
				case M3dView::kActiveAffected :
					request.setColor( ACTIVE_AFFECTED_COLOR, activeColorTable );
					break;
				case M3dView::kDormant :
					request.setColor( DORMANT_COLOR, dormantColorTable );
					break;
				case M3dView::kHilite :
					request.setColor( HILITE_COLOR, activeColorTable );
					break;
				default:	
					break;
			}

			queue.add( request );

			break;
		}

		case M3dView::kGouraudShaded:
		case M3dView::kFlatShaded:
		{
			/////////////////////////////////////////////////////
			// Create the smooth shaded draw request
//			request.setToken( appearance );

			/////////////////////////////////////////////////////
			// Need to get the material info
			MDagPath path = info.multiPath();	// path to your dag object 
			M3dView view = info.view();; 		// view to draw to
			MMaterial material = MPxSurfaceShapeUI::material( path );

			/////////////////////////////////////////////////////
			// Evaluate the material and if necessary, the texture.
			if ( ! material.evaluateMaterial( view, path ) ) 
				cerr << "Couldnt evaluate evaluateMaterial\n";

			bool drawTexture = true;
			if ( drawTexture && material.materialIsTextured() ) 
				material.evaluateTexture( data );

			request.setMaterial( material );

			/////////////////////////////////////////////////////
			// Transparent
			bool materialTransparent = false;
			material.getHasTransparency( materialTransparent );
			if ( materialTransparent ) 
				request.setIsTransparent( true );

			queue.add( request );

			// create a draw request for wireframe on shaded if
			// necessary.
			//
			if ( (displayStatus == M3dView::kActive) ||
				 (displayStatus == M3dView::kLead) ||
				 (displayStatus == M3dView::kHilite) )
			{
				MDrawRequest wireRequest = info.getPrototype( *this );
				wireRequest.setDrawData( data );
//				wireRequest.setToken( kDrawWireframeOnShaded );
				wireRequest.setDisplayStyle( M3dView::kWireFrame );

				M3dView::ColorTable activeColorTable = M3dView::kActiveColors;

				switch ( displayStatus )
				{
					case M3dView::kLead :
						wireRequest.setColor( LEAD_COLOR, activeColorTable );
						break;
					case M3dView::kActive :
						wireRequest.setColor( ACTIVE_COLOR, activeColorTable );
						break;
					case M3dView::kHilite :
						wireRequest.setColor( HILITE_COLOR, activeColorTable );
						break;
					default:	
						break;

				}
				queue.add( wireRequest );
			}
			break;
		}
		default:	
			break;
	}

}

void ocInstance2UI::draw( 
	const MDrawRequest& request, 
	M3dView& view ) const
{ 
	M3dView::DisplayStyle token = request.displayStyle();
	switch( token )
	{
		case M3dView::kBoundingBox:
			drawBoundingBox( request, view );
			break;
		case M3dView::kWireFrame:
			drawWireframe( request, view );
			break;
		case M3dView::kFlatShaded:
		case M3dView::kGouraudShaded:
			drawShaded( request, view );
			break;
		case M3dView::kPoints:
			drawVertices( request, view );
			break;
	}
}
void ocInstance2UI::drawBoundingBox( 
	const MDrawRequest & request,
	M3dView & view 
	) const
{
	MDrawData data = request.drawData();
	ocInstance2Geometry * geom = (ocInstance2Geometry*)data.geometry();

	view.beginGL(); 
	// Draw the wireframe mesh
	{
		Math::Box3f defbox;
		defbox.min = -Math::Vec3f(0.5f, 0.5f, 0.5f);
		defbox.max = Math::Vec3f(0.5f, 0.5f, 0.5f);
		if( !geom->cache || !geom->cache->drawcache.isValid()) 
		{
			drawBox(defbox);
		}
		else
		{
			defbox = geom->cache->drawcache.getBox();
			drawBox(defbox);
		}
	}
	view.endGL(); 
}

bool ocInstance2UI::select( 
	MSelectInfo &selectInfo, 
	MSelectionList &selectionList,
	MPointArray &worldSpaceSelectPts 
	) const
{
	displayStringD("ocInstance2UI::select()");

	bool selected = false;
	bool componentSelected = false;
	bool hilited = false;

	if ( !selected ) 
	{
		ocInstance* shapeNode = (ocInstance*)surfaceShape();

		// NOTE: If the geometry has an intersect routine it should
		// be called here with the selection ray to determine if the
		// the object was selected.

		selected = true;
		MSelectionMask priorityMask( MSelectionMask::kSelectNurbsSurfaces );
		MSelectionList item;
		item.add( selectInfo.selectPath() );
		MPoint xformedPt;
		if ( selectInfo.singleSelection() ) 
		{
			MPoint center = shapeNode->boundingBox().center();
			xformedPt = center;
			xformedPt *= selectInfo.selectPath().inclusiveMatrix();
		}

		selectInfo.addSelection( 
			item, xformedPt, selectionList,
			worldSpaceSelectPts, priorityMask, false );
	}

	return selected;
}

///////////////////////////////////////////////////////////////////////////////
//
// Helper routines
//
///////////////////////////////////////////////////////////////////////////////

void ocInstance2UI::drawWireframe( 
	const MDrawRequest & request,
	M3dView & view 
	) const
{
	MDrawData data = request.drawData();
	ocInstance2Geometry * geom = (ocInstance2Geometry*)data.geometry();

//	int token = request.token();
	M3dView::DisplayStyle token = request.displayStyle();

	bool wireFrameOnShaded = false;
//	if ( kDrawWireframeOnShaded == token ) 
//		wireFrameOnShaded = true;

	view.beginGL(); 

	// Query current state so it can be restored
	bool lightingWasOn = glIsEnabled( GL_LIGHTING ) ? true : false;
	if ( lightingWasOn ) glDisable( GL_LIGHTING );
	if ( wireFrameOnShaded ) glDepthMask( false );

	// Draw the wireframe mesh
	{
		Math::Box3f defbox;
		defbox.min = -Math::Vec3f(0.5f, 0.5f, 0.5f);
		defbox.max = Math::Vec3f(0.5f, 0.5f, 0.5f);
		if( !geom->cache || !geom->cache->drawcache.isValid()) 
		{
			drawBox(defbox);
		}
		else
		{
			MObject obj = surfaceShape()->thisMObject();
			if( geom->matricies.empty())
			{
				geom->cache->drawcache.RenderToMaya(obj, &view, false, true, false);
			}
			else
			{
				for(int i=0; i<(int)geom->matricies.size(); i++)
				{
					Math::Matrix4f pos = geom->matricies[i];
					float* d = pos.data();
					glPushMatrix();
					glMultMatrixf(d);
					geom->cache->drawcache.RenderToMaya(obj, &view, false, true, false);
					glPopMatrix();
				}
			}
		}
	}

	// Restore the state
	//
	if( lightingWasOn) glEnable( GL_LIGHTING );
	if( wireFrameOnShaded) glDepthMask( true );

	view.endGL(); 
}

void ocInstance2UI::drawShaded( 
	const MDrawRequest & request,
	M3dView & view 
	) const
{
	MDrawData data = request.drawData();
	ocInstance2Geometry * geom = (ocInstance2Geometry*)data.geometry();
//	int token = request.token();
	M3dView::DisplayStyle token = request.displayStyle();

	view.beginGL(); 

#if		defined(SGI) || defined(MESA)
	glEnable( GL_POLYGON_OFFSET_EXT );
#else
	glEnable( GL_POLYGON_OFFSET_FILL );
#endif

	// Set up the material
	//
	MMaterial material = request.material();
	material.setMaterial( request.multiPath(), request.isTransparent() );

	int shadeModel;
	glGetIntegerv( GL_SHADE_MODEL, &shadeModel);
	glShadeModel(token==M3dView::kFlatShaded?GL_FLAT:GL_SMOOTH);
	// Enable texturing
	//
	bool drawTexture = material.materialIsTextured();
//	if( token==M3dView::kFlatShaded) drawTexture = false;
	if ( drawTexture ) 
		glEnable(GL_TEXTURE_2D);

	// Apply the texture to the current view
	//
	if ( drawTexture )
		material.applyTexture( view, data );

	// Draw the wireframe mesh
	{
		Math::Box3f defbox;
		defbox.min = -Math::Vec3f(0.5f, 0.5f, 0.5f);
		defbox.max = Math::Vec3f(0.5f, 0.5f, 0.5f);
		if( !geom->cache || !geom->cache->drawcache.isValid()) 
		{
			drawBox(defbox);
		}
		else
		{
			MObject obj = surfaceShape()->thisMObject();
			if( geom->matricies.empty())
			{
				geom->cache->drawcache.RenderToMaya(obj, &view, false, false, true);
			}
			else
			{
				for(int i=0; i<(int)geom->matricies.size(); i++)
				{
					Math::Matrix4f pos = geom->matricies[i];
					float* d = pos.data();
					glPushMatrix();
					glMultMatrixf(d);
					geom->cache->drawcache.RenderToMaya(obj, &view, false, false, true);
					glPopMatrix();
				}
			}
		}
	}

	// Turn off texture mode
	//
	if ( drawTexture ) glDisable(GL_TEXTURE_2D);

	glShadeModel(shadeModel);

	view.endGL(); 

}

void ocInstance2UI::drawVertices( 
	const MDrawRequest & request,
	M3dView & view 
	) const
{
	MDrawData data = request.drawData();
	ocInstance2Geometry * geom = (ocInstance2Geometry*)data.geometry();

	view.beginGL(); 

	// Query current state so it can be restored
	//
	bool lightingWasOn = glIsEnabled( GL_LIGHTING ) ? true : false;
	if ( lightingWasOn ) glDisable( GL_LIGHTING );
	
	float lastPointSize;
	glGetFloatv( GL_POINT_SIZE, &lastPointSize );

	// Draw the wireframe mesh
	{
		Math::Box3f defbox;
		defbox.min = -Math::Vec3f(0.5f, 0.5f, 0.5f);
		defbox.max = Math::Vec3f(0.5f, 0.5f, 0.5f);
		if( !geom->cache || !geom->cache->drawcache.isValid()) 
		{
			drawBox(defbox);
		}
		else
		{
			MObject obj = surfaceShape()->thisMObject();
			geom->cache->drawcache.RenderToMaya(obj, &view, true, false, false);
		}
	}

	// Restore the state
	//
	if ( lightingWasOn ) 
		glEnable( GL_LIGHTING );

	glPointSize( lastPointSize );

	view.endGL(); 
}

bool ocInstance2UI::selectVertices( 
	MSelectInfo &selectInfo,
	MSelectionList &selectionList,
	MPointArray &worldSpaceSelectPts 
	) const
{
	bool selected = false;
	M3dView view = selectInfo.view();

	MPoint 		xformedPoint;
	MPoint 		currentPoint;
	MPoint 		selectionPoint;
	double		previousZ = 0.0;
 	int			closestPointVertexIndex = -1;

	const MDagPath& path = selectInfo.multiPath();

	// Create a component that will store the selected vertices
	//
	MFnSingleIndexedComponent fnComponent;
	MObject surfaceComponent = fnComponent.create( MFn::kMeshVertComponent );

	// if the user did a single mouse click and we find > 1 selection
	// we will use the alignmentMatrix to find out which is the closest
	//
	MMatrix	alignmentMatrix;
	MPoint singlePoint; 
	bool singleSelection = selectInfo.singleSelection();
	if( singleSelection ) 
		alignmentMatrix = selectInfo.getAlignmentMatrix();

	// Get the geometry information
	ocInstance* shapeNode = (ocInstance*)surfaceShape();
	ocInstance2Geometry* geom = shapeNode->getGeometry();

	// Loop through all vertices of the mesh and
	// see if they lie withing the selection area
//	int numVertices = geom->vertices.length();
//	for ( vertexIndex=0; vertexIndex<numVertices; vertexIndex++ )
	{
/*/
		MPoint currentPoint = geom->vertices[ vertexIndex ];

		// Sets OpenGL's render mode to select and stores
		// selected items in a pick buffer
		//
		view.beginSelect();

		glBegin( GL_POINTS );
		glVertex3f( (float)currentPoint[0], 
					(float)currentPoint[1], 
					(float)currentPoint[2] );
		glEnd();

		if ( view.endSelect() > 0 )	// Hit count > 0
		{
			selected = true;

			if ( singleSelection ) {
				xformedPoint = currentPoint;
				xformedPoint.homogenize();
				xformedPoint*= alignmentMatrix;
				z = xformedPoint.z;
				if ( closestPointVertexIndex < 0 || z > previousZ ) {
					closestPointVertexIndex = vertexIndex;
					singlePoint = currentPoint;
					previousZ = z;
				}
			} else {
				// multiple selection, store all elements
				//
				fnComponent.addElement( vertexIndex );
			}
		}
/*/
	}

	// If single selection, insert the closest point into the array
	if ( selected && selectInfo.singleSelection() ) 
	{
		fnComponent.addElement(closestPointVertexIndex);

		// need to get world space position for this vertex
		selectionPoint = singlePoint;
		selectionPoint *= path.inclusiveMatrix();
	}

	// Add the selected component to the selection list
	if ( selected ) 
	{
		MSelectionList selectionItem;
		selectionItem.add( path, surfaceComponent );

		MSelectionMask mask( MSelectionMask::kSelectComponentsMask );
		selectInfo.addSelection(
			selectionItem, selectionPoint,
			selectionList, worldSpaceSelectPts,
			mask, true );
	}

	return selected;
}

#endif