#include "stdafx.h"
#include "pre.h"
#include "Util/misc_create_directory.h"

#include <maya/MFnPlugin.h>
#include "ocExport.h"
#include "ocInstance.h"
#include "ocellaris/MInstancePxData.h"
#include "ocTranslator.h"
#include "ocShaderParameter.h"
#include "ocStartPrman.h"
#include "sloTranslator.h"
#include "ocParticleAttribute.h"
#include "ocScanShaderAttributes.h"
#include "ocFile.h"
#include "ocParticleInstancer.h"
//#include "ocCacheRenderData.h"
#include "ocExportAnimation.h"
#include "ocRootTransform.h"
#include "ocParticleInstancerGenerator.h"
#include "ocOcclusionPass.h"
#include "ocSurfaceShader.h"
//#include "../generators/ocInstancerGenerator.h"
#include "../generators/ProxyGenerator/ocInstancerProxyGenerator.h"
#include "../generators/Geometry/ocFileGenerator.h"
#include "../generators/ocShaderGenerator.h"
//#include "../generators/PassGenerator_Occ.h"
//#include "ocellaris/PassGenerator_Final.h"
#include "ocPopupShaderListMenu.h"
#include "ocRenderPass.h"
#include "ocCustomPass.h"
#include "ocLight.h"
#include "ocJointVis.h"
#include "ocellaris/ocGeneratorSet.h"
#include "ocSequenceFile.h"
#include "ocGeneratorProxy.h"
#include "ocFreezeVerts.h"
#include "ocRSLshader.h"
#include "ocEditStringCmd.h"
#include "ocPolyShaderLayer.h"

//ocInstancerGenerator ocinstancergenerator;
ocInstancerProxyGenerator ocinstancerproxygenerator;
ocFileGenerator ocfilegenerator;
//ocGeometryGenerator ocgeometrygenerator;

//PassGenerator_Occ occpass;
//PassGenerator_Final ocrenderpass;

cls::IGenerator* finalPassGenerator = NULL;
cls::IGenerator* customPassGenerator = NULL;
ocShaderGenerator ocshadergenerator;

ocLight::Generator oclightgenerator;
ocParticleInstancerGenerator particleInstancerGenerator;

std::string ocellarisInstallDir;
HMODULE ocellarisProc_dll = 0;
MStatus uninitializePlugin( MObject obj);
MStatus initializePlugin( MObject obj )
{ 
	char filename[512]; 
	GetModuleFileName( (HMODULE)MhInstPlugin, filename, 512); 
	ocellarisInstallDir = filename;
	ocellarisInstallDir = Util::extract_foldername(ocellarisInstallDir.c_str());

	ocellarisProc_dll = LoadLibrary("ocellarisProc.dll");

	{
		static Util::DllProcedure dllproc;
		if( dllproc.LoadFormated("PassFinal", "ocellarisExport"))
		{
			cls::GENERATOR_CREATOR pc = (cls::GENERATOR_CREATOR)dllproc.proc;
			finalPassGenerator = (*pc)();
		}
	}
	{
		static Util::DllProcedure dllproc;
		if( dllproc.LoadFormated("PassCustom", "ocellarisExport"))
		{
			cls::GENERATOR_CREATOR pc = (cls::GENERATOR_CREATOR)dllproc.proc;
			customPassGenerator = (*pc)();
		}
	}
	

	MStatus   stat;
	MFnPlugin plugin( obj, "ULITKA", "6.0", "Any");
// #include "ocPolyShaderLayer.h"
	stat = plugin.registerCommand( 
		ocPolyShaderLayer::typeName.asChar(), 
		ocPolyShaderLayer::creator, 
		ocPolyShaderLayer::newSyntax );
	if (!stat) 
	{
		displayString("cant register %s", ocPolyShaderLayer::typeName.asChar());
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}


// #include "ocEditStringCmd.h"
	stat = plugin.registerCommand( 
		ocEditStringCmd::typeName.asChar(), 
		ocEditStringCmd::creator, 
		ocEditStringCmd::newSyntax );
	if (!stat) 
	{
		displayString("cant register %s", ocEditStringCmd::typeName.asChar());
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

// #include "ocRSLshader.h"
	{
//		const MString UserClassify( "texture/2d" );
//		const MString UserClassify( "utility/color" );
		const MString UserClassify( "shader/surface" );
		stat = plugin.registerNode( 
			ocRSLshader::typeName, 
			ocRSLshader::id, 
			ocRSLshader::creator,	
			ocRSLshader::initialize,
			MPxNode::kDependNode,
			&UserClassify );
		if (!stat) 
		{
			displayString("cant register %s", ocRSLshader::typeName.asChar());
			stat.perror("registerNode");
			uninitializePlugin(obj);
			return stat;
		}
		registerGenerator(ocRSLshader::id, &ocRSLshader::generator);
	}

// #include "ocFreezeVerts.h"
	stat = plugin.registerCommand( 
		ocFreezeVerts::typeName.asChar(), 
		ocFreezeVerts::creator, 
		ocFreezeVerts::newSyntax );
	if (!stat) 
	{
		displayString("cant register %s", ocFreezeVerts::typeName.asChar());
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

// #include "ocGeneratorProxy.h"
	stat = plugin.registerNode( 
		ocGeneratorProxy::typeName, 
		ocGeneratorProxy::id, 
		ocGeneratorProxy::creator,
		ocGeneratorProxy::initialize, 
		MPxNode::kLocatorNode );
	if (!stat) 
	{
		displayString("cant register %s", ocGeneratorProxy::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
	registerGenerator(ocGeneratorProxy::id, &ocGeneratorProxy::generator);

// #include "ocGeneratorSet.h"
	stat = plugin.registerNode( 
		cls::ocGeneratorSet::typeName, 
		cls::ocGeneratorSet::id, 
		cls::ocGeneratorSet::creator,
		cls::ocGeneratorSet::initialize, 
		MPxNode::kObjectSet);
	if (!stat) 
	{
		displayString("cant register %s", cls::ocGeneratorSet::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// ��� ������������ � ������ �.�.!!!!
	stat = plugin.registerData( 
		cls::MInstancePxData::typeName, 
		cls::MInstancePxData::id, 
		cls::MInstancePxData::creator);
	if (!stat) 
	{
		displayString("cant register node cls::MInstancePxData");
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "ocJointVis.h"
	stat = plugin.registerNode( 
		ocJointVis::typeName, 
		ocJointVis::id, 
		ocJointVis::creator,
		ocJointVis::initialize, 
		MPxNode::kLocatorNode );
	if (!stat) 
	{
		displayString("cant register %s", ocJointVis::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
	registerGenerator(ocJointVis::id, ocGetGeometryGenerator());

// #include "ocLight.h"
	{
		stat = plugin.registerNode( 
			ocLight::typeName, 
			ocLight::id, 
			ocLight::creator,
			ocLight::initialize, 
			MPxNode::kLocatorNode );
		if (!stat) 
		{
			displayString("cant register %s", ocLight::typeName.asChar());
			stat.perror("registerNode");
			uninitializePlugin(obj);
			return stat;
		}
		registerGenerator( ocLight::id, &oclightgenerator);
	}

// #include "ocRenderPass.h"
	stat = plugin.registerNode( 
		ocRenderPass::typeName, 
		ocRenderPass::id, 
		ocRenderPass::creator,
		ocRenderPass::initialize );
	if (!stat) 
	{
		displayString("cant register %s", ocRenderPass::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
	registerGenerator(ocRenderPass::id, finalPassGenerator);

// #include "ocCustomPass.h"
	stat = plugin.registerNode( 
		ocCustomPass::typeName, 
		ocCustomPass::id, 
		ocCustomPass::creator,
		ocCustomPass::initialize );
	if (!stat) 
	{
		displayString("cant register %s", ocCustomPass::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
	registerGenerator(ocCustomPass::id, customPassGenerator);


// #include "ocPopupShaderListMenu.h"
	stat = plugin.registerCommand( 
		ocPopupShaderListMenu::typeName.asChar(), 
		ocPopupShaderListMenu::creator, 
		ocPopupShaderListMenu::newSyntax );
	if (!stat) 
	{
		displayString("cant register %s", ocPopupShaderListMenu::typeName.asChar());
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}


	{
//		#include "ocOcclusionPass.h"
		const MString& swatchName =	MHWShaderSwatchGenerator::initialize();
		const MString UserClassify( "shader/surface/utility/:swatch/"+swatchName );
		stat = plugin.registerNode( 
			ocSurfaceShader::typeName, ocSurfaceShader::id, 
			ocSurfaceShader::creator, ocSurfaceShader::initialize,
			MPxNode::kHwShaderNode, &UserClassify );
		if (!stat) 
		{
			displayString("cant register %s", ocSurfaceShader::typeName.asChar());
			stat.perror("registerNode");
			uninitializePlugin(obj);
			return stat;
		}
		registerGenerator(MFn::kShape, &ocshadergenerator);
	}

// #include "ocOcclusionPass.h"
	stat = plugin.registerNode( 
		ocOcclusionPass::typeName, 
		ocOcclusionPass::id, 
		ocOcclusionPass::creator,
		ocOcclusionPass::initialize );
	if (!stat) 
	{
		displayString("cant register %s", ocOcclusionPass::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerCommand( 
		ocExportAnimation::typeName.asChar(), 
		ocExportAnimation::creator, 
		ocExportAnimation::newSyntax );
	if (!stat) 
	{
		displayString("cant register %s", ocExportAnimation::typeName.asChar());
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerCommand( 
		ocExport::typeName.asChar(), 
		ocExport::creator, 
		ocExport::newSyntax );
	if (!stat) 
	{
		displayString("cant register %s", ocExport::typeName.asChar());
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

	/*/
	stat = plugin.registerData( 
		ocCacheRenderData::typeName, 
		ocCacheRenderData::id, 
		ocCacheRenderData::creator);
	if (!stat) 
	{
		displayString("cant register %s", ocCacheRenderData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
	/*/

	stat = plugin.registerCommand( 
		ocScanShaderAttributes::typeName.asChar(), 
		ocScanShaderAttributes::creator, 
		ocScanShaderAttributes::newSyntax );
	if (!stat) 
	{
		displayString("cant register %s", ocScanShaderAttributes::typeName.asChar());
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerNode( 
		ocParticleFloatAttribute::typeName, 
		ocParticleFloatAttribute::id, 
		ocParticleFloatAttribute::creator,
		ocParticleFloatAttribute::initialize );
	if (!stat) 
	{
		displayString("cant register %s", ocParticleFloatAttribute::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
	stat = plugin.registerNode( 
		ocParticleVectorAttribute::typeName, 
		ocParticleVectorAttribute::id, 
		ocParticleVectorAttribute::creator,
		ocParticleVectorAttribute::initialize );
	if (!stat) 
	{
		displayString("cant register %s", ocParticleVectorAttribute::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerFileTranslator( 
		sloTranslator::typeName, 
		"",		// icon
		sloTranslator::creator, 
		"sloTranslatorOptions",		//optionsScriptName 
		NULL,		// defaultOptionsString 
		false		// requiresFullMel 
		);
	if (!stat) 
	{
		displayString("cant register %s", sloTranslator::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
	
	if( !MGlobal::sourceFile("sloTranslatorOptions.mel"))
	{
		displayString("error source sloTranslatorOptions.mel");
	}

	stat = plugin.registerCommand( "ocStartPrman", ocStartPrman::creator, ocStartPrman::newSyntax );
	if (!stat) {
		displayString("cant register node ocStartPrman");
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerNode( ocShaderParameter::typeName, ocShaderParameter::id, ocShaderParameter::creator,
								  ocShaderParameter::initialize );
	if (!stat) {
		displayString("cant register node ocShaderParameter");
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerFileTranslator( 
		"ocTranslator", 
		"",		// icon
		ocTranslator::creator, 
		"ocTranslatorOptions",		//optionsScriptName 
		NULL,		// defaultOptionsString 
		false		// requiresFullMel 
		);
	if (!stat) {
		displayString("cant register node ocTranslator");
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
	
	if( !MGlobal::sourceFile("ocTranslatorOptions.mel"))
	{
		displayString("error source ocTranslatorOptions.mel");
	}


// #include "ocSequenceFile.h"
	stat = plugin.registerNode( 
		ocSequenceFile::typeName, 
		ocSequenceFile::id, 
		ocSequenceFile::creator,
		ocSequenceFile::initialize );
	if (!stat) 
	{
		displayString("cant register %s", ocSequenceFile::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
	registerGenerator(ocSequenceFile::id, &ocSequenceFile::generator);


	stat = plugin.registerNode( 
		ocFile::typeName, 
		ocFile::id, 
		ocFile::creator,
		ocFile::initialize );
	if (!stat) 
	{
		displayString("cant register %s", ocFile::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
	registerGenerator(ocFile::id, &ocfilegenerator);
	

	stat = plugin.registerNode( 
		ocParticleInstancer::typeName, 
		ocParticleInstancer::id, 
		ocParticleInstancer::creator,
		ocParticleInstancer::initialize, 
		MPxNode::kLocatorNode );
	if (!stat) 
	{
		displayString("cant register %s", ocParticleInstancer::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
	registerGenerator(ocParticleInstancer::id, &particleInstancerGenerator);

// #include "ocRootTransform.h"
	stat = plugin.registerTransform( 
		ocRootTransform::typeName, 
		ocRootTransform::id, 
		ocRootTransform::creator, 
		ocRootTransform::initialize, 
		MPxTransformationMatrix::creator, 
		MPxTransformationMatrix::baseTransformationMatrixId  );
	if (!stat) 
	{
		displayString("cant register %s", ocRootTransform::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	#ifdef USE_LOCATOR_INSTANCE
	{
		stat = plugin.registerNode( ocInstance::typeName, ocInstance::id, ocInstance::creator,
									ocInstance::initialize, MPxNode::kLocatorNode );
		if (!stat) 
		{
			displayString("cant register node ocInstance");	
			stat.perror("registerNode");
			uninitializePlugin(obj);
			return stat;
		}
		registerGenerator(ocInstance::id, &ocinstancerproxygenerator);
	}
	#else
	{
		stat = plugin.registerShape( 
			ocInstance::typeName, 
			ocInstance::id, 
			ocInstance::creator, 
			ocInstance::initialize, 
			ocInstance2UI::creator
			);
		if (!stat) 
		{
			displayString("cant register %s", ocInstance::typeName.asChar());
			stat.perror("registerNode");
			uninitializePlugin(obj);
			return stat;
		}
		if( !MGlobal::sourceFile("AEocInstanceTemplate.mel"))
		{
			displayString("AEocInstanceTemplate.mel");
		}
		registerGenerator(ocInstance::id, &ocinstancerproxygenerator);
	}
	#endif

/*/
#ifndef SAS
#else
	if( !SourceMelFromResource(MhInstPlugin, "OCELLARISMAYAMENU.MEL", "MEL"))
	{
		displayString("error source OcellarisMayaMenu.mel");
	}
#endif
/*/

	/*/
	if( !MGlobal::sourceFile("OcellarisMayaMenu.mel"))
	{
		displayString("error source OcellarisMayaMenu.mel");
	}
	else
	{
	/*/
#ifdef _DEBUG
		if( !MGlobal::executeCommand("eval(\"OcellarisMayaDebugMenu\");"))
		{
			displayString("Dont found OcellarisMayaDebugMenu() command");
		}
#endif
	/*/
	}
	/*/

/*/
#ifndef SAS
	if( !MGlobal::executeCommand("AEshapeAddHook(\"AEocellarisShapeHook\");"))
	{
		displayString("Dont found AEshapeAddHook() command");
	}
#endif
/*/

	return stat;
}

MStatus uninitializePlugin( MObject obj)
{
	FreeLibrary(ocellarisProc_dll);
	MFnPlugin plugin( obj );

	MStatus   stat;
	stat = plugin.deregisterCommand( "ocPolyShaderLayer" );
	if (!stat) displayString("cant deregister %s", ocPolyShaderLayer::typeName.asChar());

	stat = plugin.deregisterCommand( "ocEditStringCmd" );
	if (!stat) displayString("cant deregister %s", ocEditStringCmd::typeName.asChar());

	unregisterGenerator(ocRSLshader::id, &ocRSLshader::generator);
	stat = plugin.deregisterNode( ocRSLshader::id );
	if (!stat) displayString("cant deregister %s", ocRSLshader::typeName.asChar());

	stat = plugin.deregisterCommand( "ocFreezeVerts" );
	if (!stat) displayString("cant deregister %s", ocFreezeVerts::typeName.asChar());

	unregisterGenerator(ocGeneratorProxy::id, &ocGeneratorProxy::generator);
	stat = plugin.deregisterNode( ocGeneratorProxy::id );
	if (!stat) displayString("cant deregister %s", ocGeneratorProxy::typeName.asChar());

	unregisterGenerator(ocSequenceFile::id, &ocSequenceFile::generator);
	stat = plugin.deregisterNode( ocSequenceFile::id );
	if (!stat) displayString("cant deregister %s", ocSequenceFile::typeName.asChar());

	stat = plugin.deregisterNode( cls::ocGeneratorSet::id );
	if (!stat) displayString("cant deregister %s", cls::ocGeneratorSet::typeName.asChar());

	stat = plugin.deregisterNode( ocJointVis::id );
	if (!stat) displayString("cant deregister %s", ocJointVis::typeName.asChar());
	unregisterGenerator(ocJointVis::id, ocGetGeometryGenerator());

	{
		stat = plugin.deregisterNode( ocLight::id );
		if (!stat) displayString("cant deregister %s", ocLight::typeName.asChar());
		unregisterGenerator( ocLight::id, &oclightgenerator);
	}

	unregisterGenerator(ocRenderPass::id, finalPassGenerator);
	stat = plugin.deregisterNode( ocRenderPass::id );
	if (!stat) displayString("cant deregister %s", ocRenderPass::typeName.asChar());

	unregisterGenerator(ocCustomPass::id, customPassGenerator);
	stat = plugin.deregisterNode( ocCustomPass::id );
	if (!stat) displayString("cant deregister %s", ocCustomPass::typeName.asChar());

	stat = plugin.deregisterCommand( "ocPopupShaderListMenu" );
	if (!stat) displayString("cant deregister %s", ocPopupShaderListMenu::typeName.asChar());

	{
		stat = plugin.deregisterNode( ocSurfaceShader::id );
		if (!stat) displayString("cant deregister %s", ocSurfaceShader::typeName.asChar());
		unregisterGenerator(MFn::kShape, &ocshadergenerator);
	}
	
	stat = plugin.deregisterNode( ocOcclusionPass::id );
	if (!stat) displayString("cant deregister %s", ocOcclusionPass::typeName.asChar());

	stat = plugin.deregisterNode( ocRootTransform::id );
	if (!stat) displayString("cant deregister %s", ocRootTransform::typeName.asChar());

	unregisterGenerator(ocParticleInstancer::id, &particleInstancerGenerator);

	#ifdef USE_LOCATOR_INSTANCE
	{
		stat = plugin.deregisterNode( ocInstance::id );
		if (!stat) displayString("cant deregister ocInstance");
		unregisterGenerator(ocInstance::id, &ocinstancerproxygenerator);
	}
	#else
	{
		stat = plugin.deregisterNode( ocInstance::id );
		if (!stat) displayString("cant deregister %s", ocInstance::typeName.asChar());
		unregisterGenerator(ocInstance::id, &ocinstancerproxygenerator);
	}
	#endif
	
	stat = plugin.deregisterCommand( "ocExportAnimation" );
	if (!stat) displayString("cant deregister %s", ocExportAnimation::typeName.asChar());

	stat = plugin.deregisterCommand( "ocExport" );
	if (!stat) displayString("cant deregister %s", ocExport::typeName.asChar());

//	stat = plugin.deregisterData( ocCacheRenderData::id );
//	if (!stat) displayString("cant deregister %s", ocCacheRenderData::typeName.asChar());

	stat = plugin.deregisterNode( ocParticleInstancer::id );
	if (!stat) displayString("cant deregister %s", ocParticleInstancer::typeName.asChar());

	{
		unregisterGenerator(ocFile::id, &ocfilegenerator);
		stat = plugin.deregisterNode( ocFile::id );
		if (!stat) displayString("cant deregister %s", ocFile::typeName.asChar());
	}

	stat = plugin.deregisterCommand( "ocScanShaderAttributes" );
	if (!stat) displayString("cant deregister %s", ocScanShaderAttributes::typeName.asChar());

	stat = plugin.deregisterNode( ocParticleFloatAttribute::id );
	if (!stat) displayString("cant deregister %s", ocParticleFloatAttribute::typeName.asChar());

	stat = plugin.deregisterNode( ocParticleVectorAttribute::id );
	if (!stat) displayString("cant deregister %s", ocParticleVectorAttribute::typeName.asChar());

	stat = plugin.deregisterFileTranslator( "sloTranslator" );
	if (!stat) displayString("cant deregister %s", sloTranslator::typeName.asChar());

	stat = plugin.deregisterCommand( "ocStartPrman" );
	if (!stat) displayString("cant deregister ocStartPrman");

	stat = plugin.deregisterNode( ocShaderParameter::id );
	if (!stat) displayString("cant deregister ocShaderParameter");

	stat = plugin.deregisterFileTranslator( "ocTranslator" );
	if (!stat) displayString("cant deregister ocTranslator");

	stat = plugin.deregisterData( cls::MInstancePxData::id );
	if (!stat) displayString("cant deregister cls::MInstancePxData");


	return stat;
}
