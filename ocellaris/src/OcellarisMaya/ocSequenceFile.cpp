#include "stdafx.h"
#include "ocSequenceFile.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnPluginData.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include "ocellaris\renderStringImpl.h"

#include <maya/MGlobal.h>
#include "ocellaris/MInstancePxData.h"
#include "mathNmaya\mathNmaya.h"
#include "Util/misc_directory_processing.h"

MObject ocSequenceFile::i_frame;
MObject ocSequenceFile::i_filename;
MObject ocSequenceFile::i_proxyfilename;
MObject ocSequenceFile::i_delay;
MObject ocSequenceFile::i_drawMode;
MObject ocSequenceFile::i_drawBox;
MObject ocSequenceFile::i_addStringAllpass;
MObject ocSequenceFile::i_addStringFinalpass;
MObject ocSequenceFile::i_addStringShadowpass;
MObject ocSequenceFile::i_renderSubFiles;
MObject ocSequenceFile::i_matte;
MObject ocSequenceFile::i_ignoreShaders;

MObject ocSequenceFile::o_drawCache;
MObject ocSequenceFile::o_box;
MObject ocSequenceFile::o_filename;
MObject ocSequenceFile::o_proxyfilename;

ocSequenceFile::ocSequenceFile()
{
}
ocSequenceFile::~ocSequenceFile()
{
}

MStatus ocSequenceFile::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;
	if( plug == o_filename)
	{
		MString filename = data.inputValue( i_filename, &stat ).asString();
		double time = data.inputValue( i_frame, &stat ).asDouble();
		int frame = (int)floor(time+0.5);
		
		char buf[512];
		sprintf(buf, filename.asChar(), frame);

		MDataHandle output = data.outputValue(plug);
		output.set( MString(buf));
		output.setClean();
		return MS::kSuccess;
	}
	if( plug == o_proxyfilename)
	{
		MString filename = data.inputValue( i_proxyfilename, &stat ).asString();
		double time = data.inputValue( i_frame, &stat ).asDouble();
		int frame = (int)floor(time+0.5);
		
		char buf[512];
		sprintf(buf, filename.asChar(), frame);

		MDataHandle output = data.outputValue(plug);
		output.set( MString(buf));
		output.setClean();
		return MS::kSuccess;
	}
	if( plug == o_drawCache )
	{
		bool bProgress = MGlobal::mayaState()==MGlobal::kInteractive;

		MString filename = data.inputValue( o_filename, &stat ).asString();
		MString proxyfilename = data.inputValue( o_proxyfilename, &stat ).asString();

		int drawMode = data.inputValue( i_drawMode, &stat ).asShort();
		bool drawBox = data.inputValue( i_drawBox, &stat ).asBool();
		bool bViewSubFiles = data.inputValue( i_renderSubFiles, &stat ).asBool();

		cls::MInstancePxData* pData = NULL;
		bool bNewData = false;
		MDataHandle outputData = data.outputValue( o_drawCache, &stat );
		MPxData* userdata = outputData.asPluginData();
		pData = static_cast<cls::MInstancePxData*>(userdata);
		MFnPluginData fnDataCreator;
		bool bNew = false;
		if( !pData)
		{
			MTypeId tmpid( cls::MInstancePxData::id );
			MObject newDataObject = fnDataCreator.create( tmpid, &stat );
			pData = static_cast<cls::MInstancePxData*>(fnDataCreator.data( &stat ));
			bNew = true;
		}
		pData->drawcache.LoadOcs(
			filename.asChar(), 
			proxyfilename.asChar(), 
			(cls::enMInstanceMode)drawMode,
			drawBox, 
			false, 
			bViewSubFiles, 
			false
			);
		if(bNew)
			outputData.set(pData);
		data.setClean(plug);
		return MS::kSuccess;
	}
	if( plug == o_box )
	{
		bool bProgress = MGlobal::mayaState()==MGlobal::kInteractive;

		MString filename = data.inputValue( o_filename, &stat ).asString();
		cls::renderFileImpl infileimpl;
		infileimpl.openForRead(filename.asChar());
		
		Math::Box3f box(1.f);
		infileimpl.GetAttribute("file::box", box);

		MDataHandle outputData = data.outputValue( plug, &stat );
		MVectorArray va(2);
		::copy( va[0], box.min);
		::copy( va[1], box.max);
		MFnVectorArrayData vad; 
		MObject val = vad.create(va);
		outputData.set(val);
		data.setClean(plug);
		return MS::kSuccess;
	}
	return MS::kUnknownParameter;
}

void* ocSequenceFile::creator()
{
	return new ocSequenceFile();
}

MStatus ocSequenceFile::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnEnumAttribute	enumAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		i_frame = numAttr.create( "frame","fra", MFnNumericData::kDouble, 0, &stat);
		::addAttribute(i_frame, atInput|atKeyable);
		i_filename = typedAttr.create ("filename","fn", MFnData::kString, &stat);
		::addAttribute(i_filename, atInput);
		i_proxyfilename = typedAttr.create ("proxyfilename","pfn", MFnData::kString, &stat);
		::addAttribute(i_proxyfilename, atInput);

		{
			i_drawMode = enumAttr.create("drawMode", "dm", 0);
			enumAttr.addField("as viewport", 0);
			enumAttr.addField("wireframe", 1);
			enumAttr.addField("points", 2);
			enumAttr.addField("only box", 3);
			::addAttribute(i_drawMode, atInput|atKeyable);

			i_drawBox = numAttr.create( "drawBox","db", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_drawBox, atInput|atKeyable);
			i_delay = numAttr.create( "delay","de", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_delay, atInput|atKeyable);
			i_renderSubFiles = numAttr.create( "viewSubFiles","vsf", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_renderSubFiles, atInput|atKeyable);
			i_matte = numAttr.create( "matte","mtt", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_matte, atInput|atKeyable);
			i_ignoreShaders = numAttr.create( "ignoreShaders","igs", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_ignoreShaders, atInput|atKeyable);
		}
		{
			i_addStringAllpass = typedAttr.create ("addStringAllpass","asap", MFnData::kString, &stat);
			::addAttribute(i_addStringAllpass, atInput);
			i_addStringFinalpass = typedAttr.create ("addStringFinalpass","asfp", MFnData::kString, &stat);
			::addAttribute(i_addStringFinalpass, atInput);
			i_addStringShadowpass = typedAttr.create ("addStringShadowpass","assp", MFnData::kString, &stat);
			::addAttribute(i_addStringShadowpass, atInput);
		}

		{
			o_filename = typedAttr.create ("outfilename","ofn", MFnData::kString, &stat);
			::addAttribute(o_filename, atOutput|atWritable|atUnStorable);
			stat = attributeAffects( i_frame, o_filename);
			stat = attributeAffects( i_filename, o_filename);
		}
		{
			o_proxyfilename = typedAttr.create ("outproxyfilename","opfn", MFnData::kString, &stat);
			::addAttribute(o_proxyfilename, atOutput|atWritable|atUnStorable);
			stat = attributeAffects( i_frame, o_proxyfilename);
			stat = attributeAffects( i_proxyfilename, o_proxyfilename);
		}
		

		{
			o_drawCache = typedAttr.create( "drawCache", "dch", cls::MInstancePxData::id, MObject::kNullObj, &stat);
			::addAttribute(o_drawCache, atOutput|atWritable|atUnStorable);
			stat = attributeAffects( o_filename, o_drawCache);
			stat = attributeAffects( o_proxyfilename, o_drawCache);
			stat = attributeAffects( i_frame, o_drawCache);
			stat = attributeAffects( i_filename, o_drawCache);
			stat = attributeAffects( i_proxyfilename, o_drawCache);
			stat = attributeAffects( i_drawMode, o_drawCache);
			stat = attributeAffects( i_drawBox, o_drawCache);
			stat = attributeAffects( i_renderSubFiles, o_drawCache);
		}
		{
			o_box = typedAttr.create ("box","bx", MFnData::kVectorArray, &stat);
			::addAttribute(o_box, atOutput|atCached);
			stat = attributeAffects( o_filename, o_box);
			stat = attributeAffects( i_frame, o_box);
			stat = attributeAffects( i_filename, o_box);
		}

		if( !MGlobal::sourceFile("AEocSequenceFileTemplate.mel"))
		{
			displayString("error source AEocSequenceFileTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}
ocSequenceFile::Generator ocSequenceFile::generator;

bool ocSequenceFile::Generator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();
	MFnDependencyNode dn(node);

	MString filename;
	MPlug(node, ocSequenceFile::o_filename).getValue(filename);

	Math::Box3f thisbox(1.f);
	MObject boxval;
	MPlug(node, ocSequenceFile::o_box).getValue(boxval);
	MFnVectorArrayData vad(boxval);
	if( vad.length()>=2)
	{
		::copy( thisbox.min, vad[0]);
		::copy( thisbox.max, vad[1]);
	}
	box.insert(thisbox);

	bool bDelay = false;
	MPlug(node, ocSequenceFile::i_delay).getValue(bDelay);

	bool bMatte = false;
	MPlug(node, ocSequenceFile::i_matte).getValue(bMatte);

	bool bIgnoreShaders = false;
	MPlug(node, ocSequenceFile::i_ignoreShaders).getValue(bIgnoreShaders);

	MString addStringAllpass;
	MPlug(node, ocSequenceFile::i_addStringAllpass).getValue(addStringAllpass);

	MString addStringFinalpass;
	MPlug(node, ocSequenceFile::i_addStringFinalpass).getValue(addStringFinalpass);

	MString addStringShadowpass;
	MPlug(node, ocSequenceFile::i_addStringShadowpass).getValue(addStringShadowpass);

	if(render)
	{
		// ALL PASS
		{
			cls::renderStringImpl fileimpl;

			std::string customdata = addStringAllpass.asChar();
			customdata += "\n";

			if( fileimpl.openForRead(customdata.c_str()))
				fileimpl.Render(render);
			else
				displayString("read addStringAllpass FAILURE");
		}

		// FINAL PASS
//		if( strcmp( context->pass(), "final")==0)
		{
			render->IfAttribute("@pass", cls::PS("final"));
			cls::renderStringImpl fileimpl;
			std::string customdata = addStringFinalpass.asChar();
			customdata += "\n";
			if( fileimpl.openForRead(customdata.c_str()))
				fileimpl.Render(render);
			else
				displayString("read addStringFinalpass FAILURE");
			render->EndIfAttribute();
		}
		
		// SHADOW PASS
//		if( strcmp( context->pass(), "shadow")==0)
		{
			render->IfAttribute("@pass", cls::PS("shadow"));
			cls::renderStringImpl fileimpl;
			std::string customdata = addStringShadowpass.asChar();
			customdata += "\n";
			if( fileimpl.openForRead(customdata.c_str()))
				fileimpl.Render(render);
			else
				displayString("read addStringShadowpass FAILURE");
			render->EndIfAttribute();
		}

		std::string fn = filename.asChar();
		Util::changeSlashToSlash(fn);

		if( bMatte)
			render->Attribute("@matte", true);

		if( bIgnoreShaders)
		{
			render->Attribute("shader::ignoreSurface", true);
			render->Attribute("shader::ignoreDisplace", true);
		}

		render->Parameter("filename", fn.c_str());
		render->Parameter("#bound", thisbox);
		render->Parameter("id", (int)0);
		render->Parameter("@delay", bDelay);
		render->RenderCall("ReadOCS");
	}
	return true;
}
