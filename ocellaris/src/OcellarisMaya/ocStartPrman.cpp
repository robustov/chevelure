#include "stdafx.h"
//
// Copyright (C) 
// File: ocStartPrmanCmd.cpp
// MEL Command: ocStartPrman

#include "ocStartPrman.h"
#include <time.h>

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MItDag.h>
#include <maya/M3dView.h>

#include "mathNpixar/IPrman.h"
#include "MathNMaya/MathNMaya.h"
#include "ocellaris/OcellarisExport.h"
#include "ocellaris/renderFileImpl.h"
#include "ocellaris/renderStringImpl.h"
#include "ocellaris/renderCacheImpl.h"
#include "ocellaris/renderPrmanImpl.h"
#include "ocellaris/renderBlurParser.h"
#include "Util/misc_create_directory.h"
#include "Alfred/Alfred.h"

//#include "ocellaris/PassGenerator_Final.h"
#include "ocellaris/MayaExportContextImpl.h"

void Maya_system( const char* cmd);
extern std::string ocellarisInstallDir;

ocStartPrman::ocStartPrman()
{
}
ocStartPrman::~ocStartPrman()
{
}

MSyntax ocStartPrman::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.addFlag("rib", "ribdir", MSyntax::kString);		// rib file name
	syntax.addFlag("dir", "directory", MSyntax::kString);	// output dir
	syntax.addFlag("b", "blur", MSyntax::kLong);
	syntax.addFlag("asc", "ascii", MSyntax::kBoolean);
	syntax.addFlag("ren", "render", MSyntax::kString);
	syntax.addFlag("p", "pass", MSyntax::kString);
	syntax.addFlag("seq", "sequence", MSyntax::kLong, MSyntax::kLong, MSyntax::kLong);	// start frame, end frame, eachframe
	syntax.addFlag("alf", "alfred", MSyntax::kString);	// kString = "prman" "netrender"
	syntax.addFlag("du", "dump", MSyntax::kBoolean);		// dump
	return syntax;
}

void SetupOptions(IPrman* prman);
void SetupCamera(IPrman* prman);
void SetupModel(IPrman* prman);

RtVoid dl_FurProceduralFree(RtPointer data)
{
	RtString* sData = (RtString*) data;
	free(sData[0]);
	free(sData[1]);
	free(sData);
}

MStatus ocStartPrman::doIt( const MArgList& args)
{
	try
	{
/*/
	bool bUseMayaShader = false;

	MStatus stat = MS::kSuccess;
	char buf[2048];
	MArgDatabase argData(syntax(), args);

	int bUseMtor = 0;
	MGlobal::executeCommand("exists mtor", bUseMtor);
	if( bUseMtor)
	{
		MGlobal::executeCommand("mtor control getvalue -sync");
	}

	//! ������
	MObject passnode = MObject::kNullObj;
	MString pass = "full";
	argData.getFlagArgument("p", 0, pass);
	if(pass!="full")
	{
		nodeFromName(pass, passnode);
	}

	// rib argname
	char dirname[256];
	::GetCurrentDirectory(256, dirname);
	MString dirargname;
	argData.getFlagArgument("rib", 0, dirargname);

	// dir argname
	char outputdir[256];
	::GetCurrentDirectory(256, outputdir);
	MString outputdirarg;
	argData.getFlagArgument("dir", 0, outputdirarg);

	// bAscii
	bool bAscii = 1;
	argData.getFlagArgument("ascii", 0, bAscii);

	// blur
	int bBlur = 0;
	argData.getFlagArgument("blur", 0, bBlur);

	// set
	MObject setobj = MObject::kNullObj;
	if( !passnode.isNull())
	{
		cls::PS set = ocExport_GetAttrValue(passnode, "set");
		nodeFromName(set.data(), setobj);
		if( !setobj.hasFn(MFn::kSet))
			setobj = MObject::kNullObj;
	}

	//! ���������
	bool bSequence = false;
	int startFrame=1, endFrame=25, eachFrame=1;
	if( argData.getFlagArgument("seq", 0, startFrame))
	{
		if( argData.getFlagArgument("seq", 1, endFrame))
		{
			if( argData.getFlagArgument("seq", 2, eachFrame))
			{
				if( eachFrame<1) eachFrame = 1;
				bSequence = true;
			}
		}
	}

	MTime mayatime = MAnimControl::currentTime();
	int frame = (int)mayatime.as(MTime::uiUnit());

	std::string seqfilename = dirargname.asChar();
	std::string seqcmd;
	if( !bSequence) 
	{
		startFrame = endFrame = frame;
	}

	std::string projectDir;
	{
		MString workspacedir;
		MGlobal::executeCommand("workspace -fn", workspacedir);
		projectDir = workspacedir.asChar();
	}
	bool bDump = false;
	if( !argData.getFlagArgument("du", 0, bDump))
		bDump = false;

	// alfred
	bool bAlfred = false;
	MString alfred;
	argData.getFlagArgument("alfred", 0, alfred);
	if( alfred.length()!=0) bAlfred = true;

	// StartImodiately
	bool bStartImidiately = true;
	if(bAlfred) bStartImidiately=false;
	if(bSequence) bStartImidiately=false;

	/////////////////////////////////////////////////
	// ���������� � ��������
	// �������� ������
	MObject rootnode;
	MItDag it;
	MDagPath path;
	it.getPath(path);
	rootnode = it.item();

	//! ���������� ��� ������
//	ChiefGenerator chieflights;
	MayaExportContextImpl contextlights(pass.asChar());
	contextlights.setOutputDirectory(outputdirarg.asChar());
	contextlights.InitChiefGenerator(OCGT_LIGHTS);

//	ocellarisDefaultGenerators(OCGT_LIGHTS, chieflights, &contextlights);
	//! ���������� ������ �������������� �������� INode
	cls::INode* pLightNode = ocNodeTreeType(MFn::kLight, contextlights.getChiefGenerator(), &contextlights);

	//! ���������� �� ���������
//	ChiefGenerator chief;
	MayaExportContextImpl context(pass.asChar());
	context.setOutputDirectory(outputdirarg.asChar());
	context.setShaderDir(dirargname.asChar());
	context.InitChiefGenerator(OCGT_SCENE);

	Util::DllProcedureTmpl<cls::GENERATOR_CREATOR> mayashadergenerator;
	if( bUseMayaShader)
	{
		mayashadergenerator.LoadFormated("MayaShader", "OcellarisExport.dll");
		if( mayashadergenerator.isValid())
			context.getChiefGenerator().addGenerator(MFn::kShape, (*mayashadergenerator)());
	}


//	ocellarisDefaultGenerators(OCGT_SCENE, chief, &context);

	Alfred::Job alfredjob(context.getAlfredTaskName());
	alfredjob.atleast = 1;
	alfredjob.atmost = 1;

	//////////////////////////////////////////////////
	//! Pass ���������
	cls::IPassGenerator* passGenerator;

	PassGenerator_Final finalpass;
	if( pass=="full")
	{
		passGenerator = &finalpass;
	}
	else if( pass=="full")
	{
		passGenerator = &finalpass;
	}
	else
	{
		// ����� passGenerator ��� passnode
		if( passnode.isNull())
		{
			displayError("pass node is FAILED");
			return MS::kFailure;
		}
		Node passnodeX;
		passnodeX.set(passnode);

		ChiefGenerator::GeneratorNHisObject genNobj = context.getChiefGenerator().getPass(passnodeX);
		passGenerator = (cls::IPassGenerator*)genNobj.gen;
		if( !passGenerator)
		{
			// �� ������ pass ���������
			displayError("Dont found passGenerator");
			return MS::kFailure;
		}
	}


	//! ���������� ������ �������������� �������� INode
	cls::INode* pNode = NULL;
	pNode = passGenerator->getNodeTree(passnode, &context.getChiefGenerator(), &context);

	if( !pNode)
	{
		if( setobj.isNull())
			pNode = ocNodeTree(path, context.getChiefGenerator(), &context);
		else
			pNode = ocNodeTree_fromSet(setobj, context.getChiefGenerator(), &context);
	}

	if( bDump)
	{ 
		pNode->Dump(stdout);
		return MS::kSuccess;
	}

	//! ���������� � ������� ���� �������� �������
	std::map<float, std::set<float> > used_timestamps;

	for(frame = startFrame; frame <= endFrame; frame+=eachFrame)
	{
		float t = (float)frame;
		float shutterOpenFrame = t;
		float shutterCloseFrame = t+1;
		pLightNode->PrepareFrame(t, shutterOpenFrame, shutterCloseFrame, used_timestamps);
		pNode->PrepareFrame(t, shutterOpenFrame, shutterCloseFrame, used_timestamps);
	}

	for(frame = startFrame; frame <= endFrame; frame+=eachFrame)
	{
		contextlights.OnNextFrame();
		context.OnNextFrame();

		float shutterOpenFrame = (float)frame;
		float shutterCloseFrame = (float)frame+1;
		if( bSequence) 
		{
			MTime t( frame, MTime::uiUnit());
			MAnimControl::setCurrentTime(t);
		}

		context.setProjectDirectory(projectDir.c_str());
		contextlights.setProjectDirectory(projectDir.c_str());
		context.setFrame(frame);
		contextlights.setFrame(frame);
		cls::renderCacheImpl<> cache;
		cls::renderCacheImpl<> cache_prescene;
		cls::renderCacheImpl<> cache_prescenenextgen;

		std::string passname;
		std::vector<cls::SubGenerator> _subGenerators;
		if( passGenerator->OnInit(passnode, &context, passname, _subGenerators))
		{
		}
		else
		{
			displayError("passGenerator->OnInit FAILED");
			return MS::kFailure;
		}
		bool bNeedNonBlurFile = false;
		std::vector<SubGenerator> subGenerators(_subGenerators.size());
		for(int s=0; s<(int)_subGenerators.size(); s++)
		{
			SubGenerator subgen;
			nodeFromName(_subGenerators[s].passnodename.c_str(), subgen.passnode);
			if(subgen.passnode.isNull()) 
				continue;
			subGenerators[s].passGenerator = _subGenerators[s].passGenerator;
			subgen.passGenerator = subGenerators[s].passGenerator;
			if(!subgen.passGenerator)
			{
				Node passnodeX;
				passnodeX.set(subgen.passnode);

				ChiefGenerator::GeneratorNHisObject genNobj = context.getChiefGenerator().getPass(passnodeX);
				subgen.passGenerator = (cls::IPassGenerator*)genNobj.gen;
//				subgen.passGenerator = context.getChiefGenerator().getPass(subgen.passnode);
			}
			if( !subgen.passGenerator) 
				continue;
			std::vector<cls::SubGenerator> _sg;
			std::string subpassname;
			if( !subgen.passGenerator->OnInit(subgen.passnode, &context, subpassname, _sg))
				continue;
			subGenerators.push_back(subgen);

			bool bPassBlured = subgen.passGenerator->IsBlured(subgen.passnode, &context);
			bNeedNonBlurFile = bNeedNonBlurFile | (!bPassBlured);
		}
		bNeedNonBlurFile = bNeedNonBlurFile & (bBlur!=0);

		if( passGenerator->OnExport(passnode, &context, &cache, &cache_prescene, &cache_prescenenextgen))
		{
//			chief.addGenerator(camnode, passGenerator);
		}

		////////////////////////////////////////////
		// filename
		std::string filename = dirargname.asChar();
		{
			char fn[512];
			_snprintf(fn, 512, "%s/%04d", filename.c_str(), frame);
			filename = fn;
		}
		if( filename.empty())
		{
			displayError("invalid filename");
			return MS::kFailure;
		}
		std::string ocsfilename = filename+".ocs";
		std::string ocsfilename_noblur = "";
		if(bNeedNonBlurFile)
			ocsfilename_noblur = filename+".noblur.ocs";
		std::string ligthsfilename = filename+".lights.ocs";

		////////////////////////////////////////////
		// export scene
		// 

		/////////////////////////////////////////
		// LIGHTS
		Math::Box3f ligthsbox;
		{
			// render to file
			cls::renderFileImpl _render;
			cls::IRender* render = &_render;
			Util::create_directory_for_file(ligthsfilename.c_str());
			if( !_render.openForWrite(ligthsfilename.c_str(), bAscii))
			{
				displayStringError("ExportOc: renderFileImpl cant create \"%s\" file!!!", ligthsfilename.c_str());
				return MS::kFailure;
			}

			//! �� ���� �������
//			if(!bBlur)
			{
				// ���� �����
				pLightNode->Render(render, ligthsbox, Math::Matrix4f::id, &contextlights);
			}
		}


		/////////////////////////////////////////
		// SCENE
		Math::Box3f box;			// ���� ���� �����
		{
			// render to file 
			cls::renderFileImpl _render;
			cls::IRender* render = &_render;
			{
				Util::create_directory_for_file(ocsfilename.c_str());
				if( !_render.openForWrite(ocsfilename.c_str(), bAscii))
				{
					displayStringError("ExportOc: renderFileImpl cant create \"%s\" file!!!", ocsfilename.c_str());
					return MS::kFailure;
				}
				std::string fn = projectDir;
				Util::changeSlashToSlash(fn);
				render->Attribute("file::seachpath", fn.c_str());
			}

			// nonblur
			cls::renderFileImpl _nonblurrender;
			cls::IRender* nonblurrender = &_nonblurrender;
			if( bNeedNonBlurFile)
			{
				Util::create_directory_for_file(ocsfilename_noblur.c_str());
				if( !_nonblurrender.openForWrite(ocsfilename_noblur.c_str(), bAscii))
				{
					displayStringError("ExportOc: renderFileImpl cant create \"%s\" file!!!", ocsfilename_noblur.c_str());
					return MS::kFailure;
				}
				std::string fn = projectDir;
				Util::changeSlashToSlash(fn);
				nonblurrender->Attribute("file::seachpath", fn.c_str());
			}

			//! �� ���� �������
			if(!bBlur)
			{
				// ���� �����
				pNode->Render(render, box, Math::Matrix4f::id, &context);
			}
			else
			{
				// ���� 
				{
					cls::renderBlurParser<> blurparser;
					// phase0
					blurparser.SetCurrentPhase(0.f);
					MAnimControl::setCurrentTime( MTime(shutterOpenFrame, MTime::uiUnit()));
					pNode->Render(&blurparser, box, Math::Matrix4f::id, &context);
					if( bNeedNonBlurFile)
					{
						pNode->Render(nonblurrender, box, Math::Matrix4f::id, &context);
					}
					// phase1
					blurparser.SetCurrentPhase(1.f);
					MAnimControl::setCurrentTime( MTime(shutterCloseFrame, MTime::uiUnit()));
					pNode->Render(&blurparser, box, Math::Matrix4f::id, &context);

					blurparser.Render(render);
				}
				// ���� ���� ��������� ������� - ��������� � ��������� ����
			}
		}

		////////////////////////////////
		// ��������� ����
		std::string ocsframefilename = filename + ".frame.ocs";

		MString rendername = "prman";
		argData.getFlagArgument("render", 0, rendername);

		_snprintf( buf, 1224, "frame %d", frame);
		Alfred::Task alfredtask(buf);

		////////////////////////////////////////////
		// render
		std::string cmd="", debugcmd="";
		if( rendername=="prman")
		{
			// �������� ��������� ���� (��� �������!!!)
			{
				cls::renderFileImpl framerender;
				if( framerender.openForWrite(ocsframefilename.c_str(), true))
				{
					cache.Render(&framerender);
				}
			}
			GenRib(
				passGenerator,
//				cache,
				passnode,
				subGenerators,
				&context,
				frame,
				box, 
				filename,
				ocsfilename,
				ocsfilename_noblur,
				ligthsfilename,
				projectDir,
				bBlur!=0, 
				shutterOpenFrame, 
				shutterCloseFrame, 
				cmd, 
				debugcmd, 
				bAlfred?(&alfredtask):0
				);
		}
		else if( rendername=="gelato")
		{
			cache.RenderCall("FrameBegin");
			cache.RenderCall("WorldBegin");
			{
				cache_prescene.Render(&cache);
//				passGenerator->OnPreScene(passnode, &cache, &context);
			}
			{
				cache.Parameter("box", ligthsbox);
				cache.Parameter("filename", ligthsfilename.c_str());
				cache.RenderCall("ReadOCS");
			}
			{
				cache.Parameter("box", box);
				cache.Parameter("filename", ocsfilename.c_str());
				cache.RenderCall("ReadOCS");
			}
			cache.RenderCall("WorldEnd");

			cache.Parameter("render::cameraname", "Camera");
			cache.RenderCall("FrameEnd");
			// �������� ��������� ���� (��� �������!!!)
			{
				cls::renderFileImpl framerender;
				if( !framerender.openForWrite(ocsframefilename.c_str(), true))
				{
					displayStringError("ExportOc: renderFileImpl cant create \"%s\" file!!!", ocsframefilename.c_str());
					return MS::kFailure;
				}
				cache.Render(&framerender);
			}

			// start render!!!
			{
				cmd = "gelato.exe ";
				cmd += "\""+ocsframefilename+"\"";
			}
		}

		if(!cmd.empty())
		{
			seqcmd += cmd + "\n";

			std::string batfilename = filename+".bat";
			FILE* batfile = fopen(batfilename.c_str(), "wt");
			if( batfile)
			{
//				char* path = getenv("PATH");
//				fprintf( batfile, "set PATH=%s\n", path);
				fputs( cmd.c_str(), batfile);
				fputs( "\n", batfile);
				fputs( debugcmd.c_str(), batfile);
				fclose(batfile);
			}
			if( bAlfred)
			{
				alfredjob.AddSubtask(alfredtask);
			}

			if(bStartImidiately)
			{
				displayString("%s", cmd.c_str());
//				::system( (std::string("start ")+cmd).c_str());
				Maya_system(cmd.c_str());
			}

	//		if( rendername=="prman")
	//			::system( "start it.exe");
		}
	}

	if(bSequence)
	{
		std::string batfilename = seqfilename+".bat";
		FILE* batfile = fopen(batfilename.c_str(), "wt");
		if( batfile)
		{
			fputs( seqcmd.c_str(), batfile);
			fclose(batfile);
			if(!bAlfred)
			{
//				::system( (std::string("start ")+batfilename).c_str());
				Maya_system(batfilename.c_str());

//				::system( (batfilename).c_str());
			}
		}

		std::string fcheckfilename = projectDir+"/rmanpix/";
		fcheckfilename+=pass.asChar();
		fcheckfilename+="/";
		fcheckfilename+=pass.asChar();
		fcheckfilename+=".view.bat";
		FILE* fcheckfile = fopen(fcheckfilename.c_str(), "wt");
		if( fcheckfile)
		{
			char fn[512];
			_snprintf(fn, 512, "fcheck -n %04d %04d 1 %s.@@@@.tif", startFrame, endFrame, pass.asChar());
			fputs( fn, fcheckfile);
			fclose(fcheckfile);
		}
	}
	if( bAlfred)
	{	
		std::string alffile = projectDir + "/alfred/";
		alffile = Alfred::unequalJobName(alffile.c_str());
		Util::create_directory_for_file(alffile.c_str());

		alfredjob.AddCleanup(Alfred::CmdDeleteFile(alffile.c_str()));

		Alfred::saveAlfredFile(alffile.c_str(), alfredjob);
		bool bPaused = false;
//		Alfred::startAlfred(alffile.c_str(), bPaused);

		{
			const char* filename = alffile.c_str();
			const char* rat_tree_env = getenv("RATTREE");
			char cmd[1024];
			_snprintf(cmd, 1024, "%s/bin/alfred.exe %s\"%s\"", rat_tree_env, bPaused?"-paused ":"", filename);
			printf( cmd); printf( "\n");
			MGlobal::displayInfo(cmd);
			Maya_system(cmd);
		}
	}
/*/

	}
	catch(...)
	{
		displayStringError("Exception in ocStartPrman!!!");
	}

	return MS::kSuccess;
}

void* ocStartPrman::creator()
{
	return new ocStartPrman();
}

struct RibFile
{
	bool bOpened;
	IPrman* prman;
	std::string filename;

	RibFile(IPrman* prman)
	{
		this->prman = prman;
		bOpened = false;
	}
	~RibFile()
	{
		Close();
	}

	bool Open(const char* filename, const char* _projectDir)
	{
		if( bOpened)
			Close();

		std::string projectDir = _projectDir;
		this->filename = filename;
		prman->RiBegin( (char*)filename );
		{
			RtString format = "ascii";
			prman->RiOption(( RtToken ) "rib", ( RtToken ) "format", &format, RI_NULL);
			RtString compression = "none";
			prman->RiOption(( RtToken ) "rib", ( RtToken ) "compression", &compression, RI_NULL);
		}

		{
			std::string buf;
	//		RtString text = "//M/public/://C/Pixar/rat-6.0.1/lib/shaders://C/Pixar/rat-6.0.1/lib://C/Pixar/RenderManProServer-11.5.3/lib/shaders://C/Pixar/RenderManProServer-11.5.3/lib:.:@";
			std::string seachpath; 

			buf = projectDir; Util::changeSlashToTclSlash(buf);
			seachpath += buf; seachpath += ":";
			seachpath += buf+"/3D"; seachpath += ":";

			buf = getenv("RMANTREE"); Util::changeSlashToTclSlash(buf);
			seachpath += buf+"/lib"; seachpath += ":";
			seachpath += buf+"/lib/shaders"; seachpath += ":";

			buf = getenv("RATTREE"); Util::changeSlashToTclSlash(buf);
			seachpath += buf+"/lib/shaders"; seachpath += ":";
			
			buf = ocellarisInstallDir; Util::changeSlashToTclSlash(buf);
			seachpath += buf;seachpath += ":";
			seachpath += ".";seachpath += ":";
			seachpath += "@";seachpath += ":";
			Util::changeSlashToSlash(seachpath);

			RtString text = (RtString)seachpath.c_str();
			prman->RiOption( (RtToken )"searchpath", 
				(RtToken )"shader", ( RtPointer )&text, RI_NULL);

			prman->RiOption( (RtToken )"searchpath", 
				(RtToken )"texture", ( RtPointer )&text, RI_NULL);

			prman->RiOption( (RtToken )"searchpath", 
				(RtToken )"procedural", ( RtPointer )&text, RI_NULL);
			
		}
		bOpened = true;
		return true;
	}
	bool Close()
	{
		if( bOpened)
		{
			prman->RiEnd();
		}
		bOpened = false;
		return true;
	}
};

bool ocStartPrman::GenRib(
	cls::IPassGenerator* passGenerator,
//	cls::renderCacheImpl<>& passexportcache,
	MObject passnode, 
	std::vector<SubGenerator>& subGenerators,
	cls::IExportContext* context,
	int frame,
	Math::Box3f box, 
	std::string filename,
	std::string ocsfilename,
	std::string ocsfilename_noblur,
	std::string ligthsfilename,
	std::string projectDir,
	bool bBlur, 
	float shutterOpenFrame, 
	float shutterCloseFrame, 
	std::string& cmd, 
	std::string& debugcmd,
	Alfred::Task* alfredtask
	)
{

	bool bRemote = true;
	// RENDERMAN
	char ribfilenamebuf[512];
	int ribfileindex = 0;
	int taskindex=0;

#ifdef _DEBUG
  	getIPrmanDll dllproc("IPrmanD.dll", "getIPrman");
#else
  	getIPrmanDll dllproc("IPrman.dll", "getIPrman");
#endif
	if( !dllproc.isValid()) 
	{
		displayError("cant fount IPrman.dll or IPrmanD.dll");
		return false;
	}
	IPrman* prman = (*dllproc)();

	RibFile rib(prman);

	if( !alfredtask)
	{
		std::string ribfilename = filename + ".rib";
		rib.Open(ribfilename.c_str(), projectDir.c_str());
	}

	Alfred::Task subrenders("SubRenders%d", frame);

	// subGenerators
	for( int s=0; s<(int)subGenerators.size(); s++)
	{
		cls::IPassGenerator* subPassGenerator = subGenerators[s].passGenerator;
		if(!subPassGenerator) continue;

		MObject subpassnode = subGenerators[s].passnode;

//		if( subPassGenerator->isLazy(subpassnode, context))
//			continue;

		if( alfredtask)
		{
			sprintf(ribfilenamebuf, "%s.%02d.rib", filename.c_str(), ribfileindex++);
			rib.Open(ribfilenamebuf, projectDir.c_str());
		}


		bool bBlured = true;//subPassGenerator->IsBlured(subpassnode, context);

		prman->RiFrameBegin(s);

		//	Setup Camera and options
		cls::renderPrmanImpl<> renderprman(prman);

		/*/
		subPassGenerator->OnExport(
			subpassnode, 
			context, 
			&renderprman, 
			&subGenerators[s].cache_prescene, 
			&subGenerators[s].cache_prescene_nextgen);
		/*/

		std::string outputname = context->getEnvironment("OUTPUTFILE");

		prman->RiWorldBegin();


		// passGenerator::OnPreScene
		cls::renderStringImpl stringrender;
		stringrender.openForWrite();
//		subPassGenerator->OnPreScene(
//			subpassnode, &stringrender, context, 
		subGenerators[s].cache_prescene.Render(&stringrender);
		std::string addstream = stringrender.getWriteBuffer();

		// �������� �� ��������
		/*/
		{
			cls::renderFileImpl buf;
			if(buf.openForRead(ligthsfilename.c_str()))
			{
				buf.Render(&renderprman);
			}
		}
		/*/
		// RiProcedural
		char buf[2048];
		{
			const char* ocs = ocsfilename.c_str();
			if(!ocsfilename_noblur.empty() && !bBlured)
			{
				ocs = ocsfilename_noblur.c_str();
			}
			_snprintf(buf, 1224, "%s$%f$%f$%f$%s", 
				ocs, 
				(float)frame, shutterOpenFrame, shutterCloseFrame, "");

			addstream = buf+addstream;

			RtBound bound;
			IPrman::copy(bound, box);
			RtString *data = static_cast<RtString*>(malloc(sizeof(RtString) * 3));
			data[0] = _strdup("ocellarisPrmanDSO.dll");
			data[1] = _strdup(addstream.c_str());
			data[2] = 0;
			prman->RiProcedural(data, bound, prman->RiProcDynamicLoad(), dl_FurProceduralFree);
		}
		prman->RiWorldEnd();
		prman->RiFrameEnd();

		if( alfredtask)
		{
			rib.Close();

			std::string localcmd;
//			localcmd = getenv("RMANTREE");
//			Util::pushBackSlash(localcmd);
//			localcmd = "%RMANTREE%/";
//			localcmd += "bin/";
			localcmd += "prman.exe -p:1 -Progress ";
			localcmd += "\""+rib.filename+"\"";

			Alfred::Cmd alfredcmd( localcmd.c_str(), bRemote);
			alfredcmd.service = "pixarRender";
			alfredcmd.atmost  = 1;
			alfredcmd.tags	= "intensive";

			Alfred::Task newtask("sub%d_%d", frame, taskindex++);
			newtask.preview = std::string("sho \"")+context->getEnvironment("OUTPUTFILE")+std::string("\"");

			newtask.AddCmd(alfredcmd);

			subrenders.AddTask(newtask);

			cmd+=localcmd+"\n";
		}

	}
	if( alfredtask)
	{
		if( !subGenerators.empty())
		{
			alfredtask->AddTask(subrenders);
		}
	}

	// CROPS
	std::vector<Math::Box2f> crops;
	cls::P<bool> bCrop = ocExport_GetAttrValue(passnode, "crop");
	cls::PA<int> cropXY = ocExport_GetAttrValue(passnode, "cropXY");
	if(bCrop.empty() || !bCrop.data() || cropXY.size()!=2)
	{
		crops.push_back( Math::Box2f( Math::Vec2f(0, 0), Math::Vec2f(1, 1) ));
	}
	else
	{
		for(int x=0; x<cropXY[0]; x++)
		{
			for(int y=0; y<cropXY[1]; y++)
			{
				Math::Vec2f mi(x/ (float)cropXY[0], y/(float)cropXY[1]);
				Math::Vec2f ma((x+1)/(float)cropXY[0], (y+1)/(float)cropXY[1]);

				crops.push_back( Math::Box2f( mi, ma));
			}
		}
	}
	Alfred::Task croptask("AllCrops%d", frame);
	Alfred::Task* rendertask = alfredtask;
	if( crops.size()>1)
	{
		rendertask = &croptask;
	}

	// RENDER CROPS
	std::string mainoutput;
	std::vector<std::string> outputs;
	int i;
	for(int i=0; i<(int)crops.size(); i++)
	{
		Math::Box2f& crop = crops[i];

		if( alfredtask)
		{
			sprintf(ribfilenamebuf, "%s.%02d.rib", filename.c_str(), ribfileindex++);
			rib.Open(ribfilenamebuf, projectDir.c_str());
		}

		cls::renderCacheImpl<> cache_prescene;
		cls::renderCacheImpl<> cache_prescene_postgen;

		prman->RiFrameBegin(i);

		//	Setup Camera and options
		cls::renderPrmanImpl<> renderprman(prman);

		if( crops.size()!=1)
			context->setEnvironment("CROP", "i");
		else
			context->setEnvironment("CROP", "-1");	

		/*/
		passGenerator->OnExport(
			passnode, context, &renderprman, 
			&cache_prescene, 
			&cache_prescene_postgen
			);
		/*/
		std::string outputname = context->getEnvironment("OUTPUTFILE");


		mainoutput = context->getEnvironment("OUTPUTFILE");
		outputs.push_back(outputname);
		prman->RiCropWindow(crop.min.x, crop.max.x, crop.min.y, crop.max.y);

		// ���� �����
		if( bBlur)
		{
			float open = shutterOpenFrame;
			float close = shutterCloseFrame;
			cls::P<float> shutterAngle = ocExport_GetAttrValue(passnode, "shutterAngle");
			if( !shutterAngle.empty())
			{
				close = open + (close-open)*shutterAngle.data()/360;
			}
			prman->RiShutter(open, close);
		}

		prman->RiWorldBegin();

		// passGenerator::OnPreScene
		std::string addstream;
		{
			cls::renderStringImpl stringrender;
			stringrender.openForWrite();
			cache_prescene.Render(&stringrender);
//			passGenerator->OnPreScene(passnode, &stringrender, context);
//			passGenerator->OnPreScene(passnode, &renderprman, context);

			for( int s=0; s<(int)subGenerators.size(); s++)
			{
				subGenerators[s].cache_prescene_nextgen.Render(
					&stringrender);
//				subPassGenerator->OnPostGeneratorScene(subpassnode, &stringrender, context);
			}
			addstream = stringrender.getWriteBuffer();
		}
		// RiProcedural ligths
		/*/
		if(false)
		{
			RtBound bound;
			IPrman::copy(bound, ligthsbox);
			RtString *data = static_cast<RtString*>(malloc(sizeof(RtString) * 3));
			data[0] = strdup("ocellarisPrmanDSO.dll");
			data[1] = strdup(ligthsfilename.c_str());
			data[2] = 0;
			prman->RiProcedural(data, bound, prman->RiProcDynamicLoad(), dl_FurProceduralFree);
		}
		else
		/*/
		{

			for( int s=0; s<(int)subGenerators.size(); s++)
			{
				subGenerators[s].cache_prescene_nextgen.Render(
					&renderprman);
			}
			cls::renderFileImpl buf;
			if(buf.openForRead(ligthsfilename.c_str()))
			{
				buf.Render(&renderprman);
			}

		}
		// RiProcedural
		char buf[2048];
		{
			_snprintf(buf, 1224, "%s$%f$%f$%f$%s", 
				ocsfilename.c_str(), 
				(float)frame, shutterOpenFrame, shutterCloseFrame, "");
			addstream = buf+addstream;
			RtBound bound;
			IPrman::copy(bound, box);
			RtString *data = static_cast<RtString*>(malloc(sizeof(RtString) * 3));
			data[0] = _strdup("ocellarisPrmanDSO.dll");
			data[1] = _strdup(addstream.c_str());
			data[2] = 0;
			prman->RiProcedural(data, bound, prman->RiProcDynamicLoad(), dl_FurProceduralFree);
		}
		prman->RiWorldEnd();
		prman->RiFrameEnd();

		if( alfredtask)
		{
			rib.Close();

			std::string localcmd;
//			localcmd = getenv("RMANTREE");
//			Util::pushBackSlash(localcmd);
//			localcmd = "%RMANTREE%/";
//			localcmd += "bin/";
			localcmd += "prman.exe -p:1 -Progress ";
			localcmd += "\""+rib.filename+"\"";

			Alfred::Cmd alfredcmd( localcmd.c_str(), bRemote);
			alfredcmd.service = "pixarRender";
			alfredcmd.atmost  = 1;
			alfredcmd.tags	= "intensive";
			Alfred::Task newtask("crop%d_%d", frame, taskindex++);
			if( !subGenerators.empty())
				newtask.AddInstance(subrenders);
			newtask.AddCmd(alfredcmd);
			newtask.preview = std::string("sho \"")+context->getEnvironment("OUTPUTFILE")+std::string("\"");

			rendertask->AddTask(newtask);

			cmd+=localcmd+"\n";
		}
	}

	if( outputs.size()>1)
	{
		if( !alfredtask)
		{
			prman->RiFrameBegin(0);
			cls::renderPrmanImpl<> renderprman(prman);
			prman->RiWorldBegin();
		}

		RtBound bound;
		Math::Box3f box(10000);
		IPrman::copy(bound, box);

		char buf[10];
		std::string localcmd;
		localcmd = "ocellarisCropAsm.exe";
		localcmd += " ";
		localcmd += itoa( cropXY[0], buf, 10); 
		localcmd += " ";
		localcmd += itoa( cropXY[1], buf, 10); 
		for(int i=0; i<(int)outputs.size(); i++)
		{
			localcmd += " \"";
			localcmd += outputs[i];
			localcmd += "\"";
		}
		localcmd += " \"";
		localcmd += mainoutput;
		localcmd += "\"";

		if(!alfredtask)
		{
			RtString *data = static_cast<RtString*>(malloc(sizeof(RtString) * 3));
			data[0] = strdup(localcmd.c_str());
			data[1] = "";
			data[2] = 0;
			prman->RiProcedural(data, bound, prman->RiProcRunProgram(), dl_FurProceduralFree);

			prman->RiWorldEnd();
			prman->RiFrameEnd();
		}
		else
		{
			Alfred::Cmd alfredcmd( localcmd.c_str(), bRemote);
			alfredcmd.service = "pixarRender";
			alfredcmd.atmost  = 1;
			alfredcmd.tags	= "intensive";

			croptask.AddCmd(alfredcmd);
			alfredtask->preview = std::string("sho \"")+context->getEnvironment("OUTPUTFILE")+std::string("\"");
			alfredtask->AddTask(croptask);
			cmd+=localcmd+"\n";
		}
	}

	/*/
	prman->RiEnd();

	// start render!!!
	{
		cmd = "prman -p:1 -Progress ";
		cmd += "\""+ribfilename+"\"";
	}
	/*/



	if( !alfredtask)
	{
		rib.Close();
		cmd = getenv("RMANTREE");
		Util::pushBackSlash(cmd);
//		cmd = "%RMANTREE%/";
		cmd += "bin/prman.exe -p:1 -Progress ";
		cmd += "\""+rib.filename+"\"";
	}

	{
		debugcmd = ocellarisInstallDir+"/OcellarisTest.exe ";
		debugcmd += "\""+ocsfilename+"\"";
		debugcmd += "\n";

		debugcmd += ocellarisInstallDir+"/OcellarisTest.exe ";
		debugcmd += "\""+ocsfilename+"\" ";
		debugcmd += "\""+ocsfilename+".ocs\" -ocs -exe\n";

		debugcmd += "pause";
	}

	return true;
}

void Maya_system( const char* cmd)
{
//	system(cmd);

	STARTUPINFO si = { sizeof(si) };
	PROCESS_INFORMATION pi;
	si.wShowWindow = SW_SHOWNORMAL;

	BOOL res = CreateProcess( NULL, (LPSTR)cmd, NULL, NULL, TRUE, 0, NULL, NULL, &si, &pi);

}
