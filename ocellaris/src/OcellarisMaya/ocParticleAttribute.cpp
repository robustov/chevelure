#include "stdafx.h"
#include "ocParticleAttribute.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>

#include <maya/MGlobal.h>

// �������
MObject ocParticleFloatAttribute::i_input;
MObject ocParticleVectorAttribute::i_input;

// �����
MObject ocParticleFloatAttribute::o_output;
MObject ocParticleVectorAttribute::o_output;

ocParticleFloatAttribute::ocParticleFloatAttribute()
{
}
ocParticleVectorAttribute::ocParticleVectorAttribute()
{
}
ocParticleFloatAttribute::~ocParticleFloatAttribute()
{
}
ocParticleVectorAttribute::~ocParticleVectorAttribute()
{
}

MStatus ocParticleFloatAttribute::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	return MS::kUnknownParameter;
}
MStatus ocParticleVectorAttribute::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	return MS::kUnknownParameter;
}

void* ocParticleFloatAttribute::creator()
{
	return new ocParticleFloatAttribute();
}
void* ocParticleVectorAttribute::creator()
{
	return new ocParticleVectorAttribute();
}

MStatus ocParticleFloatAttribute::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		i_input = typedAttr.create("i_input", "iin", MFnData::kDoubleArray);
		::addAttribute(i_input, atInput);
		
		o_output = numAttr.create("o_output", "oou", MFnNumericData::kDouble);
		::addAttribute(o_output, atOutput);
		
		if( !MGlobal::sourceFile("AEocParticleAttributeTemplate.mel"))
		{
			displayString("error source AEocParticleAttributeTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}


MStatus ocParticleVectorAttribute::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		i_input = typedAttr.create("i_input", "iin", MFnData::kVectorArray);
		::addAttribute(i_input, atInput);
		
		o_output = numAttr.create("o_output", "oou", MFnNumericData::k3Float);
		::addAttribute(o_output, atOutput);

		if( !MGlobal::sourceFile("AEocParticleAttributeTemplate.mel"))
		{
			displayString("error source AEocParticleAttributeTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

