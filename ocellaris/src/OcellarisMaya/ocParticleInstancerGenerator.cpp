#include "stdafx.h"
#include "ocParticleInstancerGenerator.h"
#include "ocParticleInstancer.h"
#include "ocellaris/OcellarisExport.h"

#include <maya/MFnDependencyNode.h>
#include <maya/MFnPluginData.h> 
//! @ingroup implement_group
//! \brief ����������� ��������� ��� Instancer (particleSystem)
//! 

bool ocParticleInstancerGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();
	MFnDependencyNode dn(node);
	MTypeId type_id = dn.typeId();
	if( type_id!=ocParticleInstancer::id)
		return false;
	ocParticleInstancer* pi = (ocParticleInstancer*)dn.userNode();
	if( !pi) 
		return false;

	bool res = pi->Save(render, box, context);
	return res;
}
