#pragma once

#include "pre.h"
#include <maya/MPxLocatorNode.h>
#include "ocellaris/IProxyGenerator.h"
#include "ocellaris/IDeformGenerator.h"

 
class ocGeneratorProxy : public MPxLocatorNode
{
public:
	ocGeneratorProxy();
	virtual ~ocGeneratorProxy(); 

	bool setInternalValue( 
		const MPlug& plug,
		const MDataHandle& data);

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	virtual void draw(
		M3dView &view, const MDagPath &path, 
		M3dView::DisplayStyle style,
		M3dView::DisplayStatus status);

	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_referObject;
	static MObject i_addStringAllpass;
//	static MObject i_type;
//	static MObject i_generator;
//	static MObject i_calltypeAttribute;

public:
	static MTypeId id;
	static const MString typeName;


	//! @ingroup implement_group
	//! \brief ������ ��������� ��� ����� ocGeneratorProxy
	struct Generator : public cls::IProxyGenerator
	{
		// ���������� ���, ������ ��� �������������
		virtual const char* GetUniqueName(
			){return "ocGeneratorProxy";};

		//! OnProcess
		virtual MObject OnProcess(
			cls::INode* node,
			cls::IExportContext* context,
			MObject& generatornode
			);
	};
	static Generator generator;

	//! @ingroup implement_group
	//! \brief DeformGenerator ��������� ��� ����� ocGeneratorProxy
	//! ���������� ������� � ����������� ����� ������� RenderCall
	struct DeformGenerator : public cls::IDeformGenerator
	{
		// ���������� ���, ������ ��� �������������
		virtual const char* GetUniqueName(
			){return "ocDeformGeneratorForProxy";};

		// � ����� ������ ��������
		virtual int GetDeformCallType(
			MObject& generatornode){return cls::IDeformGenerator::CT_BEFORE_GEOMETRY;};

		//! OnGeometry
		virtual void OnGeometry(
			cls::INode& inode,				//!< ������ 
			cls::IRender* render,				//!< render
			cls::enSystemCall call,				//!< ��� ������ ��������� ��������: I_POINTS, I_CURVES, I_MESH ...
			cls::IExportContext* context, 
			MObject& generatornode			//!< generator node �.� 0
			);
	};
	static DeformGenerator deformgenerator;

};


