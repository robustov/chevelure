#include "stdafx.h"
#include "ocOcclusionPass.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnStringData.h>

#include <maya/MGlobal.h>

MObject ocOcclusionPass::i_renderable;
MObject ocOcclusionPass::i_scale;
MObject ocOcclusionPass::i_camera;

MObject ocOcclusionPass::i_lightwarp;
MObject ocOcclusionPass::i_envmap;
MObject ocOcclusionPass::i_envspace;
MObject ocOcclusionPass::i_maxpixdist;
MObject ocOcclusionPass::i_distr;
MObject ocOcclusionPass::i_bias;
MObject ocOcclusionPass::i_hitmode;
MObject ocOcclusionPass::i_falloffmode;
MObject ocOcclusionPass::i_falloff;
MObject ocOcclusionPass::i_samples;
MObject ocOcclusionPass::i_subset;
MObject ocOcclusionPass::i_maxdist;
MObject ocOcclusionPass::i_coneangle; 
MObject ocOcclusionPass::i_sides;
MObject ocOcclusionPass::i_maxvar;

ocOcclusionPass::ocOcclusionPass()
{
}
ocOcclusionPass::~ocOcclusionPass()
{
}

MStatus ocOcclusionPass::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;
	return MS::kUnknownParameter;
}

void* ocOcclusionPass::creator()
{
	return new ocOcclusionPass();
}

MStatus ocOcclusionPass::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnEnumAttribute	enumAttr;
	MFnStringData stringData;
	MStatus				stat;

	try
	{
		{
			i_renderable = numAttr.create ("renderable","renderable", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_renderable, atInput);
		}
		{
			i_scale = numAttr.create ("scale","scale", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0.0001);
			numAttr.setSoftMax(2.0);
			::addAttribute(i_scale, atInput);
		}
		{
			i_camera = typedAttr.create( "camera", "camera",
				MFnData::kString, stringData.create("", &stat), &stat );
			::addAttribute(i_camera, atInput);
		}
		


		{
			i_lightwarp = numAttr.create ("lightwarp","lightwarp", MFnNumericData::kDouble, 1, &stat);
			numAttr.setSoftMin(0.0);
			numAttr.setSoftMax(5.0);
			::addAttribute(i_lightwarp, atInput);
		}
		{
			i_envmap = typedAttr.create( "envmap", "envmap",
				MFnData::kString, stringData.create("", &stat), &stat );
			::addAttribute(i_envmap, atInput);
		}
		{
			i_envspace = typedAttr.create( "envspace", "envspace",
				MFnData::kString, stringData.create("", &stat), &stat );
			::addAttribute(i_envspace, atInput);
		}
		{
			i_maxpixdist = numAttr.create ("maxpixdist","maxpixdist", MFnNumericData::kDouble, 100, &stat);
			numAttr.setSoftMin(0.0);
			numAttr.setSoftMax(200);
			::addAttribute(i_maxpixdist, atInput);
		}
		{
			i_distr = enumAttr.create("distr", "distr", 0);
			enumAttr.addField("cosin", 0);
			enumAttr.addField("uniform", 1);
			::addAttribute(i_distr, atInput);
		}
		{
			i_bias = numAttr.create( "bias","bias", MFnNumericData::kDouble, -1, &stat);
			numAttr.setSoftMin(0.0);
			numAttr.setSoftMax(5.0);
			::addAttribute(i_bias, atInput);
		}
		{
			i_hitmode = enumAttr.create("hitmode", "hitmode", 0);
			enumAttr.addField("default", 0);
			::addAttribute(i_hitmode, atInput);
		}
		
		
		{
			i_falloffmode = numAttr.create( "falloffmode","falloffmode", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_falloffmode, atInput);
		}
		{
			i_falloff = numAttr.create ("falloff","falloff", MFnNumericData::kDouble, 0, &stat);
			numAttr.setSoftMin(0.0);
			numAttr.setSoftMax(5.0);
			::addAttribute(i_falloff, atInput);
		}
		{
			i_samples = numAttr.create ("samples","samples", MFnNumericData::kInt, 64, &stat);
			numAttr.setMin(1.0);
			numAttr.setSoftMax(2048);
			::addAttribute(i_samples, atInput);
		}
		{
			i_maxdist = numAttr.create ("maxdist","maxdist", MFnNumericData::kDouble, 50, &stat);
			numAttr.setSoftMin(0.0);
			numAttr.setSoftMax(100);
			::addAttribute(i_maxdist, atInput);
		}
		{
			i_coneangle = numAttr.create ("coneangle","coneangle", MFnNumericData::kDouble, 75, &stat);
			numAttr.setSoftMin(0.0);
			numAttr.setSoftMax(100);
			::addAttribute(i_coneangle, atInput);
		}
		{
			i_sides = enumAttr.create("sides", "sides", 0);
			enumAttr.addField("front", 0);
			enumAttr.addField("back", 1);
			enumAttr.addField("both", 2);
			::addAttribute(i_sides, atInput);
		}
		{
			i_maxvar = numAttr.create ("maxvar","maxvar", MFnNumericData::kDouble, 0.15, &stat);
			numAttr.setSoftMin(0.0);
			numAttr.setSoftMax(1);
			::addAttribute(i_maxvar, atInput);
		}

		if( !MGlobal::sourceFile("AEocOcclusionPassTemplate.mel"))
		{
			displayString("error source AEocOcclusionPassTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

