#include "stdafx.h"
#include "ocFile.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnPluginData.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include "ocellaris/MInstancePxData.h"
#include "ocellaris\renderFileImpl.h"

#include "ocellaris\renderFileImpl.h"
#include "ocellaris\renderOpenglImpl.h"
#include "ocellaris\renderCacheImpl.h"

#include "mathNgl\mathNgl.h"
#include "mathNmaya\mathNmaya.h"
#include "Util/misc_directory_processing.h"
#include <maya/MProgressWindow.h>

#include "ocellaris/MInstancePxData.h"

#include <maya/MGlobal.h>

MObject ocFile::i_filename;
MObject ocFile::i_proxyfilename;		
MObject ocFile::i_delay;				// ��������� � ������ deley 
MObject ocFile::i_readglobals;
MObject ocFile::i_renderSubFiles;		// �������
MObject ocFile::i_drawMode;				// �������
MObject ocFile::i_drawBox;				// �������
MObject ocFile::i_matte;				// ���������� �� �������
MObject ocFile::i_ignoreShaders;		// ������������ ������� �� �������
MObject ocFile::i_weight;				// ��� �������� (������������ � �������)
MObject ocFile::i_renderProxy;			// ��������� ������ ������ ��������, ��� �������� �������

MObject ocFile::i_addStringAllpass;
MObject ocFile::i_addStringFinalpass;
MObject ocFile::i_addStringShadowpass;

MObject ocFile::o_drawCache;
MObject ocFile::o_box;

ocFile::ocFile()
{
}
ocFile::~ocFile()
{
}

MStatus ocFile::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;

	if( plug == o_drawCache )
	{
		bool bProgress = MGlobal::mayaState()==MGlobal::kInteractive;

		MString filename = data.inputValue( i_filename, &stat ).asString();
		MString proxyfilename = data.inputValue( i_proxyfilename, &stat ).asString();

		int drawMode = data.inputValue( i_drawMode, &stat ).asShort();
		bool drawBox = data.inputValue( i_drawBox, &stat ).asBool();
		bool readglobals = data.inputValue( i_readglobals, &stat ).asBool();
		bool bViewSubFiles = data.inputValue( i_renderSubFiles, &stat ).asBool();
		bool brenderProxy = data.inputValue( i_renderProxy, &stat ).asBool();

		cls::MInstancePxData* pData = NULL;
		bool bNewData = false;
		MDataHandle outputData = data.outputValue( o_drawCache, &stat );
		MPxData* userdata = outputData.asPluginData();
		pData = static_cast<cls::MInstancePxData*>(userdata);
		MFnPluginData fnDataCreator;
		bool bNew = false;
		if( !pData)
		{
			MTypeId tmpid( cls::MInstancePxData::id );
			MObject newDataObject = fnDataCreator.create( tmpid, &stat );
			pData = static_cast<cls::MInstancePxData*>(fnDataCreator.data( &stat ));
			bNew = true;
		}
		pData->drawcache.LoadOcs(
			filename.asChar(), 
			proxyfilename.asChar(), 
			(cls::enMInstanceMode)drawMode,
			drawBox, 
			readglobals, 
			bViewSubFiles, 
			brenderProxy
			);
		if(bNew)
			outputData.set(pData);
		data.setClean(plug);
		return MS::kSuccess;
	}
	if( plug == o_box )
	{
		bool bProgress = MGlobal::mayaState()==MGlobal::kInteractive;

		MString filename = data.inputValue( i_filename, &stat ).asString();
		cls::renderFileImpl infileimpl;
		infileimpl.openForRead(filename.asChar());
		
		Math::Box3f box(1.f);
		infileimpl.GetAttribute("file::box", box);

		MDataHandle outputData = data.outputValue( plug, &stat );
		MVectorArray va(2);
		::copy( va[0], box.min);
		::copy( va[1], box.max);
		MFnVectorArrayData vad; 
		MObject val = vad.create(va);
		outputData.set(val);
		data.setClean(plug);
		return MS::kSuccess;
	}
	return MS::kUnknownParameter;
}

void* ocFile::creator()
{
	return new ocFile();
}

MStatus ocFile::initialize()
{
	MFnNumericAttribute numAttr;
	MFnEnumAttribute	enumAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		{
			i_filename = typedAttr.create ("filename","fn", MFnData::kString, &stat);
			::addAttribute(i_filename, atInput);
			i_proxyfilename = typedAttr.create ("proxyfilename","pfn", MFnData::kString, &stat);
			::addAttribute(i_proxyfilename, atInput);
			
			
//			i_mayafilename = typedAttr.create ("mayafilename","mfn", MFnData::kString, &stat);
//			::addAttribute(i_mayafilename, atInput);
//			i_mayaobjectname= typedAttr.create ("mayaobjectname","mon", MFnData::kString, &stat);
//			::addAttribute(i_mayaobjectname, atInput);

//			i_sequence = numAttr.create( "sequence","sc", MFnNumericData::kBoolean, 0, &stat);
//			::addAttribute(i_sequence, atInput|atKeyable);

//			i_renderPoints = numAttr.create( "renderPoints","rep", MFnNumericData::kBoolean, 0, &stat);
//			::addAttribute(i_renderPoints, atInput|atKeyable);

			i_drawMode = enumAttr.create("drawMode", "dm", 0);
			enumAttr.addField("as viewport", 0);
			enumAttr.addField("wireframe", 1);
			enumAttr.addField("points", 2);
			enumAttr.addField("only box", 3);
			::addAttribute(i_drawMode, atInput|atKeyable);

			i_drawBox = numAttr.create( "drawBox","db", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_drawBox, atInput|atKeyable);
			i_delay = numAttr.create( "delay","de", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_delay, atInput|atKeyable);
			i_renderSubFiles = numAttr.create( "viewSubFiles","vsf", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_renderSubFiles, atInput|atKeyable);
			i_readglobals = numAttr.create( "readglobals","rg", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_readglobals, atInput|atKeyable);
			i_matte = numAttr.create( "matte","mtt", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_matte, atInput|atKeyable);
			i_ignoreShaders = numAttr.create( "ignoreShaders","igs", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_ignoreShaders, atInput|atKeyable);
			i_renderProxy = numAttr.create( "renderProxy","rep", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_renderProxy, atInput|atKeyable);

			i_weight = numAttr.create( "weight","wei", MFnNumericData::kDouble, 1, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(1);
			::addAttribute(i_weight, atInput|atKeyable);
/*/
			i_renderDegradation = numAttr.create( "renderDegradation","red", MFnNumericData::kInt, 5, &stat);
			numAttr.setMin(1);
			numAttr.setSoftMax(10);
			::addAttribute(i_renderDegradation, atInput|atKeyable);
/*/
		}
		{
			i_addStringAllpass = typedAttr.create ("addStringAllpass","asap", MFnData::kString, &stat);
			::addAttribute(i_addStringAllpass, atInput);
			i_addStringFinalpass = typedAttr.create ("addStringFinalpass","asfp", MFnData::kString, &stat);
			::addAttribute(i_addStringFinalpass, atInput);
			i_addStringShadowpass = typedAttr.create ("addStringShadowpass","assp", MFnData::kString, &stat);
			::addAttribute(i_addStringShadowpass, atInput);
		}

		{
			o_drawCache = typedAttr.create( "drawCache", "dch", cls::MInstancePxData::id, MObject::kNullObj, &stat);
			::addAttribute(o_drawCache, atOutput|atWritable|atUnStorable);
			stat = attributeAffects( i_filename, o_drawCache);
			stat = attributeAffects( i_proxyfilename, o_drawCache);
			// ��� �� ������� ���� ��� �� ������!
//			stat = attributeAffects( i_renderDegradation, o_drawCache);
			stat = attributeAffects( i_drawMode, o_drawCache);
			stat = attributeAffects( i_drawBox, o_drawCache);
			stat = attributeAffects( i_readglobals, o_drawCache);
			stat = attributeAffects( i_renderSubFiles, o_drawCache);
			stat = attributeAffects( i_renderProxy, o_drawCache);
		}
		{
			o_box = typedAttr.create ("box","bx", MFnData::kVectorArray, &stat);
			::addAttribute(o_box, atOutput|atCached);
			stat = attributeAffects( i_filename, o_box);
		}
		if( !MGlobal::sourceFile("AEocFileTemplate.mel"))
		{
			displayString("error source AEocFileTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

