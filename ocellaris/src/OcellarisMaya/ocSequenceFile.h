#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ocSequenceFileNode.h
//
// Dependency Graph Node: ocSequenceFile

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include "ocellaris/IGeometryGenerator.h"

class ocSequenceFile : public MPxNode
{
public:
	ocSequenceFile();
	virtual ~ocSequenceFile(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_frame;
	static MObject i_filename;
	static MObject i_proxyfilename;
	static MObject i_drawMode;
	static MObject i_drawBox;
	static MObject i_delay;
	static MObject i_matte;
	static MObject i_addStringAllpass;
	static MObject i_addStringFinalpass;
	static MObject i_addStringShadowpass;
	static MObject i_renderSubFiles;
	static MObject i_ignoreShaders;
	static MObject o_drawCache;
	static MObject o_box;
	static MObject o_filename;
	static MObject o_proxyfilename;

public:
	static MTypeId id;
	static const MString typeName;

	struct Generator : public cls::IGeometryGenerator
	{
		// ���������� ���, ������ ��� �������������
		virtual const char* GetUniqueName(
			){return "ocSequenceFileGenerator";};

		virtual bool OnGeometry(
			cls::INode& node,				//!< ������ 
			cls::IRender* render,			//!< render
			Math::Box3f& box,				//!< box 
			cls::IExportContext* context,	//!< context
			MObject& generatornode			//!< generator node �.� 0
			);
	};
	static Generator generator;
};

