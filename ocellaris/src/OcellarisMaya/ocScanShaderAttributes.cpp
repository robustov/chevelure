#include "stdafx.h"
//
// Copyright (C) 
// File: ocScanShaderAttributesCmd.cpp
// MEL Command: ocScanShaderAttributes

#include "ocScanShaderAttributes.h"
#include "../generators/SlimShaderGenerator.h"
#include "ocellaris\renderCacheImpl.h"
#include "ocellaris/renderBaseCacheImpl.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>

const MString ocScanShaderAttributes::typeName( "ocScanShaderAttributes" );

ocScanShaderAttributes::ocScanShaderAttributes()
{
}
ocScanShaderAttributes::~ocScanShaderAttributes()
{
}

MSyntax ocScanShaderAttributes::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.addArg(MSyntax::kString);	// node.attr
	syntax.addFlag("t", "type", MSyntax::kString);
	return syntax;
}

class renderShaderInfo : public cls::renderImpl<cls::TAttributeStack_minimal, cls::TTransformStack_pure>
{
public:
	std::string shadertest;
	std::string shadername;
public:
	renderShaderInfo(const char* shadertest)
	{
		this->shadertest = shadertest;
		if( strncmp(shadertest, "oc_", 3)==0)
		{
			this->shadertest = shadertest+3;
		}
	}
	virtual void Shader(const char* shadertype)
	{
		cls::PS shadername = GetParameter("shader::name");
		if( strcmp(shadertest.c_str(), shadertype)==0)
		{
			this->shadername = shadername.data();
		}
	}
};

MStatus ocScanShaderAttributes::doIt( const MArgList& args)
{
	/*/
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MString argsl0;
	argData.getCommandArgument(0, argsl0);

	MString shadertest = "surface";
	argData.getFlagArgument("t", 0, shadertest);

	MObject obj;
	if( !nodeFromName(argsl0, obj))
	{
		return MS::kFailure;
	}

	ChiefGenerator chief;
	cls::IExportContext context;
	ocellarisDefaultGenerators(OCGT_SHADERS, chief, &context);

	MDagPath path;
	path = MDagPath::getAPathTo(obj);
	cls::INode* pNode = ocNodeTree(path, chief, &context);

	renderShaderInfo render(shadertest.asChar());
	Math::Box3f box;
	pNode->Render(&render, box, Math::Matrix4f::id, &context);
	pNode->Release();

	this->setResult(render.shadername.c_str());
	/*/

	return MS::kSuccess;
}

void* ocScanShaderAttributes::creator()
{
	return new ocScanShaderAttributes();
}

