#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ocRootTransformNode.h
//
// Dependency Graph Node: ocRootTransform

#include <maya/MPxTransform.h>
#include <maya/MPxTransformationMatrix.h>

class ocRootTransform : public MPxTransform
{
public:
	ocRootTransform();
	virtual ~ocRootTransform(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	MStatus computeOutput(
		const MPlug& plug, 
		MDataBlock& data);

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_input;
	static MObject o_drawCache;		// Example output attribute

public:
	static MTypeId id;
	static const MString typeName;
};

