#include "stdafx.h"

bool fixDisplayInRib(const char* ribfile)
{
	FILE* file = fopen(ribfile, "r+b");
	if( !file)
		return false;

	const char display[] = "Display";

	long curtell = -1;
	int curfind = 0;
	char buf[8];
	int buflen = sizeof(buf);
	for( ; !feof(file); )
	{
		int readed = fread(buf, buflen, 1, file);
		for(int i=0; i<buflen; i++)
		{
			if(display[curfind]==0)
			{
				// ��������� �� ' ' ��� 0xa0
				unsigned char c = buf[i];
				if( c==' ' || c==0xa0)
				{
					//�����! 
					fseek( file, curtell, SEEK_SET);
					char c = '#';
					fwrite(&c, 1, 1, file);
					fclose( file);
					return true;
				}
			}

			if( display[curfind]==buf[i])
			{
				if(curfind==0)
					curtell = ftell(file)-(buflen-i);
				curfind++;
			}
			else
			{
				curfind = 0;
				curtell = -1;
			}
		}
	}
	fclose( file);
	return true;
}

void CropRib(
	const char* ribfile, 
	const char* displaycmd, 
	std::vector<float> xcrops, 
	std::vector<float> ycrops, 
	std::vector<std::string>& outribfiles, 
	std::string& outasmstring 
	)
{
	int X = xcrops.size()+1;
	int Y = ycrops.size()+1;

	fixDisplayInRib(ribfile);
}