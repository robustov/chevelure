#include "stdafx.h"
#include "ocTranslator.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>

#include <maya/MGlobal.h>
#include <maya/MFileIO.h>

#include "Util/currentHostName.h"
#include "ocellaris/OcellarisExport.h"
#include "ocellaris/renderFileImpl.h"
#include "Util/misc_create_directory.h"

#include "mathNmaya/mathNmaya.h"

#include "../ocellarisExport/ExportContextImpl.h"


ocTranslator::ocTranslator()
{
}
ocTranslator::~ocTranslator()
{
}

MStatus ocTranslator::reader ( 
	const MFileObject& file,
	const MString& optionsString,
	FileAccessMode mode)
{
	return (MS::kFailure);
}
///
MStatus ocTranslator::writer ( 
	const MFileObject& file,
	const MString& optionsString,
	FileAccessMode mode)
{
	MStatus stat;
	std::string filter = optionsString.asChar();
//	displayString(filter.c_str());
	bool bBynary = false;
	std::string roottransformgen;

	char *token = strtok((char*)filter.c_str(), ";");
	while( token != NULL )
	{
		std::string filter = token;
		if( filter == "FileFormat=Ascii")
		{
			bBynary = false;
		}
		else if( filter == "FileFormat=Binary")
		{
			bBynary = true;
		}
		if(strncmp(token, "RTG=", 4)==0)
		{
			roottransformgen = token + 4;
		}
		token = strtok( NULL, ";" );
	}

	bool bMotionVelocity = false;
	bool bMayaShaders = false;

	std::string projectDir;
	{
		MString workspacedir;
		MGlobal::executeCommand("workspace -fn", workspacedir);
		projectDir = workspacedir.asChar();
	}
	// folder
	std::string folder = Util::extract_foldername(file.fullName().asChar());
	Util::create_directory(folder.c_str());

	ExportContextImpl context;
	context.setEnvironment( "WORKSPACEDIR", projectDir.c_str());
	context.setEnvironment( "OUTPUTDIR", folder.c_str());
	context.setEnvironment( "SHADERDIR", folder.c_str());
	context.setEnvironment( "OCSDIR", folder.c_str());
	context.setEnvironment( "RIBDIR", folder.c_str());

	context.setEnvironment("OUTPUTFILENAME", file.fullName().asChar());
	context.setEnvironment("OUTPUTFORMAT", (!bBynary)?("ascii"):("binary"));
	context.setEnvironment("SRCFILENAME", MFileIO::currentFile().asChar());
	context.setEnvironment("SRCHOSTNAME", Util::currentHostName().c_str());


	/////////////////////////////////////////////////
	// ���������� !!!
	context.addDefaultGenerators("");
	if( bMayaShaders)
		context.addGeneratorForType("shape", "MayaShader");

	int bUseMtor = 0;
	MGlobal::executeCommand("exists mtor", bUseMtor);
	if( bUseMtor)
		context.addGeneratorForType("shape", "SlimShader");

	// SimpleExport_PassGenerator
	cls::IPassGenerator* passgenerator = (cls::IPassGenerator*)context.loadGenerator("SimpleExportPass", cls::IGenerator::PASS);
	if( !passgenerator) 
		return (MS::kFailure);

	// roottransformgen
	// ���� ����� �������� �������!!!
	context.addGeneratorForType("roottransform", roottransformgen.c_str());
	
	if(bMotionVelocity)
	{
		context.addGeneratorForType("mesh", "VertexVelocityAttribute");
	}

	context.prepareGenerators();


	MObject obj;
	if( mode==kSaveAccessMode || mode==kExportAccessMode )
	{
		MItDag it;
		obj = it.item();
		MDagPath path;
		it.getPath(path);

		context.addnodes(path, "");
	}
	else
	{
		Math::Box3f box;
		MSelectionList sl;
		MGlobal::getActiveSelectionList(sl);
		if( sl.length()==0)
			return MS::kFailure;

		std::list<MDagPath> list;
		for(int i=0; i<(int)sl.length(); i++)
		{
			MDagPath dagPath;
			if( !sl.getDagPath( i, dagPath))
				continue;
			context.addnodes(dagPath, "");
		}
	}

	context.addpass(passgenerator, "");

	MTime t = MAnimControl().currentTime();
	double frame = t.as(t.uiUnit());
	context.addframe((float)frame);

	context.preparenodes();

	context.doexport("", "", NULL);
	/*/
	cls::IRender* render = &_render;
	render->comment(comment1.c_str());
	render->comment(comment2.c_str());
	render->Attribute("file::box", box);
	cache.RenderGlobalAttributes(&_render);
	render->PushAttributes();
	cache.Render(render);
	render->PopAttributes();
	/*/

	return (MS::kSuccess);
}
///
bool ocTranslator::haveReadMethod () const
{
	return true;
}
///
bool ocTranslator::haveWriteMethod () const
{
	return true;
}
///
bool ocTranslator::haveNamespaceSupport () const
{
	return false;
}
///
MString ocTranslator::defaultExtension () const
{
	return MString("ocs");
}
///
MString ocTranslator::filter () const
{
	return MString("*.ocs");
}
///
bool ocTranslator::canBeOpened () const
{
	return true;
}
///
MPxFileTranslator::MFileKind ocTranslator::identifyFile (	
	const MFileObject& file,
	const char* buffer,
	short size) const
{
	return (kNotMyFileType);
}

void* ocTranslator::creator()
{
	return new ocTranslator();
}
