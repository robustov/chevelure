import ocsCommands
import string
#from ctypes import *
import maya.cmds as cmds
import maya.mel as mel
import maya.OpenMaya as OpenMaya
import maya.OpenMayaAnim as anim
import os.path

def export(
			nodename,					# what export
			filename,					# destination file
			ascii=False,				# ascii or binary
			slimCopyShaders=True,		# copy shaders to export directory
			bDump = False,				# dump information then export
			mayaShaders = False,		# export maya shaders
			slimShaders = False,		# export slim shaders
			boundingbox = "",			# manual boubding box (format: minx, miny, minz, maxx, maxy, maxz). 
			exporttemplated = False,	# export templated nodes
			ocscomment = "",				# comment. will beinserted into ocs file header
			lazyinstancergenerator = False	# instancer generator not save objects files
			):
	reload (ocsCommands)

	print "clear:"
	ocsCommands.clear()

	if ascii :
		ocsCommands.setenv("OUTPUTFORMAT", "ascii")
	else:
		ocsCommands.setenv("OUTPUTFORMAT", "binary")
		
	if slimCopyShaders :
		ocsCommands.setenv("SLIMCOPYSHADERS", "1")
	else:
		ocsCommands.setenv("SLIMCOPYSHADERS", "0")

	directory = os.path.dirname(filename);
	ocsCommands.setenv("OUTPUTFILENAME", filename)
	if boundingbox!="" :
		ocsCommands.setenv("BOUNDINGBOX", boundingbox);
		
	if exporttemplated:
		ocsCommands.setenv("EXPORTTEMPLATED", "1");

	if ocscomment!="" :
		ocsCommands.setenv("OCSCOMMENT", ocscomment);
	
	if lazyinstancergenerator :
		ocsCommands.setenv("LAZYINSTANCERGENERATOR", "1");

	# envinronment
	print "envinronments:"
	setupEnv(nodename, directory);
	if bDump :
		ocsCommands.dump("-environments");

	# generators
	print "generators:"
	ocsCommands.addDefaultGenerators("")
	if mayaShaders:
		ocsCommands.addGeneratorForType("shape", "MayaShader")
		
	if slimShaders and mel.eval("exists mtor") :
		ocsCommands.addGeneratorForType("shape", "SlimShader")
	ocsCommands.preparegenerators()
	
	if bDump :
		ocsCommands.dump("-generators")

	# add root node
	print "addnodes:"
	ocsCommands.addnodes(nodename)
	if bDump :
		ocsCommands.dump("-nodes")

	# passes
	print "add pass:"
	passoptions = "Attribute uniform string filename=\""+slashUnix(filename)+"\";\n"
	ocsCommands.addpassgenerator("SimpleExportPass", passoptions)

	if bDump :
		ocsCommands.dump("-passes")

	animControl = anim.MAnimControl
	t = animControl.currentTime()
	frame = t.asUnits(t.uiUnit())
	ocsCommands.addframe(frame)
		
	if bDump :
		ocsCommands.dump("-frames")

	# prepare nodes
	print "prepare nodes:"
	ocsCommands.preparenodes()

	# export
	print "export:"
	dumpoptions = "";
	if bDump :
		dumpoptions = "-dump";
		#dumpoptions = "-dumptree";
	options = "";
	res = ocsCommands.export(dumpoptions, options, "ocellaris", "export")
	
	ocsCommands.clear()
	
	if res==0:
		# user cancel
		return

			

def exportAnimation(
			nodename, directory, filename, 
			startFrame=0, endFrame=25, 
			ascii=False, 
			singleFile=False, 
			slimCopyShaders=True, 
			bDump = False, 
			mayaShaders = False, 
			slimShaders = False,
			ocscomment = "",				# comment. will beinserted into ocs file header
			lazyinstancergenerator = False	# instancer generator not save objects files
			):
	reload (ocsCommands)

	if singleFile:
		passname="AnimationExportPass"
	else:
		passname="SimpleExportPass"

	print "clear:"
	ocsCommands.clear()

	if ascii :
		ocsCommands.setenv("OUTPUTFORMAT", "ascii")
	else:
		ocsCommands.setenv("OUTPUTFORMAT", "binary")
		
	if slimCopyShaders :
		ocsCommands.setenv("SLIMCOPYSHADERS", "1")
	else:
		ocsCommands.setenv("SLIMCOPYSHADERS", "0")

	if ocscomment!="" :
		ocsCommands.setenv("OCSCOMMENT", ocscomment);
	
	if lazyinstancergenerator :
		ocsCommands.setenv("LAZYINSTANCERGENERATOR", "1");

	# envinronment
	print "envinronments:"
	setupEnv(nodename, directory);
	if bDump :
		ocsCommands.dump("-environments");

	# generators
	print "generators:"
	ocsCommands.addDefaultGenerators("")
	if mayaShaders:
		ocsCommands.addGeneratorForType("shape", "MayaShader")
		
	if slimShaders and mel.eval("exists mtor") :
		ocsCommands.addGeneratorForType("shape", "SlimShader")
	ocsCommands.preparegenerators()
	
	if bDump :
		ocsCommands.dump("-generators")

	# add root node
	print "addnodes:"
	ocsCommands.addnodes(nodename)
	if bDump :
		ocsCommands.dump("-nodes")

	# passes
	print "add pass:"
	fn = directory+"/"+filename;
	passoptions = "Attribute uniform string filename=\""+slashUnix(fn)+"\";\n"
	ocsCommands.addpassgenerator(passname, passoptions)

	if bDump :
		ocsCommands.dump("-passes")

	# prepare frames
	print "prepare frames:"
	eachFrame  = 1
	for i in range(startFrame, endFrame+1):
		ocsCommands.addframe(i)
		
	# prepare nodes
	print "prepare nodes:"
	ocsCommands.preparenodes()

	if bDump :
		ocsCommands.dump("-frames")

	# export
	print "------------------------------"
	print "EXPORT:"
	dumpoptions = "";
	if bDump :
		dumpoptions = "-dump";
		#dumpoptions = "-dumptree";
	options = "";
	res = ocsCommands.export(dumpoptions, options, "ocellaris", "export")

	ocsCommands.clear()

	if res==0:
		# user cancel
		return




def slashUnix(str):
	out = str.replace("\\", "/");
	return out


def setupEnv(passname, directory):
	# WORKSPACEDIR
	wd = mel.eval( "workspace -fn");
	scenename = mel.eval( "file -q -shn -l");

	ocsCommands.setenv("WORKSPACEDIR", wd)
	
	# JOBNAME = scenename+passname
	jobname = scenename[0]+"_"+passname
	ocsCommands.setenv("JOBNAME", jobname)
	
	ocsCommands.setenv("RIBDIR", directory)
	ocsCommands.setenv("SHADOWDIR", directory);
	ocsCommands.setenv("OUTPUTDIR", directory);
	ocsCommands.setenv("SHADERDIR", directory);

	RATTREE = mel.eval( "getenv(\"RATTREE\")")
	RMSTREE = mel.eval( "getenv(\"RMSTREE\")")
	RMANTREE = mel.eval( "getenv(\"RMANTREE\")")

	#SHADERCOMPILER
	#ocsCommands.setenv("SHADERCOMPILER", "%RMANTREE%/bin/shader.exe -o %s %s");
	compiler = RMANTREE+"/bin/shader.exe -autoplug -C \"-I%ULITKABIN%/include\" -o \"%s\" \"%s\""
	ocsCommands.setenv("SHADERCOMPILER", compiler)
	
