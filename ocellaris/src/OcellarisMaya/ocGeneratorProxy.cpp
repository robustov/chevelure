#include "stdafx.h"
#include "ocGeneratorProxy.h"
#include "ocellaris/IAttributeGenerator.h"
#include "ocellaris/renderStringImpl.h"


#include <maya/MGlobal.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnStringData.h>

#include "Util/DllProcedure.h"

MObject ocGeneratorProxy::i_referObject;
//MObject ocGeneratorProxy::i_type;
//MObject ocGeneratorProxy::i_generator;
//MObject ocGeneratorProxy::i_calltypeAttribute;
MObject ocGeneratorProxy::i_addStringAllpass;

ocGeneratorProxy::ocGeneratorProxy()
{
}
ocGeneratorProxy::~ocGeneratorProxy()
{
}

bool ocGeneratorProxy::setInternalValue( 
	const MPlug& plug,
	const MDataHandle& data)
{
	/*/
	if(plug==i_generator)
	{
		MString nameval = data.asString();
		Util::DllProcedure dll;
		if( dll.LoadFormated(nameval.asChar(), "OcellarisExport"))
		{
			cls::GENERATOR_CREATOR pc = (cls::GENERATOR_CREATOR)dll.proc;
			cls::IGenerator* gen = (*pc)();
			if( gen)
			{
				// ��� �������� � ����
				gen->OnLoadToMaya(thisMObject());
			}
		}
	}
	/*/
	return MPxNode::setInternalValue( 
		plug,
		data);
}

MStatus ocGeneratorProxy::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;

	return MS::kUnknownParameter;
}

bool ocGeneratorProxy::isBounded() const
{
	MObject thisNode = thisMObject();
	return false;
}

MBoundingBox ocGeneratorProxy::boundingBox() const
{
	MObject thisNode = thisMObject();

	MBoundingBox box;
	//...
	return box;
}

void ocGeneratorProxy::draw(
	M3dView &view, 
	const MDagPath &path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status)
{ 
	MObject thisNode = thisMObject();

	view.beginGL(); 
	//...
	view.endGL();
};


void* ocGeneratorProxy::creator()
{
	return new ocGeneratorProxy();
}

MStatus ocGeneratorProxy::initialize()
{
	MFnMessageAttribute mesAttr;
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnEnumAttribute enumAttr;
	MFnStringData stringData;
	MStatus				stat;

	try
	{
		{
			i_referObject = mesAttr.create( "referObject", "robj");
			::addAttribute(i_referObject, atInput);
		}
		/*/
		{
			i_type = enumAttr.create ("generatorType", "gent");
			enumAttr.addField("ATTRIBUTE", cls::IGenerator::ATTRIBUTE);
			enumAttr.addField("DEFORM", cls::IGenerator::DEFORM);
			enumAttr.addField("GEOMETRY", cls::IGenerator::GEOMETRY);
			enumAttr.addField("LAYER", cls::IGenerator::LAYER);
			enumAttr.addField("NODEFILTER", cls::IGenerator::NODEFILTER);
			enumAttr.addField("NODE", cls::IGenerator::NODE);
			enumAttr.addField("PASS", cls::IGenerator::PASS);
			enumAttr.addField("SHADER", cls::IGenerator::SHADER);
			enumAttr.addField("TRANSFORM", cls::IGenerator::TRANSFORM);
			::addAttribute(i_type, atInput);
		}
		/*/
		/*/
		{
			i_generator = typedAttr.create( "generatorName", "genn",
				MFnData::kString, stringData.create(&stat), &stat);
			typedAttr.setInternal(true);
			::addAttribute(i_generator, atInput);
		}
		{
			i_calltypeAttribute = enumAttr.create ("calltypeAttribute", "catat", cls::IAttributeGenerator::CT_BEFORE_TRANSFORM);
			enumAttr.addField("BEFORE_TRANSFORM",	cls::IAttributeGenerator::CT_BEFORE_TRANSFORM);
			enumAttr.addField("BEFORE_NODE",		cls::IAttributeGenerator::CT_BEFORE_NODE);
			enumAttr.addField("BEFORE_SHADER",		cls::IAttributeGenerator::CT_BEFORE_SHADER);
			enumAttr.addField("BEFORE_GEOMETRY",	cls::IAttributeGenerator::CT_BEFORE_GEOMETRY);
			::addAttribute(i_calltypeAttribute, atInput);
		}
		/*/

		{
			i_addStringAllpass = typedAttr.create ("addStringAllpass","asap", MFnData::kString, &stat);
			::addAttribute(i_addStringAllpass, atInput);
//			i_addStringFinalpass = typedAttr.create ("addStringFinalpass","asfp", MFnData::kString, &stat);
//			::addAttribute(i_addStringFinalpass, atInput);
//			i_addStringShadowpass = typedAttr.create ("addStringShadowpass","assp", MFnData::kString, &stat);
//			::addAttribute(i_addStringShadowpass, atInput);
		}

		if( !MGlobal::sourceFile("AEocGeneratorProxyTemplate.mel"))
		{
			displayString("error source AEocGeneratorProxyTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

ocGeneratorProxy::Generator ocGeneratorProxy::generator;

//! OnProcess
MObject ocGeneratorProxy::Generator::OnProcess(
	cls::INode* node,
	cls::IExportContext* context,
	MObject& generatornode
	)
{
	MFnDependencyNode dn(node->getObject());
	MPlug plug(node->getObject(), ocGeneratorProxy::i_referObject);
	
	MPlugArray array;
	plug.connectedTo( array, true, false);
	if(array.length()==0)
	{
		// ���� ���� ��������� - ��������� ���� ����� ������ �������
		MFnDagNode dan(node->getObject());
		MObject parent = dan.parent(0);

		MFnDagNode danparent(parent);
		for( int i=0; i<(int)danparent.childCount(); i++)
		{
			MObject ch = danparent.child( i);
			MFnDependencyNode dn(ch);
			if( dn.typeId() == ocGeneratorProxy::id)
				continue;
			node->AddGenerator(&deformgenerator);
			return ch;
		}
		return node->getObject();
	}
	plug = array[0];
	MObject externnode = plug.node();

	if( externnode.hasFn(MFn::kSet))
	{
		// �� - ��� �� ������!!!
		MFnSet set(externnode);
		MSelectionList sl;
		set.getMembers(sl, true);
		for( int i=0; i<(int)sl.length(); i++)
		{
			MDagPath dag;
			if( !sl.getDagPath(i, dag)) continue;
		}
	}
	return externnode;
}

ocGeneratorProxy::DeformGenerator ocGeneratorProxy::deformgenerator;

void ocGeneratorProxy::DeformGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	cls::enSystemCall call,			//!< ��� ������ ��������� ��������: I_POINTS, I_CURVES, I_MESH ...
	cls::IExportContext* context, 
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MDagPath path = inode.getPath();
	MObject node = path.node();

	MString addStringAllpass;
	MPlug(node, ocGeneratorProxy::i_addStringAllpass).getValue(addStringAllpass);
	addStringAllpass += "\n";
	if(render)
	{
		// ALL PASS
		{
			cls::renderStringImpl fileimpl;
			if( fileimpl.openForRead(addStringAllpass.asChar()))
				fileimpl.Render(render);
			else
				displayString("read addStringAllpass FAILURE");
		}
	}
}

