//
// Copyright (C) 
// File: ocExportCmd.cpp
// MEL Command: ocExport

#include "stdafx.h"

#include <maya/MGlobal.h>
#include <maya/MAnimControl.h>
#include <maya/MFnParticleSystem.h>

#include "ocInstance.h"
#include "ocParticleInstancer.h"
#include "Util/misc_create_directory.h"
#include "mathNmaya/mathNmaya.h"

#include "ocellaris/OcellarisExport.h"
#include "ocellaris/renderStringImpl.h"
#include "ocellaris/renderFileImpl.h"
#include "ocellaris/renderBlurParser.h"
#include "ocellaris/renderCacheImpl.h"
#include "ocellaris/ChiefGenerator.h"

#include "mathNpixar/IPrman.h"
#include "ocInstance.h"
#include "ocParticleInstancer.h"

#include "ocExport_InstancerPhases.h"
#include "ocExport_InstancerData.h"
#include "ocGeneratorProxy.h"

// ocExport group1 -o "u:/test.ma" -t 0

////////////////////////////////////////
//
// �������. ���������� �� �������
// 

extern "C" {
    RtVoid dl_FurProceduralFree(RtPointer data)
	{
		RtString* sData = (RtString*) data;
		free(sData[0]);
		free(sData[1]);
		free(sData);
    }
}

void* CreateCacheFrameOpenState(
	const char* argsl0,
	const char* filename,
	const char* passname,
	bool bTree,
	bool blur,
	double shutterOpen, 
	double shutterClose, 
	double shutterOpenFrame, 
	double shutterCloseFrame,
	bool bAscii,
	bool bView, 
	int number
	)
{
	return NULL;
}
void FreeCacheFrameOpenState(void* a)
{
}

double allign(double v)
{
	double x = v*1024;
	x = floor(x+0.5);
	return x/1024;
}
bool ExportOcNode(
	const char* argsl0,
	const char* filename,
	const char* passname,
	float* camera, 
	bool bTree,
	bool blur,
	double shutterOpen, 
	double shutterClose, 
	double shutterOpenFrame, 
	double shutterCloseFrame,
	double time,
	bool bAscii,
	bool bView, 
	bool bDump,
	IPrman* prman, 
	cls::IExportContext* context,
	const char* additionalString, 
	bool DoExportToThisFile
	)
{
	MTime curtime = MAnimControl::currentTime();

	Math::Matrix4f mview(camera);

	// ��������� �� 0.001
	shutterOpenFrame = allign(shutterOpenFrame);
	shutterCloseFrame = allign(shutterCloseFrame);

	if( bDump)
		displayString("Export %s %s curtime=%f(%f) shutteropen = %f(%f) shutterclose = %f(%f)", passname, argsl0, curtime.as(MTime::kSeconds), curtime.as(MTime::uiUnit()), shutterOpenFrame, shutterOpen, shutterCloseFrame, shutterClose);

	Math::Box3f boxerror(0);
	MStatus stat = MS::kSuccess;

	std::string attributes = "";
	std::string outfilename="";
	outfilename = filename;

	_rmtmp();
	int bBynary = !bAscii;

	MObject obj;
	if( !nodeFromName(argsl0, obj))
		return false;

	MFnDependencyNode dn(obj);

	bool primaryVisibility=true, castsShadows=true;
	MPlug plug;
	plug = dn.findPlug( "primaryVisibility");
	if(!plug.isNull())
		plug.getValue( primaryVisibility);
	plug = dn.findPlug( "castsShadows");
	if(!plug.isNull())
		plug.getValue( castsShadows);
	
	if( strcmp(passname, "final")==0 && !primaryVisibility) 
		return true;
	if( strcmp(passname, "shadow")==0 && !castsShadows) 
		return true;

	if( strcmp(passname, "shadow")==0)
	{
		blur = false;
		MAnimControl::setCurrentTime( MTime(shutterOpenFrame, MTime::uiUnit()));
		if( bDump)
			displayString("pass==shadow stop BLUR");
	}

	Math::Box3f box;
	MDagPath path;
	MDagPath::getAPathTo(obj, path);

	Math::Matrix4f worldpos;
	copy( worldpos, path.inclusiveMatrix());

	if(DoExportToThisFile)
	{
		cls::renderFileImpl _render;
		cls::IRender* render = &_render;
		Util::create_directory_for_file(filename);
		if( filename==0 || filename[0]==0 || 
			(!_render.openForWrite(filename, !bBynary)))
		{
			displayStringError("ExportOc: renderFileImpl cant create %s file!!!", filename);
			return false;
		}

		//! ���������� �� ���������
		ChiefGenerator chief;
		ocellarisDefaultGenerators(OCGT_SCENE, chief, context);
		chief.addGenerator(ocGeneratorProxy::id, &ocGeneratorProxy::generator);

		cls::INode* pNode = NULL;
		if( dn.typeId()==ocGeneratorProxy::id)
		{
			// ����� ���
			MPlug plug(obj, ocGeneratorProxy::i_referObject);
			MPlugArray array;
			plug.connectedTo( array, true, false);
			if(array.length()==0)
			{
				pNode = ocNodeTree(path, chief, context);
			}
			else
			{
				plug = array[0];
				MObject externnode = plug.node();
				if( externnode.hasFn(MFn::kSet))
				{
					pNode = ocNodeTree_fromSet(externnode, chief, context);
					pNode->AddGenerator(&ocGeneratorProxy::deformgenerator);
				}
				else
				{
					MDagPath path;
					MDagPath::getAPathTo(externnode, path);
					pNode = ocNodeTree(path, chief, context);
				}
			}
		}
		else
		{
			pNode = ocNodeTree(path, chief, context);
		}

		//! ���������� ������ �������������� �������� INode
		if( !pNode)
			return false;
		if(bDump)
			pNode->Dump(stdout);
		cls::P<int> bsp = ocExport_GetAttrValue( obj, "ocellarisBlurSamples");
		int bs = 2;
		if(!bsp.empty()) bs = bsp.data();

		//! �� ���� �������
		if(!blur || bs<2)
		{
			// ���� �����
			pNode->Render(render, box, Math::Matrix4f::id, context);
		}
		else
		{
			static bool bTestBlur = false;
			if( bTestBlur)
			{
				cls::renderFileImpl _render;
				_render.openForWrite("c:/temp/blur.ocs", true);
				cls::IRender* render = &_render;
				// phase0
				render->MotionBegin();

				render->MotionPhaseBegin(0);
				MAnimControl::setCurrentTime( MTime(shutterOpenFrame, MTime::uiUnit()));
				pNode->Render(render, box, Math::Matrix4f::id, context);
				render->MotionPhaseEnd();

				// phase1
				render->MotionPhaseBegin(1);
				MAnimControl::setCurrentTime( MTime(shutterCloseFrame, MTime::uiUnit()));
				pNode->Render(render, box, Math::Matrix4f::id, context);
				render->MotionPhaseEnd();

				render->MotionEnd();
			}

			// ���� 
			cls::renderBlurParser<> blurparser;
			// phase0
			blurparser.SetCurrentPhase(0.f);
			MAnimControl::setCurrentTime( MTime(shutterOpenFrame, MTime::uiUnit()));
			pNode->Render(&blurparser, box, Math::Matrix4f::id, context);

			for( int i=1; i<bs-1; i++)
			{
				float p = i/((float)bs-1);
				MTime t = MTime( shutterOpenFrame*(1-p)+shutterCloseFrame*p, MTime::uiUnit());
				blurparser.SetCurrentPhase(p);
				MAnimControl::setCurrentTime(t);
				pNode->Render(&blurparser, box, Math::Matrix4f::id, context);
			}

			// phase1
			blurparser.SetCurrentPhase(1.f);
			MAnimControl::setCurrentTime( MTime(shutterCloseFrame, MTime::uiUnit()));
			pNode->Render(&blurparser, box, Math::Matrix4f::id, context);

//	blurparser.Dump();
			blurparser.Render(render);
		}

		if( pNode)
			pNode->Release();

		/*/
//		render->Attribute("file::box", box);

		if( bTree)
		{
			if( !obj.hasFn(MFn::kTransform))
			{
				MFnDagNode dag(obj);
				obj = dag.parent(0);
			}
			ocExportTree(obj, chief, render, box, Math::Matrix4f::id, context);
		}
		else
			ocExportTree(obj, chief, render, box, Math::Matrix4f::id, context);
		/*/
	}
	else
	{
		cls::renderFileImpl infileimpl;
		if( !infileimpl.openForRead(outfilename.c_str()))
		{
			displayStringError("File %s not found", outfilename.c_str());
			return false;
		}

		// Box!
		if( !infileimpl.GetAttribute("file::box", box))
		{
			displayStringError("Attribute file::box not found in %s", outfilename.c_str());
			return false;
		}
	}

	cls::renderStringImpl stringrender;
	stringrender.openForWrite();
	cls::IRender* prescene = &stringrender;
		prescene->Attribute("@pass", passname);
		prescene->Attribute("@camera::worldpos", mview);
		prescene->Attribute("@dso::worldtransform", worldpos);
	attributes += stringrender.getWriteBuffer();

	RtString *data = static_cast<RtString*>(malloc(sizeof(RtString) * 3));
	data[0] = strdup("OcellarisPrmanDSO.dll");
	int textsize = 10000;
	data[1] = static_cast<char*>(malloc(sizeof(char)*(textsize+2)));
	_snprintf(data[1], textsize, "%s$%f$%f$%f$%s", outfilename.c_str(), time, shutterOpen, shutterClose, attributes.c_str());
	data[2] = 0;

	RtBound bound;
	bound[0] = box.min.x;
	bound[1] = box.max.x;
	bound[2] = box.min.y;
	bound[3] = box.max.y;
	bound[4] = box.min.z;
	bound[5] = box.max.z;
	prman->RiProcedural(data, bound, prman->RiProcDynamicLoad(), dl_FurProceduralFree);

	return true;
}











