#include "stdafx.h"
#include "ocShaderParameter.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnStringData.h>

#include <maya/MGlobal.h>

MObject ocShaderParameter::i_input;

MObject ocShaderParameter::i_useMtorSurfaceShader;
MObject ocShaderParameter::i_surfaceShader;
MObject ocShaderParameter::i_surfaceShaderAttrs;

MObject ocShaderParameter::i_useMtorDisplaceShader;
MObject ocShaderParameter::i_displaceShader;
MObject ocShaderParameter::i_displaceShaderAttrs;
//MObject ocShaderParameter::i_displaceBound;
//MObject ocShaderParameter::i_displaceSpace;

MObject ocShaderParameter::o_output;

ocShaderParameter::ocShaderParameter()
{
}
ocShaderParameter::~ocShaderParameter()
{
}

MStatus ocShaderParameter::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;

	if( plug == o_output )
	{
		data.inputValue( i_surfaceShader, &stat );
		data.inputValue( i_displaceShader, &stat );

		MDataHandle outputData = data.outputValue( plug, &stat );
		int& out = outputData.asInt();
		out++;

		data.setClean(plug);
		return MS::kSuccess;
	}
	return MS::kUnknownParameter;
}

void* ocShaderParameter::creator()
{
	return new ocShaderParameter();
}

MStatus ocShaderParameter::initialize()
{
	MFnMessageAttribute mesAttr;
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnEnumAttribute enumAttr;
	MStatus				stat;

	try
	{
		i_input = mesAttr.create( "input", "iin", &stat);
		::addAttribute(i_input, atInput);

		// surface
		{
			i_useMtorSurfaceShader = numAttr.create("surfaceShaderUseMtor", "umss", MFnNumericData::kBoolean, 0);
			numAttr.setDefault(true);
			::addAttribute(i_useMtorSurfaceShader, atInput);
			i_surfaceShader = typedAttr.create ("surfaceShader", "ss", MFnData::kString, &stat);
			::addAttribute(i_surfaceShader, atInput);

			i_surfaceShaderAttrs = typedAttr.create( "surfaceShaderAttrs", "ssa", MFnData::kStringArray, &stat);
			::addAttribute(i_surfaceShaderAttrs, atInput|atHidden);
		}

		// displace
		{
			i_useMtorDisplaceShader = numAttr.create("displaceShaderUseMtor", "umds", MFnNumericData::kBoolean, 0);
			numAttr.setDefault(true);
			::addAttribute(i_useMtorDisplaceShader, atInput);
			i_displaceShader = typedAttr.create ("displaceShader", "ds", MFnData::kString, &stat);
			::addAttribute(i_displaceShader, atInput);

			/*/
			i_displaceBound = numAttr.create("displaceBound", "db", MFnNumericData::kDouble, 0);
			numAttr.setMin(0);
			numAttr.setSoftMax(2);
			::addAttribute(i_displaceBound, atInput);
			MFnStringData strData;
			i_displaceSpace = typedAttr.create ("displaceSpace", "dsp", MFnData::kString, strData.create("shader"), &stat);
			::addAttribute(i_displaceSpace, atInput);
			/*/
			
			i_displaceShaderAttrs = typedAttr.create( "displaceShaderAttrs", "dsa", MFnData::kStringArray, &stat);
			::addAttribute(i_displaceShaderAttrs, atInput|atHidden);
		}
		{
			o_output = numAttr.create( "o_output", "out", MFnNumericData::kInt, 0.0, &stat);
			::addAttribute(o_output, atOutput);
			stat = attributeAffects( i_surfaceShader, o_output );
			stat = attributeAffects( i_displaceShader, o_output );
		}
		if( !MGlobal::sourceFile("AEocShaderParameterTemplate.mel"))
		{
			displayString("error source AEocShaderParameterTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

