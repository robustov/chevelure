#pragma once

#include "pre.h"
#include <maya/MPxLocatorNode.h>
#include "ocellaris/IGeometryGenerator.h"
 
class ocLight : public MPxLocatorNode
{
public:
	ocLight();
	virtual ~ocLight(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	virtual void draw(
		M3dView &view, const MDagPath &path, 
		M3dView::DisplayStyle style,
		M3dView::DisplayStatus status);

	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_color;
	static MObject i_intensity;
	static MObject i_dropoff;
	static MObject i_coneAngle;
	static MObject i_penumbraAngle;
	static MObject i_decayRate;

	// Falloff
	static MObject i_Falloff;
	static MObject i_EndPersent;
	static MObject i_FalloffDist;
	static MObject i_StartDist;

	// visualisation
	static MObject i_viewDistance;
	static MObject i_bViewFalloff;
	static MObject i_bViewIsoparma;
	static MObject i_viewAttenuation;

//	static MObject i_input;
	static MObject o_output;

public:
	static MTypeId id;
	static const MString typeName;

	//! @ingroup implement_group
	//! \brief ����������� ��������� ��� ��������� �����
	//! ���� ������� �� �����
	struct Generator : public cls::IGeometryGenerator
	{
		// ���������� ���, ������ ��� �������������
		virtual const char* GetUniqueName(
			){return "ocLight::Generator";};

		virtual bool OnGeometry(
			cls::INode& node,				//!< ������ 
			cls::IRender* render,			//!< render
			Math::Box3f& box,				//!< box 
			cls::IExportContext* context,	//!< context
			MObject& generatornode			//!< generator node �.� 0
			);
	};

};


