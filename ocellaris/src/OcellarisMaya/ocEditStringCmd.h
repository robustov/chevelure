#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ocEditStringCmdCmd.h
// MEL Command: ocEditStringCmd

#include <maya/MPxCommand.h>

class ocEditStringCmd : public MPxCommand
{
public:
	static const MString typeName;
	ocEditStringCmd();
	virtual	~ocEditStringCmd();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

