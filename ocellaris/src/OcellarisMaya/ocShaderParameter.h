#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ocShaderParameterNode.h
//
// Dependency Graph Node: ocShaderParameter

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 

class ocShaderParameter : public MPxNode
{
public:
	ocShaderParameter();
	virtual ~ocShaderParameter(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_input;
	static MObject i_useMtorSurfaceShader;
	static MObject i_surfaceShader;
	static MObject i_surfaceShaderAttrs;

	static MObject i_useMtorDisplaceShader;
	static MObject i_displaceShader;
//	static MObject i_displaceBound;
//	static MObject i_displaceSpace;
	static MObject i_displaceShaderAttrs;

	static MObject o_output;

//	static MObject i_inputMessage;
public:
	static MTypeId id;
	static const MString typeName;
};

