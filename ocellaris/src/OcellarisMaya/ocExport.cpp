#include "stdafx.h"
//
// Copyright (C) 
// File: ocExportCmd.cpp
// MEL Command: ocExport

#include "ocExport.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include <maya/MAnimControl.h>
#include <maya/MFileIO.h>
#include "ocellaris/OcellarisExport.h"
#include <time.h>

#include "ocellaris\renderFileImpl.h"
#include "ocellaris\renderCacheImpl.h"
#include "ocellaris\ChiefGenerator.h"
#include "ocellaris\CacheStorageMulty.h"
#include "..\generators\SlimShaderGenerator.h"
#include "..\generators\VertexVelocityAttributeGenerator.h"
#include "mathNmaya/mathNmaya.h"
#include "ocellaris\MayaExportContextImpl.h"
//ExportToPrman
//#include "VertexVelocityAttributeGenerator.h"
#include "Util/misc_create_directory.h"
#include "ocellaris\CacheStorageBuildAnimation.h"
#include "../ocellarisExport/ExportContextImpl.h"
#include "Util/currentHostName.h"

const MString ocExport::typeName( "ocExport" );


ocExport::ocExport()
{
}
ocExport::~ocExport()
{
}

MSyntax ocExport::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.addFlag("o", "output", MSyntax::kString);					// ���� �� ������
	syntax.addFlag("a", "ascii", MSyntax::kBoolean);					// ascii
	syntax.addFlag("du", "dump", MSyntax::kBoolean);					// dump
	syntax.addFlag("rtg", "roottransformgenerator", MSyntax::kString);	// roottransform

	syntax.useSelectionAsDefault(true);
	syntax.setObjectType( MSyntax::kSelectionList, 1 );
	return syntax;
}

MStatus ocExport::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MString _filename;
	if( !argData.getFlagArgument("o", 0, _filename))
	{
		return MS::kFailure;
	}

	bool bAscii;
	if( !argData.getFlagArgument("a", 0, bAscii))
		bAscii = 1;

	bool bDump = false;
	if( !argData.getFlagArgument("du", 0, bDump))
		bDump = false;

	MString roottransformgen;
	if( !argData.getFlagArgument("rtg", 0, roottransformgen))
		roottransformgen = "";



	bool bMotionVelocity = false;
	bool bMayaShaders = false;

	std::string projectDir;
	{
		MString workspacedir;
		MGlobal::executeCommand("workspace -fn", workspacedir);
		projectDir = workspacedir.asChar();
	}
	// folder
	std::string folder = Util::extract_foldername(_filename.asChar());
	Util::create_directory(folder.c_str());

	ExportContextImpl context;
	context.setEnvironment( "WORKSPACEDIR", projectDir.c_str());
	context.setEnvironment( "OUTPUTDIR", folder.c_str());
	context.setEnvironment( "SHADERDIR", folder.c_str());
	context.setEnvironment( "OCSDIR", folder.c_str());
	context.setEnvironment( "RIBDIR", folder.c_str());

	context.setEnvironment("OUTPUTFILENAME", _filename.asChar());
	context.setEnvironment("OUTPUTFORMAT", (bAscii)?("ascii"):("binary"));
	context.setEnvironment("SRCFILENAME", MFileIO::currentFile().asChar());
	context.setEnvironment("SRCHOSTNAME", Util::currentHostName().c_str());

	/////////////////////////////////////////////////
	// ���������� !!!
	context.addDefaultGenerators("");
	if( bMayaShaders)
		context.addGeneratorForType("shader", "MayaShader");

	// SimpleExport_PassGenerator
	cls::IPassGenerator* passgenerator = (cls::IPassGenerator*)context.loadGenerator("SimpleExportPass", cls::IGenerator::PASS);
	if( !passgenerator) 
		return (MS::kFailure);

	// roottransformgen
	// ���� ����� �������� �������!!!
	context.addGeneratorForType("roottransform", roottransformgen.asChar());
	
	if(bMotionVelocity)
	{
		context.addGeneratorForType("mesh", "VertexVelocityAttribute");
	}

	context.prepareGenerators();

	{
		Math::Box3f box;
//		MSelectionList sl;
//		MGlobal::getActiveSelectionList(sl);

		MSelectionList sl;
		argData.getObjects( sl);
		if( sl.length()==0)
			return MS::kFailure;

		std::list<MDagPath> list;
		for(int i=0; i<(int)sl.length(); i++)
		{
			MDagPath dagPath;
			if( !sl.getDagPath( i, dagPath))
				continue;
			context.addnodes(dagPath, "");
		}
	}


	context.addpass(passgenerator, "");

	MTime t = MAnimControl().currentTime();
	double frame = t.as(t.uiUnit());
	context.addframe((float)frame);

	context.preparenodes();

	context.doexport("", "", NULL);
	/*/
	cls::IRender* render = &_render;
	render->comment(comment1.c_str());
	render->comment(comment2.c_str());
	render->Attribute("file::box", box);
	cache.RenderGlobalAttributes(&_render);
	render->PushAttributes();
	cache.Render(render);
	render->PopAttributes();
	/*/





	/*/
	std::string projectDir;
	{
		MString workspacedir;
		MGlobal::executeCommand("workspace -fn", workspacedir);
		projectDir = workspacedir.asChar();
	}

	MayaExportContextImpl context("export");
	context.setProjectDirectory(projectDir.c_str());
	context.InitChiefGenerator(OCGT_SCENE);

	std::string folder = filename.asChar();
	{
		int p = folder.rfind('.');
		if(p!=folder.npos)
			folder = folder.substr(0, p);
	}
	folder += "/";
	Util::create_directory(folder.c_str());
	context.setOcsDir(folder.c_str());
	context.setShaderDir(folder.c_str());

//	ChiefGenerator chef;
//	ocellarisDefaultGenerators(OCGT_SCENE, chef, &context);

	cls::IGenerator* gen = NULL;
	Util::DllProcedure dll;
	if( roottransformgen.length()!=0)
	{
		if( dll.LoadFormated(roottransformgen.asChar(), "OcellarisExport"))
		{
			cls::GENERATOR_CREATOR pc = (cls::GENERATOR_CREATOR)dll.proc;
			gen = (*pc)();
			if( gen->GetType()!=cls::IGenerator::TRANSFORM) 
			{
				gen = NULL;
				displayString("generator %s has not type %d", roottransformgen.asChar(), cls::IGenerator::TRANSFORM);
			}
		}
		else
		{
			displayString("Cant load generator %s", roottransformgen.asChar());
		}
	}

	Math::Box3f box;
//	cls::INode* root = ocNodeTree_fromList(daglist, context.getChiefGenerator(), (cls::ITransformGenerator*)gen, &context);
	cls::INode* root = ocNodeTree_fromList(list, context.getChiefGenerator(), (cls::ITransformGenerator*)gen, &context);
	if(bDump)
		root->Dump(stdout);
	cls::renderCacheImpl<cls::CacheStorageSimple> cache;

	if( root)
	{
		root->Render(&cache, box, Math::Matrix4f::id, &context);
		root->Release();
	}
	cls::renderFileImpl _render;
	if( !_render.openForWrite(filename.asChar(), bAscii))
	{
		displayString("Cant open file %s for write", filename.asChar());
		return MS::kFailure;
	}
	cls::IRender* render = &_render;
	render->Attribute("file::box", box);
	cache.RenderGlobalAttributes(&_render);
	render->PushAttributes();
	cache.Render(render);
	render->PopAttributes();
	/*/

	return MS::kSuccess;
}

void* ocExport::creator()
{
	return new ocExport();
}

