#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ocRSLshaderNode.h
//
// Dependency Graph Node: ocRSLshader

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include "ocellaris/IShadingNodeGenerator.h"

class ocRSLshader : public MPxNode
{
public:
	ocRSLshader();
	virtual ~ocRSLshader(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_type;
	static MObject i_outputParamName;
	static MObject i_headerText;
	static MObject i_bodyText;
	static MObject i_shaderAttrsList;	// ������ ���������� ��������


	static MObject i_input;			// Example input attribute

	static MObject i_UVCoord;		// uv

	static MObject o_color;			// Output attributes
	static MObject o_alpha;			// Output attributes

public:
	static MTypeId id;
	static const MString typeName;

	static void updateShader(MObject node);

	class Generator : public cls::IShadingNodeGenerator
	{
	public:
		// ���������� ���, ������ ��� �������������
		virtual const char* GetUniqueName(
			){return "ocRSLshaderGenerator";};

		// ��� ����
		virtual enShadingNodeType getShadingNodeType(
			MObject& node					//!< ������ (shader)
			);

		//! ��������� ��������� 
		//! ��������� ��������� ��������� ��� �������
		virtual bool BuildShader(
			MObject& node,				//!< ������
			cls::enParamType wantedoutputtype, //!< ����� ��� ����� �� ������
			cls::IShaderAssembler* as,		//!< render
			cls::IExportContext* context,	//!< context
			MObject& generatornode			//!< generator node �.� 0
			);
	};
	static Generator generator;
};

