#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ocPolyShaderLayerCmd.h
// MEL Command: ocPolyShaderLayer

#include <maya/MPxCommand.h>

class ocPolyShaderLayer : public MPxCommand
{
public:
	static const MString typeName;
	ocPolyShaderLayer();
	virtual	~ocPolyShaderLayer();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

