#include "stdafx.h"
#include "ocCustomPass.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnStringData.h>

#include <maya/MGlobal.h>

MObject ocCustomPass::i_parent;
//MObject ocCustomPass::i_passgenerator;//string 

// set
MObject ocCustomPass::i_setname;//string 

// ��� ������
MObject ocCustomPass::i_cameraname;//string 
MObject ocCustomPass::i_resolution;//int [2]
MObject ocCustomPass::i_format_ratio;// double "pixelRatio"

// �����
//MObject ocCustomPass::i_dspyGain;// float
//MObject ocCustomPass::i_dspyGamma;// float
//MObject ocCustomPass::i_pixelsample;// float 2
//MObject ocCustomPass::i_pixelfilter;// int[2]
//MObject ocCustomPass::i_shadingrate;// float
//MObject ocCustomPass::i_filtername;
//MObject ocCustomPass::i_outputfilename;

// display
//MObject ocCustomPass::i_displaytype;
//MObject ocCustomPass::i_displaymode;
// quantize
//MObject ocCustomPass::i_quantizeMode;
//MObject ocCustomPass::i_quantizeOne;
//MObject ocCustomPass::i_quantizeMax;		
//MObject ocCustomPass::i_quantizeMin;		
//MObject ocCustomPass::i_quantizeDither;

// ���������
//MObject ocCustomPass::i_secStartFrame;
//MObject ocCustomPass::i_secEndFrame;
//MObject ocCustomPass::i_secEachFrame;

// blur
//MObject ocCustomPass::i_ocsBlur;
//MObject ocCustomPass::i_ocsShutterAngle;
//MObject ocCustomPass::i_ocsShutterTiming;
//MObject ocCustomPass::i_subframeMotion;
// crop
//MObject ocCustomPass::i_crop;
//MObject ocCustomPass::i_cropXY;

// ����
//MObject ocCustomPass::i_autoshadows;

// trace
//MObject ocCustomPass::i_trace[10];

// ���������
MObject ocCustomPass::i_ocsAscii;
//MObject ocCustomPass::i_addPreScene;
MObject ocCustomPass::i_outputOptions;
MObject ocCustomPass::i_beforeScene;
MObject ocCustomPass::i_forChildPasses;
MObject ocCustomPass::i_postRenderTask;

ocCustomPass::ocCustomPass()
{
}
ocCustomPass::~ocCustomPass()
{
}

MStatus ocCustomPass::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;

	return MS::kUnknownParameter;
}

void* ocCustomPass::creator()
{
	return new ocCustomPass();
}

MStatus ocCustomPass::initialize()
{
	MFnNumericAttribute numAttr;
	MFnMessageAttribute mesAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnStringData stringData;
	MFnEnumAttribute	enumAttr;
	MStatus				stat;

	try
	{
		{
			i_parent = mesAttr.create("parent", "par", &stat);
			::addAttribute(i_parent, atInput);

			/*/
			i_passgenerator = typedAttr.create( "passGenerator", "passg",
				MFnData::kString, stringData.create("", &stat), &stat );
			::addAttribute(i_passgenerator, atInput);
			/*/
		}
		// set		
		{
			i_setname = typedAttr.create( "set", "set",
				MFnData::kString, stringData.create("", &stat), &stat );
			::addAttribute(i_setname, atInput);
		}
		// camera
		{
			i_cameraname = typedAttr.create( "camera", "camera",
				MFnData::kString, stringData.create("perspShape", &stat), &stat );
			::addAttribute(i_cameraname, atInput);
		}
		{
			i_resolution = numAttr.create( "resolution","resolution", MFnNumericData::k2Int, 0, &stat);
			numAttr.setDefault(640, 480);
			numAttr.setSoftMin(10);
			numAttr.setSoftMax(2000);
			::addAttribute(i_resolution, atInput);
		}
		{
			i_format_ratio = numAttr.create( "format_ratio", "format_ratio", MFnNumericData::kDouble, 1, &stat);
			numAttr.setSoftMin(0.0);
			numAttr.setSoftMax(5.0);
			::addAttribute(i_format_ratio, atInput);
		}

		// �����
		/*/
		{
			i_dspyGain = numAttr.create( "dspyGain", "dspyGain", MFnNumericData::kDouble, 1, &stat);
			numAttr.setSoftMin(0.0);
			numAttr.setSoftMax(5.0);
			::addAttribute(i_dspyGain, atInput);
		}
		{
			i_dspyGamma = numAttr.create( "dspyGamma", "dspyGamma", MFnNumericData::kDouble, 1, &stat);
			numAttr.setSoftMin(0.0);
			numAttr.setSoftMax(5.0);
			::addAttribute(i_dspyGamma, atInput);
		}
		{
			i_pixelsample = numAttr.create( "pixelsample", "pixelsample", MFnNumericData::k2Double, 5, &stat);
			numAttr.setSoftMin(0.0);
			numAttr.setSoftMax(5.0);
			::addAttribute(i_pixelsample, atInput);
		}
		{
			i_pixelfilter = numAttr.create( "pixelfilter", "pixelfilter", MFnNumericData::k2Int, 2, &stat);
			numAttr.setSoftMin(0.0);
			numAttr.setSoftMax(5.0);
			::addAttribute(i_pixelfilter, atInput);
		}
		{
			i_shadingrate = numAttr.create( "shadingrate", "shadingrate", MFnNumericData::kDouble, 1, &stat);
			numAttr.setSoftMin(0.0);
			numAttr.setSoftMax(5.0);
			::addAttribute(i_shadingrate, atInput);
		}
		{
			i_filtername = typedAttr.create( "filtername", "filtername",
				MFnData::kString, stringData.create("box", &stat), &stat );
			::addAttribute(i_filtername, atInput);
		}
		{
			i_outputfilename = typedAttr.create( "outputfilename", "outputfilename",
				MFnData::kString, stringData.create("", &stat), &stat );
			::addAttribute(i_outputfilename, atInput);
		}
		/*/

		// display
		/*/
		{
			i_displayname = typedAttr.create( "displayname", "displayname",
				MFnData::kString, stringData.create("foo", &stat), &stat );
			::addAttribute(i_displayname, atInput);
		}
		{
			i_displaytype = typedAttr.create( "displaytype", "displaytype",
				MFnData::kString, stringData.create("it", &stat), &stat );
			::addAttribute(i_displaytype, atInput);
		}
		{
			i_displaymode = typedAttr.create( "displaymode", "displaymode",
				MFnData::kString, stringData.create("rgba", &stat), &stat );
			::addAttribute(i_displaymode, atInput);
		}
		// quantize
		{
			i_quantizeMode = typedAttr.create( "quantizeMode", "quantizeMode",
				MFnData::kString, stringData.create("rgba", &stat), &stat );
			::addAttribute(i_quantizeMode, atInput);
		}
		{
			i_quantizeOne = numAttr.create( "quantizeOne", "quantizeOne", MFnNumericData::kInt, 255, &stat);
			::addAttribute(i_quantizeOne, atInput);
		}
		{
			i_quantizeMax = numAttr.create( "quantizeMax", "quantizeMax", MFnNumericData::kInt, 255, &stat);
			::addAttribute(i_quantizeMax, atInput);
		}
		{
			i_quantizeMin = numAttr.create( "quantizeMin", "quantizeMin", MFnNumericData::kInt, 0, &stat);
			::addAttribute(i_quantizeMin, atInput);
		}
		{
			i_quantizeDither = numAttr.create( "quantizeDither", "quantizeDither", MFnNumericData::kDouble, 0.5, &stat);
			::addAttribute(i_quantizeDither, atInput);
		}

		{
			i_secStartFrame = numAttr.create( "secStartFrame", "secStartFrame", MFnNumericData::kInt, 1, &stat);
			::addAttribute(i_secStartFrame, atInput);
			i_secEndFrame = numAttr.create( "secEndFrame", "secEndFrame", MFnNumericData::kInt, 25, &stat);
			::addAttribute(i_secEndFrame, atInput);
			i_secEachFrame = numAttr.create( "secEachFrame", "secEachFrame", MFnNumericData::kInt, 1, &stat);
			::addAttribute(i_secEachFrame, atInput);
		}
		// blur
		{
			i_ocsBlur = numAttr.create( "ocsBlur", "ocsBlur", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_ocsBlur, atInput);

			i_ocsShutterAngle = numAttr.create( "shutterAngle", "ocssa", MFnNumericData::kDouble, 90, &stat);
			numAttr.setMin(0);
			numAttr.setSoftMax(360);
			::addAttribute(i_ocsShutterAngle, atInput);

			i_subframeMotion = numAttr.create( "subframeMotion", "subfm", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_subframeMotion, atInput);

			i_ocsShutterTiming = enumAttr.create("shutterTiming", "ocsst", 0);
			enumAttr.addField("open on frame", 0);
			enumAttr.addField("center on center", 1);
			enumAttr.addField("close on frame", 2);
			::addAttribute(i_ocsShutterTiming, atInput);
		}
		{
			i_crop = numAttr.create( "crop", "crp", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_crop, atInput);

			i_cropXY = numAttr.create( "cropXY","crpXY", MFnNumericData::k2Int, 0, &stat);
			numAttr.setDefault(1, 1);
			numAttr.setMin(1);
			numAttr.setSoftMax(100);
			::addAttribute(i_cropXY, atInput);
		}
		// ����
		{
			i_autoshadows = numAttr.create( "autoShadows", "aush", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_autoshadows, atInput);
		}

		// trace
		{
			i_trace[0] = numAttr.create( "trace", "trace", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_trace[0], atInput);

			i_trace[1] = numAttr.create( "traceMaxSpecularDepth", "traceMaxSpecularDepth", MFnNumericData::kInt, 2, &stat);
			::addAttribute(i_trace[1], atInput);

			i_trace[2] = numAttr.create( "traceMaxDiffuseDepth", "traceMaxDiffuseDepth", MFnNumericData::kInt, 1, &stat);
			::addAttribute(i_trace[2], atInput);

			i_trace[3] = numAttr.create( "traceDisplacements", "traceDisplacements", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_trace[3], atInput);

			i_trace[4] = numAttr.create( "traceSampleMotion", "traceSampleMotion", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_trace[4], atInput);
			
			i_trace[5] = numAttr.create( "traceBias", "traceBias", MFnNumericData::kDouble, 0.05, &stat);
			::addAttribute(i_trace[5], atInput);

			i_trace[6] = numAttr.create( "traceBreadthFactor", "traceBreadthFactor", MFnNumericData::kDouble, 1.00, &stat);
			::addAttribute(i_trace[6], atInput);

			i_trace[7] = numAttr.create( "traceDepthFactor", "traceDepthFactor", MFnNumericData::kDouble, 1.00, &stat);
			::addAttribute(i_trace[7], atInput);

			i_trace[8] = numAttr.create( "traceMaxDepth", "traceMaxDepth", MFnNumericData::kInt, 10, &stat);
			::addAttribute(i_trace[8], atInput);
		}
		/*/

		// ���������
		{
			i_ocsAscii = numAttr.create( "ocsAscii", "ocsAscii", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_ocsAscii, atInput);

			i_outputOptions = typedAttr.create( "outputOptions", "ouop",
				MFnData::kString, stringData.create("", &stat), &stat );
			::addAttribute(i_outputOptions, atInput);

			i_beforeScene = typedAttr.create( "beforeScene", "besc",
				MFnData::kString, stringData.create("", &stat), &stat );
			::addAttribute(i_beforeScene, atInput);

			i_forChildPasses = typedAttr.create( "forChildPasses", "fcpa",
				MFnData::kString, stringData.create("", &stat), &stat );
			::addAttribute(i_forChildPasses, atInput);

			
			i_postRenderTask = typedAttr.create( "postRenderTask", "prt",
				MFnData::kString, stringData.create("", &stat), &stat );
			::addAttribute(i_postRenderTask, atInput);
		}


		if( !MGlobal::sourceFile("AEocCustomPassTemplate.mel"))
		{
			displayString("error source AEocCustomPassTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

