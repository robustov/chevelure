#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ocFileNode.h
//
// Dependency Graph Node: ocFile

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 

class ocFile : public MPxNode
{
public:
	ocFile();
	virtual ~ocFile(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_filename;
	static MObject i_proxyfilename;
//	static MObject i_mayafilename;
//	static MObject i_mayaobjectname;
//	static MObject i_sequence;
//	static MObject i_renderPoints;
//	static MObject i_renderDegradation;
	static MObject i_drawMode;
	static MObject i_drawBox;
	static MObject i_delay;
	static MObject i_readglobals;
	static MObject i_renderSubFiles;
	static MObject i_matte;
	static MObject i_ignoreShaders;
	static MObject i_weight;
	static MObject i_renderProxy;

	static MObject i_addStringAllpass;
	static MObject i_addStringFinalpass;
	static MObject i_addStringShadowpass;

	static MObject o_drawCache;
	static MObject o_box;

public:
	static MTypeId id;
	static const MString typeName;
};

