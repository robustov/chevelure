#pragma once
#include "pre.h"

#include "mathNpixar/IPrman.h"
#include "ocInstance.h"
#include "ocParticleInstancer.h"
#include "ocExport_InstancerPhases.h"

struct InstancerData
{
	int num;
	float getTime(int fullcount, double shutterOpen, double shutterClose);
	MBoundingBox box;
//	Math::Box3f box;
	InstancerPhases phases;
	MObject psobj;
	MMatrix instancer_transform;

	struct particleData
	{
		int id;
		int pathid;
		int phase;
		MMatrix matrix;
	};

	struct particleIdData
	{
		int indexInPS;

		int id;
		int pathid;
		int phase;
		MMatrix matrix;
		MBoundingBox box;
		std::vector<particleData> particles;
	};

	std::map<int, particleIdData> particleIds;

	MStatus LoadParticles(
		MObject psobj, 
		bool bDump);

	static bool Export(
		MObject psobj,
		IPrman* prman,
		InstancerData* instancedatas, 
		int count, 
		double shutterOpen, double shutterClose,
		const char* additionalString,
		bool bDump
		);
protected:
	particleData* getParticleData(
		int id, int index);

	typedef std::map<int, cls::Param> param_pp_t;
	typedef std::map<std::string, param_pp_t> attributes_t;

	static void readAttribute(
		MObject psobj, std::vector<int>& ids, 
		std::string name, param_pp_t& params);
};
