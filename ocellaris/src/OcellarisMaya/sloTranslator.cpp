#include "stdafx.h"
#include "sloTranslator.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>

#include <maya/MGlobal.h>

sloTranslator::sloTranslator()
{
}
sloTranslator::~sloTranslator()
{
}

MStatus sloTranslator::reader ( 
	const MFileObject& file,
	const MString& optionsString,
	FileAccessMode mode)
{
	return (MS::kFailure);
}
///
MStatus sloTranslator::writer ( 
	const MFileObject& file,
	const MString& optionsString,
	FileAccessMode mode)
{
	return (MS::kFailure);
}
///
bool sloTranslator::haveReadMethod () const
{
	return true;
}
///
bool sloTranslator::haveWriteMethod () const
{
	return false;
}
///
bool sloTranslator::haveNamespaceSupport () const
{
	return false;
}
///
MString sloTranslator::defaultExtension () const
{
	return MString("slo");
}
///
MString sloTranslator::filter () const
{
	return MString("*.slo");
}
///
bool sloTranslator::canBeOpened () const
{
	return false;
}
///
MPxFileTranslator::MFileKind sloTranslator::identifyFile (	
	const MFileObject& file,
	const char* buffer,
	short size) const
{
	return (kNotMyFileType);
}

void* sloTranslator::creator()
{
	return new sloTranslator();
}
