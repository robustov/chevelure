#include "stdafx.h"
#include "ocExport_InstancerPhases.h"

#include <maya/MGlobal.h>
#include <maya/MAnimControl.h>
#include <maya/MFnParticleSystem.h>

#include "ocInstance.h"
#include "ocParticleInstancer.h"
#include "Util/misc_create_directory.h"
#include "mathNmaya/mathNmaya.h"

#include "ocellaris/OcellarisExport.h"


MStatus InstancerPhases::LoadPhases(
	MObject instancer,
	MObject psobj, 
	bool bDump)
{
	this->psobj = psobj;
	phases.clear();
	phasenames.clear();

	MStatus stat;
	MFnDependencyNode dn(instancer);
	MPlug inputHierarchy = dn.findPlug("inputHierarchy", &stat);
	if( !stat)
	{
		displayString("!findPlug inputHierarchy");
		return MS::kFailure;
	}
		
	for(int i=0; i<(int)inputHierarchy.numElements(); i++)
	{
		MPlugArray array;
		inputHierarchy[i].connectedTo(array, true, false);
		if(array.length()==0)
			continue;

		MObject obj = array[0].node();

		MDagPath path;
		MDagPath::getAPathTo(obj, path);
		MMatrix transform = path.inclusiveMatrix();
		findPhasesHierarchy(path, interval(-1e+38, 1e+38));
	}
	return MS::kSuccess;
}

void InstancerPhases::findPhasesHierarchy(
	MDagPath& path, 
	interval inter)
{
	MStatus stat;
	MObject obj = path.node();

	PhaseData pd; 
	if( obj.hasFn(MFn::kShape) || 
		obj.hasFn(MFn::kLocator))
	{
		std::string phasename = path.fullPathName().asChar();
		pd.phase = obj;
		pd.interval = inter;
		MMatrix transform = path.inclusiveMatrix();
		pd.matrix = transform;
		pd.name = phasename;

		// Box
		MPlug pl;
		MFnDependencyNode dn(obj);
		MVector pt1, pt2;
		pl = dn.findPlug("boundingBoxMinX");
		pl.getValue(pt1.x);
		pl = dn.findPlug("boundingBoxMinY");
		pl.getValue(pt1.y);
		pl = dn.findPlug("boundingBoxMinZ");
		pl.getValue(pt1.z);
		pl = dn.findPlug("boundingBoxMaxX");
		pl.getValue(pt2.x);
		pl = dn.findPlug("boundingBoxMaxY");
		pl.getValue(pt2.y);
		pl = dn.findPlug("boundingBoxMaxZ");
		pl.getValue(pt2.z);
		pd.box = MBoundingBox(pt1, pt2);

		if( dn.typeId()==ocInstance::id)
		{
			ocInstance* inst = (ocInstance*)dn.userNode();
			MDataBlock data = inst->getData();
			MObject val;
			cls::MInstancePxData* cache = inst->getCache(data, val);
			int frame = inst->getFrame(data, obj);

			cache->drawcache.UpdateAttrList( psobj, true);
			pd.cache = cache;
			pd.frame = frame;
		}

		/*/
		// mapParams
		{
			MString mapParams;
			MPlug plug = dn.findPlug("mapParams", &stat);
			if(!stat)
			{
				MFnTypedAttribute tAttr;
				MObject oAttr = tAttr.create("mapParams", "mapParams", MFnData::kString,
									 MObject::kNullObj, &stat);
				dn.addAttribute(oAttr);
			}
			plug.getValue(mapParams);
			pd.ParseParams(mapParams.asChar(), this->additionParams);
		}

		// slimShader
		{
			MString slimShader;
			MPlug plug = dn.findPlug("slimShader", &stat);
			if(!stat)
			{
				MFnTypedAttribute tAttr;
				MObject oAttr = tAttr.create("slimShader", "slimShader", MFnData::kString, MObject::kNullObj, &stat);
				dn.addAttribute(oAttr);
			}
			plug.getValue(slimShader);
			pd.slimShader = slimShader.asChar();

			if( pd.slimShader.empty())
				pd.slimShader = this->defaultShader;
			if( pd.slimShader.empty())
			{
				MString shader = ObjectShaderName(obj, "shadingmodel");
				MString displacement = ObjectShaderName(obj, "displacement");
				
				pd.slimShader = shader.asChar();
				pd.displaceShader = displacement.asChar();
			}
		}
		/*/

		int pdIndex = phases.size();
		phases.push_back(pd);

		// Name
		phasenames[phasename] = pdIndex;

//displayString("%s {%g, %g}", path.fullPathName().asChar(), inter.min, inter.max);
	}
	std::vector<interval> childintervals;
	if( obj.hasFn(MFn::kLodGroup))
	{
		MFnDependencyNode dn(obj);

		bool minMaxDistance;
		double minDistance, maxDistance;
		dn.findPlug("minMaxDistance").getValue(minMaxDistance);
		dn.findPlug("minDistance").getValue(minDistance);
		dn.findPlug("maxDistance").getValue(maxDistance);
		if( minMaxDistance)
		{
			inter.min = __max(minDistance, inter.min);
			inter.max = __min(maxDistance, inter.max);
		}

		MPlug thresholdPlug = dn.findPlug("threshold", &stat);
		if( !stat) 
			displayString("!findPlug threshold");

		childintervals.resize(thresholdPlug.numElements()+1);
		childintervals.front().min = inter.min;
		childintervals.back().max  = inter.max;

//displayString("threshold %d", thresholdPlug.numElements());

		for(int i=0; i<(int)thresholdPlug.numElements(); i++)
		{
			double threshold;
			thresholdPlug[i].getValue(threshold);
//			displayString("%f", threshold);
			childintervals[i].max   = threshold;
			childintervals[i+1].min = threshold;
		}
	}

	for( int i=0; i<(int)path.childCount(); i++)
	{
		obj = path.child(i);
		MDagPath chpath; chpath = MDagPath::getAPathTo(obj);

		interval cur = inter;
		if(i < (int)childintervals.size())
			cur = childintervals[i];
		findPhasesHierarchy(chpath, cur);
	}
}
int InstancerPhases::findPhase(std::string& name)
{
	if( phasenames.find(name)==phasenames.end())
	{
		return -1;
	}
	return phasenames[name];

}
MPoint PhaseData::getBoxCorner(int i)
{
	MPoint pt;
	pt.x = (i&0x1)?(box.min().x):(box.max().x);
	pt.y = (i&0x2)?(box.min().y):(box.max().y);
	pt.z = (i&0x4)?(box.min().z):(box.max().z);
	return pt;
}



