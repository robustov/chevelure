#pragma once
#include "pre.h"

//
// Copyright (C) 
// File: ocExportCmd.h
// MEL Command: ocExport

#include <maya/MPxCommand.h>

class ocExport : public MPxCommand
{
public:
	static const MString typeName;
	ocExport();
	virtual	~ocExport();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

