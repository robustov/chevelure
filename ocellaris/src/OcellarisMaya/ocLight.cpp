#include "stdafx.h"
//
// Copyright (C) 
// File: ocLightCmd.cpp
// MEL Command: ocLight

#include "ocLight.h"

#include <maya/MGlobal.h>
#include <maya/MFnUnitAttribute.h>
#include "mathNmaya\mathNmaya.h"
#include "mathNgl\mathNgl.h"
#include "math\smoothstep.h"

MObject ocLight::i_color;
MObject ocLight::i_intensity;
MObject ocLight::i_dropoff;
MObject ocLight::i_coneAngle;
MObject ocLight::i_penumbraAngle;
MObject ocLight::i_decayRate;
// Falloff
MObject ocLight::i_Falloff;
MObject ocLight::i_EndPersent;
MObject ocLight::i_FalloffDist;
MObject ocLight::i_StartDist;

// visualisation
MObject ocLight::i_viewDistance;
MObject ocLight::i_bViewFalloff;
MObject ocLight::i_bViewIsoparma;
MObject ocLight::i_viewAttenuation;

MObject ocLight::o_output;

/*/
Falloff
EndPersent
FalloffDist
StartDist

�� ���������
output "if ([getvar Falloff] != 0) {" 
output "	a = (100/EndPersent-1)/pow(FalloffDist-getvar StartDist,Falloff);"
output "}"

output "if ([getvar Falloff] != 0) {" 
output "	if (length(L) >  [getvar StartDist] ) {"
output "		atten *= 1 / (1 + a * pow(length(L)-[getvar StartDist],[getvar Falloff]) );"
output "	}"
output "}"
/*/

/*/
�� ����
angle = [getvar ConeAngle]*0.01745329252;"
penumbra = [getvar PenumbraAngle]*0.01745329252;"
cosoutside = cos(angle+penumbra);"
cosinside = cos(angle);"

output "varying float cosangle = normalize(L).normalize(axis);"
output "varying float atten = smoothstep( cosoutside, cosinside, cosangle);"

smoothstep = smoothstep_inv
// y � ��������� �� 0-1
float smoothstep_inv(float min, float max, float y)
{
	if(y>=1)
		return max;
	if(y<=0)
		return min;

	float x = smoothstep(0, 1, y);
	x = min + x*(max-min);
	return x;
}

/*/

/*/
			AddEnumAttribute($obj,"ulBoundlessAngle", "Off:On:",0);
						
			AddFloatAttribute($obj,"ulPenumbraLinear", 0);

			AddFloatAttribute($obj,"ulMayaPenumbra", 0);

			AddFloatAttribute($obj,"ulFalloffStyle", 0);

			// End Persent of Light Falloff
			AddFloatAttribute($obj,"tkEndPersent", 5.0);
			
									
			//decay Shadow 			
			MtorAddAttribute($obj,"tkShDecay", "bool", 1);

						
//////						
			// set shadow map 
			setAttr ($obj+".dmapFilterSize") 25.0;
			
			// set shadow bias size 
			setAttr ($obj+".dmapBias") 0.1;
					
			
		
			// Category of ligth  
			MtorAddStringAttribute($obj,"tkCategory","");
				
			// Shadow Blur
			AddFloatAttribute($obj,"tkShBlur", 0.001);
				
						
			
			// Do smart shadow transformation   
			MtorAddAttribute($obj,"tkDoShTrans", "bool", 1);
						
			
			// Smart shadow transformation coorsys
			MtorAddStringAttribute($obj,"tkShCoordSys","");
			
			
			
			
			// Gen End Dist attr
			AddFloatAttribute($obj,"tkEndDist", 0);
			
			// Gen Start Dist attr
			AddFloatAttribute($obj,"tkStartDist", 0);
			
			
			// Gen Penumbra
			AddFloatAttribute($obj,"tkPenumbra", 0);
			

			// Scale for  Blurs
			AddFloatAttribute($obj,"tkBlurScale", 1.0);

			// Attenuation for  Auto Bias
			AddFloatAttribute($obj,"ulBiasAtten", 1.0);
			
			// Shadow1 Weight, Samples, Blur and Bias
			AddFloatAttribute($obj,"weightSh", 1.0);
			AddFloatAttribute($obj,"samplesSh", 16);
			AddFloatAttribute($obj,"sizeSh", 0.1);
		
			// Shadow2 Weight, Samples, Blur and Bias
			AddFloatAttribute($obj,"weightMSH", 1);
			AddFloatAttribute($obj,"samplesMSH", 36);
			AddFloatAttribute($obj,"sizeMSHMin", 0.3);
			AddFloatAttribute($obj,"sizeMSHMax", 3);
			AddFloatAttribute($obj,"LayersMSH", 3);
			AddFloatAttribute($obj,"DimensionMSH", 1.5);
/*/

// �������: cosangle ������� ���� ����� ������ � ������
//			distance ��������� �� �����
// �����: atten
float Atten(float intensity, 
			float ConeAngle, float PenumbraAngle, 
			float Falloff, float EndPersent, float FalloffDist, float StartDist, 
			float cosangle,	
			float distance)
{
	float cosoutside = cos(ConeAngle+PenumbraAngle);
	float cosinside = cos(ConeAngle);
	float atten = Math::smoothstep(cosoutside, cosinside, cosangle);

	if( Falloff!=0)
	{
		float a = (100/EndPersent-1)/pow(FalloffDist-StartDist,Falloff);

		if( distance > StartDist)
		{
			atten *= 1 / (1 + a * pow(distance-StartDist,Falloff) );
		}
	}
	return atten*intensity;
}
// ������� distance atten
// �����: cosangle
float AttenInv(
				float intensity, 
				float ConeAngle, float PenumbraAngle, 
				float Falloff, float EndPersent, float FalloffDist, float StartDist, 
				float distance, 
				float atten)
{
	atten *= 1/intensity;
	if( Falloff!=0)
	{
		float a = (100/EndPersent-1)/pow(FalloffDist-StartDist,Falloff);
		if( distance > StartDist)
		{
			atten *= (1 + a * pow(distance-StartDist,Falloff) );
		}
	}

	float cosoutside = cos(ConeAngle+PenumbraAngle);
	float cosinside = cos(ConeAngle);
	float cosangle = Math::smoothstep_inv(cosoutside, cosinside, atten);

	return cosangle;
}

bool ocLight::Generator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();
	MDagPath path = inode.getPath();

//	MMatrix _matr = path.inclusiveMatrix();
	MTransformationMatrix xform( path.inclusiveMatrix()	);
	double scale[] = { -1, 1, -1	};
	xform.setScale(	scale, MSpace::kTransform );
	MMatrix _matr = xform.asMatrix();

	Math::Matrix4f matr; ::copy(matr, _matr);
	render->PushTransform();
	render->SetTransform(matr);

	MFnLight light(node);
	MString light_name = light.name();

	//std::string shadername = "spotlight";

	switch ( node.apiType() )
	{
		case MFn::kDirectionalLight: render->Parameter("#lightshader", "mtorDirectionalLight" ); break;
		case MFn::kPointLight: render->Parameter("#lightshader", "mtorPointLight" ); break;
		case MFn::kSpotLight: render->Parameter("#lightshader", "mtorSpotLight" ); break;
		case MFn::kAmbientLight: render->Parameter("#lightshader", "mtorAmbientLight" ); break;
		default:
		{
			printf("Light type - \"%s\" are not implemented. Using \"pointLight\" type.\n", node.apiTypeStr());
			render->Parameter("#lightshader", "mtorPointLight" );
		}
	}
	//render->Parameter("#lightshader", shadername.c_str() );//

	float intensity;
	Math::Vec3f lightcolor;

	light.findPlug( "colorR" ).getValue( lightcolor[0] );
	light.findPlug( "colorG" ).getValue( lightcolor[1] );
	light.findPlug( "colorB" ).getValue( lightcolor[2] );
	light.findPlug( "intensity" ).getValue( intensity );	
	render->Parameter( "intensity", intensity );
	render->Parameter( "lightcolor", cls::PT_COLOR, lightcolor );

	switch ( node.apiType() )
	{
		case MFn::kPointLight:
		{
			double decayRate;
			light.findPlug( "decayRate" ).getValue( decayRate );
			render->Parameter( "decayRate", ( float ) decayRate );
			
			if ( light.findPlug( "lightGlow" ).isConnected() )
			{
				int glowStyle;
				Math::Vec3f glowColor;
				float glowIntensity;
				float glowSpread;
				float glowStarLevel;
				float glowRadialNoise;
				float glowStarPoints;
				float glowRotation;
				float glowNoiseFreq;

				MPlugArray plugs;
				light.findPlug( "lightGlow" ).connectedTo( plugs, 1, 0 );
				MObject opticalFXnode = plugs[0].node();
				MFnDependencyNode opticalFX( opticalFXnode );
				opticalFX.findPlug( "glowType" ).getValue( glowStyle );
				opticalFX.findPlug( "glowColorR" ).getValue( glowColor[0] );
				opticalFX.findPlug( "glowColorG" ).getValue( glowColor[1] );
				opticalFX.findPlug( "glowColorB" ).getValue( glowColor[2] );
				opticalFX.findPlug( "glowIntensity" ).getValue( glowIntensity );
				opticalFX.findPlug( "glowSpread" ).getValue( glowSpread );
				opticalFX.findPlug( "glowStarLevel" ).getValue( glowStarLevel );
				opticalFX.findPlug( "glowRadialNoise" ).getValue( glowRadialNoise );
				opticalFX.findPlug( "starPoints" ).getValue( glowStarPoints );
				opticalFX.findPlug( "rotation" ).getValue( glowRotation );
				opticalFX.findPlug( "radialFrequency" ).getValue( glowNoiseFreq );

				render->Parameter( "glowStyle", ( float )glowStyle );
				render->Parameter( "glowColor", cls::PT_COLOR, glowColor );
				render->Parameter( "glowIntensity", glowIntensity );
				render->Parameter( "glowSpread", glowSpread );
				render->Parameter( "glowStarLevel", glowStarLevel );
				render->Parameter( "glowRadialNoise", glowRadialNoise );
				render->Parameter( "glowStarPoints", glowStarPoints );
				render->Parameter( "glowRotation", glowRotation );
				render->Parameter( "glowNoiseFreq", glowNoiseFreq );
			}

//"float glowStyle" [1] enum glowType
//"color glowColor" [0.345 0.632327 1] float3
//"float glowIntensity" [5.7854] float
//"float glowSpread" [1] float
//"float glowStarLevel" [5.4546] float
//"float glowRadialNoise" [0.10744] float
//"float glowStarPoints" [5.2068] float starPoints
//"float glowRotation" [148.758] float rotation
//"float glowNoiseFreq" [1.28096] float radialFrequency

			break;
		}

		case MFn::kSpotLight:
		{
			double deg_to_rad = 3.1415926535897932 / 180;
			if ( MAngle::uiUnit() != MAngle::kRadians ) deg_to_rad = 1;

			double decayRate, coneAngle, penumbraAngle, dropoff;
			light.findPlug( "decayRate" ).getValue( decayRate );	
			light.findPlug( "coneAngle" ).getValue( coneAngle );	
			light.findPlug( "penumbraAngle" ).getValue( penumbraAngle );	
			light.findPlug( "dropoff" ).getValue( dropoff );

			render->Parameter( "decayRate", ( float ) decayRate );
			render->Parameter( "coneAngle", float( 0.5 * coneAngle * deg_to_rad ) );
			render->Parameter( "penumbraAngle", float( penumbraAngle * deg_to_rad ) );
			render->Parameter( "dropOff", ( float ) dropoff );
			break;
		}

		case MFn::kAmbientLight:
		{
			float ambientShade;
			light.findPlug( "ambientShade" ).getValue( ambientShade );
			render->Parameter( "ambientShade", ambientShade );
			break;
		}
	}

	//render->Parameter("intensity", 100.f);
	//render->Parameter("color", cls::PT_COLOR, Math::Vec3f(1,1,1));
//areaLight
//volumeLight

	box.min = Math::Vec3f(-1, -1, -1);
	box.max = Math::Vec3f( 1,  1,  1);

	//render->Parameter("intensity", 100.f);
	//render->Parameter("lightcolor", cls::PT_COLOR, Math::Vec3f(1,1,1));
//	render->Parameter("decayRate", 0.f); 
//	render->Parameter("coneAngle", 0.349066f); 
//	render->Parameter("penumbraAngle", 0.f); 
//	render->Parameter("dropOff", 0.f); 
//	render->Parameter("useBarnDoors", 0.f); 

//	float _barnDoors[4] = {0.349066f, 0.349066f, 0.349066f, 0.349066f};
//	cls::PA<float> barnDoors(cls::PI_CONSTANT, _barnDoors, 4);
//	render->Parameter("barnDoors", barnDoors);

//	render->Parameter("useDecayRegions", 0.f); 
//	float _decayRegions[6] = {1, 2, 3, 6, 8, 10,};
//	cls::PA<float> decayRegions(cls::PI_CONSTANT, _decayRegions, 6);
//	render->Parameter("decayRegions", decayRegions);

	//render->Light( light_name.asChar(), shadername.c_str());
	render->Light();//
//	LightSource "mtorSpotLight" "spotLightShape1" "float intensity" [1] "color lightcolor" [1 1 1] "float decayRate" [0] "float coneAngle" [0.349066] "float penumbraAngle" [0] "float dropOff" [0] "float useBarnDoors" [0] "float[4] barnDoors" [0.349066 0.349066 0.349066 0.349066] 
//	"float useDecayRegions" [0] "float[6] decayRegions" [1 2 3 6 8 10]
	render->PopTransform();

	return true;
}

ocLight::ocLight()
{
}
ocLight::~ocLight()
{
}

MStatus ocLight::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;

	/*/
	if( plug == o_output )
	{
		MDataHandle inputData = data.inputValue( i_input, &stat );

		MDataHandle outputData = data.outputValue( plug, &stat );

		data.setClean(plug);
		return MS::kSuccess;
	}
	/*/

	return MS::kUnknownParameter;
}

bool ocLight::isBounded() const
{
	MObject thisNode = thisMObject();
	return false;
}

MBoundingBox ocLight::boundingBox() const
{
	MObject thisNode = thisMObject();

	MBoundingBox box;
	//...
	return box;
}

void ocLight::draw(
	M3dView &view, 
	const MDagPath &path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status)
{ 
	MObject thisNode = thisMObject();

	MPlug plug;

	double coneAngle;
	double penumbraAngle;
	float intensity;

	plug = MPlug(thisNode, i_intensity);
	plug.getValue(intensity);

	plug = MPlug(thisNode, i_coneAngle);
	plug.getValue(coneAngle);
	coneAngle *= 0.5;

	plug = MPlug(thisNode, i_penumbraAngle);
	plug.getValue(penumbraAngle);

	// Falloff
	float Falloff, EndPersent, FalloffDist, StartDist;
	plug = MPlug(thisNode, i_Falloff);
	plug.getValue(Falloff);
	plug = MPlug(thisNode, i_EndPersent);
	plug.getValue(EndPersent);
	plug = MPlug(thisNode, i_FalloffDist);
	plug.getValue(FalloffDist);
	plug = MPlug(thisNode, i_StartDist);
	plug.getValue(StartDist);

	// visualisation
	float viewAttenuation, view_maxDist;
	bool bViewFalloff, bViewIsoparma;
	plug = MPlug(thisNode, i_viewAttenuation);
	plug.getValue(viewAttenuation);
	plug = MPlug(thisNode, i_viewDistance);
	plug.getValue(view_maxDist);
	plug = MPlug(thisNode, i_bViewFalloff);
	plug.getValue(bViewFalloff);
	plug = MPlug(thisNode, i_bViewIsoparma);
	plug.getValue(bViewIsoparma);
		

	view.beginGL(); 

	float view_deltaDist = 0.05f;

	std::vector<Math::Vec3f> line;
	line.reserve( (int)(view_maxDist/view_deltaDist));

	if( bViewIsoparma)
	{
		bool bFirst = true;
		for(float d=view_maxDist; d>0; d-=view_deltaDist)
		{
			float cosAngle = AttenInv( intensity, (float)coneAngle, (float)penumbraAngle, 
				Falloff, EndPersent, FalloffDist, StartDist, 
				d, 
				viewAttenuation);

			float sinAngle = sqrt(1-(float)(cosAngle*cosAngle));
			float t = d*sinAngle;
			if( Falloff==0)
				t *= 1/cosAngle;

			if( Falloff!=0) 
			{
				float i = Atten( intensity, (float)coneAngle, (float)penumbraAngle, 
					Falloff, EndPersent, FalloffDist, StartDist, 
					1, 
					d);
				if( viewAttenuation>i) continue;
				if( bFirst)
					drawCircleXY( t*2, Math::Vec3f(0, 0, -d));
//					line.push_back(Math::Vec3f(0, 0, -d));
				bFirst = false;

			}

			line.push_back(Math::Vec3f(0, t, -d));

		}
		drawLine( &line[0], (int)line.size());

		for(int i=0; i<(int)line.size(); i++)
		{
			Math::Vec3f src = line[i];
			line[i] = Math::Vec3f(0, -src.y, src.z);
		}
		drawLine( &line[0], (int)line.size());

		for(int i=0; i<(int)line.size(); i++)
		{
			Math::Vec3f src = line[i];
			line[i] = Math::Vec3f(src.y, 0, src.z);
		}
		drawLine( &line[0], (int)line.size());

		for(int i=0; i<(int)line.size(); i++)
		{
			Math::Vec3f src = line[i];
			line[i] = Math::Vec3f(-src.x, 0, src.z);
		}
		drawLine( &line[0], (int)line.size());
	}

	if( bViewFalloff)
	{
		line.clear();
		for(float d=0; d<view_maxDist; d+=view_deltaDist)
		{
			float a = Atten( intensity, (float)coneAngle, (float)penumbraAngle, 
				Falloff, EndPersent, FalloffDist, StartDist, 
				1, d);
			line.push_back( Math::Vec3f(a, 0, -d));
		}
		drawLine( &line[0], (int)line.size());
		if( Falloff!=0)
		{
			glLineStipple(1, 0x8888);
			glEnable(GL_LINE_STIPPLE);
			float a;
			a = Atten( intensity, (float)coneAngle, (float)penumbraAngle, 
				Falloff, EndPersent, FalloffDist, StartDist, 
				1, StartDist);
			glBegin( GL_LINES );
			glVertex3f( Math::Vec3f( a,  0, -StartDist));
			glVertex3f( Math::Vec3f(-a,  0, -StartDist));
			glVertex3f( Math::Vec3f( 0, -a, -StartDist));
			glVertex3f( Math::Vec3f( 0,  a, -StartDist));
			glEnd();
			drawCircleXY( a*2, Math::Vec3f(0, 0, -StartDist));

			a = Atten( intensity, (float)coneAngle, (float)penumbraAngle, 
				Falloff, EndPersent, FalloffDist, StartDist, 
				1, FalloffDist);
			glBegin( GL_LINES );
			glVertex3f( Math::Vec3f( a,  0, -FalloffDist));
			glVertex3f( Math::Vec3f(-a,  0, -FalloffDist));
			glVertex3f( Math::Vec3f( 0, -a, -FalloffDist));
			glVertex3f( Math::Vec3f( 0,  a, -FalloffDist));
			glEnd();
			drawCircleXY( a*2, Math::Vec3f(0, 0, -FalloffDist));
			glDisable(GL_LINE_STIPPLE);
		}
	}

	/*/
	{
		float d = 1;
		float cosAngle = AttenInv( intensity, (float)coneAngle, (float)penumbraAngle, viewAttenuation);
		float sinAngle = sqrt(1-(float)(cosAngle*cosAngle));
		float t = d*sinAngle/cosAngle;
		drawCircleXY( t*2, Math::Vec3f(0, 0, -d));
	}
	{
		float d = 2;
		float cosAngle = AttenInv( intensity, (float)coneAngle, (float)penumbraAngle, viewAttenuation);
		float sinAngle = sqrt(1-(float)(cosAngle*cosAngle));
		float t = d*sinAngle/cosAngle;
		drawCircleXY( t*2, Math::Vec3f(0, 0, -d));
	}
	/*/

	view.endGL();
};


void* ocLight::creator()
{
	return new ocLight();
}

MStatus ocLight::initialize()
{
	MFnUnitAttribute unitAttr;
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		{
			i_color = numAttr.createColor("color", "cl");
			numAttr.setDefault(1.0f, 1.0f, 1.0f);
			::addAttribute(i_color, atInput);
		}
		{
			i_intensity = numAttr.create("intensity", "intensity", MFnNumericData::kFloat, 1);
			numAttr.setMin(0);
			numAttr.setSoftMax(2);
			::addAttribute(i_intensity, atInput);
		}
		{
			i_dropoff = numAttr.create("dropoff", "dropoff", MFnNumericData::kFloat, 50);
			numAttr.setMin(0);
			numAttr.setSoftMax(100);
			::addAttribute(i_dropoff, atInput);
		}
		{
			i_coneAngle = unitAttr.create("coneAngle", "coneAngle", MAngle(40, MAngle::kDegrees));
			unitAttr.setMin(0*2*M_PI/180);
			unitAttr.setSoftMax(179*2*M_PI/180);
			::addAttribute(i_coneAngle, atInput);
		}
		{
			i_penumbraAngle = unitAttr.create("penumbraAngle", "penumbraAngle", MAngle(10, MAngle::kDegrees));
			unitAttr.setMin(-90*2*M_PI/180);
			unitAttr.setMax(90*2*M_PI/180);
			::addAttribute(i_penumbraAngle, atInput);
		}
		{
			i_decayRate = numAttr.create("decayRate", "decayRate", MFnNumericData::kFloat, 50);
			numAttr.setMin(0);
			numAttr.setSoftMax(100);
			::addAttribute(i_decayRate, atInput);
		}

		{
			i_viewAttenuation = numAttr.create("viewAttenuation", "viewAttenuation", MFnNumericData::kFloat, 1);
			numAttr.setMin(0);
			numAttr.setSoftMax(2);
			::addAttribute(i_viewAttenuation, atInput);
		}
		// Falloff
		{
			i_Falloff = numAttr.create("falloff", "falloff", MFnNumericData::kFloat, 0);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(i_Falloff, atInput);

			i_EndPersent = numAttr.create("endPersent", "endPersent", MFnNumericData::kFloat, 20);
			numAttr.setMin(0);
			numAttr.setSoftMax(100);
			::addAttribute(i_EndPersent, atInput);

			i_FalloffDist = numAttr.create("falloffDist", "falloffDist", MFnNumericData::kFloat, 5);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(i_FalloffDist, atInput);

			i_StartDist = numAttr.create("startDist", "startDist", MFnNumericData::kFloat, 0.5);
			numAttr.setMin(0);
			numAttr.setSoftMax(10);
			::addAttribute(i_StartDist, atInput);
		}
			
		{
			i_viewDistance = numAttr.create("viewDistance", "viewDistance", MFnNumericData::kFloat, 10);
			numAttr.setMin(0);
			numAttr.setSoftMax(100);
			::addAttribute(i_viewDistance, atInput);
			
			i_bViewFalloff = numAttr.create("bViewFalloff", "bViewFalloff", MFnNumericData::kBoolean, 1);
			::addAttribute(i_bViewFalloff, atInput);
			i_bViewIsoparma = numAttr.create("bViewIsoparma", "bViewIsoparma", MFnNumericData::kBoolean, 1);
			::addAttribute(i_bViewIsoparma, atInput);

			o_output = numAttr.create( "o_output", "out", MFnNumericData::kDouble, 0.01, &stat);
			::addAttribute(o_output, atOutput);
//			stat = attributeAffects( i_input, o_output );
		}
		if( !MGlobal::sourceFile("AEocLightTemplate.mel"))
		{
			displayString("error source AEocLightTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}