#pragma once

#include "pre.h"
#include <maya/MPxLocatorNode.h>
#include "Math/Box.h"
#include "ocellaris/MInstancePxData.h"

//#define USE_LOCATOR_INSTANCE
 
#ifdef USE_LOCATOR_INSTANCE

class ocInstance : public MPxLocatorNode
{
public:
	ocInstance();
	virtual ~ocInstance(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	virtual void draw(
		M3dView &view, const MDagPath &path, 
		M3dView::DisplayStyle style,
		M3dView::DisplayStatus status);

	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_startTime, i_time;
	static MObject i_drawCache;
	static MObject i_drawBox;
	static MObject i_motionBlurEnable;
	static MObject o_filename;

//	int getFrame(MObject thisNode) const;
//	cls::MInstancePxData* getCache(MObject thisNode, MObject& val) const;

	MDataBlock getData();

	int getFrame(MDataBlock data, MObject thisNode) const;
	cls::MInstancePxData* getCache(MDataBlock data, MObject& val) const;
public:
	static MTypeId id;
	static const MString typeName;
};

#else

#include "ocInstance2UI.h"

class ocInstance2Geometry
{
public:
	bool bDrawBox;
	cls::MInstancePxData* cache;
	int frame;
	std::vector<Math::Matrix4f> matricies;
};

class ocInstance : public MPxSurfaceShape
{
public:
	ocInstance();
	virtual ~ocInstance(); 
	///
	void postConstructor();

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static  void* creator();
	static  MStatus initialize();

	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 




public:
	ocInstance2Geometry* getGeometry();

	ocInstance2Geometry theGeometry;

public:
	static MObject i_startTime, i_time;
	static MObject i_drawCache;
	static MObject i_drawBox;
	static MObject i_motionBlurEnable;

	static MObject i_matrixArray;			// kDoubleArray ������� x16

	static MObject o_filename;

	static MTypeId id;
	static const MString typeName;

public:
	MDataBlock getData();

	int getFrame(MDataBlock data, MObject thisNode) const;
	cls::MInstancePxData* getCache(MDataBlock data, MObject& val) const;
};

#endif

