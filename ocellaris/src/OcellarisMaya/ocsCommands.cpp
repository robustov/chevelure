#include "stdafx.h"

#ifdef WIN32
#define EXPORT __declspec(dllexport) 
#else
#define EXPORT
#endif

#include <string.h>
#include "../ocellarisExport/ExportContextImpl.h"
#include "ocellaris/renderStringImpl.h"
#include "ocellaris/INode.h"
#include "MathNMaya/MayaProgress.h"
#include "MathNPixar/IPrman.h"

ExportContextImpl exportContext;

void Maya_system( const char* cmd);

Alfred::Task ocs_fastdiststart_addTask(
	Alfred::Job& job, 
	const char* filename, 
	int number, 
	Alfred::Task& prevwaittask, 
	bool bRemote
	);

extern "C"
{
//	export int ocs_clear();
//	export const char* ocs_getenv(const char* env);
//	export int ocs_setenv(const char* env, const char* value);
EXPORT cls::IExportContext* ocs_context()
{
	return &exportContext;
}
EXPORT int ocs_clear()
{
	printf("clear!\n");
	exportContext.clear();
	return 1;
}
EXPORT const char* ocs_getenv( const char* env)
{
	printf("getenv %s!\n", env);
	return exportContext.getenv(env);
}
EXPORT int ocs_setenv(const char* env, const char* value)
{
	printf("setenv %s = %s!\n", env, value);
	return exportContext.setenv(env, value)?1:0;
}
EXPORT int ocs_addDefaultGenerators(const char* options)
{
	printf("addDefaultGenerators %s !\n", options);
	return exportContext.addDefaultGenerators(options)?1:0;
}
EXPORT int ocs_addGeneratorForType(const char* type, const char* generator)
{
	printf("addGeneratorForType %s %s !\n", type, generator);
	return exportContext.addGeneratorForType(type, generator)?1:0;
}
EXPORT int ocs_addnodes(const char* root, const char* options)
{
	printf("addnodes %s %s!\n", root, options);
	if( !exportContext.addnodes(root, options))
		return 0;
	return 1;
}
EXPORT int ocs_addpass(const char* passnode, const char* options)
{
	printf("addpass %s %s!\n", passnode, options);
	return exportContext.addpass(passnode, options);
}
EXPORT int ocs_addpassgenerator(const char* passgenerator, const char* options)
{
	cls::IGenerator* pass = exportContext.loadGenerator("SimpleExportPass", cls::IGenerator::PASS);
	if( !pass)
		return 0;
	return exportContext.addpass((cls::IPassGenerator*)pass, options);
}
EXPORT int ocs_connectpass(const char* mainpass, const char* subpass, const char* options)
{
	printf("connectpass %s->%s %s!\n", mainpass, subpass, options);
	return exportContext.connectPass(mainpass, subpass, options);
}
EXPORT int ocs_addlight(const char* node, const char* options)
{
	printf("addlight %s %s!\n", node, options);
	return exportContext.addlight(node, options);
}
EXPORT int ocs_addframe(float frame)
{
	printf("addframe %f\n", frame);

	return exportContext.addframe(frame);
}
EXPORT int ocs_preparegenerators()
{
	printf("ocs_preparegenerators\n");
	return exportContext.prepareGenerators();
}

EXPORT int ocs_preparenodes(const char* progresstitle, const char* progresstext)
{
	printf("ocs_preparenodes\n");
	if(progresstitle && progresstitle[0])
	{
		MayaProgress mp(progresstitle, progresstext);
		return exportContext.preparenodes();
	}
	else
	{
		return exportContext.preparenodes();
	}
}
EXPORT int ocs_export(const char* dumpoption, const char* options, const char* progresstitle, const char* progresstext)
{
	printf("export\n");

	if(progresstitle && progresstitle[0])
	{
		MayaProgress mp(progresstitle, progresstext);
		int res;
//		try
//		{
			res = exportContext.doexport(dumpoption, options, &mp);
//		}
//		catch(...)
//		{
//			throw;
//		}
		return res;
	}
	else
	{
		return exportContext.doexport(dumpoption, options, NULL);
	}
}
EXPORT int ocs_render(const char* dumpoption)
{
	printf("render\n");

	return exportContext.render(dumpoption);
}

EXPORT int ocs_distalfred(
	const char* option, 
	const char* mayafilename, 
	const char* workspace)
{
	bool bRemote = false;
	Alfred::Job job( mayafilename);

	// ALFRED
	std::string exportdir = exportContext.getEnvironment("RIBDIR");
	std::string alfredjob_filename = exportdir+"/_alfred_";
	std::string distjob_filename = exportdir+"/_dist_";
	std::string batjob_filename = exportdir+"/_alfred_";
	{
		struct tm   *newTime;
		time_t      szClock;
		time( &szClock );
		newTime = localtime( &szClock );
		char buf[100];
		alfredjob_filename += _itoa(szClock, buf, 10);
		alfredjob_filename += ".alf";
		Util::correctFileName( (char*)alfredjob_filename.c_str(), true);

		distjob_filename += _itoa(szClock, buf, 10);
		distjob_filename += ".alf";
		Util::correctFileName( (char*)distjob_filename.c_str(), true);

		batjob_filename += _itoa(szClock, buf, 10);
		batjob_filename += ".bat";
		Util::correctFileName( (char*)batjob_filename.c_str(), true);
	}

	Alfred::Task ribTask("rib generation");
	{
		FILE* batfile = fopen( batjob_filename.c_str(), "wt");
		if( !batfile)
		{
			exportContext.error("Cant open file %s for writiong", batjob_filename.c_str());
			return false;
		}
		fputs("call ocellaris.env.cmd\n", batfile);
		fprintf(batfile, "ocellarisMayaCmd.exe -maketask %s -workspace \"%s\" -alffile \"%s\" \"%s\"", 
			option, 
			workspace, 
			alfredjob_filename.c_str(), 
			mayafilename);
		fclose(batfile);

		Alfred::Cmd cmd = Alfred::RemoteCmd(batjob_filename);
		cmd.service  = "pixarmtor";
		cmd.atmost = 1;
		cmd.atleast = 1;
		cmd.tags  = "";
		cmd.bRemote = bRemote;
		ribTask.cmds.push_back(cmd);
		job.subtasks.push_back(ribTask);
	}

	Alfred::Task readTask("read task");
	{
		Alfred::Cmd cmd = Alfred::RemoteCmd("ocellarisCmd.exe -readjob \"%s\"", 
			alfredjob_filename.c_str());
		cmd.service  = "pixarmtor";
		cmd.atmost = 1;
		cmd.atleast = 1;
		cmd.tags  = "";
		cmd.expand  = "1";
		cmd.bRemote = bRemote;
		readTask.cmds.push_back(cmd);
		readTask.subtasks.push_back(
			Alfred::Instance(ribTask)
			);
		job.subtasks.push_back(readTask);
	}
	job.AddCleanup(
		Alfred::CmdDeleteFile(alfredjob_filename.c_str())
		);
	job.AddCleanup(
		Alfred::CmdDeleteFile(mayafilename)
		);
	

	bool res = Alfred::saveAlfredFile(distjob_filename.c_str(), job);
	if(!res)
		return 0;
	Maya_system( ("alfred \"" + distjob_filename+"\"").c_str());
	return 1;
}

int number;
Alfred::Task prevwaittask="";
Alfred::Task preflight="";
std::string alfredjob_filename="";
std::string alfredjob_directory="";
EXPORT int ocs_fastdiststart(const char* options)
{
	Alfred::Job& job = exportContext.getFastDistStart();
	if( strcmp(options, "begin")==0)
	{
		std::string exportdir = exportContext.getEnvironment("RIBDIR");
		alfredjob_filename = exportdir+"/_alfred_";
		alfredjob_directory = exportdir+"/_alfred_";
		// ALFRED
		{
			struct tm   *newTime;
			time_t      szClock;
			time( &szClock );
			newTime = localtime( &szClock );
			char buf[100];
			alfredjob_filename += _itoa(szClock, buf, 10);
			alfredjob_directory += _itoa(szClock, buf, 10);
			alfredjob_directory += "/";
			alfredjob_filename += ".alf";
			Util::correctFileName((char*)alfredjob_filename.c_str(), true);
			Util::correctFileName((char*)alfredjob_directory.c_str(), true);
		}

		number = 0;
		prevwaittask = Alfred::Task("");
		preflight = Alfred::Task("");

		std::string filename = alfredjob_directory;
		char buf[100];
		filename += _itoa(number, buf, 10);
		filename += ".tcl";
		Alfred::Task task = ocs_fastdiststart_addTask(job, filename.c_str(), number++, prevwaittask, false);
		preflight = task;
		job.subtasks.push_back(task);

		exportContext.fastdiststart_preflight = filename;
	}
	if( strcmp(options, "end")==0)
	{
		std::string filename = alfredjob_directory;
		char buf[100];
		filename += _itoa(number, buf, 10);
		filename += ".tcl";
		Alfred::Task task = ocs_fastdiststart_addTask(job, filename.c_str(), number++, Alfred::Task(""), false);
		job.subtasks.push_back(task);

		exportContext.fastdiststart_postrender = filename;

		Alfred::saveAlfredFile(alfredjob_filename.c_str(), job);
//		Alfred::startAlfred(alfredjob_filename.c_str(), false);
		Maya_system( ("alfred \"" + alfredjob_filename+"\"").c_str());
	}
	return 1;
}
EXPORT int ocs_fastdiststart_addframe(float frame)
{
	std::string filename = alfredjob_directory;
	char buf[100];
	filename += _itoa(number, buf, 10);
	filename += ".tcl";

	Alfred::Job& job = exportContext.getFastDistStart();

	Alfred::Task task = ocs_fastdiststart_addTask(
		job, 
		filename.c_str(), 
		number++, 
		prevwaittask, 
		false
		);
	task.subtasks.push_back( Alfred::Instance( preflight));
	job.subtasks.push_back(task);

	MTime t( frame, MTime::uiUnit());
	exportContext.fastdiststart_framefiles[t.as(MTime::uiUnit())] = filename;
	return 1;
}



EXPORT int ocs_dump(const char* options)
{
	exportContext.dump(options);
	return 1;
}

EXPORT int ocs_test(MDagPath path)
{
//	printf("ocs_test %s\n", path->fullPathName().asChar());
	return 1;
}

EXPORT cls::IExportContext* getExportContextForRibGen(
	const char* options
	)
{
	exportContext.clear();
	return &exportContext;
}

extern "C" {
    RtVoid ExportToPrman_Free(RtPointer data)
	{
		RtString* sData = (RtString*) data;
		free(sData[0]);
		free(sData[1]);
		free(sData);
    }
}

EXPORT bool ExportToPrman(
	IPrman* prman, 
	const char* node, 
	double time, 
	bool useMotionBlur, 
	double shutterOpen, double shutterClose, 
//	double shutterOpenFrame, double shutterCloseFrame,
	double fps, 
	const float* camera, 
	bool bMayaShaders, 
	bool bSlimShaders, 
	const char* options
	)
{
	double shutterOpenFrame = shutterOpen*fps;
	double shutterCloseFrame = shutterClose*fps;

	std::string passname = exportContext.getEnvironment("PASSNAME");
	std::string outfilename = exportContext.getEnvironment("OUTPUTFILENAME");

	exportContext.setEnvironment( "EXPORTTEMPLATED", "1");
	{
		MString workspacedir;
		MGlobal::executeCommand("workspace -fn", workspacedir);
		exportContext.setEnvironment( "WORKSPACEDIR", workspacedir.asChar());
	}
	std::string folderName = Util::extract_foldername(outfilename.c_str());
	exportContext.setEnvironment( "SHADERDIR", folderName.c_str());
	exportContext.setEnvironment( "OUTPUTDIR", folderName.c_str());
	exportContext.setEnvironment( "RIBDIR", folderName.c_str());
	exportContext.setEnvironment( "OCSDIR", folderName.c_str());

		//! JOBNAME   - ��� ������
		//! SHADERCOMPILER	- "%RMANTREE%/bin/shader.exe -o %s -I%RATTREE%/lib/slim/include %s"

	/////////////////////////////////////////////////
	// ���������� !!!
	exportContext.addDefaultGenerators("");

	if( bMayaShaders)
	{
		exportContext.addGeneratorForType("shape", "MayaShader");
	}
	if( bSlimShaders)
	{
		int bUseMtor = 0;
		MGlobal::executeCommand("exists mtor", bUseMtor);
		if( bUseMtor)
			exportContext.addGeneratorForType("shape", "SlimShader");
	}


	// SimpleExport_PassGenerator
	cls::IPassGenerator* passgenerator = (cls::IPassGenerator*)exportContext.loadGenerator("SimpleExportPass", cls::IGenerator::PASS);
	if( !passgenerator) 
		return false;

	exportContext.prepareGenerators();


	cls::INode* inode = exportContext.addnodes( node, "");
	if( !inode)
		return false;

	exportContext.addpass(passgenerator, "");

	if(!useMotionBlur)
	{
		exportContext.addframe(time);
	}
	else
	{
		// DEBUG!!!
		shutterOpenFrame = time;
		shutterCloseFrame = time+1;
		shutterClose = shutterOpen+1.0/fps;


		char buf[100];
		exportContext.setEnvironment("BLUR", "1");		
		sprintf(buf, "%.5f", shutterOpenFrame);
		exportContext.setEnvironment("BLURSHUTTEROPENFRAME", buf);
		sprintf(buf, "%.5f", shutterCloseFrame);
		exportContext.setEnvironment("BLURSHUTTERCLOSEFRAME", buf);

		exportContext.addframe(time);
	}

	exportContext.preparenodes();
	if( !exportContext.doexport("", "", NULL))
		return false;

	////////////////////////////////
	// �������� � ���
	Math::Box3f box(1000);
	cls::IRender* passdata = exportContext.objectContext(MObject::kNullObj, passgenerator);
	if( passdata)
	{
		passdata->GetAttribute("box", box);
	}

	Math::Matrix4f mview(camera);

	Math::Matrix4f worldpos;
	MDagPath path = inode->getPath();
	copy( worldpos, path.inclusiveMatrix());

	cls::renderStringImpl stringrender;
	stringrender.openForWrite();
	cls::IRender* prescene = &stringrender;
		prescene->Attribute("@pass", passname);
		prescene->Attribute("@camera::worldpos", mview);
		prescene->Attribute("@dso::worldtransform", worldpos);
	std::string attributes;
	attributes += stringrender.getWriteBuffer();

	RtString *data = static_cast<RtString*>(malloc(sizeof(RtString) * 3));
	data[0] = _strdup("ocellarisPrmanDSO.dll");
	int textsize = 10000;
	data[1] = static_cast<char*>(malloc(sizeof(char)*(textsize+2)));
	_snprintf(data[1], textsize, "%s$%.5f$%.5f$%.5f$%s", outfilename.c_str(), shutterOpen-0.001f, shutterOpen-0.001f, shutterClose, attributes.c_str());
	data[2] = 0;

	RtBound bound;
	bound[0] = box.min.x;
	bound[1] = box.max.x;
	bound[2] = box.min.y;
	bound[3] = box.max.y;
	bound[4] = box.min.z;
	bound[5] = box.max.z;
	prman->RiProcedural(data, bound, prman->RiProcDynamicLoad(), ExportToPrman_Free);

	return true;
}


}



Alfred::Task ocs_fastdiststart_addTask(
	Alfred::Job& job, 
	const char* filename, 
	int number, 
	Alfred::Task& prevwaittask, 
	bool bRemote
	)
{
	Alfred::Task waittask("\"wait %d\"", number);
	{
		Alfred::Cmd cmd = Alfred::RemoteCmd("ocellarisCmd.exe -waitjob \"%s\"", 
			filename);
		cmd.service  = "pixarmtor";
		cmd.atmost = 1;
		cmd.atleast = 1;
		cmd.tags  = "";
		cmd.bRemote = bRemote;
		waittask.cmds.push_back(cmd);
	}
	if( !prevwaittask.title.empty())
		waittask.subtasks.push_back(Alfred::Instance(prevwaittask));
	job.subtasks.push_back(waittask);

	Alfred::Task task("\"read %d\"", number);
	{
		Alfred::Cmd cmd = Alfred::RemoteCmd("ocellarisCmd.exe -readjob \"%s\"", 
			filename);
		cmd.service  = "pixarmtor";
		cmd.atmost = 1;
		cmd.atleast = 1;
		cmd.tags  = "";
		cmd.expand  = "1";
		cmd.bRemote = bRemote;
		task.cmds.push_back(cmd);
	}
	task.subtasks.push_back(
		Alfred::Instance(waittask)
		);
//	job.cleanup.push_back(
//		Alfred::CmdDeleteFile(filename)
//		);

	prevwaittask = waittask;
	return task;
}
