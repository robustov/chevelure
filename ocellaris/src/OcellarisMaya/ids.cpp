#include "stdafx.h"
#include "pre.h"

#include "ocCustomPass.h"
MTypeId ocCustomPass::id( TypeIdBase+0x11 );
const MString ocCustomPass::typeName( "ocCustomPass" );

#include "ocRSLshader.h"
MTypeId ocRSLshader::id( TypeIdBase+0x10 );
const MString ocRSLshader::typeName( "ocRSLshader" );

#include "ocGeneratorProxy.h"
MTypeId ocGeneratorProxy::id( TypeIdBase+0xf );
const MString ocGeneratorProxy::typeName( "ocGeneratorProxy" );

#include "ocSequenceFile.h"
MTypeId ocSequenceFile::id( TypeIdBase+0xe );
const MString ocSequenceFile::typeName( "ocSequenceFile" );

//#include "ocGeneratorSet.h"
//MTypeId ocGeneratorSet::id( TypeIdBase+0xd );
//const MString ocGeneratorSet::typeName( "ocGeneratorSet" );


#include "ocJointVis.h"
MTypeId ocJointVis::id( TypeIdBase+0xc );
const MString ocJointVis::typeName( "ocJointVis" );

#include "ocLight.h"
MTypeId ocLight::id( TypeIdBase+0xb );
const MString ocLight::typeName( "ocLight" );

#include "ocRenderPass.h"
MTypeId ocRenderPass::id( TypeIdBase+0xa );
const MString ocRenderPass::typeName( "ocRenderPass" );

#include "ocSurfaceShader.h"
MTypeId ocSurfaceShader::id( TypeIdBase+0x09);
const MString ocSurfaceShader::typeName( "ocSurfaceShader" );

#include "ocOcclusionPass.h"
MTypeId ocOcclusionPass::id( TypeIdBase+0x08 );
const MString ocOcclusionPass::typeName( "ocOcclusionPass" );

#include "ocRootTransform.h"
MTypeId ocRootTransform::id( TypeIdBase+07 );
const MString ocRootTransform::typeName( "ocRootTransform" );

#include "ocFile.h"
MTypeId ocFile::id( TypeIdBase+0x06 );
const MString ocFile::typeName( "ocFile" );

#include "ocParticleAttribute.h"
MTypeId ocParticleVectorAttribute::id( TypeIdBase+05 );
const MString ocParticleVectorAttribute::typeName( "ocParticleVectorAttribute" );

MTypeId ocParticleFloatAttribute::id( TypeIdBase+04 );
const MString ocParticleFloatAttribute::typeName( "ocParticleFloatAttribute" );

#include "ocParticleInstancer.h"
MTypeId ocParticleInstancer::id( TypeIdBase+03 );
const MString ocParticleInstancer::typeName( "ocParticleInstancer" );

#include "ocInstance.h"
MTypeId ocInstance::id( TypeIdBase+02 );
const MString ocInstance::typeName( "ocInstance" );

#include "sloTranslator.h"
const MString sloTranslator::typeName( "sloTranslator" );

// �� ������� ������������ � OcellarisExport
#include "ocShaderParameter.h"
const MString ocShaderParameter::typeName( "ocShaderParameter" );
MTypeId ocShaderParameter::id( TypeIdBase+01 );

// ������������ � OcellarisExport
//MTypeId cls::MInstancePxData::id( ocellarisTypeIdBase+00 );
//const MString cls::MInstancePxData::typeName( "MInstancePxData" );

//#include "ocCacheRenderData.h"
//MTypeId ocCacheRenderData::id( TypeIdBase+0x6 );
//const MString ocCacheRenderData::typeName( "ocCacheRenderData" );


#include "ocPopupShaderListMenu.h"
const MString ocPopupShaderListMenu::typeName( "ocPopupShaderListMenu" );


#include "ocFreezeVerts.h"
const MString ocFreezeVerts::typeName( "ocFreezeVerts" );


#include "ocEditStringCmd.h"
const MString ocEditStringCmd::typeName( "ocEditStringCmd" );

#include "ocPolyShaderLayer.h"
const MString ocPolyShaderLayer::typeName( "ocPolyShaderLayer" );

