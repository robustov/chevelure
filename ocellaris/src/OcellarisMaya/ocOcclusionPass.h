#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ocOcclusionPassNode.h
//
// Dependency Graph Node: ocOcclusionPass

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 

class ocOcclusionPass : public MPxNode
{
public:
	ocOcclusionPass();
	virtual ~ocOcclusionPass(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_renderable;
	static MObject i_scale;
	static MObject i_camera;

	static MObject i_lightwarp;
	static MObject i_envmap;
	static MObject i_envspace;
	static MObject i_maxpixdist;
	static MObject i_distr;
	static MObject i_bias;
	static MObject i_hitmode;
	static MObject i_falloffmode;
	static MObject i_falloff;
	static MObject i_samples;
	static MObject i_subset;
	static MObject i_maxdist;
	static MObject i_coneangle; 
	static MObject i_sides;
	static MObject i_maxvar;

public:
	static MTypeId id;
	static const MString typeName;
};

