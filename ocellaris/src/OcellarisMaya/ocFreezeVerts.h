#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ocFreezeVertsCmd.h
// MEL Command: ocFreezeVerts

#include <maya/MPxCommand.h>

class ocFreezeVerts : public MPxCommand
{
public:
	static const MString typeName;
	ocFreezeVerts();
	virtual	~ocFreezeVerts();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

