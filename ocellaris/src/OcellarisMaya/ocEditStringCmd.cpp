#include "stdafx.h"
#include "ocEditStringCmd.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include "ocRSLshader.h"

ocEditStringCmd::ocEditStringCmd()
{
}
ocEditStringCmd::~ocEditStringCmd()
{
}

MSyntax ocEditStringCmd::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

//	syntax.addArg(MSyntax::kString);	// arg0
	syntax.addFlag("us", "updateshader", MSyntax::kBoolean);
	syntax.setObjectType( MSyntax::kSelectionList, 1 );
	return syntax;
}

MStatus ocEditStringCmd::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	bool updateshader = true;
	argData.getFlagArgument("us", 0, updateshader);

	MSelectionList list;
	argData.getObjects( list );

	for(int i=0; i<(int)list.length(); i++)
	{
		MPlug plug;
		if( !list.getPlug(i, plug)) 
			continue;

		MString val;
		plug.getValue(val);

		std::string filename = "c:/temp/shader.txt";
		FILE* file = fopen( filename.c_str(), "wt");
		if( !file)
			continue;
		fputs(val.asChar(), file);
		fclose(file);

		std::string cmd = "notepad \""+filename+"\"";
		system(cmd.c_str());

		file = fopen( filename.c_str(), "rt");
		if( !file)
			continue;

		std::string newval;
		char buf[1024];
 		while( !feof(file) ) 
 		{
			buf[0]=0;
			fgets(buf, 1024, file);
			newval += buf;
		}
		fclose(file);
		plug.setValue( newval.c_str());

		// UPDATE SHADER
		if( updateshader)
			ocRSLshader::updateShader(plug.node());
		
	}

	return MS::kSuccess;
}

void* ocEditStringCmd::creator()
{
	return new ocEditStringCmd();
}

