#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ocExportAnimationCmd.h
// MEL Command: ocExportAnimation

#include <maya/MPxCommand.h>

class ocExportAnimation : public MPxCommand
{
public:
	static const MString typeName;
	ocExportAnimation();
	virtual	~ocExportAnimation();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

