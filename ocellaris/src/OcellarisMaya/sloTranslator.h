#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: sloTranslatorNode.h
//
// Dependency Graph Node: sloTranslator

#include <maya/MPxFileTranslator.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 

class sloTranslator : public MPxFileTranslator
{
public:
	static const MString typeName;
	sloTranslator();
	virtual ~sloTranslator(); 

	static void* creator();

	///
	virtual MStatus		reader ( const MFileObject& file,
								 const MString& optionsString,
								 FileAccessMode mode);
	///
	virtual MStatus		writer ( const MFileObject& file,
								 const MString& optionsString,
								 FileAccessMode mode);
	///
	virtual bool		haveReadMethod () const;
	///
	virtual bool		haveWriteMethod () const;
	///
	virtual bool		haveNamespaceSupport () const;
	///
	virtual MString     defaultExtension () const;
	///
	virtual MString     filter () const;
	///
	virtual bool        canBeOpened () const;
	///
	virtual MPxFileTranslator::MFileKind identifyFile (	const MFileObject& file,
														const char* buffer,
														short size) const;

};

