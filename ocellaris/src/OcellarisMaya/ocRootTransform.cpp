#include "stdafx.h"
#include "ocRootTransform.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnPluginData.h>
#include <maya/MDagPath.h>
#include <maya/MProgressWindow.h>
#include "ocellaris/MInstancePxData.h"

#include <maya/MGlobal.h>

MObject ocRootTransform::i_input;
MObject ocRootTransform::o_drawCache;

ocRootTransform::ocRootTransform()
{
}
ocRootTransform::~ocRootTransform()
{
}

MStatus ocRootTransform::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;

	if( plug == o_drawCache )
	{
		return computeOutput(plug, data);
	}
	return MS::kUnknownParameter;
}

MStatus ocRootTransform::computeOutput(
	const MPlug& plug, 
	MDataBlock& data)
{
	// ��� � ��� �� ����� �����

	/*/
	MStatus stat;
	bool bProgress = MGlobal::mayaState()==MGlobal::kInteractive;

	cls::MInstancePxData* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( o_drawCache, &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<cls::MInstancePxData*>(userdata);
	MFnPluginData fnDataCreator;
	bool bNew = false;
	if( !pData)
	{
		MTypeId tmpid( cls::MInstancePxData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<cls::MInstancePxData*>(fnDataCreator.data( &stat ));
		bNew = true;
	}

	if( bProgress)
	{
		MProgressWindow::reserve();
		MProgressWindow::setInterruptable(false);
		MProgressWindow::setTitle("Ocs cache");
		MProgressWindow::startProgress();
		MProgressWindow::setProgressRange(0, 100);
		MProgressWindow::setProgressStatus("inprogress...");
		MProgressWindow::setProgress(100);
	}

	cls::IRender* render = pData->drawcache.asRender();

	Math::Box3f box;
	cls::IExportContext context;
	ChiefGenerator chef;
	ocellarisDefaultGenerators(OCGT_SCENE, chef, &context);
	MObject thisObject = thisMObject();
	MDagPath dag = MDagPath::getAPathTo(thisObject, &stat);
	displayString( dag.fullPathName().asChar());
	for( int i=0; i<(int)dag.childCount(); i++)
	{
		MObject obj = dag.child(i);
		MDagPath path;
		MDagPath::getAPathTo(obj, path);
		cls::INode* root = ocNodeTree(path, chef, &context);

		Math::Matrix4f t = Math::Matrix4f::id;
		root->Render(render, box, t, &context);
	}
	pData->drawcache.setLoaded(box);

	if( bProgress)
		MProgressWindow::endProgress();

	if(bNew)
		outputData.set(pData);
	data.setClean(plug);
	/*/
	return MS::kSuccess;
}

void* ocRootTransform::creator()
{
	return new ocRootTransform();
}

MStatus ocRootTransform::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		{
			i_input = numAttr.create( "input","in", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_input, atInput|atKeyable);
		}
		{
			o_drawCache = typedAttr.create( "drawCache", "dch", cls::MInstancePxData::id, MObject::kNullObj, &stat);
			::addAttribute(o_drawCache, atOutput|atWritable|atUnStorable);

			attributeAffects(i_input, o_drawCache);
		}
//		if( !SourceMelFromResource(MhInstPlugin, "AEocRootTransformTemplate", "MEL"))
//		{
			if( !MGlobal::sourceFile("AEocRootTransformTemplate.mel"))
			{
				displayString("error source AEocRootTransformTemplate.mel");
			}
//		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

