#pragma once

#include "pre.h"
#include <maya/MPxLocatorNode.h>
#include "ocellaris/MInstancePxData.h"
#include "ocellaris/OcellarisExport.h"
//#include "ocCacheRenderData.h"

 
class ocParticleInstancer : public MPxLocatorNode
{
public:
	ocParticleInstancer();
	virtual ~ocParticleInstancer(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	virtual void draw(
		M3dView &view, const MDagPath &path, 
		M3dView::DisplayStyle style,
		M3dView::DisplayStatus status);

	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 

	static void* creator();
	static MStatus initialize();

	cls::MInstancePxData* getCache(MDataBlock data, MObject& val) const;
//	cls::MInstancePxData* getCache(MObject thisNode, MObject& val) const;
//	MPlug getDataPlug(MObject thisNode, MObject particle, const char* attibute) const;

	void process(
		MObject thisNode, 
		cls::PA<int>& res_id, 
		cls::PA<Math::Matrix4f>& res_pos, 
		std::vector<float>& res_frame) const;

	MObject getParticle();

	MPlug getDataPlug(
		MObject thisNode, 
		MObject particle, 
		const char* attibute) const;

	bool Save(
		cls::IRender* render, 
		Math::Box3f& box, 
		cls::IExportContext* context);

public:
	static MObject i_drawCache;
	static MObject i_id;
	static MObject i_position;

	static MObject i_rotationType;
	// ���� ��� �����
	static MObject i_dirvector, i_upvector;
	// ���� ��� ���+����
	static MObject i_axis, i_angle;


	static MObject i_scale;
	static MObject i_phase;

	static MObject o_ocsCache;

	// display
	static MObject i_viewParticles;

public:
	static MTypeId id;
	static const MString typeName;
};


