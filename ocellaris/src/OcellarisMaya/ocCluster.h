#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ocClusterNode.h
//
// Dependency Graph Node: ocCluster

#include <maya/MPxTransform.h>
#include <maya/MPxTransformationMatrix.h>

class ocCluster : public MPxTransform
{
public:
	ocCluster();
	virtual ~ocCluster(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_input;			// Example input attribute
	static MObject o_output;		// Example output attribute

public:
	static MTypeId id;
	static const MString typeName;
};

