#include "stdafx.h"
#include "ocellaris/IParameter.h"
#pragma warning (disable:4311)

typedef struct tagDEBUGHELPER
{
	DWORD dwVersion;
	BOOL (WINAPI *ReadDebuggeeMemory)( struct tagDEBUGHELPER *pThis, DWORD dwAddr, DWORD nWant, VOID* pWhere, DWORD *nGot );
	// from here only when dwVersion >= 0x20000
	DWORDLONG (WINAPI *GetRealAddress)( struct tagDEBUGHELPER *pThis );
	BOOL (WINAPI *ReadDebuggeeMemoryEx)( struct tagDEBUGHELPER *pThis, DWORDLONG qwAddr, DWORD nWant, VOID* pWhere, DWORD *nGot );
	int (WINAPI *GetProcessorType)( struct tagDEBUGHELPER *pThis );
} DEBUGHELPER;

typedef HRESULT (WINAPI *CUSTOMVIEWER)( DWORD dwAddress, DEBUGHELPER *pHelper, int nBase, BOOL bUniStrings, char *pResult, size_t max, DWORD reserved );

#define ADDIN_API __declspec(dllexport)

extern "C" 
{
	ADDIN_API HRESULT WINAPI Ocs_Param( DWORD dwAddress, DEBUGHELPER *pHelper, int nBase, BOOL bUniStrings, char *pResult, size_t max, DWORD reserved );
}
ADDIN_API HRESULT WINAPI Ocs_Param( DWORD dwAddress, DEBUGHELPER *pHelper, int nBase, BOOL bUniStrings, char *pResult, size_t max, DWORD reserved )
{
	char buf[1024]="<XXXX>";
	DWORD nGot;

	if (pHelper->dwVersion<0x20000)
	{
		strncpy(pResult, "pHelper->dwVersion", max-1);
		return S_OK;
	}

	//////////////////////////////////////////
	// �������� cls::Param
	char Parambuf[sizeof(cls::Param)];
	if (pHelper->ReadDebuggeeMemoryEx(pHelper, pHelper->GetRealAddress(pHelper), sizeof(Parambuf), Parambuf, &nGot)!=S_OK)
	{
		strncpy(pResult, "ReadDebuggeeMemoryEx failed", max-1);
		return S_OK;
	}
	if (nGot!=sizeof(Parambuf))
	{
		strncpy(pResult, "nGot!=sizeof(p)", max-1);
		return S_OK;
	}
	cls::Param& p = *((cls::Param*)Parambuf);

	// �������� IParameterbuf
	DWORD addr = ((int*)Parambuf)[0];
	if( !addr)
	{
		strncpy(pResult, "<empty>", max-1);
		return S_OK;
	}
	char IParameterbuf[sizeof(cls::IParameter)];
	if (pHelper->ReadDebuggeeMemoryEx(pHelper, addr, sizeof(IParameterbuf), IParameterbuf, &nGot)!=S_OK)
	{
		strncpy(pResult, "ReadDebuggeeMemoryEx failed", max-1);
		return S_OK;
	}
	if (nGot!=sizeof(IParameterbuf))
	{
		strncpy(pResult, "nGot!=sizeof(p)", max-1);
		return S_OK;
	}
	((int*)Parambuf)[0] = (int) ((char*)IParameterbuf);

	// ���������
	const char* _strtype = cls::str_type(p->type);
	const char* _strinterp = cls::str_interpolation(p->interp);
	char strtype[100];
	_snprintf( strtype, 100, "%s %s", _strinterp, _strtype);

	if( p.empty())
	{
		_snprintf( buf, 1024, "%s: <empty>", strtype);
		strncpy(pResult, buf, max-1);
		return S_OK;
	}
	else
	{
		if( p.size()!=1)
		{
			_snprintf( buf, 1024, "%s[%d]=...", strtype, p.size());
			strncpy(pResult, buf, max-1);
			return S_OK;
		}
		// 
		// ������
		char value[1024];
		if( p->type!=cls::PT_STRING)
		{
			if (pHelper->ReadDebuggeeMemoryEx(pHelper, (DWORD)p.data(), p.stride(), value, &nGot)!=S_OK)
			{
				strncpy(pResult, "ReadDebuggeeMemoryEx failed", max-1);
				return S_OK;
			}
			if (nGot!=p.stride())
			{
				strncpy(pResult, "nGot!=sizeof(p)", max-1);
				return S_OK;
			}
		}
		else
		{
			const int strsize = 32;
			if (pHelper->ReadDebuggeeMemoryEx(pHelper, (DWORD)p.data(), strsize, value, &nGot)!=S_OK)
			{
				strncpy(pResult, "ReadDebuggeeMemoryEx failed", max-1);
				return S_OK;
			}
			if (nGot!=strsize)
			{
				strncpy(pResult, "nGot!=sizeof(p)", max-1);
				return S_OK;
			}
			value[28]='.';
			value[29]='.';
			value[30]='.';
			value[31]=0;
		}

		switch( p->type)
		{
		case cls::PT_VOID:
		case cls::PT_HALF:
		case cls::PT_HPOINT:
			_snprintf( buf, 1024, "%s", strtype);
			break;
		case cls::PT_BOOL:
			{
				bool& v = *(bool*)value;
				_snprintf( buf, 1024, "%s: %s", strtype, v?"true":"false");
				break;
			}
		case cls::PT_STRING:
			{
				_snprintf( buf, 1024, "%s: \"%s\"", strtype, value);
				break;
			}
		case cls::PT_FLOAT:
			{
				float& v = *(float*)value;
				_snprintf( buf, 1024, "%s: %g", strtype, v);
				break;
			}
		case cls::PT_DOUBLE:
			{
				double& v = *(double*)value;
				_snprintf( buf, 1024, "%s: %g", v);
				break;
			}
		case cls::PT_POINT:		//!< point
		case cls::PT_VECTOR:	//!< vector
		case cls::PT_NORMAL: 	//!< normal
		case cls::PT_COLOR: 	//!< color
			{
				Math::Vec3f& v = *(Math::Vec3f*)value;
				_snprintf( buf, 1024, "%s: {%g, %g, %g}", strtype, v.x, v.y, v.z);
				break;
			}
		case cls::PT_MATRIX:
			{
				Math::Matrix4f& m = *(Math::Matrix4f*)value;
				_snprintf( buf, 1024, "%s: {%g, %g, %g, %g} {%g, %g, %g, %g} {%g, %g, %g, %g} {%g, %g, %g, %g}", 
					strtype, 
					m[0].x, m[0].y, m[0].z, m[0].w,
					m[1].x, m[1].y, m[1].z, m[1].w,
					m[2].x, m[2].y, m[2].z, m[2].w,
					m[3].x, m[3].y, m[3].z, m[3].w
					);
				break;
			}
		case cls::PT_INT8: case cls::PT_UINT8: case cls::PT_INT16: case cls::PT_UINT16:
		case cls::PT_INT:
		case cls::PT_UINT:
			{
				int& v = *(int*)value;
				_snprintf( buf, 1024, "%s: %d", strtype, v);
				break;
			}
		case cls::PT_BOX:
			{
				Math::Box3f& v = *(Math::Box3f*)value;
				_snprintf( buf, 1024, "%s: min={%g, %g, %g} max={%g, %g, %g}", strtype, 
					v.min.x, v.min.y, v.min.z,
					v.max.x, v.max.y, v.max.z
					);
				break;
			}
		default:
			return E_FAIL;
		}
	}
	strncpy(pResult, buf, max-1);
	return S_OK;
}

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    return TRUE;
}

