%module ExportContext
 %{
 #include "pre.h"
 #include "../ocellarisExport/ExportContextImpl.h"
 typedef ExportContextImpl ExportContext;
 %}
 
class ExportContext 
{
public:
	ExportContext();
	~ExportContext(); 
	
	// ������ env �� ������� � �����
	void clear();
	// �������� � env 
	bool setenv(const char* env, const char* val);
	const char* getenv(const char* env) const;

	// ������ ����������� �� ���������
	bool addDefaultGenerators(const char* options);

	// ����������� ����������
	bool prepareGenerators();

	// ������ ���, �������� � �����������
	int addnodes(const char* root, const char* options);

	// ������ �������
	int addpass(const char* passnode, const char* options);

	// ��
	int addlight(const char* lightnode, const char* options);

	// �������� ���� � ��������� ������� ����� (��� ���� ���������� �� ���������)
	int addframe(float frame);

	// ���������� � �������� (�����)
	int preparenodes();

	// ���������� �������
	int doexport(const char* dumpoption);

	// dump
	void dump(const char* options) const;
	
};

