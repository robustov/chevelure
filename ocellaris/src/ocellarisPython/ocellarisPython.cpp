// ocellarisPython.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"

HANDLE hMyModule = NULL;

int GetOcellarisModule()
{
	return (int)hMyModule;
}

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		{
			hMyModule = hModule;
			break;
		}
	case DLL_PROCESS_DETACH:
		{
			break;
		}
	}
    return TRUE;
}

