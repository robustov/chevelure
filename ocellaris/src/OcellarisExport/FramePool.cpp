#include "stdafx.h"
#include "FramePool.h"

using namespace cls;

// ��������� ��� ��� ExportFrame ������ 
bool FramePool::RenderFrame::isReady()
{
	// ��������� ��� ExportFrame ��� ����� RenderFrame (���� ��� ������ - �������� � ����������)
	std::map<MTime, FramePool::ExportFrame*>::iterator itefrf = this->exportframes.begin();
	for( ; itefrf!=this->exportframes.end(); itefrf++)
	{
		FramePool::ExportFrame* exportframe = itefrf->second;
		if(!exportframe->bReady)
			return false;
	}
	return true;
}
void FramePool::RenderFrame::dump(FILE* file) const
{
	std::map<MTime, ExportFrame*>::const_iterator it = exportframes.begin();
	for( ; it != exportframes.end(); it++)
	{
		const ExportFrame* rt = it->second;
		fprintf(file, "  EXPORT TIME %f:\n", it->first.value());
		rt->dump(file);
	}
}
void FramePool::ExportFrame::dump(FILE* file) const
{
	fprintf(file, "  ExportFrame: ref=%d %s nodes=%d renderframes=%d:\n", ref_count, bReady?"ready":"notready", nodes.size(), renderframes.size());
}




//////////////////////////////////////////////
// 
// FramePool
// 
FramePool::FramePool()
{
}
FramePool::~FramePool()
{
}

// ��������
void FramePool::clear()
{
	renderframes.clear();
	exportframes.clear();
}

// �������� render frame
int FramePool::addrenderframe(MTime time)
{
	RenderFrame& rf = renderframes[time];
	rf.frame = time;
	return 1;
}

// �������� pass � render frame
int FramePool::addpass(cls::INode& node, MTime frametime)
{
	std::map<MTime, RenderFrame>::iterator it = renderframes.find(frametime);
	if(it==renderframes.end()) 
		return 0;

	RenderFrame& rf = it->second;
	PassRenderFrameData& prf = rf.passrenderdata[&node];
	return 1;
}

// �������� ���� � ���� ��� ����� � ���
int FramePool::add(cls::INode& node, MTime frametime, MTime exporttime)
{
	std::map<MTime, RenderFrame>::iterator it = renderframes.find(frametime);
	if(it==renderframes.end()) 
		return 0;
	RenderFrame& rf = it->second;

	ExportFrame& ef = exportframes[exporttime];

	ef.addref();
	NodeExportFrameData& data = ef.nodes[&node];
	ef.renderframes.insert(&rf);

	rf.exportframes[exporttime] = &ef;

	return 1;
}

// dump
void FramePool::dump(FILE* file) const
{
	{
		fprintf(file, "Render Frames:\n");
		std::map<MTime, RenderFrame>::const_iterator it = renderframes.begin();
		for( ; it != renderframes.end(); it++)
		{
			const RenderFrame& rt = it->second;
			fprintf(file, " FRAME %f:\n", it->first.value());
			rt.dump(file);
		}
	}
	{
		fprintf(file, "Export Frames:\n");
		std::map<MTime, ExportFrame>::const_iterator it = exportframes.begin();
		for( ; it != exportframes.end(); it++)
		{
			const ExportFrame& rt = it->second;
			fprintf(file, "  FRAME %f:\n", it->first.value());
			rt.dump(file);
		}
	}
	fflush(file);
}
FramePool::NodeExportFrameData* FramePool::getNodeCache(
	MTime frame, 
	cls::INode& node,				// ������
	cls::IExportContext* context	// �������� ��������
	)
{
	std::map<MTime, ExportFrame>::iterator it = exportframes.find(frame);
	if(it == exportframes.end())
	{
		context->error("FramePool::NodeExportFrameData frame %f not found\n", frame.as(MTime::uiUnit()));
		return NULL;
	}
	ExportFrame& ef = it->second;

	std::map<INode*, NodeExportFrameData>::iterator itn = ef.nodes.find(&node);
	if( itn==ef.nodes.end())
	{
		context->error("FramePool::NodeExportFrameData node %s not found\n", node.getName());
		return NULL;
	}

	NodeExportFrameData& nodedata = itn->second;
	return &nodedata;
}

