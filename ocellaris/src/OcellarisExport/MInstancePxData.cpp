#include "stdafx.h"
//
// Copyright (C) 
// File: ocInstanceDrawCacheCmd.cpp
// MEL Command: ocInstanceDrawCache

#include "ocellaris/MInstancePxData.h"

#include <maya/MGlobal.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include <sstream>

#include "ocellaris\renderFileImpl.h"
#include "ocellaris\renderOpenglImpl.h"
#include "ocellaris\renderCacheImpl.h"
#include "ocellaris/OcellarisExport.h"

#include "mathNgl\mathNgl.h"
#include "mathNmaya\mathNmaya.h"
#include "Util/misc_directory_processing.h"


MTypeId cls::MInstancePxData::id( ocellarisTypeIdBase+00 );
const MString cls::MInstancePxData::typeName( "MInstancePxData" );

cls::MInstancePxData::MInstancePxData()
{
}
cls::MInstancePxData::~MInstancePxData()
{
}

MTypeId cls::MInstancePxData::typeId() const
{
	return cls::MInstancePxData::id;
}

MString cls::MInstancePxData::name() const
{ 
	return cls::MInstancePxData::typeName; 
}

void* cls::MInstancePxData::creator()
{
	return new cls::MInstancePxData();
}

void cls::MInstancePxData::copy( const MPxData& other )
{
	const cls::MInstancePxData* arg = (const cls::MInstancePxData*)&other;
	this->drawcache = arg->drawcache;
}

MStatus cls::MInstancePxData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("cls::MInstancePxData: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus cls::MInstancePxData::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus cls::MInstancePxData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	serialize(stream);

	return MS::kSuccess;
}

MStatus cls::MInstancePxData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

void cls::MInstancePxData::serialize(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version>=0)
	{
//		stream >> ;
	}
}

