#include "stdafx.h"
#include "ocellaris/ocGeneratorSet.h"
#include "ocellaris/OcellarisExport.h"
#include "ocellaris/IAttributeGenerator.h"
#include "Util/DllProcedure.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnStringData.h>

#include <maya/MGlobal.h>

MObject cls::ocGeneratorSet::i_type;
MObject cls::ocGeneratorSet::i_generator;
MObject cls::ocGeneratorSet::i_calltypeAttribute;

MTypeId cls::ocGeneratorSet::id( ocellarisTypeIdBase+0xd );
const MString cls::ocGeneratorSet::typeName( "ocGeneratorSet" );

cls::ocGeneratorSet::ocGeneratorSet()
{
}
cls::ocGeneratorSet::~ocGeneratorSet()
{
}

MStatus cls::ocGeneratorSet::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;

	return MS::kUnknownParameter;
}

void* cls::ocGeneratorSet::creator()
{
	return new ocGeneratorSet();
}

bool cls::ocGeneratorSet::setInternalValue( 
	const MPlug& plug,
	const MDataHandle& data)
{
	if(plug==i_generator)
	{
		MString nameval = data.asString();
		Util::DllProcedure dll;
		if( dll.LoadFormated(nameval.asChar(), "OcellarisExport"))
		{
			cls::GENERATOR_CREATOR pc = (cls::GENERATOR_CREATOR)dll.proc;
			cls::IGenerator* gen = (*pc)();
			if( gen)
			{
				// ��� �������� � ����
				gen->OnLoadToMaya(thisMObject());
			}
		}
	}
	return MPxNode::setInternalValue( 
		plug,
		data);
}

MStatus cls::ocGeneratorSet::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnEnumAttribute enumAttr;
	MFnStringData stringData;
	MStatus				stat;

	try
	{
		{
			i_type = enumAttr.create ("generatorType", "gent");
			enumAttr.addField("ATTRIBUTE", IGenerator::ATTRIBUTE);
			enumAttr.addField("DEFORM", IGenerator::DEFORM);
			enumAttr.addField("GEOMETRY", IGenerator::GEOMETRY);
			enumAttr.addField("LAYER", IGenerator::LAYER);
			enumAttr.addField("NODEFILTER", IGenerator::NODEFILTER);
			enumAttr.addField("NODE", IGenerator::NODE);
			enumAttr.addField("PASS", IGenerator::PASS);
			enumAttr.addField("SHADER", IGenerator::SHADER);
			enumAttr.addField("TRANSFORM", IGenerator::TRANSFORM);
			::addAttribute(i_type, atInput);
		}
		{
			i_generator = typedAttr.create( "generatorName", "genn",
				MFnData::kString, stringData.create(&stat), &stat);
			typedAttr.setInternal(true);
			::addAttribute(i_generator, atInput);
		}
		{
			i_calltypeAttribute = enumAttr.create ("calltypeAttribute", "catat", IAttributeGenerator::CT_BEFORE_TRANSFORM);
			enumAttr.addField("BEFORE_TRANSFORM",	IAttributeGenerator::CT_BEFORE_TRANSFORM);
			enumAttr.addField("BEFORE_NODE",		IAttributeGenerator::CT_BEFORE_NODE);
			enumAttr.addField("BEFORE_SHADER",		IAttributeGenerator::CT_BEFORE_SHADER);
			enumAttr.addField("BEFORE_GEOMETRY",	IAttributeGenerator::CT_BEFORE_GEOMETRY);
			::addAttribute(i_calltypeAttribute, atInput);
		}
		if( !MGlobal::sourceFile("AEocGeneratorSetTemplate.mel"))
		{
			displayString("error source AEocGeneratorSetTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

