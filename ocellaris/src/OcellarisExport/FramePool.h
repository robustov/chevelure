#pragma once

#include "ocellaris/IOcellaris.h"
#include "ocellaris/renderCacheImpl.h"

#include <maya/MFn.h>
#include <maya/MTypeId.h>
#include <maya/MObject.h>

#include "ocellaris/INode.h"
#include "Node.h"
#pragma warning( disable : 4251)

namespace cls
{
//! @ingroup plugin_group
//! \brief �������� ������ ��� ��������
//! 
//! ������ ������ � �������� ������� ��� �� ��������
//! 
//! 
struct FramePool
{
public:
	FramePool();
	~FramePool();

	// ��������
	void clear();

	// �������� render frame
	int addrenderframe(MTime time);

	// �������� pass � render frame
	int addpass(cls::INode& node, MTime time);

	// �������� ����
	int add(cls::INode& node, MTime frametime, MTime exporttime);

	// dump
	void dump(FILE* file)const;


	// ������ ��� � ���� ���������� ��� �������� ���� 
	struct NodeExportFrameData;
	NodeExportFrameData* getNodeCache(
		MTime frame, 
		cls::INode& node,				// ������
		cls::IExportContext* context	// �������� ��������
		);

public:
	// ����� ��������
	struct NodeExportFrameData
	{
		cls::renderCacheImpl<> cache;
		Math::Box3f box;
	};
	struct PassRenderFrameData
	{
		cls::renderCacheImpl<> cache;
	};
	struct RenderFrame;
	struct ExportFrame
	{
		int ref_count;
		bool bReady;

		// ���� ������� ��������� ��������������
		std::map<INode*, NodeExportFrameData> nodes;
		// ����� � ������� ������������ 
		std::set<cls::FramePool::RenderFrame*> renderframes;

		// dump
		void dump(FILE* file)const;
		// 
		int addref(){ref_count++; return ref_count;};
		int delref(){ref_count--; return ref_count;};

		ExportFrame(){ref_count=0;bReady=false;}
	};
	// �����
	struct RenderFrame
	{
		// �����
		MTime frame;
		// ExportFrame ��������� ��� ��������
		std::map<MTime, ExportFrame*> exportframes;
		// renderpass �������������� � ���� ����
		// � ������ ��� �������� �� ������� � ������
		std::map<INode*, PassRenderFrameData> passrenderdata;
		// ������� ��������
		bool bCompleted;
	public:
		// dump
		void dump(FILE* file) const;
		// ��������� ��� ��� ExportFrame ������ 
		bool isReady();
		// ��� ��������!
		bool isCompeted(){return bCompleted;};

		RenderFrame(){bCompleted=false;};
	};
	// ������� �������� (������� �������)
	std::map<MTime, RenderFrame> renderframes;

	// ������� �������� (������� �������)
	std::map<MTime, ExportFrame> exportframes;

};

}