#include "stdafx.h"
#include "../generators/MeshGenerator.h"
#include "../generators/NuCurveGenerator.h"

#include "../generators/TransformGenerator.h"
#include "../generators/NameAttributeGenerator.h"
#include "../generators/SlimShaderGenerator.h"
#include "../generators/LightGenerator.h"
#include "../generators/ParticleGenerator.h"
#include "../generators/ocInstancerGenerator.h"
#include "../generators/ocGeometryGenerator.h"
#include "../generators/FluidGenerator.h"

#include "../generators/StandartNodeFilter.h"
#include "../generators/FullTransformGenerator.h"
#include "../generators/Transform/SkipTransformGenerator.h"

#include "ocellaris/renderFileImpl.h"
#include "ocellaris/renderProxy.h"
#include <maya/MItDag.h>
#include <mathNmaya/mathNmaya.h>

#include "ocellaris/OcellarisExport.h"
#include "ocellaris/ChiefGenerator.h"
#include "Node.h"



void ocNodeTree(
	Node& root, 
	int level, 
	ChiefGenerator& chef, 
	cls::IExportContext* context)
{
//	int id = root.getId();
	int id = 0;
//	if( !context->setStamp(id))
//	{
//		return;
//	}

	// ����������
	root.PrepareGenerators(context);

/*/
	// childs
	for( unsigned i=0; i<root.getPath().childCount(); i++)
	{
		MObject obj = root.getPath().child(i);

		MDagPath path;
		MDagPath::getAPathTo(obj, path);
		root.childs.push_back(Node());
		Node& ch = root.childs.back();

		ch.set(path);
		ocNodeTree( ch, level+1, chef, context);
	}
/*/
}

//! ���������� ������ �������������� �������� INode 
//! ��������� �������� ����
cls::INode* ocNodeTree( 
	MDagPath path, 
	ChiefGenerator& chief, 
	cls::IExportContext* context)
{
	Node* pNode = new Node();

	pNode->set(path);

	chief.prepareGenerators(context);
	ocNodeTree( *pNode, 0, chief, context);

	return pNode;
}

//! ��������� INode ��� ������ �������
cls::INode* ocNodeTree_Single( 
	MObject node,					//<! ������
	ChiefGenerator& chief,			//<! �������� ����������� �� ���������
	cls::IExportContext* context	//<! �������� ��������
	)
{
	Node* pNode = new Node();

	pNode->set(node);

	chief.prepareGenerators(context);

		// ����������
		pNode->PrepareGenerators(context);

//	chief.endExport();

	return pNode;
}

FullTransformGenerator theFullTransformGenerator;
SkipTransformGenerator theSkipTransformGenerator;

//! ���������� ������ �������������� �������� INode 
//! ��������� ������ ���� set
cls::INode* ocNodeTree_fromSet( 
	MObject setobj,					//<! ���
	ChiefGenerator& chief,			//<! �������� ����������� �� ���������
	cls::IExportContext* context	//<! �������� ��������
	)
{
	chief.prepareGenerators(context);
	Node* pRoot = new Node();

	context->setEnvironment("IGNOREOVERRIDE", "1");

/*/

	MFnSet set(setobj);
	MSelectionList sl;
	set.getMembers(sl, true);
	for( int i=0; i<(int)sl.length(); i++)
	{
		MDagPath dag;
		if( !sl.getDagPath(i, dag)) continue;

		pRoot->childs.push_back(Node());
		Node& transformNode = pRoot->childs.back();
		transformNode.set(dag);
		transformNode.AddGenerator( &theFullTransformGenerator);

		transformNode.childs.push_back(Node());
		Node& node = transformNode.childs.back();
		node.set(dag);
		ocNodeTree( node, 0, chief, context);

//		chief.endExport();
	}
/*/
	return pRoot;
}
//! ���������� ������ �������������� �������� INode 
//! ��������� ������ ���
cls::INode* ocNodeTree_fromList( 
	std::list<MDagPath>& list,				//<! ���
	ChiefGenerator& chief,					//<! �������� ����������� �� ���������
	cls::ITransformGenerator* generator,	//<! ��������� �������� �����������
	cls::IExportContext* context			//<! �������� ��������
	)
{
	chief.prepareGenerators(context);
	Node* pRoot = new Node();

	context->setEnvironment("IGNOREOVERRIDE", "1");

/*/
	std::list<MDagPath>::iterator it = list.begin(); 
	for( ; it != list.end(); it++)
	{
		MDagPath dag = *it;

		pRoot->childs.push_back(Node());
		Node& transformNode = pRoot->childs.back();
		transformNode.set(dag);
		if( !generator)
			transformNode.AddGenerator( &theFullTransformGenerator);
		else
			transformNode.AddGenerator( generator);

//		chief.startExport();
		transformNode.childs.push_back(Node());
		Node& node = transformNode.childs.back();
		node.set(dag);
		ocNodeTree( node, 0, chief, context);

//		chief.endExport();
	}
/*/
	return pRoot;
}

//! ���������� ������ �������������� �������� INode 
//! ��������� ������ MSelectionList
OCELLARISEXPORT_API cls::INode* ocNodeTree_fromList( 
	MSelectionList& list,					//<! ���
	ChiefGenerator& chief,					//<! �������� ����������� �� ���������
	cls::ITransformGenerator* generator,	//<! ��������� �������� �����������
	cls::IExportContext* context			//<! �������� ��������
	)
{
	chief.prepareGenerators(context);
	Node* pRoot = new Node();

	context->setEnvironment("IGNOREOVERRIDE", "1");

/*/
	for(int i=0; i<(int)list.length(); i++)
	{
		Node newNode;
		MObject dgNode;
		MDagPath dagPath;
		if( list.getDagPath( i, dagPath))
		{
			newNode.set(dagPath);
		}
		else if( list.getDependNode( i, dgNode))
		{
			newNode.set(dgNode);
		}
		else
			continue;
		pRoot->childs.push_back(newNode);

		{
			Node& transformNode = pRoot->childs.back();
			if( !generator)
				transformNode.AddGenerator( &theFullTransformGenerator);
			else
				transformNode.AddGenerator( generator);

//			chief.startExport();
			transformNode.childs.push_back(newNode);
			ocNodeTree( transformNode.childs.back(), 0, chief, context);
//			chief.endExport();
		}
	}
/*/
	return pRoot;
}

cls::INode* ocNodeTreeType(
	int _type,
	ChiefGenerator& chief,			//<! �������� ����������� �� ���������
	cls::IExportContext* context	//<! �������� ��������
	)
{
	chief.prepareGenerators(context);

	MFn::Type type = (MFn::Type)_type;
	Node* pRoot = new Node();

/*/
	// ������� �������
	MItDag it( MItDag::kDepthFirst, type);
	while( ! it.isDone() ) 
	{
		MDagPath dag;
		it.getPath( dag);

		pRoot->childs.push_back(Node());
		Node& transformNode = pRoot->childs.back();
		transformNode.set(dag);
		transformNode.AddGenerator( &theSkipTransformGenerator);

//		chief.startExport();
		transformNode.childs.push_back(Node());
		Node& node = transformNode.childs.back();
		node.set(dag);
		ocNodeTree( node, 0, chief, context);

//		chief.endExport();

		it.next();
	}
/*/
	return pRoot;
}
