#pragma once

#include "ocellaris/IExportContext.h"
#include "ocellaris/renderFileImpl.h"
#include "ocellaris/renderProxy.h"
#include "ocellaris/OcellarisExport.h"
#include "ocellaris/ChiefGenerator.h"

MObject getExternShaderNode(MObject& obj);

struct ExportContext : public cls::renderProxy, public cls::IExportContext
{
protected:
	MObject ocellarisNode;
	MObject currentNode;
public:
	ChiefGenerator::deformgenlist_t deformgenlist;
public:
	ExportContext(
		IRender* render, 
		MObject currentNode
		):cls::renderProxy(render)
	{
		this->currentNode = currentNode;
		ocellarisNode = MObject::kNullObj;
	}
	// getNode
	MObject getNode(
		)
	{
		return currentNode;
	}
	// getOcellarisNode
	MObject getOcellarisNode()
	{
		if( ocellarisNode.isNull())
		{
			ocellarisNode = getExternShaderNode(currentNode);
		}
		return ocellarisNode;
	}
public:
	/// Geometry
	/// 0-D prims - point clouds
	virtual void Points (
		cls::PA<Math::Vec3f>& P			//!< points
		)
	{
		cls::renderProxy::Points(P);
	}
	/// 1-D prims - lines, curves, hair
	virtual void Curves (
		const char *interp, 
		const char *wrap,		//!< wrap
		cls::PA<int>& nverts			//!< int[ncurves] ����� ��������� � ������
		)
	{
		cls::renderProxy::Curves(interp, wrap, nverts);
	}
	virtual void Curves (int ncurves, int nvertspercurve, int order,
						const float *knot, float vmin, float vmax) 
	{
		cls::renderProxy::Curves( ncurves, nvertspercurve, order, knot, vmin, vmax);
	}
	/// Mesh 
	virtual void Mesh(
		const char *interp,		//!< ��� ������������
		cls::PA<int>& loops,			//!< loops per polygon
		cls::PA<int>& nverts,		//!< ����� ��������� � �������� (size = polygoncount)
		cls::PA<int>& verts			//!< ������� ��������� ��������� (size = facevertcount)
		)
	{
		for( unsigned i=0; i<deformgenlist.size(); i++)
			deformgenlist[i]->OnDeformation(this, this, cls::I_MESH);

		cls::renderProxy::Mesh(interp, loops, nverts, verts);
	}
	/// Sphere
	virtual void Sphere(
		float radius, float zmin, float zmax,
		float thetamax=360)
	{
		cls::renderProxy::Sphere(radius, zmin, zmax, thetamax);
	}
	/// Blobby
	virtual void Blobby(
		cls::PA<Math::Matrix4f>& ellipsoids)
	{
		cls::renderProxy::Blobby(ellipsoids);
	}
};
