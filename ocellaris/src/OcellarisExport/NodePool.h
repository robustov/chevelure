#pragma once

#include "ocellaris/IOcellaris.h"

#include <maya/MFn.h>
#include <maya/MTypeId.h>
#include <maya/MObject.h>

#include "ocellaris/INode.h"
#include "Node.h"
#pragma warning( disable : 4251)

namespace cls
{
//! @ingroup plugin_group
//! \brief �������� ��������� ��� ��� ��������
//! 
//! ����������� ��������� ������� � ����������� ������������
//! �������� �� ����� ������ ��������
//! ������������ ����������� ������� ��� pass � ������
//! 
struct NodePool
{
public:
	NodePool();
	~NodePool();

	// ��������
	void clear();

	// �������� ���� � ���� ��� ����� � ���
	cls::INode* addnodetree(
		MDagPath root, 
		cls::IExportContext* context,
		cls::INode* masterpassnode
		);
	// �������� ���� � ���
	cls::INode* addnode(
		MObject node, 
		cls::IExportContext* context, 
		cls::INode* masterpassnode
		);
	// �������� ������
	cls::INode* addpass(
		MObject obj, 
		cls::IExportContext* context
		);
	cls::INode* addpass(
		cls::IPassGenerator* passgen, 
		cls::IExportContext* context
		);
	// ������� �����
	int connectPass(
		MObject mainpass, 
		MObject subpass,
		cls::IExportContext* context
		);

	// �������� ��
	int addlight(
		MObject obj, 
		cls::IExportContext* context
		);
	/*/
	// ���� ��� �������� (�������� � ������ ���� ����������� � ����??? ���)
	int addframe(
		double frame, 
		cls::IExportContext* context
		);
	// ������� OnPrepareNode � �����������
	int prepareNodes(
		cls::IExportContext* context
		);
	/*/
	

	// dump
	void dump_nodes(FILE* file)const;
	void dump_passes(FILE* file)const;
	void dump_lights(FILE* file)const;

	// ������ � ���� ��������:
	// ���� ���� - ������� Node � nodes
	cls::INode* getNode(
		MObject obj, 
		cls::IExportContext* context
		);

	// ������ ������������ � ������� ���������� ������ ��������
	void getPassSortList(
		std::vector<cls::INode*>& passes
		);
	// ������ ������ ����������� ��� ����������
	void getSubPassSortList(
		cls::INode& node,
		std::vector<cls::INode*>& subpasses
		);

public:
	// ������ �����������
	struct RootTransform
	{
		std::tree<cls::INode*> nodetree;

		// dump
		void dump(FILE* file)const;
	};
	// ������� �������� (������� �������)
	// dagPath -> RootTransform
	std::map<std::string, RootTransform> rootlist;
	
	// ������ ��������
	std::tree<cls::INode*> passtree;
	// ����� �������� (mainpass->subpass)
	std::map< cls::INode*, std::list<cls::INode*> > passconnects;

	// ������ ��
	std::list<cls::INode*> lights;

	// ���� ��������
	typedef std::map<void*, Node> nodes_t;
	nodes_t nodes;
	// ������������� � �������� ����
	std::list<Node> freenodes;

private:
	cls::INode* recAdd(
		MDagPath& path, 
		int level, 
		std::tree<cls::INode*>& nodetree, 
		std::tree<cls::INode*>::iterator insertit, 
		cls::IExportContext* context,
		cls::INode* masterpassnode
		);

	void rec_getPassSortList(
		std::vector<cls::INode*>& passes, 
		std::tree<cls::INode*>::const_iterator begin, 
		std::tree<cls::INode*>::const_iterator end
		);

	// ���� �� �������
	cls::INode* nodeFromObject(
		MObject obj,
		cls::IExportContext* context
		);
	cls::INode* nodeFromObject(
		MDagPath path,
		cls::IExportContext* context
		);
	// ������� ����
	cls::INode* addnodeFromObject(
		MObject obj,
		cls::IExportContext* context,
		cls::INode* masterpassnode
		);
	cls::INode* addnodeFromObject(
		MDagPath path,
		cls::IExportContext* context,
		cls::INode* masterpassnode
		);

};

}