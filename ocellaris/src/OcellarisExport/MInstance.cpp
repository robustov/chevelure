#include "stdafx.h"
#include "ocellaris/MInstance.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include "Util/Stream.h"
#include "Math/Box.h"
//#include "ocellaris\renderOpenglCacheImpl.h"
#include "ocellaris\OcellarisExport.h"

#include "ocellaris\renderFileImpl.h"
#include "ocellaris\renderOpenglImpl.h"
#include "ocellaris\renderCacheImpl.h"
#include "ocellaris\OcellarisExport.h"
#include "mathNgl\mathNgl.h"

/*/
bool cls::MInstance::LoadMayaFile(
	const char* mayafile, 
	const char* mayaobject, 
	enMInstanceMode drawMode, 
	bool bDrawBox, 
	bool bUseOcCache
	)
{
	return false;
}
/*/
bool cls::MInstance::LoadOcs(
	const char* ocsfile, 
	const char* proxyocsfile, 
	enMInstanceMode drawMode, 
	bool bDrawBox, 
	bool readglobals, 
	bool bViewSubFiles, 
	bool brenderProxy
	)
{
	clear();

	this->bDrawBox = bDrawBox;
	bool bInteractive = MGlobal::mayaState()==MGlobal::kInteractive;
	if( !bInteractive)
		drawMode = ODM_only_box;
	if(drawMode == ODM_only_box)
		this->bDrawBox = true;
	
	std::string fullocsfile = getFullFilename(ocsfile);
//	if( fullocsfile.empty()) return false;
	std::string fullproxyocsfile = getFullFilename(proxyocsfile);

	cls::IRender* exterrender = NULL;
	if(readglobals)
		exterrender = &this->oc_cache;

	Math::Box3f box;
	cls::renderFileImpl infileimpl;
	if( !fullproxyocsfile.empty())
	{
		// PROXY
		// box
		{
			cls::renderFileImpl fullinfileimpl;
			fullinfileimpl.openForRead(fullocsfile.c_str());
			if( !fullinfileimpl.GetAttribute("file::box", box))
			{
				printf("no attribute file::box in file %s\n", fullocsfile.c_str());
				return false;
			}
		}

		infileimpl.openForRead(fullproxyocsfile.c_str(), exterrender);
	}
	else
	{
		// FULL
		infileimpl.openForRead(fullocsfile.c_str(), exterrender);
		if( !infileimpl.GetAttribute("file::box", box))
		{
			printf("no attribute file::box in file %s\n", fullocsfile.c_str());
			return false;
		}
	}

	// Box!
	this->box = box;

	// Attributes!
	std::vector< std::pair< std::string, cls::Param> > attrlist;
	infileimpl.GetAttributeList(attrlist);

	char* prefix = "global::";
	size_t prefixlen = strlen(prefix);
	for(unsigned i=0; i<attrlist.size(); i++)
	{
		std::string name = attrlist[i].first;
		if( strncmp(name.c_str(), prefix, prefixlen)!=0) continue;
		attributes[name.c_str()+prefixlen] = attrlist[i].second;
	}

	if(drawMode != ODM_only_box)
	{
		if( bViewSubFiles)
			this->oc_cache.Attribute("viewport::bRenderFiles", true);

		//drawMode?
		infileimpl.Render( &this->oc_cache);
	}
	bLoaded = true;

	// filename
	this->filename = fullocsfile;
	if(brenderProxy && !fullproxyocsfile.empty())
	{
		this->filename = fullproxyocsfile;
	}
	return true;
}
std::string cls::MInstance::getFullFilename(const char* ocsfile)
{
	if( !ocsfile || !ocsfile[0]) return "";
	std::string fn = ocsfile;
	size_t posslash = fn.find_first_of("\\/");
	if( posslash == std::string::npos)
	{
		char buf[512];
		GetCurrentDirectory(512, buf);
		fn = buf;
		fn += "\\";
		fn += ocsfile;
	}

	cls::renderFileImpl infileimpl;
	if( !infileimpl.openForRead(fn.c_str()))
	{
		MString workspacedir;
		MGlobal::executeCommand("workspace -fn", workspacedir);

		fn = workspacedir.asChar();
		fn += "\\";
		fn += ocsfile;

		if( !infileimpl.openForRead(fn.c_str()))
			return fn;
	}
	return fn;
}

void cls::MInstance::UpdateAttrList(MObject thisNode, bool bString)
{
	std::map<std::string, cls::Param>::iterator itx = attributes.begin();
	for(;itx != attributes.end(); itx++)
	{
		std::string n = "cls_" + itx->first;
		if( !bString)
		{
			ocExport_AddAttrType(thisNode, n.c_str(), itx->second->type, itx->second);
		}
		else
		{
			// ������� ������ ��������� �������
			ocExport_AddAttrType(thisNode, n.c_str(), cls::PT_STRING, cls::PS(""));
		}
	}
}
void cls::MInstance::SetAttrToRender(
	MObject thisNode, 
	cls::IRender* render) const
{
	std::map<std::string, cls::Param>::const_iterator itx = attributes.begin();
	for(;itx != attributes.end(); itx++)
	{
		std::string n = "cls_" + itx->first;
		std::string na = "global::" + itx->first;
		MObject attr = ocExport_AddAttrType(thisNode, n.c_str(), itx->second->type, itx->second);

		cls::Param val = ocExport_GetAttrValue(thisNode, attr);
		render->Attribute( na.c_str(), val);
	}
}

bool cls::MInstance::RenderToMaya(
	MObject thisNode,
	M3dView* view,
	bool bPoints, bool bWireframe, bool bShaded
	)const
{
	if( bDrawBox)
	{
		drawBox(box);
	}

	{
		cls::renderOpenglImpl prman(view);
		if( !thisNode.isNull())
			SetAttrToRender(thisNode, &prman);

		prman.bPoints = bPoints;
		prman.bWireFrame = bWireframe;
		prman.bShaded = bShaded;
		prman.degeneration = 1;
		oc_cache.Render(&prman);
	}
	return true;
}

bool cls::MInstance::RenderToMaya(
	MObject thisNode,
	cls::IRender* render
	)const
{
	if( bDrawBox)
	{
		drawBox(box);
	}
	{
		oc_cache.Render(render);
	}
	return true;
}


