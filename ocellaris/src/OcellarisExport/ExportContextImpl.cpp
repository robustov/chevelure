#include "stdafx.h"
#include "ExportContextImpl.h"
#include "Util/misc_create_directory.h"
#include "MathNMaya/MayaProgress.h"
#include "ocellaris\renderStringImpl.h"

void Maya_system( const char* cmd)
{
//	system(cmd);

	STARTUPINFO si = { sizeof(si) };
	PROCESS_INFORMATION pi;
	si.wShowWindow = SW_SHOWNORMAL;

//	std::string cmd = "\"";
//	cmd += _cmd;
//	cmd += "\"";
	BOOL res = CreateProcess( NULL, (LPSTR)cmd, NULL, NULL, TRUE, 0, NULL, NULL, &si, &pi);
	if(!res)
	{
		displayString("Failed:");
	}
	else
	{
		displayString("Start:");
	}
	displayString(cmd);
}

ExportContextImpl::ExportContextImpl()
{
	currentExportFrame = 1e12f;
	bValidGenerators = false;
	bExportTime = false;
}
ExportContextImpl::~ExportContextImpl()
{
}

// message
void ExportContextImpl::printf(const char* text, ...)
{
	va_list argList; va_start(argList, text);
	char sbuf[256];
	_vsnprintf(sbuf, 255, text, argList);
	va_end(argList);
	fputs(sbuf, stdout);
}
// ������
void ExportContextImpl::error(const char* text, ...)
{
	va_list argList; va_start(argList, text);
	char sbuf[256];
	_vsnprintf(sbuf, 255, text, argList);
	va_end(argList);
	MGlobal::displayError(sbuf);
}

void ExportContextImpl::clear()
{
	envs.clear();
	chiefgenerator.clear();
	nodepool.clear();
	framepool.clear();
	taskpool.clear();
	exportdata.clear();

	fastdiststart_preflight = "";
	fastdiststart_postrender = "";
	fastdiststart_alfredjob.clear();
	fastdiststart_framefiles.clear();

	bValidGenerators = false;
	bExportTime = false;
}
bool ExportContextImpl::setenv(const char* env, const char* val)
{
	if( !env || !env[0])
		return false;

	std::string _env = env;
	_strupr((char*)_env.c_str());
	if( val)
		envs[_env] = val;
	else
		envs[_env] = "";
	return true;
}
const char* ExportContextImpl::getenv(const char* env) const
{
	if( !env || !env[0])
		return "";
	std::string _env = env;
	_strupr((char*)_env.c_str());

	std::map<std::string, std::string>::const_iterator it = envs.find(_env);
	if(it==envs.end())
		return "";
	return it->second.c_str();
}




///////////////////////////////////////////////////////////
//
// ����������
// 
bool ExportContextImpl::addDefaultGenerators(const char* options)
{
	if( this->bValidGenerators)
	{
		this->error("ExportContextImpl::addDefaultGenerators SO LATE");
		return false;
	}
	ocellarisDefaultGenerators(chiefgenerator, options);
	return true;
}

bool ExportContextImpl::addGeneratorForType(const char* type, const char* generator)
{
	if( this->bValidGenerators)
	{
		this->error("ExportContextImpl::addGeneratorForType SO LATE");
		return false;
	}

	// ���������������� ����
	static misc::switch_string::Token mayaFntypeslist[] = {
		{"roottransform",		MFn::kInvalid	},			// ������ ������ !!!
		{"shape",				MFn::kShape		}, 
		{"transform",			MFn::kTransform	}, 
		{"mesh",				MFn::kMesh		}, 
		{"spotLight",			MFn::kSpotLight	}, 
		{"renderLayer",			MFn::kRenderLayer}, 
		{"light",				MFn::kLight		}, 
		{NULL, 					MFn::kInvalid}};
	static misc::switch_string mayaFntypes(mayaFntypeslist);
	int code = mayaFntypes.match(type);
	if(code!=MFn::kInvalid)
		return chiefgenerator.addGenerator( ChiefGenerator::type_t( MFn::Type(code) ), generator);

	// ����������� ���
	char buf[256];
	_snprintf(buf, sizeof(buf), "objectType -tagFromType %s", type);

	int res;
	MGlobal::executeCommand(buf, res);

	MTypeId tid(res);
	return chiefgenerator.addGenerator( ChiefGenerator::type_t(tid), generator);
}

// ��������� �-��� ������ ��������� �� ����� � ����
cls::IGenerator* ExportContextImpl::loadGenerator(const char* generator, cls::IGenerator::enGenType type)
{
	cls::IGenerator* gen = chiefgenerator.loadGenerator(generator, type, this);
	if( this->bValidGenerators)
	{
		gen->OnPrepare(this);
	}
	return gen;
}

// ����������� ����������
bool ExportContextImpl::prepareGenerators()
{
	chiefgenerator.prepareGenerators(this);
	this->bValidGenerators = true;
	return true;
}

// ������ ��������� ����������� 
// ���������� �� INode::PrepareGenerators
ChiefGenerator* ExportContextImpl::getGenerators(
	)
{
	if( !this->bValidGenerators)
	{
		this->error("ExportContextImpl::getGenerators NOT READY");
		return NULL;
	}

	return &chiefgenerator;
}







// ������������ RenderFrame ��� �������
// �������� pass � ������ RenderFrame ��� ��������� �����
void ExportContextImpl::addRenderFramePass(
	cls::INode& node,				//!< ������ pass
	float frametime					//!< � ������ ����� ���������
	)
{
	int res = framepool.addpass(
		node,				//!< ������ 
		MTime(frametime, MTime::uiUnit())
		);
	if( !res)
	{
		this->error("framepool.addpass FAILED frame = %f", frametime);
	}
}

// ���������� �� ���� �����������:
void ExportContextImpl::addExportTimeNode(
	cls::INode& node,				//!< ������ 
	float frametime,				//!< � ������ ����� ���������
	float exportTime				//!< ������ ����� ������ ���� ���������� �������
	)
{
	int res = framepool.add(
		node,				//!< ������ 
		MTime(frametime, MTime::uiUnit()), 
		MTime(exportTime, MTime::uiUnit())
		);
	if( !res)
	{
		this->error("framepool.addpass FAILED frame = %f exporttiem = %f", frametime, exportTime);
	}
}
// ������ ��� � ���� ���������� ��� �������� ���� 
bool ExportContextImpl::getNodeExportFrameCache(
	float frametime, 
	cls::INode& node,				// ������
	cls::IRender* &pcache,
	Math::Box3f* &pbox
	)
{
	MTime frame(frametime, MTime::uiUnit());
	cls::FramePool::NodeExportFrameData* data = framepool.getNodeCache(frame, node, this);
	if(!data)
	{
		this->error("ExportContextImpl::getNodeExportFrameCache for %s %f not found\n", node.getName(), frametime);
		return false;
	}
	pcache = &data->cache;
	pbox = &data->box;
	return true;
}


// ������ ������� ����� ������� �����������
// ������� ������ � ������ do export - ����� ������� OnExport � pass
float ExportContextImpl::getCurrentFrame(
	)
{
	return this->currentExportFrame;
}

// ������ ����� ���������� �����
void ExportContextImpl::getGlobalLights(
	std::vector<cls::INode* >& lights
	)
{
	lights.clear();
	lights.reserve(nodepool.lights.size());
	
	std::list<cls::INode*>::iterator it = nodepool.lights.begin();
	for(;it != nodepool.lights.end(); it++)
	{
		cls::INode* node = *it;
		lights.push_back(node);
	}

}

// �������� ������ � �������
cls::INode* ExportContextImpl::addNode(
	MObject& obj,				//!< ������
	cls::INode* masterpassnode	//!< ���� ������ ����������� ����������� �������� � �� ������ �������� � �������� �������
	)
{
	cls::INode* node = nodepool.addnode(obj, this, masterpassnode);
	return node;
}

// ������� ���� �������� ��� �������
// ���������� �� �����������
cls::INode* ExportContextImpl::getNodeForObject(
	MObject& obj
	)
{
	cls::INode* node = nodepool.getNode(obj, this);
	if( !node)
	{
		this->error("ExportContextImpl::getNodeForObject node not in pool");
		return NULL;
	}

//	cls::INode* node = nodepool.addnode(obj, this);
	return node;
}
// ������� ��������� ��� �������� ���� ��������
// ���������� ��� ���������� �������
cls::IShadingNodeGenerator* ExportContextImpl::getShadingNodeGenerator(
	MObject& obj
	)
{
	cls::INode* node = nodepool.addnode(obj, this, 0);
	MObject genobj;
	cls::IGenerator* gen = node->GetGenerator(cls::IGenerator::SHADINGNODE, genobj);
	return (cls::IShadingNodeGenerator*)gen;
}
// ��������� ������, �������� � �������� ��������
// �� ������ � IGenerator::PrepareNode
// �� ������ � �����������
cls::IRender* ExportContextImpl::objectContext(
	MObject& obj,				// ����� ���� INode* ???
	cls::IGenerator* generator
	)
{
	void* id = NULL;
	if(!obj.isNull())
		id = *(void**)&obj;
	ExportDataKey edk(id, generator);
	cls::renderCacheImpl<>& cache = this->exportdata[edk];
	return &cache;
}


// �������� ������
cls::ITask* ExportContextImpl::addTask(
	cls::ITask* parent			// �.�. NULL - ����� ����������� � ����� ������
	)
{
	cls::ITask* task = taskpool.newTask(parent);
	return task;
}
// ����� ������ �� �����
cls::ITask* ExportContextImpl::findTask(
	const char* taskname, 
	bool bTopLevelOnly
	)
{
	cls::ITask* task = taskpool.findTask(taskname, bTopLevelOnly);
	return task;
}
// ���������� ����� ����� ��������
bool ExportContextImpl::addRelation(
	cls::ITask* task, 
	cls::ITask* dependon 
	)
{
	taskpool.addRelation(task, dependon, this);
	return true;
}

// ������ ����������� ����� ����� �������
cls::ITask* ExportContextImpl::getPreTask(
	)
{
	return NULL;
}






// ������ ���, �������� � �����������
cls::INode* ExportContextImpl::addnodes(const char* root, const char* options)
{
	MDagPath path;
	MObject obj;
	if( !root || !root[0])
	{
		MItDag it;
		it.getPath(path);
	}
	else if( !nodeFromName(root, obj))
	{
		this->error("node %s not have path", root);
		return 0;
	}
	else
	{
		if( !MDagPath::getAPathTo(obj, path))
		{
			this->error("node %s not have path", root);
			return 0;
		}
	}
	return nodepool.addnodetree(path, this, 0);
}
cls::INode* ExportContextImpl::addnodes(MDagPath& path, const char* options)
{
	return nodepool.addnodetree(path, this, 0);
}


// ������ �������
cls::INode* ExportContextImpl::addPass(
	MObject& obj				//!< ������
	)
{
	cls::INode* node = nodepool.addpass(obj, this);
	if( !node) return 0;

	return node;
}
int ExportContextImpl::addpass(const char* passnode, const char* options)
{
	MObject obj = MObject::kNullObj;
	if( passnode && passnode[0])
	{
		if( !nodeFromName(passnode, obj))
		{
			fprintf(stderr, "pass %s not found", passnode);
			return 0;
		}
	}
	cls::INode* node = nodepool.addpass(obj, this);
	if( !node) return 0;

	if( options && options[0])
	{
		MObject genobj;
		cls::IGenerator* gen = node->GetGenerator(cls::IGenerator::PASS, genobj);
		cls::IRender* cxt = this->objectContext(node->getObject(), gen);
		cxt->Parameter("OPTIONS", options);

		cls::renderStringImpl fileimpl;
		std::string customdata = options;
		customdata += "\n";
		if( fileimpl.openForRead(customdata.c_str()))
		{
			fileimpl.Render(cxt);
		}
	}
	return 1;
}
// ������ �������
int ExportContextImpl::addpass(cls::IPassGenerator* gen, const char* options)
{
	cls::INode* node = nodepool.addpass(gen, this);
	if( !node) return 0;

	if( options && options[0])
	{
		cls::IRender* cxt = this->objectContext(node->getObject(), gen);
		cxt->Parameter("OPTIONS", options);

		cls::renderStringImpl fileimpl;
		std::string customdata = options;
		customdata += "\n";
		if( fileimpl.openForRead(customdata.c_str()))
		{
			fileimpl.Render(cxt);
		}
	}

	return 1;
}
// ����� ��������
int ExportContextImpl::connectPass(const char* mainpass, const char* subpass, const char* options)
{
	MObject mainobj = MObject::kNullObj;
	if( mainpass && mainpass[0])
	{
		if( !nodeFromName(mainpass, mainobj))
		{
			fprintf(stderr, "pass %s not found", mainpass);
			return 0;
		}
	}
	MObject subobj = MObject::kNullObj;
	if( subpass && subpass[0])
	{
		if( !nodeFromName(subpass, subobj))
		{
			fprintf(stderr, "pass %s not found", subpass);
			return 0;
		}
	}
	return nodepool.connectPass(mainobj, subobj, this);
}

// ��
int ExportContextImpl::addlight(const char* lightnode, const char* options)
{
	MObject obj = MObject::kNullObj;
	if( !lightnode || !lightnode[0])
	{
		return 0;
	}

	if( !nodeFromName(lightnode, obj))
	{
		fprintf(stderr, "node %s not found", lightnode);
		return 0;
	}

	int res=0;
	res = nodepool.addlight(obj, this);
//	res = nodepool.addpass(obj, this);
	return res;
}

// �������� ���� � ��������� ������� �����
int ExportContextImpl::addframe(double frametime)
{
	// �������� render frame
	MTime frame(frametime, MTime::uiUnit());
	framepool.addrenderframe(frame);
	// ������� � ���� ���� ����������� OnAddFrame();
//	nodepool.addframe(frametime, this);
	return 0;
}
// ����������� � ��������
// ��� ������� RenderFrame, ������� Pass � ������� �������
//	����� ExportFrame
int ExportContextImpl::preparenodes()
{
	std::map<MTime, cls::FramePool::RenderFrame>::iterator itrf = framepool.renderframes.begin();
	for(;itrf != framepool.renderframes.end(); itrf++)
	{
		// Render Frame
		cls::FramePool::RenderFrame& renderframe = itrf->second;
		MTime t = renderframe.frame;
		float frame = (float)t.as( MTime::uiUnit());

		std::tree<cls::INode*>::const_branch_iterator cbit = nodepool.passtree.begin();
		for( ; cbit != nodepool.passtree.end(); cbit++)
		{
			// PASS
			cls::INode* passnode = *cbit;
			if( !passnode) 
				continue;
			MObject genobject;
			cls::IPassGenerator* passgen = (cls::IPassGenerator*)passnode->GetGenerator(cls::IGenerator::PASS, genobject);

			// ��������� ��������� �� ������ � ���� �����
			bool bUsed = passgen->IsUsedInFrame(
				*passnode, (float)frame, this);
			if( !bUsed)
				continue;

			// �������� pass � ������ RenderFrame
			this->addRenderFramePass(*passnode, (float)frame);

//			cls::IBlurGenerator* defaultbg = passgen->GetDefaultBlurGenerator(*node, context);

			// ��������� ��� �������
			cls::NodePool::nodes_t::iterator it = nodepool.nodes.begin();
			for( ; it != nodepool.nodes.end(); it++)
			{
				Node& node = it->second;

				bool bMaster = node.isMasterPass(passnode);
				if( !bMaster)
					continue;

				passgen->AddExportFrames(
					*passnode, 
					node, 
					frame, 
					this
					);
			}
			/*/
			std::map<std::string, cls::NodePool::RootTransform>::const_iterator it = nodepool.rootlist.begin();
			for( ; it != nodepool.rootlist.end(); it++)
			{
				const cls::NodePool::RootTransform& rt = it->second;

				std::tree<cls::INode*>::const_branch_iterator cbit = rt.nodetree.begin();
				for( ; cbit != rt.nodetree.end(); cbit++)
				{
					cls::INode* node = *cbit;

					passgen->AddExportFrames(
						*passnode, 
						*node, 
						frame, 
						this
						);
//					node->AddFrame(*node, (float)frame, passgen, defaultbg, context);
				}
			}
			/*/
		}
	}
	return 1;
}

// ���������� �������
// -dump
// -dumptree
int ExportContextImpl::doexport(const char* dumpoption, const char* options, MayaProgress* progress)
{
	bool bFastDistStart = false;
	char* substr = strtok((char*)options, " ");
	for(; substr; substr = strtok(NULL, " "))
	{
		if( strcmp(substr, "-fastdiststart")==0)
			bFastDistStart = true;
	}

	if( bFastDistStart)
	{
		// preflight
		std::string alfredjob_filename = this->fastdiststart_preflight;
		taskpool.buildAlfredJob("preflight", alfredjob_filename.c_str());
		taskpool.clear();
	}

	MTime curtime = MAnimControl::currentTime();

	FILE* dumpfile = stdout;
	bool bDump = false, bDumpTree = false;
	if( strcmp( dumpoption, "-dump")==0)
		bDump = true;
	if( strcmp( dumpoption, "-dumptree")==0)
		bDumpTree = bDump = true;

	if( bDump)
		fprintf(dumpfile, "ExportContextImpl::export\n");

	// 
	// ��������� ��� FramePool::ExportFrame �� �������
	std::map<MTime, cls::FramePool::ExportFrame>::iterator it = framepool.exportframes.begin();
	for(int frcount=0;it != framepool.exportframes.end(); it++, frcount++)
	{
		cls::FramePool::ExportFrame& exportframe = it->second;
		MTime t = it->first;

		float perc = frcount/(float)framepool.exportframes.size();
		if(progress)
		{
			char text[256];
			_snprintf(text, sizeof(text), "Read frame: %04f", t.as(MTime::uiUnit()));

			if( !progress->OnProgress( (float)perc, text))
				return 0;
		}

		if( bDump)
			fprintf( dumpfile, "ExportFrame %f\n", t.as(MTime::uiUnit()));

		if( MAnimControl::currentTime()!=t)
		{
			if( bDump)
				fprintf( dumpfile, "  setCurrentTime %f\n", t.as(MTime::uiUnit()));
			MAnimControl::setCurrentTime(t);
		}

		//////////////////////////////////////////////
		// �������������� ��� ����
		//
		std::map<cls::INode*, cls::FramePool::NodeExportFrameData>::iterator itn = exportframe.nodes.begin();
		for(;itn != exportframe.nodes.end(); itn++)
//		cls::NodePool::nodes_t::iterator itn = nodepool.nodes.begin();
//		for(;itn != nodepool.nodes.end(); itn++)
		{
			cls::INode* node = (cls::INode*)itn->first;

			cls::FramePool::NodeExportFrameData& nodedata = itn->second;//exportframe.nodes[node];

			nodedata.box = Math::Box3f();

			// � ������� ����� ������� OnExportNode()
			bool bExportNode = false;
			std::tree<cls::INode*>::const_branch_iterator cbit = nodepool.passtree.begin();
			for( ; cbit != nodepool.passtree.end(); cbit++)
			{
				cls::INode* passnode = *cbit;
				MObject genobject;
				cls::IPassGenerator* passgen = (cls::IPassGenerator*)passnode->GetGenerator(cls::IGenerator::PASS, genobject);
				bExportNode |= passgen->OnExportNode( *node, &nodedata.cache, this, *passnode, genobject);
			}

			int whatRender = cls::INode::WR_ALL;
			node->Export(
				&nodedata.cache,				// ����?
				nodedata.box,					// ����
				this,							// ��������
				whatRender						// ����� �� enWhatRender
				);
		}
		exportframe.bReady = true;

		/////////////////////////////////////////
		// ����� ������� ������
		// 
		std::set<cls::FramePool::RenderFrame*>::iterator itrf = exportframe.renderframes.begin();
		for(;itrf != exportframe.renderframes.end(); itrf++)
		{
			cls::FramePool::RenderFrame* renderframe = *itrf;
			MTime t = renderframe->frame;
			if( !renderframe->isReady()) continue;
			if( renderframe->isCompeted()) continue;

			if(progress)
			{
				char text[256];
				_snprintf(text, sizeof(text), "Save frame: %04f", t.as(MTime::uiUnit()));

				float perc = frcount/(float)framepool.exportframes.size();
				if( !progress->OnProgress( (float)perc, text))
					return 0;
			}

			//////////////////////////////////////////////
			//
			// ������� ������ t
			// 
			if( bDump)
				fprintf( dumpfile, "  RenderFrame %f is ready!!!\n", t.as(MTime::uiUnit()));

			char buf[24];
			sprintf(buf, "%04d", (int)t.as(MTime::uiUnit()));
			this->setenv("FRAMENUMBER", buf);

			// ��� ������ ���������  getCurrentFrame
			this->currentExportFrame = (float)t.as(MTime::uiUnit());

			// ��� ������� ������� - ������ � ���
			std::vector<cls::INode*> passes;
			nodepool.getPassSortList(passes);
			std::map< cls::INode*, std::list<cls::ITask*> > passtasks;	// ������ ��� ������� �������
			for(int p=0; p<(int)passes.size(); p++)
			{
				//////////////////////////////////////
				// 
				// ������� �������!!!
				// 
				cls::INode& passnode = *passes[p];
				MObject genobject;
				cls::IPassGenerator* passGenerator = (cls::IPassGenerator*)passnode.GetGenerator(cls::IGenerator::PASS, genobject);

				if( bDump)
					fprintf( dumpfile, "  Export PASS %s\n", passnode.getName());

				// � ���� ����������� ������� OnExportParentPass ������ � subpasscache
				cls::renderCacheImpl<> subpasscache;
				std::vector<cls::INode*> subpasses;
				std::list<cls::ITask*> subpassestasks;
				nodepool.getSubPassSortList( passnode, subpasses);
				for(int sp=0; sp<(int)subpasses.size(); sp++)
				{
					cls::INode& subpassnode = *subpasses[sp];
					if( bDump)
						fprintf( dumpfile, "    SubPass %s\n", subpassnode.getName());

					MObject subgenobject;
					cls::IPassGenerator* subpassGenerator = (cls::IPassGenerator*)subpassnode.GetGenerator(cls::IGenerator::PASS, subgenobject);

					subpassGenerator->OnExportParentPass(
						subpassnode, 
						&subpasscache, 
						this
						);
					// ������� ������ ����� �������
					std::list<cls::ITask*>& list = passtasks[&subpassnode];
					subpassestasks.insert(subpassestasks.end(), list.begin(), list.end());
					if( bDump)
					{
						std::list<cls::ITask*>::iterator itt = list.begin();
						for( ; itt != list.end(); itt++)
						{
							cls::ITask* task = *itt;
							cls::PS name1 = task->GetParameter("name");
							if( !name1.empty())
								this->printf("    -- %s\n", name1.data());
						}
					}
				}

				if( bDumpTree)
					fprintf( dumpfile, "  iterate Node Tree\n");

				// ����� ������ �������� - ������ � framecache + ��������� box
				cls::renderCacheImpl<> framecache;
				cls::IRender* render = &framecache;
				Math::Box3f scenebox;
				std::map<std::string, cls::NodePool::RootTransform>::iterator it = nodepool.rootlist.begin();
				for( ; it != nodepool.rootlist.end(); it++)
				{
					cls::NodePool::RootTransform& rt = it->second;

					Math::Matrix4f transform = Math::Matrix4f::id;
					rec_AssembleNodeTree(
						passnode,
						&framecache, 
						scenebox,
						(float)renderframe->frame.as(MTime::uiUnit()), 
						transform,    
						rt.nodetree.begin(), 
						rt.nodetree.end(), 
						bDumpTree?dumpfile:NULL
						);
				}

				if( bDump)
					fprintf( dumpfile, "  Export\n");

				// ������� ������ � ��������
				// ������ � ���� 
				// �������� (������/������)
				bool res = passGenerator->OnExport(
					passnode, &framecache, scenebox, &subpasscache, this);			//!< render
				if( !res) 
					continue;

				// ������ �������� �������
				std::list<cls::ITask*>& newtasklist = passtasks[&passnode];
				this->taskpool.getNewTasks(newtasklist);
				// ������� �����������
				std::list<cls::ITask*>::iterator itt = newtasklist.begin();
				for( ; itt != newtasklist.end(); itt++)
				{
					cls::ITask* main = *itt;
					std::list<cls::ITask*>::iterator itts = subpassestasks.begin();
					for( ; itts != subpassestasks.end(); itts++)
					{
						cls::ITask* sub = *itts;
						this->taskpool.addRelation(main, sub, this);
					}
				}
				// �������� ��������� � �������� ������
				newtasklist.insert(newtasklist.end(), subpassestasks.begin(), subpassestasks.end());
			}

			renderframe->bCompleted = true;
			this->currentExportFrame = 1e12f;

			// ����� �������� ������ t
			// 

			if( bFastDistStart)
			{
				std::string alfredjob_filename = this->fastdiststart_framefiles[t.as(MTime::uiUnit())];
				if( alfredjob_filename.empty())
				{
					this->error("Fast Dist Job %f not ready", t.as(MTime::uiUnit()));
				}
				else
				{
					char text[128];
					_snprintf(text, sizeof(text), "frame %04f", t.as(MTime::uiUnit()));
					taskpool.buildAlfredJob(text, alfredjob_filename.c_str());
					taskpool.clear();
				}
			}
		}
	}

	///////////////////////////////////////////////
	// � ���� �������� ������� OnPostExport
	std::vector<cls::INode*> passes;
	nodepool.getPassSortList(passes);
	for(int p=0; p<(int)passes.size(); p++)
	{
		cls::INode& passnode = *passes[p];
		MObject genobject;
		cls::IPassGenerator* passGenerator = (cls::IPassGenerator*)passnode.GetGenerator(cls::IGenerator::PASS, genobject);
		if( !passGenerator)
			continue;

		//! ���������� ����� �������� ���� ������
		passGenerator->OnPostExport( passnode, this);
	}
	if( bFastDistStart)
	{
		// preflight
		std::string alfredjob_filename = this->fastdiststart_postrender;
		taskpool.buildAlfredJob("postrender", alfredjob_filename.c_str());
		taskpool.clear();
	}

	// ������� ����� �� �����
	if( curtime!=MAnimControl::currentTime())
	{
		MAnimControl::setCurrentTime(curtime);
		if( bDump)
			fprintf( dumpfile, "setCurrentTime %f\n", curtime.as(MTime::uiUnit()));
	}
	// ALFRED ��������� � render

	fflush(dumpfile);

	return 1;
}

// �������� �� ������
int ExportContextImpl::render(const char* renderoption)
{
	std::string jobname = this->getEnvironment("JOBNAME");
	Util::correctFileName( (char*)jobname.c_str());

	bool bAlfred = false;
	bool bStart  = false;

	char* substr = strtok((char*)renderoption, " ");
	for(; substr; substr = strtok(NULL, " "))
	{
		if( strcmp(substr, "-alfred")==0)
			bAlfred = true;
		if( strcmp(substr, "-start")==0)
			bStart = true;
	}

	bool bPause = false;
	std::string pausestr = this->getEnvironment("PAUSERENDER");
	if( pausestr=="1")
		bPause = true;

	std::string exportdir = this->getEnvironment("RIBDIR");

	std::string path_env = this->getEnvironment("PATH");

	if(!bAlfred)
	{
		// ���������� ������ � ������
		std::string cmdjob_filename = exportdir;
		cmdjob_filename += jobname + ".bat";
		taskpool.buildTaskCmd(cmdjob_filename.c_str(), path_env.c_str(), bPause);
		cmdjob_filename = "\"" + cmdjob_filename + "\"";
		if( bStart)
			Maya_system( cmdjob_filename.c_str());
	}
	else
	{
		// ALFRED
		std::string alfredjob_filename = this->getEnvironment("ALFFILE");
		if(alfredjob_filename.empty())
		{
			alfredjob_filename = exportdir+"/_alfred_";
			struct tm   *newTime;
			time_t      szClock;
			time( &szClock );
			newTime = localtime( &szClock );
			char buf[100];
			alfredjob_filename += _itoa( (int)szClock, buf, 10);
			alfredjob_filename += ".alf";
			Util::correctFileName((char*)alfredjob_filename.c_str(), true);
		}

		taskpool.buildAlfredJob(jobname.c_str(), alfredjob_filename.c_str());
		if( bStart)
			Maya_system( ("alfred \"" + alfredjob_filename+"\"").c_str());
	}

//	fflush(dumpfile);
	return 1;
}


// ����������� ����� ������ ����������� � ��������� ������
void ExportContextImpl::rec_AssembleNodeTree(
	cls::INode& pass, 
	cls::IRender* render, 
	Math::Box3f& scenebox,
	float frame, 
	const Math::Matrix4f& transform,    
	std::tree<cls::INode*>::iterator begin, 
	std::tree<cls::INode*>::iterator end, 
	FILE* dumpfile
	)
{
	int whatRender = cls::INode::WR_ALL;

	std::tree<cls::INode*>::iterator cbit = begin;
	for( ; cbit != end; cbit++)
	{
		cls::INode* node = *cbit;

		// ����� ����� � ������
		Math::Matrix4f m = transform;
		Math::Box3f box;
		node->PushNode(render, m, box, this, whatRender);

		if( dumpfile)
		{
			int lev = cbit.level();
			std::string indent(lev+1, ' ');
			fprintf(dumpfile, "%snode: %s\n", indent.c_str(), node->getName());
			fprintf(dumpfile, "%s{%f %f %f}\n", indent.c_str(), m[3].x, m[3].y, m[3].z);
		}

		MObject genobject;
		cls::IPassGenerator* passgen = (cls::IPassGenerator*)pass.GetGenerator(cls::IGenerator::PASS, genobject);
//		cls::IBlurGenerator* defaultbg = passgen->GetDefaultBlurGenerator(pass, this);

		// ������� ����
		passgen->BuildNodeFromExportFrame(
			pass, 
			*node, 
			render,			// ������
			frame,
			m,				// ����������� ����� transform
			box,			// ����������� ���� ��� �������
			this			// �������� ��������
			);
//		box.transform(m);
		if( box.isvalid())
			scenebox.insert(box);
		if( dumpfile)
		{
			int lev = cbit.level();
			std::string indent(lev+1, ' ');
			fprintf(dumpfile, "%smin{%f %f %f}\n", indent.c_str(), box.min.x, box.min.y, box.min.z);
			fprintf(dumpfile, "%smax{%f %f %f}\n", indent.c_str(), box.max.x, box.max.y, box.max.z);
		}

		rec_AssembleNodeTree(
			pass,
			render, 
			scenebox, 
			frame, 
			m,    
			cbit.begin(), 
			cbit.end(), 
			dumpfile
			);
		node->PopNode(render, this, whatRender);

	}
}


// dump
void ExportContextImpl::dump(const char* options) const
{
	displayString("for dumd %s see output window", options);
	if( strcmp( options, "-generators")==0)
	{
		chiefgenerator.dump(stdout);
		return;
	}
	if( strcmp( options, "-nodes")==0)
	{
		nodepool.dump_nodes(stdout);
		return;
	}
	if( strcmp( options, "-passes")==0)
	{
		nodepool.dump_passes(stdout);
		return;
	}
	if( strcmp( options, "-lights")==0)
	{
		nodepool.dump_lights(stdout);
		return;
	}
	if( strcmp( options, "-frames")==0)
	{
		framepool.dump(stdout);
		return;
	}
	if( strcmp( options, "-environments")==0)
	{
		FILE* file = stdout;
		fprintf(file, "Environments:\n");
		std::map<std::string, std::string>::const_iterator it = envs.begin();
		for( ; it!=envs.end(); it++)
		{
			fprintf(file, "%s = %s\n", it->first.c_str(), it->second.c_str());
		}
		return;
	}
	
	displayString("dump( \"-flag\") flags:");
	displayString("-generators");
	displayString("-nodes");
	displayString("-passes");
	displayString("-lights");
	displayString("-frames");
}
