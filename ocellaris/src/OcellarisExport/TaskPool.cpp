#include "stdafx.h"
#include "TaskPool.h"
#include "Util/misc_create_directory.h"
#include "ocellaris/IExportContext.h"

using namespace cls;

TaskPool::Task::Task():alfredtask("")
{
}
/// �������� � �������� ����
void TaskPool::Task::Parameter(
	const char *name, Param& t)
{
	cache.Parameter(name, t);
}
// �������� ��������
Param TaskPool::Task::GetParameter(
	const char *name)
{
	return cache.GetParameter(name);
}
Alfred::Task* TaskPool::Task::buildAlfredTask()
{
	std::string name;
	std::string cmd;
	std::string preview;
	std::string service;
	cache.GetParameter("name", name);
	cache.GetParameter("cmd", cmd);
	cache.GetParameter("preview", preview);
	cache.GetParameter("service", service);
	int atmost = 1;
	cache.GetParameter("atmost", atmost);
	
	if( service.empty())
		service = "pixarRender";

	alfredtask = Alfred::Task(name.c_str());
		Alfred::Cmd alfredcmd = Alfred::RemoteCmd(cmd);
		alfredcmd.service = service;
		alfredcmd.atmost  = atmost;
		alfredcmd.tags	= "intensive";

	alfredtask.AddCmd( alfredcmd );
	alfredtask.preview = preview.c_str();
	return &alfredtask;
}
std::string TaskPool::Task::getCmd()
{
	std::string name;
	std::string cmd;
	cache.GetParameter("name", name);
	cache.GetParameter("cmd", cmd);

	std::string res;
	res += "  rem "+name+"\n";
	res += cmd.c_str();
	res += "\n";
	res += "\n";
	return res;
}

TaskPool::TaskPool()
{
}
TaskPool::~TaskPool()
{
}

// ��������
void TaskPool::clear()
{
	// ������ �����
	tasks.clear();
	// ���� ����� (�� ����� ����� ������� Task)
	taskgraph.clear();
	// ������ �������� ������
	tasktree.clear();
	// ����� ������
	newtasks.clear();
}

// �������� ������
cls::ITask* TaskPool::newTask(
	cls::ITask* parent
	)
{
	tasks.push_back(Task());
	Task& task = tasks.back();

	if( !parent)
	{
		tasktree.insert(
			tasktree.end(), 
			&task
			);
	}
	else
	{
		printf("TaskPool::newTask not implemented\n");
	}
	this->newtasks.push_back(&task);
	return &task;
}
// ����� ������ �� �����
cls::ITask* TaskPool::findTask(
	const char* taskname, 
	bool bTopLevelOnly
	)
{
	cls::ITask* task = NULL;
	if( bTopLevelOnly)
	{
		std::tree<Task*>::const_iterator it = tasktree.begin();
		for(;it != tasktree.end(); it++)
		{
			Task* task = *it;
			cls::PS name = task->GetParameter("name");
			if( name!=taskname)
				continue;

			return task;
		}
	}
	else
	{
		std::list<Task>::iterator it = tasks.begin();
		for(;it != tasks.end(); it++)
		{
			Task* task = &*it;
			cls::PS name = task->GetParameter("name");
			if( name!=taskname)
				continue;

			return task;
		}
	}
	return NULL;
}

// ������� ������
void TaskPool::addRelation(
	cls::ITask* task,			// ������
	cls::ITask* dependon,		// �� ����� �������
	cls::IExportContext* context
	)
{
//	printf("TaskPool::addRelation not implemented\n");
	taskgraph[task].push_back(dependon);

	cls::PS name1 = task->GetParameter("name");
	cls::PS name2 = dependon->GetParameter("name");
	if( !name1.empty()&&!name2.empty() )
	{
		context->printf("TaskPool::addRelation %s -> %s\n", name1.data(), name2.data());
	}
}
// ����� ������ � �������� ������ getNewTasks ���������� � ExportContextImpl::doexport
void TaskPool::getNewTasks(
	std::list<cls::ITask*>& newtasks
	)
{
	newtasks = this->newtasks;
	this->newtasks.clear();
}

// ������� ������������� ������	
bool TaskPool::buildAlfredJob(
	const char* jobname, 	
	const char* filename
	)
{
	Alfred::Job alfredjob(jobname);
	alfredjob.atleast = 1;
	alfredjob.atmost = 1;

	std::tree<Task*>::const_iterator it = tasktree.begin();
	for(;it != tasktree.end(); it++)
	{
		Task* task = *it;
		Alfred::Task* alfredtask = buildAlfredJob_rec(task, it.begin(), it.end() );
		alfredjob.AddSubtask(*alfredtask);
	}
	bool res = Alfred::saveAlfredFile(filename, alfredjob);
	return res;
}
// ������� bat ���� 
bool TaskPool::buildTaskCmd(
	const char* filename, 
	const char* path_env,
	bool bPause
	)
{
	Util::create_directory_for_file(filename);
	FILE* file = fopen(filename, "wt");
	if( !file) return false;

	if( path_env && path_env[0])
	{
		fprintf(file, "set PATH=%%PATH%%;%s\n", path_env);
	}

	std::tree<Task*>::const_branch_iterator it = tasktree.begin();
	for(;it != tasktree.end(); it++)
	{
		Task* task = *it;
		std::string cmd = task->getCmd();
		fputs( cmd.c_str(), file);
	}
	if( bPause)
		fputs( "pause\n", file);
	fclose(file);
	return true;
}

// ������� ������������� ������	
Alfred::Task* TaskPool::buildAlfredJob_rec(
	Task* task, 
	std::tree<Task*>::const_iterator begin, 
	std::tree<Task*>::const_iterator end
	)
{
	Alfred::Task* alfredtask = task->buildAlfredTask();
	// Instance
	{
		std::list<cls::ITask*>& depends = this->taskgraph[task];
		std::list<cls::ITask*>::iterator it = depends.begin();
		for(;it != depends.end(); it++)
		{
			cls::ITask* sub = *it;
			cls::PS name = sub->GetParameter("name");
			if( name.empty()) continue;
			alfredtask->AddTask(
				Alfred::Instance(name.data())
				);
		}
	}

	std::tree<Task*>::const_iterator it = begin;
	for(;it != end; it++)
	{
		Task* _task = *it;

		Alfred::Task* chtask = buildAlfredJob_rec(_task, it.begin(), it.end() );
		alfredtask->AddTask(*chtask);
	}

	return alfredtask;
}