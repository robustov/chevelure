#include "stdafx.h"
#include "SlimCommands.h"
#include "ocellaris/OcellarisExport.h"
#include "mathNpixar/ISlo.h"
#include "Util/misc_create_directory.h"

namespace Slim
{
	bool bDontUseMtor = false;

	#ifdef NDEBUG
		getISloDll SloDll("IPrman12.5.dll", "getISlo");
	#endif


//	MObject addAttribute(MObject obj, const char* name, const char* type);
	void setParameterToRender(MObject obj, MObject attr, cls::enParamType type, cls::IRender* render);
	void setAttributeToRender(MObject obj, const char* name, MObject attr, cls::enParamType type, cls::IRender* render, bool bGlobal);


	inline void displayStringD(const char* text, ...)
	{
	#ifdef _DEBUG
		va_list argList; va_start(argList, text);
		char sbuf[256];
		_vsnprintf(sbuf, 255, text, argList);
		va_end(argList);
		MGlobal::displayInfo(sbuf);
	#endif
	}

}


std::string Slim::readShaderNameNparameters(
	cls::IRender* render, 
	MObject obj, 
	const char* attrname, 
	paramlist_t& params, 
	MObject externnode, 
	const char* externnodeattr, 
	const char* externnodeListattr,
	shaderlist_t& shaderlist, 
	const char* workspacedir, 
	bool bUseAll
	)
{
	bool displayEnable = true;
	bool bDisplacement = strcmp(attrname, "slimDispl")==0;
	MStatus stat;
	std::string usemtorattrname = externnodeattr;
	usemtorattrname += "UseMtor";
	std::string externnodeListEditattr = std::string(externnodeListattr)+"Edit";

	MFnDependencyNode dn(externnode);
	MPlug plugListAttrs = dn.findPlug(externnodeListattr, &stat);
	MPlug plugListAttrsEdit = dn.findPlug(externnodeListEditattr.c_str(), &stat);
	if( plugListAttrs.isNull() || plugListAttrsEdit.isNull())
	{
		MFnTypedAttribute typedAttr;
		MObject i_shaderAttrsList = typedAttr.create( externnodeListattr, externnodeListattr, MFnData::kStringArray, &stat);
		dn.addAttribute(i_shaderAttrsList);
		plugListAttrs = dn.findPlug(externnodeListattr, &stat);

		MObject i_shaderAttrsListEdit = typedAttr.create( externnodeListEditattr.c_str(), externnodeListEditattr.c_str(), MFnData::kStringArray, &stat);
		dn.addAttribute(i_shaderAttrsListEdit);
		plugListAttrsEdit = dn.findPlug(externnodeListEditattr.c_str(), &stat);
	}

	{
		MFnStringArrayData array;
		plugListAttrs.setValue(array.create());
	}
	cls::PSA usedAttributes = ocExport_GetAttrArrayValue(obj, externnodeListEditattr.c_str());

	std::string shader = "";
//	MPlug plug = dn.findPlug(usemtorattrname.c_str());
	bool bUseMtor = true;
//	plug.getValue(bUseMtor);

	if( bUseMtor && !bDontUseMtor)
	{
		shader = mtorShaderNames(obj, attrname, params, shaderlist, workspacedir, displayEnable);
		ocExport_AddAttrType(externnode, externnodeattr, cls::PT_STRING, cls::PS(""));
		ocExport_SetAttrValue(externnode, externnodeattr, cls::PS(shader.c_str()));
	}
	else
	{
		MPlug plug = MFnDependencyNode(externnode).findPlug(externnodeattr);
		MString text;
		plug.getValue(text);
		shader = text.asChar();
		if( shader.empty())
			return "";

		size_t exslo = shader.rfind(".slo");
		if( exslo == shader.size()-4)
		{
			shader = shader.substr(0, exslo);
		}

		shaderlist_t::iterator it = shaderlist.find(shader);
		if( it != shaderlist.end())
		{
			params = it->second.paramlist;
		}
		else
		{
			sloReadShaderParams(shader.c_str(), params, workspacedir);
			shaderlist[shader].paramlist = params;
			shaderlist[shader].shadername = shader;
		}
	}

	if( shader.empty())
		return "";

	// ������ ����������
	MStringArray attrArray;

	if( bDisplacement)
	{
		MObject buseattr = ocExport_AddAttrType(externnode, "usedisplacementBound", cls::PT_BOOL, cls::P<bool>(false));
//		cls::P<bool> buse = ocExport_GetAttrValue(externnode, buseattr);
		MObject attr = ocExport_AddAttrType( externnode, "displacementBound", cls::PT_FLOAT, cls::P<float>(1.f));

		std::string attrdecl = "float displacementBound";
		attrArray.append(attrdecl.c_str());

		std::string defaultname = "defvaldisplacementBound";
		MObject defvalattr = ocExport_AddAttrType( externnode, defaultname.c_str(), cls::PT_FLOAT, cls::Param());

		bool buse = false;
		for(int ua=0; ua<usedAttributes.size(); ua++)
			if( strcmp(usedAttributes[ua], attrdecl.c_str())==0) buse = true;
		
		if( bUseMtor && !buse)
		{
			// ������ �� �����
			float displaceBound = 1.f;
			bool res = mtorGetDisplaceBound(obj, attrname, displaceBound);
			if( render)
				render->Parameter("displacement::bound", displaceBound);

			ocExport_SetAttrValue(externnode, defvalattr, cls::P<float>(displaceBound));
		}
		else
		{
			// ������ �� ��������
			cls::Param param = ocExport_GetAttrValue(externnode, attr);
			if( render)
				render->Parameter("displacement::bound", param);

			ocExport_SetAttrValue(externnode, defvalattr, cls::P<float>(0.f));
		}
		MPlug(externnode, defvalattr).setLocked(true);
	}

	const char* _isInSlim = "_isInSlim";
	const size_t _isInSlimLen = strlen(_isInSlim);

	paramlist_t::iterator it = params.begin();
	for( ; it!=params.end(); it++)
	{
		if( it->first.size()>_isInSlimLen && 
			strcmp( _isInSlim, it->first.c_str()+it->first.size()-_isInSlimLen)==0
			)
		{
			// ��� inSlim ����!!!
			// ���� ������� 
			if( render)
			{
				render->Parameter(it->first.c_str(), "inSlim");
			}
			continue;
		}

		cls::Param p = it->second;
		cls::enParamType type = p->type;
		const char* name = it->first.c_str();

		// � ������ ������� ���������
		std::string attrdecl = cls::str_type(type);
		attrdecl += " "; attrdecl += name;
		attrArray.append(attrdecl.c_str());

		// USE?
		bool buse = false;
		for(int ua=0; ua<usedAttributes.size(); ua++)
			if( strcmp(usedAttributes[ua], attrdecl.c_str())==0) buse = true;

//		std::string busename = (std::string("use")+it->first);
//		MObject buseattr = ocExport_AddAttrType(externnode, busename.c_str(), cls::PT_BOOL, cls::P<bool>(false));
//		cls::P<bool> buse = ocExport_GetAttrValue(externnode, buseattr);

		// ������� �������� ��� EXT
		std::string bextname = (std::string("ext")+it->first);
		MObject bextattr = ocExport_AddAttrType(externnode, bextname.c_str(), cls::PT_STRING, cls::PS(""));
		cls::PS bexternal = ocExport_GetAttrValue(externnode, bextattr);

		// default value
		{
			std::string defaultname = (std::string("defval")+it->first);
			MObject defvalattr = ocExport_AddAttrType( externnode, defaultname.c_str(), type, cls::Param());
			ocExport_SetAttrValue(externnode, defvalattr, p);
			MPlug(externnode, defvalattr).setLocked(true);
			if( bUseAll)
			{
				render->Parameter(name, p);

			}
		}

		// ��� �������
		MObject attr = ocExport_AddAttrType( externnode, name, type, p);
		if(bexternal.data() && bexternal.data()[0])
		{
			std::string extattr = "global::";
			extattr += bexternal.data();
			setAttributeToRender(externnode, extattr.c_str(), attr, type, render, true);
			if( render)
			{
				MFnAttribute fnattr(attr);
				MString name = fnattr.name();
				render->ParameterFromAttribute(name.asChar(), extattr.c_str());
			}
		}
		else if( buse)
		{
			setParameterToRender(externnode, attr, type, render);
		}
	}
	{
		MFnStringArrayData array;
		MObject val = array.create(attrArray);
		plugListAttrs.setValue(val);
	}

	return shader;
}

std::string Slim::mtorShaderNames(
	MObject obj, 
	const char* attrname, 
	paramlist_t& params, 
	shaderlist_t& shaderlist,
	const char* workspacedir,
	bool displayEnabled, 
	bool paramIterations
	)
{
	MStatus stat;
	MStringArray res;
	char buf[1024];
	MFnDependencyNode dn(obj);

	std::string id;
	// ������ ��������
	MObject o;
	MPlug pl = dn.findPlug("slimEns", &stat);
	if(stat && strcmp( attrname, "slimEns")!=0)
	{
		pl.getValue(o);
		res = MFnStringArrayData(o).array();
		for(;;)
		{
			if( res.length()<1) break;

			std::string idEn = res[0].asChar();
			if( idEn.empty()) break;

			_snprintf(buf, 1024, "slimcmd slim GetAppearances -id \"%s\"", idEn.c_str());
			if( !MGlobal::executeCommand(buf, res, displayEnabled, false))
				break;
			if(res.length()<1) 
				break;
			std::string appSurf = res[0].asChar();
			if( appSurf.empty()) 
				break;

			std::string property = "Surface";
			if( strcmp( attrname, "slimSurf")==0)
			{
				property = "Surface";
			}
			else if( strcmp( attrname, "slimDispl")==0)
			{
				property = "Displacement";
			}

			// slimcmd "func544" GetProperties -name Surface
			_snprintf(buf, 1024, "slimcmd \"%s\" GetProperties -name %s", appSurf.c_str(), property.c_str());
			if( !MGlobal::executeCommand(buf, res, displayEnabled, false))
				break;
			if(res.length()<1) 
				break;

			// slimcmd "parm22846" GetConnection;
			_snprintf(buf, 1024, "slimcmd \"%s\" GetConnection", res[0].asChar());
			if( !MGlobal::executeCommand(buf, res, displayEnabled, false))
				return "";
			if(res.length()<1) 
				return "";

			id = res[0].asChar();
			break;
		}
	}
	// ������ �������
	if( id.empty())
	{
		pl = dn.findPlug(attrname, &stat);
		if(!stat)
			return "";

		pl.getValue(o);
		res = MFnStringArrayData(o).array();
		if( res.length()<1)
			return "";

		id = res[0].asChar();
		if( id.empty()) return "";
	}

	shaderlist_t::iterator it = shaderlist.find(id);
	if( it != shaderlist.end())
	{
		params = it->second.paramlist;
		return it->second.shadername;
	}

//displayString("id: %s", id.c_str());
//	bool displayEnabled = false;

	// $appSurf = `slimcmd slim GetAppearances -id $newSurfID[0]`;
	_snprintf(buf, 1024, "slimcmd slim GetAppearances -id \"%s\"", id.c_str());
	if( !MGlobal::executeCommand(buf, res, displayEnabled, false))
		return "";
	if(res.length()<1) 
		return "";
	std::string appSurf = res[0].asChar();
	if( appSurf.empty()) return "";

	// $ShdName[0] = `slimcmd $appSurf GetName`;
	_snprintf(buf, 1024, "slimcmd \"%s\" GetName", appSurf.c_str());
	if( !MGlobal::executeCommand(buf, res, displayEnabled, false))
		return "";
	if(res.length()<1) 
		return "";
	std::string shaderName = res[0].asChar();
	shaderName = "rmanshader/" + shaderName;

	// GetMaster
	_snprintf(buf, 1024, "slimcmd \"%s\" GetMaster 1", appSurf.c_str());
	if( !MGlobal::executeCommand(buf, res, displayEnabled, false))
		return "";
	if(res.length()<1) 
		return "";
	shaderName = res[0].asChar();

//displayString("shader: %s", shaderName.c_str());

	if(paramIterations)
	{
		params.clear();
		RecursiveFindParams( displayEnabled, 0, id.c_str(), params);
	}
	if(false)
	{
//		float displaceBound;
//		mtorGetDisplaceBound( displayEnabled, id.c_str(), displaceBound);
	}

	// build shader
	// `slimcmd $appSurf PreviewRender`;
	if( strncmp(appSurf.c_str(), "inst", 4)==0 )
	{
		_snprintf(buf, 1024, "slimcmd \"%s\" GetMaster", appSurf.c_str());
		MGlobal::executeCommand(buf, res, displayEnabled, false);
		if(res.length()<1) 
			return "";
		shaderName = res[0].asChar();
	}
	else
	{
		_snprintf(buf, 1024, "slimcmd \"%s\" BuildShader", appSurf.c_str());
		MGlobal::executeCommand(buf, res, displayEnabled, false);
	}

	params.clear();
	sloReadShaderParams(shaderName.c_str(), params, workspacedir);

	shaderlist[id].paramlist = params;
	shaderlist[id].shadername = shaderName;

	return shaderName;
}


void Slim::sloReadShaderParams(const char* shaderName, paramlist_t& params, const char* workspacedir)
{
#ifdef _DEBUG
	getISloDll SloDll("IPrman12.5D.dll", "getISlo");
#endif
	if( !SloDll.isValid())
		return;
	ISlo* slo = (*SloDll)();
	int res = slo->Slo_SetShader(shaderName);
	if(res!=0)
	{
		std::string sn = std::string(workspacedir)+shaderName;
		res = slo->Slo_SetShader(sn.c_str());
		if(res!=0)
		{
			displayString("slo->Slo_SetShader(%s) failed", shaderName);
		}
	}

	int n = slo->Slo_GetNArgs();
//		std::map<std::string, cls::Param> params;
	for(int i=0; i<n; i++)
	{
		SLO_VISSYMDEF* def = slo->Slo_GetArgById(i+1);
		if( def->svd_storage == SLO_STOR_OUTPUTPARAMETER)
		{
			// ���������� �����
			continue;
		}
		switch(def->svd_type)
		{
		case SLO_TYPE_POINT:
			{
			Math::Vec3f v(def->svd_default.pointval->xval, def->svd_default.pointval->yval, def->svd_default.pointval->zval);
			params[def->svd_name] = cls::P<Math::Vec3f>(v, cls::PT_POINT);
			displayStringD( "%s: %s = %f %f %f\n", "point", def->svd_name, v.x, v.y, v.z);
			}
			break;
		case SLO_TYPE_COLOR:
			{
			Math::Vec3f v(def->svd_default.pointval->xval, def->svd_default.pointval->yval, def->svd_default.pointval->zval);
			params[def->svd_name] = cls::P<Math::Vec3f>(v, cls::PT_COLOR);
			displayStringD( "%s: %s = %f %f %f \n", "color", def->svd_name, v.x, v.y, v.z);
			}
			break;
		case SLO_TYPE_SCALAR:
			{
				float v = *def->svd_default.scalarval;
				params[def->svd_name] = cls::P<float>(v);
				displayStringD( "%s: %s = %f\n", "float", def->svd_name, v);
			}
			break;
		case SLO_TYPE_STRING:
			{
				const char* v = def->svd_default.stringval;
				params[def->svd_name] = cls::PS(v);
				displayStringD( "%s: %s = %s\n", "string", def->svd_name, v);
			}
			break;
		}
	}
	slo->Slo_EndShader();
}

bool Slim::mtorGetDisplaceBound(
	MObject obj, 
	const char* attrname, 
	float& displaceBound)
{
	bool displayEnabled = false;
	MStatus stat;
	MStringArray res;
	char buf[1024];
	MFnDependencyNode dn(obj);

	MPlug pl = dn.findPlug(attrname, &stat);
	if(!stat)
		return false;

	MObject o;
	pl.getValue(o);
	res = MFnStringArrayData(o).array();
	if( res.length()<1)
		return false;

	std::string id = res[0].asChar();
	if( id.empty()) 
		return false;

	// $appSurf = `slimcmd slim GetAppearances -id $newSurfID[0]`;
	_snprintf(buf, 1024, "slimcmd slim GetAppearances -id \"%s\"", id.c_str());
	if( !MGlobal::executeCommand(buf, res, displayEnabled, false))
		return false;
	if(res.length()<1) 
		return false;
	std::string appSurf = res[0].asChar();

	// $ShdName[0] = `slimcmd $appSurf GetName`;
	_snprintf(buf, 1024, "slimcmd \"%s\" GetName", appSurf.c_str());
	if( !MGlobal::executeCommand(buf, res, displayEnabled, false))
		return false;
	if(res.length()<1) 
		return false;
	std::string shaderName = res[0].asChar();

	// $prop = `slimcmd $appSurf GetProperties`;
	_snprintf(buf, 1024, "slimcmd \"%s\" GetProperties", appSurf.c_str());
	if( !MGlobal::executeCommand(buf, res, displayEnabled, false))
		return false;
	if(res.length()<1) 
		return false;

	std::vector<std::string> proplist;
	std::string prop = res[0].asChar();
	char* substr = strtok((char*)prop.c_str(), " ");
	for(; substr; substr = strtok(NULL, " "))
		proplist.push_back(substr);

	for(unsigned i=0; i<proplist.size(); i++)
	{
		std::string& substr = proplist[i];
//displayString("prop%d: %s", i, substr.c_str());
		MStringArray res2;
		// slimcmd $tm GetValueProvider == variable
		_snprintf(buf, 1024, "slimcmd \"%s\" GetValueProvider", substr.c_str());
		if( !MGlobal::executeCommand(buf, res2, displayEnabled, false))
			continue;
		if(res2.length()<1) 
			continue;

		if( strcmp(res2[0].asChar(), "variable")!=0)
		{
			_snprintf(buf, 1024, "slimcmd \"%s\" GetName", substr.c_str());
			if( !MGlobal::executeCommand(buf, res2, displayEnabled, false))
				continue;
			if(res2.length()<1) 
				continue;
			std::string externParam_name  = res2[0].asChar();

			if( externParam_name != "displacementbound")
			{
				continue;
			}

			_snprintf(buf, 1024, "slimcmd \"%s\" GetValue", substr.c_str());
			if( !MGlobal::executeCommand(buf, res2, displayEnabled, false))
				continue;

			if(res2.length()<1) 
				continue;

			displaceBound = (float)atof(res2[0].asChar());
			return true;

//			displayStringD("%s:", externParam_name.c_str());
//			for(unsigned x=0; x<res2.length(); x++)
//			{
//				displayStringD("%s", res2[x].asChar());
//			}
//			continue;
		}
	}
	return false;
}

void Slim::RecursiveFindParams(
	bool displayEnabled,
	int level, 
	const char* id, 
	paramlist_t& params)
{
	if( level>10) return;
	MStatus stat;
	MStringArray res;
	char buf[1024];

	// $appSurf = `slimcmd slim GetAppearances -id $newSurfID[0]`;
	_snprintf(buf, 1024, "slimcmd slim GetAppearances -id \"%s\"", id);
	if( !MGlobal::executeCommand(buf, res, displayEnabled, false))
		return;
	if(res.length()<1) 
		return;
	std::string appSurf = res[0].asChar();

	// $ShdName[0] = `slimcmd $appSurf GetName`;
	_snprintf(buf, 1024, "slimcmd \"%s\" GetName", appSurf.c_str());
	if( !MGlobal::executeCommand(buf, res, displayEnabled, false))
		return;
	if(res.length()<1) 
		return;
	std::string shaderName = res[0].asChar();

	// $prop = `slimcmd $appSurf GetProperties`;
	_snprintf(buf, 1024, "slimcmd \"%s\" GetProperties", appSurf.c_str());
	if( !MGlobal::executeCommand(buf, res, displayEnabled, false))
		return;
	if(res.length()<1) 
		return;

	std::vector<std::string> proplist;
	std::string prop = res[0].asChar();
	char* substr = strtok((char*)prop.c_str(), " ");
	for(; substr; substr = strtok(NULL, " "))
		proplist.push_back(substr);

	for(unsigned i=0; i<proplist.size(); i++)
	{
		std::string& substr = proplist[i];
//displayString("prop%d: %s", i, substr.c_str());
		MStringArray res2;

		// slimcmd $tm GetName;
		_snprintf(buf, 1024, "slimcmd \"%s\" GetName", substr.c_str());
		if( !MGlobal::executeCommand(buf, res2, displayEnabled, false))
			continue;
		if(res2.length()<1) 
			continue;
		std::string paramName = res2[0].asChar();

		// slimcmd $tm GetValueProvider == variable
		_snprintf(buf, 1024, "slimcmd \"%s\" GetValueProvider", substr.c_str());
		if( !MGlobal::executeCommand(buf, res2, displayEnabled, false))
			continue;
		if(res2.length()<1) 
			continue;
		if( strcmp(res2[0].asChar(), "connection")==0)
		{
			// slimcmd $tm GetName;
			_snprintf(buf, 1024, "slimcmd \"%s\" GetConnection", substr.c_str());
			if( !MGlobal::executeCommand(buf, res2, displayEnabled, false))
				continue;
			if(res2.length()<1) 
				continue;
			std::string connection = res2[0].asChar();
			if( connection.empty())
				continue;

//displayString("connection: %s", connection.c_str());
	
			RecursiveFindParams( displayEnabled, level+1, connection.c_str(), params);
			continue;
		}
		if( strcmp(res2[0].asChar(), "variable")!=0)
		{
			_snprintf(buf, 1024, "slimcmd \"%s\" GetName", substr.c_str());
			if( !MGlobal::executeCommand(buf, res2, displayEnabled, false))
				continue;
			if(res2.length()<1) 
				continue;
			std::string externParam_name  = res2[0].asChar();

			_snprintf(buf, 1024, "slimcmd \"%s\" GetValue", substr.c_str());
			if( !MGlobal::executeCommand(buf, res2, displayEnabled, false))
				continue;
			displayStringD("%s:", externParam_name.c_str());
			for(unsigned x=0; x<res2.length(); x++)
			{
				displayStringD("%s", res2[x].asChar());
			}
			continue;
		}

		// slimcmd $tm GetName;
		_snprintf(buf, 1024, "slimcmd \"%s\" GetName", substr.c_str());
		if( !MGlobal::executeCommand(buf, res2, displayEnabled, false))
			continue;
		if(res2.length()<1) 
			continue;
		std::string externParam_name  = res2[0].asChar();

		// slimcmd $tm GetType;
		_snprintf(buf, 1024, "slimcmd \"%s\" GetType", substr.c_str());
		if( !MGlobal::executeCommand(buf, res2, displayEnabled, false))
			continue;
		if(res2.length()<1) 
			continue;
		std::string externParam_type = res2[0].asChar();

		externParam_name = shaderName + "_" + externParam_name;

		// ��� �� ��!
		params[externParam_name] = cls::Param();
//		displayString("param: %s %s", type.c_str(), name.c_str());

		{
			//GetProperties -access * -type * -name * 
			_snprintf(buf, 1024, "slimcmd \"%s\" GetValue", substr.c_str());
			if( !MGlobal::executeCommand(buf, res2, displayEnabled, false))
				continue;
			displayStringD("%s:", externParam_name.c_str());
			for(unsigned x=0; x<res2.length(); x++)
			{
				displayStringD("%s", res2[x].asChar());
			}
		}

	}
}

void Slim::setParameterToRender(MObject obj, MObject attr, cls::enParamType type, cls::IRender* render)
{
	MStatus stat;
	MFnAttribute fnattr(attr);
	MString name = fnattr.name();

	// ��������! ���� ��������!!!
	/*/
	{
		MPlug plug(obj, attr);
		if( !plug.isNull())
		{
			MPlugArray pa;
			plug.connectedTo(pa, true, false);
			if( pa.length()==1)
			{
				MObject attrnode = pa[0].node();
				ParticleAttributeGenerator generator;
				if( generator.OnAttribute(attrnode, render, name.asChar(), type))
					return;
			}
		}
	}
	/*/

	cls::Param p = ocExport_GetAttrValue(obj, attr);
	if( p.empty()) return;

	if( render)
	{
		render->Parameter(name.asChar(), p);
	}
}

void Slim::setAttributeToRender(MObject obj, const char* name, MObject attr, cls::enParamType type, cls::IRender* render, bool bGlobal)
{
	cls::Param p = ocExport_GetAttrValue(obj, attr);
	if( p.empty()) return;
	if( render)
	{
		if( bGlobal)
			render->GlobalAttribute(name, p);
		else
			render->Attribute(name, p);
	}
}
