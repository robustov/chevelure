#include "stdafx.h"
#include "../generators/MeshGenerator.h"
#include "../generators/EdgesMeshGenerator.h"
#include "../generators/NuCurveGenerator.h"
#include "../generators/NurbsGenerator.h"

#include "../generators/TransformGenerator.h"
#include "../generators/MayaTransformGenerator.h"
#include "../generators/NameAttributeGenerator.h"
#include "../generators/SlimShaderGenerator.h"
//#include "../generators/ocShaderGenerator.h"
#include "../generators/LightGenerator.h"
#include "../generators/ParticleGenerator.h"
#include "../generators/InstancerGenerator.h"
#include "../generators/ocGeometryGenerator.h"
#include "../generators/FluidGenerator.h"
#include "../generators/SkinFilterDeformGenerator.h"

#include "../generators/StandartNodeFilter.h"

//#include "../generators/PassGenerator_Shadow.h"
#include "../generators/Node/RendAttrNodeGenerator.h"
#include "../generators/Geometry/SkinClusterGenerator.h"
#include "../generators\Shader\MayaShaderGenerator.h"
#include "../generators/camera/CameraGenerator.h"

// shading nodes
#include "../generators/ShadingNodeGenerator/LambertSNGenerator.h"
#include "../generators/ShadingNodeGenerator/PhongSNGenerator.h"
#include "../generators/ShadingNodeGenerator/ImageFileSNGenerator.h"
#include "../generators/ShadingNodeGenerator/place2dTextureSNGenerator.h"
#include "../generators/ShadingNodeGenerator/SpotLightSNGenerator.h"
#include "../generators/ShadingNodeGenerator/RampSNGenerator.h"
#include "../generators/ShadingNodeGenerator/noiseSNGenerator.h"


// obsolete
#include "../ocellaris/SlimShaderGenerator.h"

#include "ocellaris/renderFileImpl.h"
#include "ocellaris/renderProxy.h"
#include <maya/MItDag.h>
#include <mathNmaya/mathNmaya.h>

#include "ocellaris/OcellarisExport.h"
#include "ocellaris/ChiefGenerator.h"
#include "ExportDeformerProxy.h"
#include "ExportContextImpl.h"
#include "Node.h"

HANDLE ocellarisModule = 0;

// ���� � dll
const char* ocGetPath( 
	)
{
	static char filename[512]=""; 
	GetModuleFileName( (HMODULE)ocellarisModule, filename, 512); 
	return filename;
}

//! ����� ExportContext
cls::IExportContext* ocNewExportContext( 
	)
{
	return new ExportContextImpl;
}
OCELLARISEXPORT_API void ocDeleteExportContext( 
	cls::IExportContext* context
	)
{
	delete context;
}

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		{
			ocellarisModule = hModule;
			break;
		}
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
		{
			break;
		}
	case DLL_PROCESS_DETACH:
		{
			break;
		}
	}
    return TRUE;
}



ocGeometryGenerator theGeometryGenerator;
//! ����������� ��������� ��� ��� � ocsCache ��������� (ocGeometryGenerator)
cls::IGeometryGenerator* ocGetGeometryGenerator()
{
	return &theGeometryGenerator;
}


extern std::string objectname;

/*/
void ocExportTree(
	MDagPath path, 
	cls::IRender* render, 
	Math::Box3f& box, 
	Math::Matrix4f trans, 
	int level, 
	ChiefGenerator& chef, 
	cls::IExportContext* context)
{
	MObject obj = path.node();
	std::string x = path.fullPathName().asChar();
	objectname = path.partialPathName().asChar();
	MFn::Type type = path.apiType();
	Node node;
	node.set(path);

	ExportDeformerProxy proxy(render, context);
	if( render)
		render = &proxy;

// 1. ��� ������� ������ ������ �������� (���������� ������ INodeFilter) � ���� ���� ���� �� ������� 
// INodeFilter::IsRenderNode() ������ false ������ ������ �� ��������������.
	{
		ChiefGenerator::nodefilterlist_t filterlist;
		chef.getFilters( obj, filterlist);
		for( unsigned i=0; i<filterlist.size(); i++)
		{
			bool bFlag = filterlist[i]->IsRenderNode(obj, render);
			if( !bFlag) return;
		}
	}

	// 2. ��������� ITransformGenerator ��������� �������������� � ��������
	// ���� ����� ���� � ���� ���������� ����� ITransformGenerator::OnTransformStart().
	cls::ITransformGenerator* transform = chef.getTransform(path);
	if( transform)
	{
		Math::Matrix4f m;
		transform->OnTransformStart(node, render, level, trans, m);
		trans = m;
	}

	// 3. ��������� ������ ����������� ������� (INodeGenerator)
	// � ������� ���������� ����� INodeGenerator::OnNodeStart().
	ChiefGenerator::nodegenlist_t nodelist;
	chef.getNode(obj, nodelist);
	for( unsigned i=0; i<nodelist.size(); i++)
	{
		nodelist[i]->OnNodeStart(obj, render);
	}

	// 4-6. ����� ����������� ���������, ������� � ����������.
	// ���� ��� ������� ������ ��������� ��������� (IGeometryGenerator), 

	// �� ��� ����� ������� ����������� ��������� ���������:
	// - ��������� ��������� ������� (IShaderGenerator) �������������� � �������� 
	// � ���� �� ������ ���������� ����� IShaderGenerator::OnShader(obj, render);
	ChiefGenerator::attrgenlist_t attrlist;
	chef.getAttribute(path, attrlist);
	cls::IGeometryGenerator* geometry = chef.getGeometry(obj);
	ChiefGenerator::shadergenlist_t shader;
	chef.getShader(obj, shader);
	if( geometry)
	{
		for( i=0; i<shader.size(); i++)
		{
			shader[i]->OnShader(obj, render);
		}
	}

	// - ��������� ������ ����������� ��������� (IAttributeGenerator) � ��� ������� 
	// ���������� IAttributeGenerator::OnAttribute();
	for( i=0; i<attrlist.size(); i++)
	{
		attrlist[i]->OnAttribute(obj, render);
	}

	// - ���������� ����� IGeometryGenerator::OnGeometry();
	if( geometry)
	{
		// ��� draw ������� (Points, Mesh, Sphere � �.�.)
		// ����� ���������� ����� ������ �����������
		proxy.currentNode = obj;
		chef.getDeform(path, proxy.deformgenlist);
		
		Math::Matrix4f m = trans;
		Math::Box3f lb;

		geometry->OnGeometry(obj, render, lb, context);
		lb.transform(m);
		box.insert(lb);
	}

	// childs
	for( unsigned i=0; i<path.childCount(); i++)
	{
		MObject obj = path.child(i);
		MDagPath path;
		MDagPath::getAPathTo(obj, path);
		ocExportTree( path, render, box, trans, level+1, chef, context);
	}


	for( unsigned i=0; i<nodelist.size(); i++)
	{
		nodelist[i]->OnNodeEnd(obj, render);
	}
	if( transform)
	{
		transform->OnTransformEnd(node, render, level);
	}
}
/*/

// ������ ����������������� �����������
struct GeneratorDecl
{
	ChiefGenerator::type_t type;
	cls::IGenerator* gen;
	GeneratorDecl(){gen=0;};
};

std::list<GeneratorDecl> externGenerators;

// ����������� ����������
void OCELLARISEXPORT_API registerGenerator(MFn::Type t, cls::IGenerator* gen)
{
	externGenerators.push_back(GeneratorDecl());
	GeneratorDecl& decl = externGenerators.back();
	decl.type = t;
	decl.gen = gen;
}
void OCELLARISEXPORT_API registerGenerator(MTypeId t, cls::IGenerator* gen)
{
	externGenerators.push_back(GeneratorDecl());
	GeneratorDecl& decl = externGenerators.back();
	decl.type = t;
	decl.gen = gen;
}

void OCELLARISEXPORT_API unregisterGenerator(MTypeId t, cls::IGenerator* gen)
{
	std::list<GeneratorDecl>::iterator it = externGenerators.begin();
	for(;it != externGenerators.end(); it++)
	{
		GeneratorDecl& decl = *it;
		if( decl.type == t &&
			decl.gen == gen)
				break;
	}
	if( it!=externGenerators.end())
		externGenerators.erase(it);
}
void OCELLARISEXPORT_API unregisterGenerator(MFn::Type t, cls::IGenerator* gen)
{
	std::list<GeneratorDecl>::iterator it = externGenerators.begin();
	for(;it != externGenerators.end(); it++)
	{
		GeneratorDecl& decl = *it;
		if( decl.type == t &&
			decl.gen == gen)
				break;
	}
	if( it!=externGenerators.end())
		externGenerators.erase(it);
}

// ���������� �� ���������
void OCELLARISEXPORT_API ocellarisDefaultGenerators(
	ChiefGenerator& chef, 
	const char* options)
{
//	static PassGenerator_Shadow passgenerator_shadow;
//	chef.addGenerator(MFn::kSpotLight, &passgenerator_shadow);

	static TransformGenerator tg;
	chef.addGenerator(MFn::kTransform, &tg);
//	static MayaTransformGenerator mtg;
//	chef.addGenerator(MFn::kTransform, &mtg);

	static RendAttrNodeGenerator rendattrnodegenerator;
	chef.addGenerator(MFn::kShape, &rendattrnodegenerator);
	chef.addGenerator(MFn::kInstancer, &rendattrnodegenerator);

	static MeshGenerator mg;
	chef.addGenerator(MFn::kMesh, &mg);
//	static EdgesMeshGenerator mg;
//	chef.addGenerator(MFn::kMesh, &mg);

	static SkinClusterGenerator skinclustergenerator;
	chef.addGenerator(MFn::kSkinClusterFilter, &skinclustergenerator);

//	static NuCurveGenerator cg;
//	chef.addGenerator(MFn::kNurbsCurve, &cg);
	static NurbsGenerator nurg;
	chef.addGenerator(MFn::kNurbsSurface, &nurg);
	static ParticleGenerator pg;
	chef.addGenerator(MFn::kParticle, &pg);
	static InstancerGenerator ig;
	chef.addGenerator(MFn::kInstancer, &ig);
	static InstancerPassGenerator ipg;
	chef.addGenerator(MFn::kInstancer, &ipg);

//	static SlimShaderGenerator sg;
//	int bUseMtor = 0;
//	MGlobal::executeCommand("exists mtor", bUseMtor);
//	if( bUseMtor)
//		chef.addGenerator(MFn::kShape, &sg);

#ifdef SAS
//	static MayaShaderGenerator mayashadergenerator;
//	chef.addGenerator(MFn::kShape, &mayashadergenerator);
#endif

	static NameAttributeGenerator nag;
	chef.addGenerator(MFn::kBase, &nag);

	static StandartNodeFilter snf;
	chef.addGenerator(MFn::kBase, &snf);

	static FluidGenerator fg;
	chef.addGenerator( MFn::kFluid, &fg);

	static CameraGenerator cameragenerator;
	chef.addGenerator( MFn::kCamera, &cameragenerator);
	chef.addGenerator( MFn::kLight, &cameragenerator);
	{
		static LambertSNGenerator lg;
		chef.addGenerator( MFn::kLambert, &lg);
		static PhongSNGenerator phongsngenerator;
		chef.addGenerator( MFn::kPhongExplorer, &phongsngenerator);
		static ImageFileSNGenerator ifg;
		chef.addGenerator( MFn::kFileTexture, &ifg);
		static SpotLightSNGenerator spotlightsngenerator;
		chef.addGenerator( MFn::kSpotLight, &spotlightsngenerator);
		static RampSNGenerator rampsngenerator;
		chef.addGenerator( MFn::kRamp, &rampsngenerator);
		static NoiseSNGenerator noisesngenerator;
		chef.addGenerator( MFn::kNoise, &noisesngenerator);
		
		
//		static place2dTextureSNGenerator p2dtg;
//		chef.addGenerator( MFn::kPlace2dTexture, &p2dtg);
	}

//	static LightGenerator lg;
//	chef.addGenerator(MFn::kLight, &lg);

	std::list<GeneratorDecl>::iterator it = externGenerators.begin();
	for(;it != externGenerators.end(); it++)
	{
		GeneratorDecl& decl = *it;
		chef.addGenerator(decl.type, decl.gen);
	}
}

// ���������� �� ���������
void OCELLARISEXPORT_API ocellarisDefaultGenerators(
	enGeneratorsType gtype, 
	ChiefGenerator& chef, 
	cls::IExportContext* context)
{
	switch(gtype)
	{
	case OCGT_SCENE:
		{
//			static PassGenerator_Shadow passgenerator_shadow;
//			chef.addGenerator(MFn::kSpotLight, &passgenerator_shadow);

			static TransformGenerator tg;
			chef.addGenerator(MFn::kTransform, &tg);
//			static MayaTransformGenerator mtg;
//			chef.addGenerator(MFn::kTransform, &mtg);

			static RendAttrNodeGenerator rendattrnodegenerator;
			chef.addGenerator(MFn::kShape, &rendattrnodegenerator);
			chef.addGenerator(MFn::kInstancer, &rendattrnodegenerator);

			static MeshGenerator mg;
			chef.addGenerator(MFn::kMesh, &mg);
//			static EdgesMeshGenerator mg;
//			chef.addGenerator(MFn::kMesh, &mg);

			static SkinClusterGenerator skinclustergenerator;
			chef.addGenerator(MFn::kSkinClusterFilter, &skinclustergenerator);

//			static NuCurveGenerator cg;
//			chef.addGenerator(MFn::kNurbsCurve, &cg);
			static NurbsGenerator nurg;
			chef.addGenerator(MFn::kNurbsSurface, &nurg);
			static ParticleGenerator pg;
			chef.addGenerator(MFn::kParticle, &pg);
			static InstancerGenerator ig;
			chef.addGenerator(MFn::kInstancer, &ig);

			static SlimShaderGenerator sg;
			int bUseMtor = 0;
			MGlobal::executeCommand("exists mtor", bUseMtor);
			if( bUseMtor)
				chef.addGenerator(MFn::kShape, &sg);

			static NameAttributeGenerator nag;
			chef.addGenerator(MFn::kBase, &nag);

			static StandartNodeFilter snf;
			chef.addGenerator(MFn::kBase, &snf);

			static FluidGenerator fg;
			chef.addGenerator( MFn::kFluid, &fg);

			{
				static LambertSNGenerator lg;
				chef.addGenerator( MFn::kLambert, &lg);
				static ImageFileSNGenerator ifg;
				chef.addGenerator( MFn::kFileTexture, &ifg);
//				static place2dTextureSNGenerator p2dtg;
//				chef.addGenerator( MFn::kPlace2dTexture, &p2dtg);
			}

			std::list<GeneratorDecl>::iterator it = externGenerators.begin();
			for(;it != externGenerators.end(); it++)
			{
				GeneratorDecl& decl = *it;
				chef.addGenerator(decl.type, decl.gen);
			}

			break;
		}
	case OCGT_LIGHTS:
		{
			static LightGenerator lg;
			chef.addGenerator(MFn::kLight, &lg);
			static StandartNodeFilter snf;
			chef.addGenerator(MFn::kBase, &snf);
//			NameAttributeGenerator nag;
//			cheflight.addGenerator(MFn::kShape, &nag);
			break;
		}
	case OCGT_SHADERS:
		{
			static SlimShaderGenerator sg;
			chef.addGenerator(MFn::kShape, &sg);
			std::list<GeneratorDecl>::iterator it = externGenerators.begin();
			for(;it != externGenerators.end(); it++)
			{
				GeneratorDecl& decl = *it;
				switch(decl.gen->GetType())
				{
				case cls::IGenerator::SHADER:
					chef.addGenerator(decl.type, (cls::IShaderGenerator*)decl.gen);
					break;
				}
			}
			break;
		}
	}
}

/*/
// �����
void OCELLARISEXPORT_API ocExportTree( 
	MObject obj, 
	ChiefGenerator& chief, 
	cls::IRender* render, 
	Math::Box3f& box, 
	Math::Matrix4f& startTransform, 
	cls::IExportContext* context)
{
	MDagPath path;
	MDagPath::getAPathTo(obj, path);
	chief.startExport();
	ocExportTree( path, render, box, startTransform, 0, chief, context);
	chief.endExport();
}

void ocExportTree(
	MObject obj, 
	cls::IRender* render, 
	Math::Box3f& box, 
	cls::IExportContext* context)
{
	MDagPath path;
	MDagPath::getAPathTo(obj, path);

	// ������� �����
	{
		ChiefGenerator cheflight;
		ocellarisDefaultGenerators(OCGT_LIGHTS, cheflight, context);
		cheflight.startExport();
		ocExportTree( path, render, box, Math::Matrix4f::id, 0, cheflight, context);
		cheflight.endExport();
	}

	// �����
	{
		ChiefGenerator chef;
		ocellarisDefaultGenerators(OCGT_SCENE, chef, context);
		chef.startExport();
		ocExportTree( path, render, box, Math::Matrix4f::id, 0, chef, context);
		chef.endExport();
	}
}

void ocExportBlurTree(
	MObject obj, 
	cls::IRender* render, 
	Math::Box3f& box, 
	std::vector<MTime> times, 
	cls::IExportContext* context)
{
	MDagPath path;
	MDagPath::getAPathTo(obj, path);

	// ������� �����
	{
		ChiefGenerator cheflight;
		ocellarisDefaultGenerators(OCGT_LIGHTS, cheflight, context);
		cheflight.startExport();
		ocExportTree( path, render, box, Math::Matrix4f::id, 0, cheflight, context);
		cheflight.endExport();
	}
	
	// �����

	{
		ChiefGenerator chef;
		ocellarisDefaultGenerators(OCGT_SCENE, chef, context);
		chef.startExport();

		MTime curtime = MAnimControl::currentTime();
		render->MotionBegin();
		for(unsigned i=0; i<times.size(); i++)
		{
			MTime t = times[i]-curtime;
			MAnimControl::setCurrentTime(times[i]);
			render->MotionPhaseBegin( (float)t.as(MTime::kSeconds));

			ocExportTree( path, render, box, Math::Matrix4f::id, 0, chef, context);

			render->MotionPhaseEnd();
		}
		render->MotionEnd();
		chef.endExport();
		MAnimControl::setCurrentTime(curtime);
	}
}
/*/
