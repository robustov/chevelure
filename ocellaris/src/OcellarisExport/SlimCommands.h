#pragma once

namespace Slim
{
	typedef std::map<std::string, cls::Param> paramlist_t;
	struct ShaderDeclare
	{
		std::string shadername;
		paramlist_t paramlist;
	};
	typedef std::map<std::string, ShaderDeclare> shaderlist_t;
//	shaderlist_t shaderlist;

	//! ������ ��� �������, ��������� ������ ����������, ������ ���������
	std::string readShaderNameNparameters(
		cls::IRender* render, 
		MObject obj, 
		const char* attrname, 
		paramlist_t& params, 
		MObject externnode, 
		const char* externnodeattr,
		const char* externnodeListattr, 
		shaderlist_t& shaderlist,
		const char* workspacedir, 
		bool bUseAll
		);
	//! ������ ��� ������� �� mtor
	std::string mtorShaderNames(
		MObject obj, 
		const char* attrname, 
		paramlist_t& paramlist, 
		shaderlist_t& shaderlist,
		const char* workspacedir,
		bool displayEnabled = false, 
		bool paramIterations = false
		);
	//! ������ ���������� ������� ��������������� �� slo �����
	void sloReadShaderParams(
		const char* shaderName, 
		paramlist_t& params, 
		const char* workspacedir);

	//! ��������! ������ ���������� ������� �� mtor
	void RecursiveFindParams(
		bool displayEnabled,
		int level, 
		const char* id, 
		paramlist_t& params);
	//! displaceBound
	bool mtorGetDisplaceBound(
		MObject obj, 
		const char* attrname, 
		float& displaceBound);
}