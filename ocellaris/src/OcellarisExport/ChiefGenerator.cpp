#include "stdafx.h"
#include "ocellaris/OcellarisExport.h"
#include "ocellaris/ChiefGenerator.h"
#include "ocellaris/ocGeneratorSet.h"
#include <maya/MFnDependencyNode.h>
#include "Util/misc_create_directory.h"

MTypeId ChiefGenerator::kNullId(0, 0);

/*/
//! �������� ������ ����������� ��������� ���� �� set �� MDagPath
//! ����������� ������ set� ���� ocGeneratorSet
//! ���������� ������ ����������
std::string findGeneratorSet(MDagPath& path, cls::IGenerator::enGenType type)
{
	// ���� set
	{
		MSelectionList objects;
		objects.add( path);
		MObjectArray setArray;
		MGlobal::getAssociatedSets( objects, setArray );
		// Look for a set that is a "shading group"
		for ( int i=0; i<(int)setArray.length(); i++ )
		{
			MObject mobj = setArray[i];
			MFnSet fnSet( mobj );
			if( fnSet.typeId() != cls::ocGeneratorSet::id) 
				continue;

			MPlug plug(mobj, cls::ocGeneratorSet::i_type);
			short typeval;
			plug.getValue(typeval);
			if( type!=(cls::IGenerator::enGenType)typeval)
				continue;

			plug = MPlug(mobj, cls::ocGeneratorSet::i_generator);
			MString nameval;
			plug.getValue(nameval);

			displayString("generator for %s: \"%s\"", path.fullPathName().asChar(), nameval.asChar());
			return nameval.asChar();
		}
	}
	return "";
}
/*/
/*/
void findGeneratorSetList(MDagPath& path, cls::IGenerator::enGenType type, std::vector<std::string>& genlist)
{
	genlist.clear();
	// ���� set
	{
		MSelectionList objects;
		objects.add( path);
		MObjectArray setArray;
		MGlobal::getAssociatedSets( objects, setArray );
		// Look for a set that is a "shading group"
		for ( int i=0; i<(int)setArray.length(); i++ )
		{
			MObject mobj = setArray[i];
			MFnSet fnSet( mobj );
			if( fnSet.typeId() != cls::ocGeneratorSet::id) 
				continue;

			MPlug plug(mobj, cls::ocGeneratorSet::i_type);
			short typeval;
			plug.getValue(typeval);
			if( type!=(cls::IGenerator::enGenType)typeval)
				continue;

			plug = MPlug(mobj, cls::ocGeneratorSet::i_generator);
			MString nameval;
			plug.getValue(nameval);

			genlist.push_back(nameval.asChar());
			displayString("generator for %s: \"%s\"", path.fullPathName().asChar(), nameval.asChar());
		}
	}
	return;
}
/*/

void ChiefGenerator::findGeneratorSetList(MDagPath& path, cls::IGenerator::enGenType type, generatorlist_t& genlist)
{
	MSelectionList objects;
	objects.add( path);
	findGeneratorSetList(objects, type, genlist);
}
void ChiefGenerator::findGeneratorSetList(MObject& obj, cls::IGenerator::enGenType type, generatorlist_t& genlist)
{
	MSelectionList objects;
	objects.add( obj);
	findGeneratorSetList(objects, type, genlist);
}

void ChiefGenerator::findGeneratorSetList(MSelectionList& objects, cls::IGenerator::enGenType type, generatorlist_t& genlist)
{
	MStringArray stringarray;
	objects.getSelectionStrings(stringarray);
	std::string name = stringarray[0].asChar();
	
	genlist.clear();
	// ���� set
	{
		MObjectArray setArray;
		MGlobal::getAssociatedSets( objects, setArray );
		// Look for a set that is a "shading group"
		for ( int i=0; i<(int)setArray.length(); i++ )
		{
			MObject mobj = setArray[i];
			MFnSet fnSet( mobj );
			if( fnSet.typeId() != cls::ocGeneratorSet::id) 
				continue;

			MPlug plug(mobj, cls::ocGeneratorSet::i_type);
			short typeval;
			plug.getValue(typeval);
			if( type!=(cls::IGenerator::enGenType)typeval)
				continue;

			plug = MPlug(mobj, cls::ocGeneratorSet::i_generator);
			MString nameval;
			plug.getValue(nameval);

			if( externgendlls.find(nameval.asChar())==externgendlls.end())
			{
				Util::DllProcedure dll;
				if( !dll.LoadFormated(nameval.asChar(), "OcellarisExport"))
				{
					displayString("ChiefGenerator::findGeneratorSetList: Cant load generator \"%s\"", nameval.asChar());
					continue;
				}
				externgendlls[nameval.asChar()] = dll;
			}
			Util::DllProcedure& dllproc = externgendlls[nameval.asChar()];
			cls::GENERATOR_CREATOR pc = (cls::GENERATOR_CREATOR)dllproc.proc;
			cls::IGenerator* gen = (*pc)();
			if( gen->GetType()!=type) 
			{
				printf("generator %s has not type %d only %d", nameval.asChar(), type, (int)gen->GetType());
				continue;
			}

			genlist.push_back( GeneratorNHisObject(gen, mobj));
//			displayString("generator for %s: \"%s\"", name.c_str(), gen->GetUniqueName());
		}
	}
	return;
}

ChiefGenerator::GeneratorNHisObject ChiefGenerator::getProxy( 
	cls::INode& node)
{
	MObject obj = node.getObject();
	MTypeId typeId = MFnDependencyNode(obj).typeId();
	typesPROXYgens_t::iterator it = typesPROXYgens.begin();
	for(;it != typesPROXYgens.end(); it++)
	{
		type_t type = it->first;
		if( type.type==MFn::kInvalid)
		{
			if( type.typeId == typeId)
				return it->second;
		}
		if( !obj.hasFn(type.type))
			continue;
		return it->second;
	}
	return ChiefGenerator::GeneratorNHisObject(NULL);
}

ChiefGenerator::GeneratorNHisObject ChiefGenerator::getTransform(
	cls::INode& node)
{
	MDagPath path = node.getPath();
	generatorlist_t genlist;
	findGeneratorSetList(path, cls::IGenerator::TRANSFORM, genlist);
	if( !genlist.empty())
	{
		// ����� ������
//		cls::IGenerator* gen = genlist.front().gen;
		return genlist.front();
	}

	MObject obj = node.getObject();
	cls::ITransformGenerator* defgen = NULL;
	MTypeId typeId = MFnDependencyNode(obj).typeId();
	typesTRANgens_t::iterator it = typesTRANgens.begin();
	for(;it != typesTRANgens.end(); it++)
	{
		type_t type = it->first;
		if( type.type!=MFn::kInvalid)
		{
			if( !obj.hasFn(type.type))
				continue;
		}
		else
		{
			if( type.typeId != typeId)
				continue;
		}

//		cls::ITransformGenerator* defgen = NULL;
		transformgenlist_t& list = it->second;
		transformgenlist_t::iterator itg = list.begin();
		for(;itg != list.end(); itg++)
		{
			return *itg;
		}
	}
	return NULL;
}

void ChiefGenerator::getShader(
	cls::INode& node, ChiefGenerator::shadergenlist_t& gens)
{
	MObject obj = node.getPath().node();
	MTypeId typeId = MFnDependencyNode(obj).typeId();
//	gens.clear();

	MDagPath path = node.getPath();
	generatorlist_t genlist;
	findGeneratorSetList(path, cls::IGenerator::SHADER, genlist);
	gens.reserve(genlist.size());
	generatorlist_t::iterator itgen = genlist.begin();
	for(;itgen != genlist.end(); itgen++)
	{
		gens.push_back( *itgen);
	}

	typesSHADgens_t::iterator it = typesSHADgens.begin();
	for(;it != typesSHADgens.end(); it++)
	{
		type_t type = it->first;
		if( type.type==MFn::kInvalid)
		{
			if( type.typeId != typeId)
				continue;
		}
		else
		{
			if( !obj.hasFn(type.type))
				continue;
		}
		shadergenlist_t& src = it->second;
		gens.insert(gens.end(), src.begin(), src.end());
	}
	return;
}

ChiefGenerator::GeneratorNHisObject ChiefGenerator::getGeometry(
	cls::INode& node)
{
	MDagPath path = node.getPath();
	generatorlist_t genlist;
	findGeneratorSetList(path, cls::IGenerator::GEOMETRY, genlist);
	if( !genlist.empty())
	{
		GeneratorNHisObject& gen = genlist.front();
		return gen;
	}
	else if(path.node()!=node.getObject())
	{
		MDagPath path = MDagPath::getAPathTo(node.getObject());
		findGeneratorSetList(path, cls::IGenerator::GEOMETRY, genlist);
		if( !genlist.empty())
		{
			GeneratorNHisObject& gen = genlist.front();
			return gen;
		}
	}

	MObject obj = node.getObject();
	MTypeId typeId = MFnDependencyNode(obj).typeId();
	typesGEOMgens_t::iterator it = typesGEOMgens.begin();
	for(;it != typesGEOMgens.end(); it++)
	{
		type_t type = it->first;
		if( type.type==MFn::kInvalid)
		{
			if( type.typeId == typeId)
				return it->second;
		}
		if( !obj.hasFn(type.type))
			continue;
		return it->second;
	}
	return NULL;
}

ChiefGenerator::GeneratorNHisObject ChiefGenerator::getLight(
	cls::INode& node)
{
	MDagPath path = node.getPath();
	generatorlist_t genlist;
	findGeneratorSetList(path, cls::IGenerator::LIGHT, genlist);
	if( !genlist.empty())
	{
		GeneratorNHisObject& gen = genlist.front();
		return gen;
	}
	else if(path.node()!=node.getObject())
	{
		MDagPath path = MDagPath::getAPathTo(node.getObject());
		findGeneratorSetList(path, cls::IGenerator::LIGHT, genlist);
		if( !genlist.empty())
		{
			GeneratorNHisObject& gen = genlist.front();
			return gen;
		}
	}

	MObject obj = node.getObject();
	MTypeId typeId = MFnDependencyNode(obj).typeId();
	typesGEOMgens_t::iterator it = typesLIGHTgens.begin();
	for(;it != typesLIGHTgens.end(); it++)
	{
		type_t type = it->first;
		if( type.type==MFn::kInvalid)
		{
			if( type.typeId == typeId)
				return it->second;
		}
		if( !obj.hasFn(type.type))
			continue;
		return it->second;
	}
	return NULL;
}


// ���������� ���������� ��� �������� node.getObject() � node.getPath()
// 
void ChiefGenerator::getNode(
	cls::INode& node, 
	ChiefGenerator::nodegenlist_t& gens)
{
	MObject obj = node.getObject();
	MTypeId typeId = MFnDependencyNode(obj).typeId();
//	gens.clear();

	MDagPath path = node.getPath();
	MObject pathobj = path.node();
	MTypeId pathtypeId = MFnDependencyNode(pathobj).typeId();

	generatorlist_t genlist;
	findGeneratorSetList(path, cls::IGenerator::NODE, genlist);
	generatorlist_t::iterator itgen = genlist.begin();
	for(;itgen != genlist.end(); itgen++)
	{
		gens.insert( *itgen);
	}

	typesNODEgens_t::iterator it = typesNODEgens.begin();
	for(;it != typesNODEgens.end(); it++)
	{
		type_t type = it->first;
		if( type.type==MFn::kInvalid)
		{
			if( type.typeId != typeId &&
				type.typeId != pathtypeId)
					continue;
		}
		else
		{
			if( !obj.hasFn(type.type) && 
				!pathobj.hasFn(type.type))
				continue;
		}
		nodegenlist_t& src = it->second;
		nodegenlist_t::iterator itgen = src.begin();
		for(;itgen != src.end(); itgen++)
		{
			gens.insert( *itgen);
		}
	}
}

void ChiefGenerator::getFilters(
	cls::INode& node, ChiefGenerator::nodefilterlist_t& gens)
{
	MObject obj = node.getObject();
	MTypeId typeId = MFnDependencyNode(obj).typeId();
//	gens.clear();

	typesFilters_t::iterator it = typesFilters.begin();
	for(;it != typesFilters.end(); it++)
	{
		type_t type = it->first;
		if( type.type==MFn::kInvalid)
		{
			if( type.typeId != typeId)
				continue;
		}
		else
		{
			if( !obj.hasFn(type.type))
				continue;
		}
		
		nodefilterlist_t& src = it->second;
		gens.insert(gens.end(), src.begin(), src.end());
	}
}

void ChiefGenerator::getAttribute(
	cls::INode& node, ChiefGenerator::attrgenlist_t& gens)
{
	MDagPath path = node.getPath();
	MObject obj = node.getObject();
	MTypeId typeId = MFnDependencyNode(obj).typeId();
//	gens.clear();

	generatorlist_t genlist;
	findGeneratorSetList(path, cls::IGenerator::ATTRIBUTE, genlist);
	gens.reserve(genlist.size());
	generatorlist_t::iterator itgen = genlist.begin();
	for(;itgen != genlist.end(); itgen++)
		gens.push_back( *itgen);

	typesATTRgens_t::iterator it = typesATTRgens.begin();
	for(;it != typesATTRgens.end(); it++)
	{
		type_t type = it->first;
		if( type.type==MFn::kInvalid)
		{
			if( type.typeId != typeId)
				continue;
		}
		else
		{
			if( !obj.hasFn(type.type))
				continue;
		}
		
		attrgenlist_t& src = it->second;
		gens.insert(gens.end(), src.begin(), src.end());
	}
}

void ChiefGenerator::getShadingNode(
	cls::INode& node, 
	ChiefGenerator::shadingnodegenlist_t& gens)
{
	MObject obj = node.getObject();
//	MDagPath path = node.getPath();
//	MObject obj = node.getObject();
	MTypeId typeId = MFnDependencyNode(obj).typeId();
//	gens.clear();

//	generatorlist_t genlist;
//	findGeneratorSetList(path, cls::IGenerator::SHADINGNODE, genlist);
//	gens.reserve(genlist.size());
//	generatorlist_t::iterator itgen = genlist.begin();
//	for(;itgen != genlist.end(); itgen++)
//		gens.push_back( *itgen);

	typesATTRgens_t::iterator it = typesSHADINGNODEgens.begin();
	for(;it != typesSHADINGNODEgens.end(); it++)
	{
		type_t type = it->first;
		if( type.type==MFn::kInvalid)
		{
			if( type.typeId != typeId)
				continue;
		}
		else
		{
			if( obj.apiType()!=type.type)
//			if( !obj.hasFn(type.type))
				continue;
		}
		
		shadingnodegenlist_t& src = it->second;
		gens.insert(gens.end(), src.begin(), src.end());
	}
}

void ChiefGenerator::getDeform(
	cls::INode& node, ChiefGenerator::deformgenlist_t& gens)
{
	MDagPath path = node.getPath();
	MObject obj = node.getObject();
	MTypeId typeId = MFnDependencyNode(obj).typeId();
//	gens.clear();

	generatorlist_t genlist;
	findGeneratorSetList(path, cls::IGenerator::DEFORM, genlist);
	gens.reserve(genlist.size());
	generatorlist_t::iterator itgen = genlist.begin();
	for(;itgen != genlist.end(); itgen++)
		gens.push_back( *itgen);

	typesDEFORMgens_t::iterator it = typesDEFORMgens.begin();
	for(;it != typesDEFORMgens.end(); it++)
	{
		type_t type = it->first;
		if( type.type==MFn::kInvalid)
		{
			if( type.typeId != typeId)
				continue;
		}
		else
		{
			if( !obj.hasFn(type.type))
				continue;
		}
		deformgenlist_t& src = it->second;
		gens.insert(gens.end(), src.begin(), src.end());
	}
}
ChiefGenerator::GeneratorNHisObject ChiefGenerator::getPass( 
	cls::INode& node)
{
	MObject obj = node.getObject();
	MFnDependencyNode dn(obj);
	MPlug plug = dn.findPlug("passGenerator");
	MString pgproc;
	plug.getValue(pgproc);
	if( pgproc.length())
	{
		if( externgendlls.find(pgproc.asChar())==externgendlls.end())
		{
			Util::DllProcedure dll;
			if( !dll.LoadFormated(pgproc.asChar(), "OcellarisExport"))
			{
				displayString("ChiefGenerator::getPass: Cant load generator \"%s\"", pgproc.asChar());
				return NULL;
			}
			externgendlls[pgproc.asChar()] = dll;
		}
		Util::DllProcedure& dllproc = externgendlls[pgproc.asChar()];
		cls::GENERATOR_CREATOR pc = (cls::GENERATOR_CREATOR)dllproc.proc;
		cls::IGenerator* gen = (*pc)();
		if( gen->GetType()!=cls::IGenerator::PASS) 
		{
			printf("generator %s has not type %d", pgproc.asChar(), cls::IGenerator::PASS);
			return NULL;
		}
		return (cls::IPassGenerator*)gen;
	}

	return findSingleGenerator(obj, typesPASSgens);
}
ChiefGenerator::GeneratorNHisObject ChiefGenerator::getBlur(
	cls::INode& node)
{
	MDagPath path = node.getPath();
	generatorlist_t genlist;
	findGeneratorSetList(path, cls::IGenerator::BLUR, genlist);
	if( !genlist.empty())
	{
		GeneratorNHisObject& gen = genlist.front();
		return gen;
	}
	else if(path.node()!=node.getObject())
	{
		MDagPath path = MDagPath::getAPathTo(node.getObject());
		findGeneratorSetList(path, cls::IGenerator::BLUR, genlist);
		if( !genlist.empty())
		{
			GeneratorNHisObject& gen = genlist.front();
			return gen;
		}
	}

	MObject obj = node.getObject();
	MTypeId typeId = MFnDependencyNode(obj).typeId();
	typesBLURgens_t::iterator it = typesBLURgens.begin();
	for(;it != typesBLURgens.end(); it++)
	{
		type_t type = it->first;
		if( type.type==MFn::kInvalid)
		{
			if( type.typeId == typeId)
				return it->second;
		}
		if( !obj.hasFn(type.type))
			continue;
		return it->second;
	}
	return NULL;
}

// clear
void ChiefGenerator::clear()
{
	// ����������
	typesPROXYgens.clear();
	typesFilters.clear();
	typesGEOMgens.clear();
	typesLIGHTgens.clear();
	typesNODEgens.clear();
	typesATTRgens.clear();
	typesSHADgens.clear();
	typesTRANgens.clear();
	typesDEFORMgens.clear();
	typesPASSgens.clear();
	typesSHADINGNODEgens.clear();
	typesSHADINGNODEgens.clear();
	typesBLURgens.clear();
}
// dump
void ChiefGenerator::dump(FILE* file) const
{
	int i=0;
	fprintf(file, "\nGENERATORS:\n");
	// ����������
	{
		fprintf(file, "PROXY:\n");
		typesPROXYgens_t::const_iterator it = typesPROXYgens.begin();
		for( ;it != typesPROXYgens.end(); it++)
		{
			it->first.dump(file); fprintf(file, " ");
			it->second.dump(file); fprintf(file, "\n");
		}
	}
	{
		fprintf(file, "FILTERS:\n");
		typesFilters_t::const_iterator it = typesFilters.begin();
		for( ;it != typesFilters.end(); it++)
		{
			it->first.dump(file); fprintf(file, " ");
			const std::vector<GeneratorNHisObject>& list = it->second;
			for(i=0; i<(int)list.size(); i++)
				list[i].dump(file), fprintf(file, "\n");
		}
	}
	{
		fprintf(file, "GEOMETRY:\n");
		typesGEOMgens_t::const_iterator it = typesGEOMgens.begin();
		for( ;it != typesGEOMgens.end(); it++)
		{
			it->first.dump(file); fprintf(file, " ");
			it->second.dump(file); fprintf(file, "\n");
		}
	}
	{
		fprintf(file, "LIGHT:\n");
		typesLIGHTgens_t::const_iterator it = typesLIGHTgens.begin();
		for( ;it != typesLIGHTgens.end(); it++)
		{
			it->first.dump(file); fprintf(file, " ");
			it->second.dump(file); fprintf(file, "\n");
		}
	}
	{
		fprintf(file, "NODE:\n");
		typesNODEgens_t::const_iterator it = typesNODEgens.begin();
		for( ;it != typesNODEgens.end(); it++)
		{
			it->first.dump(file); fprintf(file, " ");
			const nodegenlist_t& list = it->second;
			nodegenlist_t::const_iterator itt = list.begin();
			for( ; itt != list.end(); itt++)
				itt->dump(file), fprintf(file, "\n");
		}
	}
	{
		fprintf(file, "ATTRIBUTE:\n");
		typesATTRgens_t::const_iterator it = typesATTRgens.begin();
		for( ;it != typesATTRgens.end(); it++)
		{
			it->first.dump(file); fprintf(file, " ");
			const std::vector<GeneratorNHisObject>& list = it->second;
			for(i=0; i<(int)list.size(); i++)
				list[i].dump(file), fprintf(file, "\n");
		}
	}
	{
		fprintf(file, "SHADER:\n");
		typesSHADgens_t::const_iterator it = typesSHADgens.begin();
		for( ;it != typesSHADgens.end(); it++)
		{
			it->first.dump(file); fprintf(file, " ");
			const std::vector<GeneratorNHisObject>& list = it->second;
			for(i=0; i<(int)list.size(); i++)
				list[i].dump(file), fprintf(file, "\n");
		}
	}
	{
		fprintf(file, "TRANSFORM:\n");
		typesTRANgens_t::const_iterator it = typesTRANgens.begin();
		for( ;it != typesTRANgens.end(); it++)
		{
			it->first.dump(file); fprintf(file, " ");
			const std::vector<GeneratorNHisObject>& list = it->second;
			for(i=0; i<(int)list.size(); i++)
				list[i].dump(file), fprintf(file, "\n");
		}
	}
	{
		fprintf(file, "DEFORM:\n");
		typesDEFORMgens_t::const_iterator it = typesDEFORMgens.begin();
		for( ;it != typesDEFORMgens.end(); it++)
		{
			it->first.dump(file); fprintf(file, " ");
			const std::vector<GeneratorNHisObject>& list = it->second;
			for(i=0; i<(int)list.size(); i++)
				list[i].dump(file), fprintf(file, "\n");
		}
	}
	{
		fprintf(file, "PASS:\n");
		typesPASSgens_t::const_iterator it = typesPASSgens.begin();
		for( ;it != typesPASSgens.end(); it++)
		{
			it->first.dump(file); fprintf(file, " ");
			it->second.dump(file); fprintf(file, "\n");
		}
	}
	{
		fprintf(file, "SHADING NODE:\n");
		typesSHADINGNODEgens_t::const_iterator it = typesSHADINGNODEgens.begin();
		for( ;it != typesSHADINGNODEgens.end(); it++)
		{
			it->first.dump(file); fprintf(file, " ");
			const std::vector<GeneratorNHisObject>& list = it->second;
			for(i=0; i<(int)list.size(); i++)
				list[i].dump(file), fprintf(file, "\n");
		}
	}
	{
		fprintf(file, "BLUR:\n");
		typesBLURgens_t::const_iterator it = typesBLURgens.begin();
		for( ;it != typesBLURgens.end(); it++)
		{
			it->first.dump(file); fprintf(file, " ");
			it->second.dump(file); fprintf(file, "\n");
		}
	}
	fflush(file);
}

void ChiefGenerator::addGenerator( type_t type, cls::IGenerator* gen)
{
	switch(gen->GetType())
	{
	case cls::IGenerator::ATTRIBUTE:
		addGeneratorX(type, (cls::IAttributeGenerator*)gen);
		break;
	case cls::IGenerator::DEFORM:
		addGeneratorX(type, (cls::IDeformGenerator*)gen);
		break;
	case cls::IGenerator::GEOMETRY:
		addGeneratorX(type, (cls::IGeometryGenerator*)gen);
		break;
	case cls::IGenerator::LIGHT:
		addGeneratorX(type, (cls::ILightGenerator*)gen);
		break;
	case cls::IGenerator::LAYER:
//		addGeneratorX(type, (cls::IGeometryGenerator*)gen);
		break;
	case cls::IGenerator::NODEFILTER:
		addGeneratorX(type, (cls::INodeFilter*)gen);
		break;
	case cls::IGenerator::NODE:
		addGeneratorX(type, (cls::INodeGenerator*)gen);
		break;
	case cls::IGenerator::PASS:
		addGeneratorX(type, (cls::IPassGenerator*)gen);
		break;
	case cls::IGenerator::SHADER:
		addGeneratorX(type, (cls::IShaderGenerator*)gen);
		break;
	case cls::IGenerator::TRANSFORM:
		addGeneratorX(type, (cls::ITransformGenerator*)gen);
		break;
	case cls::IGenerator::PROXY:
		addGeneratorX(type, (cls::IProxyGenerator*)gen);
		break;
	case cls::IGenerator::SHADINGNODE:
		addGeneratorX(type, (cls::IShadingNodeGenerator*)gen);
		break;
	case cls::IGenerator::BLUR:
		addGeneratorX(type, (cls::IBlurGenerator*)gen);
		break;
	}
}
bool ChiefGenerator::addGenerator( type_t type, const char* generator)
{
	if( externgendlls.find(generator)==externgendlls.end())
	{
		Util::DllProcedure dll;
		if( !dll.LoadFormated(generator, "OcellarisExport"))
		{
			displayString("ChiefGenerator::addGenerator. Cant load generator \"%s\"", generator);
			return false;
		}
		externgendlls[generator] = dll;
	}

	Util::DllProcedure& dllproc = externgendlls[generator];
	cls::GENERATOR_CREATOR pc = (cls::GENERATOR_CREATOR)dllproc.proc;
	cls::IGenerator* gen = (*pc)();

	this->addGenerator(type, gen);
	return true;
}

// ��������� �-��� ������ ��������� �� ����� � ����
cls::IGenerator* ChiefGenerator::loadGenerator(const char* generator, cls::IGenerator::enGenType type, cls::IExportContext* context)
{
	cls::IGenerator* gen = 0;
	if( generator && generator[0])
	{
		if( externgendlls.find(generator)==externgendlls.end())
		{
			Util::DllProcedure dll;
			if( !dll.LoadFormated(generator, "OcellarisExport"))
			{
				context->error("ChiefGenerator::loadGenerator: Cant load generator \"%s\"", generator);
				return NULL;
			}
			externgendlls[generator] = dll;
		}
		Util::DllProcedure& dllproc = externgendlls[generator];
		cls::GENERATOR_CREATOR pc = (cls::GENERATOR_CREATOR)dllproc.proc;
		gen = (*pc)();
		if( gen->GetType()!=type) 
		{
			gen = NULL;
			context->error("ExportContextImpl::loadGenerator: generator %s has not type %d", generator, type);
		}
	}
	return gen;
}

void ChiefGenerator::addGeneratorX( type_t type, cls::IProxyGenerator* gen)
{
	generators.insert(gen);
	typesPROXYgens[type] = gen;
}
void ChiefGenerator::addGeneratorX(type_t type, cls::IPassGenerator* gen)
{
	generators.insert(gen);
	typesPASSgens[type] = gen;
}

void ChiefGenerator::addGeneratorX(type_t type, cls::INodeFilter* gen)
{
	generators.insert(gen);
	typesFilters[type].push_back(gen);
}
void ChiefGenerator::addGeneratorX(type_t type, cls::ITransformGenerator* gen)
{
	generators.insert(gen);
	typesTRANgens[type].push_back(gen);
}
void ChiefGenerator::addGeneratorX(type_t type, cls::INodeGenerator* gen)
{
	generators.insert(gen);
	typesNODEgens[type].insert(gen);
}
void ChiefGenerator::addGeneratorX(type_t type, cls::IGeometryGenerator* gen)
{
	generators.insert(gen);
	typesGEOMgens[type] = gen;
}
void ChiefGenerator::addGeneratorX(type_t type, cls::ILightGenerator* gen)
{
	generators.insert(gen);
	typesLIGHTgens[type] = gen;
}
void ChiefGenerator::addGeneratorX(type_t type, cls::IAttributeGenerator* gen)
{
	generators.insert(gen);
	typesATTRgens[type].push_back(gen);
}
void ChiefGenerator::addGeneratorX(type_t type, cls::IShaderGenerator* gen)
{
	generators.insert(gen);
	typesSHADgens[type].push_back(gen);
}
void ChiefGenerator::addGeneratorX(type_t type, cls::IDeformGenerator* gen)
{
	generators.insert(gen);
	typesDEFORMgens[type].push_back(gen);
}
void ChiefGenerator::addGeneratorX( type_t type, cls::IShadingNodeGenerator* gen)
{
	generators.insert(gen);
	typesSHADINGNODEgens[type].push_back(gen);
}
void ChiefGenerator::addGeneratorX(type_t type, cls::IBlurGenerator* gen)
{
	generators.insert(gen);
	typesBLURgens[type] = gen;
}




// ����������� ����������
void ChiefGenerator::prepareGenerators( cls::IExportContext* context)
{
	generators_t::iterator it = generators.begin();
	for( ; it!=generators.end(); it++)
	{
		(*it)->OnPrepare(context);
	}
}


/*/
/// ????????????????????????
///
extern std::string workspacedirectory;
extern std::string scenename;
extern std::string objectname;
extern std::string framename;

void ChiefGenerator::startExport()
{
	char buf[512];
	GetCurrentDirectory(512, buf);
	currentDirectory = buf;

	MString workspacedir;
	MGlobal::executeCommand("workspace -fn", workspacedir);
	SetCurrentDirectory(workspacedir.asChar());

	{
		workspacedirectory = workspacedir.asChar();
		Util::pushBackSlash(workspacedirectory);
		Util::changeSlashToSlash(workspacedirectory);
	}

	{
		MGlobal::executeCommand("file -q -sn -shn", workspacedir);
		scenename = workspacedir.asChar();
		Util::changeSlashToSlash(scenename);
	}
	
	{
		MTime t = MAnimControl::currentTime();
		int frame = (int)t.as(MTime::uiUnit());
		char buf[24];
		itoa(frame, buf, 10);
		framename = buf;
	}

	generators_t::iterator it = generators.begin();
	for( ; it!=generators.end(); it++)
	{
		(*it)->OnPrepare(cls::IExportContext* context);
	} 
}
void ChiefGenerator::endExport()
{
	SetCurrentDirectory(currentDirectory.c_str());
}
/*/

ChiefGenerator::ChiefGenerator()
{
}
ChiefGenerator::~ChiefGenerator()
{
}
