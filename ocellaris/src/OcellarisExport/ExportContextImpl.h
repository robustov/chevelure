#pragma once

#include <string.h>
#include "ocellaris/OcellarisExport.h"
#include "ocellaris/ChiefGenerator.h"
#include "NodePool.h"
#include "FramePool.h"
#include "TaskPool.h"

class MayaProgress;
class OCELLARISEXPORT_API ExportContextImpl : public cls::IExportContext
{
// ���������� cls::IExportContext

// ��������� 
// ��� �-��� ���������� �� �����������
public:
	// message
	virtual void printf(const char* text, ...);
	// ������
	virtual void error(const char* text, ...);

// ���������� 
// ���������� ��������
public:
	virtual const char* getEnvironment(
		const char* env
		)const{return getenv(env);}
	// ����������
	virtual void setEnvironment(
		const char* env, 
		const char* value
		){setenv(env, value);};

	// ������ ��������� ����������� 
	// ���������� �� INode::PrepareGenerators
	virtual ChiefGenerator* getGenerators(
		);

	// �������� ������ � �������
	virtual cls::INode* addNode(
		MObject& obj,				//!< ������
		cls::INode* masterpassnode=0
		);
	// �������� ������ ����
	virtual cls::INode* addPass(
		MObject& obj				//!< ������
		);
	// �������� pass � ������ RenderFrame ��� ��������� �����
	virtual void addRenderFramePass(
		cls::INode& node,				//!< ������ pass
		float frametime					//!< � ������ ����� ���������
		);
	// �������� ������ � ������ ExportFrame ��� ��������� RenderFrame
	// ���������� �� ���� �����������:
	virtual void addExportTimeNode(
		cls::INode& node,				//!< ������ 
		float frametime,				//!< � ������ ����� ���������
		float exporttime				//!< ������ ����� ������ ���� ���������� �������
		);
	// ������ ��� � ���� ���������� ��� �������� ���� � ������ exporttime
	// ������������ � Node::BuildFrame 
	virtual bool getNodeExportFrameCache(
		float exporttime, 
		cls::INode& node,				// ������
		cls::IRender* &pcache,
		Math::Box3f* &pbox
		);
// ���������� �� pass �����������:
public:
	// ������ ������� ����� ������� �����������
	// ������� ������ � ������ do export - ����� ������� OnExport � pass
	virtual float getCurrentFrame(
		);


// ���������� �� �����������:
public:
	// ������ ����� ���������� �����
	virtual void getGlobalLights(
		std::vector<cls::INode* >& lights
		);
	// ������� ���� �������� ��� �������
	// ���������� �� �����������
	virtual cls::INode* getNodeForObject(
		MObject& obj
		);
	// ������� ��������� ��� �������� ���� ��������
	// ���������� ��� ���������� �������
	virtual cls::IShadingNodeGenerator* getShadingNodeGenerator(
		MObject& obj
		);
	// ��������� ������, �������� � �������� ��������
	// �� ������ � IGenerator::PrepareNode
	// �� ������ � �����������
	virtual cls::IRender* objectContext(
		MObject& obj,				// ����� ���� INode* ???
		cls::IGenerator* generator
		);

// ���������� ��������:
public:
	// �������� ������
	virtual cls::ITask* addTask(
		cls::ITask* parent			// �.�. NULL - ����� ����������� � ����� ������
		);
	// ������ ����������� ����� ����� �������
	virtual cls::ITask* getPreTask(
		);
	// ����� ������ �� �����
	virtual cls::ITask* findTask(
		const char* taskname, 
		bool bTopLevelOnly=true
		);
	// ���������� ����� ����� ��������
	virtual bool addRelation(
		cls::ITask* task, 
		cls::ITask* dependon 
		);

public:
	ExportContextImpl();
	~ExportContextImpl();
	// ������ env �� ������� � �����
	void clear();
	// �������� � env 
	bool setenv(const char* env, const char* val);
	const char* getenv(const char* env) const;

// ����������
	// ������ ����������� �� ���������
	bool addDefaultGenerators(const char* options);

	// �������� ����������� ��������� ��� ������� ����
	bool addGeneratorForType(const char* type, const char* generator);

	// ��������� �-��� ������ ��������� �� ����� � ����
	cls::IGenerator* loadGenerator(const char* generator, cls::IGenerator::enGenType type);

	// ����������� ����������
	bool prepareGenerators();


	// ������ ���, �������� � �����������
	cls::INode* addnodes(const char* root, const char* options);
	cls::INode* addnodes(MDagPath& path, const char* options);

	// ������ �������
	int addpass(const char* passnode, const char* options);
	int addpass(cls::IPassGenerator* passgen, const char* options);
	// ����� ��������
	int connectPass(const char* mainpass, const char* subpass, const char* options);

	// ��
	int addlight(const char* lightnode, const char* options);

	// �������� ���� � ��������� ������� ����� (��� ���� ���������� �� ���������)
	int addframe(double frame);

	// ���������� � �������� (�����)
	// ��������� export frames
	int preparenodes();

	// ���������� �������
	int doexport(const char* dumpoption, const char* options, MayaProgress* progress);

	// �������� �� ������
	int render(const char* renderoption);

	// ������ ��� fastdiststart 
	Alfred::Job& getFastDistStart(){return fastdiststart_alfredjob;};

	// dump
	void dump(const char* options) const;

protected:
	// env ����������
	std::map<std::string, std::string> envs;

	// ����������
	ChiefGenerator chiefgenerator;

	// ������� RenderFrame ��� ��������� �������
	void ExportRenderFrameForPass()
	{

	}
	// ����������� ����� ������ ����������� � ��������� ������
	void rec_AssembleNodeTree(
		cls::INode& pass, 
		cls::IRender* render, 
		Math::Box3f& scenebox,
		float frame, 
		const Math::Matrix4f& transform,    
		std::tree<cls::INode*>::iterator begin, 
		std::tree<cls::INode*>::iterator end,
		FILE* dumpfile
		);


	// ���� ���
	cls::NodePool nodepool;

	// ���� ������ � ��������� ����� ��������
	cls::FramePool framepool;

	// ���� �����
	cls::TaskPool taskpool;

	// ��������� ������, �������� � �������� ��������
	// ��. objectContext()
	struct ExportDataKey
	{
		void* objectid;
		cls::IGenerator* gen;
		ExportDataKey(void* objectid=0, cls::IGenerator* gen=0){this->objectid=objectid;this->gen = gen;}
		bool operator<(const ExportDataKey& arg) const 
		{
			if(this->objectid < arg.objectid)
				return true;
			if(this->objectid > arg.objectid)
				return false;
			return this->gen < arg.gen;
		}
	};
	std::map<ExportDataKey, cls::renderCacheImpl<> > exportdata;

	// ��� ������ ��������� getCurrentFrame
	// ������� ����� ������� �����������
	// ������� ������ � ������ do export - ����� ������� OnExport � pass
	float currentExportFrame;

	// ��� �������� ����������� ������� �������
	bool bValidGenerators;
	bool bExportTime;

public:
	// ������ ��� fastdiststart 
	Alfred::Job fastdiststart_alfredjob;
	std::string fastdiststart_preflight;
	std::string fastdiststart_postrender;
	std::map<double, std::string> fastdiststart_framefiles;

};
