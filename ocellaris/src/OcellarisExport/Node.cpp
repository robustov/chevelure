#include "stdafx.h"
#include "Node.h"
#include "ExportDeformerProxy.h"

Node::Node()
{
//	geometry = NULL;
	transform = NULL;
}
void Node::set(MDagPath path)
{
	this->path = path;
	this->obj  = path.node();
	this->name = path.fullPathName().asChar();
	this->proxyobj = MObject::kNullObj;
}
void Node::set(MObject obj)
{
	this->path = MDagPath::getAPathTo(obj);
	this->obj  = obj;
	this->name = MFnDependencyNode(obj).name().asChar();
	this->proxyobj = MObject::kNullObj;
}
void Node::addMasterPass(cls::INode* masterpassnode)
{
	this->masterpassnodes.insert(masterpassnode);
}


// ���� �� ����� ���� � ������ ������ ������
bool Node::isMasterPass(cls::INode* passnode) const
{
	// passnode ���� � ������
	if( this->masterpassnodes.find(passnode)!=this->masterpassnodes.end())
		return true;
	// � ������ ���� NULL
	if( this->masterpassnodes.find(0)!=this->masterpassnodes.end())
		return true;
	return false;
}

// �������� ���������
void Node::AddGenerator(cls::IGenerator* gen)
{
	switch(gen->GetType())
	{
	case cls::IGenerator::ATTRIBUTE:
		attrlist.push_back( (cls::IAttributeGenerator*)gen);
		break;
	case cls::IGenerator::DEFORM:
		deformgenlist.push_back( (cls::IDeformGenerator*)gen);
		break;
	case cls::IGenerator::GEOMETRY:
		printf("cant execute Node::AddGenerator for cls::IGenerator::GEOMETRY\n");
//		addGeneratorX(type, (cls::IGeometryGenerator*)gen);
		break;
	case cls::IGenerator::LIGHT:
		printf("cant execute Node::AddGenerator for cls::IGenerator::LIGHT\n");
//		addGeneratorX(type, (cls::IGeometryGenerator*)gen);
		break;
	case cls::IGenerator::LAYER:
		printf("cant execute Node::AddGenerator for cls::IGenerator::LAYER\n");
//		addGeneratorX(type, (cls::IGeometryGenerator*)gen);
		break;
	case cls::IGenerator::NODEFILTER:
		printf("cant execute Node::AddGenerator for cls::IGenerator::NODEFILTER\n");
//		addGeneratorX(type, (cls::INodeFilter*)gen);
		break;
	case cls::IGenerator::NODE:
		nodelist.insert( (cls::INodeGenerator*)gen);
		break;
	case cls::IGenerator::PASS:
		passgen.gen = gen;
		passgen.object = MObject::kNullObj;
		break;
	case cls::IGenerator::SHADER:
		shader.push_back( (cls::IShaderGenerator*)gen);
		break;
	case cls::IGenerator::TRANSFORM:
		transform = (cls::ITransformGenerator*)gen;
		break;
	case cls::IGenerator::PROXY:
		printf("cant execute Node::AddGenerator for cls::IGenerator::PROXY\n");
//		addGeneratorX(type, (cls::IProxyGenerator*)gen);
		break;
	}
}

// ��������� ���������
cls::IGenerator* Node::GetGenerator(cls::IGenerator::enGenType type, MObject& genobject )
{
	switch(type)
	{
	case cls::IGenerator::ATTRIBUTE:
		break;
	case cls::IGenerator::DEFORM:
		break;
	case cls::IGenerator::GEOMETRY:
		break;
	case cls::IGenerator::LIGHT:
		genobject = light.object;
		return light.gen;
	case cls::IGenerator::LAYER:
		break;
	case cls::IGenerator::NODEFILTER:
		break;
	case cls::IGenerator::NODE:
		break;
	case cls::IGenerator::PASS:
		genobject = passgen.object;
		return passgen.gen;
	case cls::IGenerator::SHADER:
		break;
	case cls::IGenerator::TRANSFORM:
		break;
	case cls::IGenerator::PROXY:
		break;
	case cls::IGenerator::SHADINGNODE:
		if( shadingnodes.size()>0)
		{
			genobject = shadingnodes[0].object;
			return shadingnodes[0].gen;
		}
		break;
	case cls::IGenerator::BLUR:
		break;
	}
	return NULL;
}

// ��������� ���� �� �������������� ���� 
// ���������� �� NodePool::addnode
bool Node::IsExported(
	cls::IExportContext* context
	)
{
	for( unsigned i=0; i<this->filterlist.size(); i++)
	{
		ChiefGenerator::GeneratorNHisObject& gno = this->filterlist[i];
		if( !gno.gen) continue;
		cls::INodeFilter* gen = (cls::INodeFilter*)gno.gen;
		bool res = gen->IsRenderNode(*this, NULL, context, gno.object);
		if( !res)
			return false;
	}
	return true;
}

/*/
// ���������� � �������� �����
void Node::AddFrame(
	cls::INode& pass,			// pass
	float frame,				// ����
	cls::IGenerator* passgen,	// ������
	cls::IBlurGenerator* defaultbg,	// ���� ��������� �� ���������
	cls::IExportContext* context	// �������� ��������
	)
{
	// �� �����������: 
	// ����� ���� ���� ���������
	cls::IBlurGenerator* bg = defaultbg;
	MObject blurGenObj = MObject::kNullObj;

	if( !bg)
	{
		// ��� �����
		context->addExportTimeNode(*this, frame, frame);
		return;
	}

	bg->OnAddFrame( pass, frame, *this, context, blurGenObj);
}
/*/

// ����������� ����������
bool Node::PrepareGenerators(
	cls::IExportContext* context
	)
{
	ChiefGenerator* pGenerators = context->getGenerators();
	if(!pGenerators) return false;

	ChiefGenerator& chef = *pGenerators;
	MFn::Type type = this->getPath().apiType();

	// ������ ����������
	for(;;)
	{
		ChiefGenerator::GeneratorNHisObject proxy = chef.getProxy(*this);
		if( !proxy.gen) break;

		cls::IProxyGenerator* gen = (cls::IProxyGenerator*)proxy.gen;
		MObject obj = gen->OnProcess(this, context, proxy.object);
		if(obj.isNull() || obj==this->getObject()) 
			break;
		this->setProxyObject(obj);
	}

// N. �������� passGenerator - ������ ��� �������� - renderpass
	this->passgen = chef.getPass( *this);

// M. �������� shadingNodeGenerator - ������ ��� �������� - Shader
	chef.getShadingNode( *this, this->shadingnodes);
	

// 1. ��� ������� ������ ������ �������� (���������� ������ INodeFilter) � ���� ���� ���� �� ������� 
// INodeFilter::IsRenderNode() ������ false ������ ������ �� ��������������.
	chef.getFilters( *this, this->filterlist);

	// 2. ��������� ITransformGenerator ��������� �������������� � ��������
	// ���� ����� ���� � ���� ���������� ����� ITransformGenerator::OnTransformStart().
	this->transform = chef.getTransform(*this);

	// 3. ��������� ������ ����������� ������� (INodeGenerator)
	// � ������� ���������� ����� INodeGenerator::OnNodeStart().
	chef.getNode(*this, this->nodelist);

	// 4-6. ����� ����������� ���������, ������� � ����������.
	// ���� ��� ������� ������ ��������� ��������� (IGeometryGenerator), 

	// �� ��� ����� ������� ����������� ��������� ���������:
	// - ��������� ��������� ������� (IShaderGenerator) �������������� � �������� 
	// � ���� �� ������ ���������� ����� IShaderGenerator::OnShader(obj, render);
	chef.getAttribute(*this, this->attrlist);
	this->geometry = chef.getGeometry(*this);
	this->light = chef.getLight(*this);

	// �������. ������� OnPrepare
	chef.getShader(*this, this->shader);
	ChiefGenerator::shadergenlist_t::iterator itsh = this->shader.begin();
	for(;itsh != this->shader.end(); itsh++)
	{
		ChiefGenerator::GeneratorNHisObject& gno = (*itsh);
		cls::IShaderGenerator* g = (cls::IShaderGenerator*)gno.gen;
//		g->OnPrepare(*this, context);
	}

	// - ���������� ����� IGeometryGenerator::OnGeometry();
	if( this->geometry.gen)
	{
		// ��� draw ������� (Points, Mesh, Sphere � �.�.)
		// ����� ���������� ����� ������ �����������
		chef.getDeform(*this, this->deformgenlist);
	}
	return true;
}
// Prepare Node �������� OnPrepare � �����������
bool Node::PrepareNode(
	cls::IExportContext* context
	)
{
	bool res;
	unsigned i;
	for( i=0; i<this->filterlist.size(); i++)
	{
		ChiefGenerator::GeneratorNHisObject& gno = this->filterlist[i];
		if( !gno.gen) continue;
		res = gno.gen->OnPrepareNode(*this, context, gno.object);
		if( !res) gno.gen = NULL;
	}

	for( i=0; i<this->attrlist.size(); i++)
	{
		ChiefGenerator::GeneratorNHisObject& gno = this->attrlist[i];
		if( !gno.gen) continue;
		res = gno.gen->OnPrepareNode(*this, context, gno.object);
		if( !res) gno.gen = NULL;
	}

	if( this->transform.gen)
	{
		ChiefGenerator::GeneratorNHisObject& gno = this->transform;
		res = gno.gen->OnPrepareNode(*this, context, gno.object);
		if( !res) gno.gen = NULL;
	}


	for( ChiefGenerator::nodegenlist_t::iterator it = this->nodelist.begin(); it!=this->nodelist.end(); it++)
	{
		// TODO: FIXME: const_cast for set element
		ChiefGenerator::GeneratorNHisObject& gno = const_cast<ChiefGenerator::GeneratorNHisObject&>(*it);
		if( !gno.gen) continue;
		res = gno.gen->OnPrepareNode(*this, context, gno.object);
		if( !res) gno.gen = NULL;
	}

	for( i=0; i<this->shader.size(); i++)
	{
		ChiefGenerator::GeneratorNHisObject& gno = this->shader[i];
		if( !gno.gen) continue;
		res = gno.gen->OnPrepareNode(*this, context, gno.object);
		if( !res) gno.gen = NULL;
	}

	if( this->geometry.gen)
	{
		// ��������� ���������� �������� ������� ����� ���� ����� ��������������
		for( unsigned i=0; i<deformgenlist.size(); i++)
		{
			ChiefGenerator::GeneratorNHisObject& gno = this->deformgenlist[i];
			if( !gno.gen) continue;
			res = gno.gen->OnPrepareNode(*this, context, gno.object);
			if( !res) gno.gen = NULL;
		}
		ChiefGenerator::GeneratorNHisObject& gno = this->geometry;
		if( gno.gen)
		{
			res = gno.gen->OnPrepareNode(*this, context, gno.object);
			if( !res) gno.gen = NULL;
		}
	}

	if( this->light.gen)
	{
		ChiefGenerator::GeneratorNHisObject& gno = this->light;
		if( gno.gen)
		{
			res = gno.gen->OnPrepareNode(*this, context, gno.object);
			if( !res) gno.gen = NULL;
		}
	}

	for( i=0; i<this->shadingnodes.size(); i++)
	{
		ChiefGenerator::GeneratorNHisObject& gno = this->shadingnodes[i];
		if( !gno.gen) continue;
		res = gno.gen->OnPrepareNode(*this, context, gno.object);
		if( !res) gno.gen = NULL;
	}

	if( this->passgen.gen)
	{
		ChiefGenerator::GeneratorNHisObject& gno = this->passgen;
		if( gno.gen)
		{
			res = gno.gen->OnPrepareNode(*this, context, gno.object);
			if( !res) gno.gen = NULL;
		}
	}

	if( this->blurgen.gen)
	{
		ChiefGenerator::GeneratorNHisObject& gno = this->blurgen;
		if( gno.gen)
		{
			res = gno.gen->OnPrepareNode(*this, context, gno.object);
			if( !res) gno.gen = NULL;
		}
	}
	return true;
}
// ����� ����� � ������
bool Node::PushNode(
	cls::IRender* render,				// ������
	Math::Matrix4f& transform,			// ����������� ����� transform
	Math::Box3f& box,					// ����������� ���� ��� ������� (�������������)
	cls::IExportContext* context,		// 
	int whatRender						// ����� �� enWhatRender
	)
{
	bool bCommentGenerators = (whatRender&WR_COMMENTS)!=0;
// �� ����� ������ �������!!!
	if( whatRender&(WR_TRANSFORM|WR_NODE))
	{
		render->PushAttributes();
	}
	if( whatRender&WR_TRANSFORM)
	{
		// 2. ��������� ITransformGenerator ��������� �������������� � ��������
		// ���� ����� ���� � ���� ���������� ����� ITransformGenerator::OnTransformStart().
		if( this->transform.gen)
		{
			cls::ITransformGenerator* gen = (cls::ITransformGenerator*)this->transform.gen;

			if( bCommentGenerators)
				render->comment(gen->GetUniqueName());
			Math::Matrix4f m;
			gen->OnTransformStart(*this, render, transform, m, context, this->transform.object);
			transform = m;
		}
	}
	return true;
}
/*/
// ������� ����
bool Node::BuildFrame(
	cls::INode& pass,				// pass
	cls::IRender* render,			// ������
	float frame,
	cls::IBlurGenerator* defaultbg,	// ���� ��������� �� ���������
	Math::Matrix4f& transform,		// ����������� ����� transform
	Math::Box3f& box,				// ����������� ���� ��� �������
	cls::IExportContext* context	// �������� ��������
	)
{
	cls::IBlurGenerator* bg = defaultbg;
	MObject blurGenObj = MObject::kNullObj;

	if( !bg)
	{
		cls::IRender* pcache=NULL;
		Math::Box3f* pbox = NULL;
		if( !context->getNodeExportFrameCache( frame, *this, pcache, pbox))
		{
			context->error( "getNodeExportFrameCache %s %f FAILED\n", frame, this->getName());
			return false;
		}
		pcache->RenderGlobalAttributes(render);
		pcache->Render(render);
		Math::Box3f wb = *pbox;
		if( wb.isvalid())
		{
			wb.transform(transform);
			box.insert(wb);
		}
		return true;
	}

	return bg->OnBuildBlur( pass, *this, render, transform, box, frame, context, blurGenObj);
}
/*/

// ����� ���� � �� �����
bool Node::PopNode(
	cls::IRender* render,				// ������
	cls::IExportContext* context,
	int whatRender						// ����� �� enWhatRender
	)
{
	bool bCommentGenerators = (whatRender&WR_COMMENTS)!=0;

	if( whatRender&WR_TRANSFORM)
	{
		if( transform.gen)
		{
			cls::ITransformGenerator* gen = (cls::ITransformGenerator*)this->transform.gen;

			if( bCommentGenerators)
				render->comment(gen->GetUniqueName());
			gen->OnTransformEnd(*this, render, context, this->transform.object);
		}
	}

// �� ����� ������ �������!!!
	if( whatRender&(WR_TRANSFORM|WR_NODE))
	{
		render->PopAttributes();
	}
	return true;
}
// �������
bool Node::Export(
	cls::IRender* _render,				// ������
	Math::Box3f& box,					// ����
	cls::IExportContext* context,		// ��������
	int whatRender						// ����� �� enWhatRender
	)
{
	unsigned i;
	bool bCommentGenerators = (whatRender&WR_COMMENTS)!=0;
//	bCommentGenerators = false;

	ExportDeformerProxy proxy(_render, context);
	cls::IRender* render = &proxy;

	if( bCommentGenerators)
	{
		std::string text = "export node ";
		text += this->name;
		render->comment(text.c_str());
	}

	// 1. ��� ������� ������ ������ �������� (���������� ������ INodeFilter) � ���� ���� ���� �� ������� 
	// INodeFilter::IsRenderNode() ������ false ������ ������ �� ��������������.
	bool bVisible = true;
	if( whatRender&WR_FILTERS)
	{
		for( unsigned i=0; i<this->filterlist.size(); i++)
		{
			cls::INodeFilter* gen = (cls::INodeFilter*)this->filterlist[i].gen;
			if( !gen) continue;
			if( bCommentGenerators)
				render->comment(gen->GetUniqueName());
//			bool bFlag = gen->IsRenderNode( *this, render, context, this->filterlist[i].object);
//			if( !bFlag) bVisible = false;

			// ���������� ��� ��������
			gen->OnExport( *this, render, context,	this->filterlist[i].object);
		}
	}
//	if( !bVisible) 
//		return false;


	if( whatRender&WR_ATTR_BEFORE_TRANSFORM)
	{
		// - ��������� ������ ����������� ��������� (IAttributeGenerator) � ��� ������� 
		// ���������� IAttributeGenerator::OnAttribute();
		for( i=0; i<this->attrlist.size(); i++)
		{
			cls::IAttributeGenerator* gen = (cls::IAttributeGenerator*)this->attrlist[i].gen;
			if( !gen) continue;
			MObject generatornode = this->attrlist[i].object;
			if( gen->GetCallType(generatornode)&cls::IAttributeGenerator::CT_BEFORE_TRANSFORM)
			{
				if( bCommentGenerators)
					render->comment(gen->GetUniqueName());
				gen->OnAttribute(*this, render, context, this->attrlist[i].object);
			}
		}
	}
	if( whatRender&WR_TRANSFORM)
	{
		// 2. ��������� ITransformGenerator ��������� �������������� � ��������
		// ���� ����� ���� � ���� ���������� ����� ITransformGenerator::OnTransformStart().
		if( this->transform.gen)
		{
			cls::ITransformGenerator* gen = (cls::ITransformGenerator*)this->transform.gen;

			if( bCommentGenerators)
				render->comment(gen->GetUniqueName());
//			Math::Matrix4f m;
			gen->OnTransform(*this, render, context, this->transform.object);
//			trans = m;
		}
	}

	// - ��������� ������ ����������� ��������� (IAttributeGenerator) � ��� ������� 
	// ���������� IAttributeGenerator::OnAttribute();
	if( whatRender&WR_ATTR_BEFORE_NODE)
	{
		for( i=0; i<this->attrlist.size(); i++)
		{
			cls::IAttributeGenerator* gen = (cls::IAttributeGenerator*)this->attrlist[i].gen;
			if( !gen) continue;
			MObject generatornode = this->attrlist[i].object;
			if( gen->GetCallType(generatornode)&cls::IAttributeGenerator::CT_BEFORE_NODE)
			{
				if( bCommentGenerators)
					render->comment(gen->GetUniqueName());
				gen->OnAttribute(*this, render, context, this->attrlist[i].object);
			}
		}
	}

	if( whatRender&WR_NODE)
	{
		// 3. ��������� ������ ����������� ������� (INodeGenerator)
		// � ������� ���������� ����� INodeGenerator::OnNodeStart().
		for( ChiefGenerator::nodegenlist_t::iterator it = this->nodelist.begin(); it!=this->nodelist.end(); it++)
		{
			// TODO: FIXME: const_cast for set element
			ChiefGenerator::GeneratorNHisObject& gno = const_cast<ChiefGenerator::GeneratorNHisObject&>(*it);
			cls::INodeGenerator* gen = (cls::INodeGenerator*)gno.gen;
			if( !gen) continue;
			if( bCommentGenerators)
				render->comment(gen->GetUniqueName());
			gen->OnNodeStart(*this, render, context, gno.object);
		}
	}

	// 4-6. ����� ����������� ���������, ������� � ����������.
	// ���� ��� ������� ������ ��������� ��������� (IGeometryGenerator), 

	// �� ��� ����� ������� ����������� ��������� ���������:
	// - ��������� ��������� ������� (IShaderGenerator) �������������� � �������� 
	// � ���� �� ������ ���������� ����� IShaderGenerator::OnShader(obj, render);
//	if( this->geometry)
//	{
	if( whatRender&WR_SHADER)
	{
		if( bCommentGenerators)
			render->comment("Surface shaders:");
		// surface
		for( int s=0; s<(int)this->shader.size(); s++)
		{
			ChiefGenerator::GeneratorNHisObject& gno = this->shader[s];
			cls::IShaderGenerator* g = (cls::IShaderGenerator*)gno.gen;
			if( !g) continue;

			render->PushAttributes();

			// - ��������� ������ ����������� ��������� (IAttributeGenerator) � ��� ������� 
			// ���������� IAttributeGenerator::OnAttribute();
			if( whatRender&WR_ATTR_BEFORE_SHADER)
			{
				for( i=0; i<this->attrlist.size(); i++)
				{
					cls::IAttributeGenerator* gen = (cls::IAttributeGenerator*)this->attrlist[i].gen;
					if( !gen) continue;
					MObject generatornode = this->attrlist[i].object;
					if( gen->GetCallType(generatornode)&cls::IAttributeGenerator::CT_BEFORE_SHADER)
					{
						if( bCommentGenerators)
							render->comment(gen->GetUniqueName());
						gen->OnAttribute(*this, render, context, this->attrlist[i].object);
					}
				}
			}
			if( bCommentGenerators)
				render->comment(g->GetUniqueName());

			proxy.currentNode = this;
			proxy.deformgenlist = this->deformgenlist;		// ��� ����������!!!
			g->OnSurface(*this, render, context, gno.object);
			proxy.deformgenlist.clear();			// ��� ����������!!!

			render->PopAttributes();
		}
		// displacement
		if( bCommentGenerators)
			render->comment("Displacement shaders:");
		for( int s=0; s<(int)this->shader.size(); s++)
		{
			ChiefGenerator::GeneratorNHisObject& gno = this->shader[s];
			cls::IShaderGenerator* g = (cls::IShaderGenerator*)gno.gen;
			if( !g) continue;

			render->PushAttributes();

			// - ��������� ������ ����������� ��������� (IAttributeGenerator) � ��� ������� 
			// ���������� IAttributeGenerator::OnAttribute();
			if( whatRender&WR_ATTR_BEFORE_SHADER)
			{
				for( i=0; i<this->attrlist.size(); i++)
				{
					cls::IAttributeGenerator* gen = (cls::IAttributeGenerator*)this->attrlist[i].gen;
					if( !gen) continue;
					MObject generatornode = this->attrlist[i].object;
					if( gen->GetCallType(generatornode)&cls::IAttributeGenerator::CT_BEFORE_SHADER)
					{
						if( bCommentGenerators)
							render->comment(gen->GetUniqueName());
						gen->OnAttribute(*this, render, context, this->attrlist[i].object);
					}
				}
			}
			if( bCommentGenerators)
				render->comment(g->GetUniqueName());

			proxy.currentNode = this;
			proxy.deformgenlist = this->deformgenlist;		// ��� ����������!!!
			g->OnDisplacement(*this, render, context, gno.object);
			proxy.deformgenlist.clear();			// ��� ����������!!!

			render->PopAttributes();
		}
	}

	if( whatRender&WR_ATTR_BEFORE_GEOMETRY)
	{
		// - ��������� ������ ����������� ��������� (IAttributeGenerator) � ��� ������� 
		// ���������� IAttributeGenerator::OnAttribute();
		for( i=0; i<this->attrlist.size(); i++)
		{
			cls::IAttributeGenerator* gen = (cls::IAttributeGenerator*)this->attrlist[i].gen;
			if( !gen) continue;
			MObject generatornode = this->attrlist[i].object;
			if( gen->GetCallType(generatornode)&cls::IAttributeGenerator::CT_BEFORE_GEOMETRY)
			{
				if( bCommentGenerators)
					render->comment(gen->GetUniqueName());
				gen->OnAttribute(*this, render, context, this->attrlist[i].object);
			}
		}
	}

	// - ���������� ����� IGeometryGenerator::OnGeometry();
	if( whatRender&WR_GEOMETRY)
	{
		if( this->geometry.gen)
		{
			cls::IGeometryGenerator* gen = (cls::IGeometryGenerator*)this->geometry.gen;
			// ��� draw ������� (Points, Mesh, Sphere � �.�.)
			// ����� ���������� ����� ������ �����������
			proxy.currentNode = this;
			proxy.deformgenlist = this->deformgenlist;		// ��� ����������!!!
			
			Math::Matrix4f m = Math::Matrix4f::id;
			Math::Box3f lb;

			MObject lastobject = this->getObject();
			// ��������� ���������� �������� ������� ����� ���� ����� ��������������
			for( unsigned i=0; i<deformgenlist.size(); i++)
			{
				cls::IDeformGenerator* gen = (cls::IDeformGenerator*)deformgenlist[i].gen;
				if( bCommentGenerators)
					render->comment(gen->GetUniqueName());
				MObject geomobject = this->getObject();
				gen->OnSetupNode(*this, context, deformgenlist[i].object);
			}

			if( bCommentGenerators)
				render->comment(gen->GetUniqueName());
			gen->OnGeometry(*this, render, lb, context, this->geometry.object);
			if( lb.isvalid())
			{
	//			if( render) render->Parameter("TEMP::Box", lb);
				lb.transform(m);
				box.insert(lb);
			}

			proxy.deformgenlist.clear();			// ��� ����������!!!

			// ������� ������ ������
			this->setProxyObject( lastobject);
		}
	}

	if( whatRender&WR_CHILDS)
	{
		/*/
		// childs
		if( bCommentGenerators)
		{
			std::string text = "childs for ";
			text += this->name;
			render->comment(text.c_str());
		}
		std::list<Node>::iterator it = childs.begin();
		for( ; it!=childs.end() ; it++)
		{
			it->Render(_render, level+1, box, trans, context, whatRender);
		}
		/*/
	}

	if( whatRender&WR_NODE)
	{
		for( ChiefGenerator::nodegenlist_t::iterator it = this->nodelist.begin(); it!=this->nodelist.end(); it++)
		{
			// TODO: FIXME: const_cast for set element
			ChiefGenerator::GeneratorNHisObject& gno = const_cast<ChiefGenerator::GeneratorNHisObject&>(*it);
			cls::INodeGenerator* g = (cls::INodeGenerator*)gno.gen;
			if( !g) continue;
			if( bCommentGenerators)
				render->comment(g->GetUniqueName());
			g->OnNodeEnd(*this, render, context, gno.object);
		}
	}
	return true;
}

// ������� ����������
void Node::Dump(FILE* file)const
{
	MObject obj = this->getPath().node();
	MTypeId typeId = MFnDependencyNode(obj).typeId();

	fprintf( file, "NODE: %s id = %d\n", path.fullPathName().asChar(), typeId.id());

	// 1. ��� ������� ������ ������ �������� (���������� ������ INodeFilter) � ���� ���� ���� �� ������� 
	// INodeFilter::IsRenderNode() ������ false ������ ������ �� ��������������.
	unsigned i;
	for( i=0; i<this->filterlist.size(); i++)
	{
		cls::INodeFilter* gen = (cls::INodeFilter*)this->filterlist[i].gen;
		if( !gen) continue;
		fprintf( file, "FILTER GENERATOR: %s\n", gen->GetUniqueName());
	}

	for( i=0; i<this->attrlist.size(); i++)
	{
		cls::IAttributeGenerator* gen = (cls::IAttributeGenerator*)this->attrlist[i].gen;
		if( !gen) continue;
		MObject generatornode = this->attrlist[i].object;
		if(gen->GetCallType(generatornode)&cls::IAttributeGenerator::CT_BEFORE_TRANSFORM)
			fprintf( file, "ATTRIBUTE GENERATOR: %s\n", gen->GetUniqueName());
	}

	// 2. ��������� ITransformGenerator ��������� �������������� � ��������
	// ���� ����� ���� � ���� ���������� ����� ITransformGenerator::OnTransformStart().
	if( this->transform.gen)
		fprintf( file, "TRANSFORM GENERATOR: %s\n", transform.gen->GetUniqueName());

	for( i=0; i<this->attrlist.size(); i++)
	{
		cls::IAttributeGenerator* gen = (cls::IAttributeGenerator*)this->attrlist[i].gen;
		if( !gen) continue;
		MObject generatornode = this->attrlist[i].object;
		if(gen->GetCallType(generatornode)&cls::IAttributeGenerator::CT_BEFORE_NODE)
			fprintf( file, "ATTRIBUTE GENERATOR: %s\n", gen->GetUniqueName());
	}

	{
		// 3. ��������� ������ ����������� ������� (INodeGenerator)
		// � ������� ���������� ����� INodeGenerator::OnNodeStart().
		for( ChiefGenerator::nodegenlist_t::const_iterator it = this->nodelist.begin(); it!=this->nodelist.end(); it++)
		{
			const ChiefGenerator::GeneratorNHisObject& gno = *it;
			cls::INodeGenerator* g = (cls::INodeGenerator*)gno.gen;
			if( !g) continue;
			fprintf( file, "NODE GENERATOR: %s\n", g->GetUniqueName());
		}
	}

	for( i=0; i<this->attrlist.size(); i++)
	{
		cls::IAttributeGenerator* gen = (cls::IAttributeGenerator*)this->attrlist[i].gen;
		if( !gen) continue;
		MObject generatornode = this->attrlist[i].object;
		if(gen->GetCallType(generatornode)&cls::IAttributeGenerator::CT_BEFORE_SHADER)
			fprintf( file, "ATTRIBUTE GENERATOR: %s\n", gen->GetUniqueName());
	}

	// 4-6. ����� ����������� ���������, ������� � ����������.
	// ���� ��� ������� ������ ��������� ��������� (IGeometryGenerator), 

	// �� ��� ����� ������� ����������� ��������� ���������:
	// - ��������� ��������� ������� (IShaderGenerator) �������������� � �������� 
	// � ���� �� ������ ���������� ����� IShaderGenerator::OnShader(obj, render);
	for( i=0; i<this->shader.size(); i++)
	{
		if( !this->shader[i].gen) continue;
		fprintf( file, "SHADER GENERATOR: %s\n", this->shader[i].gen->GetUniqueName());
	}

	for( i=0; i<this->attrlist.size(); i++)
	{
		cls::IAttributeGenerator* gen = (cls::IAttributeGenerator*)this->attrlist[i].gen;
		if( !gen) continue;
		MObject generatornode = this->attrlist[i].object;
		if(gen->GetCallType(generatornode)&cls::IAttributeGenerator::CT_BEFORE_GEOMETRY)
			fprintf( file, "ATTRIBUTE GENERATOR: %s\n", gen->GetUniqueName());
	}

	// - ���������� ����� IGeometryGenerator::OnGeometry();
	if( this->geometry.gen)
	{
		// ��������� ���������� �������� ������� ����� ���� ����� ��������������
		for( unsigned i=0; i<deformgenlist.size(); i++)
		{
			if( !deformgenlist[i].gen) continue;
			fprintf( file, "DEFORM GENERATOR: %s\n", deformgenlist[i].gen->GetUniqueName());
		}
		fprintf( file, "GEOMETRY GENERATOR: %s\n", geometry.gen->GetUniqueName());
	}
	if( this->light.gen)
	{
		fprintf( file, "LIGHT GENERATOR: %s\n", light.gen->GetUniqueName());
	}
	

	for( i=0; i<this->shadingnodes.size(); i++)
	{
		cls::IShadingNodeGenerator* gen = (cls::IShadingNodeGenerator*)this->shadingnodes[i].gen;
		if( !gen) continue;
		MObject generatornode = this->shadingnodes[i].object;
		fprintf( file, "SHADING NODE GENERATOR: %s\n", gen->GetUniqueName());
	}

	if( this->passgen.gen)
	{
		fprintf( file, "PASS GENERATOR: %s\n", this->passgen.gen->GetUniqueName());
	}

	if( this->blurgen.gen)
	{
		fprintf( file, "PASS GENERATOR: %s\n", this->blurgen.gen->GetUniqueName());
	}

	/*/
	fprintf( file, "\n");
	// childs
	std::list<Node>::const_iterator it = childs.begin();
	for( ; it!=childs.end() ; it++)
	{
		it->Dump(file);
	}
	/*/
}

/*/
// ����� �������
void Node::Render(
	cls::IRender* _render, 
	int level,
	Math::Box3f& box, 
	Math::Matrix4f trans, 
	cls::IExportContext* context, 
	int whatRender					// ����� �� enWhatRender
	)
{
	unsigned i;
	bool bCommentGenerators = (whatRender&WR_COMMENTS)!=0;
//	bCommentGenerators = false;

	ExportDeformerProxy proxy(_render, context);
	cls::IRender* render = &proxy;

	if( bCommentGenerators)
	{
		std::string text = "export node ";
		text += this->name;
		render->comment(text.c_str());
	}

	// 1. ��� ������� ������ ������ �������� (���������� ������ INodeFilter) � ���� ���� ���� �� ������� 
	// INodeFilter::IsRenderNode() ������ false ������ ������ �� ��������������.
	bool bVisible = true;
	if( whatRender&WR_FILTERS)
	{
		for( unsigned i=0; i<this->filterlist.size(); i++)
		{
			cls::INodeFilter* gen = (cls::INodeFilter*)this->filterlist[i].gen;
			if( bCommentGenerators)
				render->comment(gen->GetUniqueName());
			bool bFlag = gen->IsRenderNode( *this, render, context, this->filterlist[i].object);
			if( !bFlag) bVisible = false;
		}
	}
	if( !bVisible) 
		return;

// �� ����� ������ �������!!!
	if( whatRender&(WR_TRANSFORM|WR_NODE))
	{
		render->PushAttributes();
	}

	if( whatRender&WR_ATTR_BEFORE_TRANSFORM)
	{
		// - ��������� ������ ����������� ��������� (IAttributeGenerator) � ��� ������� 
		// ���������� IAttributeGenerator::OnAttribute();
		for( i=0; i<this->attrlist.size(); i++)
		{
			cls::IAttributeGenerator* gen = (cls::IAttributeGenerator*)this->attrlist[i].gen;
			MObject generatornode = this->attrlist[i].object;
			if( gen->GetCallType(generatornode)&cls::IAttributeGenerator::CT_BEFORE_TRANSFORM)
			{
				if( bCommentGenerators)
					render->comment(gen->GetUniqueName());
				gen->OnAttribute(*this, render, context, this->attrlist[i].object);
			}
		}
	}

	if( whatRender&WR_TRANSFORM)
	{
		// 2. ��������� ITransformGenerator ��������� �������������� � ��������
		// ���� ����� ���� � ���� ���������� ����� ITransformGenerator::OnTransformStart().
		if( this->transform.gen)
		{
			cls::ITransformGenerator* gen = (cls::ITransformGenerator*)this->transform.gen;

			if( bCommentGenerators)
				render->comment(gen->GetUniqueName());
			Math::Matrix4f m;
			gen->OnTransformStart(*this, render, level, trans, m, this->transform.object);
			trans = m;
		}
	}

	// - ��������� ������ ����������� ��������� (IAttributeGenerator) � ��� ������� 
	// ���������� IAttributeGenerator::OnAttribute();
	if( whatRender&WR_ATTR_BEFORE_NODE)
	{
		for( i=0; i<this->attrlist.size(); i++)
		{
			cls::IAttributeGenerator* gen = (cls::IAttributeGenerator*)this->attrlist[i].gen;
			MObject generatornode = this->attrlist[i].object;
			if( gen->GetCallType(generatornode)&cls::IAttributeGenerator::CT_BEFORE_NODE)
			{
				if( bCommentGenerators)
					render->comment(gen->GetUniqueName());
				gen->OnAttribute(*this, render, context, this->attrlist[i].object);
			}
		}
	}

	if( whatRender&WR_NODE)
	{
		// 3. ��������� ������ ����������� ������� (INodeGenerator)
		// � ������� ���������� ����� INodeGenerator::OnNodeStart().
		for( ChiefGenerator::nodegenlist_t::iterator it = this->nodelist.begin(); it!=this->nodelist.end(); it++)
		{
			ChiefGenerator::GeneratorNHisObject& gno = *it;
			cls::INodeGenerator* g = (cls::INodeGenerator*)gno.gen;
			if( bCommentGenerators)
				render->comment(g->GetUniqueName());
			g->OnNodeStart(*this, render, context, gno.object);
		}
	}

	// 4-6. ����� ����������� ���������, ������� � ����������.
	// ���� ��� ������� ������ ��������� ��������� (IGeometryGenerator), 

	// �� ��� ����� ������� ����������� ��������� ���������:
	// - ��������� ��������� ������� (IShaderGenerator) �������������� � �������� 
	// � ���� �� ������ ���������� ����� IShaderGenerator::OnShader(obj, render);
//	if( this->geometry)
//	{
	if( whatRender&WR_SHADER)
	{
		// surface
		for( int s=0; s<(int)this->shader.size(); s++)
		{
			render->PushAttributes();

			ChiefGenerator::GeneratorNHisObject& gno = this->shader[s];
			cls::IShaderGenerator* g = (cls::IShaderGenerator*)gno.gen;

			// - ��������� ������ ����������� ��������� (IAttributeGenerator) � ��� ������� 
			// ���������� IAttributeGenerator::OnAttribute();
			if( whatRender&WR_ATTR_BEFORE_SHADER)
			{
				for( i=0; i<this->attrlist.size(); i++)
				{
					cls::IAttributeGenerator* gen = (cls::IAttributeGenerator*)this->attrlist[i].gen;
					MObject generatornode = this->attrlist[i].object;
					if( gen->GetCallType(generatornode)&cls::IAttributeGenerator::CT_BEFORE_SHADER)
					{
						if( bCommentGenerators)
							render->comment(gen->GetUniqueName());
						gen->OnAttribute(*this, render, context, this->attrlist[i].object);
					}
				}
			}
			if( bCommentGenerators)
				render->comment(g->GetUniqueName());
			g->OnSurface(*this, render, context, gno.object);

			render->PopAttributes();
		}
		// displacement
		for( int s=0; s<(int)this->shader.size(); s++)
		{
			render->PushAttributes();

			ChiefGenerator::GeneratorNHisObject& gno = this->shader[s];
			cls::IShaderGenerator* g = (cls::IShaderGenerator*)gno.gen;

			// - ��������� ������ ����������� ��������� (IAttributeGenerator) � ��� ������� 
			// ���������� IAttributeGenerator::OnAttribute();
			if( whatRender&WR_ATTR_BEFORE_SHADER)
			{
				for( i=0; i<this->attrlist.size(); i++)
				{
					cls::IAttributeGenerator* gen = (cls::IAttributeGenerator*)this->attrlist[i].gen;
					MObject generatornode = this->attrlist[i].object;
					if( gen->GetCallType(generatornode)&cls::IAttributeGenerator::CT_BEFORE_SHADER)
					{
						if( bCommentGenerators)
							render->comment(gen->GetUniqueName());
						gen->OnAttribute(*this, render, context, this->attrlist[i].object);
					}
				}
			}
			if( bCommentGenerators)
				render->comment(g->GetUniqueName());
			g->OnDisplacement(*this, render, context, gno.object);

			render->PopAttributes();
		}
	}

	if( whatRender&WR_ATTR_BEFORE_GEOMETRY)
	{
		// - ��������� ������ ����������� ��������� (IAttributeGenerator) � ��� ������� 
		// ���������� IAttributeGenerator::OnAttribute();
		for( i=0; i<this->attrlist.size(); i++)
		{
			cls::IAttributeGenerator* gen = (cls::IAttributeGenerator*)this->attrlist[i].gen;
			MObject generatornode = this->attrlist[i].object;
			if( gen->GetCallType(generatornode)&cls::IAttributeGenerator::CT_BEFORE_GEOMETRY)
			{
				if( bCommentGenerators)
					render->comment(gen->GetUniqueName());
				gen->OnAttribute(*this, render, context, this->attrlist[i].object);
			}
		}
	}

	// - ���������� ����� IGeometryGenerator::OnGeometry();
	if( whatRender&WR_GEOMETRY)
	{
		if( this->geometry.gen)
		{
			cls::IGeometryGenerator* gen = (cls::IGeometryGenerator*)this->geometry.gen;
			// ��� draw ������� (Points, Mesh, Sphere � �.�.)
			// ����� ���������� ����� ������ �����������
			proxy.currentNode = this;
			proxy.deformgenlist = this->deformgenlist;		// ��� ����������!!!
			
			Math::Matrix4f m = trans;
			Math::Box3f lb;

			MObject lastobject = this->getObject();
			// ��������� ���������� �������� ������� ����� ���� ����� ��������������
			for( unsigned i=0; i<deformgenlist.size(); i++)
			{
				cls::IDeformGenerator* gen = (cls::IDeformGenerator*)deformgenlist[i].gen;
				if( bCommentGenerators)
					render->comment(gen->GetUniqueName());
				MObject geomobject = this->getObject();
				gen->OnSetupNode(*this, context, deformgenlist[i].object);
			}

			if( bCommentGenerators)
				render->comment(gen->GetUniqueName());
			gen->OnGeometry(*this, render, lb, context, this->geometry.object);
			if( lb.isvalid())
			{
	//			if( render) render->Parameter("TEMP::Box", lb);
				lb.transform(m);
				box.insert(lb);
			}

			proxy.deformgenlist.clear();			// ��� ����������!!!

			// ������� ������ ������
			this->setProxyObject( lastobject);
		}
	}

	if( whatRender&WR_CHILDS)
	{
		// childs
		if( bCommentGenerators)
		{
			std::string text = "childs for ";
			text += this->name;
			render->comment(text.c_str());
		}
		std::list<Node>::iterator it = childs.begin();
		for( ; it!=childs.end() ; it++)
		{
			it->Render(_render, level+1, box, trans, context, whatRender);
		}
	}

	if( whatRender&WR_NODE)
	{
		for( ChiefGenerator::nodegenlist_t::iterator it = this->nodelist.begin(); it!=this->nodelist.end(); it++)
		{
			ChiefGenerator::GeneratorNHisObject& gno = *it;
			cls::INodeGenerator* g = (cls::INodeGenerator*)gno.gen;
			if( bCommentGenerators)
				render->comment(g->GetUniqueName());
			g->OnNodeEnd(*this, render, context, gno.object);
		}
	}
	if( whatRender&WR_TRANSFORM)
	{
		if( transform.gen)
		{
			cls::ITransformGenerator* gen = (cls::ITransformGenerator*)this->transform.gen;

			if( bCommentGenerators)
				render->comment(gen->GetUniqueName());
			gen->OnTransformEnd(*this, render, level, this->transform.object);
		}
	}

// �� ����� ������ �������!!!
	if( whatRender&(WR_TRANSFORM|WR_NODE))
	{
		render->PopAttributes();
	}
}
/*/














/*/
//////////////////////////////////////////////////////////////
// 
// ��� ���� �� ������������
// 

void Node::PrepareFrame(
	float t, 
	float shutterOpenFrame, 
	float shutterCloseFrame,
	std::map<float, std::set<float> >& used_timestamps	// ���������
	)
{
	// childs
	std::list<Node>::iterator it = childs.begin();
	for( ; it!=childs.end() ; it++)
	{
		it->PrepareFrame(t, shutterOpenFrame, shutterCloseFrame, used_timestamps);
	}

	// ���� ��������
	if( shutterOpenFrame==shutterCloseFrame)
	{
		timestamp2frame[ t].push_back( t);
		used_timestamps[ t].insert( t);
		return;
	}

	MObject attr = ocExport_AddAttrType(obj, "ocellarisBlurSamples", cls::PT_INT, cls::P<int>(2));
	cls::P<int> bsp = ocExport_GetAttrValue( obj, attr);
	int bs = 2;
	if(!bsp.empty()) bs = bsp.empty();

	// ���������
	if(bs<2)
	{
		timestamp2frame[ t].push_back( t);
		used_timestamps[ t].insert( t);
		return;
	}

	{
		timestamp2frame[ shutterOpenFrame].push_back( t);
		used_timestamps[ shutterOpenFrame].insert( t);

		for(int i=1; i<bs-1; i++)
		{
			float p = (float)i/(bs-1);
			float xt = shutterOpenFrame*(1-p) + shutterCloseFrame*p;

			timestamp2frame[xt].push_back( t);
			used_timestamps[xt].insert( t);
		}

		timestamp2frame[ shutterCloseFrame].push_back( t);
		used_timestamps[ shutterCloseFrame].insert( t);
	}

}
// ������� �� ���������� ���.
void Node::Export(
	float time, 
	cls::IExportContext* context
	)
{
	int level = 0;
	// �������� ����� ��?
	if( timestamp2frame.find(time)!=timestamp2frame.end()) 
	{
		Math::Matrix4f trans = Math::Matrix4f::id;
		Item& item = caches[time];
		ExportDeformerProxy proxy(&item.cache, context);
		cls::IRender* render = &proxy;

		// 1. ��� ������� ������ ������ �������� (���������� ������ INodeFilter) � ���� ���� ���� �� ������� 
		// INodeFilter::IsRenderNode() ������ false ������ ������ �� ��������������.
		bool bVisible = true;
		for( unsigned i=0; i<this->filterlist.size(); i++)
		{
			cls::INodeFilter* gen = (cls::INodeFilter*)this->filterlist[i].gen;

			bool bFlag = gen->IsRenderNode(*this, render, context, this->filterlist[i].object);
			if( !bFlag) bVisible = false;
		}
		if( !bVisible) 
			return;

		render->Attribute("@visible", bVisible);

		// 2. ��������� ITransformGenerator ��������� �������������� � ��������
		// ���� ����� ���� � ���� ���������� ����� ITransformGenerator::OnTransformStart().
		if( this->transform.gen)
		{
			cls::ITransformGenerator* gen = (cls::ITransformGenerator*)this->transform.gen;
			Math::Matrix4f m;
			gen->OnTransformStart(*this, render, level, trans, m, this->transform.object);
			trans = m;
		}

		{
			// 3. ��������� ������ ����������� ������� (INodeGenerator)
			// � ������� ���������� ����� INodeGenerator::OnNodeStart().
			for( ChiefGenerator::nodegenlist_t::iterator it = this->nodelist.begin(); it!=this->nodelist.end(); it++)
			{
				ChiefGenerator::GeneratorNHisObject& gno = *it;
				cls::INodeGenerator* g = (cls::INodeGenerator*)gno.gen;
				g->OnNodeStart(*this, render, context, gno.object);
			}
		}

		// 4-6. ����� ����������� ���������, ������� � ����������.
		// ���� ��� ������� ������ ��������� ��������� (IGeometryGenerator), 

		// �� ��� ����� ������� ����������� ��������� ���������:
		// - ��������� ��������� ������� (IShaderGenerator) �������������� � �������� 
		// � ���� �� ������ ���������� ����� IShaderGenerator::OnShader(obj, render);
		if( this->geometry.gen)
		{
			for( i=0; i<this->shader.size(); i++)
			{
				render->PushAttributes();
				ChiefGenerator::GeneratorNHisObject& gno = this->shader[i];
				cls::IShaderGenerator* g = (cls::IShaderGenerator*)gno.gen;
				g->OnSurface(*this, render, context, gno.object);
				render->PopAttributes();
			}
		}

		// - ��������� ������ ����������� ��������� (IAttributeGenerator) � ��� ������� 
		// ���������� IAttributeGenerator::OnAttribute();
		for( i=0; i<this->attrlist.size(); i++)
		{
			cls::IAttributeGenerator* gen = (cls::IAttributeGenerator*)this->attrlist[i].gen;
			gen->OnAttribute(*this, render, context, this->attrlist[i].object);
		}

		// - ���������� ����� IGeometryGenerator::OnGeometry();
		if( this->geometry.gen)
		{
			cls::IGeometryGenerator* gen = (cls::IGeometryGenerator*)this->geometry.gen;
			// ��� draw ������� (Points, Mesh, Sphere � �.�.)
			// ����� ���������� ����� ������ �����������
			proxy.currentNode = this;
			
			proxy.deformgenlist = this->deformgenlist;		// ��� ����������!!!
			
			Math::Matrix4f m = trans;
			Math::Box3f lb;
			gen->OnGeometry(*this, render, lb, context, this->geometry.object);
			item.box = lb;
			
			proxy.deformgenlist.clear();			// ��� ����������!!!
		}
		{
			for( ChiefGenerator::nodegenlist_t::iterator it = this->nodelist.begin(); it!=this->nodelist.end(); it++)
			{
				ChiefGenerator::GeneratorNHisObject& gno = *it;
				cls::INodeGenerator* g = (cls::INodeGenerator*)gno.gen;
				g->OnNodeEnd(*this, render, context, gno.object);
			}
		}
		if( transform.gen)
		{
			cls::ITransformGenerator* gen = (cls::ITransformGenerator*)this->transform.gen;
			gen->OnTransformEnd(*this, render, level, this->transform.object);
		}
	}


	// childs
	std::list<Node>::iterator it = childs.begin();
	for( ; it!=childs.end() ; it++)
	{
		it->Export(time, context);
	}
}

void Node::FinalRender(
	int level,
	cls::IRender* render, 
	Math::Box3f& box, 
	Math::Matrix4f trans, 
	cls::IExportContext* context
	)
{
	std::map<float, Item>::iterator itc = caches.begin();
	for(;itc != caches.end(); itc++)
	{
		itc->second.cache.Render(render);
	}

	// childs
	std::list<Node>::iterator it = childs.begin();
	for( ; it!=childs.end() ; it++)
	{
		it->FinalRender(level+1, render, box, trans, context);
	}
}

/*/
