#include "../generators/MeshGenerator.h"
#include "../generators/NuCurveGenerator.h"

#include "../generators/TransformGenerator.h"
#include "../generators/NameAttributeGenerator.h"
#include "../generators/SlimShaderGenerator.h"
#include "../generators/LightGenerator.h"
#include "../generators/ParticleGenerator.h"
#include "../generators/ocInstancerGenerator.h"
#include "../generators/ocGeometryGenerator.h"
#include "../generators/FluidGenerator.h"

#include "../generators/StandartNodeFilter.h"
#pragma once
#include "ocellaris/renderFileImpl.h"
#include "ocellaris/renderProxy.h"
#include "ocellaris/IExportContext.h"
#include <maya/MItDag.h>
#include <maya/MDagPath.h>
#include <mathNmaya/mathNmaya.h>

#include "ocellaris/OcellarisExport.h"
#include "ocellaris/ChiefGenerator.h"

struct ExportDeformerProxy : public cls::renderProxy
{
	cls::INode* currentNode;
	cls::IExportContext* context;
	ChiefGenerator::deformgenlist_t deformgenlist;
public:
	ExportDeformerProxy(
		IRender* render, 
		cls::IExportContext* context
		):cls::renderProxy(render){this->context=context;}

public:
	virtual void RenderCall( 
		const char*	procname			//!< ��� ��������� � DeclareProcBegin
		)
	{
		for( unsigned i=0; i<deformgenlist.size(); i++)
		{
			cls::IDeformGenerator* gen = (cls::IDeformGenerator*)this->deformgenlist[i].gen;
			if( gen->GetDeformCallType(this->deformgenlist[i].object)&cls::IDeformGenerator::CT_BEFORE_GEOMETRY)
				gen->OnGeometry(*currentNode, this, cls::SC_UNKNOWN, this->context, this->deformgenlist[i].object);
		}
		cls::renderProxy::RenderCall(procname);
	}
	virtual void SystemCall(
		cls::enSystemCall call
		)
	{
		for( unsigned i=0; i<deformgenlist.size(); i++)
		{
			cls::IDeformGenerator* gen = (cls::IDeformGenerator*)this->deformgenlist[i].gen;
			if( gen->GetDeformCallType(this->deformgenlist[i].object)&cls::IDeformGenerator::CT_BEFORE_GEOMETRY)
				gen->OnGeometry(*currentNode, this, call, this->context, this->deformgenlist[i].object);
		}
		cls::renderProxy::SystemCall(call);
	}

	virtual void AppendTransform(
		)
	{
		for( unsigned i=0; i<deformgenlist.size(); i++)
		{
			cls::IDeformGenerator* gen = (cls::IDeformGenerator*)this->deformgenlist[i].gen;
			if( gen->GetDeformCallType(this->deformgenlist[i].object)&cls::IDeformGenerator::CT_BEFORE_TRANSFORM)
				gen->OnTransform(*currentNode, this, true, this->context, this->deformgenlist[i].object);
		}
		cls::renderProxy::AppendTransform();
	}
	virtual void SetTransform(
		)
	{
		for( unsigned i=0; i<deformgenlist.size(); i++)
		{
			cls::IDeformGenerator* gen = (cls::IDeformGenerator*)this->deformgenlist[i].gen;
			if( gen->GetDeformCallType(this->deformgenlist[i].object)&cls::IDeformGenerator::CT_BEFORE_TRANSFORM)
				gen->OnTransform(*currentNode, this, false, this->context, this->deformgenlist[i].object);
		}
		cls::renderProxy::SetTransform();
	}

	virtual void Shader(
		const char *shadertype
		)
	{
		for( unsigned i=0; i<deformgenlist.size(); i++)
		{
			cls::IDeformGenerator* gen = (cls::IDeformGenerator*)this->deformgenlist[i].gen;
			if( gen->GetDeformCallType(this->deformgenlist[i].object)&cls::IDeformGenerator::CT_BEFORE_SHADER)
				gen->OnShader(*currentNode, this, shadertype, this->context, this->deformgenlist[i].object);
		}
		cls::renderProxy::Shader(shadertype);
	}

	/*/
	/// Geometry
	/// 0-D prims - point clouds
	virtual void Points (
		cls::PA<Math::Vec3f>& P			//!< points
		)
	{
		for( unsigned i=0; i<deformgenlist.size(); i++)
			deformgenlist[i]->OnDeformation(currentNode, this, cls::I_POINTS);

		cls::renderProxy::Points(P);
	}
	/// 1-D prims - lines, curves, hair
	virtual void Curves (
		const char *interp, 
		const char *wrap,		//!< wrap
		cls::PA<int>& nverts			//!< int[ncurves] ����� ��������� � ������
		)
	{
		for( unsigned i=0; i<deformgenlist.size(); i++)
			deformgenlist[i]->OnDeformation(currentNode, this, cls::I_CURVES);

		cls::renderProxy::Curves(interp, wrap, nverts);
	}
	virtual void Curves (int ncurves, int nvertspercurve, int order,
						const float *knot, float vmin, float vmax) 
	{
		for( unsigned i=0; i<deformgenlist.size(); i++)
			deformgenlist[i]->OnDeformation(currentNode, this, cls::I_CURVES);

		cls::renderProxy::Curves( ncurves, nvertspercurve, order, knot, vmin, vmax);
	}
	/// Mesh 
	virtual void Mesh(
		const char *interp,		//!< ��� ������������
		cls::PA<int>& loops,			//!< loops per polygon
		cls::PA<int>& nverts,		//!< ����� ��������� � �������� (size = polygoncount)
		cls::PA<int>& verts			//!< ������� ��������� ��������� (size = facevertcount)
		)
	{
		for( unsigned i=0; i<deformgenlist.size(); i++)
			deformgenlist[i]->OnDeformation(currentNode, this, cls::I_MESH);

		cls::renderProxy::Mesh(interp, loops, nverts, verts);
	}
	/// Sphere
	virtual void Sphere(
		float radius, float zmin, float zmax,
		float thetamax=360)
	{
		for( unsigned i=0; i<deformgenlist.size(); i++)
			deformgenlist[i]->OnDeformation(currentNode, this, cls::I_SPHERE);

		cls::renderProxy::Sphere(radius, zmin, zmax, thetamax);
	}
	/// Blobby
	virtual void Blobby(
		cls::PA<Math::Matrix4f>& ellipsoids)
	{
		for( unsigned i=0; i<deformgenlist.size(); i++)
			deformgenlist[i]->OnDeformation(currentNode, this, cls::I_BLOBBY);

		cls::renderProxy::Blobby(ellipsoids);
	}
	/*/
};
