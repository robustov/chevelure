#pragma once
#include "../generators/MeshGenerator.h"
#include "../generators/NuCurveGenerator.h"

#include "../generators/TransformGenerator.h"
#include "../generators/NameAttributeGenerator.h"
#include "../generators/SlimShaderGenerator.h"
#include "../generators/LightGenerator.h"
#include "../generators/ParticleGenerator.h"
#include "../generators/ocInstancerGenerator.h"
#include "../generators/ocGeometryGenerator.h"
#include "../generators/FluidGenerator.h"

#include "../generators/StandartNodeFilter.h"
#include "ocellaris/renderFileImpl.h"
#include "ocellaris/renderProxy.h"
#include "ocellaris/INode.h"
#include <maya/MItDag.h>
#include <maya/MDagPath.h>
#include <mathNmaya/mathNmaya.h>

#include "ocellaris/OcellarisExport.h"
#include "ocellaris/ChiefGenerator.h"
#include "ocellaris\renderCacheImpl.h"

//! Node ���������� cls::INode 
//! ������ ������ ������ ���!
//! ���� ������ �� ������ � ���� (��� ��������� Instance) �� ���� �������� �������� ���� ��� � ExportContext
// 
struct OCELLARISEXPORT_API Node : public cls::INode
{
	Node();
	void set(MDagPath path);
	void set(MObject obj);
	void addMasterPass(cls::INode* masterpassnode);

	// ��� 
	const char* getName() const 
	{
		return name.c_str();
	}
	// ���� �� ����� ���� � ������ ������ ������
	virtual bool isMasterPass(cls::INode*) const;

public:
	// �������� ���������
	virtual void AddGenerator(cls::IGenerator* gen);

	// ��������� ���������
	virtual cls::IGenerator* GetGenerator(
		cls::IGenerator::enGenType type, 
		MObject& genobject);

	// ��������� ���� �� �������������� ���� 
	// ���������� �� NodePool::addnode
	virtual bool IsExported(
		cls::IExportContext* context
		);

	/*/
	// ���������� � �������� �����
	// ���������� � IBlurGenerator� 
	// ��� � ����� �������� � ������ context->addExportTime
	virtual void AddFrame(
		cls::INode& pass,			// pass
		float frame,					// ����
		cls::IGenerator* passgen,		// ������
		cls::IBlurGenerator* defaultbg,	// ���� ��������� �� ���������
		cls::IExportContext* context	// �������� ��������
		);
	/*/
	// ����������� ����������
	// ���������� ������-����������
	// ���������� �� chef ����������� ���������� � ������������ �� �����
	bool PrepareGenerators(
		cls::IExportContext* context	// �������� ��������
		);
	// Prepare Node �������� OnPrepare � �����������
	bool PrepareNode(
		cls::IExportContext* context	// �������� ��������
		);
	// �������
	virtual bool Export(
		cls::IRender* render,				// ������
		Math::Box3f& box,					// ���� � ��������� ����������� (�����������!!!)
		cls::IExportContext* context,		// ��������
		int whatRender = WR_ALL				// ����� �� enWhatRender
		);
	// ����� ����� � ������
	virtual bool PushNode(
		cls::IRender* render,			// ������
		Math::Matrix4f& transform,		// ����������� ����� transform
		Math::Box3f& box,				// ����������� ���� ��� ������� (�������������)
		cls::IExportContext* context,	// �������� ��������
		int whatRender = WR_ALL			// ����� �� enWhatRender
		);
/*/
	// ������� ����
	virtual bool BuildFrame(
		cls::INode& pass,				// pass
		cls::IRender* render,			// ������
		float frame,
		cls::IBlurGenerator* defaultbg,	// ���� ��������� �� ���������
		Math::Matrix4f& transform,		// ����������� ����� transform
		Math::Box3f& box,				// ����������� ���� ��� �������
		cls::IExportContext* context	// �������� ��������
		);
/*/
	// ����� ���� � �� �����
	virtual bool PopNode(
		cls::IRender* render,				// ������
		cls::IExportContext* context,
		int whatRender = WR_ALL				// ����� �� enWhatRender
		);
	// ������� ����������
	void Dump(
		FILE* file
		) const;



	// ��������!!!
	// ���������������� ������
	void Render(
		cls::IRender* render, 
		Math::Box3f& box, 
		Math::Matrix4f trans, 
		cls::IExportContext* context,
		int whatRender = WR_ALL					// ����� �� enWhatRender
		)
	{
		context->error("Node::Render not implemented!!!\n");
	};


protected:
	// 1. ��� ������� ������ ������ �������� (���������� ������ INodeFilter) � ���� ���� ���� �� ������� 
	// INodeFilter::IsRenderNode() ������ false ������ ������ �� ��������������.
	ChiefGenerator::nodefilterlist_t filterlist;

	// 2. ��������� ITransformGenerator ��������� �������������� � ��������
	// ���� ����� ���� � ���� ���������� ����� ITransformGenerator::OnTransformStart().
	ChiefGenerator::GeneratorNHisObject transform;

	// 3. ��������� ������ ����������� ������� (INodeGenerator)
	// � ������� ���������� ����� INodeGenerator::OnNodeStart().
	ChiefGenerator::nodegenlist_t nodelist;

	// 4-6. ����� ����������� ���������, ������� � ����������.
	// ���� ��� ������� ������ ��������� ��������� (IGeometryGenerator), 

	// �� ��� ����� ������� ����������� ��������� ���������:
	// - ��������� ��������� ������� (IShaderGenerator) �������������� � �������� 
	// � ���� �� ������ ���������� ����� IShaderGenerator::OnShader(obj, render);
	ChiefGenerator::attrgenlist_t attrlist;
	ChiefGenerator::GeneratorNHisObject geometry;
	ChiefGenerator::GeneratorNHisObject light;
	ChiefGenerator::shadergenlist_t shader;

	// ���������
	ChiefGenerator::deformgenlist_t deformgenlist;

	// blurGenerator 
	ChiefGenerator::GeneratorNHisObject blurgen;

	// N. �������� passGenerator - ������ ��� �������� - renderpass
	ChiefGenerator::GeneratorNHisObject passgen;

	// M. �������� shadingNodeGenerator - ������ ��� ����������� ��������
	ChiefGenerator::shadingnodegenlist_t shadingnodes;

protected:
	// ������
	MDagPath path;
	MObject obj, proxyobj;
	std::string name;

	// ��������� �������
	std::set<cls::INode*> masterpassnodes;

public:
	virtual MDagPath getPath() const
	{
		return path;
	}
	virtual MObject getObject() const
	{
		if( this->proxyobj != MObject::kNullObj)
			return proxyobj;
		return obj;
	}
	void setProxyObject(MObject obj)
	{
		this->proxyobj = obj;
	}
public:
	virtual void Release()
	{
		delete this;
	}
};
