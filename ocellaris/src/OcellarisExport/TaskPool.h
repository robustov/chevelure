#pragma once

#include "ocellaris/IOcellaris.h"
#include "ocellaris/ITask.h"
#include "ocellaris/renderCacheImpl.h"
#include "Alfred/Alfred.h"

namespace cls
{
//! @ingroup plugin_group
//! \brief �������� ����� ���������� � ������
//! 
//! ����������� ���� ����� ����������
//! 
//! 
struct TaskPool
{
public:
	struct Task : public cls::ITask
	{
		Task();
		/// �������� � �������� ����
		virtual void Parameter(
			const char *name, Param& t);
		// �������� ��������
		virtual Param GetParameter(
			const char *name);

	public:
		// ��������� alfred ������
		Alfred::Task* buildAlfredTask();
	
		// ������� ��� �������
		std::string getCmd();

	public:
		cls::renderCacheImpl<> cache;
		//
		Alfred::Task alfredtask;
	};
public:
	TaskPool();
	~TaskPool();

	// ��������
	void clear();

	// �������� ������
	cls::ITask* newTask(
		cls::ITask* parent
		);
	// ����� ������ �� �����
	cls::ITask* findTask(
		const char* taskname, 
		bool bTopLevelOnly=true
		);

	// ������� ������
	void addRelation(
		cls::ITask* task,			// ������
		cls::ITask* dependon,		// �� ����� �������
		cls::IExportContext* context
		);
	// ������� ������������� ������	
	bool buildAlfredJob(
		const char* jobname, 	
		const char* filename
		);
	// ������� bat ���� 
	bool buildTaskCmd(
		const char* cmdfilename,
		const char* path_env,
		bool bPause
		);
	// ����� ������ � �������� ������ getNewTasks ���������� � ExportContextImpl::doexport
	void getNewTasks(
		std::list<cls::ITask*>& newtasks
		);
public:
	// ������ �����
	std::list<Task> tasks;
	// ���� ����� (�� ����� ����� ������� Task)
	std::map<cls::ITask*, std::list<cls::ITask*> > taskgraph;
	// ������ �����
	std::tree<Task*> tasktree;
	// ����� ������
	std::list<cls::ITask*> newtasks;

protected:
	// ������� ������������� ������	
	Alfred::Task* buildAlfredJob_rec(
		Task* task,  
		std::tree<Task*>::const_iterator begin, 
		std::tree<Task*>::const_iterator end
		);
};

}