#ifndef utilsOcs_h
#define utilsOcs_h

color utilsOcs_normalAsColor(normal n)
{
	normal nn = ntransform("world", n);
	nn = normalize(nn);
	return color(xcomp(nn)*0.5 + 0.5, ycomp(nn)*0.5 + 0.5, zcomp(nn)*0.5 + 0.5);
}
color utilsOcs_vectorAsColor(vector n)
{
	vector nn = vtransform("world", n);
	nn = normalize(nn);
	return color(xcomp(nn)*0.5 + 0.5, ycomp(nn)*0.5 + 0.5, zcomp(nn)*0.5 + 0.5);
}
color utilsOcs_floatAsColor(float n)
{
	return color(n, n, n);
}

#endif
