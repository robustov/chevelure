#include "stdafx.h"
#include "ocellaris/OcellarisExport.h"
#include <maya/MFnStringData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFnMessageAttribute.h>
#include "MathNMaya/MathNMaya.h"

std::string workspacedirectory;
std::string scenename;
std::string objectname;
std::string framename;

// ��� ���. ����������
std::string OCELLARISEXPORT_API ocExport_GetCurrentDirectory()
{
	return workspacedirectory;
}
// ����������� �������� ��������
// $O - ��� �������
// $F - ����
// %S - �����
std::string OCELLARISEXPORT_API ocExport_ApplySubParameters(const char* text)
{
	std::string res;
	res = text;
	size_t pos = std::string::npos;
	for( ; (pos = res.find("$O"))!=std::string::npos; )
		res.replace(pos, 2, objectname.c_str());
	for( ; (pos = res.find("$F"))!=std::string::npos; )
		res.replace(pos, 2, framename.c_str());
	for( ; (pos = res.find("$S"))!=std::string::npos; )
		res.replace(pos, 2, scenename.c_str());
	return res;
}

bool ocExport_IsCompatibleType(MObject obj, MObject attr, cls::enParamType type)
{
	if( attr.hasFn(MFn::kNumericAttribute))
	{
		MFnNumericAttribute numAttr(attr);
		MFnNumericData::Type t = numAttr.unitType();
		if( t == MFnNumericData::k3Double)
		{
			if( type==cls::PT_VECTOR || cls::PT_POINT || cls::PT_COLOR)
				return true;
			return false;
		}
		else if( t == MFnNumericData::k3Float)
		{
			if( type==cls::PT_VECTOR || cls::PT_POINT || cls::PT_COLOR)
				return true;
			return false;
		}
		else if( t == MFnNumericData::kInt)
		{
			if( type==cls::PT_INT)
				return true;
			return false;
		}
		else if( t == MFnNumericData::kDouble || t == MFnNumericData::kFloat)
		{
			if( type==cls::PT_FLOAT)
				return true;
			return false;
		}
		else if( t == MFnNumericData::kBoolean)
		{
			if( type==cls::PT_BOOL)
				return true;
			return false;
		}
	}
	else if( attr.hasFn(MFn::kTypedAttribute))
	{
		MFnTypedAttribute typedAttr(attr);
		if( typedAttr.attrType() == MFnData::kString)
		{
			if( type==cls::PT_STRING)
				return true;
			return false;
		}
	}
	else if( attr.hasFn(MFn::kEnumAttribute))
	{
		if( type==cls::PT_INT)
			return true;
		return false;
	}
	else if( attr.hasFn(MFn::kUnitAttribute))
	{
		MFnUnitAttribute unitAttr(attr);
		MFnUnitAttribute::Type unitType = unitAttr.unitType();

		if( type==cls::PT_FLOAT)
			return true;
		return false;
	}
	return false;

}

// ��� �������� � ������� ocellaris
cls::enParamType ocExport_GetAttrType(MObject obj, MObject attr)
{
	if( attr.hasFn(MFn::kNumericAttribute))
	{
		MFnNumericAttribute numAttr(attr);
		MFnNumericData::Type t = numAttr.unitType();
		if( t == MFnNumericData::k3Double)
		{
			return cls::PT_VECTOR;
		}
		else if( t == MFnNumericData::k3Float)
		{
			return cls::PT_COLOR;
		}
		else if( t == MFnNumericData::kInt)
		{
			return cls::PT_INT;
		}
		else if( t == MFnNumericData::kShort)
		{
			return cls::PT_INT;
		}
		else if( t == MFnNumericData::kDouble)
		{
			return cls::PT_FLOAT;
		}
		else if( t == MFnNumericData::kFloat)
		{
			return cls::PT_FLOAT;
		}
		else if( t == MFnNumericData::kBoolean)
		{
			return cls::PT_BOOL;
		}
	}
	else if( attr.hasFn(MFn::kTypedAttribute))
	{
		MFnTypedAttribute typedAttr(attr);
		if( typedAttr.attrType() == MFnData::kString)
		{
			return cls::PT_STRING;
		}
		if( typedAttr.attrType() == MFnData::kMatrix)
		{
			return cls::PT_MATRIX;
		}
	}
	else if( attr.hasFn(MFn::kEnumAttribute))
	{
		return cls::PT_INT;
	}
	return cls::PT_UNKNOWN;
}

// ��� �������� � ������� ocellaris
cls::enParamType ocExport_GetAttrArrayType(MObject obj, MObject attr)
{
	if( attr.hasFn(MFn::kTypedAttribute))
	{
		MFnTypedAttribute typedAttr(attr);
		if( typedAttr.attrType() == MFnData::kDoubleArray)
			return cls::PT_FLOAT;
		if( typedAttr.attrType() == MFnData::kIntArray)
			return cls::PT_INT;
		if( typedAttr.attrType() == MFnData::kPointArray)
			return cls::PT_POINT;
		if( typedAttr.attrType() == MFnData::kVectorArray)
			return cls::PT_VECTOR;
		if( typedAttr.attrType() == MFnData::kStringArray)
			return cls::PT_STRING;
	}
	return cls::PT_UNKNOWN;
}

// �������� �������
MObject ocExport_AddAttrType(MObject obj, const char* name, cls::enParamType type);
MObject ocExport_AddAttrType(MObject obj, const char* name, cls::enParamType type, const cls::Param& defval)
{
	MStatus stat;
	MFnDependencyNode dn(obj);
	MObject attr = dn.attribute(name, &stat);
	if( stat)
	{
		MFnAttribute fnattr(attr);
		fnattr.setHidden(false);
		fnattr.setIndeterminant(false);
		return attr;
	}

	attr = ocExport_AddAttrType(obj, name, type);
	if( attr.isNull()) return attr;

	if( !defval.empty())
	{
		ocExport_SetAttrValue(obj, attr, defval);
	}
	return attr;
}

// �������� �������
MObject ocExport_AddAttrType(MObject obj, const char* name, cls::enParamType type)
{
	MStatus stat;
	MFnDependencyNode dn(obj);
	MObject attr = MObject::kNullObj;
	switch(type)
	{
	case cls::PT_UNKNOWN:
		{
			MFnMessageAttribute mesAttr;
			attr = mesAttr.create(name, name, &stat);
			mesAttr.setConnectable(true);
			mesAttr.setStorable(true);
			mesAttr.setWritable(true);
			mesAttr.setReadable(true);
			mesAttr.setHidden(false);
			mesAttr.setKeyable(false);
			dn.addAttribute(attr);
			return attr;
		}
	case cls::PT_BOOL:
		{
			MFnNumericData::Type dtype = MFnNumericData::kBoolean;
			MFnNumericAttribute numAttr;
			attr = numAttr.create(name, name, dtype, 0, &stat);
			numAttr.setConnectable(true);
			numAttr.setStorable(true);
			numAttr.setWritable(true);
			numAttr.setReadable(true);
			numAttr.setHidden(false);
			numAttr.setKeyable(true);
			dn.addAttribute(attr);
			return attr;
		}
	case cls::PT_INT:
		{
			MFnNumericData::Type dtype = MFnNumericData::kInt;
			MFnNumericAttribute numAttr;
			attr = numAttr.create(name, name, dtype, 0, &stat);
			numAttr.setConnectable(true);
			numAttr.setStorable(true);
			numAttr.setWritable(true);
			numAttr.setReadable(true);
			numAttr.setHidden(false);
			numAttr.setKeyable(true);
			dn.addAttribute(attr);
			return attr;
		}
	case cls::PT_FLOAT:
		{
			MFnNumericData::Type dtype = MFnNumericData::kDouble;
			MFnNumericAttribute numAttr;
			attr = numAttr.create(name, name, dtype, 0, &stat);
			numAttr.setConnectable(true);
			numAttr.setStorable(true);
			numAttr.setWritable(true);
			numAttr.setReadable(true);
			numAttr.setHidden(false);
			numAttr.setKeyable(true);
			dn.addAttribute(attr);
			return attr;
		}
	case cls::PT_VECTOR: case cls::PT_NORMAL: case cls::PT_POINT:
		{
			MFnNumericAttribute numAttr;
			attr = numAttr.create(name, name, MFnNumericData::k3Double, 0, &stat);
			numAttr.setConnectable(true);
			numAttr.setStorable(true);
			numAttr.setWritable(true);
			numAttr.setReadable(true);
			numAttr.setHidden(false);
			numAttr.setKeyable(true);
			dn.addAttribute(attr);
			return attr;
		}
	case cls::PT_COLOR:
		{
			MFnNumericAttribute numAttr;
			attr = numAttr.createColor(name, name, &stat);
			numAttr.setConnectable(true);
			numAttr.setStorable(true);
			numAttr.setWritable(true);
			numAttr.setReadable(true);
			numAttr.setHidden(false);
			numAttr.setKeyable(true);
			dn.addAttribute(attr);
			return attr;
		}
	case cls::PT_STRING:
		{
			MFnTypedAttribute typedAttr;
			MString d = "";
			MFnStringData stringData;
			attr = typedAttr.create( name, name, MFnData::kString, stringData.create(d, &stat), &stat );
			if( !stat) return MObject::kNullObj;
			typedAttr.setConnectable(true);
			typedAttr.setStorable(true);
			typedAttr.setWritable(true);
			typedAttr.setReadable(true);
			typedAttr.setHidden(false);
			typedAttr.setKeyable(true);
			dn.addAttribute(attr);
			return attr;
		}
	}
	return MObject::kNullObj;
}

void ocExport_SetAttrValue(MObject obj, const char* attr, const cls::Param& param)
{
	MFnDependencyNode dn(obj);
	MObject objattr = dn.attribute(attr);
	ocExport_SetAttrValue(obj, objattr, param);
}

// ������ �������� ��������
void ocExport_SetAttrValue(MObject obj, MObject attr, const cls::Param& param)
{
	bool bComp = ocExport_IsCompatibleType(obj, attr, param->type);
	if( !bComp)
		return;
	MPlug plug(obj, attr);
	bool bLock = plug.isLocked();
	if( bLock) plug.setLocked(false);
	switch( param->type)
	{
	case cls::PT_BOOL:
		{
			bool src = (*(bool*)param.data());
			plug.setValue( src);
			break;
		}
	case cls::PT_INT:
		{
			int src = (*(int*)param.data());
			plug.setValue( src);
			break;
		}
	case cls::PT_FLOAT:
		{
			float src = (*(float*)param.data());
			plug.setValue( (double)src);
			break;
		}
	case cls::PT_VECTOR:
	case cls::PT_POINT:
	case cls::PT_COLOR:
		{
			Math::Vec3f& src = *(Math::Vec3f*)param.data();
			MFnNumericData data;
			MObject val = data.create(MFnNumericData::k3Float);
			data.setData(src.x, src.y, src.z);
			plug.setValue( val);
			break;
		}
	case cls::PT_STRING:
		{
			cls::PS ps(param);
			MString val = ps.data();
			plug.setValue( val);
			break;
		}
	}
	if( bLock) plug.setLocked(true);
}

cls::Param ocExport_GetAttrValue(MObject obj, const char* attr)
{
	MFnDependencyNode dn(obj);
	MStatus stat;
	MPlug plug = dn.findPlug(attr, &stat);
	if(!stat) return cls::Param();
	return ocExport_GetAttrValue(obj, plug.attribute());
}
// �������� �������� ��������
cls::Param ocExport_GetAttrValue(MObject obj, MObject attr)
{
	MStatus stat;
	MFnAttribute fnattr(attr);
	MPlug plug(obj, attr);
	return ocExport_GetAttrValue(plug);
}

// �������� �������� ��������
cls::Param ocExport_GetAttrValue(MPlug plug)
{
	MStatus stat;
	MObject attr = plug.attribute();
	MFnAttribute fnattr(attr);
	MObject obj = plug.node();
//	MPlug plug(obj, attr);
//	MString name = fnattr.name();
	cls::enParamType type = ocExport_GetAttrType(obj, attr);
	switch(type)
	{
	case cls::PT_BOOL:
		{
			bool val;
			plug.getValue(val);
			cls::P<bool> dst(val);
			return dst;
		}
	case cls::PT_INT:
		{
			if( attr.hasFn(MFn::kEnumAttribute))
			{
				short val;
				plug.getValue(val);
				cls::P<int> dst((int)val);
				return dst;
			}
			if(attr.hasFn(MFn::kNumericAttribute))
			{
				MFnNumericAttribute numAttr(attr);
				MFnNumericData::Type t = numAttr.unitType();

				if(t==MFnNumericData::kShort)
				{
					short val;
					plug.getValue(val);
					cls::P<int> dst((int)val);
					return dst;
				}
				else
				{
					int val;
					plug.getValue(val);
					cls::P<int> dst(val);
					return dst;
				}
			}
		}
	case cls::PT_FLOAT:
		{
			double val;
//			MPlug plug(obj, attr);
			if( attr.hasFn(MFn::kUnitAttribute))
			{
				MFnUnitAttribute unitAttr(attr);
				MFnUnitAttribute::Type unitType = unitAttr.unitType();
				switch( unitType)
				{
					case MFnUnitAttribute::kAngle: 
						{
							MAngle ang;
							plug.getValue(ang);
							val = ang.asDegrees();
							break;
						}
					case MFnUnitAttribute::kDistance: 
						{
							MDistance ang;
							plug.getValue(ang);
							val = ang.as(MDistance::uiUnit());
							break;
						}
					case MFnUnitAttribute::kTime: 
						{
							MTime ang;
							plug.getValue(ang);
							val = ang.as(MTime::uiUnit());
							break;
						}
				}
			}
			else
			{
				plug.getValue(val);
			}
			cls::P<float> dst((float)val);
			return dst;
		}
	case cls::PT_VECTOR:
	case cls::PT_POINT:
		{
			MObject val;
			plug.getValue(val);
			MFnNumericData data(val, &stat);
			Math::Vec3d v3d;
			stat = data.getData(v3d.x, v3d.y, v3d.z);
			Math::Vec3f v3f((float)v3d.x, (float)v3d.y, (float)v3d.z);
			cls::P<Math::Vec3f> dst(v3f, cls::PT_VECTOR);
			return dst;
		}
	case cls::PT_COLOR:
		{
			MObject val;
			plug.getValue(val);
			MFnNumericData data(val, &stat);
			Math::Vec3f v4d;
			stat = data.getData(v4d.x, v4d.y, v4d.z);
			cls::P<Math::Vec3f> dst(v4d, cls::PT_COLOR);
			return dst;
		}
	case cls::PT_STRING:
		{
			MString str;
//			MPlug plug(obj, attr);
			if( !plug.getValue(str))
				break;
			cls::PS dst(str.asChar());
			return dst;
		}
	case cls::PT_MATRIX:
		{
//			MPlug plug(obj, attr);
			MObject val;
			if( !plug.getValue(val))
				break;
			MFnMatrixData md(val, &stat);
			if( !stat)
				break;
			Math::Matrix4f matr = Math::Matrix4f::id;
			::copy( matr, md.matrix());
			
			cls::P<Math::Matrix4f> dst(matr);
			return dst;
		}

	}
	if( attr.hasFn(MFn::kNumericAttribute))
	{
		MObject val;
		plug.getValue(val);
		MFnNumericData data(val, &stat);
		MFnNumericAttribute numAttr(attr);
		MFnNumericData::Type t = numAttr.unitType();
		if( t == MFnNumericData::k2Double)
		{
			cls::PA<float> v(cls::PI_UNKNOWN, 2);
			double d2[2];
			data.getData(d2[0], d2[1]);
			v[0] = (float)d2[0];
			v[1] = (float)d2[1];
			return v;
		}
		if( t == MFnNumericData::k2Float)
		{
			cls::PA<float> v(cls::PI_UNKNOWN, 2);
			float d2[2];
			data.getData(d2[0], d2[1]);
			v[0] = d2[0];
			v[1] = d2[1];
			return v;
		}
		if( t == MFnNumericData::k2Int)
		{
			cls::PA<int> v(cls::PI_UNKNOWN, 2);
			int d2[2];
			data.getData(d2[0], d2[1]);
			v[0] = d2[0];
			v[1] = d2[1];
			return v;
		}
	}
	if( attr.hasFn(MFn::kUnitAttribute))
	{
//		MPlug plug(obj, attr);
		MFnUnitAttribute unitAttr(attr);
		MFnUnitAttribute::Type t = unitAttr.unitType();
		switch(t)
		{
		case MFnUnitAttribute::kAngle:
			{
			MAngle data;
			plug.getValue(data);
			double val = data.as(MAngle::kDegrees);
			cls::P<float> dst((float)val);
			return dst;
			}
		case MFnUnitAttribute::kDistance:
			{
			MDistance data;
			plug.getValue(data);
			double val = data.as(MDistance::uiUnit());
			cls::P<float> dst((float)val);
			return dst;
			}
		case MFnUnitAttribute::kTime:
			{
			MTime data;
			plug.getValue(data);
			double val = data.as(MTime::uiUnit());
			cls::P<float> dst((float)val);
			return dst;
			}
		}

	}
	return cls::Param();
}
cls::Param ocExport_GetAttrArrayValue(MObject obj, const char* attr)
{
	MFnDependencyNode dn(obj);
	MStatus stat;
	MPlug plug = dn.findPlug(attr, &stat);
	if(!stat) return cls::Param();
	return ocExport_GetAttrArrayValue(obj, plug.attribute());
}

cls::Param ocExport_GetAttrArrayValue(MObject obj, MObject attr)
{
	MStatus stat;
	MFnAttribute fnattr(attr);
	cls::enParamType type = ocExport_GetAttrArrayType(obj, attr);
	switch(type)
	{
	case cls::PT_INT:
		{
			MObject val;
			MPlug(obj, attr).getValue(val);
			MFnIntArrayData arr(val);
			cls::PA<int> dst(cls::PI_UNKNOWN, arr.length());
			for(int i=0; i<dst.size(); i++)
				dst[i] = arr[i];
			return dst;
		}
	case cls::PT_FLOAT:
		{
			MObject val;
			MPlug(obj, attr).getValue(val);
			MFnDoubleArrayData arr(val);
			cls::PA<float> dst(cls::PI_UNKNOWN, arr.length());
			for(int i=0; i<dst.size(); i++)
				dst[i] = (float)arr[i];
			return dst;
		}
	case cls::PT_VECTOR:
	case cls::PT_COLOR:
		{
			MObject val;
			MPlug(obj, attr).getValue(val);
			MFnVectorArrayData arr(val);
			cls::PA<Math::Vec3f> dst(cls::PI_UNKNOWN, arr.length());
			for(int i=0; i<dst.size(); i++)
			{
				dst[i].x = (float)arr[i].x;
				dst[i].y = (float)arr[i].y;
				dst[i].z = (float)arr[i].z;
			}
			return dst;
		}
	case cls::PT_POINT:
		{
			MObject val;
			MPlug(obj, attr).getValue(val);
			MFnPointArrayData arr(val);
			cls::PA<Math::Vec3f> dst(cls::PI_UNKNOWN, arr.length());
			for(int i=0; i<dst.size(); i++)
			{
				dst[i].x = (float)arr[i].x;
				dst[i].y = (float)arr[i].y;
				dst[i].z = (float)arr[i].z;
			}
			return dst;
		}
	case cls::PT_STRING:
		{
			MObject val;
			MPlug(obj, attr).getValue(val);
			MFnStringArrayData arr(val);
			std::vector<std::string> dst(arr.length());
			for(int i=0; i<(int)dst.size(); i++)
			{
				dst[i] = arr[i].asChar();
			}
			return cls::PSA(dst);
		}
	}
	return cls::Param();
}

MObject getExternShaderNode(MObject& obj)
{
	return obj;

	MStatus stat;
	const char* externShaderAttrName = "message";
	MTypeId id( ocellarisTypeIdBase+01 );

	MFnDependencyNode dn(obj);
	MPlug plug = dn.findPlug(externShaderAttrName);
	/*/
	if(plug.isNull())
	{
		MFnNumericData::Type dtype = MFnNumericData::kInt;
		MFnNumericAttribute numAttr;
		MObject attr = numAttr.create(externShaderAttrName, externShaderAttrName, dtype, 0, &stat);
		numAttr.setConnectable(true);
		numAttr.setStorable(true);
		numAttr.setWritable(true);
		numAttr.setReadable(true);
		numAttr.setHidden(true);
		numAttr.setKeyable(false);
		dn.addAttribute(attr);
		plug = MPlug(obj, attr);
	}
	/*/
	MPlugArray pa;
	plug.connectedTo(pa, false, true);
	MObject ext = MObject::kNullObj;
	for(unsigned i=0; i<pa.length(); i++)
	{
		ext = pa[i].node();
		if( MFnDependencyNode(ext).typeId() == id)
			break;
		ext = MObject::kNullObj;
	}

	if( ext == MObject::kNullObj)
	{
/*/
sprintf(buf, "\
{\n\
struct $nameocShaderParameter = `createNode ocShaderParameter`;\n\
connectAttr %s.message $nameocShaderParameter.input;\n\
}", 
/*/
		// ������� � ���������
		MDGModifier dgmod;
		ext = dgmod.createNode(id);
		if( ext.isNull())
			return MObject::kNullObj;
		MPlug dstplug = MFnDependencyNode(ext).findPlug("input");
		dgmod.connect( plug, dstplug);
		if( !dgmod.doIt())
		{
			return MObject::kNullObj;
		}
	}
//	displayStringD( MFnDependencyNode(ext).name().asChar());
	return ext;
}
