#include "stdafx.h"
#include "NodePool.h"

using namespace cls;


NodePool::NodePool()
{
}
NodePool::~NodePool()
{
}

// ��������
void NodePool::clear()
{
	rootlist.clear();
	passtree.clear();
	passconnects.clear();
	lights.clear();
	nodes.clear();
	freenodes.clear();
}

// �������� ���� � ���
cls::INode* NodePool::addnode(
	MObject obj, 
	cls::IExportContext* context, 
	cls::INode* masterpassnode
	)
{
	INode* node = addnodeFromObject(obj, context, masterpassnode);
	return node;
}

// �������� ���� � ���� ��� ����� � ���
cls::INode* NodePool::addnodetree(
	MDagPath path, 
	cls::IExportContext* context, 
	cls::INode* masterpassnode
	)
{
	std::string rootname = path.fullPathName().asChar();
	RootTransform& rt = rootlist[rootname];

	return recAdd( path, 0, rt.nodetree, rt.nodetree.begin(), context, masterpassnode);
}

cls::INode* NodePool::addpass(
	MObject obj, 
	cls::IExportContext* context
	)
{
	if( obj.isNull())
	{
		// ���������� ����
		return 0;
	}

	INode* node = addnodeFromObject(obj, context, 0);
	MObject genobject;
	IGenerator* passgen = node->GetGenerator(IGenerator::PASS, genobject);
	if( !passgen) 
		return 0;

//	if( !passgen->OnPrepareNode(*node, context, genobject))
//		return 0;

	passtree.insert(passtree.end(), node);

	// �� �����������:
	// ������� ��� ��� �������!!!

	return node;
}
cls::INode* NodePool::addpass(
	cls::IPassGenerator* passgen, 
	cls::IExportContext* context
	)
{
	// ������� ������ ����
	freenodes.push_back(Node());
	Node& node = freenodes.back();
	node.AddGenerator(passgen);
	passtree.insert(passtree.end(), &node);
	return &node;
}
// ������� �����
int NodePool::connectPass(
	MObject mainpass, 
	MObject subpass,
	cls::IExportContext* context
	)
{
	if( mainpass.isNull())
		return 0;
	if( subpass.isNull())
		return 0;

	INode* mainnode = nodeFromObject(mainpass, context);
	if( !mainnode) 
		return 0;
	MObject maingenobject;
	IGenerator* mainpassgen = mainnode->GetGenerator(IGenerator::PASS, maingenobject);
	if( !mainpassgen) 
		return 0;

	INode* subnode = nodeFromObject(subpass, context);
	if( !subnode) 
		return 0;
	MObject subgenobject;
	IGenerator* subpassgen = mainnode->GetGenerator(IGenerator::PASS, subgenobject);
	if( !subpassgen) 
		return 0;

	passconnects[mainnode].push_back( subnode);
	return 1;
}

// �������� ��
int NodePool::addlight(
	MObject obj, 
	cls::IExportContext* context
	)
{
	if( obj.isNull())
		return 0;

	INode* node = addnodeFromObject(obj, context, 0);

	lights.push_back(node);

	return 1;
}
/*/
// ���� ��� �������� (�������� � ������ ���� ����������� � ����??? ���)
int NodePool::addframe(
	double frame, 
	cls::IExportContext* context
	)
{
	std::tree<cls::INode*>::const_branch_iterator cbit = passtree.begin();

	for( ; cbit != passtree.end(); cbit++)
	{
		cls::INode* node = *cbit;
		MObject genobject;
		IPassGenerator* passgen = (IPassGenerator*)node->GetGenerator(IGenerator::PASS, genobject);

		// ��������� ��������� �� ������ � ���� �����
		bool bUsed = passgen->IsUsedInFrame(
			*node, (float)frame, context);
		if( !bUsed)
			continue;

		// �������� pass � ������ RenderFrame
		context->addRenderFramePass(*node, (float)frame);

		cls::IBlurGenerator* defaultbg = passgen->GetDefaultBlurGenerator(*node, context);

		// ��������� ��� �������
		std::map<std::string, RootTransform>::const_iterator it = rootlist.begin();
		for( ; it != rootlist.end(); it++)
		{
			const RootTransform& rt = it->second;

			std::tree<cls::INode*>::const_branch_iterator cbit = rt.nodetree.begin();
			for( ; cbit != rt.nodetree.end(); cbit++)
			{
				cls::INode* node = *cbit;

				node->AddFrame(*node, (float)frame, passgen, defaultbg, context);
			}
		}
	}
	return 1;
}
// ������� OnPrepareNode � �����������
int NodePool::prepareNodes(
	cls::IExportContext* context
	)
{
	// ��������� ��� �������
	std::map<std::string, RootTransform>::const_iterator it = rootlist.begin();
	for( ; it != rootlist.end(); it++)
	{
		const RootTransform& rt = it->second;

		std::tree<cls::INode*>::const_branch_iterator cbit = rt.nodetree.begin();
		for( ; cbit != rt.nodetree.end(); cbit++)
		{
			cls::INode* node = *cbit;

			node->PrepareNode(context);
		}
	}
	return 1;
}
/*/

// dump
void NodePool::dump_nodes(FILE* file)const
{
	fprintf(file, "\nDUMP NODES:\n");
	nodes_t::const_iterator it = nodes.begin();
	for( ; it != nodes.end(); it++)
	{
		const Node& node = it->second;
		fprintf(file, "-----------------------------\n");
		fprintf(file, "\"%s\":\n", node.getName());
		node.Dump(file);
	}
/*/
	std::map<std::string, RootTransform>::const_iterator it = rootlist.begin();
	for( ; it != rootlist.end(); it++)
	{
		const RootTransform& rt = it->second;
		fprintf(file, "ROOT \"%s\":\n", it->first.c_str());
		rt.dump(file);
	}
/*/
	fflush(file);
}
// dump
void NodePool::dump_passes(FILE* file)const
{
	fprintf(file, "\nDUMP PASSES:\n");
	std::tree<cls::INode*>::const_branch_iterator cbit = passtree.begin();
	for( ; cbit != passtree.end(); cbit++)
	{
		cls::INode* node = *cbit;
		for( int i=cbit.level(); i>0; i--)
			fputc(' ', file);
		fprintf(file, "\"%s\":\n", node->getName());
		node->Dump(file);
	}
	fflush(file);
}
// dump
void NodePool::dump_lights(FILE* file)const
{
	fprintf(file, "\nDUMP LIGHTS:\n");
	std::list<cls::INode*>::const_iterator cbit = lights.begin();
	for( ; cbit != lights.end(); cbit++)
	{
		cls::INode* node = *cbit;
		fprintf(file, "\"%s\":\n", node->getName());
		node->Dump(file);
	}
	fflush(file);
}

// ������ �����������
void NodePool::RootTransform::dump(FILE* file)const
{
	std::tree<cls::INode*>::const_branch_iterator cbit = nodetree.begin();

	for( ; cbit != nodetree.end(); cbit++)
	{
		cls::INode* node = *cbit;
		for( int i=cbit.level(); i>0; i--)
			fputc(' ', file);
		fprintf(file, "\"%s\":\n", node->getName());
		node->Dump(file);
	}
};

// ������ � ���� ��������:
cls::INode* NodePool::getNode(
	MObject obj, 
	cls::IExportContext* context
	)
{
	return nodeFromObject(obj, context);
}

// ������ ������������ � ������� ���������� ������ ��������
void NodePool::getPassSortList(
	std::vector<cls::INode*>& passes
	)
{
	// ������ passconnects
	std::list<cls::INode*> res;
	std::map< cls::INode*, std::list<cls::INode*> >::iterator it = passconnects.begin();
	for( ; it != passconnects.end(); it++)
	{
		INode* cur = it->first;
		MObject genobject;
		cls::IGenerator* gen = cur->GetGenerator(cls::IGenerator::PASS, genobject);
		if( !gen)
			continue;

		std::list<cls::INode*>& list = it->second;
		// ����� ����� ������ � res
		// ��� ����� ����� �������
		std::list<cls::INode*>::iterator place = std::find(res.begin(), res.end(), cur);
		// ���� �� ����� ��������� � ����� ���� �������
		res.insert(place, list.begin(), list.end());
		// ������ ����
		res.insert(place, cur);
	}
	// �������� �����������
	{
		std::tree<cls::INode*>::branch_iterator it = passtree.begin();
		for(;it != passtree.end(); it++)
		{
			INode* cur = *it;
			MObject genobject;
			cls::IGenerator* gen = cur->GetGenerator(cls::IGenerator::PASS, genobject);
			if( !gen)
				continue;

			std::list<cls::INode*>::iterator place = std::find(res.begin(), res.end(), cur);
			if( place==res.end())
				res.push_back(cur);
		}
	}

	// � ������
	// � ��������� ������������
	{
		passes.clear();
		passes.reserve(res.size());
		std::list<cls::INode*>::iterator it = res.begin();
		for( ; it != res.end(); it++)
		{
			INode* cur = *it;
			MObject genobject;
			cls::IGenerator* gen = cur->GetGenerator(cls::IGenerator::PASS, genobject);
			if( !gen)
				continue;

			std::vector<cls::INode*>::iterator itfnd = std::find(passes.begin(), passes.end(), cur);
			if( itfnd==passes.end())
				passes.push_back(cur);
		}
	}
}
// ������ ������ ����������� ��� ����������
void NodePool::getSubPassSortList(
	cls::INode& node,
	std::vector<cls::INode*>& subpasses
	)
{
	subpasses.clear();
	std::map< cls::INode*, std::list<cls::INode*> >::iterator it = passconnects.find(&node);
	if( it == passconnects.end())
		return;

	std::list<cls::INode*>& list = it->second;
	{
		subpasses.reserve(list.size());
		std::list<cls::INode*>::iterator it = list.begin();
		for( ; it != list.end(); it++)
		{
			cls::INode* node = *it;
			MObject genobject;
			cls::IGenerator* gen = node->GetGenerator(cls::IGenerator::PASS, genobject);
			if( !gen)
				continue;
			subpasses.push_back(node);
		}
	}
//	subpasses.assign(list.begin(), list.end());

	/*/
	std::tree<cls::INode*>::const_branch_iterator cbit = passtree.begin();
	for( ; cbit != passtree.end(); cbit++)
	{
		if(&node == *cbit)
		{
			// ������ ������ ������� � ���������
			rec_getPassSortList(subpasses, cbit.begin(), cbit.end());
			return;
		}
	}
	/*/
}

// ����� �� �����
void NodePool::rec_getPassSortList(
	std::vector<cls::INode*>& passes, 
	std::tree<cls::INode*>::const_iterator begin, 
	std::tree<cls::INode*>::const_iterator end
	)
{
	std::tree<cls::INode*>::const_iterator it = begin;
	for(;it != end; it++)
	{
		// ������� ������
		rec_getPassSortList( passes, it.begin(), it.end());
		// ������ ����, ������ ���� ��� �� ��������!!!
		cls::INode* node = *it;
		int i;
		for(i=0; i<(int)passes.size(); i++)
			if(passes[i]==node) break;
		if( i>=(int)passes.size())
			passes.push_back(node);
	}
}



cls::INode* NodePool::recAdd(
	MDagPath& path, 
	int level, 
	std::tree<cls::INode*>& nodetree, 
	std::tree<cls::INode*>::iterator insertit, 
	cls::IExportContext* context,
	cls::INode* masterpassnode
	)
{
//	MObject obj = path.node();
	

	INode* node = addnodeFromObject(path, context, masterpassnode);
	if(!node) 
		return NULL;

	if( !node->IsExported(context))
		return NULL;

	std::tree<cls::INode*>::iterator itnew = nodetree.insert(insertit, node);
	for( unsigned int i=0; i<path.childCount(); i++)
	{
		MObject obj = path.child(i);

		MDagPath chpath;
		if( !MDagPath::getAPathTo(obj, chpath))
			continue;

		recAdd( chpath, level+1, nodetree, itnew.end(), context, masterpassnode);
	}
	return node;
}

// ���� �� �������
cls::INode* NodePool::nodeFromObject(
	MObject obj, 
	cls::IExportContext* context
	)
{
	void* id = *(void**)&obj;
	nodes_t::iterator it = nodes.find(id);
	if( it==nodes.end())
	{
		return 0;
	}
	else
	{
		return &it->second;
	}
}

cls::INode* NodePool::nodeFromObject(
	MDagPath path,
	cls::IExportContext* context
	)
{
	MObject obj = path.node();
	void* id = *(void**)&obj;
	nodes_t::iterator it = nodes.find(id);
	if( it==nodes.end())
	{
		return 0;
	}
	else
	{
		return &it->second;
	}
}



// ���� �� �������
cls::INode* NodePool::addnodeFromObject(
	MObject obj, 
	cls::IExportContext* context,
	cls::INode* masterpassnode
	)
{
	void* id = *(void**)&obj;
	nodes_t::iterator it = nodes.find(id);
	if( it==nodes.end())
	{
		// �������� ����
		Node& n = nodes[id];
//		n.set(path);
		n.set(obj);
		n.PrepareGenerators(
			context
			);
		n.PrepareNode(
			context
			);
		n.addMasterPass(masterpassnode);
		return &n;
	}
	else
	{
		Node& n = it->second;
		n.addMasterPass(masterpassnode);
		return &n;
	}
}

cls::INode* NodePool::addnodeFromObject(
	MDagPath path,
	cls::IExportContext* context,
	cls::INode* masterpassnode
	)
{
	MObject obj = path.node();
	void* id = *(void**)&obj;
	nodes_t::iterator it = nodes.find(id);
	if( it==nodes.end())
	{
		// �������� ����
		Node& n = nodes[id];
		n.set(path);
		n.PrepareGenerators(
			context
			);
		n.PrepareNode(
			context
			);
		n.addMasterPass(masterpassnode);
		return &n;
	}
	else
	{
		Node& n = it->second;
		n.addMasterPass(masterpassnode);
		return &n;
	}
}
