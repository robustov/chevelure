#pragma once

#include "ocellaris/IRender.h"
#include "ocellaris/IGenerator.h"
#include "ocellaris/INodeGenerator.h"
#include "ocellaris/IAttributeGenerator.h"
#include "ocellaris/IGeometryGenerator.h"
#include "ocellaris/IShaderGenerator.h"
#include "ocellaris/ITransformGenerator.h"
#pragma warning( disable : 4251)
// �������� ����������� 
struct OCELLARISEXPORT_API ChiefGenerator
{
	typedef std::vector<cls::INodeGenerator*> nodegenlist_t;
	typedef std::vector<cls::IAttributeGenerator*> attrgenlist_t;
	typedef std::map<MFn::Type, cls::IGeometryGenerator* >	typesGEOMgens_t;
	typedef std::map<MFn::Type, nodegenlist_t >				typesNODEgens_t;
	typedef std::map<MFn::Type, attrgenlist_t >				typesATTRgens_t;
	typedef std::map<MFn::Type, cls::IShaderGenerator* >	typesSHADgens_t;
	typedef std::map<MFn::Type, cls::ITransformGenerator* >	typesTRANgens_t;
	
	// ����������
	typesGEOMgens_t typesGEOMgens;
	typesNODEgens_t typesNODEgens;
	typesATTRgens_t typesATTRgens;
	typesSHADgens_t typesSHADgens;
	typesTRANgens_t typesTRANgens;
public:
	cls::ITransformGenerator* getTransform(MObject obj);
	cls::IShaderGenerator* getShader(MObject obj);
	cls::IGeometryGenerator* getGeometry(MObject obj);
	void getNode(MObject obj, nodegenlist_t& gens);
	void getAttribute(MObject obj, attrgenlist_t& gens);

	void addGenerator(MFn::Type, cls::ITransformGenerator* gen);
	void addGenerator(MFn::Type, cls::INodeGenerator* gen);
	void addGenerator(MFn::Type, cls::IGeometryGenerator* gen);
	void addGenerator(MFn::Type, cls::IAttributeGenerator* gen);
	void addGenerator(MFn::Type, cls::IShaderGenerator* gen);
};