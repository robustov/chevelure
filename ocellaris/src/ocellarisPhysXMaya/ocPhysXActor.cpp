#include "ocPhysXActor.h"
#include <maya/MFnTransform.h>
#include <maya/MTransformationMatrix.h>
#include <maya/MDagPath.h>
#include <mathNmaya/mathNmaya.h>
#include "ocellaris\ocellarisExport.h"
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MGlobal.h>
#include <maya/M3dView.h>

MObject ocPhysXActor::i_kinematic;

ocPhysXActor::ocPhysXActor()
{
}
ocPhysXActor::~ocPhysXActor()
{
}

MStatus ocPhysXActor::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;

//	if( plug == o_output )
//	{
//		return MS::kSuccess;
//	}
	return MPxTransform::compute(plug, data);
}

void* ocPhysXActor::creator()
{
	return new ocPhysXActor();
}

MStatus ocPhysXActor::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		{
			i_kinematic = numAttr.create ("kinematic","kin", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_kinematic, atInput);
		}
		if( !MGlobal::sourceFile("AEocPhysXActorTemplate.mel"))
		{
			displayString("error source AEocPhysXActorTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}


// ocellarisPhysXMaya.mll@PhysXActor

ocPhysXActor::Generator ocPhysXActor::generator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl PhysXActor()
	{
		return &ocPhysXActor::generator;
	}
}

ocPhysXActor::Generator::Generator()
{
}

//! render ����� ���� = NULL
void ocPhysXActor::Generator::OnTransformStart(
	cls::INode& node, 
	cls::IRender* render, 
	int level, 
	const Math::Matrix4f& curtransform, 
	Math::Matrix4f& newtransform,
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MDagPath path = node.getPath();
	MMatrix tm = path.inclusiveMatrix();
	Math::Matrix4f m;
	copy( m, tm);

	newtransform = m*curtransform;

	ocExport_AddAttrType( node.getObject(), "kinematic", cls::PT_BOOL, cls::P<bool>(false));

	//MString name = path.partialPathName();
	MString name = path.fullPathName();

	if( render)
	{
		std::string globalattr = "ExternTransform::";
		globalattr += name.asChar();

		render->GlobalAttribute(globalattr.c_str(), cls::P<Math::Matrix4f>(m));

		MDagPath cPath;
		M3dView::active3dView().getCamera( cPath );
		MMatrix cm = cPath.inclusiveMatrix();
		Math::Matrix4f mat;
		copy( mat, cm);

		render->GlobalAttribute("PhysX::Camera", cls::P<Math::Matrix4f>(mat));

		MString fullName = path.fullPathName();
		render->Attribute("fullName", fullName.asChar());

		render->PushTransform();
		render->PushAttributes();
		render->Parameter("transform", m);
		render->ParameterFromAttribute("transform", globalattr.c_str());
		render->AppendTransform();
		//render->SetTransform();
		render->PopAttributes();
		render->RenderCall("ocellarisPhysX.dll@BeginActor");
	}
}

void ocPhysXActor::Generator::OnTransformEnd(
	cls::INode& node, 
	cls::IRender* render,				//!< render
	int level,							//!< ������� ��������
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MDagPath path = node.getPath();
	MString name = path.partialPathName();
	if( render)
	{
		std::string globalattr = "ExternTransform::";
		globalattr += name.asChar();
		render->Parameter("externTransformName", globalattr.c_str());

		cls::P<bool> kinematic = false;
		kinematic = ocExport_GetAttrValue( node.getObject(), "kinematic" );
		render->Parameter( "kinematic", kinematic );

		render->RenderCall("ocellarisPhysX.dll@EndActor");
		render->PopTransform();
	}
}