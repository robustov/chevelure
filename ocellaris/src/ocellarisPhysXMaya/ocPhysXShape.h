#pragma once

#include "pre.h"
#include <maya/MPxLocatorNode.h>

#include "ocellaris/IGeometryGenerator.h"
 
class ocPhysXShape : public MPxLocatorNode
{
public:
	ocPhysXShape();
	virtual ~ocPhysXShape(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	virtual void draw(
		M3dView &view, const MDagPath &path, 
		M3dView::DisplayStyle style,
		M3dView::DisplayStatus status);

	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_type;
	static MObject o_output;

	// box
	static MObject i_box[3];
	// capsule
	static MObject i_capsule[2];
	static MObject i_mesh[2];
	static MObject i_flag[2];
	static MObject i_density;

public:
	static MTypeId id;
	static const MString typeName;

	//! @ingroup implement_group
	//! \brief ����� ��� ��� �� �����, �������� ����
	//! 
	struct Generator : public cls::IGeometryGenerator
	{
		// ���������� ���, ������ ��� �������������
		virtual const char* GetUniqueName(
			){return "ocPhysXShapeGenerator";};

		virtual bool OnGeometry(
			cls::INode& node,				//!< ������ 
			cls::IRender* render,			//!< render
			Math::Box3f& box,				//!< box 
			cls::IExportContext* context,	//!< context
			MObject& generatornode			//!< generator node �.� 0
			);
	};

	static Generator generator;
};


