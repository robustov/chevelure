#pragma once

#include "pre.h"
#include <maya/MPxLocatorNode.h>
#include "ocellaris/IGeometryGenerator.h"

 
class ocPhysXJoint : public MPxLocatorNode
{
public:
	ocPhysXJoint();
	virtual ~ocPhysXJoint(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	virtual void draw(
		M3dView &view, const MDagPath &path, 
		M3dView::DisplayStyle style,
		M3dView::DisplayStatus status);

	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_type;
	static MObject i_actor[2];
	static MObject i_axis[3];
	static MObject i_motor[4];
	static MObject i_spring[4];
	static MObject i_limit[3];
	static MObject i_ballancer[4];
	static MObject o_output;

public:
	static MTypeId id;
	static const MString typeName;


	//! @ingroup implement_group
	//! \brief ����� ��� ��� �� �����, �������� ����
	//! 
	struct Generator : public cls::IGeometryGenerator
	{
		// ���������� ���, ������ ��� �������������
		virtual const char* GetUniqueName(
			){return "ocPhysXJointGenerator";};

		virtual bool OnGeometry(
			cls::INode& node,				//!< ������ 
			cls::IRender* render,			//!< render
			Math::Box3f& box,				//!< box 
			cls::IExportContext* context,	//!< context
			MObject& generatornode			//!< generator node �.� 0
			);
	};

	static Generator generator;
};


