#include "pre.h"
#include <maya/MFnPlugin.h>
#include "ocellaris/OcellarisExport.h"
#include "ocPhysXShape.h"
#include "ocPhysXJoint.h"
#include "ocPhysXActor.h"

MStatus uninitializePlugin( MObject obj);
MStatus initializePlugin( MObject obj )
{ 
	MStatus   stat;
	MFnPlugin plugin( obj, "ULITKA", "6.0", "Any");

	if( !MGlobal::executeCommand("loadPlugin -qt ocellarisMaya.mll", true))
	{
		stat.perror("Hair: Error loadPlugin ocellarisMaya.mll");
		uninitializePlugin(obj);
		return stat;
	}

// #include "ocPhysXActor.h"
	stat = plugin.registerTransform( 
		ocPhysXActor::typeName, 
		ocPhysXActor::id, 
		ocPhysXActor::creator, 
		ocPhysXActor::initialize, 
		MPxTransformationMatrix::creator, 
		MPxTransformationMatrix::baseTransformationMatrixId  );
	if (!stat) 
	{
		displayString("cant register %s", ocPhysXActor::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
	registerGenerator(ocPhysXActor::id, &ocPhysXActor::generator);
	

// #include "ocPhysXJoint.h"
	stat = plugin.registerNode( 
		ocPhysXJoint::typeName, 
		ocPhysXJoint::id, 
		ocPhysXJoint::creator,
		ocPhysXJoint::initialize, 
		MPxNode::kLocatorNode );
	if (!stat) 
	{
		displayString("cant register %s", ocPhysXJoint::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
	registerGenerator(ocPhysXJoint::id, &ocPhysXJoint::generator);

// #include "ocPhysXShape.h"
	stat = plugin.registerNode( 
		ocPhysXShape::typeName, 
		ocPhysXShape::id, 
		ocPhysXShape::creator,
		ocPhysXShape::initialize, 
		MPxNode::kLocatorNode );
	if (!stat) 
	{
		displayString("cant register %s", ocPhysXShape::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
	registerGenerator(ocPhysXShape::id, &ocPhysXShape::generator);

	

/*/
	if( !MGlobal::sourceFile("ocellarisPhysXMayaMenu.mel"))
	{
		displayString("error source AEmyCmdTemplate.mel");
	}
	else
	{
		if( !MGlobal::executeCommand("ocellarisPhysXMayaDebugMenu()"))
		{
			displayString("Dont found ocellarisPhysXMayaDebugMenu() command");
		}
		MGlobal::executeCommand("ocellarisPhysXMaya_RegistryPlugin()");
	}
/*/
	return stat;
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus   stat;
	MFnPlugin plugin( obj );

	unregisterGenerator(ocPhysXActor::id, &ocPhysXActor::generator);
	stat = plugin.deregisterNode( ocPhysXActor::id );
	if (!stat) displayString("cant deregister %s", ocPhysXActor::typeName.asChar());

	unregisterGenerator(ocPhysXJoint::id, &ocPhysXJoint::generator);
	stat = plugin.deregisterNode( ocPhysXJoint::id );
	if (!stat) displayString("cant deregister %s", ocPhysXJoint::typeName.asChar());

	unregisterGenerator(ocPhysXShape::id, &ocPhysXShape::generator);
	stat = plugin.deregisterNode( ocPhysXShape::id );
	if (!stat) displayString("cant deregister %s", ocPhysXShape::typeName.asChar());


	MGlobal::executeCommand("ocellarisPhysXMayaDebugMenuDelete()");
	return stat;
}
