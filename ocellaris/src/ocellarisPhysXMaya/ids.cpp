#include "pre.h"

const unsigned TypeIdBase = 0xda0f0;

#include "ocPhysXShape.h"
MTypeId ocPhysXShape::id( TypeIdBase+00 );
const MString ocPhysXShape::typeName( "ocPhysXShape" );

#include "ocPhysXJoint.h"
MTypeId ocPhysXJoint::id( TypeIdBase+01 );
const MString ocPhysXJoint::typeName( "ocPhysXJoint" );

#include "ocPhysXActor.h"
MTypeId ocPhysXActor::id( TypeIdBase+02 );
const MString ocPhysXActor::typeName( "ocPhysXActor" );

