#pragma once
#include "pre.h"

#include "ocellaris/ITransformGenerator.h"

#include <maya/MPxTransform.h>
#include <maya/MPxTransformationMatrix.h>

class ocPhysXActor : public MPxTransform
{
public:
	ocPhysXActor();
	virtual ~ocPhysXActor(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_kinematic;
//	static MObject o_output;		// Example output attribute

public:
	static MTypeId id;
	static const MString typeName;


	struct Generator : public cls::ITransformGenerator
	{
	//	int level;
	//	Math::Matrix4f trans;

		Generator();
	public:
		// ���������� ���, ������ ��� �������������
		virtual const char* GetUniqueName(
			){return "ocPhysXActorGenerator";};

		//! render ����� ���� = NULL
		virtual void OnTransformStart(
			cls::INode& node,						//!< ������
			cls::IRender* render,				//!< render
			int level,							//!< ������� ��������
			const Math::Matrix4f& curtransform,	//!< ������� ���������
			Math::Matrix4f& newtransform,			//!< ����� ���������
			MObject& generatornode			//!< generator node �.� 0
			);
		//! render ����� ���� = NULL
		virtual void OnTransformEnd(
			cls::INode& node,						//!< ������
			cls::IRender* render,				//!< render
			int level,							//!< ������� ��������
			MObject& generatornode			//!< generator node �.� 0
			);
	};
	static Generator generator;
};
