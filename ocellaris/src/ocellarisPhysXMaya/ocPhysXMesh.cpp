#include "ocPhysXMesh.h"
//#include <mathNmaya/mathNmaya.h>
#include <Maya/MDistance.h>
#include "ocellaris\ocellarisExport.h"

// ocellarisPhysXMaya.mll@PhysXMesh

ocPhysXMeshGenerator ocphysxmeshgenerator;

extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl PhysXMesh()
	{
		return &ocphysxmeshgenerator;
	}
}

bool ocPhysXMeshGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();
	printf("OnGeometryMesh\n");

	Math::Vec3f boxDim( 1 );
	Math::Box3f _box( -boxDim, boxDim );
	box.insert( _box );

	double scale = 1/MDistance(1.f, MDistance::uiUnit()).asMeters();
	
	if( render)
	{
		render->GlobalAttribute("physX::scale", cls::P<float>((float)scale));
		render->Parameter("physX::type", "Mesh");
		render->RenderCall("ocellarisPhysX.dll@Shape");
	}

	return true;
}