#pragma once

/*/
#include "Math\Math.h"
#include "MathNgl\MathNgl.h"
#include "MathNmaya\MathNmaya.h"
/*/
typedef unsigned int uint;

#include <math.h>
#include <assert.h>

#include <string>
#include <vector>
#include <map>
#include <list>
#include <set>
#include <algorithm>

#include <maya/MPoint.h>
#include <maya/MString.h>
#include <maya/MTime.h>
#include <maya/MVector.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVector.h>
#include <maya/MIntArray.h>
#include <maya/MPointArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MMatrix.h>


#include <maya/MPlug.h>
#include <maya/MPlugArray.h>

#include <maya/MFnMatrixData.h>

#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MBoundingBox.h>

#include <maya/MGlobal.h>
#include <maya/MSelectionList.h>
#include <maya/MPxNode.h>

#include <maya/MFnDependencyNode.h>
#include <maya/MFnDagNode.h>

#include <GL/glu.h>

#include <maya/MFnNumericData.h>


inline void displayString(const char* text, ...)
{
	va_list argList; va_start(argList, text);
	char sbuf[1024];
	_vsnprintf(sbuf, 1024, text, argList);
	va_end(argList);
	MGlobal::displayInfo(sbuf);
}
inline void displayStringD(const char* text, ...)
{
#ifdef _DEBUG
	va_list argList; va_start(argList, text);
	char sbuf[1024];
	_vsnprintf(sbuf, 1024, text, argList);
	va_end(argList);
	MGlobal::displayInfo(sbuf);
#endif
}

inline MMatrix getMatrixForPlug(MPlug worldMatrix, MTime t)
{
	MDGContext context(t);
	MObject val;
	worldMatrix.getValue(val, context);
	MFnMatrixData matrData(val);
	MMatrix m = matrData.matrix();
	return m;
}

inline MMatrix move( const MMatrix& m, const MVector& v)
{
	MMatrix L = m;
	L(3, 0) += v[0];
	L(3, 1) += v[1];
	L(3, 2) += v[2];
//	L(3, 3) += v[3];
	return L;
}

inline void displayPoint( const MPoint& p)
{
	displayString("%g, %g, %g, %g", p[0], p[1], p[2], p[3]);
}

inline void displayMatrix( const MMatrix& L)
{
	displayString("%g, %g, %g, %g", L(0, 0), L(0, 1), L(0, 2), L(0, 3));
	displayString("%g, %g, %g, %g", L(1, 0), L(1, 1), L(1, 2), L(1, 3));
	displayString("%g, %g, %g, %g", L(2, 0), L(2, 1), L(2, 2), L(2, 3));
	displayString("%g, %g, %g, %g", L(3, 0), L(3, 1), L(3, 2), L(3, 3));
}

// Equivalence test for floats.  Equality tests are dangerous for floating      
// point values 
//
#define FLOAT_EPSILON 0.0001

inline bool equiv( float val1, float val2 )
{
    return ( fabsf( val1 - val2 ) < FLOAT_EPSILON );
}

// CMD
inline MStatus nodeFromName(MString name, MObject & obj)
{
	MSelectionList tempList;
    tempList.add( name );
    if ( tempList.length() > 0 ) 
	{
		tempList.getDependNode( 0, obj );
		return MS::kSuccess;
	}
	return MS::kFailure;
}
inline MStatus plugFromName(MString name, MPlug & plug)
{
	MSelectionList tempList;
    tempList.add( name );
    if ( tempList.length() > 0 ) 
	{
		tempList.getPlug( 0, plug );
		return MS::kSuccess;
	}
	return MS::kFailure;
}

template <class T>
void copyBox(T dst[6], MBoundingBox& src)
{
	dst[0] = (T)src.min().x;
	dst[1] = (T)src.max().x;
	dst[2] = (T)src.min().y;
	dst[3] = (T)src.max().y;
	dst[4] = (T)src.min().z;
	dst[5] = (T)src.max().z;
}

enum aTypes
{
	atReadable		= 0x1,
	atUnReadable	= 0x10000,
	atWritable		= 0x2,
	atUnWritable	= 0x20000,
	atConnectable	= 0x4,
	atUnConnectable	= 0x40000,
	atStorable		= 0x8,
	atUnStorable	= 0x80000,
	atCached		= 0x10,
	atUnCached		= 0x100000,
	atArray			= 0x20, 
	atKeyable		= 0x40,
	atHidden		= 0x80,
	atUnHidden		= 0x800000,
	atUsedAsColor	= 0x100,
	atRenderSource	= 0x200,

	atInput			= atReadable|atWritable|atStorable|atCached,
	atOutput		= atReadable|atUnWritable|atConnectable|atUnStorable,
};

inline void addAttribute(MObject& aobj, int flags)
{
	if( aobj.isNull())
	{
		displayString("cant create attribute");
		throw MString("cant create attribute");
	}

	MFnAttribute attr(aobj);
	if(flags & atReadable)
		attr.setReadable(true);
	if(flags & atUnReadable)
		attr.setReadable(false);

	if(flags & atWritable)
		attr.setWritable(true);
	if(flags & atUnWritable)
		attr.setWritable(false);

	if(flags & atConnectable)
		attr.setConnectable(true);
	if(flags & atUnConnectable)
		attr.setConnectable(false);

	if(flags & atStorable)
		attr.setStorable(true);
	if(flags & atUnStorable)
		attr.setStorable(false);

	if(flags & atCached)
		attr.setCached(true);
	if(flags & atUnCached)
		attr.setCached(false);

	if(flags & atArray)
		attr.setArray(true);
	if(flags & atKeyable)
		attr.setKeyable(true);

	if(flags & atHidden)
		attr.setHidden(true);
	if(flags & atUnHidden)
		attr.setHidden(false);

	if(flags & atUsedAsColor)
		attr.setUsedAsColor(true);
	if(flags & atRenderSource)
		attr.setRenderSource(true);

	MStatus stat;
	stat = MPxNode::addAttribute(attr.object());
	if(!stat)
	{
		displayString("cant add attribute %s", attr.name().asChar());
		throw attr.name();
	}
}

inline MStatus SourceMelFromResource(HINSTANCE hModule, LPCSTR lpName, LPCSTR lpType)
{
	HRSRC res = FindResource( hModule, lpName, lpType);
	if( !res)
		return MS::kFailure;

	HGLOBAL mem = ::LoadResource(MhInstPlugin, res);
	if(!mem)
		return MS::kFailure;

	char* data = (char*)LockResource(mem);
	if(!data)
		return MS::kFailure;

	MString str(data);
	MStatus stat = MGlobal::executeCommand(str);
	return stat;
}
