//
// Copyright (C) 
// File: ocPhysXJointCmd.cpp
// MEL Command: ocPhysXJoint

#include "ocPhysXJoint.h"

#include <maya/MGlobal.h>
#include <maya/MFnEnumAttribute.h>
#include "mathNgl\mathNgl.h"
#include "mathNmaya\mathNmaya.h"
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnDagNode.h>
#include <maya/MDagPath.h>

MObject ocPhysXJoint::i_type;
MObject ocPhysXJoint::i_actor[2];
MObject ocPhysXJoint::i_axis[3];
MObject ocPhysXJoint::i_motor[4];
MObject ocPhysXJoint::i_spring[4];
MObject ocPhysXJoint::i_limit[3];
MObject ocPhysXJoint::i_ballancer[4];
MObject ocPhysXJoint::o_output;

ocPhysXJoint::ocPhysXJoint()
{
}
ocPhysXJoint::~ocPhysXJoint()
{
}

MStatus ocPhysXJoint::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;

	if( plug == o_output )
	{
		MDataHandle outputData = data.outputValue( plug, &stat );

		data.setClean(plug);
		return MS::kSuccess;
	}

	return MS::kUnknownParameter;
}

bool ocPhysXJoint::isBounded() const
{
	MObject thisNode = thisMObject();
	return false;
}

MBoundingBox ocPhysXJoint::boundingBox() const
{
	MObject thisNode = thisMObject();

	MBoundingBox box;
	//...
	return box;
}

void ocPhysXJoint::draw(
	M3dView &view, 
	const MDagPath &path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status)
{ 
	MObject thisNode = thisMObject();
	MObject node = thisNode;
	MPlug plug;

	short type;
	MPlug(thisNode, i_type).getValue(type);
	MMatrix myinv = path.inclusiveMatrixInverse();

	view.beginGL(); 
	//...

 
	float r = 0;
	{
		plug = MPlug(node, ocPhysXJoint::i_actor[0]);
		MPlugArray pa;
		plug.connectedTo(pa, true, true);
		if(pa.length()!=0)
		{
			MDagPath path;
//printf("a0: %s\n", pa[0].name());
			path = MDagPath::getAPathTo(pa[0].node());
//printf("a0: %s\n", path.fullPathName().asChar());
			MMatrix m = path.inclusiveMatrix();
			MPoint pt = MPoint(0, 0, 0, 1)*m;
			pt = pt*myinv;
			Math::Vec3f p; ::copy(p, pt);

			r += p.length();
			drawLine(Math::Vec3f(0), p);
		}
	}
	{
		plug = MPlug(node, ocPhysXJoint::i_actor[1]);
		MPlugArray pa;
		plug.connectedTo(pa, true, true);
		if(pa.length()!=0)
		{
			MDagPath path;
			path = MDagPath::getAPathTo(pa[0].node());
//printf("a1: %s\n", path.fullPathName().asChar());
			MMatrix m = path.inclusiveMatrix();
			MPoint pt = MPoint(0, 0, 0, 1)*m;
			pt = pt*myinv;
			Math::Vec3f p; ::copy(p, pt);
			r += p.length();

			drawLine(Math::Vec3f(0), p);
		}
	}
	if( r<=0)
		r = 0.1f;
	else
		r = r/10;

	switch(type)
	{
	case 0:
		{
			float z = 2*r;
			drawCircleXY(r, Math::Vec3f(0, 0, -z));
			drawCircleXY(r, Math::Vec3f(0, 0, z));
			drawLine(Math::Vec3f(0, r/2, -z), Math::Vec3f(0, r/2, z));
			drawLine(Math::Vec3f(0, -r/2, -z), Math::Vec3f(0, -r/2, z));
			drawLine(Math::Vec3f(-r/2, 0, -z), Math::Vec3f(-r/2, 0, z));
			drawLine(Math::Vec3f(r/2, 0, -z), Math::Vec3f(r/2, 0, z));
			break;
		}
	case 1:
		{
			drawSphere(2*r, Math::Vec3f(0, 0, 0));

			float z = 1.5f*r;
			drawCircleXY(r, Math::Vec3f(0, 0, -z));
			drawCircleXY(r, Math::Vec3f(0, 0, z));

			break;
		}
		/*/
	case 2:
		{
			float z = 2*r;
			drawCircleXY(r, Math::Vec3f(0, 0, -z));
			drawCircleXY(r, Math::Vec3f(0, 0, z));
			drawLine(Math::Vec3f(0, r/2, -z), Math::Vec3f(0, r/2, z));
			drawLine(Math::Vec3f(0, -r/2, -z), Math::Vec3f(0, -r/2, z));
			drawLine(Math::Vec3f(-r/2, 0, -z), Math::Vec3f(-r/2, 0, z));
			drawLine(Math::Vec3f(r/2, 0, -z), Math::Vec3f(r/2, 0, z));

			drawCircleYZ(r, Math::Vec3f(-z, 0, 0));
			drawCircleYZ(r, Math::Vec3f(z, 0, 0));
			drawLine(Math::Vec3f(-z, 0, r/2), Math::Vec3f(z, 0, r/2));
			drawLine(Math::Vec3f(-z, 0, -r/2), Math::Vec3f(z, 0, -r/2));
			drawLine(Math::Vec3f(-z, -r/2, 0), Math::Vec3f(z, -r/2, 0));
			drawLine(Math::Vec3f(-z, r/2, 0), Math::Vec3f(z, r/2, 0));
			break;
		}
		/*/
	}

	view.endGL();
};


void* ocPhysXJoint::creator()
{
	return new ocPhysXJoint();
}

MStatus ocPhysXJoint::initialize()
{
	MFnMessageAttribute mesAttr;
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnEnumAttribute enumAttr;
	MStatus				stat;

	try
	{
		{
			i_type = enumAttr.create ("type","ty", 0, &stat);
			enumAttr.addField("Revolute", 0);
			enumAttr.addField("Spherical", 1);
//			enumAttr.addField("Revolute2D", 2);
			::addAttribute(i_type, atInput|atKeyable);
		}
		{
			i_actor[0] = mesAttr.create("actor_0","a_0", &stat);
			::addAttribute(i_actor[0], atInput);
			i_actor[1] = mesAttr.create("actor_1","a_1", &stat);
			::addAttribute(i_actor[1], atInput);
		}
		{
			i_motor[0] = numAttr.create("enableMotor", "enm", MFnNumericData::kBoolean, 0, &stat);
			i_motor[1] = numAttr.create("maxMotorSpeed", "mms", MFnNumericData::kDouble, 10, &stat);
			i_motor[2] = numAttr.create("maxMotorForce", "mmf", MFnNumericData::kDouble, 1000, &stat);
			i_motor[3] = numAttr.create("freeMotorSpin", "fms", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_motor[0], atInput|atKeyable);
			::addAttribute(i_motor[1], atInput|atKeyable);
			::addAttribute(i_motor[2], atInput|atKeyable);
			::addAttribute(i_motor[3], atInput|atKeyable);
		}
		{
			i_spring[0] = numAttr.create("springEnable", "sen", MFnNumericData::kBoolean, 0, &stat);
			i_spring[1] = numAttr.create("springValue",  "sv", MFnNumericData::kDouble, 10, &stat);
			i_spring[2] = numAttr.create("springDamper", "sd", MFnNumericData::kDouble, 0, &stat);
			i_spring[3] = numAttr.create("springTarget", "st", MFnNumericData::kDouble, 0, &stat);
			::addAttribute(i_spring[0], atInput|atKeyable);
			::addAttribute(i_spring[1], atInput|atKeyable);
			::addAttribute(i_spring[2], atInput|atKeyable);
			::addAttribute(i_spring[3], atInput|atKeyable);
		}
		{
			i_limit[0] = numAttr.create("limitEnable", "len", MFnNumericData::kBoolean, 0, &stat);
			i_limit[1] = numAttr.create("limitMin",  "lmi", MFnNumericData::kDouble, -45, &stat);
			i_limit[2] = numAttr.create("limitMax",  "lma", MFnNumericData::kDouble, 45, &stat);
			::addAttribute(i_limit[0], atInput|atKeyable);
			::addAttribute(i_limit[1], atInput|atKeyable);
			::addAttribute(i_limit[2], atInput|atKeyable);
		}
		{		
			i_ballancer[0] = numAttr.create("ballancerEnable",	"ben", MFnNumericData::kBoolean, 0, &stat);
//			i_ballancer[1] = numAttr.create("ballancerSpeed",	"bsp", MFnNumericData::kDouble, 10, &stat);
//			i_ballancer[2] = numAttr.create("ballancerForce",	"bfo", MFnNumericData::kDouble, 1000, &stat);
			i_ballancer[1] = numAttr.create("ballancerFrequency", "bfr", MFnNumericData::kDouble, 1, &stat);
			i_ballancer[2] = numAttr.create("ballancerPhase",	"bph", MFnNumericData::kDouble, 0, &stat);
			i_ballancer[3] = numAttr.create("ballancerOffset",	"bof", MFnNumericData::kDouble, 0, &stat);
			::addAttribute(i_ballancer[0], atInput|atKeyable);
			::addAttribute(i_ballancer[1], atInput|atKeyable);
			::addAttribute(i_ballancer[2], atInput|atKeyable);
			::addAttribute(i_ballancer[3], atInput|atKeyable);
//			::addAttribute(i_ballancer[4], atInput|atKeyable);
		}


		{
			o_output = numAttr.create( "o_output", "out", MFnNumericData::kDouble, 0.01, &stat);
			::addAttribute(o_output, atOutput);
//			stat = attributeAffects( i_input, o_output );
		}
		if( !MGlobal::sourceFile("AEocPhysXJointTemplate.mel"))
		{
			displayString("error source AEocPhysXJointTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}


// static
ocPhysXJoint::Generator ocPhysXJoint::generator;

bool ocPhysXJoint::Generator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();
	MFnDependencyNode dn(node);
	ocPhysXJoint* ocinstance = (ocPhysXJoint*)dn.userNode();
	if( !ocinstance) return false;

	MPlug plug;
	plug = MPlug(node, ocPhysXJoint::i_type);
	short type;
	plug.getValue(type);


	std::string actor0, actor1;
	{
		plug = MPlug(node, ocPhysXJoint::i_actor[0]);
		MPlugArray pa;
		plug.connectedTo(pa, true, true);
		if(pa.length()==0)
			return false;
		MDagPath path;
		path = MDagPath::getAPathTo(pa[0].node());
		MFnDagNode dagn( pa[0].node());
		actor0 = dagn.fullPathName().asChar();
	}
	{
		plug = MPlug(node, ocPhysXJoint::i_actor[1]);
		MPlugArray pa;
		plug.connectedTo(pa, true, true);
		if(pa.length()==0)
			return false;
		MDagPath path;
		path = MDagPath::getAPathTo(pa[0].node());
		MFnDagNode dagn( pa[0].node());
		actor1 = dagn.fullPathName().asChar();
	}


	Math::Vec3f boxDim(1);
	Math::Box3f mybox(-boxDim, boxDim);
	box.insert(mybox);

	MDagPath path = inode.getPath();
	MMatrix mm = path.inclusiveMatrix();
	Math::Matrix4f m;
	copy( m, mm);
	render->PushTransform();
		render->PushAttributes();
		//render->SetTransform(m);
		render->PopAttributes();


	{
		bool enableMotor = false; 
		double maxMotorSpeed = 0;
		double maxMotorForce = 0;
		bool freeMotorSpin = true;
		MPlug(node, ocPhysXJoint::i_motor[0]).getValue(enableMotor);
		MPlug(node, ocPhysXJoint::i_motor[1]).getValue(maxMotorSpeed);
		MPlug(node, ocPhysXJoint::i_motor[2]).getValue(maxMotorForce);
		MPlug(node, ocPhysXJoint::i_motor[3]).getValue(freeMotorSpin);
		render->Parameter("physX::enableMotor", enableMotor);
		render->Parameter("physX::maxMotorSpeed", (float)maxMotorSpeed);
		render->Parameter("physX::maxMotorForce", (float)maxMotorForce);
		render->Parameter("physX::freeMotorSpin", freeMotorSpin);
	}
	{
		bool springEnable = false;
		double springValue=0, springDamper=0, springTarget=0;
		MPlug(node, ocPhysXJoint::i_spring[0]).getValue(springEnable);
		MPlug(node, ocPhysXJoint::i_spring[1]).getValue(springValue);
		MPlug(node, ocPhysXJoint::i_spring[2]).getValue(springDamper);
		MPlug(node, ocPhysXJoint::i_spring[3]).getValue(springTarget);
		render->Parameter("physX::springEnable", springEnable);
		render->Parameter("physX::springValue", (float)springValue);
		render->Parameter("physX::springDamper", (float)springDamper);
		render->Parameter("physX::springTarget", (float)springTarget);
	}
	{
		bool limitEnable = false;
		double limitMin=0, limitMax=0;
		MPlug(node, ocPhysXJoint::i_limit[0]).getValue(limitEnable);
		MPlug(node, ocPhysXJoint::i_limit[1]).getValue(limitMin);
		MPlug(node, ocPhysXJoint::i_limit[2]).getValue(limitMax);
		render->Parameter("physX::limitEnable", limitEnable);
		render->Parameter("physX::limitMin", (float)(limitMin*M_PI/180.f));
		render->Parameter("physX::limitMax", (float)(limitMax*M_PI/180.f));
	}
	{
		bool ballancerEnable = false; 
		double ballancerFrequency = 1, ballancerPhase=0, ballancerOffset=0;
//		double maxMotorSpeed=0, maxMotorForce = 0;
		MPlug(node, ocPhysXJoint::i_ballancer[0]).getValue(ballancerEnable);
		MPlug(node, ocPhysXJoint::i_ballancer[1]).getValue(ballancerFrequency);
		MPlug(node, ocPhysXJoint::i_ballancer[2]).getValue(ballancerPhase);
		MPlug(node, ocPhysXJoint::i_ballancer[3]).getValue(ballancerOffset);

		render->Parameter("physX::ballancerEnable", ballancerEnable);
		render->Parameter("physX::ballancerFrequency", (float)ballancerFrequency);
		render->Parameter("physX::ballancerPhase", (float)ballancerPhase);
		render->Parameter("physX::ballancerOffset", (float)(ballancerOffset*M_PI/180.f));
//		render->Parameter("physX::ballancerSpeed", (float)maxMotorSpeed);
//		render->Parameter("physX::ballancerForce", (float)maxMotorForce);
	}

	if( render)
	{
		switch(type)
		{
		case 0:
			{

				render->Parameter("physX::type", "Revolute");
				render->Parameter("physX::actor0", actor0.c_str());
				render->Parameter("physX::actor1", actor1.c_str());

				render->RenderCall("ocellarisPhysX.dll@Joint");
				break;
			}
		case 1:
			{
				render->Parameter("physX::type", "Spherical");
				render->Parameter("physX::actor0", actor0.c_str());
				render->Parameter("physX::actor1", actor1.c_str());
				render->RenderCall("ocellarisPhysX.dll@Joint");
				break;
			}
		}
	}
	render->PopTransform();

	return true;
}
