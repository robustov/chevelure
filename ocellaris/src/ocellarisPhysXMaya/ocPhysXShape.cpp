//
// Copyright (C) 
// File: ocPhysXShapeCmd.cpp
// MEL Command: ocPhysXShape

#include "ocPhysXShape.h"

#include <maya/MGlobal.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MDistance.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshPolygon.h>
#include "mathNgl\mathNgl.h"
#include <mathNmaya/mathNmaya.h>

MObject ocPhysXShape::i_type;
MObject ocPhysXShape::o_output;

// box
MObject ocPhysXShape::i_box[3];
// capsule
MObject ocPhysXShape::i_capsule[2];
// mesh
MObject ocPhysXShape::i_mesh[2];

MObject ocPhysXShape::i_flag[2];
MObject ocPhysXShape::i_density;

ocPhysXShape::ocPhysXShape()
{
}
ocPhysXShape::~ocPhysXShape()
{
}

MStatus ocPhysXShape::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;

	if( plug == o_output )
	{
		MDataHandle outputData = data.outputValue( plug, &stat );

		data.setClean(plug);
		return MS::kSuccess;
	}

	return MS::kUnknownParameter;
}

bool ocPhysXShape::isBounded() const
{
	MObject thisNode = thisMObject();
	return false;
}

MBoundingBox ocPhysXShape::boundingBox() const
{
	MObject thisNode = thisMObject();

	float x=1;
	MBoundingBox box(MPoint(-x, -x,-x), MPoint(x, x,x));
	//...
	return box;
}

void ocPhysXShape::draw(
	M3dView &view, 
	const MDagPath &path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status)
{ 
	MObject thisNode = thisMObject();

	MPlug plug;
	plug = MPlug(thisNode, i_type);
	short type;
	plug.getValue(type);

	view.beginGL(); 

	switch(type)
	{
	case 0:
		{
			double b[3];
			MPlug(thisNode, i_box[0]).getValue(b[0]);
			MPlug(thisNode, i_box[1]).getValue(b[1]);
			MPlug(thisNode, i_box[2]).getValue(b[2]);
			
			Math::Vec3f r( (float)b[0]/2, (float)b[1]/2, (float)b[2]/2);
			Math::Box3f box(-r, r);
			drawBox(box);
			break;
		}
	case 1:
		{
			double c[2];
			MPlug(thisNode, ocPhysXShape::i_capsule[0]).getValue(c[0]);
			MPlug(thisNode, ocPhysXShape::i_capsule[1]).getValue(c[1]);

			Math::Vec3f p1(0, (float)c[0]/2, 0);
			Math::Vec3f p2(0, -(float)c[0]/2, 0);

			drawSphere((float)c[1]*2, p1);
			drawSphere((float)c[1]*2, p2);
			drawLine(p1, p2);
			break;
		}
	case 2:
		{
			double c[2];
			MPlug(thisNode, ocPhysXShape::i_capsule[0]).getValue(c[0]);
			MPlug(thisNode, ocPhysXShape::i_capsule[1]).getValue(c[1]);

			Math::Box3f box(1);
			drawLine(box.corner(0), box.corner(7));
			drawLine(box.corner(1), box.corner(6));
			drawLine(box.corner(2), box.corner(5));
			drawLine(box.corner(3), box.corner(4));
			break;
		}
	case 3:
		{
			MObject meshObj;
			MPlug(thisNode, ocPhysXShape::i_mesh[0]).getValue(meshObj);
			MFnMesh mesh(meshObj);
			MPointArray mpoints;
			mesh.getPoints(mpoints);
			MIntArray faces;
			MPointArray allPoints;

			for ( MItMeshPolygon polyIt( meshObj ); !polyIt.isDone(); polyIt.next() ) 
			{
				if( polyIt.hasValidTriangulation() )
				{
					MPointArray points;
					MIntArray vertices;
					polyIt.getTriangles( points, vertices );

					for( unsigned int i = 0; i < vertices.length(); i++ )
					{
						faces.append( vertices[i] );
					}
				}
			}

			for( unsigned int i = 0; i < faces.length(); i+=3 )
			{
				Math::Vec3f v1, v2;
				v1[0] = (float)mpoints[faces[i]].x;
				v1[1] = (float)mpoints[faces[i]].y;
				v1[2] = (float)mpoints[faces[i]].z;
				v2[0] = (float)mpoints[faces[i+1]].x;
				v2[1] = (float)mpoints[faces[i+1]].y;
				v2[2] = (float)mpoints[faces[i+1]].z;
				drawLine(v1,v2);
//				MGlobal::displayInfo( MString("_") + (int)faces[i] );
			}


	//while(nbTriangles--)
	//{
	//	DrawLine(points[triangles->p0], points[triangles->p1], color);
	//	DrawLine(points[triangles->p1], points[triangles->p2], color);
	//	DrawLine(points[triangles->p2], points[triangles->p0], color);
	//	triangles++;
	//}

			Math::Vec3f r( 1,1,1);
			Math::Box3f box(-r, r);
			drawBox(box);
			break;
		}

	}

	//...
	view.endGL();
};


void* ocPhysXShape::creator()
{
	return new ocPhysXShape();
}

MStatus ocPhysXShape::initialize()
{
	MFnEnumAttribute enumAttr;
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		{
			i_type = enumAttr.create ("type","ty", 0, &stat);
			enumAttr.addField("BOX", 0);
			enumAttr.addField("CAPSULE", 1);
			enumAttr.addField("VOID", 2);
			enumAttr.addField("MESH", 3);
			::addAttribute(i_type, atInput);
		}
		{
			i_box[0] = numAttr.create ("box_X","b_0", MFnNumericData::kDouble, 1, &stat);
			i_box[1] = numAttr.create ("box_Y","b_1", MFnNumericData::kDouble, 1, &stat);
			i_box[2] = numAttr.create ("box_Z","b_2", MFnNumericData::kDouble, 1, &stat);
			::addAttribute(i_box[0], atInput|atKeyable);
			::addAttribute(i_box[1], atInput|atKeyable);
			::addAttribute(i_box[2], atInput|atKeyable);
		}
		{
			i_capsule[0] = numAttr.create ("capsule_height", "c_0", MFnNumericData::kDouble, 1, &stat);
			i_capsule[1] = numAttr.create ("capsule_radius", "c_1", MFnNumericData::kDouble, 1, &stat);
			::addAttribute(i_capsule[0], atInput|atKeyable);
			::addAttribute(i_capsule[1], atInput|atKeyable);
		}
		{
			i_mesh[0] = typedAttr.create ("inMesh", "m_0", MFnData::kMesh, &stat);
			i_mesh[1] = numAttr.create ("convex", "m_1", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_mesh[0], atInput);
			::addAttribute(i_mesh[1], atInput|atKeyable);			
		}
		{
			i_flag[0] = numAttr.create ("flag_collidable", "f_0", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_flag[0], atInput|atKeyable);			
//			i_flag[1] = numAttr.create ("flag_kinematic", "f_1", MFnNumericData::kBoolean, 0, &stat);
//			::addAttribute(i_flag[1], atInput|atKeyable);
		}
		{
			i_density = numAttr.create ("density", "den", MFnNumericData::kDouble, 1, &stat);
			::addAttribute(i_density, atInput|atKeyable);
		}


		{
			o_output = numAttr.create( "o_output", "out", MFnNumericData::kDouble, 0.01, &stat);
			::addAttribute(o_output, atOutput);
			stat = attributeAffects( i_type, o_output );
		}
		if( !MGlobal::sourceFile("AEocPhysXShapeTemplate.mel"))
		{
			displayString("error source AEocPhysXShapeTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

// static
ocPhysXShape::Generator ocPhysXShape::generator;

// �� ������������ ��. ocInstancerProxyGenerator
bool ocPhysXShape::Generator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();
	MFnDependencyNode dn(node);
	ocPhysXShape* ocinstance = (ocPhysXShape*)dn.userNode();
	if( !ocinstance) return false;

	double scale = 1/MDistance(1.f, MDistance::uiUnit()).asMeters();

	MPlug plug;
	plug = MPlug(node, ocPhysXShape::i_type);
	short type;
	plug.getValue(type);

	Math::Vec3f boxDim(1);
	Math::Box3f mybox(-boxDim, boxDim);
	box.insert(mybox);

	bool flags[1];
	MPlug(node, ocPhysXShape::i_flag[0]).getValue(flags[0]);
	float density=1;
	MPlug(node, ocPhysXShape::i_density).getValue(density);

	render->Parameter("physX::collidable", flags[0]);
	render->Parameter("physX::kinematic", flags[1]);
	render->Parameter("physX::density", density);

	if( render)
	{
		render->GlobalAttribute("physX::scale", cls::P<float>((float)scale));

		switch(type)
		{
		case 0:
			{
				double b[3];
				MPlug(node, ocPhysXShape::i_box[0]).getValue(b[0]);
				MPlug(node, ocPhysXShape::i_box[1]).getValue(b[1]);
				MPlug(node, ocPhysXShape::i_box[2]).getValue(b[2]);
				Math::Vec3f r((float)b[0]/2, (float)b[1]/2, (float)b[2]/2);

				render->Parameter("physX::type", "Box");
				render->Parameter("physX::boxDim", r);
				render->RenderCall("ocellarisPhysX.dll@Shape");
				render->Parameter("#box", Math::Box3f(-r, r));
				render->RenderCall("SolidBox");
				break;
			}
		case 1:
			{
				double c[2];
				MPlug(node, ocPhysXShape::i_capsule[0]).getValue(c[0]);
				MPlug(node, ocPhysXShape::i_capsule[1]).getValue(c[1]);

				render->Parameter("physX::type", "Capsule");
				render->Parameter("physX::height", (float)c[0]);
				render->Parameter("physX::radius", (float)c[1]);
				render->RenderCall("ocellarisPhysX.dll@Shape");
				break;
			}
		case 2:
			{
				render->Parameter("physX::type", "Void");
				render->RenderCall("ocellarisPhysX.dll@Shape");
				break;
			}
		case 3:
			{
				bool m_1 = true;
				MPlug(node, ocPhysXShape::i_mesh[1]).getValue(m_1);

				MObject meshObj;
				MPlug(node, ocPhysXShape::i_mesh[0]).getValue(meshObj);
				MFnMesh mesh(meshObj);
				MPointArray mpoints;
				mesh.getPoints(mpoints);

				//cls::PA<Math::Vec3f> points(cls::PI_CONSTANT);
				cls::PA<float> points(cls::PI_CONSTANT);
				cls::PA<int> faces(cls::PI_CONSTANT);

				faces.reserve( mesh.numFaceVertices()*3 );
				//points.resize( mpoints.length()*3 );
				points.reserve( mpoints.length()*3 );

				for(unsigned int i = 0; i < mpoints.length(); i++)
				{
					//copy( points[i], mpoints[i] );
					points.push_back( (float)mpoints[i].x );
					points.push_back( (float)mpoints[i].y );
					points.push_back( (float)mpoints[i].z );
				}

				for ( MItMeshPolygon polyIt( meshObj ); !polyIt.isDone(); polyIt.next() ) 
				{
					if( polyIt.hasValidTriangulation() )
					{
						MPointArray points;
						MIntArray vertices;
						polyIt.getTriangles( points, vertices );

						for( unsigned int i = 0; i < vertices.length(); i++ )
						{
							faces.push_back( vertices[i] );
						}
					}
				}

				render->Parameter("physX::type", "Mesh");
				render->Parameter("physX::convex", m_1);
				render->Parameter("physX::points", points);
				render->Parameter("physX::faces", faces);
				render->RenderCall("ocellarisPhysX.dll@Shape");
				break;
			}
		}
	}

	return true;
}

