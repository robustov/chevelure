#include "ocPhysXCloth.h"
#include <mathNmaya/mathNmaya.h>
#include "ocellaris\ocellarisExport.h"
#include <maya/MFnMesh.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MDagPath.h>
#include <maya/M3dView.h>

// ocellarisPhysXMaya.mll@PhysXMesh

#define render_bool(attr) cls::P<bool> attr = false;\
						attr = ocExport_GetAttrValue( node, #attr );\
						render->Parameter( #attr, attr );

#define render_float(attr) cls::P<float> attr = 0.0f;\
						attr = ocExport_GetAttrValue( node, #attr );\
						render->Parameter( #attr, attr );

#define render_int(attr) cls::P<int> attr = 0;\
						attr = ocExport_GetAttrValue( node, #attr );\
						render->Parameter( #attr, attr );

ocPhysXClothGenerator ocphysxclothgenerator;

extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl PhysXCloth()
	{
		return &ocphysxclothgenerator;
	}
}

bool ocPhysXClothGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,			//!< render
	Math::Box3f& box,				//!< box 
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();

	Math::Vec3f boxDim( 1 );
	Math::Box3f _box( -boxDim, boxDim );
	box.insert( _box );

	ocExport_AddAttrType( node, "glueStart", cls::PT_BOOL, cls::P<bool>(true));
	ocExport_AddAttrType( node, "glueEnd", cls::PT_BOOL, cls::P<bool>(true));
	ocExport_AddAttrType( node, "glueVertices", cls::PT_STRING, cls::PS(""));

	ocExport_AddAttrType( node, "pressureFlag", cls::PT_BOOL, cls::P<bool>(false));
	ocExport_AddAttrType( node, "static", cls::PT_BOOL, cls::P<bool>(false));
	ocExport_AddAttrType( node, "disableCollision", cls::PT_BOOL, cls::P<bool>(false));
	ocExport_AddAttrType( node, "selfcollision", cls::PT_BOOL, cls::P<bool>(false));
	//ocExport_AddAttrType( node, "visualization", cls::PT_BOOL, cls::P<bool>(false));
	ocExport_AddAttrType( node, "gravity", cls::PT_BOOL, cls::P<bool>(true));
	ocExport_AddAttrType( node, "bending", cls::PT_BOOL, cls::P<bool>(false));
	ocExport_AddAttrType( node, "bendingOrtho", cls::PT_BOOL, cls::P<bool>(false));
	ocExport_AddAttrType( node, "damping", cls::PT_BOOL, cls::P<bool>(false));
	ocExport_AddAttrType( node, "collisionTwoway", cls::PT_BOOL, cls::P<bool>(false));
	//ocExport_AddAttrType( node, "triangleCollision", cls::PT_BOOL, cls::P<bool>(false));
	ocExport_AddAttrType( node, "tearable", cls::PT_BOOL, cls::P<bool>(false));
	ocExport_AddAttrType( node, "hardware", cls::PT_BOOL, cls::P<bool>(false));
	ocExport_AddAttrType( node, "comdamping", cls::PT_BOOL, cls::P<bool>(false));
	//ocExport_AddAttrType( node, "validbounds", cls::PT_BOOL, cls::P<bool>(false));
	//ocExport_AddAttrType( node, "fluidCollision", cls::PT_BOOL, cls::P<bool>(false));

	ocExport_AddAttrType( node, "thickness", cls::PT_FLOAT, cls::P<float>(0.01f)); //[0,inf)
	ocExport_AddAttrType( node, "density", cls::PT_FLOAT, cls::P<float>(1.0f)); //(0,inf)
	ocExport_AddAttrType( node, "bendingStiffness", cls::PT_FLOAT, cls::P<float>(1.0f)); //[0,1]
	ocExport_AddAttrType( node, "stretchingStiffness", cls::PT_FLOAT, cls::P<float>(1.0f)); //(0,1]
	ocExport_AddAttrType( node, "dampingCoefficient", cls::PT_FLOAT, cls::P<float>(0.5f)); //[0,1]
	ocExport_AddAttrType( node, "friction", cls::PT_FLOAT, cls::P<float>(0.5f)); //[0,1]
	ocExport_AddAttrType( node, "pressure", cls::PT_FLOAT, cls::P<float>(1.0f)); //[0,inf)
	ocExport_AddAttrType( node, "tearFactor", cls::PT_FLOAT, cls::P<float>(1.5f)); //(1,inf)
	ocExport_AddAttrType( node, "collisionResponseCoefficient", cls::PT_FLOAT, cls::P<float>(0.2f)); //[0,inf)
	ocExport_AddAttrType( node, "attachmentResponseCoefficient", cls::PT_FLOAT, cls::P<float>(0.2f)); //[0,inf)
	ocExport_AddAttrType( node, "attachmentTearFactor", cls::PT_FLOAT, cls::P<float>(1.5f)); //(1,inf)
	ocExport_AddAttrType( node, "solverIterations", cls::PT_INT, cls::P<int>(5)); //[1,inf)
	ocExport_AddAttrType( node, "sleepLinearVelocity", cls::PT_FLOAT, cls::P<float>(-1.0f)); //[0,inf)
	//NxReal toFluidResponseCoefficient
    //NxReal fromFluidResponseCoefficient
	//NxBounds3 validBounds
	ocExport_AddAttrType( node, "relativeGridSpacing", cls::PT_FLOAT, cls::P<float>(0.25f)); //[0.01,inf)

	ocExport_AddAttrType( node, "numVerticesInThreads", cls::PT_STRING, cls::PS(""));

	ocExport_AddAttrType( generatornode, "subSteps", cls::PT_INT, cls::P<int>(8)); //[1,inf)

	ocExport_AddAttrType( generatornode, "field", cls::PT_BOOL, cls::P<bool>(false));
	ocExport_AddAttrType( generatornode, "fieldForceX", cls::PT_FLOAT, cls::P<float>(0.0f)); //[0,inf)
	ocExport_AddAttrType( generatornode, "fieldForceY", cls::PT_FLOAT, cls::P<float>(0.0f)); //[0,inf)
	ocExport_AddAttrType( generatornode, "fieldForceZ", cls::PT_FLOAT, cls::P<float>(0.0f)); //[0,inf)
	ocExport_AddAttrType( generatornode, "fieldNoiseX", cls::PT_FLOAT, cls::P<float>(0.0f)); //[0,inf)
	ocExport_AddAttrType( generatornode, "fieldNoiseY", cls::PT_FLOAT, cls::P<float>(0.0f)); //[0,inf)
	ocExport_AddAttrType( generatornode, "fieldNoiseZ", cls::PT_FLOAT, cls::P<float>(0.0f)); //[0,inf)

	if( render)
	{
		MFnMesh mesh( node );
		MPointArray mpoints;
		mesh.getPoints(mpoints);

		cls::PA<float> points(cls::PI_CONSTANT);
		cls::PA<int> faces(cls::PI_CONSTANT);

		faces.reserve( mesh.numFaceVertices()*3 );
		points.reserve( mpoints.length()*3 );

		for(unsigned int i = 0; i < mpoints.length(); i++)
		{
			points.push_back( (float)mpoints[i].x );
			points.push_back( (float)mpoints[i].y );
			points.push_back( (float)mpoints[i].z );
		}

		for ( MItMeshPolygon polyIt( node ); !polyIt.isDone(); polyIt.next() ) 
		{
			if( polyIt.hasValidTriangulation() )
			{
				MPointArray points;
				MIntArray vertices;
				polyIt.getTriangles( points, vertices );

				for( unsigned int i = 0; i < vertices.length(); i++ )
				{
					faces.push_back( vertices[i] );
				}
			}
		}

		render->Parameter("physX::type", "Cloth");

		render_bool( glueStart );
		render_bool( glueEnd );

		cls::PS glueVertices("");
        glueVertices = ocExport_GetAttrValue( node, "glueVertices" );
		render->Parameter( "glueVertices", glueVertices );

		render_bool( pressureFlag );
		render_bool( staticFlag );
		render_bool( disableCollision );
		render_bool( selfcollision );
		render_bool( gravity );
		render_bool( bending );
		render_bool( bendingOrtho );
		render_bool( damping );
		render_bool( collisionTwoway );
		render_bool( tearable );
		render_bool( hardware );
		render_bool( comdamping );

		render_float( thickness );
		render_float( density );
		render_float( bendingStiffness );
		render_float( stretchingStiffness );
		render_float( dampingCoefficient );
		render_float( friction );
		render_float( pressure );
		render_float( tearFactor );
		render_float( collisionResponseCoefficient );
		render_float( attachmentResponseCoefficient );
		render_float( attachmentTearFactor );
		render_int( solverIterations );
		render_float( sleepLinearVelocity );
		render_float( relativeGridSpacing );

		render->Parameter("physX::points", points);
		render->Parameter("physX::faces", faces);

		cls::PS numVerticesInThreads("");
        numVerticesInThreads = ocExport_GetAttrValue( node, "numVerticesInThreads" );

		char* numVerticesInThreadsStr = strdup(numVerticesInThreads.data());
		char seps[] = " ,";
		char* token = NULL;
		std::vector<int> numVerticesVec;

		token = strtok( numVerticesInThreadsStr, seps );

		while( token != NULL )
		{
			//printf("_%s\n",token);
			numVerticesVec.push_back(atoi(token));
			token = strtok( NULL, seps );
		}

		if( numVerticesVec.size() > 1 )
		{
			cls::PA<int> numVertices(cls::PI_CONSTANT);
			numVertices.reserve(numVerticesVec.size());
			for( unsigned int i = 0; i < numVerticesVec.size(); i++ )
			{
				numVertices.push_back( numVerticesVec[i] );
			}

			render->Parameter( "numVerticesInThreads", numVertices );
		}

		render->RenderCall("ocellarisPhysX.dll@Cloth");

		MDagPath cPath;
		M3dView::active3dView().getCamera( cPath );
		MMatrix cm = cPath.inclusiveMatrix();
		Math::Matrix4f m;
		copy( m, cm);

		render->GlobalAttribute("PhysX::Camera", cls::P<Math::Matrix4f>(m));

		cls::P<int> subSteps = 8;
		subSteps = ocExport_GetAttrValue( generatornode, "subSteps" );
		render->GlobalAttribute("PhysX::subSteps", subSteps);

		cls::P<bool> field = false;
		field = ocExport_GetAttrValue( generatornode, "field" );
		render->GlobalAttribute("PhysX::field", field);

		if( field.data() )
		{
			cls::P<float> value = 0.0f;
			cls::P<Math::Vec3f> vecAttr;
			cls::P<Math::Vec3f> vecAttr2;

			value = ocExport_GetAttrValue( generatornode, "fieldForceX" );
			vecAttr.data()[0] = value.data();
			value = ocExport_GetAttrValue( generatornode, "fieldForceY" );
			vecAttr.data()[1] = value.data();
			value = ocExport_GetAttrValue( generatornode, "fieldForceZ" );
			vecAttr.data()[2] = value.data();
            render->GlobalAttribute("PhysX::fieldForce", vecAttr);

			value = ocExport_GetAttrValue( generatornode, "fieldNoiseX" );
			vecAttr2.data()[0] = value.data();
            value = ocExport_GetAttrValue( generatornode, "fieldNoiseY" );
			vecAttr2.data()[1] = value.data();
			value = ocExport_GetAttrValue( generatornode, "fieldNoiseZ" );
			vecAttr2.data()[2] = value.data();
			render->GlobalAttribute("PhysX::fieldNoise", vecAttr2);
		}
	}

	return true;
}