#pragma once
#include "shading/IShaderTemplate.h"

class ImageFile : public shn::IShaderTemplate
{
public:
	ImageFile(void);
	~ImageFile(void);

// ���������:
public:
	shn::StringParameter ImageFileName;				// ��� �����
	shn::EnumParameter AlphaOp;				// �����
	shn::FloatParameter SFilt, TFilt;				// ����������
	shn::EnumStringParameter Filter;
	shn::BoolParameter lerp;						// linear interpolation 

	// ColorBalance
	shn::ColorParameter ColorBias;					// bias
	shn::ColorParameter ColorGain;					// Gain

	shn::ManifoldParameter Manifold;				// UV ���������������

	shn::ColorParameter result;						// out

public:
	void Init();
public:
	//! ��� �������
	virtual enTemplateType GetType(
		){return shn::IShaderTemplate::COLOR;};

	virtual shn::Parameter* Parameters(
		shn::IShadingGraphNode& graphnode);
	virtual void Build(
		shn::IShadingGraphNode& graphnode);

};
