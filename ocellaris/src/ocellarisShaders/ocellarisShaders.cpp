// ocellarisShaders.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "Constant.h"
#include "ImageFile.h"

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    return TRUE;
}

// "��� �������" "����" "���"
char* theShaderList[] = 
{
	"Constant",		"SurfaceShaders",		"shadingmodel",
	"Plastic",		"SurfaceShaders",		"shadingmodel",
	"ImageFile",	"Colors",				"color",
	"NormalToColor","Colors",				"color",
	"ST",			"Manifolds",			"manifold",
	"SurfaceNormal","Normals",				"normal",	
	NULL
};

extern "C"
{
	// Constant
	__declspec(dllexport) char** __cdecl GetShaderList()
	{
		return theShaderList;
	}
}
