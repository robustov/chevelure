#include "StdAfx.h"
#include ".\normaltocolor.h"

NormalToColor theNormalToColor;
extern "C"
{
	// Constant
	__declspec(dllexport) shn::IShaderTemplate* __cdecl NormalToColor()
	{
		return &theNormalToColor;
	}
}

NormalToColor::NormalToColor(void)
{
	Init();
}

NormalToColor::~NormalToColor(void)
{
}
// ���������� 1 ���: ����� ��������
void NormalToColor::Init(void)
{
	normal.Init("n", "normal", Math::Vec3f(1.f, 0.f, 0.f), shn::CONNECTABLE);
	space.Init("space", "space", "NDC");
//	normal.DefConnectTo("SurfaceNormal");

	output.Init("result");
}
shn::Parameter* NormalToColor::Parameters(
	shn::IShadingGraphNode& graphnode
	)
{
	graphnode.RegisterInput(normal);
	graphnode.RegisterInput(space);
	return &output;
}

// ���������� ��� ��������� ���� �������
void NormalToColor::Build(
	shn::IShadingGraphNode& gn
	)
{
	gn.output("normal nn = n;");
	gn.output("if(space!=\"\") nn = ntransform(space, nn);");
	gn.output("nn = normalize(nn);");
	gn.output("result = color(xcomp(nn)*0.5+0.5, ycomp(nn)*0.5+0.5, zcomp(nn)*0.5+0.5);");
}

