#pragma once
#include "shading/IShaderTemplate.h"
#include "shading/IShaderParameter.h"

class SimpleDisplace : public shn::IShaderTemplate
{
public:
	shn::FloatParameter color;

	shn::DisplaceOutput output;

	SimpleDisplace(void);
	~SimpleDisplace(void);
	void Init();

	//! ��� �������
	virtual enTemplateType GetType(
		){return shn::IShaderTemplate::DISPLACESHADER;};

	virtual shn::Parameter* Parameters(
		shn::IShadingGraphNode& graphnode);
	virtual void Build(
		shn::IShadingGraphNode& graphnode);
};
