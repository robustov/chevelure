#include "StdAfx.h"
#include ".\constant.h"

Constant theConstant;
extern "C"
{
	// Constant
	__declspec(dllexport) shn::IShaderTemplate* __cdecl Constant()
	{
		return &theConstant;
	}
}

Constant::Constant(void)
{
	Init();
}

Constant::~Constant(void)
{
}
// ���������� 1 ���: ����� ��������
void Constant::Init(void)
{
	color.Init(
		"SurfaceColor", 
		"Surface Color", 
		Math::Vec3f(0.f, .25f, 1.f), 
		shn::CONNECTABLE);
//	color.DefConnectTo("ImageFile");

	opacity.Init(
		"SurfaceOpacity", 
		"Opacity", 
		Math::Vec3f(1.f, 1.f, 1.f), 
		shn::CONNECTABLE);

	output.Init();
}


shn::Parameter* Constant::Parameters(
	shn::IShadingGraphNode& graphnode
	)
{
	graphnode.RegisterInput(color);
	graphnode.RegisterInput(opacity);

	return &output;
}

// ���������� ��� ��������� ���� �������
void Constant::Build(
	shn::IShadingGraphNode& gn
	)
{
	gn.output("CI = SurfaceColor * SurfaceOpacity;");
	gn.output("OI = SurfaceOpacity;");
}

