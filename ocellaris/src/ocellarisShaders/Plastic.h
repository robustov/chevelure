#pragma once
#include "shading/IShaderTemplate.h"
#include "shading/IShaderParameter.h"

class Plastic : public shn::IShaderTemplate
{
public:
	shn::ColorParameter opacity;
	shn::FloatParameter Ka;
	shn::ColorParameter ambient;

	// diffuse
	shn::FloatParameter Kd;
	shn::ColorParameter diffuse;
	shn::NormalParameter diffuse_normal;

	// specular
	shn::FloatParameter Ks;
	shn::ColorParameter specular;
	shn::FloatParameter roughness;
	shn::NormalParameter specular_normal;

	// output
	shn::SurfaceOutput output;

	Plastic(void);
	~Plastic(void);

	void Init();
	//! ��� �������
	virtual enTemplateType GetType(
		){return shn::IShaderTemplate::SURFACESHADER;};

	virtual shn::Parameter* Parameters(
		shn::IShadingGraphNode& graphnode);
	virtual void Build(
		shn::IShadingGraphNode& graphnode);
};
