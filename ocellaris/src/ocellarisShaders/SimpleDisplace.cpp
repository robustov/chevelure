#include "StdAfx.h"
#include ".\SimpleDisplace.h"

SimpleDisplace theSimpleDisplace;
extern "C"
{
	// SimpleDisplace
	__declspec(dllexport) shn::IShaderTemplate* __cdecl SimpleDisplace()
	{
		return &theSimpleDisplace;
	}
}

SimpleDisplace::SimpleDisplace(void)
{
	Init();
}

SimpleDisplace::~SimpleDisplace(void)
{
}
// ���������� 1 ���: ����� ��������
void SimpleDisplace::Init(void)
{
	/*/
	color.Init(
		"SurfaceColor", 
		"Surface Color", 
		Math::Vec3f(0.f, .25f, 1.f), 
		shn::CONNECTABLE);

	opacity.Init(
		"SurfaceOpacity", 
		"Opacity", 
		Math::Vec3f(1.f, 1.f, 1.f), 
		shn::CONNECTABLE);

	output.Init();
	/*/
}


shn::Parameter* SimpleDisplace::Parameters(
	shn::IShadingGraphNode& graphnode
	)
{
	/*/
	graphnode.RegisterInput(color);
	graphnode.RegisterInput(opacity);
	/*/

	return &output;
}

// ���������� ��� ��������� ���� �������
void SimpleDisplace::Build(
	shn::IShadingGraphNode& gn
	)
{
	/*/
	gn.output("CI = SurfaceColor * SurfaceOpacity;");
	gn.output("OI = SurfaceOpacity;");
	/*/
}


/*/

SLIM_SHADERTYPE
SLIM_INSTANCENAME (
)
{

/* static functions ----------------------*

/* dynamic functions ------------------------*

/* local variables --------------------------*

/* main body --------------------------------*
  point PP = P;
  /* generate (all) ----------- *
  normal Nn = normalize(N);
  vector Nf = Nn * ( .1 * 0 ) /
              length(vtransform("shader", Nn));
  PP += Nf;
  if(0 != 0) { 
      normal deltaN = Nn - normalize(Ng);
      N = normalize(calculatenormal(PP)) + deltaN;
  } else {
      N = calculatenormal(PP);
  }
  if(0 != 0)
      P = PP;
}












#define SLIM_TYPEID_surface 0
#define SLIM_TYPEID_displacement 1
#define SLIM_TYPEID_volume 2
#define SLIM_TYPEID_light 3
#define SLIM_VERSION 650
#define SLIM_SHADERTYPE displacement
#define SLIM_SHADERTYPEID SLIM_TYPEID_displacement
#define SLIM_INSTANCETYPE displacement
#define SLIM_INSTANCENAME Simple

SLIM_SHADERTYPE
SLIM_INSTANCENAME (
  string pixar_SurfacePoint_Space="";
  varying point __Pref=point(-1e10,-1e10,-1e10);
)
{


#include "pxslUtil.h"
    	void
	pxslFNoise( float frequency;
	    	    point p;
		    vector dpu;
		    vector dpv;
		    output float result;
		   )
	{
	    point pp = frequency * p;
	    vector dppu = frequency * dpu;
	    vector dppv = frequency * dpv;
	    result = pxslFilteredFNoise(pp, dppu, dppv);
	}
    

		void pxslSurfacePoint (
		    uniform string coordsys;
		    uniform float frequency;
		    uniform float ignorePref;
		    output point Q; 
		    output vector dQu;
		    output vector dQv;
		) 
		{
		    uniform string sys;
#if SLIM_SHADERTYPEID == SLIM_TYPEID_volume
                    extern point Pv;
                    if(coordsys == "")
                        sys = "shader";
                    else
                        sys = coordsys;
                    Q = frequency * transform(sys, Pv);
                    dQu = 0;
                    dQv = 0;
#else           
		    extern point P, Ps;
		    extern vector dPdu, dPdv;
		    extern point __Pref;
		    extern float du, dv;

		    if(coordsys == "")
		    	sys = "shader";
		    else
		    	sys = coordsys;
#if SLIM_SHADERTYPEID == SLIM_TYPEID_light
		    if (ignorePref == 0 && 1 == surface("__Pref", Q)) 
		    {
		        if (xcomp(Q) == -1e10 ) {
		            Q = frequency * transform(sys, Ps);
		        }
		        else
		        {
		            Q = frequency * transform(sys, Q);
		        }
		    }
		    else
		    {
		    	Q = frequency * transform(sys, Ps);
		    }
		    dQu = vector Du(Q)*du;
		    dQv = vector Dv(Q)*dv;
#else
		    if (xcomp(__Pref) == -1e10 || ignorePref != 0)
		    {
			Q = frequency * transform (sys, P);
		    	dQu = vtransform (sys, dPdu*du*frequency);
		    	dQv = vtransform (sys, dPdv*dv*frequency);
		    }
		    else 
		    {
			Q = frequency * transform (sys, __Pref);
			dQu = vector Du(Q)*du;
		        dQv = vector Dv(Q)*dv;
		    }
#endif
#endif
		}
	    

  float tmp3;
  point tmp0;
  vector tmp1;
  vector tmp2;

  point PP = P;
  pxslSurfacePoint ( /* SurfacePoint *
    pixar_SurfacePoint_Space, /* Space *
    1, /* Frequency *
    0, /* IgnorePref *
    tmp0, /* Q * 
    tmp1, /* dQu * 
    tmp2  /* dQv * 
    );
  pxslFNoise ( /* Noise *
    1, /* frequency *
    tmp0, /* p * 
    tmp1, /* dpu * 
    tmp2, /* dpv * 
    tmp3  /* result * 
    );
  normal Nn = normalize(N);
  vector Nf = Nn * ( .1 * tmp3 ) /
              length(vtransform("shader", Nn));
  PP += Nf;
  if(0 != 0) { 
      normal deltaN = Nn - normalize(Ng);
      N = normalize(calculatenormal(PP)) + deltaN;
  } else {
      N = calculatenormal(PP);
  }
  if(0 != 0)
      P = PP;
}

/*/
