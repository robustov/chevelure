#include "StdAfx.h"
#include ".\st.h"

ST theST;
extern "C"
{
	// Constant
	__declspec(dllexport) shn::IShaderTemplate* __cdecl ST()
	{
		return &theST;
	}
}

ST::ST(void)
{
	Init();
}

ST::~ST(void)
{
}

void ST::Init()
{
	// Angle
	Angle.Init(
		"angle",
		"", 
		0.0f
		);
	Angle.SetRange( 0, 360);


	// repeatS, repeatT
	repeatS.Init(
		"repeatS",
		"", 
		1.0f
		);
	repeatS.SetRange( -10, 10);
	repeatT.Init(
		"repeatT",
		"", 
		1.0f
		);
	repeatT.SetRange( -10, 10);


	// offsetS, offsetT
	offsetS.Init(
		"offsetS",
		"", 
		0.0f
		);
	offsetS.SetRange( -10, 10);
	offsetT.Init(
		"offsetT",
		"", 
		0.0f
		);
	offsetT.SetRange( -10, 10);


	Manifold.Init(
		"Manifold",
		"Q", 
		"dQu",
		"dQv"
		);

}

shn::Parameter* ST::Parameters(
	shn::IShadingGraphNode& graphnode
	)
{
	graphnode.RegisterInput(Angle);
	graphnode.RegisterInput(repeatS);
	graphnode.RegisterInput(repeatT);
	graphnode.RegisterInput(offsetS);
	graphnode.RegisterInput(offsetT);
	return &Manifold;
}

// 
void ST::Build(
	shn::IShadingGraphNode& gn)
{
	gn.Add("extern float s, t, du, dv;");
	gn.Add("setxcomp(Q, repeatS * s + offsetS);");
	gn.Add("setycomp(Q, repeatT * t + offsetT);");
	gn.Add("setzcomp(Q, 0);");
	gn.Add("if(angle != 0)");
	gn.Add("    Q = rotate(Q, radians(angle), point(0,0,0), point(0,0,1));");
	gn.Add("dQu = vector Du(Q)*du;");
	gn.Add("dQv = vector Dv(Q)*dv;");
}
