#pragma once
#include "shading/IShaderTemplate.h"

class SurfaceNormal : public shn::IShaderTemplate
{
public:
	SurfaceNormal(void);
	~SurfaceNormal(void);
// ���������:
public:
	shn::BoolParameter normalize;
	shn::NormalParameter output;

public:
	void Init();
public:
	//! ��� �������
	virtual enTemplateType GetType(
		){return shn::IShaderTemplate::NORMAL;};
	virtual shn::Parameter* Parameters(
		shn::IShadingGraphNode& graphnode);
	virtual void Build(
		shn::IShadingGraphNode& graphnode);

};
