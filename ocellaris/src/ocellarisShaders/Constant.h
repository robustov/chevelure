#pragma once
#include "shading/IShaderTemplate.h"
#include "shading/IShaderParameter.h"

class Constant : public shn::IShaderTemplate
{
public:
	shn::ColorParameter color;
	shn::ColorParameter opacity;


	shn::SurfaceOutput output;


	Constant(void);
	~Constant(void);
	void Init();

	//! ��� �������
	virtual enTemplateType GetType(
		){return shn::IShaderTemplate::SURFACESHADER;};

	virtual shn::Parameter* Parameters(
		shn::IShadingGraphNode& graphnode);
	virtual void Build(
		shn::IShadingGraphNode& graphnode);
};
