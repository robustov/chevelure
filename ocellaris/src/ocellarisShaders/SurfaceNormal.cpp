#include "StdAfx.h"
#include ".\SurfaceNormal.h"

SurfaceNormal theSurfaceNormal;
extern "C"
{
	// Constant
	__declspec(dllexport) shn::IShaderTemplate* __cdecl SurfaceNormal()
	{
		return &theSurfaceNormal;
	}
}

SurfaceNormal::SurfaceNormal(void)
{
	Init();
}

SurfaceNormal::~SurfaceNormal(void)
{
}
// ���������� 1 ���: ����� ��������
void SurfaceNormal::Init(void)
{
	normalize.Init("normalize", "normalize", false);
	output.Init("result");
}


shn::Parameter* SurfaceNormal::Parameters(
	shn::IShadingGraphNode& graphnode
	)
{
	graphnode.RegisterInput(normalize);
	return &output;
}

// ���������� ��� ��������� ���� �������
void SurfaceNormal::Build(
	shn::IShadingGraphNode& gn
	)
{
	gn.output("extern normal N;");
	gn.output("if(normalize == 0)");
	gn.output("	result = N;");
	gn.output("	else");
	gn.output("	result = normalize(N);");
}

