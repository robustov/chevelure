#include "StdAfx.h"
#include ".\plastic.h"

Plastic thePlastic;
extern "C"
{
	// Constant
	__declspec(dllexport) shn::IShaderTemplate* __cdecl Plastic()
	{
		return &thePlastic;
	}
}

Plastic::Plastic(void)
{
	Init();
}

Plastic::~Plastic(void)
{
}
// ���������� 1 ���: ����� ��������
void Plastic::Init(void)
{
	opacity.Init(
		"SurfaceOpacity", 
		"Opacity", 
		Math::Vec3f(1.f, 1.f, 1.f), 
		shn::CONNECTABLE);
	Ka.Init(
		"Ka", 
		"Ka", 
		0.05f, 
		shn::CONNECTABLE);
	ambient.Init(
		"Ambient", 
		"Ambient", 
		Math::Vec3f(1.f, 1.f, 1.f), 
		shn::CONNECTABLE);

	// diffuse
	Kd.Init(
		"Kd", 
		"Kd", 
		1.f, 
		shn::CONNECTABLE);
	diffuse.Init(
		"Diffuse", 
		"Diffuse", 
		Math::Vec3f(1.f, 1.f, 1.f), 
		shn::CONNECTABLE);
	diffuse_normal.Init(
		"DiffuseNormal", 
		"DiffuseNormal", 
		Math::Vec3f(1.f, 0.f, 0.f), 
		shn::CONNECTABLE);
	diffuse_normal.DefConnectTo("SurfaceNormal");

	// specular
	Ks.Init(
		"Ks", 
		"Ks", 
		1.f, 
		shn::CONNECTABLE);
	specular.Init(
		"Specular", 
		"Specular", 
		Math::Vec3f(1.f, 1.f, 1.f), 
		shn::CONNECTABLE);
	roughness.Init(
		"Roughness", 
		"Roughness", 
		0.1f, 
		shn::CONNECTABLE);
	specular_normal.Init(
		"SpecularNormal", 
		"SpecularNormal", 
		Math::Vec3f(1.f, 0.f, 0.f), 
		shn::CONNECTABLE);
	specular_normal.DefConnectTo("SurfaceNormal");

	output.Init();
}
shn::Parameter* Plastic::Parameters(
	shn::IShadingGraphNode& graphnode
	)
{
	graphnode.RegisterInput(opacity);
	graphnode.RegisterInput(Ka);
	graphnode.RegisterInput(ambient);

	// diffuse
	graphnode.RegisterInput(Kd);
	graphnode.RegisterInput(diffuse);
	graphnode.RegisterInput(diffuse_normal);

	// specular
	graphnode.RegisterInput(Ks);
	graphnode.RegisterInput(specular);
	graphnode.RegisterInput(roughness);
	graphnode.RegisterInput(specular_normal);

	return &output;
}
// ���������� ��� ��������� ���� �������
void Plastic::Build(
	shn::IShadingGraphNode& gn
	)
{
	gn.output("CI = SurfaceColor * SurfaceOpacity;");
	gn.output("OI = SurfaceOpacity;");
}

