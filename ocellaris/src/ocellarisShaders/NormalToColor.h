#pragma once
#include "shading/IShaderTemplate.h"

class NormalToColor : public shn::IShaderTemplate
{
public:
	NormalToColor(void);
	~NormalToColor(void);
// ���������:
public:
	shn::NormalParameter normal;
	shn::StringParameter space;
	shn::ColorParameter output;

public:
	void Init();
public:
	//! ��� �������
	virtual enTemplateType GetType(
		){return shn::IShaderTemplate::COLOR;};
	virtual shn::Parameter* Parameters(
		shn::IShadingGraphNode& graphnode);
	virtual void Build(
		shn::IShadingGraphNode& graphnode);

};
