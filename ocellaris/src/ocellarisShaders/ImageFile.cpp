#include "StdAfx.h"
#include ".\imagefile.h"

ImageFile theImageFile;
extern "C"
{
	// Constant
	__declspec(dllexport) shn::IShaderTemplate* __cdecl ImageFile()
	{
		return &theImageFile;
	}
}

ImageFile::ImageFile(void)
{
	Init();
}

ImageFile::~ImageFile(void)
{
}

// ���������� 1 ���: ����� ��������
void ImageFile::Init(void)
{
	// ImageFileName
	ImageFileName.Init(
		"File", 
		"Choose an image file", 
		"grid.tex");

	// AlphaOp
	AlphaOp.Init(
		"AlphaOp", 
		"If your fourth channel is a true alpha channel", 
		0, 
		shn::OPTION);
	AlphaOp.SetField( 0, "No Op");
	AlphaOp.SetField( 1, "Apply Mask");
	AlphaOp.SetField( 2, "Unassociate Alpha");

	// SFilt
	SFilt.Init(
		"SFilt",
		"The amount to overfilter in the S direction. 1 is standard filtering.", 
		1.0
		);
	SFilt.SetRange( 0, 16);

	// TFilt
	TFilt.Init(
		"TFilt",
		"The amount to overfilter in the T direction. 1 is  standard filtering.", 
		1.0
		);
	TFilt.SetRange( 0, 16);

	Filter.Init(
		"Filter", 
		"Use of gaussian should produce higher quality results. \
		The disk filter is optimized for large blur sizes, and \
		(unlike the other filters) is free of mipmap artifacts \
		because it uses all mipmap levels in its computations; \
		it is about twice as expensive as the other filters. \
		The radial-bspline filter is specifically designed for\
		optimally filtering displacement textures.", 
		"box"
		);
	Filter.SetField("box","box");
	Filter.SetField("gaussian","gaussian");
	Filter.SetField("disk","disk");
	Filter.SetField("radial-bspline","radial-bspline");

	lerp.Init(
		"lerp", 
		"Selects whether to interpolate between adjacent resolutions\
		of a multi-resolution texture in order to smooth the transition\
		between resolutions.",
		false
		);
	ColorBias.Init(
		"ColorBias", 
		"A normalized gamma correction factor.\
		Values greater than .5 lighten the result, \
		values less than .5 make it darker.", 
		Math::Vec3f(.5f, .5f, .5f));
	ColorGain.Init(
		"ColorGain", 
		"Gain is used to favor dark area when less\
		than .5 or light areas when greater than .5",
		Math::Vec3f(.5f, .5f, .5f));

	Manifold.Init(
		"Manifold",
		"Pt", 
		"dPdu",
		"dPdv"
		);
	Manifold.DefConnectTo("ST");

	result.Init(
		"result", 
		"result");
}

shn::Parameter* ImageFile::Parameters(
	shn::IShadingGraphNode& graphnode
	)
{
	graphnode.RegisterInput(ImageFileName);
	graphnode.RegisterInput(AlphaOp);

	graphnode.RegisterInput(SFilt);
	graphnode.RegisterInput(TFilt);
	graphnode.RegisterInput(Filter);
	graphnode.RegisterInput(lerp);
	graphnode.RegisterInput(ColorBias);
	graphnode.RegisterInput(ColorGain);
	graphnode.RegisterInput(Manifold);

	return &result;
}

// 
void ImageFile::Build(
	shn::IShadingGraphNode& gn)
{
	gn.Add("if (File != \"\") ");
	gn.Add("{");
	gn.Add("	result = color texture(File, ");
	gn.Add("	xcomp(Pt),");
	gn.Add("	ycomp(Pt),");
	gn.Add("	\"swidth\", SFilt,");
	gn.Add("	\"twidth\", TFilt,");
	gn.Add("	\"lerp\", lerp);");
	gn.Add("} ");
	gn.Add("else");
	gn.Add("{");
	gn.Add("    result = color(1,1,1);");
	gn.Add("}");

	int alphaOp;
	gn.getValue(AlphaOp, alphaOp);
	if( alphaOp != 0)
	{
		gn.Add("\
		uniform float nChannels=3;\
		textureinfo(File, \"channels\", nChannels);\
		if( nChannels > 3 )\
		{\
			float a = float texture(File[3],\
				xcomp(Pt),\
				ycomp(Pt),\
				\"swidth\", SFilt,\
				\"twidth\", TFilt,\
				\"filter\", filter,\
				\"lerp\", lerp);\
			if(AlphaOp == \"mask\")\
			{\
				result *= a;\
			}\
			else\
			{\
				/* assume AlphaOp == unassociated */\
				if( a != 0 )\
				{\
					result /= a;\
					result = clamp(result, color(0), color(1));\
				}\
			}\
		}\
		");

	}
	gn.Include( "pxslRemap.h");
	gn.Add("result = pxslColorBias(ColorBias, result);");
	gn.Add("result = pxslColorGain(ColorGain, result);");
}