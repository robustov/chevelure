#pragma once
#include "shading/IShaderTemplate.h"

class ST : public shn::IShaderTemplate
{
public:
	ST(void);
	~ST(void);

// параметры:
public:
	shn::FloatParameter Angle;
	shn::FloatParameter repeatS, repeatT;
	shn::FloatParameter offsetS, offsetT;

	shn::ManifoldParameter Manifold;				// UV преобразователь

public:
	void Init();
public:
	//! тип шаблона
	virtual enTemplateType GetType(
		){return shn::IShaderTemplate::MANIFOLD;};
	virtual shn::Parameter* Parameters(
		shn::IShadingGraphNode& graphnode);
	virtual void Build(
		shn::IShadingGraphNode& graphnode);

};
