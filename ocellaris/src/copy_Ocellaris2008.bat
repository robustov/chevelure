set BIN_DIR=.\out\ocellaris
set BIN_DIR=\\server\bin

if "%DSTPATH%"=="" (set BIN_DIR=%BIN_DIR%) else (set BIN_DIR=%DSTPATH%)

rem SN
mkdir %BIN_DIR%
mkdir %BIN_DIR%\bin
mkdir %BIN_DIR%\mel
mkdir %BIN_DIR%\python
mkdir %BIN_DIR%\include

%BIN_DIR%\bin\svn\svn.exe -m "" commit %BIN_DIR%

rem copy BinRelease\OcellarisAutoExp.dll "C:\Program Files\Microsoft Visual Studio .NET 2003\Common7\IDE"

copy bin\OcellarisRibGen.slim				%BIN_DIR%\bin

cd /D %ULITKABIN%
copy binrelease85\OcellarisExport.dll				%BIN_DIR%\bin
copy binrelease85\OcellarisMaya.mll					%BIN_DIR%\bin
copy BinRelease\ocellarisCmd.exe					%BIN_DIR%\bin
copy BinRelease85\ocellarisMayaCmd.exe				%BIN_DIR%\bin
copy BinRelease\OcellarisPrmanDSO.dll				%BIN_DIR%\bin
copy BinRelease\OcellarisProc.dll					%BIN_DIR%\bin
copy BinRelease\ocellarisShaders.dll				%BIN_DIR%\bin
copy BinRelease\OcellarisRibGen.dll					%BIN_DIR%\bin
copy BinRelease\OcellarisTest.exe					%BIN_DIR%\bin
copy BinRelease\ocellarisCropAsm.exe				%BIN_DIR%\bin


copy BinRelease\IPrman13.5.dll						%BIN_DIR%\bin\IPrman.dll
copy BinRelease\IPrmanDSO13.5.dll					%BIN_DIR%\bin\IPrmanDSO.dll
copy BinRelease\IPrman12.5.dll						%BIN_DIR%\bin\
copy BinRelease\IPrman13.5.dll						%BIN_DIR%\bin
copy BinRelease\IPrman14.0.dll						%BIN_DIR%\bin
copy BinRelease\IPrmanDSO13.5.dll					%BIN_DIR%\bin
copy BinRelease\IPrmanDSO14.0.dll					%BIN_DIR%\bin

copy python\ocsRender.py					%BIN_DIR%\python
copy python\ocsExport.py					%BIN_DIR%\python
copy python\ocsCommands.py					%BIN_DIR%\python

copy Mel\OcellarisMayaMenu.Mel				%BIN_DIR%\mel
copy Mel\ocTranslatorOptions.mel			%BIN_DIR%\mel
copy Mel\sloTranslatorOptions.mel			%BIN_DIR%\mel
copy Mel\ocellarisPassesMenu.mel			%BIN_DIR%\mel
copy Mel\ocellarisShadersMenu.mel			%BIN_DIR%\mel

rem AE
copy Mel\AEocRootTransformTemplate.mel		%BIN_DIR%\mel
copy Mel\AEocOcclusionPassTemplate.mel		%BIN_DIR%\mel
copy Mel\AEocShaderParameterTemplate.mel	%BIN_DIR%\mel
copy Mel\AEocFileTemplate.mel				%BIN_DIR%\mel
copy Mel\AEocInstanceTemplate.mel			%BIN_DIR%\mel
copy Mel\AEocGeneratorSetTemplate.mel		%BIN_DIR%\mel
copy Mel\AEocJointVisTemplate.mel			%BIN_DIR%\mel
copy Mel\AEocRenderPassTemplate.mel			%BIN_DIR%\mel
copy Mel\AEocLightTemplate.mel				%BIN_DIR%\mel
copy Mel\AEocSequenceFileTemplate.mel		%BIN_DIR%\mel
copy Mel\AEocSurfaceShaderTemplate.mel		%BIN_DIR%\mel
copy Mel\AEocGeneratorProxyTemplate.mel		%BIN_DIR%\mel
copy Mel\AEocCustomPassTemplate.mel			%BIN_DIR%\mel

rem include
copy include\bake3dNormal.h					%BIN_DIR%\include
copy include\hairShaderUtils.h				%BIN_DIR%\include
copy include\LambertOcs.h					%BIN_DIR%\include
copy include\PhongOcs.h						%BIN_DIR%\include
copy include\OcclusionOcs.h					%BIN_DIR%\include
copy include\SpotLigthOcs.h					%BIN_DIR%\include
copy include\utilsOcs.h						%BIN_DIR%\include
copy include\ul_pxslRemap.h					%BIN_DIR%\include
copy include\ul_patterns.h					%BIN_DIR%\include
copy include\ul_pxslUtil.h					%BIN_DIR%\include
copy include\ul_noises.h					%BIN_DIR%\include
copy include\ul_filterwidth.h				%BIN_DIR%\include

rem PHYSX
rem copy binrelease85\ocellarisPhysXMaya.mll				%BIN_DIR%\bin
rem copy BinRelease\ocellarisPhysX.dll					%BIN_DIR%\bin
rem copy BinRelease\ragdoll.exe						%BIN_DIR%\bin
rem copy Mel\AEocPhysXActorTemplate.mel			%BIN_DIR%\mel
rem copy Mel\AEocPhysXShapeTemplate.mel			%BIN_DIR%\mel
rem copy Mel\ocellarisPhysXMayaMenu.mel			%BIN_DIR%\mel
rem copy Mel\AEocPhysXJointTemplate.mel			%BIN_DIR%\mel

if "%DSTPATH%"=="" (pause) else (set BIN_DIR=%BIN_DIR%)

