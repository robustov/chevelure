// OcellarisGelatoDSO.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include <gelatoapi.h>
#include "ocellaris/renderGelatoImpl.h"
#include "ocellaris/renderFileImpl.h"

cls::renderGelatoImpl<> gelatorender;

class OcsGenerator : public GelatoAPI::Generator 
{
public:
	std::string filename;
	OcsGenerator(const char *command) 
	{
		filename = command;
	}
	~OcsGenerator() {}
	bool bound (float *bbox) 
	{
		cls::renderFileImpl file;
		if(!file.openForRead(filename.c_str()))
			return false;

		Math::Box3f box;
		if( !file.GetAttribute("file::box", box))
			return false;

		bbox[0] = box.min.x;  bbox[1] = box.max.x;
		bbox[2] = box.min.y;  bbox[3] = box.max.y;
		bbox[4] = box.min.z;  bbox[5] = box.max.z;
		return true;
	}
	void run( GelatoAPI *r, const char *params) 
	{
		gelatorender.setGelato(r);
		cls::IRender* render = &gelatorender;
		render->Parameter("filename", params);
		render->RenderCall("ReadOCS");
	}
};

#define GELATO_EXPORT __declspec(dllexport)
extern "C"
{
	GELATO_EXPORT GelatoAPI::Generator* ocs_generator_create(const char *command)
	{
		return new OcsGenerator(command);
	}
	GELATO_EXPORT int generator_version = GelatoAPI::API_VERSION;
}

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    return TRUE;
}

