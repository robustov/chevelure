; example2.nsi
;
; This script is based on example1.nsi, but it remember the directory, 
; has uninstall support and (optionally) installs start menu shortcuts.
;
; It will install example2.nsi into a directory that the user selects,

;--------------------------------
!include "WordFunc.nsh"
!include "TextFunc.nsh"

!insertmacro un.WordAdd


!ifdef MAYA70
	Name "ocellaris70"
	OutFile "$%ULITKABIN%\ocellaris70.exe"
!endif
!ifdef MAYA2008
	Name "ocellaris2008"
	OutFile "$%ULITKABIN%\ocellaris2008.exe"
!endif


; The default installation directory
InstallDir ""

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
!ifdef MAYA70
	InstallDirRegKey HKLM "Software\ulitkabin70" "Install_Dir"
!endif
!ifdef MAYA2008
	InstallDirRegKey HKLM "Software\ulitkabin2008" "Install_Dir"
!endif


;--------------------------------
; Pages

Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

; The stuff to install
Section "Ocllaris"

  SectionIn RO
  
  ;-----------------------------------------
  ; BIN
  SetOutPath $INSTDIR\bin
  
  File "$%ULITKABIN%\binrelease\IPrman.dll"
  
  ; �� ������� �� ������ ����
  File "$%ULITKABIN%\binrelease\ocellarisCropAsm.exe"
  File "$%ULITKABIN%\binrelease\ocellarisPhysX.dll"
  File "$%ULITKABIN%\binrelease\ocellarisPrmanDSO.dll"
  File "$%ULITKABIN%\binrelease\ocellarisProc.dll"
  File "$%ULITKABIN%\binrelease\ocellarisPrmanDSO.dll"
  File "$%ULITKABIN%\binrelease\ocellarisShaders.dll"
  File "$%ULITKABIN%\binrelease\ocellarisTest.exe"
  File "$%ULITKABIN%\binrelease\ragdoll.exe"

  ; ������
  File "$%ULITKABIN%\binrelease\ocellarisRibGen.dll"
  File "$%ULITKABIN%\bin\ocellarisRibGen.slim"
  
	!ifdef MAYA70
		File "$%ULITKABIN%\binrelease\ocellarisExport.dll" 
		File "$%ULITKABIN%\binrelease\ocellarisMaya.mll"
		File "$%ULITKABIN%\binrelease\ocellarisPhysXMaya.mll"
	!endif
	!ifdef MAYA2008
		File "$%ULITKABIN%\binrelease85\ocellarisExport.dll" 
		File "$%ULITKABIN%\binrelease85\ocellarisMaya.mll"
		File "$%ULITKABIN%\binrelease85\ocellarisPhysXMaya.mll"
	!endif
	
  ;-----------------------------------------
  ; MEL
  SetOutPath $INSTDIR\mel
	Delete "$INSTDIR\mel\ocellarisMayaMenu.mel"

	File "$%ULITKABIN%\mel\AEocRootTransformTemplate.mel"
	File "$%ULITKABIN%\mel\AEocOcclusionPassTemplate.mel"
	File "$%ULITKABIN%\mel\AEocShaderParameterTemplate.mel"
	File "$%ULITKABIN%\mel\AEocFileTemplate.mel"
	File "$%ULITKABIN%\mel\AEocInstanceTemplate.mel"
	File "$%ULITKABIN%\mel\ocellarisMayaMenu.mel"
	File "$%ULITKABIN%\mel\ocTranslatorOptions.mel"
	File "$%ULITKABIN%\mel\sloTranslatorOptions.mel"
	File "$%ULITKABIN%\mel\AEocGeneratorSetTemplate.mel"
	File "$%ULITKABIN%\mel\AEocJointVisTemplate.mel"
	File "$%ULITKABIN%\mel\AEocRenderPassTemplate.mel"
	File "$%ULITKABIN%\mel\AEocLightTemplate.mel"
	File "$%ULITKABIN%\mel\AEocSequenceFileTemplate.mel"
	File "$%ULITKABIN%\mel\AEocGeneratorProxyTemplate.mel"
  
  
	File "$%ULITKABIN%\mel\AEocPhysXActorTemplate.mel"
	File "$%ULITKABIN%\mel\AEocPhysXShapeTemplate.mel"
	File "$%ULITKABIN%\mel\ocellarisPhysXMayaMenu.mel"
	File "$%ULITKABIN%\mel\AEocPhysXJointTemplate.mel"
	
	;-----------------------------------------
	; SLIM
	SetOutPath $INSTDIR\slim
	File "$%ULITKABIN%\slim\ulLayerUltimoExt.slim"
	 
	;-----------------------------------------
	; Write the installation path into the registry
	!ifdef MAYA70
		WriteRegStr HKLM SOFTWARE\ulitkabin70 "Install_Dir" "$INSTDIR"
	!endif
	!ifdef MAYA2008
		WriteRegStr HKLM SOFTWARE\ulitkabin2008 "Install_Dir" "$INSTDIR"
	!endif
    
SectionEnd

;--------------------------------
; Uninstaller
Section "Uninstall"

SectionEnd
