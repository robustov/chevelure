// ocellarisCmd.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Util/misc_create_directory.h"

int openfile(int argc, _TCHAR* argv[]);
int readJob( const char* filename);
int waitJob( const char* filename);
int maketask( int argc, _TCHAR* argv[]);

int _tmain(int argc, _TCHAR* argv[])
{
	if(argc>2)
	{
		if(strcmp(argv[1], "-openfile")==0)
		{
			return openfile( argc-2, argv+2);
		}
		if(strcmp(argv[1], "-waitjob")==0)
		{
			return waitJob( argv[2]);
		}
		if(strcmp(argv[1], "-readjob")==0)
		{
			return readJob( argv[2]);
		}
	}
	return 0;
}

int openfile(int argc, _TCHAR* argv[])
{
	if(argc==1)
	{
		std::string filename = argv[0];
		Util::changeSlashToWindowsSlash(filename);
		system( ("start explorer \""+filename+"\"").c_str());
	}
	if(argc>=2)
	{
		std::string filename = argv[1];
		Util::changeSlashToWindowsSlash(filename);
		if( strcmp( argv[0], "-select")==0)
		{
			system( ("start explorer /select,\""+filename+"\"").c_str());
		}
	}
	return 0;
}


int waitJob( const char* filename)
{
	for(;;)
	{
		FILE* file = fopen(filename, "rt");
		float sec = 0;
		if(!file)
		{
			Sleep(250);
			continue;
		}
		fclose(file);
		fprintf(stderr, "%f sec\n", sec);
		fprintf(stderr, "%s opened\n", filename);
		break;
	}
	return 0;
}


int readJob( const char* filename)
{
	FILE* file = fopen(filename, "rt");
	if(!file)
	{
		fprintf(stderr, "Cant open %s \n", filename);
		return -1;
	}
	while( !feof( file ) )
	{
		int c = fgetc(file);
		if(c==EOF)
			break;
		fputc(c, stdout);
	}
	fputs("\n", stdout);
	fclose(file);

	fprintf(stderr, "%s readed\n", filename);
	return 0;
}
