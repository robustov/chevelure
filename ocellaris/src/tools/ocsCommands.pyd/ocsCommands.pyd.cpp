// ocsCommands.pyd.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "Python.h"
#include "Util\DllProcedure.h"
class MDagPath;
#include "ocellaris/IExportContext.h"

typedef cls::IExportContext* (*ocs_context_t)();
typedef int (*ocs_preparenodes_t)(const char* progresstitle, const char* progresstext);
typedef int (*ocs_export_t)(const char* dumpoption, const char* options, const char* progresstitle, const char* progresstext);

static PyObject* ocs_clear(PyObject *self, PyObject *args)
{
	Util::DllProcedureTmpl<ocs_context_t> dllproc("ocellarisMaya.mll", "ocs_context");
	if( !dllproc.isValid())
		return NULL;

	cls::IExportContext* context = (*dllproc)();
	if( !context)
		return NULL;

	context->clear();
	printf("clear!\n");
	fflush(stdout);
	Py_INCREF(Py_None);
	return Py_None;
}
static PyObject* ocs_getenv(PyObject *self, PyObject *args)
{
    char* env;

    if ( !PyArg_ParseTuple ( args, "s", &env) )
        return NULL;

	Util::DllProcedureTmpl<ocs_context_t> dllproc("ocellarisMaya.mll", "ocs_context");
	if( !dllproc.isValid())
		return NULL;

	cls::IExportContext* context = (*dllproc)();
	if( !context)
		return NULL;

	const char* res = context->getenv(env);
	printf("getenv %s=%s!\n", env, res);
	fflush(stdout);

	return Py_BuildValue("s", res);
}
static PyObject* ocs_setenv(PyObject *self, PyObject *args)
{
    char* env, *value;

    if ( !PyArg_ParseTuple ( args, "ss", &env, &value) )
        return NULL;

	Util::DllProcedureTmpl<ocs_context_t> dllproc("ocellarisMaya.mll", "ocs_context");
	if( !dllproc.isValid()) return NULL;

	cls::IExportContext* context = (*dllproc)();
	if( !context) return NULL;
	cls::IExportContext& exportContext = *context;

	int res = exportContext.setenv(env, value)?1:0;
	printf("setenv %s = %s!\n", env, value);
	fflush(stdout);

	return Py_BuildValue("i", res);
}


//EXPORT int ocs_addDefaultGenerators(const char* options)
static PyObject* ocs_addDefaultGenerators(PyObject *self, PyObject *args)
{
    char* options="";

    if ( !PyArg_ParseTuple ( args, "|s", &options) )
        return NULL;

	Util::DllProcedureTmpl<ocs_context_t> dllproc("ocellarisMaya.mll", "ocs_context");
	if( !dllproc.isValid()) return NULL;

	cls::IExportContext* context = (*dllproc)();
	if( !context) return NULL;
	cls::IExportContext& exportContext = *context;

	printf("addDefaultGenerators %s !\n", options);
	int res = exportContext.addDefaultGenerators(options)?1:0;
	return Py_BuildValue("i", res);
}
//EXPORT int ocs_addGeneratorForType(const char* type, const char* generator)
static PyObject* ocs_addGeneratorForType(PyObject *self, PyObject *args)
{
	Util::DllProcedureTmpl<ocs_context_t> dllproc("ocellarisMaya.mll", "ocs_context");
	if( !dllproc.isValid()) return NULL;

	cls::IExportContext* context = (*dllproc)();
	if( !context) return NULL;
	cls::IExportContext& exportContext = *context;

	char* type, *generator;
    if ( !PyArg_ParseTuple ( args, "ss", &type, &generator) )
        return NULL;

	printf("addGeneratorForType %s %s !\n", type, generator);
	int res = exportContext.addGeneratorForType(type, generator)?1:0;
	return Py_BuildValue("i", res);
}
//EXPORT int ocs_addnodes(const char* root, const char* options)
static PyObject* ocs_addnodes(PyObject *self, PyObject *args)
{
	Util::DllProcedureTmpl<ocs_context_t> dllproc("ocellarisMaya.mll", "ocs_context");
	if( !dllproc.isValid()) return NULL;

	cls::IExportContext* context = (*dllproc)();
	if( !context) return NULL;
	cls::IExportContext& exportContext = *context;

	char* root="", *options="";
    if ( !PyArg_ParseTuple ( args, "|ss", &root, &options) )
        return NULL;

	printf("addnodes %s %s!\n", root, options);
	int res = 0;
	if( exportContext.addnodes(root, options))
		res = 1;
	return Py_BuildValue("i", res);
}
//EXPORT int ocs_addpass(const char* passnode, const char* options)
static PyObject* ocs_addpass(PyObject *self, PyObject *args)
{
	Util::DllProcedureTmpl<ocs_context_t> dllproc("ocellarisMaya.mll", "ocs_context");
	if( !dllproc.isValid()) return NULL;

	cls::IExportContext* context = (*dllproc)();
	if( !context) return NULL;
	cls::IExportContext& exportContext = *context;

	char* passnode="", *options="";
    if ( !PyArg_ParseTuple ( args, "|ss", &passnode, &options) )
        return NULL;

	printf("addpass %s %s!\n", passnode, options);
	int res = exportContext.addpass(passnode, options);
	return Py_BuildValue("i", res);
}
//EXPORT int ocs_addpassgenerator(const char* passgenerator, const char* options)
static PyObject* ocs_addpassgenerator(PyObject *self, PyObject *args)
{
	Util::DllProcedureTmpl<ocs_context_t> dllproc("ocellarisMaya.mll", "ocs_context");
	if( !dllproc.isValid()) return NULL;

	cls::IExportContext* context = (*dllproc)();
	if( !context) return NULL;
	cls::IExportContext& exportContext = *context;

	char* passgenerator="", *options="";
    if ( !PyArg_ParseTuple ( args, "|ss", &passgenerator, &options) )
        return NULL;

	cls::IGenerator* pass = exportContext.loadGenerator("SimpleExportPass", cls::IGenerator::PASS);
	if( !pass)
		return 0;
	int res = exportContext.addpass((cls::IPassGenerator*)pass, options);
	return Py_BuildValue("i", res);
}
//EXPORT int ocs_connectpass(const char* mainpass, const char* subpass, const char* options)
static PyObject* ocs_connectpass(PyObject *self, PyObject *args)
{
	Util::DllProcedureTmpl<ocs_context_t> dllproc("ocellarisMaya.mll", "ocs_context");
	if( !dllproc.isValid()) return NULL;

	cls::IExportContext* context = (*dllproc)();
	if( !context) return NULL;
	cls::IExportContext& exportContext = *context;

	char* mainpass, *subpass, *options="";
    if ( !PyArg_ParseTuple ( args, "ss|s", &mainpass, &subpass, &options) )
        return NULL;

	printf("connectpass %s->%s %s!\n", mainpass, subpass, options);
	int res = exportContext.connectPass(mainpass, subpass, options);
	return Py_BuildValue("i", res);
}
//EXPORT int ocs_addlight(const char* node, const char* options)
static PyObject* ocs_addlight(PyObject *self, PyObject *args)
{
	Util::DllProcedureTmpl<ocs_context_t> dllproc("ocellarisMaya.mll", "ocs_context");
	if( !dllproc.isValid()) return NULL;

	cls::IExportContext* context = (*dllproc)();
	if( !context) return NULL;
	cls::IExportContext& exportContext = *context;

	char* node="", *options="";
    if ( !PyArg_ParseTuple ( args, "|ss", &node, &options) )
        return NULL;

	printf("addlight %s %s!\n", node, options);
	int res = exportContext.addlight(node, options);
	return Py_BuildValue("i", res);
}
//EXPORT int ocs_addframe(float frame)
static PyObject* ocs_addframe(PyObject *self, PyObject *args)
{
	Util::DllProcedureTmpl<ocs_context_t> dllproc("ocellarisMaya.mll", "ocs_context");
	if( !dllproc.isValid()) return NULL;

	cls::IExportContext* context = (*dllproc)();
	if( !context) return NULL;
	cls::IExportContext& exportContext = *context;

	float frame;
    if ( !PyArg_ParseTuple ( args, "f", &frame) )
        return NULL;

	printf("addframe %f\n", frame);

	int res = exportContext.addframe(frame);
	return Py_BuildValue("i", res);
}
//EXPORT int ocs_preparegenerators()
static PyObject* ocs_preparegenerators(PyObject *self, PyObject *args)
{
	Util::DllProcedureTmpl<ocs_context_t> dllproc("ocellarisMaya.mll", "ocs_context");
	if( !dllproc.isValid()) return NULL;

	cls::IExportContext* context = (*dllproc)();
	if( !context) return NULL;
	cls::IExportContext& exportContext = *context;

	printf("ocs_preparegenerators\n");
	int res = exportContext.prepareGenerators();
	return Py_BuildValue("i", res);
}

//EXPORT int ocs_preparenodes(const char* progresstitle, const char* progresstext)
static PyObject* ocs_preparenodes(PyObject *self, PyObject *args)
{
	Util::DllProcedureTmpl<ocs_preparenodes_t> dllproc("ocellarisMaya.mll", "ocs_preparenodes");
	if( !dllproc.isValid()) return NULL;

	char* progresstitle="", *progresstext="";
    if ( !PyArg_ParseTuple ( args, "|ss", &progresstitle, &progresstext) )
        return NULL;

	int res = (*dllproc)(progresstitle, progresstext);
	return Py_BuildValue("i", res);
}
//EXPORT int ocs_export(const char* dumpoption, const char* options, const char* progresstitle, const char* progresstext)
static PyObject* ocs_export(PyObject *self, PyObject *args)
{
	Util::DllProcedureTmpl<ocs_export_t> dllproc("ocellarisMaya.mll", "ocs_export");
	if( !dllproc.isValid()) return NULL;

	char* dumpoption="", *options="", *progresstitle="", *progresstext="";
    if ( !PyArg_ParseTuple ( args, "|ssss", &dumpoption, &options, &progresstitle, &progresstext) )
        return NULL;

	int res = (*dllproc)(dumpoption, options, progresstitle, progresstext);
	return Py_BuildValue("i", res);
}
//EXPORT int ocs_render(const char* dumpoption)
static PyObject* ocs_render(PyObject *self, PyObject *args)
{
	Util::DllProcedureTmpl<ocs_context_t> dllproc("ocellarisMaya.mll", "ocs_context");
	if( !dllproc.isValid()) return NULL;

	cls::IExportContext* context = (*dllproc)();
	if( !context) return NULL;
	cls::IExportContext& exportContext = *context;

	char* dumpoption="";
    if ( !PyArg_ParseTuple ( args, "|s", &dumpoption) )
        return NULL;

	printf("render\n");

	int res = exportContext.render(dumpoption);
	return Py_BuildValue("i", res);
}

// EXPORT int ocs_dump(const char* options)
static PyObject* ocs_dump(PyObject *self, PyObject *args)
{
	Util::DllProcedureTmpl<ocs_context_t> dllproc("ocellarisMaya.mll", "ocs_context");
	if( !dllproc.isValid()) return NULL;

	cls::IExportContext* context = (*dllproc)();
	if( !context) return NULL;
	cls::IExportContext& exportContext = *context;

	char* options="";
    if ( !PyArg_ParseTuple ( args, "|s", &options) )
        return NULL;

	exportContext.dump(options);
	return Py_BuildValue("i", 1);
}




static PyMethodDef ocsCommands_methods[] = {
	{"clear", ocs_clear, METH_VARARGS, "ocs_clear()"},
	{"getenv", ocs_getenv, METH_VARARGS, "ocs_getenv()"},
	{"setenv", ocs_setenv, METH_VARARGS, "ocs_setenv()"},
	{"addDefaultGenerators", ocs_addDefaultGenerators, METH_VARARGS, "addDefaultGenerators"},
	{"addGeneratorForType", ocs_addGeneratorForType, METH_VARARGS, "addGeneratorForType"},
	{"addnodes", ocs_addnodes, METH_VARARGS, "addnodes"},
	{"addpass", ocs_addpass, METH_VARARGS, "addpass"},
	{"addpassgenerator", ocs_addpassgenerator, METH_VARARGS, "addpassgenerator"},
	{"connectpass", ocs_connectpass, METH_VARARGS, "connectpass"},
	{"addlight", ocs_addlight, METH_VARARGS, "addlight"},
	{"addframe", ocs_addframe, METH_VARARGS, "addframe"},
	{"preparegenerators", ocs_preparegenerators, METH_VARARGS, "preparegenerators"},
	{"preparenodes", ocs_preparenodes, METH_VARARGS, "preparenodes"},
	{"export", ocs_export, METH_VARARGS, "export"},
	{"render", ocs_render, METH_VARARGS, "render"},
	{"dump", ocs_dump, METH_VARARGS, "dump"},

	{NULL, NULL}
};

PyMODINIT_FUNC
initocsCommands(void)
{
	Py_InitModule("ocsCommands", ocsCommands_methods);
}



