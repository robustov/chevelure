// ocellarisCropAsm.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>

#include <Math/matrixMxN.h>
#include <Util/math_bmpMatrixMN.h>

int BuildExr(int X, int Y, Math::matrixMxN<std::string >& filenamess, const char* outfilename);
bool LoadEXR( const char* filename, Math::matrixMxN<Util::frgba>& m );
bool SaveEXR( const char* filename, Math::matrixMxN<Util::frgba>& m );

bool LoadTiff(const char* filename, Math::matrixMxN<Util::frgba>& m);
bool TiffSave(const char* filename, Math::matrixMxN<Util::frgba>& m );

int BuildMov(int argc, _TCHAR* argv[]);

// 8 4 C:\RAT-200_v6\crop.0.0\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.0.1\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.0.2\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.0.3\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.0.4\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.0.5\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.0.6\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.0.7\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.0\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.1\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.2\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.3\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.4\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.5\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.6\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.7\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.0\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.1\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.2\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.3\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.4\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.5\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.6\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.7\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.3.0\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.3.1\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.3.2\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.3.3\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.3.4\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.3.5\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.3.6\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.3.7\RAT-200_v6.0001.tif C:\RAT-200_v6\RAT-200_v6.0001.tif
// 8 1 C:\RAT-200_v6\crop.1.0\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.1\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.2\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.3\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.4\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.5\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.6\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.7\RAT-200_v6.0001.tif C:\RAT-200_v6\RAT-200_v6.1.0001.tif
// 8 1 C:\RAT-200_v6\crop.2.0\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.1\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.2\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.3\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.4\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.5\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.6\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.7\RAT-200_v6.0001.tif C:\RAT-200_v6\RAT-200_v6.2.0001.tif

//C:\RAT-200_v6\crop.1.0\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.1\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.2\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.3\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.4\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.5\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.6\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.1.7\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.0\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.1\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.2\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.3\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.4\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.5\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.6\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.2.7\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.3.0\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.3.1\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.3.2\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.3.3\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.3.4\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.3.5\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.3.6\RAT-200_v6.0001.tif C:\RAT-200_v6\crop.3.7\RAT-200_v6.0001.tif C:\RAT-200_v6\RAT-200_v6.0001.tif

int _tmain(int argc, _TCHAR* argv[])
{
	bool tiff = true;
printf("%c",0xFF); 
	bool bInvert = false;

	argc--;argv++;

	if( strcmp(argv[0], "-mov")==0)
	{
		return BuildMov(argc, argv);
	}

	bool bExr = false;
	if( strcmp(argv[0], "-exr")==0)
	{
		bExr = true;
		argc--;argv++;
	}
	int X=0, Y=0;
	if(argc>=1)
	{
		X = atoi(argv[0]);
		argc--;argv++;
	}
	if(argc>=1)
	{
		Y = atoi(argv[0]);
		argc--;argv++;
	}
	Math::matrixMxN<Math::matrixMxN<Util::frgba> > files(X, Y);
	Math::matrixMxN<std::string > filenamess(X, Y);
	int x=0, y=0;
	for(int xy=0; xy<X*Y; xy++)
	{
		char* filename = argv[0];
		argc--;argv++;

		Math::matrixMxN<Util::frgba>& m = files[y][x];
		filenamess[y][x] = filename;

			/*/
		const char* extension = strrchr( filename, '.' );
		if( extension)
		{
			extension++;
			if ( !strncmp( extension, "tif", 3 ) ) tiff = true;
			else if ( !strncmp( extension, "exr", 3 ) ) { tiff = false; }
			else { printf("error *.exr or *.tiff(tif)\n"); continue; }
		}
			/*/

		if ( !bExr )
		{
			LoadTiff( filename, m);
			{
				int pr = (int)(100*(xy)/((float)X*Y));
				fprintf(stderr, "R90000   %02d%%\n", pr);
				fflush(stderr);
			}
			bInvert = true;
		}
		else
		{
/*/
			fprintf(stderr, "Read file %s\n", filename);
			LoadEXR(
			filename,
			m);
/*/
		}

		x++;
		if(x>=X)
		{
			y++;
			x=0;
		}

	}

	char* outfilename = "";
	if(argc>=1)
	{
		outfilename = argv[0];
		argc--;argv++;
	}

	if( bExr)
	{
		int res = BuildExr(X, Y, filenamess, outfilename);
		return res;
	}

	std::vector<int> wsize(X, 0), hsize(Y, 0), shsize(Y, 0);
	for(int y=0; y<Y; y++)
	{
		for(int x=0; x<X; x++)
		{
			Math::matrixMxN<Util::frgba>& m = files[y][x];
			printf("x=%d,y=%d = [%d][%d] %s\n", x, y, m.sizeX(), m.sizeY(), filenamess[y][x].c_str());
			wsize[x] = __max( wsize[x], m.sizeX());
			hsize[y] = __max( hsize[y], m.sizeY());
		}
	}
	int w = 0;
	int h = 0;
	for(int y=0; y<Y; y++)
	{
		int _y = y;
		if(bInvert) _y = Y-y-1;
		printf("Y[%d]=%d\n", _y, hsize[_y]);
		int hh = h+hsize[_y];
		shsize[_y] = h;
		h = hh;
	}
	for(int x=0; x<X; x++)
	{
		printf("X[%d]=%d\n", x, wsize[x]);
		int ww = w+wsize[x];
		wsize[x] = w;
		w = ww;
	}
	/*/
	for(int y=0; y<Y; y++)
	{
		shsize[Y-y-1] = hsize[y];
	}
	/*/
printf("----------------------------\noutsize %dx%d\n", w, h);

	Math::matrixMxN<Util::frgba> mr(w, h);

	for(int y=0; y<Y; y++)
	{
		for(int x=0; x<X; x++)
		{
			Math::matrixMxN<Util::frgba>& m = files[y][x];
			printf("x=%d, y=%d: [h=%d, w=%d] file: [%dx%d]\n", x, y, shsize[y], wsize[x], m.sizeX(), m.sizeY());
			for(int j=0; j<m.sizeY(); j++)
			{
				for(int i=0; i<m.sizeX(); i++)
				{
					mr[ shsize[y] + j][ wsize[x] +i] = m[j][i];
				}
			}
		}
	}

//	if ( tiff )
//	{
		TiffSave(
		outfilename, 
		mr);
//	}
//	else
//	{
//		SaveEXR(
//		outfilename, 
//		mr);		
//	}

fprintf(stderr, "CROP ASSEMBLER\n%s\n", outfilename);

//	printf("NOP\\n\n\r");
fflush(stdout); 
	return 0;
}

