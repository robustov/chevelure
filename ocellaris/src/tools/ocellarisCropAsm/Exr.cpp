#include "stdafx.h"
#include <vector>

#include <Math/matrixMxN.h>
#include <Util/math_bmpMatrixMN.h>

#include <ImfHeader.h>
#include <ImfInputFile.h>
#include <ImfOutputFile.h>
#include <ImfChannelList.h>

int BuildExr(int X, int Y, Math::matrixMxN<std::string >& filenamess, const char* outfilename)
{
	std::map<std::string, ::Math::matrixMxN<float> > outchannels;

	using namespace Imath;
	using namespace Imf;

	Header outheader;
	int fullwidth=0;
	int fullheight=0;
	bool bSwapRGB = false;

	int xy=0;
	for(int y=0; y<Y; y++)
	{
		for(int x=0; x<X; x++, xy++)
		{
			try
			{
				const char* filename = filenamess[y][x].c_str();
				InputFile file( filename );

				Box2i disw = file.header().displayWindow();
				Box2i dw = file.header().dataWindow();

				int width = dw.max.x - dw.min.x + 1;
				int height = dw.max.y - dw.min.y + 1;

				if(x==0 && y==0)
				{
					fullwidth = disw.max.x - disw.min.x + 1;
					fullheight = disw.max.y - disw.min.y + 1;

					outheader = Header(fullwidth, fullheight);
					outheader.dataWindow() = disw;

					const ChannelList &channels = file.header().channels();
					for( ChannelList::ConstIterator i = channels.begin(); i != channels.end(); ++i )
					{
						const Channel &channel = i.channel();
						fprintf(stderr, "channel: %s: %d sampling= {%d %d}\n", i.name(), channel.type, channel.xSampling, channel.ySampling);
						outheader.channels().insert( i.name(), Channel(Imf::FLOAT) );

						::Math::matrixMxN<float>& m = outchannels[i.name()];
						m.init(fullwidth, fullheight);
					}

				}
				// ������
				FrameBuffer frameBuffer;
				std::map<std::string, ::Math::matrixMxN<float> >::iterator it = outchannels.begin();
				for(;it != outchannels.end(); it++)
				{
					std::string name = it->first;
					::Math::matrixMxN<float>& m = it->second;

					frameBuffer.insert( name.c_str(),
						Slice( Imf::FLOAT, (char*)(&m[0][0]),
						sizeof( float ),
						sizeof( float ) * fullwidth ) );
				}
				file.setFrameBuffer( frameBuffer );
				file.readPixels( dw.min.y, dw.max.y );
				fprintf(stderr, "Read file %s\n", filename);

				{
					int pr = (int)(100*(xy)/((float)X*Y));
					fprintf(stderr, "R90000   %02d%%\n", pr);
					fflush(stderr);
				}
			}
			catch ( const std::exception &exc )
			{
				std::cerr << exc.what() << std::endl;
			}

//			Math::matrixMxN<Util::frgba> m;
//			LoadEXR(filenamess[y][x].c_str(), m);

		}
	}
	OutputFile ofile(outfilename, outheader);

	try
	{
		FrameBuffer frameBuffer;
		std::map<std::string, ::Math::matrixMxN<float> >::iterator it = outchannels.begin();
		for(;it != outchannels.end(); it++)
		{
			std::string name = it->first;
			if( bSwapRGB)
			{
				if(name.rfind(".b")!=name.npos)
					name[name.size()-1] = 'r';
				else if(name.rfind(".r")!=name.npos)
					name[name.size()-1] = 'b';
			}

			::Math::matrixMxN<float>& m = it->second;

			frameBuffer.insert( name.c_str(),
				Slice( Imf::FLOAT, (char*)(&m[0][0]),
				sizeof( float ),
				sizeof( float ) * fullwidth ) );

			fprintf(stderr, "save channel: %s\n", name.c_str());
		}
		ofile.setFrameBuffer( frameBuffer );
		ofile.writePixels( fullheight );
	}
	catch ( const std::exception &exc )
	{
		std::cerr << exc.what() << std::endl;
	}

	return 0;
}


bool LoadEXR( const char* filename, Math::matrixMxN<Util::frgba>& m )
{
	using namespace Imath;
	using namespace Imf;

	try
	{
		InputFile file( filename );

		Box2i disw = file.header().displayWindow();
		Box2i dw = file.header().dataWindow();

		int width = dw.max.x - dw.min.x + 1;
		int height = dw.max.y - dw.min.y + 1;

		const ChannelList &channels = file.header().channels();
		for( ChannelList::ConstIterator i = channels.begin(); i != channels.end(); ++i )
		{
			const Channel &channel = i.channel();
			printf("channel: %s: %d sampling= {%d %d}\n", i.name(), channel.type, channel.xSampling, channel.ySampling);
		}

		m.init( width, height );

		FrameBuffer frameBuffer;

		frameBuffer.insert( "R",
		Slice( Imf::FLOAT,
		(char*)(&m[0][0].r) - (dw.min.x + dw.min.y * width)*sizeof( m[0][0]),
		sizeof( m[0][0] ),
		sizeof( m[0][0] ) * width ) );

		frameBuffer.insert( "G",
		Slice( Imf::FLOAT,
		(char*)(&m[0][0].g) - (dw.min.x + dw.min.y * width)*sizeof( m[0][0]),
		sizeof( m[0][0] ),
		sizeof( m[0][0] ) * width ) );

		frameBuffer.insert( "B",
		Slice( Imf::FLOAT,
		(char*)(&m[0][0].b) - (dw.min.x + dw.min.y * width)*sizeof( m[0][0]),
		sizeof( m[0][0] ),
		sizeof( m[0][0] ) * width ) );

		frameBuffer.insert( "A",
		Slice( Imf::FLOAT,
		(char *)(&m[0][0].a) - (dw.min.x + dw.min.y * width)*sizeof( m[0][0]),
		sizeof( m[0][0] ),
		sizeof( m[0][0] ) * width ) );

		file.setFrameBuffer( frameBuffer );
		file.readPixels( dw.min.y, dw.max.y );
	}
	catch ( const std::exception &exc )
	{
		std::cerr << exc.what() << std::endl;
	}

	return true;
}

bool SaveEXR( const char* filename, Math::matrixMxN<Util::frgba>& m )
{
	using namespace Imath;
	using namespace Imf;

	int width = m.sizeX();
	int height = m.sizeY();

	try
	{
		Header header( width, height );
		header.channels().insert( "R", Channel( Imf::FLOAT ) );
		header.channels().insert( "G", Channel( Imf::FLOAT ) );
		header.channels().insert( "B", Channel( Imf::FLOAT ) );
		header.channels().insert( "A", Channel( Imf::FLOAT ) );
		
		OutputFile file( filename, header );

		FrameBuffer frameBuffer;

		frameBuffer.insert( "R",
		Slice( Imf::FLOAT,
		(char*)(&m[0][0].r),
		sizeof( m[0][0] ),
		sizeof( m[0][0] ) * width ) );

		frameBuffer.insert( "G",
		Slice( Imf::FLOAT,
		(char*)(&m[0][0].g),
		sizeof( m[0][0] ),
		sizeof( m[0][0] ) * width ) );

		frameBuffer.insert( "B",
		Slice( Imf::FLOAT,
		(char*)(&m[0][0].b),
		sizeof( m[0][0] ),
		sizeof( m[0][0] ) * width ) );

		frameBuffer.insert( "A",
		Slice( Imf::FLOAT,
		(char*)(&m[0][0].a),
		sizeof( m[0][0] ),
		sizeof( m[0][0] ) * width ) );

		file.setFrameBuffer( frameBuffer );
		file.writePixels( height );
	}
	catch ( const std::exception &exc )
	{
		std::cerr << exc.what() << std::endl;
	}

	return true;
}
