#include "stdafx.h"
#include <tiffio.h>
#include <vector>

#include <Math/matrixMxN.h>
#include <Util/math_bmpMatrixMN.h>


bool LoadTiff(const char* filename, Math::matrixMxN<Util::frgba>& m)
{
	TIFF *image;
	uint16 bps, spp;
	tsize_t stripSize;

	unsigned long imageOffset, result;
	int stripMax, stripCount;

	// Open the TIFF image
	if((image = TIFFOpen(filename, "r")) == NULL)
	{
		fprintf(stderr, "Could not open incoming image\n");
		return false;
	}

	int comp, planarconf, photometric;
	TIFFGetField(image, TIFFTAG_COMPRESSION, &comp);
	TIFFGetField(image, TIFFTAG_PLANARCONFIG, &planarconf);
	TIFFGetField(image, TIFFTAG_PHOTOMETRIC, &photometric);

	int w, h;
	TIFFGetField(image, TIFFTAG_IMAGEWIDTH, &w);
	TIFFGetField(image, TIFFTAG_IMAGELENGTH, &h);
	m.init(w, h);

	int istiled = TIFFIsTiled(image);

	// Check that it is of a type that we support
	if( TIFFGetField(image, TIFFTAG_BITSPERSAMPLE, &bps) == 0)
	{
		fprintf(stderr, "Either undefined or unsupported number of bits per sample\n");
		return false;
	}
	int byps = bps/8;
	if( TIFFGetField(image, TIFFTAG_SAMPLESPERPIXEL, &spp) == 0)
	{
		fprintf(stderr, "Either undefined or unsupported number of samples per pixel\n");
		return false;
	}
	// Read in the possibly multiple strips
	stripSize = TIFFStripSize (image);
	stripMax = TIFFNumberOfStrips (image);
	imageOffset = 0;
	unsigned char* buffer = (unsigned char*)(new char[stripSize]);
	if(!buffer)
	{
		fprintf(stderr, "Could not allocate enough memory for the uncompressed image\n");
		return false;
	}

	int x=0, y=0;
	for (stripCount = 0; stripCount < stripMax; stripCount++)
	{
		result = TIFFReadEncodedStrip (image, stripCount,
			buffer, stripSize);
		int s = stripSize/(spp*byps);
		for(int i=0; i<s; i++)
		{
			Util::frgba color;
			for( int c=0; c<spp; c++)
			{
				unsigned char* val = buffer + (i*spp+c)*byps;
				float v=0;
				if(bps==8)
				{
					v = (*val)/255.f;
				}
				if(bps==32)
				{
					v = *(float*)val;
				}
				if(c<3)
					color[c]=v;
				else
					color[c]=v;
			}
			m[h-y-1][x] = color;

			// ������
			x++;
			if(x>=w)
			{
				y++;
				if(y>=h) break;
				x=0;
			}
		}

		if(y>=h) break;
		if( result==-1)
		{
			fprintf(stderr, "Read error on input strip number %d\n", stripCount);
			return false;
		}
	}
	return true;
}

bool TiffSave(const char* filename, Math::matrixMxN<Util::frgba>& m )
{
	// Open the output image
	TIFF* output = TIFFOpen(filename, "w");
	if(!output)
		return false;

	int w = m.sizeX();
	int h = m.sizeY();
	std::vector<unsigned char> data(w*h*4, 0);
	for(int y=0; y<h; y++)
	{
		for(int x=0; x<w; x++)
		{
			Util::frgba color = m[h-y-1][x];
			unsigned char* d = &data[0] + (y*w+x)*4;
			d[0] = (unsigned char)(color[0]*255);
			d[1] = (unsigned char)(color[1]*255);
			d[2] = (unsigned char)(color[2]*255);
			d[3] = (unsigned char)(color[3]*255);
		}
	}

	// Magical stuff for creating the image
	// ...
	// Write the tiff tags to the file
	TIFFSetField(output, TIFFTAG_IMAGEWIDTH, w);
	TIFFSetField(output, TIFFTAG_IMAGELENGTH, h);
	TIFFSetField(output, TIFFTAG_COMPRESSION, COMPRESSION_LZW);
	TIFFSetField(output, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
	TIFFSetField(output, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
	TIFFSetField(output, TIFFTAG_BITSPERSAMPLE, 8);
	TIFFSetField(output, TIFFTAG_SAMPLESPERPIXEL, 4);
	// Actually write the image
	if(TIFFWriteEncodedStrip(
		output, 0, &data[0], data.size()) == 0)
	{
		fprintf(stderr, "Could not write image\n");
		exit(42);
	}
	TIFFClose(output);
	return true;
}
