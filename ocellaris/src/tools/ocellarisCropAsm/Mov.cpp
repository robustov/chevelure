#include "stdafx.h"
#include <vector>
#include <list>

#include <Math/matrixMxN.h>
#include <Util/math_bmpMatrixMN.h>
#include "Qt/QTMovieFile.h"

bool LoadTiff(const char* filename, Math::matrixMxN<Util::frgba>& m);

int BuildMov(int argc, _TCHAR* argv[])
{
	argc--;argv++;

	if(argc<=0)
		return -1;

	QTMovieFile movfile;
	std::string movfilename = argv[0];
	argc--;argv++;

// ������ ������ � ����������
// void Util::getFileList(
//	const TCHAR* dirname, 
//	const TCHAR* mask, 
//	tag_filedatalist& files)

	std::list<std::string> inputfiles;
	for( ; argc>0; argc--, argv++)
	{
		inputfiles.push_back(argv[0]);
	}
	if( inputfiles.empty()) 
		return -1;

	int progress=0, fullsize = inputfiles.size();
	int sizeY=0, sizeX=0;

	std::list<std::string>::iterator it = inputfiles.begin();
	Math::matrixMxN<Util::frgba> m;
	if( !LoadTiff(it->c_str(), m))
		return -2;
	sizeY=m.sizeY(), sizeX=m.sizeX();
	movfile.InitGraphicsWorld( sizeY, sizeX, movfilename.c_str());
	movfile.AppendNewFrame(m);
	it++;
	progress++;

	for( ; it!=inputfiles.end(); it++, progress++)
	{
		fprintf(stderr, "R90000   %02d%%\n", (int)(progress*100.f/fullsize));
		fflush(stderr);

		Math::matrixMxN<Util::frgba> m;
		LoadTiff(it->c_str(), m);
		movfile.AppendNewFrame(m, sizeY, sizeX);
	}
	return 0;
}
