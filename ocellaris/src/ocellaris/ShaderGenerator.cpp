#include "ShaderGenerator.h"
#include <maya/MFnMesh.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MPlug.h>
#include <maya/MFnStringArrayData.h>
#include <maya/MGlobal.h>
#include <maya/MFnNumericData.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnStringData.h>
#include <maya/MStringArray.h>
#include <maya/MFnStringArrayData.h>
#include <string>
#include "Util/misc_switch_string.h"
#include "ocellaris/OcellarisExport.h"

struct shaderExtern
{
	std::string type;
	std::string name;
};
std::string mtorShaderNames(MObject obj, const char* attrname, std::vector<shaderExtern>* params);

MObject addAttribute(MObject obj, const char* name, const char* type);

void setAttributeToRender(MObject obj, MObject attr, const char* type, cls::IRender* render);


bool ShaderGenerator::OnShader(MObject& obj, cls::IRender* render)
{
	std::vector<shaderExtern> surfaceShaderParams, displaceShaderParams;
	std::string surfaceShader  = mtorShaderNames( obj, "slimSurf",  &surfaceShaderParams);
	std::string displaceShader = mtorShaderNames( obj, "slimDispl", &displaceShaderParams);

	if( !surfaceShader.empty())
	{
		for(unsigned i=0; i<surfaceShaderParams.size(); i++)
		{
			const char* type = surfaceShaderParams[i].type.c_str();
			MObject attr = addAttribute( obj, surfaceShaderParams[i].name.c_str(), type);
			setAttributeToRender(obj, attr, type, render);
		}
		if( render)
			render->Shader("surface", surfaceShader.c_str());
	}
	if( !displaceShader.empty())
	{
		for(unsigned i=0; i<displaceShaderParams.size(); i++)
		{
			const char* type = displaceShaderParams[i].type.c_str();
			MObject attr = addAttribute( obj, displaceShaderParams[i].name.c_str(), type);
			setAttributeToRender(obj, attr, type, render);
		}
		if( render)
			render->Shader("displacement", displaceShader.c_str());
	}
	return true;
}

misc::switch_string::Token semslist[] = {
	{"float",	cls::PT_FLOAT	}, 
	{"color",	cls::PT_COLOR	}, 
	{"vector",	cls::PT_VECTOR	}, 
	{"string",	cls::PT_STRING	}, 
	{NULL, 		-1	}};
misc::switch_string sems(semslist);

MObject addAttribute(MObject obj, const char* name, const char* type)
{
	MStatus stat;
	MFnDependencyNode dn(obj);
	MObject attr = dn.attribute(name, &stat);
	if( stat)
	{
		return attr;
	}

	int code = sems.match(type);
	return ocExport_AddAttrType(obj, name, (cls::enParamType )code);
}
void setAttributeToRender(MObject obj, MObject attr, const char* type, cls::IRender* render)
{
	MStatus stat;
	MFnAttribute fnattr(attr);
	MString name = fnattr.name();

	cls::Param p = ocExport_GetAttrValue(obj, attr);
	if( p.empty()) return;

	if( render)
		render->Parameter(name.asChar(), p);
	/*/
	int code = sems.match(type);
	switch(code)
	{
	case 0:
		{
			double val;
			MPlug(obj, attr).getValue(val);
			render->Parameter(name.asChar(), (float)val);
			break;
		}
	case 1:
		{
			MObject val;
			MPlug(obj, attr).getValue(val);
			MFnNumericData data(val, &stat);
			Math::Vec3f v4d;
			stat = data.getData(v4d.x, v4d.y, v4d.z);
			render->Parameter(name.asChar(), cls::PT_COLOR, v4d);
			break;
		}
	case 2:
		{
			MObject val;
			MPlug(obj, attr).getValue(val);
			MFnNumericData data(val, &stat);
			Math::Vec3f v4d;
			stat = data.getData(v4d.x, v4d.y, v4d.z);
			render->Parameter(name.asChar(), cls::PT_VECTOR, v4d);
			break;
		}
	case 10:
		{
			MString str;
			MPlug plug(obj, attr);
			if( !plug.getValue(str))
				break;
			render->Parameter(name.asChar(), str.asChar());
			break;
		}
	}
	/*/
}

void RecursiveFindParams(int level, const char* appid, std::vector<shaderExtern>& params);

std::string mtorShaderNames(MObject obj, const char* attrname, std::vector<shaderExtern>* params)
{
	MStatus stat;
	MStringArray res;
	char buf[1024];

	MFnDependencyNode dn(obj);
//displayString("node: %s", dn.name().asChar());

	/*/
	std::string attrname;
	if( type==ST_SURFACE)
		attrname = "slimSurf";
	else if( type==ST_DISPLACE)
		attrname = "slimDispl";
	/*/
	MPlug pl = dn.findPlug(attrname, &stat);
	if(!stat)
		return "";

	MObject o;
	pl.getValue(o);
	res = MFnStringArrayData(o).array();
	if( res.length()<1)
		return "";

	std::string id = res[0].asChar();
	if( id.empty()) return "";
//displayString("id: %s", id.c_str());

	// $appSurf = `slimcmd slim GetAppearances -id $newSurfID[0]`;
	_snprintf(buf, 1024, "slimcmd slim GetAppearances -id \"%s\"", id.c_str());
	if( !MGlobal::executeCommand(buf, res, false, false))
		return "";
	if(res.length()<1) 
		return "";
	std::string appSurf = res[0].asChar();

	// $ShdName[0] = `slimcmd $appSurf GetName`;
	_snprintf(buf, 1024, "slimcmd \"%s\" GetName", appSurf.c_str());
	if( !MGlobal::executeCommand(buf, res, false, false))
		return "";
	if(res.length()<1) 
		return "";

	std::string shaderName = res[0].asChar();
//displayString("shader: %s", shaderName.c_str());
	shaderName = "rmanshader/" + shaderName;

	std::vector<shaderExtern> static_params;
	if( !params)
		params = &static_params;

	params->clear();
	RecursiveFindParams( 0, id.c_str(), *params);


	// SAVE
	std::string shadername_attr = attrname;
	shadername_attr += "_oc";
	std::string typesname_attr = attrname;
	typesname_attr += "_types";
	std::string attrname_attr = attrname;
	attrname_attr += "_names";

	MStringArray extern_types( params->size(), "");
	MStringArray extern_names( params->size(), "");
	for(unsigned i=0; i<params->size(); i++)
	{
		extern_types[i] = (*params)[i].type.c_str();
		extern_names[i] = (*params)[i].name.c_str();
	}


	MObject attr = dn.attribute(shadername_attr.c_str(), &stat);
	if( !stat)
	{
		MFnTypedAttribute typedAttr;
		MFnStringData stringData;
		attr = typedAttr.create( shadername_attr.c_str(), shadername_attr.c_str(), MFnData::kString, stringData.create("", &stat), &stat );
		typedAttr.setConnectable(true);
		typedAttr.setStorable(true);
		typedAttr.setWritable(true);
		typedAttr.setReadable(true);
		typedAttr.setHidden(false);
		typedAttr.setKeyable(true);
		dn.addAttribute(attr);
	}
	MPlug(obj, attr).setValue(shaderName.c_str());

	MFnStringArrayData array;
	MObject val = array.create(extern_types);
	attr = dn.attribute(typesname_attr.c_str(), &stat);
	if( !stat)
	{
		MFnTypedAttribute typedAttr;
		attr = typedAttr.create( typesname_attr.c_str(), typesname_attr.c_str(), MFnData::kStringArray, val, &stat );
		typedAttr.setStorable(true);
		typedAttr.setWritable(true);
		typedAttr.setReadable(true);
		dn.addAttribute(attr);
	}
	MPlug(obj, attr).setValue(val);
	
	val = array.create(extern_names);
	attr = dn.attribute(attrname_attr.c_str(), &stat);
	if( !stat)
	{
		MFnTypedAttribute typedAttr;
		attr = typedAttr.create( attrname_attr.c_str(), attrname_attr.c_str(), MFnData::kStringArray, val, &stat );
		typedAttr.setStorable(true);
		typedAttr.setWritable(true);
		typedAttr.setReadable(true);
		dn.addAttribute(attr);
	}
	MPlug(obj, attr).setValue(val);

	return shaderName;
}

void RecursiveFindParams(int level, const char* id, std::vector<shaderExtern>& params)
{
	if( level>10) return;
	MStatus stat;
	MStringArray res;
	char buf[1024];

	// $appSurf = `slimcmd slim GetAppearances -id $newSurfID[0]`;
	_snprintf(buf, 1024, "slimcmd slim GetAppearances -id \"%s\"", id);
	if( !MGlobal::executeCommand(buf, res, false, false))
		return;
	if(res.length()<1) 
		return;
	std::string appSurf = res[0].asChar();

	// $ShdName[0] = `slimcmd $appSurf GetName`;
	_snprintf(buf, 1024, "slimcmd \"%s\" GetName", appSurf.c_str());
	if( !MGlobal::executeCommand(buf, res, false, false))
		return;
	if(res.length()<1) 
		return;
	std::string shaderName = res[0].asChar();

	// $prop = `slimcmd $appSurf GetProperties`;
	_snprintf(buf, 1024, "slimcmd \"%s\" GetProperties", appSurf.c_str());
	if( !MGlobal::executeCommand(buf, res, false, false))
		return;
	if(res.length()<1) 
		return;

	std::vector<std::string> proplist;
	std::string prop = res[0].asChar();
	char* substr = strtok((char*)prop.c_str(), " ");
	for(; substr; substr = strtok(NULL, " "))
		proplist.push_back(substr);

	for(unsigned i=0; i<proplist.size(); i++)
	{
		std::string& substr = proplist[i];
//displayString("prop%d: %s", i, substr.c_str());
		MStringArray res2;
		// slimcmd $tm GetValueProvider == variable
		_snprintf(buf, 1024, "slimcmd \"%s\" GetValueProvider", substr.c_str());
		if( !MGlobal::executeCommand(buf, res2, false, false))
			continue;
		if(res2.length()<1) 
			continue;
		if( strcmp(res2[0].asChar(), "connection")==0)
		{
			// slimcmd $tm GetName;
			_snprintf(buf, 1024, "slimcmd \"%s\" GetConnection", substr.c_str());
			if( !MGlobal::executeCommand(buf, res2, false, false))
				continue;
			if(res2.length()<1) 
				continue;
			std::string connection = res2[0].asChar();
			if( connection.empty())
				continue;

//displayString("connection: %s", connection.c_str());
	
			RecursiveFindParams( level+1, connection.c_str(), params);
			continue;
		}
		if( strcmp(res2[0].asChar(), "variable")!=0)
			continue;

		// slimcmd $tm GetName;
		_snprintf(buf, 1024, "slimcmd \"%s\" GetName", substr.c_str());
		if( !MGlobal::executeCommand(buf, res2, false, false))
			continue;
		if(res2.length()<1) 
			continue;
		shaderExtern externParam;
		externParam.name  = res2[0].asChar();

		// slimcmd $tm GetType;
		_snprintf(buf, 1024, "slimcmd \"%s\" GetType", substr.c_str());
		if( !MGlobal::executeCommand(buf, res2, false, false))
			continue;
		if(res2.length()<1) 
			continue;
		externParam.type = res2[0].asChar();

		externParam.name = shaderName + "_" + externParam.name;
		params.push_back(externParam);
//		displayString("param: %s %s", type.c_str(), name.c_str());
	}
}


