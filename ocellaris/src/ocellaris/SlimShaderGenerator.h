#pragma once

#include "ocellaris/IShaderGenerator.h"

struct SlimShaderGeneratorOld : public cls::IShaderGenerator
{
	bool bLazy;
public:
	SlimShaderGeneratorOld(bool bLazy);

	bool OnShader(MObject& node, cls::IRender* render);
};
