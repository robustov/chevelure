//! @ingroup implement_group
//! \brief layer ��������� ��� Instance (��������� ������)
//! 
struct InstanceLayerGenerator : public cls::ILayerGenerator
{
	// ���������� ���� ��� ��� �������� �����
	// INode* node - ������ � �������� ����������� �����
	// � ������ ������ node - ����������� ������ (�������� ��������)
	void OnFrameStart(INode* node, IExportContext* context)
	{
		IRender* render = context->render();

		// ��������� ���������
		render->startArchive("name");
		INodeGenerator* nodeGenerator = node->nodeGenerator("layername");
		nodeGenerator->OnNode(root, context);
		render->endArchive();
	}

	void OnGeometry(INode* node, IRender* render)
	{
	}
};

//! @ingroup implement_group
//! \brief node ��������� ��� Instance (��������� ������)
//! 
struct InstanceNodeGenerator : public cls::INodeGenerator
{

};
