#pragma once

#include "ocellaris/INode.h"

#include <maya/MDagPath.h>
#include <maya/MObject.h>
#include <vector>

//! @ingroup core_group
//! \brief Node
//! 
//! ���������� cls::INode
struct Node : public cls::INode
{
	MDagPath dagPath;
	MObject obj;
	bool bVisible;

	// ��������
	std::vector<cls::INode*> childs;

	// ���������� ???
	// ��� �������� �� �����?
	cls::ILayerGenerator* pLayerGenerator;
	cls::INodeGenerator* pNodeGenerator;
	cls::IGeometryGenerator* pGeometryGenerator;
	std::vector<cls::IAttributeGenerator*> pAttributeGenerator;
	cls::IShader* pShader;

	//! �����������
	Node(
		MObject object,
//		MDagPath& dagPath,	//!< ������
		bool bVisible=true		//!< ���� ��������� � ������ ��������
		)
	{
//		this->dagPath = dagPath;
//		this->obj = dagPath.node();
		this->obj = object;
		this->bVisible = bVisible;
	}

//!@name ���������� INode
//@{
public:
	//! ������ Maya
	virtual MObject& node(
		)
	{
		return obj;
	}
	//! Visible???
	virtual bool isVisible(
		)
	{
		return bVisible;
	}

	//! ��������
	virtual INode* child(
		int index				//!< ������
		)
	{
		return NULL;
	}

	//! ������������� ��������� ����
	virtual cls::ILayerGenerator* layerGenerator(
		)
	{
		return pLayerGenerator;
	}

	//! ������������� ��������� �������
	virtual cls::INodeGenerator* nodeGenerator(
		const char* layer		//!< ����
		)
	{
		return pNodeGenerator;
	}

	//! ������������� ��������� ���������
	virtual cls::IGeometryGenerator* geometryGenerator(
		const char* layer		//!< ����
		)
	{
		return pGeometryGenerator;
	}

	//! ������������� ���������� ��������. 
	//! �������� ��������������� ���� �� ������ NULL
	virtual cls::IAttributeGenerator* attributeGenerator(
		int index,				//!< ����� ����������
		const char* layer		//!< ����
		)
	{
		if(index<0 || index>=(int)pAttributeGenerator.size())
			return NULL;

		return pAttributeGenerator[index];
	}

	//! �������������� ������ ���� type ��� ���� layer
	virtual cls::IShader* shader(
		const char* type,		//!< ��� ������� "surface", "displacement", "volume", "ligth"
		const char* layer		//!< ����
		)
	{
		return pShader;
	}

//@}


};