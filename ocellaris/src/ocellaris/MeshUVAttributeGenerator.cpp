#include "MeshUVAttributeGenerator.h"
#include <maya/MFnMesh.h>
#include <maya/MFloatArray.h>

//! @ingroup implement_group
//! \brief ����������� ��������� �������� ���������� ���������� ��� Mesh � Subdiv
//! 
void MeshUVAttributeGenerator::OnAttribute(cls::INode& node, INode* node)
{
	// ���������
	const char* uvSet = NULL;
	int fvc;
	render->GetAttribute("uvSetName", &uvSet);
//	if( !uvSet)
//		uvSet = NULL;

	MFnMesh fnMesh(node);
	int fvc = fnMesh.numFaceVertices();

	MFloatArray _uArray, _vArray;
	fnMesh.getUVs(_uArray, _vArray, uvSet);
	std::PA<float> s(cls::PI_PRIMITIVEVERTEX), t(cls::PI_PRIMITIVEVERTEX);
	s.reserve(fvc);
	t.reserve(fvc);

	for ( MItMeshPolygon polyIt(mesh); !polyIt.isDone(); polyIt.next() ) 
	{
		int count = polyIt.polygonVertexCount();
		for(int v=0; v<count; v++)
		{
			// ST
			int stind = polyIt.getUVIndex(uvSet);
			s.push_back(_uArray[stind]);
			t.push_back(1-_vArray[stind]);
		}

	}
	render->Parameter("s", s);
	render->Parameter("t", t);
}
