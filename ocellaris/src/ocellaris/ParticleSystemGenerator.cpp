//! @ingroup implement_group
//! \brief ����������� ��������� ��� ������
//! 
struct ParticleSystemGenerator : public cls::IGeometryGenerator
{
	void OnGeometry(INode* node, IRender* render)
	{
		MStatus stat;
		MFnParticleSystem ps(node->node(), &stat);
		if( !stat) return;

		int type;
		dn.findPlug("particleRenderType", &stat).getValue(type);

		if( type!=1 && type!=6 &&
			type!=0 && type!=3)
		{
			displayString("ERROR type must be: MultyStreak, Streak, Point or MultiPoint");
			return MS::kFailure;
		}

		MIntArray ids;
		ps.particleIds(ids);

		void			age				( MDoubleArray& ) const;
		position
		radius
		MPlug plug_velocity = dn.findPlug("velocity", &stat);
		if(!stat)
		{
			displayString("plug velocity not found");
			return MS::kFailure;
		}

		bool bPoints = (type==0 || type==3);
		bool bMulti  = (type==0 || type==1);

	}
};
