// ???

// Load transform tree
Load()
{
	// ������
	MDagPath rootpath;
	Node* root = RecursiveLoad(rootpath, true);

	// ����������� ��������
	ExportContext context;
	RecursiveIteration(root, &GetAllGenerators, &context, NULL);

	// ������� OnFrameStart ��� ���� �����������
	context->CallGenerators(IGenerator::OnFrameStart);

	// �������������� � ��������� ���������� �����
	RecursiveIteration(root, &RegisterLayers, &context, NULL)

	// ������� ���������� �����
	for(int i=0; ;i++)
	{
		ILayerGenerator* pLayerGenerator = context->GetLayer(i);
		Node* layernode;
		pLayerGenerator->OnLayer(layernode, context, root);
	}

	// ������� ��������� ������
	context->exportDelayed();

	// ������� OnFrameEnd ��� ���� �����������
	context->CallGenerators(IGenerator::OnFrameEnd);

	// ������ ������
	context->SaveAll();
}

void GetAllGenerators(Node* node, ExportContext* context, void* data)
{
	std::set<IGenerator*>* generators = (std::set<IGenerator*>*)data;

	IGenerator* pGenerator;

	pGenerator = node->layerGenerator();
	if( pGenerator) context->AddGenerator(pGenerator);

	//
	...
}

void RegisterLayers(Node* node, ExportContext* context, void* data)
{
	ILayerGenerator* pLayerGenerator = node->layerGenerator();
	if( !pLayerGenerator) return;

	context->RegisterLayer( node);
}

void RecursiveIteration(Node* root, void (*proc)(Node* node, ExportContext* context, void* data), ExportContext* context, void* data)
{
	proc( root, context, data);
	for(int i=0;;i++)
	{
		INode* node = root->child();
		if(!node) break;

		RecursiveIteration(static_cast<Node*>(node), context, data);
	}
}

Node* RecursiveLoad(MDagPath& path, bool bVisible)
{
	MStatus stat;
	MObject obj = dagPath.node();

	// ���� ��������� � ������ ��������
	MFnDependencyNode dn(obj);
	MPlug plug = dn.findPlug("visibility");
	if( !plug.isNull())
	{
		bool bLocalVisible;
		plug.getValue(bLocalVisible);
		bVisible &= bLocalVisible;
	}
	Node* node = new Node(path, bVisible);

	for(unsigned i=0; i<path.childCount(); i++)
	{
		MObject ch = path.child(i, &stat);
		if( !stat) continue;

		MDagPath chpath;
		stat = MDagPath::getAPathTo( ch, chpath )

		Node* chnode = RecursiveLoad(chpath, bVisible);
		// append node to list!!!
		// ...
	}
}
