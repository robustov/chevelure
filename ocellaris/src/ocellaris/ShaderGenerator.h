#pragma once

#include "ocellaris/IShaderGenerator.h"
//! @ingroup implement_group
//! \brief ����������� ��������� ��������� ��� �������� ���� Mesh � Subdiv
//! 
struct ShaderGenerator : public cls::IShaderGenerator
{
	bool OnShader(MObject& node, cls::IRender* render);
};
