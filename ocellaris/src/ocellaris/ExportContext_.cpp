//! @ingroup core_group
//! \brief ExportContext
//! 
//! ���������� cls::IExportContext
//! ������� �������� ��������������� � ������ ��������� �����
class ExportContext : public cls::IExportContext
{
	//! ��� ����������
	std::set<IGenerator*> generators;
	//! ����
	std::list<ILayerGenerator* > layers;
	//! ���������� ������� � �������� ������� �������
	std::map< double, std::list<delayExportCall> > delayexport;

	//! ������ ������ ���������� ???
	RenderTreeItem rendertree;


//!@name ���������� ������
//@{
public:
	//! �����������
	ExportContext();
	// ����������� �����������
	void AddGenerator( IGenerator* generator);
	//! ������� ��������� proc � ���� �����������
	void CallGenerators( void (IGenerator::*proc)(IExportContext* context));
	//! ����������� ����
	void RegisterLayer( Node* node);
	//! ���� �� �������
	//! ������ 0 ��� ���������� ����������
	ILayerGenerator* GetLayer( int index);
	//! ������� ��������� ������
	void exportDelayed();
	//! ������ ������
	void SaveAll();
//@}

//!@name ���������� IExportContext
//@{
public:
	//! ������� �����
	virtual double time(
		)=0;
	// ��� �������� ����
	virtual const char* layertype(
		)=0;
	//! ������� ����
	virtual const char* layer(
		)=0;
	//! IRender � ������� �������
	virtual IRender* render(
		)=0;
	//! ���������� �� INodeGenerator::OnNode.
	//! ��������� ��������� ������� ��������� method ������� nodegenerator � ��������� ����� scenetime.
	//! � ����������� ����������� ������� ����� ���� �� �������� ���������� � �������� ����� getValue( DGContext(time)),
	//! �� ��� ��������� - ��� ����������, �������� ��� ������������� ��������
	virtual void delayExport( 
		double scenetime, 
		INodeGenerator* nodegenerator,
		void (INodeGenerator::*method)(INode* node, IExportContext* context)
		)=0;
//@}
};
