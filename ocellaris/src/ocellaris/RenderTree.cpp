//! @ingroup core_group
//! \brief ������ ������ ����������
//! 
struct RenderTree
{
};

//! @ingroup core_group
//! \brief �������� �������� (� ���� �������)
//! 
enum enAttributeSuperclass
{
	AST_ATTRIBUTE	= 1,	//!< �������
	AST_PARAMETER	= 2,	//!< ��������
	AST_FANTOM		= 4,	//!< ������� ������������ ������ ��� ��������
	AST_INSTRUCTION	= 8,	//!< ������� ���������� (���. Shader, Mesh, Curve, ...)
};

//! @ingroup core_group
//! \brief ������ ��������
//! 
struct RenderAttribute
{
	//! �������� �������� (� ���� �������)
	enAttributeSuperclass superclass;
	//! ��� ��������
	std::string name;
	//! ��������
	ParamValue value;
	//! ��������� �������
	//...
	//! ����
	//...
};

//! @ingroup core_group
//! \brief ������ ���������� (���������� ���� ������ Push/Pop Attribute)
//! 
struct RenderTreeItem
{
	RenderTreeItem* parent;

	//! ��������
	std::list< RenderRecord> attributes;

	//! �������� �� ������
	std::map<std::string, RenderAttribute*> attributesByName;

	//! ���������
	std::list< RenderTreeItem> subitems;

//!@name ���������� ������� - �� Render � ExportContext
//@{ 
public:
	//! AddAttribute
	void AddAttribute(ParamType t, ParamValue v);
	//! GetAttribute
	void GetAttribute(const char* string, ParamValue& v);
	//! AddCommand
	void AddCommand();
	//! Blur
	void AddBlur(
		int count
		);
//@}

//!@name ����������
//@{ 
public:
	//! ����� ������� - ��������� ������ MotionBlur!!!
	//! ���� ������ ����� �������� - �� ��� ����� ������� �����
	//! ��� ������� - ��������� ������ ��� �� ����� ���������� - 
	//! ������ ������ ����������
	void ParseMotionBlur(
		);
//@}

};

