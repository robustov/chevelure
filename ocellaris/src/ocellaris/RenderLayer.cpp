//! @ingroup implement_group
//! \brief ����������� ��������� ������ ���� 
//! 
//! ����� ���� ����. �����: "final", "reflection", "shadow", "spatialdb"
//! ����� ���� ����������� � ��������� ����� ��������: "camera" "ligth"
struct RenderLayer : public cls::ILayerGenerator
{
	std::string layername;
	std::string layertype;

	//! ��� ����
	const char* name(
		)
	{
		return layername.c_str();
	}
	//! ��� ����
	const char* type(
		)
	{
		return layertype.c_str();
	}

	// ���������� ���� ��� ��� �������� �����
	// INode* node - ������ � �������� ����������� �����
	// � ������ ������ node - ������
	void OnFrameStart(INode* node, IExportContext* context)
	{
		// ???
		std::string layer = context->AddLayer();
		IRender* render = context->render();
		// ��������� �������� ���������� � ������

		// motion blur
		render->Attribute("blur:deform", true);
		render->Attribute("blur:transform", true);
		render->Attribute("blur:count", 2);
		render->Attribute("blur:timestamps", 12, 13);

		//...
	}

	// ���������� ��� ��������������� ������� ��� ��������� ����
	void OnLayer(INode* node, IExportContext* context, INode* root)
	{
		IRender* render = context->render();
		INodeGenerator* nodeGenerator = root->nodeGenerator(layername.c_str());

		// ��� �� ������� ��� ��������� �������� �� ��������?
		// INodeGenerator ��� ILayerGenerator! ��������!!!
		nodeGenerator->OnNode(root, context);
	}


};

/*/
// ����� ������������ ������ ???
bool CameraLayer::isNodeAttachable(INode* node)
{
	return node->hasFn(MFn::kCamera);
}

// ������������ ������ ???
void CameraLayer::OnAttachNode(INode* node)
{
}


// ���������� ��� ��������������� ������� �� ������ ����
void CameraLayer::OnNodeAttribute(INode* node, context)
{
}

void CameraLayer::OnParseNode(INode* node, context, void* pdata)
{
	CameraLayer* pThis = (CameraLayer*)pdata;

	context->AddNode(node, pThis->layer);
}
/*/
