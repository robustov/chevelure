#pragma once

#include "ocellaris/IAttributeGenerator.h"

struct MeshUVAttributeGenerator : public cls::IAttributeGenerator
{
	void OnAttribute(cls::INode& node, INode* node);
}
