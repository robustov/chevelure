
//! @ingroup implement_group
//! \brief ����������� ��������� �������
//! 
//! ������������ ������ (�� ���������) ���� ��� ������� �� ����� ������ NodeGenerator
class DefaultNodeGenerator : public cls::INodeGenerator
{
	void OnNode(INode* node, IExportContext* context)
	{
		IRender* render = context->render();

		if( !node.isVisible())
		{
			// invisible!!!
			return;
		}
		if( bTransform)
		{
			// Transform
			bool bTransformBlur;
			render->GetAttribute("blur:transform", bTransformBlur);
			int motionCount;
			render->GetAttribute("blur:count", motionCount);

			// PushAttributes
			render->PushAttributes();

			if( !motionCount && !bTransformBlur)
			{
				// No blur
				context->delayExport(context->time(), this, &PushTransform);
			}
			else
			{
				// blur
				render->Motion(motionCount);
				for(int mt=0; mt<motionCount; mt++)
				{
					char buf[128]; sprintf("blur:time%f", buf);
					double time;
					render->GetAttribute(buf, time);
					context->delayExport(time, this, &PushTransform);
				}
			}
			// Children
			// ��� �� ������� ��� ��������� �������� �� ��������?
			// INodeGenerator ��� ILayerGenerator! ��������!!!
			node->ProcessChildren();
			// ����� ������ ���� ���������!!!
			render->PopTransform();
			// PopAttributes
			render->PopAttributes();
			return;
		}
		if(bGeometry)
		{
			// Geometry
			bool bDeformBlur;
			render->GetAttribute("blur:deform", bDeformBlur);
			int motionCount;
			render->GetAttribute("blur:count", motionCount);

			// PushAttributes
			render->PushAttributes();

			// ����������� � Motion!!!
			context->delayExport(context->time(), this, Shaders);

			if( !motionCount && !bDeformBlur)
			{
				// No blur
				context->delayExport(context->time(), this, &Geometry);
			}
			else
			{
				// blur
				render->Motion(motionCount);
				for(int mt=0; mt<motionCount; mt++)
				{
					char buf[128]; sprintf("blur:time%f", buf);
					double time;
					render->GetAttribute(buf, time);
					context->delayExport(time, this, &Geometry);
				}
			}

			// PopAttributes
			render->PopAttributes();
			return;
		}
	}
	// "����������" ������

	// ���������
	void PushTransform(INode* node, IExportContext* context)
	{
		IRender* render = context->render();
		MFnTransform transform(node);
		MMatrix m = transform.matrix();
		render->PushTransform(m);
	}
	// ���������
	void Geometry(INode* node, IExportContext* context)
	{
		IRender* render = context->render();
		const char* layer = context->layer();

		IGeometryGenerator* gg = node->geometryGenerator(layer);

		// ������� "������ ���������"
		gg->OnGeometry(node, context);

		// ������� �������������� ��������� ???
		for(int i=0; ; i++)
		{
			IAttributeGenerator* ag = node->attributeGenerator(i, layer);
			if( !ag) break;
		}
	}
	// �������
	void Shaders(INode* node, IExportContext* context)
	{
		IRender* render = context->render();
		const char* layer = context->layer();

		// surface
		{
			const char* shadertype = "surface";
			IShader* shader = node->shader(shadertype, layer);
			if(shader)
				render->Shader(shadertype, shader->name.c_str());
		}
		// displacement
		{
			const char* shadertype = "displacement";
			IShader* shader = node->shader(shadertype, layer);
			if(shader)
				render->Shader(shadertype, shader->name.c_str());
		}
		// volume
		{
			const char* shadertype = "volume";
			IShader* shader = node->shader(shadertype, layer);
			if(shader)
				render->Shader(shadertype, shader->name.c_str());
		}
	}
};
