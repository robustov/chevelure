//#include "stdafx.h"
#include <windows.h>
#include "ocellaris/IRender.h"
#include "ocellaris/IProcedural.h"


BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		{
			// ��� �������� �����
			char filename[512]; 
			GetModuleFileName( (HMODULE)hModule, filename, 512); 
//			fprintf(stderr, "load %s\n", filename);
			fflush(stderr);
			break;
		}
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		{
			// ��� �������� �����
			char filename[512]; 
			GetModuleFileName( (HMODULE)hModule, filename, 512); 
//			fprintf(stderr, "unload %s\n", filename);
			fflush(stderr);
		}
		break;
	}
    return TRUE;
}



/*/
class QuadProcedure : public cls::IProcedural
{
public:
	// declared parameter
//	float z;
//	float defsx, defsy;
public:
	virtual bool Export(std::string& dllname, std::string& procname, bool& bNeedInitialize)
	{
		dllname = "dsds.dll";
		procname = "sss";
		bNeedInitialize = false;
		return true;
	}
	// ������ ����������
	virtual void Init(cls::IRender* render){};
	// ������ ����������
	virtual void Render(cls::IRender* render);
};

// ������ ����������
void QuadProcedure::Render(cls::IRender* render)
{
	float z = 0;
	float sx = 1, sy = 1;
	render->GetParameter("sx", sx);
	render->GetParameter("sy", sy);

	cls::PA<Math::Vec3f> P( cls::PI_VERTEX, 5, cls::PT_POINT);

	Math::Vec3f N(0, 0, 1);
	P[0] = Math::Vec3f( 0, 0, z);
	P[1] = Math::Vec3f(sx, 0, z);
	P[2] = Math::Vec3f( 0, sy, z);
	P[3] = Math::Vec3f(sx, sy, z);
	P[4] = P[0];

	cls::PA<int> prims( cls::PI_CONSTANT, 1); 
	prims[0] = 5;

	render->PushAttributes();
	render->Parameter("P", P);
	render->Parameter("N", cls::PT_NORMAL, N);
	render->Curves("linear", "nonperiodic", prims);
}
BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    return TRUE;
}

cls::IProcedural* TestProc1(cls::IRender* prman)
{
	QuadProcedure* proc = new QuadProcedure();
//	prman->GetParameter("z", proc->z);
//	prman->GetParameter("sx", proc->defsx);
//	prman->GetParameter("sy", proc->defsy);
	return proc;
}

/*/
