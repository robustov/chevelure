#pragma once
#include "CmdParams.h"

void ocsInit(CmdParams& params);
void ocsBeforeSimulate(NxScene* gScene);
void ocsAfterSimulate(NxScene* gScene);
