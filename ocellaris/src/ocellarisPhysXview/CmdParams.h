#pragma once
// CmdParams

#include <list>
#include <string>

// Парсинг параметров командной строки
struct CmdParams
{
	std::list< std::string > inputocs;

	CmdParams()
	{
	}
	void Parse(int argc, _TCHAR* argv[]);
};

