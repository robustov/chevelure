#include "stdafx.h"
#include "qtmoviefile.h"

QTMovieFile::QTMovieFile() 
{	
	m_pMovie=NULL;
	m_pTrack=NULL;
	m_pMedia=NULL;
	m_resRefNum=0;
	m_gWorld=NULL;
	m_savedGD=NULL;	
	m_savedPort=NULL;
	m_imageDesc=NULL;
	m_compressedData=NULL;;
	m_compressedDataPtr=NULL;

	pAppendFrame[0]=&QTMovieFile::AppendDummy;
	pAppendFrame[1]=&QTMovieFile::AppendFrameFirstTime;
	pAppendFrame[2]=&QTMovieFile::AppendFrameUsual;
	nAppendFuncSelector=0;		//Point to Dummy Function

	if(InitializeQTML(0)!=noErr)
	{
		TerminateQTML();
		MessageBox(NULL,_T("Unable to Initialize Quick Time Environment"),_T("Error"),MB_OK|MB_ICONERROR);
		return;
	}

	EnterMovies();
 }
	
HRESULT QTMovieFile::InitGraphicsWorld(int h, int w, LPCTSTR lpszFileName)
{
	int Width = w;
	int Height = h;

	OSErr		err = noErr;
	FSSpec		fileSpec;
	char		szAbsFileName[MAX_PATH];

	if(_fullpath(szAbsFileName,lpszFileName,MAX_PATH)==NULL)
	{
		_tcscpy(m_szErrMsg,_T("\tInvalid output Filename \n\nPlease use Absolute output Path"));
		goto TerminateQuickTimeConstructor;
	}

	Rect bounds;
    bounds.top = bounds.left = 0; 
    bounds.bottom = Height;
	bounds.right = Width; 

	if(QTNewGWorld(&m_gWorld,k32BGRAPixelFormat,&bounds,NULL,NULL,NULL)!=noErr)
	{
		_tcscpy(m_szErrMsg,_T("Unable to Create New Quick Time World From the Bitmap"));
		goto TerminateQuickTimeConstructor;
	}

	GetGWorld(&m_savedPort,&m_savedGD);
	SetGWorld(m_gWorld,NULL);

	c2pstrcpy(m_FileName,szAbsFileName);
	FSMakeFSSpec(0,0,m_FileName,&fileSpec);

	err=CreateMovieFile(&fileSpec,FOUR_CHAR_CODE('TVOD'),smCurrentScript,createMovieFileDeleteCurFile | createMovieFileDontCreateResFile,&m_resRefNum,&m_pMovie);
	if(err!=noErr)
	{
		_tcscpy(m_szErrMsg,_T("Unable to Create Movie File"));
		goto TerminateQuickTimeConstructor;		
	}

	m_pTrack = NewMovieTrack (m_pMovie, 
							FixRatio(Width,1),//Do not cast to (short) - Overflow occurs
							FixRatio(Height,1), 
							0);
	if(m_pTrack==NULL)
	{
        _tcscpy(m_szErrMsg,_T("Unable to Create Track"));
		goto TerminateQuickTimeConstructor;		
	}

	m_trackFrame.left=0; m_trackFrame.top=0; 
	m_trackFrame.right = Width;	//Do not cast to (short) - Overflow occurs
	m_trackFrame.bottom = Height ;

	m_pMedia = NewTrackMedia (m_pTrack, VideoMediaType,
							kVideoTimeScale, /* Video Time Scale */
							nil, 0);
	if(m_pMedia==NULL)
	{
		_tcscpy(m_szErrMsg,_T("Unable to Create Track Media"));
		goto TerminateQuickTimeConstructor;		
	}

	nAppendFuncSelector=1;		//0=Dummy	1=FirstTime	2=Usual
	return S_OK;

TerminateQuickTimeConstructor:

	ReleaseMemory();
	MessageBox(NULL,m_szErrMsg,_T("Error"),MB_OK|MB_ICONERROR);
	return E_FAIL;

}

QTMovieFile::~QTMovieFile(void)
{
	if(nAppendFuncSelector==2)
	{
		short resId = movieInDataForkResID;
		if(m_pMedia)  EndMediaEdits (m_pMedia);
		if(m_pTrack)  InsertMediaIntoTrack (m_pTrack, kTrackStart,/* track start time */
									kMediaStart, /* media start time */
									GetMediaDuration (m_pMedia),
									fixed1);
		if(m_pMovie)  AddMovieResource (m_pMovie, m_resRefNum, &resId,m_FileName);
	}

	nAppendFuncSelector=0;

	ReleaseMemory();

	ExitMovies();

	TerminateQTML();
}

void QTMovieFile::ReleaseMemory()
{
	nAppendFuncSelector=0;		//Point to Dummy
	if (m_imageDesc)
	{
		DisposeHandle ((Handle)m_imageDesc);
		m_imageDesc=NULL;
	}
	if (m_compressedData)
	{
		DisposeHandle (m_compressedData);
		m_compressedData=NULL;
	}
	if(m_resRefNum)
	{
		CloseMovieFile(m_resRefNum);
		m_resRefNum=0;
	}		
	if(m_pMovie)
	{	
		DisposeMovie(m_pMovie);
		m_pMovie=NULL;
	}
	if(m_savedPort)
	{
		SetGWorld(m_savedPort,m_savedGD);
		m_savedPort=NULL;
	}
	if(m_gWorld)
	{
		DisposeGWorld(m_gWorld);
		m_gWorld=NULL;
	}
}

HRESULT	QTMovieFile::AppendFrameFirstTime()
{
	long maxCompressedSize;
	OSErr err = noErr;

	Rect rect;
	rect.left=m_trackFrame.left;rect.top=m_trackFrame.top;
	rect.right=m_trackFrame.right;
	rect.bottom=m_trackFrame.bottom;

	if(GetSystemMetrics(SM_CXSCREEN)>rect.right) rect.right=GetSystemMetrics(SM_CXSCREEN);
	if(GetSystemMetrics(SM_CYSCREEN)>rect.bottom) rect.bottom=GetSystemMetrics(SM_CYSCREEN);


	err = GetMaxCompressionSize(m_gWorld->portPixMap,
								&rect, 
								kMgrChoose, /* let ICM choose depth */
								VIDEO_CODEC_QUALITY, 
								VIDEO_CODEC_TYPE    , 
								(CompressorComponent) anyCodec,
								&maxCompressedSize);
	if(err!=noErr)
	{
		_tcscpy(m_szErrMsg,_T("Unable to Get Compression Size"));
		goto TerminateQuickTimeAppendFirstFrame;
	}

	m_compressedData = NewHandle(maxCompressedSize);
	if(m_compressedData==NULL)
	{
		_tcscpy(m_szErrMsg,_T("Unable to Allocate Memory"));
		goto TerminateQuickTimeAppendFirstFrame;
	}

	MoveHHi( m_compressedData );
	HLock( m_compressedData );
	m_compressedDataPtr = StripAddress( *m_compressedData );

	m_imageDesc = (ImageDescriptionHandle)NewHandle(4);
	if(m_imageDesc==NULL)
	{
		_tcscpy(m_szErrMsg,_T("Unable to Allocate Memory Handle"));
		goto TerminateQuickTimeAppendFirstFrame;
	}

	err = BeginMediaEdits (m_pMedia);
	if(err!=noErr)
	{
		_tcscpy(m_szErrMsg,_T("Unable to Begin Editing"));
		goto TerminateQuickTimeAppendFirstFrame;	
	}

	nAppendFuncSelector=2;

	return AppendFrameUsual();

TerminateQuickTimeAppendFirstFrame:

	ReleaseMemory();
	MessageBox(NULL,m_szErrMsg,_T("Error"),MB_OK|MB_ICONERROR);

	return E_FAIL;

}

HRESULT QTMovieFile::AppendFrameUsual()
{
	OSErr err = noErr;
	err = CompressImage (m_gWorld->portPixMap, 
						&m_trackFrame, 
						VIDEO_CODEC_QUALITY,
						VIDEO_CODEC_TYPE,
						m_imageDesc, 
						m_compressedDataPtr );
	if(err!=noErr)	return E_FAIL;

	err = AddMediaSample(m_pMedia, 
						m_compressedData,
						kNoOffset,	/* no offset in data */
						(**m_imageDesc).dataSize, 
						kSampleDuration,	/* frame duration = 1/10 sec */
						(SampleDescriptionHandle)m_imageDesc, 
						kAddOneVideoSample,	/* one sample */
						kSyncSample,	/* self-contained samples */
						nil);
	if(err!=noErr)	return E_FAIL;

	return S_OK;
}

HRESULT QTMovieFile::AppendDummy()
{
	return E_FAIL;
}

HRESULT QTMovieFile::AppendNewFrame(Math::matrixMxN<Util::frgba>& tiff, int sizeY, int sizeX)
{
	Math::matrixMxN<Util::rgb> bmp;
	bmp.init(tiff.sizeX(), tiff.sizeY());

	int w = tiff.sizeX();
	int h = tiff.sizeY();
	for(int y=0; y<h; y++)
	{
		for(int x=0; x<w; x++)
		{
			Util::frgba color = tiff[h-y-1][x];
//			Util::frgba color = tiff[y][x];
			Util::rgb d;
			d[0] = (unsigned char)(color[0]*255);
			d[1] = (unsigned char)(color[1]*255);
			d[2] = (unsigned char)(color[2]*255);
//			d[3] = (unsigned char)(color[3]*255);
			bmp[y][x] = d;
		}
	}
	return AppendNewFrame(bmp);
}

HRESULT QTMovieFile::AppendNewFrame(Math::matrixMxN<Util::rgb>& bmp, int sizeY, int sizeX)
{
	int x, y;
	for(y=0; y<bmp.sizeY(); y++)
	{
		if(sizeY>0 && y>=sizeY)
			continue;
		for(x=0; x<bmp.sizeX(); x++)
		{
			if(sizeX>0 && x>=sizeX)
				continue;
			Util::rgb& src = bmp[y][x];
			RGBColor c;
			c.blue = src.b<<8;
			c.green = src.g<<8;
			c.red = src.r<<8;
			SetCPixel(x, y, &c);
		}
	}

	return (this->*pAppendFrame[nAppendFuncSelector])();
}