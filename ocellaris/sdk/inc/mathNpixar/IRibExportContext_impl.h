#pragma once
#include "mathNpixar/IRibExportContext.h"
#include <map>


class IRibExportContext_impl : public IRibExportContext
{
// IRibExportContext
public:
	virtual const char* getSlimParameter_string(const char* name)
	{
		std::map<std::string, std::string>::iterator it = slimParams.find(name);
		if( it==slimParams.end()) return "";

		return it->second.c_str();
	}
	virtual float getSlimParameter_float(const char* name)
	{
		std::map<std::string, std::string>::iterator it = slimParams.find(name);
		if( it==slimParams.end()) return 0.f;

		return (float)atof(it->second.c_str());
	}

	virtual void GetRenderingPass(RenderingPass *p, RtBoolean *declarePhase)
	{
		context->GetRenderingPass( (RIBContext::RenderingPass*)p, declarePhase);
	}
	virtual RtBoolean GetMotionBlur()
	{
		return context->GetMotionBlur();
	}
	virtual int GetFrame()
	{
		return context->GetFrame();
	}
	virtual void GetShutter( RtFloat *open, RtFloat *close)
	{
		context->GetShutter( open, close);
	}
	virtual const char * GetJobName()
	{
		return context->GetJobName();
	}
	virtual const char * GetObjName()
	{
		return context->GetObjName();
	}
	virtual void GetCamBasis( RtMatrix m, int inverse = 0 )
	{
		context->GetCamBasis( m, inverse);
	}
//	virtual void ReportError( RenderingError e, const char *fmt, ... ) = 0;
	virtual void GetMotionInfo( 
		RtFloat *shutterAngle,
				RtFloat *fps,
				bool *subframeMotion,
				bool *blurCamera=0L,
				ShutterTiming *shutterTiming=0L,
				ShutterConfig *shutterConfig=0L
				)
	{
		RtBoolean _subframeMotion, _blurCamera;
		context->GetMotionInfo( 
				shutterAngle,
				fps,
				&_subframeMotion, &_blurCamera,
				*((RIBContext::ShutterTiming**)&shutterTiming),
				*((RIBContext::ShutterConfig**)&shutterConfig)
				);
		if( subframeMotion)
			*subframeMotion = _subframeMotion!=0;
		if( blurCamera)
			*blurCamera = _blurCamera!=0;
	}

	virtual RtBasis	* GetBasis( BasisFunction func)
	{
		return context->GetBasis( *((RIBContext::BasisFunction*)&func));
	}
	virtual void	  SetDefaultFilterFunction( FilterFunction func)
	{
		context->SetDefaultFilterFunction( *((RIBContext::FilterFunction*)&func));
	}
	virtual RtFilterFunc GetFilterFunction( FilterFunction func)
	{
		return (RtFilterFunc)context->GetFilterFunction( *((RIBContext::FilterFunction*)&func));
	}
	virtual RtFilterFunc GetFilterFunction( const char *nm )
	{
		return (RtFilterFunc)context->GetFilterFunction( nm);
	}
	virtual const char *GetOutputDir(OutDirCategory od)
	{
		return context->GetOutputDir( *((RIBContext::OutDirCategory*)&od));
	}

public:
	IRibExportContext_impl()
	{
		this->context = NULL;
	}
	void setParameter(const char* name, void* value)
	{
		std::string n = name, type = "";
		for( int i=0; name[i]; i++)
			if(name[i]==' '||name[i]=='\t')
			{
				type = std::string(name, i);
				n = std::string(name+i+1);
				break;
			}
		char buf[1024]="";
		if(type=="float")
			sprintf(buf, "%f", *(float*)(value));
		else if(type=="string")
			sprintf(buf, "%s", *(static_cast<char **>(value)));
		else if(type=="int")
			sprintf(buf, "%d", *(int*)(value));

		slimParams[n] = buf;
	}
	void setContext(RIBContext* context)
	{
		this->context = context;
	}
protected:
	RIBContext* context;
	std::map<std::string, std::string> slimParams;
};