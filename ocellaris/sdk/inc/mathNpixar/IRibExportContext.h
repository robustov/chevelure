#pragma once

#include <ri.h>
#include <vector>
#include <string>

struct IRibExportContext
{
	typedef enum
	{
		rpInvalid, // during caching
		rpTraverseOnly,
		rpFinal,
		rpShadow,
		rpReflection,
		rpEnvironment,
		rpDepth,
		rpReference,
		rpArchive,
		rpPhoton
	} RenderingPass;

	typedef enum
	{
		kDefaultBasis,
		kBezierBasis,
		kBSplineBasis,
		kCatmullRomBasis,
		kHermiteBasis, 
		kPowerBasis
	} BasisFunction;

	typedef enum
	{
		kDefaultFilter,
		kGaussianFilter,
		kBoxFilter,
		kTriangleFilter,
		kCatmullRomFilter,
		kSincFilter,
		kDiskFilter,
		kBesselFilter,
		kSeparableCatmullRomFilter,
		kBlackmanHarrisFilter,
		kMitchellFilter
	} FilterFunction;

	typedef enum
	{
		kShutterOpenOnFrame=0,
		kShutterCenterOnFrame,
		kShutterCloseOnFrame
	} ShutterTiming;

	typedef enum
	{
		kShutterMoving,
		kShutterStationary
	} ShutterConfig;

	typedef enum
	{
		kRib,
		kImage,
		kTmp,
		kShader,
		kTexture
	} OutDirCategory;

	// getSlimParameter
	virtual const char* getSlimParameter_string(const char* name)=0;
	virtual float getSlimParameter_float(const char* name)=0;

	virtual void GetRenderingPass(RenderingPass *p, RtBoolean *declarePhase) = 0;
	virtual RtBoolean GetMotionBlur() = 0;
	virtual int GetFrame() = 0;
	virtual void GetShutter( RtFloat *open, RtFloat *close) = 0;
	virtual const char * GetJobName() = 0;
	virtual const char * GetObjName() = 0;
	virtual void GetCamBasis( RtMatrix m, int inverse = 0 ) = 0;
//	virtual void ReportError( RenderingError e, const char *fmt, ... ) = 0;
	virtual void GetMotionInfo( 
		RtFloat *shutterAngle,
				RtFloat *fps,
				bool *subframeMotion,
				bool *blurCamera=0L,
				ShutterTiming *shutterTiming=0L,
				ShutterConfig *shutterConfig=0L
				) = 0;
	/*/
	virtual const RIBContextResult *
			ExecuteHostCmd( const char *cmd,
				std::string &errstr ) = 0;
	virtual const RIBContextResult *
			ExecuteHostCmd( const char *cmd,
				char** errstr ) = 0; // Caller must free errstr.
	/*/
	virtual RtBasis	* GetBasis( BasisFunction ) = 0;
	virtual void	  SetDefaultFilterFunction( FilterFunction ) = 0;
	virtual RtFilterFunc GetFilterFunction( FilterFunction ) = 0;
	virtual RtFilterFunc GetFilterFunction( const char *nm ) = 0;
	virtual const char *GetOutputDir(OutDirCategory) = 0;

};