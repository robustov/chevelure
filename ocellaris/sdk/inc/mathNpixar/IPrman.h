#pragma once

#include <ri.h>
#include <rx.h>

#ifndef _IPRMAN_NO_MATH
#include "Math/Math.h"
#include "Math/Vector.h"
#include "Math/Box.h"
#include "Math/Matrix.h"
#else
#	ifndef _ED_Math__h_
	namespace Math
	{
		struct Matrix4f
		{
		};
	}
#	endif
#endif

typedef RtVoid (*RtSubdivfunc)(RtPointer, RtFloat);
typedef RtVoid (*RtFreefunc)(RtPointer);

struct IPrman
{
	bool bExecuteProcedural;
	IPrman()
	{
		bExecuteProcedural = false;
	}

#ifndef _IPRMAN_NO_MATH
	static void copy(RtMatrix ct, const Math::Matrix4f& m)
	{
		for(int x=0; x<4; x++)
			for(int y=0; y<4; y++)
				ct[x][y] = m[x][y];
	}
	static void copy(RtBound bound, const Math::Box3f& box)
	{
		bound[0] = box.min.x; 
		bound[1] = box.max.x; 
		bound[2] = box.min.y; 
		bound[3] = box.max.y; 
		bound[4] = box.min.z; 
		bound[5] = box.max.z;
	}
	static void copy( RtFloat dst[3], const Math::Vec3f& src)
	{
		(dst)[0] = src.x;
		(dst)[1] = src.y;
		(dst)[2] = src.z;
	}
	static void copy( Math::Vec3f& dst, RtFloat src[3])
	{
		dst.x = (src)[0];
		dst.y = (src)[1];
		dst.z = (src)[2];
	}
#endif
	virtual int getRenderPass()=0;

	// ����� ��� ������� ���� ������
	enum enRenderMode
	{
		RENDERMODE_NONE				= 0,
		RENDERMODE_SHADER			= 1,
		RENDERMODE_GEOMETRY			= 2,
		RENDERMODE_ATTRIBUTEBEGIN	= 4,
		RENDERMODE_ALL				= 0xFF,
		RENDERMODE_P_ONLY			= 0x100,
	};
	virtual enRenderMode getRenderMode()=0;
	virtual void setRenderMode(enRenderMode mode)=0;

	virtual RtToken	RI_FRAMEBUFFER()=0; 
	virtual RtToken	RI_FILE()=0; 
	virtual RtToken	RI_RGB()=0; 
	virtual RtToken	RI_RGBA()=0; 
	virtual RtToken	RI_RGBZ()=0; 
	virtual RtToken	RI_RGBAZ()=0; 
	virtual RtToken	RI_A()=0; 
	virtual RtToken	RI_Z()=0; 
	virtual RtToken	RI_AZ()=0;
/*/
DLLIMPORT extern RtToken	RI_FRAMEBUFFER, RI_FILE;
DLLIMPORT extern RtToken	RI_RGB, RI_RGBA, RI_RGBZ, RI_RGBAZ, RI_A, RI_Z, RI_AZ;
DLLIMPORT extern RtToken  	RI_DEEPSHADOWERROR;
DLLIMPORT extern RtToken 	RI_ADDCOVERAGE;
DLLIMPORT extern RtToken	RI_I;
DLLIMPORT extern RtToken	RI_RGBI, RI_RGBAI, RI_RGBZI, RI_RGBAZI, RI_AI, RI_ZI, RI_AZI;
/*/
	virtual RtToken	RI_PERSPECTIVE()=0;
	virtual RtToken	RI_ORTHOGRAPHIC()=0;
/*/
DLLIMPORT extern RtToken	RI_HIDDEN, RI_PAINT, RI_DEPTHFILTER, RI_PHOTON;
DLLIMPORT extern RtToken	RI_CONSTANT, RI_SMOOTH;
DLLIMPORT extern RtToken  	RI_ORIGIN;
DLLIMPORT extern RtToken  	RI_FLATNESS, RI_MOTIONFACTOR, RI_FOCUSFACTOR, RI_OLDMOTIONFACTOR, 
				RI_TRIMDEVIATION, RI_FOV;
/*/
	virtual RtToken	RI_FOV()=0;
/*/
DLLIMPORT extern RtToken	RI_AMBIENTLIGHT, RI_POINTLIGHT, RI_DISTANTLIGHT, RI_SPOTLIGHT;
DLLIMPORT extern RtToken	RI_INTENSITY, RI_LIGHTCOLOR, RI_FROM, RI_TO, RI_CONEANGLE,
				RI_CONEDELTAANGLE, RI_BEAMDISTRIBUTION;
DLLIMPORT extern RtToken	_RI_SHADERINSTANCEID;
DLLIMPORT extern RtToken	RI_MATTE, RI_METAL, RI_SHINYMETAL;
DLLIMPORT extern RtToken	RI_PLASTIC, RI_PAINTEDPLASTIC;
DLLIMPORT extern RtToken	RI_KA, RI_KD, RI_KS, RI_ROUGHNESS, RI_SPECULARCOLOR;
DLLIMPORT extern RtToken	RI_KR, RI_TEXTURENAME;
DLLIMPORT extern RtToken	RI_DEPTHCUE, RI_FOG, RI_BUMPY;
DLLIMPORT extern RtToken	RI_MINDISTANCE, RI_MAXDISTANCE, RI_BACKGROUND, RI_DISTANCE;
DLLIMPORT extern RtToken	RI_AMPLITUDE;

DLLIMPORT extern RtToken	RI_RASTER, RI_SCREEN, RI_CAMERA, RI_WORLD, RI_OBJECT;
DLLIMPORT extern RtToken	RI_INSIDE, RI_OUTSIDE, RI_LH, RI_RH;
/*/
	virtual RtToken	RI_P()=0;
/*/
DLLIMPORT extern RtToken	RI_P, RI_PZ, RI_PW, RI_N, RI_NG, RI_NP, RI_CS, RI_OS, RI_CI, RI_OI,
		RI_S, RI_T, RI_ST;
DLLIMPORT extern RtToken	RI_MAXSPECULARDEPTH, RI_MAXDIFFUSEDEPTH;//ray//
DLLIMPORT extern RtToken	RI_DISPLACEMENTS, RI_BIAS, RI_SAMPLEMOTION; // for ray tracing//
DLLIMPORT extern RtToken	RI_DPDU, RI_DPDV; // for ray tracing //
DLLIMPORT extern RtToken	RI_ESTIMATOR; // for globillum and caustics //
DLLIMPORT extern RtToken	RI_SHADINGMODEL; // for photon tracing //
DLLIMPORT extern RtToken	RI_GLOBALMAP; // for global illum //
DLLIMPORT extern RtToken	RI_CAUSTICMAP; // for caustics //
DLLIMPORT extern RtToken	RI_MAXERROR, RI_MAXPIXELDIST; // irrad cache //
DLLIMPORT extern RtToken	RI_FORCEDSAMPLING; // irrad cache //
DLLIMPORT extern RtToken	RI_HANDLE, RI_FILEMODE; // irradiance cache //
/*/

	virtual RtToken	RI_BILINEAR()=0;
	virtual RtToken	RI_BICUBIC()=0;
	virtual RtToken	RI_LINEAR()=0;
	virtual RtToken	RI_CUBIC()=0;
/*/
DLLIMPORT extern RtToken	RI_PRIMITIVE, RI_INTERSECTION, RI_UNION, RI_DIFFERENCE;
DLLIMPORT extern RtToken	RI_PERIODIC, RI_NONPERIODIC, RI_CLAMP, RI_BLACK;
DLLIMPORT extern RtToken	RI_IGNORE, RI_PRINT, RI_ABORT, RI_HANDLER;
/*/
	virtual RtToken	RI_COMMENT()=0;
	virtual RtToken	RI_STRUCTURE()=0;
	virtual RtToken	RI_VERBATIM()=0;
/*/
DLLIMPORT extern RtToken	RI_IDENTIFIER, RI_NAME, RI_SHADINGGROUP;
DLLIMPORT extern RtToken	RI_WIDTH, RI_CONSTANTWIDTH;


DLLIMPORT extern RtToken 	RI_MINWIDTH, RI_CLAMPWIDTH;


// Added for 4.0 compile
DLLIMPORT extern RtToken	RI_DEVIATION, RI_RASTER, RI_TESSELATION;
DLLIMPORT extern RtToken  RI_TRIMCURVE, RI_PARAMETRIC;
//

DLLIMPORT extern RtToken 	RI_QUANTIZE, RI_DITHER, RI_FILTER, RI_FILTERWIDTH;
DLLIMPORT extern RtToken 	RI_REPELFILE, RI_REPELPARAMS;
DLLIMPORT extern RtToken 	RI_HANDLEID;
DLLIMPORT extern RtToken	RI_THRESHOLD;
DLLIMPORT extern RtBasis	RiBezierBasis, RiBSplineBasis, RiCatmullRomBasis,
		RiHermiteBasis, RiPowerBasis;

#define RI_BEZIERSTEP		((RtInt)3)
#define RI_BSPLINESTEP		((RtInt)1)
#define RI_CATMULLROMSTEP	((RtInt)1)
#define RI_HERMITESTEP		((RtInt)2)
#define RI_POWERSTEP		((RtInt)4)

DLLIMPORT extern RtInt	RiLastError;


	// Declarations of All the RenderMan Interface Subroutines //
/*/
	virtual RtFilterFunc RiGaussianFilter()=0;
	virtual RtFilterFunc RiBoxFilter()=0;
	virtual RtFilterFunc RiTriangleFilter()=0;
	virtual RtFilterFunc RiCatmullRomFilter()=0;
	virtual RtFilterFunc RiSeparableCatmullRomFilter()=0;
	virtual RtFilterFunc RiBlackmanHarrisFilter()=0;
	virtual RtFilterFunc RiLanczosFilter()=0;
	virtual RtFilterFunc RiMitchellFilter()=0;
	virtual RtFilterFunc RiSincFilter()=0;
	virtual RtFilterFunc RiBesselFilter()=0;
	virtual RtFilterFunc RiDiskFilter()=0;
/*/
DLLIMPORT extern RtVoid	RiErrorIgnore(RtInt code, RtInt severity, char *msg);
DLLIMPORT extern RtVoid	RiErrorPrint(RtInt code, RtInt severity, char *msg);
DLLIMPORT extern RtVoid	RiErrorPrintOnce(RtInt code, RtInt severity, char *msg);
DLLIMPORT extern RtVoid	RiErrorCondAbort(RtInt code, RtInt severity, char *msg);
DLLIMPORT extern RtVoid	RiErrorAbort(RtInt code, RtInt severity, char *msg);
DLLIMPORT extern RtVoid   RiErrorCleanup(void);
/*/

	virtual RtSubdivfunc RiProcDelayedReadArchive()=0;
	virtual RtSubdivfunc RiProcRunProgram()=0;
	virtual RtSubdivfunc RiProcDynamicLoad()=0;

/*/
DLLIMPORT extern RtContextHandle RiGetContext(void);
DLLIMPORT extern RtVoid RiContext(RtContextHandle);

DLLIMPORT extern RtToken
	RiDeclare(char *name, char *declaration);

DLLIMPORT extern RtVoid
/*/
	virtual RtVoid RiBegin(RtToken name)=0;
	virtual RtVoid RiEnd(void)=0;
	virtual RtVoid RiFrameBegin(RtInt frame)=0;
	virtual RtVoid RiFrameEnd(void)=0;
	virtual RtVoid RiWorldBegin(void)=0;
	virtual RtVoid RiWorldEnd(void)=0;

	virtual RtVoid RiFormat(RtInt xres, RtInt yres, RtFloat aspect)=0;
	virtual RtVoid RiFrameAspectRatio(RtFloat aspect)=0;
	virtual RtVoid RiScreenWindow(RtFloat left, RtFloat right, RtFloat bot, RtFloat top)=0;
	virtual RtVoid RiCropWindow(RtFloat xmin, RtFloat xmax, RtFloat ymin, RtFloat ymax)=0;
	virtual RtVoid RiProjection(RtToken name, ...)=0;
	virtual RtVoid RiProjectionV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[])=0;
	virtual RtVoid RiClipping(RtFloat hither, RtFloat yon)=0;
//	virtual RtVoid RiClippingPlane(RtFloat Nx, RtFloat Ny, RtFloat Nz, RtFloat Px, 
//                        RtFloat Py, RtFloat Pz)=0;
	virtual RtVoid RiDepthOfField(RtFloat fstop, RtFloat focallength, RtFloat focaldistance)=0;
	virtual RtVoid RiShutter(RtFloat min, RtFloat max)=0;
/*/

DLLIMPORT extern RtVoid
	RiPixelVariance(RtFloat variation),
	RiPixelFidelity(RtFloat variation),
	/*/
	virtual RtVoid RiPixelSamples(RtFloat xsamples, RtFloat ysamples)=0;
	virtual RtVoid RiPixelFilter(RtFilterFunc filterfunc, RtFloat xwidth, RtFloat ywidth)=0;
	virtual RtVoid RiExposure(RtFloat gain, RtFloat gamma)=0;
	virtual RtVoid RiImager(RtToken name, ...)=0;
	virtual RtVoid RiImagerV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[])=0;
	virtual RtVoid RiQuantize(RtToken type, RtInt one, RtInt min, RtInt max, RtFloat ampl)=0;
	/*/
	/*/
	virtual RtVoid RiDisplay(char *name, RtToken type, RtToken mode, ...)=0;
	virtual RtVoid RiDisplayV(char *name, RtToken type, RtToken mode,
			RtInt n, RtToken tokens[], RtPointer parms[])=0;

	virtual RtVoid RiDisplayChannel(RtToken channel, ...)=0;
	virtual RtVoid RiDisplayChannelV(RtToken channel, RtInt n, RtToken tokens[], RtPointer parms[])=0;

	virtual RtVoid RiHider(RtToken type, ...)=0;
	virtual RtVoid RiHiderV(RtToken type, RtInt n, RtToken tokens[], RtPointer parms[])=0;

	virtual RtVoid RiColorSamples(RtInt n, RtFloat nRGB[], RtFloat RGBn[])=0;
	virtual RtVoid RiRelativeDetail(RtFloat relativedetail)=0;
	virtual RtVoid RiOption(RtToken name, ...)=0;
/*/
	RiOptionV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[]);

DLLIMPORT extern RtVoid
/*/
	virtual RtVoid RiAttributeBegin(void)=0;
	virtual RtVoid RiAttributeEnd(void)=0;
	virtual RtVoid RiColor(RtColor color)=0;
	virtual RtVoid RiOpacity(RtColor color)=0;
	virtual RtVoid RiTextureCoordinates(RtFloat s1, RtFloat t1, RtFloat s2, RtFloat t2,
				RtFloat s3, RtFloat t3, RtFloat s4, RtFloat t4)=0;
/*/

	virtual RtLightHandle RiLightSource(RtToken name, ...)=0;
/*/
	virtual RtLightHandle RiLightSourceV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[])=0;
/*/
	virtual RtLightHandle RiAreaLightSource(RtToken name, ...)=0;
	virtual RtLightHandle RiAreaLightSourceV(RtToken name,
				RtInt n, RtToken tokens[], RtPointer parms[])=0;

DLLIMPORT extern RtVoid
	RiIlluminate(RtLightHandle light, RtBoolean onoff),
/*/
	virtual RtVoid RiSurface(RtToken name, ...)=0;
	virtual RtVoid RiSurfaceV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[])=0;
/*/
	RiAtmosphere(RtToken name, ...),
	RiAtmosphereV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[]),
	RiInterior(RtToken name, ...),
	RiInteriorV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[]),
	RiExterior(RtToken name, ...),
	RiExteriorV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[]),
/*/
	virtual RtVoid RiShadingRate(RtFloat size)=0;
	virtual RtVoid RiShadingInterpolation(RtToken type)=0;
	virtual RtVoid RiMatte(RtBoolean onoff)=0;
/*/

DLLIMPORT extern RtVoid
/*/
	virtual RtVoid RiBound(RtBound bound)=0;
	virtual RtVoid RiDetail(RtBound bound)=0;
	virtual RtVoid RiDetailRange(RtFloat minvis, RtFloat lowtran, RtFloat uptran,
			RtFloat maxvis)=0;
/*/
	RiGeometricApproximation(RtToken type, RtFloat value),
	RiOrientation(RtToken orientation),
/*/
	virtual RtVoid RiReverseOrientation(void)=0;
	virtual RtVoid RiSides(RtInt sides)=0;
	virtual RtVoid RiIdentity(void)=0;
	virtual RtVoid RiTransform(RtMatrix transform)=0;
	virtual RtVoid RiConcatTransform(RtMatrix transform)=0;
	virtual RtVoid RiConcatTransform(Math::Matrix4f& m)=0;
	virtual RtVoid RiPerspective(RtFloat fov)=0;
	virtual RtVoid RiTranslate(RtFloat dx, RtFloat dy, RtFloat dz)=0;
	virtual RtVoid RiRotate(RtFloat angle, RtFloat dx, RtFloat dy, RtFloat dz)=0;
	virtual RtVoid RiScale(RtFloat sx, RtFloat sy, RtFloat sz)=0;
	virtual RtVoid RiSkew(RtFloat angle, RtFloat dx1, RtFloat dy1, RtFloat dz1,
		RtFloat dx2, RtFloat dy2, RtFloat dz2)=0;
/*/
	RiDeformation(RtToken name, ...),
	RiDeformationV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[]),
/*/
	virtual RtVoid RiDisplacement(RtToken name, ...)=0;
	virtual RtVoid RiDisplacementV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[])=0;
/*/
	RiScopedCoordinateSystem(RtToken space),
	RiCoordSysTransform(RtToken space);
DLLIMPORT extern RtPoint
	*RiTransformPoints(RtToken fromspace, RtToken tospace, RtInt n,
			   RtPoint points[]);
DLLIMPORT extern RtVoid
/*/
	virtual RtVoid RiTransformBegin(void)=0;
	virtual RtVoid RiTransformEnd(void)=0;

	virtual RtVoid RiAttribute(RtToken name, ...)=0;
/*/
	RiAttributeV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[]);

DLLIMPORT extern RtVoid
	RiPolygon(RtInt nverts, ...),
/*/
	virtual RtVoid RiPolygonV(RtInt nverts, RtInt n, RtToken tokens[], RtPointer parms[])=0;
/*/
	RiGeneralPolygon(RtInt nloops, RtInt nverts[], ...),
	RiGeneralPolygonV(RtInt nloops, RtInt nverts[],
				RtInt n, RtToken tokens[], RtPointer parms[]),
	RiPointsPolygons(RtInt npolys, RtInt nverts[], RtInt verts[], ...),
/*/
	virtual RtVoid RiPointsPolygonsV(RtInt npolys, RtInt nverts[], RtInt verts[],
				RtInt n, RtToken tokens[], RtPointer parms[])=0;
/*/
	RiPointsGeneralPolygons(RtInt npolys, RtInt nloops[], RtInt nverts[],
				RtInt verts[], ...),
/*/
	virtual RtVoid RiPointsGeneralPolygonsV(RtInt npolys, RtInt nloops[], RtInt nverts[],
			RtInt verts[], RtInt n, RtToken tokens[], RtPointer parms[])=0;
	virtual RtVoid RiBasis(RtBasis ubasis, RtInt ustep, RtBasis vbasis, RtInt vstep)=0;

	virtual RtVoid RiPatch(RtToken type, ...)=0;
	virtual RtVoid RiPatchV(RtToken type, RtInt n, RtToken tokens[], RtPointer parms[])=0;
	virtual RtVoid RiPatchMesh(RtToken type, RtInt nu, RtToken uwrap,
			RtInt nv, RtToken vwrap, ...)=0;
	virtual RtVoid RiPatchMeshV(RtToken type, RtInt nu, RtToken uwrap,
			RtInt nv, RtToken vwrap,
			RtInt n, RtToken tokens[], RtPointer parms[])=0;
	virtual RtVoid RiNuPatch(RtInt nu, RtInt uorder, RtFloat uknot[], RtFloat umin, RtFloat umax,
			RtInt nv, RtInt vorder, RtFloat vknot[], RtFloat vmin,
			RtFloat vmax, ...)=0;
	virtual RtVoid RiNuPatchV(RtInt nu, RtInt uorder, RtFloat uknot[], RtFloat umin,
			RtFloat umax, RtInt nv, RtInt vorder, RtFloat vknot[],
			RtFloat vmin, RtFloat vmax,
			RtInt n, RtToken tokens[], RtPointer parms[])=0;
/*/

DLLIMPORT extern RtVoid
	RiSphere(RtFloat radius, RtFloat zmin, RtFloat zmax, RtFloat tmax, ...),
	/*/
	virtual RtVoid RiSphereV(RtFloat radius, RtFloat zmin, RtFloat zmax, RtFloat tmax,
			RtInt n, RtToken tokens[], RtPointer parms[])=0;
	/*/
	RiCone(RtFloat height, RtFloat radius, RtFloat tmax, ...),
	RiConeV(RtFloat height, RtFloat radius, RtFloat tmax,
			RtInt n, RtToken tokens[], RtPointer parms[]),
	RiCylinder(RtFloat radius, RtFloat zmin, RtFloat zmax, RtFloat tmax, ...),
	RiCylinderV(RtFloat radius, RtFloat zmin, RtFloat zmax, RtFloat tmax,
			RtInt n, RtToken tokens[], RtPointer parms[]),
	RiHyperboloid(RtPoint point1, RtPoint point2, RtFloat tmax, ...),
	RiHyperboloidV(RtPoint point1, RtPoint point2, RtFloat tmax,
			RtInt n, RtToken tokens[], RtPointer parms[]),
	RiParaboloid(RtFloat rmax, RtFloat zmin, RtFloat zmax, RtFloat tmax, ...),
	RiParaboloidV(RtFloat rmax, RtFloat zmin, RtFloat zmax, RtFloat tmax,
			RtInt n, RtToken tokens[], RtPointer parms[]),
	RiDisk(RtFloat height, RtFloat radius, RtFloat tmax, ...),
	RiDiskV(RtFloat height, RtFloat radius, RtFloat tmax,
			RtInt n, RtToken tokens[], RtPointer parms[]),
	RiTorus(RtFloat majrad, RtFloat minrad, RtFloat phimin, RtFloat phimax,
			RtFloat tmax, ...),
	RiTorusV(RtFloat majrad, RtFloat minrad, RtFloat phimin, RtFloat phimax,
			RtFloat tmax, RtInt n, RtToken tokens[], RtPointer parms[]);

DLLIMPORT extern RtVoid RiBlobby(RtInt nleaf, RtInt ncode, RtInt code[], 
		       RtInt nflt, RtFloat flt[],
		       RtInt nstr, RtToken str[], ...);
			   /*/
	virtual RtVoid RiBlobbyV(RtInt nleaf, RtInt ncode, RtInt  code[], 
			RtInt nflt, RtFloat flt[],
			RtInt nstr, RtToken str[],
			RtInt n , RtToken tokens[], RtPointer parms[])=0;
			   /*/


DLLIMPORT extern RtVoid
	RiCurves(RtToken type, RtInt ncurves,
		 RtInt nvertices[], RtToken wrap, ...),
/*/
	virtual RtVoid RiCurvesV(RtToken type, RtInt ncurves, RtInt nvertices[], RtToken wrap,
		  RtInt n, RtToken tokens[], RtPointer parms[])=0;
/*/
	RiPoints(RtInt nverts,...),
/*/
	virtual RtVoid RiPointsV(RtInt nverts, RtInt n, RtToken tokens[], RtPointer parms[])=0;
/*/
	RiSubdivisionMesh(RtToken mask, RtInt nf, RtInt nverts[],
			  RtInt verts[],
			  RtInt ntags, RtToken tags[], RtInt numargs[],
			  RtInt intargs[], RtFloat floatargs[], ...),
			  /*/
	virtual RtVoid RiSubdivisionMeshV(RtToken mask, RtInt nf, RtInt nverts[],
			   RtInt verts[], RtInt ntags, RtToken tags[],
			   RtInt nargs[], RtInt intargs[],
			   RtFloat floatargs[], RtInt n,
			   RtToken tokens[], RtPointer *parms)=0;

	virtual RtVoid RiHierarchicalSubdivisionMeshV(
				RtToken mask, RtInt nf, RtInt nverts[],
			   RtInt verts[], RtInt ntags, RtToken tags[],
			   RtInt nargs[], RtInt intargs[],
			   RtFloat floatargs[], RtToken stringargs[], RtInt n,
			   RtToken tokens[], RtPointer *parms)=0;

	virtual RtVoid RiTrimCurve(RtInt nloops, RtInt ncurves[], RtInt order[], RtFloat knot[],
			RtFloat min[], RtFloat max[], RtInt n[],
			RtFloat u[], RtFloat v[], RtFloat w[])=0;
	virtual RtVoid RiProcedural(RtPointer data, RtBound bound,
			RtVoid (*subdivfunc)(RtPointer, RtFloat),
			RtVoid (*freefunc)(RtPointer))=0;
	virtual RtVoid RiGeometry(RtToken type, ...)=0;
	virtual RtVoid RiGeometryV(RtToken type, RtInt n, RtToken tokens[], RtPointer parms[])=0;
			/*/

DLLIMPORT extern RtVoid
	RiSolidBegin(RtToken operation),
	RiSolidEnd(void);
DLLIMPORT extern RtObjectHandle
	RiObjectBegin(void);
DLLIMPORT extern RtVoid
	RiObjectEnd(void),
	RiObjectInstance(RtObjectHandle handle),
	RiMotionBegin(RtInt n, ...),
/*/
	virtual RtVoid RiMotionBeginV(RtInt n, RtFloat times[])=0;
	virtual RtVoid RiMotionEnd(void)=0;

/*/
DLLIMPORT extern RtVoid
	RiResource(RtToken handle, RtToken type, ...),
	RiResourceV(RtToken handle, RtToken type,
		RtInt n, RtToken tokens[], RtPointer parms[]);

DLLIMPORT extern RtVoid
	RiMakeTexture(char *pic, char *tex, RtToken swrap, RtToken twrap,
		RtFilterFunc filterfunc, RtFloat swidth, RtFloat twidth, ...),
	RiMakeTextureV(char *pic, char *tex, RtToken swrap, RtToken twrap,
		RtFilterFunc filterfunc, RtFloat swidth, RtFloat twidth,
		RtInt n, RtToken tokens[], RtPointer parms[]),
	RiMakeBump(char *pic, char *tex, RtToken swrap, RtToken twrap,
		RtFilterFunc filterfunc, RtFloat swidth, RtFloat twidth, ...),
	RiMakeBumpV(char *pic, char *tex, RtToken swrap, RtToken twrap,
		RtFilterFunc filterfunc, RtFloat swidth, RtFloat twidth,
		RtInt n, RtToken tokens[], RtPointer parms[]),
	RiMakeLatLongEnvironment(char *pic, char *tex, RtFilterFunc filterfunc,
		RtFloat swidth, RtFloat twidth, ...),
	RiMakeLatLongEnvironmentV(char *pic, char *tex, RtFilterFunc filterfunc,
		RtFloat swidth, RtFloat twidth,
		RtInt n, RtToken tokens[], RtPointer parms[]),
	RiMakeCubeFaceEnvironment(char *px, char *nx, char *py, char *ny,
		char *pz, char *nz, char *tex, RtFloat fov,
		RtFilterFunc filterfunc, RtFloat swidth, RtFloat ywidth, ...),
	RiMakeCubeFaceEnvironmentV(char *px, char *nx, char *py, char *ny,
		char *pz, char *nz, char *tex, RtFloat fov,
		RtFilterFunc filterfunc, RtFloat swidth, RtFloat ywidth,
		RtInt n, RtToken tokens[], RtPointer parms[]),
	RiMakeShadow(char *pic, char *tex, ...),
	RiMakeShadowV(char *pic, char *tex,
		RtInt n, RtToken tokens[], RtPointer parms[]);

/*/
	virtual void RiErrorHandler(RtErrorHandler handler)=0;
/*/

DLLIMPORT extern RtVoid
	RiSynchronize(RtToken);

DLLIMPORT extern RtVoid
/*/
	virtual void RiArchiveRecord(RtToken type, char *format)=0;
/*/
	RiReadArchive(RtToken name, RtVoid (*callback)(RtToken,char*,...), ...),
	RiReadArchiveV(RtToken name, RtVoid (*callback)(RtToken,char*,...),
		RtInt n, RtToken tokens[], RtPointer parms[]);

DLLIMPORT extern RtArchiveHandle
	RiArchiveBegin(RtToken name, ...),
    	RiArchiveBeginV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[]);

DLLIMPORT extern RtVoid
	RiArchiveEnd(void);

DLLIMPORT extern RtVoid
	RiIfBegin(char *expr, ...),
	RiIfBeginV(char *expr, RtInt n, RtToken tokens[], RtPointer parms[]),
	RiElseIf(char *expr, ...),
	RiElseIfV(char *expr, RtInt n, RtToken tokens[], RtPointer parms[]),
	RiElse(void),
	RiIfEnd(void);


// OBSOLETE call: see RiErrorHandler //
DLLIMPORT extern RtVoid
	RiErrorMode(RtToken mode, RtErrorHandler handler);
/*/

	virtual RtVoid RiCoordinateSystem(RtToken space)=0;

	// ��� ������
	virtual void RxOption(const char *name, void *result, int resultlen,
				RxInfoType_t *resulttype, int *resultcount){};
};

typedef IPrman* (*t_getIPrman)(void);

#include "Util/DllProcedure.h"
typedef Util::DllProcedureTmpl< t_getIPrman> getIPrmanDll;

//	USAGE:
//	{
//	#ifndef _DEBUG
//		getIPrmanDll prmanproc("IPrman.dll", "getIPrman");
//	#else
//		getIPrmanDll prmanproc("IPrmanD.dll", "getIPrman");
//	#endif
// 		IPrman* prman = (*prmanproc)();
//	}
