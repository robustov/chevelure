#pragma once

#include <slo.h>

/*/
#include "Math/Math.h"
#include "Math/Vector.h"
#include "Math/Box.h"
#include "Math/Matrix.h"
/*/

struct ISlo
{
	virtual void Slo_SetPath(const char *path)=0;
	virtual const char *Slo_GetPath(void)=0;
	virtual int Slo_SetShader(const char *name)=0;
	virtual const char *Slo_GetName(void)=0;
	virtual SLO_TYPE Slo_GetType(void)=0;
	virtual int Slo_GetNArgs(void)=0;
	virtual SLO_VISSYMDEF *Slo_GetArgByName(const char *name)=0;
	virtual SLO_VISSYMDEF *Slo_GetArgById(int id)=0;
	virtual SLO_VISSYMDEF *Slo_GetArrayArgElement(SLO_VISSYMDEF *array, int index)=0;
	virtual void Slo_EndShader(void)=0;
	virtual char *Slo_TypetoStr(SLO_TYPE type)=0;
	virtual char *Slo_StortoStr(SLO_STORAGE storage)=0;
	virtual char *Slo_DetailtoStr(SLO_DETAIL detail)=0;
};

typedef ISlo* (*t_getISlo)(void);

#include "Util/DllProcedure.h"
typedef Util::DllProcedureTmpl< t_getISlo> getISloDll;

/*/
USAGE:
{
#ifndef _DEBUG
	getISloDll SloDll("IPrman12.5.dll", "getISlo");
#else
	getISloDll SloDll("IPrman12.5D.dll", "getISlo");
#endif
	if( !SloDll.isValid())
		return;
	ISlo* slo = (*SloDll)();
}
/*/


