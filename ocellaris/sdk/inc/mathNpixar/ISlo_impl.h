#pragma once

#include "ISlo.h"
#include <vector>
struct Slo_Impl : public ISlo
{
	virtual void Slo_SetPath(const char *path)
	{
		::Slo_SetPath( (char*)path);
	}
	virtual const char *Slo_GetPath(void)
	{
		return ::Slo_GetPath();
	}
	virtual int Slo_SetShader(const char *name)
	{
		return ::Slo_SetShader((char*)name);
	}
	virtual const char *Slo_GetName(void)
	{
		return ::Slo_GetName();
	}
	virtual SLO_TYPE Slo_GetType(void)
	{
		return ::Slo_GetType();
	}
	virtual int Slo_GetNArgs(void)
	{
		return ::Slo_GetNArgs();
	}
	virtual SLO_VISSYMDEF *Slo_GetArgByName(const char *name)
	{
		return ::Slo_GetArgByName((char*)name);
	}
	virtual SLO_VISSYMDEF *Slo_GetArgById(int id)
	{
		return ::Slo_GetArgById(id);
	}
	virtual SLO_VISSYMDEF *Slo_GetArrayArgElement(SLO_VISSYMDEF *array, int index)
	{
		return ::Slo_GetArrayArgElement(array, index);
	}
	virtual void Slo_EndShader(void)
	{
		::Slo_EndShader();
	}
	virtual char *Slo_TypetoStr(SLO_TYPE type)
	{
		return ::Slo_TypetoStr(type);
	}
	virtual char *Slo_StortoStr(SLO_STORAGE storage)
	{
		return ::Slo_StortoStr(storage);
	}
	virtual char *Slo_DetailtoStr(SLO_DETAIL detail)
	{
		return ::Slo_DetailtoStr(detail);
	}
};