#ifndef _ED_Util_config_h_
#define _ED_Util_config_h_
#pragma once


#ifdef ED_UTIL_INTERNALS
#define ED_CORE_INTERNALS
#endif


#include "../_config.h"


#ifndef ED_UTIL_EXTERN
#define ED_UTIL_EXTERN ED_CORE_EXTERN
#endif


#ifndef ED_UTIL_API
#define ED_UTIL_API ED_CORE_API
#endif


/**
 * @brief Utility classes.
 */
namespace Util {}


#endif /* _ED_Util_config_h_ */
