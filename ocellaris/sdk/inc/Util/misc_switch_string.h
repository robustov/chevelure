#ifndef __MISC_SWITCH_STRING_H__
#define __MISC_SWITCH_STRING_H__

#pragma warning(disable : 4503 )
#pragma warning(disable : 4200 )
#pragma warning(disable : 4786 )
#include <map>

namespace misc
{
	// ����� ���������� ����� � �������,
	// ������� ����� ������������ � switch() �����������
	class switch_string
	{
		// �������� ��� ����� ��������
		struct check_member
		{
			typedef const char* TYPE;
			bool operator()(const TYPE& x, const TYPE& y) const
			{
				return (_stricmp(x, y) < 0);
			}
		};

		typedef std::map< const char*, int, check_member> tag_list;
		tag_list strlist;
	public:
		struct Token
		{ 
			char* token;
			int tag;
		};
		switch_string( Token* tokens, int length=-1)//:strlist(check_member)
		{
			if( tokens) append(tokens, length);
		};
		void append(Token* tokens, int len=-1)
		{
			for(int i=0; ; i++)
			{
				Token* pT = tokens + i;
				if( len>=0) 
				{
					if( i<len) 
						break;
				}
				else if(!pT->token) 
						break;
				strlist[pT->token] = pT->tag;
			}
		}
		int match(const char* str) const
		{
			tag_list::const_iterator it = strlist.find(str);
			if( it==strlist.end()) return -1;
			return it->second;
		}
		unsigned long size()
		{
			return (unsigned long)strlist.size();
		}
	};
/*/
	// ����� ���������� ����� � �������,
	// ������� ����� ������������ � switch() �����������
	template<class T, class TYPE=char*> 
	class switch_string
	{
		TYPE T::* textmember;
		// �������� ��� ����� ��������
		struct check_member
		{
			TYPE T::* textmember;
			check_member(){this->textmember = NULL;}
			check_member(TYPE T::* textmember){this->textmember = textmember;}

			bool operator()(const TYPE& x, const TYPE& y) const
			{
				return (stricmp(x, y) < 0);
			}
		};

		typedef std::map< TYPE, T*, check_member> tag_list;
		tag_list strlist;
	public:
		switch_string( TYPE T::* textmember, T* array=NULL, unsigned long len=0):
			strlist(check_member(textmember))
		{
			this->textmember=textmember;
			if( array) append(array, len);
		};
		void append(T* array, unsigned long len)
		{
			for(int i=0; i<len; i++)
			{
				T* pT = array + i;
				TYPE str = pT->*textmember;
				if( !str) continue;
				strlist[str] = pT;
			}
		}
		T* match(TYPE str) const
		{
			tag_list::const_iterator it = strlist.find(str);
			if( it==strlist.end()) return NULL;
			return it->second;
		}
		unsigned long size()
		{
			return strlist.size();
		}
	};
/*/
}

#endif
