#pragma once

#include <windows.h>
#include <set>
#include <list>
#include <vector>
#include "Util/misc_create_directory.h"

// ������ ������ � ����������
// void Util::getFileList(
//	const TCHAR* dirname, 
//	const TCHAR* mask, 
//	tag_filedatalist& files)

namespace Util
{
	struct FileOperation
	{
		std::string startdirname;

		virtual bool onDirectory(
			const TCHAR* dir, int level
			){return true;};
		virtual void postDirectory(
			const TCHAR* dir, int level
			){};
		virtual bool onFile(
			const TCHAR* dir, 
			const TCHAR* file, 
			const WIN32_FIND_DATA* fromdata
			){return true;};
	};

	bool Directory_Processing(
		FileOperation& operation, 
		const TCHAR* dir);
}

namespace Util
{
	bool Directory_Processing(
		FileOperation& operation, 
		int level, const TCHAR* dir);

	inline bool operator ==(const FILETIME& arg1, const FILETIME& arg2)
	{
		return (arg1.dwHighDateTime == arg2.dwHighDateTime) && (arg1.dwLowDateTime==arg2.dwLowDateTime);
	}
	inline bool operator <(const FILETIME& arg1, const FILETIME& arg2)
	{
		if( arg1.dwHighDateTime < arg2.dwHighDateTime) return true;
		if( arg1.dwHighDateTime > arg2.dwHighDateTime) return false;
		return arg1.dwLowDateTime < arg2.dwLowDateTime;
	}

	struct filedatalist_march
	{
		bool compare_by_date;
		filedatalist_march(bool compare_by_date=false)
		{
			this->compare_by_date = compare_by_date;
		}

		bool operator ()(const WIN32_FIND_DATA& arg1, const WIN32_FIND_DATA& arg2) const
		{
			if( compare_by_date)
			{
				return arg1.ftLastWriteTime < arg2.ftLastWriteTime;
			}
			else
			{
				int dir1 = arg1.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY;
				int dir2 = arg2.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY;
				if(dir2<dir1) return true;
				if(dir2>dir1) return false;

				return _strcmpi(arg1.cFileName, arg2.cFileName)<0;
			}
		}
	};
	typedef std::set<WIN32_FIND_DATA, filedatalist_march> tag_filedatalist;

	void getFileList(const TCHAR* dirname, const TCHAR* mask, tag_filedatalist& files);
};

inline bool Util::Directory_Processing(
	FileOperation& operation, 
	const TCHAR* dir)
{
	operation.startdirname = dir;
	{
		Util::pushBackSlash(operation.startdirname);
	}
	return Directory_Processing(operation, 0, "");
}

inline bool Util::Directory_Processing(
	FileOperation& operation, 
	int level, const TCHAR* dir)
{
	bool res = operation.onDirectory(dir, level);
	if( !res) return false;

	
	std::string from = operation.startdirname + dir;
	tag_filedatalist from_list;
	getFileList( from.c_str(), "*.*", from_list);

	res = true;
	tag_filedatalist::iterator it = from_list.begin();
	for(;it != from_list.end(); it++)
	{
		const WIN32_FIND_DATA& FindFileData = *it;

		std::string from_file = std::string(from)+ it->cFileName;

		// dir
		if( (FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) !=0)
		{
			std::string newdir = std::string(dir) + it->cFileName;
			newdir += '\\';

			res &= Directory_Processing( 
				operation, level+1, newdir.c_str());
		}
		else
		{
			res &= operation.onFile(
				dir, 
				it->cFileName,
				&FindFileData);
		}
	}
	operation.postDirectory(from.c_str(), level);
	return res;
}

inline void Util::getFileList(const TCHAR* dirname, const TCHAR* mask, tag_filedatalist& files)
{
	int count = 0;
	std::string strDirName = dirname;
	Util::pushBackSlash(strDirName);
	strDirName += mask;

	WIN32_FIND_DATA FindFileData;
	HANDLE hFind;
	hFind = FindFirstFile(strDirName.c_str(), &FindFileData);
	if (hFind == INVALID_HANDLE_VALUE) 
	{
		if( GetLastError()==3) return;
		printf ("%s", strDirName.c_str());
		printf ("Invalid File Handle. GetLastError reports %d\n", GetLastError ());
		return;
	}
	BOOL res = TRUE;
	for(; res; res=FindNextFile(hFind, &FindFileData))
	{
		if( strcmp(FindFileData.cFileName, ".")==0 ||
			strcmp(FindFileData.cFileName, "..")==0)
				continue;

		TCHAR fullname[1024];
		strcpy(fullname, dirname);
		strcat(fullname, "\\");
		strcat(fullname, FindFileData.cFileName);

		files.insert(FindFileData);
	}
	FindClose(hFind);
}
