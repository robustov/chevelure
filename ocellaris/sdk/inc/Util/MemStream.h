#pragma once

#include <vector>
#include <Util/Stream.h>
#include <Util/STLStream.h>

/// ��������� �������, ������� ������������� ������ � ������
namespace Util
{
class MemStream : public Stream
{
public:
	bool bSaveMode;
	// ��� ������
	std::vector<char> writedata;
	// ��� ������
	const char* readdata;
	unsigned cur;
	unsigned readlength;
public:
	MemStream()
	{
	}
	~MemStream()
	{
	}
	MemStream& operator=(const MemStream& arg)
	{
		this->writedata = arg.writedata;
		startReading();
		return *this;
	}
	bool open(const char* data, unsigned length)
	{
		this->bSaveMode = false;
		this->writedata.clear();
		this->cur = 0;
		this->readlength = length;
		this->readdata = data;
		return true;
	}
	bool open()
	{
		this->bSaveMode = true;
		this->writedata.clear();
		this->cur = 0;
		this->readlength = 0;
		this->readdata = 0;
		return true;
	};
	void close()
	{	
		startReading();
	}
	void startReading()
	{
		this->readlength = (unsigned int)this->writedata.size();
		this->readdata = this->writedata.empty()?NULL:&this->writedata[0];
		this->cur = 0;
		this->bSaveMode = false;
	}

	/// ����� ������
	virtual bool isLoading(){return !bSaveMode;}
	virtual bool isSaving(){return bSaveMode;}
//	virtual bool is_ascii(){return false;}

	/// ������������
	virtual Stream& serialize(void* data, unsigned size)
	{
		if(bSaveMode)
		{
			size_t d = writedata.capacity()-writedata.size();
			if(d<size)
			{
				size_t ns = writedata.size()+size;
				if(writedata.size()*2 > ns) ns = writedata.size()*2;
				writedata.reserve(ns);
			}
			writedata.insert(writedata.end(), (char*)data, (char*)data+size);
		}
		else
		{
			if(cur+size > readlength)
			{
				cur = readlength;
			}
			else
			{
				memcpy(data, readdata+cur, size);
				cur+=size;
			}
		}
		return *this;
	}
	void serialize(Util::Stream& out)
	{
		int version = 0;
		out >> version;
		if( version>=0)
		{
			if( out.isLoading())
			{
				int s;
				out >> s;
				if(s)
				{
					writedata.resize(s);
					out.serialize(&writedata[0], s);
				}
			}
			else
			{
				int s = (int)writedata.size();
				out >> s;
				if(s)
				{
					out.serialize(&writedata[0], s);
				}
			}
		}
		this->startReading();
	}
	#ifdef OCELLARIS
	void serializeOcs(
		const char* _name, 
		cls::IRender* render, 
		bool bSave
		)
	{
		std::string name = _name;

		if(bSave)
		{
			cls::PA<char> value(cls::PI_CONSTANT, writedata);
			render->Parameter( name.c_str(), value);
		}
		else
		{
			cls::PA<char> value = render->GetParameter( name.c_str());
			value.data(writedata);
		}
		this->startReading();
	}
	#endif

	/*/
	template<typename T>
	MemStream& ascii(T& data)
	{
		serialize(&data, sizeof(T));
		return *this;
	}
	/*/
};
}
