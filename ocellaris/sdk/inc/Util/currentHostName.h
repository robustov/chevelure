#pragma once

#include <string>

#ifdef _MSC_VER

#include "Winsock.h"
#include "Iphlpapi.h"
#pragma comment( lib, "Ws2_32" )
#pragma comment( lib, "Iphlpapi" )

namespace Util
{
	inline std::string currentHostName()
	{
		std::string host = "";
		PMIB_IPADDRTABLE    pAddrTable;
		PMIB_IPADDRROW      pAddrRow;
		in_addr             ia;

		BYTE* m_pBuffer = NULL;
		ULONG m_ulSize = 0;
		DWORD m_dwResult;

		GetIpAddrTable((PMIB_IPADDRTABLE) m_pBuffer, &m_ulSize, TRUE);
		m_pBuffer = new BYTE[m_ulSize];

		if (NULL != m_pBuffer)
		{
			m_dwResult = GetIpAddrTable((PMIB_IPADDRTABLE) m_pBuffer, &m_ulSize, TRUE);
			if (m_dwResult == NO_ERROR)
			{
				pAddrTable = (PMIB_IPADDRTABLE) m_pBuffer;

				for (int x = 0; x < (int)pAddrTable->dwNumEntries; x++)
				{
					pAddrRow = (PMIB_IPADDRROW) &(pAddrTable->table[x]);
	                
					ia.S_un.S_addr = pAddrRow->dwAddr;
					if( ia.S_un.S_addr == 0x0100007f)
						continue;
					host = inet_ntoa(ia);
				}
			}
		}
		delete [] m_pBuffer;
		return host;
	}
}
#else

namespace Util
{
	inline std::string currentHostName()
	{
		fprintf(stderr, "currentHostName not implemented\n");
		return "unknownhost";		
	}
}

#endif

