#ifndef _ED_Util_ClassCaps_h_
#define _ED_Util_ClassCaps_h_


namespace Util {


/**
 * Tag base class. Prevents copying.
 */
class NoCopy {
private:
	NoCopy(const NoCopy&);
	void operator = (const NoCopy&);
protected:
	NoCopy() {}
};


/**
 * Tag base class. Prevents array allocation.
 */
class NoArray {
private:
	void* operator new[](size_t);
	void operator delete[](void*, size_t);
protected:
	NoArray() {}
};


/**
 * Tag base class. Prevents heap allocation.
 */
class NoHeap : NoArray {
private:
	void* operator new(size_t);
	void operator delete(void*, size_t);
protected:
	NoHeap() {}
};


}

#endif /* _ED_Util_ClassCaps_h_ */
