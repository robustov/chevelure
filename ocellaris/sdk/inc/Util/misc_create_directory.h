#pragma once
#include <direct.h>
#include <string>
#include <sys/stat.h>

// �������� ������ � ����������� ������!
namespace Util
{
	inline bool is_file_exist(const char* filename)
	{
		struct _stat buf;
		int result = _stat( filename, &buf );
		if( result!=0) return false;
		if( buf.st_mode & _S_IFREG)
			return true;
		return false;
	}
	inline bool is_dir_exist(const char* filename)
	{
		struct _stat buf;
		int result = _stat( filename, &buf );
		if( result!=0) return false;
		if( buf.st_mode & _S_IFDIR)
			return true;
		return false;
	}
	inline time_t getFileTime(const char* filename)
	{
		struct _stat buf;
		int result = _stat( filename, &buf );
		if( result!=0) return 0;
		return buf.st_mtime;
	}

	// add slash to directory
	inline void pushBackSlash(std::string& str)
	{
		char c = str[str.size()-1];
		if( c!='/' && c!='\\')
			str += '/';
	}
	// remove last slash
	inline void removeBackSlash(std::string& str)
	{
		char c = str[str.size()-1];
		if( c=='/' || c=='\\')
			str.erase(str.size()-1);
	}
	// ������� ������� � ��������� � ����� ������
	inline void remove_spaceinback( std::string& str)
	{
		for( ; !str.empty(); )
		{
			char c = str[str.size()-1];
			if( c!=' ' && c!='\t')
				break;
			str.erase(str.size()-1);
		}
	}
	// unix naming
	inline void changeSlashToSlash(std::string& str)
	{
		for(unsigned i=0; i<str.size(); i++)
			if(str[i]=='\\')
				str[i]='/';
	}
	inline void changeSlashToUnixSlash(std::string& str)
	{
		for(unsigned i=0; i<str.size(); i++)
			if(str[i]=='\\')
				str[i]='/';
	}
	// windows naming
	inline void changeSlashToWindowsSlash(std::string& str)
	{
		for(unsigned i=0; i<str.size(); i++)
			if(str[i]=='/')
				str[i]='\\';
	}
	// � tcl naming ��� //C/Pixar/prman/lib
	inline void changeSlashToTclSlash(std::string& str)
	{
		changeSlashToSlash(str);
		if(str[1]==':')
		{
			str.erase(1, 1);
			str.insert(0, "//");
		}
	}
	inline void duplicatePercent(std::string& str)
	{
		for(unsigned i=0; i<str.size(); i++)
			if(str[i]=='%')
				str.insert(i++, "%");
	}
	// �������� ���������� (��� �����) �� ����� 
	inline std::string extract_extention( const char* pathname)
	{
		std::string filename(pathname);
		int pos = (int)filename.rfind('.');
		if(pos==std::string::npos)
			return filename;

		return filename.substr(pos+1);
	}
	// �������� ��� ����� �� ����� ����
	inline std::string extract_filename( const char* pathname)
	{
		std::string filename(pathname);
		changeSlashToSlash(filename);
		int pos = (int)filename.rfind('/');
		if(pos==std::string::npos)
			return filename;

		return filename.substr(pos+1);
	}
	// �������� ��� ����� �� ����� ���� ��� ����������
	inline std::string extract_filename_noextention( const char* pathname)
	{
		std::string filename(pathname);
		changeSlashToSlash(filename);
		int pos = (int)filename.rfind('/');
		if(pos==std::string::npos)
		{
		}
		else
		{
			filename = filename.substr(pos+1);
		}

		pos = (int)filename.rfind('.');
		if(pos==std::string::npos)
			return filename;
		
		return filename.substr(0, pos);
	}
	// �������� ��� ���������� �� ����� ����
	inline std::string extract_foldername( const char* pathname)
	{
		std::string filename(pathname);
		changeSlashToSlash(filename);
		int pos = (int)filename.rfind('/');
		if(pos==std::string::npos)
			return "";

		return filename.substr(0, pos+1);
	}

	// Creates the directory path if it doesn't exist. Even creates all its
	// parent paths.
	inline void create_directory( const char* dirname)
	{
		std::string buf = dirname;
		char seps[] = "\\/";
		char *token = strtok( (char*)buf.c_str()+3, seps );
		while( token != NULL )
		{
			std::string path(dirname, token-buf.c_str());
			_mkdir(path.c_str());

			token = strtok( NULL, seps );
		}
		_mkdir(dirname);
	}
	// ������� ���������� ��� ����� filename
	inline void create_directory_for_file( const char* filename)
	{
		std::string buf = filename;
		char seps[] = "\\/";
		char *token = 0;
		if(buf[1]==':')
			token = strtok( (char*)buf.c_str()+3, seps );
		else if(buf[0]=='\\'&&buf[1]=='\\')
		{
			token = strtok( (char*)buf.c_str(), seps );
			if(token!=NULL)
				token = strtok( NULL, seps );
		}
		else
			token = strtok( (char*)buf.c_str(), seps );

		while( token != NULL )
		{
			std::string path(filename, token-buf.c_str());
			_mkdir(path.c_str());
			token = strtok( NULL, seps );
//			if( !token) break;
		}
//		_mkdir(dirname);
	}
	inline void correctFileName(char* c, bool bReplaceSpaces=false)
	{
		for( int i=0;*c; c++, i++)
		{
			if(i==1 && *c==':') continue;
			if( *c=='|' || 
				*c=='?' || 
				*c=='*' || 
				*c=='"' || 
				*c==':' || 
				*c=='<' || 
				*c=='>' //|| 
				)
				*c = '#';
			if( bReplaceSpaces)
			{
				if( *c==' ' || *c=='\t')
					*c = '_';
				if( *c==0xa || *c==0xd)
					*c = '_';
			}
		}
	}
	inline void correctFileName(std::string& str, char replsym='#', bool bReplaceSpaces=false)
	{
		for( int i=0;i<(int)str.size(); i++)
		{
			char* c = &str[i];
			if(i==1 && *c==':') continue;
			if( *c=='|' || 
				*c=='?' || 
				*c=='*' || 
				*c=='"' || 
				*c==':' || 
				*c=='<' || 
				*c=='>' //|| 
				)
				*c = replsym;
			if( bReplaceSpaces)
			{
				if( *c==' ' || *c=='\t')
					*c = '_';
				if( *c==0xa || *c==0xd)
					*c = '_';
			}
		}
	}

};
