#pragma once

//#include "STREAM.h"

#include <vector>
#include <map>
#include <list>
#include <set>
#include <queue>

#ifdef _MSC_VER
#define LINUXSTDDECL
#else
#define LINUXSTDDECL class
#endif

// сериализация STL
template <class STREAM, class T1, class T2>
STREAM& operator>>(STREAM& serializer, std::pair<T1, T2> &l)
{
	serializer >> l.first >> l.second;
	return serializer;
}

template <class STREAM, class T>
STREAM& serializeContainer(STREAM& serializer, T &l)
{
	int size = (int)l.size();
	serializer >> size;
	if (serializer.isLoading())
	{
		if (size > 0) 
		{
			l.resize(size);
		}
		else 
		{
			l = T();
		}
	}
	LINUXSTDDECL T::iterator _iterator;
	_iterator = l.begin();
	for (; _iterator != l.end(); _iterator++)
	{
		serializer >> (*_iterator);
	}
	return serializer;
}

template <class STREAM, class T, class A>
STREAM& operator >> (STREAM& serializer, std::list<T, A> &l)
{
    return serializeContainer(serializer, l);
}

template <class STREAM, class T, class A>
STREAM& operator >> (STREAM& serializer, std::vector<T, A> &v)
{
    return serializeContainer(serializer, v);
}

template <class STREAM, class T, class A>
STREAM& operator >> (STREAM& serializer, std::deque<T, A> &q)
{
    return serializeContainer(serializer, q);
}

template <class STREAM>
inline STREAM& operator >> (STREAM& serializer, std::string &s)
{
    return serializeContainer(serializer, s);
}

template <class STREAM, class T, class A> 
class QueueSerializer: public std::queue<T, A>
{
public:
    QueueSerializer(const std::queue<T, A> &q): std::queue<T, A>(q) {}
    void serialize(STREAM& serializer) { serializer >> this->c; }
};

template <class STREAM, class T, class A, class P> 
class PriorityQueueSerializer: public std::priority_queue<T, A, P>
{
public:
    PriorityQueueSerializer(const std::priority_queue<T, A, P> &q):std::priority_queue<T, A, P>(q) {}
    void serialize(STREAM& serializer) { serializer >> this->c; }
};

template <class STREAM, class T, class A>
STREAM& operator >> (STREAM& serializer, std::queue<T, A> &q)
{
    QueueSerializer<STREAM, T, A> qs(q);
    qs.serialize(serializer);
    if (serializer.isLoading())
    {
        q = qs;
    }
    return serializer;
}

template <class STREAM, class T, class A, class P>
STREAM& operator >> (STREAM& serializer, std::priority_queue<T, A, P> &q)
{
    PriorityQueueSerializer<STREAM, T, A, P> qs(q);
    qs.serialize(serializer);
    if (serializer.isLoading())
    {
        q = qs;
    }
    return serializer;
}

template <class STREAM, class T>
STREAM& serializeMap(STREAM& serializer, T &m)
{
    unsigned long size = (unsigned long)m.size();
    serializer >> size;
    if (serializer.isLoading())
    {
        unsigned long i = 0;
        for (; i < size; i++)
        {
            LINUXSTDDECL T::key_type k;
            LINUXSTDDECL T::referent_type t;
            serializer >> k >> t;
            m.insert(T::value_type(k, t));
        }
        return serializer;
    }
    LINUXSTDDECL T::iterator iterator = m.begin();
    for (; iterator != m.end(); iterator++)
    {
        serializer >> const_cast<LINUXSTDDECL T::key_type &>(iterator->first) >> iterator->second;
    }
    return serializer;
}

template<class STREAM, class K, class T, class Pr, class A>
STREAM& operator >> (STREAM& serializer, std::map<K, T, Pr, A> &m)
{
    return serializeMap(serializer, m);
}

template<class STREAM, class K, class T, class Pr, class A>
STREAM& operator >> (STREAM& serializer, std::multimap<K, T, Pr, A> &m)
{
    return serializeMap(serializer, m);
}

template <class STREAM, class T>
STREAM& serializeSet(STREAM& serializer, T &m)
{
    unsigned long size = (unsigned long)m.size();
    serializer >> size;
    if (serializer.isLoading())
    {
        unsigned long i = 0;
        for (; i < size; i++)
        {
            LINUXSTDDECL T::key_type   v;
            serializer >> v;
            m.insert(v);
        }
        return serializer;
    }
    LINUXSTDDECL T::iterator iterator = m.begin();
    for (; iterator != m.end(); iterator++)
    {
        // TODO: FIXME: without const_cast not compiling under vc10
        serializer >> const_cast<T::reference>(*iterator);
    }
    return serializer;
}

template<class STREAM, class T, class Pr, class A>
STREAM& operator >> (STREAM& serializer, std::set<T, Pr, A> &m)
{
    return serializeSet(serializer, m);
}

template<class STREAM, class T, class Pr, class A>
STREAM& operator >> (STREAM& serializer, std::multiset<T, Pr, A> &m)
{
    return serializeSet(serializer, m);
}
