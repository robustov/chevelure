#ifndef _W32_MMF_H__
#define _W32_MMF_H__

#include "../Util/error.h"
#include <windows.h>
#include "../Util/misc_ptr.h"

namespace misc
{
	// ����� ��� �������� MMF �����
	class mmf
	{
		// �������� ����
		HANDLE hFile;
		HANDLE hMMF;
		char* map;
		unsigned long filesize;
		bool bReadOnly;
		Util::Error errormsg;
	public:
		bool operator!() const
		{
			return (hFile==0);
		}
		operator char*()
		{
//			if( bReadOnly) return NULL;
			return map;
		}
		operator const char*() const
		{
			return map;
		}
		
		long Length()
		{
			return filesize;
		}
		char operator[](int i)
		{
			if(i<0 || (unsigned long)i>=filesize) return 0;
			return map[i];
		}

		template<class T> T* get(unsigned long offset, T*& dest, const char* errortag=NULL)
		{
			if(offset > (filesize-sizeof(T))) 
				if(!errortag) throw Util::Error("try outside file access");
					else throw Util::Error( "try outside file access on reading %s", errortag);
			return (dest = (T*)(map+offset));
		}
		template<class T> T* getup(unsigned long& offset, T*& dest, const char* errortag=NULL)
		{
			unsigned long res = filesize-sizeof(T);
			if( offset > res) 
				if(!errortag) throw Util::Error("try outside file access");
					else throw Util::Error( "try outside file access on reading %s", errortag);
			unsigned long oldoffset = offset; offset+=sizeof(T);
			return (dest = (T*)(map+oldoffset));
		}
		const char* getline(unsigned long& offset, int& len)
		{
			len = 0;
			if( offset>=filesize) return NULL;
			char* text = map + offset;
			for( ; offset+len<filesize; len++)
				if(text[len]==0xd || text[len]==0xa) break;
			offset+=len;
			for( ; offset<filesize; offset++)
				if(map[offset]!=0xd && map[offset]!=0xa) break;
			return text;
		}
		mmf(LPCTSTR filename=NULL, bool bReadOnly=true)
		{
			map = NULL;
			hMMF = NULL;
			hFile = NULL;
			filesize = NULL;
			if( !filename) return;
			open(filename, bReadOnly);
		}
		~mmf()
		{
			close();
		}
		const char* error()
		{
			return errormsg;
		}
	public:
		bool open(LPCTSTR filename, bool bReadOnly=true)
		{
			close();
			if( !filename) return false;
			this->bReadOnly = bReadOnly;
			try
			{
				// ������� ����
				hFile = CreateFile(filename, GENERIC_READ|(bReadOnly?0:GENERIC_WRITE), FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL|FILE_FLAG_RANDOM_ACCESS, NULL);
				if( hFile==INVALID_HANDLE_VALUE) throw Util::Error( "CreateFile() %d", GetLastError());

				filesize = GetFileSize(hFile, NULL);

				// ������� MMF
				hMMF = CreateFileMapping(hFile, NULL, bReadOnly?PAGE_READONLY:PAGE_READWRITE, 0, filesize, NULL);
				if(!hMMF) throw Util::Error( "CreateFileMapping() %d", GetLastError());

				// ������������ ����
				map = (char*)MapViewOfFileEx(hMMF, bReadOnly?FILE_MAP_READ:FILE_MAP_ALL_ACCESS, 0, 0, filesize, 0);
				if(!map) throw Util::Error( "MapViewOfFileEx() %d", GetLastError());
			}
			catch(Util::Error& err)
			{
				errormsg = err;
				close();
				return false;
			}
			catch(...)
			{
				// ������� MMF
				errormsg = "Unhandled error";
				close();
				return false;
			}
			return true;
		}
		// �������� ����� � ����� ������� FILE_MAP_COPY ��� ���������� ������ �������
		// �������� = 64��
		bool open_copyaccess(LPCTSTR filename, int pagecount=1)
		{
			close();
			if( !filename) return false;
			this->bReadOnly = bReadOnly;
			try
			{
				// ������� ����
				hFile = CreateFile(filename, GENERIC_READ|(bReadOnly?0:GENERIC_WRITE),
					FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL|FILE_FLAG_RANDOM_ACCESS, NULL);
				if( hFile==INVALID_HANDLE_VALUE) throw Util::Error( "CreateFile() %d", GetLastError());
				
				filesize = GetFileSize(hFile, NULL);
				
				OSVERSIONINFO osversion;
				osversion.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
				GetVersionEx(&osversion);
				if( osversion.dwPlatformId == VER_PLATFORM_WIN32_NT)
				{
					// ������� MMF
					hMMF = CreateFileMapping(hFile, NULL, bReadOnly?PAGE_READONLY:PAGE_READWRITE, 0, filesize,
						NULL); 
					if(!hMMF) throw Util::Error( "CreateFileMapping() %d", GetLastError());

					LPVOID mem = VirtualAlloc( NULL, filesize, MEM_RESERVE, PAGE_READWRITE);
					if(!mem) throw Util::Error( "VirtualAlloc() %d", GetLastError());
					BOOL res = VirtualFree(mem, 0, MEM_RELEASE);
					// ������������ ����
					SYSTEM_INFO SystemInfo;
					GetSystemInfo(&SystemInfo);
					unsigned long add = SystemInfo.dwAllocationGranularity*pagecount;
					if( add>filesize) add=filesize;
					if( add)
					{
						map = (char*)MapViewOfFileEx(hMMF, bReadOnly?FILE_MAP_COPY:FILE_MAP_ALL_ACCESS, 0,
							0, add, mem);
						if(!map) throw Util::Error( "MapViewOfFileEx() %d", GetLastError());
					}
					int rest = filesize-add;
					if(rest>0)
					{
						char* map2 = (char*)MapViewOfFileEx(hMMF,
							bReadOnly?FILE_MAP_READ:FILE_MAP_ALL_ACCESS, 0, add, rest, (char*)mem+add);
						if(!map2) throw Util::Error( "MapViewOfFileEx() %d", GetLastError());
						if(!add)
							map = map2;
					}
				}
				else
				{
					// ������� MMF
					hMMF = CreateFileMapping(hFile, NULL, bReadOnly?PAGE_WRITECOPY:PAGE_READWRITE, 0, filesize,
						NULL); 
					if(!hMMF) throw Util::Error( "CreateFileMapping() %d", GetLastError());
					// ������������ ����
					map = (char*)MapViewOfFileEx(hMMF, bReadOnly?FILE_MAP_COPY:FILE_MAP_ALL_ACCESS, 0, 0, filesize, 0);
					if(!map) throw Util::Error( "MapViewOfFileEx() %d", GetLastError());
				}
				return true;
			}
			catch(Util::Error& err)
			{
				errormsg = err;
				close();
				return false;
			}
			catch(...)
			{
				// ������� MMF
				errormsg = "Unhandled error";
				close();
				return false;
			}
			return true;
		}

		// �������
		void close()
		{
			// ������� MMF
			if( map) UnmapViewOfFile(map);
			if( hMMF) CloseHandle( hMMF);
			if( hFile) CloseHandle( hFile);
			map = NULL;
			hMMF = NULL;
			hFile = NULL;
			filesize = NULL;
		}
	};
}

#endif
