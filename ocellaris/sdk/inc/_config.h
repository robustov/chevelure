#ifndef _ED_Core_config_h_
#define _ED_Core_config_h_


#ifndef ED_CORE_EXPORT
#if defined(_DEBUG) && 0
	#define ED_CORE_EXPORT __declspec(dllexport)
//	#define ED_CORE_EXPORT 
#else
	#define ED_CORE_EXPORT
#endif

#endif


#ifndef ED_CORE_IMPORT
#if defined(_DEBUG) && 0
	#define ED_CORE_IMPORT __declspec(dllimport)
//	#define ED_CORE_IMPORT
#else
	#define ED_CORE_IMPORT 
#endif
#endif


#ifdef ED_CORE_EXTERN
#	undef ED_CORE_EXTERN
#endif

#ifdef UL_MATH_EXPORTS
#	define ED_CORE_EXTERN ED_CORE_EXPORT
#else
#	define ED_CORE_EXTERN ED_CORE_IMPORT
#endif


#ifndef ED_CORE_API
#	ifdef _MSC_VER
#		if _MSC_VER >= 1400
#			define ED_CORE_API
#		else
#			define ED_CORE_API __stdcall
#		endif
#	else	// LINUX
#		define ED_CORE_API
#	endif
#endif


#ifdef _MSC_VER
#pragma warning (disable : 4786 4290)
#endif


#endif /* _ED_Core_config_h_ */
