#pragma once

#include "Math/math.h"
#include "Math/box.h"

inline void glVertex3f(const Math::Vec3f& v)
{
	glVertex3f(v.x, v.y, v.z);
}

inline void glNormal3f(const Math::Vec3f& v)
{
	glNormal3f(v.x, v.y, v.z);
}

inline void glColor3f(const Math::Vec3f& v)
{
	glColor3f(v.x, v.y, v.z);
}
inline void glColor4f(const Math::Vec4f& v)
{
	glColor4f(v.x, v.y, v.z, v.w);
}


//! plane: 0 - X, 1 - Y, 2 - Z
inline void drawQuad(const Math::Box2f& quad, float z, int plane)
{
	glBegin( GL_LINE_STRIP );
	glVertex3f( quad.min.x, z, quad.min.y);
	glVertex3f( quad.min.x, z, quad.max.y);
	glVertex3f( quad.max.x, z, quad.max.y);
	glVertex3f( quad.max.x, z, quad.min.y);
	glVertex3f( quad.min.x, z, quad.min.y);
	glEnd();
}
inline void drawLine(Math::Vec3f* v, int count)
{
	glBegin( GL_LINE_STRIP );
	for(int i=0; i<count; i++)
		glVertex3f( v[i]);
	glEnd();
}
inline void drawPoints(Math::Vec3f* v, int count, float pointsize)
{
	float lastPointSize;
	glGetFloatv( GL_POINT_SIZE, &lastPointSize );
	glPointSize( pointsize );

	glBegin( GL_POINTS );
	for(int i=0; i<count; i++)
		glVertex3f( v[i]);
	glEnd();

	glPointSize( lastPointSize );
}
inline void drawLine(const Math::Vec3f& v1, const Math::Vec3f& v2)
{
	glBegin( GL_LINES );
	glVertex3f( v1);
	glVertex3f( v2);
	glEnd();
}

inline void drawBox(const Math::Box3f& box)
{
	glBegin( GL_LINE_STRIP );
	glVertex3f( box.corner(0));
	glVertex3f( box.corner(1));
	glVertex3f( box.corner(3));
	glVertex3f( box.corner(2));
	glVertex3f( box.corner(0));
	glEnd();

	glBegin( GL_LINE_STRIP );
	glVertex3f( box.corner(4));
	glVertex3f( box.corner(5));
	glVertex3f( box.corner(7));
	glVertex3f( box.corner(6));
	glVertex3f( box.corner(4));
	glEnd();

	glBegin( GL_LINES );
	glVertex3f( box.corner(0));
	glVertex3f( box.corner(0+4));
	glVertex3f( box.corner(1));
	glVertex3f( box.corner(1+4));
	glVertex3f( box.corner(2));
	glVertex3f( box.corner(2+4));
	glVertex3f( box.corner(3));
	glVertex3f( box.corner(3+4));
	glEnd();
}

inline void drawBox(const Math::Box3f& box, const Math::Vec3f& offset, int mask=0xFF)
{
	if( mask & 0x1)
	{
		glBegin( GL_LINE_STRIP );
		glVertex3f( box.corner(0)+offset);
		glVertex3f( box.corner(2)+offset);
		glVertex3f( box.corner(6)+offset);
		glVertex3f( box.corner(4)+offset);
		glVertex3f( box.corner(0)+offset);
		glEnd();
	}
	if( mask & 0x2)
	{
		glBegin( GL_LINE_STRIP );
		glVertex3f( box.corner(0+1)+offset);
		glVertex3f( box.corner(2+1)+offset);
		glVertex3f( box.corner(6+1)+offset);
		glVertex3f( box.corner(4+1)+offset);
		glVertex3f( box.corner(0+1)+offset);
		glEnd();
	}

	if( mask & 0x10)
	{
		glBegin( GL_LINE_STRIP );
		glVertex3f( box.corner(0)+offset);
		glVertex3f( box.corner(1)+offset);
		glVertex3f( box.corner(3)+offset);
		glVertex3f( box.corner(2)+offset);
		glVertex3f( box.corner(0)+offset);
		glEnd();
	}

	if( mask & 0x20)
	{
		glBegin( GL_LINE_STRIP );
		glVertex3f( box.corner(4)+offset);
		glVertex3f( box.corner(5)+offset);
		glVertex3f( box.corner(7)+offset);
		glVertex3f( box.corner(6)+offset);
		glVertex3f( box.corner(4)+offset);
		glEnd();
	}
	if( mask & 0x4)
	{
		glBegin( GL_LINE_STRIP );
		glVertex3f( box.corner(0)+offset);
		glVertex3f( box.corner(0+4)+offset);
		glVertex3f( box.corner(1+4)+offset);
		glVertex3f( box.corner(1)+offset);
		glVertex3f( box.corner(0)+offset);
		glEnd();
	}
	if( mask & 0x8)
	{
		glBegin( GL_LINE_STRIP );
		glVertex3f( box.corner(2)+offset);
		glVertex3f( box.corner(2+4)+offset);
		glVertex3f( box.corner(3+4)+offset);
		glVertex3f( box.corner(3)+offset);
		glVertex3f( box.corner(2)+offset);
		glEnd();
	}

	if(false)
	{
		glBegin( GL_LINES );
		glVertex3f( box.corner(0)+offset);
		glVertex3f( box.corner(0+4)+offset);
		glVertex3f( box.corner(1)+offset);
		glVertex3f( box.corner(1+4)+offset);
		glVertex3f( box.corner(2)+offset);
		glVertex3f( box.corner(2+4)+offset);
		glVertex3f( box.corner(3)+offset);
		glVertex3f( box.corner(3+4)+offset);
		glEnd();
	}
}

// � ��������� XY
inline void drawBoxShaded(const Math::Box3f& box)
{
	GLfloat CubeNormals[6][3] = 
	{  
		{-1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {1.0, 0.0, 0.0},
		{0.0, -1.0, 0.0}, {0.0, 0.0, 1.0}, {0.0, 0.0, -1.0} 
	};
	GLint CubeFaces[6][4] = 
	{  
		{0, 3, 2, 1}, {3, 7, 6, 2}, {7, 4, 5, 6},
		{4, 0, 1, 5}, {5, 6, 2, 1}, {7, 4, 0, 3} 
	};
	GLfloat v[8][3];

	/* Setup cube vertex data. */
	v[0][0] = v[1][0] = v[2][0] = v[3][0] = box.min.x;
	v[4][0] = v[5][0] = v[6][0] = v[7][0] = box.max.x;
	v[0][1] = v[1][1] = v[4][1] = v[5][1] = box.min.y;
	v[2][1] = v[3][1] = v[6][1] = v[7][1] = box.max.y;
	v[0][2] = v[3][2] = v[4][2] = v[7][2] = box.max.z;
	v[1][2] = v[2][2] = v[5][2] = v[6][2] = box.min.z;

	for(int i = 0; i < 6; i++) 
	{
		glBegin(GL_QUADS);

		glNormal3fv(&CubeNormals[i][0]);
//		cgGLSetParameter3f(TestColorParam, 1.0, 0.0, 0.0);
		glVertex3fv(&v[CubeFaces[i][0]][0]);

//		cgGLSetParameter3f(TestColorParam, 0.0, 1.0, 0.0);
		glVertex3fv(&v[CubeFaces[i][1]][0]);

//		cgGLSetParameter3f(TestColorParam, 0.0, 0.0, 1.0);
		glVertex3fv(&v[CubeFaces[i][2]][0]);

//		cgGLSetParameter3f(TestColorParam, 1.0, 1.0, 1.0);
		glVertex3fv(&v[CubeFaces[i][3]][0]);

		glEnd();
	}
}
// � ��������� XY
inline void drawCircleXY(float radius=1, Math::Vec3f center = Math::Vec3f(0, 0, 0))
{
	int XXX = 30;
	const float x[] = {0.000000f, 0.103956f, 0.203368f, 0.293893f, 0.371572f, 0.433013f, 0.475528f, 0.497261f, 0.497261f, 0.475528f, 0.433013f, 0.371572f, 0.293893f, 0.203368f, 0.103956f, -0.000000f, -0.103956f, -0.203368f, -0.293893f, -0.371572f, -0.433013f, -0.475528f, -0.497261f, -0.497261f, -0.475528f, -0.433013f, -0.371572f, -0.293893f, -0.203368f, -0.103956f, 0.000000f};
	const float y[] = {0.500000f, 0.489074f, 0.456773f, 0.404509f, 0.334565f, 0.250000f, 0.154508f, 0.052264f, -0.052264f, -0.154509f, -0.250000f, -0.334565f, -0.404509f, -0.456773f, -0.489074f, -0.500000f, -0.489074f, -0.456773f, -0.404508f, -0.334565f, -0.250000f, -0.154509f, -0.052264f, 0.052264f, 0.154509f, 0.250000f, 0.334565f, 0.404508f, 0.456773f, 0.489074f, 0.500000f};

	glBegin( GL_LINE_STRIP );
	for(int i=0; i<=XXX; i++)
		glVertex3f( center+Math::Vec3f(x[i], y[i], 0)*radius);
	glEnd();
}
// � ��������� XY
inline void drawCircleXZ(float radius=1, Math::Vec3f center = Math::Vec3f(0, 0, 0))
{
	int XXX = 30;
	const float x[] = {0.000000f, 0.103956f, 0.203368f, 0.293893f, 0.371572f, 0.433013f, 0.475528f, 0.497261f, 0.497261f, 0.475528f, 0.433013f, 0.371572f, 0.293893f, 0.203368f, 0.103956f, -0.000000f, -0.103956f, -0.203368f, -0.293893f, -0.371572f, -0.433013f, -0.475528f, -0.497261f, -0.497261f, -0.475528f, -0.433013f, -0.371572f, -0.293893f, -0.203368f, -0.103956f, 0.000000f};
	const float y[] = {0.500000f, 0.489074f, 0.456773f, 0.404509f, 0.334565f, 0.250000f, 0.154508f, 0.052264f, -0.052264f, -0.154509f, -0.250000f, -0.334565f, -0.404509f, -0.456773f, -0.489074f, -0.500000f, -0.489074f, -0.456773f, -0.404508f, -0.334565f, -0.250000f, -0.154509f, -0.052264f, 0.052264f, 0.154509f, 0.250000f, 0.334565f, 0.404508f, 0.456773f, 0.489074f, 0.500000f};

	glBegin( GL_LINE_STRIP );
	for(int i=0; i<=XXX; i++)
		glVertex3f( center+Math::Vec3f(x[i], 0, y[i])*radius);
	glEnd();
}
// � ��������� YZ
inline void drawCircleYZ(float radius=1, Math::Vec3f center = Math::Vec3f(0, 0, 0))
{
	int XXX = 30;
	const float x[] = {0.000000f, 0.103956f, 0.203368f, 0.293893f, 0.371572f, 0.433013f, 0.475528f, 0.497261f, 0.497261f, 0.475528f, 0.433013f, 0.371572f, 0.293893f, 0.203368f, 0.103956f, -0.000000f, -0.103956f, -0.203368f, -0.293893f, -0.371572f, -0.433013f, -0.475528f, -0.497261f, -0.497261f, -0.475528f, -0.433013f, -0.371572f, -0.293893f, -0.203368f, -0.103956f, 0.000000f};
	const float y[] = {0.500000f, 0.489074f, 0.456773f, 0.404509f, 0.334565f, 0.250000f, 0.154508f, 0.052264f, -0.052264f, -0.154509f, -0.250000f, -0.334565f, -0.404509f, -0.456773f, -0.489074f, -0.500000f, -0.489074f, -0.456773f, -0.404508f, -0.334565f, -0.250000f, -0.154509f, -0.052264f, 0.052264f, 0.154509f, 0.250000f, 0.334565f, 0.404508f, 0.456773f, 0.489074f, 0.500000f};

	glBegin( GL_LINE_STRIP );
	for(int i=0; i<=XXX; i++)
		glVertex3f( center+Math::Vec3f(0, x[i], y[i])*radius);
	glEnd();
}
inline void drawSphere(float radius=1, Math::Vec3f center = Math::Vec3f(0, 0, 0))
{
	drawCircleXY(radius, center);
	drawCircleYZ(radius, center);
	drawCircleXZ(radius, center);
}
