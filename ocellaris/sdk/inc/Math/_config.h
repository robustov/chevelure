#ifndef _ED_Math_config_h_
#define _ED_Math_config_h_


#ifdef ED_MATH_INTERNALS
#define ED_CORE_INTERNALS
#endif


#include "../_config.h"


#ifndef ED_MATH_EXTERN
#define ED_MATH_EXTERN ED_CORE_EXTERN
#endif


#ifndef ED_MATH_API
#define ED_MATH_API ED_CORE_API
#endif


/**
 * @brief 2D & 3D mathematics.
 */
namespace Math {}

#include <algorithm>

#ifndef _MSC_VER
template <class T>
const T& __min(const T& a, const T& b)
{
	return std::min(a, b);
}
template <class T>
const T& __max(const T& a, const T& b)
{
	return std::max(a, b);
}
#endif


#endif /* _ED_Math_config_h_ */
