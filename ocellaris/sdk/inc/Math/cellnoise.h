#ifndef CELLNOISE
#define CELLNOISE

#include "Math\Math.h"

namespace Math
{
//#include "ri.h"
ED_MATH_EXTERN float ED_MATH_API cellnoise( float x);
ED_MATH_EXTERN float ED_MATH_API cellnoise( int narg, float v[]);
//void cellnoise( float x, RtPoint res);
ED_MATH_EXTERN void ED_MATH_API cellnoise(float v[3], float res[3]);

template<class T> inline
Vector<3,T> cellnoise(const Vector<3,T>& in)
{
	float v[3] = {(float)in[0], (float)in[1], (float)in[2]};
	float res[3];
	cellnoise(v, res);

	return Vector<3,T>(res[0]+(int)in[0], res[1]+(int)in[1], res[2]+(int)in[2]);
}
}
#endif
