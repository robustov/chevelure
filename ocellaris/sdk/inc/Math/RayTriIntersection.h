//#include "RayTriIntersection.h"
#pragma once

#include "Math/Vector.h"

namespace Math
{
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	*	Computes a ray-triangle intersection test.
	*	Original code from Tomas Moller's "Fast Minimum Storage Ray-Triangle Intersection".
	*	It's been optimized a bit with integer code, and modified to return a non-intersection if distance from
	*	ray origin to triangle is negative.
	*
	*	\param		vert0	[in] triangle vertex
	*	\param		vert1	[in] triangle vertex
	*	\param		vert2	[in] triangle vertex
	*	\return		true on overlap. mStabbedFace is filled with relevant info.
	*/
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	template< bool mCulling>
	inline bool RayTriIntersection(
		const Math::Vec3f& vert0, 
		const Math::Vec3f& vert1, 
		const Math::Vec3f& vert2, 
		const Math::Vec3f& mOrigin,
		const Math::Vec3f& mDir, 
		Math::Vec3f& bary, 
		float& mDistance
		)
	{
		float mU, mV;
		bool res = Math::RayTriIntersection<mCulling>(
			vert0, vert1, vert2, mOrigin, mDir, mU, mV, mDistance);
		if(!res) return false;
		bary = Math::Vec3f(1-mU-mV, mU, mV);
		return true;
	}

	template< bool mCulling>
	inline bool RayTriIntersection(
		const Math::Vec3f& vert0, 
		const Math::Vec3f& vert1, 
		const Math::Vec3f& vert2, 
		const Math::Vec3f& mOrigin,
		const Math::Vec3f& mDir, 
		float& mU, 
		float& mV, 
		float& mDistance
		)
	{
		const float LOCAL_EPSILON = 0.000001f;

//		bool mCulling = false;
		// Stats
	//	mNbRayPrimTests++;

		// Find vectors for two edges sharing vert0
		Math::Vec3f edge1 = vert1 - vert0;
		Math::Vec3f edge2 = vert2 - vert0;

		// Begin calculating determinant - also used to calculate U parameter
		Math::Vec3f pvec = Math::cross(mDir, edge2);//mDir^edge2;

		// If determinant is near zero, ray lies in plane of triangle
		float det = Math::dot(edge1, pvec); //edge1|pvec;

		if(mCulling)
		{
			if(det<LOCAL_EPSILON) 
				return false;

			// From here, det is > 0. So we can use integer cmp.

			// Calculate distance from vert0 to ray origin
			Math::Vec3f tvec = mOrigin - vert0;

			// Calculate U parameter and test bounds
			mU = Math::dot(tvec, pvec);
	//		if(IS_NEGATIVE_FLOAT(mStabbedFace.mU) || IR(mStabbedFace.mU)>IR(det))		return FALSE;
			if( mU<0 || mU>det) return false;

			// Prepare to test V parameter
			Math::Vec3f qvec = Math::cross(tvec, edge1);

			// Calculate V parameter and test bounds
			mV = Math::dot(mDir, qvec);
	//		if(IS_NEGATIVE_FLOAT(mStabbedFace.mV) || mStabbedFace.mU+mStabbedFace.mV>det)	return false;
			if( mV<0 || mU+mV>det) return false;

			// Calculate t, scale parameters, ray intersects triangle
			mDistance = Math::dot(edge2, qvec);
			// Det > 0 so we can early exit here
			// Intersection point is valid if distance is positive (else it can just be a face behind the orig point)
			if(mDistance<0) return false;
			// Else go on
			float OneOverDet = 1.0f / det;
			mDistance *= OneOverDet;
			mU *= OneOverDet;
			mV *= OneOverDet;
		}
		else
		{
			// the non-culling branch
			if(det>-LOCAL_EPSILON && det<LOCAL_EPSILON)
				return false;
			float OneOverDet = 1.0f / det;

			// Calculate distance from vert0 to ray origin
			Math::Vec3f tvec = mOrigin - vert0;

			// Calculate U parameter and test bounds
			mU = (Math::dot(tvec,pvec)) * OneOverDet;
	//		if(IS_NEGATIVE_FLOAT(mStabbedFace.mU) || IR(mStabbedFace.mU)>IEEE_1_0)		return false;
			if(mU<0 || mU>1.0) return false;

			// prepare to test V parameter
			Math::Vec3f qvec = Math::cross(tvec,edge1);

			// Calculate V parameter and test bounds
			mV = Math::dot(mDir,qvec) * OneOverDet;
			if(mV<0 || mU+mV>1.0f) return false;

			// Calculate t, ray intersects triangle
			mDistance = Math::dot(edge2,qvec) * OneOverDet;
			// Intersection point is valid if distance is positive (else it can just be a face behind the orig point)
			if(mDistance<0) return false;
		}
		return true;
	}

	// ����������� ���� � ���������� ��-��
	inline bool RayTriPlaneIntersection(
		const Math::Vec3f& vert0, 
		const Math::Vec3f& vert1, 
		const Math::Vec3f& vert2, 
		const Math::Vec3f& mOrigin,
		const Math::Vec3f& mDir, 
		Math::Vec3f& inter)
	{
		const float LOCAL_EPSILON = 0.000001f;

		// Find vectors for two edges sharing vert0
		Math::Vec3f edge1 = vert1 - vert0;
		Math::Vec3f edge2 = vert2 - vert0;
		Math::Vec3f N = Math::cross( edge1, edge2);

		float s1 = Math::dot( N, mDir);
		if(s1<LOCAL_EPSILON && s1>LOCAL_EPSILON)
			return false;

		float s2 = Math::dot( N, vert0-mOrigin);
		float t = s2/s1;
		inter = mOrigin + mDir*t;
		return true;
	}
}
