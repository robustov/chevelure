#ifndef _ED_Math__h_
#define _ED_Math__h_
#pragma once

// ��� autoext.dat
/*/
Math::Vector<2,float>=<x,g>, <y,g>
Math::Vector<3,float>=<x,g>, <y,g>, <z,g>
Math::Vector<4,float>=<x,g>, <y,g>, <z,g>, <w,g>
Math::Vector<2,int>=<x>, <y>
Math::Vector<3,int>=<x>, <y>, <z>
Math::Vector<4,int>=<x>, <y>, <z>, <w>

Math::Box<2,float>=min=<min,t>, max=<max,t>
Math::Box<3,float>=min=<min,t>, max=<max,t>

Math::Matrix<2,float>=<m[0],t>, <m[1],t>
Math::Matrix<3,float>=<m[0],t>, <m[1],t>, <m[2],t>
Math::Matrix<4,float>=<m[0],t>, <m[1],t>, <m[2],t>, <m[3],t>
/*/

#include "Math/Vector.h"
#include "Math/Matrix.h"
//#include "Math\Rotation3.h"
//#include "Math\Position.h"
//#include "Math\Plane.h"
#include "Math/Face.h"
#include "Math/Box.h"

#endif /* _ED_Math__h_ */
