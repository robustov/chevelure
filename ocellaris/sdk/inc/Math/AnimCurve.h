#pragma once

#ifdef MAYA_API_VERSION
	#include <maya/MFnAnimCurve.h>
	#include <maya/MIntArray.h>
	#include <maya/MFloatArray.h>
#endif
#include "Math/Math.h"
#include <vector>

namespace Math
{
	template<class T>
	class AnimCurve
	{
	public:
		bool bTime;				// ��� ��������� �� ����� ��� �� DK
		T minTime, maxTime;
		std::vector<T> data;
	public:
		AnimCurve()
		{
			minTime = maxTime = 0;
		}
		void clear()
		{
			bTime = false;
			minTime = maxTime = 0;
			data.clear();
//			data.push_back(defval);
		}
		bool isValid()
		{
			return !data.empty();
		}
		void set(T value)
		{
			clear();
			data.push_back(value);
		}
		#ifdef MAYA_API_VERSION
		bool set( MFnAnimCurve& src, int res)
		{
			if( src.isUnitlessInput())
			{
				int c = src.numKeys();
				for(int i=0; i<c; i++)
				{
					T key = (T)src.unitlessInput(i);
					if(i==0)
						minTime = maxTime = key;
					minTime = __min(minTime, key);
					maxTime = __max(maxTime, key);
				}
				if(res<2) res=2;
				data.resize(res);
				for(i=0; i<res; i++)
				{
					T factor = i/(res-1.f);
					double t = minTime*(1-factor) + maxTime*factor;
					double v;
					src.evaluate(t, v);
					data[i] = (T)v;
				}
			}
			else
			{
				MTime _minTime, _maxTime;
				int c = src.numKeys();
				for(int i=0; i<c; i++)
				{
					MTime t = src.time(i);
					T key = ((T)t.as(MTime::uiUnit()));
					if(i==0)
					{
						minTime = maxTime = key;
						_minTime = _maxTime = t;
					}
					if(t<_minTime) _minTime=t;
					if(t>_maxTime) _maxTime=t;
					minTime = __min(minTime, key);
					maxTime = __max(maxTime, key);
				}
				if(res<2) res=2;
				data.resize(res);
				for(i=0; i<res; i++)
				{
					T factor = i/(res-1.f);
					MTime t = _minTime*(1-factor) + _maxTime*factor;
					double v;
					src.evaluate(t, v);
					data[i] = (T)v;
				}
			}
			return true;
		}
		#endif

		#ifdef OCELLARIS
		void AnimCurve2Ocs(const char* _name, cls::IRender* render)
		{
			std::string name = _name;

			render->Parameter( (name+"::bTime").c_str(), bTime);
			render->Parameter( (name+"::minKey").c_str(), minTime);
			render->Parameter( (name+"::maxKey").c_str(), maxTime);
			cls::PA<float> P(cls::PI_CONSTANT, data);
			render->Parameter( (name+"::data").c_str(), P);
			
		}
		void Ocs2AnimCurve(const char* _name, cls::IRender* render)
		{
			std::string name = _name;

			render->GetParameter( (name+"::bTime").c_str(), bTime);
			render->GetParameter( (name+"::minKey").c_str(), minTime);
			render->GetParameter( (name+"::maxKey").c_str(), maxTime);
			cls::PA<float> P = render->GetParameter( (name+"::data").c_str());
			P.data(data);
		}
		#endif

		inline T getValue(T pos) const
		{
			if(data.empty()) return 0;
			if(data.size()==1) return data.front();
			if(pos<=minTime) return data.front();
			if(pos>=maxTime) return data.back();

			T x = (pos-minTime)/(maxTime-minTime);
			x = x*(data.size()-1);
			int ind = (int)floor(x);
			T factor = x-ind;

			T val = data[ind]*(1-factor) + data[ind]*factor;
			return val;
		}
	};

};

#define SERIALIZEOCSANIM(ARG1)\
if( bSave) \
{\
	this->##ARG1.AnimCurve2Ocs( (name+"::"#ARG1).c_str(), render);\
} \
else \
{\
	this->##ARG1.Ocs2AnimCurve( (name+"::"#ARG1).c_str(), render);\
}

template<class T, class Stream> inline
Stream& operator >> (Stream& out, Math::AnimCurve<T>& v)
{
	unsigned char version = 1;
	out >> version;
	if( version>=0)
	{
		out >> v.minTime >> v.maxTime >> v.data;
	}
	if( version>=1)
	{
		out >> v.bTime;
	}
	return out;
}
