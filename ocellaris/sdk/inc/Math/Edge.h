#pragma once

#include <list>
#include <vector>
#include <set>

#include "Math/Polygon.h"
#include "Math/Face.h"

namespace Math
{
	////////////////////////////////////////////
	// �����
	struct Edge32
	{
//		typedef unsigned long NOFACE;
		unsigned long v1, v2;
//		unsigned long face1, face2;
//		unsigned char face1edge, face2edge;
		Edge32()
		{
			this->v1 = 0xFFFFFFFF;
			this->v2 = 0xFFFFFFFF;
//			face1 = face2 = 0xFFFFFFFF;
//			face1edge = face2edge = 0xFF;
		}
		Edge32(unsigned long v1, unsigned long v2)
		{
			if(v1>v2) std::swap(v1, v2);
			this->v1 = v1;
			this->v2 = v2;
//			face1 = face2 = 0xFFFFFFFF;
//			face1edge = face2edge = 0xFF;
		}
		bool isValid() const
		{
			if( this->v1 != 0xFFFFFFFF && this->v2 != 0xFFFFFFFF)
				return true;
			return false;
		}
		/*/
		void addface(unsigned long face, unsigned char edge)
		{
			if( face1 == 0xFFFFFFFF)
			{
				face1 = face, face1edge = edge;
			}
			else
			{
				face2 = face, face2edge = edge;
			}
		}
		/*/
		bool operator <(const Edge32& edge) const
		{
			if(v1<edge.v1) return true;
			if(v1>edge.v1) return false;
			return v2<edge.v2;
		}
		bool operator ==(const Edge32& edge) const
		{
			if(v1!=edge.v1) return false;
			return v2==edge.v2;
		}
		int opposite(int v) const
		{
			if( v==v1) return v2;
			if( v==v2) return v1;
			return -1;
		}
		/*/
		int adjoiningFace(int toFace)
		{
			if(face1!=toFace)
				return face1;
			return face2;
		}
		/*/
	};
	struct EdgeBind32
	{
		unsigned long face1, face2;
		unsigned char face1edge, face2edge;
		EdgeBind32()
		{
			face1 = face2 = 0xFFFFFFFF;
			face1edge = face2edge = 0xFF;
		}
		void addface(unsigned long face, unsigned char edge)
		{
			if( face1 == 0xFFFFFFFF)
			{
				face1 = face, face1edge = edge;
			}
			else
			{
				face2 = face, face2edge = edge;
			}
		}
		int facecount()
		{
			if( face1 == 0xFFFFFFFF) return 0;
			if( face2 == 0xFFFFFFFF) return 1;
			return 2;
		}
		int face(int i)
		{
			if(i==0) return face1;
			if(i==1) return face2;
			return 0;
		}
		int adjoiningFace(int toFace)
		{
			if(face1!=toFace)
				return face1;
			return face2;
		}
	};

	typedef std::map<Edge32, EdgeBind32> tag_edge_list;

	// ��������� ������ �����
	inline void GetMeshEdgeList(Math::Face32* faces, int facecount, tag_edge_list& edges)
	{
		for( int i=0; i<facecount; i++)
		{
			tag_edge_list::iterator it;
			Face32& face = faces[i];
			edges[Edge32(face.v[0], face.v[1])].addface(i, 0);
			edges[Edge32(face.v[1], face.v[2])].addface(i, 1);
			edges[Edge32(face.v[2], face.v[0])].addface(i, 2);
		}
	}
	// ��������� ������ �����
	inline void GetMeshEdgeList(Math::Polygon32* faces, int facecount, tag_edge_list& edges)
	{
		for( int i=0; i<facecount; i++)
		{
			Polygon32& face = faces[i];
			for( int v=0; v<face.size(); v++)
			{
				edges[Edge32(face.cycle(v), face.cycle(v+1))].addface(i, v);
			}
		}
	}
	// ������ ����� �������� � ���� �������
	inline void GetVertEdgeList( tag_edge_list& edges, std::list<Edge32>& edgelist, int v)
	{
		tag_edge_list::iterator it = edges.begin();
		for(;it != edges.end(); it++)
		{
			if(it->first.v1 == v)
				edgelist.push_back(it->first);
			if(it->first.v2 == v)
				edgelist.push_back(it->first);
		}
	}
}

/**
 * Write/Load components to/from a stream.
 */
template<class Stream> inline
Stream& operator >> (Stream& out, Math::Edge32& v)
{
	out >> v.v1 >> v.v2;
	return out;
}
