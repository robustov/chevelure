#ifndef _ED_Math_Face_h_
#define _ED_Math_Face_h_
#pragma once

namespace Math 
{
	template <class T>
	struct Face
	{
		union
		{
			T v[3];
			struct {T i1, i2, i3; }i;
		};
		Face(T i1=0, T i2=0, T i3=0)
		{
			this->i.i1 = i1;
			this->i.i2 = i2;
			this->i.i3 = i3;
		}
		inline int size() const
		{
			return 3;
		}
		int cycle(int i) const
		{
			if( i>=3) 
				return v[ i%3 ];
			if( i<0) 
				return v[ (i - 3*(i/3) + 6)%3 ];
			return v[i];
		}
		int operator [](int i) const
		{
			return v[i];
		}
		// ����� ������� � ��������
		int getVertexIndex(int gv) const
		{
			if(v[0]==gv) return 0;
			if(v[1]==gv) return 1;
			return 2;
		}
	};


	typedef Face<unsigned short> Face16;
	typedef Face<unsigned long> Face32;

	template<class Stream, class T> inline
	Stream& operator >> (Stream& out, Face<T>& v)
	{
		for( int i=0; i<3; ++i )
			out >> v.v[i];
		return out;
	}

}
#endif /* _ED_Math_Face_h_ */
