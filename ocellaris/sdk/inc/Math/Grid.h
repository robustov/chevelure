#pragma once

#include "Math/Edge.h"
#include "Util\Stream.h"
#include "Util\STLStream.h"
#include "Math/meshutil.h"

namespace Math
{
	struct Grid;

	// ???? ��
	void test_Grid(int level);

	// �����
	enum enGridNeigbour3d
	{
		NEI_X_UP	= 0x00,		// 2 � 3 ���� - ���
		NEI_X_DOWN	= 0x01,		// 1 ��� - ����������� �� ��� (0:++ 1:--)
		NEI_Y_UP	= 0x02, 
		NEI_Y_DOWN	= 0x03, 
		NEI_Z_UP	= 0x04, 
		NEI_Z_DOWN	= 0x05
	};
	// �������� �����������
	enGridNeigbour3d gridInvertDirection(enGridNeigbour3d d);
	inline int gridInvertDirection(int d){return gridInvertDirection((enGridNeigbour3d)d);};
	// �������� �� enGridNeigbour3d
	Math::Vec3i gridNeigbour3dDirection(enGridNeigbour3d d);

	// �� ������ (��������)
	struct GridCellId
	{
		int level;
		int x, y, z;	
	public:
		GridCellId()
		{
			level = 0xFFFFFFF;
		}
		GridCellId(int* data)
		{
			this->level = data[0];
			this->x = data[1];
			this->y = data[2];
			this->z = data[3];
		}
		GridCellId(int level, int x, int y, int z)
		{
			this->level = level;
			this->x = x;
			this->y = y;
			this->z = z;
		}
		GridCellId(int level, const Math::Vec3i& arg)
		{
			this->level = level;
			this->x = arg.x;
			this->y = arg.y;
			this->z = arg.z;
		}
		Math::Vec3i getXYZ() const
		{
			return Math::Vec3i(x, y, z);
		}
		bool isValid() const
		{
			return level != 0xFFFFFFF;
		}
		bool operator<(const GridCellId& arg)const 
		{
			if( level<arg.level) return true;
			if( level>arg.level) return false;
			if( x<arg.x) return true;
			if( x>arg.x) return false;
			if( y<arg.y) return true;
			if( y>arg.y) return false;
			return ( z<arg.z);
		}
		bool operator==(const GridCellId& arg)const 
		{
			if( level!=arg.level) return false;
			if( x!=arg.x) return false;
			if( y!=arg.y) return false;
			return ( z==arg.z);
		}
		bool operator!=(const GridCellId& arg)const 
		{
			return !operator==(arg);
		}
		GridCellId& operator+=(const Math::Vec3i& arg)
		{
			this->x += arg.x;
			this->y += arg.y;
			this->z += arg.z;
			return *this;
		}
	};
	template<class Stream> inline
	Stream& operator >> (Stream& out, Math::GridCellId& v)
	{
		out >> v.level;
		out >> v.x >> v.y >> v.z;	
		return out;
	}

	// �������� ������ � �����������
	struct GridDeformerCell
	{
		// ����� �����������
		int defirmerId;
		// ������ � ����������� (� ������ ��������)
		Math::Vec4f bindmapping;
		// ��� ����������
		float w;
	public:
		GridDeformerCell(int defirmerId=-1)
		{
			this->defirmerId = defirmerId;
			this->bindmapping = Math::Vec4f(0);
			this->w = 0;
		}
		GridDeformerCell(int defirmerId, const Math::Vec4f& bindmapping, float w)
		{
			this->defirmerId = defirmerId;
			this->bindmapping = bindmapping;
			this->w = w;
		}
	};

	// ������ (�������)
	struct GridCell
	{
		GridCellId id;

		// ��� �������
		int timestamp;
		Math::Vec3f cachepos;
		Math::Vec3f cacheposNodeformer;

		// ��������
		Math::Vec3f offset;

		// ����� ������ �� ��������� �������� ������
		std::vector< std::pair<GridCellId, float> > subdiveSheme;
		// ����� ����������
//		std::vector< GridDeformerCell > deformations;

	public:
		GridCell();
		GridCell(GridCellId id);

		void dump(FILE* file)const;

		// ��. GridDeformerCell
//		GridDeformerCell& addGridDeformerCell(
//			int defirmerId
//			);
	};

	// �����
	struct GridEdge
	{
		GridCellId v;
		int nb;
	public:
		GridCellId v1() const;
		GridCellId v2() const;
		GridEdge();
		static bool GridEdgeFromPair(const GridCellId& _v1, const GridCellId& _v2, GridEdge& res);
		GridEdge(GridCellId v, enGridNeigbour3d nb);
		GridEdge(const GridCellId& _v1, const GridCellId& _v2);	// ������ ���� ���������!!!
		bool isValid() const;
		bool operator<(const GridEdge& arg)const;
		bool operator==(const GridEdge& arg)const;
		bool operator!=(const GridEdge& arg)const;
		void dump(FILE* file)const;
		int level()const{return v.level;};
	};
	// NULL Edge
	static GridEdge GridEdgeNULL(GridCellId(0xFFFF, 0, 0, 0), (enGridNeigbour3d)0);

	// ��������� �����
	struct GridFreeEdge
	{
	private:
		// � ��� ������ ���������� �������� ������������ v1() � v2()!
		GridCellId _v1;
		GridCellId _v2;
		bool _invert;
	public:
		GridCellId v1() const;
		GridCellId v2() const;
		GridFreeEdge();
		GridFreeEdge(const GridEdge& edge);
		GridFreeEdge(const GridCellId& _v1, const GridCellId& _v2);
		GridFreeEdge(int level, const Math::Vec3i& _v1, const Math::Vec3i& _v2);
		bool isValid() const;
		bool operator<(const GridFreeEdge& arg)const;
		bool operator==(const GridFreeEdge& arg)const;
		bool operator!=(const GridFreeEdge& arg)const;
		void dump(FILE* file)const;
		int level()const{return _v1.level;};
		Math::Vec3i direction() const;

		template<class Stream>
		void serialize(Stream& out)
		{
			unsigned char version = 0;
			out >> version;
			if( version>=0)
			{
				out >> _invert;
				out >> _v1;
				out >> _v2;
			}
		}
	};

	struct IGridDeformer
	{
		int deformid;

	public:
		// ������������� ����������� ������� ������ ��� ����������
		virtual float deform(
			void* customdata, 
			const Math::Grid& grid, 
			const Math::GridCellId& id, 
			Math::Vec3f& v
			)
		{
			return 0;
		}
		// �������� �������
		virtual void offset(
			void* customdata, 
			const Math::Grid& grid, 
			const Math::GridCellId& id, 
			const Math::Vec3f& v,
			Math::Vec3f& offset
			)
		{
		}

	};
	// 3d �����
	struct Grid
	{
		// �������� � ��������

		// ������ ������
		GridCell* getCell(GridCellId id);
		// ������ ������ (���� ��� - ��������� ������������)
		GridCell& addCell(GridCellId id);
		// ������� ������
		Math::Vec3f getCellPos(GridCell& cell, bool bDeformers=true);
		// ������� ������ �� id (������������ ������ ����� offset=0)
		Math::Vec3f getCellPos(const GridCellId& id, bool bDeformers=true);
		// ������� �������� �������� ������
		Math::Vec3f getTopLevelCellPos(const GridCellId& id);

		// ����� � ����? � ���?
		bool isPointInCube(const Math::Vec3f& p, const GridCellId& cube, Math::Vec3f* posInCube);

		// ������ ������ � ���� ���� ������ levelWanted � ������� �������� �����
		// posInCube - ������ ������������ ������� � ���� (�� 0 �� 1)
		GridCellId findCube(Math::Vec3f p, int levelWanted, Math::Vec3f* posInCube=0);
		// ������ ������ � ���� ���� �������� ������ levelWanted � ������� �������� �����
		// posInCube - ������ ������������ ������� � ���� (�� 0 �� 1)
		GridCellId findTopLevelCube(Math::Vec3f p, int levelWanted, Math::Vec3f* posInCube=0);
		// ������ ��������� ������ � �����
		// posInCube - ������ ������������ ������� � ���� (�� -0.5 �� 0.5)
		GridCellId findVertex(Math::Vec3f p, int levelWanted, Math::Vec3f* posInCube=0);
		// ������ ��������� ������ � ����� (��� ������� ������� � ����)
		GridCellId findClosestVertex(Math::Vec3f p, int levelWanted, bool bIgnoreDeform=false);

		

		// ������ ����� ������ level
		float getLevelDimention(int level);

		// �������� � ��������
		static GridCellId getNullId();

		// �������� ������
		static GridCellId getNeigbour(const GridCellId& id, int nb);
		static GridCellId getNeigbour(const GridCellId& id, enGridNeigbour3d nb){return getNeigbour(id, (int)nb);}
//		GridCell* getNeigbour(GridCell& cell, int nb){return getCell(getNeigbour(cell.id, nb));}
//		GridCell* getNeigbour(GridCell& cell, enGridNeigbour3d nb){return getCell(getNeigbour(cell.id, (int)nb));}

		// ����� �� ������ ����
		static GridCellId getLowCell(const GridCellId& id);
		// ����� �� ������ ���� (������ ���������� ����� ���� ���)
		// ���� bAlways - ������ ����� ���� ���� �� ������ � ������� �������� ������
		static GridCellId getUpCell(const GridCellId& id, bool bAlways=false);

		// �������� ����� (�� ������� ����)
		static GridCellId getEdgeMiddle(const GridEdge& edge);

		// ������ ��������� ����� � ���� (��.findCube � findTopLevelCube)
		static GridEdge getClosestEdge(const GridCellId& cubeid, const Math::Vec3f& posInCube);

		// �������� ����
		static void CubeVert(
			const GridCellId& id,					// �� ���� (������� �������)
			const Math::Vec3f& posInCube,	// ������� � ���� [0, 1)
			int v,							// ����� ������� [0, 7]
			GridCellId& cubevert,			// ������
			Math::Vec3f& cubevertofset);	// �������� �� ������

		// �������� ����� �� NAN
		bool TestGrid(
			);
	public:
		// �������
		struct Level
		{
			std::map< GridCellId, GridCell> cells;
		};
		// ������ �������
		std::map<int, Level> levels;

		// ������� ������� ��������� ���������
		int startLevel;
		// ������ ������ ��� ������ 0 
		float dimention0;

		// ������� �����������
		void* deform_customdata;
		IGridDeformer** deforms; 
		int deformcount;
		// World ������� �����, ���������� ��� �����������(����)
		Math::Matrix4f worldMatrix;
		Math::Matrix4f worldMatrixInv;

		// ��� ����������� ������� �����;
		int timestamp;
	public:
		Grid();

		void clear();

		// �������� ������ ��������� (���� deformedId==-1 ��������� ���)
//		void clearGridDeformerCells(
//			int deformerId = -1
//			);

		void dump(int level, FILE* file);

		void setUpDeformers(
			void* customdata, 
			Math::Matrix4f& worldMatrix,
			IGridDeformer** deforms=NULL, 
			int deformcount=0
			);

		// ������ ����� ������� ��� ������ (������ ������ �������� ������)
		static void getCellSubdiv(
			GridCellId id, 
			std::vector< std::pair<GridCellId, float> >& subdiveSheme		// out
			);
	};

	#include "Grid.hpp"
}

template<class Stream> inline
Stream& operator >> (Stream& out, Math::GridDeformerCell& v)
{
	unsigned char version = 0;
	out >> version;
	if( version>=0)
	{
		out >> v.defirmerId;
		out >> v.bindmapping;
		out >> v.w;
	}
	return out;
}

template<class Stream> inline
Stream& operator >> (Stream& out, Math::GridCell& v)
{
	unsigned char version = 0;
	out >> version;
	if( version>=0)
	{
		out >> v.id;
		out >> v.offset;	
		out >> v.subdiveSheme;
		std::vector< Math::GridDeformerCell > deformations;
		out >> deformations;
	}
	return out;
}
template<class Stream> inline
Stream& operator >> (Stream& out, Math::GridEdge& v)
{
	out >> v.v;
	out >> v.nb;	
	return out;
}
template<class Stream> inline
Stream& operator >> (Stream& out, Math::GridFreeEdge& v)
{
	v.serialize(out);
	return out;
}

template<class Stream> inline
Stream& operator >> (Stream& out, Math::Grid::Level& v)
{
	unsigned char version = 0;
	out >> version;
	if( version>=0)
	{
		out >> v.cells;
	}
	return out;
}
template<class Stream> inline
Stream& operator >> (Stream& out, Math::Grid& v)
{
	unsigned char version = 0;
	out >> version;
	if( version>=0)
	{
		out >> v.levels;
		out >> v.startLevel;
		out >> v.dimention0;
	}
	return out;
}
