
// �������� �����������
inline enGridNeigbour3d gridInvertDirection(enGridNeigbour3d d)
{
	int dd = (int)d;
	dd = dd^0x1;
	return (enGridNeigbour3d)dd;
}
// �������� �� enGridNeigbour3d
inline Math::Vec3i getNeigbour3dDirection(enGridNeigbour3d nb)
{
	int dir = nb&0x1;
	if( dir==0) 
		dir = 1;
	else
		dir = -1;
	int axis = nb>>1;
	Math::Vec3i offset(0);
	switch(axis)
	{
	case 0:
		offset.x += dir;
		break;
	case 1:
		offset.y += dir;
		break;
	case 2:
		offset.z += dir;
		break;
	}
	return offset;
}

inline GridCell::GridCell()
{
	timestamp = -1;
}

inline GridCell::GridCell(GridCellId id)
{
	timestamp = -1;
	offset = Math::Vec3f(0.f);
	this->id = id;

	Grid::getCellSubdiv(id, subdiveSheme);
}

inline void GridCell::dump(FILE* file)const
{
	fprintf(file, "  cell: %d %d %d %d:\n", this->id.level, this->id.x, this->id.y, this->id.z);
	// ��������� ��� ��� ������� ���������
	for(int i=0; i<(int)this->subdiveSheme.size(); i++)
	{
		const GridCellId& idparent = this->subdiveSheme[i].first;
		fprintf(file, "  parent: %f -> %d %d %d %d:\n", this->subdiveSheme[i].second, 
			idparent.level, idparent.x, idparent.y, idparent.z);
	}
}
inline GridDeformerCell& GridCell::addGridDeformerCell(
	int deformerId
	)
{
	for(int i=0; i<(int)this->deformations.size(); i++)
	{
		if( this->deformations[i].defirmerId==deformerId)
		{
			return this->deformations[i];
		}
	}
	this->deformations.push_back( GridDeformerCell(deformerId));
	return this->deformations.back();
}


/////////////////////////////////////////
//
//
// GridEdge
// 

inline GridCellId GridEdge::v1() const
{
	return v;
}
inline GridCellId GridEdge::v2() const
{
	return Grid::getNeigbour(v, nb);
}
inline GridEdge::GridEdge()
{
}
inline GridEdge::GridEdge(GridCellId v, enGridNeigbour3d nb)
{
	this->v = v;
	this->nb = (int)nb;
	int dir = nb&0x1;
	if(dir==1)
	{
		// �������������
		int axis = nb>>1;
		this->nb = this->nb&(~0x1);
		if(axis==0)
			this->v.x-=1;
		else if(axis==1)
			this->v.y-=1;
		else if(axis==2)
			this->v.z-=1;
	}
}
inline bool GridEdge::GridEdgeFromPair(const GridCellId& _v1, const GridCellId& _v2, GridEdge& res)
{
	res.v.level = _v1.level;
	res.v.x = __min(_v1.x, _v2.x);
	res.v.y = __min(_v1.y, _v2.y);
	res.v.z = __min(_v1.z, _v2.z);
	GridCellId xv;
	xv.x = __max(_v1.x, _v2.x);
	xv.y = __max(_v1.y, _v2.y);
	xv.z = __max(_v1.z, _v2.z);
	
	if(res.v.x!=xv.x)
	{
		res.nb = NEI_X_UP;
		if(res.v.y!=xv.y || res.v.z!=xv.z)
			return false;
		return true;
	}
	if(res.v.y!=xv.y)
	{
		res.nb = NEI_Y_UP;
		if(res.v.x!=xv.x || res.v.z!=xv.z)
			return false;
		return true;
	}
	if(res.v.z!=xv.z)
	{
		res.nb = NEI_Z_UP;
		if(res.v.x!=xv.x || res.v.y!=xv.y)
			return false;
		return true;
	}
	return false;
}

// ������ ���� ���������!!!
inline GridEdge::GridEdge(const GridCellId& _v1, const GridCellId& _v2)
{
	this->v.level = _v1.level;
	this->v.x = __min(_v1.x, _v2.x);
	this->v.y = __min(_v1.y, _v2.y);
	this->v.z = __min(_v1.z, _v2.z);
	GridCellId xv;
	xv.x = __max(_v1.x, _v2.x);
	xv.y = __max(_v1.y, _v2.y);
	xv.z = __max(_v1.z, _v2.z);
	
	if(this->v.x!=xv.x)
		this->nb = NEI_X_UP;
	if(this->v.y!=xv.y)
		this->nb = NEI_Y_UP;
	if(this->v.z!=xv.z)
		this->nb = NEI_Z_UP;
}

inline bool GridEdge::isValid() const
{
	return this->v.isValid();
}
inline bool GridEdge::operator<(const GridEdge& arg)const 
{
	if( nb<arg.nb) return true;
	if( nb>arg.nb) return false;
	return ( v<arg.v);
}
inline bool GridEdge::operator==(const GridEdge& arg)const 
{
	if( nb==arg.nb && v==arg.v) return true;
	return false;
}
inline bool GridEdge::operator!=(const GridEdge& arg)const 
{
	if( nb!=arg.nb || v!=arg.v) return true;
	return false;
}
inline void GridEdge::dump(FILE* file)const
{
	fprintf(file, "  edge: %d %d %d %d (nb=%d):\n", this->v.level, this->v.x, this->v.y, this->v.z, this->nb);
}




/////////////////////////////////////////
//
//
// GridFreeEdge
// 

inline GridCellId GridFreeEdge::v1() const
{
	if( !_invert)
		return _v1;
	else
		return _v2;
}
inline GridCellId GridFreeEdge::v2() const
{
	if( !_invert)
		return _v2;
	else
		return _v1;
}
inline GridFreeEdge::GridFreeEdge()
{
	_invert = false;
}

// ������ ���� ���������!!!
inline GridFreeEdge::GridFreeEdge(const GridCellId& _v1, const GridCellId& _v2)
{
	this->_v1 = _v1;
	this->_v2 = _v2;
	if(_v2<_v1)
	{
		std::swap(this->_v1, this->_v2);
		this->_invert = true;
	}
}
inline GridFreeEdge::GridFreeEdge(const GridEdge& edge)
{
	this->_v1 = edge.v1();
	this->_v2 = edge.v2();
	this->_invert = false;
}
inline GridFreeEdge::GridFreeEdge(int level, const Math::Vec3i& _v1, const Math::Vec3i& _v2)
{
	this->_v1 = GridCellId(level, _v1);
	this->_v2 = GridCellId(level, _v2);
	if(this->_v2<this->_v1)
	{
		std::swap(this->_v1, this->_v2);
		this->_invert = true;
	}
}

inline bool GridFreeEdge::isValid() const
{
	return this->_v1.isValid() && this->_v2.isValid();
}
inline bool GridFreeEdge::operator<(const GridFreeEdge& arg)const 
{
	if( _v1<arg._v1) return true;
	if( arg._v1<_v1) return false;
	return ( _v2<arg._v2);
}
inline bool GridFreeEdge::operator==(const GridFreeEdge& arg)const 
{
	if( _v1==arg._v1 && _v2==arg._v2) return true;
	return false;
}
inline bool GridFreeEdge::operator!=(const GridFreeEdge& arg)const 
{
	if( _v1!=arg._v1 || _v2!=arg._v2) return true;
	return false;
}
inline void GridFreeEdge::dump(FILE* file)const
{
	fprintf(file, "  edge: (%d %d %d %d) - (%d %d %d %d) %s:\n", 
		this->_v1.level, this->_v1.x, this->_v1.y, this->_v1.z, 
		this->_v2.level, this->_v2.x, this->_v2.y, this->_v2.z,
		this->_invert?"forward":"revert"
		);
}
inline Math::Vec3i GridFreeEdge::direction() const
{
	if( !this->_invert)
		return Math::Vec3i(
			this->_v2.x - this->_v1.x, 
			this->_v2.y - this->_v1.y, 
			this->_v2.z - this->_v1.z
			);
	else
		return Math::Vec3i(
			this->_v1.x - this->_v2.x, 
			this->_v1.y - this->_v2.y, 
			this->_v1.z - this->_v2.z
			);

}





/////////////////////////////////////////
//
//
// Grid
// 
inline Grid::Grid()
{
	clear();
}
inline void Grid::clear()
{
	timestamp = -1;
	startLevel = 0;
	dimention0 = 1;
	levels.clear();
	deforms = NULL;
	deformcount = 0;
	worldMatrix=Math::Matrix4f(1);
}
// �������� ������ ��������� (���� deformedId==-1 ��������� ���)
inline void Grid::clearGridDeformerCells(
	int deformerId
	)
{
	std::map<int, Level>::iterator itl = levels.begin();
	for(;itl != levels.end(); itl++)
	{
		Level& lev = itl->second;
		std::map<GridCellId, GridCell>::iterator it = lev.cells.begin();
		for(;it != lev.cells.end(); it++)
		{
			GridCell& cell = it->second;
			if( deformerId==-1)
				cell.deformations.clear();
			else
			{
				for(int i=0; i<(int)cell.deformations.size(); )
					if( cell.deformations[i].defirmerId==deformerId)
						cell.deformations.erase( &cell.deformations[i]);
					else
						i++;
			}
		}
	}
}


inline GridCell* Grid::getCell(const GridCellId& id)
{
	std::map<int, Level>::iterator it = levels.find(id.level);
	if( it == levels.end())
		return NULL;
	Level& level = it->second;

	{
		std::map<GridCellId, GridCell>::iterator it = level.cells.find(id);
		if(it!=level.cells.end())
			return &it->second;
	}
	return NULL;
}
// ������ ������ (���� ��� - ��������� ������������)
inline GridCell& Grid::addCell(const GridCellId& id)
{
	if(id.level<startLevel)
	{
		// �������� ������� ������� ���������
		startLevel = id.level;
		// ���� �������� ������!
		printf("Grid::addCell: id.level<startLevel - not implemented!!!\n");
	}

	Level& level = levels[id.level];
	std::map<GridCellId, GridCell>::iterator it = level.cells.find(id);
	if(it!=level.cells.end())
	{
		return it->second;
	}
	level.cells[id] = GridCell(id);

	GridCell& cell = level.cells[id];
	if(cell.id != id)
	{
		printf("add error!!!\n");
	}
	
	// ��������� ��� ��� ������� ���������
	for(int i=0; i<(int)cell.subdiveSheme.size(); i++)
	{
		GridCellId& idparent = cell.subdiveSheme[i].first;
		if( idparent.level < startLevel)
			continue;
		if( !this->getCell(idparent))
			addCell(idparent);
	}
	return level.cells[id];
}

// ������ ����� ������ level
inline float Grid::getLevelDimention(int level)
{
	float dim = dimention0 * (float)(pow((double)2, -level));
	return dim;
}

// ������� ������
inline Math::Vec3f Grid::getCellPos(GridCell& cell, bool bDeformers)
{
	if( cell.id.level<startLevel)
		return Math::Vec3f(0);

	if( this->timestamp!=-1 && cell.timestamp == this->timestamp)
		return cell.cachepos;

	Math::Vec3f pos(0);
	for( int i=0; i<(int)cell.subdiveSheme.size(); i++)
	{
		GridCellId& id = cell.subdiveSheme[i].first;
		float w = cell.subdiveSheme[i].second;

		if( id.level<startLevel)
		{
			// ������������
			Math::Vec3f pp = getTopLevelCellPos(id);
			pos += pp*w;
		}
		else
		{
			GridCell* parent = this->getCell(id);
			if( !parent) 
			{
				parent = &this->addCell(id);
			}

			Math::Vec3f pp = this->getCellPos(*parent, bDeformers);
			pos += pp*w;
		}
	}
	pos += cell.offset;

	// ����� ����������
	if( this->deforms && bDeformers && !cell.deformations.empty())
	{
		// ����������� ���������
		Math::Vec3f offset(0);
		Math::Vec3f defpos(0);
		float sumw = 0;
		for(int d=0; d<(int)cell.deformations.size(); d++)
		{
			GridDeformerCell& gdc = cell.deformations[d];
			int defId = gdc.defirmerId;
			if( defId<0)
				continue;

			// ��������� ��� ���������
			for(int ddd=0; ddd<deformcount; ddd++)
			{
				IGridDeformer* deformer = this->deforms[ddd]; 
				if( !deformer) continue;

				Math::Vec3f v = pos;
				if( deformer->deform(*this, cell.id, v, offset, gdc, this->worldMatrix))
				{
					defpos += gdc.w*v;
					sumw += gdc.w;
				}
			}
		}
		if( sumw!=0)
		{
			if(sumw>1) 
			{
				defpos = defpos*(1/sumw);
				sumw=1;
			}
			pos = (1-sumw)*pos + defpos;
		}
		pos += offset;
	}
	if( bDeformers)
	{
		// ������������� ���������
		for(int d=0; d<deformcount; d++)
		{
			IGridDeformer* deformer = this->deforms[d]; 
			if( !deformer) continue;

			deformer->deform(*this, cell.id, this->worldMatrix, pos);
		}
	}

	cell.timestamp = this->timestamp;
	cell.cachepos = pos;

	return pos;
}
// ������� ������ �� id (������������ ������ ����� offset=0)
inline Math::Vec3f Grid::getCellPos(const GridCellId& id, bool bDeformers)
{
	if( id.level<startLevel)
		return Math::Vec3f(0);

	GridCell* cell = getCell(id);
	if(cell)
		return Grid::getCellPos(*cell, bDeformers);

	// ������ ����� ������� ��� ������ (������ ������ �������� ������)
	std::vector< std::pair<GridCellId, float> > subdiveSheme;
	Grid::getCellSubdiv(id, subdiveSheme);

	Math::Vec3f pos(0);
	for( int i=0; i<(int)subdiveSheme.size(); i++)
	{
		GridCellId& id = subdiveSheme[i].first;
		float w = subdiveSheme[i].second;

		if( id.level<startLevel)
		{
			// ������������
			Math::Vec3f pp = getTopLevelCellPos(id);
			pos += pp*w;
		}
		else
		{
			Math::Vec3f pp = this->getCellPos(id, bDeformers);
			pos += pp*w;
		}
	}
	return pos;
}

// ������� �������� �������� ������
inline Math::Vec3f Grid::getTopLevelCellPos(const GridCellId& id)
{
	float dim = getLevelDimention(id.level);

	float startlevelcelldim = 1;
	Math::Vec3f pp = Math::Vec3f(dim*id.x, dim*id.y, dim*id.z)*startlevelcelldim;
	return pp;
}



// ������ ������ � ���� ���� ������ levelWanted � ������� �������� �����
inline GridCellId Grid::findCube(Math::Vec3f p, int levelWanted, Math::Vec3f* posInCube)
{
	if(levelWanted<this->startLevel)
		// �� ���� ������!
		return this->findTopLevelCube(p, levelWanted, posInCube);

	// ���������� � this->startLevel-1 � ������ ������� ���������
	GridCellId curcube = this->findTopLevelCube(p, this->startLevel-1, posInCube);
	for( ; curcube.level < levelWanted; )
	{
		// ����� ��������� ������� ������ curcube.level+1 ����� curcube
		GridCellId centerid = curcube;
		centerid.level++;
		centerid.x = (centerid.x<<1) + 1;
		centerid.y = (centerid.y<<1) + 1;
		centerid.z = (centerid.z<<1) + 1;

		// ��������� ������ ����������� � id
		bool bFind = false;
		int _min=-2, _max=1;
		for(int x=_min; x<=_max; x++)
		{
			for(int y=_min; y<=_max; y++)
			{
				for(int z=_min; z<=_max; z++)
				{
					GridCellId cube = centerid;
					cube += Math::Vec3i(x, y, z);
					if( isPointInCube(p, cube, posInCube))
					{
						curcube = cube;
						bFind = true;
						break;
					}
				}
			}
		}
		/*/
		float mindist=1e32f;
		GridCellId id = centerid;
		for(int x=-1; x<=1; x++)
		{
			for(int y=-1; y<=1; y++)
			{
				for(int z=-1; z<=1; z++)
				{
					GridCellId cur = centerid;
					cur += Math::Vec3i(x, y, z);
					Math::Vec3f pos = this->getCellPos(cur);
					float d = (pos-p).length2();
					if(d<mindist)
					{
						mindist = d;
						id = cur;
					}
				}
			}
		}

		// ��������� ������ ����������� � id
		bool bFind = false;
		for(int i=0; i<8; i++)
		{
			GridCellId cube = id;
			if( (i&0x1)==0) cube.x--;
			if( (i&0x2)==0) cube.y--;
			if( (i&0x4)==0) cube.z--;
			
			if( isPointInCube(p, cube, posInCube))
			{
				curcube = cube;
				bFind = true;
				break;
			}
		}
		/*/
		if(!bFind)
		{
			// ������� �����
			printf("pt={%f %f %f}\n", p.x, p.y, p.z);
			printf("fucking grid here cube=%d {%d %d %d}\n", centerid.level, centerid.x, centerid.y, centerid.z);
			// ���� ����� ������!!!
			return findTopLevelCube(p, levelWanted, posInCube);
		}
	}
	
	return curcube;

//	GridCellId id = Grid::findTopLevelCube(p, startLevel-1, posInCube);
	// ���������� ����
	// 
	// �� �����������!
//	return findTopLevelCube(p, levelWanted, posInCube);
}

// ����� � ����? � ���?
inline bool Grid::isPointInCube(const Math::Vec3f& p, const GridCellId& cube, Math::Vec3f* posInCube)
{
	Math::Vec3f pts[8];
	for(int i=0; i<8; i++)
	{
		GridCellId id = cube;
		if(i&0x1) id.x++;
		if(i&0x2) id.y++;
		if(i&0x4) id.z++;
		pts[i] = getCellPos(id, false);
	}
	return Math::isPointInCube(p, pts, posInCube);
}

// ������ ������ � ���� ���� ������ levelWanted ��� ����� ������� ���������
inline GridCellId Grid::findTopLevelCube(Math::Vec3f p, int levelWanted, Math::Vec3f* posInCube)
{
	GridCellId id;
	id.level = levelWanted;
	float dim = getLevelDimention(id.level);
	Math::Vec3f pn;
	p.x/=dim;
	p.y/=dim;
	p.z/=dim;
	pn.x = std::floor(p.x);
	pn.y = std::floor(p.y);
	pn.z = std::floor(p.z);
	if( posInCube)
	{
		*posInCube = p-pn;
	}
	id.x = int(pn.x);
	id.y = int(pn.y);
	id.z = int(pn.z);
	return id;
}

// ������ ��������� ������ � �����
// posInCube - ������ ������������ ������� � ���� (�� -0.5 �� 0.5)
inline GridCellId Grid::findVertex(Math::Vec3f p, int levelWanted, Math::Vec3f* outpc)
{
	Math::Vec3f posInCube;
	GridCellId id = findCube(p, levelWanted, &posInCube);

	if( posInCube.x>0.5f)
		id.x++, posInCube.x-=1.f;
	if( posInCube.y>0.5f)
		id.y++, posInCube.y-=1.f;
	if( posInCube.z>0.5f)
		id.z++, posInCube.z-=1.f;

	if( outpc)
		*outpc = posInCube;
	return id;
}
// ������ ��������� ������ � ����� (��� ������� ������� � ����)
inline GridCellId Grid::findClosestVertex(Math::Vec3f p, int levelWanted)
{
	if(levelWanted<this->startLevel)
		// �� ���� ������!
		return this->findTopLevelCube(p, levelWanted, NULL);

	// ���������� � this->startLevel-1 � ������ ������� ���������
	Math::Vec3f posInCube;
	GridCellId curcube = this->findTopLevelCube(p, this->startLevel-1, &posInCube);
	if( posInCube.x>0.5f)
		curcube.x++, posInCube.x-=1.f;
	if( posInCube.y>0.5f)
		curcube.y++, posInCube.y-=1.f;
	if( posInCube.z>0.5f)
		curcube.z++, posInCube.z-=1.f;

	for( ; curcube.level < levelWanted; )
	{
		curcube = this->getLowCell(curcube);

		// ��������� �������� ����������� � id
		float mindist=1e32f;
		GridCellId id = curcube;
		int _min=-1, _max=1;
		for(int x=_min; x<=_max; x++)
		{
			for(int y=_min; y<=_max; y++)
			{
				for(int z=_min; z<=_max; z++)
				{
					GridCellId cur = curcube;
					cur += Math::Vec3i(x, y, z);
					Math::Vec3f pos = this->getCellPos(cur);
					float d = (pos-p).length2();
					if(d<mindist)
					{
						mindist = d;
						id = cur;
					}
				}
			}
		}
		curcube = id;
	}
	return curcube;
}

inline GridCellId Grid::getNullId()
{ 
	return GridCellId(0xFFFFFFF, 0, 0, 0);
}

inline GridCellId Grid::getNeigbour(const GridCellId& id, int nb)
{
	int dir = nb&0x1;
	if( dir==0) 
		dir = 1;
	else
		dir = -1;
	int axis = nb>>1;
	GridCellId cell = id;
	switch(axis)
	{
	case 0:
		cell.x += dir;
		break;
	case 1:
		cell.y += dir;
		break;
	case 2:
		cell.z += dir;
		break;
	}
	return cell;
}

// ����� �� ������ ����
inline GridCellId Grid::getLowCell(const GridCellId& _id)
{
	GridCellId id = _id;
	id.level++;
	id.x = id.x<<1;
	id.y = id.y<<1;
	id.z = id.z<<1;
	return id;
}
// ����� �� ������ ���� (������ ���������� ����� ���� ���)
// ���� bAlways - ������ ����� ���� ���� �� ������ � ������� �������� ������
inline GridCellId Grid::getUpCell(const GridCellId& _id, bool bAlways)
{
	GridCellId id = _id;
	if( !bAlways)
	{
		int firstbitcount = 0;
		firstbitcount += (id.x & 0x1);
		firstbitcount += (id.y & 0x1);
		firstbitcount += (id.z & 0x1);
		if(firstbitcount!=0) 
			return GridCellId();
	}

	id.level--;
	id.x = id.x>>1;
	id.y = id.y>>1;
	id.z = id.z>>1;
	return id;
}

// �������� ����� (�� ������� ����)
inline GridCellId Grid::getEdgeMiddle(const GridEdge& edge)
{
	GridCellId id = edge.v;
	id.level++;
	id.x = id.x<<1;
	id.y = id.y<<1;
	id.z = id.z<<1;
	
	int axis = edge.nb>>1;
	if(axis==0)
		id.x++;
	else if(axis==1)
		id.y++;
	else if(axis==2)
		id.z++;

	return id;
}

// ������ ��������� ����� � ���� (��.findCube � findTopLevelCube)
inline GridEdge MathOld::Grid::getClosestEdge(
	const GridCellId& cubeid, 
	const Math::Vec3f& posInCube)
{
	// ��������� �����
	float x=posInCube.x, y=posInCube.y, z=posInCube.z;
	int ix=cubeid.x, iy=cubeid.z, iz=cubeid.z;
	
	if( x>0.5f)
	{
		x = 1-x;
		ix++;
	}
	if( y>0.5f)
	{
		y = 1-y;
		iy++;
	}
	if( z>0.5f)
	{
		z = 1-z;
		iz++;
	}

	if( x>y && x>z)
	{
		// yz
		return GridEdge(
			GridCellId(cubeid.level, cubeid.x, iy, iz), 
			NEI_X_UP
			);
	}
	if( y>=x && y>z)
	{
		// xz
		return GridEdge(
			GridCellId(cubeid.level, ix, cubeid.y, iz), 
			NEI_Y_UP
			);
	}
	if( z>=x && z>=y)
	{
		// xy
		return GridEdge(
			GridCellId(cubeid.level, ix, iy, cubeid.z), 
			NEI_Z_UP
			);
	}
	printf("MathOld::Grid::getClosestEdge some error!!!\n");
	// xy
	return GridEdge(
		GridCellId(cubeid.level, ix, iy, cubeid.z), 
		NEI_Z_UP
		);
}

// �������� ����
inline void MathOld::Grid::CubeVert(
	const GridCellId& id,					// �� ���� (������� �������)
	const Math::Vec3f& posInCube,	// ������� � ���� [0, 1)
	int v,							// ����� ������� [0, 7]
	GridCellId& cubevert, 
	Math::Vec3f& cubevertofset)
{
	cubevert = id;
	cubevertofset = posInCube;
	if(v&0x1)
	{
		cubevert.x += 1;
		cubevertofset.x -= 1;
	}
	if(v&0x2)
	{
		cubevert.y += 1;
		cubevertofset.y -= 1;
	}
	if(v&0x4)
	{
		cubevert.z += 1;
		cubevertofset.z -= 1;
	}
}

// ������ ����� ������� ��� ������ (������ ������ �������� ������)
inline void Grid::getCellSubdiv(
	GridCellId id, 
	std::vector< std::pair<GridCellId, float> >& subdiveSheme		// out
	)
{
	int firstbitcount = 0;
	firstbitcount += (id.x & 0x1);
	firstbitcount += (id.y & 0x1);
	firstbitcount += (id.z & 0x1);

	GridCellId par = id;
	par.level--;
	par.x = par.x>>1;
	par.y = par.y>>1;
	par.z = par.z>>1;

	if(firstbitcount==0)
	{
		// Av - � ������� ��������
		subdiveSheme.push_back(
			std::pair<GridCellId, float>(par, 1.f)
			);
	}
	else if(firstbitcount==1)
	{
		// Ae - �� ����� - ������� �� 2-� ���������
		// ����� � ����� 1
		if( (id.x & 0x1)) 
		{
			subdiveSheme.push_back(
				std::pair<GridCellId, float>(GridCellId(par.level, par.x+0, par.y, par.z), 0.5f)
				);
			subdiveSheme.push_back(
				std::pair<GridCellId, float>(GridCellId(par.level, par.x+1, par.y, par.z), 0.5f)
				);
		}
		else if( (id.y & 0x1)) 
		{
			subdiveSheme.push_back(
				std::pair<GridCellId, float>(GridCellId(par.level, par.x, par.y+0, par.z), 0.5f)
				);
			subdiveSheme.push_back(
				std::pair<GridCellId, float>(GridCellId(par.level, par.x, par.y+1, par.z), 0.5f)
				);
		}
		if( (id.z & 0x1)) 
		{
			subdiveSheme.push_back(
				std::pair<GridCellId, float>(GridCellId(par.level, par.x, par.y, par.z+0), 0.5f)
				);
			subdiveSheme.push_back(
				std::pair<GridCellId, float>(GridCellId(par.level, par.x, par.y, par.z+1), 0.5f)
				);
		}
	}
	else if(firstbitcount==2)
	{
		// Af �� ����� - ������� �� 4-�
		if( (id.x & 0x1)==0) 
		{
			subdiveSheme.push_back(
				std::pair<GridCellId, float>(GridCellId(par.level, par.x, par.y+0, par.z+0), 0.25f)
				);
			subdiveSheme.push_back(
				std::pair<GridCellId, float>(GridCellId(par.level, par.x, par.y+1, par.z+0), 0.25f)
				);
			subdiveSheme.push_back(
				std::pair<GridCellId, float>(GridCellId(par.level, par.x, par.y+0, par.z+1), 0.25f)
				);
			subdiveSheme.push_back(
				std::pair<GridCellId, float>(GridCellId(par.level, par.x, par.y+1, par.z+1), 0.25f)
				);
		}
		if( (id.y & 0x1)==0) 
		{
			subdiveSheme.push_back(
				std::pair<GridCellId, float>(GridCellId(par.level, par.x+0, par.y, par.z+0), 0.25f)
				);
			subdiveSheme.push_back(
				std::pair<GridCellId, float>(GridCellId(par.level, par.x+1, par.y, par.z+0), 0.25f)
				);
			subdiveSheme.push_back(
				std::pair<GridCellId, float>(GridCellId(par.level, par.x+0, par.y, par.z+1), 0.25f)
				);
			subdiveSheme.push_back(
				std::pair<GridCellId, float>(GridCellId(par.level, par.x+1, par.y, par.z+1), 0.25f)
				);
		}
		if( (id.z & 0x1)==0) 
		{
			subdiveSheme.push_back(
				std::pair<GridCellId, float>(GridCellId(par.level, par.x+0, par.y+0, par.z), 0.25f)
				);
			subdiveSheme.push_back(
				std::pair<GridCellId, float>(GridCellId(par.level, par.x+1, par.y+0, par.z), 0.25f)
				);
			subdiveSheme.push_back(
				std::pair<GridCellId, float>(GridCellId(par.level, par.x+0, par.y+1, par.z), 0.25f)
				);
			subdiveSheme.push_back(
				std::pair<GridCellId, float>(GridCellId(par.level, par.x+1, par.y+1, par.z), 0.25f)
				);
		}
	}
	else
	{
		// Ac - � ������ ������� �� 8��
		subdiveSheme.push_back(
			std::pair<GridCellId, float>(GridCellId(par.level, par.x+0, par.y, par.z), 0.125f)
			);
		subdiveSheme.push_back(
			std::pair<GridCellId, float>(GridCellId(par.level, par.x+1, par.y, par.z), 0.125f)
			);

		subdiveSheme.push_back(
			std::pair<GridCellId, float>(GridCellId(par.level, par.x+0, par.y+1, par.z), 0.125f)
			);
		subdiveSheme.push_back(
			std::pair<GridCellId, float>(GridCellId(par.level, par.x+1, par.y+1, par.z), 0.125f)
			);

		subdiveSheme.push_back(
			std::pair<GridCellId, float>(GridCellId(par.level, par.x+0, par.y, par.z+1), 0.125f)
			);
		subdiveSheme.push_back(
			std::pair<GridCellId, float>(GridCellId(par.level, par.x+1, par.y, par.z+1), 0.125f)
			);

		subdiveSheme.push_back(
			std::pair<GridCellId, float>(GridCellId(par.level, par.x+0, par.y+1, par.z+1), 0.125f)
			);
		subdiveSheme.push_back(
			std::pair<GridCellId, float>(GridCellId(par.level, par.x+1, par.y+1, par.z+1), 0.125f)
			);
	}
}

inline void Grid::dump(int level, FILE* file)
{
	Grid::Level& lev = this->levels[level];


	printf("LEVEL %d:\n", level);
	std::set<MathOld::GridEdge> edges;
	std::map<GridCellId, GridCell>::iterator it = lev.cells.begin();
	for(;it != lev.cells.end(); it++)
	{
		GridCell& cell = it->second;
		Math::Vec3f pos = this->getCellPos(cell);
		printf("  cell: %d %d %d %d:\n", cell.id.level, cell.id.x, cell.id.y, cell.id.z);
		printf("  pos: %f, %f, %f\n", pos.x, pos.y, pos.z);
		for(int nb=0; nb<6; nb++)
		{
			if( this->getNeigbour(cell, nb))
			{
				MathOld::GridEdge edge(cell.id, (enGridNeigbour3d)nb);
				edges.insert(edge);
			}
		}
	}

	{
		std::set<MathOld::GridEdge>::iterator it = edges.begin();
		for(;it != edges.end(); it++)
		{
			MathOld::GridEdge& edge = *it;
			MathOld::GridCell* cell1 = this->getCell(edge.v1());
			MathOld::GridCell* cell2 = this->getCell(edge.v2());
			if( !cell1 || !cell2)
				continue;

			MathOld::GridCell cell = *cell1;
			printf("  Edge: (%d):\n", edge.nb);
			printf("  cell: %d %d %d %d:\n", cell.id.level, cell.id.x, cell.id.y, cell.id.z);
			cell = *cell2;
			printf("  cell: %d %d %d %d:\n", cell.id.level, cell.id.x, cell.id.y, cell.id.z);
		}
	}
}

inline void Grid::setUpDeformers(
	Math::Matrix4f& worldMatrix,
	IGridDeformer** deforms, 
	int deformcount
	)
{
	this->deforms = deforms; 
	this->deformcount = deformcount;
	this->worldMatrix = worldMatrix;
}

