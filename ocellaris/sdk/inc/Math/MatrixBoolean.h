/**
 * Based on
 * EasyGL library v0.1
 * (C)opyright 2002 by Dmitry S. Baikov
 *
 * @file Matrix.h
 * @author Dmitry S. Baikov
 */
#ifndef _ED_Math_MatrixBoolean_h_
#define _ED_Math_MatrixBoolean_h_
#include "Magick++.h"


#include "./_config.h"
#include "Util/CTAssert.h"
#include "Util/math_bmpMatrixMN.h"

#include "./Vector.h"
#include "./Box.h"


namespace Math 
{
	typedef Box<2, int> Rect2i;
	typedef Box<2, float> Rect2f;
	typedef Box<2, double> Rect2d;

	// ��������������� �������������
	class MatrixBoolean
	{
		typedef unsigned long WORD4;
		// ����������� ������� (� DWORD)
		long dimX, dimY, _xSize;
		// �������� �� ������ (� ����.)
		Math::Vec2i center;
		// ����. �������������� ��������� (��/����)
		double scale_factor;
		// ������ ���
		WORD4* data;
	public:
		MatrixBoolean(){data=NULL;dimX=dimY=0;_xSize=0;};
		virtual ~MatrixBoolean(){if(data) delete[] data;};
		MatrixBoolean(const MatrixBoolean& arg);
		MatrixBoolean& operator=(const MatrixBoolean& arg);
		bool operator()(int y, int x) const
		{
			WORD4* row = data + y*dimX;
			int x2 = x>>5; 
			x = x & 0x1F;
			WORD4* w = row + x2;
			WORD4 mask = 1<<x;
			return (mask&(*w)) != 0;
		}
		void setPix(int y, int x, bool val)
		{
			WORD4* row = data + y*dimX;
			int x2 = x>>5; 
			x = x & 0x1F;
			WORD4* w = row + x2;
			WORD4 mask = 1<<x;
			if(val)
				*w |= mask;
			else
				*w &= ~mask;
		}
		void invert();

		void set(
			int x, int y, 
			float scale_factor=1, 
			Math::Vec2i center = Math::Vec2i(0, 0)
			);
		bool open( 
			const char* filename, 
			bool invX=false, bool invY=false,
			float scale_factor=1,
			Math::Vec2i center = Math::Vec2i(0, 0)
			);
		bool openTga( 
			const char* filename, 
			bool invX=false, bool invY=false,
			float scale_factor=1,
			Math::Vec2i center = Math::Vec2i(0, 0)
			);
		bool Math::MatrixBoolean::save( 
			const char* filename) const;

		int xSize()const{return _xSize;};
		int ySize()const{return dimY;};
		/*/
		// ��������� ������ ��� ��������
		void set(
			const AcGePoint2dArray& polygon, 
			double scale_factor,
			const AcGePoint2d& center
			);
		/*/
		// �������� ����������� ��������� � �������� ������� ����������
		bool intersection(
			const Math::Vec2f& center,
			MatrixBoolean& opponent, 
			const Math::Vec2f& center_oponent,
			double& area) const;
		bool extract(
			const Math::Vec2f& center,
			MatrixBoolean& opponent, 
			const Math::Vec2f& center_oponent);
		bool join(
			const Math::Vec2f& center,
			MatrixBoolean& opponent, 
			const Math::Vec2f& center_oponent);
/*/
		// ���������� ���������
		void TestDraw(
			varentities& vardecl,	// �������� AcDb �������� ����������
			IDrawEnt* pDrawer,		// ��������� ���������
			const AcGeVector2d& bitmapoffset, 
			int mode = 0
			);
/*/

	public:
		// ��������� ������������� � �������
		void FillRect(const Math::Rect2i& p1);

		static inline double around( double arg)
		{
			double c = ceil(arg);
			if(arg+0.5<c) return c-1;
			return c;
		}
		enum enOperation
		{
			exclude = 0,
			add = 1,
		};
		bool operation(
			const Math::Vec2f& center,
			MatrixBoolean& opponent, 
			const Math::Vec2f& center_oponent, 
			enOperation op
			);
	};

}

inline Math::MatrixBoolean& Math::MatrixBoolean::operator=(const Math::MatrixBoolean& arg)
{
	// ����������� ������� (� DWORD)
	this->dimX = arg.dimX;
	this->dimY = arg.dimY;
	this->_xSize = arg._xSize;
	// �������� �� ������ (� ����.)
	this->center = arg.center;
	// ����. �������������� ��������� (��/����)
	this->scale_factor = arg.scale_factor;
	// ������ ���
	data = NULL;
	if( arg.data)
	{
		data = new WORD4[dimX*dimY];
		memcpy(data, arg.data, dimX*dimY*4);
	}
	return *this;
}

inline void Math::MatrixBoolean::set(
	int x, int y, 
	float scale_factor, 
	Math::Vec2i center)
{
	if(data)
		delete data;
	data = NULL;

	this->scale_factor = scale_factor;
	this->center = center;
	this->dimX = x;
	this->dimY = y;
	this->_xSize = x;
	this->dimX = ((this->dimX-1)>>5) + 1;
	data = new WORD4[dimX*dimY];
	memset(data, 0, dimX*dimY*4);
}
inline bool Math::MatrixBoolean::open( 
	const char* filename, 
	bool invX, bool invY,
	float scale_factor,
	Math::Vec2i center
	)
{
	try
	{
		invY = !invY;
		if(data)
			delete data;
		data = NULL;

		this->scale_factor = scale_factor;
		this->center = center;

		Magick::Image i;
		i.read(filename);
		Magick::Geometry sz = i.size();

		Magick::Blob blob; 
		i.magick( "RGB" ); // Set JPEG output format 
		i.write( &blob ); 

		this->dimX = sz.width();
		this->dimY = sz.height();
		this->_xSize = sz.width();
		this->dimX = ((this->dimX-1)>>5) + 1;
		data = new WORD4[dimX*dimY];
		memset(data, 0, dimX*dimY*4);

		int l = blob.length();
		math::rgb* f = (math::rgb*)blob.data();

		int x, y;
		for(y=0; y<this->dimY; y++)
		{
			for(x=0; x<this->_xSize; x++)
			{
				math::rgb& pix = f[y*this->_xSize + x];
				bool v = (pix.r>10 || pix.g>10 || pix.b>10);

				int _x = x, _y = y;
				if( invX) _x = this->_xSize - x - 1;
				if( invY) _y = this->dimY - y - 1;
				this->setPix(_y, _x, pix.r>10);
			}
		}
		return true;
	}
	catch(...)
	{
	}
	return false;

/*/
	math::bmpMatrix bmp;
	if( !math::loadBmp(filename, bmp, NULL, invX, invY))
		return false;

	this->dimX = bmp.xSize();
	this->dimY = bmp.ySize();
	this->_xSize = bmp.xSize();
	this->dimX = ((this->dimX-1)>>5) + 1;
	data = new WORD4[dimX*dimY];
	memset(data, 0, dimX*dimY*4);

	int x, y;
	for(y=0; y<bmp.ySize(); y++)
	{
		for(x=0; x<bmp.xSize(); x++)
		{
			this->setPix(y, x, bmp[y][x].r>10);
		}
	}
/*/
}

inline bool Math::MatrixBoolean::openTga( 
	const char* filename, 
	bool invX, bool invY,
	float scale_factor,
	Math::Vec2i center
	)
{
	if(data)
		delete data;
	data = NULL;

	this->scale_factor = scale_factor;
	this->center = center;

	math::tgaMatrix bmp;
	if( !math::loadTga(filename, bmp, invX, invY))
		return false;

	this->dimX = bmp.xSize();
	this->dimY = bmp.ySize();
	this->_xSize = bmp.xSize();
	this->dimX = ((this->dimX-1)>>5) + 1;
	data = new WORD4[dimX*dimY];
	memset(data, 0, dimX*dimY*4);

	int x, y;
	for(y=0; y<bmp.ySize(); y++)
	{
		for(x=0; x<bmp.xSize(); x++)
		{
			this->setPix(y, x, bmp[y][x].r>10);
		}
	}
	return true;
}

inline bool Math::MatrixBoolean::save( 
	const char* filename) const
{
	math::bmpMatrix resmask(this->xSize(), this->ySize());

	int x, y;
	for(y=0; y<this->ySize(); y++)
	{
		for(x=0; x<this->xSize(); x++)
		{
			bool a = (*this)(y, x);
			unsigned char f = 0;
			if( a) f = 255;
			resmask[y][x] = math::rgb(f, f, f);
		}
	}
	return math::saveBmp(filename, resmask);
}


inline Math::MatrixBoolean::MatrixBoolean(const Math::MatrixBoolean& arg)
{
	*this = arg;
}

/*/
// ��������� ������ ��� ��������
void Math::MatrixBoolean::set(
	const AcGePoint2dArray& polygon, 
	double scale_factor,
	const AcGePoint2d& center
	)
{
	delete[] data;
	this->scale_factor = scale_factor;
	// ��������� �� ������ ��� ����� ������� ������ ��������
	POINT* pv = new POINT[polygon.length()];
	for(int i=0; i<polygon.length(); i++)
	{
		double value = polygon[i].x/scale_factor;
		pv[i].x = (value>0) ? floor(value): ceil(value);
		value = polygon[i].y/scale_factor;
		pv[i].y = (value>0) ? floor(value): ceil(value);
	}
	// �������� ������
	HRGN region = CreatePolygonRgn(pv, polygon.length(), WINDING);
	delete pv;

	// �������� �������
	DWORD datalen = GetRegionData( region, 0, NULL);
	RGNDATA* rgdata = (RGNDATA*)(new char[datalen]);
	datalen = GetRegionData( region, datalen, rgdata);
	Math::Box2i* rects = (Math::Box2i*) rgdata->Buffer;
	WORD4 size = rgdata->rdh.nCount;

	// �������� ������
	this->dimX = rgdata->rdh.rcBound.max.x  - rgdata->rdh.rcBound.min.x;
	this->dimY = rgdata->rdh.rcBound.max.y - rgdata->rdh.rcBound.min.y;
	this->dimX = ((this->dimX-1)>>5) + 1;
	data = new DWORD[dimX*dimY];
	memset(data, 0, dimX*dimY*4);

	// �����
	this->center.cx = - rgdata->rdh.rcBound.min.x;
	this->center.cy = - rgdata->rdh.rcBound.min.y;

	// ������� ������
	for( i=0; i<size; i++)
	{
		Math::Box2i& rc = rects[i];
		FillRect(rc);
	}
	DeleteObject( region);
}
/*/
inline void Math::MatrixBoolean::invert()
{
	for(int i=0; i<dimX*dimY; i++)
		data[i] = ~data[i];
}

// �������� ����������� ��������� � �������� ������� ����������
inline bool Math::MatrixBoolean::intersection(
	const Math::Vec2f& cnt,
	Math::MatrixBoolean& opponent, 
	const Math::Vec2f& cnt_oponent,
	double& area) const
{
	const unsigned char SpeedTable[256] =
	{
		0x00, 0x01, 0x01, 0x02, 0x01, 0x02, 0x02, 0x03, 0x01, 0x02, 0x02, 0x03, 0x02, 0x03, 0x03, 0x04,
		0x01, 0x02, 0x02, 0x03, 0x02, 0x03, 0x03, 0x04, 0x02, 0x03, 0x03, 0x04, 0x03, 0x04, 0x04, 0x05,
		0x01, 0x02, 0x02, 0x03, 0x02, 0x03, 0x03, 0x04, 0x02, 0x03, 0x03, 0x04, 0x03, 0x04, 0x04, 0x05,
		0x02, 0x03, 0x03, 0x04, 0x03, 0x04, 0x04, 0x05, 0x03, 0x04, 0x04, 0x05, 0x04, 0x05, 0x05, 0x06,
		0x01, 0x02, 0x02, 0x03, 0x02, 0x03, 0x03, 0x04, 0x02, 0x03, 0x03, 0x04, 0x03, 0x04, 0x04, 0x05,
		0x02, 0x03, 0x03, 0x04, 0x03, 0x04, 0x04, 0x05, 0x03, 0x04, 0x04, 0x05, 0x04, 0x05, 0x05, 0x06,
		0x02, 0x03, 0x03, 0x04, 0x03, 0x04, 0x04, 0x05, 0x03, 0x04, 0x04, 0x05, 0x04, 0x05, 0x05, 0x06,
		0x03, 0x04, 0x04, 0x05, 0x04, 0x05, 0x05, 0x06, 0x04, 0x05, 0x05, 0x06, 0x05, 0x06, 0x06, 0x07,
		0x01, 0x02, 0x02, 0x03, 0x02, 0x03, 0x03, 0x04, 0x02, 0x03, 0x03, 0x04, 0x03, 0x04, 0x04, 0x05,
		0x02, 0x03, 0x03, 0x04, 0x03, 0x04, 0x04, 0x05, 0x03, 0x04, 0x04, 0x05, 0x04, 0x05, 0x05, 0x06,
		0x02, 0x03, 0x03, 0x04, 0x03, 0x04, 0x04, 0x05, 0x03, 0x04, 0x04, 0x05, 0x04, 0x05, 0x05, 0x06,
		0x03, 0x04, 0x04, 0x05, 0x04, 0x05, 0x05, 0x06, 0x04, 0x05, 0x05, 0x06, 0x05, 0x06, 0x06, 0x07,
		0x02, 0x03, 0x03, 0x04, 0x03, 0x04, 0x04, 0x05, 0x03, 0x04, 0x04, 0x05, 0x04, 0x05, 0x05, 0x06,
		0x03, 0x04, 0x04, 0x05, 0x04, 0x05, 0x05, 0x06, 0x04, 0x05, 0x05, 0x06, 0x05, 0x06, 0x06, 0x07,
		0x03, 0x04, 0x04, 0x05, 0x04, 0x05, 0x05, 0x06, 0x04, 0x05, 0x05, 0x06, 0x05, 0x06, 0x06, 0x07,
		0x04, 0x05, 0x05, 0x06, 0x05, 0x06, 0x06, 0x07, 0x05, 0x06, 0x06, 0x07, 0x06, 0x07, 0x07, 0x08
	};

//	abris::FunEvalTime ft(eval_intersection);
	// �� �����. ����. �������������� ��������� - ������
	if( scale_factor != opponent.scale_factor)
		return false;

	// ��������� ������������� ��������
	Math::Vec2i c; 
	c.x = (int)around(cnt.x/scale_factor) - this->center.x;
	c.y = (int)around(cnt.y/scale_factor) - this->center.y; 
	Math::Vec2i c_op; 
	c_op.x = (int)(cnt_oponent.x/scale_factor) - opponent.center.x;
	c_op.y = (int)(cnt_oponent.y/scale_factor) - opponent.center.y;
	c.x = c_op.x - c.x;
	c.y = c_op.y - c.y;

	Math::Box2i rect, rect_oponent;
	rect.min.x = 0;
	rect.min.y = 0;
	rect.max.x = dimX<<5;
	rect.max.y = dimY;
	rect_oponent.min.x   = c.x;
	rect_oponent.min.y    = c.y;
	rect_oponent.max.x  = c.x + (opponent.dimX<<5);
	rect_oponent.max.y = c.y + opponent.dimY;

	// ����������� 
	Math::Box2i result;
	if(!rect.intersect(rect_oponent, result)) 
		return false;

	// �������������� �������������
	Math::Vec2i& offset = c;
	// ����� ��� ��� ������
	int xbitoffset = c.x&0x1F;
	int xbitoffset_pr = 0x20 - xbitoffset;
	int y1 = result.min.y;
	int y2 = result.max.y;
	int x1 = result.min.x>>5;
	int x2 = ((result.max.x-1)>>5) + 1;
	if(x2>=dimX) x2 = dimX;

	int oy1 = result.min.y - offset.y;
	int oy2 = result.max.y - offset.y;
	int ox1 = -1;
	if(offset.x<0) ox1 = (-offset.x-1)>>5;
	int ox2 = opponent.dimX;

#ifdef TEST_AND
DWORD* newdata = new DWORD[dimX*dimY];
memset(newdata, 0, dimX*dimY*4);
#endif

// ��� �� ���� ->
	// ���� �� y
	register int int_area = 0;
	for(int y=y1, oy=oy1; y<y2; y++, oy++)
	{
		// ���� �� �
		WORD4* pray = data + y*dimX + x1;
		WORD4* pray_op = opponent.data + oy*opponent.dimX + ox1;
		for( int x = x1, ox=ox1; x<x2; x++, pray++)
		{
			// ������ ������ ��������:
			// ��� ����� �� ���� DWORD
			register WORD4 op = 0;
			if( ox >= 0 && ox < ox2 && xbitoffset)
			{
				// ���������� DWORD
				op = (*pray_op)>>xbitoffset_pr;
			}
			ox++;
			pray_op++;
			if( ox>=0 && ox<ox2)
			{
				// ������� DWORD
				op |= (*pray_op)<<xbitoffset;
			}
			// 
			register WORD4 dt = (*pray) & op;
			// ��� ��������
#ifdef TEST_AND
newdata[ x + y*dimX] = dt;
#endif
			// ��������� ����� ��� (�������� ��������� �������� 0xFFFFFFFF)
			// ����� ����� ���������� ���� 0x0FFFFFFF, 0x1FFFF, 0x3FF �� � �.�.
			//                         ��� 0xFFFFFFF0, 0xFFFF8000 � �.�.
			if( dt==0xFFFFFFFF) { int_area+=0x20;continue;}

			int_area += SpeedTable[dt&0xFF];
			dt >>= 8;
			int_area += SpeedTable[dt&0xFF];
			dt >>= 8;
			int_area += SpeedTable[dt&0xFF];
			dt >>= 8;
			int_area += SpeedTable[dt&0xFF];
			dt >>= 8;
/*/
			__asm
			{
				xor		eax,eax
				xchg	eax,[dt]
				mov		ebx,offset SpeedTable
				
				movzx	ecx,eax
				movzx	edx,[ecx+ebx]
				shr		eax,8

				movzx	ecx,eax
				add		dl,[ecx+ebx]
				shr		eax,8

				movzx	ecx,eax
				add		dl,[ecx+ebx]
				shr		eax,8

				movzx	ecx,eax
				add		dl,[ecx+ebx]

				add		[int_area],edx
			}
/*/
		}
	}
// ��� �� ���� ->

#ifdef TEST_AND
memcpy(data, newdata, dimX*dimY*4);
delete[] newdata;
#endif
	if( int_area==0) return false;
	area += int_area*scale_factor*scale_factor;
	return true;
}
//#pragma optimize("t",off)

inline bool Math::MatrixBoolean::extract(
	const Math::Vec2f& center,
	MatrixBoolean& opponent, 
	const Math::Vec2f& center_oponent)
{
	return operation(
		center,
		opponent,
		center_oponent, 
		exclude);
}
inline bool Math::MatrixBoolean::join(
	const Math::Vec2f& center,
	MatrixBoolean& opponent, 
	const Math::Vec2f& center_oponent)
{
	return operation(
		center,
		opponent,
		center_oponent, 
		add);
}

inline bool Math::MatrixBoolean::operation(
	const Math::Vec2f& cnt,
	Math::MatrixBoolean& opponent, 
	const Math::Vec2f& cnt_oponent,
	enOperation operation
	)
{
	// �� �����. ����. �������������� ��������� - ������
	if( scale_factor != opponent.scale_factor)
		return false;

	// ��������� ������������� ��������
	Math::Vec2i c; 
	c.x = (int)around(cnt.x/scale_factor) - this->center.x;
	c.y = (int)around(cnt.y/scale_factor) - this->center.y; 
	Math::Vec2i c_op; 
	c_op.x = (int)(cnt_oponent.x/scale_factor) - opponent.center.x;
	c_op.y = (int)(cnt_oponent.y/scale_factor) - opponent.center.y;
	c.x = c_op.x - c.x;
	c.y = c_op.y - c.y;

	Math::Box2i rect, rect_oponent;
	rect.min.x = 0;
	rect.min.y = 0;
	rect.max.x = dimX<<5;
	rect.max.y = dimY;
	rect_oponent.min.x   = c.x;
	rect_oponent.min.y    = c.y;
	rect_oponent.max.x  = c.x + (opponent.dimX<<5);
	rect_oponent.max.y = c.y + opponent.dimY;

	// ����������� 
	Math::Box2i result;
	if(!rect.intersect(rect_oponent, result)) 
		return false;

	// �������������� �������������
	Math::Vec2i& offset = c;
	// ����� ��� ��� ������
	int xbitoffset = c.x&0x1F;
	int xbitoffset_pr = 0x20 - xbitoffset;
	int y1 = result.min.y;
	int y2 = result.max.y;
	int x1 = result.min.x>>5;
	int x2 = ((result.max.x-1)>>5) + 1;
	if(x2>=dimX) x2 = dimX;

	int oy1 = result.min.y - offset.y;
	int oy2 = result.max.y - offset.y;
	int ox1 = -1;
	if(offset.x<0) ox1 = (-offset.x-1)>>5;
	int ox2 = opponent.dimX;


	// ���� �� y
	register int int_area = 0;
	for(int y=y1, oy=oy1; y<y2; y++, oy++)
	{
		// ���� �� �
		WORD4* pray = data + y*dimX + x1;
		WORD4* pray_op = opponent.data + oy*opponent.dimX + ox1;
		for( int x = x1, ox=ox1; x<x2; x++, pray++)
		{
			// ������ ������ ��������:
			// ��� ����� �� ���� DWORD
			register WORD4 op = 0;
			if( ox >= 0 && ox < ox2 && xbitoffset)
			{
				// ���������� DWORD
				op = (*pray_op)>>xbitoffset_pr;
			}
			ox++;
			pray_op++;
			if( ox>=0 && ox<ox2)
			{
				// ������� DWORD
				op |= (*pray_op)<<xbitoffset;
			}
			// 
			switch(operation)
			{
				case exclude:
					(*pray) &= ~op;
					break;
				case add:
					(*pray) |= op;
					break;
			}
		}
	}
	return true;
}


inline void Math::MatrixBoolean::FillRect(const Math::Rect2i& rect)
{
	int y1 = rect.min.y + this->center.y;
	int y2 = rect.max.y + this->center.y;
	int x1 = rect.min.x + this->center.x;
	int x2 = rect.max.x + this->center.x;
	WORD4 startmask = ~( (1<<(x1&0x1f)) - 1);
	WORD4 endmask   =    (1<<(x2&0x1f)) - 1;

	// ���� �� y
	x1 = x1>>5; x2 = x2>>5; 
	for(int y = y1; y<y2; y++)
	{
		// ���� �� �
		for( int x = x1; x<=x2; x++)
		{
			WORD4 add = WORD4(-1);
			if(x == x1) add &= startmask;
			if(x == x2) add &= endmask;
			// ��������
			data[ x + y*dimX] |= add;
		}
	}
}


#endif /* _ED_Math_MatrixBoolean_h_ */
