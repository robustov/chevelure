namespace Math
{

// ����
template <class CELL, class THIS>
bool LSystem2d<CELL, THIS>::Loop(int sizeX, int sizeY, int stepperframe)
{
	int maxframe = 400;
	work.init(sizeX, sizeY);

	this->Init();
	for(; this->frame<maxframe; this->frame++)
	{
		this->Frame();
		for(int s=0; s<stepperframe; s++)
		{
			this->Step();

			if( kbhit())
			{
				int input = getch();
				printf("press: %x\n", input);
				if(input==0x1b)
					return false;
				if(input==0xd)
					return true;
			}
		}
	}
}

// ������������
// � ���� ������ �������� ����� Init
template <class CELL, class THIS>
void LSystem2d<CELL, THIS>::Init()
{
	this->frame = 0;
	this->add_to_alives.clear();
	this->remove_from_alives.clear();

	this->add_to_alives.reserve(work.sizeY()*work.sizeX());
	this->remove_from_alives.reserve(work.sizeY()*work.sizeX());

	int x, y;
	for(y=0; y<work.sizeY(); y++)
	{
		for(x=0; x<work.sizeX(); x++)
		{
			work[y][x].Init(
				Math::Vec2i(x, y), 
				*(THIS*)this, 
				this->add_to_alives
				);
		}
	}

	// �������� add_to_alives � alives ������� remove_from_alives �� alives
	AddRemoveAlives();
	
}
// ����� ����
// � ���� ������ �������� ����� Frame
template <class CELL, class THIS>
void LSystem2d<CELL, THIS>::Frame()
{
	int x, y;
	for(y=0; y<work.sizeY(); y++)
	{
		for(x=0; x<work.sizeX(); x++)
		{
			work[y][x].Frame(
				Math::Vec2i(x, y), 
				*(THIS*)this, 
				this->add_to_alives
				);
		}
	}
	// �������� add_to_alives � alives ������� remove_from_alives �� alives
	AddRemoveAlives();
}
// ��� ���������
// ���������� ��� ����� ������ � �������� � ��� ����� step
template <class CELL, class THIS>
void LSystem2d<CELL, THIS>::Step()
{
	std::set<Math::Vec2i>::iterator it = alives.begin();
	for(;it != alives.end(); it++)
	{
		const Math::Vec2i& pt = *it;
		bool res = work[pt.y][pt.x].Step(
			pt, 
			*(THIS*)this, 
			this->add_to_alives
			);
		if( !res)
			remove_from_alives.push_back(pt);

	}
	// �������� add_to_alives � alives ������� remove_from_alives �� alives
	AddRemoveAlives();
}

// �����
template <class CELL, class THIS>
Math::Vec2i LSystem2d<CELL, THIS>::neibour(const Math::Vec2i& pt, int dir) const
{
	int invert = dir&0x1;
	Math::Vec2i res = pt;

	int axis = (dir>>1)&0x1;
	if(axis==0)
	{
		if(invert)
		{
			res.x--;
			if(res.x<0)
				res.x = 0;
		}
		else
		{
			res.x++;
			if(res.x>=work.sizeX())
				res.x = work.sizeX()-1;
		}
	}
	else
	{
		if(invert)
		{
			res.y--;
			if(res.y<0)
				res.y = 0;
		}
		else
		{
			res.y++;
			if(res.y>=work.sizeY())
				res.y = work.sizeY()-1;
		}
	}
	return res;
}

template <class CELL, class THIS>
const CELL& LSystem2d<CELL, THIS>::getCell(const Math::Vec2i& pt) const
{
	return work[pt.y][pt.x];
}
template <class CELL, class THIS>
CELL& LSystem2d<CELL, THIS>::getCell(const Math::Vec2i& pt)
{
	return work[pt.y][pt.x];
}

// �������� add_to_alives � alives ������� remove_from_alives �� alives
template <class CELL, class THIS>
void LSystem2d<CELL, THIS>::AddRemoveAlives()
{
//	printf("+%d-%d ", this->add_to_alives.size(), this->remove_from_alives.size());

	int i=0;
	for(i=0; i<(int)remove_from_alives.size(); i++)
	{
		Math::Vec2i pt = this->remove_from_alives[i];
		alives.erase(pt);
	}
	for(i=0; i<(int)add_to_alives.size(); i++)
	{
		Math::Vec2i pt = this->add_to_alives[i];
		alives.insert(pt);
	}

	this->add_to_alives.clear();
	this->remove_from_alives.clear();

}


}