#pragma once

#ifdef MAYA_API_VERSION
	#include <maya/MRampAttribute.h>
	#include <maya/MIntArray.h>
	#include <maya/MFloatArray.h>
#endif
#include "Math/Math.h"
#include <map>

namespace Math
{
	class Ramp
	{
	public:
		enum enInterpolation
		{
			None = 0, 
			Linear = 1,
			Smooth = 2,
			Spline = 3,
		};
		struct key
		{
			float val;
			enInterpolation itype;

			key(){};
			key( float val, enInterpolation itype)
			{
				this->val = val;
				this->itype = itype;
			}
		};
	public:
		typedef std::map<float, key> ramp_t;
		float defval;
		ramp_t ramp;
	public:
		Ramp()
		{
		}
		Ramp& operator=(const Ramp& arg)
		{
			defval = arg.defval;
			ramp = arg.ramp;
			return *this;
		}
		void clear(float defval = 0.f)
		{
			this->defval = defval;
			ramp.clear();
		}
		#ifdef MAYA_API_VERSION
		void set( MRampAttribute& src, float min = 0.f, float max = 1.f, float defval = 0.f)
		{
			this->defval = defval;
			MStatus stat;
			MIntArray twist_indexes;
			MFloatArray twist_positions;
			MFloatArray twist_values;
			MIntArray twist_interps;
			src.getEntries( twist_indexes, twist_positions, twist_values, twist_interps, &stat);

			ramp.clear();
			for( unsigned i=0; i<twist_positions.length(); i++)
			{
				float val = twist_values[i];
				int inter = twist_interps[i];
				val *= max-min;
				val += min;
				ramp[ twist_positions[i]] = key(val, (enInterpolation)inter);
			}
		}
		#endif

		#ifdef OCELLARIS

		void serializeOcs(
			const char* _name, 
			cls::IRender* render, 
			bool bSave
			)
		{
			std::string name = _name;

			if(bSave)
			{
				cls::PA<float> keys;
				cls::PA<float> value;
				cls::PA<int> interp;
				keys.reserve( (int)ramp.size());
				value.reserve( (int)ramp.size());
				interp.reserve( (int)ramp.size());

				ramp_t::iterator it = ramp.begin();
				for(;it != ramp.end(); it++)
				{
					keys.push_back(it->first);
					value.push_back(it->second.val);
					interp.push_back((int)it->second.itype);
				}
				render->Parameter( (name+"::defval").c_str(), defval);
				render->Parameter( (name+"::keys").c_str(), keys);
				render->Parameter( (name+"::value").c_str(), value);
				render->Parameter( (name+"::interp").c_str(), interp);
			}
			else
			{
				render->GetParameter( (name+"::defval").c_str(), defval);
				this->clear(defval);

				cls::PA<float> keys = render->GetParameter( (name+"::keys").c_str());
				cls::PA<float> value = render->GetParameter( (name+"::value").c_str());
				cls::PA<int> interp = render->GetParameter( (name+"::interp").c_str());
				int size = keys.size();
				size = __min(size, value.size());
				size = __min(size, interp.size());
				for(int i=0; i<size; i++)
				{
					ramp[keys[i] ].val = value[i];
					ramp[keys[i] ].itype = (enInterpolation)interp[i];
				}
			}
		}

		cls::PA<float> Ramp2Ocs()
		{
			std::vector<float> data;
			data.reserve(this->ramp.size()*3 + 1);

			data.push_back(this->defval);

			Math::Ramp::ramp_t::const_iterator it = this->ramp.begin();
			for( ; it!=this->ramp.end(); it++)
			{
				const Math::Ramp::key& k = it->second;
				data.push_back(it->first);
				data.push_back((float)k.itype);
				data.push_back(k.val);
			}
			cls::PA<float> P(cls::PI_CONSTANT, data);

			return P;
		}
		static Math::Ramp Ocs2Ramp(cls::PA<float>& P)
		{
			Math::Ramp ramp;
			if( P.empty()) return ramp;

			ramp.defval = P[0];

			int s = (P.size()-1)/3;
			for(int i=0, k=1; i<s; i++)
			{
				float t = P[k++];
				int it = (int)P[k++];
				float v = P[k++];
				ramp.ramp[t] = Math::Ramp::key(v, (Math::Ramp::enInterpolation)it);
			}
			return ramp;
		}
		#endif

		void addkey(float param, float val, enInterpolation inter)
		{
			ramp[ param] = key(val, (enInterpolation)inter);
		}
		Ramp& operator +=(float shift)
		{
			ramp_t::iterator it = ramp.begin();
			for( ; it!=ramp.end(); it++)
			{
				it->second.val += shift;
			}
			return *this;
		}
		Ramp& operator *=(float scale)
		{
			ramp_t::iterator it = ramp.begin();
			for( ; it!=ramp.end(); it++)
			{
				it->second.val *= scale;
			}
			return *this;
		}
		inline float getValue(float pos) const
		{
			ramp_t::const_iterator it = ramp.begin();
			if( it == ramp.end()) 
				return defval;
			float lastPos = it->first;
			float lastVal = it->second.val;
			enInterpolation lastInt = it->second.itype;
			if(pos <= lastPos) 
				return lastVal;
			it++;
			for( ; it!=ramp.end(); it++)
			{
				float curPos = it->first;
				float curVal = it->second.val;
				enInterpolation curInt = it->second.itype;
				if( pos <= curPos)
				{
					float factor = (pos-lastPos)/(curPos-lastPos);
					if( factor<0) factor=0;
					if( factor>1) factor=1;
					switch(lastInt)
					{
					case Linear:
						break;
					case Smooth: case Spline:
						factor = 0.5f*(-cos(factor*(float)M_PI)+1);
						break;
					case None:
						factor = 0;
						break;
					}
					float val = (1-factor)*lastVal + factor*curVal;
					return val;
				}
				lastPos = curPos;
				lastVal = curVal;
				lastInt = curInt;
			}
			return lastVal;
		}
	};

};

template<class Stream> inline
Stream& operator >> (Stream& out, Math::Ramp& v)
{
	out >> v.defval >> v.ramp;
	return out;
}

template<class Stream> inline
Stream& operator >> (Stream& out, Math::Ramp::key& v)
{
	out >> v.val;
	out >> *(int*)&v.itype;
	return out;
}
