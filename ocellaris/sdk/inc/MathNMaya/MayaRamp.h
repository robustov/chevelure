#pragma once

#ifdef MAYA_API_VERSION
	#include <maya/MRampAttribute.h>
	#include <maya/MFnNumericAttribute.h>
	#include <maya/MFnEnumAttribute.h>
	#include <maya/MFnCompoundAttribute.h>
#endif
#include "Math/Math.h"
#include <map>

namespace Math
{
	class Ramp
	{
	public:
		enum enInterpolation
		{
			None = 1, 
			Linear = 0,
			Smooth = 2,
			Spline = 3,
		};
		struct key
		{
			float val;
			enInterpolation itype;

			key(){};
			key( float val, enInterpolation itype)
			{
				this->val = val;
				this->itype = itype;
			}
		};
	public:
		typedef std::map<float, key> ramp_t;
		float defval;
		ramp_t ramp;
	public:
		Ramp()
		{
		}
		Ramp& operator=(const Ramp& arg)
		{
			defval = arg.defval;
			ramp = arg.ramp;
			return *this;
		}
		void clear(float defval = 0.f)
		{
			this->defval = defval;
			ramp.clear();
		}
		#ifdef MAYA_API_VERSION
		void set( MRampAttribute& src, float min = 0.f, float max = 1.f, float defval = 0.f)
		{
			this->defval = defval;
			MStatus stat;
			MIntArray twist_indexes;
			MFloatArray twist_positions;
			MFloatArray twist_values;
			MIntArray twist_interps;
			src.getEntries( twist_indexes, twist_positions, twist_values, twist_interps, &stat);

			ramp.clear();
			for( unsigned i=0; i<twist_positions.length(); i++)
			{
				float val = twist_values[i];
				int inter = twist_interps[i];
				val *= max-min;
				val += min;
				ramp[ twist_positions[i]] = key(val, (enInterpolation)inter);
			}
		}
		#endif
		void addkey(float param, float val, enInterpolation inter)
		{
			ramp[ param] = key(val, (enInterpolation)inter);
		}
		Ramp& operator +=(float shift)
		{
			ramp_t::iterator it = ramp.begin();
			for( ; it!=ramp.end(); it++)
			{
				it->second.val += shift;
			}
			return *this;
		}
		Ramp& operator *=(float scale)
		{
			ramp_t::iterator it = ramp.begin();
			for( ; it!=ramp.end(); it++)
			{
				it->second.val *= scale;
			}
			return *this;
		}
		inline float getValue(float pos) const
		{
			ramp_t::const_iterator it = ramp.begin();
			if( it == ramp.end()) 
				return defval;
			float lastPos = it->first;
			float lastVal = it->second.val;
			enInterpolation lastInt = it->second.itype;
			if(pos <= lastPos) 
				return lastVal;
			it++;
			for( ; it!=ramp.end(); it++)
			{
				float curPos = it->first;
				float curVal = it->second.val;
				enInterpolation curInt = it->second.itype;
				if( pos <= curPos)
				{
					float factor = (pos-lastPos)/(curPos-lastPos);
					if( factor<0) factor=0;
					if( factor>1) factor=1;
					switch(lastInt)
					{
					case Linear:
						break;
					case Smooth: case Spline:
						factor = 0.5f*(-cos(factor*(float)M_PI)+1);
						break;
					case None:
						factor = 0;
						break;
					}
					float val = (1-factor)*lastVal + factor*curVal;
					return val;
				}
				lastPos = curPos;
				lastVal = curVal;
				lastInt = curInt;
			}
			return lastVal;
		}
#		ifdef MAYA_API_VERSION
		static MObject CreateRampAttr(
			std::string fullname, 
			std::string shortname);

#		endif
	};

};

template<class Stream> inline
Stream& operator >> (Stream& out, Math::Ramp& v)
{
	out >> v.defval >> v.ramp;
	return out;
}

template<class Stream> inline
Stream& operator >> (Stream& out, Math::Ramp::key& v)
{
	out >> v.val;
	out >> *(int*)&v.itype;
	return out;
}


#ifdef MAYA_API_VERSION

inline MObject Math::Ramp::CreateRampAttr(
	std::string fullname, 
	std::string shortname)
{
	MObject rampchilds[3];
	MObject i_RampAttr;
	MStatus stat;
	MFnNumericAttribute nAttr;
	MFnEnumAttribute eAttr;
	MFnCompoundAttribute cAttr;
	std::string fn, sn;

	fn = fullname + "_Position";
	sn = shortname + std::string("p");
	rampchilds[0] = nAttr.create(fn.c_str(), sn.c_str(), MFnNumericData::kFloat, 0.0);
	float vmin = -1;
	float vmax = 1;
	
	nAttr.setSoftMin(vmin);
	nAttr.setSoftMax(vmax);
//	stat = addAttribute(i_HairAttr[1]);

	fn = fullname + std::string("_FloatValue");
	sn = shortname + std::string("fv");
	rampchilds[1] = nAttr.create(fn.c_str(), sn.c_str(), MFnNumericData::kFloat, 0.0);
	nAttr.setSoftMin(vmin);
	nAttr.setSoftMax(vmax);
//	stat = addAttribute(fv);

	fn = std::string(fullname) + std::string("_Interp");
	sn = std::string(shortname) + std::string("i");
	rampchilds[2] = eAttr.create(fn.c_str(), sn.c_str());
	eAttr.addField("None",   0);
	eAttr.addField("Linear", 1);
	eAttr.addField("Smooth", 2);
	eAttr.addField("Spline", 3);
//	stat = addAttribute(in);

	i_RampAttr = cAttr.create(fullname.c_str(), shortname.c_str());
	cAttr.addChild(rampchilds[0]);
	cAttr.addChild(rampchilds[1]);
	cAttr.addChild(rampchilds[2]);
	cAttr.setWritable(true);
	cAttr.setStorable(true);
	cAttr.setKeyable(false);
	cAttr.setArray(true);
	return i_RampAttr;
}

#endif
