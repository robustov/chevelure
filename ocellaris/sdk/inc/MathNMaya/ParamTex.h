#pragma once

// ParamTex<>, �������� ����� ���� ������ ���������
// ParamTex2d, ParamTex3d - ���������� �������� 2d � 3d ��������������

#include <vector>
#include <string>
#include <Math/Vector.h>
#include <Math/Matrix.h>

#ifdef MAYA_API_VERSION
	#include <Maya/MPlug.h>
#endif

#include "Math/matrixMxN.h"
#include "MathNMaya/IFFMatrix.h"

#pragma warning( disable : 4251)

namespace Math
{
	enum enJittingType
	{
		EJT_NONE	= 0, 
		EJT_MIDDLE	= 1, 
		EJT_MAX		= 2,
		EJT_MUL		= 3,

		EJT_EGZ			= 0x1000,
		EJT_MIDDLE_EGZ	= 0x1001,
		EJT_MAX_EGZ		= 0x1002,
		EJT_MULEGZ		= 0x1003,
	};

	//! �������� �� ����� (per hair) 2d �.�. ����������� ����� {u, v}
	//! float ��� �������� (�� ����� ����� ��� � ������)
	template <class T, enJittingType JT = EJT_MIDDLE_EGZ>
	struct ParamTex
	{
		float defValue;
		// �������
		float min, max;
		Math::matrixMxN<T>* matrix;
		// ����
		std::string filename;
		float factor, offset;
		// noise
		float jitter;				// 0 - ��������� 1 - ???
		// ��������� ������
		bool bNeedDelmatrix;

		ParamTex(){bNeedDelmatrix=false; matrix = NULL;reset();}
		~ParamTex(){reset();}

		float getMax();
		float getValue(float u, float v);
		float getValueAngle(float u, float v, bool bNoJitter=false);
		// ��� jittera
		float getValueSource(float u, float v);
		// �������
		float getAverageValueSource();
		// ������ jitter
		float getValueJitter();
		void reset()
		{
			if( bNeedDelmatrix) delete matrix; 
			matrix=0;
			min=max=defValue=factor=offset=0; 
			filename="";
			jitter=0;
		}
		ParamTex(const ParamTex<T, JT>& arg)
		{
			*this = arg;
		}
		ParamTex<T, JT>& operator=(const ParamTex<T, JT>& arg)
		{
			this->defValue			= arg.defValue;
			this->min				= arg.min;
			this->max				= arg.max;
			this->matrix			= NULL;
			if( arg.matrix)
				this->matrix = new Math::matrixMxN<T>( *arg.matrix);
			this->filename			= arg.filename;
			this->factor			= arg.factor;
			this->offset			= arg.offset;
			this->bNeedDelmatrix	= true;
			this->jitter			= arg.jitter;
			return *this;
		}
		// �������� ����� �������� �������
		void scale(float factor)
		{
			min *= factor;
			max *= factor;
			defValue *= factor;
		}
		#ifdef OCELLARIS
		void serializeOcs(
			const char* _name, 
			cls::IRender* render, 
			bool bSave
			);
		#endif
	};

	// ���������� ��������
	template <class T, enJittingType JT = EJT_MIDDLE_EGZ>
	struct ParamTex2d : public ParamTex<T, JT>
	{
		std::string name;
		std::string uvsetname;
		int uvsetnumber;			// �� �������������

		ParamTex2d()
		{
			uvsetnumber = -1;
		}
		ParamTex2d(const ParamTex2d<T, JT>& arg)
		{
			*this = arg;
		}

		template <class T1, enJittingType JT1>
		ParamTex2d<T, JT>& operator=(const ParamTex<T1, JT1>& arg)
		{
			this->ParamTex<T, JT>::operator=(arg);
	//		this->name = "";
			return *this;
		}
		ParamTex2d<T, JT>& operator=(const ParamTex2d<T, JT>& arg)
		{
			this->ParamTex<T, JT>::operator=(arg);
			this->name = arg.name;
			this->uvsetname = arg.uvsetname;
			this->uvsetnumber = arg.uvsetnumber;
			return *this;
		}
		#ifdef OCELLARIS
		void serializeOcs(
			const char* _name, 
			cls::IRender* render, 
			bool bSave
			);
		#endif

#ifdef MAYA_API_VERSION
		// ������ �� �����
		bool load(
			const MPlug& src, 
			int resX, int resY, 
			const char* uvsetname
			);
#endif
	};

	// �������� 3d (� �������)
	template <class T, enJittingType JT>
	struct ParamTex3d
	{
		std::string name;

		struct key
		{
			float param;
			int interpolation;
			ParamTex<T, JT> value;
		};
		std::vector<key> ramp;

		ParamTex3d(){};
		~ParamTex3d(){};

		float getValue(float u, float v, float p);
	};
}



template <class T, Math::enJittingType JT>
float Math::ParamTex<T, JT>::getMax()
{
	if(matrix && !matrix->empty())
		return max;
	return defValue;
}

// ��� jittera
template <class T, Math::enJittingType JT>
float Math::ParamTex<T, JT>::getValueSource(float u, float v)
{
	float d;
	if(this->matrix && !this->matrix->empty())
	{
		unsigned char c = this->matrix->interpolation((float)u, (float)v, false, false);
		d = (c/255.f)*(this->max-this->min)+this->min;
	}
	else
		d = this->defValue;
	return d;
}
// ������� ��� �������
template <class T, Math::enJittingType JT>
float Math::ParamTex<T, JT>::getAverageValueSource()
{
	if(this->matrix && !this->matrix->empty())
	{
		float d = this->matrix->average();
		d = d*(this->max-this->min)+this->min;
		return d;
	}
	else
	{
		return this->defValue;
	}
}

template <class T, Math::enJittingType JT>
float Math::ParamTex<T, JT>::getValue(float u, float v)
{
	enJittingType type = (enJittingType)( JT&(~EJT_EGZ));

	float d = getValueSource(u, v);

	float j = getValueJitter();
	if( type==EJT_NONE)
		return d;

	if( type==EJT_MUL)
		d = d*j;
	else
		d = d+j;
	if( (JT&EJT_EGZ) != 0)
		if( d<0) d = 0.00001f;

	return d;
}
template <class T, Math::enJittingType JT>
float Math::ParamTex<T, JT>::getValueJitter()
{
	enJittingType type = (enJittingType)( JT&(~EJT_EGZ));

	float j = rand()/(float)RAND_MAX;
	if(type==EJT_NONE)
		return 0;

	// j = [0,1]
	if( type==EJT_MIDDLE)
		// j = [-1, 1]
		j = 2*(j - 0.5f);
	else if( type==EJT_MAX)
		// j = [-1, 0]
		j = -j;

	if( type==EJT_MUL)
	{
		j = 1-this->jitter*j;
	}
	else
	{
		j = this->jitter*j;
	}
	return j;
}

template <class T, Math::enJittingType JT>
float Math::ParamTex<T, JT>::getValueAngle(float u, float v, bool bNoJitter)
{
	float j = 0;
	if(this->jitter!=0)
	{
		j = this->jitter*(rand()/(float)RAND_MAX - 0.5f);
	}
	if( bNoJitter)
		j = 0;
	if(this->matrix && !this->matrix->empty())
	{
		float c = (float)(this->matrix->angle_cubic_interpolation((float)u, (float)v, false, false));
		float d = (c/255.f)*(this->max-this->min)+this->min;
		return d+j;
	}
	return this->defValue+j;
}

template <class T, Math::enJittingType JT>
float Math::ParamTex3d<T, JT>::getValue(float u, float v, float p)
{
	unsigned vc = ramp.size();
	for(unsigned g=1; g<vc; g++)
	{
		if( (ramp[g-1].param <= f || g==1)&&
			(ramp[g].param >= f   || g==vc-1)
			)
		{
			float factor = (f-ramp[g-1].param)/(ramp[g].param-ramp[g-1].param);
			if( factor<0) factor=0;
			if( factor>1) factor=1;
			float res = (1-factor)*ramp[g-1].value.getValue(u, v) + factor*ramp[g].value.getValue(u, v);
			return pt;
		}
	}
}

#ifdef MAYA_API_VERSION

#include <maya/MRenderUtil.h>
#include <maya/MFloatMatrix.h>

// ������ �� �����
template <class T, Math::enJittingType JT> inline
bool Math::ParamTex2d<T, JT>::load(
	const MPlug& src, 
	int resX, int resY, 
	const char* uvsetname
	)
{
	Math::ParamTex2d<T, JT>& param2d = *this;
	param2d.reset();
	double d; 
	src.getValue(d);
	param2d.min = 0; 
	param2d.max = 1;
	param2d.defValue = (float)d;
	param2d.jitter = 0;
	param2d.uvsetname = uvsetname?uvsetname:"";

	MPlugArray plar;
	src.connectedTo(plar, true, false);
	if( plar.length()==1)
	{
		MPlug plug = plar[0];

		// FILE
		MObject shader = plug.node();

		MStatus stat;
		if( shader.hasFn(MFn::kFileTexture))
		{
			MString texturePath;
			MRenderUtil::exactFileTextureName(shader, texturePath);
			param2d.filename = texturePath.asChar();
		}

		if(!param2d.filename.empty()) 
		{
			MFnDependencyNode dn(shader);
			double alphaGain;
			dn.findPlug("alphaGain").getValue(alphaGain);
			double alphaOffset;
			dn.findPlug("alphaOffset").getValue(alphaOffset);
			param2d.offset = (float)alphaOffset;
			param2d.factor = (float)alphaGain;
		}
		if( plug.node().hasFn( MFn::kTexture2d))
		{
			MFloatArray uCoords(resX*resY), vCoords(resX*resY);
			// read from SHADER
			for(int u=0; u<resX; u++)
				for(int v=0; v<resY; v++)
				{
					int ind = u*resY + v;
					uCoords[ind] = (float)(u+0.5)/resX;
					vCoords[ind] = (float)(v+0.5)/resY;
				}

			MFloatMatrix cameraMat;
			MFloatVectorArray colors, transps;
			bool res;
			res = MRenderUtil::sampleShadingNetwork( 
					plug.name(), resX*resY,
					false, false, cameraMat,
					NULL,
					&uCoords, &vCoords,
					NULL, NULL, NULL, NULL, NULL,
					colors, transps );
			if( colors.length()!=resX*resY)
				res = false;
			if(!res)
				return false;

			if( res && colors.length()==resX*resY)
			{
				param2d.max = -1e6; 
				param2d.min = 1e6;
				for(int i=0; i<(int)colors.length(); i++)
				{
					float d = (colors[i][0]+colors[i][1]+colors[i][2])/3;
					param2d.max = __max(param2d.max, d);
					param2d.min = __min(param2d.min, d);
				}

				if( param2d.max == param2d.min) 
				{
					param2d.defValue = param2d.max;
					param2d.min = param2d.defValue-0.5f; param2d.max = param2d.defValue+0.5f;
					return true;
				}

				param2d.matrix = new Math::matrixMxN<unsigned char>();
				param2d.matrix->init(resX, resY);
				param2d.bNeedDelmatrix = true;

				for(int x=0, i=0; x<resX; x++)
				{
					for(int y=0; y<resY; y++, i++)
					{
						float d = (colors[i][0]+colors[i][1]+colors[i][2])/3;
						unsigned int f = (unsigned int)( 255.f*( (d - param2d.min)/(param2d.max - param2d.min)));
						f = __max( 0, __min(255, f));
						unsigned char c = (unsigned char)f;
						(*param2d.matrix)[y][x] = c;

	//					printf("[%d][%d]=%d\n", x, y, (int)c);
					}
				}
			}
		}
	}
	return true;
}
#endif


#include "IFFMatrix.h"

template <class STREAM, class T, Math::enJittingType JT>
STREAM& operator>>(STREAM& serializer, Math::ParamTex<T, JT> &v)
{
	int version = 1;
	serializer >> version;
	if(version==1)
	{
		serializer >> v.defValue;
		serializer >> v.min >> v.max;
		serializer >> v.filename;
		serializer >> v.factor >> v.offset;
		serializer >> v.jitter;

		if(serializer.isLoading())
		{
			// load texture
			bool bMatrix;
			serializer >> bMatrix;
			if(bMatrix)
			{
				Math::matrixMxN<T>* matrix = new Math::matrixMxN<T>;
				serializer >> *matrix;
				v.bNeedDelmatrix = true;
#ifndef CHEVELUREDEMO
				v.matrix = matrix;
#else
#ifndef HAIRMATH_EXPORTS
				v.matrix = matrix;
#endif
#endif
			}
			else if(!v.filename.empty())
			{
#ifndef CHEVELUREDEMO
				// �� �����
				v.matrix = new Math::matrixMxN<T>;
				Math::LoadMatrixFromIFF(v.filename.c_str(), *v.matrix);
				v.min = v.offset;
				v.max = v.offset + v.factor;
#endif
				v.bNeedDelmatrix = true;
			}
		}
		else
		{
			// save texture
			bool bMatrix = v.matrix && !v.matrix->empty() && v.filename.empty();
			serializer >> bMatrix;
			if(bMatrix)
				serializer >> *v.matrix;
		}
	}
	else
	{
		fprintf(stderr, "Hair::ParamTex loading failed\n");
	}
	return serializer;
}

#ifdef OCELLARIS
template <class T, Math::enJittingType JT> inline 
void Math::ParamTex<T, JT>::serializeOcs(
	const char* _name, 
	cls::IRender* render, 
	bool bSave
	)
{
	std::string name = _name;

	SERIALIZEOCS(defValue);
	SERIALIZEOCS(min);
	SERIALIZEOCS(max);
	SERIALIZEOCS(filename);
	SERIALIZEOCS(factor);
	SERIALIZEOCS(offset);
	SERIALIZEOCS(jitter);

	if(bSave)
	{
		if( matrix && !matrix->empty() && filename.empty())
		{
			matrix->serializeOcs((name+"::matrix").c_str(), render, bSave);
		}
	}
	else
	{
		if( !filename.empty())
		{
	#ifndef CHEVELUREDEMO
			// �� �����
			matrix = new Math::matrixMxN<T>;
			Math::LoadMatrixFromIFF(filename.c_str(), *matrix);
			min = offset;
			max = offset + factor;
	#endif
			bNeedDelmatrix = true;
		}
		else
		{
			matrix = new Math::matrixMxN<T>;
			matrix->serializeOcs((name+"::matrix").c_str(), render, bSave);
			bNeedDelmatrix = true;
		}
		if( matrix && matrix->empty())
		{
			delete matrix;
			matrix = NULL;
		}
	}
}


template <class T, Math::enJittingType JT>
void Math::ParamTex2d<T, JT>::serializeOcs(
	const char* _name, 
	cls::IRender* render, 
	bool bSave
	)
{
	std::string name = _name;

	Math::ParamTex<T, JT>::serializeOcs(_name, render, bSave);
	SERIALIZEOCS(name);
	SERIALIZEOCS(uvsetname);
}
#endif

template <class STREAM, class T, Math::enJittingType JT>
STREAM& operator>>(STREAM& serializer, Math::ParamTex2d<T, JT> &v)
{
	int version = 1;
	serializer >> version;
	if(version>=1)
	{
		serializer >> v.name;
		serializer >> v.uvsetname;
		serializer >> *(Math::ParamTex<T, JT>*)&v;
	}
	return serializer;
}

template <class STREAM, class T, Math::enJittingType JT>
STREAM& operator>>(STREAM& serializer, Math::ParamTex3d<T, JT> &v)
{
	serializer >> v.name;
	serializer >> ramp;
	return serializer;
}
