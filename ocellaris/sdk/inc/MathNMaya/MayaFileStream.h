#pragma once

#include <maya/MIOStream.h>
#if MAYA_API_VERSION >= 850
	#include <maya/MStreamUtils.h>
#endif
#include "Util\Stream.h"
#include "Util\STLStream.h"

/// ��������� �������, ������� ������������� ������
class MayaFileStream : public Util::Stream
{
public:
	std::istream* in;
	std::ostream* out;

	bool bSaveMode;
	bool bAscii;
public:
	MayaFileStream(std::istream& in, bool bAscii=false)
	{
		this->in = &in;
		bSaveMode = false;
		this->bAscii = bAscii;
	}
	MayaFileStream(std::ostream& out, bool bAscii=false)
	{
		this->out = &out;
		bSaveMode = true;
		this->bAscii = bAscii;
	}
	
	~MayaFileStream()
	{
	}

	/// ����� ������
	virtual bool isLoading(){return !bSaveMode;}
	virtual bool isSaving(){return bSaveMode;}

	/// ������������
	virtual Stream &serialize(void *data, unsigned size)
	{
		if( !bAscii)
		{

#if MAYA_API_VERSION >= 850
			if(bSaveMode)
			{
				for(unsigned i=0; i<size; i++)
					MStreamUtils::writeChar(*out, ((char*)data)[i], true);
			}
			else
			{
				for(unsigned i=0; i<size; i++)
					MStreamUtils::readChar(*in, ((char*)data)[i], true);
			}
#else

			if(bSaveMode)
				out->write((char*)data, size);
			else
				in->read((char*)data, size);

#endif

		}
		else
		{
			// ascii
			unsigned char buf[2];
			for(unsigned i=0; i<size; i++)
			{
				if(bSaveMode)
				{
					unsigned char c = ((unsigned char*)data)[i];
					buf[0] = (c&0xf)+'A';
					buf[1] = ((c&0xf0)>>4)+'a';
					(*out)<<buf[0]<<buf[1];
//					out->write((char*)data, 2);
				}
				else
				{
					unsigned char c = 0;
					(*in)>>buf[0]>>buf[1];
//					in->read((char*)data, 2);
					c  = buf[0]-'A';
					c |= (buf[1]-'A')<<4;
					((unsigned char*)data)[i] = c;
				}
			}
		}
		return *this;
	}
};

