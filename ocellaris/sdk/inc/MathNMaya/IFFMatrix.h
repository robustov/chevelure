#pragma once
#include "Math/Math.h"
#include "Math/matrixMxN.h"

namespace Math
{
ED_MATH_EXTERN bool ED_MATH_API LoadMatrixFromIFF(
	const char* filename, 
	Math::matrixMxN<unsigned char>& matrix);
}
