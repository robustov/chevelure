#include <time.h>
#include "Util/misc_create_directory.h"

namespace Alfred
{
inline std::string correctAlfredString(const char* src);

inline std::string tagToString(const char* tagname, const char* value, bool bBrackets=true);

inline std::string gettabs(int tabs)
{
	std::string out;
	for(int i=0; i<tabs; i++) out += "\t";
	return out;
}

inline Cmd::Cmd()
{
	bRemote=false;			// Cmd ��� RemoteCmd
	atmost = -1;
	atleast = -1;
}
inline Cmd::Cmd(const char* cmd, bool bRemote)
{
	this->launch_expr = cmd;
	this->bRemote=bRemote;			// Cmd ��� RemoteCmd
	atmost = -1;
	atleast = -1;
}

inline std::string Cmd::ToString(int tabs)
{
	char buf[1224];
	std::string out;
	out += gettabs(tabs);
	out += "\t";

	if(!bRemote)
		out += "Cmd {";
	else
		out += "RemoteCmd {";
	out += correctAlfredString( launch_expr.c_str());
	out += "} ";

	if(!msg.empty())
		out += tagToString("-msg", msg.c_str());
	if(!service.empty())
		out += tagToString("-service", service.c_str());
	if(atmost!=-1)
		out += tagToString("-atmost", _itoa(atmost, buf, 10), false);
	if(atleast!=-1)
		out += tagToString("-atleast", _itoa(atleast, buf, 10), false);

	if(!tags.empty())
		out += tagToString("-tags", tags.c_str());

	if(!expand.empty())
		out += tagToString("-expand", expand.c_str(), false);
	
	out += "\n";
	return out;
};

inline Assign::Assign()
{
}
inline Assign::Assign(const char* name, const char* text)
{
	this->name = name;
	if( text)
		this->text = text;
}

inline std::string Assign::ToString(int tabs)
{
	std::string out;
	out += gettabs(tabs);
	out += "\t";
	out += "Assign ";
	out += name;
	out += " {";
	out += text;
	out += "}";
	out += "\n";
	return out;
}

// ������� RemoteCmd
inline Cmd LocalCmd(const char* cmdstr)
{
	Cmd cmd;
	cmd.bRemote = false;
	cmd.launch_expr = cmdstr;
	return cmd;
}
inline Cmd RemoteCmd(const char* cmd, ...)
{
	va_list argList; va_start(argList, cmd);
	char sbuf[1024];
	_vsnprintf(sbuf, 1024, cmd, argList);
	va_end(argList);

	Cmd pThis;
	pThis.launch_expr = sbuf;
	pThis.bRemote = true;
	return pThis;
}
inline Cmd RemoteCmd(const std::string& cmd)
{
	Cmd pThis;
	pThis.launch_expr = cmd;
	pThis.bRemote = true;
	return pThis;
}

inline Cmd CmdDeleteFile(const char* filename)
{
	Cmd cmd;
	cmd.bRemote = false;
	cmd.launch_expr = "Alfred";

	cmd.msg = "File delete \"";
	cmd.msg += filename;
	cmd.msg += "\"";
	return cmd;
}


inline Alfred::Task Instance(const char* name)
{
	Alfred::Task task(name);
	task.bInstance = true;
	return task;
}
inline Alfred::Task Instance(const Alfred::Task& src)
{
	Alfred::Task task(src.title.c_str());
	task.bInstance = true;
	return task;
}


// Task
inline Task::Task(const char* text, ...)
{
	va_list argList; va_start(argList, text);
	char sbuf[1024];
	_vsnprintf(sbuf, 1024, text, argList);
	va_end(argList);

	this->title = sbuf;
	bInstance = false;
}
inline std::string Task::ToString(int tabs)
{
	std::string out;
	out += gettabs(tabs);
	if( bInstance)
	{
		out += "Instance {";
		out += correctAlfredString(title.c_str());
		out += "}\n";
		return out;
	}

	out += "Task ";
	out += tagToString("-title", title.c_str());

	out += "-subtasks {\n";
	/*/
	std::list<std::string>::iterator insit;
	for( insit = instance.begin(); insit!=instance.end(); insit++)
	{
		out += gettabs(tabs+2);
		out += "Instance {";
		out += *insit;
		out += "}\n";
	}
	/*/

	// subtasks
	std::list<Task>::iterator taskit;
	for( taskit = subtasks.begin(); taskit!=subtasks.end(); taskit++)
	{
		Task& t = *taskit;
		out += t.ToString(tabs+1);
	}

	// cmds
	out += gettabs(tabs+1);
	out += "} -cmds {\n";
	std::list<Cmd>::iterator cmdit;
	for( cmdit = cmds.begin(); cmdit!=cmds.end(); cmdit++)
	{
		Cmd& cmd = *cmdit;
		out += cmd.ToString(tabs+1);
	}

	// cleanup
	out += gettabs(tabs+1);
	out += "} -cleanup {\n";
	for( cmdit = cleanup.begin(); cmdit!=cleanup.end(); cmdit++)
	{
		Cmd& cmd = *cmdit;
		out += cmd.ToString(tabs+1);
	}

	// chaser
	out += gettabs(tabs+1);
	out += "} -chaser {\n";
	if( !chaser.empty())
	{
		out += gettabs(tabs+2);
		out += correctAlfredString(chaser.c_str());
		out += "\n";
	}

	// preview
	out += gettabs(tabs+1);
	out += "} -preview {\n";
	if( !preview.empty())
	{
		out += gettabs(tabs+2);
		out += correctAlfredString(preview.c_str());
		out += "\n";
	}

	out += gettabs(tabs+1);
	out += "}\n";
	return out;
}
inline void Task::AddTask(Task& task)
{
	subtasks.push_back(task);
}

inline void Task::AddCmd(Cmd& cmd)
{
	cmds.push_back(cmd);
}
inline void Task::AddInstance(Task& task)
{
	Task insttask(task.title.c_str());
	insttask.bInstance = true;
	subtasks.push_back(insttask);
}




// Job
inline Job::Job(const char* title)
{
	clear();
	this->title = title;
}
inline std::string Job::ToString()
{
	std::string out;
	char buf[1224];
	std::list<Assign>::iterator assit;
	std::list<Cmd>::iterator cmdit;
	std::list<Task>::iterator taskit;

	out += "Job ";
	out += tagToString("-title",	title.c_str());
	out += tagToString("-comment", comment.c_str());
	out += tagToString("-envkey",	envkey.c_str());
	out += tagToString("-dirmaps", dirmaps.c_str());
	out += tagToString("-service", service.c_str());
	out += tagToString("-tags",	tags.c_str());

	if( atleast!=-1)
		out += tagToString("-atleast", _itoa(atleast, buf, 10), false);
	if( atmost!=-1)
		out += tagToString("-atmost",	_itoa(atmost, buf, 10), false);

	out += tagToString("-whendone",	whendone.c_str());
	out += tagToString("-whenerror", whenerror.c_str());
	out += tagToString("-crews", crews.c_str());
	out += tagToString("-metadata", metadata.c_str());
	if( pbias!=-1)
		out += tagToString("-pbias",	_itoa(pbias, buf, 10), false);

	std::map<std::string, std::string>::iterator addit = additionaloptions.begin();
	for(;addit != additionaloptions.end(); addit++)
	{
		out += tagToString(addit->first.c_str(), addit->second.c_str(), true);
	}

	out += "-init {\n";
	for( assit = init.begin(); assit!=init.end(); assit++)
	{
		Assign& ass = *assit;
//		out += "\t";
		out += ass.ToString(0);
	}

	out += "} -subtasks {\n";
	for( taskit = subtasks.begin(); taskit!=subtasks.end(); taskit++)
	{
		Task& task = *taskit;
		out += task.ToString(1);
	}

	out += "} -cleanup {\n";
	for( cmdit = cleanup.begin(); cmdit!=cleanup.end(); cmdit++)
	{
		Cmd& cmd = *cmdit;
		out += cmd.ToString(0);
	}
	out += "}\n";

	return out;
}
inline void Job::AddSubtask(Task& task)
{
	subtasks.push_back(task);
}
inline void Job::AddCleanup(Cmd& cmd)
{
	cleanup.push_back(cmd);
}
inline void Job::clear()
{
	pbias = 0;
	atleast = -1;
	atmost = -1;

	title="";		// -title {untitled (mtor job)}
	comment="";	// -comment {#Created for mitka by mtor 6.5.1, RAT 6.5.1 (Sep  8 2005 11:54:01)}
	envkey="";
	dirmaps="";
	service="";
	tags="";

	// ???
	whendone="";
	whenerror="";
	crews="";
	metadata="";

	init.clear();
	subtasks.clear();
	cleanup.clear();

	additionaloptions.clear();

}

inline std::string correctAlfredString(const char* src)
{
	std::string out = src;
	for( int i=0; i<(int)out.size(); i++)
	{
		if( out[i]=='"')
		{
			out.insert(i++, "\\");
		}
		if( out[i]=='%')
		{
			// ��� �� �� ���� �� ������, ������
//			out.insert(i++, "%");
		}
	}
	return out;
}
inline std::string tagToString(const char* tagname, const char* value, bool bBrackets)
{
	std::string out;
	out += tagname;
	out += " ";
	if( bBrackets)
		out += "{";
	out += correctAlfredString(value);
	if( bBrackets)
		out += "}";
	out += " ";
	return out;
}


// ����� ��� ����� ������
inline std::string unequalJobName(const char* dir)
{
	std::string alffile = dir;
	alffile += "_";
	time_t t = time(NULL);
	struct tm* newTime = localtime( &t);
	alffile+=asctime(newTime);
	alffile = alffile.substr(0, alffile.size()-1);
	for(int i=4; i<(int)alffile.size(); i++)
	{
		if(alffile[i]==' ') alffile[i]='_';
		if(alffile[i]==':') alffile[i]='.';
	}
	alffile+=".alf";
	return alffile;

}

inline bool saveAlfredFile(const char* filename, Job& job)
{
	std::string out;
	out += "##AlfredToDo 3.0\n";

	out += job.ToString();

	Util::create_directory_for_file(filename);
	FILE* file = fopen(filename, "wt");
	if( !file) return false;

	fwrite( out.c_str(), out.size(), 1, file);
	fclose(file);
	return true;
}

inline bool startAlfred(const char* filename, bool bPaused)
{
	std::string rat_tree_env = getenv("RATTREE");
	rat_tree_env += "\\bin";

	// �� ��������:
	/*/
	char cmd[1024];
	_snprintf(cmd, 1024, "\"%s\\alfcmdline.exe\" %s\"%s\"", rat_tree_env.c_str(), bPaused?"-paused ":"", filename);
	printf( cmd); printf( "\n");
	int res = system(cmd);
	/*/

	char cmd[1024];
	_snprintf(cmd, 1024, "alfcmdline.exe %s\"%s\"", bPaused?"-paused ":"", filename);
	printf( cmd); printf( "\n");
	int res = system(cmd);

	return res!=-1;
}

}
