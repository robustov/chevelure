#pragma once
#include <vector>
#include <string>
#include <Nb30.h>
#include "Iphlpapi.h"

namespace Net
{
	/*/
	inline bool GetOSVersion(LPDWORD dwPlatformID, LPDWORD dwMajorVersion, LPDWORD dwMinorVersion)
	{
		// Try calling GetVersionEx using the OSVERSIONINFOEX structure.
		// If that fails, try using the OSVERSIONINFO structure.
		OSVERSIONINFOEX osvi;
		ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
		osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);

		if(!GetVersionEx((OSVERSIONINFO*) &osvi))
		{
			// If OSVERSIONINFOEX doesn't work, try OSVERSIONINFO.
			osvi.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);

			if(!GetVersionEx((OSVERSIONINFO *)&osvi)) 
				return false;
		}

		// Set the values
		*dwMajorVersion = osvi.dwMajorVersion;
		*dwMinorVersion = osvi.dwMinorVersion;
		*dwPlatformID = osvi.dwPlatformId;

		return true;
	}

	inline bool IsOSPlatformNT(void)
	{
		bool bWindowsMinNT = false;

		DWORD dwMajorVersion;
		DWORD dwMinorVersion;
		DWORD dwPlatformID;

		GetOSVersion(&dwPlatformID, &dwMajorVersion, &dwMinorVersion);

		switch (dwPlatformID)
		{
			// Win 98/ME
		case VER_PLATFORM_WIN32_WINDOWS:
			{
				bWindowsMinNT = false;
			}
			break;

			// NT or better
		case VER_PLATFORM_WIN32_NT:
			{
				bWindowsMinNT = true;
			}
			break;
		}

		return bWindowsMinNT;
	}

	inline BOOL ResetAdapter(UCHAR lana)
	{
		NCB ncb;

		::ZeroMemory(&ncb, sizeof(NCB));

		ncb.ncb_command = NCBRESET;
		ncb.ncb_lsn = 0x00;
		ncb.ncb_callname[0] = 20;
		ncb.ncb_callname[2] = 100;
		ncb.ncb_lana_num = lana;

		UCHAR uRet = Netbios(&ncb);
		if(uRet == NRC_GOODRET)
			return TRUE;

		::SetLastError(uRet);
		return FALSE;
	}

	const long lNAME_SIZE = 1024;

	inline BOOL GetAdapterStatus(UCHAR lana, UCHAR uMACAddress[])
	{
		NCB ncb;
		BYTE buffer[lNAME_SIZE+2];
		ADAPTER_STATUS *pAS;

		::ZeroMemory(&ncb, sizeof(NCB));
		::ZeroMemory(buffer, lNAME_SIZE+2);

		ncb.ncb_command = NCBASTAT;
		ncb.ncb_buffer = buffer;
		ncb.ncb_length = lNAME_SIZE;
		ncb.ncb_callname[0] = '*';
		ncb.ncb_lana_num = lana;

		UCHAR uRet = Netbios(&ncb);
		if(uRet == NRC_GOODRET)
		{
			pAS = (ADAPTER_STATUS *)buffer;
			memcpy(uMACAddress, &(pAS->adapter_address), 6);
			return TRUE;
		}

		::SetLastError(uRet);
		return FALSE;
	}
	/*/

	inline bool GetMacAddress(std::vector< std::string>& macaddress)
	{
		macaddress.clear();

		// Allocate information for NIC
		DWORD dwBufLen = 0;
		DWORD dwStatus = GetAdaptersInfo(NULL,&dwBufLen);
		std::vector<char> buf(dwBufLen);
		dwStatus = GetAdaptersInfo( (PIP_ADAPTER_INFO)&buf[0], &dwBufLen);
		if( dwStatus == ERROR_SUCCESS)
		{
			PIP_ADAPTER_INFO pAdapterInfo = (PIP_ADAPTER_INFO)&buf[0];
			for( ; pAdapterInfo; )
			{
				UCHAR* uMACAddress = (UCHAR*)pAdapterInfo->Address;
				char szBuffer[24];
				sprintf(szBuffer, "%02X%02X%02X%02X%02X%02X", (int)uMACAddress[0], (int)uMACAddress[1], (int)uMACAddress[2], (int)uMACAddress[3], (int)uMACAddress[4], (int)uMACAddress[5]);
				macaddress.push_back(szBuffer);
				pAdapterInfo = pAdapterInfo->Next;
			}
		}
		else
		{
			printf("GetAdaptersInfo failed. Error = 0x%x\n", dwStatus);
		}
/*/
		NCB ncb;
		LANA_ENUM lenum;
		UCHAR uMACAddress[6];
		int i;
		int iLEnumCnt = 3;
		UCHAR uRASAdapterAddress[6] = { 0x44, 0x45, 0x53, 0x54, 0x0, 0x0 }; // DEST

		::ZeroMemory(&ncb, sizeof(NCB));
		::ZeroMemory(&lenum, sizeof(lenum));

		// Helper function using GetVersionEx()
		if(IsOSPlatformNT())
		{
			// NT or better supports adapter enumeration
			ncb.ncb_command = NCBENUM;
			ncb.ncb_buffer = (UCHAR *)&lenum;
			ncb.ncb_length = sizeof(lenum);

			UCHAR uRet = Netbios(&ncb);

			if(uRet == NRC_GOODRET) 
			{
				iLEnumCnt = (int)lenum.length;
				printf("Netbios return %d elements\n", iLEnumCnt);
			}
			else
			{
				printf("Netbios failed\n");
			}
		}
		else
		{
			// Can't do enumeration, so we'll pretend and query the first adapter only
			lenum.length = (UCHAR)iLEnumCnt;

			for(i=0; i<iLEnumCnt; ++i)
				lenum.lana[i] = (UCHAR)i;
		}

		for(i = 0; i < iLEnumCnt; ++i) 
		{
			if(ResetAdapter(lenum.lana[i]))
			{
				if (GetAdapterStatus(lenum.lana[i], uMACAddress))
				{
					// ignore RAS adapter address, we want MAC
					if (memcmp(uMACAddress, uRASAdapterAddress, 6) != 0)
					{
						char szBuffer[24];
						sprintf(szBuffer, "%02X%02X%02X%02X%02X%02X", (int)uMACAddress[0], (int)uMACAddress[1], (int)uMACAddress[2], (int)uMACAddress[3], (int)uMACAddress[4], (int)uMACAddress[5]);
						macaddress.push_back(szBuffer);
					}
				}
			}
			else
			{
				printf("ResetAdapter failed for %d\n", i);
			}
		}
/*/
		return !macaddress.empty();
	}
}