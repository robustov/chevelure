// IPrman.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "mathNpixar/IPrman.h"
#include "mathNpixar/IPrman_impl.h"
#include "mathNpixar/ISlo.h"
#include "mathNpixar/ISlo_impl.h"
#include "mathNpixar/IPointCloud_impl.h"

// librib.lib libzip.lib libprmutil.lib libtarget.lib Ws2_32.lib Netapi32.lib
// libircmt.lib libjpeg.lib liblkm.lib libmmt.lib libprmutil.lib librib.lib libsloargs.lib libtarget.lib libzip.lib Ws2_32.lib Netapi32.lib

#ifdef _MSC_VER

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}

    return TRUE;
}
#endif

#ifdef WIN32
    #define EXPORT_API __declspec(dllexport)
#else
    #define EXPORT_API
#endif


Prman_Impl impl;
extern "C" EXPORT_API IPrman* getIPrman(void)
{
	return &impl;
}
Slo_Impl sloimpl;
extern "C" EXPORT_API ISlo* getISlo(void)
{
	return &sloimpl;
}

// ������ � 13.5 ����������
#ifdef RI_POINTCLOUD

IPointCloud_impl pointcloud_impl;
extern "C" IPointCloud* getIPointCloud(void)
{
	return &pointcloud_impl;
}

#endif

