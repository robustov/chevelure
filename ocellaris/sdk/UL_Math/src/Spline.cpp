//#include "stdafx.h"
#define ED_MATH_INTERNALS
#define ED_MATH_IMPLEMENT_POSITION
#include "Math/Spline.h"

namespace Math 
{
	const float cf = 0.5;
	float CatmullRom_float[] =
	{
		-1 * cf ,  3 * cf , -3 * cf ,  1 * cf , 
		2 * cf , -5 * cf ,  4 * cf , -1 * cf ,
		-1 * cf ,  0 * cf ,  1 * cf ,  0 * cf ,
		0 * cf ,  2 * cf ,  0 * cf ,  0 * cf 
	};

	template<> Spline1f::basis_t Spline1f::CatmullRom = Spline1f::basis_t(CatmullRom_float);
	template<> Spline2f::basis_t Spline2f::CatmullRom = Spline2f::basis_t(CatmullRom_float);
	template<> Spline3f::basis_t Spline3f::CatmullRom = Spline3f::basis_t(CatmullRom_float);


	float Bezier_float[] =
	{
		-1,  3, -3,  1, 
		 3, -6,  3,  0, 
		-3,  3, -0,  0, 
		 1,  0,  0,  0 
	};

	template<> Spline1f::basis_t Spline1f::Bezier = Spline1f::basis_t(Bezier_float);
	template<> Spline2f::basis_t Spline2f::Bezier = Spline2f::basis_t(Bezier_float);
	template<> Spline3f::basis_t Spline3f::Bezier = Spline3f::basis_t(Bezier_float);

	const float BScf = 1.0f / 6.0f;
	float BSpline_float[] =
	{
							-1 * BScf,  3 * BScf, -3 * BScf,  1 * BScf,
							 3 * BScf, -6 * BScf,  3 * BScf,  0 * BScf,
							-3 * BScf,  0 * BScf,  3 * BScf,  0 * BScf,
							 1 * BScf,  4 * BScf,  1 * BScf,  0 * BScf
	};

	template<> Spline1f::basis_t Spline1f::BSpline = Spline1f::basis_t(BSpline_float);
	template<> Spline2f::basis_t Spline2f::BSpline = Spline2f::basis_t(BSpline_float);
	template<> Spline3f::basis_t Spline3f::BSpline = Spline3f::basis_t(BSpline_float);


	float Linear_float[] =
	{
		 0,  0,  0,  0, 
		 0,  0,  0,  0, 
		 0,  1, -1,  0, 
		 0,  0,  1,  0 
	};

	template<> Spline1f::basis_t Spline1f::Linear = Spline1f::basis_t(Linear_float);
	template<> Spline2f::basis_t Spline2f::Linear = Spline2f::basis_t(Linear_float);
	template<> Spline3f::basis_t Spline3f::Linear = Spline3f::basis_t(Linear_float);

}
