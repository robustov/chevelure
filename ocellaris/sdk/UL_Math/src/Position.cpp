//#include "stdafx.h"
#define ED_MATH_INTERNALS
#define ED_MATH_IMPLEMENT_POSITION
#include "Math/Position.h"

namespace Math {

#define _INSTANTIATE( N, R, T ) \
	template<> \
	Position<N, R, T> Position<N, R, T>::id(0);

_INSTANTIATE( 2, float, int )
_INSTANTIATE( 3, float, int )

_INSTANTIATE( 2, float, float )
_INSTANTIATE( 3, float, float )

_INSTANTIATE( 2, float, double )
_INSTANTIATE( 3, float, double )

}
