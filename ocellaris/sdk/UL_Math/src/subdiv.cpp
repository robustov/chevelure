//#include "stdafx.h"
#include <math.h>
#include "Math/Subdiv.h"
#include "Math/Edge.h"

// ������� ��� �������� Subdiv
void ED_MATH_API Math::test_Subdiv(
	int facecount, 
	int vertexcount, 
	int level
	)
{
	printf("TEST SUBDIV!!!\n");
	std::vector<Math::Polygon32> faces;
	if( facecount<1) facecount=1;
	if( facecount>2)
	{
		printf("facecount=2\n");
		facecount=2;
	}
	int facevertexcount=facecount*vertexcount;
	faces.resize(facecount);
	if(facecount==1)
	{
		faces[0].verts.resize(vertexcount);
		for(int i=0; i<vertexcount; i++)
			faces[0].verts[i] = i;
		int facevertexcount = vertexcount;

		faces[1].verts.resize(vertexcount);
		for(int i=0; i<vertexcount; i++)
			faces[0].verts[i] = i;
	}
	else if(facecount==2)
	{
		faces[0].verts.resize(vertexcount);
		for(int i=0; i<vertexcount; i++)
			faces[0].verts[i] = i;

		faces[1].verts.resize(vertexcount);
		faces[1].verts[0] = 0;
		faces[1].verts[1] = 1;
		for(int i=2; i<vertexcount; i++)
			faces[1].verts[i] = vertexcount+i-2;

		vertexcount = facevertexcount-2;
	}

	// ������������ ����� �������
	std::vector< Quad32> quads;
	std::vector< subdivWeight> neoverts;
	std::vector<int> faces_polygonIndex;
	std::map<Math::Edge32, Math::Edge32> edges_srcedge;
	Math::Subdiv(
		level,									// ����� ������������
		&faces[0],							// ����: ��������
		faces.size(),							// ����� ���������
		vertexcount,							// ����� ���������
		facevertexcount,						// ����� fv
		quads,				// �����: �����
		neoverts,		// ���� ��� ������� ��������
		&faces_polygonIndex,	// ���� ������� ����� ��������� �������� �� �������� ��������� ����
		&edges_srcedge // �������� ���������� -> ������ ����� (� ������ ���� ������ ����� ��������� �������� �������� �����)
		);

	{
		printf("src:\n");
		for( int v=0; v<(int)faces.size(); v++)
		{
			printf("  P %d: ", v);
			Math::Polygon32& P = faces[v];
			for(int i=0; i<(int)P.verts.size(); i++)
			{
				printf(" %04d", P.verts[i]);
			}
			printf("\n");
		}
	}

	{
		printf("neoverts:\n");
		for( int v=0; v<(int)neoverts.size(); v++)
		{
			printf("  V %d:\n", v);
			subdivWeight& vert = neoverts[v];
			for(int i=0; i<(int)vert.data.size(); i++)
			{
				printf("    %04d*%f\n", vert.data[i].first, vert.data[i].second);
			}
			
		}
	}

	{
		printf("edges_srcedge:\n");
		std::map<Math::Edge32, Math::Edge32>::iterator it = edges_srcedge.begin();
		for( ; it != edges_srcedge.end(); it++)
		{
			const Math::Edge32& edge = it->first;
			const Math::Edge32& src_edge = it->second;
			subdivWeight& v1 = neoverts[edge.v1];
			subdivWeight& v2 = neoverts[edge.v2];
			std::set<int> s;
			int i;
			for(i=0; i<(int)v1.data.size(); i++)
				s.insert( v1.data[i].first);
			for(i=0; i<(int)v2.data.size(); i++)
				s.insert( v2.data[i].first);

			printf("edge(%d, %d) -> src edge(%d %d)", edge.v1, edge.v2, src_edge.v1, src_edge.v2);

			std::set<int>::iterator its = s.begin();
			for(;its!=s.end(); its++)
				printf(" %d", *its);
			printf("\n");
		}
	}
}

struct EdgeData32 : public Math::EdgeBind32
{
	int neovertindex;		// ������ �������� � ������ ��������
	Math::Edge32 src_edge;	// �������� �����
	EdgeData32()
	{
		neovertindex = 0;
	}
};
struct VertexBind
{
	std::set<Math::Edge32> edges;
	std::set<int> faces;

	void addface(int face)
	{
		faces.insert(face);
	}
	void addedge(const Math::Edge32& e)
	{
		edges.insert(e);
	}
};

struct weightedvertex
{
	typedef std::map<int, float> type_t;
	type_t data;

	void add(int v, float w)
	{
		if(w==0) return;
		type_t::iterator it = data.find(v);
		if( it==data.end()) 
			data[v] = w;
		else
			it->second += w;
	}
	void add(weightedvertex& v, float w)
	{
		if(w==0) return;
		type_t::iterator it = v.data.begin();
		for(;it != v.data.end(); it++)
			add(it->first, it->second*w);
	}
	void copyto(Math::subdivWeight& dst)
	{
		dst.data.resize(this->data.size());
		type_t::iterator it = this->data.begin();
		for(int i=0;it != this->data.end(); it++, i++)
		{
			dst.data[i] = *it;
		}
	}
};


void ED_MATH_API Math::Subdiv(
	int level,
	Math::Polygon32* faces, int poligoncount, int vertexcount, int facevertexcount, 
	std::vector< Math::Quad32>& quads,
	std::vector< Math::subdivWeight>& outverts, 
	std::vector<int>* faces_polygonIndex,	// �.� NULL
	std::map<Math::Edge32, Math::Edge32>* edges_srcedge	// �.� NULL
	)
{
	if( level>0) level--;
		else level = 0;

	std::vector< weightedvertex> neoverts;

	std::map<Math::Edge32, int> edgesdiv;

	std::vector< Math::Quad32> neofaces;
	neofaces.reserve(100);

	// �����
	std::map<Math::Edge32, EdgeData32> edges;
	// ����� ����������� � �������
	std::vector< VertexBind> vertexedges;
	vertexedges.resize(vertexcount);

	neoverts.reserve(poligoncount+vertexcount+facevertexcount);
	int startfacevertexindex = 0;
	// ������ ��������� � ������ edges � vertexedges
	int i;
	for( i=0; i<poligoncount; i++)
	{
		Math::Polygon32& face = faces[i];
		neoverts.push_back(weightedvertex());
		weightedvertex& neovert = neoverts.back();

		float w = 1.f/face.size();
		for( int v=0; v<face.size(); v++)
		{
			// ����� ��������
			neovert.add(face[v], w);
			// �����
			Math::Edge32 edge(face.cycle(v), face.cycle(v+1));
			EdgeData32& edgedata = edges[edge];
			edgedata.src_edge = edge;			// �������� �����
			edgedata.addface(i, v);	
			// ������ ����� ����������� � �������
			int vi = face[v];
			VertexBind& vb = vertexedges[vi];
			vb.addface(i);
		}
	}

	// ������ �����
	std::map<Math::Edge32, EdgeData32>::iterator it = edges.begin();
	for( ; it != edges.end(); it++)
	{
		const Math::Edge32& edge = it->first;
		EdgeData32& edgedata = it->second;
		edgedata.neovertindex = (int)neoverts.size();

		neoverts.push_back(weightedvertex());
		weightedvertex& neovert = neoverts.back();

		if( edgedata.facecount()==2)
		{
			float w = 1.f/(4.f);
			neovert.add(edge.v1, w);
			neovert.add(edge.v2, w);
			neovert.add(neoverts[edgedata.face(0)], w);
			neovert.add(neoverts[edgedata.face(1)], w);
		}
		else
		{
			neovert.add(edge.v1, 0.5f);
			neovert.add(edge.v2, 0.5f);
		}

		{
			vertexedges[edge.v1].addedge(edge);
			vertexedges[edge.v2].addedge(edge);
		}
	}
	// ���� ��������
	int startvertexindex = (int)neoverts.size();
	for( i=0; i<vertexcount; i++)
	{
		VertexBind& vb = vertexedges[i];

		neoverts.push_back(weightedvertex());
		weightedvertex& neovert = neoverts.back();
		int fc = vb.faces.size();
		int ec = vb.edges.size();
		if(ec==fc && fc>1)
		{
			float n = (float)vb.edges.size();
			float w = (n-2)/n;
			float we = 1/(n*ec);
			float wf = 1/(n*fc);

			neovert.add(i, w);
			{
				std::set<Math::Edge32>::iterator it = vb.edges.begin();
				for( ; it != vb.edges.end(); it++)
				{	
					const Math::Edge32& e = *it;
					int v = e.opposite(i);
					neovert.add(v, we);
				}
			}
			{
				std::set<int>::iterator it = vb.faces.begin();
				for( ; it != vb.faces.end(); it++)
					neovert.add( neoverts[ startfacevertexindex+*it], wf);
			}
		}
		else if(fc > 1)
		{
			// ����� ������ �����
			float we = 1/4.f;
			float w  = 1/2.f;
			neovert.add(i, w);
			std::set<Math::Edge32>::iterator it = vb.edges.begin();
			for( ; it != vb.edges.end(); it++)
			{
				EdgeData32& ed = edges[*it];
				if( ed.facecount()!=1) continue;
				neovert.add( neoverts[ ed.neovertindex], we);
			}
		}
		else
		{
			neovert.add(i, 1.f);
		}

	}

	// ������� �����
	quads.reserve(edges.size()*2);
	if( faces_polygonIndex)
		faces_polygonIndex->reserve(edges.size()*2);
	std::vector< VertexBind> neovertexedges;
	std::map<Math::Edge32, EdgeData32> neoedges;
	neovertexedges.resize(neoverts.size());
	for( i=0; i<poligoncount; i++)
	{
		Math::Polygon32& face = faces[i];
		for( int v=0; v<face.size(); v++)
		{
			Math::Edge32 prevedge(face.cycle(v-1), face.cycle(v));
			Math::Edge32 nextedge(face.cycle(v), face.cycle(v+1));
			int dstind = quads.size();
			quads.push_back(Math::Quad32());
			if( faces_polygonIndex)
				faces_polygonIndex->push_back(i);

			Math::Quad32& quad = quads.back();
			quad[0] = startvertexindex + face[v];		// Vk
			quad[1] = edges[nextedge].neovertindex;		// E
			quad[2] = startfacevertexindex + i;			// Fk
			quad[3] = edges[prevedge].neovertindex;		// E


			//  ����������� � ����. �������
//			if( level)
			{
				// Vk-E (��������� src_edge)
				neoedges[ Math::Edge32(quad[0], quad[1])].addface(dstind, 0);
				neoedges[ Math::Edge32(quad[0], quad[1])].src_edge = edges[nextedge].src_edge;
				// E-Fk
				neoedges[ Math::Edge32(quad[1], quad[2])].addface(dstind, 1);
				// Fk-E
				neoedges[ Math::Edge32(quad[2], quad[3])].addface(dstind, 2);
				// E-Vk (��������� src_edge)
				neoedges[ Math::Edge32(quad[3], quad[0])].addface(dstind, 3);
				neoedges[ Math::Edge32(quad[3], quad[0])].src_edge = edges[prevedge].src_edge;

				for(int ii=0; ii<4; ii++)
				{
					VertexBind& vb = neovertexedges[ quad[ii]];
					vb.addface(dstind);
					vb.addedge( Math::Edge32(quad.cycle(ii-1), quad.cycle(ii)));
					vb.addedge( Math::Edge32(quad.cycle(ii), quad.cycle(ii+1)));
				}
			}
		}
	}

	if(level==0)
	{
		outverts.resize(neoverts.size());
		for(unsigned i=0; i<neoverts.size(); i++)
			neoverts[i].copyto(outverts[i]);

		if( edges_srcedge)
		{
			// ������� ���������� -> ������ �����
			std::map<Math::Edge32, EdgeData32>::iterator it = neoedges.begin();
			for(;it != neoedges.end(); it++)
			{
				if( !it->second.src_edge.isValid()) 
					continue;
				(*edges_srcedge)[it->first] = it->second.src_edge;
			}
		}
	}
	else
	{
		// ���� ������� ������ ������� - ����� �����
		std::vector< weightedvertex> verts = neoverts;
		std::vector< VertexBind> vertexedges = neovertexedges;
		std::map<Math::Edge32, EdgeData32> edges = neoedges;		
		for( ; level>0; )
		{
			level--;
			int startfacevertexindex = 0;
			int startedgeindex = startfacevertexindex + quads.size();
			int startvertexindex = startedgeindex + edges.size();

			int facevertscount = quads.size()*4;
			std::vector< Math::Quad32> neoquads;
			neoquads.resize(facevertscount);
			std::vector< weightedvertex> neoverts;
			neoverts.resize(quads.size() + edges.size() + verts.size());

			// neofaces_polygonIndex
			std::vector<int> theneofaces_polygonIndex;
			std::vector<int>* neofaces_polygonIndex = NULL;
			if(faces_polygonIndex)
			{
				neofaces_polygonIndex = &theneofaces_polygonIndex;
				neofaces_polygonIndex->resize(facevertscount);
			}

			// 
			std::map<Math::Edge32, EdgeData32> neoedges;
			// ����� � ����� ����������� � �������
			std::vector< VertexBind> neovertexedges;
			if( level)
				neovertexedges.resize(neoverts.size());

			// �������� �� ������
			unsigned q=0;
			for(q=0; q<quads.size(); q++)
			{
				Math::Quad32& src = quads[q];
				weightedvertex& neovert = neoverts[q];
				neovert.add(verts[src[0]], 0.25f);
				neovert.add(verts[src[1]], 0.25f);
				neovert.add(verts[src[2]], 0.25f);
				neovert.add(verts[src[3]], 0.25f);
			}
			// �������� �� ������
			std::map<Math::Edge32, EdgeData32>::iterator it = edges.begin();
			for( int e=0; it != edges.end(); it++, e++)
			{
				const Math::Edge32& edge = it->first;
				EdgeData32& edgedata = it->second;
				edgedata.neovertindex = startedgeindex+e;
				weightedvertex& neovert = neoverts[startedgeindex+e];

				if( edgedata.facecount()==2)
				{
					float w = 1.f/(4.f);
					neovert.add(verts[edge.v1], w);
					neovert.add(verts[edge.v2], w);
					neovert.add(neoverts[edgedata.face(0)], w);
					neovert.add(neoverts[edgedata.face(1)], w);
				}
				else
				{
					float w = 0.5f;
					neovert.add(verts[edge.v1], w);
					neovert.add(verts[edge.v2], w);
				}
			}
			// �������� �� ��������
			for(unsigned v=0; v<vertexedges.size(); v++)
			{
				VertexBind& vb = vertexedges[v];

				weightedvertex& neovert = neoverts[startvertexindex+v];
				int fc = vb.faces.size();
				int ec = vb.edges.size();
				if(ec==fc && fc>1)
				{
					float n = (float)vb.edges.size();
					float w = (n-2)/n;
					float we = 1/(n*ec);
					float wf = 1/(n*fc);

					neovert.add( verts[v], w);
					{
						std::set<Math::Edge32>::iterator it = vb.edges.begin();
						for( ; it != vb.edges.end(); it++)
						{	
							const Math::Edge32& e = *it;
							int vop = e.opposite(v);
							neovert.add( verts[vop], we);
						}
					}
					{
						std::set<int>::iterator it = vb.faces.begin();
						for( ; it != vb.faces.end(); it++)
						{
							int f = *it;
							neovert.add( neoverts[ startfacevertexindex + f], wf);
						}
					}
				}
				else if(fc > 1)
				{
					// ����� ������ �����
					float we = 1/4.f;
					float w  = 1/2.f;
					neovert.add(verts[v], w);
					std::set<Math::Edge32>::iterator it = vb.edges.begin();
					for( ; it != vb.edges.end(); it++)
					{
						EdgeData32& ed = edges[*it];
						if( ed.facecount()!=1) continue;
						neovert.add( neoverts[ ed.neovertindex], we);
					}
				}
				else
				{
					neovert.add(verts[v], 1.f);
				}
			}

			// ����� �����
			for(q=0; q<quads.size(); q++)
			{
				Math::Quad32& src = quads[q];
				for(int i=0; i<src.size(); i++)
				{
					int dstind = q*4+i;
					Math::Quad32& dst = neoquads[dstind];
					if( neofaces_polygonIndex && faces_polygonIndex)
						(*neofaces_polygonIndex)[dstind] = (*faces_polygonIndex)[q];

					Math::Edge32 prevedge(src.cycle(i-1), src.cycle(i));
					Math::Edge32 nextedge(src.cycle(i), src.cycle(i+1));

					dst[0] = startvertexindex + src[i];			// Vk
					dst[1] = edges[nextedge].neovertindex;		// E
					dst[2] = startfacevertexindex + q;			// Fk
					dst[3] = edges[prevedge].neovertindex;		// E

//					if( level)
					{
						//  ����������� � ����. �������
						neoedges[ Math::Edge32(dst[0], dst[1])].addface(dstind, 0);
						neoedges[ Math::Edge32(dst[0], dst[1])].src_edge = edges[nextedge].src_edge;
						neoedges[ Math::Edge32(dst[1], dst[2])].addface(dstind, 1);
						neoedges[ Math::Edge32(dst[2], dst[3])].addface(dstind, 2);
						neoedges[ Math::Edge32(dst[3], dst[0])].addface(dstind, 3);
						neoedges[ Math::Edge32(dst[3], dst[0])].src_edge = edges[prevedge].src_edge;

						if( level)
						{
							for(int ii=0; ii<4; ii++)
							{
								VertexBind& vb = neovertexedges[dst[ii]];
								vb.addface(dstind);
								vb.addedge( Math::Edge32(dst.cycle(ii-1), dst.cycle(ii)));
								vb.addedge( Math::Edge32(dst.cycle(ii), dst.cycle(ii+1)));
							}
						}
					}
				}
			}
			if( level>0)
			{
				verts = neoverts;
				vertexedges = neovertexedges;
				edges = neoedges;

				quads = neoquads;
				if( neofaces_polygonIndex&&faces_polygonIndex)
					*faces_polygonIndex = *neofaces_polygonIndex;
			}
			else
			{
				quads = neoquads;
				if( neofaces_polygonIndex&&faces_polygonIndex)
					*faces_polygonIndex = *neofaces_polygonIndex;

				outverts.resize(neoverts.size());
				for(unsigned i=0; i<neoverts.size(); i++)
					neoverts[i].copyto(outverts[i]);

				if( edges_srcedge)
				{
					// ������� ���������� -> ������ �����
					std::map<Math::Edge32, EdgeData32>::iterator it = neoedges.begin();
					for(;it != neoedges.end(); it++)
					{
						if( !it->second.src_edge.isValid()) 
							continue;
						(*edges_srcedge)[it->first] = it->second.src_edge;
					}
				}
				return;
			}
		}
	}
}

/*/
void ED_MATH_API Math::SubdivN(
	Math::Quad32* faces, int poligoncount, 
	weightedvertex* verts, int vertexcount, 
	int edgecount, 
	std::vector< Math::Quad32>& quads,
	std::vector< weightedvertex>& neoverts
	)
{

}
/*/
