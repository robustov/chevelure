//#include "stdafx.h"

#include <stdio.h>
#include <string.h>
#include <math.h>
#include "Math/meshutil.h"
#include "Math/Matrix.h"


// � ���� (����� �������)
bool ED_MATH_API Math::isPointInCube(
	const Math::Vec3f& p, 
	const Math::Vec3f* pts,		// Vec3f[8]
	Math::Vec3f* posInCube)
{
	// 6 ����������
	int verts[6][4] = {
		{0, 1, 2, 4}, 
		{4, 6, 5, 2},
		{4, 5, 1, 2},
		{7, 6, 5, 3},
		{3, 1, 2, 5},
		{3, 2, 6, 5},
		};
	for(int i=0;i<6;i++)
	{
		Math::Vec3f curpts[4];
		curpts[0] = pts[ verts[i][0]];
		curpts[1] = pts[ verts[i][1]];
		curpts[2] = pts[ verts[i][2]];
		curpts[3] = pts[ verts[i][3]];
		Math::Vec4f bary = Math::CalcBaryCoords3d(curpts, p);
		if(bary[0]>=0 && bary[1]>=0 && bary[2]>=0 && bary[3]>=0)
		{
			// ������!!!
			if( posInCube)
			{
				// ����� posInCube
				*posInCube = Math::Vec3f(0);
				for(int pi=0; pi<4; pi++)
				{
					Math::Vec3f indweigth(0);
					int ind = verts[i][pi];
					if(ind&0x1) indweigth.x+=1.f;
					if(ind&0x2) indweigth.y+=1.f;
					if(ind&0x4) indweigth.z+=1.f;
					*posInCube += indweigth*bary[pi];
				}
			}
			return true;
		}
	}
	return false;
}

// � ���������
Math::Vec4f ED_MATH_API Math::CalcBaryCoords3d(const Vec3f* pts, Vec3f p)
{
	Math::Matrix3f m;
	m[0][0] = pts[0].x - pts[3].x;
	m[0][1] = pts[1].x - pts[3].x;
	m[0][2] = pts[2].x - pts[3].x;

	m[1][0] = pts[0].y - pts[3].y;
	m[1][1] = pts[1].y - pts[3].y;
	m[1][2] = pts[2].y - pts[3].y;

	m[2][0] = pts[0].z - pts[3].z;
	m[2][1] = pts[1].z - pts[3].z;
	m[2][2] = pts[2].z - pts[3].z;

	m.transpose();
	m.invert();
	Math::Vec3f c = p-pts[3];

	Math::Vec3f b = m*c;
	return Math::Vec4f(b, 1-(b.x+b.y+b.z));
}

void ED_MATH_API Math::CalcTangentSpace(
	Math::Vec3f V[3], 
	Math::Vec2f uv[3],
	Math::Vec3f& S, Math::Vec3f& T)
{
	S = T = Math::Vec3f(0, 1, 0);
	Math::Vec3f Edge01, Edge02;
	Math::Vec3f Cp;

	// X
	Edge01 = Math::Vec3f(V[1].x - V[0].x, 
						uv[1].x - uv[0].x, 
						uv[1].y - uv[0].y);
	Edge02 = Math::Vec3f(V[2].x - V[0].x, 
						uv[2].x - uv[0].x, 
						uv[2].y - uv[0].y);
	Cp = cross( Edge01, Edge02);
	Cp.normalize();
	if(fabs(Cp.x) > 1e-10f)
	{
		S.x = -Cp.y / Cp.x;
		T.x = -Cp.z / Cp.x;
	}

	// Y
	Edge01 = Math::Vec3f(V[1].y - V[0].y, 
						uv[1].x - uv[0].x, 
						uv[1].y - uv[0].y);
	Edge02 = Math::Vec3f(V[2].y - V[0].y, 
						uv[2].x - uv[0].x, 
						uv[2].y - uv[0].y);
	Cp = cross( Edge01, Edge02);
	Cp.normalize();
	if(fabs(Cp.x) > 1e-10f)
	{
		S.y = -Cp.y / Cp.x;
		T.y = -Cp.z / Cp.x;
	}

	// Z
	Edge01 = Math::Vec3f(V[1].z - V[0].z, 
						uv[1].x - uv[0].x, 
						uv[1].y - uv[0].y);
	Edge02 = Math::Vec3f(V[2].z - V[0].z, 
						uv[2].x - uv[0].x, 
						uv[2].y - uv[0].y);
	Cp = cross( Edge01, Edge02);
	Cp.normalize();
	if(fabs(Cp.x) > 0.0001f)
	{
		S.z = -Cp.y / Cp.x;
		T.z = -Cp.z / Cp.x;
	}

//	T = -T;
	float slen = S.length();
	float tlen = T.length();
//	S = S/slen;
//	T = T/tlen;
}

float ED_MATH_API Math::facearea( 
	const Math::Vec3f& a,
	const Math::Vec3f& b, 
	const Math::Vec3f& c )
{
	Math::Vec3f ptarea = cross(a-c, b-c);
	float parea = ptarea.length();

	return parea/2;
}

// 
Math::Vec3f ED_MATH_API Math::CalcBaryCoords(
	const Math::Vec3f& p0, const Math::Vec3f& p1, const Math::Vec3f& p2, 
	const Math::Vec3f& p)
{ 
	Math::Vec3f tpos[3];
	Math::Vec3f cpos;
	Math::Vec3f bary;

	tpos[0] = p0;
	tpos[1] = p1;
	tpos[2] = p2;
	cpos = p;

	float area_abc, area_pbc, area_apc, area_abp; 
	
	area_pbc =facearea(cpos   ,tpos[1],tpos[2]); 
	area_apc =facearea(tpos[0],cpos   ,tpos[2]);
	area_abp =facearea(tpos[0],tpos[1],cpos   );
	area_abc = 1/(area_pbc+area_apc+area_abp);

	bary.x = area_pbc *area_abc ;
	bary.y = area_apc *area_abc ;
	bary.z = area_abp *area_abc ;
	
	return bary;
}
Math::Vec3f ED_MATH_API Math::CalcBaryCoords(Math::Vec3f* pts, Math::Vec3f p)
{
	return Math::CalcBaryCoords(pts[0], pts[1], pts[2], p);
}
Math::Vec3f ED_MATH_API Math::CalcBaryCoords(Math::Vec2f* pts, Math::Vec2f p)
{
	Math::Vec3f v0(pts[0].x, pts[0].y, 0);
	Math::Vec3f v1(pts[1].x, pts[1].y, 0);
	Math::Vec3f v2(pts[2].x, pts[2].y, 0);
	Math::Vec3f pp(p.x, p.y, 0);
	return Math::CalcBaryCoords(v0, v1, v2, pp);
}

bool ED_MATH_API Math::IsInFace(Math::Vec2f* pts, const Math::Vec2f& p)
{
	float x=p.x, z=p.y;
	float x0, z0, x1, z1, x2, z2;
	Math::Vec2f* vt;
	vt = pts+0;
	x0 = vt->x;
	z0 = vt->y;
	vt = pts+1;
	x1 = vt->x;
	z1 = vt->y;
	vt = pts+2;
	x2 = vt->x;
	z2 = vt->y;

	float a0 = (z1 - z0) * (x - x0) - (z - z0) * (x1 - x0);
	float a1 = (z2 - z1) * (x - x1) - (z - z1) * (x2 - x1);
	float a2 = (z0 - z2) * (x - x2) - (z - z2) * (x0 - x2);

	if(a0>=0 && a1>=0 &&a2>=0)
		return true;
	if(a0<=0 && a1<=0 &&a2<=0)
		return true;
	return false;
}

	//
	Math::Vec3f CalcBaryCoords2d( Math::Vec2f* vt, const Math::Vec2f& p)
	{
		Math::Vec2f a(vt[1].x - vt[0].x, vt[1].y - vt[0].y);
		Math::Vec2f b(vt[2].x - vt[0].x, vt[2].y - vt[0].y);

		float x = p.x - vt[0].x;
		float y = p.y - vt[0].y;

		float d = b.x*a.y - b.y*a.x;
		if(fabs(d) < 0.00001) 
			return Math::Vec3f(1, 0, 0);

		float k = (x*a.y - y*a.x)/d;

		float t;
		if(fabs(a.x) > fabs(a.y)) 
			t = (x - k*b.x)/a.x;
		else 
			t = (y - k*b.y)/a.y;

		return Math::Vec3f(1-t-k, t, k);
//		return t*(vt[1].y - vt[0].y) + k*(vt[2].y - vt[0].y) + vt[0].y;
	}
