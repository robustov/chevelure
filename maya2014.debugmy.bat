pushd %temp%

set CHEV_PATH=m:/chevelure/bin/x86_64.vc100.debug.maya2014
set CHEV_PATH=d:/chevelure/bin/x86_64.vc100.debug.maya2014
set CHEVELURE=%CHEV_PATH%

set PATH=%CHEV_PATH%;c:\Pixar\RenderManProServer-17.0\bin\;%PATH%
set ULITKABIN=%CHEV_PATH%
set MAYA_PLUG_IN_PATH=%CHEV_PATH%
set MAYA_SCRIPT_PATH=%CHEV_PATH%/../mel
set PYTHONPATH=%CHEV_PATH%/../python

pushd "C:\Program Files\Autodesk\Maya2014\bin\"
start maya.exe

