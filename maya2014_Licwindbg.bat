pushd %temp%

set CHEV_PATH=c:/chevelure2013
set CHEV_PATH=d:/chevelure/bin/x86_64.vc100.debug.maya2014

set PATH=%CHEV_PATH%;C:\Program Files\Pixar\RenderManProServer-17.0\bin\;%PATH%
set ULITKABIN=%CHEV_PATH%
set MAYA_PLUG_IN_PATH=%CHEV_PATH%;%MAYA_PLUG_IN_PATH%
set MAYA_SCRIPT_PATH=%CHEV_PATH%/../mel;%MAYA_SCRIPT_PATH%
set PYTHONPATH=%CHEV_PATH%/../python;%PYTHONPATH%
set RMS_SCRIPT_PATHS=d:\chevelure~\proj
start "" "C:\Program Files\Autodesk\Maya2014\bin\maya.exe"
