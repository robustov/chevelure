#pragma once
#include <time.h>

#pragma pack(1)

typedef unsigned int time_sas;

struct UlLD_Header
{
	unsigned short len;
	unsigned short type;
};

// client -> server
struct UlLD_IMStarted : public UlLD_Header
{
	static const unsigned short TYPE = 1;
	char question[16];
	char productname[128];
	time_sas clienttime;
	UlLD_IMStarted()
	{
		len = sizeof(*this);
		type = TYPE;
		clienttime = time(NULL);
	}
};

// server -> client, reply on UlLD_IMStarted
struct UlLD_IMStartedReply : public UlLD_Header
{
	static const unsigned short TYPE = 2;
	int session_id;
	DWORD licenseflag;
	char serialNumber[128];
	time_sas servertime;
	UlLD_IMStartedReply()
	{
		len = sizeof(*this);
		type = TYPE;
		servertime = time(NULL);
	}
};

// client -> server
struct UlLD_IAMAlive : public UlLD_Header
{
	static const unsigned short TYPE = 3;
	int session_id;
	time_sas clienttime;
	UlLD_IAMAlive()
	{
		len = sizeof(*this);
		type = TYPE;
		clienttime = time(NULL);
	}
};

// client -> server
struct UlLD_IAMDead : public UlLD_Header
{
	static const unsigned short TYPE = 4;
	int session_id;
	time_sas clienttime;
	UlLD_IAMDead()
	{
		len = sizeof(*this);
		type = TYPE;
		clienttime = time(NULL);
	}
};
