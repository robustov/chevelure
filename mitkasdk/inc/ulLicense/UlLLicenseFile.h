#pragma once
#pragma pack(1)

#define UlL_SIGNATURE_LEN 256

//typedef wchar_t* PTCHAR;
#include "rsa_routines.h"
#include <vector>

#define ULITKA_LIC_SERVER_VERSION "0.2.1"

struct UlLLicenseFile
{
	static const int maxProductCount = 10;

	char addressname[128];
	unsigned short port;
	char macaddress[32];
	char serialNumber[128];

	struct Product
	{
		char productname[128];
		unsigned long copyAllow;
		char datestring[32];
	};
	Product products[maxProductCount];
	unsigned long random;
	std::vector<unsigned char> random_sign;
	std::vector<unsigned char> sign;

	bool bValid;
	UlLLicenseFile();

	bool Read(
		const char* filename, 
		bool bUnsignedAvailable=false,		// �� �������� �� ������������ �����
		bool bDump = false
		);
	bool Write(
		const char* filename, 
		bool bWriteSign=true				// ���������� ���������
		);
	// ���������
	bool Sign(
		Prsa_secret_key_t psk
		);
	// ���������
	bool Verify(
		Prsa_public_key_t ppk
		);
};