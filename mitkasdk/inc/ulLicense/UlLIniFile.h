#pragma once
#pragma pack(1)

struct UlLIniFile
{
	char addressname[128];
	unsigned short port;

	bool bValid;
public:
	UlLIniFile();

	bool Read(
		const char* filename, 
		bool bDump = false
		);
};