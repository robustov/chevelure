#pragma once
#include <winsock2.h>
#include <map>
#include <string>
#include "UlLDatagram.h"
#include "UlLLicenseFile.h"
#include "rsa_routines.h"

class UlLServer
{
public:
	UlLServer(void);
	~UlLServer(void);

	bool Start(
		const char* licensefile, 
		Prsa_public_key_t ppk, 
		bool bVerbose, 
		const char* logfilename=NULL
		);
	void Stop(
		);

	bool Loop(
		const char* logfilename=NULL
		);

protected:
	bool bValid;
	bool bTrace;
	bool validlicense;
//	time_t licensetime;

	UlLLicenseFile license;

	// adress port
	sockaddr_in server;
	//
	SOCKET sd;

	struct SessionData
	{
		//address
		sockaddr_in client;
		time_t lastalive;
		char productname[128];
	};
	// �� ������, ������
	std::map<int, SessionData> sessions;
	// 
	int next_session_id;

	// ����� ��������
	int GetClientCount(const char* productname);

	// �������� ����� ��������
	void UpdateClientCount();
	// mac
//	std::string macaddress;
	// license
//	std::string license;

	// �������� �������� ��� ��������
	bool Check(
		const char* logfilename, 
		const char* productname, 
		const time_t& clienttime, 
		DWORD& licenseflag
		);
public:
	bool ThreadProc(
		SOCKET newsd, 
		const char* logfilename, 
		sockaddr_in& client);
};
