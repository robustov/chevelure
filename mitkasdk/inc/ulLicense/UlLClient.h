#pragma once
#include <winsock2.h>
#include <string>
#include "UlLDatagram.h"

class UlLClient
{
	// 
	bool bValid;
	bool bTrace;

	std::string productname;
	// adress port
	sockaddr_in server;
	sockaddr_in client;
	//
//	SOCKET sd;
	// id ������ �� �������
	int sessionId;
public:
	UlLClient(void);
	~UlLClient(void);

	bool Start(const char* inifilename, const char* productname, bool bTrace=false);
	void Stop();

	bool GetLicense(void* keydest, int keysize);
	bool KeepAliveLoop();
	bool SendImDead();
};
