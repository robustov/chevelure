#pragma once

#include "./UlLClient.h"

struct UlLClientThread
{
	HANDLE thisModule;
	std::string productname;
	std::string inifile;
	DWORD* licenseflag;
	bool bTrace;

protected:
	HANDLE hThread;
	DWORD threadId;
	HANDLE hEvent;

public:
	UlLClientThread(
		);
	~UlLClientThread(
		);
	bool StartThread(
		HANDLE thisModule, 
		const char* productname,
		const char* inifilename,		// �� NULL
		DWORD* licenseflag,
		bool bTrace=false
		);
	bool WaitForStart(
		bool bExitIfError = false
		);
	void StopThread(
		);

protected:
	DWORD ThreadLoop(
		);
private:
	static DWORD WINAPI ThreadProc(
		LPVOID lpParameter
		);

};
