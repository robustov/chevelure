#include <windows.h>
#include <stdio.h>

#include "..\\Crypto\\interfaces\\rsa_routines_keygen.h"
#include "..\\Crypto\\interfaces\\rsa_routines.h"
#include "..\\Crypto\\interfaces\\key_files_support.h"

static
int __stdcall __GenPrimeProgressFunc( int num_bits, char progress_type, int gen_prime )
{
	wprintf( L"%c", progress_type );
	return( 0 );
}



int wmain( int argc, WCHAR **argv )
{
		// ������ ������� �������� ���� � 512 ���, ����� 256, 512, 1024, 2048, 4096, 8192 �� � �� ����� �������� ������
		rsa_secret_key_t sk;
		ZeroMemory( &sk, sizeof(sk) );
		int flags = rsa_generate_secret_key( &sk, 256, NULL );

		UCHAR	ucBuffer[11] = { 0xff, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		ULONG	ulSignSize;

		PVOID pSign = rsa_sign_raw( ucBuffer, sizeof(ucBuffer), &ulSignSize, &sk );




		// ������ �� ��������� �������� (������� �������� ����)
		rsa_public_key_t pk;
		ZeroMemory( &pk, sizeof(pk) );
		rsa_get_public_key( &sk, &pk );

		rsa_verify_raw( ucBuffer, sizeof(ucBuffer), pSign, ulSignSize, &pk );

		rsa_destroy_secret_key( &sk, 0 );
		rsa_destroy_public_key( &pk, 0 );
		rsa_free_memory( pSign );

		// TODO: ��� ���� ����� ��������� � ������ � ������� rsa_save_public_key_to_memory( &pk, 0 );
		// � � ���� �� ������ ph �������� ph->ulSize
		

		// ������ ����� � ������ � ������ �� � rsa_load_public_key_from_memory() ��� rsa_load_secret_key_from_memory()


	return( 0 );
}