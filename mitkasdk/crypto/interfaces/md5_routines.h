#ifndef _MD5_ROUTINES_H_INCLUDED_
#define _MD5_ROUTINES_H_INCLUDED_


#pragma pack(push, 4)
#ifdef __cplusplus
extern "C"{
#endif 

/*
 *	��� �������� �� ��������� ��� ������������� MD5
 */
#define MD5_DEFAULT_INITVALUE_A ((unsigned long)0x783ba901)
#define MD5_DEFAULT_INITVALUE_B ((unsigned long)0xfe4046bc)
#define MD5_DEFAULT_INITVALUE_C ((unsigned long)0x98374bac)
#define MD5_DEFAULT_INITVALUE_D ((unsigned long)0xef0eab78)

/*
 *	������ ���������� ��� MD5
 */
#define MD5_DIGEST_LENGTH_UCHAR	(16)

/*
 * ��������� ��� MD5 ��������
 */
typedef struct tds_md5context
{
    unsigned long state_A;
    unsigned long state_B;
    unsigned long state_C;
    unsigned long state_D;
    
} md5_context_t, *Pmd5_context_t;

/*
 *	��� ���������� ���������� �� ��������� MD5
 */
static const md5_context_t md5_default_init_context =
{
	MD5_DEFAULT_INITVALUE_A,
	MD5_DEFAULT_INITVALUE_B,
	MD5_DEFAULT_INITVALUE_C,
	MD5_DEFAULT_INITVALUE_D
};

/*
 * ���������������� �������� ��� MD5 � ���������� ����������
 */
void __stdcall md5_init_context_states( Pmd5_context_t	p_md5,
										unsigned long	init_state_A,
										unsigned long	init_state_B,
										unsigned long	init_state_C,
										unsigned long	init_state_D );

/*
 * ���������������� �������� ��� MD5
 */
void __stdcall md5_init_context( Pmd5_context_t	p_md5 );

/*
 * ���������� ���� ��� MD5
 */
void __stdcall md5_process_block( Pmd5_context_t	p_md5,
								  const void		*p_data,
								  unsigned long		num_bytes );

/*
 * �������� ��������� MD5
 */
void __stdcall md5_get_result( Pmd5_context_t	p_md5,
							   unsigned char	result[MD5_DIGEST_LENGTH_UCHAR] );

#ifdef __cplusplus
}
#endif 
#pragma pack(pop)


#endif // _MD5_ROUTINES_H_INCLUDED_
