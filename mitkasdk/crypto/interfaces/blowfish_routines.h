#ifndef _BLOWFISH_ROUTINES_H_INCLUDED_
#define _BLOWFISH_ROUTINES_H_INCLUDED_

#pragma pack(push, 4)
#ifdef __cplusplus
extern "C" {
#endif

/*
 * ��� ��������� 
 */
typedef struct tds_blowfish_context
{
    unsigned long s0[256];
    unsigned long s1[256];
    unsigned long s2[256];
    unsigned long s3[256];
    unsigned long p[16+2];
	
} blowfish_context_t, *Pblowfish_context_t;

#define BLOWFISH_BLOCK_LENGTH_UCHAR		8

/*
 * ��� ���������� �����
 */
void __stdcall blowfish_encrypt_block( Pblowfish_context_t	p_bfc, 
									   unsigned char		in[BLOWFISH_BLOCK_LENGTH_UCHAR],
									   unsigned char		out[BLOWFISH_BLOCK_LENGTH_UCHAR] );

/*
 * ��� ����������� �����
 */
void __stdcall blowfish_decrypt_block( Pblowfish_context_t	p_bfc,
									   unsigned char		in[BLOWFISH_BLOCK_LENGTH_UCHAR],
									   unsigned char		out[BLOWFISH_BLOCK_LENGTH_UCHAR] );

/*
 *	��� ���������� ������
 */
void __stdcall blowfish_encrypt( unsigned char			*in, 
								 unsigned char			*out,
								 unsigned long			length, 
								 unsigned long			*num,
								 Pblowfish_context_t	p_bfc,
								 unsigned char			ivec[BLOWFISH_BLOCK_LENGTH_UCHAR] );

/*
 *	��� ����������� ������
 */
void __stdcall blowfish_decrypt( unsigned char			*in, 
								 unsigned char			*out,
								 unsigned long			length, 
								 unsigned long			*num,
								 Pblowfish_context_t	p_bfc,
								 unsigned char			ivec[BLOWFISH_BLOCK_LENGTH_UCHAR] );

/*
 * ��� ������������� ��������� � ��������� ����� ���������� -1 ���� ���� ����� ���� ������, �� �������� ������������� ���
 */
int __stdcall blowfish_init_context( Pblowfish_context_t p_bfc, unsigned char *key, unsigned long keylen );

#ifdef __cplusplus
}
#endif
#pragma pack(pop)


#endif // _BLOWFISH_ROUTINES_H_INCLUDED_