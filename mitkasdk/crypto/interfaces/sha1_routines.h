#ifndef _SHA1_ROUTINES_H_INCLUDED_
#define _SHA1_ROUTINES_H_INCLUDED_

#pragma pack(push, 4)
#ifdef __cplusplus
extern "C"{
#endif 

/*
 *	��� �������� �� ��������� ��� ������������� SHA1
 */
#define SHA1_DEFAULT_INITVALUE_A		((unsigned long)0x59afdeb4)
#define SHA1_DEFAULT_INITVALUE_B		((unsigned long)0x783ba901)
#define SHA1_DEFAULT_INITVALUE_C		((unsigned long)0xfe4046bc)
#define SHA1_DEFAULT_INITVALUE_D		((unsigned long)0xb83d7431)
#define SHA1_DEFAULT_INITVALUE_E		((unsigned long)0xef0eab78)

#define SHA1_DEFAULT_INITVALUE_K_00_19	((unsigned long)0x5a827999)
#define SHA1_DEFAULT_INITVALUE_K_20_39	((unsigned long)0x6ed9eba1)
#define SHA1_DEFAULT_INITVALUE_K_40_59	((unsigned long)0x8f1bbcdc)
#define SHA1_DEFAULT_INITVALUE_K_60_79	((unsigned long)0xca62c1d6)


/*
 *	������ ���������� ��� SHA1
 */
#define SHA1_DIGEST_LENGTH_UCHAR	(20)

/*
 * ��������� ��� SHA1 ��������
 */
typedef struct tds_sha1context
{
    unsigned long state_A;
    unsigned long state_B;
    unsigned long state_C;
    unsigned long state_D;
	unsigned long state_E;

	unsigned long K_00_19;
	unsigned long K_20_39;
	unsigned long K_40_59;
	unsigned long K_60_79;
    
} sha1_context_t, *Psha1_context_t;

/*
 *	��� ���������� ���������� �� ��������� SHA1
 */
static const sha1_context_t sha1_default_init_context =
{
	SHA1_DEFAULT_INITVALUE_A,
	SHA1_DEFAULT_INITVALUE_B,
	SHA1_DEFAULT_INITVALUE_C,
	SHA1_DEFAULT_INITVALUE_D,
	SHA1_DEFAULT_INITVALUE_E,
	SHA1_DEFAULT_INITVALUE_K_00_19,
	SHA1_DEFAULT_INITVALUE_K_20_39,
	SHA1_DEFAULT_INITVALUE_K_40_59,
	SHA1_DEFAULT_INITVALUE_K_60_79
};

/*
 * ���������������� �������� ��� SHA1 � ���������� ����������
 */
void __stdcall sha1_init_context_states( Psha1_context_t	p_sha1,
										 unsigned long		init_state_A,
										 unsigned long		init_state_B,
										 unsigned long		init_state_C,
										 unsigned long		init_state_D,
										 unsigned long		init_state_E,
										 unsigned long		init_K_00_19,
										 unsigned long		init_K_20_39,
										 unsigned long		init_K_40_59,
										 unsigned long		init_K_60_79 );

/*
 * ���������������� �������� ��� SHA1
 */
void __stdcall sha1_init_context( Psha1_context_t	p_sha1 );

/*
 * ���������� ���� ��� SHA1
 */
void __stdcall sha1_process_block( Psha1_context_t	p_sha1,
								   const void		*p_data,
								   unsigned long	num_bytes );

/*
 * �������� ��������� SHA1
 */
void __stdcall sha1_get_result( Psha1_context_t	p_sha1,
								unsigned char	result[SHA1_DIGEST_LENGTH_UCHAR] );

#ifdef __cplusplus
}
#endif 
#pragma pack(pop)

#endif // _SHA1_ROUTINES_H_INCLUDED_