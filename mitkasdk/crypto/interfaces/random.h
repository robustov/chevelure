#ifndef _RANDOM_H_INCLUDED_
#define _RANDOM_H_INCLUDED_

#pragma pack(push, 4)
#ifdef __cplusplus
extern "C"{
#endif 

/*
 *	��� ������������� ������� ��������� �����
 */
VOID __stdcall RandGen_InitializeGenerator( IN PUCHAR	pSeeds		OPTIONAL,
										    IN ULONG	ulSeedsSize	OPTIONAL );

/*
 *	��� ��������� �����
 */
VOID __stdcall RandGen_GenerateByte( PUCHAR pChar, BOOLEAN bNeedReInit );

/*
 *	��� ��������� ������
 */
VOID __stdcall RandGen_GenerateBuffer( PUCHAR pBuffer, ULONG ulBufferSizeBytes, BOOLEAN bNeedReInit );


#ifdef __cplusplus
}
#endif 
#pragma pack(pop)


#endif // _RANDOM_H_INCLUDED_
