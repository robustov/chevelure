#ifndef _KEY_FILES_SUPPORT_H_INCLUDED_
#define _KEY_FILES_SUPPORT_H_INCLUDED_

#include "sha1_routines.h"

#pragma pack(push, 4)
#ifdef __cplusplus
extern "C"{
#endif 

/*
 *	�������������� ������ ����������
 */
typedef enum tde_KFS_CryptoAlgoType
{
	KFS_CAT_FIRST_VALUE		= 1,
	KFS_CAT_RSA				= KFS_CAT_FIRST_VALUE,
	KFS_CAT_LAST_VALUE		= KFS_CAT_RSA,
	KFS_CAT_NONE			= -2,
	KFS_CAT_UNKNOWN			= -1

} KFS_CryptoAlgoType_e, *PKFS_CryptoAlgoType_e;

/*
 *	�������������� ������ ������
 */
typedef enum tde_KFS_CryptoKeyType
{
	KFS_CKT_FIRST_VALUE		= 1,
	KFS_CKT_RSA_PRV			= KFS_CKT_FIRST_VALUE,
	KFS_CKT_RSA_PUB,
	KFS_CKT_LAST_VALUE		= KFS_CKT_RSA_PUB,
	KFS_CKT_UNKNOWN			= -1

} KFS_CryptoKeyType_e, *PKFS_CryptoKeyType_e;

/*
 *	��� ������ �����
 */
#define KFS_HEADER_VERSION			0x00010000

/*
 *	��� Tag �����
 */
#define KFS_HEADER_TAG				'_SFK'

/*
 *	������
 */
#define KFS_HEADER_FLAGS_ENCRYPTED_KEY	0x80000000

/*
 *	������
 */
#define RSA_FLAGS_CAN_ENCRYPT			0x00000001
#define RSA_FLAGS_CAN_SIGN				0x00000002
#define RSA_FLAGS_CAN_CHECK_SIGN		0x00000004


/*
 *	��� ��������� ������
 */
typedef struct tds_KFS_Header
{
	ULONG		ulTag;
	ULONG		ulVersion;
	ULONGLONG	ulValidFrom;							// � ������ �������
	ULONGLONG	ulValidTo;								// �� ������ �������
	ULONG		ulKeyType;
	ULONG		ulFlags;								// ������
	ULONG		ulSize;									// ������ ������ ������ � ���� ����������
	UCHAR		ucaKeyId[SHA1_DIGEST_LENGTH_UCHAR];		// ������������� �����

} KFS_Header_t, *PKFS_Header_t;

/*
 *	��� �������������� ����������
 */
typedef struct tds_KFS_ExtentedInfo
{
	PTCHAR	pKeyId;
	PTCHAR	pAlgoInfo;
	PTCHAR	pKeyFlags;
	PTCHAR	pValid;

} KFS_ExtentedInfo_t, *PKFS_ExtentedInfo_t;

/*
 *	��� ������� MPI
 */
#define KFS_MPI_FLAGS_NEGATIVE_VALUE			0x00000001

/*
 *	��� �������� MPI
 */
typedef struct tds_KFS_MPI
{
	ULONG	nlimbs;		// ���������� Limb
	ULONG	flags;

} KFS_MPI_t, *PKFS_MPI_t;

/*
 *	��� ��������� ����� Rsa
 */
typedef struct tds_KFS_Header_Rsa_Pub
{
	ULONG		ulKeySizeBits;
	KFS_MPI_t	n;
	KFS_MPI_t	e;
// n, e
} KFS_Header_Rsa_Pub_t, *PKFS_Header_Rsa_Pub_t;

/*
 *	��� ��������� ����� Rsa
 */
typedef struct tds_KFS_Header_Rsa_Prv
{
	KFS_Header_Rsa_Pub_t	pub;
	KFS_MPI_t				d;
	KFS_MPI_t				p;
	KFS_MPI_t				q;
	KFS_MPI_t				u;

// n, e, d, p, q, u
} KFS_Header_Rsa_Prv_t, *PKFS_Header_Rsa_Prv_t;

/*
 *	��� ������������ ������
 */
VOID __stdcall KFS_FreeMemory( IN PVOID pMem OPTIONAL );

/*
 *	��� ������������� ���������
 */
VOID __stdcall KFS_InitializeHeader( PKFS_Header_t pHeader );

/*
 * ��� �������� ���������
 */
ULONG __stdcall KFS_CheckHeader( IN PKFS_Header_t pHeader );

/*
 *	��� ��������� ������ �� ���� ���������
 */
PTCHAR __stdcall KFS_GetCryptoAlogNameFromType( IN KFS_CryptoAlgoType_e eType );

/*
 *	��� ��������� �������������� ����������
 */
BOOLEAN __stdcall KFS_GetExtentedInfo( IN  PKFS_Header_t		pHeader,
									   OUT PKFS_ExtentedInfo_t	pExInfo );

/*
 *	��� ���������� �������������� ����������
 */
VOID __stdcall KFS_DestroyExtentedInfo( IN PKFS_ExtentedInfo_t	pExInfo );

/*
 *	������� ������������� �����
 */
BOOLEAN __stdcall KFS_GenerateKeyId( IN  PVOID	pBuffer,
									 IN  ULONG	ulSize,
									 OUT PUCHAR	pucaKeyId );	// ������ SHA1_DIGEST_LENGTH_UCHAR

#ifdef __cplusplus
}
#endif 
#pragma pack(pop)

#endif // _KEY_FILES_SUPPORT_H_INCLUDED_