#ifndef _RSA_ROUTINES_H_INCLUDED_
#define _RSA_ROUTINES_H_INCLUDED_

#pragma pack(push, 4)
#ifdef __cplusplus
extern "C" {
#endif

/*
 *	��� �������� ���������� ����� RSA
 */
typedef struct tds_rsa_public_key
{
	struct tds_MPI *n;	    /* modulus */
	struct tds_MPI *e;	    /* exponent */

} rsa_public_key_t, *Prsa_public_key_t;

/*
 *	��� �������� ��������� ����� RSA
 */
typedef struct tds_rsa_secret_key
{
    struct tds_MPI *n;	    /* public modulus */
    struct tds_MPI *e;	    /* public exponent */
    struct tds_MPI *d;	    /* exponent */
    struct tds_MPI *p;	    /* prime  p. */
    struct tds_MPI *q;	    /* prime  q. */
    struct tds_MPI *u;	    /* inverse of p mod q. */

} rsa_secret_key_t, *Prsa_secret_key_t;

/*
 *	��� ���������� �� �������� �����
 */
void __stdcall rsa_encrypt( struct tds_MPI *output, struct tds_MPI *input, Prsa_public_key_t pkey );

/*
 *	��� ����������� �� �������� �����
 */
void __stdcall rsa_decrypt( struct tds_MPI *output, struct tds_MPI *input, Prsa_secret_key_t skey );

/*
 *	��� ������������ ���������� ����� RSA
 */
int __stdcall rsa_test_secret_key( Prsa_secret_key_t sk, unsigned nbits );


/*
 *	���������� ������ ������ ��� �������������
 */
PVOID __stdcall rsa_prepare_any_buffer_for_decrypt_raw( IN  Prsa_secret_key_t	skey,
														IN  PVOID				pInBuffer,
														IN  ULONG				ulInBufferSize,
														OUT PULONG				pulOutBufferSize );

/*
 *	��� ����������� ������ ������
 */
PVOID __stdcall rsa_decrypt_buffer_raw( IN  Prsa_secret_key_t	skey,
										IN  PVOID				pInBuffer,
										IN  ULONG				ulInBufferSize,
										OUT PULONG				pulOutBufferSize );

/*
 * ��������� �������� ���� �� ������
 */
Prsa_public_key_t __stdcall rsa_load_public_key_from_memory( IN  struct tds_KFS_Header		*pHeader,
															 OUT PULONG						pulNumBits  OPTIONAL,
															 OUT PULONG						pulFlags	OPTIONAL );


/*
 *	���������� ������ ����� � ������ �� ��������� �����
 */
int __stdcall rsa_get_block_size_for_encrypt( IN Prsa_public_key_t	pkey );

/*
 *	���������� ������ ����� � ������ �� ��������� �����
 */
int __stdcall rsa_get_block_size_for_decrypt( IN Prsa_secret_key_t	skey );


/*
 *	��� ������� ������ ����� ����������
 */
PVOID __stdcall rsa_clear_any_buffer_after_encrypt_raw( IN  Prsa_public_key_t	pkey,
														IN  PVOID				pInBuffer,
														IN  ULONG				ulInBufferSize,
														OUT PULONG				pulOutBufferSize );
/*
 *	��� ���������� ������ �� �������� �����
 */
PVOID __stdcall rsa_encrypt_buffer_raw( IN  Prsa_public_key_t	pkey,
										IN  PVOID				pInBuffer,
										IN  ULONG				ulInBufferSize,
										OUT PULONG				pulOutBufferSize );

/*
 *	��� ����������� ������ �� �������� �����
 */
PVOID __stdcall rsa_decrypt_buffer( IN  Prsa_secret_key_t	skey,
									IN  PVOID				pInBuffer,
									IN  ULONG				ulInBufferSize,
									OUT PULONG				pulOutBufferSize );

/*
 *	��� ��������� ��������� ����� �� ����������
 */
Prsa_public_key_t __stdcall rsa_get_public_key( Prsa_secret_key_t sk, Prsa_public_key_t pk );

/*
 *	��� ���������� ���������� �����
 */
void __stdcall rsa_destroy_secret_key( Prsa_secret_key_t sk, int iFullDestroy );

/*
 *	��� ���������� ��������� �����
 */
void __stdcall rsa_destroy_public_key( Prsa_public_key_t pk, int iFullDestroy );

/*
 *	��� ������������
 */
void __stdcall rsa_sign( struct tds_MPI *output, struct tds_MPI *input,  Prsa_secret_key_t skey );

/*
 *	��� �������� �������
 */
int __stdcall rsa_verify( struct tds_MPI *digest, struct tds_MPI *input,  Prsa_public_key_t pkey );

/*
 *	��� ������������
 */
PVOID __stdcall rsa_sign_raw( IN PVOID pInBuffer, IN ULONG ulInBufferSize, OUT PULONG pulSignSize, Prsa_secret_key_t skey );

/*
 *	��� �������� �������: �� ������ != 0 ������� �����
 */
int __stdcall rsa_verify_raw( IN PVOID pInBuffer, IN ULONG ulInBufferSize, IN PVOID pInSign, IN ULONG ulSignSize, Prsa_public_key_t pkey );


/*
 *	��� ���������� ���������� ����� � ������
 */
struct tds_KFS_Header *__stdcall rsa_save_public_key_to_memory( IN  Prsa_public_key_t	pk, IN  ULONG ulFlags );

/*
 *	��� ���������� ���������� ����� � ������
 */
struct tds_KFS_Header *__stdcall rsa_save_secret_key_to_memory( IN  Prsa_secret_key_t	sk,
																IN  ULONG				ulFlags );

/*
 * ��������� ��������� ���� �� ������
 */
Prsa_secret_key_t __stdcall rsa_load_secret_key_from_memory( IN  struct tds_KFS_Header	*pHeader,
															 OUT PULONG					pulNumBits  OPTIONAL,
															 OUT PULONG					pulFlags	OPTIONAL );

/*
 *	��� ������������ ������
 */
VOID __stdcall rsa_free_memory( IN PVOID pMem );

#ifdef __cplusplus
}
#endif
#pragma pack(pop)


#endif // _RSA_ROUTINES_H_INCLUDED_