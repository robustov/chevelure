#ifndef _RSA_ROUTINES_KEYGEN_H_INCLUDED_
#define _RSA_ROUTINES_KEYGEN_H_INCLUDED_

#include "rsa_routines.h"

#pragma pack(push, 4)
#ifdef __cplusplus
extern "C" {
#endif

/*
 *	������� ��� ���������
 *	���� ���������� �� ����, �� ������� ��������� �����������
 */
typedef int (__stdcall *pfGenPrimeProgressFunc)( int num_bits, char progress_type, int gen_prime );

/*
 *	��� ��������� ���������� ����� RSA
 *	���� ���������� -1, �� ������� ��� ������� � ��� ���������
 */
int __stdcall rsa_generate_secret_key( Prsa_secret_key_t sk, unsigned nbits, pfGenPrimeProgressFunc	pfProgress OPTIONAL );


#ifdef __cplusplus
}
#endif
#pragma pack(pop)



#endif // _RSA_ROUTINES_KEYGEN_H_INCLUDED_