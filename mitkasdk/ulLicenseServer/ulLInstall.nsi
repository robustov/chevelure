; example2.nsi
;
; This script is based on example1.nsi, but it remember the directory, 
; has uninstall support and (optionally) installs start menu shortcuts.
;
; It will install example2.nsi into a directory that the user selects,

;--------------------------------
!include "WordFunc.nsh"
!include "TextFunc.nsh"

;!insertmacro WordAdd
;!insertmacro WordReplace
;!insertmacro un.WordAdd
;!insertmacro LineFind
!define TEMP1 $R0 ;Temp variable
!define LIC $R1 ;Temp variable


; The name of the installer
Name "UlitkaLicenseServer"
; The file to write
OutFile "$%ULITKABIN%\UlitkaLicenseServer.exe"

ReserveFile "ulLInstall.ini"

; The default installation directory
InstallDir $PROGRAMFILES\UlitkaLicenseServer

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\UlitkaLicenseServer" "Install_Dir"

;--------------------------------

; Pages

Page directory
Page custom SetLicenseDir
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

Function .onInit
	InitPluginsDir
	File /oname=$PLUGINSDIR\ulLInstall.ini "ulLInstall.ini"
;	MessageBox MB_OK $PLUGINSDIR
FunctionEnd

Function SetLicenseDir
;	MessageBox MB_OK $PLUGINSDIR

	ReadRegStr ${LIC} HKLM SOFTWARE\UlitkaLicenseServer "LicenseFile"
;	MessageBox MB_OK ${LIC}
    StrCmp ${LIC} "" empty
      WriteIniStr $PLUGINSDIR\ulLInstall.ini "Field 2" "State" "${LIC}"
    empty:  
	
	Push ${TEMP1}
		InstallOptions::dialog "$PLUGINSDIR\ulLInstall.ini"
    Pop ${TEMP1}

  Pop ${TEMP1}
	
FunctionEnd


;--------------------------------

; The stuff to install
Section "UlitkaLicense"

  SectionIn RO

  ExecWait '"$INSTDIR\ulLicenseServer.exe" "-stop"' $0
  IntCmp $0 3002 done0
  IntCmp $0 3003 done0
  IntCmp $0 0 done0
	MessageBox MB_OK "Error stop service (return $0)";
  done0:
	  
  ExecWait '"$INSTDIR\ulLicenseServer.exe" "-uninstall"' $0
  IntCmp $0 0 done1
  IntCmp $0 2002 done1
    MessageBox MB_OK "Uninstall latest version. return $0";
  done1:


  ReadINIStr ${LIC} "$PLUGINSDIR\ulLInstall.ini" "Field 2" "State"
  
  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  
  ; Put file there
  File "$%ULITKABIN%\binrelease\ulLicenseServer.exe"
  File "installation.txt"

;	MessageBox MB_OK "2"

  ExecWait '"$INSTDIR\ulLicenseServer.exe" "-license" "${LIC}" "-install" '
  ExecWait '"$INSTDIR\ulLicenseServer.exe" "-start" '

;	MessageBox MB_OK "3"

	FileOpen $0 "$INSTDIR\start_as_server.bat" w
	FileWrite $0 `"$INSTDIR\ulLicenseServer.exe"`
	FileWrite $0 ` -license "${LIC}" -server$\r$\n`
	FileWrite $0 `pause`
	FileClose $0
 
	FileOpen $0 "$INSTDIR\start_as_client.bat" w
	FileWrite $0 `"$INSTDIR\ulLicenseServer.exe"`
	FileWrite $0 ` -ini "${LIC}" -client "UlHair"$\r$\n`
	FileWrite $0 `pause`
	FileClose $0
 
  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\UlitkaLicenseServer "Install_Dir" "$INSTDIR"
  WriteRegStr HKLM SOFTWARE\UlitkaLicenseServer "LicenseFile" "${LIC}"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\UlitkaLicenseServer" "DisplayName" "UlitkaLicenseServer"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\UlitkaLicenseServer" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\UlitkaLicenseServer" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\UlitkaLicenseServer" "NoRepair" 1
  WriteUninstaller "uninstall.exe"
 
SectionEnd

;--------------------------------
; Uninstaller
Section "Uninstall"

  ExecWait '"$INSTDIR\ulLicenseServer.exe" "-stop"' $0
  IntCmp $0 3002 done0
  IntCmp $0 3003 done0
  IntCmp $0 0 done0
	MessageBox MB_OK "Error stop service (return $0)";
  done0:
	  
  ExecWait '"$INSTDIR\ulLicenseServer.exe" "-uninstall"' $0
  IntCmp $0 0 done1
    MessageBox MB_OK "Uninstall latest version. return $0";
  done1:
	
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\UlitkaLicenseServer"
  ;DeleteRegKey HKLM SOFTWARE\UlitkaLicenseServer

  ; Remove files and uninstaller
  Delete $INSTDIR\*.*

  ; Remove directories used
  RMDir "$INSTDIR"

SectionEnd

