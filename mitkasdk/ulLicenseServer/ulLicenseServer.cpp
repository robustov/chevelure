// ulLicenseServer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <signal.h>
#include "UlLicense/UlLClient.h"
#include "UlLicense/UlLServer.h"
#include "UlLicense/UlLUtils.h"
#include "UlLicense/UlLLicenseFile.h"
#include "UlLicense/UlLKeyUtil.h"
#include "UlLicense/UlLClientThread.h"
//#include <winnt.h>
#include "ServiceProc.h"
typedef wchar_t* PTCHAR;
#include "rsa_routines_keygen.h"
#include "rsa_routines.h"
#include "md5_routines.h"
#include "sha1_routines.h"
#include "key_files_support.h"
#include "net/macaddress.h"

#include <vector>

UlLClient client;
void SignalHandler(int signal)
{
	client.SendImDead();
    printf("SendImDead\n");
}

// ��� rsa_routines_keygen
static int __stdcall __GenPrimeProgressFunc( int num_bits, char progress_type, int gen_prime )
{
//	if( progress_type!='b')
		printf( "%c", progress_type );
	return( 0 );
}


int _tmain(int argc, _TCHAR* argv[])
{
	bool bClient = false, bThread = false;
	bool bServer = false;
	bool bService = false;
	bool bKeyGen = false;
	bool bMacs = false;
	int key_length;
//	std::string keysfilename;
	std::string publickeyfilename;
	std::string securekeyfilename;

	bool bSign = false;
	std::string fileforsign = "";

	char* filename = argv[0];
	std::string licensefile = UlLsearchLicenseFile(filename);
	std::string licensefileforinstall;

	std::string productname = "";
	std::string inifile = "";

	argc--;argv++;

	for(;argc!=0; argc--,argv++)
	{
		if( strcmp( argv[0], "-macs")==0)
			bMacs = true;

		if( strcmp( argv[0], "-server")==0)
			bServer = true;

		if( strcmp( argv[0], "-service")==0)
			bClient = false, bServer=false, bService=true;
			
		if( strcmp( argv[0], "-client")==0)
		{
			bClient = true, bServer=false;
			argc--,argv++;
			if( argc==0)
			{
				printf("specify client ProductName\n");
				break;
			}
			productname = argv[0];
		}
		if( strcmp( argv[0], "-thread")==0)
			bThread = true;
		
		if( strcmp( argv[0], "-license")==0)
		{
			argc--,argv++;
			if( argc==0)
			{
				printf("specify license file name\n");
				break;
			}
			licensefile = argv[0];
			licensefileforinstall = argv[0];
		}
		if( strcmp( argv[0], "-ini")==0)
		{
			argc--,argv++;
			if( argc==0)
			{
				printf("specify ini file name\n");
				break;
			}
			inifile = argv[0];
		}
		if( strcmp( argv[0], "-install")==0)
		{
			std::string startstring = filename;
			startstring += " -service";
			if(!licensefileforinstall.empty())
			{
				startstring += " -license \"";
				startstring += licensefileforinstall;
				startstring += "\"";
			}
//printf("%s\n", startstring.c_str());
			int res = Install(startstring.c_str());
			return res;
		}
		if( strcmp( argv[0], "-uninstall")==0)
		{
			int res = UnInstall();
			return res;
		}
		if( strcmp( argv[0], "-start")==0)
		{
			int res = RunService();
			return res;
		}
		if( strcmp( argv[0], "-stop")==0)
		{
			int res = KillService();
			return res;
		}
		if( strcmp( argv[0], "-keygen")==0)
		{
			argc--,argv++;
			if( argc==0)
			{
				printf("specify key length and filename for keys\n");
				break;
			}
			key_length = atoi( argv[0]);
			if( (key_length & (key_length-1)) != 0 || 
				key_length<256)
			{
				printf("key must be 256, 512, 1024, ...\n");
				break;
			}
			argc--,argv++;
			if( argc==0)
			{
				printf("specify key length, secure key filename and public key filename\n");
				break;
			}
			securekeyfilename = argv[0];
			argc--,argv++;
			if( argc==0)
			{
				printf("specify key length, secure key filename and public key filename\n");
				break;
			}
			publickeyfilename = argv[0];
			bKeyGen = true;
			continue;
		}
		if( strcmp( argv[0], "-sign")==0)
		{
			argc--,argv++;
			if( argc==0)
			{
				printf("specify SECURE KEY FILE and filename for sign\n");
				break;
			}
			securekeyfilename = argv[0];
			argc--,argv++;
			if( argc==0)
			{
				printf("specify SECURE KEY FILE and filename for sign\n");
				break;
			}
			fileforsign = argv[0];
			bSign = true;
			continue;
		}
	}


	if( bMacs)
	{
		std::vector< std::string> macaddress;
		Net::GetMacAddress(macaddress);
		for(int i=0; i<(int)macaddress.size(); i++)
		{
			printf("%d: %s\n", i, macaddress[i].c_str());
		}
	}
	if( bSign)
	{
		FILE* file = fopen(securekeyfilename.c_str(), "rb");
		if( !file)
		{
			printf("cant read to KEYS FILE %s\n", securekeyfilename.c_str());
			return 1;
		}
		int res;
		ULONG ulSize;
		res = (int)fread(&ulSize, 1, sizeof(ulSize), file);
		std::vector<char> skdata(ulSize);
		res = (int)fread(&skdata[0], 1, ulSize, file);
		Prsa_secret_key_t psk = rsa_load_secret_key_from_memory( (tds_KFS_Header*)&skdata[0], NULL, NULL);

		/*/
		res = fread(&ulSize, 1, sizeof(ulSize), file);
		std::vector<char> pkdata(ulSize);
		res = fread(&pkdata[0], 1, ulSize, file);
		Prsa_public_key_t ppk = rsa_load_public_key_from_memory( (tds_KFS_Header*)&pkdata[0], NULL, NULL );
		/*/

		fclose( file);

		UlLLicenseFile license;
		if( !license.Read(fileforsign.c_str(), true))
		{
			printf("cant read FILE %s\n", fileforsign.c_str());
			return 1;
		}
		license.Sign(psk);

		license.Write(fileforsign.c_str());

		printf("license file %s signed\n", fileforsign.c_str());

		rsa_destroy_secret_key(psk, 1);
//		rsa_destroy_public_key(ppk, 1);
		return 0;
	}
	if( bKeyGen)
	{
		// secure
		FILE* file = fopen(securekeyfilename.c_str(), "wb");
		if( !file)
		{
			printf("cant write to file %s\n", securekeyfilename.c_str());
			return 1;
		}
		rsa_secret_key_t sk;
		memset( &sk, 0, sizeof(sk) );
		int flags = rsa_generate_secret_key( &sk, 512, __GenPrimeProgressFunc );
		PKFS_Header_t ph = rsa_save_secret_key_to_memory( &sk, flags );

		// � ����
		fwrite(&ph->ulSize, sizeof(ph->ulSize), 1, file);
		fwrite(ph, ph->ulSize, 1, file);
		KFS_FreeMemory( ph );
		fclose( file);
		printf("secure key was writed to file %s\n", securekeyfilename.c_str());


		// public
		file = fopen(publickeyfilename.c_str(), "wb");
		if( !file)
		{
			printf("cant write to file %s\n", publickeyfilename.c_str());
			return 1;
		}

		// ������ �� ��������� �������� (������� �������� ����)
		rsa_public_key_t pk;
		memset( &pk, 0, sizeof(pk) );
		rsa_get_public_key( &sk, &pk );
		PKFS_Header_t pph = rsa_save_public_key_to_memory( &pk, 0 );

		// � ����
		fwrite(&pph->ulSize, sizeof(pph->ulSize), 1, file);
		fwrite(pph, pph->ulSize, 1, file);
		KFS_FreeMemory( pph );

		fclose( file);
		printf("public key was writed to file %s\n", publickeyfilename.c_str());
		return 0;
	}
	if( bService)
	{
		ExecuteStartService(licensefile.c_str());
		return 0;
	}
	if(bClient)
	{
		if( bThread)
		{
			static UlLClientThread clientThread;
			static DWORD licflag=0;
			clientThread.StartThread(GetModuleHandle(NULL), productname.c_str(), inifile.c_str(), &licflag);
			for(int i=0; i<100; i++)
			{
				Sleep(100);
				if( licflag==1)
					printf(".");
			}
			clientThread.StopThread();
			return 0;
		}
		typedef void (*SignalHandlerPointer)(int);

		SignalHandlerPointer previousHandler;
		signal(SIGABRT, SignalHandler);
		signal(SIGBREAK, SignalHandler);
		signal(SIGTERM, SignalHandler);

		if( !client.Start(inifile.c_str(), productname.c_str(), true))
			return -1;

		DWORD key;
		client.GetLicense(&key, sizeof(key));

		try
		{
			while(1)
			{
				Sleep(1000);
				client.KeepAliveLoop();
				Sleep(1000);
			}
		}
		catch(...)
		{
			client.SendImDead();
		}
		client.Stop();
		return 0;
	}

	if(bServer)
	{
		UlLServer server;
		Prsa_public_key_t ppk = LoadPublicKeyFromResource(GetModuleHandle(NULL), "PUBLICKEY", "L");
		if( !ppk)
			return -1;
		if( !server.Start(licensefile.c_str(), ppk, true))
			return -1;
		rsa_destroy_public_key(ppk, 1);

		server.Loop();
		server.Stop();
		return 0;
	}
	
	return 0;
}

