#include "stdafx.h"
#include <iostream>
#include <tchar.h>
#include <stdio.h>
#include <WinSock2.h>
#include <windows.h>
#include <winbase.h>

#include <winsvc.h>
#include "ServiceProc.h"
#include "UlLicense/UlLClient.h"
#include "UlLicense/UlLServer.h"
#include "UlLicense/UlLUtils.h"
#include "UlLicense/UlLKeyUtil.h"


SERVICE_STATUS_HANDLE hServiceStatusHandle = NULL;

VOID WINAPI ServiceHandler(DWORD fdwControl);

std::string licenseFileName= "";

const int nBufferSize = 500;
CHAR pServiceName[nBufferSize+1] = "ulitkaLicenseService";
VOID WINAPI ServiceMain(DWORD dwArgc, LPTSTR *lpszArgv);
SERVICE_TABLE_ENTRY lpServiceStartTable[] = 
{
	{pServiceName, ServiceMain},
	{NULL, NULL}
};
char* pLogFile = "c:/ulitka.log";
//char* pLogFile = NULL;


VOID WriteLog(char* pFile, const char* pMsg, long error=-1)
{
	SYSTEMTIME oT;
	::GetLocalTime(&oT);

	std::string message;
	if( error!=-1)
	{
		LPVOID lpMsgBuf;
		if (FormatMessage( 
			FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			FORMAT_MESSAGE_FROM_SYSTEM | 
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			error,
			MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), // Default language
			(LPTSTR) &lpMsgBuf,
			0,
			NULL ))
		{
			message = (LPCTSTR)lpMsgBuf;
			// Free the buffer.
			LocalFree( lpMsgBuf );
		}
	}

	printf("%02d/%02d/%04d, %02d:%02d:%02d\n    %s",oT.wMonth,oT.wDay,oT.wYear,oT.wHour,oT.wMinute,oT.wSecond,pMsg); 
	if(!message.empty())
		printf(message.c_str()); 
	if( pFile)
	{
		FILE* f = fopen(pFile, "at");
		if( f)
		{
			fprintf(f, "%02d/%02d/%04d, %02d:%02d:%02d\n    %s",oT.wMonth,oT.wDay,oT.wYear,oT.wHour,oT.wMinute,oT.wSecond,pMsg); 
			if(!message.empty())
				fprintf(f, message.c_str()); 
			fclose(f);
		}
	}
}

bool ExecuteStartService(const char* license)
{
	licenseFileName = license;
	if(!StartServiceCtrlDispatcher(lpServiceStartTable))
	{
		long nError = GetLastError();
		char pTemp[121];
		sprintf(pTemp, "StartServiceCtrlDispatcher failed, error code = %d\n", nError);
		WriteLog(pLogFile, pTemp, nError);
		return false;
	}
	return true;
}

int Install(const char* pPath)
{  
	int res = 0;
	char* pName = pServiceName;
	SC_HANDLE schSCManager = OpenSCManager( NULL, NULL, SC_MANAGER_CREATE_SERVICE); 
	if (schSCManager==0) 
	{
		long nError = GetLastError();
		char pTemp[121];
		sprintf(pTemp, "OpenSCManager failed, error code = %d\n", nError);
		WriteLog(pLogFile, pTemp, nError);
		res = 1001;
	}
	else
	{
		SC_HANDLE schService = CreateService
		( 
			schSCManager,	/* SCManager database      */ 
			pName,			/* name of service         */ 
			pName,			/* service name to display */ 
			SERVICE_ALL_ACCESS,        /* desired access          */ 
			SERVICE_WIN32_OWN_PROCESS, /* service type            */ 
			SERVICE_AUTO_START,			/* start type              */ 
			SERVICE_ERROR_NORMAL,      /* error control type      */ 
			pPath,			/* service's binary        */ 
			NULL,                      /* no load ordering group  */ 
			NULL,                      /* no tag identifier       */ 
			NULL,                      /* no dependencies         */ 
			NULL,                      /* LocalSystem account     */ 
			NULL
		);                     /* no password             */ 
		if (schService==0) 
		{
			long nError =  GetLastError();
			char pTemp[121];
			sprintf(pTemp, "Failed to create service %s, error code = %d\n", pName, nError);
			WriteLog(pLogFile, pTemp, nError);
			res = 1002;
		}
		else
		{
			char pTemp[121];
			sprintf(pTemp, "Service %s installed\n", pName);
			WriteLog(pLogFile, pTemp);
			CloseServiceHandle(schService); 
			res = 1003;
		}
		CloseServiceHandle(schSCManager);
	}	
	return res;
}

int UnInstall()
{
	int res = 0;
	char* pName = pServiceName;
	SC_HANDLE schSCManager = OpenSCManager( NULL, NULL, SC_MANAGER_ALL_ACCESS); 
	if (schSCManager==0) 
	{
		long nError = GetLastError();
		char pTemp[121];
		sprintf(pTemp, "OpenSCManager failed, error code = %d\n", nError);
		WriteLog(pLogFile, pTemp, nError);
		res = 2001;
	}
	else
	{
		SC_HANDLE schService = OpenService( schSCManager, pName, SERVICE_ALL_ACCESS);
		if (schService==0) 
		{
			long nError = GetLastError();
			char pTemp[121];
			sprintf(pTemp, "OpenService failed, error code = %d\n", nError);
			WriteLog(pLogFile, pTemp, nError);
			res = 2002;
		}
		else
		{
			if(!DeleteService(schService)) 
			{
				char pTemp[121];
				sprintf(pTemp, "Failed to delete service %s\n", pName);
				WriteLog(pLogFile, pTemp, GetLastError());
				res = 2003;
			}
			else 
			{
				char pTemp[121];
				sprintf(pTemp, "Service %s removed\n",pName);
				WriteLog(pLogFile, pTemp);
				res = 0;
			}
			CloseServiceHandle(schService); 
		}
		CloseServiceHandle(schSCManager);	
	}
	DeleteFile(pLogFile);
	return res;
}

int KillService() 
{ 
	int res = 0;
	char* pName = pServiceName;
	// kill service with given name
	SC_HANDLE schSCManager = OpenSCManager( NULL, NULL, SC_MANAGER_ALL_ACCESS); 
	if (schSCManager==0) 
	{
		long nError = GetLastError();
		char pTemp[121];
		sprintf(pTemp, "OpenSCManager failed, error code = %d\n", nError);
		WriteLog(pLogFile, pTemp, nError);
		res = 3001;
	}
	else
	{
		// open the service
		SC_HANDLE schService = OpenService( schSCManager, pName, SERVICE_ALL_ACCESS);
		if (schService==0) 
		{
			long nError = GetLastError();
			char pTemp[121];
			sprintf(pTemp, "OpenService failed, error code = %d\n", nError);
			WriteLog(pLogFile, pTemp, nError);
			res = 3002;
		}
		else
		{
			// call ControlService to kill the given service
			SERVICE_STATUS status;
			if(!ControlService(schService,SERVICE_CONTROL_STOP,&status))
			{
				long nError = GetLastError();
				char pTemp[121];
				sprintf(pTemp, "ControlService failed, error code = %d\n", nError);
				WriteLog(pLogFile, pTemp, nError);
				res = 3003;
			}
			CloseServiceHandle(schService); 
		}
		CloseServiceHandle(schSCManager); 
	}
	return res;
}

int RunService() 
{ 
	int res=0;
	char* pName = pServiceName;
	// run service with given name
	SC_HANDLE schSCManager = OpenSCManager( NULL, NULL, SC_MANAGER_ALL_ACCESS); 
	if (schSCManager==0) 
	{
		long nError = GetLastError();
		char pTemp[121];
		sprintf(pTemp, "OpenSCManager failed, error code = %d\n", nError);
		WriteLog(pLogFile, pTemp, nError);
		res = 4001;
	}
	else
	{
		// open the service
		SC_HANDLE schService = OpenService( schSCManager, pName, SERVICE_ALL_ACCESS);
		if (schService==0) 
		{
			long nError = GetLastError();
			char pTemp[121];
			sprintf(pTemp, "OpenService failed, error code = %d\n", nError);
			WriteLog(pLogFile, pTemp, nError);
			res = 4002;
		}
		else
		{
			// call StartService to run the service
			if( !StartService(schService, 0, (const char**)NULL))
			{
				long nError = GetLastError();
				char pTemp[121];
				sprintf(pTemp, "StartService failed, error code = %d\n", nError);
				WriteLog(pLogFile, pTemp, nError);
				res = 4003;
			}
			CloseServiceHandle(schService); 
		}
		CloseServiceHandle(schSCManager); 
	}
	return res;
}
//PROCESS_INFORMATION pi = {0, 0, 0, 0};
bool bRuning = false;

VOID WINAPI ServiceMain(DWORD dwArgc, LPTSTR *lpszArgv)
{
	DWORD   status = 0; 
    DWORD   specificError = 0xfffffff; 
 
	SERVICE_STATUS ServiceStatus; 
    ServiceStatus.dwServiceType        = SERVICE_WIN32; 
    ServiceStatus.dwCurrentState       = SERVICE_START_PENDING; 
    ServiceStatus.dwControlsAccepted   = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN | SERVICE_ACCEPT_PAUSE_CONTINUE; 
    ServiceStatus.dwWin32ExitCode      = 0; 
    ServiceStatus.dwServiceSpecificExitCode = 0; 
    ServiceStatus.dwCheckPoint         = 0; 
    ServiceStatus.dwWaitHint           = 0; 
 
	hServiceStatusHandle = RegisterServiceCtrlHandler(pServiceName, ServiceHandler); 
    if (hServiceStatusHandle==0) 
    {
		long nError = GetLastError();
		char pTemp[121];
		sprintf(pTemp, "RegisterServiceCtrlHandler failed, error code = %d\n", nError);
		WriteLog(pLogFile, pTemp, nError);
        return; 
    } 
 
    // Initialization complete - report running status 
    ServiceStatus.dwCurrentState       = SERVICE_RUNNING; 
    ServiceStatus.dwCheckPoint         = 0; 
    ServiceStatus.dwWaitHint           = 0;  


	int argc = dwArgc;
	_TCHAR** argv = lpszArgv;

	char filename[512];
	GetModuleFileName(NULL, filename, 512);

	std::string licensefile = UlLsearchLicenseFile(filename);
	if( !licenseFileName.empty())
		licensefile = licenseFileName;

	{
		char pTemp[121];
		sprintf(pTemp, "UlLServer started with licensefile = %s\n", licensefile.c_str());
		WriteLog(pLogFile, pTemp);
	}

	Prsa_public_key_t ppk = LoadPublicKeyFromResource(GetModuleHandle(NULL), "PUBLICKEY", "L");
	if( !ppk)
		return;

	UlLServer server;
	if( !server.Start(licensefile.c_str(), ppk, false, pLogFile))
	{
		WriteLog(pLogFile, "UlLServer dont started\n");
	    ServiceStatus.dwCurrentState  = SERVICE_STOPPED; 
		ServiceStatus.dwWin32ExitCode = 0xda000;
		SetServiceStatus(hServiceStatusHandle, &ServiceStatus);
		return;
	}
	rsa_destroy_public_key(ppk, 1);

    if(!SetServiceStatus(hServiceStatusHandle, &ServiceStatus)) 
    { 
		long nError = GetLastError();
		char pTemp[121];
		sprintf(pTemp, "SetServiceStatus failed, error code = %d\n", nError);
		WriteLog(pLogFile, pTemp, nError);
    } 

	WriteLog(pLogFile, "Start loop");
	server.Loop(pLogFile);
	/*/
	bRuning = true;
	while(bRuning)
	{
		Sleep(500);
		//Place Your Code for processing here....
	}
	/*/

	{
		char pTemp[121];
		sprintf(pTemp, "UlLServer exit\n");
		WriteLog(pLogFile, pTemp);
	}

	/*/
	std::string startstring = licensefile;
	startstring += "-server -license ";
	startstring += licensefile;
	::system(startstring.c_str());

	WriteLog(pLogFile, "UlLServer started with licensefile = \n");
	WriteLog(pLogFile, licensefile.c_str());

	server.Loop();
	server.Stop();
	/*/
	return;
}

VOID WINAPI ServiceHandler(DWORD fdwControl)
{
	{
		char pTemp[121];
		sprintf(pTemp, "ServiceHandler mode=%d\n", fdwControl);
		WriteLog(pLogFile, pTemp);
	}

	SERVICE_STATUS ServiceStatus; 
	BOOL ProcessStarted = TRUE;
	switch(fdwControl) 
	{
		case SERVICE_CONTROL_STOP:
		case SERVICE_CONTROL_SHUTDOWN:
			{
				ProcessStarted = FALSE;
			    ServiceStatus.dwServiceType        = SERVICE_WIN32; 
			    ServiceStatus.dwControlsAccepted   = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN | SERVICE_ACCEPT_PAUSE_CONTINUE; 
			    ServiceStatus.dwServiceSpecificExitCode = 0; 
				ServiceStatus.dwWin32ExitCode = 0; 
				ServiceStatus.dwCurrentState  = SERVICE_STOPPED; 
				ServiceStatus.dwCheckPoint    = 0; 
				ServiceStatus.dwWaitHint      = 0;
			}
			bRuning = false;
			/*/
			if( pi.hProcess)
			{
				TerminateProcess(pi.hProcess, 0);
				pi.hProcess = NULL;
			}
			/*/
			break; 
		case SERVICE_CONTROL_PAUSE:
			ServiceStatus.dwCurrentState = SERVICE_PAUSED; 
			break;
		case SERVICE_CONTROL_CONTINUE:
			ServiceStatus.dwCurrentState = SERVICE_RUNNING; 
			break;
		case SERVICE_CONTROL_INTERROGATE:
			break;
		default:
			break;
	};
    if (!SetServiceStatus(hServiceStatusHandle,  &ServiceStatus)) 
	{ 
		long nError = GetLastError();
		char pTemp[121];
		sprintf(pTemp, "SetServiceStatus failed, error code = %d\n", nError);
		WriteLog(pLogFile, pTemp, nError);
    } 
}

