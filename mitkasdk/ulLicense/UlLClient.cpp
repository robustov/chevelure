#include "StdAfx.h"
#include "ulLicense/UlLclient.h"
#include "ulLicense/UlLIniFile.h"

const char* ulitkaserverlog = "\\\\server2\\ulitka\\ulitkaclients.log";
//const char* ulitkaserverlog = "c:\\ulitkaclients.log";

inline void logprintf( const char* logfilename, const char* text, ...)
{
	char hostname[256];
	gethostname(hostname, 256);

	va_list argList; va_start(argList, text);
	char sbuf[1024];
	_vsnprintf(sbuf, 1024, text, argList);
	sbuf[1023]=0;
	va_end(argList);

	struct tm* gmt;
	time_t t = time(NULL);
	gmt = localtime(&t);
	std::string mes;
	mes += asctime( gmt );
	mes += "\n";
	mes = hostname;
	mes += " report error: ";

	FILE* f = stderr;
	if(logfilename && logfilename[0])
	{
		f = fopen(logfilename, "at");
	}
	if(f)
	{
		fprintf(f, mes.c_str());
		fprintf(f, sbuf);
		fclose(f);
	}
	fprintf(stderr, sbuf);
}

UlLClient::UlLClient(void)
{
	bValid = false;
//	sd = INVALID_SOCKET;
//	bTrace = true;
	bTrace = false;
}
UlLClient::~UlLClient(void)
{
}
bool UlLClient::Start(const char* inifilename, const char* productname, bool bTrace)
{
	bValid = false;
	this->bTrace = bTrace;
	this->productname = productname;

//	std::string addressname;
	// read licensefile
	UlLIniFile inifile;
	if( !inifile.Read(inifilename, true))
		return bValid;

	unsigned short port = 9292;
	if(inifile.port!=0) port = inifile.port;

	if( bTrace)
		printf("Ulitka license: server=%s port=%d\n", inifile.addressname, port);

	// init winsock
	{
		WSADATA w;
		if (WSAStartup(0x0101, &w) != 0)
		{
			int err = WSAGetLastError();
			logprintf( ulitkaserverlog, "Ulitka license: WSAStartup failed err=%d.\n", err);
			return bValid;
		}
	}

	// resolve server address
	{
		hostent* hp = NULL;
		if(inet_addr(inifile.addressname)==INADDR_NONE)
		{
			hp = gethostbyname(inifile.addressname);
		}
		else
		{
			unsigned int addr;
			addr=inet_addr(inifile.addressname);
			hp = gethostbyaddr((char*)&addr,sizeof(addr),AF_INET);
		}
		if(hp==NULL)
		{
			int err = WSAGetLastError();
			logprintf( ulitkaserverlog, "Ulitka license: cant resolve address \"%s\" err=%d\n", inifile.addressname, err);
			WSACleanup();
			return bValid;
		}
		this->server.sin_addr.s_addr=*((unsigned long*)hp->h_addr);
		this->server.sin_family=AF_INET;
		this->server.sin_port=htons(port);
	}
	if( bTrace)
		printf("Ulitka license: address=%d.%d.%d.%d\n", 
			this->server.sin_addr.S_un.S_un_b.s_b1,
			this->server.sin_addr.S_un.S_un_b.s_b2,
			this->server.sin_addr.S_un.S_un_b.s_b3,
			this->server.sin_addr.S_un.S_un_b.s_b4);

	// Get host name of this computer
	{
		char host_name[256];
		gethostname(host_name, sizeof(host_name));
		hostent* hp = gethostbyname(host_name);
		if(hp == NULL)
		{
			int err = WSAGetLastError();
			logprintf( ulitkaserverlog, "Ulitka license: gethostname failed. err=%d\n", err);
			WSACleanup();
			return bValid;
		}
		this->client.sin_addr.s_addr=*((unsigned long*)hp->h_addr);
		this->client.sin_family = AF_INET;
		this->client.sin_port = htons(0);
	}
	if( bTrace)
		printf("Ulitka license: this host=%d.%d.%d.%d\n", 
			this->client.sin_addr.S_un.S_un_b.s_b1,
			this->client.sin_addr.S_un.S_un_b.s_b2,
			this->client.sin_addr.S_un.S_un_b.s_b3,
			this->client.sin_addr.S_un.S_un_b.s_b4);

	bValid = true;
	return bValid;
}
void UlLClient::Stop()
{
//	closesocket(sd);
	WSACleanup();
}
bool UlLClient::GetLicense(void* keydest, int keysize)
{
//fprintf(stderr, "UlLClient: socket\n");
	// Open&bind socket
	SOCKET sd = socket(AF_INET, SOCK_STREAM, 0);
	if( sd == INVALID_SOCKET)
	{
		int err = WSAGetLastError();
		logprintf( ulitkaserverlog, "Ulitka license: socket() failed. err=%d\n", err);
		return false;
	}
//fprintf(stderr, "UlLClient: connect\n");
	/* Bind local address to socket */
	if( connect(sd, (const sockaddr *)&server, sizeof(server))== SOCKET_ERROR)
	{
		int err = WSAGetLastError();
		logprintf( ulitkaserverlog, "Ulitka license: Cannot connect to server.err=%d\n", err);
		shutdown(sd, SD_BOTH);
		closesocket(sd);
		return false;
	}

	// �����:
	UlLD_IMStarted question;
	strncpy( question.question, "�� �� �� ��", sizeof(question.question)-1);
	strncpy( question.productname, this->productname.c_str(), sizeof(question.productname)-1);
	char* send_buffer = (char*)&question;
	int send_buffer_len = sizeof(question);

//fprintf(stderr, "UlLClient: send\n");
	// send
	int snd = send(sd, send_buffer, send_buffer_len, 0);
	if( snd == SOCKET_ERROR)
	{
		int err = WSAGetLastError();
		logprintf( ulitkaserverlog, "Ulitka license: Error transmitting data. err=%d\n", err);
		shutdown(sd, SD_BOTH);
		closesocket(sd);
		return false;
	}
	if( bTrace)
		printf("Ulitka license: send IMStarted = %d bytes to %d.%d.%d.%d\n", snd, 
			this->server.sin_addr.S_un.S_un_b.s_b1,
			this->server.sin_addr.S_un.S_un_b.s_b2,
			this->server.sin_addr.S_un.S_un_b.s_b3,
			this->server.sin_addr.S_un.S_un_b.s_b4);

	// receive
//fprintf(stderr, "UlLClient: rec\n");
	char recbuf[1500];
	int rec = recv(sd, recbuf, (int)sizeof(recbuf), 0);
	if( rec<0)
	{
		int err = WSAGetLastError();
		logprintf( ulitkaserverlog, "Ulitka license: Error receiving data. error=%d\n", err);
		shutdown(sd, SD_BOTH);
		closesocket(sd);
		return false;
	}
	if( bTrace)
		printf("Ulitka license: receive=%d bytes from %d.%d.%d.%d\n", rec, 
			server.sin_addr.S_un.S_un_b.s_b1,
			server.sin_addr.S_un.S_un_b.s_b2,
			server.sin_addr.S_un.S_un_b.s_b3,
			server.sin_addr.S_un.S_un_b.s_b4);

//fprintf(stderr, "UlLClient: gut\n");

	int needlen = sizeof(UlLD_IMStartedReply);
	if( rec!=needlen)
	{
		logprintf( ulitkaserverlog, "Ulitka license: Invalid reply from server. rec=%d need=%d\n", rec, needlen);
		shutdown(sd, SD_BOTH);
		closesocket(sd);
		return false;
	}
	UlLD_IMStartedReply* reply = (UlLD_IMStartedReply*)recbuf;
	
	this->sessionId = reply->session_id;
	if( bTrace)
	{
		printf("Ulitka license: serial: %s\n", reply->serialNumber);
		printf("Ulitka license: session_id=%d\n", reply->session_id);
	}

	// ��� ���������
	// ...
	keysize = __min( keysize, sizeof( reply->licenseflag));
	memcpy( keydest, &reply->licenseflag, keysize);

	shutdown(sd, SD_BOTH);
	closesocket(sd);
	return true;
}
bool UlLClient::KeepAliveLoop()
{
	/*/
	// ���� IAMALIVE
	// Open&bind socket
	SOCKET sd = socket(AF_INET, SOCK_STREAM, 0);
	if( sd == INVALID_SOCKET)
	{
		fprintf(stderr, "Ulitka license: socket() failed.\n");
		return false;
	}
	if( connect(sd, (const sockaddr *)&server, sizeof(server))== SOCKET_ERROR)
	{
		fprintf(stderr, "Ulitka license: Cannot connect to server.\n");
		shutdown(sd, SD_BOTH);
		closesocket(sd);
		return false;
	}

	UlLD_IAMAlive pack;
	pack.session_id = this->sessionId;

	// send
	int snd = send(sd, (char*)&pack, sizeof(pack), 0);
	if( snd == SOCKET_ERROR)
	{
		fprintf(stderr, "Ulitka license: Error transmitting data.\n");
		shutdown(sd, SD_BOTH);
		closesocket(sd);
		return false;
	}
	if( bTrace)
		printf("Ulitka license: send IAMAlive. session_id=%d bytes to %d.%d.%d.%d\n", 
			pack.session_id, 
			this->server.sin_addr.S_un.S_un_b.s_b1,
			this->server.sin_addr.S_un.S_un_b.s_b2,
			this->server.sin_addr.S_un.S_un_b.s_b3,
			this->server.sin_addr.S_un.S_un_b.s_b4);
	shutdown(sd, SD_BOTH);
	closesocket(sd);
	/*/
	return true;
}

bool UlLClient::SendImDead()
{
	// Open&bind socket
	SOCKET sd = socket(AF_INET, SOCK_STREAM, 0);
	if( sd == INVALID_SOCKET)
	{
		int err = WSAGetLastError();
		logprintf( ulitkaserverlog, "Ulitka license: socket() failed. err=%d\n", err);
		return false;
	}
	/* Bind local address to socket */
	if( connect(sd, (const sockaddr *)&server, sizeof(server))== SOCKET_ERROR)
	{
		int err = WSAGetLastError();
		logprintf( ulitkaserverlog, "Ulitka license: Cannot connect to server. err=%d\n", err);
		shutdown(sd, SD_BOTH);
		closesocket(sd);
		return false;
	}

	UlLD_IAMDead pack;
	pack.session_id = this->sessionId;

	// send
	int snd = sendto(sd, (char*)&pack, sizeof(pack), 0, (struct sockaddr *)&server, sizeof(server));
	if( snd == SOCKET_ERROR)
	{
		int err = WSAGetLastError();
		logprintf( ulitkaserverlog, "Ulitka license: Error transmitting data. err=%d\n", err);
		shutdown(sd, SD_BOTH);
		closesocket(sd);
		return false;
	}
	if( bTrace)
		printf("Ulitka license: send IAMDead. session_id=%d bytes to %d.%d.%d.%d\n", 
			pack.session_id, 
			this->server.sin_addr.S_un.S_un_b.s_b1,
			this->server.sin_addr.S_un.S_un_b.s_b2,
			this->server.sin_addr.S_un.S_un_b.s_b3,
			this->server.sin_addr.S_un.S_un_b.s_b4);
	shutdown(sd, SD_BOTH);
	closesocket(sd);
	return true;
}

