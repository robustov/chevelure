#include "StdAfx.h"
#include "ulLicense/UlLUtils.h"
#include "Util/misc_create_directory.h"

// ����� ����� �������� � ���������� ��� ����� � ������
std::string UlLsearchLicenseFile(
	const char* directory)
{
	std::string test = std::string(directory)+"\\ulitka.lic";
	FILE* f = 0;
	f = fopen(test.c_str(), "rt");
	if(f)
	{
		fclose(f);
		return test;
	}

	test = Util::extract_foldername(directory);
	test = test+"\\ulitka.lic";
	f = fopen(test.c_str(), "rt");
	if(f)
	{
		fclose(f);
		return test;
	}
	return "";
}

// ����� ini ����� � ���������� ��� ����� � ������
std::string UlLsearchIniFile(
	const char* directory)
{
	std::string test = std::string(directory)+"\\ulitka.ini";
	FILE* f = 0;
	f = fopen(test.c_str(), "rt");
	if(f)
	{
		fclose(f);
		return test;
	}

	test = Util::extract_foldername(directory);
	test = test+"\\ulitka.ini";
	f = fopen(test.c_str(), "rt");
	if(f)
	{
		fclose(f);
		return test;
	}
	return "";
}


