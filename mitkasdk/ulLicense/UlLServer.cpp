#include "StdAfx.h"
#include "ulLicense/UlLserver.h"
#include "ulLicense/UlLLicenseFile.h"
#include <time.h>
#include <winsock2.h>
#include <Windows.h>
#include "net/macaddress.h"

const char* version = "1.0.0";

inline void logprintf( const char* logfilename, const char* text, ...)
{
	va_list argList; va_start(argList, text);
	char sbuf[1024];
	_vsnprintf(sbuf, 1024, text, argList);
	sbuf[1023]=0;
	va_end(argList);

	struct tm* gmt;
	time_t t = time(NULL);
	gmt = localtime(&t);
	std::string mes = asctime( gmt );

	FILE* f = stderr;
	if(logfilename && logfilename[0])
	{
		f = fopen(logfilename, "at");
	}
	if(!f)
		f = stderr;

	static std::string lastmes = "";
	if( lastmes!=mes)
	{
		fprintf(f, mes.c_str());
		lastmes = mes;
	}

//	fputs("\n", f);
	fprintf(f, sbuf);
//	fputs("\n", f);

	if(f && f != stderr)
		fclose(f);
}

CRITICAL_SECTION CriticalSection; 

UlLServer::UlLServer(void)
{
	validlicense = false;
	bValid = false;
	sd = INVALID_SOCKET;
	bTrace = true;
	next_session_id = 0;

	// Initialize the critical section one time only.
    InitializeCriticalSection(&CriticalSection);
}

UlLServer::~UlLServer(void)
{
    // Release resources used by the critical section object.
    DeleteCriticalSection(&CriticalSection);
}
bool UlLServer::Start(
	const char* licensefile, 
	Prsa_public_key_t ppk, 
	bool bVerbose,
	const char* logfilename
	)
{
	logprintf(logfilename, "\nStart ulLicenseServer. version=%s\n\n\n\n", version);
	logprintf(logfilename, "license %s\n", licensefile);
	
	bValid = false;

//	std::string addressname;
	// read licensefile
	if( !license.Read(licensefile, false, bVerbose))
	{
		logprintf(logfilename, "reading file %s failed\n", licensefile);
		return bValid;
	}

	if( license.Verify(ppk))
		validlicense = true;
	else
		logprintf(logfilename, "Verification failed\n");

	unsigned short port = 9191;
	if(license.port!=0) port = license.port;

	// init winsock
	{
		WSADATA w;
		if (WSAStartup(0x0101, &w) != 0)
		{
			logprintf(logfilename, "WSAStartup failed.\n");
			return bValid;
		}
	}
	// Get host name of this computer
	{
		char host_name[256];
		gethostname(host_name, sizeof(host_name));
		hostent* hp = gethostbyname(host_name);
		if(hp == NULL)
		{
			logprintf(logfilename, "gethostname failed\n");
			WSACleanup();
			return bValid;
		}
		this->server.sin_addr.s_addr=*((unsigned long*)hp->h_addr);
		this->server.sin_family = AF_INET;
		this->server.sin_port = htons(port);
	}
	if( bTrace)
		logprintf(logfilename, "this host=%d.%d.%d.%d:%d\n", 
			this->server.sin_addr.S_un.S_un_b.s_b1,
			this->server.sin_addr.S_un.S_un_b.s_b2,
			this->server.sin_addr.S_un.S_un_b.s_b3,
			this->server.sin_addr.S_un.S_un_b.s_b4, 
			port);

	// Open&bind socket
	{
		this->sd = socket(AF_INET, SOCK_STREAM, 0);
		if( this->sd == INVALID_SOCKET)
		{
			logprintf(logfilename, "socket() failed.\n");
			WSACleanup();
			return bValid;
		}
		/* Bind local address to socket */
		if( bind(sd, (struct sockaddr *)&server, sizeof(server)) == SOCKET_ERROR)
		{
			logprintf(logfilename, "Cannot bind address to socket.\n");
			closesocket(this->sd);
			WSACleanup();
			return bValid;
		}
		if(listen(sd, 100000) == SOCKET_ERROR)
		{
			logprintf(logfilename, "Cannot listen socket.\n");
			closesocket(this->sd);
			WSACleanup();
			return bValid;
		}

	}
	{
		std::vector< std::string> macaddress;
		Net::GetMacAddress(macaddress);
		logprintf(logfilename, "mac adresses:\n");
		for(int i=0; i<(int)macaddress.size(); i++)
		{
			logprintf(logfilename, "mac: %s\n", macaddress[i].c_str());
		}
	}

	/*/
	if( bTrace)
	{
		printf( "license limit: %d\n", license.copyAllow);
	}
	if( license.datestring[0]==0)
	{
		this->licensetime = 0;
	}
	else
	{
		tm dt;
		memset( &dt, 0, sizeof(dt));
		char buf[32];
		strncpy( buf, license.datestring, sizeof(buf)-1);
		dt.tm_year = atoi(buf+04)-1900; buf[04]=0;
		dt.tm_mon  = atoi(buf+02)-1; buf[02]=0;
		dt.tm_mday = atoi(buf+00);
		this->licensetime = mktime(&dt);
		if(this->licensetime==-1)
		{
			printf( "mktime failed\n");
			bValid = false;
		}
	}
	/*/

	bValid = true;
	return bValid;
}
int threadcount = 0;

struct THREADDATA
{
	UlLServer* server;
	SOCKET newsd;
	std::string logfilename;
	sockaddr_in client;
};

DWORD WINAPI ThreadProc(
	LPVOID lpParameter
	);

bool UlLServer::Loop(
	const char* logfilename
	)
{
	if( bTrace)
		logprintf(logfilename, "start loop\n");

	// Loop and get data from clients
	while (1)
	{
		std::string raiseplace="";
		try
		{
			sockaddr_in client;
			int client_length = (int)sizeof(client);

			raiseplace="accept";
			SOCKET newsd = accept( this->sd, (sockaddr*)&client, &client_length);
			if( newsd==SOCKET_ERROR)
			{
				logprintf( logfilename, "accept error=%d\n", 
					WSAGetLastError()
					);
				continue;
			}
			raiseplace="after accept";

			logprintf(logfilename, "accept %d.%d.%d.%d\n",
							client.sin_addr.S_un.S_un_b.s_b1,
							client.sin_addr.S_un.S_un_b.s_b2,
							client.sin_addr.S_un.S_un_b.s_b3,
							client.sin_addr.S_un.S_un_b.s_b4);

			THREADDATA* threaddata = new THREADDATA;
			threaddata->server = this;
			threaddata->newsd = newsd;
			threaddata->logfilename = logfilename?logfilename:"";

			DWORD threadId;
			threadcount++;
			HANDLE hThread = CreateThread(NULL, 0, &::ThreadProc, threaddata, 0, &threadId);
		}
		catch(...)
		{
			logprintf( logfilename, "exception in Loop in %s\n", 
				raiseplace.c_str()
				);
		}
	}
	return true;
}
void UlLServer::Stop()
{
	if(bValid)
	{
		closesocket(sd);
		WSACleanup();
	}
	bValid = false;
}

//const int clientdead_waittime = 300;		// 5min
const int clientdead_waittime = 20;		// 20 sec

// �������� ����� ��������
void UlLServer::UpdateClientCount()
{
	EnterCriticalSection(&CriticalSection); 

	try
	{
		int count = 0;
		time_t current = time(NULL);
		std::map<int, SessionData>::iterator itdead = sessions.end();
		std::map<int, SessionData>::iterator it = sessions.begin();
		for( ;it != sessions.end(); it++)
		{
			SessionData& sd = it->second;
			if(sd.lastalive < current-clientdead_waittime)
				// �������
			{
				itdead = it;
			}
			else
			{
				count++;
			}
		}
		if( itdead!=sessions.end())
			sessions.erase(itdead);

		static int lastcount = 0;
		if( count!=lastcount)
		{
			printf("	clients = %d\n", count);
		}
		lastcount = count;
	}
	catch(...)
	{
	}
	LeaveCriticalSection(&CriticalSection);
}
// ����� �������� ��� ��������
int UlLServer::GetClientCount(const char* productname)
{
	int count = 0;
	time_t current = time(NULL);
	std::map<int, SessionData>::iterator it = sessions.begin();
	for( ;it != sessions.end(); it++)
	{
		SessionData& sd = it->second;
		if(sd.lastalive < current-clientdead_waittime)
			// �������
		{
		}
		else
		{
			if( strcmp(sd.productname, productname)==0)
				count++;
		}
	}
	return count;
}

// �������� �������� ��� ��������
bool UlLServer::Check(
	const char* logfilename, 
	const char* productname, 
	const time_t& clienttime, 
	DWORD& licenseflag
	)
{
	EnterCriticalSection(&CriticalSection); 
	try
	{
		// ����� ������� � ����� ��������
		for( int p=0; p<UlLLicenseFile::maxProductCount; p++)
		{
			UlLLicenseFile::Product& product = license.products[p];
			if( strcmp( product.productname, productname)!=0)
				continue;

			// �������� ����
			time_t licensetime;
			if( product.datestring[0]==0)
			{
				licensetime = 0;
			}
			else
			{
				tm dt;
				memset( &dt, 0, sizeof(dt));
				char buf[32];
				strncpy( buf, product.datestring, sizeof(buf)-1);
				dt.tm_year = atoi(buf+04)-1900; buf[04]=0;
				dt.tm_mon  = atoi(buf+02)-1; buf[02]=0;
				dt.tm_mday = atoi(buf+00);
				licensetime = mktime(&dt);
				if(licensetime==-1)
				{
					logprintf(logfilename, "mktime failed\n");
					licenseflag = 0;
				}
			}

			time_t currenttime = time(NULL);
			if(licensetime)
			{
				if(currenttime > licensetime || clienttime > licensetime)
				{
					logprintf(logfilename, "license expired\n");
					licenseflag = 0;
				}
			}
			// �������� ����� ��������
			int count = GetClientCount(productname);
			if( count>=(int)product.copyAllow)
			{
				logprintf(logfilename, "copy allow for %s expired\n", productname);
				licenseflag = 0;
			}

			LeaveCriticalSection(&CriticalSection);
			return true;
		}
		logprintf(logfilename, "product %s not found\n", productname);

		licenseflag = 0;
	}
	catch(...)
	{
	}
	LeaveCriticalSection(&CriticalSection);
	return false;
}


DWORD WINAPI ThreadProc(
	LPVOID lpParameter
	)
{
	THREADDATA* threaddata = (THREADDATA*)lpParameter;

	UlLServer* server = threaddata->server;
	SOCKET newsd = threaddata->newsd;
	std::string logfilename = threaddata->logfilename;
	sockaddr_in client = threaddata->client;

	delete threaddata;

	if( !server->ThreadProc(newsd, logfilename.c_str(), client))
	{
		shutdown(newsd, SD_BOTH);
	}
	closesocket(newsd);
	threadcount--;
printf("				threads = %d\n", threadcount);
	return 0;
}

bool UlLServer::ThreadProc(SOCKET newsd, const char* logfilename, sockaddr_in& client)
{
	std::string raiseplace="";
	try
	{
		// ���� ��� ������
		raiseplace = "GetMacAddress";
		std::vector< std::string> macaddress;
		Net::GetMacAddress(macaddress);

		// Receive bytes from client
		raiseplace = "recv";
		char buffer[1500];
		int rec = recv(newsd, buffer, sizeof(buffer), 0);
		if (rec < 0)
		{
			logprintf(logfilename, "Could not receive data error=%d\n", 
				WSAGetLastError()
				);
			return false;
		}
		raiseplace = "after recv";
		UlLD_Header* header = (UlLD_Header*)buffer;
		raiseplace = "after recv 1";
		if(header->len!=rec)
		{
			raiseplace = "after recv 2";
			logprintf(logfilename, "Recieving failed. len=%d != %d\n", header->len, rec);
			raiseplace = "after recv 3";
			return false;
		}
		raiseplace = "after recv 4";
		UpdateClientCount();

		raiseplace = "after recv 5";
		switch( header->type)
		{
		case UlLD_IMStarted::TYPE:
			{
				raiseplace = "UlLD_IMStarted";
//				printf("\n");
				UlLD_IMStarted* IMStarted = (UlLD_IMStarted*)header;
				if( bTrace)
				{
//					logprintf(logfilename, "receive IMStarted from %d.%d.%d.%d\n",
//						client.sin_addr.S_un.S_un_b.s_b1,
//						client.sin_addr.S_un.S_un_b.s_b2,
//						client.sin_addr.S_un.S_un_b.s_b3,
//						client.sin_addr.S_un.S_un_b.s_b4);
//					logprintf(logfilename, "Question: \"%s\"\n", IMStarted->question);
					logprintf(logfilename, "	Product:  \"%s\"\n", IMStarted->productname);
				}
				UlLD_IMStartedReply reply;
				reply.session_id = next_session_id++;
				reply.licenseflag = 0;
				{
					// �������� ��� ������
					bool bValidMac = false;
					for( int m=0; m<(int)macaddress.size(); m++)
					{
						if( strcmp( macaddress[m].c_str(), this->license.macaddress)==0)
						{
							bValidMac = true;
//							logprintf(logfilename, "valid MAC address (%s)\n", macaddress[m].c_str(), this->license.macaddress);
						}
//						else
//							logprintf(logfilename, "invalid mac address (%s!=%s)\n", macaddress[m].c_str(), this->license.macaddress);
					}
					if( bValidMac)
						reply.licenseflag = this->validlicense;
					else
						logprintf(logfilename, "invalid mac address (%s)\n", this->license.macaddress);

					// �������� ��������
					this->Check(logfilename, IMStarted->productname, IMStarted->clienttime, reply.licenseflag);

				}
				strcpy( reply.serialNumber, this->license.serialNumber);
				raiseplace = "send";
				int snd = send(newsd, (char*)&reply, (int)sizeof(reply), 0);
				if ( snd != sizeof(reply))
				{
					logprintf(logfilename, "Error sending datagram. error =%d\n", WSAGetLastError());
					shutdown(newsd, SD_BOTH);
					return false;
				}
				raiseplace = "after send";
				if( bTrace)
				{
/*/
					printf("send UlLD_IMStartedReply to %d.%d.%d.%d session_id=%d\n",
						client.sin_addr.S_un.S_un_b.s_b1,
						client.sin_addr.S_un.S_un_b.s_b2,
						client.sin_addr.S_un.S_un_b.s_b3,
						client.sin_addr.S_un.S_un_b.s_b4, 
						reply.session_id);
					if( reply.licenseflag)
						logprintf(logfilename, "client validated\n");
					else
						logprintf(logfilename, "client invalidated\n");
/*/
				}

				raiseplace = "sessions";
				EnterCriticalSection(&CriticalSection); 
				if( reply.licenseflag)
				{
					SessionData& sd = sessions[reply.session_id];
					sd.client = client;
					sd.lastalive = time(NULL);
					strncpy( sd.productname, IMStarted->productname, sizeof(sd.productname)-1);
				}
				LeaveCriticalSection(&CriticalSection);
				UpdateClientCount();
			}
			break;

		case UlLD_IAMAlive::TYPE:
			{
				UlLD_IAMAlive* IAMAlive = (UlLD_IAMAlive*)header;
				/*/
				if( bTrace)
					printf("receive UlLD_IAMAlive. session_id=%d from %d.%d.%d.%d\n",
						IAMAlive->session_id,
						client.sin_addr.S_un.S_un_b.s_b1,
						client.sin_addr.S_un.S_un_b.s_b2,
						client.sin_addr.S_un.S_un_b.s_b3,
						client.sin_addr.S_un.S_un_b.s_b4
						);
				/*/
				EnterCriticalSection(&CriticalSection); 
				if( sessions.find(IAMAlive->session_id)==sessions.end())
				{
//					printf("unknown session_id!!! ignored!\n");
				}
				else
				{
					sessions[IAMAlive->session_id].lastalive = time(NULL);
				}
				LeaveCriticalSection(&CriticalSection);

				if( bTrace)
					printf(".");
				UpdateClientCount();
			}
			break;
		case UlLD_IAMDead::TYPE:
			{
				raiseplace = "UlLD_IAMDead";
				UlLD_IAMAlive* IAMAlive = (UlLD_IAMAlive*)header;


				/*/
				if( bTrace)
					logprintf(logfilename, "receive UlLD_IAMDead. session_id=%d from %d.%d.%d.%d\n",
						IAMAlive->session_id,
						client.sin_addr.S_un.S_un_b.s_b1,
						client.sin_addr.S_un.S_un_b.s_b2,
						client.sin_addr.S_un.S_un_b.s_b3,
						client.sin_addr.S_un.S_un_b.s_b4
						);
				/*/
				raiseplace = "sessions";
				EnterCriticalSection(&CriticalSection); 
				std::map<int, SessionData>::iterator it = sessions.find(IAMAlive->session_id);
				if( it==sessions.end())
				{
//					logprintf(logfilename, "unknown session_id!!! ignored!\n");
				}
				else
				{
					sessions.erase(it);
				}
				LeaveCriticalSection(&CriticalSection);

				UpdateClientCount();
			}
			break;
		}
	}
	catch(...)
	{
		logprintf( logfilename, "exception in thread in %s\n", 
			raiseplace.c_str()
			);
		return false;
	}
	return true;
}
