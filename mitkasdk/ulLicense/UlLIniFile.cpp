#include "StdAfx.h"
#include <tchar.h>
#include <iostream>
#include "ulLicense/UlLIniFile.h"
#include <string>

#include "random.h"
#include "md5_routines.h"
#include "sha1_routines.h"
#include <time.h>
#include <vector>

bool readBinaryFromText(unsigned char* dest, const char* src, int dstsize);
bool readBinaryFromText(std::vector<unsigned char>& dest, const char* src);
bool writeBinaryToText(char* dest, const unsigned char* src, int dstsize, int srcsize);
bool writeBinaryToText(char* dest, const std::vector<unsigned char>& src, int dstsize);

UlLIniFile::UlLIniFile()
{
	memset(this, 0, sizeof(UlLIniFile));
}


bool UlLIniFile::Read(
	const char* filename, 
	bool bDump
	)
{
	char buf[2048];
	FILE* file = fopen(filename, "rt");
	if( !file)
	{
		fprintf(stderr, "Ulitka license: cant open ini file \"%s\".\n", filename);
		return bValid;
	}
	// ������ ������ - ����� �������:���� (���� �� ��������� 9191)
	char addressport[256];
	if( !fgets(addressport, 256, file))
	{
		fprintf(stderr, "Ulitka license: invalid ini file \"%s\".\n", filename);
		fclose(file);
		return bValid;
	}
	char* seps = ":\r\n \t";
	char* sepsn = "\r\n";
	char* s0 = strtok(addressport, seps);
	if( !s0)
	{
		fprintf(stderr, "Ulitka license: invalid address or port in ini file \"%s\".\n", filename);
		fclose(file);
		return bValid;
	}
	strncpy( this->addressname, s0, sizeof(this->addressname)-1);
	char* s1 = strtok( NULL, seps );
	if( s1)
		port = (unsigned short)atoi(s1);
	else
		port = 0;

	fclose(file);
	bValid = true;
	return bValid;
}
