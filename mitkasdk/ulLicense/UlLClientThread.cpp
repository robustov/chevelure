#include "StdAfx.h"
#include "ulLicense/UlLClientThread.h"
#include "ulLicense/UlLUtils.h"

const DWORD timeout = 5000;

UlLClientThread::UlLClientThread()
{
	thisModule = INVALID_HANDLE_VALUE;
	hThread = INVALID_HANDLE_VALUE;
	threadId = 0;
	hEvent = INVALID_HANDLE_VALUE;

	inifile = "";
	bTrace = false;

	licenseflag = NULL;
}

UlLClientThread::~UlLClientThread()
{
	StopThread();
}
bool UlLClientThread::StartThread(
	HANDLE thisModule, 
	const char* productname,
	const char* inifilename,
	DWORD* licenseflag,
	bool bTrace
	)
{
	if(this->hThread == INVALID_HANDLE_VALUE)
	{
		this->thisModule = thisModule;
		this->productname = productname?productname:"";
		if( inifilename)
			this->inifile = inifilename;
		this->licenseflag = licenseflag;
		this->bTrace = bTrace;

		this->hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
		this->hThread = CreateThread(NULL, 0, &this->ThreadProc, this, 0, &threadId);

		return true;
	}
	return false;
}
bool UlLClientThread::WaitForStart(
	bool bExitIfError
	)
{
	if(hThread == INVALID_HANDLE_VALUE)
	{
		fprintf(stderr, "Ulitka license: stop!\n");
		if( bExitIfError)
			exit(-1);
	}
	if( this->hEvent!=INVALID_HANDLE_VALUE)
	{
		DWORD ret = WaitForSingleObject(this->hEvent, timeout);
		if( ret==WAIT_TIMEOUT)
		{
			fprintf(stderr, "Ulitka license: start timeout > %d\n", timeout);
			hThread = INVALID_HANDLE_VALUE;
			if( bExitIfError)
				exit(-1);
			return false;
		}
		if( ret!=WAIT_OBJECT_0)
		{
			fprintf(stderr, "Ulitka license: Start error\n");
			hThread = INVALID_HANDLE_VALUE;
			if( bExitIfError)
				exit(-1);
			return false;
		}
		if( hThread== INVALID_HANDLE_VALUE)
		{
			if( bExitIfError)
				exit(-1);
			return false;
		}
		return true;
	}
	return false;
}

void UlLClientThread::StopThread()
{
	if(hThread != INVALID_HANDLE_VALUE)
	{
//		fprintf(stderr, "Ulitka license: PostThreadMessage Quit\n");
		ResetEvent(hEvent);
		PostThreadMessage(threadId, WM_QUIT, 0, 0);

		DWORD ret = WaitForSingleObject(hEvent, 500);
//		if( ret==WAIT_TIMEOUT)
//			fprintf(stderr, "Ulitka license: stop timeout > %d\n", timeout);
//		if( ret!=WAIT_OBJECT_0)
//			fprintf(stderr, "Ulitka license: Stop error\n");
		hThread = INVALID_HANDLE_VALUE;
		licenseflag = 0;

		hThread = INVALID_HANDLE_VALUE;
	}
}

DWORD UlLClientThread::ThreadLoop()
{
//fprintf(stderr, "UlLClientThread: start\n");
	char filename[512];
	GetModuleFileName((HMODULE)this->thisModule, filename, 512);

	if( !this->inifile.empty())
		strncpy( filename, this->inifile.c_str(), 512);
	std::string inifile = UlLsearchIniFile(filename);

	if( !inifile.empty())
	{
//fprintf(stderr, "UlLClientThread: start client\n");
		UlLClient client;
		if( !client.Start(inifile.c_str(), productname.c_str(), bTrace))
		{
			fprintf(stderr, "Ulitka license: start failed\n");
			hThread = INVALID_HANDLE_VALUE;
			SetEvent(hEvent);
			return -1;
		}

//		fprintf(stderr, "Ulitka license: started 1!\n");

		if( licenseflag)
		{
//fprintf(stderr, "UlLClientThread: GetLicense\n");
			if( !client.GetLicense(licenseflag, sizeof(DWORD)))
			{
				hThread = INVALID_HANDLE_VALUE;
				fprintf(stderr, "Ulitka license: start failed\n");
				SetEvent(hEvent);
				return -1;
			}
		}

//		fprintf(stderr, "Ulitka license: started 2!\n");
		SetEvent(hEvent);

		while(1)
		{
			DWORD res = MsgWaitForMultipleObjectsEx(0, NULL, 1000, QS_ALLEVENTS, 0);
			if( res != WAIT_TIMEOUT)
				break;
			client.KeepAliveLoop();
		}
		client.SendImDead();
//		fprintf(stderr, "Ulitka license: exit ThreadProc\n");
	}
	else
	{
		fprintf(stderr, "Ulitka license: cant find license file for %s\n", filename);
	}
	PulseEvent(hEvent);
	CloseHandle(hEvent);
	return 0;
}


DWORD WINAPI UlLClientThread::ThreadProc(
	LPVOID lpParameter)
{
	UlLClientThread* pThis = (UlLClientThread*)lpParameter;
	DWORD res = pThis->ThreadLoop();
	return res;
}

