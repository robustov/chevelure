// Test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "hairProcessor.h"
#include "hairRender.h"
#include "mathNpixar\IPrman.h"


int main(int argc, char* argv[])
{
	printf("%d\n", sizeof(unsigned));
	char* furDataFile = argv[1];
	std::string ribname = furDataFile;
	ribname += ".rib";

	if( argc>=3)
		ribname = argv[2];

	HairRender furData;
	if( !furData.Load(furDataFile))
	{
		fprintf(stderr, "Fur: Loading %s failed\n", furDataFile);
		return NULL;
	}
	fprintf(stderr, "Fur: Loading succesed\n");

#ifndef _DEBUG
	getIPrmanDll prmanproc("IPrman.dll", "getIPrman");
#else
	getIPrmanDll prmanproc("IPrmanD.dll", "getIPrman");
#endif
	if( !prmanproc.isValid())
		return -1;
	IPrman* prman = (*prmanproc)();
	prman->bExecuteProcedural = true;

	RtToken names[] = 
	{
		"format",
		"compression"
	};
	prman->RiBegin( (RtToken)ribname.c_str());

	RtString format = "ascii";
	prman->RiOption(( RtToken ) "rib", ( RtToken ) "format", &format, RI_NULL);
	RtString compression = "none";
	prman->RiOption(( RtToken ) "rib", ( RtToken ) "compression", &compression, RI_NULL);

	furData.GenRIB(prman);
	prman->RiEnd( );

}

