#include "stdafx.h"

#include <Shellapi.h>

#include <ri.h>

//#define HAIR_TRACELOG

//#include "HairDSO.h"
#include "hairProcessor.h"
#include "hairRender.h"
//#include "mathNpixar\IPrman_impl.h"

#include "HairRenderProc.h"
#include "hairProcessor.h"
#include "hairRender.h"

/**************************************************/
#ifdef WIN32
#define export __declspec(dllexport)
#else
#define export
#endif

//Prman_Impl prman;

extern "C" {
	export RtPointer ConvertParameters(RtString paramstr);
	export RtVoid Subdivide(RtPointer data, RtFloat detail);
	export RtVoid Free(RtPointer data);
}


getIPrmanDll prmanproc;
IPrman* prman = NULL;


static bool CommandLineToArgv(const char* cmdline, std::vector<std::string>& args)
{
	std::vector<wchar_t> paramstrw(strlen(cmdline)*3);
	MultiByteToWideChar(CP_ACP, 0, cmdline, -1, &paramstrw.front(), paramstrw.size());
	int argc = 0;
	LPWSTR *argvw = CommandLineToArgvW(&paramstrw.front(), &argc);
	if (!argvw)
		return false;

	for (int i = 0; i < argc; ++i)
	{
		std::vector<char> arg(wcslen(argvw[i])+2);
		WideCharToMultiByte(CP_ACP, 0, argvw[i], wcslen(argvw[i]), &arg.front(), arg.size(), 0, false);
		args.push_back(&arg.front());
	}

	LocalFree(argvw);
	return true;
}


RtPointer ConvertParameters(RtString paramstr) 
{
/*
	printf("paramstr: %s\n", paramstr);
//*/
	
	bool bRFM = false;

	std::string parameters(paramstr);
	std::vector<std::string> args;
	std::vector<std::string> files;
	std::vector<float> motion;
	if( strncmp( paramstr, "#RFM# ", strlen("#RFM# "))==0)
	{
		parameters = paramstr+strlen("#RFM# ");
		bRFM = true;
	}
	if (!CommandLineToArgv(parameters.c_str(), args))
	{
		fprintf(stderr, "HairDSO: Parsing args %s failed\n", paramstr);
		return nullptr;
	}

/*
	printf("args:\n");
	for (int i = 0; i < args.size(); ++i)
		printf("%s\n", args[i].c_str());
//*/

	if (args.size() < 2)
	{
		fprintf(stderr, "HairDSO: Wrong args format, first two args must specify files and motion times count");
		return nullptr;
	}
	int filesCount = atoi(args[0].c_str());
	int motionTimesCount = atoi(args[1].c_str());

	if (filesCount != motionTimesCount && motionTimesCount != 0)
	{
		fprintf(stderr, "HairDSO: Wrong args format: files (%d) and motion times (%d) count must be equal or motion times count must be zero",
				filesCount, motionTimesCount);
		return nullptr;
	}

	int left = args.size() - 2;
	if (filesCount + motionTimesCount != left)
	{
		fprintf(stderr, "HairDSO: Wrong args format: files (%d) and motion times (%d) count does not corresponds with next args passed (%d)",
				filesCount, motionTimesCount, left);
		return nullptr;
	}

	for (int i = 0; i < filesCount; ++i)
		files.push_back(args[2+i]);
	for (int i = 0; i < motionTimesCount; ++i)
		motion.push_back(atof(args[2+filesCount+i].c_str()));

/*
		printf("files:\n");
		for (int i = 0; i < files.size(); ++i)
			printf("%s\n", files[i].c_str());
		printf("motion:\n");
		for (int i = 0; i < motion.size(); ++i)
			printf("%f\n", motion[i]);
//*/
	
	if( !prman)
	{
		#ifdef _DEBUG
		{
			if(!bRFM)
				prmanproc.Load("IPrmanDSOD.dll", "getIPrman");
			else
				prmanproc.Load("IPrmanRMSD.dll", "getIPrman");
		}
		#else
		{
			if(!bRFM)
				prmanproc.Load("IPrmanDSO.dll", "getIPrman");
			else
				prmanproc.Load("IPrmanRMS.dll", "getIPrman");
		}
		#endif
		if( prmanproc.isValid())
			prman = (*prmanproc)();
	}

	try
	{
		HairRender* furData = HairRender::creator();
		if (bRFM)
		{
			if( !furData->Load2(files, motion))
			{
				fprintf(stderr, "HairDSO: Loading failed\n");
				return NULL;
			}
		}
		else
		{
			if( !furData->Load2(files, motion))
			{
				fprintf(stderr, "HairDSO: Loading failed\n");
				return NULL;
			}
		}
		return static_cast<RtPointer>(furData);
	}
	catch(...)
	{
		fprintf(stderr, "HairDSO: Loading %s exception\n", parameters.c_str());
	}
	return NULL;
}

RtVoid Subdivide(RtPointer data, RtFloat detail) 
{
	if( !data) return;

//fprintf(stderr, "Subdivide\n");
	try
	{
		HairRender* furData = static_cast<HairRender*>(data);
		furData->GenRIB(prman);
	}
	catch(...)
	{
		fprintf(stderr, "HairDSO: GenRIB exception\n");
	}
}

RtVoid Free(RtPointer data) 
{
	if( !data) return;
	try
	{
		HairRender* furData = static_cast<HairRender*>(data);
		furData->release();
	}
	catch(...)
	{
		fprintf(stderr, "HairDSO: Free exception\n");
	}
//fprintf(stderr, "Free complete\n");
}

BOOL APIENTRY DllMain( HANDLE hModule, 
					   DWORD  ul_reason_for_call, 
					   LPVOID lpReserved
					 )
{
//	fprintf(stdout, "Fur: DllMain %d\n", ul_reason_for_call);
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		{
#ifdef HAIR_TRACELOG
			// ��� �������� �����
			char filename[512]; GetModuleFileName( (HMODULE)hModule, filename, 512); fprintf(stderr, "%s\n", filename);
#endif
			break;
		}
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}
