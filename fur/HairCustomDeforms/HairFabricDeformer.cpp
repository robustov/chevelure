#include "stdafx.h"

#include "MathNMaya/ParamTex.h"
#include "IHairDeformer.h"

class HairFabricDeformer : public IHairDeformer
{
//	Math::ParamTex<unsigned char> radius;
	float radius;
	bool bottom;
	int dimU, dimV;
	Math::ParamTex2d<unsigned char> color;

public:
	virtual int getType(
		)
	{
		return PARSEHAIR;
	}
	virtual void release()
	{
		delete this;
	};

public:
	// �������� ��������� ������
	virtual void ParseHair(
		float affect, 
		HAIRPARAMS& c_hairs,
		bool bClump
		);
	// ���������� ������ �� ����������, ������ true ���� ������� � ������� ����������� �� �����
	// ��� ���� ���� ��� ������ true ������� ��������� �� ����������
	virtual bool BuildHair(
		float affect, 
		const HAIRPARAMS& c_hairs, 			// ��������� ������
		int SegmCount,					// ����� ���������
		HAIRVERTS& hair					// ���������
		);
public:
	// �������������
	virtual void init(
		);
	// ������ �� UI
	virtual void fromUI(
		artist::IShape* shape
		);
	// ������������
	virtual void serialise(
		Util::Stream& out
		);

};
// /export:getFabricDeformer
IHairDeformer* getFabricDeformer()
{
	return new HairFabricDeformer();
}
enum enLoopType
{
	NORMAL = 0,
	LEFT = 1,
	RIGHT = 2,
	FREE = 3
};
// �������� ��������� ������
void HairFabricDeformer::ParseHair(
	float affect, 
	HAIRPARAMS& c_hairs,
	bool bClump
	)
{
	Math::Vec2f v = polar2vector(c_hairs.polar);

	float c = color.getValue(c_hairs.u, c_hairs.v);

	int x = (int)(c_hairs.u * dimU);
	int y = (int)(c_hairs.v * dimV);
	
	enLoopType type = NORMAL;

	bool inv = bottom;
	if( c<0.5f)
	{
		inv = !inv;
		type = FREE;
	}
	else
	{
		if( ((x+y) & 0x1) == 1) inv = !inv;
	}

	c_hairs.position.x -= c_hairs.length*0.25f*v.x;
	c_hairs.position.z += c_hairs.length*0.25f*v.y;

	float r = radius;
	float sinA = c_hairs.length/(2*r);
	float A = asin( sinA);
	if(2*r<=c_hairs.length)
	{
		A = (float)( M_PI/2);
		sinA = 1;
		r = c_hairs.length/2;
	}

	switch( type)
	{
	case NORMAL:
		{
		//	float r = radius.getValue(c_hairs.u, c_hairs.v);

			float inclination = 1 - (float)(2*A/M_PI);
			if(inv)
				inclination = 1 + (float)(2*A/M_PI);
			float length = r*A;
			float curl = 2*A;
			if(inv)
				curl = -curl;

			c_hairs.length = length;
			c_hairs.inclination = v*inclination;
			c_hairs.baseCurl = v*curl;
			c_hairs.tipCurl = v*curl;
			break;
		}
	case FREE:
		{
			float displace = (1-cos(A))*r*0.5f;
			if(inv) displace = -displace;
			float length = c_hairs.length*0.5f;

			c_hairs.displace = displace;
			c_hairs.length = length;
			c_hairs.inclination = v;
			c_hairs.baseCurl = v*0.f;
			c_hairs.tipCurl = v*0.f;
			break;
		}
	}
}

// ���������� ������ �� ����������, ������ true ���� ������� � ������� ����������� �� �����
// ��� ���� ���� ��� ������ true ������� ��������� �� ����������
bool HairFabricDeformer::BuildHair(
	float affect, 
	const HAIRPARAMS& c_hairs, 			// ��������� ������
	int SegmCount,					// ����� ���������
	HAIRVERTS& hair					// ���������
	)
{
	return false;
}

void HairFabricDeformer::init(		
	)
{
}

// ������ �� UI
void HairFabricDeformer::fromUI(
	artist::IShape* shape
	)
{
//	shape->getValue(radius, "radius", 2.f);
	shape->getValue(radius, "radius", sn::UNIFORM_SHAPE, 0.5f, 0, 1);
	shape->getValue(bottom, "bottom", sn::UNIFORM_SHAPE, false);
	shape->getValue(dimU, "dimU", sn::UNIFORM_SHAPE, 200, 0, 2000);
	shape->getValue(dimV, "dimV", sn::UNIFORM_SHAPE, 200, 0, 2000);
	shape->getValue(color, "color", 0);
	
}
// ������������
void HairFabricDeformer::serialise(
	Util::Stream& out
	)
{
	int version = 0;
	out >> version;
	if( version>=0)
	{
		out>>radius;
		out>>bottom;
		out>>dimU;
		out>>dimV;
		out>>color;
	}
}
