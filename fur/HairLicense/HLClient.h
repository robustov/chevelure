#pragma once
#include <winsock.h>
#include <string>
#include "HLDatagram.h"

class HLClient
{
	// 
	bool bValid;
	bool bTrace;
	// adress port
	sockaddr_in server;
	sockaddr_in client;
	//
	SOCKET sd;
	// id ������ �� �������
	int sessionId;
public:
	HLClient(void);
	~HLClient(void);

	bool Start(const char* licensefile, bool bTrace=false);
	void Stop();

	bool GetLicense(void* keydest, int keysize);
	bool KeepAliveLoop();
	bool SendImDead();
};
