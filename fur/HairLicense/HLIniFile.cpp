#include "StdAfx.h"
#include <tchar.h>
#include <iostream>
#include "HLIniFile.h"
#include <string>

HLIniFile::HLIniFile()
{
	memset(this, 0, sizeof(HLIniFile));
}

bool HLIniFile::Read(const char* filename, bool bDump)
{
	if( bDump)
	{
		fprintf(stdout, "Dmitry Robustov (mailto:robustov@mail.ru)\n\n");
		fprintf(stdout, "license file: %s\n", filename);
	}

	bValid = false;
	memset(this, 0, sizeof(HLIniFile));

	char buf[2048];
	FILE* file = fopen(filename, "rt");
	if( !file)
	{
		fprintf(stderr, "Chevelure license: cant open license file \"%s\".\n", filename);
		return bValid;
	}
	// ������ ������ - ����� �������:���� (���� �� ��������� 9191)
	char addressport[256];
	if( !fgets(addressport, 256, file))
	{
		fprintf(stderr, "Chevelure license: invalid license file \"%s\".\n", filename);
		fclose(file);
		return bValid;
	}
	char* seps = ":\r\n \t";
	char* sepsn = "\r\n";
	char* s0 = strtok(addressport, seps);
	if( !s0)
	{
		fprintf(stderr, "Chevelure license: invalid address or port in license file \"%s\".\n", filename);
		fclose(file);
		return bValid;
	}
	strncpy( this->addressname, s0, sizeof(this->addressname)-1);
	char* s1 = strtok( NULL, seps );
	if( s1)
		port = (unsigned short)atoi(s1);
	else
		port = 0;

	fclose(file);
	bValid = true;
	return bValid;
}
