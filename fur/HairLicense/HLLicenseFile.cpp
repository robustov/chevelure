#include "StdAfx.h"
#include <tchar.h>
#include <iostream>
#include "HLLicenseFile.h"
#include <string>

#include "random.h"
#include "md5_routines.h"
#include "sha1_routines.h"
#include <time.h>

bool readBinaryFromText(unsigned char* dest, const char* src, int dstsize);
bool readBinaryFromText(std::vector<unsigned char>& dest, const char* src);
bool writeBinaryToText(char* dest, const unsigned char* src, int dstsize, int srcsize);
bool writeBinaryToText(char* dest, const std::vector<unsigned char>& src, int dstsize);

struct md5_sha1
{
	unsigned char md5[MD5_DIGEST_LENGTH_UCHAR];
	unsigned char sha1[SHA1_DIGEST_LENGTH_UCHAR];
};

HLLicenseFile::HLLicenseFile()
{
	Clear();
}
void HLLicenseFile::Clear(
	)
{
	memset(addressname, 0, sizeof(addressname));
	port = 0;
	memset(macaddress, 0, sizeof(macaddress));
	memset(serialNumber, 0, sizeof(serialNumber));
	copyAllow = 0;
	memset(datestring, 0, sizeof(datestring));
	random = 0;
	bValid = false;
}

bool HLLicenseFile::Read(const char* filename, bool unsignedAvailable, bool bDump)
{
	if( bDump)
	{
		fprintf(stdout, "Chevelure license\nversion %s\n", CHEVELURE_LIC_SERVER_VERSION);
		fprintf(stdout, "Dmitry Robustov (mailto:robustov@mail.ru)\n\n");
		fprintf(stdout, "license file: %s\n", filename);
	}

	Clear();

	char buf[2048];
	FILE* file = fopen(filename, "rt");
	if( !file)
	{
		fprintf(stderr, "Chevelure license: cant open license file \"%s\".\n", filename);
		return bValid;
	}
	// ������ ������ - ����� �������:���� (���� �� ��������� 9191)
	char addressport[256];
	if( !fgets(addressport, 256, file))
	{
		fprintf(stderr, "Chevelure license: invalid license file \"%s\".\n", filename);
		fclose(file);
		return bValid;
	}
	char* seps = ":\r\n \t";
	char* sepsn = "\r\n";
	char* s0 = strtok(addressport, seps);
	if( !s0)
	{
		fprintf(stderr, "Chevelure license: invalid address or port in license file \"%s\".\n", filename);
		fclose(file);
		return bValid;
	}
	strncpy( this->addressname, s0, sizeof(this->addressname)-1);
	char* s1 = strtok( NULL, seps );
	if( s1)
		port = (unsigned short)atoi(s1);
	else
		port = 0;

	// ������ ������ - ��� �����
	if( !fgets(buf, sizeof(buf), file))
	{
		fprintf(stderr, "Chevelure license: invalid macaddress in license file \"%s\".\n", filename);
		fclose(file);
		return bValid;
	}
	s1 = strtok( buf, sepsn );
	if( s1)
	{
		_strupr(s1);
		for(int i=0, n=0; s1[i]!=0; i++)
		{
			if( (s1[i]>='0' && s1[i]<='9') ||
				(s1[i]>='A' && s1[i]<='F'))
			{
				this->macaddress[n++] = s1[i];
			}
		}
	}


	// �������� ����� ��������
	if( !fgets(buf, sizeof(buf), file))
	{
		fprintf(stderr, "Chevelure license: invalid serialNumber in license file \"%s\".\n", filename);
		fclose(file);
		return bValid;
	}
	s1 = strtok( buf, sepsn );
	if( s1)
		strncpy( this->serialNumber, s1, sizeof(this->serialNumber)-1);

	if( bDump)
	{
		fprintf(stdout, "FOR: %s\n", this->serialNumber);
		fprintf(stdout, "server: %s, port: %d\n", this->addressname, port);
	}

	// ������� ����� ���������
	if( !fgets(buf, sizeof(buf), file))
	{
		fprintf(stderr, "Chevelure license: invalid copyAllow in license file \"%s\".\n", filename);
		fclose(file);
		return bValid;
	}
	this->copyAllow = atoi(buf);
	if( bDump)
		fprintf(stdout, "copy allow: %d\n", this->copyAllow);

	// ���� ���������
	if( !fgets(buf, sizeof(buf), file))
	{
		fprintf(stderr, "Chevelure license: invalid Date in license file \"%s\".\n", filename);
		fclose(file);
		return bValid;
	}
	memset(this->datestring, 0, sizeof(this->datestring));
//	this->date = 0;
	if(buf[0]!=0 && buf[0]!=0xa)
	{
		tm dt;
		memset( &dt, 0, sizeof(dt));
		strncpy( this->datestring, buf, sizeof(this->datestring)-1);
		dt.tm_year = atoi(buf+04)-1900; buf[04]=0;
		dt.tm_mon  = atoi(buf+02)-1; buf[02]=0;
		dt.tm_mday = atoi(buf+00);
		time_t date = mktime(&dt);
		if( date==-1) 
		{
			fprintf(stderr, "Chevelure license: invalid Date in license file \"%s\".\n", filename);
			fclose(file);
			return bValid;
		}
		this->datestring[8] = 0;
		if( bDump)
			fprintf(stdout, "finish time: %s", asctime(&dt));
	}

	// ��������� ����� 32 �����
	if( !fgets(buf, sizeof(buf), file))
	{
		if( unsignedAvailable)
			bValid = true;
		else 
			fprintf(stderr, "Chevelure license: invalid random field in license file \"%s\".\n", filename);
		fclose(file);
		return bValid;
	}
	if( bDump)
		fprintf(stdout, "r: %s\n", buf);
	if( !readBinaryFromText( (unsigned char*)&this->random, buf, sizeof(this->random)))
	{
		if( unsignedAvailable)
			bValid = true;
		else 
			fprintf(stderr, "Chevelure license: invalid random sign field in license file \"%s\".\n", filename);
		fclose(file);
		return bValid;
	}

	// ������� ���������� �����
	if( !fgets(buf, sizeof(buf), file))
	{
		if( unsignedAvailable)
			bValid = true;
		else 
			fprintf(stderr, "Chevelure license: invalid random sign field in license file \"%s\".\n", filename);
		fclose(file);
		return bValid;
	}
	char dump[16];
	strncpy(dump, buf, 15);dump[15]=0;
	if( bDump)
		fprintf(stdout, "r sign: %s...(%d)\n", dump, strlen(buf));
	if( !readBinaryFromText(random_sign, buf))
	{
		if( unsignedAvailable)
			bValid = true;
		else 
			fprintf(stderr, "Chevelure license: invalid random sign field in license file \"%s\".\n", filename);
		fclose(file);
		return bValid;
	}
	
	// ������� ���� ������� �����
	if( !fgets(buf, sizeof(buf), file))
	{
		if( unsignedAvailable)
			bValid = true;
		else 
			fprintf(stderr, "Chevelure license: invalid sign field in license file \"%s\".\n", filename);
		fclose(file);
		return bValid;
	}
	strncpy(dump, buf, 15);dump[15]=0;
	if( bDump)
		fprintf(stdout, "f sign: %s...(%d)\n", dump, strlen(buf));
	if( !readBinaryFromText(sign, buf))
	{
		if( unsignedAvailable)
			bValid = true;
		else 
			fprintf(stderr, "Chevelure license: invalid random sign field in license file \"%s\".\n", filename);
		fclose(file);
		return bValid;
	}

	fclose(file);
	bValid = true;
	return bValid;
}
bool HLLicenseFile::Write(const char* filename, bool bWriteSign)
{
	char buf[1024];
	FILE* file = fopen(filename, "wt");
	if( !file)
	{
		fprintf(stderr, "Chevelure license: cant open license file \"%s\".\n", filename);
		return false;
	}

	fputs(addressname, file);
	if(port!=0)
	{
		fputs(":", file);
		_ultoa(port, buf, 10); 
		fputs(buf, file);
	}
	fputs("\n", file);

	fputs(macaddress, file);
	fputs("\n", file);
	fputs(serialNumber, file);
	fputs("\n", file);
	_ultoa(copyAllow, buf, 10); fputs(buf, file);
	fputs("\n", file);
	fputs(this->datestring, file);
//	if( this->date!=0) 
//	{
//		tm* dt = localtime(&this->date);
//		fprintf(file, "%02d%02d%04d", dt->tm_mday, dt->tm_mon+1, dt->tm_year+1900);
//	}
	fputs("\n", file);

	if( bWriteSign)
	{
		// random
		if( !writeBinaryToText(buf, (const unsigned char*)&random, sizeof(buf), sizeof(random)))
		{
			fprintf(stderr, "Chevelure license: cant write random sign field in license file \"%s\".\n", filename);
			fclose(file);
			return false;
		}
		fputs(buf, file);
		fputs("\n", file);

		// random_sign
		if( !writeBinaryToText(buf, random_sign, sizeof(buf)))
		{
			fprintf(stderr, "Chevelure license: cant write random sign field in license file \"%s\".\n", filename);
			fclose(file);
			return false;
		}
		fputs(buf, file);
		fputs("\n", file);
		
		// sign
		if( !writeBinaryToText(buf, sign, sizeof(buf)))
		{
			fprintf(stderr, "Chevelure license: cant write sign field in license file \"%s\".\n", filename);
			fclose(file);
			return false;
		}
		fputs(buf, file);
		fputs("\n", file);
	}
	fclose(file);
	return true;
}

bool HLLicenseFile::Sign(Prsa_secret_key_t psk)
{
	// �������� random � ��������� 
	{
		char* buf = (char*)&this->random;
		int sz = sizeof(this->random);

		RandGen_InitializeGenerator(0, 0);
		RandGen_GenerateBuffer( (PUCHAR)buf, sz, false);

		md5_sha1 result;
		// md5
		md5_context_t md5;
		md5_init_context(&md5);
		md5_process_block(&md5, buf, sz);
		md5_get_result(&md5, result.md5);

		// sha1
		sha1_context_t sha1;
		sha1_init_context(&sha1);
		sha1_process_block(&sha1, buf, sz);
		sha1_get_result( &sha1, result.sha1);

		// SIGN
		ULONG ulSignSize;
		unsigned char* pSign = (unsigned char*)rsa_sign_raw( &result, sizeof(result), &ulSignSize, psk );
		random_sign.assign(pSign, pSign+ulSignSize);
		rsa_free_memory( pSign );
	}


	// ��������� ���
	{
		char* buf = (char*)this;
		int sz = (int)( (char*)(&this->random) - buf);

		md5_sha1 result;
		// md5
		md5_context_t md5;
		md5_init_context(&md5);
		md5_process_block(&md5, buf, sz);
		md5_get_result(&md5, result.md5);

		// sha1
		sha1_context_t sha1;
		sha1_init_context(&sha1);
		sha1_process_block(&sha1, buf, sz);
		sha1_get_result( &sha1, result.sha1);

		// SIGN
		ULONG ulSignSize;
		unsigned char* pSign = (unsigned char*)rsa_sign_raw( &result, sizeof(result), &ulSignSize, psk );
		sign.assign(pSign, pSign+ulSignSize);
		rsa_free_memory( pSign );
	}

	return true;
}
// ���������
bool HLLicenseFile::Verify(
	Prsa_public_key_t ppk
	)
{
	bool res = false;
	{
		char* buf = (char*)&this->random;
		int sz = sizeof(this->random);

		md5_sha1 result;
		// md5
		md5_context_t md5;
		md5_init_context(&md5);
		md5_process_block(&md5, buf, sz);
		md5_get_result(&md5, result.md5);

		// sha1
		sha1_context_t sha1;
		sha1_init_context(&sha1);
		sha1_process_block(&sha1, buf, sz);
		sha1_get_result( &sha1, result.sha1);

		res = rsa_verify_raw( &result, sizeof(result), &random_sign[0], random_sign.size(), ppk );
		if( !res) return false;
	}
	{
		char* buf = (char*)this;
		int sz = (int)( (char*)(&this->random) - buf);

		md5_sha1 result;
		// md5
		md5_context_t md5;
		md5_init_context(&md5);
		md5_process_block(&md5, buf, sz);
		md5_get_result(&md5, result.md5);

		// sha1
		sha1_context_t sha1;
		sha1_init_context(&sha1);
		sha1_process_block(&sha1, buf, sz);
		sha1_get_result( &sha1, result.sha1);

		res = rsa_verify_raw( &result, sizeof(result), &sign[0], sign.size(), ppk );
		if( !res) return false;
	}
	return true;
}

bool readBinaryFromText(unsigned char* dest, const char* src, int dstsize)
{
	for(int i=0; i<dstsize; i++)
	{
		if( *src == 0) 
			return false;
		char x = *(src++);
		if( x<'A' || x>'A'+0xF) 
			return false;
		char x1 = x-'A';

		if( *src == 0) 
			return false;
		x = *(src++);
		if( x<'A' || x>'A'+0xF) 
			return false;
		char x2 = x-'A';

		unsigned char z = x1 + (x2<<4);
		dest[i] = (char)z;
	}
	return true;
}
bool readBinaryFromText(std::vector<unsigned char>& dest, const char* src)
{
	dest.clear();
	dest.reserve(strlen(src)/2+1);
	for( ; *src!=0; )
	{
		char x = *(src++);
		if( x==0xd || x==0xa) 
			break;
		if( x<'A' || x>'A'+0xF) 
			return false;
		char x1 = x-'A';

		if( *src == 0) 
			return false;
		x = *(src++);
		if( x<'A' || x>'A'+0xF) 
			return false;
		char x2 = x-'A';

		unsigned char z = x1 + (x2<<4);
		dest.push_back(z);
	}
	return true;
}

bool writeBinaryToText(char* dest, const unsigned char* src, int dstsize, int srcsize)
{
	for(int i=0; i<srcsize; i++)
	{
		char x1 = (src[i]&0xF)+'A';
		char x2 = ((src[i]>>4)&0xF)+'A';
		if( dstsize<0) 
			return false;
		*(dest++) = x1;
		dstsize--;
		if( dstsize<0) 
			return false;
		*(dest++) = x2;
		dstsize--;
	}
	if( dstsize<0) 
		return false;
	*(dest++) = 0;
	return true;
}
bool writeBinaryToText(char* dest, const std::vector<unsigned char>& src, int dstsize)
{
	return writeBinaryToText(dest, &src[0], dstsize, (int)src.size());
}
