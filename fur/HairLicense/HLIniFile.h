#pragma once
#pragma pack(1)

#define HL_SIGNATURE_LEN 256

//typedef wchar_t* PTCHAR;
#pragma once

struct HLIniFile
{
	char addressname[128];
	unsigned short port;

	bool bValid;

	HLIniFile();

	bool Read(
		const char* filename, 
		bool bDump = false
		);
};