#include "StdAfx.h"
#include ".\hlserver.h"
#include "HLLicenseFile.h"
#include <time.h>
#include "net/macaddress.h"

HLServer::HLServer(void)
{
	validlicense = false;
	bValid = false;
	sd = INVALID_SOCKET;
	bTrace = true;
	next_session_id = 0;
}

HLServer::~HLServer(void)
{
}
bool HLServer::Start(
	const char* licensefile, 
	Prsa_public_key_t ppk, 
	bool bVerbose)
{
	bValid = false;

//	std::string addressname;
	// read licensefile
	if( !license.Read(licensefile, false, bVerbose))
		return bValid;

	if( license.Verify(ppk))
		validlicense = true;
	else
		printf("Verification failed\n");

	unsigned short port = 9191;
	if(license.port!=0) port = license.port;

	// init winsock
	{
		WSADATA w;
		if (WSAStartup(0x0101, &w) != 0)
		{
			fprintf(stderr, "Chevelure license: WSAStartup failed.\n");
			return bValid;
		}
	}
	// Get host name of this computer
	{
		char host_name[256];
		gethostname(host_name, sizeof(host_name));
		hostent* hp = gethostbyname(host_name);
		if(hp == NULL)
		{
			fprintf(stderr, "Chevelure license: gethostname failed\n");
			WSACleanup();
			return bValid;
		}
		this->server.sin_addr.s_addr=*((unsigned long*)hp->h_addr);
		this->server.sin_family = AF_INET;
		this->server.sin_port = htons(port);
	}
	if( bTrace)
		printf("Chevelure license: this host=%d.%d.%d.%d:%d\n", 
			this->server.sin_addr.S_un.S_un_b.s_b1,
			this->server.sin_addr.S_un.S_un_b.s_b2,
			this->server.sin_addr.S_un.S_un_b.s_b3,
			this->server.sin_addr.S_un.S_un_b.s_b4, 
			port);

	// Open&bind socket
	{
		this->sd = socket(AF_INET, SOCK_DGRAM, 0);
		if( this->sd == INVALID_SOCKET)
		{
			fprintf(stderr, "Chevelure license: socket() failed.\n");
			WSACleanup();
			return bValid;
		}
		/* Bind local address to socket */
		if( bind(sd, (struct sockaddr *)&server, sizeof(server)) == SOCKET_ERROR)
		{
			fprintf(stderr, "Chevelure license: Cannot bind address to socket.\n");
			closesocket(this->sd);
			WSACleanup();
			return bValid;
		}
	}
	{
		Net::GetMacAddress(macaddress);
		printf("mac adresses:\n");
		for(int i=0; i<(int)macaddress.size(); i++)
		{
			printf( "mac: %s\n", macaddress[i].c_str());
		}
	}
	if( bTrace)
	{
		printf( "license limit: %d\n", license.copyAllow);
	}
	
	if( license.datestring[0]==0)
	{
		this->licensetime = 0;
	}
	else
	{
		tm dt;
		memset( &dt, 0, sizeof(dt));
		char buf[32];
		strncpy( buf, license.datestring, sizeof(buf)-1);
		dt.tm_year = atoi(buf+04)-1900; buf[04]=0;
		dt.tm_mon  = atoi(buf+02)-1; buf[02]=0;
		dt.tm_mday = atoi(buf+00);
		this->licensetime = mktime(&dt);
		if(this->licensetime==-1)
		{
			printf( "mktime failed\n");
			bValid = false;
		}
	}

	bValid = true;
	return bValid;
}
bool HLServer::Loop()
{
	if( bTrace)
		printf("Chevelure license: start loop\n");

	// Loop and get data from clients
	while (1)
	{
		sockaddr_in client;
		int client_length = (int)sizeof(client);

		// Receive bytes from client
		char buffer[1500];
		int rec = recvfrom(sd, buffer, sizeof(buffer), 0, (struct sockaddr *)&client, &client_length);
		if (rec < 0)
		{
			fprintf(stderr, "Chevelure license: Could not receive datagram.\n");
			return false;
		}
		HLD_Header* header = (HLD_Header*)buffer;
		if(header->len!=rec)
		{
			fprintf(stderr, "Chevelure license: Recieving failed.\n");
			return false;
		}
		switch( header->type)
		{
		case HLD_IMStarted::TYPE:
			{
				HLD_IMStarted* IMStarted = (HLD_IMStarted*)header;
				if( bTrace)
					printf("Chevelure license: receive IMStarted from %d.%d.%d.%d\n",
						client.sin_addr.S_un.S_un_b.s_b1,
						client.sin_addr.S_un.S_un_b.s_b2,
						client.sin_addr.S_un.S_un_b.s_b3,
						client.sin_addr.S_un.S_un_b.s_b4);
				HLD_IMStartedReply reply;
				reply.session_id = next_session_id++;
				reply.licenseflag = 0;
				{
					// �������� ��� ������
					for( int m=0; m<(int)macaddress.size(); m++)
					{
						if( strcmp( macaddress[m].c_str(), this->license.macaddress)==0)
						{
							reply.licenseflag = this->validlicense;
							printf("Chevelure license: valid MAC address (%s)\n", macaddress[m].c_str(), this->license.macaddress);
						}
						else
							printf("Chevelure license: invalid mac address (%s!=%s)\n", macaddress[m].c_str(), this->license.macaddress);
					}
					// �������� ����
					time_t currenttime = time(NULL);
					if(this->licensetime)
					{
						if(currenttime > this->licensetime || IMStarted->clienttime > this->licensetime)
						{
							printf("Chevelure license: license expired\n");
							reply.licenseflag = 0;
						}
					}
					// �������� ����� ��������
					int count = GetClientCount();
					if( count>=(int)license.copyAllow)
					{
						printf("Chevelure license: copy allow expired\n");
						reply.licenseflag = 0;
					}
				}
				strcpy( reply.serialNumber, this->license.serialNumber);
				int snd = sendto(sd, (char*)&reply, (int)sizeof(reply), 0, (struct sockaddr *)&client, client_length);
				if ( snd != sizeof(reply))
				{
					fprintf(stderr, "Chevelure license: Error sending datagram.\n");
					return false;
				}
				if( bTrace)
				{
					printf("Chevelure license: send HLD_IMStartedReply to %d.%d.%d.%d session_id=%d\n",
						client.sin_addr.S_un.S_un_b.s_b1,
						client.sin_addr.S_un.S_un_b.s_b2,
						client.sin_addr.S_un.S_un_b.s_b3,
						client.sin_addr.S_un.S_un_b.s_b4, 
						reply.session_id);
					if( reply.licenseflag)
						printf("client validated\n");
					else
						printf("client invalidated\n");
				}

				if( reply.licenseflag)
				{
					sessions[reply.session_id].client = client;
					sessions[reply.session_id].lastalive = time(NULL);
				}
				int count = GetClientCount();
			}
			break;

		case HLD_IAMAlive::TYPE:
			{
				HLD_IAMAlive* IAMAlive = (HLD_IAMAlive*)header;
				/*/
				if( bTrace)
					printf("Chevelure license: receive HLD_IAMAlive. session_id=%d from %d.%d.%d.%d\n",
						IAMAlive->session_id,
						client.sin_addr.S_un.S_un_b.s_b1,
						client.sin_addr.S_un.S_un_b.s_b2,
						client.sin_addr.S_un.S_un_b.s_b3,
						client.sin_addr.S_un.S_un_b.s_b4
						);
				/*/
				if( sessions.find(IAMAlive->session_id)==sessions.end())
				{
//					printf("Chevelure license: unknown session_id!!! ignored!\n");
				}
				else
				{
					sessions[IAMAlive->session_id].lastalive = time(NULL);
				}
				int count = GetClientCount();
			}
			break;
		case HLD_IAMDead::TYPE:
			{
				HLD_IAMAlive* IAMAlive = (HLD_IAMAlive*)header;
				if( bTrace)
					printf("Chevelure license: receive HLD_IAMDead. session_id=%d from %d.%d.%d.%d\n",
						IAMAlive->session_id,
						client.sin_addr.S_un.S_un_b.s_b1,
						client.sin_addr.S_un.S_un_b.s_b2,
						client.sin_addr.S_un.S_un_b.s_b3,
						client.sin_addr.S_un.S_un_b.s_b4
						);
				std::map<int, SessionData>::iterator it = sessions.find(IAMAlive->session_id);
				if( it==sessions.end())
				{
					printf("Chevelure license: unknown session_id!!! ignored!\n");
				}
				else
				{
					sessions.erase(it);
				}
				int count = GetClientCount();
			}
			break;
		}

	}
	return true;
}
void HLServer::Stop()
{
	if(bValid)
	{
		closesocket(sd);
		WSACleanup();
	}
	bValid = false;
}

// ����� ��������
int HLServer::GetClientCount()
{
	int count = 0;
	int waittime = 300;		// 5min
	time_t current = time(NULL);
	std::map<int, SessionData>::iterator it = sessions.begin();
	for( ;it != sessions.end(); it++)
	{
		SessionData& sd = it->second;
		if(sd.lastalive < current-waittime)
			// �������
		{
		}
		else
		{
			count++;
		}
	}
	static int lastcount = 0;
	if( count!=lastcount)
	{
		printf("clients = %d\n", count);
	}
	lastcount = count;
	return count;
}
