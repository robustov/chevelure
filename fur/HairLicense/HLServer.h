#pragma once
#include <winsock.h>
#include <map>
#include <string>
#include "HLDatagram.h"
#include "HLLicenseFile.h"
#include "rsa_routines.h"

class HLServer
{
public:
	HLServer(void);
	~HLServer(void);

	bool Start(
		const char* licensefile, 
		Prsa_public_key_t ppk, 
		bool bVerbose);
	void Stop();

	bool Loop();

protected:
	bool bValid;
	bool bTrace;
	bool validlicense;
	time_t licensetime;

	HLLicenseFile license;

	// ���� ��� ������
	std::vector< std::string> macaddress;

	// adress port
	sockaddr_in server;
	//
	SOCKET sd;

	struct SessionData
	{
		//address
		sockaddr_in client;
		time_t lastalive;
	};
	// �� ������, ������
	std::map<int, SessionData> sessions;
	// 
	int next_session_id;

	// ����� ��������
	int GetClientCount();
	// mac
//	std::string macaddress;
	// license
//	std::string license;
};
