#pragma once

#include "rsa_routines.h"
bool writeBinaryToText(char* dest, const unsigned char* src, int dstsize, int srcsize);

// public key from resource
inline Prsa_public_key_t LoadPublicKeyFromResource(
	HINSTANCE hModule, 
	LPCSTR lpName, 
	LPCSTR lpType
	)
{
	HRSRC res = FindResource( hModule, lpName, lpType);
	if( !res)
	{
        fprintf(stderr, "chevelure: FindResource %s failed: %d\n", lpName, ::GetLastError());
		return NULL;
	}

	HGLOBAL mem = ::LoadResource( hModule, res);
	if(!mem)
	{
		fprintf(stderr, "chevelure: LoadResource %s failed\n", lpName);
		return NULL;
	}

	char* data = (char*)LockResource(mem);
	if(!data)
	{
		fprintf(stderr, "chevelure: LockResource %s failed\n", lpName);
		return NULL;
	}
	ULONG ulSize = *(ULONG*)data;
	data+=sizeof(ulSize);

	char dump[16];
	writeBinaryToText(dump, (const unsigned char*)data, 15, ulSize);
	dump[15]=0;
	fprintf(stdout, "psa: %s...(%d)\n", dump, ulSize);

	Prsa_public_key_t psk = rsa_load_public_key_from_memory( (tds_KFS_Header*)data, NULL, NULL);
	return psk;
}
