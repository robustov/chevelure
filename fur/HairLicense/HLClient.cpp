#include "StdAfx.h"
#include ".\hlclient.h"
#include "HLIniFile.h"

HLClient::HLClient(void)
{
	bValid = false;
	sd = INVALID_SOCKET;
//	bTrace = true;
	bTrace = false;
}
HLClient::~HLClient(void)
{
}
bool HLClient::Start(const char* licensefile, bool bTrace)
{
	bValid = false;
	this->bTrace = bTrace;

//	std::string addressname;
	// read licensefile
	HLIniFile license;
	if( !license.Read(licensefile))
		return bValid;

	unsigned short port = 9191;
	if(license.port!=0) port = license.port;

	if( bTrace)
		printf("Chevelure license: server=%s port=%d\n", license.addressname, port);

	// init winsock
	{
		WSADATA w;
		if (WSAStartup(0x0101, &w) != 0)
		{
			fprintf(stderr, "Chevelure license: WSAStartup failed.\n");
			return bValid;
		}
	}

	// resolve server address
	{
		hostent* hp = NULL;
		if(inet_addr(license.addressname)==INADDR_NONE)
		{
			hp = gethostbyname(license.addressname);
		}
		else
		{
			unsigned int addr;
			addr=inet_addr(license.addressname);
			hp = gethostbyaddr((char*)&addr,sizeof(addr),AF_INET);
		}
		if(hp==NULL)
		{
			fprintf(stderr, "Chevelure license: cant resolve address %s\n", license.addressname);
			WSACleanup();
			return bValid;
		}
		this->server.sin_addr.s_addr=*((unsigned long*)hp->h_addr);
		this->server.sin_family=AF_INET;
		this->server.sin_port=htons(port);
	}
	if( bTrace)
		printf("Chevelure license: address=%d.%d.%d.%d\n", 
			this->server.sin_addr.S_un.S_un_b.s_b1,
			this->server.sin_addr.S_un.S_un_b.s_b2,
			this->server.sin_addr.S_un.S_un_b.s_b3,
			this->server.sin_addr.S_un.S_un_b.s_b4);

	// Get host name of this computer
	{
		char host_name[256];
		gethostname(host_name, sizeof(host_name));
		hostent* hp = gethostbyname(host_name);
		if(hp == NULL)
		{
			fprintf(stderr, "Chevelure license: gethostname failed\n");
			WSACleanup();
			return bValid;
		}
		this->client.sin_addr.s_addr=*((unsigned long*)hp->h_addr);
		this->client.sin_family = AF_INET;
		this->client.sin_port = htons(0);
	}
	if( bTrace)
		printf("Chevelure license: this host=%d.%d.%d.%d\n", 
			this->client.sin_addr.S_un.S_un_b.s_b1,
			this->client.sin_addr.S_un.S_un_b.s_b2,
			this->client.sin_addr.S_un.S_un_b.s_b3,
			this->client.sin_addr.S_un.S_un_b.s_b4);

	// Open&bind socket
	{
		this->sd = socket(AF_INET, SOCK_DGRAM, 0);
		if( this->sd == INVALID_SOCKET)
		{
			fprintf(stderr, "Chevelure license: socket() failed.\n");
			WSACleanup();
			return bValid;
		}
		/* Bind local address to socket */
		if( bind(sd, (struct sockaddr *)&client, sizeof(client)) == SOCKET_ERROR)
		{
			fprintf(stderr, "Chevelure license: Cannot bind address to socket.\n");
			closesocket(this->sd);
			WSACleanup();
			return bValid;
		}
	}

	bValid = true;
	return bValid;
}
void HLClient::Stop()
{
	closesocket(sd);
	WSACleanup();
}
bool HLClient::GetLicense(void* keydest, int keysize)
{
	// �����:
	HLD_IMStarted question;
	strcpy( question.question, "�� �� �� ��");
	char* send_buffer = (char*)&question;
	int send_buffer_len = sizeof(question);

	// send
	int snd = sendto(sd, send_buffer, send_buffer_len, 0, (struct sockaddr *)&server, sizeof(server));
	if( snd == SOCKET_ERROR)
	{
		fprintf(stderr, "Chevelure license: Error transmitting data.\n");
		return false;
	}
	if( bTrace)
		printf("Chevelure license: send IMStarted = %d bytes to %d.%d.%d.%d\n", snd, 
			this->server.sin_addr.S_un.S_un_b.s_b1,
			this->server.sin_addr.S_un.S_un_b.s_b2,
			this->server.sin_addr.S_un.S_un_b.s_b3,
			this->server.sin_addr.S_un.S_un_b.s_b4);

	// receive
	char recbuf[1500];
	sockaddr_in server_in;
	int server_in_length = sizeof(server_in);
	int rec = recvfrom(sd, recbuf, (int)sizeof(recbuf), 0, (struct sockaddr *)&server_in, &server_in_length);
	if( rec<0)
	{
		int err = WSAGetLastError()-WSABASEERR;
		fprintf(stderr, "Chevelure license: Error receiving data. error=%d\n", err);
		return false;
	}
	if( bTrace)
		printf("Chevelure license: receive=%d bytes from %d.%d.%d.%d\n", rec, 
			server_in.sin_addr.S_un.S_un_b.s_b1,
			server_in.sin_addr.S_un.S_un_b.s_b2,
			server_in.sin_addr.S_un.S_un_b.s_b3,
			server_in.sin_addr.S_un.S_un_b.s_b4);

	int needlen = sizeof(HLD_IMStartedReply);
	if( rec!=needlen)
	{
		fprintf(stderr, "Chevelure license: Invalid reply from server. rec=%d needlen=%d\n", rec, needlen);
		return false;
	}
	HLD_IMStartedReply* reply = (HLD_IMStartedReply*)recbuf;
	
	this->sessionId = reply->session_id;
	if( bTrace)
	{
		printf("Chevelure license: serial: %s\n", reply->serialNumber);
		printf("Chevelure license: session_id=%d\n", reply->session_id);
	}

	// ��� ���������
	// ...
	keysize = __min( keysize, sizeof( reply->licenseflag));
	memcpy( keydest, &reply->licenseflag, keysize);

	return true;
}
bool HLClient::KeepAliveLoop()
{
	// ���� IAMALIVE

		HLD_IAMAlive pack;
		pack.session_id = this->sessionId;

		// send
		int snd = sendto(sd, (char*)&pack, sizeof(pack), 0, (struct sockaddr *)&server, sizeof(server));
		if( snd == SOCKET_ERROR)
		{
			fprintf(stderr, "Chevelure license: Error transmitting data.\n");
			return false;
		}
		if( bTrace)
			printf("Chevelure license: send IAMAlive. session_id=%d bytes to %d.%d.%d.%d\n", 
				pack.session_id, 
				this->server.sin_addr.S_un.S_un_b.s_b1,
				this->server.sin_addr.S_un.S_un_b.s_b2,
				this->server.sin_addr.S_un.S_un_b.s_b3,
				this->server.sin_addr.S_un.S_un_b.s_b4);
	return true;
}

bool HLClient::SendImDead()
{
	HLD_IAMDead pack;
	pack.session_id = this->sessionId;

	// send
	int snd = sendto(sd, (char*)&pack, sizeof(pack), 0, (struct sockaddr *)&server, sizeof(server));
	if( snd == SOCKET_ERROR)
	{
		fprintf(stderr, "Chevelure license: Error transmitting data.\n");
		return false;
	}
	if( bTrace)
		printf("Chevelure license: send IAMDead. session_id=%d bytes to %d.%d.%d.%d\n", 
			pack.session_id, 
			this->server.sin_addr.S_un.S_un_b.s_b1,
			this->server.sin_addr.S_un.S_un_b.s_b2,
			this->server.sin_addr.S_un.S_un_b.s_b3,
			this->server.sin_addr.S_un.S_un_b.s_b4);
	return true;
}

