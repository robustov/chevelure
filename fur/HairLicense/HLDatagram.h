#pragma once
#include <time.h>

#pragma pack(1)

typedef unsigned int time_sas;

struct HLD_Header
{
	unsigned short len;
	unsigned short type;
};

// client -> server
struct HLD_IMStarted : public HLD_Header
{
	static const unsigned short TYPE = 1;
	char question[16];
	time_sas clienttime;
	HLD_IMStarted()
	{
		len = sizeof(*this);
		type = TYPE;
		clienttime = (time_sas)time(NULL);
	}
};

// server -> client, reply on HLD_IMStarted
struct HLD_IMStartedReply : public HLD_Header
{
	static const unsigned short TYPE = 2;
	int session_id;
	DWORD licenseflag;
	char serialNumber[128];
	time_sas servertime;
	HLD_IMStartedReply()
	{
		len = sizeof(*this);
		type = TYPE;
		servertime = (time_sas)time(NULL);
	}
};

// client -> server
struct HLD_IAMAlive : public HLD_Header
{
	static const unsigned short TYPE = 3;
	int session_id;
	time_sas clienttime;
	HLD_IAMAlive()
	{
		len = sizeof(*this);
		type = TYPE;
		clienttime = (time_sas)time(NULL);
	}
};

// client -> server
struct HLD_IAMDead : public HLD_Header
{
	static const unsigned short TYPE = 4;
	int session_id;
	time_sas clienttime;
	HLD_IAMDead()
	{
		len = sizeof(*this);
		type = TYPE;
		clienttime = (time_sas)time(NULL);
	}
};
