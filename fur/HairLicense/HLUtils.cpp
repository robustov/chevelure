#include "StdAfx.h"
#include "HLUtils.h"
#include "Util/misc_create_directory.h"

// ����� ����� �������� � ���������� ��� ����� � ������
std::string HLsearchLicenseFile(
	const char* directory)
{
	std::string test = std::string(directory)+"\\chevelure.lic";
	FILE* f = 0;
	f = fopen(test.c_str(), "rt");
	if(f)
	{
		fclose(f);
		return test;
	}

	test = Util::extract_foldername(directory);
	test = test+"\\chevelure.lic";
	f = fopen(test.c_str(), "rt");
	if(f)
	{
		fclose(f);
		return test;
	}
	return "";
}
