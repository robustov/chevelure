//
// Copyright (C) 
// File: HairRenderPassCmd.cpp
// MEL Command: HairRenderPassNode
#include "pre.h"
#include "HairRenderPassNode.h"
#include "Math/Box.h"
#include "MathNMaya/MathNMaya.h"
#include "MathNGl/MathNGl.h"
#include "HairShape.h"
#include "HairDeformerData.h"

#include <Maya/MFnUnitAttribute.h>
#include <Maya/MFnEnumAttribute.h>
#include <Maya/MFnCompoundAttribute.h>
#include <Maya/MFnMessageAttribute.h>
#include <maya/MGlobal.h>

MObject HairRenderPassNode::i_enable;
//MObject HairRenderPassNode::i_mode;
MObject HairRenderPassNode::o_standAlone;

MObject HairRenderPassNode::i_source;
MObject HairRenderPassNode::i_bRenderClumps;
MObject HairRenderPassNode::i_degradation;
MObject HairRenderPassNode::i_degradationWidth;
MObject HairRenderPassNode::i_Shader;

MObject HairRenderPassNode::i_RenderWidth;
MObject HairRenderPassNode::i_mulWidthByLength;
MObject HairRenderPassNode::i_useClumpRadiusAsWidth;

MObject HairRenderPassNode::i_SegmCount;

MObject HairRenderPassNode::i_interpolation;

MObject HairRenderPassNode::i_diceHair;
MObject HairRenderPassNode::i_sigmaHiding;
MObject HairRenderPassNode::i_renderDoubleSide;
MObject HairRenderPassNode::i_reverseOrientation;

MObject HairRenderPassNode::i_renderNormalSource;
MObject HairRenderPassNode::i_renderRayTraceEnable;
MObject HairRenderPassNode::i_renderRayCamera;
MObject HairRenderPassNode::i_renderRayTrace;
MObject HairRenderPassNode::i_renderRayPhoton;

MObject HairRenderPassNode::i_useDetails;
MObject HairRenderPassNode::i_DetailRange[4];

MObject HairRenderPassNode::i_final;
MObject HairRenderPassNode::i_shadow;
MObject HairRenderPassNode::i_reflection;
MObject HairRenderPassNode::i_environment;
MObject HairRenderPassNode::i_depth;
MObject HairRenderPassNode::i_reference;
MObject HairRenderPassNode::i_archive;
MObject HairRenderPassNode::i_photon;
MObject i_passClasses;

MObject i_passIds;

// blur
MObject HairRenderPassNode::i_MotionBlur;

// out
MObject HairRenderPassNode::o_output;

MObject HairRenderPassNode::i_bPrune;
MObject HairRenderPassNode::i_pruningrate;			// �������
MObject HairRenderPassNode::i_prunedistmin;			// ��������� ������ ������������
MObject HairRenderPassNode::i_prunemin_value;		// ����������� ��������
MObject HairRenderPassNode::i_prunemin_width;		// ������ ��� ����������� ��������
MObject HairRenderPassNode::i_pruneTestDistance;

// 13.03.2008
MObject HairRenderPassNode::i_widthBias;

// 22.08.2008
MObject HairRenderPassNode::i_opacity;

HairRenderPassNode::HairRenderPassNode()
{
}
HairRenderPassNode::~HairRenderPassNode()
{
}

MObject HairRenderPassNode::getHairShapeNode() const
{
	MPlug plug(thisMObject(), o_output);

	MPlugArray pa;
	plug.connectedTo(pa, false, true);
	if(pa.length()!=1) 
		return MObject::kNullObj;

	MObject obj = pa[0].node();
	MFnDependencyNode dn(obj);
	if( dn.typeId()!=HairShape::id)
		return MObject::kNullObj;
	return obj;
}
HairShape* HairRenderPassNode::getHairShape() const
{
	MObject obj = getHairShapeNode();
	if( obj.isNull()) return NULL;
	MFnDependencyNode dn(obj);
	MPxNode* node = dn.userNode();
	return (HairShape*)node;
}
bool HairRenderPassNode::IsStandAlone()  const
{
	MPlug plug(thisMObject(), o_standAlone);

	MPlugArray pa;
	plug.connectedTo(pa, false, true);
	if(pa.length()!=1) 
		return false;

	MObject obj = pa[0].node();
	if( obj.isNull()) return false;
	return true;
}

MStatus HairRenderPassNode::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;

	if( plug == o_output )
		return HairRenderPassNode::ComputeOutput(plug, data);

	return MS::kUnknownParameter;
}

MStatus HairRenderPassNode::ComputeOutput(const MPlug& plug, MDataBlock& data)
{
	MStatus stat;

	HairRenderPassData* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( plug.attribute(), &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<HairRenderPassData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( HairRenderPassData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<HairRenderPassData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}
displayStringD( "HairRenderPassNode::ComputeOutput");

	if( !pData->Init())
		return MS::kFailure;

	HairRenderPass& pass = pData->data;

	
	pass.bStandAlone = this->IsStandAlone();

	pass.bEnable = data.inputValue( i_enable).asBool();

	pass.passName = MFnDependencyNode(thisMObject()).name().asChar();

	pass.shader = data.inputValue(i_Shader).asString().asChar();

	pass.source = (HairRenderPass::enSource)data.inputValue(i_source).asShort();
	pass.bRenderClumps = data.inputValue( i_bRenderClumps).asBool();

	pass.degradation = (float)(data.inputValue(i_degradation).asDouble());
	pass.degradation = pass.degradation*0.01f;

	pass.degradationWidth = (float)(data.inputValue(i_degradationWidth).asDouble());
	pass.degradationWidth = pass.degradationWidth*0.01f;
	pass.degradationWidth = __max(pass.degradationWidth, 0);

	pass.RenderWidth = (float)data.inputValue(i_RenderWidth).asDouble();
	pass.mulWidthByLength = data.inputValue( i_mulWidthByLength).asBool();
	pass.useClumpRadiusAsWidth = data.inputValue( i_useClumpRadiusAsWidth).asBool();
	

	pass.interpolation = data.inputValue(i_interpolation, &stat).asShort();

	pass.SegmCount = data.inputValue(i_SegmCount, &stat).asInt();

	pass.diceHair = data.inputValue(i_diceHair, &stat).asBool();
	pass.sigmaHiding = data.inputValue(i_sigmaHiding, &stat).asBool();
	pass.renderDoubleSide = data.inputValue(i_renderDoubleSide, &stat).asBool();
	pass.reverseOrientation = data.inputValue(i_reverseOrientation, &stat).asBool();

	pass.renderNormalSource = (enRenderNormalSource)data.inputValue(i_renderNormalSource, &stat).asShort();

	pass.renderRayTraceEnable = data.inputValue(i_renderRayTraceEnable, &stat).asBool();
	pass.renderRayCamera	= data.inputValue(i_renderRayCamera, &stat).asBool();
	pass.renderRayTrace	= data.inputValue(i_renderRayTrace, &stat).asBool();
	pass.renderRayPhoton	= data.inputValue(i_renderRayPhoton, &stat).asBool();

	pass.bUseDetails = data.inputValue(i_useDetails, &stat).asBool();
	LoadDetailRangeAttr( data, i_DetailRange, pass.detailrange);

	// passes
	std::vector< std::string> passes;
	if( data.inputValue(i_final).asBool()) passes.push_back("final");
	if( data.inputValue(i_shadow).asBool()) passes.push_back("shadow");
	if( data.inputValue(i_reflection).asBool()) passes.push_back("reflection");
	if( data.inputValue(i_environment).asBool()) passes.push_back("environment");
	if( data.inputValue(i_depth).asBool()) passes.push_back("depth");
	if( data.inputValue(i_reference).asBool()) passes.push_back("reference");
	if( data.inputValue(i_archive).asBool()) passes.push_back("archive");
	if( data.inputValue(i_photon).asBool()) passes.push_back("photon");

	// passClasses
	{
		std::string passClasses = data.inputValue(i_passClasses).asString().asChar();
		char* substr = strtok((char*)passClasses.c_str(), " \t=");
		for(; substr; substr = strtok(NULL, " \t="))
			passes.push_back(substr);
	}
	
	pass.passes = passes;

	// passIds
	{
		std::string passIds = data.inputValue(i_passIds).asString().asChar();
		std::vector< std::string> _passIds;
		char* substr = strtok((char*)passIds.c_str(), " \t=");
		for(; substr; substr = strtok(NULL, " \t="))
			_passIds.push_back(substr);
		pass.passIds = _passIds;
	}

	pass.bMotionBlur = data.inputValue(i_MotionBlur, &stat).asBool();

	pass.bPrune = data.inputValue(i_bPrune, &stat).asBool();
	pass.pruningrate = (float)data.inputValue(i_pruningrate, &stat).asDouble();
	pass.prunedistmin = (float)data.inputValue(i_prunedistmin, &stat).asDouble();
	pass.prunemin_value = (float)data.inputValue(i_prunemin_value, &stat).asDouble();
	pass.prunemin_width = (float)data.inputValue(i_prunemin_width, &stat).asDouble();
	pass.pruneTestDistance = (float)data.inputValue(i_pruneTestDistance, &stat).asDouble();

	// 13.03.2008
	pass.widthBias = (float)data.inputValue(i_widthBias, &stat).asDouble();

	// 22.08.2008
	pass.opacity = (float)data.inputValue(i_opacity, &stat).asDouble();

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();

	return MS::kSuccess;
}

/*/
bool HairRenderPassNode::isBounded() const
{
	short mode=0;
	MPlug(thisMObject(), i_mode).getValue(mode);
	if( mode==0) return false;
	return true;
}

MBoundingBox HairRenderPassNode::boundingBox() const
{
	short mode=0;
	MPlug(thisMObject(), i_mode).getValue(mode);
	if( mode==0) return MBoundingBox();

	HairShape* hs = getHairShape();
	if( !hs) return MBoundingBox();
	return hs->boundingBox();
}

void HairRenderPassNode::draw(
	M3dView &view, 
	const MDagPath &path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status)
{ 
	MObject thisNode = thisMObject();
	MFnDependencyNode nd(thisNode);
	MString str = nd.name();

	MDataBlock data = forceCache();
	int mode = data.inputValue(i_mode).asShort();
	if( mode==0) return ;

	view.beginGL(); 
	view.drawText(str, MPoint(0, 0, 0));
	view.endGL();
};
/*/

void* HairRenderPassNode::creator()
{
	return new HairRenderPassNode();
}

MStatus HairRenderPassNode::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnEnumAttribute enumAttr;
	MFnTypedAttribute	typedAttr;
	MFnUnitAttribute unitAttr;
	MFnMessageAttribute mesAttr;
	MFnStringData stringData;
	MStatus				stat;

	bool bStorableAttributes = true;

	try
	{
		{
			o_standAlone = mesAttr.create ("standalone", "standalone");
			::addAttribute(o_standAlone, atOutput);

			/*/
			i_mode = enumAttr.create ("shapemode", "shapemode", 0);
			enumAttr.addField("RENDER WITH HairShape", 0);
			enumAttr.addField("STANDALONE SHAPE", 1);
			::addAttribute(i_mode, atInput);
			/*/
		}
		{
			i_enable = numAttr.create( "enable", "enable", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_enable, atInput);
		}
		{
			i_source = enumAttr.create ("source", "src", 0);
			enumAttr.addField("DEFAULT", 0);
			enumAttr.addField("HAIRS", 1);
			enumAttr.addField("TUBES", 2);
			enumAttr.addField("SUBDIV TUBES", 4);
			enumAttr.addField("INSTANCES", 3);
			::addAttribute(i_source, atInput);
		}
		{
			i_bRenderClumps = numAttr.create( "bRenderClumps", "bRenderClumps", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_bRenderClumps, atInput);
		}
		{
			i_degradation = numAttr.create ("degradation","deg",MFnNumericData::kDouble, 100, &stat);
			numAttr.setMin(0);	
			numAttr.setMax(100);	
			::addAttribute(i_degradation, atInput);

			i_degradationWidth = numAttr.create ("degradationWidth","degw",MFnNumericData::kDouble, 0, &stat);
			numAttr.setMin(0);	
			numAttr.setMax(100);	
			::addAttribute(i_degradationWidth, atInput);
		}
		{
			i_MotionBlur = numAttr.create( "bMotionBlur", "bMotionBlur", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_MotionBlur, atInput);
			
		}
		{
			i_Shader = typedAttr.create( "shader", "sh",
				MFnData::kString, stringData.create(&stat), &stat );
			::addAttribute(i_Shader, atInput);
		}
		{
			i_RenderWidth = numAttr.create ("RenderWidth","rW",MFnNumericData::kDouble, 0.02,&stat);
			numAttr.setSoftMin(0);	
			numAttr.setSoftMax(0.1);	
			::addAttribute(i_RenderWidth, atInput);

			i_mulWidthByLength = numAttr.create( "mulWidthByLength", "mwbl", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_mulWidthByLength, atInput);

			i_useClumpRadiusAsWidth = numAttr.create( "useClumpRadiusAsWidth", "ucraw", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_useClumpRadiusAsWidth, atInput);
		}
		{
			i_interpolation = enumAttr.create( "interpolation", "intr", 0, &stat );
			enumAttr.addField( "Linear", 0 );
			enumAttr.addField( "Catmul-Rom", 1 );
			::addAttribute(i_interpolation, atInput);
		}
		{
			i_SegmCount = numAttr.create ("segmentCount","sC",MFnNumericData::kInt, 2,&stat);
			if (!stat) return stat;
			numAttr.setKeyable(true);
			numAttr.setStorable (true);
			numAttr.setHidden(false);
			numAttr.setMin(1);
			numAttr.setSoftMax(50);
			stat = addAttribute (i_SegmCount);
			if (!stat) return stat;
		}
		{
			i_diceHair = numAttr.create( "diceHair", "bdc", MFnNumericData::kBoolean, 1, &stat);
			if (!stat) return stat;
			numAttr.setStorable (true);
			numAttr.setHidden(false);
			stat = addAttribute (i_diceHair);
			if (!stat) return stat;

			i_sigmaHiding = numAttr.create( "sigmaHiding", "shd", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute( i_sigmaHiding, atInput);

			i_renderDoubleSide = numAttr.create( "renderDoubleSide", "rds", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute (i_renderDoubleSide, atInput);

			i_renderNormalSource = enumAttr.create("renderNormalSource", "rns", 0);
			enumAttr.addField("none (on camera)",	0);
			enumAttr.addField("tangentU",			1);
			enumAttr.addField("clumpVector",		2);
			::addAttribute( i_renderNormalSource, atInput);

			i_reverseOrientation = numAttr.create( "reverseOrientation", "rro", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute (i_reverseOrientation, atInput);

			i_renderRayTraceEnable = numAttr.create( "renderRayTraceEnable", "rrte", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute (i_renderRayTraceEnable, atInput);
			i_renderRayCamera = numAttr.create( "renderRayCamera", "rrtc", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute (i_renderRayCamera, atInput);
			i_renderRayTrace = numAttr.create( "renderRayTrace", "rrt", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute (i_renderRayTrace, atInput);
			i_renderRayPhoton = numAttr.create( "renderRayPhoton", "rrtph", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute (i_renderRayPhoton, atInput);
		}
		{
			i_useDetails = numAttr.create( "useDetails", "usde", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_useDetails, atInput);

			CreateDetailRangeAttr("det", i_DetailRange);
		}
		{
			i_final = numAttr.create( "final", "final", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_final, atInput);

			i_shadow = numAttr.create( "shadow", "shadow", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_shadow, atInput);

			i_reflection = numAttr.create( "reflection", "reflection", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_reflection, atInput);

			i_environment = numAttr.create( "environment", "environment", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_environment, atInput);

			i_depth = numAttr.create( "depth", "depth", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_depth, atInput);

			i_reference = numAttr.create( "reference", "reference", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_reference, atInput);

			i_archive = numAttr.create( "archive", "archive", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_archive, atInput);

			i_photon = numAttr.create( "photon", "photon", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_photon, atInput);

			i_passClasses = typedAttr.create( "passClasses", "pcs",
				MFnData::kString, stringData.create(&stat), &stat );
			::addAttribute(i_passClasses, atInput);
		}

		{
			i_passIds = typedAttr.create( "passIds", "pids",
				MFnData::kString, stringData.create(&stat), &stat );
			::addAttribute(i_passIds, atInput);
		}
		
		{
			i_bPrune = numAttr.create( "prune", "prn", MFnNumericData::kBoolean, 0, &stat);
			::addAttribute(i_bPrune, atInput);
			i_pruningrate = numAttr.create( "pruningRate", "prr", MFnNumericData::kDouble, 2, &stat);
			::addAttribute(i_pruningrate, atInput);
			i_prunedistmin = numAttr.create( "pruneDistMin", "prdm", MFnNumericData::kDouble, 1, &stat);
			::addAttribute(i_prunedistmin, atInput);

			i_prunemin_value = numAttr.create( "pruneMinValue", "prmv", MFnNumericData::kDouble, 0.00, &stat);
			::addAttribute(i_prunemin_value, atInput);
			i_prunemin_width = numAttr.create( "pruneMinWidth", "prmw", MFnNumericData::kDouble, 0.05, &stat);
			::addAttribute(i_prunemin_width, atInput);
			i_pruneTestDistance = numAttr.create( "pruneTestDistance", "pr��", MFnNumericData::kDouble, 0.0, &stat);
			::addAttribute(i_pruneTestDistance, atInput);
		}
		{
			// 13.03.2008

			i_widthBias = numAttr.create( "widthBias", "wbi", MFnNumericData::kDouble, 0, &stat);
			numAttr.setSoftMin(0);
			numAttr.setSoftMax(0.1);
			::addAttribute(i_widthBias, atInput);
		}
		{
			// 22.08.2008
			i_opacity = numAttr.create( "opacity", "opa", MFnNumericData::kDouble, -1, &stat);
			numAttr.setMin(-1);
			numAttr.setSoftMin(0);
			numAttr.setMax(1);
			::addAttribute(i_opacity, atInput);
		}

		{
			o_output = typedAttr.create( "o_output", "out", HairRenderPassData::id);
			::addAttribute(o_output, atOutput|atHidden);

			MObject isAffected = o_output;

//			attributeAffects( i_mode, isAffected);
			attributeAffects( i_enable, isAffected);
			attributeAffects( i_bRenderClumps, isAffected);
			attributeAffects( i_degradation, isAffected);
			attributeAffects( i_degradationWidth, isAffected);
			attributeAffects( i_SegmCount, isAffected);
			attributeAffects( i_Shader, isAffected);
			attributeAffects( i_source, isAffected);
			attributeAffects( i_RenderWidth, isAffected);
			attributeAffects( i_mulWidthByLength, isAffected);
			attributeAffects( i_useClumpRadiusAsWidth, isAffected);
			attributeAffects( i_interpolation, isAffected);
			attributeAffects( i_diceHair, isAffected);
			attributeAffects( i_sigmaHiding, isAffected);
			attributeAffects( i_renderDoubleSide, isAffected);
			attributeAffects( i_reverseOrientation, isAffected);
			attributeAffects( i_renderNormalSource, isAffected);
			attributeAffects( i_renderRayTraceEnable, isAffected);
			attributeAffects( i_renderRayCamera, isAffected);
			attributeAffects( i_renderRayTrace, isAffected);
			attributeAffects( i_renderRayPhoton, isAffected);
			attributeAffects( i_useDetails, isAffected);
			HairShape::attributeAffectsT4( i_DetailRange, isAffected);
			attributeAffects( i_final, isAffected);
			attributeAffects( i_shadow, isAffected);
			attributeAffects( i_reflection, isAffected);
			attributeAffects( i_environment, isAffected);
			attributeAffects( i_depth, isAffected);
			attributeAffects( i_reference, isAffected);
			attributeAffects( i_archive, isAffected);
			attributeAffects( i_photon, isAffected);
			attributeAffects( i_passIds, isAffected);
			attributeAffects( i_passClasses, isAffected);

			attributeAffects( i_MotionBlur, isAffected);

			attributeAffects( i_bPrune, isAffected);
			attributeAffects( i_pruningrate, isAffected);
			attributeAffects( i_prunedistmin, isAffected);
			attributeAffects( i_prunemin_value, isAffected);
			attributeAffects( i_prunemin_width, isAffected);
			attributeAffects( i_pruneTestDistance, isAffected);
			// 13.03.2008
			attributeAffects( i_widthBias, isAffected);
			attributeAffects( i_opacity, isAffected);
		}

		if( !SourceMelFromResource(MhInstPlugin, "AEHairRenderPassNodeTemplate", "MEL", defines))
		{
			displayString("error source AEHairRenderPassTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

void HairRenderPassNode::CreateDetailRangeAttr(const char* name, MObject i_DetailRange[4])
{
	MStatus stat;
	std::string an = name;
	std::string longname, shortname;
	MFnNumericAttribute numAttr;

	longname = an+"minvisible"; shortname = an+"minvis";
	i_DetailRange[0] = numAttr.create( longname.c_str(), shortname.c_str(), MFnNumericData::kDouble, 0, &stat);
	numAttr.setMin(0);numAttr.setSoftMax(1000);
	::addAttribute(i_DetailRange[0], atInput|atKeyable);

	longname = an+"lowertransition"; shortname = an+"lowtran";
	i_DetailRange[1] = numAttr.create( longname.c_str(), shortname.c_str(), MFnNumericData::kDouble, 0, &stat);
	numAttr.setMin(0);numAttr.setSoftMax(1000);
	::addAttribute(i_DetailRange[1], atInput|atKeyable);

	longname = an+"uppertransition"; shortname = an+"uptran";
	i_DetailRange[2] = numAttr.create( longname.c_str(), shortname.c_str(), MFnNumericData::kDouble, 10000, &stat);
	numAttr.setMin(0);numAttr.setSoftMax(1000);
	::addAttribute(i_DetailRange[2], atInput|atKeyable);

	longname = an+"maxvisible"; shortname = an+"maxvis";
	i_DetailRange[3] = numAttr.create( longname.c_str(), shortname.c_str(), MFnNumericData::kDouble, 10000, &stat);
	numAttr.setMin(0);numAttr.setSoftMax(1000);
	::addAttribute(i_DetailRange[3], atInput|atKeyable);
}

void HairRenderPassNode::LoadDetailRangeAttr( MDataBlock& data, MObject i_DetailRange[4], HairRenderPass::DetailRange& v)
{
	v.minvisible		= (float)data.inputValue( i_DetailRange[0]).asDouble();
	v.lowertransition	= (float)data.inputValue( i_DetailRange[1]).asDouble();
	v.uppertransition	= (float)data.inputValue( i_DetailRange[2]).asDouble();
	v.maxvisible		= (float)data.inputValue( i_DetailRange[3]).asDouble();
}
