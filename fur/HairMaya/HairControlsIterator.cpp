///////////////////////////////////////////////////////////////////////////////
//
// HairControlsIterator.cpp
//
///////////////////////////////////////////////////////////////////////////////
#include "pre.h"
#include <maya/MVectorArray.h>

#include "HairControlsIterator.h"
#include "MathNMaya/MathNMaya.h"

HairControlsIterator::HairControlsIterator( void * geom, MObjectArray & comps )
	: MPxGeometryIterator( geom, comps ),
	geometry( (HairCVandGeometry*)geom )
{
	reset();
}

HairControlsIterator::HairControlsIterator( void * geom, MObject & comps )
	: MPxGeometryIterator( geom, comps ),
	geometry( (HairCVandGeometry*)geom )
{
	reset();
}

/* override */
void HairControlsIterator::reset()
//
// Description
//
//  	
//   Resets the iterator to the start of the components so that another
//   pass over them may be made.
//
{
	MPxGeometryIterator::reset();
	setCurrentPoint( 0 );
	if ( NULL != geometry ) 
	{
		int maxVertex = geometry->cv->controlVertexCount()*geometry->cv->getCount();
		setMaxPoints( maxVertex );
	}
}

/* override */
MPoint HairControlsIterator::point() const
//
// Description
//
//    Returns the point for the current element in the iteration.
//    This is used by the transform tools for positioning the
//    manipulator in component mode. It is also used by deformers.	 
//
{
	MPoint pnt;
	if ( NULL != geometry ) 
	{
		int v = index();
		int cvcount = geometry->cv->controlVertexCount();
		int h = v/cvcount;
		v = v-h*cvcount;
		HairCV::CV& hc = geometry->cv->getControlHair(h);

		Math::Vec3f vert = hc.ts[v];
		vert = geometry->bases->bases[h]*vert;
		::copy(pnt, vert);
	}
	return pnt;
}

/* override */
void HairControlsIterator::setPoint( const MPoint & pnt ) const
//
// Description
//
//    Set the point for the current element in the iteration.
//    This is used by deformers.	 
//
{
	if ( NULL != geometry ) 
	{
		int v = index();
		int cvcount = geometry->cv->controlVertexCount();
		int h = v/cvcount;
		v = v-h*cvcount;
		HairCV::CV& hc = geometry->cv->getControlHair(h);

		Math::Vec3f vert;
		::copy(vert, pnt);
		vert = geometry->bases->bases_inv[h]*vert;
		hc.ts[v] = vert;
	}
}

/* override */
int HairControlsIterator::iteratorCount() const
{
//
// Description
//
//    Return the number of vertices in the iteration.
//    This is used by deformers such as smooth skinning
//
	int maxVertex = geometry->cv->controlVertexCount()*geometry->cv->getCount();
	return maxVertex;
	
}

/* override */
bool HairControlsIterator::hasPoints() const
//
// Description
//
//    Returns true since the shape data has points.
//
{
	return geometry!=0;
}
