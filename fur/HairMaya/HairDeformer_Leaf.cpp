#include "HairDeformer_Leaf.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnCompoundAttribute.h>

#include <maya/MGlobal.h>

MObject HairDeformer_Leaf::i_Deformer[2];
MObject HairDeformer_Leaf::i_envelope[3];
MObject HairDeformer_Leaf::o_output;
MObject HairDeformer_Leaf::i_PaintResXY[2];

MObject HairDeformer_Leaf::i_sourceWidth;
MObject HairDeformer_Leaf::i_rampWidth;

HairDeformer_Leaf::HairDeformer_Leaf()
{
}
HairDeformer_Leaf::~HairDeformer_Leaf()
{
}

MStatus HairDeformer_Leaf::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;
	displayStringD("HairDeformer_Leaf::compute %s", plug.name().asChar());

	if( plug == this->o_output )
	{
		return ComputeOutput(plug, data, i_Deformer, i_envelope);
	}
	return MS::kUnknownParameter;
}

void* HairDeformer_Leaf::creator()
{
	return new HairDeformer_Leaf();
}

MStatus HairDeformer_Leaf::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnUnitAttribute unitAttr;
	MFnEnumAttribute enumAttr;
	MStatus				stat;

#ifndef SAS
	if( !HairDeformer::initialize("HairMath", "getLeafDeformer", i_Deformer, i_envelope, i_PaintResXY, o_output))
		return MS::kFailure;
#else
	if( !HairDeformer::initialize("chevelureMath", "getLeafDeformer", i_Deformer, i_envelope, i_PaintResXY, o_output))
		return MS::kFailure;
#endif
	try
	{
		MString attrLongName =  "rampWidth";
		MString attrShortName = "raw";
		i_rampWidth = MRampAttribute::createCurveRamp(attrLongName, attrShortName);
		::addAttribute(i_rampWidth, atInput);

		i_sourceWidth = enumAttr.create ("sourceWidth", "swi", 1);
		enumAttr.addField("USE_IDENTITY", 0);
		enumAttr.addField("USE_BASEWIDTH", 1);
		enumAttr.addField("USE_TIPWIDTH", 2);
		enumAttr.addField("USE_BOTH", 3);
		::addAttribute(i_sourceWidth, atInput);

		MObject isAffected = o_output;
		attributeAffects( i_rampWidth, isAffected);
		attributeAffects( i_sourceWidth, isAffected);

		if( !SourceMelFromResource(MhInstPlugin, "AEHairDeformer_LeafTemplate", "MEL", defines))
		{
			displayString("error source AEHairDeformer_LeafTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}
