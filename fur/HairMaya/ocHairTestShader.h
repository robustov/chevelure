#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ocHairTestShaderNode.h
//
// Dependency Graph Node: ocHairTestShader

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include "ocellaris/IShadingNodeGenerator.h"

class ocHairTestShader : public MPxNode
{
public:
	ocHairTestShader();
	virtual ~ocHairTestShader(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();

public:
	static MObject space;				// ="world";
	static MObject viewN;
	static MObject KdAmbient;
  	static MObject Kd1;					// �� ������� 1
  	static MObject Kd2;					// �� ������� 2
  	static MObject Kd3;					// �� ������� 3
  	static MObject Knond;				// ����������� ����
  	static MObject Sc;					// Surface color
  	static MObject opacityMinForShadow;
  	static MObject opacityMaxForShadow;

	static MObject i_UVCoord;		// uv

	static MObject o_color;			// Output attributes
	static MObject o_alpha;			// Output attributes

public:
	static MTypeId id;
	static const MString typeName;

	class Generator : public cls::IShadingNodeGenerator
	{
	public:
		// ���������� ���, ������ ��� �������������
		virtual const char* GetUniqueName(
			){return "ocHairTestShaderGenerator";};

		// ��� ����
		virtual enShadingNodeType getShadingNodeType(
			MObject& node					//!< ������ (shader)
			);

		//! ��������� ��������� 
		//! ��������� ��������� ��������� ��� �������
		virtual bool BuildShader(
			MObject& node,				//!< ������
			cls::enParamType wantedoutputtype, //!< ����� ��� ����� �� ������
			cls::IShaderAssembler* as,		//!< render
			cls::IExportContext* context,	//!< context
			MObject& generatornode			//!< generator node �.� 0
			);
	};
	static Generator generator;
};

