#pragma once

#include "pre.h"
#include <maya/MIOStream.h>
#include "HairProcessor.h"

#include "Util/DllProcedure.h"

#include "supernova/IArtistShape.h"
#include "supernova/ISnShaper.h"
#include "supernova/SnData.h"

#include "ocellaris/IRender.h"
#include "ocellaris/renderCacheImpl.h"
#include "ocellaris/renderOpenglImpl.h"

struct DrawCache : public DrawSynk
{
	DrawCache();

	void ClearInstances();

public:
	virtual void ClearBox();

	/*/
	virtual void InitCV(int hairCountCV, int vertexInHairCV);
	virtual void setCVhair(int cvhair, int vertcount, const Math::Vec3f& rootpt, bool bVisible);
	virtual void setCVhairvertex(int cvhair, int vertindex, const Math::Vec3f& pt);
	/*/

	virtual void Init(
		int hairCount, int maxVertexInHair, enHairInterpolation interpolation, bool bNormals, std::vector<const char*>* adduv,
		int flags=0x0
		);

	// ���������� ��� ������ ������
	virtual void SetHair(
		int hairindex, 
		int hairid,
		HAIRPARAMS& params, 
		HAIRVERTS& hair,
		enRenderNormalSource renderNormalSource, 
		float RenderWidth, 
		float widthBias
		);

	virtual void sethair(int hair, int vertcount, float u, float v, int id, const Math::Vec3f& p, const Math::Vec3f& n, Math::Vec2f* adduv=NULL);
	virtual void sethairvertex(int hair, int vertindex, const Math::Vec3f& pt);
	virtual void sethaircolor(int hair, int vertindex, const Math::Vec3f& pt);
	// INSTANCE
	virtual void InitSn(int hairCount);
	virtual void setSnShape(int index, sn::ShapeShortcutPtr& ptr);
	virtual void setOcsShape(int index, cls::MInstance& cache);

	// ???
protected:
	// ������������ setHair
	virtual void setSnHair(int cvhair, int instance, HairPersonageData& data);

public:
	bool bValidBox;
	MBoundingBox getBox()const{return box;};
	int UI_getCVCount() const;
	MPoint UI_getCV(int index) const;
	int UI_getHairCount() const;
	MVector UI_getHair(int index) const;

	bool bDrawAsBox;

	mutable cls::renderOpenglImpl glrender;

protected:
	// ������ ������� CV
	std::vector<float> CVverts;
	std::vector<int> hairIndicies;
	std::vector<int> CVVertexOffset;	// �������� � CV �������
	std::vector<int> CVVertexCount;		// ����� ��������� � ������ CV ������
	std::vector<Math::Vec3f> CVroots;		// ����� CV
	std::vector<bool> CVvisible;

friend class HairProcessor;
friend struct HairPersonage;
	// ��� ���������
	int hairCount;
	std::vector<Math::Vec2f> uvs;
	std::vector<Math::Vec3f> hairpos;
	std::vector<float> Vertices;
	std::vector<Math::Vec2i> VerticesInds;
	std::vector<float> Color;
	std::vector<int> hairVertexOffset;	// �������� � �������
	std::vector<int> vertexInHair;		// ����� ��������� � ������ ������

	// INSTANCE
	mutable std::map< int, sn::ShapeShortcutPtr > snshapes;
	std::vector<HairPersonageData> instHairs;
	// OCS INSTANCE
	mutable std::map< int, cls::MInstance> ocsshapes;


	MBoundingBox box;
public:
	bool isInstance()
	{
		return !snshapes.empty() || !ocsshapes.empty();
	};

	void drawHair( bool colorize, bool asPoints) const;
	void drawHairText( M3dView &view) const;
	void drawCV(int index) const;
	void drawCVs() const;
	void drawInstance() const;

	void testOcsRender(MObject obj, M3dView* view) const;

};