#include "pre.h"

#include "HairCombTool.h"

#include "HairBrushProxy.h"
#include "HairControlsShape.h"

#include "HairCV.h"

#include "ocellaris/IRender.h"
#include "ocellaris/renderCacheImpl.h"

#include <mathNmaya/mathNmaya.h>
#include <mathNgl/mathNgl.h>

#include <maya/MModelMessage.h>
#include <maya/MArgList.h>
#include <maya/MUiMessage.h>

#include <algorithm>

/*/
������:
- �������� ������� ���������
- ���������� ��������� ����� ����� ���� HairBrushProxy
/*/

////////////////////////////////////////////////////////////////////////////////////////////////////
// HairCombTool - ������� ������ ����������.
////////////////////////////////////////////////////////////////////////////////////////////////////

		HairCombTool::HairCombTool()
{
	this->setCommandString(HairCombTool::typeName);
}

		HairCombTool::~HairCombTool() { }

void*	HairCombTool::creator()
{
	return new HairCombTool;
}

MStatus HairCombTool::doIt(const MArgList& args)
{
	return MStatus::kSuccess;
}

MStatus HairCombTool::redoIt()
{
	return MStatus::kSuccess;
}

MStatus HairCombTool::undoIt()
{
	return MStatus::kSuccess;
}

bool	HairCombTool::isUndoable() const
{
	return false;
}

MStatus HairCombTool::finalize()
{
	MArgList command;
	//command.addArg(commandString());
	//command.addArg("-size");
	//command.addArg(size);
	//command.addArg(lastPoint);
	//command.addArg(lastVector);
	//command.addArg(point);
	//command.addArg(vector);
	return MPxToolCommand::doFinalize(command);
}

MSyntax HairCombTool::newSyntax()
{
	MSyntax syntax;
	return syntax;
}

//void	HairCombTool::setVertices(std::list<)
////////////////////////////////////////////////////////////////////////////////////////////////////
// HairCombContext
////////////////////////////////////////////////////////////////////////////////////////////////////
HairCombContext*	HairCombContext::currentContext = NULL;
HHOOK				HairCombContext::hHookMouseMsg = NULL;
const MString		HairCombContext::brushProxyName = HairBrushProxy::typeName.substring(0, HairBrushProxy::typeName.length() - 6) + "1";

					HairCombContext::HairCombContext()
{
	size = 0.1f;
	this->setImage("chevelureCombTool.xpm", MPxContext::kImage1);
}

// ��� ������������� ����������
void				HairCombContext::getClassName( MString & name ) const
{
	name = toolName;
}

// �������������
void				HairCombContext::toolOnSetup(MEvent &event)
{
	MGlobal::executeCommand( MString("createNode -ss ") + HairBrushProxy::typeName + "; move -1 -1 0 " + brushProxyName + ";");
	if( currentContext == NULL)
	{
		MStatus stat;
		// Callback issued when selection list changes
		selectionChanged = MModelMessage::addCallback(
			MModelMessage::kActiveListModified,
			updateSelectionCB, 
			this, &stat);
		// ��� ������� ���������
		Unhook();

		// ��� ������� ���������
		M3dView view = M3dView::active3dView();
		DWORD ProcessId, ThreadId;
		ThreadId = GetWindowThreadProcessId(view.window(), &ProcessId);
		hHookMouseMsg = SetWindowsHookEx(WH_CALLWNDPROC, &MouseProc, 0, ThreadId); 

		// ������� ��������
		currentContext = this;

		updateSelection();
	}
	else
	{
		printf("HairCombContext::toolOnSetup currentContext != NULL!!!\n");
	}
}
void				HairCombContext::toolOffCleanup()
{
	displayStringD("HairCombContext::toolOffCleanup");

	// ��� ������� ���������
	Unhook();
	MGlobal::executeCommand( MString("delete ") + brushProxyName );

	if( currentContext == this)
	{
		MStatus stat;

		// Callback issued when selection list changes
		stat = MModelMessage::removeCallback(selectionChanged);

		// ������� ��������
		currentContext = NULL;
	}
}
void				HairCombContext::Unhook()
{
	if( hHookMouseMsg ) UnhookWindowsHookEx(hHookMouseMsg);
	hHookMouseMsg = NULL;
}

// �������: ������ ������
MStatus				HairCombContext::doPress( MEvent& event )
{
	// �� ������ ������
	manipulator = NULL;

	short x, y;
	event.getPosition(x, y);

	M3dView view = M3dView::active3dView();

	// projection space
	Math::Vec2f cursorPosPS = Math::Vec2f(
		x/(float)view.portWidth(), y/(float)view.portWidth()
		);
	cursorPosPS = cursorPosPS * 2 - Math::Vec2f(1, 1);

	Math::Matrix4f viewMatrix;	// view 
	Math::Matrix4f projMatrix;	// projection
	MMatrix m;
	view.modelViewMatrix(m);
	::copy(viewMatrix, m);
	view.projectionMatrix(m);
	::copy(projMatrix, m);

	// ������� worldMatrix
	Math::Matrix4f worldMatrix = Math::Matrix4f::id;	// ������� ���������� ����� � WS
	
	for ( size_t i = 0; i < targethairs.size(); i++ )
		this->prepare( targethairs[i], worldMatrix, viewMatrix, projMatrix, cursorPosPS );
	
	// ��� ��������� � HairCV::selectVertecies
	{
		Math::Vec4f vert(0, 0, 0, 1);


		vert = vert*worldMatrix;
		vert = vert*viewMatrix;
		vert = vert*projMatrix;
		vert *= 1/vert.w;

		//displayString("%f, %f - %f, %f", vert.x, vert.y, cursorPosPS.x, cursorPosPS.y);
	}

	// ��� ��������� ������ �����������
	// 
	std::string manipulatorname = "getTransformManipulator";

	#ifndef SAS
	const char* hairdllname = "HairMath.dll";
	#else
	const char* hairdllname = "chevelureMath.dll";
	#endif
	if( !dllproc.LoadFormated( manipulatorname.c_str(), hairdllname))
		return MS::kFailure;

	manipulator = (*dllproc)();
	if( !manipulator)
		return MS::kFailure;

	// ������ �����������
	cls::renderCacheImpl<> attributes;
	attributes.Attribute("worldMatrix", worldMatrix);
	attributes.Attribute("viewMatrix", viewMatrix);
	attributes.Attribute("projMatrix", projMatrix);
	attributes.Attribute("cursorPos", cursorPosPS);
	if( !manipulator->Start(&attributes))
	{
		return MS::kFailure;
	}

	Math::Vec2i cursorPos( x, y );
	// ��������
	drawBrush(view, cursorPos);
	//view.refresh( false, true );
	
	rememberCV();
	
	// ��������� �������. �������
	this->lastCursorPosPS = cursorPosPS;
	this->lastCursorPos = cursorPos;

	return MS::kSuccess;
}

// �������: ����������� ���� ��� ������� ������
MStatus				HairCombContext::doDrag( MEvent& event )
{
	short x, y;
	event.getPosition(x, y);

	M3dView view = M3dView::active3dView();

	// projection space
	Math::Vec2f cursorPosPS = Math::Vec2f(
		x/(float)view.portWidth(), y/(float)view.portWidth()
		);
	cursorPosPS = cursorPosPS * 2 - Math::Vec2f(1, 1);


	if( !manipulator)
		return MS::kFailure;

	// ������� worldMatrix
	Math::Matrix4f worldMatrix = Math::Matrix4f::id;

	// ������ �����������
	cls::renderCacheImpl<> attributes;
	attributes.Attribute("worldMatrix", worldMatrix);
	attributes.Attribute("cursorPos", cursorPosPS);
	attributes.Attribute("lastCursorPos", this->lastCursorPosPS);
	if( !manipulator->Step(&attributes))
	{
		return MS::kFailure;
	}
	
	std::list< AffectedHairs >::iterator affectedIt = affectedhairs.begin();
	std::list< HairCV* >::iterator cvIt = cv.begin();
	std::list< HairCvBasesData* >::iterator basesIt = bases.begin();
	for ( ; affectedIt != affectedhairs.end(); affectedIt++, cvIt++, basesIt )
	{
		AffectedHairs& affectedhairs = *affectedIt;
		HairCV *cv = *cvIt;
		HairCvBasesData *bases = *basesIt;
		
		std::map<int, std::vector<float> >::iterator it = affectedhairs.begin();
		for( ; it != affectedhairs.end(); it++)
		{
			int hairindex = it->first;
			HairCV::CV& hair = cv->getControlHair(hairindex);
			Math::Matrix4f& basis = bases->bases[hairindex];
			Math::Matrix4f& basis_inv = bases->bases_inv[hairindex];
			manipulator->ComputeSingleHair(hair, it->second, basis, basis_inv);
		}
		// affectedhairs;

		// ������ ����������� �� ��������� �����
		//virtual void ComputeSingleHair(
		//	HairCV::CV& hair,				// �����
		//	std::vector<float> affects,		// ������� ����������� �� ������ ������� ������
		//	Math::Matrix4f& basis,			// ������� ��� ��������� �� TS -> OS
		//	Math::Matrix4f& basis_inv		// ������� ��� ��������� �� OS -> TS
		//	)=0;
	}

	//execToolCommand(view, lastCursorPos, cursorPos);
	
	Math::Vec2i cursorPos( x, y );
	// ��������
	drawBrush(view, cursorPos);
	view.refresh( false, true );

	// ��������� �������. �������
	this->lastCursorPosPS = cursorPosPS;
	this->lastCursorPos = cursorPos;

	return MS::kSuccess;
}

// �������: ������ ��������
MStatus				HairCombContext::doRelease( MEvent& event )
{
	manipulator->Finish();
	
	//MDataHandle dHandle = data->outputValue( HairControlsShape::o_HairCV );
	//MPlug plug(targethairs[0].node(), HairControlsShape::o_HairCV);
	//plug.setValue(dHandle);
	//dHandle.setClean();
	
	affectedhairs.clear();
	data.clear();
	cv.clear();
	bases.clear();
	
	return MS::kSuccess;
}
// M�� �������: ����� �������� ����
MStatus				HairCombContext::doDragIdle( int x, int y)
{
	//if( targethairs.empty())
	//	return MS::kSuccess;

	M3dView view = M3dView::active3dView();
	Math::Vec2i cursorPos = Math::Vec2i(x, y);
	
	//view.refresh( false, true );

	// ��������
	drawBrush(view, cursorPos);

	return MS::kSuccess;
}

void				HairCombContext::drawBrush(M3dView& view, Math::Vec2i cursorPos)
{
	float rf = (float)std::min(view.portHeight(), view.portWidth());
	rf *= 0.5f * this->size;
	if( rf < 4 ) rf = 4;
	int r = (int)rf;

	int dx = cursorPos[0];
	int dy = cursorPos[1];
	double dz = r;
	MGlobal::executeCommand( MString("move ") + dx + " " + dy + " " + dz + " " + brushProxyName );
}

// ������ ����������.
void				HairCombContext::execToolCommand(const M3dView& view, const Math::Vec2i& lastCursorPos, const Math::Vec2i& cursorPos)
{
	//HairCombTool *cmd = (HairCombTool*)newToolCommand();
	//MPoint lastPoint, point;
	//MVector lastVector, vector;
	//view.viewToWorld(lastCursorPos[0], lastCursorPos[1], lastPoint, lastVector);
	//view.viewToWorld(cursorPos[0], cursorPos[1], point, vector);
	//cmd->setSize(size);
	//cmd->setLastPoint(lastPoint);
	//cmd->setLastVector(lastVector);
	//cmd->setPoint(point);
	//cmd->setVector(vector);
	//cmd->redoIt();
	//cmd->finalize();
}

// �������: ������ ������������ - ���������� ��������� ����� ��� � ���.
MStatus				HairCombContext::doHold( MEvent& event )
{
	short x, y;
	event.getPosition(x, y);
	//displayStringD("HairCombContext::doHold (%d, %d)", x, y);
	return MS::kSuccess;
}
LRESULT CALLBACK	HairCombContext::MouseProc( int nCode, WPARAM wParam, LPARAM lParam)
{
	CWPSTRUCT* ms = (CWPSTRUCT*)lParam;
	if( !currentContext
	||	ms->message != WM_NCHITTEST
		)
		return CallNextHookEx( hHookMouseMsg, nCode, wParam, lParam);
	
	M3dView view = M3dView::active3dView();
	HWND active3dView = view.window();

	static bool updated = false;

	if( ms->hwnd == active3dView )
	{
		updated = false;
	
		int xPos, yPos;
		xPos = LOWORD(ms->lParam);
		yPos = HIWORD(ms->lParam);

		POINT pt={xPos, yPos};
		ScreenToClient(active3dView, &pt);
		
		try
		{
			currentContext->doDragIdle(pt.x, view.portHeight()-pt.y);
		}
		catch(...)
		{
			displayString("HairCombContext::MouseProc exception\n");
		}
	}
	else
	{
		if (updated)
			return CallNextHookEx( hHookMouseMsg, nCode, wParam, lParam);
		
		int xPos, yPos;
		xPos = LOWORD(ms->lParam);
		yPos = HIWORD(ms->lParam);

		POINT pt={xPos, yPos};
		ScreenToClient(active3dView, &pt);
		int x = pt.x;
		int y = view.portHeight() - pt.y;

		if	( x < 0 || y < 0 || x > view.portWidth() || y > view.portHeight() )
		{
			MGlobal::executeCommand( MString("if (`objExists ") + brushProxyName + "`) { move -1 -1 0 " + brushProxyName + "; }" );
			updated = true;
		}
	}
	return CallNextHookEx( hHookMouseMsg, nCode, wParam, lParam);
}
void				HairCombContext::updateSelection()
{
	MStatus stat;
	targethairs.clear();

	MSelectionList list;
	stat = MGlobal::getActiveSelectionList(list);

	for(size_t i=0; i < list.length(); i++)
	{
		MDagPath dagPath;
		if ( !list.getDagPath(i, dagPath) ) continue;

		updateSelectionRec(dagPath, 0);
	}
	return;
}
void				HairCombContext::updateSelectionRec(MDagPath& dagPath, int level)
{
	MFnDependencyNode dn(dagPath.node());
	MTypeId id = dn.typeId();
	if( id == HairControlsShape::id )
	{
		for(size_t i = 0; i < targethairs.size(); i++)
			if(dagPath == targethairs[i]) return;
		targethairs.push_back(dagPath);
		return;
	}
	// childs
	for( size_t c = 0; c < dagPath.childCount(); c++)
	{
		MObject obj;
		obj = dagPath.child(c);
		MDagPath dagPath2 = MDagPath::getAPathTo(obj);
		updateSelectionRec(dagPath2, level + 1);
	}
}

void				HairCombContext::updateSelectionCB(void* data)
{
	HairCombContext* pThis = (HairCombContext*)data;
	pThis->updateSelection();
}



void				HairCombContext::rememberCV()
{
	if ( targethairs.size() == 0 )
		return;
	try
	{
	}
	catch (...)
	{
		MGlobal::displayInfo("exception while casting");
	}
}

void				HairCombContext::prepare( const MDagPath& path, const Math::Matrix4f& worldMatrix, const Math::Matrix4f& viewMatrix, const Math::Matrix4f& projMatrix, Math::Vec2f& combPosPS )
{
	// 
	// ������� ������ �� ������� ������������
	// 
	// ����� HairCV::selectVertecies
	// 
	// ��������� affectedhairs


	HairControlsShape *hairControlsShape = (HairControlsShape*) MFnDependencyNode(path.node()).userNode();
	data.push_back( &hairControlsShape->forceCache() );
	MDataBlock *data = this->data.back();

	cv.push_back( hairControlsShape->LoadHairCV( *data ) );
	HairCV *cv = this->cv.back();

	bases.push_back( hairControlsShape->LoadHairCvBases( *data ) );
	HairCvBasesData *bases = this->bases.back();
	
	std::list<int> selectedvert;
	cv->selectVertecies(
		bases->bases,
		worldMatrix, viewMatrix, projMatrix,
		combPosPS, this->size, selectedvert
		);
	MGlobal::displayInfo( MString() + selectedvert.size() );
	
	affectedhairs.push_back( AffectedHairs() );
	AffectedHairs& affectedhairs = this->affectedhairs.back();
	for (	std::list<int>::iterator it = selectedvert.begin();
			it != selectedvert.end();
			it++ )
	{
		// [����� �� HairControlsShape::transformUsing]
		int hairindex = 0, vertindex=0;
		Math::Vec3f* v = cv->getSingleIndexedVertex( *it, &hairindex );
		bool res = cv->getSingleIndexedVertex( *it, hairindex, vertindex );
		
		if( affectedhairs.find( hairindex ) == affectedhairs.end() )
		{
			HairCV::CV& hair = cv->getControlHair( hairindex );
			affectedhairs[hairindex].resize( hair.ts.size(), 0);
		}
		affectedhairs[hairindex][vertindex] = 1;
		// [/����� �� HairControlsShape::transformUsing]
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////
// HairCombContextCmd - ������� �������� ���������.
////////////////////////////////////////////////////////////////////////////////////////////////////
			HairCombContextCmd::HairCombContextCmd() 
{
	context = NULL;
};
MStatus		HairCombContextCmd::appendSyntax()
{
	MSyntax mySyntax = syntax();

	mySyntax.addFlag(
		"-sz", "-size",
		MSyntax::kDouble);
	return MS::kSuccess;
}
MStatus		HairCombContextCmd::doEditFlags()
{
	MArgParser argData = parser();
	
	if( argData.isFlagSet("-sz")) 
	{
		double size;
		if( argData.getFlagArgument("-sz", 0, size))
		{
			context->setSize((float)size);
		}
		if( argData.getFlagArgument("-size", 0, size))
		{
			context->setSize((float)size);
		}
	}

	return MS::kSuccess;
}

MStatus		HairCombContextCmd::doQueryFlags()
{
	MArgParser argData = parser();

	if (argData.isFlagSet("-sz")) 
	{
		setResult(context->getSize());
	}

	return MS::kSuccess;
}

MPxContext*	HairCombContextCmd::makeObj()
{
	context = new HairCombContext();
	return context;
}
void*		HairCombContextCmd::creator()
{
	return new HairCombContextCmd();
}

// �������� �������. ���������� ��������� ��� ������������� � ������� ������ �� shelf'e Chevelure.
MStatus		HairCombContextCmd::initShelf()
{
	MStatus status;
	if( (status = MGlobal::sourceFile("chevelureCombToolInit.mel")) != MStatus::kSuccess )
	{
		displayString("Error source chevelureCombToolInit.mel.");
		return status;
	}
	else
	{
		if( (status = MGlobal::executeCommand("initChevelureCombTool()")) != MStatus::kSuccess )
		{
			displayString("Error while executing initChevelureCombTool() command or dont found it.");


			return status;
		}
	}
	//if( !SourceMelFromResource(MhInstPlugin, "HairCombToolValues", "MEL", defines))
	//{
	//	displayString("error source HairCombToolValues.mel");
	//}
	//if( !SourceMelFromResource(MhInstPlugin, "HairCombToolProperties", "MEL", defines))
	//{
	//	displayString("error source HairCombToolProperties.mel");
	//}
	return MStatus::kSuccess;
}
