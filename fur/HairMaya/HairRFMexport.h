#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: HairStartBrowserCmdCmd.h
// MEL Command: HairStartBrowserCmd

#include <maya/MPxCommand.h>

class HairRFMexport : public MPxCommand
{
public:
	static const MString typeName;
	HairRFMexport();
	virtual	~HairRFMexport();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();

private:
	MString filePath;
};
