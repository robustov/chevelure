#pragma once

#include "pre.h"

#include "HairCVBasesData.h"

#include "IHairCVmanipulator.h"
#include "HairCV.h"

#include "Util/DllProcedure.h"

#include <maya/MPxContext.h>
#include <maya/MPxContextCommand.h>
#include <maya/MPxToolCommand.h>
#include <maya/MDataBlock.h>

// ��������
class HairCombContext : public MPxContext
{
	// !!! ���������� ������� � ������� ������ doPress �� doRelease

	std::list<MDataBlock*> data;			//
	std::list<HairCV*> cv;					// ��� ������� ����������� �����
	std::list<HairCvBasesData*> bases;		//

	// ����� ����� ������������
	Util::DllProcedureTmpl<getIHairCVmanipulator_t> dllproc;
	// �����������
	IHairCVmanipulator* manipulator;

	// ��� ������� ���������� �����

		// �� ����� ������ ������������ cvhair -> affect list
		typedef std::map<int, std::vector<float> > AffectedHairs;
		std::list< AffectedHairs > affectedhairs;

public:

	HairCombContext();

	// ��� ������������� ����������
	virtual void getClassName( MString & name ) const;
	// �������������
	virtual void toolOnSetup(MEvent &event);
	virtual void toolOffCleanup();

	// ���������
	virtual MStatus doDrag( MEvent& event );
	virtual MStatus doPress( MEvent& event );
	virtual MStatus doRelease( MEvent& event );
	virtual MStatus doHold( MEvent& event );
	// ��� ��������� (����� �������� ����)
	virtual MStatus doDragIdle( int x, int y );

public:
	float size;
	void setSize(float size){this->size=size;}
	float getSize(){return this->size;}

protected:
	// ��� ��������, ������������� �����.
	static const MString brushProxyName;

protected:
	Math::Vec2f lastCursorPosPS;	// � projSpace
	Math::Vec2i lastCursorPos;		// � ����������� �������� ��� ���������� �������� �����
	// ������� �����, �� ������� ����������� � ��������� ������� data, cv, bases, affectedhairs
	virtual void prepare( const MDagPath& path, const Math::Matrix4f& worldpos, const Math::Matrix4f& viewMatrix, const Math::Matrix4f& projMatrix, Math::Vec2f& combPosPS );
	virtual void rememberCV();
	// ����� �������.
	void execToolCommand( const M3dView& view, const Math::Vec2i& lastCursorPos, const Math::Vec2i& cursorPos );
	
protected:
	void beginOverlayDraw(M3dView& view);
	void endOverlayDraw(M3dView& view);
	
	void clear(HDC& hdc);
	void drawBrush(M3dView& view, Math::Vec2i cursorPos);

protected:
	// Callback issued when selection list changes
	MCallbackId selectionChanged;
	void updateSelection();
	static void updateSelectionCB(void* data);
	void updateSelectionRec(MDagPath& dagPath, int level);

	// ������ �� ������� ������������, ����������� � updateSelection
	std::vector<MDagPath> targethairs;

protected:
	// ��� ������� ���������
	static HHOOK hHookMouseMsg;
	static LRESULT CALLBACK MouseProc( int nCode, WPARAM wParam, LPARAM lParam);
public:
	static void Unhook();
protected:
	// ������� ��������
	static HairCombContext* currentContext;

	static const MString toolName;
};


// �������
class HairCombContextCmd : public MPxContextCommand
{
	HairCombContext* context;
public:
	HairCombContextCmd();
	virtual MPxContext* makeObj();

	virtual MStatus appendSyntax();
	///
	virtual MStatus doEditFlags();
	///
	virtual MStatus doQueryFlags();

public:
	// �������� �������. ���������� ��������� ��� ������������� � ������� ������ �� shelf'e Chevelure.
	static MStatus initShelf();
	static void* creator();
	static const MString typeName;
};

// ������� ��� ������ ����������.
class HairCombTool : public MPxToolCommand
{
public:
	static const MString typeName;

					HairCombTool();
	virtual			~HairCombTool();
	static void*	creator();
	MStatus			doIt( const MArgList& args );
	MStatus			redoIt(); 
	MStatus			undoIt();
	bool			isUndoable() const;
	MStatus			finalize();
	static MSyntax	newSyntax();

};
