#pragma once

#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include "Util/Stream.h"
#include "HairCV.h"

class HairControlsAnimData : public MPxData
{
public:
	// MTime -> KeyFrame
	std::map<double, HairCV> keys;

	// ���� ��� �������� ����������
	bool bModify;
	// ��� ����������� �����
	std::string loadedFilename;
	// ��� ���� - ����������� � HairControlAnim
	std::string relative_filename;

	// ��������
	bool setKey(MTime t, MObject val);
	bool deleteKey(MTime t);
	bool saveAnim(const char* filename);
	bool loadAnim(const char* filename);
	bool getCv(MTime t, HairCV& cv);

public:
	HairControlsAnimData();
	virtual ~HairControlsAnimData(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

public:
	static MTypeId id;
	static const MString typeName;
	static int current_file_version;

protected:
	void serialize(Util::Stream& stream);
	void serializeData(Util::Stream& stream);
};
