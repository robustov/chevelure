//
// Copyright (C) 
// File: HairModifierDataCmd.cpp
// MEL Command: HairModifierData

#include "HairModifierData.h"

#include <maya/MGlobal.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include "textureCacheData.h"
#include <sstream>

bool HairModifierData::Init()
{
	return true;
}

void HairModifierData::setArray( enType name, MPxData* _data)
{
	textureCacheData* data = static_cast<textureCacheData*>(_data);
	Math::ParamTex2d<unsigned char>* param = NULL;

	switch(name)
	{
		case T_LENGTH		: param = (Math::ParamTex2d<unsigned char>*)&this->data.length; break;
		case T_INCLINATION	: param = (Math::ParamTex2d<unsigned char>*)&this->data.inclination; break;
		case T_POLAR		: param = (Math::ParamTex2d<unsigned char>*)&this->data.polar; break;
		case T_BASECURL		: param = (Math::ParamTex2d<unsigned char>*)&this->data.baseCurl; break;
		case T_TIPCURL		: param = (Math::ParamTex2d<unsigned char>*)&this->data.tipCurl; break;
		case T_BASEWIDTH	: param = (Math::ParamTex2d<unsigned char>*)&this->data.baseWidth; break;
		case T_TIPWIDTH		: param = (Math::ParamTex2d<unsigned char>*)&this->data.tipWidth; break;
		case T_SCRAGGLE		: param = (Math::ParamTex2d<unsigned char>*)&this->data.scragle; break;
		case T_SCRAGGLEFREQ	: param = (Math::ParamTex2d<unsigned char>*)&this->data.scragleFreq; break;
		case T_BASECLUMP	: param = (Math::ParamTex2d<unsigned char>*)&this->data.baseClump; break;
		case T_TIPCLUMP		: param = (Math::ParamTex2d<unsigned char>*)&this->data.tipClump; break;
		case T_TWIST		: param = (Math::ParamTex2d<unsigned char>*)&this->data.twist; break;
		case T_DISPLACE		: param = (Math::ParamTex2d<unsigned char>*)&this->data.displace; break;
	}

	if(!param) return;
	*param = data->paramTex2d;
}


HairModifierData::HairModifierData()
{
}
HairModifierData::~HairModifierData()
{
}

MTypeId HairModifierData::typeId() const
{
	return HairModifierData::id;
}

MString HairModifierData::name() const
{ 
	return HairModifierData::typeName; 
}

void* HairModifierData::creator()
{
	return new HairModifierData();
}

void HairModifierData::copy( const MPxData& other )
{
	const HairModifierData* arg = (const HairModifierData*)&other;
	this->data = arg->data;
}

MStatus HairModifierData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("HairModifierData: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus HairModifierData::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus HairModifierData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	serialize(stream);

	return MS::kSuccess;
}

MStatus HairModifierData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

void HairModifierData::serialize(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version>=0)
	{
		stream >> data;
	}
}

