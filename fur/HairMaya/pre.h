#pragma once

#include <Windows.h>

#include <math.h>
#include <maya/MIOStream.h>

#include <maya/MPxNode.h>   

#include <maya/MPxLocatorNode.h> 

#include <maya/MFnStringData.h> 
#include <maya/MArgDatabase.h> 
#include <maya/MPxCommand.h> 
#include <maya/MString.h> 
#include <maya/MTypeId.h> 
#include <maya/MPlug.h>
#include <maya/MVector.h>
#include <maya/MAngle.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MColor.h>
#include <maya/M3dView.h>
#include <maya/MDistance.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshPolygon.h>

#include <maya/MFnUnitAttribute.h>
#include <maya/MFnNumericAttribute.h>

#include <maya/MFn.h>
#include <maya/MPxNode.h>
#include <maya/MPxManipContainer.h> 
#include <maya/MCommandResult.h> 
#include <maya/MQuaternion.h> 
#include <maya/MFnNurbsSurfaceData.h> 
#include <maya/MPxGeometryData.h> 

#include <maya/MFnCircleSweepManip.h>
#include <maya/MFnDirectionManip.h>
#include <maya/MFnDiscManip.h>
#include <maya/MFnDistanceManip.h> 
#include <maya/MFnFreePointTriadManip.h>
#include <maya/MFnStateManip.h>
#include <maya/MFnToggleManip.h>

#include <maya/MPxContext.h>
#include <maya/MPxSelectionContext.h>
#include <maya/MFnPluginData.h>

#include <maya/MFnNumericData.h>
#include <maya/MManipData.h>
#include <maya/MTime.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnMatrixData.h>
#include <maya/MMatrix.h>
#include <maya/MGlobal.h>
#include <maya/MAnimMessage.h>
#include <maya/MMessage.h>
#include <maya/MPlugArray.h>
#include <maya/MFnAnimCurve.h>
#include <maya/MAnimControl.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MVectorArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MNodeMessage.h>

#include <maya/MIOStream.h>
#include <maya/MPxSurfaceShape.h>
#include <maya/MPxSurfaceShapeUI.h>
//#include <maya/MFnPlugin.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MPoint.h>
#include <maya/MPlug.h>
#include <maya/MDrawData.h>
#include <maya/MDrawRequest.h>
#include <maya/MSelectionMask.h>
#include <maya/MSelectionList.h>
#include <maya/MDagPath.h>
#include <maya/MMaterial.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFnNurbsCurveData.h>
#include <GL/glu.h>
#include <maya/MPointArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MFloatVector.h>
#include <maya/MFnNurbsSurface.h>

#include <maya/MIOStream.h>
#include <maya/MPxSurfaceShape.h>
#include <maya/MPxSurfaceShapeUI.h>
#include <maya/MAttributeSpec.h>
#include <maya/MAttributeSpecArray.h>
#include <maya/MAttributeIndex.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MPoint.h>
#include <maya/MPlug.h>
#include <maya/MDrawData.h>
#include <maya/MDrawRequest.h>
#include <maya/MSelectionMask.h>
#include <maya/MSelectionList.h>
#include <maya/MDagPath.h>
#include <maya/MMaterial.h>
#include <maya/MFnSingleIndexedComponent.h>
#include <maya/MFileIO.h>
#include <GL/glu.h>
#include <time.h>

#include "Math\Math.h"
#include <string>
#include <vector>
#include <map>
#include <list>
#include <set>
#include "Util/misc_create_directory.h"

// ������� ��� �������� ��������
extern std::map<std::string, std::string> defines;

#define DATACOPY(ARG) this->##ARG = arg->##ARG;
#define COPYCONST(MEMBER) this->##MEMBER = ##MEMBER;
#define SERIALIZE(MEMBER) stream >> this->##MEMBER;

extern bool hair_suppressProgress;
/*/
#define e \
    if (MS::kSuccess != s) { \
		char buf[256];\
		sprintf(buf, "ERROR %d ", __LINE__);\
		MGlobal::displayError(buf);\
    }
/*/

inline std::string scenefoldername()
{
	std::string filename = MFileIO::currentFile().asChar();
	filename = Util::extract_foldername(filename.c_str());
	return filename;
}

inline void displayString(const char* text, ...)
{
	va_list argList; va_start(argList, text);
	char sbuf[1024];
	_vsnprintf(sbuf, 1024, text, argList);
	sbuf[1023]=0;
	va_end(argList);
//	MGlobal::displayInfo(sbuf);

	struct tm* gmt;
	time_t t = time(NULL);
	gmt = localtime(&t);
	std::string mes = asctime( gmt );
	mes += sbuf;
	MGlobal::displayInfo(mes.c_str());
}
inline void displayStringError(const char* text, ...)
{
	va_list argList; va_start(argList, text);
	char sbuf[1024];
	_vsnprintf(sbuf, 1024, text, argList);
	va_end(argList);
//	MGlobal::displayInfo(sbuf);

	struct tm* gmt;
	time_t t = time(NULL);
	gmt = localtime(&t);
	std::string mes = asctime( gmt );
	mes += sbuf;
	MGlobal::displayError(mes.c_str());
}

#ifdef _DEBUG
	#define UL_TRACE
#else
//	#define UL_TRACE
#endif

inline void displayStringD(const char* text, ...)
{
#ifdef UL_TRACE
	
	va_list argList; va_start(argList, text);
	char sbuf[1024];
	_vsnprintf(sbuf, 1024, text, argList);
	va_end(argList);
//	MGlobal::displayInfo(sbuf);

	struct tm* gmt;
	time_t t = time(NULL);
	gmt = localtime(&t);
	std::string mes = asctime( gmt );
	mes += sbuf;
	MGlobal::displayInfo(mes.c_str());
#endif
}

inline MMatrix getMatrixForPlug(MPlug worldMatrix, MTime t)
{
	MDGContext context(t);
	MObject val;
	worldMatrix.getValue(val, context);
	MFnMatrixData matrData(val);
	MMatrix m = matrData.matrix();
	return m;
}

inline MMatrix move( const MMatrix& m, const MVector& v)
{
	MMatrix L = m;
	L(3, 0) += v[0];
	L(3, 1) += v[1];
	L(3, 2) += v[2];
//	L(3, 3) += v[3];
	return L;
}

inline void displayPoint( const MPoint& p)
{
	displayString("%g, %g, %g, %g", p[0], p[1], p[2], p[3]);
}

inline void displayMatrix( const MMatrix& L)
{
	displayString("%g, %g, %g, %g", L(0, 0), L(0, 1), L(0, 2), L(0, 3));
	displayString("%g, %g, %g, %g", L(1, 0), L(1, 1), L(1, 2), L(1, 3));
	displayString("%g, %g, %g, %g", L(2, 0), L(2, 1), L(2, 2), L(2, 3));
	displayString("%g, %g, %g, %g", L(3, 0), L(3, 1), L(3, 2), L(3, 3));
}
inline void displayMatrix( const Math::Matrix4f& L)
{
	displayString("%g, %g, %g, %g", L[0][0], L[0][1], L[0][2], L[0][3]);
	displayString("%g, %g, %g, %g", L[1][0], L[1][1], L[1][2], L[1][3]);
	displayString("%g, %g, %g, %g", L[2][0], L[2][1], L[2][2], L[2][3]);
	displayString("%g, %g, %g, %g", L[3][0], L[3][1], L[3][2], L[3][3]);
}

// CMD
inline MStatus nodeFromName(MString name, MObject & obj)
{
	MSelectionList tempList;
    tempList.add( name );
    if ( tempList.length() > 0 ) 
	{
		tempList.getDependNode( 0, obj );
		return MS::kSuccess;
	}
	return MS::kFailure;
}
inline MStatus plugFromName(MString name, MPlug & plug)
{
	MSelectionList tempList;
    tempList.add( name );
    if ( tempList.length() > 0 ) 
	{
		tempList.getPlug( 0, plug );
		return MS::kSuccess;
	}
	return MS::kFailure;
}
enum aTypes
{
	atReadable		= 0x1,
	atUnReadable	= 0x10000,
	atWritable		= 0x2,
	atUnWritable	= 0x20000,
	atConnectable	= 0x4,
	atUnConnectable	= 0x40000,
	atStorable		= 0x8,
	atUnStorable	= 0x80000,
	atCached		= 0x10,
	atUnCached		= 0x100000,
	atArray			= 0x20, 
	atKeyable		= 0x40,
	atHidden		= 0x80,
	atUnHidden		= 0x800000,
	atUsedAsColor	= 0x100,
	atRenderSource	= 0x200,

	atInput			= atReadable|atWritable|atStorable|atCached,
	atOutput		= atReadable|atUnWritable|atConnectable|atUnStorable,
};

inline void addAttribute(MObject& aobj, int flags)
{
	if( aobj.isNull())
	{
		displayString("cant create attribute");
		throw MString("cant create attribute");
	}

	MFnAttribute attr(aobj);
	if(flags & atReadable)
		attr.setReadable(true);
	if(flags & atUnReadable)
		attr.setReadable(false);

	if(flags & atWritable)
		attr.setWritable(true);
	if(flags & atUnWritable)
		attr.setWritable(false);

	if(flags & atConnectable)
		attr.setConnectable(true);
	if(flags & atUnConnectable)
		attr.setConnectable(false);

	if(flags & atStorable)
		attr.setStorable(true);
	if(flags & atUnStorable)
		attr.setStorable(false);

	if(flags & atCached)
		attr.setCached(true);
	if(flags & atUnCached)
		attr.setCached(false);

	if(flags & atArray)
		attr.setArray(true);
	if(flags & atKeyable)
		attr.setKeyable(true);

	if(flags & atHidden)
		attr.setHidden(true);
	if(flags & atUnHidden)
		attr.setHidden(false);

	if(flags & atUsedAsColor)
		attr.setUsedAsColor(true);
	if(flags & atRenderSource)
		attr.setRenderSource(true);

	MStatus stat;
	stat = MPxNode::addAttribute(attr.object());
	if(!stat)
	{
		displayString("cant add attribute %s", attr.name().asChar());
		throw attr.name();
	}
}

inline MStatus SourceMelFromResource(HINSTANCE hModule, LPCSTR lpName, LPCSTR lpType)
{
	HRSRC res = FindResource( hModule, lpName, lpType);
	if( !res)
	{
		displayString("FindResource %s failed", lpName);
		return MS::kFailure;
	}

	HGLOBAL mem = ::LoadResource(MhInstPlugin, res);
	if(!mem)
	{
		displayString("LoadResource %s failed", lpName);
		return MS::kFailure;
	}
	DWORD size = SizeofResource(hModule, res);

	char* data = (char*)LockResource(mem);
	if(!data)
	{
		displayString("LockResource %s failed", lpName);
		return MS::kFailure;
	}

	MString str(data, size);
	MStatus stat = MGlobal::executeCommand(str);
	if(!stat)
	{
		displayString("execute %s failed", lpName);
		return MS::kFailure;
	}
	return stat;
}

inline MStatus SourceMelFromResource(HINSTANCE hModule, LPCSTR lpName, LPCSTR lpType, std::map<std::string, std::string>& defines, bool writeToFile=false)
{
	HRSRC res = FindResource( hModule, lpName, lpType);
	if( !res)
	{
		displayString("FindResource %s failed", lpName);
		return MS::kFailure;
	}

	HGLOBAL mem = ::LoadResource(MhInstPlugin, res);
	if(!mem)
	{
		displayString("LoadResource %s failed", lpName);
		return MS::kFailure;
	}
	DWORD size = SizeofResource(hModule, res);

	char* data = (char*)LockResource(mem);
	if(!data)
	{
		displayString("LockResource %s failed", lpName);
		return MS::kFailure;
	}
	std::string text( data, size);

	// defines
	std::map<std::string, std::string>::iterator it = defines.begin();
	for(;it != defines.end(); it++)
	{
		const std::string& match = it->first;
		size_t pos;
		for( ;(pos=text.find(match))!=std::string::npos; )
		{
			text.replace(pos, match.size(), it->second);
		}
	}

	MString str(text.c_str());
	MStatus stat = MGlobal::executeCommand(str);
	if(!stat)
	{
#ifdef _DEBUG
		writeToFile = true;
#endif// _DEBUG

		displayString("execute %s failed", lpName);
		stat = MS::kFailure;
	}

	if( writeToFile)
	{
		std::string fn = "m:/";
		fn += lpName;
		fn += ".mel";
		FILE* file = fopen(fn.c_str(), "wt");
		if(file)
		{
			fputs(text.c_str(), file);
//		fwrite(text.c_str(), text.size(), 1, file);
			fclose(file);
		}
	}

	return stat;
}

