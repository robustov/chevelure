#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: HairDeformer_WindNode.h
//
// Dependency Graph Node: HairDeformer_Wind

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include "HairDeformer.h"


class HairDeformer_Wind : public HairDeformer
{
public:
	HairDeformer_Wind();
	virtual ~HairDeformer_Wind(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_Deformer[2];	// Example input attribute
	static MObject i_envelope[3];	// ����������� ���������
	static MObject o_output;		// Example output attribute
	static MObject i_PaintResXY[2];

	static MObject i_time;			// Example input attribute
	static MObject i_fftDimention;
	static MObject i_fftPhysicalSize;
	static MObject i_fftFilterSize;
	static MObject i_fftWaveDumper;
	static MObject i_fftWindAngle;
	static MObject i_fftWindSpeed;
	static MObject i_affectTipCurl;
	static MObject i_affectBaseCurl;
	static MObject i_affectInclination;

public:
	static MTypeId id;
	static const MString typeName;
};

