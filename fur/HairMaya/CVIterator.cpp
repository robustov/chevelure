#include "CVIterator.h"

#include <maya/MIOStream.h>

CVIterator::CVIterator( void * geom, MObjectArray & comps )
	: MPxGeometryIterator( geom, comps ),
	geometry( (HairProcessor*)geom )
{
	reset();
}

CVIterator::CVIterator( void * geom, MObject & comps )
	: MPxGeometryIterator( geom, comps ),
	geometry( (HairProcessor*)geom )
{
	reset();
}

void CVIterator::reset()
//
// Description
//
//  	
//   Resets the iterator to the start of the components so that another
//   pass over them may be made.
//
{
	MPxGeometryIterator::reset();
	setCurrentPoint( 0 );
	if ( NULL != geometry ) 
	{
		int maxVertex = geometry->getCVCount();
		setMaxPoints( maxVertex );
	}
}

/* override */
MPoint CVIterator::point() const
//
// Description
//
//    Returns the point for the current element in the iteration.
//    This is used by the transform tools for positioning the
//    manipulator in component mode. It is also used by deformers.	 
//
{
	MPoint pnt;
	if ( NULL != geometry ) 
	{
		Math::Vec3f v = geometry->getCV( index() );
		pnt = MPoint(v.x, v.y, v.z);
	}
	return pnt;
}

/* override */
void CVIterator::setPoint( const MPoint & pnt ) const
//
// Description
//
//    Set the point for the current element in the iteration.
//    This is used by deformers.	 
//
{
	if ( NULL != geometry ) 
	{
		geometry->setCV( index(), Math::Vec3f((float)pnt.x, (float)pnt.y, (float)pnt.z));
	}
}

/* override */
int CVIterator::iteratorCount() const
{
//
// Description
//
//    Return the number of vertices in the iteration.
//    This is used by deformers such as smooth skinning
//
	return geometry->getCVCount();
	
}

/* override */
bool CVIterator::hasPoints() const
//
// Description
//
//    Returns true since the shape data has points.
//
{
	return true;
}

