#include "pre.h"
#include "HairArtistShape2.h"
//#include "SnShape.h"
#include "mathNmaya/mathNmaya.h"
#include "textureCacheData.h"
#include <maya/MFnStringArrayData.h>


HairArtistShape2::HairArtistShape2(MObject& obj)
{
//	this->datablock = datablock;
	this->obj = obj;
	this->sourceObject = MObject::kNullObj;

	MStatus stat;
	MFnDependencyNode dn(obj);
	MPlug plug = dn.findPlug("i_message", &stat);
	if( stat)
	{
		MPlugArray pa;
		plug.connectedTo(pa, true, false, &stat);
		if( stat && pa.length()==1)
		{
			this->sourceObject = pa[0].node();
		}
	}
}
MObject HairArtistShape2::getThis()
{
	return obj;
}
MObject HairArtistShape2::getSourceObject()
{
	return sourceObject;
}

MPlug HairArtistShape2::getPlug(const char* name)
{
	MStatus stat;
	MFnDependencyNode dn(obj);
	return dn.findPlug(name, &stat);
}


MObject HairArtistShape2::getValue(const char* name, sn::VALUETYPE type, sn::ATTRIBUTE_PLACE place, const char* defminmax)
{
	MStatus stat;
	MObject attr = findNaddAttr(obj, name, type, place, defminmax);
	if(attr.isNull()) 
		return MObject::kNullObj;

	// ������!
	if( attr.hasFn(MFn::kNumericAttribute))
		return MObject::kNullObj;

	MFnDependencyNode dn(obj);
	MPlug plug = dn.findPlug(attr, &stat);
	MObject obj;
	if( !plug.getValue(obj))
		return MObject::kNullObj;

	return obj;
}
bool HairArtistShape2::getValue( bool& val, const char* name, sn::ATTRIBUTE_PLACE place, bool defval)
{
	char buf[256]; _snprintf(buf, 256, "%d", defval?1:0);
	MObject attr = findNaddAttr(obj, name, sn::VT_BOOL, place, buf);
	return getValue( val, attr);
}
bool HairArtistShape2::getValue( bool& val, MObject attr)
{
	if(attr.isNull()) return false;

	MStatus stat;
	MFnDependencyNode dn(obj);
	MPlug plug = dn.findPlug(attr, &stat);
	if( !plug.getValue(val))
		return false;
	return true;
}

bool HairArtistShape2::getValue( int& val, const char* name, sn::ATTRIBUTE_PLACE place, int defval, int min, int max)
{
	char buf[256]; _snprintf(buf, 256, "%d %d %d", defval, min, max);
	MObject attr = findNaddAttr(obj, name, sn::VT_INT, place, buf);
	return getValue( val, attr);
}
bool HairArtistShape2::getValue( int& val, MObject attr)
{
	if(attr.isNull()) return false;

	MStatus stat;
	MFnDependencyNode dn(obj);
	MPlug plug = dn.findPlug(attr, &stat);
	if( !plug.getValue(val))
		return false;
	return true;
}
bool HairArtistShape2::getValue( float& val, const char* name, sn::ATTRIBUTE_PLACE place, float defval, float min, float max)
{
	char buf[256]; _snprintf(buf, 256, "%f %f %f", defval, min, max);
	MObject attr = findNaddAttr(obj, name, sn::VT_FLOAT, place, buf);
	return getValue( val, attr);
}
bool HairArtistShape2::getValue( float& val, MObject attr)
{
	if(attr.isNull()) return false;

	MStatus stat;
	MFnDependencyNode dn(obj);
	MPlug plug = dn.findPlug(attr, &stat);
	if( !plug.getValue(val))
		return false;
	return true;
}
bool HairArtistShape2::getValue( Math::Vec3f& val, const char* name)
{
	MObject attr = findNaddAttr(obj, name, sn::VT_VECTOR3, sn::UNIFORM_SHAPE, NULL);
	return getValue( val, attr);
}

bool HairArtistShape2::getValue( Math::Vec3f& val, MObject attr)
{
	if(attr.isNull()) return false;

	MStatus stat;
	MFnDependencyNode dn(obj);
	MPlug plug = dn.findPlug(attr, &stat);
	MObject vall;
	if( !plug.getValue(vall))
		return false;
	MFnNumericData data(vall, &stat);
	if( !stat)
		return false;
	if( data.numericType()==MFnNumericData::k3Double)
	{
		Math::Vec3d v4d;
		stat = data.getData(v4d.x, v4d.y, v4d.z);
		val.x = (float)v4d.x;
		val.y = (float)v4d.y;
		val.z = (float)v4d.z;
	}
	else if( data.numericType()==MFnNumericData::k3Float)
	{
		stat = data.getData(val.x, val.y, val.z);
	}
	return true;
}

bool HairArtistShape2::getValue( Math::Matrix4f& val, const char* name)
{
	MObject attr = findNaddAttr(obj, name, sn::VT_MATRIX4X4, sn::UNIFORM_SHAPE, NULL);
	return getValue( val, attr);
}

bool HairArtistShape2::getValue( Math::Matrix4f& val, MObject attr)
{
	if(attr.isNull()) return false;

	MStatus stat;
	MFnDependencyNode dn(obj);
	MPlug plug = dn.findPlug(attr, &stat);

	MObject valobj;
	if( !plug.getValue(valobj))
		return false;

	MFnMatrixData matDataFn( valobj, &stat);
	if( !stat) return false;
	MMatrix mm = matDataFn.matrix();
	copy( val, mm);
	return true;
}

bool HairArtistShape2::getValue( std::string& val, const char* name, sn::ATTRIBUTE_PLACE place, const char* defval)
{
	MObject attr = findNaddAttr(obj, name, sn::VT_STRING, place, defval);
	return getValue( val, attr);
}

bool HairArtistShape2::getValue( std::string& val, MObject attr)
{
	val = "";
	if(attr.isNull()) return false;
	MStatus stat;
	MFnDependencyNode dn(obj);
	MPlug plug = dn.findPlug(attr, &stat);
	MString str;
	if( !plug.getValue(str))
		return true;
	val = str.asChar();
	return true;
}
/*/
bool HairArtistShape2::getValue( artist::ShapeValue& val, const char* name)
{
	MObject attr = findNaddAttr(obj, name, sn::VT_SNSHAPE, sn::UNIFORM_SHAPE, NULL);
	if(attr.isNull()) return false;

	MStatus stat;
	MFnDependencyNode dn(obj);
	MPlug plug = dn.findPlug(attr, &stat);

	MPlugArray pa;
	plug.connectedTo(pa, true, false);
	if( pa.length()!=1)
		return false;
	plug = pa[0];

	MObject valobj;
	if( !plug.getValue(valobj))
		return false;
	MFnPluginData pdata( valobj, &stat);
	if( !stat)
		return false;
	SnData* sndata = (SnData*)pdata.constData(&stat);
	if( !sndata)
		return false;

//	val.data = sndata->data;
	val.ssp = sndata->ssp;
	val.attrlist = sndata->attrlist;
	return true;
}
/*/
bool HairArtistShape2::getValue( std::vector<char>& data, const artist::attr_decl& val)
{
	MObject attr = findNaddAttr(obj, val.name.c_str(), val.type, val.place, val.defminmax.c_str());
	if(attr.isNull()) return false;

	switch(val.type)
	{
	case sn::VT_BOOL:
		{
			bool val;
			if( !getValue(val, attr)) return false;
			data = std::vector<char>( (char*)&val, (char*)&val + sizeof(val));
			return true;
		}
	case sn::VT_INT:
		{
			int val;
			if( !getValue(val, attr)) return false;
			data = std::vector<char>( (char*)&val, (char*)&val + sizeof(val));
			return true;
		}
	case sn::VT_FLOAT:
		{
			float val;
			if( !getValue(val, attr)) return false;
			data = std::vector<char>( (char*)&val, (char*)&val + sizeof(val));
			return true;
		}
	case sn::VT_VECTOR3:
	case sn::VT_COLOR:
		{
			Math::Vec3f val;
			if( !getValue(val, attr)) return false;
			data = std::vector<char>( (char*)&val, (char*)&val + sizeof(val));
			return true;
		}
	case sn::VT_MATRIX4X4:
		{
			Math::Matrix4f val;
			if( !getValue(val, attr)) return false;
			data = std::vector<char>( (char*)&val, (char*)&val + sizeof(val));
			return true;
		}
	case sn::VT_STRING:
		{
			std::string val;
			if( !getValue(val, attr)) return false;
			data = std::vector<char>( val.c_str(), val.c_str() + (val.size()+1));
			return true;
		}
	}
	return false;
}
bool HairArtistShape2::getValue( Math::Ramp& ramp, const char* name, float min, float max, float defval)
{
	MStatus stat;
	MFnDependencyNode dn(obj);
	MObject attr = dn.attribute(name, &stat);
	if( attr.isNull()) return false;

	MRampAttribute rampattr(obj, attr);
	ramp.set(rampattr, min, max, defval);
	return true;
}
bool HairArtistShape2::getValue( Math::ParamTex<unsigned char>& paramtex, const char* name, float defval)
{
	MStatus stat;
	MFnDependencyNode dn(obj);
	std::string cachename = name + std::string("Cache");
	MObject attr = dn.attribute(cachename.c_str(), &stat);
	if( attr.isNull())
	{
		// �������
		MObject attrs[4];
		bool bStorableAttributes = true;
		if( !CreateAttrNTextureCache4(obj, name, name, attrs, 0, 1, defval, bStorableAttributes))
			return MS::kFailure;

		attr = dn.attribute(cachename.c_str(), &stat);
		if( attr.isNull())
			return false;

		MPlug plug(obj, attrs[0]);
		plug.setValue((double)defval);
	}

	MObject outval;
	MPlug(obj, attr).getValue(outval);
	MPxData* pxdata = MFnPluginData(outval).data();
	if( !pxdata) return false;

	textureCacheData* data = static_cast<textureCacheData*>(pxdata);
	paramtex = data->paramTex2d;

	return true;
}

// ��������� �������� ���. ������� (������� ������� ������ �� ���� � ������������� �� ���� �����)
std::string HairArtistShape2::mirrorAttr( MObject src, const char* attribute, int index)
{
	MStatus stat;
	MFnDependencyNode dn(src);
	MPlug plugsrc = dn.findPlug(attribute, &stat);
	if(!stat) return "";
	MObject attrobj = plugsrc.attribute(&stat);
	if( !stat) return "";
	if( index >= 0 && plugsrc.isArray())
		plugsrc = plugsrc[index];

	if( plugsrc.isNull()) return "";

	std::string myname = std::string(dn.name().asChar()) + std::string("_") + std::string(attribute);
	if( index>=0)
	{
		char buf[24];_snprintf(buf, 24, "_%d_", index);myname+=buf;
	}

	MFnAttribute attr(attrobj);
	MFnDependencyNode mydn(this->obj);
	MPlug plugdst = mydn.findPlug(myname.c_str(), &stat);
	if( stat)
	{
		MPlugArray array;
		plugdst.connectedTo(array, true, false);
		if(array.length()==1 && array[0]==plugsrc)
			return myname;
		// �� �����������
		::displayString("connectAttr %s %s", plugsrc.name().asChar(), plugdst.name().asChar());
		MDGModifier mod;
		mod.connect(plugsrc, plugdst);
		mod.doIt();
		return myname;
	}

	if( attrobj.hasFn( MFn::kTypedAttribute))
	{
		MFnTypedAttribute srcattr(attrobj);
		MFnTypedAttribute dstattr;
		MObject i_attr = dstattr.create(myname.c_str(), myname.c_str(), srcattr.attrType(), &stat);
		if( !stat) return false;
		dstattr.setStorable(false);
		dstattr.setWritable(true);
		dstattr.setReadable(true);
		dstattr.setConnectable(true);
		dstattr.setHidden(true);
		dstattr.setInternal(true);

//		displayString( attrobj.apiTypeStr());
		mydn.addAttribute(i_attr);
		plugdst = mydn.findPlug(i_attr, &stat);
		if( !stat) return "";

		::displayString("connectAttr %s %s", plugsrc.name().asChar(), plugdst.name().asChar());
		MDGModifier mod;
		mod.connect(plugsrc, plugdst);
		mod.doIt();
	}
	else if( attrobj.hasFn( MFn::kNumericAttribute))
	{
		MFnNumericAttribute srcattr(attrobj);
		MFnNumericAttribute dstattr;
		MObject i_attr = dstattr.create(myname.c_str(), myname.c_str(), srcattr.unitType(), 0, &stat);
		if( !stat) return false;
		dstattr.setStorable(false);
		dstattr.setWritable(true);
		dstattr.setReadable(true);
		dstattr.setConnectable(true);
		dstattr.setHidden(true);
		dstattr.setInternal(true);

//		displayString( attrobj.apiTypeStr());
		mydn.addAttribute(i_attr);
		plugdst = mydn.findPlug(i_attr, &stat);
		if( !stat) return "";

		::displayString("connectAttr %s %s", plugsrc.name().asChar(), plugdst.name().asChar());
		MDGModifier mod;
		mod.connect(plugsrc, plugdst);
		mod.doIt();
	}

	return myname;
}

// ������ ��������
sn::attr_t HairArtistShape2::getAttr(
	const char* name,
	sn::VALUETYPE type, 
	sn::ATTRIBUTE_PLACE place, const char* defminmax)
{
	artist::attr_decl ad;
	ad.defminmax = defminmax?defminmax:"";
	ad.name = name;
	ad.place = place;
	ad.type = type;
	attrlist.push_back(ad);
	return sn::attr_t( name, type, sn::PER_UNKNOWN, -1);
}
sn::attr_t HairArtistShape2::getShaderAttr(
	const char* name, 
	const char* type)
{
	if(strcmp( type, "float")==0)
		return getAttr(name, sn::VT_FLOAT, sn::UNIFORM_SHAPE);
	if(strcmp( type, "color")==0)
		return getAttr(name, sn::VT_COLOR, sn::UNIFORM_SHAPE);
	if(strcmp( type, "vector")==0)
		return getAttr(name, sn::VT_VECTOR3, sn::UNIFORM_SHAPE);
	return sn::attr_t( "", sn::VT_UNKNOWN, sn::PER_UNKNOWN, -1);
}

void HairArtistShape2::displayString(const char* text, ...)
{
	va_list argList; va_start(argList, text);
	char sbuf[256];
	_vsnprintf(sbuf, 255, text, argList);
	va_end(argList);
	MGlobal::displayInfo(sbuf);
}

MObject HairArtistShape2::findNaddAttr(MObject obj, const char* name, sn::VALUETYPE type, sn::ATTRIBUTE_PLACE place, const char* defstring)
{
	MStatus stat;
	MFnDependencyNode dn(obj);
	MObject attr = dn.attribute(name, &stat);
	if( stat)
	{
//		if( datablock)
//		{
//			MDataHandle inputData = datablock->inputValue(attr);
//		}
		return attr;
	}

	switch( type)
	{
	case sn::VT_BOOL: case sn::VT_INT: case sn::VT_FLOAT: case sn::VT_VECTOR3:
		{
			// ��������
			MFnNumericData::Type dtype = MFnNumericData::kInvalid;
			if(type==sn::VT_BOOL)
				dtype = MFnNumericData::kBoolean;
			else if(type==sn::VT_INT)
				dtype = MFnNumericData::kInt;
			else if(type==sn::VT_FLOAT)
				dtype = MFnNumericData::kFloat;
			else if(type==sn::VT_VECTOR3)
				dtype = MFnNumericData::k3Double;

			MFnNumericAttribute numAttr;
			attr = numAttr.create(name, name, dtype, 0, &stat);
			if( !stat) return MObject::kNullObj;
			numAttr.setConnectable(true);
			numAttr.setKeyable(true);
			numAttr.setStorable(true);
			numAttr.setWritable(true);
			numAttr.setReadable(true);
			numAttr.setHidden(false);
//			numAttr.setInternal(true);

			if( defstring && defstring[0] && type==sn::VT_BOOL)
			{
				int def=0;
				setlocale(LC_ALL, "C");
				sscanf(defstring, "%d", &def);
				numAttr.setDefault(def?true:false);
			}
			if( defstring && defstring[0] && type==sn::VT_FLOAT)
			{
				float def=0, min=-666, max=-666;
				setlocale(LC_ALL, "C");
				sscanf(defstring, "%f %f %f", &def, &min, &max);
				numAttr.setDefault(def);
				if(min!=-666) numAttr.setSoftMin(min);
				if(max!=-666)numAttr.setSoftMax(max);
			}
			if( defstring && defstring[0] && type==sn::VT_INT)
			{
				int def=0, min=-666, max=-666;
				setlocale(LC_ALL, "C");
				sscanf(defstring, "%d %d %d", &def, &min, &max);
				numAttr.setDefault(def);
				if(min!=-666) numAttr.setSoftMin(min);
				if(max!=-666)numAttr.setSoftMax(max);
			}

			if( !dn.addAttribute(attr))
				return MObject::kNullObj;
			return attr;
		}
	case sn::VT_MATRIX4X4:
		{
			MFnMatrixAttribute matrixAttribute;
			attr = matrixAttribute.create( name, name, MFnMatrixAttribute::kFloat, &stat);
			if( !stat) 
				return MObject::kNullObj;
			matrixAttribute.setDefault(MMatrix::identity);
			matrixAttribute.setConnectable(true);
			matrixAttribute.setInternal(true);
			if( !dn.addAttribute(attr))
				return MObject::kNullObj;

			return attr;
		}
	case sn::VT_TRANSFORM:
			return MObject::kNullObj;
	case sn::VT_STRING:
		{
			MFnTypedAttribute typedAttr;
			MFnStringData stringData;
			MString d = defstring;
			attr = typedAttr.create( name, name, MFnData::kString, stringData.create(d, &stat), &stat );
			if( !stat) return MObject::kNullObj;
			typedAttr.setConnectable(true);
			typedAttr.setStorable(true);
			typedAttr.setWritable(true);
			typedAttr.setReadable(true);
			typedAttr.setHidden(false);
			typedAttr.setInternal(true);
			dn.addAttribute(attr);
			return attr;
		}
	case sn::VT_GEOMETRY:
		{
			return MObject::kNullObj;
		}
		/*/
	case sn::VT_SNSHAPE:
	case sn::VT_SNSHAPEARRAY:
		{
			MFnTypedAttribute typedAttr;
			attr = typedAttr.create( name, name, SnData::id, MObject::kNullObj, &stat);
			if( !stat)
				return MObject::kNullObj;
			if( type==sn::VT_SNSHAPEARRAY) 
				typedAttr.setArray(true);
			typedAttr.setConnectable(true);
			typedAttr.setCached(false);
			typedAttr.setStorable(false);
			typedAttr.setWritable(true);
			typedAttr.setReadable(true);
			typedAttr.setHidden(true);
			if( !dn.addAttribute(attr))
				return MObject::kNullObj;
			return attr;
		}
		/*/
	case sn::VT_COLOR:
		{
			MFnNumericAttribute numAttr;
			attr = numAttr.createColor(name, name, &stat);
			if( !stat) return MObject::kNullObj;
			numAttr.setConnectable(true);
			numAttr.setStorable(true);
			numAttr.setWritable(true);
			numAttr.setReadable(true);
			numAttr.setHidden(false);
			numAttr.setInternal(true);
			dn.addAttribute(attr);
			return attr;
		}
	}
	return MObject::kNullObj;
}

/*/
void HairArtistShape2::addAttributes(MObject obj, std::vector<artist::attr_decl>& attrlist)
{
	for( unsigned i=0; i<attrlist.size(); i++)
	{
		artist::attr_decl& ad = attrlist[i];
		findNaddAttr(obj, ad.name.c_str(), ad.type, ad.place, ad.defminmax.c_str());
	}
}
/*/

MStatus HairArtistShape2::CreateAttrNTextureCache4( 
	MObject obj, 
	std::string longname, std::string shortname, 
	MObject* i_HairAttr, double vmin, double vmax, double def, bool bStorableAttributes)
{
	MFnDependencyNode dn(obj);
	MStatus _Status;

	std::string ln_cache = longname + "Cache";
	std::string sn_cache = shortname+ "ch";

	MPlug textureCacheAffect = dn.findPlug("textureCacheAffect");
	std::vector<std::string> array;
	if( !textureCacheAffect.isNull())
	{
		MObject val;
		textureCacheAffect.getValue(val);
		MFnStringArrayData valdata(val);
		MStringArray marray = valdata.array();
		for(int i=0; i<(int)marray.length(); i++)
		{
			displayString("%d: %s", i, marray[i].asChar());
			array.push_back(marray[i].asChar());
		}
	}

	// Value
	MFnNumericAttribute numAttr;
	i_HairAttr[0] = numAttr.create(longname.c_str(), shortname.c_str(), MFnNumericData::kDouble, def, &_Status);
	if (!_Status) return _Status;
	numAttr.setKeyable(true);
	numAttr.setStorable(true);
	numAttr.setHidden(false);
	numAttr.setSoftMin(vmin);	
	numAttr.setSoftMax(vmax);	
//	_Status = addAttribute (i_HairAttr[0]);	
//	if (!_Status) return _Status;
	
	_Status = dn.addAttribute(i_HairAttr[0]);
//	attributeAffects(i_HairAttr[0], o_buildMessage);
	array.push_back(ln_cache.c_str());
	array.push_back(longname.c_str());

	// Cache
	{
		MFnTypedAttribute	typedAttr;

		i_HairAttr[1] = typedAttr.create( ln_cache.c_str(), sn_cache.c_str(), textureCacheData::id);
		typedAttr.setStorable(false);
		typedAttr.setReadable(true);
		typedAttr.setWritable(true);
		typedAttr.setHidden(true);
		_Status = dn.addAttribute(i_HairAttr[1]);
		if (!_Status) 
		{
			displayString("cand create attr %s", ln_cache.c_str());
			return _Status;
		}
//		attributeAffects(src,				attr);
//		attributeAffects(i_PaintAttrResX,	attr);
//		attributeAffects(i_PaintAttrResY,	attr);
//		attributeAffects(attr, o_buildMessage);
	}
//	CreateTextureCacheAttr( longname+"Cache", shortname+"ch", i_HairAttr[0], i_HairAttr[1], bStorableAttributes);

	// jitter
	std::string ln = longname + "Jitter";
	std::string sn = shortname+ "jt";
	i_HairAttr[2] = numAttr.create(ln.c_str(), sn.c_str(), MFnNumericData::kDouble, 0, &_Status);
	numAttr.setSoftMin(0);	
	numAttr.setSoftMax(1);	
	_Status = dn.addAttribute(i_HairAttr[2]);
//	::addAttribute(i_HairAttr[2], atInput);	
//	attributeAffects(i_HairAttr[2], o_buildMessage);
//	attributeAffects(i_HairAttr[2], i_HairAttr[1]);
	if (!_Status) return _Status;
	array.push_back(ln_cache.c_str());
	array.push_back(ln.c_str());

	// uvsetname
	ln = longname + "UVset";
	sn = shortname+ "uvs";
	MFnTypedAttribute typedAttr;
	MFnStringData stringData;
	i_HairAttr[3] = typedAttr.create( ln.c_str(), sn.c_str(), 
		MFnData::kString, stringData.create(&_Status), &_Status );
	_Status = dn.addAttribute(i_HairAttr[3]);	
//	attributeAffects(i_HairAttr[3], o_buildMessage);
//	attributeAffects(i_HairAttr[3], i_HairAttr[1]);
	if (!_Status) return _Status;
	array.push_back(ln_cache.c_str());
	array.push_back(ln.c_str());

	if( !textureCacheAffect.isNull())
	{
		MStringArray marray(array.size(), "");
		for(int i=0; i<(int)array.size(); i++)
		{
			marray[i] = array[i].c_str();
			displayString("%d: %s", i, marray[i].asChar());
		}

		MFnStringArrayData valdata;
		MObject val = valdata.create(marray);
		textureCacheAffect.setValue(val);

	}
	return MS::kSuccess;
}
