#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: HairDeformerNode.h
//
// Dependency Graph Node: HairDeformer

#include "HairMath.h"
#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include "HairDeformerData.h"

class HairDeformer : public MPxNode
{
public:
	HairDeformer();
	virtual ~HairDeformer(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	MStatus ComputeOutput(const MPlug& plug, MDataBlock& data, MObject i_Deformer[2], MObject i_envelope[3]);
	MStatus ComputeTextureCache3(const MPlug& plug, MObject attr[4], MObject i_PaintResXY[2], MDataBlock& data);
	MStatus ComputeTextureCache4(const MPlug& plug, MObject attr[4], MObject i_PaintResXY[2], MDataBlock& data);

	static void* creator();
	static MStatus initialize();
	static MStatus initialize(
		const char* dll, 
		const char* proc,
		MObject i_Deformer[2], 
		MObject i_envelope[3], 
		MObject i_PaintResXY[2],
		MObject& o_output);

	virtual MStatus setDependentsDirty( 
		const MPlug& src, 
		MPlugArray& dst);

	static MStatus CreateAttrNTextureCache( 
		std::string longname, std::string shortname, 
		MObject* i_HairAttr, double vmin, double vmax, double def, bool bStorableAttributes, MObject& o_output, MObject i_PaintResXY[2]);
	static void CreateTextureCacheAttr(
		std::string longName, std::string shortName, MObject& src, MObject& attr, bool bStorableAttributes, MObject& o_output, MObject i_PaintResXY[2]);

public:
	static MObject i_Deformer[2];	// Example input attribute
	static MObject i_textureCacheAffect;
	static MObject o_output;		// Example output attribute

	static MObject i_PaintResXY[2];
	
	static MObject i_envelope[3];	// ����������� ���������

public:
	static MTypeId id;
	static const MString typeName;
};

