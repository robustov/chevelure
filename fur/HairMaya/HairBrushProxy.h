#pragma once

#include "pre.h"

#include <maya/MPxLocatorNode.h>

class HairBrushProxy: public MPxLocatorNode {
public:
	static		 MTypeId	id;
	static const MString	typeName;
	
public:
							HairBrushProxy();
	virtual					~HairBrushProxy();

	virtual void			draw(	M3dView& view,
									const MDagPath& path, 
									M3dView::DisplayStyle style,
									M3dView::DisplayStatus status );

	virtual bool			isBounded() const;
	virtual bool			drawLast() const;

	static void*			creator();
	static MStatus			initialize();
};
