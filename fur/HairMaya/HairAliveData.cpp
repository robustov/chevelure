//
// Copyright (C) 
// File: HairAliveDataCmd.cpp
// MEL Command: HairAliveData

#include "HairAliveData.h"

#include <maya/MGlobal.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include <sstream>







HairAliveData::HairAliveData()
{
}
HairAliveData::~HairAliveData()
{
}

MTypeId HairAliveData::typeId() const
{
	return HairAliveData::id;
}

MString HairAliveData::name() const
{ 
	return HairAliveData::typeName; 
}

void* HairAliveData::creator()
{
	return new HairAliveData();
}

void HairAliveData::copy( const MPxData& other )
{
	const HairAliveData* arg = (const HairAliveData*)&other;
	this->data = arg->data;
}

MStatus HairAliveData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("HairAliveData: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus HairAliveData::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus HairAliveData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	serialize(stream);

	return MS::kSuccess;
}

MStatus HairAliveData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

void HairAliveData::serialize(Util::Stream& stream)
{
	data.serialize(stream);
}

