//
// Copyright (C) 
// File: HairCvDataCmd.cpp
// MEL Command: HairCVData

#include "HairCVData.h"

#include <maya/MGlobal.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include <sstream>


HairCVData::HairCVData()
{
}
HairCVData::~HairCVData()
{
}

MTypeId HairCVData::typeId() const
{
	return HairCVData::id;
}

MString HairCVData::name() const
{ 
	return HairCVData::typeName; 
}

void* HairCVData::creator()
{
	return new HairCVData();
}

void HairCVData::copy( const MPxData& other )
{
	const HairCVData* arg = (const HairCVData*)&other;
	this->cv = arg->cv;
}

MStatus HairCVData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("HairClumpPositionsData: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	if( !this->serialize(stream)) 
		return MS::kFailure;

	return MS::kSuccess;
}

MStatus HairCVData::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus HairCVData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	if( !serialize(stream))
		return MS::kFailure;

	return MS::kSuccess;
}

MStatus HairCVData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

bool HairCVData::serialize(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version>=0)
	{
		cv.serialize( stream);
	}
	return true;
}

