#pragma once

#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include "Util/Stream.h"
#include "HairMeshModifier.h"

/*/
	USING:
	{
		MFnTypedAttribute fnAttr;
		MObject newAttr = fnAttr.create( 
			"fullName", "briefName", HairModifierData::id );
	}
/*/


class HairModifierData : public MPxData
{
public:
	HairModifierData();
	virtual ~HairModifierData(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

public:
	static MTypeId id;
	static const MString typeName;

	bool Init();
	HairMeshModifier data;
protected:
	void serialize(Util::Stream& stream);

public:
	enum enType
	{
		T_LENGTH			= 0,
		T_INCLINATION		= 1,
		T_POLAR				= 2,
		T_BASECURL			= 4,
		T_TIPCURL			= 5,
		T_BASEWIDTH			= 6,
		T_TIPWIDTH			= 7,
		T_SCRAGGLE			= 8,
		T_SCRAGGLEFREQ		= 9,
		T_BASECLUMP			= 10,
		T_TIPCLUMP			= 11,
		T_TWIST				= 12,
		T_DISPLACE			= 13,
		T_MAX				= 14,
	};
	void setArray( enType name, MPxData* data);//textureCacheData

};
