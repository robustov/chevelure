#pragma once

#include <maya/MPxGeometryIterator.h>
#include <maya/MPoint.h>
#include "pre.h"
#include "DrawCache.h"

class CVIterator : public MPxGeometryIterator
{
public:
	CVIterator( void * userGeometry, MObjectArray & components );
	CVIterator( void * userGeometry, MObject & components );

    //////////////////////////////////////////////////////////
	//
	// Overrides
	//
    //////////////////////////////////////////////////////////

	virtual void		reset();
	virtual MPoint		point() const;
	virtual void		setPoint( const MPoint & ) const;
	virtual int			iteratorCount() const;
	virtual bool        hasPoints() const;

public:
	HairProcessor* geometry;
};
