//
// Copyright (C) 
// File: HairControlsAnimDataCmd.cpp
// MEL Command: HairControlsAnimData

#include "HairControlsAnimData.h"
#include "HairCVData.h"

#include <maya/MGlobal.h>
#include <maya/MFileIO.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include <sstream>
#include "Util/Stream.h"
#include "Util/FileStream.h"

int HairControlsAnimData::current_file_version=0;

// ��������
bool HairControlsAnimData::setKey(MTime t, MObject val)
{
	double time = t.as( MTime::k6000FPS);

	MFnPluginData pd(val);
	if( pd.typeId()!= HairCVData::id)
	{
		return MS::kFailure;
	}
	HairCVData* cvdata = (HairCVData*)pd.data();

	HairCV& dst = this->keys[time];
	dst.copy(cvdata->cv);

	this->bModify = true;

	return MS::kSuccess;
}
bool HairControlsAnimData::deleteKey(MTime t)
{
	this->bModify = true;

	double time = t.as( MTime::k6000FPS);

	this->keys.erase(time);
	return MS::kSuccess;
}
bool HairControlsAnimData::saveAnim(const char* filename)
{
	Util::FileStream file;
	if( !file.open(filename, true))
	{
		displayStringError("cant open file %s for write", filename);
		return MS::kFailure;
	}
	int version = current_file_version;
	file >> version;

	file >> this->keys;

	return MS::kSuccess;
}
bool HairControlsAnimData::loadAnim(const char* filename)
{
	this->bModify = false;

	Util::FileStream file;
	if( !file.open(filename, false))
	{
		displayStringError("cant open file %s", filename);
		return MS::kFailure;
	}
	int version = 0;
	file >> version;
	if( version!=current_file_version)
	{
		displayStringError("invalid file version");
		return MS::kFailure;
	}

	file >> this->keys;

	return MS::kSuccess;
}
bool HairControlsAnimData::getCv(MTime t, HairCV& cv)
{
	double time = t.as( MTime::k6000FPS);

	double f1=0, f2=0;

	std::map<double, HairCV>::iterator it1, it2;
	it2 = keys.lower_bound(time);
	if( it2!=keys.end())
	{
		it1 = it2;
		it1--;
		if( it2==keys.begin())
			it1 = it2;
	}
	else
	{
		it2--;
		it1 = keys.end();
	}
	if( it2==keys.end())
		return false;

	if( it1 != keys.end() && it1!=it2)
	{
		f2 = (time - it1->first)/(it2->first-it1->first);
		f1 = 1-f2;
	}
	else
	{
		f2 = 0;
		f1 = 1;
		it1 = it2;
	}
	HairCV& r1 = it1->second;
	HairCV& r2 = it2->second;
	double time1 = it1->first;
	double time2 = it2->first;

//	cv = r1;
	cv.linear_interpolation(r1, r2, (float)f1, (float)f2);
	return true;
}

HairControlsAnimData::HairControlsAnimData()
{
	this->bModify = false;
}
HairControlsAnimData::~HairControlsAnimData()
{
}

MTypeId HairControlsAnimData::typeId() const
{
	return HairControlsAnimData::id;
}

MString HairControlsAnimData::name() const
{ 
	return HairControlsAnimData::typeName; 
}

void* HairControlsAnimData::creator()
{
	return new HairControlsAnimData();
}

void HairControlsAnimData::copy( const MPxData& other )
{
	const HairControlsAnimData* arg = (const HairControlsAnimData*)&other;
	DATACOPY(keys);
	this->bModify = true;
}

MStatus HairControlsAnimData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("HairControlsAnimData: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus HairControlsAnimData::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus HairControlsAnimData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	serialize(stream);

	return MS::kSuccess;
}

MStatus HairControlsAnimData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

void HairControlsAnimData::serialize(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version>=0)
	{
		if(stream.isLoading())
		{
			// ��������� �� ����� - ��� �������� � �����
			stream >> this->loadedFilename;

			this->relative_filename = loadedFilename;
			this->loadedFilename = scenefoldername() + this->loadedFilename;

			Util::FileStream file;
			if( !file.open(this->loadedFilename.c_str(), false))
			{
				displayStringError("cant open file %s", this->loadedFilename.c_str());
				return;
			}
			this->serializeData(file);
		}
		// �������� ������ ���� ���� ����������� ��� ���������� ��� �����
		else 
		{
			std::string filename = scenefoldername()+this->relative_filename;

			if( this->loadedFilename != filename)
				this->bModify = true;
			else
				if( !Util::is_file_exist(this->loadedFilename.c_str()))
					this->bModify = true;

			if(this->bModify)
			{
				Util::FileStream file;
				Util::create_directory_for_file(filename.c_str());
				if( !file.open(filename.c_str(), true))
				{
					displayStringError("cant save to file %s", filename.c_str());
					return;
				}
				this->serializeData(file);
			}
			stream >> this->relative_filename;
			this->loadedFilename = filename;
		}
	}

	// �������� ����
	this->bModify = false;

}



void HairControlsAnimData::serializeData(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version>=0)
	{
		stream >> keys;
	}
}
