#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: HairDeformer_ClumpRampNode.h
//
// Dependency Graph Node: HairDeformer_ClumpRamp

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include "HairDeformer.h"


class HairDeformer_LenghtRamp : public HairDeformer
{
public:
	HairDeformer_LenghtRamp();
	virtual ~HairDeformer_LenghtRamp(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_Deformer[2];	// Example input attribute
	static MObject i_envelope[4];	// ����������� ���������
	static MObject o_output;		// Example output attribute
	static MObject i_PaintResXY[2];

	// Ramp
	static MObject i_lenghtClump;
	static MObject i_useU;


public:
	static MTypeId id;
	static const MString typeName;
};

