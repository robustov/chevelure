#pragma once

#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
//#include <maya/MProgressWindow.h>

#include "HairRootPositions.h"
#include "HairGeometryIndices.h"
#include "HairClumpPositions.h"

/*/
	USING:
	{
		MFnTypedAttribute fnAttr;
		MObject newAttr = fnAttr.create( 
			"fullName", "briefName", HairRootPositionsData::id );
	}
/*/


class HairRootPositionsData : public MPxData
{
public:
	enum enClumpAlgorithm
	{
		AUTO = 0,
		QUICK = 1,
		STUPID = 2,
	};
public:
	HairRootPositionsData();
	virtual ~HairRootPositionsData(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

public:
//	std::string filename;
	HairRootPositions hp;

public:
	// ������������� (�������������� ���)
	static bool Init(
		HairRootPositions& hp, 
		MObject thisObject, 
		HairClumpPositions* hc, 
		HairGeometryIndices* hgh, 
		HairGeometry* geometry, 
		int count, 
		bool bEqualize,
		int maxCount, 
		int seed, 
		int resX, int resY,
		float CLUMPblending, 
		bool bProgress, 
		float maxClumpRadius,				// ����. ������ ������ - �� ��� ������ - �� � ������
		enClumpAlgorithm algorithm,			// �������� ��������
		const std::set<int>& workingfaces
		);
	// ������������� (���������� ���)
	static bool Init(
		HairRootPositions& hp, 
		MObject thisObject, 
		HairClumpPositions* hc, 
		HairGeometryIndices* hgh, 
		HairGeometry* geometry, 
		Math::Vec2i countUV, 
		Math::Vec2f offset,
		Math::Vec2f shear,
		Math::Vec2f jitter,
		int seed, 
		int count,						// ������ ��� ������������, ����� -1
		int resX, int resY,
		float CLUMPblending, 
		bool bProgress, 
		float maxClumpRadius,				// ����. ������ ������ - �� ��� ������ - �� � ������
		enClumpAlgorithm algorithm,			// �������� ��������
		const std::set<int>& workingfaces);

	// ������������� (�� �������)
	static bool InitFromClumps(
		HairRootPositions& hp, 
		HairClumpPositions* hc, 
		HairGeometryIndices* hgh, 
		HairGeometry* geometry, 
		int maxCount, 
		const std::set<int>& workingfaces
		);

public:
	static MTypeId id;
	static const MString typeName;
protected:
	// ������ ����� �����
	static int generate(
		HairGeometryIndices* hgh, 
		HairGeometry* geometry, 
		std::vector< HairGeometryIndices::pointonsurface>& data,
		std::vector< int>& hairseeds, 
		std::vector< HairGeometryIndices::facegroup>& groups, 
		std::vector< Math::Vec2f>& uvs,
		MPlug& densityplug, MPlug& densitypluguvset,
		bool bEqualize,
		double area, 
		int count, int globalSeed,
		int resX, int resY,
		const char* progressString, 
		const std::set<int>& workingfaces);

	// 
	static double calcArea(
		HairGeometryIndices* hgh, 
		HairGeometry* geometry, 
		const std::set<int>& workingfaces, 
		double& fullarea		// out
		);

	// �������� ����� � �������
	static void BindHairsToClumps(
		HairRootPositions& hp, 
		HairClumpPositions* hc, 
		HairGeometryIndices* hgh, 
		HairGeometry* geometry, 
		float CLUMPblending,				// ����������
		std::vector<Math::Vec2f>& posuv,	
		double area,						// ���. ������� �����������
		bool bProgress,						
		float maxClumpRadius,				// ����. ������ ������ - �� ��� ������ - �� � ������
		enClumpAlgorithm algorithm			// �������� ��������
		);

	bool serialize(Util::Stream& stream);
};
