//
// Copyright (C) 
// File: UlHairConnectCVCurvesCmd.cpp
// MEL Command: UlHairConnectCVCurves
/*/
#include "UlHairConnectCVCurves.h"

#include <maya/MGlobal.h>
#include "HairShape.h"

UlHairConnectCVCurves::UlHairConnectCVCurves()
{
}
UlHairConnectCVCurves::~UlHairConnectCVCurves()
{
}

MSyntax UlHairConnectCVCurves::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.addArg(MSyntax::kString);	// arg0
//	syntax.addArg(MSyntax::kLong);		// countU
//	syntax.addArg(MSyntax::kLong);		// countV
	syntax.addArg(MSyntax::kLong);		// segmCount
//	syntax.addFlag("f", "format", MSyntax::kString);
	return syntax;
}

MStatus UlHairConnectCVCurves::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MString argsl0;
	argData.getCommandArgument(0, argsl0);
	MObject obj;
	if( !nodeFromName(argsl0, obj))
		return MS::kFailure;

	int segmCount;
//	argData.getCommandArgument(1, countU);
//	argData.getCommandArgument(2, countV);
	argData.getCommandArgument(1, segmCount);

	MStringArray res = HairShape::ConnectCVCurves(obj, segmCount);

	setResult(res);

	return MS::kSuccess;
}

void* UlHairConnectCVCurves::creator()
{
	return new UlHairConnectCVCurves();
}

/*/
