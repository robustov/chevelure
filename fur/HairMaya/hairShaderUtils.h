#ifndef hairShaderUtils_h
#define hairShaderUtils_h

#include "utilsOcs.h"

// ��������� ����� �� �� ������
float hairShaderUtils_hairRandom(float hairId)
{
	float n = cellnoise(hairId);
	return n;
}

// ��!
color hairShaderUtils_diffuselgt( 
	color Cin; 
	vector Lin; 
	normal Nin; 
	float diffPower
	)
{
	color Cout = Cin;
	vector LN, NN;
	float Atten;
	LN = normalize(vector(Lin));
	NN = normalize(vector(Nin));
	Atten = max(0.0,(pow(LN.NN, diffPower)));
	Cout *= Atten;
	return (Cout);
}

// ��!
color hairShaderUtils_diffuseEdgelgt (color Cin; vector Lin; normal Nin; float Edginess; float edgePower)
{
	color Cout = Cin;
	vector LN, NN;
	extern vector I;
	float edgeFade;
	float edge;
	float edgeDiff;
	float cosin;

	LN = normalize(vector(Lin));
	NN = normalize(vector(Nin));
	vector In = normalize(I);
	cosin = LN.NN;
	edgeDiff = sqrt(1-(cosin*cosin));

	cosin = In.NN;
	if (Edginess != 1)
	{
		edge = pow (sqrt(1-(cosin*cosin)), Edginess);
	} 
	else 
	{
		edge = sqrt(1-(cosin*cosin));
	}

	if (edgePower == 0)
	{
		edgeFade = 1;
	}
	else
	{
		edgeFade = pow ((LN.In+1)/2, edgePower);
	}
	Cout *= edge * edgeFade * edgeDiff;

	return (Cout);
}


normal hairShaderUtils_hairNormal(
	vector T;		// Tangent
	normal SN;		// surface normal
	)
{
	normal nSN = normalize( SN);
	vector Tangent = normalize( T);
	float factor = clamp( nSN.Tangent, 0, 1);

	vector S = nSN^Tangent;

	vector N_hair = Tangent^S;

	normal norm_hair = (factor * nSN) + (1-factor) * N_hair;
	norm_hair = normalize(norm_hair);
	return norm_hair;
}

float hairShaderUtils_Kajiya( 
	vector L;		// light
	vector I;		// eye
	vector T;		// tangent
	float specwidth;
	output float diffuse;
	output float specular;
	output color Ci;// for debug
	)
{
	// �������������� �������
	vector nE = -normalize(I);
	vector nL = normalize(L);
	vector nT = normalize(T);

	vector crossLT = nT^nL;

	// Diffuse
	// This is correct
	// nE_ nL_ - in hair normal plane 
//	vector nE_ = normalize( nE - (nE.nT)*nT);
//	vector nL_ = normalize( nL - (nL.nT)*nT);
//	diffuse = length(crossLT) * (1 + nE_.nL_) * 0.5;		

	// Diffuse
	// This is uncorrect
	// But not dirty
	diffuse = length(crossLT) * (1 + nE.nL - (nE.nT)*(nT.nL) ) * 0.5; 
	diffuse = clamp(diffuse, 0, 1);


	// Specular
	// cross product half vector and T = cos(phi/2)
	float cos_phi = length( normalize(nE+nL)^nT);
	specular = pow( abs(cos_phi), 1/specwidth );

	return specular;
}

// attenuation in clump
float hairShaderUtils_AttenuationInClump(
	float KclumpAttenuation; 
	vector nTangent;
	vector nL;
	vector clumpVectorV;
	float clumpRadius;
	)
{
	vector nL_ = -normalize( nL - (nL.nTangent)*nTangent);
	vector nClV_ = normalize( clumpVectorV - (clumpVectorV.nTangent)*nTangent);

	float r = length( clumpVectorV);
	float R = clumpRadius;
	if(r>R) r = R;

	float cosPhi = nL_.nClV_;
	
	float B = sqrt(R*R - r*r*(1-cosPhi*cosPhi));
	float b = -r*cosPhi;

	float path_ = B+b;
	path_ -= R*0.1;
	float result = 1;
	if(path_>0)
	{
		result = exp(-path_*KclumpAttenuation);
	}
	return result;
}


void hairShaderUtils_shadeHair(
	// extern
	point P_nonoise;
	normal SN;
	float clump_radius;
	vector clump_normal;
	vector clump_vector;
	float hairId;
	float clumpId;
	// color
	color hairColor;
	color hairOpacity;
	color SpecColor;
	color StaticAmbient;

	// inClumpAttenuation
	float inClumpAttenuation;
	float _depthAttenuation;		// ��� ��������
	// FromSurface 
	float SurfaceKd;
	// Kajiya
	float KajiyaKd;
	float KajiyaKs; 
	float KajiyaSpecWitdh;
	// Output
	output color Oi;
	output color Ci;
	)
{
	float depthAttenuation = clamp(_depthAttenuation, 0, 1);

	if( clumpId>=0)
	{
		vector cv = vtransform("object", clump_vector);
		float c = length( cv)/clump_radius;
		Oi = color( 1);
		Ci = color( c);
	}
//	else
	{
	vector TangentNoNoise = Dv( P_nonoise);
	vector Tangent = dPdv;
	normal norm_hair = hairShaderUtils_hairNormal(Tangent, SN);

	color kajiyaColor = 0;
	color surfaceDiffuseColor = 0;

	illuminance( P, norm_hair, PI)
	{
		color lightColor = Cl;

		// attenuation in clump
		if( clumpId>=0)
		{
			float AttenuationInClump = hairShaderUtils_AttenuationInClump(
				inClumpAttenuation, 
				normalize( Tangent), 
				normalize( L), 
				clump_vector,
				clump_radius
				);
			lightColor = lightColor*AttenuationInClump;
		}
		lightColor = lightColor*depthAttenuation;

		// surface diffuse
		float diffuse, specular;
		diffuse = normalize(L).normalize(norm_hair);
		if(diffuse>0)
			surfaceDiffuseColor += hairColor*SurfaceKd*diffuse*lightColor;


		// Kayaa
		float kajiya = hairShaderUtils_Kajiya(L, I, Tangent, KajiyaSpecWitdh, diffuse, specular, Ci);
		kajiyaColor += 
			hairColor*KajiyaKd*diffuse*lightColor + 
			SpecColor*KajiyaKs*specular*lightColor + 
			hairColor*StaticAmbient*lightColor;

	}

	Oi = color( 1);
	Ci = kajiyaColor + surfaceDiffuseColor;
//	Ci = color( utilsOcs_normalAsColor(norm_hair));
//	Ci = color( utilsOcs_vectorAsColor(TangentNoNoise));

	Ci = Oi*Ci;
	}
}

#endif
