#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: UlHairConnectCVCurvesCmd.h
// MEL Command: UlHairConnectCVCurves

/*/
#include <maya/MPxCommand.h>

class UlHairConnectCVCurves : public MPxCommand
{
public:
	static const MString typeName;
	UlHairConnectCVCurves();
	virtual	~UlHairConnectCVCurves();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

/*/
