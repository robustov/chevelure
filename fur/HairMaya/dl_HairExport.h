#pragma once

#include "pre.h"

bool HairExport(
	const char* argsl0,
	const char* filename,
	const char* pass,
	const float* camera, 
	int useMotionBlur,
	double frametime, 
	double shutterOpenSec, 
	double shutterCloseSec,
	double shutterOpen, 
	double shutterClose, 
	float outbox[6]
	);

class dl_HairExport : public MPxCommand 
{
public:
	static const MString typeName;
	dl_HairExport() {};
	static MSyntax	newSyntax();
	virtual MStatus	doIt ( const MArgList& args);
	static void*	creator();
};
