#include "MeshSurfaceSynk.h"
//#include "Math/Math.h"
#include "Math\MeshUtil.h"
#include "Util/EvalTime.h"
#include <maya/MFnDynSweptGeometryData.h>
#include <maya/MDynSweptTriangle.h>
#include "MathNMaya/MathNMaya.h"

MeshSurfaceSynk::MeshSurfaceSynk()
{
	vectorarrays.resize(T_VECTORMAX);
}

Math::Vec3f MeshSurfaceSynk::getBaseColor(double u, double v)
{
	return getVectorValue(T_BASECOLOR, u, v);
}
Math::Vec3f MeshSurfaceSynk::getTipColor(double u, double v)
{
	return getVectorValue(T_TIPCOLOR, u, v);
}
void MeshSurfaceSynk::setArray2d( enType name, MPxData* _data)
{
	textureCacheData* data = static_cast<textureCacheData*>(_data);
	Math::ParamTex2d<unsigned char>* param = NULL;

	switch(name)
	{
		case T_LENGTH		: param = (Math::ParamTex2d<unsigned char>*)&length; break;
		case T_INCLINATION	: param = (Math::ParamTex2d<unsigned char>*)&inclination; break;
		case T_POLAR		: param = (Math::ParamTex2d<unsigned char>*)&polar; break;
		case T_BASECURL		: param = (Math::ParamTex2d<unsigned char>*)&baseCurl; break;
		case T_TIPCURL		: param = (Math::ParamTex2d<unsigned char>*)&tipCurl; break;
		case T_BASEWIDTH	: param = (Math::ParamTex2d<unsigned char>*)&baseWidth; break;
		case T_TIPWIDTH		: param = (Math::ParamTex2d<unsigned char>*)&tipWidth; break;
		case T_SCRAGGLE		: param = (Math::ParamTex2d<unsigned char>*)&scragle; break;
		case T_SCRAGGLEFREQ	: param = (Math::ParamTex2d<unsigned char>*)&scragleFreq; break;
		case T_BASECLUMP	: param = (Math::ParamTex2d<unsigned char>*)&baseClump; break;
		case T_TIPCLUMP		: param = (Math::ParamTex2d<unsigned char>*)&tipClump; break;
		case T_TWIST		: param = (Math::ParamTex2d<unsigned char>*)&twist; break;
		case T_DISPLACE		: param = (Math::ParamTex2d<unsigned char>*)&displace; break;
		case T_TWISTFROMPOLAR: param = (Math::ParamTex2d<unsigned char>*)&twistFromPolar; break;
		case T_BLENDCVFACTOR: param = (Math::ParamTex2d<unsigned char>*)&blendCVfactor; break;
		case T_BLENDDEFORMFACTOR: param = (Math::ParamTex2d<unsigned char>*)&blendDeformfactor; break;
	}
	if(!param) return;
	*param = data->paramTex2d;
}
/*/
void MeshSurfaceSynk::setArray( enType name, MPxData* _data)
{
	textureCacheData* data = static_cast<textureCacheData*>(_data);
	HairParam<unsigned char>* param = NULL;

//	switch(name)
//	{
//		case T_LENGTH		: param = (HairParam<unsigned char>*)&length; break;
//		case T_INCLINATION	: param = (HairParam<unsigned char>*)&inclination; break;
//		case T_POLAR		: param = (HairParam<unsigned char>*)&polar; break;
//		case T_BASECURL		: param = (HairParam<unsigned char>*)&baseCurl; break;
//		case T_TIPCURL		: param = (HairParam<unsigned char>*)&tipCurl; break;
//		case T_BASEWIDTH	: param = (HairParam<unsigned char>*)&baseWidth; break;
//		case T_TIPWIDTH		: param = (HairParam<unsigned char>*)&tipWidth; break;
//		case T_SCRAGGLE		: param = (HairParam<unsigned char>*)&scragle; break;
//		case T_SCRAGGLEFREQ	: param = (HairParam<unsigned char>*)&scragleFreq; break;
//		case T_BASECLUMP	: param = (HairParam<unsigned char>*)&baseClump; break;
//		case T_TIPCLUMP		: param = (HairParam<unsigned char>*)&tipClump; break;
//		case T_TWIST		: param = (HairParam<unsigned char>*)&twist; break;
//		case T_DISPLACE		: param = (HairParam<unsigned char>*)&displace; break;
//	}

	if(!param) return;
	*param = data->paramTex2d;
}
/*/
void MeshSurfaceSynk::setVectorArray( enVectorType name, MPxData* _data)
{
	textureCacheData* data = (textureCacheData*)_data;

	if( name>=T_MAX)
		return;

	VectorArrayDesc& desc = vectorarrays[name];
	desc.resX = data->textureub.sizeX();
	desc.resY = data->textureub.sizeY();
	desc.cache = data;
}

Math::Vec3f MeshSurfaceSynk::getVectorValue(enVectorType name, double u, double v)
{
	VectorArrayDesc& desc = vectorarrays[name];
	if( desc.cache)
	{
		Math::Vec3f d = desc.cache->getVector(u, v);
		return d;
	}

	int iu0, iu1, iv0, iv1;
	float w00, w01, w10, w11;
	Math::interpolation((float)u, (float)v, desc.resX, desc.resY, 
		iu0, iu1, iv0, iv1, 
		w00, w01, w10, w11);

	int i00 = desc.resY*iu0 + iv0;
	int i01 = desc.resY*iu0 + iv1;
	int i10 = desc.resY*iu1 + iv0;
	int i11 = desc.resY*iu1 + iv1;

	Math::Vec3f res = desc.array[i00]*w00 + desc.array[i01]*w01 + desc.array[i10]*w10 + desc.array[i11]*w11;
	return res;
}


