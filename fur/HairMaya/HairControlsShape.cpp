///////////////////////////////////////////////////////////////////////////////
//
// HairControlsShape.cpp
//
////////////////////////////////////////////////////////////////////////////////
#include "pre.h"

#include <math.h>           
#include <maya/MArrayDataBuilder.h>
#include <maya/MIOStream.h>
#include <maya/MEulerRotation.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MVectorArray.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnVectorArrayData.h>

#include "HairControlsShape.h"           
#include "HairControlsUI.h"     
#include "HairControlsIterator.h"

#include "HairGeometryIndicesData.h"
#include "HairGeometryData.h"
#include "HairCVPositionsData.h"
#include "HairCVData.h"
#include "HairCvBasesData.h"
#include "HairShape.h"
#include "IHairCVmanipulator.h"



#include "MathNMaya/MathNMaya.h"
#include "Util/FileStream.h"
#include "MathNMaya/IFFMatrix.h"

//MObject HairControlsShape::i_bUseBake;				//!< ������������ ����
//MObject HairControlsShape::i_time;					//!< ����� (��� �����)
MObject HairControlsShape::i_message;
MObject HairControlsShape::i_importfrom;
//MObject HairControlsShape::i_keepLength;			//!< ���� ���������� �����
MObject HairControlsShape::i_CVcount;
MObject HairControlsShape::i_HairGeometryIndices;	//!< ���������� ���������� ���������
MObject HairControlsShape::i_HairGeometry;			//!< ��������� ���������
MObject HairControlsShape::o_HairCvBases;
MObject HairControlsShape::o_HairCVPositions;		//!< ��������� ����������� �����
MObject HairControlsShape::o_Guides;
MObject HairControlsShape::o_HairCV;				//!< ����������� ������

MObject HairControlsShape::o_CVTransform;		//!< ��������� ��� ����������� ������
	MObject HairControlsShape::o_CVtranslate;
	MObject HairControlsShape::o_CVrotate;

MObject HairControlsShape::i_scale[3];
MObject HairControlsShape::i_PaintResXY[2];

MObject HairControlsShape::o_HairCVPositionsOut;	//!< ��������� ����������� ����� (�����������)
MObject HairControlsShape::o_HairCVout;				//!< ����������� ������ (�����������)

HairControlsShape::HairControlsShape() 
{
}

HairControlsShape::~HairControlsShape() 
{
}

///////////////////////////////////////////////
// 

MStatus HairControlsShape::computeHairCVPositions( 
	const MPlug& plug,
	MDataBlock& data )
{
	MStatus stat;

	HairGeometryIndices* gi = this->LoadHairGeometryIndices(data);
	if( !gi) 
		return MS::kSuccess;
	int CVcount = data.inputValue(i_CVcount, &stat).asInt();

	bool bReinitCV = false;
	HairCVPositionsData* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( o_HairCVPositions, &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<HairCVPositionsData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( HairCVPositionsData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<HairCVPositionsData*>(fnDataCreator.data( &stat ));
		bNewData = true;
		bReinitCV = true;
	}
	else
	{
		if( pData->cvpos.controlVertexCount() != CVcount)
			bReinitCV = true;
		HairCV* cv = LoadHairCV(data);
		if( !cv || 
			cv->controlVertexCount()!=CVcount ||
			cv->getCount() != gi->src_vertexCount()
			)
		{
			bReinitCV = true;
		}
	}
	pData->Init(CVcount, gi);

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();

	if( bReinitCV)
	{
		MFnPluginData fnHairCVDataCreator;
		MObject newHairCVDataObject = fnHairCVDataCreator.create( MTypeId( HairCVData::id), &stat );
		HairCVData* pHairCVData = static_cast<HairCVData*>(fnHairCVDataCreator.data( &stat ));

		pHairCVData->cv.init(CVcount, &pData->cvpos);

		MDataHandle outputCVData = data.outputValue( o_HairCV, &stat );
		outputCVData.set(newHairCVDataObject);
		outputCVData.setClean();
	}

	return MS::kSuccess;
}
MStatus HairControlsShape::ComputeCvBases(const MPlug& plug, MDataBlock& data)
{
	MStatus stat;

	HairGeometryIndices* gi = this->LoadHairGeometryIndices(data);
	if( !gi) 
		return MS::kSuccess;
	HairGeometry* g = this->LoadHairGeometry(data);
	if( !gi) 
		return MS::kSuccess;
	HairCVPositions* cvp = LoadHairCVPositions(data);
	if( !cvp) 
		return MS::kSuccess;

	HairCvBasesData* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( plug , &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<HairCvBasesData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( HairCvBasesData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<HairCvBasesData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	pData->bases.resize(cvp->controlhairCount());
	pData->bases_inv.resize(cvp->controlhairCount());
	for(int h=0; h<cvp->controlhairCount(); h++)
	{
		HairCV::getControlHairBasisSrc(h, cvp, gi, g, pData->bases[h]);
		pData->bases_inv[h] = pData->bases[h].inverted();
//displayStringD("%d(%d):", h, cvp->controlhairCount());
//displayMatrix(pData->bases[h]);
	}

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}

MStatus HairControlsShape::computeHairCVPositionsOut( 
	const MPlug& plug,
	MDataBlock& data )
{
	MStatus stat;

	HairGeometryIndices* gi = this->LoadHairGeometryIndices(data);
	if( !gi) 
		return MS::kSuccess;

	HairCVPositions* cvp = this->LoadHairCVPositions(data);
	if( !cvp) 
		return MS::kSuccess;

	HairCVPositionsData* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( o_HairCVPositionsOut, &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<HairCVPositionsData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( HairCVPositionsData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<HairCVPositionsData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	pData->Subdiv(*gi, *cvp);



	if( bNewData)
		outputData.set(pData);
	outputData.setClean();

	return MS::kSuccess;
}

MStatus HairControlsShape::computeHairCVout( const MPlug& plug, MDataBlock& data)
{
	MStatus stat;

	int CVcount = data.inputValue(i_CVcount, &stat).asInt();

	HairGeometryIndices* gi = this->LoadHairGeometryIndices(data);
	if( !gi) 
		return MS::kSuccess;
	HairGeometry* g = this->LoadHairGeometry(data);
	if( !gi) 
		return MS::kSuccess;
	HairCVPositions* cvpout = LoadHairCVPositionsOut(data);
	if( !cvpout) 
		return MS::kSuccess;
	HairCVPositions* cvp = LoadHairCVPositions(data);
	if( !cvp) 
		return MS::kSuccess;
	HairCV* cv = LoadHairCV(data);
	if( !cv) 
		return MS::kSuccess;

	HairCVData* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( o_HairCVout, &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<HairCVData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( HairCVData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<HairCVData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}


	// subdive
	if( gi->isSubdived())
	{
		int chc = cvpout->controlhairCount();
		int CVcount = cvpout->controlVertexCount();
		pData->cv.cvcount = CVcount;
		pData->cv.cvs.resize(chc);

		for(int i=0; i<(int)chc; i++)
		{
			HairCV::CV& outch = pData->cv.getControlHair(i);
			outch.clear();
			outch.ts.resize(CVcount, Math::Vec3f(0));
			float desirelen = 0;

			// ������� ������� ��� �������
			int vert = cvpout->getVertexByGuide(i);
			const Math::subdivWeight& w = gi->getSubdiveShemeForVertex(vert);
			for(unsigned x=0; x<w.data.size(); x++)
			{
				float we = w.data[x].second;
				int vi = w.data[x].first;
				vi = cvp->getGuideByVertex(vi);
				HairCV::CV& inch = cv->getControlHair(vi);

				desirelen += we * inch.getLength();

				// ��� ������������ � object space
				Math::Matrix4f basisScr;
				HairCV::getControlHairBasisSrc(vi, cvp, gi, g, basisScr);

				for( int v=0; v<CVcount; v++)
					outch.ts[v] += we * (basisScr * inch.ts[v]);
			}
			// ������� � ts
			Math::Matrix4f basis;
			HairCV::getControlHairBasis(i, cvpout, gi, g, basis);
			Math::Vec3f offset = outch.ts[0] - basis[3].as<3>();
			basis.invert();
			for( int v=0; v<CVcount; v++)
				outch.ts[v] = basis * (outch.ts[v]-offset);

			// ��������� ���������� �����
			float len = outch.getLength();
			outch.scale( desirelen/len);
			outch.bValid = true;
		}
	}
	else
	{
		pData->cv = *cv;
	}


	// scale
	{
		MPxData* _data = data.inputValue(i_scale[1]).asPluginData();
		if(_data)
		{
			textureCacheData* texturecachedata = static_cast<textureCacheData*>(_data);
			Math::ParamTex2d<unsigned char, Math::EJT_MUL>* scaleparam = (Math::ParamTex2d<unsigned char, Math::EJT_MUL>*)&texturecachedata->paramTex2d;
			for( int i=0; i<pData->cv.getCount(); i++)
			{
				HairCV::CV& outch = pData->cv.getControlHair(i);

				// ����� UV ��� ��������
				std::vector<int> fv;
				gi->findFacevertexForVertex(i, fv);
				Math::Vec2f uv(0);
				if( !fv.empty())
				{
					int unindex = gi->FV_uvindex( fv[0]);
					uv = g->uv(unindex);
				}

				float scale = scaleparam->getValue(uv.x, uv.y);
				if( scale<0.001f)
					scale = 0.001f;
				outch.scale(scale);

		//		Math::Matrix4f basis;
		//		HairCV::getControlHairBasis(i, cvpout, gi, g, basis);
		//		Math::Matrix4f basisinv = basis.inverted();
			}
		}
	}

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}

/*/
MStatus HairControlsShape::computeHairCV( const MPlug& plug, MDataBlock& data)
{
	MStatus stat;

	HairGeometryIndices* gi = this->LoadHairGeometryIndices(data);
	if( !gi) 
		return MS::kSuccess;
	HairGeometry* g = this->LoadHairGeometry(data);
	if( !gi) 
		return MS::kSuccess;
	HairCVPositions* cvp = LoadHairCVPositions(data);
	if( !cvp) 
		return MS::kSuccess;

	HairCVData* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( o_HairCV, &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<HairCVData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( HairCVData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<HairCVData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}


	MArrayDataHandle cpdata = data.inputArrayValue(mControlPoints, &stat);
	int ec = cpdata.elementCount();

	pData->cv.cvcount = cvp->cvcount;
	pData->cv.cvs.resize(cvp->controlhairCount());
	for(int h=0; h<cvp->controlhairCount(); h++)
	{
		HairCV::CV& hc = pData->cv.cvs[h];
		hc.init(cvp->cvcount);

		for( int v=0; v<cvp->cvcount; v++)
		{
			int index = h*cvp->cvcount + v;
			stat = cpdata.jumpToElement(index);
			if( !stat) 
				continue;

			MVector vert = cpdata.inputValue(&stat).asVector();
			if( !stat) 
				continue;
			::copy( hc.ts[v], vert);
		}
//		hc.calcVFactor();
		hc.bValid = true;
	}
//	pData->cv.calcOS(cvp, gi, g);

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}
/*/
MStatus HairControlsShape::ComputeCVTransform(const MPlug& plug, MDataBlock& data)
{
	MStatus stat;
displayStringD("ComputeCVTransform");
	HairGeometryIndices* geometryIndices = LoadHairGeometryIndices(data);
	if( !geometryIndices) return MS::kFailure;
	HairGeometry* geometry = LoadHairGeometry(data);
	if( !geometry) return MS::kFailure;
	HairCVPositions* hairCVPositions = LoadHairCVPositions(data);
	if( !hairCVPositions) return MS::kFailure;

	HairCvBasesData* basas = LoadHairCvBases(data);
	if( !basas) return MS::kFailure;

	int count = hairCVPositions->controlhairCount();
	MArrayDataHandle dh = data.outputArrayValue(o_CVTransform);

	MArrayDataBuilder oldbuilder = dh.builder(&stat);
	MArrayDataBuilder builder( oldbuilder);
	for(int i=0; i<count; i++)
	{
		MDataHandle rec = builder.addElement(i);

		Math::Matrix4f matrix = basas->bases[i];
//		HairCV::getControlHairBasis(i, hairCVPositions, geometryIndices, geometry, matrix);

//displayStringD("+OrthoBasis %d {%f, %f}", i, uv.x, uv.y);
//displayStringD("%d(%d):", i, basas->bases.size());
//displayMatrix(matrix);

		// ������ �����!
		float l = 1;
		std::swap(matrix[1], matrix[2]);
//		matrix[0] = -matrix[1];

		MMatrix m; copy(m, matrix);
		{
			MTransformationMatrix mtm(m);
			double sc[3], sh[3];
			stat = mtm.getScale(sc, MSpace::kObject);


//			MEulerRotation rot = mtm.eulerRotation();
			double rot[3];
			MTransformationMatrix::RotationOrder rotationOrder;
			mtm.getRotation( rot, rotationOrder );

			stat = mtm.getShear(sh, MSpace::kObject);

			MVector tr = mtm.translation(MSpace::kTransform, &stat);

			rec.child(o_CVtranslate).set(MFloatVector(tr));

			float factor = (float)MAngle( 1, MAngle::uiUnit()).asRadians();
//			float factor = 1;
			MFloatVector r( (float)rot[0]/factor, (float)rot[1]/factor, (float)rot[2]/factor); 
			rec.child(o_CVrotate).set(r);

		}
	}
	dh.set(builder);
	dh.setAllClean();
	return MS::kSuccess;
}


HairCVPositions* HairControlsShape::LoadHairCVPositions(MDataBlock& data)
{
	MStatus stat;
	MDataHandle inputData = data.inputValue(o_HairCVPositions, &stat);
	MPxData* pxdata = inputData.asPluginData();
	if( !pxdata)
		return NULL;

	HairCVPositionsData* hc = (HairCVPositionsData*)pxdata;
	return &hc->cvpos;
}
HairCVPositions* HairControlsShape::LoadHairCVPositionsOut(MDataBlock& data)
{
	MStatus stat;
	MDataHandle inputData = data.inputValue(o_HairCVPositionsOut, &stat);
	MPxData* pxdata = inputData.asPluginData();
	if( !pxdata)
		return NULL;

	HairCVPositionsData* hc = (HairCVPositionsData*)pxdata;
	return &hc->cvpos;
}
HairCvBasesData* HairControlsShape::LoadHairCvBases(MDataBlock& data)
{
	MStatus stat;
	MDataHandle inputData = data.inputValue(o_HairCvBases, &stat);
	MPxData* pxdata = inputData.asPluginData();
	if( !pxdata) return NULL;
	HairCvBasesData* hc = (HairCvBasesData*)pxdata;
	return hc;
}

HairCV* HairControlsShape::LoadHairCV(MDataBlock& data)
{
	MStatus stat;
	MDataHandle inputData = data.inputValue(o_HairCV, &stat);
	MPxData* pxdata = inputData.asPluginData();
	if( !pxdata) return NULL;
	HairCVData* hc = (HairCVData*)pxdata;
	return &hc->cv;
}

HairGeometryIndices* HairControlsShape::LoadHairGeometryIndices(MDataBlock& data)
{
	MStatus stat;
	MDataHandle inputData = data.inputValue(i_HairGeometryIndices, &stat);
	MPxData* pxdata = inputData.asPluginData();
	if( !pxdata)
		return NULL;
	HairGeometryIndicesData* hgh = (HairGeometryIndicesData*)pxdata;
	return &hgh->hgh;
}
HairGeometry* HairControlsShape::LoadHairGeometry(MDataBlock& data)
{
	MStatus stat;
	MDataHandle inputData = data.inputValue(i_HairGeometry, &stat);
	MPxData* pxdata = inputData.asPluginData();
	if( !pxdata) 
		return NULL;
	HairGeometryData* gd = (HairGeometryData*)pxdata;
	if( !gd->geometry.isValid())
		return NULL;
	return &gd->geometry;
}



HairCVandGeometry* HairControlsShape::getPointsInOS()
{
	MDataBlock data = this->forceCache();
	haircvandgeometry.cv = LoadHairCV(data);
	haircvandgeometry.bases = LoadHairCvBases(data);
	if(!haircvandgeometry.cv || !haircvandgeometry.bases)
		return 0;
	return &haircvandgeometry;
/*/
//	MVectorArray* ts = getControlPoints();
	HairGeometry* g = LoadHairGeometry(data);
	if( !g) return &pointsinos;
	HairGeometryIndices* gi = LoadHairGeometryIndices(data);
	if( !gi) return &pointsinos;

	HairCVPositions* cvp = LoadHairCVPositions(data);
	if( !cvp) return &pointsinos;

	HairCV* cv = LoadHairCV(data);
	if( !cv) return &pointsinos;

	int chcount = cv->cvs.size();
	int cvcount = cvp->cvcount;
	pointsinos.init(cvcount, chcount);
	
	for(int h=0; h<chcount; h++)
	{
		HAIRCONTROL& hc = cv->cvs[h];
		for(int v=0; v<cvcount; v++)
		{
			if(hc.bValid)
				pointsinos[h][v] = hc.os[v];
		}
	}
	return &pointsinos;
/*/
}



///////////////////////////////////////////////
// MPxNode  

MStatus HairControlsShape::compute( 
	const MPlug& plug,
	MDataBlock& data )
{
	displayStringD("HairControlsShape::compute %s", plug.name().asChar());
	data.inputValue(i_message).setClean();

	try
	{
		if(plug==o_HairCVPositions)
		{
			return computeHairCVPositions( plug, data);
		}
		else if(plug==o_HairCvBases)
		{
			return ComputeCvBases(plug, data);
		}
	//	else if(plug==o_HairCV)
	//	{
	//		data.setClean(plug);
	//		return MS::kSuccess;
	//	}
		else if(plug==o_Guides)
		{
			HairCVPositions* cvp = LoadHairCVPositions(data);
			int guides = 0;
			if(cvp)
				guides = cvp->controlhairCount();
			data.outputValue(plug).set(guides);
			data.setClean(plug);
			return MS::kSuccess;
		}
		if(plug == o_CVTransform || plug.parent()==o_CVTransform)
			return ComputeCVTransform(plug, data);
		if(plug == o_HairCVPositionsOut)
			return computeHairCVPositionsOut( plug, data);
		if(plug == o_HairCVout)
			return computeHairCVout( plug, data);
		if( plug == i_scale[1])
			return ComputeTextureCache3(plug, i_scale, this->i_PaintResXY, data);
	}
	catch(...)
	{
		displayStringError("exception in HairControlsShape::compute %s", plug.name().asChar());
	}
	return MS::kUnknownParameter;
}
MStatus	HairControlsShape::setDependentsDirty( const MPlug& plug, MPlugArray & affected)
{
 	if( plug==mControlPoints || plug.array()==mControlPoints)
		displayStringD("setDependentsDirty %s", plug.name().asChar());
	return MPxComponentShape::setDependentsDirty( plug, affected);
}
///
bool HairControlsShape::getInternalValue( 
	const MPlug& plug,
	MDataHandle& result)
{
	int index = plug.logicalIndex();
	if( plug == mControlPoints)
	{
		Math::Vec3f v = vertexOS(index);
		result.set( (double)v.x, (double)v.y, (double)v.z );
		return true;
	}
	else if(plug == mControlValueX)
	{
		index = plug.parent().logicalIndex();
		Math::Vec3f v = vertexOS(index);
		result.set( (double)v.x );
		return true;
	}
	else if ( plug == mControlValueY ) 
	{
		index = plug.parent().logicalIndex();
		Math::Vec3f v = vertexOS(index);
		result.set( (double)v.y );
		return true;
	}
	else if ( plug == mControlValueZ ) 
	{
		index = plug.parent().logicalIndex();
		Math::Vec3f v = vertexOS(index);
		result.set( (double)v.z );
		return true;
	}
	return MPxComponentShape::getInternalValue( plug, result);
}
///
bool HairControlsShape::setInternalValue( 
	const MPlug& plug,
	const MDataHandle& handle)
{
	int index = plug.logicalIndex();
	if( plug == mControlPoints)
	{
		Math::Vec3f* v = vertex(index);
		if(!v) return false;
		double3& ptData = handle.asDouble3();
		::copy( *v, ptData);
		return true;
	}
	else if(plug == mControlValueX)
	{
		index = plug.parent().logicalIndex();
		Math::Vec3f* v = vertex(index);
		if(!v) return false;
		double val = handle.asDouble();
		v->x = (float)val;
		return true;
	}
	else if ( plug == mControlValueY ) 
	{
		index = plug.parent().logicalIndex();
		Math::Vec3f* v = vertex(index);
		if(!v) return false;
		double val = handle.asDouble();
		v->y = (float)val;
		return true;
	}
	else if ( plug == mControlValueZ ) 
	{
		index = plug.parent().logicalIndex();
		Math::Vec3f* v = vertex(index);
		if(!v) return false;
		double val = handle.asDouble();
		v->z = (float)val;
		return true;
	}
	return MPxComponentShape::setInternalValue( plug, handle);
}
#if MAYA_API_VERSION >= 200800
///
int HairControlsShape::internalArrayCount( 
	const MPlug& plug,
	const MDGContext& ctx) const
{
//	displayString("internalArrayCount %s (NOT IMPLEMENTED)", plug.name().asChar());
	return MPxComponentShape::internalArrayCount( plug, ctx);
}
#endif


/// 
bool HairControlsShape::isBounded() const
{
	HairControlsShape* pThis = (HairControlsShape*)this;
	HairCVandGeometry* pva = pThis->getPointsInOS();
	if( !pva || !pva->cv->getCount()) 
		return false;
	return true;
//	return false;
}
///
MBoundingBox HairControlsShape::boundingBox() const
{
	MBoundingBox bx;
	HairControlsShape* pThis = (HairControlsShape*)this;
	HairCVandGeometry* pva = pThis->getPointsInOS();
	if( !pva || !pva->cv->getCount()) 
		return bx;

	for(int y=0; y<pva->cv->getCount(); y++)
	{
		HairCV::CV& ch = pva->cv->getControlHair(y);
		if( !ch.bValid)
			continue;
		for(int x=0; x<ch.vc(); x++)
		{
			Math::Vec3f v = ch.ts[x];
			v = pva->bases->bases[y]*v;
			MVector point;
			::copy(point, v);
			bx.expand( MPoint(point));
		}
	}
	return bx;
}


void HairControlsShape::componentToPlugs( 
	MObject& component,
	MSelectionList& list) const
{
	/*/
	MStatus stat;
	if ( component.hasFn(MFn::kSingleIndexedComponent) ) 
	{
		MFnSingleIndexedComponent fnVtxComp( component );
    	MObject thisNode = thisMObject();
		MPlug plug( thisNode, mControlPoints );
		// If this node is connected to a tweak node, reset the
		// plug to point at the tweak node.
		//
		bool res = convertToTweakNodePlug(plug);

		int len = fnVtxComp.elementCount();

		for ( int i = 0; i < len; i++ )
		{
			stat = plug.selectAncestorLogicalIndex(fnVtxComp.element(i),
											plug.attribute());
			list.add(plug);
			displayStringD("HairControlsShape::componentToPlugs: %s", plug.name().asChar());
		}
	}
	/*/
	MPxComponentShape::componentToPlugs( component, list);
	/*/
	MStringArray array;
	list.getSelectionStrings(array);
	for( unsigned int i=0; i<array.length(); i++)
	{
		displayStringD("HairControlsShape::componentToPlugs: %s", array[i].asChar());
	}
	/*/
}

MPxSurfaceShape::MatchResult HairControlsShape::matchComponent( 
	const MSelectionList& item,
	const MAttributeSpecArray& spec,
	MSelectionList& list 
	)
{
	/*/
	for(int i=0; i<(int)spec.length(); i++)
	{
		MAttributeSpec s = spec[i];
		displayStringD("spec[%d] = %s (dim=%d)", i, s.name().asChar(), s.dimensions());
		for(int d=0; d<s.dimensions(); d++)
		{
			MAttributeIndex ai = s[d];
			displayStringD("  ai[%d] = %d %d %d %d", d, ai.isBounded(), ai.hasRange(), ai.hasUpperBound(), ai.hasLowerBound());
			
		}
	}
	/*/

	// Look for attributes specifications of the form :
	//     vtx[ index ]
	//     vtx[ lower:upper ]
	//
	if( spec.length()==1)
	{
		MAttributeSpec attrSpec = spec[0];
		int dim = attrSpec.dimensions();
		if(dim > 0 && attrSpec.name() == "vtx")
		{
			MAttributeIndex attrIndex = attrSpec[0];

			MDataBlock data = forceCache();
			HairCV* cv = LoadHairCV(data);
			if( !cv)
			{
				displayString("HairControlsShape::matchComponent() FAILED HairCV");
				return MPxSurfaceShape::kMatchInvalidAttributeDim;	
			}

			int numVertices = cv->getCount()*cv->controlVertexCount();

			int upper = 0;
			int lower = 0;
			if ( attrIndex.hasLowerBound() ) 
				attrIndex.getLower( lower );
			if ( attrIndex.hasUpperBound() )
				attrIndex.getUpper( upper );

			// Check the attribute index range is valid
			//
			if ( (lower > upper) || (upper >= numVertices) ) 
			{
				displayStringD("HairControlsShape::matchComponent() FAILED RANGE");
				return MPxSurfaceShape::kMatchInvalidAttributeRange;	
			}

			MDagPath path;
			item.getDagPath( 0, path );
			MFnSingleIndexedComponent fnVtxComp;
			MObject vtxComp = fnVtxComp.create( MFn::kMeshVertComponent );

			for ( int i=lower; i<=upper; i++ )
			{
				fnVtxComp.addElement( i );
			}
			list.add( path, vtxComp );
	/*/
			displayStringD("HairControlsShape::matchComponent() OK");
	/*/
			return MPxSurfaceShape::kMatchOk;
		}
	}

	MPxSurfaceShape::MatchResult res = MPxComponentShape::matchComponent( item, spec, list);
	/*/
	displayStringD("HairControlsShape::matchComponent() return %d", (int)res);
	/*/
	return res;
}

bool HairControlsShape::match(	
	const MSelectionMask & mask,
	const MObjectArray& componentList 
	) const
{
	bool res = MPxComponentShape::match(mask,componentList);
	/*/
	displayStringD("HairControlsShape::match() return %d", res?1:0);
	/*/
	return res;
}


void HairControlsShape::transformUsing( 
	const MMatrix& mat,
	const MObjectArray& componentList )
{
	transformUsing( mat, componentList, MPxSurfaceShape::kNoPointCaching, NULL);
}
void HairControlsShape::transformUsing( 
	const MMatrix& mat,
	const MObjectArray& componentList,
	MVertexCachingMode cachingMode,
	MPointArray* pointCache)
{
	displayStringD("HairControlsShape::transformUsing");

	Math::Matrix4f m;
	::copy( m, mat);
	MStatus stat;
	MDataBlock data = forceCache();
	HairCV* cv = LoadHairCV(data);
	HairCvBasesData* bases = LoadHairCvBases(data);
	if( !bases || !cv) 
		return;
	int len = componentList.length();
	if (cachingMode == MPxSurfaceShape::kRestorePoints) 
	{
		// restore the points based on the data provided in the pointCache attribute
		//
		int cacheLen = pointCache->length();
		int j=0;
		if (len > 0) 
		{
			for ( int i = 0; i < len && j < cacheLen; i++ )
			{
				MObject comp = componentList[i];
				MFnSingleIndexedComponent fnComp( comp );
				int elemCount = fnComp.elementCount();
				for ( int idx=0; idx<elemCount && j < cacheLen; idx++, ++j ) 
				{
					int elemIndex = fnComp.element( idx );
					Math::Vec3f* v = cv->getSingleIndexedVertex(elemIndex);
					if(!v) continue;
					Math::Vec3f chv; 
					::copy( chv, (*pointCache)[j]);
					*v = chv;
				}
			}
		} 
		else 
		{
			// if the component list is of zero-length, it indicates that we
			// should transform the entire surface
			//
			len = cv->controlVertexCount()*cv->getCount();
			for ( int idx = 0; idx < len && j < cacheLen; ++idx, ++j ) 
			{
				int elemIndex = idx;

				Math::Vec3f* v = cv->getSingleIndexedVertex(elemIndex);
				if(!v) continue;
				Math::Vec3f chv; 
				::copy( chv, (*pointCache)[j]);
				*v = chv;
			}
		}
	}
	else
	{
		bool savePoints = (cachingMode == MPxSurfaceShape::kSavePoints);

		std::map<int, std::vector<float> > affectedhairs;

		// Transform the surface vertices with the matrix.
		// If savePoints is true, save the points to the pointCache.
		if (len > 0) 
		{
			// �� ��� ����������
			for ( int i=0; i<len; i++ )
			{
				MObject comp = componentList[i];
				MFnSingleIndexedComponent fnComp( comp );
				int elemCount = fnComp.elementCount();

				if (savePoints && 0 == i) 
					pointCache->setSizeIncrement(elemCount);
				for ( int idx=0; idx<elemCount; idx++ )
				{
					int elemIndex = fnComp.element( idx );

					int hairindex = 0, vertindex=0;
					Math::Vec3f* v = cv->getSingleIndexedVertex(elemIndex, &hairindex);
					bool res = cv->getSingleIndexedVertex(elemIndex, hairindex, vertindex);

					MPoint savept(0, 0); 
					if( v) ::copy(savept, *v);
					if (savePoints) 
						pointCache->append(savept);
					if(!res) continue;

					if( affectedhairs.find(hairindex)==affectedhairs.end())
					{
						HairCV::CV& hair = cv->getControlHair(hairindex);
						affectedhairs[hairindex].resize( hair.ts.size(), 0);
					}
					affectedhairs[hairindex][vertindex] = 1;
				}
			}
		} 
		else 
		{
			// ��� ��������
			len = cv->controlVertexCount()*cv->getCount();
			if (savePoints) 
				pointCache->setSizeIncrement(len);
			for ( int elemIndex = 0; elemIndex < len; ++elemIndex ) 
			{
				int hairindex = 0, vertindex=0;
				Math::Vec3f* v = cv->getSingleIndexedVertex(elemIndex, &hairindex);
				bool res = cv->getSingleIndexedVertex(elemIndex, hairindex, vertindex);

				MPoint savept(0, 0); if( v) ::copy(savept, *v);
				if (savePoints) pointCache->append(savept);

				if(!res) continue;

				if( affectedhairs.find(hairindex)==affectedhairs.end())
				{
					HairCV::CV& hair = cv->getControlHair(hairindex);
					affectedhairs[hairindex].resize( hair.ts.size(), 0);
				}
				affectedhairs[hairindex][vertindex] = 1;
			}
		}

		// ����� �������������
		Util::DllProcedureTmpl<getIHairCVmanipulator_t> dllproc;
		#ifndef SAS
		const char* hairdllname = "HairMath.dll";
		#else
		const char* hairdllname = "chevelureMath.dll";
		#endif

		if( dllproc.LoadFormated("getTransformManipulator", hairdllname))
		{
			IHairCVmanipulator* manipulator = (*dllproc)();
			if( manipulator)
			{
				cls::renderCacheImpl<> attributes;
				attributes.Attribute("transform", m);
				if( manipulator->Start(&attributes))
				{
					std::map<int, std::vector<float> >::iterator it = affectedhairs.begin();
					for( ; it != affectedhairs.end(); it++)
					{
						int hairindex = it->first;
						HairCV::CV& hair = cv->getControlHair(hairindex);
						Math::Matrix4f& basis = bases->bases[hairindex];
						Math::Matrix4f& basis_inv = bases->bases_inv[hairindex];
						manipulator->ComputeSingleHair(hair, it->second, basis, basis_inv);
					}
				}
				manipulator->Finish();
			}
		}
	}
	MDataHandle dHandle = data.outputValue( o_HairCV, &stat );
	MPlug plug(thisMObject(), o_HairCV);
	plug.setValue(dHandle);
	dHandle.setClean();


	childChanged( MPxSurfaceShape::kBoundingBoxChanged );
}
void HairControlsShape::tweakUsing( 
	const MMatrix& mat,
	const MObjectArray& componentList,
	MVertexCachingMode cachingMode,
	MPointArray* pointCache,
	MArrayDataHandle& handle )
{
	displayString("HairControlsShape::tweakUsing NOT IMPLEMENTED");

}


MPxGeometryIterator* HairControlsShape::geometryIteratorSetup(
	MObjectArray& componentList,
	MObject& components,
	bool forReadOnly )
{
	HairControlsIterator * result = NULL;
	if ( components.isNull() ) 
	{
		result = new HairControlsIterator( getPointsInOS(), componentList );
	}
	else 
	{
		result = new HairControlsIterator( getPointsInOS(), components );
	}
	return result;
}

bool HairControlsShape::acceptsGeometryIterator( bool writeable )
{
	return getPointsInOS()!=0;
}

bool HairControlsShape::acceptsGeometryIterator( MObject&, bool writeable, bool forReadOnly )
{
	return true;
}

int HairControlsShape::current_file_version = 0;

bool HairControlsShape::saveFrame(const char* filename)
{
	MStatus stat;
	MDataBlock data = this->forceCache();
	MFnDependencyNode thisdn(thisMObject());

	HairGeometryIndices* geometryIndices = LoadHairGeometryIndices(data);
	if( !geometryIndices) return false;
	HairGeometry* geometry = LoadHairGeometry(data);
	if( !geometry) return false;
	HairCVPositions* hairCVPositions = LoadHairCVPositions(data);
	if( !hairCVPositions) return false;
	HairCV* hairCV = LoadHairCV(data);
	if( !hairCV) return false;

	Util::FileStream file;
	if( !file.open(filename, true))
	{
		displayStringError("cant open file %s for write", filename);
		return false;
	}
	int version = current_file_version;
	file >> version;
	
	hairCV->serialize(file);
	return true;
}
bool HairControlsShape::loadFrame(const char* filename)
{
	MStatus stat;
	MDataBlock data = this->forceCache();
	MFnDependencyNode thisdn(thisMObject());

	HairGeometryIndices* geometryIndices = LoadHairGeometryIndices(data);
	if( !geometryIndices) return false;
	HairGeometry* geometry = LoadHairGeometry(data);
	if( !geometry) return false;
	HairCVPositions* hairCVPositions = LoadHairCVPositions(data);
	if( !hairCVPositions) return false;
	HairCV* hairCV = LoadHairCV(data);
	if( !hairCV) return false;


	Util::FileStream file;
	if( !file.open(filename, false))
	{
		displayStringError("cant open file %s", filename);
		return false;
	}
	int version = 0;
	file >> version;
	if( version!=current_file_version)
	{
		displayStringError("invalid file version");
		return false;
	}

	hairCV->serialize(file);

	MDataHandle dHandle = data.outputValue( o_HairCV, &stat );
	MPlug(thisMObject(), o_HairCV).setValue(dHandle);
	dHandle.setClean();

	return true;
}

// ������ �� �������� ���������
bool HairControlsShape::importCV(
	std::vector< std::vector<Math::Vec3f> >& shavepoints
	)
{
	MStatus stat;
	int cvcount;
	MPlug( thisMObject(), i_CVcount).getValue(cvcount);

	MDataBlock data = this->forceCache();
	HairGeometry* g = LoadHairGeometry(data);
	HairGeometryIndices* gi = LoadHairGeometryIndices(data);
	HairCVPositions* cvp = LoadHairCVPositions(data);
	HairCV* cv = LoadHairCV(data);
	if(!g || !gi || !cvp || !cv)
		return false;

	cv->import(shavepoints, gi, g, cvp);

	MDataHandle dHandle = data.outputValue( o_HairCV, &stat );
	MPlug(thisMObject(), o_HairCV).setValue(dHandle);
	dHandle.setClean();

	return true;
}

// �������
bool HairControlsShape::exportCV(
	std::vector< std::vector<Math::Vec3f> >& shavepoints,
	int cvinhair
	)
{
	MStatus stat;
	int cvcount;
	MPlug( thisMObject(), i_CVcount).getValue(cvcount);

	MDataBlock data = this->forceCache();
	HairGeometry* g = LoadHairGeometry(data);
	HairGeometryIndices* gi = LoadHairGeometryIndices(data);
	HairCVPositions* cvp = LoadHairCVPositions(data);
	HairCV* cv = LoadHairCV(data);
	if(!g || !gi || !cvp || !cv)
		return false;

	cv->export_(shavepoints, cvinhair, gi, g, cvp);
	return true;
}

bool HairControlsShape::connectMayaHair(
	MObject hairSystem, 
	MObject follicleroottransform, 
	MObject outputCurvesTransform
	)
{
	MStatus result=MS::kFailure, stat;
displayStringD("ConnectCVCurves");
	MDataBlock data = this->forceCache();
	MFnDependencyNode thisdn(thisMObject());
	MFnDependencyNode fnhairSystem(hairSystem);

	HairGeometryIndices* geometryIndices = LoadHairGeometryIndices(data);
	if( !geometryIndices) return false;
	HairGeometry* geometry = LoadHairGeometry(data);
	if( !geometry) return result;
	HairCVPositions* hairCVPositions = LoadHairCVPositions(data);
	if( !hairCVPositions) return false;
	HairCV* hairCV = LoadHairCV(data);
	if( !hairCV) return result;


	int count = hairCVPositions->controlhairCount();
	int vCount = hairCVPositions->controlVertexCount();

	char cmd[1024];


	// hairSystem
	MPlug plug_CVTransform(thisMObject(), o_CVTransform);
	for(int i=0; i<count; i++)
	{
		HairCV::CV& hc = hairCV->cvs[i];
		if( !hc.bValid)
			continue;
		if( hc.getLength()<1e-8)
			continue;

		MObject transform;
		MObject follicle;

		// ����� ��������� 
		MPlug pel = plug_CVTransform[i];
		MPlug pl = pel.child(o_CVtranslate);
		MPlugArray ar;
		pl.connectedTo(ar, false, true);
		if(ar.length()<1)
		{
			// ������� ����� transform � follecle
			MFnTransform dntransform;
			transform = dntransform.create(follicleroottransform);
		}
		else
		{
			// ��� ���������
			transform = ar[0].node();
		}
		MFnTransform dntransform(transform);
		// ��������� transform � ����������� o_CVTransform -> follicle 
		{
			sprintf(cmd, "connectAttr -f %s.o_CVTransform[%d].o_CVtranslate %s.t", 
				thisdn.name().asChar(), i, dntransform.name().asChar());
			MGlobal::executeCommand(cmd);

			sprintf(cmd, "connectAttr -f %s.o_CVTransform[%d].o_CVrotate %s.r", 
				thisdn.name().asChar(), i, dntransform.name().asChar());
			MGlobal::executeCommand(cmd);
		}

		// ����� ��������
		if( dntransform.childCount()==0)
		{
			// follicle
			MString folliclename;
			sprintf(cmd, "createNode follicle -p %s", dntransform.name().asChar());
			MGlobal::executeCommand(cmd, folliclename);
			nodeFromName(folliclename, follicle);

			sprintf(cmd, "setAttr %s.restPose 1", folliclename.asChar());
			MGlobal::executeCommand(cmd);

		}
		else
		{
			follicle = dntransform.child(0);
		}
		MString folliclename = MFnDependencyNode(follicle).name();

		// follicle -> hairSystem
		sprintf(cmd, "connectAttr -f %s.outHair %s.inputHair[%d]", 
			folliclename.asChar(), fnhairSystem.name().asChar(), i);
		MGlobal::executeCommand(cmd);

		// hairSystem -> follicle
		sprintf(cmd, "connectAttr -f %s.outputHair[%d] %s.currentPosition", 
			fnhairSystem.name().asChar(), i, folliclename.asChar());
		MGlobal::executeCommand(cmd);

		// ��������� ����
		{
			// DEBUG ��� �����!!!
			MString curve;
			{
				int vCount = hc.vc();
				MPointArray controlVertices(vCount);
				MDoubleArray knotSequences(vCount);
				for(int k=0; k<vCount; k++)
				{
					Math::Vec3f pt = hc.ts[k];
					std::swap( pt.y, pt.z);
					// ��������� � TS
					knotSequences[k] = k;
					controlVertices[k] = MPoint(pt.x, pt.y, pt.z);
				}

				MFnNurbsCurve _curve;
				MObject dncurve = _curve.create(controlVertices, knotSequences, 1, MFnNurbsCurve::kOpen, false, false, transform);
				curve = _curve.name();
				// curve.intermediateObject
				sprintf(cmd, "setAttr %s.intermediateObject 1", curve.asChar());
				MGlobal::executeCommand(cmd);
			}
			// curve -> follicle
			sprintf(cmd, "connectAttr -f %s.worldSpace[0] %s.startPosition", 
				curve.asChar(), folliclename.asChar());
			MGlobal::executeCommand(cmd);

			// ����� �� ����?!!!
			sprintf(cmd, "connectAttr -f %s.worldSpace[0] %s.restPosition", 
				curve.asChar(), folliclename.asChar());
			MGlobal::executeCommand(cmd);
		}

		// follicle -> curve
		// ����� outputcurve
		pl = MFnDependencyNode( follicle).findPlug("outCurve");
		pl.connectedTo(ar, false, true);
		if(ar.length()<1)
		{
			MString outcurvetransform;
			sprintf(cmd, "createNode transform -p %s", MFnDependencyNode(outputCurvesTransform).name().asChar());
			MGlobal::executeCommand(cmd, outcurvetransform);

			// ������� ���������
			MString outcurve;
			sprintf(cmd, "createNode nurbsCurve -p %s", outcurvetransform.asChar());
			MGlobal::executeCommand(cmd, outcurve);

			sprintf(cmd, "connectAttr -f %s.outCurve %s.create", 
				folliclename.asChar(), outcurve.asChar());
			MGlobal::executeCommand(cmd);
		}
	}
	return true;
}

bool projectPoint(
	Math::Vec3f v, const Math::Matrix4f& m, const Math::Matrix4f& shapemat, const Math::Matrix4f& meshmatinv, 
	MFnMesh& mesh, MPoint& pt, MPoint& res, MVector& normal, double& dist
	)
{
	MStatus stat;
	v = m*v;
	v = shapemat*v;
	v = meshmatinv*v;

	::copy(pt, v);
	stat = mesh.getClosestPointAndNormal(
		pt, res, normal);
	if(!stat)
		return false;
	normal.normalize();

	double dot = normal*(pt-res);
	dist = (pt-res).length();
	if( dot<0) dist = -dist;
	dist = dot;
	return true;
}

// �������� � ������
bool HairControlsShape::meshOp(
	const char* op, 
	MMatrix _shapemat, 
	MObject meshobj, 
	MMatrix _meshmat,
	MObject meshobj2, 
	MMatrix _meshmat2,
	const char* texturename
	)
{
	Math::Matrix4f shapemat, meshmat;
	::copy(shapemat, _shapemat);
	::copy(meshmat, _meshmat);

	MStatus stat;
	MFnMesh mesh(meshobj, &stat);
	if(!stat) return false;

	// ��������
	Math::matrixMxN<unsigned char> texture;
	if( texturename && texturename[0])
	{
		Math::LoadMatrixFromIFF(texturename, texture);
	}

	MDataBlock data = this->forceCache();
	HairGeometryIndices* geometryIndices = LoadHairGeometryIndices(data);
	if( !geometryIndices) return false;
	HairGeometry* geometry = LoadHairGeometry(data);
	if( !geometry) return false;
	HairCVPositions* hairCVPositions = LoadHairCVPositions(data);
	if( !hairCVPositions) return false;
	HairCV* hairCV = LoadHairCV(data);
	if( !hairCV) return false;

	Math::Matrix4f shapematinv = shapemat.inverted();
	Math::Matrix4f meshmatinv = meshmat.inverted();

	int count = hairCVPositions->controlhairCount();
	int vCount = hairCVPositions->controlVertexCount();

	MFnDependencyNode dn(thisMObject());
	MPlug srcPoseplug = dn.findPlug("srcPose");
	if( srcPoseplug.isNull())
	{
		MFnTypedAttribute mesAttr;
		MObject _mesAttr = mesAttr.create("srcPose", "srpo", MFnData::kVectorArray);
		mesAttr.setConnectable(true);
		mesAttr.setStorable(true);
		mesAttr.setWritable(true);
		mesAttr.setReadable(true);
		mesAttr.setHidden(false);
		mesAttr.setKeyable(false);
		dn.addAttribute(_mesAttr);
		srcPoseplug = MPlug(thisMObject(), _mesAttr);
	}
	MVectorArray srcPose;
	{
		MObject val;
		srcPoseplug.getValue(val);
		MFnVectorArrayData arr(val);
		srcPose = arr.array();
	}
	if( strcmp( op, "setSrcPose")==0)
	{
		srcPose = MVectorArray(count*vCount);
	}


	for(int i=0; i<count; i++)
	{
		HairCV::CV& hc = hairCV->cvs[i];
		if( !hc.bValid)
			continue;
		if( hc.getLength()<1e-8)
			continue;

		Math::Matrix4f m;
		bool res = HairCV::getControlHairBasisSrc(i, hairCVPositions, geometryIndices, geometry, m);
		Math::Matrix4f minv = m.inverted();

		Math::Vec2f uv = HairCV::getControlHairUV(i, hairCVPositions, geometryIndices, geometry);

		float affect = 1.f;
		if( !texture.empty())
		{
			unsigned char c = texture.interpolation((float)uv.x, (float)uv.y, false, false);
			affect = (c/255.f);
		}

		int vCount = hc.vc();


		if( strcmp( op, "makeLikeTantens2")==0)
		{
			double dist[2]={0, 0};
			MVector normal[2];
			MPoint pt[2], res[2];
			if( !projectPoint(
				hc.ts.front(), m, shapemat, meshmatinv, 
				mesh, pt[0], res[0], normal[0], dist[0]
				))
			{
				displayString("getClosestPointAndNormal(%d %d) failed", i, 0);
				continue;
			}
			if( !projectPoint(
				hc.ts.back(), m, shapemat, meshmatinv, 
				mesh, pt[1], res[1], normal[1], dist[1]
				))
			{
				displayString("getClosestPointAndNormal(%d %d) failed", i, hc.ts.size()-1);
				continue;
			}
			double rootdist = dist[0];

			for(int k=0; k<vCount; k++)
			{
				Math::Vec3f v = hc.ts[k];
				float f = k/(float)(vCount-1);

				MVector normalx = normal[0]*(1-f)+normal[1]*f;
				MPoint resx = res[0]*(1-f) + res[1]*f;

				double newdist = 1-atan(4*k/(double)vCount);
				MPoint pt = normalx*rootdist*newdist + resx;

				::copy(v, pt);
				v = meshmat*v;
				v = shapematinv*v;
				v = minv*v;
				hc.ts[k] = v;
			}

			continue;
		}


		for(int k=0; k<vCount; k++)
		{
			Math::Vec3f v = hc.ts[k];
			double dist=0;
			MVector normal;
			MPoint pt, res;
			if( !projectPoint(
				v, m, shapemat, meshmatinv, 
				mesh, pt, res, normal, dist
				))
			{
				displayString("getClosestPointAndNormal(%d %d) failed", i, k);
				continue;
			}

			if( strcmp( op, "makeLikeTantens")==0)
			{
				static double rootdist = 0;
				if(k==0) 
				{
					rootdist = dist;
					continue;
				}
				double newdist = 1-atan(4*k/(double)vCount);
				pt = normal*rootdist*newdist + res;
				
				::copy(v, pt);
				v = meshmat*v;
				v = shapematinv*v;
				v = minv*v;
				hc.ts[k] = affect*v + (1-affect)*hc.ts[k];
			}
			else if( strcmp( op, "setSrcPose")==0)
			{
				srcPose[vCount*i + k].x = dist;
			}
			else if( strcmp( op, "correct")==0)
			{
				if(k==0) continue;
				// ��������� ��������� �� �����������
				int index = vCount*i + k;
				if( (int)srcPose.length()>=index)
				{
					double srcdist = srcPose[index].x;
					double offset = srcdist - dist;
					MVector dir = (pt-res).normal();
					dir = normal;
					dir*=offset;
//					if(srcdist*dist>0 )
//						dir = -dir;
					pt = pt + dir;

					::copy(v, pt);
					v = meshmat*v;
					v = shapematinv*v;
					v = minv*v;
					hc.ts[k] = v;
				}
			}
			else if( strcmp( op, "test")==0)
			{
				if(k==0) continue;
				double dot = normal*(pt-res);
				if(dot>0)
				{
					::copy(v, res);
					v = meshmat*v;
					v = shapematinv*v;
					v = minv*v;
					hc.ts[k] = v;
				}
			}
		}

	}
	MDataHandle dHandle = data.outputValue( o_HairCV, &stat );
	MPlug(thisMObject(), o_HairCV).setValue(dHandle);
	dHandle.setClean();

	if( strcmp( op, "setSrcPose")==0)
	{
		MFnVectorArrayData data;
		MObject val = data.create(srcPose);
		srcPoseplug.setValue(val);
	}

	return true;
}

MStatus HairControlsShape::initialize()
{ 
	MStatus stat;
	MFnTypedAttribute typedAttr;
	MFnNumericAttribute numAttr;
	MFnCompoundAttribute comAttr;
	MFnMessageAttribute mesAttr;
	MFnUnitAttribute unitAttr;

	try
	{
		/*/
		{
			i_time = unitAttr.create( "time", "tm", MTime(0.0));
			::addAttribute(i_time, atInput);

			i_bUseBake = numAttr.create( "useBake", "usb", MFnNumericData::kBoolean, 0);
			::addAttribute(i_bUseBake, atInput);
		}
		/*/
		{
			i_message = mesAttr.create ("messageForAffect", "mfa");
			::addAttribute(i_message, atInput|atConnectable);
		}
		{
			i_importfrom = mesAttr.create ("importfrom", "imf");
			mesAttr.setReadable(false);
			mesAttr.setArray(true);
			mesAttr.setDisconnectBehavior( MFnAttribute::kDelete );
			mesAttr.setIndexMatters(false);
			::addAttribute(i_importfrom, atUnReadable|atWritable|atConnectable|atHidden|atArray);
		}
		{
			i_CVcount = numAttr.create("CVcount", "cvc", MFnNumericData::kInt, 4, &stat);
			numAttr.setMin(3);
			numAttr.setSoftMax(20);
			::addAttribute(i_CVcount, atReadable|atStorable|atCached|atUnConnectable);
		}
//		{
//			i_keepLength = numAttr.create("keepLength", "kl", MFnNumericData::kBoolean, 1, &stat);
//			::addAttribute(i_keepLength, atReadable|atStorable|atCached|atUnConnectable);
//		}
		
		{
			i_HairGeometryIndices = typedAttr.create( "hairGeometryIndices", "hgh", HairGeometryIndicesData::id);
			typedAttr.setStorable(false);
			typedAttr.setReadable(true);
			typedAttr.setWritable(true);
			typedAttr.setHidden(true);
			typedAttr.setConnectable(true);
			stat = addAttribute (i_HairGeometryIndices);
			if (!stat) return stat;

			i_HairGeometry = typedAttr.create( "hairGeometry", "hge", HairGeometryData::id);
			typedAttr.setStorable(false);
			typedAttr.setReadable(true);
			typedAttr.setWritable(true);
			typedAttr.setHidden(true);
			typedAttr.setConnectable(true);
			stat = addAttribute (i_HairGeometry);
			if (!stat) return stat;
		}
		{
			o_HairCvBases = typedAttr.create( "hairCvBases", "hcvb", HairCvBasesData::id);
			::addAttribute(o_HairCvBases, atReadable|atUnWritable|atConnectable|atStorable|atHidden);
		}
		

		{
			o_HairCVPositions = typedAttr.create( "hairCVPositionsInt", "hcvpin", HairCVPositionsData::id);
			typedAttr.setStorable(true);
			typedAttr.setReadable(true);
			typedAttr.setWritable(true);
			typedAttr.setHidden(true);
			typedAttr.setConnectable(true);
			stat = addAttribute (o_HairCVPositions);
			if (!stat) return stat;

			o_Guides = numAttr.create("guides", "gds", MFnNumericData::kInt, 0, &stat);
			::addAttribute(o_Guides, atReadable|atUnWritable|atConnectable|atUnStorable|atUnCached);			

			o_HairCV = typedAttr.create( "hairCVint", "hcvin", HairCVData::id);
			::addAttribute(o_HairCV, atReadable|atWritable|atConnectable|atStorable|atCached);			
			if (!stat) return stat;
		}

		// ��� �������� � maya hair
		{
			//!< ��������� ��� ����������� ������
			o_CVTransform = comAttr.create("o_CVTransform", "ocvt");
			{
				o_CVtranslate = numAttr.createPoint("o_CVtranslate", "ocvtt");
				comAttr.addChild(o_CVtranslate);
			}
			{
				o_CVrotate = numAttr.createPoint("o_CVrotate", "ocvtr");
				comAttr.addChild(o_CVrotate);
			}
			comAttr.setArray(true);
			comAttr.setUsesArrayDataBuilder(true);
			::addAttribute(o_CVTransform, atOutput);
		}
		// OUT:
		{
			o_HairCVPositionsOut = typedAttr.create( "hairCVPositions", "hcvp", HairCVPositionsData::id);
			typedAttr.setStorable(true);
			typedAttr.setReadable(true);
			typedAttr.setWritable(true);
			typedAttr.setHidden(true);
			typedAttr.setConnectable(true);
			stat = addAttribute (o_HairCVPositionsOut);
			if (!stat) return stat;

			o_HairCVout = typedAttr.create( "hairCV", "hcv", HairCVData::id);
			::addAttribute(o_HairCVout, atReadable|atWritable|atConnectable|atStorable|atCached);			
			if (!stat) return stat;
		}
		bool bStorableAttributes = true;
		{
			i_PaintResXY[0] = numAttr.create("PaintAttrResX","parx", MFnNumericData::kInt, 16, &stat);
			::addAttribute(i_PaintResXY[0], atInput|atHidden);
			i_PaintResXY[1] = numAttr.create("PaintAttrResY","pary", MFnNumericData::kInt, 16, &stat);
			::addAttribute(i_PaintResXY[1], atInput|atHidden);

			if( !CreateAttrNTextureCache("scale","sc", i_scale, 0, 1, 1.0, bStorableAttributes, i_PaintResXY))
				return MS::kFailure;
		}


		// �������
		{
			MObject affect = o_HairCVPositions;
			attributeAffects(i_CVcount, affect);
			attributeAffects(i_message, affect);
//			attributeAffects(i_HairGeometryIndices, affect);
		}
		{
			MObject affect = o_Guides;
			attributeAffects(i_CVcount, affect);
			attributeAffects(i_message, affect);
//			attributeAffects(i_HairGeometryIndices, affect);
		}
		{
			MObject affect = o_HairCvBases;
			attributeAffects(i_CVcount, affect);
			attributeAffects(i_message, affect);
			attributeAffects(o_HairCVPositions, affect);
			attributeAffects(i_HairGeometry, affect);
		}
		{
			MObject affect = o_HairCV;
//			attributeAffects(i_CVcount, affect);
//			attributeAffects(i_HairGeometryIndices, affect);
//			attributeAffects(i_HairGeometry, affect);
//			attributeAffects(o_HairCVPositions, affect);
//			attributeAffects(mControlPoints, affect);
		}
		{
			MObject affect = o_CVTransform;
//			attributeAffects(i_CVcount, affect);
//			attributeAffects(i_HairGeometryIndices, affect);
			attributeAffects(i_message, affect);
			attributeAffects(i_HairGeometry, affect);
			attributeAffects(o_HairCVPositions, affect);
			attributeAffects(o_HairCvBases, affect);
		}

		{
			MObject affect = o_HairCVPositionsOut;
			attributeAffects(i_CVcount, affect);
			attributeAffects(i_message, affect);
			attributeAffects(o_HairCVPositions, affect);
//			attributeAffects(i_HairGeometryIndices, affect);
		}
		{
			MObject affect = o_HairCVout;
			HairShape::attributeAffectsT4( i_scale, affect);
			attributeAffects(i_PaintResXY[0], affect);
			attributeAffects(i_PaintResXY[1], affect);
			attributeAffects(i_CVcount, affect);
			attributeAffects(i_message, affect);
			attributeAffects(o_HairCV, affect);
			attributeAffects(o_HairCVPositions, affect);
		}

		if( !SourceMelFromResource(MhInstPlugin, "AEHAIRCONTROLSSHAPETEMPLATE", "MEL", defines))
		{
			displayString("error source AEHairControlsShapeTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}

	return MS::kSuccess;
}

void* HairControlsShape::creator()
{
	return new HairControlsShape();
}


MStatus HairControlsShape::CreateAttrNTextureCache( 
	std::string longname, std::string shortname, 
	MObject* i_HairAttr, double vmin, double vmax, double def, bool bStorableAttributes, 
	MObject i_PaintResXY[2])
{
	MStatus stat;

	// Value
	MFnNumericAttribute numAttr;
	i_HairAttr[0] = numAttr.create(longname.c_str(), shortname.c_str(), MFnNumericData::kDouble, def, &stat);
	if (!stat) return stat;
	numAttr.setStorable(true);
	numAttr.setHidden(false);
	numAttr.setSoftMin(vmin);	
	numAttr.setSoftMax(vmax);	
	stat = addAttribute (i_HairAttr[0]);	
	if (!stat) return stat;
//	attributeAffects(i_HairAttr[0], o_output);

	// Cache
	CreateTextureCacheAttr( longname+"Cache", shortname+"ch", i_HairAttr[0], i_HairAttr[1], bStorableAttributes, i_PaintResXY);

	// jitter
	longname += "Jitter";
	shortname+= "jt";
	i_HairAttr[2] = numAttr.create(longname.c_str(), shortname.c_str(), MFnNumericData::kDouble, 0, &stat);
	numAttr.setSoftMin(0);	
	numAttr.setSoftMax(1);	
	::addAttribute(i_HairAttr[2], atInput);	
	attributeAffects(i_HairAttr[2], i_HairAttr[1]);
	if (!stat) return stat;

	return MS::kSuccess;
}

void HairControlsShape::CreateTextureCacheAttr(
	std::string longName, std::string shortName, MObject& src, MObject& attr, bool bStorableAttributes, MObject i_PaintResXY[2])
{
	MStatus stat;
	MFnTypedAttribute	typedAttr;

	attr = typedAttr.create( longName.c_str(), shortName.c_str(), textureCacheData::id);
	typedAttr.setStorable(false);
	typedAttr.setReadable(true);
	typedAttr.setWritable(true);
	typedAttr.setHidden(true);
	stat = addAttribute(attr);
	if (!stat) 
	{
		displayString("cand create attr %s", longName);
		return;
	}
	attributeAffects(src,				attr);
	attributeAffects(i_PaintResXY[0],	attr);
	attributeAffects(i_PaintResXY[1],	attr);
}

MStatus HairControlsShape::ComputeTextureCache3(const MPlug& plug, MObject attr[3], MObject i_PaintResXY[2], MDataBlock& data)
{
	MStatus stat;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( plug, &stat );
	MPxData* userdata = outputData.asPluginData();
	textureCacheData* pData = static_cast<textureCacheData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( textureCacheData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<textureCacheData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}
	int resX = data.inputValue(i_PaintResXY[0], &stat).asInt();
	int resY = data.inputValue(i_PaintResXY[1], &stat).asInt();

	MPlug src(thisMObject(), attr[0]);
	const char* pname = src.name().asChar();
	displayStringD("HairDeformer::ComputeTextureCache4 for %s", pname);

	MFnNumericAttribute numattr( attr[0]);
	if( numattr.unitType()==MFnNumericData::kDouble)
	{
		double jitter = data.inputValue(attr[2]).asDouble();
//		MString uvsetname = data.inputValue(attr[3]).asString();
		pData->setFloats(src, data, resX, resY, jitter, NULL);
	}
	else
		pData->setVectors(src, data, resX, resY);


	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}
