#pragma once

#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include "Util/Stream.h"
#include "HairCVPositions.h"
//#include "HairGeometry.h"
//#include "HairGeometryIndices.h"

/*/
	USING:
	{
		MFnTypedAttribute fnAttr;
		MObject newAttr = fnAttr.create( 
			"fullName", "briefName", HairCVPositionsData::id );
	}
/*/


class HairCVPositionsData : public MPxData
{
public:
	HairCVPositions cvpos;

	/*/
	bool Init( 
		Math::Vec2i count,
		HairGeometry& hairGeometry, 
		HairGeometryIndices& hairGeometryIndices
		);
	/*/
	bool Init( 
		int cvcount,
		HairGeometryIndices* hairGeometryIndices
		);
	static bool Init( 
		HairCVPositions& cvpos, 
		int cvcount,
		HairGeometryIndices* hairGeometryIndices
		);
	bool Subdiv(
		HairGeometryIndices& hairGeometryIndices, 
		HairCVPositions& cvp
		);

	HairCVPositionsData();
	virtual ~HairCVPositionsData(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

public:
	static MTypeId id;
	static const MString typeName;

protected:
	bool serialize(Util::Stream& stream);
};
