#pragma once
//
#include "pre.h"
#include "DrawCache.h"
#include "HairProcessor.h"
//#include "HairRootPositionsData.h"
#include "MeshSurfaceSynk.h"
#include "HairRootPositionsData.h"
#include "HairGeometryIndicesData.h"
#include "HairGeometryData.h"
#include "HairClumpPositionsData.h"
#include "HairFixedFileData.h"
#include "HairCVPositionsData.h"
#include "HairCVData.h"
#include "HairSystem.h"
#include "HairAliveData.h"

#include "ocellaris/MInstancePxData.h"

class HairDeformerData;
class HairCombDeformer;

enum enPaintOperation
{
	PO_REPLACE = 0,
};

class HairShape : public MPxSurfaceShape
{
public:
	HairShape();
	virtual ~HairShape(); 
	///
	virtual void postConstructor();

	virtual MStatus compute( const MPlug& plug, MDataBlock& data );
//	virtual MStatus	setDependentsDirty( const MPlug &, MPlugArray & );

	static  void* creator();
	static  MStatus initialize();

	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 

	virtual MStatus connectionBroken( 
		const MPlug& plug,
		const MPlug& otherPlug,
		bool asSrc );

	virtual bool getInternalValue( const MPlug& plug, MDataHandle& data);
    virtual bool setInternalValue( const MPlug& plug, const MDataHandle& data);
	MStatus	setDependentsDirty( const MPlug& plug, MPlugArray & affected);

	MStatus shouldSave( const MPlug& plug, bool& result );

public:
	bool isColorize()
	{
		MDataBlock datablock = forceCache();
		MDataHandle handle = datablock.inputValue( i_Colorize );
		return handle.asBool();
	};

	DrawCache* getDrawCache()
	{
		MDataBlock datablock = forceCache();
		MDataHandle handle = datablock.inputValue( o_buildMessage );
		return &geometry;
	};
//	static MStringArray ConnectCVCurves(MObject obj, int segmCount);

public:
	// Output
	static MObject o_buildMessage;
	static MObject i_HairRootPositionFile;
	static MObject i_ocsFrameFile;		// ��������� �������� ��������� ocs ����� ��� ��� �����

	// Geometry
	static MObject i_geometry;			//!< ������� ���������
	static MObject i_uvSetName;			//!< ��� ������������� uvSet�
	static MObject i_HairGeometryIndices;	//!< ���������� ���������� ���������
	static MObject i_HairGeometry;			//!< ��������� ���������
	static MObject i_HairCVPositions;		//!< ��������� ����������� �����
	static MObject i_HairCV;				//!< ����������� ������
	static MObject i_HairClumpPositions;	//!< ��������� ������ �������
//	static MObject i_HairRootPositions;	//!< ��������� ����� (��������� � ����, ��� ���� �������� � ���� i_HairGeometryIndices � i_hairClumpPlacement)
	static MObject i_HairTimeStamp;		//!< ����� ��������� ������ ��� ������� RootPositions
	static MObject i_hairDrawPlacement;	//!< ��������� ����� � ������ i_maxHairDrawable � i_HairDensityArray
	static MObject i_HairAlive;
	static MObject i_HairRenderPasses;	//!< Render passes

	// Hair painting & caching
	static MObject i_PaintAttrResX;
	static MObject i_PaintAttrResY;

	// �����������
	static MObject i_preSubdivSurface;		//!< ����� ��������������� ����� �����������

	// ��������� �����
	static MObject i_generateType;		//!< ��� - ����������/��������������
	static MObject i_HairDensity;		//!< ��������� ��������� (�� �����������!)
		static MObject i_HairDensityUvSet;
		// �������������� ��� ���������
		static MObject i_count;
		static MObject i_Seed;
		// ���������� ��� ���������
		static MObject i_regularDim;
		static MObject i_regularOffset;
		static MObject i_regularShear;
		static MObject i_jitter;

	// Width
	static MObject i_HairRenderWidth;	//!< ������ ��� ���������� � object space
//	static MObject i_HairWidthRamp[5];	// 0-attr, 1-pos 2-val 3-inter 4 cache
	static MObject i_HairBaseWidth[4];	//!< � ��������� 0-1
	static MObject i_HairTipWidth[4];	//!< � ��������� 0-1
	static MObject i_mulWidthByLength;	//!< �������� �������������� �� �����

	// Hair controllers
	static MObject i_CVcountU;
	static MObject i_CVcountV;
//	static MObject i_CVsegmCount;
	static MObject i_CVtailU, i_CVtailV;
	static MObject o_CVTransform;		//!< ��������� ��� ����������� ������
		static MObject o_CVtranslate;
		static MObject o_CVrotate;
		static MObject o_CVscale;
		static MObject o_CVshear;
	static MObject o_CVCurveCreate;	//!< �������� ��� ����������� ������
	static MObject i_CVCurves;			//!< ����������� ������
	static MObject i_CVDynArrays;		//!< ����������� ������ ����� ��������
	static MObject i_CVDisplay;		//!< �������� ����������� ������
	static MObject i_useDynamic;		//!< ������������ ��������

	// CLUMPing
	static MObject i_CLUMPcount;
	static MObject i_bCLUMP;
	static MObject i_CLUMPradius;
	static MObject i_CLUMPalgorithm;
	static MObject i_CLUMPblending;
	static MObject i_CLUMPdensity;
	static MObject i_CLUMPdensityVersion;	// obsolete
	static MObject i_HairBaseClump[4];
	static MObject i_HairTipClump[4];
	static MObject i_clumpPositionFile;		//!< ���� � ������� ��������� �������� ������� �������

	// Deforming
//	static MObject i_time;				//!< �����, ������������ ��� ���������
	static MObject i_orthogonalize;
	static MObject i_SegmCount;
	static MObject i_HairLength[4];
	static MObject i_HairInclination[4];
	static MObject i_HairPolar[4];
	static MObject i_HairBaseCurl[4];
	static MObject i_HairTipCurl[4];
	static MObject i_HairScraggle[4];
	static MObject i_HairScraggleFreq[4];
	static MObject i_HairTwist[4];
	static MObject i_HairDisplace[4];
	static MObject i_HairTwistFromPolar[4];
	static MObject i_blendCVfactor[4];
	static MObject i_blendDeformfactor[4];
//	static MObject i_Deformer[2];		//!< deformerdll + deformerproc
	static MObject i_HairModifiers;		//!< ��������
	static MObject i_HairDeformers;		//!< ��������

	// Hair display
	static MObject i_Colorize;
	static MObject i_colorUvSetName;
	static MObject i_maxHairDrawable;
	static MObject i_drawMode;
	static MObject i_BaseColor, i_BaseColorCache;
	static MObject i_TipColor, i_TipColorCache;
	static MObject i_WorkingFaces;

	// Rendering
	static MObject i_interpolation;
	static MObject i_diceHair;
	static MObject i_sigmaHiding;
	static MObject i_renderDoubleSide;
	static MObject i_renderNormalSource;
	static MObject i_reverseOrientation;
	static MObject i_renderRayTraceEnable, i_renderRayCamera, i_renderRayTrace, i_renderRayPhoton;
	static MObject i_RenderResX;
	static MObject i_RenderResY;
	//static MObject i_MotionBlur
	static MObject i_MotionSamples;
	static MObject i_bUseWorkingFaces;
	static MObject i_splitGroups;
	static MObject i_bRenderBoxes;
	static MObject i_bAccurateBoxes;
	static MObject i_FreezeFile;
	static MObject i_dumpInformation;
	static MObject i_pruningScaleFactor;

	// ��� ��������
	static MObject i_snShape;
	static MObject i_ocellarisShape;
	static MObject i_instanceweights;

	// ������
	static MObject o_positions;			// ������� �����
	static MObject o_normals, o_tangentUs, o_tangentVs;
	static MObject o_hairVector;

	enum enDrawMode
	{
		DM_NONE				= 0,
		DM_HAIRS			= 1,
		DM_CLUMPS			= 2,
		DM_DYNAMIC_CURVES	= 3,
		DM_INSTANCER		= 4,
		DM_INSTANCERBOX		= 5,
		DM_BOX				= 6,
	};
	enDrawMode getDrawMode()
	{
		MDataBlock datablock = forceCache();
		MDataHandle handle = datablock.inputValue( i_drawMode );
		return (enDrawMode)handle.asInt();
	};

	static MTypeId		id;
	static const MString typeName;

protected:
	DrawCache geometry;

public:
	bool bValidBox;
	MBoundingBox box;
	int timeStamp_internal;

	// ���.. �� ���� ��� �� ������� �������...
	bool bRebuildClumpData, bRebuildRootData, bRebuildDrawData, bRebuildCVPositions;

public:
	static void LoadProcessor( MDataBlock& data, HairProcessor& _hairprocessor);
	static bool LoadMeshSurface( MeshSurfaceSynk& meshsurface, MDataBlock& data, MObject thisMObject);

	MPxData* LoadAttr(MDGContext& context, MObject attr, MObject& outval);

	std::string GetHairRootPositionFile(int& timeStamp, bool bAlways=false, bool bDump=false);

	HairGeometryIndices*			LoadHairGeometryIndices(MDGContext& context, MObject& outval);
	HairGeometry*					LoadHairGeometry(MDGContext& context, MObject& outval);
	HairCVPositions*				LoadHairCVPositions(MDGContext& context, MObject& outval);
	HairCV*							LoadHairCV(MDGContext& context, MObject& outval);
	HairClumpPositions*				LoadHairClumpPositions(MDGContext& context, MObject& outval);
	void							LoadHairSystem(MDGContext& context, HairSystem& hairsystem, const char* pass);
	bool							LoadRenderPasses(MDGContext& context, std::vector< HairRenderPass>& passes, const char* pass);
	bool							LoadMeshSurface( MeshSurfaceSynk& meshsurface, MDGContext& context);
	HairAlive*						LoadHairAlive(MDGContext& context, MObject& outval);

	HairGeometryIndices*			LoadHairGeometryIndices(MDataBlock& data, bool forceCompute=false);
	HairGeometry*					LoadHairGeometry(MDataBlock& data);
	HairCVPositions*				LoadHairCVPositions(MDataBlock& data);
	HairCV*							LoadHairCV(MDataBlock& data);
	HairClumpPositions*				LoadHairClumpPositions(MDataBlock& data);
	std::string						LoadHairRootPositions(time_t& timestamp);
	std::string						LoadHairRootPositions(MDataBlock& data, time_t& timestamp);
	HairRootPositions*				LoadHairDrawPlacement(MDataBlock& data);
	void							LoadHairSystem(MDataBlock& data, HairSystem& hairsystem);
	static HairAlive*				LoadHairAlive(MDataBlock& data);
	void							LoadHairWorkingFaces(MDataBlock& data, std::set<int>& workingfases);

	bool							LoadInstanceWeights(MObject i_attribute, MDataBlock& data, std::map< int, HairSystem::weight_t>& snshapes_weight);
	bool							LoadOcellarisShapes(MObject attribute, MDataBlock& data, std::map< int, cls::MInstance>& shapes);

	MStatus ComputeBuildMessage(const MPlug& plug, MDataBlock& data);
	// �������
	MStatus Save(
		Util::Stream& file, 
		Math::Box3f& box, 
		MDGContext& context, 
		HairSystem& hairsystem, 
		cls::IRender* render=NULL);
	// ������� CV � ��������
	MStatus saveCVtoTexture(
		const char* what, 
		const char* filename, 
		int resolution);
	// ������� �������� ���������� � CV
	MStatus exportDeformationToCV(
		MDataBlock& data, 
		const HairCVPositions* dstCVpos, 
		HairCV* dstCV
		);

	// ����������� � ����� (��������� � from)
	bool intersection(
		const Math::Vec3f& from, const Math::Vec3f& direction, 
		Math::Vec3f& closest, Math::Vec3f& closestbary, int& face);
	// �������� �������� (����� ��� ������ �������)
	bool updateTexture(
		const char* streamname,
		enPaintOperation operation, 
		float value,
		const Math::Vec3f& from1, const Math::Vec3f& direction1, 
		const Math::Vec3f& bary1, int face1,
		const Math::Vec3f& from2, const Math::Vec3f& direction2);
	// �������� �������� ������
	bool updatePolarTexture(
		const Math::Vec3f& from1, const Math::Vec3f& direction1, 
		const Math::Vec3f& bary1, int face1,
		const Math::Vec3f& from2, const Math::Vec3f& direction2, 
		Math::Vec3f& polarvector);

	// ������� CombDeformer (������� ���� ����)
	HairCombDeformer* getCombDeformer(HairDeformerData* &pCombDeformerData);
	// �������� ������ CombDeformer (������� ���� ����)
	void setCombDeformer(HairDeformerData* pCombDeformerData, HairCombDeformer* combdeformer);
	// ��������� combDeformer
	HairDeformerData* attachCombDeformer();
	// ����� combDeformer
	HairDeformerData* findCombDeformer();


	// public forceCache
	MDataBlock forceCache( MDGContext& ctx=MDGContext::fsNormal );

	MStatus ComputeHairGeometryIndices(const MPlug& plug, MDataBlock& data);
	MStatus ComputeHairGeometry(const MPlug& plug, MDataBlock& data);
//	MStatus ComputeHairCVPositions(const MPlug& plug, MDataBlock& data);
//	MStatus ComputeHairCV(const MPlug& plug, MDataBlock& data);
	MStatus ComputeHairClumpPositions(const MPlug& plug, MDataBlock& data);
	MStatus ComputeHairTimeStamp(const MPlug& plug, MDataBlock& data);
	MStatus ComputeHairRootPositions(const MPlug& plug, MDataBlock& data);
	MStatus ComputeHairDrawPlacement(const MPlug& plug, MDataBlock& data);
//	MStatus ComputeCVTransform(const MPlug& plug, MDataBlock& data);
	MStatus ComputeOutput( const MPlug& plug, MDataBlock& data, const char* type);
	MStatus ComputeOcsFrameFile( const MPlug& plug, MDataBlock& data);

	static void AffectByMain(const MObject& isAffected);
	static void AffectHairGeometryIndices(const MObject& isAffected);
	static void AffectHairGeometry(const MObject& isAffected);
	static void AffectHairCVPositions(const MObject& isAffected);
	static void AffectHairCVTransform(const MObject& isAffected);
	static void AffectHairCV(const MObject& isAffected);
	static void AffectHairClumpPositions(const MObject& isAffected);
	static void AffectHairRootPositions(const MObject& isAffected);
	static void AffectHairDrawPlacement(const MObject& isAffected);
	static void AffectHairBuildMessage(const MObject& isAffected);

public:
	static void CreateArrayAttrib(const char* longName, const char* shortName, int size, float val, MObject& attr);
	MStatus ComputeArrayAttrib(const MPlug& plug, MDataBlock& data);

	static void CreateVectorArrayAttrib(const char* longName, const char* shortName, int size, const MVector& val, MObject& attr);
	MStatus ComputeVectorArrayAttrib(const MPlug& plug, MDataBlock& data);

	static void CreateTextureCacheAttr(std::string longName, std::string shortName, MObject& src, MObject& attr, bool bStorableAttributes);
	MStatus ComputeTextureCacheV(const MPlug& dat, const MPlug& src, MDataBlock& data);
	MStatus ComputeTextureCache(MObject attr[3], MDataBlock& data);
	MStatus ComputeTextureCache4(MObject attr[4], MDataBlock& data);

	static MStatus CreateAttrNTextureCache( 
		std::string longname, std::string shortname, 
		MObject* i_HairAttr, double min, double max, double def, bool bStorableAttributes);
	static MStatus CreateAttrNTextureCache4( 
		std::string longname, std::string shortname, 
		MObject* i_HairAttr, double min, double max, double def, bool bStorableAttributes);

	static MStatus CreateRampAttrNTextureCache(
		MObject main[5], std::string fullname, std::string shortname, 
		double min, double max, double def, bool bStorableAttributes);

//	void LoadDetailRangeAttr( MDataBlock& data, MObject i_DetailRange[4], HairSystem::DetailRange& v);
//	static void CreateDetailRangeAttr(const char* name, MObject i_DetailRange[4]);

//	static void CreateRenderPass(const char* passname, const char* passnameshort, MObject i_pass[2]);

	static MStatus attributeAffectsT(MObject whenChanges[3], const MObject & isAffected)
	{
		int N=3;
		for(int i=0; i<N; i++)
		{
			MStatus stat = attributeAffects(whenChanges[i], isAffected);
			if(!stat)
				return stat;
		}
		return MS::kSuccess;
	}
	static MStatus attributeAffectsT4(MObject whenChanges[4], const MObject & isAffected)
	{
		int N=4;
		for(int i=0; i<N; i++)
		{
			MStatus stat = attributeAffects(whenChanges[i], isAffected);
			if(!stat)
				return stat;
		}
		return MS::kSuccess;
	}

public:
	static void correctFileName(std::string& str);

	bool SaveHairPositionToFile(int resolution);
};

inline MDataBlock HairShape::forceCache( MDGContext& ctx)
{
	return MPxSurfaceShape::forceCache( ctx);
}
