#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: HairDeformer_LeafNode.h
//
// Dependency Graph Node: HairDeformer_Leaf

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include "HairDeformer.h"


class HairDeformer_Leaf : public HairDeformer
{
public:
	HairDeformer_Leaf();
	virtual ~HairDeformer_Leaf(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_Deformer[2];	// Example input attribute
	static MObject i_envelope[3];	// ����������� ���������
	static MObject o_output;		// Example output attribute
	static MObject i_PaintResXY[2];

	// Ramp
	static MObject i_sourceWidth;
	static MObject i_rampWidth;

public:
	static MTypeId id;
	static const MString typeName;
};

