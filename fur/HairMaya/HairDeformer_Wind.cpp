#include "HairDeformer_Wind.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>

#include <maya/MGlobal.h>

MObject HairDeformer_Wind::i_Deformer[2];
MObject HairDeformer_Wind::i_envelope[3];
MObject HairDeformer_Wind::o_output;
MObject HairDeformer_Wind::i_PaintResXY[2];

MObject HairDeformer_Wind::i_time;			// Example input attribute
MObject HairDeformer_Wind::i_fftDimention;
MObject HairDeformer_Wind::i_fftPhysicalSize;
MObject HairDeformer_Wind::i_fftFilterSize;
MObject HairDeformer_Wind::i_fftWaveDumper;
MObject HairDeformer_Wind::i_fftWindAngle;
MObject HairDeformer_Wind::i_fftWindSpeed;
MObject HairDeformer_Wind::i_affectTipCurl;
MObject HairDeformer_Wind::i_affectBaseCurl;
MObject HairDeformer_Wind::i_affectInclination;

HairDeformer_Wind::HairDeformer_Wind()
{
}
HairDeformer_Wind::~HairDeformer_Wind()
{
}

MStatus HairDeformer_Wind::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;
	displayStringD("HairDeformer_Wind::compute %s", plug.name().asChar());

	if( plug == this->o_output )
	{
		return ComputeOutput(plug, data, i_Deformer, i_envelope);
	}
	return MS::kUnknownParameter;
}

void* HairDeformer_Wind::creator()
{
	return new HairDeformer_Wind();
}

MStatus HairDeformer_Wind::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnUnitAttribute unitAttr;
	MStatus				stat;

#ifndef SAS
	if( !HairDeformer::initialize("HairFftDeformer", "getDeformer", i_Deformer, i_envelope, i_PaintResXY, o_output))
		return MS::kFailure;
#else
	if( !HairDeformer::initialize("chevelureFftDeformer", "getDeformer", i_Deformer, i_envelope, i_PaintResXY, o_output))
		return MS::kFailure;
#endif
	try
	{
		i_time = numAttr.create( "time", "t", MFnNumericData::kDouble, 0, &stat);
		::addAttribute(i_time, atInput);
		numAttr.setMin(0);
		numAttr.setSoftMax(10);

		i_fftDimention = numAttr.create("fftDimention", "fftDimention", MFnNumericData::kInt, 32, &stat);
		::addAttribute(i_fftDimention, atReadable|atStorable|atCached|atKeyable);

		i_fftPhysicalSize = numAttr.create("fftPhysicalSize", "fftPhysicalSize", MFnNumericData::kDouble, 20, &stat);
		::addAttribute(i_fftPhysicalSize, atReadable|atStorable|atCached);
		numAttr.setMin(1);
		numAttr.setSoftMax(70);
		numAttr.setMax(1000);

		i_fftFilterSize = numAttr.create("fftFilterSize", "fftFilterSize", MFnNumericData::kInt, 32, &stat);
		::addAttribute(i_fftFilterSize, atReadable|atStorable|atCached);
		numAttr.setMin(4);
		numAttr.setMax(512);

		i_fftWaveDumper = numAttr.create("fftWaveDumper", "fftWaveDumper", MFnNumericData::kDouble, 1, &stat);
		::addAttribute(i_fftWaveDumper, atReadable|atStorable|atCached);
		numAttr.setMin(0.f);
		numAttr.setSoftMax(2.1f);
		numAttr.setMax(100.f);

		i_fftWindAngle = numAttr.create("fftWindAngle", "fftWindAngle", MFnNumericData::kDouble, 0, &stat);
		::addAttribute(i_fftWindAngle, atReadable|atStorable|atCached|atKeyable);
		numAttr.setDefault(0.0);
		numAttr.setSoftMin(0.f);
		numAttr.setSoftMax(1);

		i_fftWindSpeed = numAttr.create("fftWindSpeed", "fftWindSpeed", MFnNumericData::kDouble, 5, &stat);
		::addAttribute(i_fftWindSpeed, atReadable|atStorable|atCached);
		numAttr.setMin(0.0001f);
		numAttr.setSoftMax(20.f);
		numAttr.setMax(1000.f);

		i_affectTipCurl = numAttr.create("affectTipCurl", "affectTipCurl", MFnNumericData::kDouble, 1, &stat);
		::addAttribute(i_affectTipCurl, atReadable|atStorable|atCached);
		numAttr.setMin(0.f);
		numAttr.setSoftMax(10.f);

		i_affectBaseCurl = numAttr.create("affectBaseCurl", "affectBaseCurl", MFnNumericData::kDouble, 1, &stat);
		::addAttribute(i_affectBaseCurl, atReadable|atStorable|atCached);
		numAttr.setMin(0.f);
		numAttr.setSoftMax(10.f);

		i_affectInclination = numAttr.create("affectInclination", "affectInclination", MFnNumericData::kDouble, 0, &stat);
		::addAttribute(i_affectInclination, atReadable|atStorable|atCached);
		numAttr.setMin(0.f);
		numAttr.setSoftMax(10.f);

		MObject isAffected = o_output;
		attributeAffects( i_time, isAffected);
		attributeAffects( i_fftDimention, isAffected);
		attributeAffects( i_fftPhysicalSize, isAffected);
		attributeAffects( i_fftFilterSize, isAffected);
		attributeAffects( i_fftWaveDumper, isAffected);
		attributeAffects( i_fftWindAngle, isAffected);
		attributeAffects( i_fftWindSpeed, isAffected);
		attributeAffects( i_affectTipCurl, isAffected);
		attributeAffects( i_affectBaseCurl, isAffected);
		attributeAffects( i_affectInclination, isAffected);

		if( !SourceMelFromResource(MhInstPlugin, "AEHairDeformer_WindTemplate", "MEL", defines))
		{
			displayString("error source AEHairDeformer_WindTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

