//
// Copyright (C) 
// File: HairRenderPassDataCmd.cpp
// MEL Command: HairRenderPassData

#include "HairRenderPassData.h"

#include <maya/MGlobal.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include "textureCacheData.h"
#include <sstream>

bool HairRenderPassData::Init()
{
	return true;
}

HairRenderPassData::HairRenderPassData()
{
}
HairRenderPassData::~HairRenderPassData()
{
}

MTypeId HairRenderPassData::typeId() const
{
	return HairRenderPassData::id;
}

MString HairRenderPassData::name() const
{ 
	return HairRenderPassData::typeName; 
}

void* HairRenderPassData::creator()
{
	return new HairRenderPassData();
}

void HairRenderPassData::copy( const MPxData& other )
{
	const HairRenderPassData* arg = (const HairRenderPassData*)&other;
	this->data = arg->data;
}

MStatus HairRenderPassData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("HairRenderPassData: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus HairRenderPassData::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus HairRenderPassData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	serialize(stream);

	return MS::kSuccess;
}

MStatus HairRenderPassData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

void HairRenderPassData::serialize(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version>=0)
	{
		stream >> data;
	}
}

