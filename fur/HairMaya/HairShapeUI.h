#pragma once

#include "pre.h"

/////////////////////////////////////////////////////////////////////
//
// UI class	- defines the UI part of a shape node
//
class HairShapeUI : public MPxSurfaceShapeUI
{
	// Draw Tokens
	//
	enum enDrawTokens
	{
		kDrawHair=0,
		kDrawControl=1,
		kDrawBox=3,
		kDrawInstance=4,
		kNothing=5,
		kDrawColor		=0x1000, 
		kDrawVertices	=0x2000,
		kDrawFlatShaded	=0x4000, 
		kMask			= 0xF000,
		kLastToken
	};

public:
	HairShapeUI();
	virtual ~HairShapeUI(); 

	virtual void	getDrawRequests( const MDrawInfo & info,
									 bool objectAndActiveOnly,
									 MDrawRequestQueue & requests );
	virtual void	draw( const MDrawRequest & request,
						  M3dView & view ) const;
	virtual bool	select( MSelectInfo &selectInfo,
							MSelectionList &selectionList,
							MPointArray &worldSpaceSelectPts ) const;

	static  void *  creator();


private:
	// ������ true ���� ���� ������������ ����� ���� 
	bool setRequestColor( 
		MDrawRequest&,
		const MDrawInfo&, 
		int& token
		);

	void drawVertices( 
		const MDrawRequest & request,
		M3dView & view ) const;

	void drawBox() const;

	bool selectVertices( MSelectInfo &selectInfo,
								MSelectionList &selectionList,
								MPointArray &worldSpaceSelectPts ) const;

};
