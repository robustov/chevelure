#include "stdafx.h"
#include "ocHairShader.h"
#include "ocellaris/OcellarisExport.h"
#include "ocellaris/shaderAssemblerImpl.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnStringData.h>
#include <maya/MFnStringArrayData.h>

#include <maya/MGlobal.h>

// �����
MObject ocHairShader::RootColor;
MObject ocHairShader::RootOpacity;
MObject ocHairShader::TipColor;
MObject ocHairShader::TipOpacity;
MObject ocHairShader::SpecColor;
MObject ocHairShader::StaticAmbient;

// in clump Attenuation
MObject ocHairShader::inClumpAttenuation;
MObject ocHairShader::DepthAttenuation;

// from surface normal
MObject ocHairShader::SurfaceKd;

// Kaiya model
MObject ocHairShader::KajiyaKd;
MObject ocHairShader::KajiyaKs;
MObject ocHairShader::KajiyaSpecWitdh;



// ������
MObject ocHairShader::HairDifferenceMin;
MObject ocHairShader::HairDifferenceMax;
MObject ocHairShader::StartSpec;
MObject ocHairShader::EndSpec;
MObject ocHairShader::SpecSizeFade;
MObject ocHairShader::IllumWidth;
MObject ocHairShader::UseSpecularFade;
MObject ocHairShader::SpecularFadeRate; 
MObject ocHairShader::ClumpDarkStrength; 
MObject ocHairShader::ClumpSpecLightening ;
MObject ocHairShader::SPEC1 ;
MObject ocHairShader::SPEC2 ;
MObject ocHairShader::Roughness1 ;
MObject ocHairShader::Roughness2 ;
MObject ocHairShader::ClumpDiffDarkening ;
MObject ocHairShader::BackFadeRate;
MObject ocHairShader::Edginess;
MObject ocHairShader::edgeLightRate;
MObject ocHairShader::FurKd;
MObject ocHairShader::VarFadeEnd;
MObject ocHairShader::VarFadeStart;
MObject ocHairShader::RootReflect;
MObject ocHairShader::TipReflect;
MObject ocHairShader::KrefRoot;
MObject ocHairShader::KrefTip;
MObject ocHairShader::FurKa;
MObject ocHairShader::FurKdBack;
MObject ocHairShader::FurKdEdge;




MObject ocHairShader::i_UVCoord;
MObject ocHairShader::o_color;
MObject ocHairShader::o_alpha;

ocHairShader::ocHairShader()
{
}
ocHairShader::~ocHairShader()
{
}

MStatus ocHairShader::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;

	if( plug == o_color || plug.parent() == o_color )
	{
		float2& uv = data.inputValue( i_UVCoord ).asFloat2();

		MFloatVector resultColor(0, 0, 0);
		resultColor.x = uv[0] - floor( uv[0]);
		resultColor.y = uv[1] - floor( uv[1]);

		MDataHandle outputData = data.outputValue( o_color, &stat );
	    MFloatVector& outColor = outputData.asFloatVector();
	    outColor = resultColor;
	    outputData.setClean();

		return MS::kSuccess;
	}
	return MS::kUnknownParameter;
}

void* ocHairShader::creator()
{
	return new ocHairShader();
}

MStatus ocHairShader::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnEnumAttribute	enumAttr;
	MStatus				stat;
	MFnStringData stringData;

	try
	{
		{
			RootColor = numAttr.createColor( "RootColor", "RootColor");
			numAttr.setDefault(0.0, 0.0, 0.0);
			::addAttribute(RootColor, atInput);
		}
		{
			TipColor = numAttr.createColor( "TipColor", "TipColor");
			numAttr.setDefault(1., 1., 1.);
			::addAttribute(TipColor, atInput);
		}
		{
			RootOpacity = numAttr.createColor( "RootOpacity", "RootOpacity");
			numAttr.setDefault(1.0, 1.0, 1.0);
			::addAttribute(RootOpacity, atInput);
		}
		{
			TipOpacity = numAttr.createColor( "TipOpacity", "TipOpacity");
			numAttr.setDefault(1.0, 1.0, 1.0);
			::addAttribute(TipOpacity, atInput);
		}
		{
			SpecColor = numAttr.createColor( "SpecColor", "SpecColor");
			numAttr.setDefault(1.0, 1.0, 1.0);
			::addAttribute(SpecColor, atInput);
		}
		{
			StaticAmbient = numAttr.createColor( "StaticAmbient", "StaticAmbient");
			numAttr.setDefault(0.0, 0.0, 0.0);
			::addAttribute(StaticAmbient, atInput);
		}


		{
			inClumpAttenuation = numAttr.create("inClumpAttenuation", "inClumpAttenuation", MFnNumericData::kDouble, 1.0);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(2);
			::addAttribute(inClumpAttenuation, atInput);

			DepthAttenuation = numAttr.create("depthAttenuation", "depthAttenuation", MFnNumericData::kDouble, 1.0);
			numAttr.setMin(0.0);
			numAttr.setMax(1);
			::addAttribute(DepthAttenuation, atInput);
		}

		{
			SurfaceKd = numAttr.create("SurfaceKd", "SurfaceKd", MFnNumericData::kDouble, 0.0);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(1);
			::addAttribute(SurfaceKd, atInput);
		}

		{
			KajiyaKd = numAttr.create("KajiyaKd", "KajiyaKd", MFnNumericData::kDouble, 1);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(1);
			::addAttribute(KajiyaKd, atInput);
		}
		{
			KajiyaKs = numAttr.create("KajiyaKs", "KajiyaKs", MFnNumericData::kDouble, 1);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(1);
			::addAttribute(KajiyaKs, atInput);
		}
		{
			KajiyaSpecWitdh = numAttr.create("KajiyaSpecWitdh", "KajiyaSpecWitdh", MFnNumericData::kDouble, 0.01);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(0.1);
			::addAttribute(KajiyaSpecWitdh, atInput);
		}


		// ������
		{
			HairDifferenceMin = numAttr.create("HairDifferenceMin", "HairDifferenceMin", MFnNumericData::kDouble, 0);
			numAttr.setMin(0.0);
			numAttr.setMax(1);
			::addAttribute(HairDifferenceMin, atInput);
		}
		{
			HairDifferenceMax = numAttr.create("HairDifferenceMax", "HairDifferenceMax", MFnNumericData::kDouble, 0.2);
			numAttr.setMin(0.0);
			numAttr.setMax(1);
			::addAttribute(HairDifferenceMax, atInput);
		}

		{
			StartSpec = numAttr.create("StartSpec", "StartSpec", MFnNumericData::kDouble, 0.3);
			numAttr.setMin(0.0);
			numAttr.setMax(1);
			::addAttribute(StartSpec, atInput);
		}
		{
			EndSpec = numAttr.create("EndSpec", "EndSpec", MFnNumericData::kDouble, 0.95);
			numAttr.setMin(0.0);
			numAttr.setMax(1);
			::addAttribute(EndSpec, atInput);
		}
		{
			SpecSizeFade = numAttr.create("SpecSizeFade", "SpecSizeFade", MFnNumericData::kDouble, 0.1);
			numAttr.setMin(0.0);
			numAttr.setMax(1);
			::addAttribute(SpecSizeFade, atInput);
		}
		{
			IllumWidth = numAttr.create("IllumWidth", "IllumWidth", MFnNumericData::kDouble, 180);
			numAttr.setMin(0.0);
			numAttr.setMax(360);
			::addAttribute(IllumWidth, atInput);
		}
		{
			UseSpecularFade = numAttr.create("UseSpecularFade", "UseSpecularFade", MFnNumericData::kBoolean, 0);
			::addAttribute(UseSpecularFade, atInput);
		}
		{
			SpecularFadeRate = numAttr.create("SpecularFadeRate", "SpecularFadeRate", MFnNumericData::kDouble, 5);
			numAttr.setMin(0.0);
			numAttr.setMax(10);
			::addAttribute(SpecularFadeRate, atInput);
		}
		{
			ClumpDarkStrength = numAttr.create("ClumpDarkStrength", "ClumpDarkStrength", MFnNumericData::kDouble, 5);
			numAttr.setMin(0.0);
			numAttr.setMax(10);
			::addAttribute(ClumpDarkStrength, atInput);
		}
		{
			ClumpSpecLightening = numAttr.create("ClumpSpecLightening", "ClumpSpecLightening", MFnNumericData::kDouble, 1);
			numAttr.setMin(0.0);
			numAttr.setMax(10);
			::addAttribute(ClumpSpecLightening, atInput);
		}

		{
			SPEC1 = numAttr.create("SPEC1", "SPEC1", MFnNumericData::kDouble, 0.01);
			numAttr.setMin(0.0);
			numAttr.setMax(1);
			::addAttribute(SPEC1, atInput);
		}
		{
			SPEC2 = numAttr.create("SPEC2", "SPEC2", MFnNumericData::kDouble, 0.0001);
			numAttr.setMin(0.0);
			numAttr.setMax(1);
			::addAttribute(SPEC2, atInput);
		}
		{
			Roughness1 = numAttr.create("Roughness1", "Roughness1", MFnNumericData::kDouble, 0.008);
			numAttr.setMin(0.0);
			numAttr.setMax(1);
			::addAttribute(Roughness1, atInput);
		}
		{
			Roughness2 = numAttr.create("Roughness2", "Roughness2", MFnNumericData::kDouble, 0.016);
			numAttr.setMin(0.0);
			numAttr.setMax(1);
			::addAttribute(Roughness2, atInput);
		}

		{
			ClumpDiffDarkening = numAttr.create("ClumpDiffDarkening", "ClumpDiffDarkening", MFnNumericData::kDouble, 0.0);
			numAttr.setMin(0.0);
			numAttr.setMax(10);
			::addAttribute(ClumpDiffDarkening, atInput);
		}
		{
			BackFadeRate = numAttr.create("BackFadeRate", "BackFadeRate", MFnNumericData::kDouble, 1.0);
			numAttr.setMin(0.0);
			numAttr.setMax(10);
			::addAttribute(BackFadeRate, atInput);
		}

		{
			Edginess = numAttr.create("Edginess", "Edginess", MFnNumericData::kDouble, 1.0);
			numAttr.setMin(0.0);
			numAttr.setMax(10);
			::addAttribute(Edginess, atInput);
		}
		{
			edgeLightRate = numAttr.create("edgeLightRate", "edgeLightRate", MFnNumericData::kDouble, 0.0);
			numAttr.setMin(0.0);
			numAttr.setMax(10);
			::addAttribute(edgeLightRate, atInput);
		}
		{
			FurKd = numAttr.create("FurKd", "FurKd", MFnNumericData::kDouble, 0.6);
			numAttr.setMin(0.0);
			numAttr.setMax(1);
			::addAttribute(FurKd, atInput);
		}

		{
			VarFadeStart = numAttr.create("VarFadeStart", "VarFadeStart", MFnNumericData::kDouble, .005);
			numAttr.setMin(0.0);
			numAttr.setMax(1);
			::addAttribute(VarFadeStart, atInput);
		}
		{
			VarFadeEnd = numAttr.create("VarFadeEnd", "VarFadeEnd", MFnNumericData::kDouble, .001);
			numAttr.setMin(0.0);
			numAttr.setMax(1);
			::addAttribute(VarFadeEnd, atInput);
		}

		{
			KrefRoot = numAttr.create("KrefRoot", "KrefRoot", MFnNumericData::kDouble, .0001);
			numAttr.setMin(0.0);
			numAttr.setMax(1);
			::addAttribute(KrefRoot, atInput);
		}
		{
			KrefTip = numAttr.create("KrefTip", "KrefTip", MFnNumericData::kDouble, .0001);
			numAttr.setMin(0.0);
			numAttr.setMax(1);
			::addAttribute(KrefTip, atInput);
		}
		{
			RootReflect = numAttr.createColor( "RootReflect", "RootReflect");
			numAttr.setDefault(0.5, 0.5, 0.5);
			::addAttribute(RootReflect, atInput);
		}
		{
			TipReflect = numAttr.createColor( "TipReflect", "TipReflect");
			numAttr.setDefault(0.5, 0.5, 0.5);
			::addAttribute(TipReflect, atInput);
		}

		{
			FurKa = numAttr.create("FurKa", "FurKa", MFnNumericData::kDouble, 1.);
			numAttr.setMin(0.0);
			numAttr.setMax(1);
			::addAttribute(FurKa, atInput);
		}
		{
			FurKdBack = numAttr.create("FurKdBack", "FurKdBack", MFnNumericData::kDouble, 0.);
			numAttr.setMin(0.0);
			numAttr.setMax(1);
			::addAttribute(FurKdBack, atInput);
		}
		{
			FurKdEdge = numAttr.create("FurKdEdge", "FurKdEdge", MFnNumericData::kDouble, 0.);
			numAttr.setMin(0.0);
			numAttr.setMax(1);
			::addAttribute(FurKdEdge, atInput);
		}


		

		{
			MObject child1 = numAttr.create( "uCoord", "u", MFnNumericData::kFloat);
			MObject child2 = numAttr.create( "vCoord", "v", MFnNumericData::kFloat);
			i_UVCoord = numAttr.create( "uvCoord", "uv", child1, child2);
			::addAttribute(i_UVCoord, atInput|atHidden);
		}
		{
			o_color = numAttr.createColor( "outColor", "oc");
			numAttr.setDisconnectBehavior(MFnAttribute::kReset);
			numAttr.setUsedAsColor(false);
			::addAttribute(o_color, atReadable|atUnWritable|atConnectable|atUnStorable|atUnCached);

			stat = attributeAffects( i_UVCoord, o_color );
		}
		{
			o_alpha = numAttr.create( "outAlpha", "oa", MFnNumericData::kDouble, 0);
			::addAttribute(o_alpha, atReadable|atUnWritable|atConnectable|atUnStorable|atUnCached);

			stat = attributeAffects( i_UVCoord, o_alpha);
		}
		if( !SourceMelFromResource(MhInstPlugin, "AEocHairShaderTemplate.mel", "MEL", defines))
		{
			displayString("error source AEocHairShaderTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

ocHairShader::Generator ocHairShader::generator;

// ��� ����
cls::IShadingNodeGenerator::enShadingNodeType ocHairShader::Generator::getShadingNodeType(
	MObject& node					//!< ������ (shader)
	)
{
	return cls::IShadingNodeGenerator::SURFACESHADER;
}

#define output(STRING) (as->Add(STRING))
//! ��������� ��������� 
//! ��������� ��������� ��������� ��� �������
bool ocHairShader::Generator::BuildShader(
	MObject& node,					//!< ������
	cls::enParamType wantedoutputtype, //!< ����� ��� ����� �� ������
	cls::IShaderAssembler* as,		//!< render
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	as->Include("#include 'hairShaderUtils.h'");


	as->ExternParameter("SN", cls::P<Math::Vec3f>(Math::Vec3f(0, 1, 0)), cls::PT_NORMAL);
	as->ExternParameter("clump_radius", cls::P<float>(0), cls::PT_FLOAT);
	as->ExternParameter("clump_normal", cls::P<Math::Vec3f>(Math::Vec3f(0, 1, 0)), cls::PT_VECTOR);
	as->ExternParameter("clump_vector", cls::P<Math::Vec3f>(Math::Vec3f(0, 0, 1)), cls::PT_VECTOR);
	as->ExternParameter("hairId", cls::P<float>(0), cls::PT_FLOAT);
	as->ExternParameter("clumpId", cls::P<float>(0), cls::PT_FLOAT);
	as->ExternParameter("P_nonoise", cls::P<Math::Vec3f>(Math::Vec3f(0, 0, 0)), cls::PT_POINT);
	as->ExternParameter("hairlengthscale", cls::P<float>(0), cls::PT_FLOAT);

	as->InputParameter( ocHairShader::RootColor, cls::PT_COLOR);
	as->InputParameter( ocHairShader::RootOpacity, cls::PT_COLOR);
	as->InputParameter( ocHairShader::TipColor, cls::PT_COLOR);
	as->InputParameter( ocHairShader::TipOpacity, cls::PT_COLOR);
	as->InputParameter( ocHairShader::SpecColor, cls::PT_COLOR);
	as->InputParameter( ocHairShader::StaticAmbient, cls::PT_COLOR);

	as->InputParameter( ocHairShader::inClumpAttenuation, cls::PT_FLOAT);
	as->InputParameter( ocHairShader::DepthAttenuation, cls::PT_FLOAT);

	as->InputParameter( ocHairShader::SurfaceKd, cls::PT_FLOAT);

	as->InputParameter( ocHairShader::KajiyaKd, cls::PT_FLOAT);
	as->InputParameter( ocHairShader::KajiyaKs, cls::PT_FLOAT);
	as->InputParameter( ocHairShader::KajiyaSpecWitdh, cls::PT_FLOAT);

	output ("float DepthAttenuation = 1 - depthAttenuation + v*hairlengthscale*depthAttenuation;			");
	output ("color hairColor   = color mix(RootColor, TipColor, v);											");
	output ("color hairOpacity = color mix(RootOpacity, TipOpacity, v);										");
	output ("hairShaderUtils_shadeHair(																		");
	output ("	P_nonoise, SN, clump_radius, clump_normal, clump_vector, hairId, clumpId,					");
	output ("	hairColor, hairOpacity,	SpecColor, StaticAmbient,											");
	output ("	inClumpAttenuation,	DepthAttenuation, 														");
	output ("	SurfaceKd,																					");
	output ("	KajiyaKd, KajiyaKs, KajiyaSpecWitdh, Oi, Ci);												");

/*/
	output ("extern normal SN;");
	output ("extern vector clump_normal;");
	output ("extern vector clump_vector;"	);
	output ("extern float hairId;");

	
	output ("uniform	float Swap = 0;");
	output ("uniform	float SwapUV = 0;");
	output ("uniform float hair_col_var=1;");
	output ("vector clump_vect=clump_normal;");
	output ("float clump_val=length(clump_vector);"	   );
	output ("float Vres = v;");
	output ("vector DPdx = dPdv;");
	output ("vector T = normalize (DPdx);");
	output ("vector V = -normalize(I);");
	output ("color Cspec = 0, Cdiff = 0, CdiffBack = 0, CdiffEdge = 0;");
	as->InputParameter( "FurKs", "FurKs", cls::PT_FLOAT);
	output ("float Kspec = FurKs;");
	output ("vector nL;");
	output ("varying normal nSN = normalize(SN);");
	output ("vector S = nSN^T;");
	output ("vector N_hair = (T^S);");
	output ("vector norm_hair;");
	output ("float  l = clamp(nSN.T,0,1);");
	output ("float clump_darkening = 1.0;");
	output ("float T_Dot_nL = 0;");
	output ("float T_Dot_e = 0;");
	output ("float SpecDiffFade = 0;");
	output ("vector T_Cross_nL = 0;");
	output ("vector T_Cross_e = 0;");
	output ("float Alpha = 0;");
	output ("float Beta = 0;");
	output ("float Gamma = 0;");
	output ("float Kajiya = 0;");
	output ("float darkening = 1.0;");
	output ("varying color final_c;");
	output ("varying color final_spec;");
	output ("uniform float nonspecular = 0;");
	output ("norm_hair = (l * nSN) + ((1-l) * N_hair);");
	output ("norm_hair = normalize(norm_hair);");
	output ("darkening = float cellnoise(hairId);");
	as->InputParameter( "HairDifferenceMin", "HairDifferenceMin", cls::PT_FLOAT);
	as->InputParameter( "HairDifferenceMax", "HairDifferenceMax", cls::PT_FLOAT);
	output ("darkening = HairDifferenceMin + (HairDifferenceMax - HairDifferenceMin)*darkening;"		);

	as->InputParameter( "StartSpec", "StartSpec", cls::PT_FLOAT);
	as->InputParameter( "EndSpec", "EndSpec", cls::PT_FLOAT);
	as->InputParameter( "SpecSizeFade", "SpecSizeFade", cls::PT_FLOAT);
	output ("Kspec *= min(smoothstep(StartSpec, StartSpec + SpecSizeFade, Vres), 1 - smoothstep(EndSpec, EndSpec - SpecSizeFade, Vres));");
	as->InputParameter( "IllumWidth", "IllumWidth", cls::PT_FLOAT);
	output ("illuminance (P, norm_hair, radians(IllumWidth))");
	output ("{");
	output ("nL = normalize(L);");
	output ("T_Cross_nL =T^nL;");
	output ("T_Cross_e = T^V;");
	output ("T_Dot_nL = T.nL;");
	output ("T_Dot_e = T.(-V);");
	output ("Gamma = acos(T_Cross_nL.T_Cross_e);");
	as->InputParameter( "UseSpecularFade", "UseSpecularFade", cls::PT_BOOL);
	as->InputParameter( "SpecularFadeRate", "SpecularFadeRate", cls::PT_FLOAT);
	output ("if (UseSpecularFade) SpecDiffFade = 1-pow((cos(PI-Gamma)+1)/2, SpecularFadeRate);");
	output ("else SpecDiffFade = 1;");
	output ("Alpha = acos(T_Dot_nL);");
	output ("Beta = acos(T_Dot_e);");
	output ("Kajiya = (T_Dot_nL * T_Dot_e + sin(Alpha) * sin(Beta))* SpecDiffFade;"			);
	as->InputParameter( "ClumpDarkStrength", "ClumpDarkStrength", cls::PT_FLOAT);
	output ("if (ClumpDarkStrength > 0.0) clump_darkening = 1 - (clump_val * ClumpDarkStrength * abs(clamp(nL.normalize(-1*clump_vect), -1, 0)));");
	output ("else clump_darkening = 1.0;");
	as->InputParameter( "ClumpSpecLightening", "ClumpSpecLightening", cls::PT_FLOAT);

	as->InputParameter( "SPEC1", "SPEC1", cls::PT_FLOAT);
	as->InputParameter( "SPEC2", "SPEC2", cls::PT_FLOAT);
	as->InputParameter( "Roughness1", "Roughness1", cls::PT_FLOAT);
	as->InputParameter( "Roughness2", "Roughness2", cls::PT_FLOAT);

	output ("Cspec += SpecColor * (1+ClumpSpecLightening * clump_val)* ((SPEC1*Cl*pow(Kajiya, 1/Roughness1))+ (SPEC2*Cl*pow(Kajiya, 1/Roughness2)));");
	as->InputParameter( "ClumpDiffDarkening", "ClumpDiffDarkening", cls::PT_FLOAT);

	output ("Cdiff += (1-ClumpDiffDarkening*clump_val)*clump_darkening*hairShaderUtils_diffuselgt(Cl, vector(L), normal(norm_hair), 1);");
	as->InputParameter( ocHairShader::BackFadeRate);
	output ("CdiffBack += (1-ClumpDiffDarkening*clump_val)*clump_darkening*hairShaderUtils_diffuselgt(Cl, vector(L), normal(-norm_hair), BackFadeRate);");

	as->InputParameter( ocHairShader::Edginess);
	as->InputParameter( ocHairShader::edgeLightRate);
	output ("CdiffEdge += (1-ClumpDiffDarkening*clump_val)*clump_darkening*hairShaderUtils_diffuseEdgelgt(Cl, vector(L), normal(norm_hair), Edginess, edgeLightRate);");
	output ("}");
	as->InputParameter( ocHairShader::FurKd);

	output ("float lumin = comp(FurKd*Cdiff,0)*0.299 + comp(FurKd*Cdiff,1)*0.587 + comp(FurKd*Cdiff,2)*0.114;");
	as->InputParameter( ocHairShader::VarFadeEnd);
	as->InputParameter( ocHairShader::VarFadeStart);
	
	output ("darkening = 1 - (smoothstep(VarFadeEnd, VarFadeStart, abs(lumin))*darkening);");
	as->InputParameter( ocHairShader::TipOpacity, cls::PT_COLOR);
	as->InputParameter( ocHairShader::RootOpacity, cls::PT_COLOR);

	output ("Oi = color mix(RootOpacity, TipOpacity, Vres);");
	output ("Oi = clamp(Oi, color 0, color 1);");

	as->InputParameter( ocHairShader::RootColor, cls::PT_COLOR);
	as->InputParameter( ocHairShader::TipColor, cls::PT_COLOR);
	output ("final_c = color mix(RootColor, TipColor, Vres) * darkening;");
	as->InputParameter( ocHairShader::RootReflect, cls::PT_COLOR);
	as->InputParameter( ocHairShader::TipReflect, cls::PT_COLOR);

	as->InputParameter( "KrefRoot", "KrefRoot", cls::PT_FLOAT);
	as->InputParameter( "KrefTip", "KrefTip", cls::PT_FLOAT);

	output ("color reflection = color mix(KrefRoot*RootReflect, KrefTip*TipReflect, Vres);");
	output ("reflection = 0;");
/*/

/*/
	output ("extern color hBaseC;");
	output ("extern color hBaseRootC;");
	output ("extern color hBaseTipC;");
	output ("extern color hBaseRefl;"	);
	output ("extern color hTipRefl;");
	output ("extern color hSpec;");
	output ("extern color hSpecRoot;");
	output ("extern color hSpecTip;");
	output ("extern color hDiff;");
	output ("extern color hbackDiff;");
	output ("extern color hEdgeDiff;");
	output ("extern color hOp;");
	output ("extern color hClumpDark;");
	output ("extern color hDark;"	);

	output ("hDark = darkening*color(1);");
	output ("hBaseRefl = RootReflect*Oi;");
	output ("hTipRefl = TipReflect*Oi;");
	output ("hOp = Oi;");
	output ("hClumpDark = clump_darkening*color(1);");
	output ("hDiff = clamp(Cdiff, color 0, color 1);");
	output ("hbackDiff = clamp(CdiffBack, color 0, color 1);");
	output ("hEdgeDiff = clamp(CdiffEdge, color 0, color 1);"	);
	output ("hBaseC = color mix(RootColor, TipColor, Vres)*Oi;"	);
	output ("hBaseRootC = RootColor*Oi;");
	output ("hBaseTipC = TipColor*Oi;"	);
	output ("hSpec	= Vres * Kspec * Cspec*Oi;"	);
/*/

/*/
	as->InputParameter( "FurKa", cls::PT_FLOAT);
	as->InputParameter( "StaticAmbient", cls::PT_COLOR);
	as->InputParameter( "FurKdBack", cls::PT_FLOAT);
	as->InputParameter( "FurKdEdge", cls::PT_FLOAT);

	output ("Ci = ((FurKa*ambient()*StaticAmbient + FurKd*Cdiff + FurKdBack*CdiffBack) * final_c + ((Vres) * Kspec * Cspec)+ FurKdEdge*CdiffEdge*SpecColor+ reflection);");
	output ("Ci = clamp(Ci, color 0, color 1);");
	output ("Ci = Oi * Ci;");
/*/
	return true;
}


