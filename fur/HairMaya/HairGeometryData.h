#pragma once

#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include "Util/Stream.h"
#include "HairGeometryIndices.h"
#include "HairGeometry.h"

/*/
	USING:
	{
		MFnTypedAttribute fnAttr;
		MObject newAttr = fnAttr.create( 
			"fullName", "briefName", HairGeometryData::id );
	}
/*/


class HairGeometryData : public MPxData
{
public:
	HairGeometry geometry;
	bool Init(
		MObject obj, 
		HairGeometryIndices* geometryIndices, 
		MString uvSetName);
	static bool Init(
		HairGeometry& geometry, 
		MObject obj, 
		HairGeometryIndices* geometryIndices, 
		MString uvSetName);

public:
	HairGeometryData();
	virtual ~HairGeometryData(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

public:
	static MTypeId id;
	static const MString typeName;

protected:
	bool serialize(Util::Stream& stream);
};
