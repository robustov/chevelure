#include "DrawCache.h"
#include "MathNgl/MathNgl.h"
#include "HairPersonage.h"

DrawCache::DrawCache()
{
	hairCount = 0;
}
void DrawCache::ClearBox()
{
	box = MBoundingBox();
	bValidBox = false;

}

// ���������� ��� ������ ������
void DrawCache::SetHair(
	int hairindex, 
	int hairid,
	HAIRPARAMS& params, 
	HAIRVERTS& hair,
	enRenderNormalSource renderNormalSource, 
	float RenderWidth, 
	float widthBias
	)
{
	HairPersonageData hairPersonageData;
	Math::Matrix4f matr;
	matr[0] = Math::Vec4f(params.tangentU,	0);
	matr[1] = Math::Vec4f(params.normal,	0);
	matr[2] = Math::Vec4f(params.tangentV,	0);
	matr[3] = Math::Vec4f(params.position,	1);
	if(false)
	{
		for(int x=0; x<(int)hair.basas.size(); x++)
		{
			Math::Matrix4f& matr = hair.basas[x];
			printf("matrix %d:\n", x);
			printf("%f %f %f %f\n", matr[0][0], matr[0][1], matr[0][2], matr[0][3]);
			printf("%f %f %f %f\n", matr[1][0], matr[1][1], matr[1][2], matr[1][3]);
			printf("%f %f %f %f\n", matr[2][0], matr[2][1], matr[2][2], matr[2][3]);
			printf("%f %f %f %f\n", matr[3][0], matr[3][1], matr[3][2], matr[3][3]);
		}
		fflush(stdout);
	}
	hairPersonageData.transforms	= hair.basas;
	hairPersonageData.parameters	= hair.vfactor;
	hairPersonageData.transform		= matr;
	hairPersonageData.polar			= params.polar;
	hairPersonageData.inclination	= params.inclination;
	hairPersonageData.baseCurl		= params.baseCurl;
	hairPersonageData.tipCurl		= params.tipCurl;
	float scalelen = params.length*params.lengthjitter;

	hairPersonageData.length		= scalelen;
	hairPersonageData.baseWidth		= params.baseWidth;
	hairPersonageData.tipWidth		= params.tipWidth;
	hairPersonageData.twist			= params.twist;
	hairPersonageData.u				= params.u;
	hairPersonageData.v				= params.v;
	hairPersonageData.id			= params.seed;
	hairPersonageData.animation		= params.scraggleFreq;

	this->setSnHair(
		hairindex, params.instance, hairPersonageData);

/*/
	// �������!
	this->sethair(hairindex, hair.vc(), params.u, params.v, hairid, params.position, params.normal, NULL);
//	this->sethairclumpdata(hairindex, hair.clump_normal, hair.clump_vector);
	for(int v=0; v<hair.vc(); v++)
	{
		this->sethairvertex(hairindex, v, hair.cvint[v]);
		Math::Vec3f color(1);
		this->sethaircolor(hairindex, v, color);
	}
/*/
}

/*/
void DrawCache::InitCV(int hairCountCV, int vertexInHairCV)
{
//	this->vertexInHairCV = vertexInHairCV;
	int s = hairCountCV * vertexInHairCV;

	CVverts.clear();
	CVverts.reserve(s*3);
	hairIndicies.reserve(s);

	CVVertexOffset.resize(hairCountCV);
	CVVertexCount.resize(hairCountCV);
	CVroots.resize(hairCountCV);
	CVvisible.resize(hairCountCV);
}
void DrawCache::setCVhair(int cvhair, int vertcount, const Math::Vec3f& rootpt, bool bVisible)
{
	if(cvhair<0 || cvhair>=(int)CVVertexOffset.size()) 
		return;

	CVVertexOffset[cvhair] = (int)CVverts.size();
	CVVertexCount[cvhair]  = vertcount;
	CVroots[cvhair] = rootpt;
	CVvisible[cvhair] = bVisible;
	CVverts.insert(CVverts.end(), vertcount*3, 0);
	hairIndicies.insert(hairIndicies.end(), vertcount, cvhair);

	if( bVisible)
	{
		MVector src(rootpt.x, rootpt.y, rootpt.z);
		box.expand(src); bValidBox = true;
	}

}
void DrawCache::setCVhairvertex(int cvhair, int vertindex, const Math::Vec3f& pt)
{
	if( cvhair<0 || cvhair>=(int)CVVertexCount.size())
		return;
	if( vertindex<0 || vertindex>=(int)CVVertexCount[cvhair])
		return;

	float* dst = &CVverts[0] + CVVertexOffset[cvhair];
	dst += vertindex*3;
	dst[0] = pt.x;
	dst[1] = pt.y;
	dst[2] = pt.z;

	if( CVvisible[cvhair])
	{
		MVector src(pt.x, pt.y, pt.z);
		box.expand(src); bValidBox = true; 
	}
}
/*/




void DrawCache::Init(
	int hairCount, int maxVertexInHair, enHairInterpolation interpolation, bool bNormals, std::vector<const char*>* adduv, 
	int flags
	)
{
//	this->SegmCount = vertexInHair-1;
	int s = hairCount * maxVertexInHair;
	this->Vertices.clear();
	this->Vertices.reserve(s*3);
	this->VerticesInds.clear();
	this->VerticesInds.reserve(s);
	this->Color.clear();
	this->Color.reserve(s*3);
	this->uvs.resize(hairCount);
	this->hairpos.resize(hairCount);

	hairVertexOffset.resize(hairCount);
	vertexInHair.resize(hairCount);
	this->hairCount = 0;
}
void DrawCache::sethair(int hair, int vertcount, float u, float v, int id, const Math::Vec3f& p, const Math::Vec3f& n, Math::Vec2f* adduv)
{
	if(hair<0 || hair>=(int)hairVertexOffset.size()) 
		return;
	this->hairCount = hair+1;

	hairVertexOffset[hair] = (int)Vertices.size();
	vertexInHair[hair] = vertcount;
	this->uvs[hair] = Math::Vec2f(u, v);
	this->hairpos[hair] = p;

	Vertices.insert(Vertices.end(), vertcount*3, 0);
	Color.insert(Color.end(), vertcount*3, 0);

	box.expand(MPoint(p.x, p.y, p.z)); bValidBox = true;
}
void DrawCache::sethairvertex(int cvhair, int vertindex, const Math::Vec3f& pt)
{
	if( cvhair<0 || cvhair>=(int)vertexInHair.size())
		return;
	if( vertindex<0 || vertindex>=(int)vertexInHair[cvhair])
		return;

	float* dst = &Vertices[0] + hairVertexOffset[cvhair];
	dst += vertindex*3;
	dst[0] = pt.x;
	dst[1] = pt.y;
	dst[2] = pt.z;

#ifdef SAS
//printf("%d, %d: %f %f %f\n", cvhair, vertindex, pt.x, pt.y, pt.z);
#endif
	box.expand(MVector(pt.x, pt.y, pt.z)); bValidBox = true;

	if( vertindex!=0)
	{
		int fv = hairVertexOffset[cvhair]/3;
		this->VerticesInds.push_back(Math::Vec2i(fv+vertindex-1, fv+vertindex));
	}
}
void DrawCache::sethaircolor(int cvhair, int vertindex, const Math::Vec3f& pt)
{
	float* dst = &Color[0] + hairVertexOffset[cvhair];
	if( vertindex<0 || vertindex>=vertexInHair[cvhair])
		return;

	dst += vertindex*3;
	dst[0] = pt.x;
	dst[1] = pt.y;
	dst[2] = pt.z;
}



void DrawCache::ClearInstances()
{
	snshapes.clear();
	ocsshapes.clear();
}

// INSTANCE
void DrawCache::InitSn(int hairCount)
{
	instHairs.resize(hairCount);
//	ocsshapes.resize(hairCount);
}
void DrawCache::setSnShape(int index, sn::ShapeShortcutPtr& ptr)
{
	snshapes[index] = ptr;
}
void DrawCache::setOcsShape(int index, cls::MInstance& cache)
{
	ocsshapes[index] = cache;
}

void DrawCache::setSnHair(int cvhair, int instance, HairPersonageData& data)
//, const Math::Vec3f& p, const Math::Vec3f& n, const Math::Vec3f& tangentu, const Math::Vec3f& tangentv,
//	float polar, float inclination, float baseCurl, float tipCurl, float length, 
//	float baseWidth, float tipWidth, float twist, float u, float v)
{
	instHairs[cvhair] = data;
	instHairs[cvhair].instance = instance;
	/*/
	Math::Matrix4f matr;
	matr[0] = Math::Vec4f(tangentu, 0);
	matr[1] = Math::Vec4f(n, 0);
	matr[2] = Math::Vec4f(tangentv, 0);
	matr[3] = Math::Vec4f(p, 1);
	instHairs[cvhair].transform = matr;
	instHairs[cvhair].polar = polar;
	instHairs[cvhair].inclination = inclination;
	instHairs[cvhair].baseCurl = baseCurl;
	instHairs[cvhair].tipCurl = tipCurl;
	instHairs[cvhair].length = length;
	instHairs[cvhair].baseWidth = baseWidth;
	instHairs[cvhair].tipWidth	= tipWidth;
	instHairs[cvhair].twist		= twist;
	instHairs[cvhair].u = u;
	instHairs[cvhair].v = v;
	/*/
	Math::Matrix4f m = data.transform;
	m[0] *= data.length;
	m[1] *= data.length;
	m[2] *= data.length;

	if(false)
	{
		for(int x=0; x<(int)data.transforms.size(); x++)
		{
			Math::Matrix4f& matr = data.transforms[x];
			printf("DrawCache::setSnHair matrix %d:\n", cvhair);
			printf("%f %f %f %f\n", matr[0][0], matr[0][1], matr[0][2], matr[0][3]);
			printf("%f %f %f %f\n", matr[1][0], matr[1][1], matr[1][2], matr[1][3]);
			printf("%f %f %f %f\n", matr[2][0], matr[2][1], matr[2][2], matr[2][3]);
			printf("%f %f %f %f\n", matr[3][0], matr[3][1], matr[3][2], matr[3][3]);
		}
		fflush(stdout);
	}

	{
		if( snshapes.find(instance)!=snshapes.end())
		{
			sn::ShapeShortcutPtr snInst = snshapes[instance];
			Math::Box3f b;
			if( snInst->shaper && 
				snInst->shaper->getDirtyBox( snInst->shape, m, b))
			{
				for( int c=0; c<8; c++)
				{
					Math::Vec3f pt = data.transform * b.corner(c);
					box.expand(MVector(pt.x, pt.y, pt.z)); 
				}
				bValidBox = true;
			}
		}
	}


	{
		if(ocsshapes.find(instance)!=ocsshapes.end())
		{
			cls::MInstance& snInst = ocsshapes[instance];
			if( snInst.isValid())
			{
				Math::Box3f b = snInst.getBox();
				for( int c=0; c<8; c++)
				{
					Math::Vec3f pt = data.transform * b.corner(c);
					box.expand(MVector(pt.x, pt.y, pt.z)); 
				}
				bValidBox = true;
			}
		}
	}
}




int DrawCache::UI_getCVCount() const
{
	return (int)CVverts.size()/3;
}
MPoint DrawCache::UI_getCV(int index) const
{
	if( index>=(int)CVverts.size()/3) 
		return MPoint(0, 0, 0, 1);

	const float* dst = &CVverts[index*3];
	MVector v;
	v.x = dst[0];
	v.y = dst[1];
	v.z = dst[2];
	return MPoint(v);
}
int DrawCache::UI_getHairCount() const
{
	return hairCount;
}
MVector DrawCache::UI_getHair(int index) const
{
	const Math::Vec3f& v = hairpos[index];
	return MVector(v.x, v.y, v.z);
}



void DrawCache::drawHair( bool colorize, bool asPoints) const
{
	if(asPoints)
	{
		// �������� �������
		int ElementsCounter = (int)Vertices.size()/3;

		if( ElementsCounter)
		{
			glEnableClientState(GL_VERTEX_ARRAY);
			glVertexPointer( 3, GL_FLOAT, 0, &this->Vertices[0]);

			if(colorize)
			{
				glEnableClientState(GL_COLOR_ARRAY);
				glColorPointer( 3, GL_FLOAT, 0, &this->Color[0]);
			}
			glDrawArrays(GL_POINTS, 0, ElementsCounter);

			if(colorize)
				glDisableClientState (GL_COLOR_ARRAY);
			glDisableClientState (GL_VERTEX_ARRAY);
		}
		return;
	}
	else
	{
		// �������
		int ElementsCounter = hairCount;

		if( ElementsCounter)
		{
			glEnableClientState(GL_VERTEX_ARRAY);
			glVertexPointer( 3, GL_FLOAT, 0, &this->Vertices[0]);
			if(colorize)
			{
				glEnableClientState(GL_COLOR_ARRAY);
				glColorPointer( 3, GL_FLOAT, 0, &this->Color[0]);
			}

			int lastSM;
			glGetIntegerv(GL_SHADE_MODEL, &lastSM);
			glShadeModel(GL_SMOOTH);
			glDrawElements(GL_LINES, VerticesInds.size()*2, GL_UNSIGNED_INT, &VerticesInds[0]);

			glShadeModel(lastSM);
			if(colorize)
			{
				glDisableClientState (GL_COLOR_ARRAY);
				glColorPointer( 0, GL_FLOAT, 0, 0);
			}
			glDisableClientState (GL_VERTEX_ARRAY);
		}
	}
}

void DrawCache::drawInstance() const
{
	HairPersonage personage;
	for(unsigned i=0; i<instHairs.size(); ++i)
	{
		const HairPersonageData& snhair = instHairs[i];
		personage.data = snhair;

		Math::Matrix4f m = snhair.transform;
		m[0] *= snhair.length;
		m[1] *= snhair.length;
		m[2] *= snhair.length;

//		glPushMatrix();
//		float* d = m.data();
//		glMultMatrixf(d);


		if( snshapes.find(snhair.instance)!=snshapes.end())
		{
			sn::ShapeShortcutPtr snInst = this->snshapes[snhair.instance];

			if( snInst->shaper)
			{
				Math::Box3f box;
				snInst->shaper->getDirtyBox( snInst->shape, m, box);
				if( bDrawAsBox)
					drawBox(box);
				else
					snInst->shaper->renderGL(snInst->shape, &personage, m);
			}
			else
			{
				fprintf(stderr, "DrawCache::drawHair snInst->shaper = NULL\n");
				fflush(stderr);
			}
		}



		if(ocsshapes.find(snhair.instance)!=ocsshapes.end())
		{
			const cls::MInstance& cache = this->ocsshapes[snhair.instance];
			if( cache.isValid())
			{
				Math::Box3f box = cache.getBox();
				float maxDot = box.max.y;

				// scale �� maxDot
				float boxScale = 1/maxDot;
				if( maxDot<0.00001)
					boxScale = 1;
				Math::Matrix4f Tscale = Math::Matrix4f::id;
				Tscale.scale(Math::Vec4f(boxScale, boxScale, boxScale, 1));

//				m[0] *= boxScale;
//				m[1] *= boxScale;
//				m[2] *= boxScale;
				maxDot = 1;

				if( bDrawAsBox)
				{
					glPushMatrix();

					const float* d = m.data();
					glMultMatrixf(d);
					drawBox(box);
					glPopMatrix(); 
				}
				else
				{
					glrender.clear();
//					glrender.bPoints = 0;
//					glrender.bWireFrame = 1;
//					glrender.bShaded = 0;
					glrender.degeneration = 1;

					cls::IRender* render = &glrender;
		//			render->clear();

					render->PushTransform();
					render->PushAttributes();
//					m[0] = -m[0];
					render->AppendTransform(m);
					render->PopAttributes();
					
					cls::PA<Math::Matrix4f> transforms(cls::PI_CONSTANT, snhair.transforms);
					cls::PA<float> parameters(cls::PI_CONSTANT, snhair.parameters);
					
					render->Parameter("HairFilter::u", snhair.u);
					render->Parameter("HairFilter::v", snhair.v);
					render->Parameter("HairFilter::id", snhair.id);

					render->Parameter("HairFilter::transforms", transforms);
					render->Parameter("HairFilter::parameters", parameters);
					render->Parameter("HairFilter::maxDot", maxDot);
					render->Filter("HairFilter");

					render->Attribute("global::phase", snhair.animation);

					render->PushTransform();
					render->PushAttributes();
					render->AppendTransform(Tscale);
					render->PopAttributes();
					{
						cache.Render(render);
					}
					render->PopTransform();

					render->PopTransform();
				}
			}
		}
	}

}
void DrawCache::testOcsRender(MObject obj, M3dView* view) const
{
	std::map< int, cls::MInstance>::iterator it = ocsshapes.begin();
	for(;it != ocsshapes.end(); it++)
	{
		cls::MInstance& inst = it->second;
//		inst.RenderToMaya(obj, view, false, false, true);
		cls::IRender* render = &glrender;
		inst.Render(render);
	}
}

void DrawCache::drawHairText( M3dView &view) const
{
	if(true)
	{
		// N
		for(int v=0; v<this->hairCount; v++)
		{
			char text[128]; sprintf(text, "%d", v);
			MPoint pt(this->hairpos[v].x, this->hairpos[v].y, this->hairpos[v].z);
			view.drawText(text, pt);
		}
	}
	if(false)
	{
		// uv
		for(int v=0; v<this->hairCount; v++)
		{
			char text[128]; sprintf(text, "%g, %g", this->uvs[v].x, this->uvs[v].y);
			MPoint pt(this->hairpos[v].x, this->hairpos[v].y, this->hairpos[v].z);
			view.drawText(text, pt);
		}
	}
}
void DrawCache::drawCV(int index) const
{
	if( index>=(int)CVverts.size()/3) return;

	int cvhair = hairIndicies[index];
	if(!CVvisible[cvhair])
		return;

	MPoint vertex = UI_getCV(index);
	vertex.homogenize();

	glBegin( GL_POINTS );
	glVertex3f( (float)vertex[0], 
				(float)vertex[1], 
				(float)vertex[2] );
	glEnd();
//displayPoint(vertex);
}

void DrawCache::drawCVs() const
{
	{
		float lastPointSize;
		glGetFloatv( GL_POINT_SIZE, &lastPointSize );
		glPointSize( 4.0 );

		if( !CVverts.empty())
		{
			for( int h=0; h<(int)CVroots.size(); h++)
			{
				if(!CVvisible[h])
					continue;
				glBegin( GL_POINTS );
				glVertex3f( CVroots[h].x, 
							CVroots[h].y, 
							CVroots[h].z );
				glEnd();
			}
		}

		int ElementsCounter = (int)CVverts.size()/3;

		if( ElementsCounter)
		{
			glEnableClientState(GL_VERTEX_ARRAY);
			glVertexPointer( 3, GL_FLOAT, 0, &this->CVverts[0]);
			glDrawArrays(GL_POINTS, 0, ElementsCounter);
			glDisableClientState(GL_VERTEX_ARRAY);
			glPointSize( lastPointSize );
		}
	}

	if(true)
	{
		if( !CVverts.empty())
		{
			for( int h1=0; h1<(int)CVVertexCount.size(); h1++)
			{
				int start = CVVertexOffset[h1]/3;
				MPoint vertex = UI_getCV(start);
				vertex.homogenize();

				if(!CVvisible[h1])
					continue;

				glBegin( GL_LINES );
				glVertex3f( CVroots[h1].x, CVroots[h1].y, CVroots[h1].z );
				glVertex3f( (float)vertex[0], (float)vertex[1], (float)vertex[2] );
				glEnd();
			}
		}

		if( !this->CVverts.empty())
		{
			glEnableClientState(GL_VERTEX_ARRAY);
			glVertexPointer( 3, GL_FLOAT, 0, &this->CVverts[0]);
			for( int h=0; h<(int)CVVertexCount.size(); h++)
			{
				if(!CVvisible[h])
					continue;
				int start = CVVertexOffset[h]/3;
				int count  = CVVertexCount[h];
				glDrawArrays(GL_LINE_STRIP, start, count);
			}
			glDisableClientState(GL_VERTEX_ARRAY);
		}
	}
}


