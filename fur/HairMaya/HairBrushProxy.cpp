#include "pre.h"

#include "HairBrushProxy.h"

#include <maya/MFnTransform.h>

void*	HairBrushProxy::creator(  )
{
	return new HairBrushProxy;
}

MStatus	HairBrushProxy::initialize()
{
	return MStatus::kSuccess;
}

		HairBrushProxy::HairBrushProxy()
{
}

		HairBrushProxy::~HairBrushProxy()
{
}

void	HairBrushProxy::draw(	M3dView& view,
								const MDagPath& path, 
								M3dView::DisplayStyle style,
								M3dView::DisplayStatus status )
{
	if ( view.window() != M3dView::active3dView().window() )
		return;
	
	int xc = 0;
	int yc = 0;
	int r = 0;
	MFnTransform transform( path );
	MPlug xPlug = transform.findPlug( "translateX" );
	MPlug yPlug = transform.findPlug( "translateY" );
	MPlug zPlug = transform.findPlug( "translateZ" );
	xPlug.getValue(xc);
	yPlug.getValue(yc);
	zPlug.getValue(r);
	
	if( xc < 0
	||	yc < 0
	||	xc > view.portWidth()
	||	yc > view.portHeight() )
	{
		return;
	}

	view.beginGL();
	
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
		glLoadIdentity();
		
		gluOrtho2D( 0, view.portWidth(), 0, view.portHeight() );
		
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		
		glPushAttrib( GL_ALL_ATTRIB_BITS );
			glColor3d( 1.0, 0.0, 0.0 );
			glPointSize( 1.0f );
			int y = 0;
			int d = 3 - 2 * y;
			int x = 0;
			y = r;
			while (x <= y)
			{
				glBegin(GL_POINTS);
					glVertex2i( x + xc,  y + yc);
					glVertex2i( x + xc, -y + yc);
					glVertex2i(-x + xc, -y + yc);
					glVertex2i(-x + xc,  y + yc);
					glVertex2i( y + xc,  x + yc);
					glVertex2i( y + xc, -x + yc);
					glVertex2i(-y + xc, -x + yc);
					glVertex2i(-y + xc,  x + yc);
				glEnd();
				if ( d < 0 )
					d = d + 4 * x + 6;
				else
				{
					d = d + 4 * ( x - y ) + 10;
					y--;
				}
				x++;
			}
		glPopAttrib();
		
	glPopMatrix();
	view.endGL();
}

bool	HairBrushProxy::isBounded() const
{
	return false;
}

bool	HairBrushProxy::drawLast() const
{
	return true;
}
