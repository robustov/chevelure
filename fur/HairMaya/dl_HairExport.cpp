#include "dl_HairExport.h"
#include "HairShape.h"
#include "HairRenderPassNode.h"
#include <maya/MFnVectorArrayData.h>
#include "Util/FileStream.h"
#include "Util/misc_create_directory.h"
#include "Util/currentHostName.h"
#include "MathNMaya/MathNMaya.h"

bool HairExport(
	const char* argsl0,
	const char* filename,
	const char* pass,
	const float* camera, 
	int useMotionBlur,
	double frametime, 
	double shutterOpenSec, 
	double shutterCloseSec,
	double shutterOpen, 
	double shutterClose, 
	float outbox[6]
	)
{
	MTime mTime = MAnimControl::currentTime();
	shutterOpen = MTime(shutterOpen, MTime::uiUnit()).as(MTime::uiUnit());
	shutterClose = MTime(shutterClose, MTime::uiUnit()).as(MTime::uiUnit());

//	04166
	displayString("HairExport \"%s\" %s file=\"%s\" pass=\"%s\"\n frametime=%f open=%f(%f) close=%f(%f) mayaTime=%f!", 
		argsl0, useMotionBlur?"BLUR":"NO BLUR", filename, pass, 
		frametime, shutterOpen, shutterOpenSec, shutterClose, shutterCloseSec, 
		mTime.as(MTime::uiUnit()));
	Math::Matrix4f mview(camera);
//	displayMatrix(mview);
	MStatus stat;
	MString	arg;

	outbox[0] = 0.0;
	outbox[1] = 0.0;
	outbox[2] = 0.0;
	outbox[3] = 0.0;
	outbox[4] = 0.0;
	outbox[5] = 0.0;


	MObject obj;
	nodeFromName(argsl0, obj);
	MFnDependencyNode dn(obj);

	MDagPath path;
	MDagPath::getAPathTo(obj, path);
	Math::Matrix4f thisTransform;
	::copy( thisTransform, path.inclusiveMatrix());


	if(true)
	{
		HairShape* shape = NULL;
		bool bStandAlonePass = false;
		HairRenderPass standAlonePass;

		MTypeId tid = dn.typeId();
		if( tid == HairShape::id)
		{
			shape = (HairShape*)dn.userNode();
		}
		else if(obj.hasFn(MFn::kLocator))
		{
			MPlug referenceObjectPlug = dn.findPlug("referenceObject");
			MPlugArray pa;
			referenceObjectPlug.connectedTo(pa, true, false);
			if(pa.length()!=1) 
			{
				displayString("HairExport cant find referenceObject");
				return false;
			}
			
			obj = pa[0].node();
			dn.setObject(obj);
			if( dn.typeId() != HairRenderPassNode::id)
			{
				displayString("HairExport cant find referenceObject");
				return false;
			}

			// ��������� mode
			/*/
			short mode=0;
			MPlug(obj, HairRenderPassNode::i_mode).getValue(mode);
			if( mode==0) return MS::kSuccess;	// mode = RENDER WITH HairShape
			/*/
			// Enable
			bool bEnable=0;
			MPlug(obj, HairRenderPassNode::i_enable).getValue(bEnable);
			if( !bEnable) return false;	// disable

			HairRenderPassNode* renderPass = (HairRenderPassNode*)dn.userNode();

			// ������
			MObject passVal;
			MPlug(obj, HairRenderPassNode::o_output).getValue(passVal);
			if( passVal.isNull()) return false;
			MFnPluginData pdata(passVal);
			HairRenderPassData* pxdata = (HairRenderPassData*)pdata.data();
			if( !pxdata) return false;

			standAlonePass = pxdata->data;

			// ��������� ���������� �������
			bool bOk = false;
			for( int x=0; x<(int)standAlonePass.passes.size(); x++)
			{
				if(standAlonePass.passes[x]!=pass) continue;
				bOk = true;
				break;
			}
			if( !bOk)
			{
				displayString("HairExport cant find PASS");
				return false;
			}

			bStandAlonePass = true;
			shape = renderPass->getHairShape();
			obj = renderPass->getHairShapeNode();
			dn.setObject(obj);
		}

		if(!shape)
		{
			displayString("HairExport shape is not HairShape");
			return false;
		}

		// HairSystem
		HairSystem hairsystem;
		{
			MDGContext context;
			shape->LoadHairSystem(context, hairsystem, "all");
			if( bStandAlonePass)
			{
				hairsystem.passes.clear();
				hairsystem.passes.push_back(standAlonePass);
			}
			else
			{
				if(hairsystem.passes.empty())
					return false;
			}
		}
		hairsystem.camera_worldpos = mview;
		hairsystem.this_worldpos = thisTransform;
		hairsystem.passforocs = pass;
		hairsystem.currentpassname = pass;

		bool bMotionBlur = true;
//		MPlug (obj, HairShape::i_MotionBlur).getValue(bMotionBlur);
		MPlug i_MotionBlur = dn.findPlug("motionBlur");
		i_MotionBlur.getValue(bMotionBlur);
		bMotionBlur = bMotionBlur & (useMotionBlur!=0);

		int timestamp;
		MPlug(obj, HairShape::i_HairTimeStamp).getValue(timestamp);

//		time_t timestamp;
//		std::string hairrootfilename = shape->LoadHairRootPositions(timestamp);
		std::string hairrootfilename = shape->GetHairRootPositionFile(timestamp);
		if( hairrootfilename.empty()) return false;
	
		Util::create_directory_for_file(filename);

		Util::FileStream file;
		if( !file.open(filename, true))
		{
			std::string host = Util::currentHostName();
			displayStringError("MayaHair: cant write file %s Host %s!", filename, host.c_str());
			return false;
		}
		file >> hairrootfilename;
		file >> timestamp;

		// HairSystem
		hairsystem.serialize(file);

		Math::Box3f box;
		if( !bMotionBlur || useMotionBlur==0)
		{
			unsigned int count = 1;
			file >> count;

			// ���. ����.
//			if( useMotionBlur!=0)
			MTime time(frametime, MTime::uiUnit());
			if( MAnimControl::currentTime()!=time)
			{
				double val = time.as(MTime::uiUnit());
				fprintf(stderr, "HairMaya: MAnimControl::setCurrentTime(%f) noblur", val);
				MAnimControl::setCurrentTime(time);
			}
			MDGContext context;//(time);
			float t = (float)frametime;
			file >> t;
			shape->Save(file, box, context, hairsystem);
		}
		else
		{
			int _samples;
			MPlug(obj, HairShape::i_MotionSamples).getValue(_samples);
			unsigned int samples = _samples;
			if(samples<2) samples = 2;

//			std::vector<double> times, timesSec;
//			times.push_back(shutterOpen);
//			timesSec.push_back(shutterOpenSec);
//			times.push_back((shutterOpen+shutterClose)/2);
//			timesSec.push_back((shutterOpenSec+shutterCloseSec)/2);
//			times.push_back(shutterClose);
//			timesSec.push_back(shutterCloseSec);

			file >> samples;

			// ���. ����.
			for(int x=0; x<(int)samples; x++)
			{
				float factor = (float)x/(float)(samples-1);
				double sceneTime = (1-factor)*shutterOpen + factor*shutterClose;
				double sceneTimeSec = (1-factor)*shutterOpenSec + factor*shutterCloseSec;
				if( x==0)
				{
					sceneTime = shutterOpen;
					sceneTimeSec = shutterOpenSec;
				}
				if( x==samples-1)
				{
					sceneTime = shutterClose;
					sceneTimeSec = shutterCloseSec;
				}

				Math::Box3f boxX;
				MTime time(sceneTime, MTime::uiUnit());
				if( MAnimControl::currentTime()!=time)
				{
					double val = time.as(MTime::uiUnit());
					fprintf(stderr, "HairMaya: MAnimControl::setCurrentTime(%f)", val);
					MAnimControl::setCurrentTime(time);
				}
				MDGContext context;//(time);
				float t = (float)sceneTimeSec;
				file >> t;
				shape->Save(file, boxX, context, hairsystem);
				box.insert(boxX);
			}

			/*/
			// ���. ����.
			{
				Math::Box3f boxX;
				MTime time(shutterClose, MTime::uiUnit());
				MAnimControl ac;
				ac.setCurrentTime(time);
				MDGContext context;//(time);
				float t = (float)shutterCloseSec;
				file >> t;
				shape->Save(file, boxX, context);
				box.merge( boxX);
			}
			/*/
		}
		outbox[0] = box.min.x;
		outbox[1] = box.max.x;
		outbox[2] = box.min.y;
		outbox[3] = box.max.y;
		outbox[4] = box.min.z;
		outbox[5] = box.max.z;
	}
	if( MAnimControl::currentTime()!=mTime)
		MAnimControl::setCurrentTime(mTime);

//	displayString("pass: %s, shutterOpenSec=%f shutterCloseSec=%f", pass, shutterOpenSec, shutterCloseSec);
	return true;
}




void* dl_HairExport::creator()
{
	return new dl_HairExport;
}

MSyntax dl_HairExport::newSyntax()			// Aaeea?e?oai iiaua neioaene?aneea eiino?oeoee
{
	MSyntax syntax;

	syntax.addArg(MSyntax::kString);	// shapename
	syntax.addFlag("ff", "framefile", MSyntax::kString);// shapename
	syntax.addFlag("f", "force", MSyntax::kBoolean);	// force - ��������� ������������ ���������� ����
	syntax.addFlag("d", "dump", MSyntax::kBoolean);		// ������� ���������� � ������

	return syntax;
}

MStatus	dl_HairExport::doIt ( const MArgList& args)
{
	this->clearResult();

	MStatus stat;
	MArgDatabase argData(syntax(), args);
	MString	arg;

	MString argsl0;
	argData.getCommandArgument(0, argsl0);

	MString framefile;
	if( !argData.getFlagArgument("ff", 0, framefile))
		framefile="";

	bool force = true;
	if( !argData.getFlagArgument("f", 0, force))
		force=true;

	bool dump = true;
	if( !argData.getFlagArgument("d", 0, dump))
		dump=true;

	MObject obj;
	if( !nodeFromName(argsl0, obj))
		return MS::kFailure;
	MFnDependencyNode dn(obj);

	HairShape* shape = NULL;

	MTypeId tid = dn.typeId();
	if( tid != HairShape::id)
		return MS::kFailure;

	shape = (HairShape*)dn.userNode();
	int timeStamp; 
	std::string hairrootfilename = shape->GetHairRootPositionFile(timeStamp, force, dump);

	if(framefile!="")
	{
		Util::create_directory_for_file(framefile.asChar());
		Util::FileStream file;
		if( !file.open(framefile.asChar(), true))
		{
			displayString("cant write file %s!", framefile.asChar());
			return MS::kFailure;
		}
		int timestamp;
		MPlug(obj, HairShape::i_HairTimeStamp).getValue(timestamp);

		file >> hairrootfilename;
		file >> timestamp;

		// HairSystem
		HairSystem hairsystem;
		MDGContext context;
		shape->LoadHairSystem(context, hairsystem, "final");
		hairsystem.serialize(file);

		Math::Box3f box;
		int count = 1;
		file >> count;

		float t = 0;
		file >> t;
		shape->Save(file, box, context, hairsystem);
	}
	
	return MS::kSuccess;
}
