#include "HairArrayMapper.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>

#include <maya/MGlobal.h>

/*/
��������:

source AEHairArrayMapperTemplate;
eval("loadPlugin -qt HairMaya.mll");
HairArrayMapperTest("", "");

getAttr particleShape1.hairP
getAttr particleShape1.hairN
getAttr particleShape1.hairS
getAttr particleShape1.hairT
/*/

MObject HairArrayMapper::i_particleCount;
MObject HairArrayMapper::i_input;
MObject HairArrayMapper::o_output;

HairArrayMapper::HairArrayMapper()
{
}
HairArrayMapper::~HairArrayMapper()
{
}

MStatus HairArrayMapper::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;

	if( plug == o_output )
	{
		int nParticles = data.inputValue( i_particleCount, &stat ).asInt();
		MDataHandle inputData = data.inputValue( i_input, &stat );
		MVectorArray points;
		MFnVectorArrayData( inputData.data()).copyTo(points);
		int nPoints = points.length();

		MDataHandle outputData = data.outputValue( plug, &stat );

		MFnVectorArrayData dataVectorArrayFn;
		MVectorArray outArray;
		MObject outdata = data.outputValue( plug ).data();
		stat = dataVectorArrayFn.setObject( outdata );
		if( stat == MS::kSuccess )
		{
			outArray = dataVectorArrayFn.array();
		}

		outArray.setLength( nParticles );
		for( int i = 0; i < nParticles; i++ )
		{
			unsigned int index = i;
			if( nParticles > nPoints )
			{
				index = i % nPoints;
			}
			MVector point = points[index];
			outArray[i] = point;
		}
		dataVectorArrayFn.set( outArray );
		return MS::kSuccess;
	}
	return MS::kUnknownParameter;
}

void* HairArrayMapper::creator()
{
	return new HairArrayMapper();
}

MStatus HairArrayMapper::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		{
			i_particleCount = numAttr.create( "particleCount", "pc", MFnNumericData::kInt );
			::addAttribute( i_particleCount, atInput );
		}

		{
			i_input = typedAttr.create( "input", "in", MFnData::kVectorArray, &stat );
			typedAttr.setWritable(true);
			typedAttr.setStorable(true);
			::addAttribute(i_input, atInput);
		}
		{
			MVectorArray defaultVectArray;
			MFnVectorArrayData vectArrayDataFn;
			vectArrayDataFn.create( defaultVectArray );
			o_output = typedAttr.create( "output", "out", MFnData::kVectorArray, vectArrayDataFn.object(), &stat );
			typedAttr.setWritable(false);
			typedAttr.setStorable(false);
			::addAttribute(o_output, atOutput);

			stat = attributeAffects( i_particleCount, o_output );
			stat = attributeAffects( i_input, o_output );
		}
		if( !SourceMelFromResource(MhInstPlugin, "AEHAIRARRAYMAPPERTEMPLATE.MEL", "MEL", defines))
		{
			displayString("error source AEHairArrayMapperTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

