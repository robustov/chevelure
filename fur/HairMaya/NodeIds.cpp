#include "HairEmitter.h"
#include "HairShape.h"
#include "HairAliveData.h"
#include "HairClumpPositionsData.h"
#include "HairCVData.h"
#include "HairCVPositionsData.h"
#include "HairFixedFileData.h"
#include "HairGeometryData.h"
#include "HairGeometryIndicesData.h"
#include "HairRootPositionsData.h"
#include "textureCacheData.h"

#include "dl_HairExport.h"
#include "getsetFurCmd.h"
#include "UlHairConnectCVCurves.h"
#include "UlHairSetNullSnShape.h"
#include "HairModifier.h"
#include "HairModifierData.h"
#include "HairDeformerData.h"
#include "HairDeformer.h"
#include "HairDeformer_Wind.h"
#include "HairDeformer_Leaf.h"
#include "HairDeformer_ClumpRamp.h"
#include "HairRenderPassNode.h"
#include "HairRenderPassData.h"
#include "HairCombTool.h"
#include "HairBrushProxy.h"
#include "HairUtilCmd.h"
#include "HairControlsShape.h"
#include "ocHairShader.h"
#include "HairCvBasesData.h"
#include "HairControlsAnim.h"
#include "ocHairTestShader.h"
#include "HairControlsAnimData.h"
#include "HairArrayMapper.h"
#include "HairDeformer_LenghtRamp.h"
#include "HairRFMexport.h"


#ifndef SAS
	
	MTypeId HairDeformer_LenghtRamp::id( 0x80094 );
	const MString HairDeformer_LenghtRamp::typeName( "HairDeformer_LenghtRamp" );

	MTypeId HairArrayMapper::id( 0x80093 );
	const MString HairArrayMapper::typeName( "HairArrayMapper" );

	MTypeId HairControlsAnimData::id( 0x80092 );
	const MString HairControlsAnimData::typeName( "HairControlsAnimData" );

	MTypeId ocHairTestShader::id( 0x80091);
	const MString ocHairTestShader::typeName( "HairTestShader" );

	MTypeId HairControlsAnim::id( 0x80090 );
	const MString HairControlsAnim::typeName( "HairControlsAnim" );

	MTypeId HairCvBasesData::id( 0x80089 );
	const MString HairCvBasesData::typeName( "HairCvBasesData" );

	MTypeId HairControlsShape::id( 0x80088);
	const MString HairControlsShape::typeName( "HairControlsShape" );

	MTypeId ocHairShader::id( 0x80087);
	const MString ocHairShader::typeName( "HairShader" );

	const MString HairCombContext::toolName("HairCombTool");
	const MString HairCombContextCmd::typeName("HairCombContextCmd" );

	MTypeId HairDeformer_ClumpRamp::id( 0x80086 );
	const MString HairDeformer_ClumpRamp::typeName( "HairDeformer_ClumpRamp" );

	MTypeId HairRenderPassNode::id( 0x80085 );
	const MString HairRenderPassNode::typeName( "HairRenderPassNode" );

	MTypeId HairRenderPassData::id( 0x80084 );
	const MString HairRenderPassData::typeName( "HairRenderPassData" );

	MTypeId HairDeformer_Leaf::id( 0x80083 );
	const MString HairDeformer_Leaf::typeName( "HairDeformer_Leaf" );

	MTypeId HairDeformer_Wind::id( 0x80082 );
	const MString HairDeformer_Wind::typeName( "HairDeformer_Wind" );

	MTypeId HairDeformer::id( 0x80081 );
	const MString HairDeformer::typeName( "HairDeformer" );

	MTypeId HairDeformerData::id( 0x80080 );
	const MString HairDeformerData::typeName( "HairDeformerData" );

	MTypeId HairModifierData::id( 0x8007f );
	const MString HairModifierData::typeName( "HairModifierData" );

	MTypeId HairModifier::id( 0x8007e );
	const MString HairModifier::typeName( "HairModifier" );

	MTypeId HairEmitter::id( 0x8007d);
	const MString HairEmitter::typeName( "HairEmitter" );

	MTypeId HairShape::id(0x8007b);
	const MString HairShape::typeName( "HairShape" );

	MTypeId HairAliveData::id( 0xC0FFEE9c );
	const MString HairAliveData::typeName( "HairAliveData" );

	MTypeId HairClumpPositionsData::id( 0xC0FFEE3f );
	const MString HairClumpPositionsData::typeName( "HairClumpPositionsData" );

	MTypeId HairCVData::id( 0xC0FFEEea );
	const MString HairCVData::typeName( "HairCVData" );

	MTypeId HairCVPositionsData::id( 0xC0FFEEE9 );
	const MString HairCVPositionsData::typeName( "HairCVPositionsData" );

	MTypeId HairFixedFileData::id( 0xC0FFEE5f );
	const MString HairFixedFileData::typeName( "HairFixedFileData" );

	MTypeId HairGeometryData::id( 0xC0FFEE4f );
	const MString HairGeometryData::typeName( "HairGeometryData" );

	MTypeId HairGeometryIndicesData::id( 0xC0FFEE2f );
	const MString HairGeometryIndicesData::typeName( "HairGeometryIndicesData" );

	MTypeId HairRootPositionsData::id( 0xC0FFEE1f );
	const MString HairRootPositionsData::typeName( "HairRootPositionsData" );

	MTypeId textureCacheData::id( 0xC0FFEE91 );
	const MString textureCacheData::typeName( "textureCacheData" );

	const MString dl_HairExport::typeName( "HairExport" );
	const MString HairWorkingFacesCmd::typeName( "Hair_WorkingFaces" );
//	const MString cmd_setHairValue::typeName( "dl_setHairValue" );
//	const MString UlHairConnectCVCurves::typeName( "HairConnectCVCurves" );
	const MString UlHairSetNullSnShape::typeName( "HairSetNullSnShape" );

	#include "HairStartBrowserCmd.h"
	const MString HairStartBrowserCmd::typeName( "HairStartBrowserCmd" );

	const MString HairUtilCmd::typeName( "HairUtilCmd" );

	const MString HairRFMexport::typeName( "HairRFMexport" );
#else
	unsigned id_interval = 0x00daa00;

	const MString HairCombTool::typeName("chevelureCombToolCmd");
	const MString HairCombContext::toolName("chevelureCombTool");
	const MString HairCombContextCmd::typeName("chevelureCombContextCmd" );
	
	MTypeId HairBrushProxy::id( id_interval+0x22 );
	const MString HairBrushProxy::typeName("chevelureBrushProxyShape"); // ��� ������ ������������� �� Shape

	MTypeId HairDeformer_LenghtRamp::id( id_interval+0x21 );
	const MString HairDeformer_LenghtRamp::typeName( "chevelureDeformer_LenghtRamp" );

	MTypeId HairArrayMapper::id( id_interval+0x20);
	const MString HairArrayMapper::typeName( "chevelureArrayMapper" );

	MTypeId HairControlsAnimData::id( id_interval+0x19 );
	const MString HairControlsAnimData::typeName( "chevelureControlsAnimData" );

	MTypeId ocHairTestShader::id( id_interval+0x18 );
	const MString ocHairTestShader::typeName( "chevelureTestShader" );

	MTypeId HairControlsAnim::id( id_interval+0x17 );
	const MString HairControlsAnim::typeName( "chevelureControlsAnim" );

	MTypeId HairCvBasesData::id( id_interval+0x16);
	const MString HairCvBasesData::typeName( "chevelureCvBasesData" );

	MTypeId HairControlsShape::id( id_interval+0x15);
	const MString HairControlsShape::typeName( "chevelureControlsShape" );

	MTypeId ocHairShader::id( id_interval+0x14 );
	const MString ocHairShader::typeName( "chevelureShader" );

	MTypeId HairDeformer_ClumpRamp::id( id_interval+0x13 );
	const MString HairDeformer_ClumpRamp::typeName( "chevelureDeformer_ClumpRamp" );

	MTypeId HairRenderPassNode::id( id_interval+0x12 );
	const MString HairRenderPassNode::typeName( "chevelureRenderPassNode" );

	MTypeId HairRenderPassData::id( id_interval+0x11 );
	const MString HairRenderPassData::typeName( "chevelureRenderPassData" );

	MTypeId HairDeformer_Leaf::id( id_interval+0x10 );
	const MString HairDeformer_Leaf::typeName( "chevelureDeformer_Leaf" );

	MTypeId HairDeformer_Wind::id( id_interval+0x0f );
	const MString HairDeformer_Wind::typeName( "chevelureDeformer_Wind" );

	MTypeId HairDeformer::id( id_interval+0x0e );
	const MString HairDeformer::typeName( "chevelureDeformer" );

	MTypeId HairDeformerData::id( id_interval+0x0d );
	const MString HairDeformerData::typeName( "chevelureDeformerData" );

	MTypeId HairModifierData::id( id_interval+0x0c );
	const MString HairModifierData::typeName( "chevelureModifierData" );

	MTypeId HairModifier::id( id_interval+0x0b );
	const MString HairModifier::typeName( "chevelureModifier" );

	MTypeId HairEmitter::id( id_interval+0x00);
	const MString HairEmitter::typeName( "chevelureEmitter" );

	MTypeId HairShape::id( id_interval+0x01);
	const MString HairShape::typeName( "chevelureShape" );

	MTypeId HairAliveData::id( id_interval+0x02 );
	const MString HairAliveData::typeName( "chevelureAliveData" );

	MTypeId HairClumpPositionsData::id( id_interval+0x03 );
	const MString HairClumpPositionsData::typeName( "chevelureClumpPositionsData" );

	MTypeId HairCVData::id( id_interval+0x04 );
	const MString HairCVData::typeName( "chevelureCVData" );

	MTypeId HairCVPositionsData::id( id_interval+0x05 );
	const MString HairCVPositionsData::typeName( "chevelureCVPositionsData" );

	MTypeId HairFixedFileData::id( id_interval+0x06 );
	const MString HairFixedFileData::typeName( "chevelureFixedFileData" );

	MTypeId HairGeometryData::id( id_interval+0x07 );
	const MString HairGeometryData::typeName( "chevelureGeometryData" );

	MTypeId HairGeometryIndicesData::id( id_interval+0x08 );
	const MString HairGeometryIndicesData::typeName( "chevelureGeometryIndicesData" );

	MTypeId HairRootPositionsData::id( id_interval+0x09 );
	const MString HairRootPositionsData::typeName( "chevelureRootPositionsData" );

	MTypeId textureCacheData::id( id_interval+0x0a );
	const MString textureCacheData::typeName( "chevelureTextureCacheData" );

	const MString dl_HairExport::typeName( "chevelureExport" );
	const MString HairWorkingFacesCmd::typeName( "chevelure_WorkingFaces" );
//	const MString cmd_setHairValue::typeName( "chevelureSetValue" );
//	const MString UlHairConnectCVCurves::typeName( "chevelureConnectCVCurves" );
	const MString UlHairSetNullSnShape::typeName( "chevelureSetNullSnShape" );

	#include "HairStartBrowserCmd.h"
	const MString HairStartBrowserCmd::typeName( "chevelureStartBrowserCmd" );

	const MString HairUtilCmd::typeName( "chevelureUtilCmd" );

	const MString HairRFMexport::typeName( "chevelureRFMexport" );
#endif


