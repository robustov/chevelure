#pragma once

#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include "Math/Math.h"
#include "Math/matrixMxN.h"
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/ParamTex.h"
//#include "HairParam.h"

/*/
	USING:
	{
		MFnTypedAttribute fnAttr;
		MObject newAttr = fnAttr.create( 
			"fullName", "briefName", textureCacheData::id );
	}
/*/


class textureCacheData : public MPxData
{
public:
	textureCacheData();
	virtual ~textureCacheData(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

public:
	static MTypeId id;
	static const MString typeName;

//	HairParam2d<unsigned char> param2d;
	Math::ParamTex2d<unsigned char> paramTex2d;

	Math::Vec3f defVec;
	Math::matrixMxN<Math::Vec3f> textureub;

public:
	float getFloat(double u, double v);
//	void setFloats(const MPlug& src, MDataBlock& data, int resX, int resY, double jitter);
	void setFloats(const MPlug& src, MDataBlock& data, int resX, int resY, double jitter, const char* uvsetname);

	Math::Vec3f getVector(double u, double v);
	void setVectors(const MPlug& src, MDataBlock& data, int resX, int resY);

	bool serialize(Util::Stream& stream);

public:
	// loadFloatParam
//	static bool loadFloatParam(
//		HairParam2d<unsigned char>& param2d, 
//		const MPlug& src, int resX, int resY, float jitter
//		);
	// loadFloatParam
	static bool loadFloatParam(
		Math::ParamTex2d<unsigned char>& param2d, 
		const MPlug& src, int resX, int resY, float jitter, const char* uvsetname
		);
	// ������ �� �����
	static bool loadFloatParamFromFile(
		Math::ParamTex2d<unsigned char>& param2d, 
		const MPlug& src, int resX, int resY, 
		float jitter, const char* uvsetname
		);

protected:
	static std::string textureCacheData::isFileName(MObject shader);
};
