//
// Copyright (C) 
// File: HairFixedFileDataCmd.cpp
// MEL Command: HairFixedFileData

#include "HairFixedFileData.h"

#include <maya/MGlobal.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include "Util/FileStream.h"
#include <sstream>


HairFixedFileData::HairFixedFileData()
{
}
HairFixedFileData::~HairFixedFileData()
{
}

MTypeId HairFixedFileData::typeId() const
{
	return HairFixedFileData::id;
}

MString HairFixedFileData::name() const
{ 
	return HairFixedFileData::typeName; 
}

void* HairFixedFileData::creator()
{
	return new HairFixedFileData();
}

void HairFixedFileData::copy( const MPxData& other )
{
	const HairFixedFileData* arg = (const HairFixedFileData*)&other;
	this->filename = arg->filename;
	this->timestamp = arg->timestamp;
}

MStatus HairFixedFileData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("HairClumpPositionsData: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	if( !this->serialize(stream)) 
		return MS::kFailure;

	return MS::kSuccess;
}

MStatus HairFixedFileData::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus HairFixedFileData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	if( !serialize(stream))
		return MS::kFailure;

	return MS::kSuccess;
}

MStatus HairFixedFileData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

bool HairFixedFileData::serialize(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version>=0)
	{
		stream >> filename;
		int _timestamp = (int)timestamp;
		stream >> _timestamp;
		timestamp = _timestamp;
	}
	if(stream.isLoading())
	{
		this->Validate();
	}
	return true;
}

bool HairFixedFileData::IsValid()
{
	Validate();
	return !filename.empty();
}

void HairFixedFileData::Validate()
{
	if( filename.empty()) return;
	Util::FileStream file;
	if( !file.open( filename.c_str(), false))
	{
		// invalid
		filename = "";
		timestamp = 0;
	}
	else
	{
		int currenttime;
		file >> currenttime;
		if(timestamp!=currenttime)
		{
			// invalid
			filename = "";
			timestamp = 0;
		}
	}
}