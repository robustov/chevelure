//
// Copyright (C) 
// File: HairCvBasesDataCmd.cpp
// MEL Command: HairCvBasesData

#include "HairCvBasesData.h"

#include <maya/MGlobal.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include <sstream>


HairCvBasesData::HairCvBasesData()
{
}
HairCvBasesData::~HairCvBasesData()
{
}

MTypeId HairCvBasesData::typeId() const
{
	return HairCvBasesData::id;
}

MString HairCvBasesData::name() const
{ 
	return HairCvBasesData::typeName; 
}

void* HairCvBasesData::creator()
{
	return new HairCvBasesData();
}

void HairCvBasesData::copy( const MPxData& other )
{
	const HairCvBasesData* arg = (const HairCvBasesData*)&other;
	this->bases_inv = arg->bases_inv;
	this->bases = arg->bases;
}

MStatus HairCvBasesData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("HairCvBasesData: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus HairCvBasesData::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus HairCvBasesData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	serialize(stream);

	return MS::kSuccess;
}

MStatus HairCvBasesData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

void HairCvBasesData::serialize(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version>=0)
	{
		stream >> bases_inv;
		stream >> bases;
	}
}

