//
// Copyright (C) 
// File: HairStartBrowserCmdCmd.cpp
// MEL Command: HairStartBrowserCmd

#include "HairStartBrowserCmd.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include "Util/misc_create_directory.h"

HairStartBrowserCmd::HairStartBrowserCmd()
{
}
HairStartBrowserCmd::~HairStartBrowserCmd()
{
}

MSyntax HairStartBrowserCmd::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.addArg(MSyntax::kString);	// url
//	syntax.addFlag("f", "format", MSyntax::kString);
	return syntax;
}

#ifdef UNICODE
    typedef HINSTANCE (WINAPI* LPShellExecute)(HWND hwnd, LPCWSTR lpOperation, LPCWSTR lpFile, LPCWSTR lpParameters, LPCWSTR lpDirectory, INT nShowCmd);
#else
    typedef HINSTANCE (WINAPI* LPShellExecute)(HWND hwnd, LPCSTR lpOperation, LPCSTR lpFile, LPCSTR lpParameters, LPCSTR lpDirectory, INT nShowCmd);
#endif

MStatus HairStartBrowserCmd::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MString argsl0;
	argData.getCommandArgument(0, argsl0);

	std::string url = argsl0.asChar();

	char filename[512]; 
	GetModuleFileName( (HMODULE)MhInstPlugin, filename, 512); 
	std::string foldername = Util::extract_foldername(filename);

	std::string env = "%ULITKABIN%";
	if( strncmp(url.c_str(), env.c_str(), env.size())==0)
	{
		char* replace = getenv("ULITKABIN");
		url.replace(0, env.size(), replace);
	}

	bool bAddFullname = false;
	for( ;strncmp( url.c_str(), "..", 2)==0; )
	{
		if(foldername.size()>0 && foldername[foldername.size()-1]=='/')
			foldername = foldername.erase(foldername.size()-1);
		else
			break;

		foldername = Util::extract_foldername(foldername.c_str());
		url = url.substr(3);
		bAddFullname = true;
	}
	if( bAddFullname)
		url = foldername + url;

	HINSTANCE hInstShell32 = LoadLibrary(TEXT("shell32.dll"));
	if (hInstShell32 != NULL)
	{
		bool bSuccess = false;
		LPShellExecute pShellExecute = NULL;
#ifdef UNICODE
		pShellExecute = (LPShellExecute)GetProcAddress(hInstShell32, _TWINCE("ShellExecuteW"));
#else
		pShellExecute = (LPShellExecute)GetProcAddress(hInstShell32, "ShellExecuteA");
#endif
		if( pShellExecute != NULL )
		{
			if( pShellExecute( NULL, TEXT("open"), url.c_str(), NULL, foldername.c_str(), SW_SHOW ) > (HINSTANCE) 32 )
				bSuccess = true;
		}
		FreeLibrary(hInstShell32);
	}
	return MS::kSuccess;
}

void* HairStartBrowserCmd::creator()
{
	return new HairStartBrowserCmd();
}

