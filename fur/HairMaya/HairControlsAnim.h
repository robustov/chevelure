#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: HairControlsAnimNode.h
//
// Dependency Graph Node: HairControlsAnim

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 

#ifndef SAS
// ������������ ���� HairControlsAnimData ��� �������� ��������
#define USE_ANIMDATA
// ������������ ������ HairCvData ���������
//#define USE_HAIRCVARRAY
#else
// ������������ ���� HairControlsAnimData ��� �������� ��������
#define USE_ANIMDATA
// ������������ ������ HairCvData ���������
//#define USE_HAIRCVARRAY
#endif


#ifdef USE_ANIMDATA
	class HairControlsAnimData;
#endif// USE_ANIMDATA


class HairControlsAnim : public MPxNode
{
public:
	HairControlsAnim();
	virtual ~HairControlsAnim(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_time;		// Example input attribute
#ifdef USE_HAIRCVARRAY
	static MObject i_keys;		// Example output attribute
	static MObject i_values;	// Example output attribute
#endif// USE_HAIRCVARRAY

#ifdef USE_ANIMDATA
	static MObject i_animData;
	static MObject i_blendFrame;
	static MObject i_blendFactor;

#endif// USE_ANIMDATA
	static MObject o_HairCV;	// Example output attribute

	MStatus computeHairCV( 
		const MPlug& plug,
		MDataBlock& data);

	MStatus setKey(MTime t, MObject val);
	MStatus deleteKey(MTime t);
	MStatus saveAnim(const char* filename);
	MStatus loadAnim(const char* filename);

//	#if MAYA_API_VERSION > 800
	virtual bool isPassiveOutput( const MPlug& plug ) const;
//	#endif

#ifdef USE_ANIMDATA
	HairControlsAnimData* LoadAnimData(MDataBlock& data);
	MStatus BuildAnimData(MDataBlock& data);

	// ����� ���� ��������� ��� �����
	virtual MStatus shouldSave( const MPlug& plug, bool& result );

#endif// USE_ANIMDATA

public:
	static MTypeId id;
	static const MString typeName;
	static int current_file_version;
};

