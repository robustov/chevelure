#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: ocHairShaderNode.h
//
// Dependency Graph Node: ocHairShader

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include "ocellaris/IShadingNodeGenerator.h"

class ocHairShader : public MPxNode
{
public:
	ocHairShader();
	virtual ~ocHairShader(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();

public:
	// �����
	static MObject TipColor;
	static MObject TipOpacity;
	static MObject RootColor;
	static MObject RootOpacity;
	static MObject SpecColor;
	static MObject StaticAmbient;

	// from surface normal
	static MObject SurfaceKd;

	// in clump Attenuation
	static MObject inClumpAttenuation;
	static MObject DepthAttenuation;

	// Kaiya model
	static MObject KajiyaKd;
	static MObject KajiyaKs;
	static MObject KajiyaSpecWitdh;

	// ������
	static MObject HairDifferenceMin;
	static MObject HairDifferenceMax;
	static MObject StartSpec;
	static MObject EndSpec;
	static MObject SpecSizeFade;
	static MObject IllumWidth;
	static MObject UseSpecularFade;
	static MObject SpecularFadeRate; 
	static MObject ClumpDarkStrength; 
	static MObject ClumpSpecLightening ;
	static MObject SPEC1 ;
	static MObject SPEC2 ;
	static MObject Roughness1 ;
	static MObject Roughness2 ;
	static MObject ClumpDiffDarkening ;
	static MObject BackFadeRate ;

	static MObject Edginess;
	static MObject edgeLightRate;
	static MObject FurKd;
	static MObject VarFadeEnd;
	static MObject VarFadeStart;
	static MObject RootReflect;
	static MObject TipReflect;
	static MObject KrefRoot;
	static MObject KrefTip;
	static MObject FurKa;
	static MObject FurKdBack;
	static MObject FurKdEdge;


	static MObject i_UVCoord;		// uv

	static MObject o_color;			// Output attributes
	static MObject o_alpha;			// Output attributes

public:
	static MTypeId id;
	static const MString typeName;

	class Generator : public cls::IShadingNodeGenerator
	{
	public:
		// ���������� ���, ������ ��� �������������
		virtual const char* GetUniqueName(
			){return "ocHairShaderGenerator";};

		// ��� ����
		virtual enShadingNodeType getShadingNodeType(
			MObject& node					//!< ������ (shader)
			);

		//! ��������� ��������� 
		//! ��������� ��������� ��������� ��� �������
		virtual bool BuildShader(
			MObject& node,				//!< ������
			cls::enParamType wantedoutputtype, //!< ����� ��� ����� �� ������
			cls::IShaderAssembler* as,		//!< render
			cls::IExportContext* context,	//!< context
			MObject& generatornode			//!< generator node �.� 0
			);
	};
	static Generator generator;
};

