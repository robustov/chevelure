//
// Copyright (C) 
// File: HairCVPositionsDataCmd.cpp
// MEL Command: HairCVPositionsData

#include "HairCVPositionsData.h"
#include "HairGeometry.h"

#include <maya/MGlobal.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include <sstream>

bool HairCVPositionsData::Init( 
	int cvcount,
	HairGeometryIndices* hairGeometryIndices
	)
{
	return HairCVPositionsData::Init( 
		cvpos, 
		cvcount,
		hairGeometryIndices);
}

bool HairCVPositionsData::Init( 
	HairCVPositions& cvpos, 
	int cvcount,
	HairGeometryIndices* hairGeometryIndices
	)
{
	cvpos.clear();

	cvpos.guideCount = hairGeometryIndices->src_vertexCount();
	cvpos.cvcount = cvcount;
	cvpos.facevertsByGuide.resize( hairGeometryIndices->src_vertexCount());

	int fvc = hairGeometryIndices->src_facevertexCount();
	for(int i=0; i<fvc; i++)
	{
		int v = hairGeometryIndices->src_FV_vertexindex(i);

		std::vector<int>& fvForGuide = cvpos.facevertsByGuide[v];
		fvForGuide.push_back(i);
	}
	return true;
}
bool HairCVPositionsData::Subdiv(
	HairGeometryIndices& gi, 
	HairCVPositions& cvp
	)
{
	// subdive
	if( gi.isSubdived())
	{
		cvpos.clear();

		cvpos.guideCount = gi.vertexCount();
		cvpos.cvcount = cvp.cvcount;
		cvpos.facevertsByGuide.resize( gi.vertexCount());

		int fvc = gi.facevertexCount();
		for(int i=0; i<fvc; i++)
		{
			int v = gi.FV_vertexindex(i);

			std::vector<int>& fvForGuide = cvpos.facevertsByGuide[v];
			fvForGuide.push_back(i);
		}
	}
	else
	{
		this->cvpos = cvp;
	}

	return true;
}
/*/
bool HairCVPositionsData::Init( 
	Math::Vec2i count, 
	HairGeometry& hairGeometry, 
	HairGeometryIndices& hairGeometryIndices)
{
	cvpos.clear();

//	float startU = hairGeometry.minUV.x;
//	float startV = hairGeometry.minUV.y;
//	float slopeU = hairGeometry.maxUV.x - hairGeometry.minUV.x;
//	float slopeV = hairGeometry.maxUV.y - hairGeometry.minUV.y; 

	float startU = 0;
	float startV = 0;
	float slopeU = 1;
	float slopeV = 1; 

	cvpos.regular.init(count.x, count.y);
	cvpos.cvs.reserve(count.x*count.y);
	for(int mU = 0; mU < count.x; mU++)
	{
		for(int mV=0;mV < count.y; mV++)
		{
			cvpos.regular[mV][mU] = -1;
			
			float u = 0.0, v = 0.0;
			u = startU + slopeU*((mU+0.5f)/count.x); 
			v = startV + slopeV*((mV+0.5f)/count.y);

			Math::Vec2f uv((float)u, (float)v);

			HairGeometryIndices::pointonsurface pons;
			pons.faceindex = hairGeometry.findUvFace(uv, &hairGeometryIndices, pons.b);
			if( pons.faceindex<0) continue;

			cvpos.regular[mV][mU] = cvpos.cvs.size();
			cvpos.cvs.push_back(pons);
		}
	}
	return true;
}
/*/

HairCVPositionsData::HairCVPositionsData()
{
}
HairCVPositionsData::~HairCVPositionsData()
{
}

MTypeId HairCVPositionsData::typeId() const
{
	return HairCVPositionsData::id;
}

MString HairCVPositionsData::name() const
{ 
	return HairCVPositionsData::typeName; 
}

void* HairCVPositionsData::creator()
{
	return new HairCVPositionsData();
}

void HairCVPositionsData::copy( const MPxData& other )
{
	const HairCVPositionsData* arg = (const HairCVPositionsData*)&other;
	this->cvpos = arg->cvpos;
}

MStatus HairCVPositionsData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("HairCVPositionsData: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus HairCVPositionsData::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus HairCVPositionsData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	if( !serialize(stream))
		return MS::kFailure;

	return MS::kSuccess;
}

MStatus HairCVPositionsData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

bool HairCVPositionsData::serialize(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version>=0)
	{
		cvpos.serialize(stream);
	}
	return true;
}

