#pragma once

#include "pre.h"
#include <maya/MPxLocatorNode.h>
#include "HairMath.h"
#include "HairModifierData.h"
 
class HairModifier : public MPxLocatorNode
{
public:
	HairModifier();
	virtual ~HairModifier(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );
	virtual void draw(
		M3dView &view, const MDagPath &path, 
		M3dView::DisplayStyle style,
		M3dView::DisplayStatus status);

	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_testWM;

	static MObject i_type;
	static MObject i_directionType;

	// Ramp
	static MObject i_affect_ramp;

	// Deforming
	static MObject i_PaintAttrResX;
	static MObject i_PaintAttrResY;

	static MObject i_HairBaseWidth[4];	//!< � ��������� 0-1
	static MObject i_HairTipWidth[4];	//!< � ��������� 0-1
	static MObject i_HairBaseClump[4];
	static MObject i_HairTipClump[4];

	static MObject i_bRelative;
	static MObject i_HairLength[4];
	static MObject i_HairInclination[4];
	static MObject i_HairPolar[4];
	static MObject i_HairBaseCurl[4];
	static MObject i_HairTipCurl[4];
	static MObject i_HairScraggle[4];
	static MObject i_HairScraggleFreq[4];
	static MObject i_HairTwist[4];
	static MObject i_HairDisplace[4];
	static MObject i_HairDeformers;		//!< deformerdll + deformerproc
	// out
	static MObject o_output;

	MStatus ComputeOutput(const MPlug& plug, MDataBlock& data);
	MStatus ComputeTextureCache(const MPlug& dst, const MPlug& src, MDataBlock& data);
	MStatus ComputeTextureCache(MObject attr[3], MDataBlock& data);

	bool LoadMeshSurface(HairModifierData& meshsurface, MDataBlock& data);
	void LoadDeformers(MObject i_HairDeformers, MDataBlock& data, std::vector<HairDeformerDecl>& deformers);

public:
	static MTypeId id;
	static const MString typeName;

public:
	static MObject CreateRampAttr(
		std::string fullname, 
		std::string shortname);
	static MStatus CreateAttrNTextureCache( 
		std::string longname, std::string shortname, 
		MObject* i_HairAttr, double vmin, double vmax, double def, bool bStorableAttributes);
	static MStatus CreateAttrNTextureCacheNFlag( 
		std::string longname, std::string shortname, 
		MObject* i_HairAttr, double vmin, double vmax, double def, bool bStorableAttributes);
	static void CreateTextureCacheAttr(
		std::string longName, std::string shortName, MObject& src, MObject& attr, bool bStorableAttributes);
};


