
global proc AEchevelureShapeTemplate ( string $nodeName )
{
	editorTemplate -beginScrollLayout;

	editorTemplate -beginLayout "1. Surface" -collapse 1;
		editorTemplate -addControl "preSubdivSurface";
		editorTemplate -label "Surface Displacement" -addControl "displace";
	editorTemplate -endLayout;

	// suppress attributes that are connected by default
	editorTemplate -beginLayout "2. Generation" -collapse 0;
		editorTemplate -addControl "hairGenerateType";
		editorTemplate -addControl "Density";
		editorTemplate -addControl "Length";
		editorTemplate -addControl "LengthJitter";
		editorTemplate -addControl "globalSeed";
		editorTemplate -callCustom "AEchevelureShapeRegeneration" "AEchevelureShapeRegenerationReplace" "saveMessage";
		editorTemplate -beginLayout " 2.1 Gen.type: Casual" -collapse 0;
			editorTemplate -addControl "count";
		editorTemplate -endLayout;
		editorTemplate -beginLayout " 2.2 Gen.type: UV Regular" -collapse 1;
			editorTemplate -addControl "regularDim";
			editorTemplate -addControl "regularOffset";
			editorTemplate -addControl "regularShear";
			editorTemplate -addControl "jitter";
		editorTemplate -endLayout;
	editorTemplate -endLayout;

	editorTemplate -beginLayout "3. Width" -collapse 1;
		editorTemplate -addControl "BaseWidth";
		editorTemplate -addControl "TipWidth";
		editorTemplate -addControl "RenderWidth";
		editorTemplate -addControl "mulWidthByLength";
		editorTemplate -beginLayout " 3.1 Width jitters" -collapse 1;
			editorTemplate -addControl "BaseWidthJitter";
			editorTemplate -addControl "TipWidthJitter";
		editorTemplate -endLayout;
//		AEaddRampControl "width";
	editorTemplate -endLayout;
		
	editorTemplate -beginLayout "4. Clump" -collapse 1;
		editorTemplate -addControl "bCLUMP";
		editorTemplate -addControl "CLUMPcount";
		editorTemplate -addControl "CLUMPradius";
		editorTemplate -addControl "CLUMPblending";
		editorTemplate -addControl "CLUMPdensity";
		editorTemplate -addControl "BaseClump";
		editorTemplate -addControl "TipClump";
		editorTemplate -beginLayout " 4.1 Jitter" -collapse 1;
			editorTemplate -addControl "BaseClumpJitter";
			editorTemplate -addControl "TipClumpJitter";
		editorTemplate -endLayout;
	editorTemplate -endLayout;
	
	editorTemplate -beginLayout "5. Deforming" -collapse 1;
		editorTemplate -addControl "orthogonalize";
		editorTemplate -addControl "segmentCount";
		editorTemplate -addControl "Inclination";
		editorTemplate -addControl "Polar";
		editorTemplate -addControl "BaseCurl";
		editorTemplate -addControl "TipCurl";
		editorTemplate -addControl "Scraggle";
		editorTemplate -addControl "scraggleFreq";
		editorTemplate -addControl "twist";
		editorTemplate -beginLayout " 5.1 Deforming jitters" -collapse 1;
			editorTemplate -addControl "InclinationJitter";
			editorTemplate -addControl "PolarJitter";
			editorTemplate -addControl "BaseCurlJitter";
			editorTemplate -addControl "TipCurlJitter";
			editorTemplate -addControl "twistJitter";
		editorTemplate -endLayout;
	editorTemplate -endLayout;
	
	editorTemplate -beginLayout "6. Hair controllers" -collapse 1;
		editorTemplate -callCustom "AEchevelureShapeCV" "AEchevelureShapeCVReplace" $nodeName;
		editorTemplate -addControl "CVtailU";
		editorTemplate -addControl "CVtailV";
		editorTemplate -addControl "CvDisplay";
		editorTemplate -addControl "useDynamic";
	editorTemplate -endLayout;

	editorTemplate -beginLayout "7. Rendering" -collapse 1;
		editorTemplate -addControl "interpolation";
		editorTemplate -addControl "diceHair";
		editorTemplate -addSeparator;
		editorTemplate -addControl "MotionBlur";
		editorTemplate -addControl "MotionSamples";
		editorTemplate -beginLayout " 7.1 Texture sampling for render" -collapse 0;
			editorTemplate -addControl "RenderResX";
			editorTemplate -addControl "RenderResY";
		editorTemplate -endLayout;
		
		editorTemplate -addControl "useDetails";
		editorTemplate -beginLayout " 7.2 Level0 (clumps)" -collapse 1;
			editorTemplate -addControl "clumpminvisible";
			editorTemplate -addControl "clumplowertransition";
			editorTemplate -addControl "clumpuppertransition";
			editorTemplate -addControl "clumpmaxvisible";
		editorTemplate -endLayout;
		editorTemplate -beginLayout " 7.3 Level1 (hairs)" -collapse 1;
			editorTemplate -addControl "hairminvisible";
			editorTemplate -addControl "hairlowertransition";
			editorTemplate -addControl "hairuppertransition";
			editorTemplate -addControl "hairmaxvisible";
		editorTemplate -endLayout;
		editorTemplate -beginLayout " 7.4 Level2 (instances)" -collapse 1;
			editorTemplate -addControl "instanceminvisible";
			editorTemplate -addControl "instancelowertransition";
			editorTemplate -addControl "instanceuppertransition";
			editorTemplate -addControl "instancemaxvisible";
		editorTemplate -endLayout;
	editorTemplate -endLayout;

	editorTemplate -beginLayout "8. Hair display" -collapse 1;
		editorTemplate -addControl "drawMode";
		editorTemplate -addControl "maxHairDrawable";
		editorTemplate -addControl "Colorize";
		editorTemplate -addControl "BaseColor";
		editorTemplate -addControl "TipColor";
		editorTemplate -beginLayout " 8.1 Texture sampling for display" -collapse 0;
			editorTemplate -addControl "PaintAttrResX";
			editorTemplate -addControl "PaintAttrResY";
		editorTemplate -endLayout;
	editorTemplate -endLayout;
	

//	AEdependNodeTemplate $nodeName;
	AEshapeTemplate $nodeName;

	editorTemplate -addExtraControls;
	editorTemplate -endScrollLayout;
}



global proc AEchevelureShapeRegeneration(string $ftn)
{
	print("AEchevelureShapeRegeneration: "+$ftn+"\n");
	setUITemplate -pst attributeEditorTemplate;
	rowLayout -nc 2;
		text -l "";
		button -l "Rebuild cache" 
			   -c ("chevelureShapeRegeneration " + $ftn) filechevelureShapeRegeneration;
		setParent ..;
	setUITemplate -ppt;
}

global proc AEchevelureShapeRegenerationReplace (string $ftn)
{
	print("AEchevelureShapeRegenerationReplace: "+$ftn+"\n");
	button -e -c ("chevelureShapeRegeneration " + $ftn) filechevelureShapeRegeneration;
}

global proc AEchevelureShapeCV(string $ftn)
{
	setUITemplate -pst attributeEditorTemplate;
	rowLayout -nc 2;
		text -l "";
		button -l "Control curves" 
			   -c ("chevelure_AttachMayaDynamicUI " + $ftn) btnchevelureShapeCV;
		setParent ..;
	setUITemplate -ppt;
}
global proc AEchevelureShapeCVReplace (string $ftn)
{
	button -e -c ("chevelure_AttachMayaDynamicUI " + $ftn) btnchevelureShapeCV;
}

global proc chevelureShapeRegeneration(string $ftn)
{
	eval("getAttr "+$ftn);
	print("chevelureShapeRegeneration: "+$ftn+"\n");
}
