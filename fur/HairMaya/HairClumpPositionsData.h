#pragma once

#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include "Util/Stream.h"
#include "HairClumpPositions.h"
#include "HairGeometryIndices.h"

/*/
	USING:
	{
		MFnTypedAttribute fnAttr;
		MObject newAttr = fnAttr.create( 
			"fullName", "briefName", HairClumpPositionsData::id );
	}
/*/
class HairRootPositions;


class HairClumpPositionsData : public MPxData
{
public:
	HairClumpPositions hc;

	bool Init(
		MObject thisObject, 
		HairGeometryIndices* geometryIndices, 
		HairGeometry* geometry, 
		int CLUMPcount, 
		int resX, int resY);

	bool Init(
		HairGeometryIndices* geometryIndices, 
		HairGeometry* geometry, 
		Math::matrixMxN<unsigned char>& matrix
		);
	static void CalcClumpRadius(
		HairRootPositions* hp, 
		HairClumpPositions* hc, 
		HairGeometryIndices* hgh, 
		HairGeometry* geometry
		);

public:
	HairClumpPositionsData();
	virtual ~HairClumpPositionsData(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

public:
	static MTypeId id;
	static const MString typeName;

protected:
	bool serialize(Util::Stream& stream);
};
