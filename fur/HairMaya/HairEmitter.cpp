#include "HairEmitter.h"

#include <maya/MArrayDataBuilder.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnArrayAttrsData.h>
#include <maya/MFnMatrixData.h>
#include <mathNmaya/mathNmaya.h>

#include <maya/MGlobal.h>
#include "textureCacheData.h"

MObject HairEmitter::i_HairGeometryIndices;
MObject HairEmitter::i_HairGeometry;		
MObject HairEmitter::i_HairRootPositions;
MObject HairEmitter::i_HairAlive;
MObject HairEmitter::i_EmitStartTime[2];

MObject HairEmitter::i_PaintAttrRes;		//!< ����������� ��� ���� �������
MObject HairEmitter::i_EmitInclination[2];	//!< ���������� ���������� � TS
MObject HairEmitter::i_EmitPolar[2];		//!< ���������� ���������� � TS
MObject HairEmitter::i_EmitVelocity[2];		//!< �������� ���������

HairEmitter::HairEmitter()
{
}
HairEmitter::~HairEmitter()
{
}

//! Main Generate proc
void HairEmitter::emit(
	int emitCount,					//!< wanted particle count
	MVectorArray& fnOutPos,			//!< output positions
	MVectorArray& fnOutVel,			//!< output velocities
	MVectorArray& fnOutScale,		//!< output scale
	int plugIndex,					//!< N particle system
	MDataBlock& data				//!< data
	)
{
	double speed = data.inputValue(mSpeed).asDouble();
	MVector direction = data.inputValue(mSpeed).asDouble3();
//	MVector ownerPosData = data.inputValue(mOwnerPosData).asDouble3();
//	mOwnerVelData
//	mOwnerCentroid

	MPlug wmplug(thisMObject(), mWorldMatrix);
	MObject wmobj;
	wmplug.elementByLogicalIndex(0).getValue(wmobj);
	MMatrix worldMatrix = MFnMatrixData(wmobj).matrix();

//	MMatrix worldMatrix = data.inputValue(mWorldMatrix).asMatrix();
	double inheritFactor = data.inputValue(mInheritFactor).asDouble();
	int seed = data.inputValue(mSeed).asInt();
//	srand(seed);

	MTime cT = this->currentTimeValue( data );
	double time = cT.as(MTime::kSeconds);

	HairGeometryIndices*	hgi = LoadHairGeometryIndices(data);
	HairGeometry*			hg  = LoadHairGeometry(data);
	HairRootPositions*		hrp = LoadHairRootPositions(data);
	HairAlive*				ha  = LoadHairAlive(data);

	textureCacheData* EmitStartTime = (textureCacheData*)data.inputValue( i_EmitStartTime[1]).asPluginData();

	textureCacheData* EmitInclination	= (textureCacheData*)data.inputValue( i_EmitInclination[1]).asPluginData();
	textureCacheData* EmitPolar			= (textureCacheData*)data.inputValue( i_EmitPolar[1]).asPluginData();
	textureCacheData* EmitVelocity		= (textureCacheData*)data.inputValue( i_EmitVelocity[1]).asPluginData();

	int count = hrp->getCount();
	ha->resize(count);

	for(int i=0; i<emitCount; i++)
	{
		MPoint newPos(0, 0, 0);
		MVector newVel(0, 1, 0);

		int ind = 0;
		Math::Vec2f uv;
		int x;
		for(x=0; x<count; x++)
		{
			double s = rand()/(double)RAND_MAX;
			ind = (int)(count*s);
			ind = ha->findTrue(ind);
			if( ind<0)
				break;

			uv = hrp->getUV(ind, hgi, hg);
			float st = EmitStartTime->getFloat(uv.x, uv.y);
			if( st>time)
				continue;
			break;
		}
		if( ind<0)
			break;
		if(x==count) 
			break;
		ha->set(ind, false);

		Math::Vec3f pos = hrp->getPOS(ind, hgi, hg);
		copy( newPos, pos);
		newPos *= worldMatrix;

		Math::Vec3f normal, tangentU, tangentV;
		hrp->getBasis( ind, hgi, hg, normal, tangentU, tangentV);

		float Polar = EmitPolar->getFloat(uv.x, uv.y);
		float Inclination = EmitInclination->getFloat(uv.x, uv.y);
		float Velocity = EmitVelocity->getFloat(uv.x, uv.y);
		float lenght = 0.1f;

		double theta = M_PI * (2 * Polar - 1);
		Math::Rot3f PolarRot( (float)theta, normal);
		Math::Matrix3f rm;
		PolarRot.getBasis(rm[0].data(), rm[1].data(), rm[2].data());
		tangentU = rm*tangentU;
		tangentV = rm*tangentV;

		normal = (1 - Inclination) * normal + Inclination * tangentU;
		normal.normalize();
		normal *= Velocity;

		copy( newVel, normal);
		newVel *= worldMatrix;

		
		// ...
		fnOutPos.append( MVector(newPos) );
		fnOutVel.append( newVel );
		fnOutScale.append( MVector(lenght, lenght, lenght) );
	}
}

MStatus HairEmitter::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;

	displayStringD("HairEmitter::compute %s", plug.name().asChar());

	if( plug == mOutput)// || plug == i_HairAlive)
		return computeOutput(plug, data);
	if( plug == i_HairAlive)
	{
		MTime cT = this->currentTimeValue( data );
		MTime sT = this->startTimeValue( 0, data );
		MTime dT = this->deltaTimeValue( 0, data );
		if( (cT <= sT) || (dT <= 0.0) )
		{
			HairRootPositions*		hrp = LoadHairRootPositions(data);
			HairAlive*				ha  = LoadHairAlive(data);
			int count = hrp->getCount();
			ha->resize(0);
			ha->resize(count);

			data.setClean( i_HairAlive );
			return( MS::kSuccess );
		}

//		MPlug plug(thisMObject(), mOutput);
//		return computeOutput(plug.elementByLogicalIndex(0), data);
	}
	if( plug == i_EmitStartTime[1] )
		return ComputeTextureCache(plug, MPlug(thisMObject(), i_EmitStartTime[0]), data);
	if( plug == i_EmitInclination[1] )
		return ComputeTextureCache(plug, MPlug(thisMObject(), i_EmitInclination[0]), data);
	if( plug == i_EmitPolar[1] )
		return ComputeTextureCache(plug, MPlug(thisMObject(), i_EmitPolar[0]), data);
	if( plug == i_EmitVelocity[1] )
		return ComputeTextureCache(plug, MPlug(thisMObject(), i_EmitVelocity[0]), data);

	return MS::kUnknownParameter;
}

MStatus HairEmitter::computeOutput( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;
	// Get the logical index of the element this plug refers to,
	// because the node can be emitting particles into more 
    // than one particle shape.
	int multiIndex = plug.logicalIndex( &stat );

	// Get output data arrays (position, velocity, or parentId)
	// that the particle shape is holding from the previous frame.
	MArrayDataHandle hOutArray = data.outputArrayValue( mOutput, &stat);

	// Create a builder to aid in the array construction efficiently.
	MArrayDataBuilder bOutArray = hOutArray.builder( &stat );

	// Get the appropriate data array that is being currently evaluated.
	MDataHandle hOut = bOutArray.addElement(multiIndex, &stat);

	// Create the data and apply the function set,
	// particle array initialized to length zero, 
	// fnOutput.clear()
	MFnArrayAttrsData fnOutput;
	MObject dOutput = fnOutput.create ( &stat );

	// Get the position and velocity arrays to append new particle data.
	//
	MVectorArray fnOutPos = fnOutput.vectorArray("position", &stat);
    MVectorArray fnOutVel = fnOutput.vectorArray("velocity", &stat);
    MVectorArray fnOutScale = fnOutput.vectorArray("scale", &stat);

	// Check if the particle object has reached it's maximum,
	// hence is full. If it is full then just return with zero particles.
	//
	bool beenFull = this->isFullValue( multiIndex, data );
	if( beenFull )
	{
		return( MS::kSuccess );
	}

	// Get deltaTime, currentTime and startTime.
	// If deltaTime <= 0.0, or currentTime <= startTime,
	// do not emit new pariticles and return.
	//
	MTime cT = this->currentTimeValue( data );
	MTime sT = this->startTimeValue( multiIndex, data );
	MTime dT = this->deltaTimeValue( multiIndex, data );
	if( (cT <= sT) || (dT <= 0.0) )
	{
		// We do not emit particles before the start time, 
		// and do not emit particles when moving backwards in time.

		HairRootPositions*		hrp = LoadHairRootPositions(data);
		HairAlive*				ha  = LoadHairAlive(data);
		int count = hrp->getCount();
		ha->resize(0);
		ha->resize(count);

		// This code is necessary primarily the first time to 
		// establish the new data arrays allocated, and since we have 
		// already set the data array to length zero it does 
		// not generate any new particles.
		hOut.set( dOutput );
		data.setClean( plug );
		data.setClean( i_HairAlive );
		return( MS::kSuccess );
	}

    // Get rate and delta time.
    //
    double rate = data.inputValue(mRate, &stat).asDouble();
    double dblCount = rate * dT.as( MTime::kSeconds );
    int emitCount = (int)dblCount;

	// Generate proc
	emit(emitCount, fnOutPos, fnOutVel, fnOutScale, multiIndex, data);

	hOut.set( dOutput );
	data.setClean( plug );
	data.setClean( i_HairAlive );

	return( MS::kSuccess );
}

void* HairEmitter::creator()
{
	return new HairEmitter();
}

// LOAD
HairGeometryIndices* HairEmitter::LoadHairGeometryIndices(MDataBlock& data)
{
	MStatus _Status, stat;
	MDataHandle inputData = data.inputValue(i_HairGeometryIndices, &stat);
	MPxData* pxdata = inputData.asPluginData();
	if( !pxdata) return NULL;
	HairGeometryIndicesData* hgh = (HairGeometryIndicesData*)pxdata;
	return &hgh->hgh;
}
HairGeometry* HairEmitter::LoadHairGeometry(MDataBlock& data)
{
	MStatus _Status, stat;
	MDataHandle inputData = data.inputValue(i_HairGeometry, &stat);
	MPxData* pxdata = inputData.asPluginData();
	if( !pxdata) return NULL;
	HairGeometryData* gd = (HairGeometryData*)pxdata;
	return &gd->geometry;
}
HairRootPositions* HairEmitter::LoadHairRootPositions(MDataBlock& data)
{
	MStatus _Status, stat;
	MDataHandle inputData = data.inputValue(i_HairRootPositions, &stat);
	MPxData* pxdata = inputData.asPluginData();
	if( !pxdata) return NULL;
	HairRootPositionsData* placement = (HairRootPositionsData*)pxdata;
	return &placement->hp;
}
HairAlive* HairEmitter::LoadHairAlive(MDataBlock& data)
{
	MStatus _Status, stat;
	HairAliveData* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( i_HairAlive, &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<HairAliveData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( HairAliveData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<HairAliveData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	if( bNewData)
		outputData.set(pData);
	return &pData->data;
}


MStatus HairEmitter::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		{
			i_HairAlive = typedAttr.create( "HairAliveData", "ha", HairAliveData::id);
			::addAttribute( i_HairAlive, atReadable|atWritable|atStorable|atCached|atConnectable|atHidden);

			i_PaintAttrRes = numAttr.create("PaintAttrRes","par", MFnNumericData::k2Int, 16, &stat);
			::addAttribute( i_PaintAttrRes, atInput);

			i_HairGeometryIndices = typedAttr.create( "HairGeometryIndices", "hgh", HairGeometryIndicesData::id);
			::addAttribute( i_HairGeometryIndices, atReadable|atUnStorable|atUnCached|atConnectable|atHidden);

			i_HairGeometry = typedAttr.create( "HairGeometry", "hge", HairGeometryData::id);
			::addAttribute( i_HairGeometry, atReadable|atUnStorable|atUnCached|atConnectable|atHidden);

			i_HairRootPositions = typedAttr.create( "HairRootPositions", "hpl", HairRootPositionsData::id);
			::addAttribute( i_HairRootPositions, atReadable|atUnStorable|atUnCached|atConnectable|atHidden);

			if( !CreateAttrNTextureCache("emitStartTime","est", i_EmitStartTime, 0, 1, 0))
				return MS::kFailure;

			if( !CreateAttrNTextureCache("EmitInclination","einc", i_EmitInclination, 0, 1, 0))
				return MS::kFailure;

			if( !CreateAttrNTextureCache("EmitPolar","epol", i_EmitPolar, 0, 1, 0))
				return MS::kFailure;

			if( !CreateAttrNTextureCache("EmitVelocity","evel", i_EmitVelocity, 0, 1, 0))
				return MS::kFailure;

			AffectToOutput(i_HairAlive);
			AffectToOutput(mOutput);
		}
		if( !SourceMelFromResource(MhInstPlugin, "AEHairEmitterTemplate", "MEL", defines))
		{
			displayString("error source AEHairEmitterTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

MStatus HairEmitter::ComputeTextureCache(const MPlug& dst, const MPlug& src, MDataBlock& data)
{
	displayStringD("HairShape::ComputeTextureCache %s", dst.name().asChar());
	MStatus stat;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( dst, &stat );
	MPxData* userdata = outputData.asPluginData();
	textureCacheData* pData = static_cast<textureCacheData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( textureCacheData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<textureCacheData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}
	int2& res = data.inputValue(i_PaintAttrRes).asInt2();

	MFnNumericAttribute attr( src.attribute());
	if( attr.unitType()==MFnNumericData::kDouble)
		pData->setFloats(src, data, res[0], res[1], 0, "");
	else
		pData->setVectors(src, data, res[0], res[1]);


	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}

void HairEmitter::CreateTextureCacheAttr(
	std::string longName, std::string shortName, MObject& src, MObject& attr)
{
	MStatus _Status;
	MFnTypedAttribute	typedAttr;

	attr = typedAttr.create( longName.c_str(), shortName.c_str(), textureCacheData::id);
	typedAttr.setStorable(false);
	typedAttr.setReadable(true);
	typedAttr.setWritable(true);
	typedAttr.setHidden(true);
	_Status = addAttribute(attr);
	if (!_Status) 
	{
		displayString("cand create attr %s", longName);
		return;
	}
	attributeAffects(src,				attr);
	attributeAffects(i_PaintAttrRes,	attr);
	attributeAffects(attr,				mOutput);
	attributeAffects(attr,				i_HairAlive);
}

MStatus HairEmitter::CreateAttrNTextureCache( 
	std::string longname, std::string shortname, 
	MObject* i_HairAttr, double vmin, double vmax, double def)
{
	MStatus _Status;
	MFnNumericAttribute numAttr;
	i_HairAttr[0] = numAttr.create(longname.c_str(), shortname.c_str(), MFnNumericData::kDouble, def, &_Status);
	if (!_Status) return _Status;
	numAttr.setStorable(true);
	numAttr.setHidden(false);
	numAttr.setSoftMin(vmin);	
	numAttr.setSoftMax(vmax);	
	_Status = addAttribute (i_HairAttr[0]);	
	if (!_Status) return _Status;
	attributeAffects(i_HairAttr[0], mOutput);
	attributeAffects(i_HairAttr[0], i_HairAlive);

	CreateTextureCacheAttr( longname+"Cache", shortname+"ch", i_HairAttr[0], i_HairAttr[1]);
	return MS::kSuccess;
}

void HairEmitter::AffectToOutput(const MObject& isAffected)
{
	attributeAffects(i_HairGeometryIndices,	isAffected);
	attributeAffects(i_HairGeometry,		isAffected);
	attributeAffects(i_HairRootPositions,	isAffected);
	attributeAffects(i_PaintAttrRes,		isAffected);
	attributeAffects(mRate,					isAffected);
	attributeAffects(mSpeed,				isAffected);
	attributeAffects(mDirection,			isAffected);
	attributeAffects(mDirectionX,			isAffected);
	attributeAffects(mDirectionY,			isAffected);
	attributeAffects(mDirectionZ,			isAffected);
	attributeAffects(mOwnerPosData,			isAffected);
	attributeAffects(mOwnerVelData,			isAffected);
	attributeAffects(mOwnerCentroid,		isAffected);
	attributeAffects(mOwnerCentroidX,		isAffected);
	attributeAffects(mOwnerCentroidY,		isAffected);
	attributeAffects(mOwnerCentroidZ,		isAffected);
	attributeAffects(mSweptGeometry,		isAffected);
	attributeAffects(mWorldMatrix,			isAffected);
	attributeAffects(mStartTime,			isAffected);
	attributeAffects(mDeltaTime,			isAffected);
	attributeAffects(mIsFull,				isAffected);
	attributeAffects(mInheritFactor,		isAffected);
	attributeAffects(mSeed,					isAffected);
	attributeAffects(mCurrentTime,			isAffected);
}
