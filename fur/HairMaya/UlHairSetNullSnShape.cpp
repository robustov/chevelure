//
// Copyright (C) 
// File: UlHairSetNullSnShapeCmd.cpp
// MEL Command: UlHairSetNullSnShape

#include "UlHairSetNullSnShape.h"

#include <maya/MGlobal.h>

UlHairSetNullSnShape::UlHairSetNullSnShape()
{
}
UlHairSetNullSnShape::~UlHairSetNullSnShape()
{
}

MSyntax UlHairSetNullSnShape::newSyntax()			// ����������� ����� �������������� �����������
{
	MSyntax syntax;

	syntax.addArg(MSyntax::kString);	// arg0
	return syntax;
}

MStatus UlHairSetNullSnShape::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MString argsl0;
	argData.getCommandArgument(0, argsl0);
	MObject obj;
	nodeFromName(argsl0, obj);

	MFnDependencyNode dn(obj);
	MPlug pl = dn.findPlug("snShape");

	unsigned numElements = pl.numElements();
	for( unsigned i = 0; i < numElements; i++ )
	{
		MPlug tweak = pl.elementByPhysicalIndex(i);
		if( !tweak.isNull() )
		{
			MPxData* data = NULL;
//			tweak.setValue(data);
			tweak.setValue(MObject::kNullObj);
		}
	}
	return MS::kSuccess;
}

void* UlHairSetNullSnShape::creator()
{
	return new UlHairSetNullSnShape();
}

