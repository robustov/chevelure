import maya.mel

objExportCache = {
}

def expandBbox(bbox, byBbox):
	out = bbox[:]
	for i in xrange(0, 5, 2):
		out[i] = min(out[i], byBbox[i])
	for i in xrange(1, 6, 2):
		out[i] = max(out[i], byBbox[i])
	return out

def addObjectToExportCache(obj, file, motionTime, bbox):
	if not objExportCache.has_key(obj):
		objExportCache[obj] = [[file], [motionTime], bbox]
	else:
		objExportCache[obj][0].append(file)
		objExportCache[obj][1].append(motionTime)
		expandedBbox = expandBbox(objExportCache[obj][2], bbox)
		'''
		print expandedBbox
		print objExportCache[obj][2]
		print bbox
		'''
		objExportCache[obj][2] = expandedBbox
	#print 'object', obj, 'cached, f:', file

def clearExportCache():
	objExportCache.clear()
	#print 'export cache cleared'

def generateRibCall(obj):
	if not objExportCache.has_key(obj):
		print 'no found', obj, ' in export cache'
		return
	#print obj
	objCache = objExportCache[obj]
	#print objCache
	#print objExportCache
	bbox = objCache[2]
	bboxStr = ''
	for b in bbox:
		bboxStr += str(b) + ' '

	files = objCache[0]
	motionTime = objCache[1]
	motionTimeCount = len(motionTime) if len(motionTime) > 1 else 0
	
	paramStr = str(len(files)) + ' ' + str(motionTimeCount) + ' '

	for file in files:
		paramStr += '\\"' + file + '\\" '

	for i in xrange(motionTimeCount):
		paramStr += str(motionTime[i]) + ' '
	
	bindir = ''
	rfmFlag = ''
	if not maya.mel.eval('rman ctxIsRib'):
		rfmFlag = '#RFM# '
	evalStr = "RiProcedural \"DynamicLoad\" \"" + bindir + "chevelureDSO.dll\" " + bboxStr\
			+ " \"" + rfmFlag + paramStr + "\";"
	print evalStr
	maya.mel.eval(evalStr)
