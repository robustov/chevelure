#pragma once

#include "pre.h"

class HairWorkingFacesCmd : public MPxCommand 
{
public:
	static const MString typeName;
	HairWorkingFacesCmd() {};
	static MSyntax	newSyntax();
	virtual MStatus	doIt ( const MArgList& args);
	static void*	creator();

protected:
	void updateSelectionRec(std::vector<MDagPath>& targethairs, MDagPath& dagPath, int level);

};

/*/
class cmd_setHairValue : public MPxCommand 
{
public:
	static const MString typeName;
	cmd_setHairValue() {};
	static MSyntax	newSyntax();
	virtual MStatus	doIt ( const MArgList& args);
	static void*	creator();
};
/*/
