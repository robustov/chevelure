#include "getsetFurCmd.h"
#include "HairShape.h"
#include <Maya/MIntArray.h>
#include <Maya/MFnSingleIndexedComponent.h>

void* HairWorkingFacesCmd::creator()
{
	return new HairWorkingFacesCmd;
}

MSyntax HairWorkingFacesCmd::newSyntax()			// Aaeea?e?oai iiaua neioaene?aneea eiino?oeoee
{
	MSyntax syntax;

	syntax.addFlag("s", "select");
	syntax.addFlag("r", "reset");
	syntax.addFlag("ff", "facefilter");		// ��������� � facefilter

	return syntax;
}

MStatus	HairWorkingFacesCmd::doIt ( const MArgList& args)
{
	MStatus stat;
	MArgDatabase argData(syntax(), args);

	bool bSet = argData.isFlagSet("s");
	bool bReset = argData.isFlagSet("r");
	bool bFacefilter = argData.isFlagSet("ff");

	MObject targetattr = HairShape::i_WorkingFaces;
//	if( bFacefilter) 
//		targetattr = HairShape::i_FaceFilter;

	if(bSet )
	{
		MSelectionList list;
		MGlobal::getActiveSelectionList(list);

		for( int i=0; i<(int)list.length(); i++)
		{
			MDagPath dagPath;
			MObject component;
			list.getDagPath( i, dagPath, component);
			if( component.isNull()) continue;

			std::vector<MDagPath> hairs;
			updateSelectionRec(hairs, dagPath, 0);
			if( hairs.empty()) continue;

			MFnSingleIndexedComponent single(component);
			if( component.apiType() != MFn::kMeshPolygonComponent)
				continue;
			MIntArray elements;
			single.getElements( elements );
			for(int e=0; e<(int)hairs.size(); e++)
			{
				// setAttr "pPlane1HairShape1.woFa" -type "Int32Array" 2 3 2
				MPlug plug( hairs[e].node(), HairShape::i_WorkingFaces);
				MFnIntArrayData intdata;
				MObject val = intdata.create(elements, &stat);
				stat = plug.setValue(val);
			}
		}
	}
	if(bReset)
	{
		MSelectionList list;
		MGlobal::getActiveSelectionList(list);

		for( int i=0; i<(int)list.length(); i++)
		{
			MDagPath dagPath;
			MObject component;
			list.getDagPath( i, dagPath, component);

			std::vector<MDagPath> hairs;
			updateSelectionRec(hairs, dagPath, 0);
			if( hairs.empty()) continue;

			for(int e=0; e<(int)hairs.size(); e++)
			{
				MPlug plug( hairs[e].node(), HairShape::i_WorkingFaces);
				plug.setValue(MObject::kNullObj);
			}
		}

	}
	// ����� 
	return MS::kSuccess;
}

void HairWorkingFacesCmd::updateSelectionRec(std::vector<MDagPath>& targethairs, MDagPath& dagPath, int level)
{
	if( level>15) return;
	MFnDependencyNode dn(dagPath.node());
	if( dn.typeId()==HairShape::id)
	{
		for(int i=0; i<(int)targethairs.size(); i++)
			if(dagPath==targethairs[i]) return;
		targethairs.push_back(dagPath);
		return;
	}
	// connectios
	MPlugArray array;
	dn.getConnections(array);
	for(int i=0; i<(int)array.length(); i++)
	{
		MPlug p = array[i];
		MPlugArray pa;
		p.connectedTo(pa, false, true);
		for(int pi=0; pi<(int)pa.length(); pi++)
		{
			MPlug p = pa[pi];
			MFnDependencyNode dn2(p.node());
			if( dn2.typeId()==HairShape::id)
			{
				MDagPath dagPath2 = MDagPath::getAPathTo(p.node());
				for(int i=0; i<(int)targethairs.size(); i++)
					if(dagPath2==targethairs[i]) return;
				targethairs.push_back(dagPath2);
			}
		}
	}

	// childs
	for( int c=0; c<(int)dagPath.childCount(); c++)
	{
		MObject obj;
		obj = dagPath.child(c);
		MDagPath dagPath2 = MDagPath::getAPathTo(obj);
		updateSelectionRec(targethairs, dagPath2, level+1);
	}
}






/*/


void* cmd_setHairValue::creator()
{
	return new cmd_setHairValue;
}

MSyntax cmd_setHairValue::newSyntax()			// Aaeea?e?oai iiaua neioaene?aneea eiino?oeoee
{
	MSyntax syntax;

	syntax.addArg(MSyntax::kString);	// shapename
	syntax.addArg(MSyntax::kString);	// attributename
	syntax.addArg(MSyntax::kLong);		// index
	syntax.addArg(MSyntax::kDouble);	// value

	return syntax;
}

MStatus	cmd_setHairValue::doIt ( const MArgList& args)
{
	MStatus stat;
	MArgDatabase argData(syntax(), args);
	MString	arg;

	MString argsl0;
	argData.getCommandArgument(0, argsl0);
	MString attributename;
	argData.getCommandArgument(1, attributename);
	int index;
	argData.getCommandArgument(2, index);
	double value;
	argData.getCommandArgument(3, value);

	MObject obj;
	if( !nodeFromName(argsl0, obj))
		return MS::kSuccess;

	MFnDependencyNode dn(obj, &stat);
	if( !stat) 
		return MS::kSuccess;
	MPlug pl = dn.findPlug(attributename, &stat);
	if( !stat) 
		return MS::kSuccess;
	MObject val;
	if( !pl.getValue(val))
		return MS::kSuccess;

	MFnDoubleArrayData array(val, &stat);
	if(index<0 || index>=(int)array.length())
	{
	}
	else
	{
		array[index] = value;
		pl.setValue(val);
	}

	return MS::kSuccess;
}
/*/
