#include ".\hairutilcmd.h"
#include "HairShape.h"
#include "Util/FileStream.h"
#include "Util/misc_create_directory.h"
#include "Util/currentHostName.h"
#include "MathNMaya/MathNMaya.h"
#include <maya/MFileIO.h>
#include <maya/MFnDoubleIndexedComponent.h>
#include <maya/MFnGeometryData.h>
#include <maya/MItSurfaceCV.h>
#include <maya/MItGeometry.h>
#include "HairControlsShape.h"
#include "HairControlsAnim.h"

#include <maya/MGlobal.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>
#include "Util/misc_create_directory.h"

bool loadCvFromShave(MObject shaveobj, MMatrix shapeinvmat, std::vector< std::vector<Math::Vec3f> >& shavepoints, bool bLoad);

HairUtilCmd::HairUtilCmd(void)
{
}

HairUtilCmd::~HairUtilCmd(void)
{
}

// �������:
// HairUtilCmd
// freeze		- ������ ������ ��� ������� __Pref
// fromshave	- ������� ����������� ����� �� shave
// frommaya		- ������� ����������� ����� �� maya
// tomayahair	- import ����������� ����� � maya
// setkey		- ���� �� �������� ����������� �����
// deletekey	- ������� ���� �� �������� ����������� �����
// saveanim		- ��������� �������� ����������� ����� � ����
// loadanim		- ��������� �������� ����������� ����� �� �����
// saveframe	- ��������� ����������� ������ � ����
// loadframe	- ��������� ����������� ������ �� �����
// cvtotexture  - ��������� ���������� ����������� ����� � ��������
// initcv		- ���������������� ����������� ������ �� ���������� ����������
// buildcache	- ����
// cvmeshop		- �������� cv � ������ 
// 
// 
// 
MSyntax HairUtilCmd::newSyntax()			// ����������� ����� �������������� �����������
{
	MStatus stat;
	MSyntax syntax;

	stat = syntax.addArg(MSyntax::kString);	// cmd
	stat = syntax.addFlag("s", "shape", MSyntax::kString);	// hair shape
	stat = syntax.addFlag("cs", "controlshape", MSyntax::kString);	// hair controls shape
	stat = syntax.addFlag("sh", "shave", MSyntax::kString);	// shave shape
	stat = syntax.addFlag("hs", "hairsystem", MSyntax::kString); //hairsystem
	stat = syntax.addFlag("ft", "follicestr", MSyntax::kString); //follices transform
	stat = syntax.addFlag("oct", "outcurvetransform", MSyntax::kString); //out curve transform
	stat = syntax.addFlag("can", "controlsanim", MSyntax::kString); //controls anim node
	stat = syntax.addFlag("fn", "filename", MSyntax::kString); // filename for anim file
	stat = syntax.addFlag("res", "resolution", MSyntax::kLong); // texture resolution
	stat = syntax.addFlag("at", "attribute", MSyntax::kString); // ��� ���������?
	stat = syntax.addFlag("me", "mesh", MSyntax::kString); // 
	stat = syntax.addFlag("op", "operation", MSyntax::kString); // 
	stat = syntax.addFlag("te", "texture", MSyntax::kString); // 
	return syntax;
}

/*/
Result: message caching isHistoricallyInteresting nodeState binMembership boundingBox boundingBoxMin boundingBoxMinX boundingBoxMinY boundingBoxMinZ boundingBoxMax boundingBoxMaxX boundingBoxMaxY boundingBoxMaxZ boundingBoxSize boundingBoxSizeX boundingBoxSizeY boundingBoxSizeZ center boundingBoxCenterX boundingBoxCenterY boundingBoxCenterZ matrix inverseMatrix worldMatrix worldInverseMatrix parentMatrix parentInverseMatrix visibility intermediateObject template ghosting instObjGroups instObjGroups.objectGroups instObjGroups.objectGroups.objectGrpCompList instObjGroups.objectGroups.objectGroupId instObjGroups.objectGroups.objectGrpColor useObjectColor objectColor drawOverride overrideDisplayType overrideLevelOfDetail overrideShading overrideTexturing overridePlayback overrideEnabled overrideVisibility overrideColor lodVisibility renderInfo identification layerRenderable layerOverrideColor renderLayerInfo renderLayerInfo.renderLayerId renderLayerInfo.renderLayerRenderable renderLayerInfo.renderLayerColor ghostingControl ghostCustomSteps ghostPreSteps ghostPostSteps ghostStepSize ghostFrames ghostRangeStart ghostRangeEnd ghostDriver renderType renderVolume visibleFraction motionBlur visibleInReflections visibleInRefractions castsShadows receiveShadows maxVisibilitySamplesOverride maxVisibilitySamples geometryAntialiasingOverride antialiasingLevel shadingSamplesOverride shadingSamples maxShadingSamples volumeSamplesOverride volumeSamples depthJitter ignoreSelfShadowing primaryVisibility referenceObject compInstObjGroups compInstObjGroups.compObjectGroups compInstObjGroups.compObjectGroups.compObjectGrpCompList compInstObjGroups.compObjectGroups.compObjectGroupId tweak relativeTweak controlPoints controlPoints.xValue controlPoints.yValue controlPoints.zValue weights tweakLocation blindDataNodes uvSet uvSet.uvSetName uvSet.uvSetPoints uvSet.uvSetPoints.uvSetPointsU uvSet.uvSetPoints.uvSetPointsV uvSet.uvSetTweakLocation currentUVSet displayImmediate displayColors displayColorChannel currentColorSet colorSet colorSet.colorName colorSet.clamped colorSet.representation colorSet.colorSetPoints colorSet.colorSetPoints.colorSetPointsR colorSet.colorSetPoints.colorSetPointsG colorSet.colorSetPoints.colorSetPointsB colorSet.colorSetPoints.colorSetPointsA ignoreHwShader doubleSided opposite smoothShading boundingBoxScale boundingBoxScaleX boundingBoxScaleY boundingBoxScaleZ featureDisplacement initialSampleRate extraSampleRate textureThreshold normalThreshold displayHWEnvironment collisionOffsetVelocityIncrement collisionOffsetVelocityIncrement.collisionOffsetVelocityIncrement_Position collisionOffsetVelocityIncrement.collisionOffsetVelocityIncrement_FloatValue collisionOffsetVelocityIncrement.collisionOffsetVelocityIncrement_Interp collisionDepthVelocityIncrement collisionDepthVelocityIncrement.collisionDepthVelocityIncrement_Position collisionDepthVelocityIncrement.collisionDepthVelocityIncrement_FloatValue collisionDepthVelocityIncrement.collisionDepthVelocityIncrement_Interp collisionOffsetVelocityMultiplier collisionOffsetVelocityMultiplier.collisionOffsetVelocityMultiplier_Position collisionOffsetVelocityMultiplier.collisionOffsetVelocityMultiplier_FloatValue collisionOffsetVelocityMultiplier.collisionOffsetVelocityMultiplier_Interp collisionDepthVelocityMultiplier collisionDepthVelocityMultiplier.collisionDepthVelocityMultiplier_Position collisionDepthVelocityMultiplier.collisionDepthVelocityMultiplier_FloatValue collisionDepthVelocityMultiplier.collisionDepthVelocityMultiplier_Interp disableDynamics collisionObjects collisionObjectsGroupID displayNode displayAs displayGuides displaySegmentLimit growthObjectsGroupID hairColorTexture hairColorTextureR hairColorTextureG hairColorTextureB inputCurve inputMesh inputSurface instanceMesh active mutantHairColorTexture mutantHairColorTextureR mutantHairColorTextureG mutantHairColorTextureB neverBeenInstanced nodeVersion shaveVersion outputMesh overrideGeomShader renderState ribStuff rootHairColorTexture rootHairColorTextureR rootHairColorTextureG rootHairColorTextureB runDynamics blindShaveData evalOption amb/diff enableCollision collisionMethod dampening frizzAnim frizzAnimDir animDirX animDirY animDirZ animSpeed displacement frizzXFrequency frizzYFrequency frizzZFrequency rootFrizz tipFrizz geomShadow gloss hairColor hairRed hairGreen hairBlue hairCount hueVariation instancingStatus kinkXFrequency kinkYFrequency kinkZFrequency rootKink tipKink multiStrandCount mutantHairColor mutantHairRed mutantHairGreen mutantHairBlue percentMutantHairs interpolateGuides hairPasses randomizeMulti randScale rootColor rootRed rootGreen rootBlue rootStiffness scale hairSegments selfShadow specular rootSplay tipSplay stiffness rootThickness tipThickness totalGuides valueVariation shaveTex uSubdivisions vSubdivisions time trigger hairUVSetAssignments hairUVSetAssignments.hairUVSetName hairUVSetAssignments.hairUVSetTextures versionLock displayHairMax displayHairRatio textureCacheUpdated guide guide.guideX guide.guideY guide.guideZ hairGroup specularTint specularTintR specularTintG specularTintB tipFade cachedBBox cachedBBoxMin cachedBBoxMin0 cachedBBoxMin1 cachedBBoxMin2 cachedBBoxMax cachedBBoxMax0 cachedBBoxMax1 cachedBBoxMax2 collisionSet growthSet splineLock splineLockTrigger instancingStatusChanged instanceMeshChanged // 

/*/
MStatus HairUtilCmd::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MString cmd;
	argData.getCommandArgument(0, cmd);

	MString shape;
	if( !argData.getFlagArgument("s", 0, shape))
		shape="";

	if(cmd=="buildcache")
	{
		MObject obj;
		if( !nodeFromName(shape, obj))
			return MS::kFailure;
		MFnDependencyNode dn(obj);

		MTypeId tid = dn.typeId();
		if( tid != HairShape::id)
		{
			displayString("cant find hair shape!");
			return MS::kFailure;
		}

		HairShape* shape = NULL;
		shape = (HairShape*)dn.userNode();

		int timeStamp;
		shape->GetHairRootPositionFile(timeStamp, true, false);
		return MS::kSuccess;
	}
	if(cmd=="freeze")
	{
		MObject obj;
		if( !nodeFromName(shape, obj))
			return MS::kFailure;
		MFnDependencyNode dn(obj);

		MTypeId tid = dn.typeId();
		if( tid != HairShape::id)
		{
			displayString("cant find hair shape!");
			return MS::kFailure;
		}

		HairShape* shape = NULL;
		shape = (HairShape*)dn.userNode();

		// File name
		std::string filename = MFileIO::currentFile().asChar();
		size_t n = filename.rfind('/');
		if( n!=filename.npos)
		{
			filename.insert(n+1, ".HairCache//");
		}
		filename += ".";
		std::string dgNodeName = MFnDagNode(obj).name().asChar();
		HairShape::correctFileName( dgNodeName);
		filename += dgNodeName;
		filename += ".freeze";


		Util::create_directory_for_file(filename.c_str());
		Util::FileStream file;
		if( !file.open(filename.c_str(), true))
		{
			displayStringError("cant write file %s!", filename.c_str());
			return MS::kFailure;
		}

		// HairSystem
		HairSystem hairsystem;
		MDGContext context;
		shape->LoadHairSystem(context, hairsystem, "final");

		Math::Box3f boxX;
//		MDGContext context;//(time);
		shape->Save(file, boxX, context, hairsystem);
		file.close();

		displayString("freeze file %s!", filename.c_str());
		MPlug(obj, HairShape::i_FreezeFile).setValue(filename.c_str());
	}
	if(cmd=="fromshave")
	{
		MObject shapeobj;
		if( !nodeFromName(shape, shapeobj))
			return MS::kFailure;
		MFnDagNode dnshapeobj(shapeobj);
		MDagPath shapepath;
		dnshapeobj.getPath( shapepath);
		MMatrix shapeinvmat = shapepath.inclusiveMatrixInverse();

		MString shave;
		if( !argData.getFlagArgument("sh", 0, shave))
			return MS::kFailure;

		MObject shaveobj;
		if( !nodeFromName(shave, shaveobj))
			return MS::kFailure;
		std::vector< std::vector<Math::Vec3f> > shavepoints;
		if( !loadCvFromShave(shaveobj, shapeinvmat, shavepoints, true))
			return MS::kFailure;

		// ���� ������������:
		MPxNode* pxNode = dnshapeobj.userNode(&stat);
		HairControlsShape* pHairControlsShape = (HairControlsShape*)pxNode;

		pHairControlsShape->importCV(shavepoints);
		return MS::kSuccess;
	}
	if(cmd=="toshave")
	{
		MObject shapeobj;
		if( !nodeFromName(shape, shapeobj))
			return MS::kFailure;
		MFnDagNode dnshapeobj(shapeobj);
		MDagPath shapepath;
		dnshapeobj.getPath( shapepath);
		MMatrix shapemat = shapepath.inclusiveMatrix();
		MMatrix shapeinvmat = shapepath.inclusiveMatrixInverse();

		MPxNode* pxNode = dnshapeobj.userNode(&stat);
		HairControlsShape* pHairControlsShape = (HairControlsShape*)pxNode;
		std::vector< std::vector<Math::Vec3f> > shavepoints;
		pHairControlsShape->exportCV(shavepoints, 15);

		MString shave;
		if( !argData.getFlagArgument("sh", 0, shave))
			return MS::kFailure;

		MObject shaveobj;
		if( !nodeFromName(shave, shaveobj))
			return MS::kFailure;
		if( !loadCvFromShave(shaveobj, shapemat, shavepoints, false))
			return MS::kFailure;

		// ���� ������������:
		return MS::kSuccess;
	}
	if(cmd=="frommaya")
	{
		MObject shapeobj;
		if( !nodeFromName(shape, shapeobj))
			return MS::kFailure;
		MDagPath shapepath;
		shapepath = MDagPath::getAPathTo(shapeobj);
		MMatrix shapeinvmat = shapepath.inclusiveMatrixInverse();

		MString hairsystem;
		if( !argData.getFlagArgument("hairsystem", 0, hairsystem))
			return MS::kFailure;
		MObject hairsystemobj;
		if( !nodeFromName(hairsystem, hairsystemobj))
			return MS::kFailure;

		MPlug srcplug = MFnDependencyNode(hairsystemobj).findPlug("outputHair");
//		srcplug.evaluateNumElements();
		int s = srcplug.numElements();
		std::vector< std::vector<Math::Vec3f> > shavepoints(s);
		for(int h=0; h<s; h++)
		{
			MPlug plug = srcplug[h];
			if(plug.isNull())
				continue;

//			printf("%s\n", plug.name().asChar());

			MObject val;
			plug.getValue(val);
			MFnVectorArrayData va(val);
			int valen = va.length();

			std::vector<Math::Vec3f>& array = shavepoints[h];
			array.resize(valen);
			for(int v=0; v<valen; v++)
			{
				MPoint pt = MPoint(va[v]);
				pt = pt*shapeinvmat;
				::copy( array[v], pt);
//				printf("%f, %f, %f\n", pt.x, pt.y, pt.z);
			}
//			fflush(stderr);
		}

		MPxNode* pxNode = MFnDependencyNode( shapeobj).userNode(&stat);
		HairControlsShape* pHairControlsShape = (HairControlsShape*)pxNode;
		pHairControlsShape->importCV(shavepoints);
		return MS::kSuccess;
	}
	if(cmd=="tomayahair")
	{
		MObject shapeobj;
		if( !nodeFromName(shape, shapeobj))
			return MS::kFailure;

		MString hairsystem;
		if( !argData.getFlagArgument("hairsystem", 0, hairsystem))
			return MS::kFailure;
		MObject hairsystemobj;
		if( !nodeFromName(hairsystem, hairsystemobj))
			return MS::kFailure;

		MString follicestr;
		if( !argData.getFlagArgument("follicestr", 0, follicestr))
			return MS::kFailure;
		MObject follicestrobj;
		if( !nodeFromName(follicestr, follicestrobj))
			return MS::kFailure;

		MString oct;
		if( !argData.getFlagArgument("oct", 0, oct))
			return MS::kFailure;
		MObject octobj;
		if( !nodeFromName(oct, octobj))
			return MS::kFailure;


		MPxNode* pxNode = MFnDependencyNode( shapeobj).userNode(&stat);
		HairControlsShape* pHairControlsShape = (HairControlsShape*)pxNode;

		pHairControlsShape->connectMayaHair(hairsystemobj, follicestrobj, octobj);
		return MS::kSuccess;
	}
		
	if(cmd=="setkey")
	{
		MObject shapeobj;
		if( !nodeFromName(shape, shapeobj))
			return MS::kFailure;
		MFnDependencyNode shapedn(shapeobj);
//		HairControlsShape* hcsh = (HairControlsShape*)shapedn.userNode(&stat);
//		MDataBlock data = hcsh->forceCache();
//		HairCV* cv = hcsh->LoadHairCV(data);

		MString anim;
		if( !argData.getFlagArgument("can", 0, anim))
			return MS::kFailure;
		MObject animobj;
		if( !nodeFromName(anim, animobj))
			return MS::kFailure;
		MFnDependencyNode animdn(animobj);
		
		MPlug plugsrc( shapeobj, HairControlsShape::o_HairCV);
//		MPlug plugdstV( shapeobj, HairControlsAnim::i_values);
//		MPlug plugdstK( shapeobj, HairControlsAnim::i_keys);

		MObject val;
		plugsrc.getValue(val);

		MPxNode* pxNode = MFnDependencyNode( animobj).userNode(&stat);
		HairControlsAnim* pHairControlsAnim = (HairControlsAnim*)pxNode;

		MTime t = MAnimControl::currentTime();
		pHairControlsAnim->setKey(t, val);

		return MS::kSuccess;
	}
	if(cmd=="deletekey")
	{
		MObject shapeobj;
		if( !nodeFromName(shape, shapeobj))
			return MS::kFailure;
		MFnDependencyNode shapedn(shapeobj);

		MString anim;
		if( !argData.getFlagArgument("can", 0, anim))
			return MS::kFailure;
		MObject animobj;
		if( !nodeFromName(anim, animobj))
			return MS::kFailure;
		MFnDependencyNode animdn(animobj);

		MPlug plugsrc( shapeobj, HairControlsShape::o_HairCV);
//		MPlug plugdstV( shapeobj, HairControlsAnim::i_values);
//		MPlug plugdstK( shapeobj, HairControlsAnim::i_keys);

		MPxNode* pxNode = MFnDependencyNode( animobj).userNode(&stat);
		HairControlsAnim* pHairControlsAnim = (HairControlsAnim*)pxNode;

		MTime t = MAnimControl::currentTime();
		pHairControlsAnim->deleteKey(t);
		return MS::kSuccess;
	}
	
	if(cmd=="saveanim" || cmd=="saveframe")
	{
		MObject shapeobj;
		if( !nodeFromName(shape, shapeobj))
			return MS::kFailure;
		MFnDependencyNode shapedn(shapeobj);

		MString fn;
		if( !argData.getFlagArgument("fn", 0, fn))
			return MS::kFailure;


		if( cmd=="saveanim")
		{
			MString anim;
			if( !argData.getFlagArgument("can", 0, anim))
				return MS::kFailure;
			MObject animobj;
			if( !nodeFromName(anim, animobj))
				return MS::kFailure;
			MFnDependencyNode animdn(animobj);

			MPxNode* pxNode = MFnDependencyNode( animobj).userNode(&stat);
			HairControlsAnim* pHairControlsAnim = (HairControlsAnim*)pxNode;

			pHairControlsAnim->saveAnim(fn.asChar());
		}
		else
		{
			MPxNode* pxNode = MFnDependencyNode( shapeobj).userNode(&stat);
			HairControlsShape* pHairControlsShape = (HairControlsShape*)pxNode;

			pHairControlsShape->saveFrame(fn.asChar());
		}
		return MS::kSuccess;
	}

	if(cmd=="loadanim" || cmd=="loadframe")
	{
		MObject shapeobj;
		if( !nodeFromName(shape, shapeobj))
			return MS::kFailure;
		MFnDependencyNode shapedn(shapeobj);

		MString fn;
		if( !argData.getFlagArgument("fn", 0, fn))
			return MS::kFailure;

		if( cmd=="loadanim")
		{
			MString anim;
			if( !argData.getFlagArgument("can", 0, anim))
				return MS::kFailure;
			MObject animobj;
			if( !nodeFromName(anim, animobj))
				return MS::kFailure;
			MFnDependencyNode animdn(animobj);

			MPxNode* pxNode = MFnDependencyNode( animobj).userNode(&stat);
			HairControlsAnim* pHairControlsAnim = (HairControlsAnim*)pxNode;

			pHairControlsAnim->loadAnim(fn.asChar());
		}
		else
		{
			MPxNode* pxNode = MFnDependencyNode( shapeobj).userNode(&stat);
			HairControlsShape* pHairControlsShape = (HairControlsShape*)pxNode;

			pHairControlsShape->loadFrame(fn.asChar());
		}
		return MS::kSuccess;
	}
	if(cmd=="cvtotexture")
	{
		MObject shapeobj;
		if( !nodeFromName(shape, shapeobj))
			return MS::kFailure;
		MFnDagNode dnshapeobj(shapeobj);
		MDagPath shapepath;
		dnshapeobj.getPath( shapepath);
		MMatrix shapeinvmat = shapepath.inclusiveMatrixInverse();

		// ��� �����
		MString fn;
		if( !argData.getFlagArgument("fn", 0, fn))
			return MS::kFailure;

		// �������
		MString what;
		if( !argData.getFlagArgument("at", 0, what))
			return MS::kFailure;

		// ����������
		int res = 256;
		if( !argData.getFlagArgument("res", 0, res))
			return MS::kFailure;

		MTypeId tid = dnshapeobj.typeId();
		if( tid == HairShape::id)
		{
			// � ��������
			MPxNode* pxNode = dnshapeobj.userNode(&stat);
			HairShape* shape = (HairShape*)pxNode;

			shape->saveCVtoTexture(what.asChar(), fn.asChar(), res);
			return MS::kSuccess;
		}
		else
		{
			MPlug plug = dnshapeobj.findPlug("inputMesh");
			plug = plug[0];
			MPlugArray plugs;
			plug.connectedTo(plugs, true, false);
			if( plugs.length()==0)
				return MS::kFailure;

			MObject mesh = plugs[0].node();
			MDagPath shapepath;
			shapepath = MDagPath::getAPathTo(mesh);
			MMatrix shapeinvmat = shapepath.inclusiveMatrixInverse();

			std::vector< std::vector<Math::Vec3f> > shavepoints;
			if( !loadCvFromShave(shapeobj, shapeinvmat, shavepoints, true))
				return MS::kFailure;

			HairGeometryIndices geometryindices;
			HairGeometry geometry;
			HairCVPositions cvpositions;
			HairCV cv;

			int CVcount = 3;
			MIntArray brokenEdges;
			if( !HairGeometryIndicesData::Init(geometryindices, mesh, 0, 0, brokenEdges, ""))
				return MS::kFailure;
			if( !HairGeometryData::Init(geometry, mesh, &geometryindices, ""))
				return MS::kFailure;
			if( !HairCVPositionsData::Init(cvpositions, CVcount, &geometryindices))
				return MS::kFailure;
			if( !cv.init(CVcount, &cvpositions))
				return MS::kFailure;

			cv.import(shavepoints, &geometryindices, &geometry, &cvpositions);

			// ������� � ��������
			Math::matrixMxN<Util::rgb> texture;
			cv.export_(
				texture, 
				what.asChar(),			// ����� ��������
				res,					// ����������
				&geometryindices, 
				&geometry,
				&cvpositions
				);

			saveBmp(fn.asChar(), texture);

//			HairMesh hairmesh;
//			hairmesh.init(&geometryindices, &geometry, NULL, NULL, &cvpositions, &cv, NULL);		
			return MS::kSuccess;
		}
	}
	if(cmd=="initcv")
	{
		HairShape* hairshape = NULL;
		{
			MObject shapeobj;
			if( !nodeFromName(shape, shapeobj))
				return MS::kFailure;

			MFnDagNode dnshapeobj(shapeobj);
			MTypeId tid = dnshapeobj.typeId();
			if( tid != HairShape::id)
			{
				return MS::kFailure;
			}

			MPxNode* pxNode = dnshapeobj.userNode(&stat);
			hairshape = (HairShape*)pxNode;
		}

		HairControlsShape* pHairControlsShape = NULL;
		{
			// ����� HairControlsShape
			MString controlsshape;
			if( !argData.getFlagArgument("cs", 0, controlsshape))
				controlsshape="";

			MObject shapeobj;
			if( !nodeFromName(controlsshape, shapeobj))
				return MS::kFailure;

			MFnDagNode dnshapeobj(shapeobj);
			MTypeId tid = dnshapeobj.typeId();
			if( tid != HairControlsShape::id)
			{
				return MS::kFailure;
			}

			MPxNode* pxNode = dnshapeobj.userNode(&stat);
			pHairControlsShape = (HairControlsShape*)pxNode;
		}

		// ������������� cv � pHairControlsShape �� ���������� �� hairshape
		

		MDataBlock datahair = hairshape->forceCache();
		MDataBlock datacontrols = pHairControlsShape->forceCache();

		const HairCVPositions* dstCVpos = pHairControlsShape->LoadHairCVPositions(datacontrols);
		HairCV* dstCV = pHairControlsShape->LoadHairCV(datacontrols);
		
		if( !dstCVpos)
			return MS::kFailure;
		if( !dstCV)
			return MS::kFailure;

		hairshape->exportDeformationToCV(
			datahair, 
			dstCVpos, 
			dstCV
			);

		{
			MDataHandle dHandle = datacontrols.outputValue( pHairControlsShape->o_HairCV, &stat );
			MPlug plug(pHairControlsShape->thisMObject(), pHairControlsShape->o_HairCV);
			plug.setValue(dHandle);
		}
		return MS::kSuccess;
	}

	if(cmd=="cvmeshop")
	{
		MObject shapeobj;
		if( !nodeFromName(shape, shapeobj))
			return MS::kFailure;
		MDagPath shapepath;
		shapepath = MDagPath::getAPathTo(shapeobj);
		MMatrix shapemat = shapepath.inclusiveMatrix();
		MMatrix shapeinvmat = shapepath.inclusiveMatrixInverse();

		MPxNode* pxNode = MFnDependencyNode( shapeobj).userNode(&stat);
		HairControlsShape* pHairControlsShape = (HairControlsShape*)pxNode;


		MString meshname;
		if( !argData.getFlagArgument("mesh", 0, meshname))
			return MS::kFailure;
		MObject meshobj;
		if( !nodeFromName(meshname, meshobj))
			return MS::kFailure;
		MDagPath meshpath;
		meshpath = MDagPath::getAPathTo(meshobj);
		MMatrix meshmat = meshpath.inclusiveMatrix();

		MString meshname2;
		argData.getFlagArgument("mesh2", 0, meshname2);
		MObject meshobj2 = MObject::kNullObj;
		if( meshname2.length())
			if( !nodeFromName(meshname2, meshobj2))
				return MS::kFailure;
		MDagPath meshpath2;
		if(!meshobj2.isNull())
			meshpath2 = MDagPath::getAPathTo(meshobj2);
		MMatrix meshmat2 = meshpath2.inclusiveMatrix();

		MString operation;
		if( !argData.getFlagArgument("operation", 0, operation))
			return MS::kFailure;
		
		MString texturename;
		argData.getFlagArgument("texture", 0, texturename);

		pHairControlsShape->meshOp(operation.asChar(), shapemat, meshobj, meshmat, meshobj2, meshmat2, texturename.asChar());
		return MS::kSuccess;
	}

	return MS::kSuccess;
}

void* HairUtilCmd::creator()
{
	return new HairUtilCmd();
}

bool loadCvFromShave(MObject shaveobj, MMatrix shapeinvmat, std::vector< std::vector<Math::Vec3f> >& shavepoints, bool bLoad)
{
	MStatus stat = MS::kSuccess;

	MFnDagNode dnshaveobj(shaveobj);
	MDagPath shavepath;
	dnshaveobj.getPath( shavepath);
	MMatrix shavemat = shavepath.inclusiveMatrix();

	int guidecount=0;
	dnshaveobj.findPlug("totalGuides").getValue(guidecount);
	if( !guidecount)
		return MS::kFailure;
	char buf[1024];
	sprintf(buf, "%s.cv[0:%d][0:14]", dnshaveobj.name().asChar(), guidecount-1);

	MSelectionList tempList;
	tempList.add(buf);
	
	shavepoints.resize(guidecount);
	if ( tempList.length() > 0 ) 
	{
		MDagPath dagPath;
		MObject component;
		if( tempList.getDagPath( 0, dagPath, component))
		{
//			displayString( component.apiTypeStr());
			MFn::Type type = component.apiType();

			MFnDoubleIndexedComponent dic(component, &stat);
			int gg = dic.elementCount();

			MPxNode* pxNode = MFnDependencyNode(dagPath.node()).userNode(&stat);
			MPxSurfaceShape* ss = (MPxSurfaceShape*)pxNode;
			MSelectionList selectionList;
			ss->componentToPlugs(component, selectionList);
			for( int plug=0; plug<(int)selectionList.length(); plug++) 
			{
				MPlug p;
				int u, v;
				dic.getElement(plug, u, v);
				selectionList.getPlug(plug, p);
				MObject val;
				p.getValue(val);
				MFnNumericData valfata(val);
				valfata.numericType();

				if(bLoad)
				{
					MVector vect;
					valfata.getData(vect.x, vect.y, vect.z);

					MPoint pt = MPoint(vect);
					pt = pt*shapeinvmat;

					// �������� � ������
					if( u>=(int)shavepoints.size())
						continue;
					std::vector<Math::Vec3f>& array = shavepoints[u];
					if( array.empty())
						array.resize(15);

					if(v>=(int)array.size()) 
						continue;

					::copy( array[v], pt);
				}
				else
				{
					std::vector<Math::Vec3f>& array = shavepoints[u];

					MPoint pt;
					::copy(pt, array[v]);
					pt = pt*shapeinvmat;

					MFnNumericData valfata;
					MObject val = valfata.create(MFnNumericData::k3Double);
					valfata.setData(pt.x, pt.y, pt.z);
					p.setValue(val);
				}
			}
		}
	}
	return true;
}
