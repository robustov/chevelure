#include "HairDeformer_ClumpRamp.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnCompoundAttribute.h>

#include <maya/MGlobal.h>

MObject HairDeformer_ClumpRamp::i_Deformer[2];
MObject HairDeformer_ClumpRamp::i_envelope[3];
MObject HairDeformer_ClumpRamp::o_output;
MObject HairDeformer_ClumpRamp::i_PaintResXY[2];

MObject HairDeformer_ClumpRamp::i_rampClump;
MObject HairDeformer_ClumpRamp::i_type;

HairDeformer_ClumpRamp::HairDeformer_ClumpRamp()
{
}
HairDeformer_ClumpRamp::~HairDeformer_ClumpRamp()
{
}

MStatus HairDeformer_ClumpRamp::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;
	displayStringD("HairDeformer_ClumpRamp::compute %s", plug.name().asChar());

	if( plug == this->o_output )
	{
		return ComputeOutput(plug, data, i_Deformer, i_envelope);
	}
	if( plug == i_envelope[1])
		return ComputeTextureCache3(plug, i_envelope, i_PaintResXY, data);

	return MS::kUnknownParameter;
}

void* HairDeformer_ClumpRamp::creator()
{
	return new HairDeformer_ClumpRamp();
}

MStatus HairDeformer_ClumpRamp::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnUnitAttribute unitAttr;
	MFnEnumAttribute enumAttr;
	MStatus				stat;

#ifndef SAS
	if( !HairDeformer::initialize("HairMath", "getClumpRampDeformer", i_Deformer, i_envelope, i_PaintResXY, o_output))
		return MS::kFailure;
#else
	if( !HairDeformer::initialize("chevelureMath", "getClumpRampDeformer", i_Deformer, i_envelope, i_PaintResXY, o_output))
		return MS::kFailure;
#endif
	try
	{
		MString attrLongName =  "rampClump";
		MString attrShortName = "rac";
		i_rampClump = MRampAttribute::createCurveRamp(attrLongName, attrShortName);
		::addAttribute(i_rampClump, atInput);

		i_type = enumAttr.create ("sourceClump", "scl", 0);
		enumAttr.addField("IGNORE", 0);
		enumAttr.addField("PRODUCT", 1);
		::addAttribute(i_type, atInput);

		MObject isAffected = o_output;
		attributeAffects( i_rampClump, isAffected);
		attributeAffects( i_type, isAffected);

		if( !SourceMelFromResource(MhInstPlugin, "AEHairDeformer_ClumpRampTemplate", "MEL", defines))
		{
			displayString("error source AEHairDeformer_ClumpRampTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}
