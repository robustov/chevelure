#pragma once

#include "pre.h"
#include "DrawCache.h"
#include "HairProcessor.h"
//#include "BaseSurfaceSynk.h"
#include "Math/Face.h"
//#include "HairRootPositionsData.h"
#include <maya/MFnIntArrayData.h>
#include <maya/MFloatPointArray.h>
#include "HairMesh.h"
#include "textureCacheData.h"


class MeshSurfaceSynk : public HairMesh
{
public:
	MeshSurfaceSynk();

public:
	virtual Math::Vec3f getBaseColor(double u, double v);
	virtual Math::Vec3f getTipColor(double u, double v);

public:
	enum enType
	{
		T_LENGTH			= 0,
		T_INCLINATION		= 1,
		T_POLAR				= 2,
		T_BASECURL			= 4,
		T_TIPCURL			= 5,
		T_BASEWIDTH			= 6,
		T_TIPWIDTH			= 7,
		T_SCRAGGLE			= 8,
		T_SCRAGGLEFREQ		= 9,
		T_BASECLUMP			= 10,
		T_TIPCLUMP			= 11,
		T_TWIST				= 12,
		T_DISPLACE			= 13,
		T_TWISTFROMPOLAR	= 14,
		T_CUSTOM			= 15,
		T_BLENDCVFACTOR		= 16,
		T_BLENDDEFORMFACTOR	= 17,
		T_MAX				= 18,
	};
	enum enVectorType
	{
		T_BASECOLOR			= 0,
		T_TIPCOLOR			= 1,
		T_VECTORMAX			= 2,
	};

	struct VectorArrayDesc
	{
		std::vector<Math::Vec3f> array;
		int resX, resY;
		textureCacheData* cache;
		VectorArrayDesc(){cache=0;resX=resY=0;}
	};
	std::vector<VectorArrayDesc> vectorarrays;

//	void setArray( enType name, MPxData* data);//textureCacheData
	void setArray2d( enType name, MPxData* data);//textureCacheData
	void setVectorArray( enVectorType name, MPxData* data);//textureCacheData

	Math::Vec3f getVectorValue(enVectorType name, double u, double v);

};

