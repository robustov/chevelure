#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: HairStartBrowserCmdCmd.h
// MEL Command: HairStartBrowserCmd

#include <maya/MPxCommand.h>

class HairUtilCmd : public MPxCommand
{
public:
	static const MString typeName;
	HairUtilCmd();
	virtual	~HairUtilCmd();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();

};
