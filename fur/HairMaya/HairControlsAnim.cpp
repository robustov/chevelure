#include "HairControlsAnim.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>

#include <maya/MGlobal.h>
#include <maya/MFileIO.h>

#include "HairGeometryIndicesData.h"
#include "HairGeometryData.h"
#include "HairCVPositionsData.h"
#include "HairCVData.h"
#include "HairCvBasesData.h"
#include "Util/Stream.h"
#include "Util/FileStream.h"
#include "Util/misc_create_directory.h"
#ifdef USE_ANIMDATA
#include "HairControlsAnimData.h"
#endif// USE_ANIMDATA

MObject HairControlsAnim::i_time;		// Example input attribute
#ifdef USE_HAIRCVARRAY
MObject HairControlsAnim::i_keys;		// Example output attribute
MObject HairControlsAnim::i_values;	// Example output attribute
#endif// USE_HAIRCVARRAY

#ifdef USE_ANIMDATA
MObject HairControlsAnim::i_animData;
MObject HairControlsAnim::i_blendFrame;
MObject HairControlsAnim::i_blendFactor;
#endif// USE_ANIMDATA

MObject HairControlsAnim::o_HairCV;	// Example output attribute

int HairControlsAnim::current_file_version=0;


HairControlsAnim::HairControlsAnim()
{
}
HairControlsAnim::~HairControlsAnim()
{
}

MStatus HairControlsAnim::setKey(MTime t, MObject val)
{
	try
	{
		MDataBlock data = this->forceCache();
#ifdef USE_ANIMDATA
		HairControlsAnimData* animData = LoadAnimData(data);
		if( !animData) return MS::kFailure;
		animData->setKey(t, val);
#endif// USE_ANIMDATA

#ifdef USE_HAIRCVARRAY
		// ����� ����� ���� ��� ��������!!!
		int element = -1;
		MArrayDataHandle cpdata = data.outputArrayValue(i_keys);
		for( unsigned i=0; i<cpdata.elementCount(); i++)
		{
			MTime matchtime = cpdata.outputValue().asTime();
			if( matchtime==t)
			{
				element = cpdata.elementIndex();
				break;
			}
			cpdata.next();
		}

		{
			MArrayDataBuilder builder = cpdata.builder();

			if( element<0) 
			{
				MDataHandle dh = builder.addLast();
				dh.set( MTime(t));
			}
			else
			{
				MDataHandle dh = builder.addElement(element);
				dh.set( MTime(t));
			}
			cpdata.set(builder);
		}
		{
			MArrayDataHandle cpdata = data.outputArrayValue(i_values);
			MArrayDataBuilder builder = cpdata.builder();

			if( element<0) 
			{
				MDataHandle dh = builder.addLast();
				dh.set( val);
			}
			else
			{
				MDataHandle dh = builder.addElement(element);
				dh.set( val);
			}
			cpdata.set(builder);
		}
#endif// USE_HAIRCVARRAY
		return MS::kSuccess;
	}
	catch(...)
	{
		displayStringError("exception in HairControlsAnim::setKey");
	}
	return MS::kFailure;
}

MStatus HairControlsAnim::deleteKey(MTime t)
{
	try
	{
		MDataBlock data = this->forceCache();

#ifdef USE_ANIMDATA
		HairControlsAnimData* animData = LoadAnimData(data);
		if( !animData) return MS::kFailure;
		animData->deleteKey(t);
#endif// USE_ANIMDATA

#ifdef USE_HAIRCVARRAY
		// ����� ����� ���� ��� ��������!!!
		int element = -1;
		MArrayDataHandle cpdata = data.outputArrayValue(i_keys);
		for( unsigned i=0; i<cpdata.elementCount(); i++)
		{
			MTime matchtime = cpdata.outputValue().asTime();
			if( matchtime==t)
			{
				element = cpdata.elementIndex();
				break;
			}
			cpdata.next();
		}
		if( element<0) 
			return MS::kFailure;

		MArrayDataBuilder builder = cpdata.builder();
		builder.removeElement(element);
		cpdata.set(builder);
#endif// USE_HAIRCVARRAY

		return MS::kSuccess;
	}
	catch(...)
	{
		displayStringError("exception in HairControlsAnim::deleteKey");
	}
	return MS::kFailure;
}
MStatus HairControlsAnim::saveAnim(const char* filename)
{
	MStatus stat;
	MDataBlock data = this->forceCache();
#ifdef USE_ANIMDATA
	HairControlsAnimData* animData = LoadAnimData(data);
	if( !animData) return MS::kFailure;
	animData->saveAnim(filename);
#endif// USE_ANIMDATA

#ifdef USE_HAIRCVARRAY
	MArrayDataHandle kdata = data.inputArrayValue(i_keys, &stat);
	MArrayDataHandle vdata = data.inputArrayValue(i_values, &stat);

	Util::FileStream file;
	if( !file.open(filename, true))
	{
		displayStringError("cant open file %s for write", filename);
		return MS::kFailure;
	}
	int version = current_file_version;
	file >> version;

	for( unsigned i=0; i<kdata.elementCount(); i++)
	{
		int index = kdata.elementIndex(&stat);
		MDataHandle kd = kdata.inputValue(&stat);
		if( !stat) continue;
		
		MTime cur = kd.asTime();

		HairCVData* cv1 = NULL;
		stat = vdata.jumpToElement( index);
		if( stat)
		{
			cv1 = (HairCVData*)vdata.inputValue(&stat).asPluginData();

			double t = cur.as(MTime::kSeconds);
			file >> t;
			cv1->cv.serialize(file);
		}

		kdata.next();
	}
	double eof = -66e6;
	file >> eof;
	file.close();
#endif// USE_HAIRCVARRAY
	return MS::kSuccess;
}
MStatus HairControlsAnim::loadAnim(const char* filename)
{
	MStatus stat;
	MDataBlock data = this->forceCache();

#ifdef USE_ANIMDATA
	HairControlsAnimData* animData = LoadAnimData(data);
	if( !animData) return MS::kFailure;
	animData->loadAnim(filename);
#endif// USE_ANIMDATA

#ifdef USE_HAIRCVARRAY
	MArrayDataHandle kdata = data.inputArrayValue(i_keys, &stat);
	MArrayDataHandle vdata = data.inputArrayValue(i_values, &stat);

	double eof = -66e6;

	Util::FileStream file;
	if( !file.open(filename, false))
	{
		displayStringError("cant open file %s", filename);
		return MS::kFailure;
	}
	int version = 0;
	file >> version;
	if( version!=current_file_version)
	{
		displayStringError("invalid file version");
		return MS::kFailure;
	}

	for(;;)
	{
		double t=0;
		file>>t;
		if ( t == eof)
			break;
		MTime cur(t, MTime::kSeconds);

		MFnPluginData fnDataCreator;
		MTypeId tmpid( HairCVData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		HairCVData* pData = static_cast<HairCVData*>(fnDataCreator.data( &stat ));

		pData->cv.serialize(file);

		this->setKey(cur, newDataObject);
	}
#endif// USE_HAIRCVARRAY

	return MS::kSuccess;
}

#ifdef USE_ANIMDATA
HairControlsAnimData* HairControlsAnim::LoadAnimData(MDataBlock& data)
{
	typedef HairControlsAnimData CLASS;
	MObject i_attribute = i_animData;

	MStatus stat;
	MDataHandle inputData = data.inputValue(i_attribute, &stat);
	MPxData* pxdata = inputData.asPluginData();
	CLASS* hgh = (CLASS*)pxdata;
	if( !hgh)
	{
		BuildAnimData(data);
		MDataHandle inputData = data.inputValue(i_attribute, &stat);
		MPxData* pxdata = inputData.asPluginData();
		CLASS* hgh = (CLASS*)pxdata;
		return hgh;
	}
	return hgh;
}
MStatus HairControlsAnim::BuildAnimData(MDataBlock& data)
{
	typedef HairControlsAnimData CLASS;
	MObject i_attribute = i_animData;

	MStatus stat;
	CLASS* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue(i_attribute);
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<CLASS*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( CLASS::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<CLASS*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}
// ����� ���� ��������� ��� �����
MStatus HairControlsAnim::shouldSave( const MPlug& plug, bool& result )
{
	if( plug==i_animData)
	{
		displayStringD("HairControlsAnim::shouldSave %s", plug.name().asChar());

		MDataBlock data = forceCache();
		HairControlsAnimData* cvdata = LoadAnimData(data);

		std::string relative_filename;
		relative_filename += ".HairAnim//";
		std::string filename = MFileIO::currentFile().asChar();
		std::string mbname = Util::extract_filename(filename.c_str());
		relative_filename += mbname;
		relative_filename += ".";
		std::string dgNodeName = MFnDependencyNode(thisMObject()).name().asChar();
		Util::correctFileName( (char*)dgNodeName.c_str());
		relative_filename += dgNodeName;
		relative_filename += ".hairanim";

//		std::string filename = MFileIO::currentFile().asChar();
//		std::string mbname = Util::extract_filename(filename.c_str());
//		filename = Util::extract_foldername(filename.c_str());
//		filename += relative_filename;

		cvdata->relative_filename = relative_filename;

		result = true;
		return MS::kSuccess;
	}
	return MPxNode::shouldSave( plug, result);
}

#endif// USE_ANIMDATA

MStatus HairControlsAnim::computeHairCV( 
	const MPlug& plug,
	MDataBlock& data )
{
	MStatus stat;

	MTime time = data.inputValue(i_time).asTime();

#ifdef USE_ANIMDATA
	HairControlsAnimData* animData = LoadAnimData(data);
	if( !animData) return MS::kFailure;
	HairCV cv;

	MTime blendFrame = data.inputValue(i_blendFrame).asTime();
	double blendFactor = data.inputValue(i_blendFactor).asDouble();

	if( !animData->getCv(time, cv))
	{
		MDataHandle outputData = data.outputValue( o_HairCV, &stat );
		outputData.set(MObject::kNullObj);
		outputData.setClean();
		return MS::kSuccess;
	}
	else
	{
		if( blendFactor!=0)
		{
			HairCV cvforblend;
			if( animData->getCv(blendFrame, cvforblend))
			{
				HairCV cvsrc = cv;
				cv.linear_interpolation(cvsrc, cvforblend, (float)(1-blendFactor), (float)blendFactor);
			}
		}

		HairCVData* pData = NULL;
		bool bNewData = false;
		MDataHandle outputData = data.outputValue( o_HairCV, &stat );
		MPxData* userdata = outputData.asPluginData();
		pData = static_cast<HairCVData*>(userdata);
		MFnPluginData fnDataCreator;
		if( !pData)
		{
			MTypeId tmpid( HairCVData::id );
			MObject newDataObject = fnDataCreator.create( tmpid, &stat );
			pData = static_cast<HairCVData*>(fnDataCreator.data( &stat ));
			bNewData = true;
		}
		pData->cv = cv;

		{
			if( bNewData)
				outputData.set(pData);
			outputData.setClean();
		}
		return MS::kSuccess;
	}

#endif// USE_ANIMDATA

#ifdef USE_HAIRCVARRAY
	HairCVData* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( o_HairCV, &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<HairCVData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( HairCVData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<HairCVData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	// ������������ ���
	int lowbound=-1, upbound=-1; 
	MTime lowtime, uptime;
	MArrayDataHandle kdata = data.inputArrayValue(i_keys, &stat);
	for( unsigned i=0; i<kdata.elementCount(); i++)
	{
		int index = kdata.elementIndex(&stat);
		MDataHandle kd = kdata.inputValue(&stat);
		if( !stat) continue;
		
		MTime cur = kd.asTime();

		if(cur>=time)
		{
			// ��� ����� ���� uptime
			if(upbound<0 || uptime>cur)
			{
				uptime = cur;
				upbound = index;
			}
		}
		if(cur<=time)
		{
			// ��� ����� ���� uptime
			if(lowbound<0 || lowtime<cur)
			{
				lowtime = cur;
				lowbound = index;
			}
		}

		double t = cur.as(MTime::uiUnit());
//		displayString("Time=%f, index = %d", t, index);
		kdata.next();
	}
	// ����� 2 ��� 1 ���� ��� ������������
	if(upbound<0)
	{
		upbound = lowbound;
		uptime  = lowtime;
	}
	if(lowbound<0)
	{
		lowbound = upbound;
		lowtime  = uptime;
	}
	if(lowbound<0)
	{
		// ��� ������
		outputData.set(MObject::kNullObj);
		outputData.setClean();
		return MS::kSuccess;
	}
	else
	{
		MArrayDataHandle vdata = data.inputArrayValue(i_values, &stat);

		HairCVData* cv1 = NULL;
		stat = vdata.jumpToElement( lowbound);
		if( stat)
			cv1 = (HairCVData*)vdata.inputValue(&stat).asPluginData();

		HairCVData* cv2 = NULL;
		stat = vdata.jumpToElement( upbound);
		if( stat)
			cv2 = (HairCVData*)vdata.inputValue(&stat).asPluginData();
		if(!cv1 || !cv2)
		{
			// ������
			outputData.set(MObject::kNullObj);
			outputData.setClean();
			return MS::kSuccess;
		}
		if(uptime!=lowtime)
		{
			float f = (float)((time-lowtime).as(MTime::kSeconds)/(uptime-lowtime).as(MTime::kSeconds));

			// ���������������!!!
			pData->cv = cv1->cv;
			for(int h=0; h<pData->cv.getCount(); h++)
			{
				HairCV::CV& ch1 = pData->cv.getControlHair(h);
				if(h>=cv1->cv.getCount())
					break;
				HairCV::CV& ch2 = cv2->cv.getControlHair(h);
				if( ch2.vc()!=ch1.vc())
					continue;
				for( int v=0; v<ch1.vc(); v++)
				{
					ch1.ts[v] = ch1.ts[v]*(1-f) + ch2.ts[v]*f;
				}
			}
		}
		else
		{
			pData->cv = cv1->cv;
		}
		if( bNewData)
			outputData.set(pData);
		outputData.setClean();
		return MS::kSuccess;
	}
#endif// USE_HAIRCVARRAY

}

MStatus HairControlsAnim::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;
	displayStringD("HairControlsAnim::compute %s", plug.name().asChar());

	try
	{
		if( plug == o_HairCV )
		{
			return computeHairCV( plug, data);
		}
	}
	catch(...)
	{
		displayStringError("exception in HairControlsAnim::compute %s", plug.name().asChar());
	}
	return MS::kUnknownParameter;
}

#if MAYA_API_VERSION > 800
bool HairControlsAnim::isPassiveOutput( const MPlug& plug ) const
{
	if( plug==o_HairCV)
		return true;
	return MPxNode::isPassiveOutput( plug );
}
#endif


void* HairControlsAnim::creator()
{
	return new HairControlsAnim();
}

MStatus HairControlsAnim::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnUnitAttribute	unitAttr;
	MFnTypedAttribute	typedAttr;
	MStatus				stat;

	try
	{
		{
			i_time = unitAttr.create ("time","tm", MTime(0.0));
			::addAttribute(i_time, atInput);
		}
#ifdef USE_HAIRCVARRAY
		{
			i_keys = unitAttr.create ("keys","ks", MTime(0.0));
		 	stat = unitAttr.setUsesArrayDataBuilder( true );
			::addAttribute(i_keys, atInput|atArray);
		}
		{
			i_values = typedAttr.create( "values", "vals", HairCVData::id);
		 	stat = typedAttr.setUsesArrayDataBuilder( true );
			::addAttribute(i_values, atInput|atArray);
			if (!stat) return stat;
		}
#endif// USE_HAIRCVARRAY

#ifdef USE_ANIMDATA
		{
			i_animData = typedAttr.create( "animData", "ad", HairControlsAnimData::id);
			::addAttribute(i_animData, atInput|atHidden);
			if (!stat) return stat;

			i_blendFrame = unitAttr.create ("blendFrame","blfr", MTime(1.0));
			::addAttribute(i_blendFrame, atInput);

			i_blendFactor = numAttr.create("blendFactor", "blfa", MFnNumericData::kDouble, 0);
			numAttr.setMin(0);
			numAttr.setMax(1);
			::addAttribute(i_blendFactor, atInput);
		}
#endif// USE_ANIMDATA
		{
			o_HairCV = typedAttr.create( "hairCV", "hcv", HairCVData::id);
			::addAttribute(o_HairCV, atReadable|atWritable|atConnectable|atStorable|atCached);			
			if (!stat) return stat;
		}
		{
			MObject affect = o_HairCV;
			attributeAffects(i_time, affect);
#ifdef USE_HAIRCVARRAY
			attributeAffects(i_keys, affect);
			attributeAffects(i_values, affect);
#endif// USE_HAIRCVARRAY

#ifdef USE_ANIMDATA
			attributeAffects(i_animData, affect);
			attributeAffects(i_blendFrame, affect);
			attributeAffects(i_blendFactor, affect);

#endif// USE_ANIMDATA
		}

		if( !SourceMelFromResource(MhInstPlugin, "AEHAIRCONTROLSANIMTEMPLATE.MEL", "MEL", defines))
		{
			displayString("error source AEHairControlsAnimTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

