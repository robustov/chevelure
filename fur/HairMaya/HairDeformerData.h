#pragma once

#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include "Util/Stream.h"
#include "Util/MemStream.h"
#include "Util/DllProcedure.h"
#include "IHairDeformer.h"

/*/
	USING:
	{
		MFnTypedAttribute fnAttr;
		MObject newAttr = fnAttr.create( 
			"fullName", "briefName", HairDeformerData::id );
	}
/*/


class HairDeformerData : public MPxData
{
public:
	HairDeformerData();
	virtual ~HairDeformerData(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

public:
	static MTypeId id;
	static const MString typeName;

	// ����������+���������
	Util::DllProcedureTmpl<getIHairDeformer> deformerproc;
	// ������ ��� ���������
	Util::MemStream stream;

protected:
	void serialize(Util::Stream& stream);
};
