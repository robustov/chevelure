//
// Copyright (C) 
// File: HairGeometryIndicesDataCmd.cpp
// MEL Command: HairGeometryIndicesData

#include "HairGeometryIndicesData.h"

#include <maya/MGlobal.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include "Math/Polygon.h"
#include <sstream>

#include "MathNMaya/MathNMaya.h"
#include <maya/MFnMesh.h>
#include <maya/MFnDynSweptGeometryData.h>
#include <maya/MDynSweptTriangle.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatMatrix.h>
#include <maya/MRenderUtil.h>

// �������������
bool HairGeometryIndicesData::Init(
	MObject obj, 
	int seed, 
	int nSubdiv,
	MIntArray& brokenEdges, 
	MString uvSetName)
{
	return Init(
		this->hgh, 
		obj, 
		seed, 
		nSubdiv,
		brokenEdges, 
		uvSetName);
}

// �������������
bool HairGeometryIndicesData::Init(
	HairGeometryIndices& hgh, 
	MObject obj, 
	int seed, 
	int nSubdiv,
	MIntArray& brokenEdges, 
	MString uvSetName)
{
	// ��� subdiv
	int facevertexcount = 0;
	std::vector<Math::Polygon32> sd_faces, sd_uvfaces, sd_nfaces;

	std::vector<std::string> uvs_Names;
	std::vector<MString> uvs_NamesMaya;
	std::vector<int> uvs_count;
	std::vector< std::vector<Math::Polygon32> > uvs_sd_uvfaces;

	hgh.setValid();
	if(obj.hasFn(MFn::kMesh))
	{
		MFnMesh mesh(obj);

		if( uvSetName.length()==0)
			mesh.getCurrentUVSetName(uvSetName);
		bool bUVSetValid = true;
		int uvcount = mesh.numUVs(uvSetName);
		if(!uvcount)
		{
			displayString("Hair: UV Set \"%s\" is empty", uvSetName.asChar());
			bUVSetValid = false, uvcount = mesh.numVertices();
		}
		// add uv!
		MStringArray meshUVSetNames;
		mesh.getUVSetNames(meshUVSetNames);
		int set;
		for(set=0; set<(int)meshUVSetNames.length(); set++)
		{
			MString name = meshUVSetNames[set];
			if( name==uvSetName) continue;
			int uvcount = mesh.numUVs(name);
			if( !uvcount) continue;

			uvs_Names.push_back(name.asChar());
			uvs_NamesMaya.push_back(name);
			uvs_count.push_back(uvcount);
			uvs_sd_uvfaces.push_back(std::vector<Math::Polygon32>());

			displayStringD("uv set %d: %s", uvs_Names.size()-1, name.asChar());
		}

		// ���������!
		// �����! ���� ��������� brokenEdges
		if( hgh.isValid(mesh.numVertices(), mesh.numPolygons(), mesh.numNormals(), uvcount, nSubdiv, uvSetName.asChar(), uvs_Names.size(), 
			uvs_Names.empty()?NULL:&*uvs_Names.begin(), 
			uvs_count.empty()?NULL:&*uvs_count.begin()))
			return false;	// ��� ���������!

		hgh.init(
			mesh.numVertices(), mesh.numPolygons(), mesh.numNormals(), 
			uvcount, nSubdiv, uvSetName.asChar(), 
			uvs_Names.size(), 
			uvs_Names.empty()?NULL:&uvs_Names[0], 
			uvs_count.empty()?NULL:&uvs_count[0]);

		hgh.faces.clear();
		hgh.faces.reserve(mesh.numPolygons()*3);
		hgh.faces_uv.clear();
		hgh.faces_uv.reserve(mesh.numPolygons()*3);
		hgh.faces_n.clear();
		hgh.faces_n.reserve(mesh.numPolygons()*3);
		hgh.faces_polygonIndex.clear();
		hgh.faces_polygonIndex.reserve(mesh.numPolygons()*3);
		for( set=0; set<(int)uvs_sd_uvfaces.size(); set++)
		{
			hgh.uvSets[set].faces_uv.clear();
			hgh.uvSets[set].faces_uv.reserve(mesh.numPolygons()*3);
		}

		// ��� subdiv
		if( nSubdiv!=0)
		{
			sd_faces.resize(mesh.numPolygons());
			sd_uvfaces.resize(mesh.numPolygons());
			sd_nfaces.resize(mesh.numPolygons());
			for( set=0; set<(int)uvs_sd_uvfaces.size(); set++)
				uvs_sd_uvfaces[set].resize(mesh.numPolygons());
		}
		// ������ ��������
		MItMeshPolygon polyIter(obj);
		for(polyIter.reset(); !polyIter.isDone(); polyIter.next())
		{
			int polyIndex = polyIter.index();

			MIntArray vertices, uvindicies, nindicies;
			std::vector<MIntArray> uvs_uvindicies(uvs_sd_uvfaces.size());
			polyIter.getVertices( vertices );
			// uv
			unsigned z;
			if( bUVSetValid)
			{
				uvindicies.setLength(vertices.length());
				for(z=0; z<vertices.length(); z++)
				{
					int index=0;
					MStatus stat = polyIter.getUVIndex(z, index, &uvSetName);
					uvindicies[z] = index;
				}
			}
			else
			{
				// ������ UVSet
				uvindicies = vertices;
			}
			// normal
			nindicies.setLength(vertices.length());
			for(z=0; z<vertices.length(); z++)
				nindicies[z] = polyIter.normalIndex(z);

			// add uv
			for( set=0; set<(int)uvs_sd_uvfaces.size(); set++)
			{
				MIntArray& uvindicies = uvs_uvindicies[set];
				uvindicies.setLength(vertices.length());
				MString& uvSetName = uvs_NamesMaya[set];
				for(z=0; z<vertices.length(); z++)
				{
					int index=0;
					MStatus stat = polyIter.getUVIndex(z, index, &uvSetName);
					uvindicies[z] = index;
				}
			}

			// ��� subdiv
			if( nSubdiv!=0)
			{
				unsigned vc = vertices.length();
				facevertexcount += vc;
				sd_faces[polyIndex].resize(vc);
				sd_uvfaces[polyIndex].resize(vc);
				sd_nfaces[polyIndex].resize(vc);
				for( set=0; set<(int)uvs_sd_uvfaces.size(); set++)
					uvs_sd_uvfaces[set][polyIndex].resize(vc);

				for(unsigned v=0; v<vc; v++)
				{
					sd_faces[polyIndex][v]   = vertices[v];
					sd_uvfaces[polyIndex][v] = uvindicies[v];
					sd_nfaces[polyIndex][v]  = nindicies[v];
					for( set=0; set<(int)uvs_sd_uvfaces.size(); set++)
						uvs_sd_uvfaces[set][polyIndex][v] = uvs_uvindicies[set][v];
				}
			}

			int tngCount = 0;
			polyIter.numTriangles(tngCount);
			for(int hh = 0; hh<tngCount; hh++)
			{
				MPointArray FaceVertex;
				MIntArray FaceVtxTrngl;
				polyIter.getTriangle(hh, FaceVertex, FaceVtxTrngl);

				Math::Face32 f( FaceVtxTrngl[0], FaceVtxTrngl[1], FaceVtxTrngl[2]);
				hgh.faces.push_back(f);

				// ������ ��������� ��������
				int inds[3];
				for(unsigned vi=0; vi<FaceVtxTrngl.length(); vi++)
					for(unsigned d=0; d<vertices.length(); d++)
						if( vertices[d]==FaceVtxTrngl[vi])
							inds[vi] = d;

				// faces_uv
				Math::Face32 fuv(uvindicies[inds[0]], uvindicies[inds[1]], uvindicies[inds[2]]);
				hgh.faces_uv.push_back(fuv);
				// faces_n
				Math::Face32 fn(nindicies[inds[0]], nindicies[inds[1]], nindicies[inds[2]]);
				hgh.faces_n.push_back(fn);
				// add uv
				for( set=0; set<(int)uvs_sd_uvfaces.size(); set++)
				{
					MIntArray& uvindicies = uvs_uvindicies[set];
					Math::Face32 fuv(uvindicies[inds[0]], uvindicies[inds[1]], uvindicies[inds[2]]);
					hgh.uvSets[set].faces_uv.push_back(fuv);
				}
				hgh.faces_polygonIndex.push_back(polyIndex);
			}
		}

		// ��� subdiv
		if( nSubdiv!=0)
		{
			if( sd_faces == sd_uvfaces)
				sd_uvfaces.clear();
			if( sd_faces == sd_nfaces)
				sd_nfaces.clear();
			for(unsigned i=0; i<uvs_sd_uvfaces.size(); i++)
				if( uvs_sd_uvfaces[i] == sd_faces)
					uvs_sd_uvfaces[i].clear();
		}

		// brokenEdges
		for( int be=0; be<(int)brokenEdges.length(); be++)
		{
			int2 edgeverts;
			mesh.getEdgeVertices(brokenEdges[be], edgeverts);
			hgh.brokenEdges.insert(Math::Edge32(edgeverts[0], edgeverts[1]));
		}

	}
	else if(obj.hasFn(MFn::kDynSweptGeometryData))
	{
		MFnDynSweptGeometryData swept(obj);
		int poligoncount = swept.triangleCount();

		if( hgh.isValid(poligoncount*3, poligoncount, poligoncount*3, poligoncount*3, nSubdiv, "", 0, NULL, NULL))
			return false;	// ��� ���������!

		hgh.init(poligoncount*3, poligoncount, poligoncount*3, poligoncount*3, nSubdiv, "", 0, NULL, NULL);

		// ��� subdiv
		if( nSubdiv!=0)
		{
			sd_faces.resize(poligoncount);
		}

		// faces
		hgh.faces.reserve(poligoncount);
		hgh.faces_uv.reserve(poligoncount);
		hgh.faces_n.reserve(poligoncount);
		hgh.faces_polygonIndex.reserve(poligoncount);

		for(int i=0; i<poligoncount; i++)
		{
			MDynSweptTriangle tri = swept.sweptTriangle(i);
			Math::Face32 face(i*3+0, i*3+1, i*3+2);
			hgh.faces.push_back(face);
			hgh.faces_n.push_back(face);
			hgh.faces_uv.push_back(face);
			hgh.faces_polygonIndex.push_back(i);
			// ��� subdiv
			if( nSubdiv!=0)
			{
				sd_faces[i].resize(3);
				sd_faces[i][0] = face.v[0];
				sd_faces[i][1] = face.v[1];
				sd_faces[i][2] = face.v[2];
			}
		}

	}
	else 
		return true;


	// ��������� ����� subdivide 
	if( nSubdiv!=0)
	{
		hgh.buildSubdivisionScheme(nSubdiv, facevertexcount, sd_faces, sd_uvfaces, sd_nfaces);

		for(unsigned i=0; i<hgh.uvSetCount(); i++)
		{
			hgh.buildSubdivisionSchemeForUv( nSubdiv, facevertexcount, i, uvs_sd_uvfaces[i]);
		}
	}

	// buildAssociated
	hgh.buildAssociated();
	// seeds
	srand( seed);
	hgh.faceseeds.clear();
	hgh.faceseeds.resize(hgh.faces.size());
	for(unsigned f=0; f<hgh.faces.size(); f++)
		hgh.faceseeds[f] = ~rand();
	return true;
}

HairGeometryIndicesData::HairGeometryIndicesData()
{
}
HairGeometryIndicesData::~HairGeometryIndicesData()
{
}

MTypeId HairGeometryIndicesData::typeId() const
{
	return HairGeometryIndicesData::id;
}

MString HairGeometryIndicesData::name() const
{ 
	return HairGeometryIndicesData::typeName; 
}

void* HairGeometryIndicesData::creator()
{
	return new HairGeometryIndicesData();
}

void HairGeometryIndicesData::copy( const MPxData& other )
{
	HairGeometryIndicesData* arg = (HairGeometryIndicesData*)&other;
	hgh = arg->hgh;
}

MStatus HairGeometryIndicesData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("HairClumpPositionsData: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	if( !this->serialize(stream)) 
		return MS::kFailure;

	return MS::kSuccess;
}

MStatus HairGeometryIndicesData::writeASCII( 
	std::ostream& out )
{
// ����� ����� ��� �������� ������ � �������� ��������!!!!!!
//return MS::kFailure;

    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus HairGeometryIndicesData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	if( !serialize(stream))
		return MS::kFailure;

	return MS::kSuccess;
}

MStatus HairGeometryIndicesData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

bool HairGeometryIndicesData::serialize(Util::Stream& stream)
{
	hgh.serialize(stream);
	return true;
}

