//
// Copyright (C) 
// File: HairDeformerDataCmd.cpp
// MEL Command: HairDeformerData

#include "HairDeformerData.h"

#include <maya/MGlobal.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include <sstream>


HairDeformerData::HairDeformerData()
{
//	displayString("HairDeformerData 0x%x", this);
}
HairDeformerData::~HairDeformerData()
{
//	displayString("~HairDeformerData 0x%x", this);
}

MTypeId HairDeformerData::typeId() const
{
	return HairDeformerData::id;
}

MString HairDeformerData::name() const
{ 
	return HairDeformerData::typeName; 
}

void* HairDeformerData::creator()
{
	return new HairDeformerData();
}

void HairDeformerData::copy( const MPxData& other )
{
	const HairDeformerData* arg = (const HairDeformerData*)&other;
	this->deformerproc = arg->deformerproc;
	this->stream = arg->stream;
}

MStatus HairDeformerData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("HairDeformerData: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	this->serialize(stream);

	return MS::kSuccess;
}

MStatus HairDeformerData::writeASCII( 
	std::ostream& out )
{
    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus HairDeformerData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	serialize(stream);

	return MS::kSuccess;
}

MStatus HairDeformerData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

void HairDeformerData::serialize(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version>=0)
	{
		stream >> this->deformerproc;
		this->stream.serialize(stream);
	}
}

