//
// Copyright (C) 
// File: HairGeometryDataCmd.cpp
// MEL Command: HairGeometryData

#include "HairGeometryData.h"
#include <maya/MFnDynSweptGeometryData.h>
#include <maya/MDynSweptTriangle.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MFloatPointArray.h>
#include "MathNMaya/MathNMaya.h"
#include "Math\MeshUtil.h"

#include <maya/MGlobal.h>
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include "MathNMaya\MathNMaya.h"
#include <sstream>

bool HairGeometryData::Init(
	MObject obj, 
	HairGeometryIndices* geometryIndices, 
	MString uvSetName)
{
	return HairGeometryData::Init(geometry, obj, geometryIndices, uvSetName);
}

bool HairGeometryData::Init(
	HairGeometry& geometry, 
	MObject obj, 
	HairGeometryIndices* geometryIndices, 
	MString uvSetName)
{
	geometry.setValid();
	geometry.clear();
	if(obj.hasFn(MFn::kMesh))
	{
		MFnMesh surface(obj);

		MFloatPointArray vertexArray;
		MFloatVectorArray normals;
		MFloatArray uArray, vArray;
		surface.getPoints(vertexArray);
		surface.getNormals(normals);

		if( uvSetName.length()==0)
			surface.getCurrentUVSetName(uvSetName);
		surface.getUVs(uArray, vArray, &uvSetName);
		int uvcount = uArray.length();
		if( uvcount==0)
		{
			// ������ UVSet
			uvcount = vertexArray.length();
			uArray.setLength(uvcount);
			vArray.setLength(uvcount);
			for(unsigned x=0; x<vertexArray.length(); x++)
			{
				uArray[x] = vertexArray[ x ].x;
				vArray[x] = vertexArray[ x ].y;
			}
		}
		std::vector<MFloatArray> uvSet_uArray(geometryIndices->uvSetCount());
		std::vector<MFloatArray> uvSet_vArray(geometryIndices->uvSetCount());
		std::vector<int> uvSet_counts(geometryIndices->uvSetCount());
		int i;
		for( i=0; i<(int)geometryIndices->uvSetCount(); i++)
		{
			MString name = geometryIndices->uvSetName(i);
			surface.getUVs(uvSet_uArray[i], uvSet_vArray[i], &name);
			uvSet_counts[i] = uvSet_uArray[i].length();
		}

		geometry.resize(vertexArray.length(), normals.length(), uvcount, &uvSet_counts);

		int x;
		for(x=0; x<(int)vertexArray.length(); x++)
			::copy( geometry.vertex(x), vertexArray[x]);

		for(x=0; x<(int)normals.length(); x++)
			::copy( geometry.normal(x), normals[x]);

		for(x=0; x<(int)uArray.length(); x++)
		{
			Math::Vec2f& uv = geometry.uv(x);
			uv.x = uArray[ x ];
			uv.y = vArray[ x ];
		}
		for( i=0; i<(int)geometryIndices->uvSetCount(); i++)
		{
			MFloatArray& uArray = uvSet_uArray[i];
			MFloatArray& vArray = uvSet_vArray[i];
			for(x=0; x<(int)uArray.length(); x++)
			{
				Math::Vec2f& uv = geometry.uvSet(x, i);
				uv.x = uArray[ x ];
				uv.y = vArray[ x ];
			}
		}
	}
	else if(obj.hasFn(MFn::kDynSweptGeometryData))
	{
		MFnDynSweptGeometryData swept(obj);
		int poligoncount = swept.triangleCount();

		geometry.resize(poligoncount*3, poligoncount*3, poligoncount*3, NULL);
		for(int i=0; i<poligoncount; i++)
		{
			MDynSweptTriangle tri = swept.sweptTriangle(i);
			for(int v=0; v<3; v++)
			{
				MPoint pt = tri.vertex(v);
				::copy( geometry.vertex(i*3+v), pt);
				::copy( geometry.uv(i*3+v), tri.uvPoint(v));
//				::copy( normals[i*3+v], tri.normalToPoint(pt.x, pt.y, pt.z));
				::copy( geometry.normal(i*3+v), tri.normal());
			}
		}
	}
	else 
		return false;

	geometry.subdivide(geometryIndices);

	// Tangent space
	geometry.calcTangentSpace(geometryIndices);
	// Bounds
	geometry.calcBounds();

	return true;
}

HairGeometryData::HairGeometryData()
{
}
HairGeometryData::~HairGeometryData()
{
}

MTypeId HairGeometryData::typeId() const
{
	return HairGeometryData::id;
}

MString HairGeometryData::name() const
{ 
	return HairGeometryData::typeName; 
}

void* HairGeometryData::creator()
{
	return new HairGeometryData();
}

void HairGeometryData::copy( const MPxData& other )
{
	const HairGeometryData* arg = (const HairGeometryData*)&other;
	this->geometry = arg->geometry;
}

MStatus HairGeometryData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("HairClumpPositionsData: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	if( !this->serialize(stream)) 
		return MS::kFailure;

	return MS::kSuccess;
}

MStatus HairGeometryData::writeASCII( 
	std::ostream& out )
{
// ����� ����� ��� �������� ������ � �������� ��������!!!!!!
//return MS::kFailure;

    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus HairGeometryData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	if( !serialize(stream))
		return MS::kFailure;

	return MS::kSuccess;
}

MStatus HairGeometryData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

bool HairGeometryData::serialize(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version>=0)
	{
		geometry.serialize(stream);
	}
	return true;
}

