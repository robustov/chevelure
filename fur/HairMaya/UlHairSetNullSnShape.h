#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: UlHairSetNullSnShapeCmd.h
// MEL Command: UlHairSetNullSnShape

#include <maya/MPxCommand.h>

class UlHairSetNullSnShape : public MPxCommand
{
public:
	static const MString typeName;
	UlHairSetNullSnShape();
	virtual	~UlHairSetNullSnShape();

	MStatus	doIt( const MArgList& );

	bool isUndoable() const{return false;};
	static MSyntax newSyntax();
	static void* creator();
};

