#pragma once

#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include "Util/Stream.h"

/*/
	USING:
	{
		MFnTypedAttribute fnAttr;
		MObject newAttr = fnAttr.create( 
			"fullName", "briefName", HairCvBasesData::id );
	}
/*/


class HairCvBasesData : public MPxData
{
public:
	HairCvBasesData();
	virtual ~HairCvBasesData(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

	// ������ ��� CH
	std::vector< Math::Matrix4f> bases;		// ts->os
	std::vector< Math::Matrix4f> bases_inv;	// os->ts

public:
	static MTypeId id;
	static const MString typeName;

protected:
	void serialize(Util::Stream& stream);
};
