#include "HairDeformer_LenghtRamp.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnCompoundAttribute.h>

#include <maya/MGlobal.h>

MObject HairDeformer_LenghtRamp::i_Deformer[2];
MObject HairDeformer_LenghtRamp::i_envelope[4];
MObject HairDeformer_LenghtRamp::o_output;
MObject HairDeformer_LenghtRamp::i_PaintResXY[2];

MObject HairDeformer_LenghtRamp::i_lenghtClump;
MObject HairDeformer_LenghtRamp::i_useU;

HairDeformer_LenghtRamp::HairDeformer_LenghtRamp()
{
}
HairDeformer_LenghtRamp::~HairDeformer_LenghtRamp()
{
}

MStatus HairDeformer_LenghtRamp::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;
	displayStringD("HairDeformer_LenghtRamp::compute %s", plug.name().asChar());

	if( plug == this->o_output )
	{
		return ComputeOutput(plug, data, i_Deformer, i_envelope);
	}
	if( plug == i_envelope[1])
		return ComputeTextureCache3(plug, i_envelope, i_PaintResXY, data);

	return MS::kUnknownParameter;
}

void* HairDeformer_LenghtRamp::creator()
{
	return new HairDeformer_LenghtRamp();
}

MStatus HairDeformer_LenghtRamp::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnUnitAttribute unitAttr;
	MFnEnumAttribute enumAttr;
	MStatus				stat;

#ifndef SAS
	if( !HairDeformer::initialize("HairMath", "getLenghtRampDeformer", i_Deformer, i_envelope, i_PaintResXY, o_output))
		return MS::kFailure;
#else
	if( !HairDeformer::initialize("chevelureMath", "getLenghtRampDeformer", i_Deformer, i_envelope, i_PaintResXY, o_output))
		return MS::kFailure;
#endif
	try
	{
		MString attrLongName =  "lenghtClump";
		MString attrShortName = "ral";
		i_lenghtClump = MRampAttribute::createCurveRamp(attrLongName, attrShortName);
		::addAttribute(i_lenghtClump, atInput);

		i_useU = numAttr.create ("useU", "usu", MFnNumericData::kBoolean, 1);
		::addAttribute(i_useU, atInput);

		MObject isAffected = o_output;
		attributeAffects( i_lenghtClump, isAffected);
		attributeAffects( i_useU, isAffected);

		if( !SourceMelFromResource(MhInstPlugin, "AEHAIRDEFORMER_LENGHTRAMPTEMPLATE.MEL", "MEL", defines))
		{
			displayString("error source AEHairDeformer_LenghtRampTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}
