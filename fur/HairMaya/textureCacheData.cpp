//
// Copyright (C) 
// File: textureCacheDataCmd.cpp
// MEL Command: textureCacheData

#include "textureCacheData.h"
#include <maya/MRenderUtil.h>
#include <maya/MFloatMatrix.h>
#include <maya/MAnimUtil.h>
#include <sstream>

#include <maya/MGlobal.h>


// loadFloatParam
bool textureCacheData::loadFloatParam(
	Math::ParamTex2d<unsigned char>& param2d, 
	const MPlug& src, 
	int resX, int resY, float jitter, const char* uvsetname
	)
{
	param2d.reset();
	double d; src.getValue(d);
	param2d.min = 0; param2d.max = 1;
	param2d.defValue = (float)d;
	param2d.jitter = jitter;
	param2d.uvsetname = uvsetname?uvsetname:"";

	MPlugArray plar;
	src.connectedTo(plar, true, false);
	if( plar.length()==1)
	{
		MPlug plug = plar[0];

		// FILE
		MObject shader = plug.node();
		param2d.filename = textureCacheData::isFileName(shader);

		if(!param2d.filename.empty()) 
		{
			MFnDependencyNode dn(shader);
			double alphaGain;
			dn.findPlug("alphaGain").getValue(alphaGain);
			double alphaOffset;
			dn.findPlug("alphaOffset").getValue(alphaOffset);
			param2d.offset = (float)alphaOffset;
			param2d.factor = (float)alphaGain;
		}

		bool bLoadSampler = true;
		if(MGlobal::mayaState()!=MGlobal::kInteractive && !param2d.filename.empty())
		{
			// �� ������� ���� ��� ������
			bLoadSampler = false;
		}
		if( bLoadSampler && plug.node().hasFn( MFn::kTexture2d))
		{
			if( MGlobal::mayaState()!=MGlobal::kInteractive)
			{
				fprintf(stderr,  "Fur: Stand alone maya -> dont read sampling\n");
				fprintf( stderr, "     else MRenderUtil::sampleShadingNetwork may broken\n");
				fflush(stderr);
				return true;
			}

			MFloatArray uCoords(resX*resY), vCoords(resX*resY);
			// read from SHADER
			for(int u=0; u<resX; u++)
				for(int v=0; v<resY; v++)
				{
					int ind = u*resY + v;
					uCoords[ind] = (float)(u+0.5)/resX;
					vCoords[ind] = (float)(v+0.5)/resY;
				}

			MFloatMatrix cameraMat;
			MFloatVectorArray colors(resX*resY), transps(resX*resY);
			bool res;
			res = MRenderUtil::sampleShadingNetwork( 
					plug.name(), resX*resY,
					false, false, cameraMat,
					NULL,
					&uCoords, &vCoords,
					NULL, NULL, NULL, NULL, NULL,
					colors, transps );
			if( colors.length()!=resX*resY)
				res = false;
			if(!res)
				displayStringError("load texture for %s FAILED", plug.name().asChar());

			if( res && colors.length()==resX*resY)
			{
				param2d.max = -1e6; 
				param2d.min = 1e6;
				for(int i=0; i<(int)colors.length(); i++)
				{
					float d = (colors[i][0]+colors[i][1]+colors[i][2])/3;
					param2d.max = __max(param2d.max, d);
					param2d.min = __min(param2d.min, d);
				}

				if( param2d.max == param2d.min) 
				{
					param2d.defValue = param2d.max;
					param2d.min = param2d.defValue-0.5f; param2d.max = param2d.defValue+0.5f;
					return true;
				}

				param2d.matrix = new Math::matrixMxN<unsigned char>();
				param2d.matrix->init(resX, resY);
				param2d.bNeedDelmatrix = true;

				for(int x=0, i=0; x<resX; x++)
				{
					for(int y=0; y<resY; y++, i++)
					{
						float d = (colors[i][0]+colors[i][1]+colors[i][2])/3;
						unsigned int f = (unsigned int)( 255.f*( (d - param2d.min)/(param2d.max - param2d.min)));
						f = __max( 0, __min(255, f));
						unsigned char c = (unsigned char)f;
						(*param2d.matrix)[y][x] = c;

	//					printf("[%d][%d]=%d\n", x, y, (int)c);
					}
				}
			}
		}
	}
	return true;
}

// ������ �� �����
bool textureCacheData::loadFloatParamFromFile(
	Math::ParamTex2d<unsigned char>& param2d, 
	const MPlug& src, int resX, int resY, 
	float jitter, const char* uvsetname
	)
{
printf("loadFloatParamFromFile: %d %d\n", resX, resY);
fflush(stdout);

	param2d.reset();
	double d; src.getValue(d);
	param2d.min = 0; param2d.max = 1;
	param2d.defValue = (float)d;
	param2d.jitter = jitter;
	param2d.uvsetname = uvsetname?uvsetname:"";

	MPlugArray plar;
	src.connectedTo(plar, true, false);
	if( plar.length()==1)
	{
		MPlug plug = plar[0];

		// FILE
		MObject shader = plug.node();
		param2d.filename = textureCacheData::isFileName(shader);

		if(!param2d.filename.empty()) 
		{
			MFnDependencyNode dn(shader);
			double alphaGain;
			dn.findPlug("alphaGain").getValue(alphaGain);
			double alphaOffset;
			dn.findPlug("alphaOffset").getValue(alphaOffset);
			param2d.offset = (float)alphaOffset;
			param2d.factor = (float)alphaGain;

			param2d.matrix = new Math::matrixMxN<unsigned char>;
			Math::LoadMatrixFromIFF(param2d.filename.c_str(), *param2d.matrix);
			param2d.bNeedDelmatrix = true;
		}
		if( param2d.filename.empty() && plug.node().hasFn( MFn::kTexture2d))
		{
			if( MGlobal::mayaState()!=MGlobal::kInteractive)
			{
				fprintf(stderr,  "Fur: Stand alone maya -> dont read sampling\n");
				fprintf( stderr, "     else MRenderUtil::sampleShadingNetwork may broken\n");
				fflush(stderr);
				return true;
			}


			MFloatArray uCoords(resX*resY), vCoords(resX*resY);
			// read from SHADER
			for(int u=0; u<resX; u++)
				for(int v=0; v<resY; v++)
				{
					int ind = u*resY + v;
					uCoords[ind] = (float)(u+0.5)/resX;
					vCoords[ind] = (float)(v+0.5)/resY;
				}

			MFloatMatrix cameraMat;
			MFloatVectorArray colors, transps;
			bool res;
			res = MRenderUtil::sampleShadingNetwork( 
					plug.name(), resX*resY,
					false, false, cameraMat,
					NULL,
					&uCoords, &vCoords,
					NULL, NULL, NULL, NULL, NULL,
					colors, transps );
			if( colors.length()!=resX*resY)
				res = false;
			if(!res)
				displayStringError("load texture for %s FAILED", plug.name().asChar());

			if( res && colors.length()==resX*resY)
			{
				param2d.max = -1e6; 
				param2d.min = 1e6;
				for(int i=0; i<(int)colors.length(); i++)
				{
					float d = (colors[i][0]+colors[i][1]+colors[i][2])/3;
					param2d.max = __max(param2d.max, d);
					param2d.min = __min(param2d.min, d);
				}

				if( param2d.max == param2d.min) 
				{
					param2d.defValue = param2d.max;
					param2d.min = param2d.defValue-0.5f; param2d.max = param2d.defValue+0.5f;
					return true;
				}

				param2d.matrix = new Math::matrixMxN<unsigned char>();
				param2d.matrix->init(resX, resY);
				param2d.bNeedDelmatrix = true;

				for(int x=0, i=0; x<resX; x++)
				{
					for(int y=0; y<resY; y++, i++)
					{
						float d = (colors[i][0]+colors[i][1]+colors[i][2])/3;
						unsigned int f = (unsigned int)( 255.f*( (d - param2d.min)/(param2d.max - param2d.min)));
						f = __max( 0, __min(255, f));
						unsigned char c = (unsigned char)f;
						(*param2d.matrix)[y][x] = c;

	//					printf("[%d][%d]=%d\n", x, y, (int)c);
					}
				}
			}
		}
	}
	return true;
}


/*/
void textureCacheData::setFloats(const MPlug& src, MDataBlock& data, int resX, int resY, double jitter)
{
	defVec = Math::Vec3f();
	textureub.clear();
//	paramTex2d.clear();

	data.inputValue(src).asDouble();
	loadFloatParam(param2d, src, resX, resY, (float)jitter);
}
/*/

void textureCacheData::setFloats(const MPlug& src, MDataBlock& data, int resX, int resY, double jitter, const char* uvsetname)
{
	defVec = Math::Vec3f();
	textureub.clear();
//	param2d.clear();
//	paramTex2d.clear();

	data.inputValue(src).asDouble();
	loadFloatParam(paramTex2d, src, resX, resY, (float)jitter, uvsetname);
}
void textureCacheData::setVectors(const MPlug& src, MDataBlock& data, int resX, int resY)
{
//	param2d.reset();
	paramTex2d.reset();

	defVec = Math::Vec3f();
	textureub.clear();
//	paramTex2d.clear();
//	this->filename = "";

	MFloatVector c = data.inputValue(src).asFloatVector();
	defVec = Math::Vec3f(c[0], c[1], c[2]);

	MPlugArray plar;
	src.connectedTo(plar, true, false);
	if( plar.length()==1)
	{
		MFloatArray uCoords(resX*resY), vCoords(resX*resY);
		for(int u=0; u<resX; u++)
			for(int v=0; v<resY; v++)
			{
				int ind = u*resY + v;
				uCoords[ind] = (float)(u+0.5)/resX;
				vCoords[ind] = (float)(v+0.5)/resY;
			}

		MPlug plug = plar[0];
		MFloatMatrix cameraMat;
		MFloatVectorArray colors, transps;
		if( MRenderUtil::sampleShadingNetwork( 
				plug.name(), resX*resY,
				false, false, cameraMat,
				NULL,
				&uCoords, &vCoords,
				NULL, NULL, NULL, NULL, NULL,
				colors, transps ) )
		{
			this->textureub.init(resX, resY);
			for(int x=0, i=0; x<resX; x++)
				for(int y=0; y<resY; y++, i++)
				{
//					Math::Vec3ub c(
//						colors[i][0]*255.f, 
//						colors[i][1]*255.f, 
//						colors[i][2]*255.f);
					Math::Vec3f c( colors[i][0], colors[i][1], colors[i][2]);
					this->textureub[y][x] = c;
				}
		}
	}
}

float textureCacheData::getFloat(double u, double v)
{
	return paramTex2d.getValue((float)u, (float)v);
}
Math::Vec3f textureCacheData::getVector(double u, double v)
{
	if( textureub.empty())
		return defVec;
//	Math::Vec3ub c = textureub.interpolation((float)u, (float)v, false, false);
//	Math::Vec3f d( c.x/255.f, c.y/255.f, c.z/255.f);
	Math::Vec3f d = textureub.interpolation((float)u, (float)v, false, false);
	return d;
}

std::string textureCacheData::isFileName(MObject shader)
{
	MStatus stat;
	if( !shader.hasFn(MFn::kFileTexture))
		return "";

	MString texturePath;
	MRenderUtil::exactFileTextureName(shader, texturePath);
	return texturePath.asChar();

/*/
	MPlugArray array;
	MAnimUtil::findAnimatedPlugs(shader, array, false);
	if( array.length()!=0) 
	{
		displayStringD("zry connect %s", array[0].name().asChar());
		return "";
	}

	MFnDependencyNode dn(shader);

	MString filename;
	dn.findPlug("fileTextureName").getValue(filename);

	return filename.asChar();
/*/
}

textureCacheData::textureCacheData()
{
}
textureCacheData::~textureCacheData()
{
}

MTypeId textureCacheData::typeId() const
{
	return textureCacheData::id;
}

MString textureCacheData::name() const
{ 
	return textureCacheData::typeName; 
}

void* textureCacheData::creator()
{
	return new textureCacheData();
}

void textureCacheData::copy( const MPxData& other )
{
	textureCacheData* arg = (textureCacheData*)&other;

//	this->param2d = arg->param2d;

	this->paramTex2d = arg->paramTex2d;

	this->defVec = arg->defVec;
	this->textureub = arg->textureub;
}

MStatus textureCacheData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("HairClumpPositionsData: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	if( !this->serialize(stream)) 
		return MS::kFailure;

	return MS::kSuccess;
}

MStatus textureCacheData::writeASCII( 
	std::ostream& out )
{
// ����� ����� ��� �������� ������ � �������� ��������!!!!!!
//return MS::kFailure;

    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus textureCacheData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	if( !serialize(stream))
		return MS::kFailure;
		
	return MS::kSuccess;
}

MStatus textureCacheData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);
    return MS::kSuccess;
}

bool textureCacheData::serialize(Util::Stream& stream)
{
	int version = 3;
	stream >> version;
	if( version==2)
	{
		Math::ParamTex2d<unsigned char> param2d;
		stream >> param2d;
//		stream >> this->defVal;
//		stream >> this->min; 
//		stream >> this->max;
//		stream >> this->texture;
		stream >> this->defVec;
		stream >> this->textureub;
//		stream >> this->filename;
//		stream >> this->offset;
//		stream >> this->factor;
//		return true;
		if(version<3)
		{
			Math::ParamTex2d<unsigned char>* pOld = &param2d;

			this->paramTex2d = *((Math::ParamTex<unsigned char>*)pOld);
		}
//		printf("Hair::textureCacheData loading version 2\n");
	}
	if( version==3)
	{
		stream >> this->paramTex2d;
		stream >> this->defVec;
		stream >> this->textureub;
//		printf("Hair::textureCacheData loading version 3\n");
	}
	return true;
}
