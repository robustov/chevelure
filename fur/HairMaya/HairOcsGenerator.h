#pragma once
#include "ocellaris/IGeometryGenerator.h"
#include "ocellaris/IDeformGenerator.h"
#include "ocellaris/nodeProxy.h"

//! @ingroup implement_group
//! \brief ��������� ��� HairShape
//! 
struct HairOcsGenerator : public cls::IGeometryGenerator
{
	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		)
	{
		return "HairOcsGenerator";
	}

	//! ����������� ���� � �������� 
	virtual bool OnPrepareNode(
		cls::INode& node, 
		cls::IExportContext* context,
		MObject& generatornode
		);

	bool OnGeometry(
		cls::INode& node,				//!< ������ 
		cls::IRender* render, 
		Math::Box3f& box, 
		cls::IExportContext* context,
		MObject& generatornode			//!< generator node �.� 0
		);
};
extern HairOcsGenerator hairOcsGenerator;

/*/
//! @ingroup implement_group
//! \brief ������ ��������� ��� HairShape �� �������� ���������
//!
struct HairSkinFilterDeformGenerator : public cls::IDeformGenerator
{
	// � ����� ������ ��������
	virtual int GetCallType(
		MObject& generatornode){return cls::IDeformGenerator::CT_BEFORE_GEOMETRY;};

	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "HairSkinFilterDeformGenerator";};

	//! ��������� ���������� �������� �������� ���� ������� ����� �������������� ��������� ��������� 
	virtual void OnSetupNode(
		cls::INode& node, 
		cls::IExportContext* context,
		MObject& generatornode			//!< generator node �.� 0
		);

	//! render ����� ���� = NULL
	virtual void OnGeometry(
		cls::INode& inode,				//!< ������ 
		cls::IRender* render,				//!< render
		cls::enSystemCall drawCallType,		//!< ��� ������ ��������� ��������: I_POINTS, I_CURVES, I_MESH ...
		cls::IExportContext* context, 
		MObject& generatornode			//!< generator node �.� 0
		);
};

//! @ingroup implement_group
//! \brief ������ ��������� ��� HairShape �� ������� ���������
//!
struct HairExternVNDeformGenerator : public cls::IDeformGenerator
{
	// � ����� ������ ��������
	virtual int GetCallType(
		MObject& generatornode){return cls::IDeformGenerator::CT_BEFORE_GEOMETRY;};

	// ���������� ���, ������ ��� �������������
	virtual const char* GetUniqueName(
		){return "HairExternVNDeformGenerator";};

	//! render ����� ���� = NULL
	virtual void OnGeometry(
		cls::INode& inode,				//!< ������ 
		cls::IRender* render,				//!< render
		cls::enSystemCall drawCallType,		//!< ��� ������ ��������� ��������: I_POINTS, I_CURVES, I_MESH ...
		cls::IExportContext* context, 
		MObject& generatornode			//!< generator node �.� 0
		);
};
/*/
