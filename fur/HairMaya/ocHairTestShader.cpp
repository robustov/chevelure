#include "stdafx.h"
#include "ocHairTestShader.h"
#include "ocellaris/OcellarisExport.h"
#include "ocellaris/shaderAssemblerImpl.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnStringData.h>
#include <maya/MFnStringArrayData.h>

#include <maya/MGlobal.h>

MObject ocHairTestShader::space;				// ="world";
MObject ocHairTestShader::viewN;
MObject ocHairTestShader::KdAmbient;
MObject ocHairTestShader::Kd1;					// �� ������� 1
MObject ocHairTestShader::Kd2;					// �� ������� 2
MObject ocHairTestShader::Kd3;					// �� ������� 3
MObject ocHairTestShader::Knond;				// ����������� ����
MObject ocHairTestShader::Sc;					// Surface color
MObject ocHairTestShader::opacityMinForShadow;
MObject ocHairTestShader::opacityMaxForShadow;

MObject ocHairTestShader::i_UVCoord;
MObject ocHairTestShader::o_color;
MObject ocHairTestShader::o_alpha;

ocHairTestShader::ocHairTestShader()
{
}
ocHairTestShader::~ocHairTestShader()
{
}

MStatus ocHairTestShader::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;

	if( plug == o_color || plug.parent() == o_color )
	{
		float2& uv = data.inputValue( i_UVCoord ).asFloat2();

		MFloatVector resultColor(0, 0, 0);
		resultColor.x = uv[0] - floor( uv[0]);
		resultColor.y = uv[1] - floor( uv[1]);

		MDataHandle outputData = data.outputValue( o_color, &stat );
	    MFloatVector& outColor = outputData.asFloatVector();
	    outColor = resultColor;
	    outputData.setClean();

		return MS::kSuccess;
	}
	return MS::kUnknownParameter;
}

void* ocHairTestShader::creator()
{
	return new ocHairTestShader();
}

MStatus ocHairTestShader::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnEnumAttribute	enumAttr;
	MStatus				stat;
	MFnStringData stringData;

	try
	{
		{
			space = typedAttr.create( "space", "space",
				MFnData::kString, stringData.create("world", &stat), &stat );
			::addAttribute(space, atInput);
		}
		{
			viewN = enumAttr.create("view", "vi", 0);
			enumAttr.addField("COLOR",	0);
			enumAttr.addField("N1",		1);
			enumAttr.addField("N2",		2);
			enumAttr.addField("N3",		3);
			::addAttribute(viewN, atInput);
		}
		{
			KdAmbient = numAttr.create("KdAmbient", "KdAmbient", MFnNumericData::kDouble, 0.);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(1);
			::addAttribute(KdAmbient, atInput);
		}
		{
			Kd1 = numAttr.create("Kd1", "Kd1", MFnNumericData::kDouble, 0.);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(1);
			::addAttribute(Kd1, atInput);
		}
		{
			Kd2 = numAttr.create("Kd2", "Kd2", MFnNumericData::kDouble, 0.);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(1);
			::addAttribute(Kd2, atInput);
		}
		{
			Kd3 = numAttr.create("Kd3", "Kd3", MFnNumericData::kDouble, 0.);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(1);
			::addAttribute(Kd3, atInput);
		}
		{
			Knond = numAttr.create("Knond", "Knond", MFnNumericData::kDouble, 0.);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(1);
			::addAttribute(Knond, atInput);
		}
		{
			Sc = numAttr.createColor( "Sc", "Sc");
			numAttr.setDefault(1.0, 1.0, 1.0);
			::addAttribute(Sc, atInput);
		}
		{
			opacityMinForShadow = numAttr.create("opacityMinForShadow", "opacityMinForShadow", MFnNumericData::kDouble, 0.1);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(1);
			::addAttribute(opacityMinForShadow, atInput);
		}
		{
			opacityMaxForShadow = numAttr.create("opacityMaxForShadow", "opacityMaxForShadow", MFnNumericData::kDouble, 0.4);
			numAttr.setMin(0.0);
			numAttr.setSoftMax(1);
			::addAttribute(opacityMaxForShadow, atInput);
		}
		

		{
			MObject child1 = numAttr.create( "uCoord", "u", MFnNumericData::kFloat);
			MObject child2 = numAttr.create( "vCoord", "v", MFnNumericData::kFloat);
			i_UVCoord = numAttr.create( "uvCoord", "uv", child1, child2);
			::addAttribute(i_UVCoord, atInput|atHidden);
		}
		{
			o_color = numAttr.createColor( "outColor", "oc");
			numAttr.setDisconnectBehavior(MFnAttribute::kReset);
			numAttr.setUsedAsColor(false);
			::addAttribute(o_color, atReadable|atUnWritable|atConnectable|atUnStorable|atUnCached);

			stat = attributeAffects( i_UVCoord, o_color );
		}
		{
			o_alpha = numAttr.create( "outAlpha", "oa", MFnNumericData::kDouble, 0);
			::addAttribute(o_alpha, atReadable|atUnWritable|atConnectable|atUnStorable|atUnCached);

			stat = attributeAffects( i_UVCoord, o_alpha);
		}
		if( !SourceMelFromResource(MhInstPlugin, "AEocHairTestShaderTemplate.mel", "MEL", defines))
		{
			displayString("error source AEocHairTestShaderTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

ocHairTestShader::Generator ocHairTestShader::generator;

// ��� ����
cls::IShadingNodeGenerator::enShadingNodeType ocHairTestShader::Generator::getShadingNodeType(
	MObject& node					//!< ������ (shader)
	)
{
	return cls::IShadingNodeGenerator::SURFACESHADER;
}

#define output(STRING) (as->Add(STRING))
//! ��������� ��������� 
//! ��������� ��������� ��������� ��� �������
bool ocHairTestShader::Generator::BuildShader(
	MObject& node,					//!< ������
	cls::enParamType wantedoutputtype, //!< ����� ��� ����� �� ������
	cls::IShaderAssembler* as,		//!< render
	cls::IExportContext* context,	//!< context
	MObject& generatornode			//!< generator node �.� 0
	)
{
	as->ExternParameter("SN", cls::P<Math::Vec3f>(Math::Vec3f(0, 1, 0)), cls::PT_NORMAL);
	as->ExternParameter("clump_normal", cls::P<Math::Vec3f>(Math::Vec3f(0, 1, 0)), cls::PT_VECTOR);
	as->ExternParameter("clump_vector", cls::P<Math::Vec3f>(Math::Vec3f(0, 0, 1)), cls::PT_VECTOR);
	as->ExternParameter("hairId", cls::P<float>(0), cls::PT_FLOAT);

	output ("extern normal SN;");
	output ("extern vector clump_normal;");
	output ("extern vector clump_vector;");
	output ("extern float hairId;");

	as->InputParameter( "space",				"space",				cls::PT_STRING);
	as->InputParameter( "view",					"viewN",				cls::PT_INT);
	as->InputParameter( "KdAmbient",			"KdAmbient",			cls::PT_FLOAT);
	as->InputParameter( "Kd1",					"Kd1",					cls::PT_FLOAT);
	as->InputParameter( "Kd2",					"Kd2",					cls::PT_FLOAT);
	as->InputParameter( "Kd3",					"Kd3",					cls::PT_FLOAT);
	as->InputParameter( "Knond",				"Knond",				cls::PT_FLOAT);
	as->InputParameter( "Sc",					"Sc",					cls::PT_COLOR);
	as->InputParameter( "opacityMinForShadow",	"opacityMinForShadow",	cls::PT_FLOAT);
	as->InputParameter( "opacityMaxForShadow",	"opacityMaxForShadow",	cls::PT_FLOAT);


	output("	// ������� 1								");
	output("	normal Ns = normalize(N);					");
	output("	Ns = faceforward(Ns, I);					");
	output("												");
	output("	// ������� 2								");
	output("	normal SurfaceN = normalize(SN);			");
	output("												");
	output("	// ������� 3 ������������ dPdv				");
	output("	vector toClumPV = -normalize(clump_vector);	");
	output("	vector dir = dPdv;							");	
	output("	float dotdir = toClumPV.dir;				");	
	output("	toClumPV -= dotdir*dir;						");
	output("	toClumPV = normalize(toClumPV);				");
	output("												");
	output("	color col=0;								");

	output("												");
	output("	illuminance(\"-environment\", P, Ns, 3.14,	");
	output("		\"lightcache\", \"reuse\")				");
	output("	{											");
	output("		float dot = normalize(L).Ns;			");
	output("		if(dot>0)								");
	output("			col += Cl * dot * Kd1;				");
	output("	}											");
	output("												");

	output("												");
	output("	illuminance(\"-environment\", P, Ns, 3.14,	");
	output("		\"lightcache\", \"reuse\")				");
	output("	{											");
	output("	float dot = normalize(L).SurfaceN;			");
	output("	if(dot>0)									");
	output("		col += Cl * dot * Kd2;					");
	output("	}											");

	output("	illuminance(\"-environment\", P, Ns, 3.14,	");
	output("		\"lightcache\", \"reuse\")				");
	output("	{											");
	output("	float dot = normalize(L).toClumPV;			");
	output("	if(dot>0)									");
	output("		col += Cl * dot * Kd3;					");
	output("	}											");

	output("	illuminance(\"-environment\", P, Ns, 3.14,	");
	output("		\"lightcache\", \"reuse\")				");
	output("	{											");
	output("	col += Cl * Knond;											");
	output("	}															");

	output("																");
	output("	col += KdAmbient;											");
	output("	col *= Sc;													");

	output("																");
	output("	if( viewN==1)												");
	output("	{															");
	output("		normal nn = ntransform(space, Ns);						");
	output("		color c = color(xcomp(nn), ycomp(nn), zcomp(nn) );		");
	output("		col = c*0.5 + color(0.5);								");
	output("	}															");
	output("	if( viewN==2)												");
	output("	{															");
	output("		normal nn = ntransform(space, SurfaceN);				");
	output("		color c = color(xcomp(nn), ycomp(nn), zcomp(nn) );		");
	output("		col = c*0.5 + color(0.5);								");
	output("	}															");
	output("	if( viewN==3)												");
	output("	{															");
	output("		vector nn = vtransform(space, toClumPV);				");
	output("		color c = color(xcomp(nn), ycomp(nn), zcomp(nn) );		");
	output("		col = c*0.5 + color(0.5);								");
	output("	}															");

	output("																");
	output("Ci = col;														");

	output("																");
	output("																");
	output("	color op = 1;												");
	output("																");
	output("	float isshadow=0; 											");
	output("	float _isshadow=0;											");
	output("	if(option(\"user:RATFilterLightSource\", _isshadow) != 0) 	");
	output("		isshadow = _isshadow;									");
	output("																");
	output("	if( isshadow!=0) 											");
	output("	{															");
	output("		float T = normalize(I).normalize(dPdv);					");
	output("		T = abs(T);												");
	output("		op = mix( opacityMinForShadow, opacityMaxForShadow, T);	");
	output("	}															");
	output("	Oi = op;													");

	return true;
}


