#include "HairRootPositionsData.h"

#include <maya/MGlobal.h>
#include <maya/MFloatMatrix.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MRenderUtil.h>
#include "Math/interpolation.h"
#include "MathNMaya/MathNMaya.h"
#include "Math/meshutil.h"
#include <maya/MFnDynSweptGeometryData.h>
#include <maya/MDynSweptTriangle.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include "MathNMaya/MayaFileStream.h"
#include <maya/MFileIO.h>
#include <maya/MProgressWindow.h>
#include "Util/FileStream.h"
#include "Util/EvalTime.h"
#include <sstream>
#include "textureCacheData.h"

// ������������� (��������� ���)
bool HairRootPositionsData::Init(
	HairRootPositions& hp, 
	MObject thisObject, 
	HairClumpPositions* hc, 
	HairGeometryIndices* hgh, 
	HairGeometry* geometry, 
	int count, 
	bool bEqualize,
	int maxCount, 
	int seed, 
	int resX, int resY,
	float CLUMPblending, 
	bool bProgress, 
	float maxClumpRadius,				// ����. ������ ������ - �� ��� ������ - �� � ������
	enClumpAlgorithm algorithm,			// �������� ��������
	const std::set<int>& workingfaces)
{
	bProgress &= (!::hair_suppressProgress) && (MGlobal::mayaState()==MGlobal::kInteractive);
	MFnDependencyNode dn(thisObject);

	hp.clear();

	double fullarea;
	double area = calcArea(hgh, geometry, workingfaces, fullarea);

	if( maxCount!=0)
	{
		maxCount = (int) ( maxCount * area/fullarea);
		if( count>maxCount) count = maxCount;
	}

	// ��������� �����
	srand( seed);
	std::vector<Math::Vec2f> posuv;
	MPlug densityplug = dn.findPlug("Density");
	MPlug densitypluguvset = dn.findPlug("densityUVset");

	generate(hgh, geometry, hp.hairs, hp.hairseeds, hp.hairgroups, posuv, 
		densityplug, densitypluguvset, bEqualize, 
		area, count, seed, 
		resX, resY, 
		bProgress?"generate hairs":NULL, 
		workingfaces);

	// �������� ����� � clump
	if( !hc->clumps.empty())
	{
		BindHairsToClumps(hp, hc, hgh, geometry, CLUMPblending, posuv, fullarea, bProgress, maxClumpRadius, algorithm);
	}

	return true;
}
// ������������� (���������� ���)
bool HairRootPositionsData::Init(
	HairRootPositions& hp, 
	MObject thisObject, 
	HairClumpPositions* hc, 
	HairGeometryIndices* hgh, 
	HairGeometry* geometry, 
	Math::Vec2i countUV, 
	Math::Vec2f offset,
	Math::Vec2f shear,
	Math::Vec2f jitter,
	int seed, 
	int count,						// ������ ��� ������������, ����� -1
	int resX, int resY,
	float CLUMPblending, 
	bool bProgress, 
	float maxClumpRadius,				// ����. ������ ������ - �� ��� ������ - �� � ������
	enClumpAlgorithm algorithm,			// �������� ��������
	const std::set<int>& workingfaces)
{
	bool checkWorkingFaces = !workingfaces.empty();

	bProgress &= (!::hair_suppressProgress) && (MGlobal::mayaState()==MGlobal::kInteractive);
	MFnDependencyNode dn(thisObject);

	hp.clear();

	// area UV
	double areaUV=0, fullareaUV=0;
	for(unsigned f=0; f<hgh->faceCount(); f++)
	{
		Math::Face32& face = hgh->faceUV(f);
		Math::Vec2f &pt0 = geometry->uv(face.v[0]);
		Math::Vec2f &pt1 = geometry->uv(face.v[1]);
		Math::Vec2f &pt2 = geometry->uv(face.v[2]);
		Math::Vec2f v10 = pt1 - pt0;
		Math::Vec2f v20 = pt2 - pt0;
		float a = fabs( v10.x*v20.y - v10.y*v20.x)/2;
		fullareaUV += a;
		bool checkWorkingFaces = !workingfaces.empty();
		if( checkWorkingFaces)
			if( workingfaces.find(hgh->face_polygonIndex(f))==workingfaces.end())
				continue;
		areaUV += a;
	}

	double fullarea;
	double area = calcArea(hgh, geometry, workingfaces, fullarea);

	// ��������� �����
	srand( seed);
	std::vector<Math::Vec2f> posuv;

	if( bProgress)
	{
		MProgressWindow::reserve();
		MProgressWindow::setInterruptable(false);
		MProgressWindow::setTitle("Hair file cache");
		MProgressWindow::startProgress();
		MProgressWindow::setProgressRange(0, hgh->faceCount());
		MProgressWindow::setProgressStatus("generate hairs");
	}

	Math::ParamTex2d<unsigned char> density;
	MPlug densityplug = dn.findPlug("Density");
	MPlug densitypluguvset = dn.findPlug("densityUVset");
	 
	MString densityuvsetname;
	densitypluguvset.getValue(densityuvsetname);

	textureCacheData::loadFloatParamFromFile(density, densityplug, resX, resY, 0, densityuvsetname.asChar());
	int uvsetind = hgh->finduvSetByName(densityuvsetname.asChar());

//	count *- 1/sumdensitytex;

	// ���������
	int maxCount = (int)(areaUV*(float)countUV.x*(float)countUV.y);
	float dencityFactor = 10;
	if(count>0)
		dencityFactor = count/(float)maxCount;
	maxCount = 100 + (int)(maxCount*1.5);
	
	float dx = 1.f/countUV.x;
	float dy = 1.f/countUV.y;

	offset.x = offset.x*dx;
	offset.y = offset.y*dy;

	Math::Matrix3f shearmat = Math::Matrix3f::id;
	shearmat[0][1] = shear.x*dy/dx;
	shearmat[1][0] = shear.y*dx/dy;
	Math::Matrix3f shearmatinv = shearmat.inverted();

	std::vector< HairGeometryIndices::pointonsurface>& data = hp.hairs;
	std::vector< int>& hairseeds							= hp.hairseeds;
	std::vector< HairGeometryIndices::facegroup>& groups	= hp.hairgroups;
	std::vector< Math::Vec2f>& uvs							= posuv;

	uvs.clear();
	data.clear();
	data.reserve(maxCount);
	hairseeds.reserve(maxCount);
	uvs.reserve(maxCount);
	groups.resize(hgh->faceCount());
	std::set<Math::Vec2i> exists;
	std::map< Math::Vec2i, int> rand_by_position;
	for(unsigned f=0; f<hgh->faceCount(); f++)
	{
		if( checkWorkingFaces)
			if( workingfaces.find(hgh->face_polygonIndex(f))==workingfaces.end())
				continue;

		if(bProgress)
			MProgressWindow::setProgress(f);

		HairGeometryIndices::facegroup& facegroup = groups[f];
		facegroup.startindex = data.size();
		facegroup.endindex = data.size();
		srand( hgh->faceSeed(f)+seed);
		Math::Face32& face = hgh->face(f);

		// verts
		const Math::Vec3f &pt0 = geometry->vertex(face.v[0]);
		const Math::Vec3f &pt1 = geometry->vertex(face.v[1]);
		const Math::Vec3f &pt2 = geometry->vertex(face.v[2]);

		// uvs
		Math::Face32& face_uv = hgh->faceUV(f);
		Math::Vec2f uv[3];
		uv[0] = geometry->uv( face_uv.v[0] );
		uv[1] = geometry->uv( face_uv.v[1] );
		uv[2] = geometry->uv( face_uv.v[2] );

		Math::Box2f box;
		box.insert(shearmatinv*uv[0]);
		box.insert(shearmatinv*uv[1]);
		box.insert(shearmatinv*uv[2]);

		Math::Box2i ibox;
		ibox.min.x = (int)floor( (box.min.x - offset.x)/dx - jitter.x);
		ibox.max.x = (int)ceil(  (box.max.x - offset.x)/dx + jitter.x);
		ibox.min.y = (int)floor( (box.min.y - offset.y)/dy - jitter.y);
		ibox.max.y = (int)ceil(  (box.max.y - offset.y)/dy + jitter.y);

		Math::Vec2i ipt;
		for( ipt.x = ibox.min.x; ipt.x<=ibox.max.x; ipt.x++)
		{
			for(ipt.y = ibox.min.y;ipt.y<=ibox.max.y; ipt.y++)
			{
				// ������������
				float dt = rand()/(float)RAND_MAX;
				if( dt > dencityFactor) continue;

				Math::Vec2f pt( (ipt.x+0.5f)*dx, (ipt.y+0.5f)*dy);
				pt += offset;

				if( exists.find(ipt)!=exists.end()) continue;

				int lastrand = rand();
				if( jitter.y!=0 || jitter.x!=0)
				{
					std::map< Math::Vec2i, int>::iterator itr = rand_by_position.find(ipt);
					if( itr == rand_by_position.end())
					{
						int sr = lastrand;
						srand(lastrand);
						rand_by_position[ipt] = lastrand;
					}
					else 
						srand(itr->second);

//					srand(abs(ipt.x*749)+abs(ipt.y));
//					srand(rand()+);
//					int x = rand();
					pt.x += jitter.x*dx*( rand()/(float)RAND_MAX - 0.5f);
					pt.y += jitter.y*dy*( rand()/(float)RAND_MAX - 0.5f);
				}
				srand(lastrand);

				pt = shearmat*pt;

				if( !Math::IsInFace(uv, pt)) continue;

				Math::Vec3f b = Math::CalcBaryCoords(uv, pt);

				dt = rand()/(float)RAND_MAX;
				Math::Vec2f _uv = uv[0]*b.x + uv[1]*b.y + uv[2]*b.z;

				// ��������� �� ���������!
				float d = density.getValue(_uv.x, _uv.y);
				if(uvsetind>=0)
				{
					Math::Vec2f uvdens = geometry->getUVset(f, b, hgh, uvsetind);
					d = density.getValue(uvdens.x, uvdens.y);
				}
				if(dt>d) continue;

				// ��������
				HairGeometryIndices::pointonsurface hr;
				hr.faceindex = f;
				hr.clumpindex = -1;
				hr.decimationValue = rand()/(float)RAND_MAX;
				hr.b = b;

//				if(ipt.y>160) continue;
//				if(ipt.x<146) continue;
				// ��������
				data.push_back(hr);
				hairseeds.push_back(rand());

				// uv
				uvs.push_back(_uv);
				// facegroup.count
				facegroup.endindex++;
				exists.insert( ipt);

//				printf("{%d %d} -> {%f, %f}\n", ipt.x, ipt.y, _uv.x, _uv.y);
			}
		}
	}

	if(bProgress)
		MProgressWindow::endProgress();

	// �������� ����� � clump
	if( !hc->clumps.empty())
	{
		BindHairsToClumps(hp, hc, hgh, geometry, CLUMPblending, posuv, fullarea, bProgress, maxClumpRadius, algorithm);
	}
	return true;
}

// ������������� (�� �������)
bool HairRootPositionsData::InitFromClumps(
	HairRootPositions& hp, 
	HairClumpPositions* hc, 
	HairGeometryIndices* hgh, 
	HairGeometry* geometry, 
	int maxHairDrawable, 
	const std::set<int>& workingfaces
	)
{
	bool checkWorkingFaces = !workingfaces.empty();

	double fullarea;
	double area = calcArea(hgh, geometry, workingfaces, fullarea);

	int count = hc->getCount();
	count = (int)(count*area/fullarea);

	if( !checkWorkingFaces && maxHairDrawable >= hc->getCount())
	{
		hp.hairgroups = hc->clumpgroups;
		hp.hairs      = hc->clumps;
		hp.hairseeds  = hc->clumpseeds;
	}
	else
	{
		// ��������� ������ hc->hc.clumps
		float decimation = (float)maxHairDrawable/(float)count;
		hp.hairs.reserve(maxHairDrawable);
		hp.hairseeds.reserve(maxHairDrawable);
		for(int i=0; i<hc->getCount(); i++)
		{
			HairGeometryIndices::pointonsurface& pos = hc->clumps[i];
			int f = pos.faceindex;
			if( checkWorkingFaces)
				if( workingfaces.find(hgh->face_polygonIndex(f))==workingfaces.end())
					continue;

			float dt = rand()/(float)RAND_MAX;
			if(dt>decimation) continue;
			pos.clumpindex = -1;
			hp.hairs.push_back(pos);
			hp.hairseeds.push_back(hc->clumpseeds[i]);
		}
		hp.hairgroups.push_back(
			HairGeometryIndices::facegroup(0, hp.hairs.size()));
	}
	return true;
}


void HairRootPositionsData::BindHairsToClumps(
	HairRootPositions& hp, 
	HairClumpPositions* hc, 
	HairGeometryIndices* hgh, 
	HairGeometry* geometry, 
	float CLUMPblending, 
	std::vector<Math::Vec2f>& posuv, 
	double area,
	bool bProgress,
	float maxClumpRadius,				// ����. ������ ������ - �� ��� ������ - �� � ������
	enClumpAlgorithm algorithm			// �������� ��������
	)
{
	bProgress &= (!::hair_suppressProgress) && (MGlobal::mayaState()==MGlobal::kInteractive);
	static bool bStuped = false;
	if(!bStuped)
	{
		std::vector<Math::Vec3f> clumppos;
		clumppos.reserve(hc->getCount());
		for(int c=0; c<hc->getCount(); c++)
		{
			Math::Vec3f pos = hc->getPOS(c, hgh, geometry);
			clumppos.push_back(pos);
		}

		if( bProgress)
		{
			MProgressWindow::reserve();
			MProgressWindow::setInterruptable(false);
			MProgressWindow::setTitle("Hair file cache");
			MProgressWindow::startProgress();
			MProgressWindow::setProgressRange(0, hp.hairgroups.size());
			MProgressWindow::setProgressStatus("connect hairs to clumps");
		}

		// ����������� clumpRadius
		float areaPerClump = (float)(area/hc->getCount());
		float areaPerFace = (float)(area/hgh->faceCount());
		float maxAreaPerClump = (maxClumpRadius*maxClumpRadius)/2;
		float clumpRadius = sqrt( 2*areaPerClump);
		float clumpRadius2 = clumpRadius*clumpRadius;
		float maxClumpRadius2 = maxClumpRadius*maxClumpRadius;
		bool bFullIteration = areaPerClump>=areaPerFace;

		if( areaPerClump > maxAreaPerClump) 
			areaPerClump = maxAreaPerClump;

		if( algorithm==QUICK)
			bFullIteration = false;
		if( algorithm==STUPID)
			bFullIteration = true;

		/*/
		printf("area=%f, clumpCount=%d faceCount=%d\nareaPerClump=%f areaPerFace=%f\n", 
			area, hc->getCount(), hgh->faceCount(), 
			areaPerClump, areaPerFace);
		/*/

		// wantedfaces. ����������� 1 ��� ���� bFullIteration
		std::vector<int> wantedfaces;
		wantedfaces.reserve(hgh->faceCount());
		if(bFullIteration)
		{
			for(unsigned i=0; i<hgh->faceCount(); i++)
				wantedfaces.push_back(i);

			if( bProgress)
				MProgressWindow::setProgressStatus("connect hairs to clumps: not use associated");

		/*/
			printf("areaPerClump>=areaPerFace\n");
		/*/
		}

		// ����������
		int noclumpcount = 0;
		int sumcallcount = 0, sumfacesreturn = 0;
		Util::FunEvalTimeCounter etc;
		for(unsigned gr=0; gr<hp.hairgroups.size(); gr++)
		{
			HairGeometryIndices::facegroup& group = hp.hairgroups[gr];
			if( group.startindex == group.endindex)
				continue;
			if( bProgress)
				MProgressWindow::setProgress(gr);

			Util::FunEvalTime fet(etc);

			// ����� ��������� �����
			if(!bFullIteration)
			{
				int callCount = hgh->faceCount()*3;
				hgh->findAssociatedFaces(geometry, gr, clumpRadius, wantedfaces, callCount);
				sumcallcount += callCount;
				sumfacesreturn += wantedfaces.size();
			}
			// ��� ������� ������
			for(int vert=group.startindex; vert<group.endindex; vert++)
			{
				Math::Vec2f uv = posuv[vert];
				Math::Vec3f pos = hp.getPOS(vert, hgh, geometry);
				// ���� 2 ���������
				int clumpindex=-1, clumpindex2=-1;

//				float oponentp = rand()/(float)RAND_MAX;
//				float curoponentp = 1;
//				bool bOponent = oponentp > (1-CLUMPblending);

				float dist = 1e34f, dist2 = 1e34f;
				// ��� ������� ��������� �����
				for(unsigned af=0; af<wantedfaces.size(); af++)
				{
					unsigned f = wantedfaces[af];
					// ��� ������� ������ �� �����
					HairGeometryIndices::facegroup& clumpgroup = hc->clumpgroups[f];
					for( int c=clumpgroup.startindex; c<clumpgroup.endindex; c++)
					{
						Math::Vec3f cpos = clumppos[c];
						float d = (cpos-pos).length2();
						if( d>maxClumpRadius2)
							continue;
						if( !bFullIteration && d>clumpRadius2)
							continue;
//						if( !bOponent)
						{
							if( d>dist)
							{
								if(d>dist2)
									continue;
								clumpindex2 = c;
								dist2 = d;
								continue;
							}
							clumpindex2 = clumpindex;
							dist2 = dist;
							clumpindex = c;
							dist = d;
						}
						/*/
						else
						{
							float op = rand()/(float)RAND_MAX;
							if( op < 1/curoponentp)
								hp.hairs[vert].clumpindex = c;
							curoponentp+=1;
						}
						/*/
					}
					// blend
					if(clumpindex2!=-1)
					{
						float op = rand()/(float)RAND_MAX;
						float cr = clumpRadius*op*CLUMPblending;
						if(dist2<cr)
							clumpindex = clumpindex2;
					}
					hp.hairs[vert].clumpindex = clumpindex;
				}
				if( hp.hairs[vert].clumpindex<0)
					noclumpcount++;
			}
		}
		if( bProgress)
			MProgressWindow::endProgress();

		/*/
		printf( "clumpRadius=%f, evalcnt=%d, sumtime=%f, sumcallcount=%f, sumfacesreturn=%f noclumpcount=%d\n", 
			clumpRadius, etc.evalcnt, etc.sumtime, 
			sumcallcount/(double)etc.evalcnt, 
			sumfacesreturn/(double)etc.evalcnt, 
			noclumpcount);
		/*/
	}
	else
	{
		std::vector<Math::Vec2f> clumpuv;
		clumpuv.reserve(hc->clumps.size());
		for(unsigned c=0; c<hc->clumps.size(); c++)
		{
			HairGeometryIndices::pointonsurface& p = hc->clumps[c];
			Math::Vec2f uv;
			uv = hc->getUV(c, hgh, geometry);
			clumpuv.push_back(uv);
		}

		if( bProgress)
		{
			MProgressWindow::reserve();
			MProgressWindow::setInterruptable(false);
			MProgressWindow::setTitle("Hair file cache");
			MProgressWindow::startProgress();
			MProgressWindow::setProgressRange(0, hp.hairs.size());
			MProgressWindow::setProgressStatus("connect hairs to clumps");
		}
		for(unsigned int vert=0; vert<hp.hairs.size(); vert++)
		{
			Math::Vec2f uv = posuv[vert];
			if( bProgress)
				MProgressWindow::setProgress(vert);
			
			float dist = 1e34f;
			for( unsigned int c=0; c<hc->clumps.size(); c++)
			{
				Math::Vec2f cuv = clumpuv[c];
				if( (cuv-uv).length2()>dist)
					continue;
				hp.hairs[vert].clumpindex = c;
				dist = (cuv-uv).length2();
			}
		}
		if( bProgress)
			MProgressWindow::endProgress();
	}
}

// ������ ����� �����
int HairRootPositionsData::generate(
	HairGeometryIndices* hgh, 
	HairGeometry* geometry, 
	std::vector< HairGeometryIndices::pointonsurface>& data,
	std::vector< int>& hairseeds, 
	std::vector< HairGeometryIndices::facegroup>& groups, 
	std::vector< Math::Vec2f>& uvs,
	MPlug& densityplug,
	MPlug& densitypluguvset,
	bool bEqualize,
	double area, 
	int count, int globalSeed, 
	int resX, int resY,
	const char* progressString, 
	const std::set<int>& workingfaces)
{
	bool bProgress = progressString!=NULL;
	bProgress &= (!::hair_suppressProgress) && (MGlobal::mayaState()==MGlobal::kInteractive);

	bool checkWorkingFaces = !workingfaces.empty();

	if( bProgress)
	{
		MProgressWindow::reserve();
		MProgressWindow::setInterruptable(false);
		MProgressWindow::setTitle("Hair file cache");
		MProgressWindow::startProgress();
		MProgressWindow::setProgressRange(0, hgh->faceCount());
		MProgressWindow::setProgressStatus(progressString);
	}

	Math::ParamTex2d<unsigned char> density;
	MString densityuvsetname;
	densitypluguvset.getValue(densityuvsetname);

	textureCacheData::loadFloatParamFromFile(density, densityplug, resX, resY, 0, densityuvsetname.asChar());
	int uvsetind = hgh->finduvSetByName(densityuvsetname.asChar());

	// ��������
	if( bEqualize)
	{
		float sumdensitytex = density.getAverageValueSource();
		count = (int)(count*density.getMax()/sumdensitytex);
	}

//	Math::ParamTex2d<unsigned char, Math::EJT_MAX_EGZ> density;
//	textureCacheData::loadFloatParamFromFile( *((Math::ParamTex2d<unsigned char>*)&density), densityplug, resX, resY, 0, "");

	uvs.clear();
	data.clear();
	data.reserve(count + hgh->faceCount());
	hairseeds.reserve(count + hgh->faceCount());
	uvs.reserve(count + hgh->faceCount());
	groups.resize(hgh->faceCount());
	for(unsigned f=0; f<hgh->faceCount(); f++)
	{
		if( checkWorkingFaces)
			if( workingfaces.find(hgh->face_polygonIndex(f))==workingfaces.end())
				continue;

		if( bProgress)
			MProgressWindow::setProgress(f);


		HairGeometryIndices::facegroup& facegroup = groups[f];
		facegroup.startindex = data.size();
		facegroup.endindex = data.size();
		srand( hgh->faceSeed(f)+globalSeed);
		Math::Face32& face = hgh->face(f);

		// verts
		const Math::Vec3f &pt0 = geometry->vertex(face.v[0]);
		const Math::Vec3f &pt1 = geometry->vertex(face.v[1]);
		const Math::Vec3f &pt2 = geometry->vertex(face.v[2]);

		// uvs
		Math::Face32& face_uv = hgh->faceUV(f);
		const Math::Vec2f& uv0 = geometry->uv( face_uv.v[0] );
		const Math::Vec2f& uv1 = geometry->uv( face_uv.v[1] );
		const Math::Vec2f& uv2 = geometry->uv( face_uv.v[2] );

		// area
		Math::Vec3f v10 = pt1 - pt0;
		Math::Vec3f v20 = pt2 - pt0;
		float a = Math::cross(v10,v20).length();
		double fa = a/area;
		int fc = (int)(count*fa);

		float dt = rand()/(float)RAND_MAX;
		if( dt <= (float)count*fa - fc)
			fc++;

		for(int i=0; i<fc; i++)
		{
			// ������������
			float dt = rand()/(float)RAND_MAX;

			Math::Vec3f b;
			b.x = rand()/(float)RAND_MAX;
			b.y = rand()/(float)RAND_MAX;
			if(b.y+b.x > 1) 
			{ 
				b.x = 1-b.x;
				b.y = 1-b.y;
				std::swap(b.x, b.y);
			}
			b.z = 1 - b.x - b.y;
			b = b/(b.x+b.y+b.z);
			Math::Vec2f uv = uv0*b.x + uv1*b.y + uv2*b.z;

			// ���������:
			double d = density.getValue(uv.x, uv.y);
			if(uvsetind>=0)
			{
				Math::Vec2f uvdens = geometry->getUVset(f, b, hgh, uvsetind);
				d = density.getValue(uvdens.x, uvdens.y);
			}
			if(dt>d)
				continue;

			HairGeometryIndices::pointonsurface hr;
			hr.faceindex = f;
			hr.clumpindex = -1;
			hr.decimationValue = rand()/(float)RAND_MAX;
			hr.b = b;
			data.push_back(hr);
			hairseeds.push_back(rand());

			// uv
			uvs.push_back(uv);
			// facegroup.count
			facegroup.endindex++;

		}
	}

	if(bProgress)
		MProgressWindow::endProgress();
	return (int)data.size();
}


//
HairRootPositionsData::HairRootPositionsData()
{
}
//
HairRootPositionsData::~HairRootPositionsData()
{
}

//
MTypeId HairRootPositionsData::typeId() const
{
	return HairRootPositionsData::id;
}

//
MString HairRootPositionsData::name() const
{ 
	return HairRootPositionsData::typeName; 
}

//
void* HairRootPositionsData::creator()
{
	return new HairRootPositionsData();
}

//
void HairRootPositionsData::copy( const MPxData& other )
{
	const HairRootPositionsData* arg = (const HairRootPositionsData*)&other;

	this->hp	    = arg->hp;
}

//
MStatus HairRootPositionsData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("HairClumpPositionsData: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	if( !this->serialize(stream)) 
		return MS::kFailure;

	return MS::kSuccess;
}

//
MStatus HairRootPositionsData::writeASCII( 
	std::ostream& out )
{
// ����� ����� ��� �������� ������ � �������� ��������!!!!!!
//return MS::kFailure;

	MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

//
MStatus HairRootPositionsData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	if( !serialize(stream))
		return MS::kFailure;

	return MS::kSuccess;
}

MStatus HairRootPositionsData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}
//
bool HairRootPositionsData::serialize(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version>=0)
	{
		hp.serialize(stream);
	}
	return true;
}







//
double HairRootPositionsData::calcArea(
	HairGeometryIndices* hgh, 
	HairGeometry* geometry, 
	const std::set<int>& workingfaces,
	double& fullarea		// out
	)
{
	bool checkWorkingFaces = !workingfaces.empty();
	// area
	double area = 0;
	fullarea = 0;
	for(unsigned f=0; f<hgh->faceCount(); f++)
	{
		Math::Face32& face = hgh->face(f);
		Math::Vec3f &pt0 = geometry->vertex(face.v[0]);
		Math::Vec3f &pt1 = geometry->vertex(face.v[1]);
		Math::Vec3f &pt2 = geometry->vertex(face.v[2]);
		Math::Vec3f v10 = pt1 - pt0;
		Math::Vec3f v20 = pt2 - pt0;
		float a = Math::cross(v10,v20).length();
		fullarea += a;

		if( checkWorkingFaces)
			if( workingfaces.find(hgh->face_polygonIndex(f))==workingfaces.end())
				continue;

		area += a;
	}
	return area;
}

