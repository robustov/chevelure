#include "HairShapeUI.h"
#include "HairShape.h"
#include "MathNMaya/MathNMaya.h"
#include "MathNgl/MathNgl.h"

#define ACTIVE_VERTEX_COLOR		16	// yellow

#define LEAD_COLOR				18	// green
#define ACTIVE_COLOR			15	// white
#define ACTIVE_AFFECTED_COLOR	8	// purple
#define DORMANT_COLOR			4	// blue
#define HILITE_COLOR			17	// pale blue

HairShapeUI::HairShapeUI() 
{
}
HairShapeUI::~HairShapeUI() 
{
}

void* HairShapeUI::creator()
{
	return new HairShapeUI();
}

void HairShapeUI::getDrawRequests( 
	const MDrawInfo & info,
	bool objectAndActiveOnly,
	MDrawRequestQueue & queue )
{
	// ��� ������ COMB TOOL (!!!)
	/*/
	{
		M3dView view = info.view();
		HWND active3dView = view.window();
		POINT pt;
		GetCursorPos(&pt);
		::PostMessage(active3dView, WM_NCHITTEST, 0, MAKELONG(pt.x, pt.y));
	}
	/*/
//displayStringD("HairShapeUI::getDrawRequests (style=%d, status=%d)", 
//			   info.displayStyle(), info.displayStatus());


	// The draw data is used to pass geometry through the 
	// draw queue. The data should hold all the information
	// needed to draw the shape.
	//
	HairShape* shapeNode = (HairShape*)surfaceShape();

	// ��������� ��������� compute
	DrawCache* mGeom = shapeNode->getDrawCache();
	if( !mGeom) return;

	MDrawData data;
	this->getDrawData( mGeom, data );

	MDagPath path = info.multiPath();	// path to your dag object 
	MMaterial material = MPxSurfaceShapeUI::material( path );

	// kDrawHair
	if( shapeNode->getDrawMode() == HairShape::DM_BOX)
	{
		MDrawData data;
		this->getDrawData( shapeNode, data );

		MDrawRequest request = info.getPrototype( *this );
		int token = kDrawBox;
		setRequestColor(request, info, token);
		request.setMaterial( material);
		request.setDrawData( data );
		request.setToken( token);
		queue.add( request );
	}
	else
	{
		bool bDrawColor = shapeNode->isColorize();
		int token = kDrawHair;
		if(mGeom->isInstance())
			token = kDrawInstance;
		token |= (bDrawColor?kDrawColor:0);

		MDrawRequest request = info.getPrototype( *this );
		request.setDrawData( data );
		setRequestColor(request, info, token);
//		bool bDrawVertices = (token&kDrawVertices)!=0;
//		token &= ~kDrawVertices;

		M3dView::DisplayStyle  appearance    = info.displayStyle();
		M3dView::DisplayStatus  displaystatus    = info.displayStatus();

		bool Shading = true;

		if( 
			displaystatus == M3dView::kTemplate || 
			displaystatus == M3dView::kActiveTemplate
			)
		{
			Shading = false;
		}

		if(Shading && mGeom->isInstance())
		{
			if( appearance == M3dView::kGouraudShaded ||
				appearance == M3dView::kFlatShaded)
			{
				if( displaystatus == M3dView::kLead || 
					displaystatus == M3dView::kActive)
				{
					MDrawRequest wirerequest = info.getPrototype( *this );
					wirerequest.setDrawData( data );
					setRequestColor(wirerequest, info, token);
					wirerequest.setToken( token);
					queue.add( wirerequest );
				}

				///////////////
				// 
				M3dView view = info.view();; 		// view to draw to

				if ( ! material.evaluateMaterial( view, path ) ) 
					cerr << "Couldnt evaluate Material\n";

				bool drawTexture = true;
				if ( drawTexture && material.materialIsTextured() ) 
					material.evaluateTexture( data );

				request.setMaterial( material );

				bool materialTransparent = false;
				material.getHasTransparency( materialTransparent );
				if ( materialTransparent ) 
				{
					request.setIsTransparent( true );
				}
				request.setMaterial( material);
				token |= kDrawFlatShaded;
			}
		}
		request.setToken( token);

		queue.add( request );

		/*/
		if( bDrawVertices)
		{
			MDrawRequest vertrequest = info.getPrototype( *this );
			vertrequest.setDrawData( data );
			setRequestColor(vertrequest, info, token);
			vertrequest.setToken( kNothing|kDrawVertices);
			queue.add( vertrequest );
		}
		/*/
	}

	if ( !objectAndActiveOnly ) 
	{
		// Active components
		//
		if ( surfaceShape()->hasActiveComponents() ) 
		{
			MDrawRequest activeVertexRequest = info.getPrototype( *this ); 
			activeVertexRequest.setDrawData( data );
		    activeVertexRequest.setToken( kDrawVertices );
		    activeVertexRequest.setColor( ACTIVE_VERTEX_COLOR,
										  M3dView::kActiveColors );

		    MObjectArray clist = surfaceShape()->activeComponents();
		    MObject vertexComponent = clist[0]; // Should filter list
		    activeVertexRequest.setComponent( vertexComponent );

			queue.add( activeVertexRequest );
		}
	}

}

void HairShapeUI::draw( 
	const MDrawRequest & request, 
	M3dView & view ) const
{ 
	M3dView::LightingMode lightingmode;
	view.getLightingMode(lightingmode);
//displayStringD("HairShapeUI::draw (style=%d, lightingmode=%d rendername=%d)", 
//			   view.displayStyle(), 
//			   lightingmode, 
//			   view.getRendererName(NULL));

	MDrawData data = request.drawData();
	DrawCache* geom = (DrawCache*)data.geometry();
	short token = request.token();
	bool drawColor = (token & (short)kDrawColor)!=0;
	bool drawVertexes = (token & (short)kDrawVertices)!=0;
	bool bDrawFlatShaded = (token & (short)kDrawFlatShaded)!=0;
	token &= ~kMask;

//	view.setDrawColor(request.color());

	if( drawVertexes)
	{
		view.beginGL();
		float lastPointSize;
		glGetFloatv( GL_POINT_SIZE, &lastPointSize );
		glPointSize( 2.0 );

		{
			geom->glrender.clear();
			geom->glrender.bPoints = true;
			geom->glrender.bShaded = false;
			geom->glrender.bWireFrame = false;
		}

		geom->drawHair( false, true);
		glPointSize(lastPointSize);
		view.endGL(); 
	}

	switch( token )
	{
		case kDrawHair:
			{
			view.beginGL();
			geom->drawHair(drawColor, false);
			view.endGL(); 
			break;
			}
		case kDrawInstance:
			{
				view.beginGL();
				M3dView::DisplayStyle displayStyle = view.displayStyle();

				{
					geom->glrender.clear();
					geom->glrender.bPoints = false;
					geom->glrender.bShaded = bDrawFlatShaded;
					geom->glrender.bWireFrame = !bDrawFlatShaded;
				}
				int shadeModel;
				glGetIntegerv( GL_SHADE_MODEL, &shadeModel);

				if( bDrawFlatShaded)
				{
					#if		defined(SGI) || defined(MESA)
						glEnable( GL_POLYGON_OFFSET_EXT );
					#else
						glEnable( GL_POLYGON_OFFSET_FILL );
					#endif

					glShadeModel(GL_SMOOTH);

					MMaterial material = request.material();
					material.setMaterial( request.multiPath(), request.isTransparent() );

					bool drawTexture = material.materialIsTextured();
					if ( drawTexture ) 
					{
						glEnable(GL_TEXTURE_2D);
						material.applyTexture( view, data );
					}

					geom->drawInstance();

					if ( drawTexture ) 
						glDisable(GL_TEXTURE_2D);
				}
				else
				{
					geom->drawInstance();
				}
				glShadeModel(shadeModel);
				view.endGL(); 
			}
			break;

		case kDrawBox:
			view.beginGL();
			drawBox();
			view.endGL(); 
			break;
	}

}

bool HairShapeUI::select( 
	MSelectInfo &selectInfo,
	MSelectionList &selectionList,
	MPointArray &worldSpaceSelectPts ) const
{
//displayStringD("HairShapeUI::select");
	bool selected = false;
	bool componentSelected = false;
	bool hilited = false;
	HairShape* meshNode = (HairShape*)surfaceShape();

	hilited = (selectInfo.displayStatus() == M3dView::kHilite);
	if ( hilited ) 
	{
		componentSelected = selectVertices( selectInfo, selectionList,
											worldSpaceSelectPts );
		selected = selected || componentSelected;
	}

	if ( !selected )
	{
		DrawCache* geom = meshNode->getDrawCache();

		MSelectionMask priorityMask( MSelectionMask::kSelectMeshes );
		MSelectionList item;
		item.add( selectInfo.selectPath() );
		MPoint xformedPt;
		if ( selectInfo.singleSelection() ) 
		{
			MPoint center = meshNode->boundingBox().center();
			xformedPt = center;
			xformedPt *= selectInfo.selectPath().inclusiveMatrix();
		}

		M3dView view = selectInfo.view();
		view.beginSelect();

		if( meshNode->getDrawMode() == HairShape::DM_BOX)
		{
			this->drawBox();
		}
		else if( geom)
		{
			{
				geom->glrender.clear();
				geom->glrender.bPoints = false;
				geom->glrender.bShaded = false;
				geom->glrender.bWireFrame = true;
			}
			if(geom->isInstance())
				geom->drawInstance();
			else
				geom->drawHair(false, false);
			geom->drawCVs();
		}

		if( view.endSelect()>0)
		{
			selectInfo.addSelection( item, xformedPt, selectionList,
									worldSpaceSelectPts, priorityMask, false );
			selected = true;
		}
	}
	return selected;
}


void HairShapeUI::drawBox() const
{
	HairShape* shapeNode = (HairShape*)surfaceShape();
	Math::Box3f box;
	copy( box, shapeNode->box);
	::drawBox(box);
}


void HairShapeUI::drawVertices( 
	const MDrawRequest & request,
	M3dView & view ) const
{
	MDrawData data = request.drawData();
	DrawCache* geom = (DrawCache*)data.geometry();

	bool lightingWasOn = glIsEnabled( GL_LIGHTING ) ? true : false;
	if ( lightingWasOn ) 
	{
		glDisable( GL_LIGHTING );
	}
	float lastPointSize;
	glGetFloatv( GL_POINT_SIZE, &lastPointSize );

	glPointSize( 4.0 );

	MObject comp = request.component();
	if ( ! comp.isNull() ) 
	{
		MFnSingleIndexedComponent fnComponent( comp );
		for ( int i=0; i<fnComponent.elementCount(); i++ )
		{
			int index = fnComponent.element( i );

			index = index%geom->UI_getCVCount();
			view.setDrawColor(ACTIVE_VERTEX_COLOR);
			geom->drawCV(index);
		}
	}
	// Restore the state
	//
	if ( lightingWasOn ) 
	{
		glEnable( GL_LIGHTING );
	}
	glPointSize( lastPointSize );

}



/*/
void selectPoint(
	bool singleSelection, 
	MPoint currentPoint, int vertexIndex, MMatrix alignmentMatrix,
	double& previousZ, int& closestPointVertexIndex, MPoint& singlePoint,
	MFnSingleIndexedComponent& fnComponent)
{
	if ( singleSelection ) 
	{
		MPoint xformedPoint = currentPoint;
		xformedPoint.homogenize();
		xformedPoint*= alignmentMatrix;
		double z = xformedPoint.z;
		if ( closestPointVertexIndex < 0 || z > previousZ ) 
		{
			closestPointVertexIndex = vertexIndex;
			singlePoint = currentPoint;
			previousZ = z;
		}
	} 
	else 
	{
		// multiple selection, store all elements
		//
		fnComponent.addElement( vertexIndex );
	}
}
/*/

bool HairShapeUI::selectVertices( 
	MSelectInfo &selectInfo,
	MSelectionList &selectionList,
	MPointArray &worldSpaceSelectPts ) const
{
	bool selected = false;
	M3dView view = selectInfo.view();

	MPoint 		xformedPoint;
	MPoint 		currentPoint;
	MPoint 		selectionPoint;
	double		previousZ = 0.0;
 	int			closestPointVertexIndex = -1;

	const MDagPath & path = selectInfo.multiPath();

	// Create a component that will store the selected vertices
	//
	MFnSingleIndexedComponent fnComponent;
	MObject surfaceComponent = fnComponent.create( MFn::kMeshVertComponent );
	int vertexIndex;

	// if the user did a single mouse click and we find > 1 selection
	// we will use the alignmentMatrix to find out which is the closest
	//
	MMatrix	alignmentMatrix;
	MPoint singlePoint; 
	bool singleSelection = selectInfo.singleSelection();
	if( singleSelection ) 
	{
		alignmentMatrix = selectInfo.getAlignmentMatrix();
	}

	// Get the geometry information
	//
	HairShape* meshNode = (HairShape*)surfaceShape();
	DrawCache* geom = meshNode->getDrawCache();
	if( !geom) return false;

	// SELECT VERTECIES
	for ( vertexIndex=0; vertexIndex<geom->UI_getCVCount(); vertexIndex++ )
	{
		MPoint currentPoint = geom->UI_getCV(vertexIndex);

		view.beginSelect();
		geom->drawCV(vertexIndex);
		if ( view.endSelect() > 0 )	
		{
			selected = true;
			if ( singleSelection ) 
			{
				MPoint xformedPoint = currentPoint;
				xformedPoint.homogenize();
				xformedPoint*= alignmentMatrix;
				double z = xformedPoint.z;
				if ( closestPointVertexIndex < 0 || z > previousZ ) 
				{
					closestPointVertexIndex = vertexIndex;
					singlePoint = currentPoint;
					previousZ = z;
				}
			} 
			else 
			{
				// multiple selection, store all elements
				//
				fnComponent.addElement( vertexIndex );
//displayString("select %d", vertexIndex);
			}

		}
	}

	// If single selection, insert the closest point into the array
	//
	if ( selected && selectInfo.singleSelection() ) 
	{
		fnComponent.addElement(closestPointVertexIndex);
//displayString("select %d", closestPointVertexIndex);

		// need to get world space position for this vertex
		//
		selectionPoint = singlePoint;
		selectionPoint *= path.inclusiveMatrix();
	}

	// Add the selected component to the selection list
	//
	if ( selected ) 
	{
		MSelectionList selectionItem;
		selectionItem.add( path, surfaceComponent );

		MSelectionMask mask( MSelectionMask::kSelectComponentsMask );
		selectInfo.addSelection(
			selectionItem, selectionPoint,
			selectionList, worldSpaceSelectPts,
			mask, true );
	}

	return selected;
}


bool HairShapeUI::setRequestColor( 
	MDrawRequest& request,
	const MDrawInfo& info, 
	int& token)
{
	M3dView::DisplayStyle displayStile = info.displayStyle();
	M3dView::DisplayStatus displayStatus = info.displayStatus();
	M3dView::ColorTable activeColorTable = M3dView::kActiveColors;
	M3dView::ColorTable dormantColorTable = M3dView::kDormantColors;
//displayStringD("color: %d %d", displayStile, displayStatus);

	bool drawColor = (token & kDrawColor)!=0;
	bool drawVertexes = (token & kDrawVertices)!=0;
	enDrawTokens drawToken = (enDrawTokens)( token&(~kMask));
	if( drawToken==kDrawBox)
		drawColor = false;


	/*/
	if(displayStile!=M3dView::kWireFrame && token==kDrawHair)
	{
		request.setColor( DORMANT_COLOR, dormantColorTable );
		if( displayStatus != M3dView::kDormant)
		{

		}
		return false;
	}
	/*/
	switch ( displayStatus )
	{
		case M3dView::kLead :
			if(token==kDrawControl)
				request.setColor( ACTIVE_AFFECTED_COLOR, activeColorTable );
			else
				request.setColor( LEAD_COLOR, activeColorTable );
			if( drawColor) drawVertexes = true;
			break;
		case M3dView::kActive :
			if(token==kDrawControl)
				request.setColor( ACTIVE_AFFECTED_COLOR, activeColorTable );
			else
				request.setColor( ACTIVE_COLOR, activeColorTable );
			if( drawColor) drawVertexes = true;
			break;
		case M3dView::kActiveAffected :
			if(token==kDrawControl)
				request.setColor( ACTIVE_AFFECTED_COLOR, activeColorTable );
			else
				request.setColor( ACTIVE_AFFECTED_COLOR, activeColorTable );
			if( drawColor) drawVertexes = true;
			break;
		case M3dView::kDormant :
			if(token==kDrawControl)
				request.setColor( ACTIVE_AFFECTED_COLOR, activeColorTable );
			else
				request.setColor( DORMANT_COLOR, dormantColorTable );
			break;
		case M3dView::kHilite :
			if(token==kDrawControl)
				request.setColor( LEAD_COLOR, activeColorTable );
			else
				request.setColor( HILITE_COLOR, activeColorTable );
			if( drawColor) drawVertexes = true;
			break;
		default:	
			break;
	}
	token = drawToken | (drawVertexes?kDrawVertices:0) | (drawColor?kDrawColor:0);
	return true;
}










