import maya.cmds as cmds
import maya.mel as mm


def hzHairInit (mode):
	global hzHairPlugin
	global hzHairCtrl
	global hzHair
	global hzShave2Hair

	if mode=="chevelure":
		hzHairPlugin="chevelure"
		hzHairCtrl='chevelureControlsShape'
		hzHair='chevelureShape'
		hzShave2Hair='chevelureShave2Hair'
	else:
   		hzHairPlugin="chevelure"
		hzHairCtrl='chevelureControlsShape'
		hzHair='chevelureShape'
		hzShave2Hair='chevelureShave2Hair'

def hzAddHairAttractorsDo (attractor, hairCtrlSh):
	try:
		cmds.connectAttr ((attractor+".message"), (hairCtrlSh+".importfrom"), f=1,  na=1 )
		print (attractor+" connected to" + hairCtrlSh)
	except Exception, e :
		print (attractor+" is already connected to" + hairCtrlSh)
		pass


def hzAddHairAttractors():
	global hzHairCtrl
	sel=cmds.ls (sl=1, dag=1, leaf=1,  type=[hzHairCtrl, 'shaveHair'])
	print len(sel)

	if len(sel)!=2 :
		print ("Inocrrect selection. Please select at least 2 nodes\n")
		return
	else:
		countAttractors=len(sel)-1
		hairCtrlSh=sel[-1]																	# last item
		print hairCtrlSh
		print cmds.nodeType (hairCtrlSh)
		if cmds.nodeType (hairCtrlSh)!=hzHairCtrl:
			print  ("Inocrrect selection. Please select hairControls last\n")
			return
		else:
			sel.pop ()																					#remove last item
			for curr in sel:
				hzAddHairAttractorsDo (curr, hairCtrlSh)

######################################

def hzAttractHairDo (attractor, ctrl):
	global hzHairCtrl
	global hzShave2Hair
	attractors=cmds.listConnections ( (ctrl+".importfrom"),d=0, s=1, sh=1)
	print ("Attractors is: \n")
	print attractors


	if cmds.nodeType (ctrl)==hzHairCtrl:
		for curr in attractors:
			if cmds.nodeType (curr)=="shaveHair":
				if attractor=="":
					mm.eval (hzShave2Hair+" \""+curr+"\" \""+ctrl+"\"")
     			elif attractor==curr:
					mm.eval (hzShave2Hair+" \""+curr+"\" \""+ctrl+"\"")
			else:
				type=cmds.nodeType (curr)
				print(curr +" is mismatch node's type!!!\n")
				print ("node's type is "+type+"!!!\n")

	else:
		print (node +" mismatch node's type!!!\n")


def hzAttractHair ():
	global hzHairCtrl
	sel=cmds.ls (sl=1, dag=1, leaf=1, type=hzHairCtrl)
	for ctrl in sel:
		hzAttractHairDo ('', ctrl)