#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: HairEmitterNode.h
//
// Dependency Graph Node: HairEmitter

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include <maya/MPxEmitterNode.h>

#include "HairGeometryIndicesData.h"
#include "HairGeometryData.h"
#include "HairRootPositionsData.h"
#include "HairAliveData.h"
#include "HairFixedFileData.h"

//! HairEmitter
class HairEmitter : public MPxEmitterNode
{
public:
	HairEmitter();
	virtual ~HairEmitter(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();


	//! Main Generate proc
	void emit(
		int emitCount,					//!< wanted particle count
		MVectorArray& fnOutPos,			//!< output positions
		MVectorArray& fnOutVel,			//!< output velocities
		MVectorArray& fnOutScale,		//!< output scale
		int plugIndex,					//!< N particle system
		MDataBlock& data				//!< data
		);

public:
	static MObject i_HairGeometryIndices;	//!< ���������� ���������� ���������
	static MObject i_HairGeometry;			//!< ��������� ���������
	static MObject i_HairRootPositions;		//!< ��������� ����� 
	static MObject i_HairAlive;				//!< ������� ����� "�����" �����
	static MObject i_EmitStartTime[2];		//!< ����� ������ ���������

	static MObject i_PaintAttrRes;		//!< ����������� ��� ���� �������
	static MObject i_EmitInclination[2];	//!< ���������� ���������� � TS
	static MObject i_EmitPolar[2];			//!< ���������� ���������� � TS
	static MObject i_EmitVelocity[2];		//!< �������� ���������

public:
	static MTypeId id;
	static const MString typeName;

	static HairGeometryIndices*		LoadHairGeometryIndices(MDataBlock& data);
	static HairGeometry*			LoadHairGeometry(MDataBlock& data);
	static HairRootPositions*		LoadHairRootPositions(MDataBlock& data);
	static HairAlive*				LoadHairAlive(MDataBlock& data);



public:
	static void CreateTextureCacheAttr(std::string longName, std::string shortName, MObject& src, MObject& attr);
	MStatus ComputeTextureCache(const MPlug& dat, const MPlug& src, MDataBlock& data);
	static void AffectToOutput(const MObject& isAffected);

	static MStatus CreateAttrNTextureCache( 
		std::string longname, std::string shortname, 
		MObject* i_HairAttr, double min, double max, double def);

protected:
	MStatus computeOutput( 
		const MPlug& plug, 
		MDataBlock& data );

	bool isFullValue(int plugIndex, MDataBlock& block)
	{
		MDataHandle hValue;
		if( !getPlugValue(hValue, mIsFull, plugIndex, block))
			return true;
		return hValue.asBool();
	}
	double inheritFactorValue(int plugIndex,MDataBlock& block)
	{
		MDataHandle hValue;
		if( !getPlugValue(hValue, mInheritFactor, plugIndex, block))
			return 0.0;
		return hValue.asDouble();
	}

	MTime currentTimeValue( MDataBlock& block )
	{
		MStatus stat;
		MDataHandle hValue = block.inputValue( mCurrentTime, &stat );
		if( !stat) return MTime(0.0);
		return hValue.asTime();
	}
	MTime startTimeValue( int plugIndex, MDataBlock& block )
	{
		MDataHandle hValue;
		if( !getPlugValue(hValue, mStartTime, plugIndex, block))
			return MTime(0.0);
		return hValue.asTime();
	}
	MTime deltaTimeValue( int plugIndex, MDataBlock& block )
	{
		MDataHandle hValue;
		if( !getPlugValue(hValue, mDeltaTime, plugIndex, block))
			return MTime(0.0);
		return hValue.asTime();
	}

	bool getPlugValue(MDataHandle& hValue, MObject attribute, int plugIndex, MDataBlock& block);
};

inline bool HairEmitter::getPlugValue(MDataHandle& hValue, MObject attribute, int plugIndex, MDataBlock& block)
{
	MStatus stat;
	bool value = true;

	MArrayDataHandle mhValue = block.inputArrayValue( attribute, &stat );
	if( stat == MS::kSuccess )
	{
		stat = mhValue.jumpToArrayElement( plugIndex );
		if( stat == MS::kSuccess )
		{
			hValue = mhValue.inputValue( &stat );
			return ( stat == MS::kSuccess );
		}
	}
	return false;
}


