#pragma once
#include "pre.h"
//
// Copyright (C) 
// File: HairArrayMapperNode.h
//
// Dependency Graph Node: HairArrayMapper

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include <maya/MPxParticleAttributeMapperNode.h>

class HairArrayMapper : public MPxParticleAttributeMapperNode
{
public:
	HairArrayMapper();
	virtual ~HairArrayMapper(); 

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_particleCount;
	static MObject i_input;			// input vectors from hair
	static MObject o_output;		// output vectors to particles

public:
	static MTypeId id;
	static const MString typeName;
};

