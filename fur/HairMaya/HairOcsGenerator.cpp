#include "HairOcsGenerator.h"
#include "HairShape.h"
#include "HairRenderPassNode.h"
#include "Util/FileStream.h"
#include "Util/misc_create_directory.h"
#include <maya/MGlobal.h>
#include "ocellaris/renderCacheImpl.h"
#include "ocellaris/renderProxy.h"

/*/
struct renderProxySkipRenderCall : public cls::renderProxy
{
	renderProxySkipRenderCall(IRender* render):cls::renderProxy(render){}
	/// ����� ��������� ���������� ������� ������
	virtual void SystemCall(
		cls::enSystemCall call
		){};
	virtual void RenderCall( 
		cls::IProcedural* pProcedural		//!< ����� ������� IProcedural::Render()
		){};
};
/*/

/*/
HairSkinFilterDeformGenerator hairskinfilterdeformgenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl HairSkinFilterDeform()
	{
		return &hairskinfilterdeformgenerator;
	}
}
HairExternVNDeformGenerator hairexternvndeformgenerator;
extern "C"
{
	// Dump
	__declspec(dllexport) cls::IGenerator* __cdecl HairExternVNDeform()
	{
		return &hairexternvndeformgenerator;
	}
}
/*/

//! ����������� ���� � �������� 
bool HairOcsGenerator::OnPrepareNode(
	cls::INode& node, 
	cls::IExportContext* context,
	MObject& generatornode
	)
{
	bool bCopyRootPositionFile = true;

	std::string ocsdirectory = context->getEnvironment("RIBDIR");

	MFnDependencyNode dn(node.getObject());
	HairShape* shape = NULL;
	MTypeId tid = dn.typeId();
	if( tid != HairShape::id)
		return false;
	shape = (HairShape*)dn.userNode();

	int timestamp;
	MPlug(node.getObject(), HairShape::i_HairTimeStamp).getValue(timestamp);

	std::string hairrootfilename = shape->GetHairRootPositionFile(timestamp);
	if( hairrootfilename.empty()) 
		return false;

	if(bCopyRootPositionFile)
	{
		// ���������� ���� �������
		std::string new_shaderfile = ocsdirectory + "/" + Util::extract_filename(hairrootfilename.c_str());
		Util::create_directory_for_file(new_shaderfile.c_str());
		if( !::CopyFile( hairrootfilename.c_str(), new_shaderfile.c_str(), FALSE))
		{
			printf("ERROR COPY HAIR ROOT POSITIONS %s -> %s\n", hairrootfilename.c_str(), new_shaderfile.c_str());
//			return shader;
		}
		else
		{
			hairrootfilename = new_shaderfile;
		}
	}

	cls::IRender* contextcache = context->objectContext(node.getObject(), this);
	if( contextcache)
	{
		contextcache->Parameter("@Hair::positionfilename", hairrootfilename.c_str());
		contextcache->Parameter("@Hair::timestamp", timestamp);
	}
	return true;
}


bool HairOcsGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render, 
	Math::Box3f& box, 
	cls::IExportContext* context, 
	MObject& generatornode			//!< generator node �.� 0
	)
{
	::hair_suppressProgress = true;

	MObject node = inode.getObject();

	const char* pass = "all";
	MFnDependencyNode dn(node);

	std::string ocsdirectory = context->getEnvironment("RIBDIR");

	HairShape* shape = NULL;

	bool bStandAlonePass = false;
	HairRenderPass standAlonePass;

	MTypeId tid = dn.typeId();
	if( tid == HairShape::id)
	{
		shape = (HairShape*)dn.userNode();
	}
	else if(node.hasFn(MFn::kLocator))
	{
		MPlug referenceObjectPlug = dn.findPlug("referenceObject");
		MPlugArray pa;
		referenceObjectPlug.connectedTo(pa, true, false);
		if(pa.length()!=1) 
			return false;
		
		node = pa[0].node();
		dn.setObject(node);
		if( dn.typeId() != HairRenderPassNode::id)
			return false;

		// Enable
		bool bEnable=0;
		MPlug(node, HairRenderPassNode::i_enable).getValue(bEnable);
		if( !bEnable) return false;	// disable

		HairRenderPassNode* renderPass = (HairRenderPassNode*)dn.userNode();

		// ������
		MObject passVal;
		MPlug(node, HairRenderPassNode::o_output).getValue(passVal);
		if( passVal.isNull()) return false;
		MFnPluginData pdata(passVal);
		HairRenderPassData* pxdata = (HairRenderPassData*)pdata.data();
		if( !pxdata) return false;

		standAlonePass = pxdata->data;

		// ��������� ���������� �������
		bool bOk = false;
		for( int x=0; x<(int)standAlonePass.passes.size(); x++)
		{
			if(standAlonePass.passes[x]!=pass) continue;
			bOk = true;
			break;
		}
		if( !bOk)
			return false;

		bStandAlonePass = true;
		shape = renderPass->getHairShape();
		node = renderPass->getHairShapeNode();
		dn.setObject(node);
	}

	if(!shape)
		return false;

	cls::IRender* contextcache = context->objectContext(inode.getObject(), this);
	if( !contextcache)
		return false;

	int timestamp;
	std::string hairrootfilename;
	contextcache->GetParameter("@Hair::positionfilename", hairrootfilename);
	contextcache->GetParameter("@Hair::timestamp", timestamp);

	if( hairrootfilename.empty())
		hairrootfilename = shape->GetHairRootPositionFile(timestamp);

	// HairSystem
	HairSystem hairsystem;
	{
		MDGContext context;
		shape->LoadHairSystem(context, hairsystem, pass);
		if( bStandAlonePass)
		{
			hairsystem.passes.clear();
			hairsystem.passes.push_back(standAlonePass);
		}
		else
		{
			if(hairsystem.passes.empty())
				return false;
		}
	}

	// ������ 
	{
		Util::FileStream file;

		/*/
		{
			MObject node = inode.getObject();

			MFnDependencyNode dn(node);
			MTypeId tid = dn.typeId();
			if( tid != HairShape::id)
				return false;
			MPlug geom_plug(node, HairShape::i_geometry);
			MPlugArray pa;
			geom_plug.connectedTo(pa, true, true);
			if(pa.length()>0)
			{
				MObject val = pa[0].node();

				// ������� ��������� ����� ����������� ����������
				cls::INode* geomnode = context->getNodeForObject(val);
				// ������ ���������
				Math::Box3f lb;
				cls::renderCacheImpl<> cache;
				geomnode->Render( &cache, lb, Math::Matrix4f::id, context, cls::INode::WR_GEOMETRY);

				// ��������� ��� SystemCall � RenderCall 
				renderProxySkipRenderCall skipRender(render);
				cache.RenderGlobalAttributes(&skipRender);
				cache.Render(&skipRender);
			}
			else
			{
				displayString("FUR error: %s not connected to geometry", geom_plug.name().asChar());
			}
		}
		/*/

		// �����!!! � ���� �� �����!
		MDGContext dgcontext;
		shape->Save(file, box, dgcontext, hairsystem, render);
	}

	hairsystem.serializeOcs("@hairsystem", render, true, ocsdirectory.c_str(), inode.getName());


	// pruningScaleFactor
	double pruningScaleFactor=1;
	MPlug(node, HairShape::i_pruningScaleFactor).getValue(pruningScaleFactor);
	render->Attribute("@pruningScaleFactor", (float)pruningScaleFactor);

	render->Parameter("@Hair::positionfilename", hairrootfilename.c_str());
	render->Parameter("@Hair::timestamp", timestamp);

#ifndef SAS
	render->RenderCall("HairMath.dll@HairRenderProc");
#else
	render->RenderCall("chevelureMath.dll@HairRenderProc");
#endif
	return true;
}




/*/
struct CustomObjectNode : public cls::nodeProxy
{
	MObject obj;

	CustomObjectNode(INode* node, MObject& obj):cls::nodeProxy(node)
	{
		this->obj = obj;
	}
	virtual MObject getObject()
	{
		return obj;
	}
};


//! ��������� ���������� �������� �������� ���� ������� ����� �������������� ��������� ��������� 
void HairSkinFilterDeformGenerator::OnSetupNode(
	cls::INode& inode,				//!< ������ 
	cls::IExportContext* context,
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();
	Util::DllProcedure dllproc;
	dllproc.LoadFormated("SkinFilterDeform", "OcellarisExport");

	cls::GENERATOR_CREATOR pc = (cls::GENERATOR_CREATOR)dllproc.proc;
	cls::IGenerator* gen = (*pc)();
	cls::IDeformGenerator* defgen = (cls::IDeformGenerator*)gen;

	MFnDependencyNode dn(node);
	MTypeId tid = dn.typeId();
	if( tid != HairShape::id)
		return;

	HairShape* shape = NULL;
	shape = (HairShape*)dn.userNode();

	MPlug geom_plug(node, HairShape::i_geometry);
	MPlugArray pa;
	geom_plug.connectedTo(pa, true, true);
	if(pa.length()<0)
		return;

	MObject val = pa[0].node();
	CustomObjectNode cnode(&inode, val);

	defgen->OnSetupNode(cnode, context, MObject::kNullObj);
}
/*/

/*/
//! render ����� ���� = NULL
void HairSkinFilterDeformGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,				//!< render
	cls::enSystemCall drawCallType,		//!< ��� ������ ��������� ��������: I_POINTS, I_CURVES, I_MESH ...
	cls::IExportContext* context,
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();
	Util::DllProcedure dllproc;
	dllproc.LoadFormated("SkinFilterDeform", "OcellarisExport");

	cls::GENERATOR_CREATOR pc = (cls::GENERATOR_CREATOR)dllproc.proc;
	cls::IGenerator* gen = (*pc)();
	cls::IDeformGenerator* defgen = (cls::IDeformGenerator*)gen;

	MFnDependencyNode dn(node);
	MTypeId tid = dn.typeId();
	if( tid != HairShape::id)
		return;

	MPlug geom_plug(node, HairShape::i_geometry);
	MPlugArray pa;
	geom_plug.connectedTo(pa, true, true);
	if(pa.length()<0)
		return;

	MObject val = pa[0].node();
	CustomObjectNode cnode(&inode, val);

	node = val;
	defgen->OnSetupNode(cnode, context, MObject::kNullObj);
	MStatus stat;
	MFnMesh mesh(node, &stat);
	if( !stat) 
		return;
	int vc = mesh.numVertices();
	cls::PA<Math::Vec3f> P(cls::PI_VERTEX, vc, cls::PT_VECTOR);
	MFloatPointArray _verts;
	mesh.getPoints(_verts);
	for(unsigned v=0; v<_verts.length(); v++)
	{
		MFloatPoint& pt = _verts[v];
		Math::Vec3f p(pt.x, pt.y, pt.z);
		P[v]=p;
	}
	render->Parameter("P", P);

	MFloatVectorArray normals;
	mesh.getNormals(normals);
	cls::PA< Math::Vec3f> geometry_normal(cls::PI_PRIMITIVEVERTEX, normals.length(), cls::PT_NORMAL);
	for(v=0; v<normals.length(); v++)
	{
		MFloatVector& pt = normals[v];
		Math::Vec3f p(pt.x, pt.y, pt.z);
		geometry_normal[v]=p;
	}
	render->Parameter("N", geometry_normal);

	defgen->OnGeometry(cnode, render, drawCallType, context, MObject::kNullObj);
}
/*/


	/*/
//! render ����� ���� = NULL
void HairExternVNDeformGenerator::OnGeometry(
	cls::INode& inode,				//!< ������ 
	cls::IRender* render,				//!< render
	cls::enSystemCall drawCallType,		//!< ��� ������ ��������� ��������: I_POINTS, I_CURVES, I_MESH ...
	cls::IExportContext* context,
	MObject& generatornode			//!< generator node �.� 0
	)
{
	MObject node = inode.getObject();

	MFnDependencyNode dn(node);
	MTypeId tid = dn.typeId();
	if( tid != HairShape::id)
		return;

	MPlug geom_plug(node, HairShape::i_geometry);
	MPlugArray pa;
	geom_plug.connectedTo(pa, true, true);
	if(pa.length()<0)
		return;

	MObject val = pa[0].node();

	// ��� ����
	if( render)
	{
		MStatus stat;
		MObject node = val;
		MFnDependencyNode dn(node, &stat);
		std::string name;
		if(stat) name = dn.name().asChar();

		// �������� ��������
		int n = name.find(':');
		if( n!=name.npos) name = name.substr(n+1);

		// ������� ���������
		cls::INode* geomnode = context->getNodeForObject(node);
		// ������ ���������
		Math::Box3f lb;
		cls::renderCacheImpl<> cache;
		geomnode->Render( &cache, lb, Math::Matrix4f::id, context, cls::INode::WR_GEOMETRY);

		// ��������� ��� SystemCall � RenderCall 
		renderProxySkipRenderCall skipRender(render);
		cache.RenderGlobalAttributes(&skipRender);
		cache.Render(&skipRender);

		{
//			std::string attrname = name+"::P";
	//	render->GlobalAttribute(attrname.c_str(), P);
//			render->ParameterFromAttribute("P", attrname.c_str());
		}

		{
//		std::string attrname = name+"::N";
	//	render->GlobalAttribute(attrname.c_str(), N);
		
		render->Parameter("Hair::NormalsIsFacevertex", true);

//		render->ParameterFromAttribute("N", attrname.c_str());
		}
	}
}
	/*/
