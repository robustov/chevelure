#pragma once

#include "pre.h"
#include <maya/MPxLocatorNode.h>
#include "HairMath.h"
#include "HairRenderPassData.h"
class HairShape;
 
class HairRenderPassNode : public MPxNode//MPxLocatorNode
{
public:
	HairRenderPassNode();
	virtual ~HairRenderPassNode(); 

	MObject getHairShapeNode() const;
	HairShape* getHairShape() const;
	bool IsStandAlone() const;

	virtual MStatus compute( 
		const MPlug& plug, 
		MDataBlock& data );

	/*/
	virtual bool isBounded() const;
	virtual MBoundingBox boundingBox() const; 
	virtual void draw(
		M3dView &view, const MDagPath &path, 
		M3dView::DisplayStyle style,
		M3dView::DisplayStatus status);
	/*/

	static void* creator();
	static MStatus initialize();

public:
	static MObject i_enable;

	static MObject o_standAlone;

	static MObject i_source;
	static MObject i_bRenderClumps;
	static MObject i_degradation, i_degradationWidth;
	static MObject i_Shader;

	static MObject i_RenderWidth;
	static MObject i_mulWidthByLength;
	static MObject i_useClumpRadiusAsWidth;

	static MObject i_SegmCount;

	static MObject i_interpolation;

	static MObject i_diceHair;
	static MObject i_sigmaHiding;
	static MObject i_renderDoubleSide;
	static MObject i_reverseOrientation;

	static MObject i_renderNormalSource;
	static MObject i_renderRayTraceEnable;
	static MObject i_renderRayCamera;
	static MObject i_renderRayTrace;
	static MObject i_renderRayPhoton;

	static MObject i_useDetails;
	static MObject i_DetailRange[4];

	// pass
	static MObject i_final;
	static MObject i_shadow;
	static MObject i_reflection;
	static MObject i_environment;
	static MObject i_depth;
	static MObject i_reference;
	static MObject i_archive;
	static MObject i_photon;

	// blur
	static MObject i_MotionBlur;

	// Prune
	static MObject i_bPrune;
	static MObject i_pruningrate;			// �������
	static MObject i_prunedistmin;			// ��������� ������ ������������
	static MObject i_prunemin_value;		// ����������� ��������
	static MObject i_prunemin_width;		// ������ ��� ����������� ��������
	static MObject i_pruneTestDistance;		// �������� ��������� ��������

	// 13.03.2008
	static MObject i_widthBias;				// ������� � w

	// 22.08.2008
	static MObject i_opacity;				// ������������

	// out
	static MObject o_output;

	MStatus ComputeOutput(const MPlug& plug, MDataBlock& data);

	static void CreateDetailRangeAttr(const char* name, MObject i_DetailRange[4]);
	void LoadDetailRangeAttr( MDataBlock& data, MObject i_DetailRange[4], HairRenderPass::DetailRange& v);

public:
	static MTypeId id;
	static const MString typeName;

};


