//
// Copyright (C) 
// File: HairClumpPositionsDataCmd.cpp
// MEL Command: HairClumpPositionsData

#include "HairClumpPositionsData.h"

#include <maya/MGlobal.h>
#include <maya/MFloatMatrix.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MRenderUtil.h>
#include "Math/interpolation.h"
#include "MathNMaya/MayaFileStream.h"
#include "MathNMaya/MayaSerialize.h"
#include <sstream>
#include "MathNMaya/MathNMaya.h"
#include <maya/MFnDynSweptGeometryData.h>
#include <maya/MDynSweptTriangle.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatArray.h>
#include <maya/MProgressWindow.h>
#include "HairRootPositions.h"

// �������������
bool HairClumpPositionsData::Init(
	MObject thisObject, 
	HairGeometryIndices* geometryIndices, 
	HairGeometry* geometry, 
	int CLUMPcount, 
	int resX, int resY
//	int ClumpDensityVersion
	)
{
	MFnDependencyNode dn(thisObject);

	hc.clear();

	// ��������� clump!
	bool bClump = CLUMPcount!=0;
	if( !bClump)
		return false;

	bool bProgress = (!::hair_suppressProgress) && (MGlobal::mayaState()==MGlobal::kInteractive);

	if( bProgress)
	{
		MProgressWindow::reserve();
		MProgressWindow::setInterruptable(false);
		MProgressWindow::setTitle("Hair clumps");
		MProgressWindow::startProgress();
		MProgressWindow::setProgressRange(0, geometryIndices->faceCount());
		MProgressWindow::setProgressStatus("Hair clumps");
	}

	// area
	double area = 0;
	for(unsigned f=0; f<geometryIndices->faceCount(); f++)
	{
		Math::Face32& face = geometryIndices->face(f);
		Math::Vec3f &pt0 = geometry->vertex(face.v[0]);
		Math::Vec3f &pt1 = geometry->vertex(face.v[1]);
		Math::Vec3f &pt2 = geometry->vertex(face.v[2]);
		Math::Vec3f v10 = pt1 - pt0;
		Math::Vec3f v20 = pt2 - pt0;
		float a = Math::cross(v10,v20).length();

		area += a;
	}

	double density = 1;
	std::vector<float> densityarray;
//	int resX=128, resY=128;

	// Read density
	MPlug densityplug = dn.findPlug("CLUMPdensity");
	MPlugArray plar;
	densityplug.connectedTo(plar, true, false);
	if( plar.length()!=1)
	{
		densityplug.getValue(density);
	}
	else
	{
		MFloatArray uCoords(resX*resY), vCoords(resX*resY);
		for(int u=0; u<resX; u++)
		{
			for(int v=0; v<resY; v++)
			{
				int ind = u*resY + v;
				uCoords[ind] = (float)(u+0.5)/resX;
				vCoords[ind] = (float)(v+0.5)/resY;
			}
		}

		MPlug plug = plar[0];
		MFloatMatrix cameraMat;
		MFloatVectorArray colors, transps;
		if( MRenderUtil::sampleShadingNetwork( 
				plug.name(), resX*resY,
				false, false, cameraMat,
				NULL,
				&uCoords, &vCoords,
				NULL, NULL, NULL, NULL, NULL,
				colors, transps ) )
		{
			densityarray.resize(transps.length());
			for(unsigned i=0; i<transps.length(); i++)
				densityarray[i] = (colors[i][0]+colors[i][1]+colors[i][2])/3;
		}
	}

//	hc.uvs.clear();
	hc.clumps.clear();
	hc.clumps.reserve(CLUMPcount + geometryIndices->faceCount());

//	hc.uvs.reserve(count + geometryIndices->faces.size());
	hc.clumpgroups.resize(geometryIndices->faceCount());
	for(unsigned f=0; f<geometryIndices->faceCount(); f++)
	{
		if( bProgress)
			MProgressWindow::setProgress(f);

		HairGeometryIndices::facegroup& facegroup = hc.clumpgroups[f];
		facegroup.startindex = hc.clumps.size();
		facegroup.endindex = hc.clumps.size();
		srand( geometryIndices->faceSeed(f)+1000);
		Math::Face32& face = geometryIndices->face(f);

		// verts
		const Math::Vec3f &pt0 = geometry->vertex(face.v[0]);
		const Math::Vec3f &pt1 = geometry->vertex(face.v[1]);
		const Math::Vec3f &pt2 = geometry->vertex(face.v[2]);

		// uvs
		Math::Face32& face_uv = geometryIndices->faceUV(f);
		const Math::Vec2f& uv0 = geometry->uv( face_uv.v[0] );
		const Math::Vec2f& uv1 = geometry->uv( face_uv.v[1] );
		const Math::Vec2f& uv2 = geometry->uv( face_uv.v[2] );

		// area
		Math::Vec3f v10 = pt1 - pt0;
		Math::Vec3f v20 = pt2 - pt0;
		float a = Math::cross(v10,v20).length();
		double fa = a/area;
		int fc = (int)(CLUMPcount*fa);

		float dt = rand()/(float)RAND_MAX;
		if( dt <= (float)CLUMPcount*fa - fc)
			fc++;

		for(int i=0; i<fc; i++)
		{
			// ������������
			float dt = rand()/(float)RAND_MAX;

			Math::Vec3f b;
			b.x = rand()/(float)RAND_MAX;
			b.y = rand()/(float)RAND_MAX;
			if(b.y+b.x > 1) 
			{ 
				b.x = 1-b.x;
				b.y = 1-b.y;
				std::swap(b.x, b.y);
			}
			b.z = 1 - b.x - b.y;
			b = b/(b.x+b.y+b.z);
			Math::Vec2f uv = uv0*b.x + uv1*b.y + uv2*b.z;

			// ���������:
			double d = density;
			if( !densityarray.empty())
			{
				int iu0, iu1, iv0, iv1;
				float w00, w01, w10, w11;
				Math::interpolation(uv.x, uv.y, resX, resY, 
					iu0, iu1, iv0, iv1, 
					w00, w01, w10, w11, 
					false, false);

				int i00 = resY*iu0 + iv0;
				int i01 = resY*iu0 + iv1;
				int i10 = resY*iu1 + iv0;
				int i11 = resY*iu1 + iv1;
				d = densityarray[i00]*w00 + densityarray[i01]*w01 + densityarray[i10]*w10 + densityarray[i11]*w11;
			}
			if(dt>d)
				continue;

			HairGeometryIndices::pointonsurface hr;
			hr.faceindex = f;
			hr.decimationValue = rand()/(float)RAND_MAX;
			hr.b = b;
			hc.clumps.push_back(hr);

			// uv
//			uvs.push_back(uv);
			// facegroup.count
			facegroup.endindex++;
		}
	}

	// clumpseeds
	hc.clumpseeds.clear();
	hc.clumpseeds.resize(hc.clumps.size());
	for(unsigned i=0; i<hc.clumps.size(); i++)
	{
		hc.clumpseeds[i] = rand();
	}

	if( bProgress)
		MProgressWindow::endProgress();
	return true;
}

bool HairClumpPositionsData::Init(
	HairGeometryIndices* hgh, 
	HairGeometry* geometry, 
	Math::matrixMxN<unsigned char>& matrix
	)
{
	/*/
	{
		printf("--\n");
		for(int y=0; y<matrix.sizeY(); y++)
		{
			for(int x=0; x<matrix.sizeX(); x++)
			{
				printf( matrix[y][x]>128?"#":".");
			}
			printf("\n");
		}
	}
	/*/

	hc.clumps.clear();
	hc.clumpgroups.clear();
	hc.clumpseeds.clear();

	float dx = 1.f/matrix.sizeX();
	float dy = 1.f/matrix.sizeY();

	Math::Vec2f jitter(0, 0);

	hc.clumpgroups.resize(hgh->faceCount());
	for(unsigned f=0; f<hgh->faceCount(); f++)
	{
		HairGeometryIndices::facegroup& facegroup = hc.clumpgroups[f];
		facegroup.startindex = hc.clumps.size();
		facegroup.endindex = hc.clumps.size();
		Math::Face32& face = hgh->face(f);

		// ��������� ��� ����� ���������� �� �����������

		// uvs
		Math::Face32& face_uv = hgh->faceUV(f);
		Math::Vec2f uv[3];
		uv[0] = geometry->uv( face_uv.v[0] );
		uv[1] = geometry->uv( face_uv.v[1] );
		uv[2] = geometry->uv( face_uv.v[2] );

		Math::Box2f box;
		box.insert(uv[0]);
		box.insert(uv[1]);
		box.insert(uv[2]);

		Math::Box2i ibox;
		ibox.min.x = (int)floor( (box.min.x)/dx - jitter.x);
		ibox.max.x = (int)ceil(  (box.max.x)/dx + jitter.x);
		ibox.min.y = (int)floor( (box.min.y)/dy - jitter.y);
		ibox.max.y = (int)ceil(  (box.max.y)/dy + jitter.y);

		Math::Vec2i ipt;
		for( ipt.x = ibox.min.x; ipt.x<=ibox.max.x; ipt.x++)
		{
			for(ipt.y = ibox.min.y;ipt.y<=ibox.max.y; ipt.y++)
			{
				Math::Vec2i nipt;
				nipt.x = Math::align_norma(ipt.x, matrix.sizeX());
				nipt.y = Math::align_norma(ipt.y, matrix.sizeY());

				unsigned char flag = matrix[nipt.y][nipt.x];
				if(flag<128) continue;

				Math::Vec2f pt( (nipt.x+0.5f)*dx, (nipt.y+0.5f)*dy);
				if( !Math::IsInFace(uv, pt)) continue;
				Math::Vec3f b = Math::CalcBaryCoords(uv, pt);

//				matrix[nipt.y][nipt.x] = 0;

				HairGeometryIndices::pointonsurface hr;
				hr.faceindex = f;
				hr.decimationValue = rand()/(float)RAND_MAX;
				hr.b = b;
				hc.clumps.push_back(hr);

				// facegroup.count
				facegroup.endindex++;
			}
		}
	}

	hc.clumpseeds.resize(hc.clumps.size());
	for(unsigned i=0; i<hc.clumps.size(); i++)
	{
		hc.clumpseeds[i] = rand();
	}
	return true;
}

void HairClumpPositionsData::CalcClumpRadius(
	HairRootPositions* hp, 
	HairClumpPositions* hc, 
	HairGeometryIndices* hgh, 
	HairGeometry* geometry
	)
{
	/////////////////////////////////////////
	// ��������� ������ ������!
	hc->radius.resize(hc->clumps.size(), 0.f);
	unsigned int vert;
	for(vert=0; vert<hc->radius.size(); vert++)
	{
		hc->radius[vert] = 0;
	}
	// ���� ���� ��� ����� ������ �� ������ ����������� ������ �������
	std::vector<Math::Vec3d> clumpcenters(hc->clumps.size(), Math::Vec3d(0));
	std::vector<int> clumphaircount(hc->clumps.size(), 0);
	for(vert=0; vert<hp->hairs.size(); vert++)
	{
		int ci = hp->getClumpIndex(vert);
		if(ci<0) 
			continue;
		if(ci>=(int)hc->radius.size())
			continue;
		Math::Vec3f hpos = hp->getPOS(vert, hgh, geometry);

		clumpcenters[ci] += hpos;
		clumphaircount[ci]++;
	}
	for(unsigned int ci=0; ci<hc->radius.size(); ci++)
	{
		if( clumphaircount[ci]!=0)
			clumpcenters[ci] = clumpcenters[ci]*(1.0/clumphaircount[ci]);
		else
			clumpcenters[ci] = hc->getPOS(ci, hgh, geometry);
	}

	for(vert=0; vert<hp->hairs.size(); vert++)
	{
		int ci = hp->getClumpIndex(vert);
		if(ci<0) 
			continue;
		if(ci>=(int)hc->radius.size())
			continue;

		Math::Vec3f hpos = hp->getPOS(vert, hgh, geometry);
		Math::Vec3f cpos_ = hc->getPOS(ci, hgh, geometry);
		Math::Vec3d cpos = clumpcenters[ci];
		float d = 2*(hpos-cpos).length();

		hc->radius[ci] = __max(hc->radius[ci], d);
	}
}

HairClumpPositionsData::HairClumpPositionsData()
{
}
HairClumpPositionsData::~HairClumpPositionsData()
{
}

MTypeId HairClumpPositionsData::typeId() const
{
	return HairClumpPositionsData::id;
}

MString HairClumpPositionsData::name() const
{ 
	return HairClumpPositionsData::typeName; 
}

void* HairClumpPositionsData::creator()
{
	return new HairClumpPositionsData();
}

void HairClumpPositionsData::copy( const MPxData& other )
{
	const HairClumpPositionsData* arg = (const HairClumpPositionsData*)&other;

	this->hc = arg->hc;
}

MStatus HairClumpPositionsData::readASCII(  
	const MArgList& args,
    unsigned& lastParsedElement )
{
    MStatus stat;
	int argLength = args.length();

	if(argLength!=1)
	{
		displayString("HairClumpPositionsData: args.length() != 1");
		return MS::kFailure;
	}
	MString str = args.asString(0);
	int l = str.length();
	const char* d = str.asChar();

	std::string strin(d);
	std::istringstream in(strin);
	MayaFileStream stream(in, true);
	if( !this->serialize(stream)) 
		return MS::kFailure;

	return MS::kSuccess;
}

MStatus HairClumpPositionsData::writeASCII( 
	std::ostream& out )
{
// ����� ����� ��� �������� ������ � �������� ��������!!!!!!
//return MS::kFailure;

    MStatus stat;

	MayaFileStream stream(out, true);
	serialize(stream);
    return MS::kSuccess;
}

MStatus HairClumpPositionsData::readBinary( 
	std::istream& in, 
	unsigned length )
{
	MStatus stat;
	if ( length <= 0 ) 
		return MS::kFailure;

	MayaFileStream stream(in);
	if( !serialize(stream))
		return MS::kFailure;

	return MS::kSuccess;
}

MStatus HairClumpPositionsData::writeBinary( 
	std::ostream& out )
{
	MStatus stat;

	MayaFileStream stream(out);
	serialize(stream);

    return MS::kSuccess;
}

bool HairClumpPositionsData::serialize(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version>=0)
	{
		hc.serialize(stream);
	}
	return true;
}

