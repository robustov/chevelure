#include "HairDeformer.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnStringArrayData.h>

#include <maya/MGlobal.h>
#include "HairArtistShape2.h"
#include "HairShape.h"
#include "textureCacheData.h"

MObject HairDeformer::i_Deformer[2];
MObject HairDeformer::i_textureCacheAffect;
MObject HairDeformer::o_output;
MObject HairDeformer::i_envelope[3];
MObject HairDeformer::i_PaintResXY[2];

HairDeformer::HairDeformer()
{
}
HairDeformer::~HairDeformer()
{
}
MStatus HairDeformer::setDependentsDirty( 
	const MPlug& src, 
	MPlugArray& dst)
{
	if( src == i_textureCacheAffect)
		return MS::kSuccess;

	displayStringD("setDependentsDirty %s", src.name().asChar());
	dst.append( MPlug(thisMObject(), o_output));

	{
		MFnDependencyNode dn(src.node());
		MString name = src.partialName(false, false, false, false, false, true);

		MDataBlock data = forceCache();
		MObject val = data.inputValue(i_textureCacheAffect).data();
//		MPlug textureCacheAffectPlug(thisMObject(), i_textureCacheAffect);
//		MObject val;
//		textureCacheAffectPlug.getValue(val);

		MFnStringArrayData valdata(val);
		MString curaffect;
		for(int i=0; i<(int)valdata.length(); i++)
		{
			if( (i&0x1) == 0)
			{
				curaffect = valdata[i];
			}
			else
			{
				displayStringD("def %d: %s", i, valdata[i].asChar());
				if( valdata[i] == name)
				{
					MPlug plug = dn.findPlug(curaffect);
					if( !plug.isNull())
						dst.append( plug);
					break;
				}
			}
		}
	}

	return MS::kSuccess;
}

MStatus HairDeformer::compute( 
	const MPlug& plug, 
	MDataBlock& data )
{
	MStatus stat;
	displayStringD("HairDeformer::compute %s", plug.name().asChar());

	if( plug == o_output )
	{
		return ComputeOutput(plug, data, this->i_Deformer, this->i_envelope);
	}
	if( plug == i_envelope[1])
		return ComputeTextureCache3(plug, i_envelope, this->i_PaintResXY, data);

	if( plug.isDynamic())
	{
		// ������� �����������
		MFnDependencyNode dn(plug.node());
		MObject val = data.inputValue(i_textureCacheAffect).data();
		MFnStringArrayData valdata(val);
		MString name = plug.partialName(false, false, false, false, false, true);

		MObject attr[4];
		int z = 0;
		bool bMy = false;
		for(int i=0; i<(int)valdata.length(); i++)
		{
			if( (i&0x1) == 0)
			{
				bMy = false;
				if( valdata[i] == name)
					bMy = true;
				continue;
			}
			if( bMy)
			{
				MPlug plug = dn.findPlug(valdata[i]);
				if( plug.isNull())
					break;	// ������
				
				attr[z++] = plug.attribute();
				if( z==1) 
					attr[z++] = plug.attribute();
			}
		}
		if(z == 4)
			return ComputeTextureCache4(plug, attr, this->i_PaintResXY, data);
		else
			return MS::kFailure;
	}
	return MS::kUnknownParameter;
}

MStatus HairDeformer::ComputeOutput(
	const MPlug& plug, 
	MDataBlock& data, 
	MObject i_Deformer[2], 
	MObject i_envelope[3])
{
	MStatus stat;

	HairDeformerData* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( plug.attribute(), &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<HairDeformerData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( HairDeformerData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<HairDeformerData*>(fnDataCreator.data( &stat ));
//		displayString("new 0x%x", pData);
		bNewData = true;
	}
displayStringD( "HairDeformer::ComputeOutput");

	MString dll = data.inputValue( i_Deformer[0], &stat ).asString();
	MString proc = data.inputValue( i_Deformer[1], &stat ).asString();
	pData->deformerproc.Load(dll.asChar(), proc.asChar());
	pData->stream.open();
	if( pData->deformerproc.isValid())
	{
		IHairDeformer* deformer = (*pData->deformerproc)();

		// AFFECT
		{
			MPxData* _data = data.inputValue(i_envelope[1]).asPluginData();
			if(_data)
			{
				Math::ParamTex2d<unsigned char>* param = (Math::ParamTex2d<unsigned char>*)&deformer->affect;
				textureCacheData* texturecachedata = static_cast<textureCacheData*>(_data);
				*param = texturecachedata->paramTex2d;
			}
		}

		HairArtistShape2 snShape(thisMObject());
		deformer->fromUI(&snShape);
		pData->stream >> deformer->affect;
		deformer->serialise(pData->stream);
		deformer->release();
	}
	pData->stream.close();

	if( bNewData)
	{
//		displayString("setUp 0x%x", pData);
		outputData.set(pData);
	}
	outputData.setClean();

	return MS::kSuccess;
}

MStatus HairDeformer::ComputeTextureCache4(
	const MPlug& plug, 
	MObject attr[4], 
	MObject i_PaintResXY[2], 
	MDataBlock& data)
{
	MStatus stat;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( plug, &stat );
	MPxData* userdata = outputData.asPluginData();
	textureCacheData* pData = static_cast<textureCacheData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( textureCacheData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<textureCacheData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}
	int resX = data.inputValue(i_PaintResXY[0], &stat).asInt();
	int resY = data.inputValue(i_PaintResXY[1], &stat).asInt();

	MPlug src(thisMObject(), attr[0]);
	displayStringD("HairDeformer::ComputeTextureCache4 for %s", src.name().asChar());

	MFnNumericAttribute numattr( attr[0]);
	if( numattr.unitType()==MFnNumericData::kDouble)
	{
		double jitter = data.inputValue(attr[2]).asDouble();
		MString uvsetname = data.inputValue(attr[3]).asString();
		pData->setFloats(src, data, resX, resY, jitter, uvsetname.asChar());
	}
	else
		pData->setVectors(src, data, resX, resY);


	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}

MStatus HairDeformer::ComputeTextureCache3(const MPlug& plug, MObject attr[3], MObject i_PaintResXY[2], MDataBlock& data)
{
	MStatus stat;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( plug, &stat );
	MPxData* userdata = outputData.asPluginData();
	textureCacheData* pData = static_cast<textureCacheData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( textureCacheData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<textureCacheData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}
	int resX = data.inputValue(i_PaintResXY[0], &stat).asInt();
	int resY = data.inputValue(i_PaintResXY[1], &stat).asInt();

	MPlug src(thisMObject(), attr[0]);
	const char* pname = src.name().asChar();
	displayStringD("HairDeformer::ComputeTextureCache4 for %s", pname);

	MFnNumericAttribute numattr( attr[0]);
	if( numattr.unitType()==MFnNumericData::kDouble)
	{
		double jitter = data.inputValue(attr[2]).asDouble();
//		MString uvsetname = data.inputValue(attr[3]).asString();
		pData->setFloats(src, data, resX, resY, jitter, NULL);
	}
	else
		pData->setVectors(src, data, resX, resY);


	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}

void* HairDeformer::creator()
{
	return new HairDeformer();
}

MStatus HairDeformer::initialize()
{
	MStatus stat = initialize("", "", i_Deformer, i_envelope, i_PaintResXY, o_output);
	if(!stat) return stat;

	if( !SourceMelFromResource(MhInstPlugin, "AEHairDeformerTemplate", "MEL", defines))
	{
		displayString("error source AEHairDeformer_WindTemplate.mel");
	}
	return stat;
}

MStatus HairDeformer::initialize(
	const char* dll, const char* proc, 
	MObject i_Deformer[2], 
	MObject i_envelope[3], 
	MObject i_PaintResXY[2],
	MObject& o_output)
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute	typedAttr;
	MFnStringData stringData;
	MStatus				stat;

	try
	{
		{
			i_Deformer[0] = typedAttr.create( "deformerDll", "ddll",
				MFnData::kString, stringData.create(dll, &stat), &stat );
			::addAttribute(i_Deformer[0], atInput);

			i_Deformer[1] = typedAttr.create( "deformerProc", "dproc",
				MFnData::kString, stringData.create(proc, &stat), &stat );
			::addAttribute(i_Deformer[1], atInput);
		}
		{
			i_textureCacheAffect = typedAttr.create( "textureCacheAffect", "txca",
				MFnData::kStringArray, &stat );
			::addAttribute(i_textureCacheAffect, atInput|atHidden);
		}
		{
			i_PaintResXY[0] = numAttr.create("PaintAttrResX","parx", MFnNumericData::kInt, 16, &stat);
			::addAttribute(i_PaintResXY[0], atInput|atHidden);
			i_PaintResXY[1] = numAttr.create("PaintAttrResY","pary", MFnNumericData::kInt, 16, &stat);
			::addAttribute(i_PaintResXY[1], atInput|atHidden);
		}

		{
			o_output = typedAttr.create( "o_output", "out", HairDeformerData::id);
			::addAttribute(o_output, atReadable|atUnWritable|atConnectable|atStorable|atHidden);

			MObject isAffected = o_output;

			attributeAffects( i_PaintResXY[0], isAffected);
			attributeAffects( i_PaintResXY[1], isAffected);
			attributeAffects( i_Deformer[0], isAffected);
			attributeAffects( i_Deformer[1], isAffected);
			attributeAffects( i_textureCacheAffect, isAffected);
		}
		bool bStorableAttributes = true;
		{
			if( !CreateAttrNTextureCache("envelope","env", i_envelope, 0, 1, 1.0, bStorableAttributes, o_output, i_PaintResXY))
				return MS::kFailure;
			MObject isAffected = o_output;
			HairShape::attributeAffectsT( i_envelope, isAffected);
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

MStatus HairDeformer::CreateAttrNTextureCache( 
	std::string longname, std::string shortname, 
	MObject* i_HairAttr, double vmin, double vmax, double def, bool bStorableAttributes, 
	MObject& o_output, MObject i_PaintResXY[2])
{
	MStatus stat;

	// Value
	MFnNumericAttribute numAttr;
	i_HairAttr[0] = numAttr.create(longname.c_str(), shortname.c_str(), MFnNumericData::kDouble, def, &stat);
	if (!stat) return stat;
	numAttr.setStorable(true);
	numAttr.setHidden(false);
	numAttr.setSoftMin(vmin);	
	numAttr.setSoftMax(vmax);	
	stat = addAttribute (i_HairAttr[0]);	
	if (!stat) return stat;
	attributeAffects(i_HairAttr[0], o_output);

	// Cache
	CreateTextureCacheAttr( longname+"Cache", shortname+"ch", i_HairAttr[0], i_HairAttr[1], bStorableAttributes, o_output, i_PaintResXY);

	// jitter
	longname += "Jitter";
	shortname+= "jt";
	i_HairAttr[2] = numAttr.create(longname.c_str(), shortname.c_str(), MFnNumericData::kDouble, 0, &stat);
	numAttr.setSoftMin(0);	
	numAttr.setSoftMax(1);	
	::addAttribute(i_HairAttr[2], atInput);	
	attributeAffects(i_HairAttr[2], o_output);
	attributeAffects(i_HairAttr[2], i_HairAttr[1]);
	if (!stat) return stat;

	return MS::kSuccess;
}

void HairDeformer::CreateTextureCacheAttr(
	std::string longName, std::string shortName, MObject& src, MObject& attr, bool bStorableAttributes, MObject& o_output, MObject i_PaintResXY[2])
{
	MStatus stat;
	MFnTypedAttribute	typedAttr;

	attr = typedAttr.create( longName.c_str(), shortName.c_str(), textureCacheData::id);
	typedAttr.setStorable(false);
	typedAttr.setReadable(true);
	typedAttr.setWritable(true);
	typedAttr.setHidden(true);
	stat = addAttribute(attr);
	if (!stat) 
	{
		displayString("cand create attr %s", longName);
		return;
	}
	attributeAffects(src,				attr);
	attributeAffects(i_PaintResXY[0],	attr);
	attributeAffects(i_PaintResXY[1],	attr);
	attributeAffects(attr, o_output);
}
