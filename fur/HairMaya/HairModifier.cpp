//
// Copyright (C) 
// File: HairModifierCmd.cpp
// MEL Command: HairModifier
#include "pre.h"
#include "HairModifier.h"
#include "Math/Box.h"
#include "MathNMaya/MathNMaya.h"
#include "MathNGl/MathNGl.h"
#include "HairShape.h"
#include "HairDeformerData.h"

#include <Maya/MFnUnitAttribute.h>
#include <Maya/MFnEnumAttribute.h>
#include <Maya/MFnCompoundAttribute.h>
#include <maya/MGlobal.h>

MObject HairModifier::i_testWM;

MObject HairModifier::i_type;
MObject HairModifier::i_directionType;

// Ramp
MObject HairModifier::i_affect_ramp;

MObject HairModifier::i_PaintAttrResX;
MObject HairModifier::i_PaintAttrResY;

// Deforming
MObject HairModifier::i_HairBaseWidth[4];
MObject HairModifier::i_HairTipWidth[4];
MObject HairModifier::i_HairBaseClump[4];
MObject HairModifier::i_HairTipClump[4];

MObject HairModifier::i_bRelative;
MObject HairModifier::i_HairLength[4];
MObject HairModifier::i_HairInclination[4];
MObject HairModifier::i_HairPolar[4];
MObject HairModifier::i_HairBaseCurl[4];
MObject HairModifier::i_HairTipCurl[4];
MObject HairModifier::i_HairScraggle[4];
MObject HairModifier::i_HairScraggleFreq[4];
MObject HairModifier::i_HairTwist[4];
MObject HairModifier::i_HairDisplace[4];
MObject HairModifier::i_HairDeformers;
// out
MObject HairModifier::o_output;

HairModifier::HairModifier()
{
}
HairModifier::~HairModifier()
{
}

MStatus HairModifier::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;

	if( plug == i_HairLength[1])
		return ComputeTextureCache(i_HairLength, data);
	if( plug == i_HairInclination[1])
		return ComputeTextureCache(i_HairInclination, data);
	if( plug == i_HairPolar[1])
		return ComputeTextureCache(i_HairPolar, data);
	if( plug == i_HairBaseCurl[1])
		return ComputeTextureCache(i_HairBaseCurl, data);
	if( plug == i_HairTipCurl[1])
		return ComputeTextureCache(i_HairTipCurl, data);
	if( plug == i_HairScraggle[1])
		return ComputeTextureCache(i_HairScraggle, data);
	if( plug == i_HairScraggleFreq[1])
		return ComputeTextureCache(i_HairScraggleFreq, data);
	if( plug == i_HairTwist[1])
		return ComputeTextureCache(i_HairTwist, data);
	if( plug == i_HairDisplace[1])
		return ComputeTextureCache(i_HairDisplace, data);

	if( plug == i_HairBaseWidth[1])
		return ComputeTextureCache(i_HairBaseWidth, data);
	if( plug == i_HairTipWidth[1])
		return ComputeTextureCache(i_HairTipWidth, data);
	if( plug == i_HairBaseClump[1])
		return ComputeTextureCache(i_HairBaseClump, data);
	if( plug == i_HairTipClump[1])
		return ComputeTextureCache(i_HairTipClump, data);

	if( plug == o_output )
		return HairModifier::ComputeOutput(plug, data);

	return MS::kUnknownParameter;
}

MStatus HairModifier::ComputeOutput(const MPlug& plug, MDataBlock& data)
{
	MStatus stat;

	HairModifierData* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( plug.attribute(), &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<HairModifierData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( HairModifierData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<HairModifierData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}
displayStringD( "HairModifier::ComputeOutput");

	if( !pData->Init())
		return MS::kFailure;

	MDataHandle tdh = data.inputValue(i_testWM);

	MDagPath path;
	MDagPath::getAPathTo(thisMObject(), path);
	copy( pData->data.transform, path.inclusiveMatrixInverse());

	MRampAttribute rampattr(thisMObject(), this->i_affect_ramp);
	pData->data.affectRamp.set(rampattr, 0.f, 1.f, 1.f);

	if( !LoadMeshSurface(*pData, data))
		return MS::kFailure;

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();

	return MS::kSuccess;
}

bool HairModifier::LoadMeshSurface(HairModifierData& meshsurface, MDataBlock& data)
{
	MStatus stat;
	short type = data.inputValue(i_type).asShort();
	short directionType = data.inputValue(i_directionType).asShort();

	meshsurface.data.modiferType = (HairMeshModifier::enType)type;
	meshsurface.data.directionType = (HairMeshModifier::enDirectionType)directionType;

	meshsurface.data.bRelative = data.inputValue( i_bRelative).asBool();
	meshsurface.setArray( HairModifierData::T_LENGTH,		data.inputValue( i_HairLength[1]).asPluginData());
	meshsurface.setArray( HairModifierData::T_INCLINATION,	data.inputValue( i_HairInclination[1]).asPluginData());
	meshsurface.setArray( HairModifierData::T_POLAR,		data.inputValue( i_HairPolar[1]).asPluginData());
	meshsurface.setArray( HairModifierData::T_BASECURL,		data.inputValue( i_HairBaseCurl[1]).asPluginData());
	meshsurface.setArray( HairModifierData::T_TIPCURL,		data.inputValue( i_HairTipCurl[1]).asPluginData());
	meshsurface.setArray( HairModifierData::T_BASEWIDTH,	data.inputValue( i_HairBaseWidth[1]).asPluginData());
	meshsurface.setArray( HairModifierData::T_TIPWIDTH,		data.inputValue( i_HairTipWidth[1]).asPluginData());
	meshsurface.setArray( HairModifierData::T_SCRAGGLE,		data.inputValue( i_HairScraggle[1]).asPluginData());
	meshsurface.setArray( HairModifierData::T_SCRAGGLEFREQ,	data.inputValue( i_HairScraggleFreq[1]).asPluginData());
	meshsurface.setArray( HairModifierData::T_TWIST,		data.inputValue( i_HairTwist[1]).asPluginData());
	meshsurface.setArray( HairModifierData::T_DISPLACE,		data.inputValue( i_HairDisplace[1]).asPluginData());
	meshsurface.setArray( HairModifierData::T_BASECLUMP,	data.inputValue( i_HairBaseClump[1]).asPluginData());
	meshsurface.setArray( HairModifierData::T_TIPCLUMP,		data.inputValue( i_HairTipClump[1]).asPluginData());

	meshsurface.data.hairParamBit = 0;
	meshsurface.data.hairParamBit |= data.inputValue( i_HairLength[3]).asBool()?HPB_LENGTH:0;
	meshsurface.data.hairParamBit |= data.inputValue( i_HairInclination[3]).asBool()?HPB_INCLINATION:0;
	meshsurface.data.hairParamBit |= data.inputValue( i_HairPolar[3]).asBool()?HPB_POLAR:0;
	meshsurface.data.hairParamBit |= data.inputValue( i_HairBaseCurl[3]).asBool()?HPB_BASECURL:0;
	meshsurface.data.hairParamBit |= data.inputValue( i_HairTipCurl[3]).asBool()?HPB_TIPCURL:0;
	meshsurface.data.hairParamBit |= data.inputValue( i_HairBaseWidth[3]).asBool()?HPB_BASEWIDTH:0;
	meshsurface.data.hairParamBit |= data.inputValue( i_HairTipWidth[3]).asBool()?HPB_TIPWIDTH:0;

	meshsurface.data.hairParamBit |= data.inputValue( i_HairScraggle[3]).asBool()?HPB_SCRAGLE:0;
	meshsurface.data.hairParamBit |= data.inputValue( i_HairScraggleFreq[3]).asBool()?HPB_SCRAGLEFREQ:0;

	meshsurface.data.hairParamBit |= data.inputValue( i_HairBaseClump[3]).asBool()?HPB_BASECLUMP:0;
	meshsurface.data.hairParamBit |= data.inputValue( i_HairTipClump[3]).asBool()?HPB_TIPCLUMP:0;
	meshsurface.data.hairParamBit |= data.inputValue( i_HairTwist[3]).asBool()?HPB_TWIST:0;
	meshsurface.data.hairParamBit |= data.inputValue( i_HairDisplace[3]).asBool()?HPB_DISPLACE:0;

	LoadDeformers(i_HairDeformers, data, meshsurface.data.deformers);
	return true;
}

void HairModifier::LoadDeformers(MObject i_HairDeformers, MDataBlock& data, std::vector<HairDeformerDecl>& deformers)
{
	MStatus stat;
	deformers.clear();
	MArrayDataHandle arh = data.inputArrayValue(i_HairDeformers, &stat);
	if( stat)
	{
		deformers.reserve(arh.elementCount());
		for( unsigned i=0; i<arh.elementCount(); i++)
		{
			if( arh.jumpToArrayElement(i))
			{
				MPxData* pxdata = arh.inputValue(&stat).asPluginData();
				if( stat && pxdata && pxdata->typeId() == HairDeformerData::id)
				{
					HairDeformerData* sndata = (HairDeformerData*)pxdata;
					deformers.push_back(HairDeformerDecl());
					HairDeformerDecl& decl = deformers.back();
					decl.deformerproc = sndata->deformerproc;
					decl.deformerstream = sndata->stream;
					decl.Load();
				}
			}
		}
	}
}



MStatus HairModifier::ComputeTextureCache(const MPlug& dst, const MPlug& src, MDataBlock& data)
{
	displayStringD("HairShape::ComputeTextureCache %s", dst.name().asChar());
	MStatus stat;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( dst, &stat );
	MPxData* userdata = outputData.asPluginData();
	textureCacheData* pData = static_cast<textureCacheData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( textureCacheData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<textureCacheData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}
	int resX = data.inputValue(i_PaintAttrResX, &stat).asInt();
	int resY = data.inputValue(i_PaintAttrResY, &stat).asInt();

	MFnNumericAttribute attr( src.attribute());
	if( attr.unitType()==MFnNumericData::kDouble)
		pData->setFloats(src, data, resX, resY, 0, "");
	else
		pData->setVectors(src, data, resX, resY);


	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}
MStatus HairModifier::ComputeTextureCache(MObject attr[3], MDataBlock& data)
{
	MStatus stat;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( attr[1], &stat );
	MPxData* userdata = outputData.asPluginData();
	textureCacheData* pData = static_cast<textureCacheData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( textureCacheData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<textureCacheData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}
	int resX = data.inputValue(i_PaintAttrResX, &stat).asInt();
	int resY = data.inputValue(i_PaintAttrResY, &stat).asInt();

	MPlug src(thisMObject(), attr[0]);
	displayStringD("HairShape::ComputeTextureCache for %s", src.name().asChar());

	MFnNumericAttribute numattr( attr[0]);
	if( numattr.unitType()==MFnNumericData::kDouble)
	{
		double jitter = data.inputValue(attr[2]).asDouble();
		pData->setFloats(src, data, resX, resY, jitter, "");
	}
	else
		pData->setVectors(src, data, resX, resY);


	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}

bool HairModifier::isBounded() const
{
	MObject thisNode = thisMObject();
	return true;
}

MBoundingBox HairModifier::boundingBox() const
{
	MObject thisNode = thisMObject();

	MBoundingBox box;
	Math::Box3f mbox(1);
	copy( box, mbox);
	//...
	return box;
}

void HairModifier::draw(
	M3dView &view, 
	const MDagPath &path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status)
{ 
	MObject thisNode = thisMObject();

	MPlug plug(thisNode, i_type);
	short stype;
	plug.getValue(stype);
	HairMeshModifier::enType type = (HairMeshModifier::enType)stype;

	Math::Box3f box(1);
	view.beginGL(); 
	switch( type)
	{
		case HairMeshModifier::MT_SPHERE:
			drawSphere();
			break;
		case HairMeshModifier::MT_CYLINDER:
			drawCircleXZ(1, Math::Vec3f(0, 0.5, 0));
			drawCircleXZ(1, Math::Vec3f(0, -0.5, 0));
			glBegin( GL_LINES );
			//�������
			glVertex3f(Math::Vec3f(0.05f, 0.45f, 0));
			glVertex3f(Math::Vec3f(0.0, 0.5f, 0));
			glVertex3f(Math::Vec3f(-0.05f, 0.45f, 0));
			glVertex3f(Math::Vec3f(0.0, 0.5f, 0));

			// 
			glVertex3f(Math::Vec3f(0, 0.5f, 0));
			glVertex3f(Math::Vec3f(0, -0.5f, 0));
			glVertex3f(Math::Vec3f(0.5f, 0.5f, 0));
			glVertex3f(Math::Vec3f(0.5f, -0.5f, 0));
			glVertex3f(Math::Vec3f(-0.5f, 0.5f, 0));
			glVertex3f(Math::Vec3f(-0.5f, -0.5f, 0));
			glVertex3f(Math::Vec3f(0, 0.5f, 0.5f));
			glVertex3f(Math::Vec3f(0, -0.5f, 0.5f));
			glVertex3f(Math::Vec3f(0, 0.5f, -0.5f));
			glVertex3f(Math::Vec3f(0, -0.5f, -0.5f));
			glEnd();
			break;
	}
//	drawBox(box);
	view.endGL();
};


void* HairModifier::creator()
{
	return new HairModifier();
}

MStatus HairModifier::initialize()
{
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnEnumAttribute enumAttr;
	MFnTypedAttribute	typedAttr;
	MFnUnitAttribute unitAttr;
	MFnStringData stringData;
	MStatus				stat;

	bool bStorableAttributes = true;

	try
	{
		{
			i_type = enumAttr.create ("modiferType", "mtp", 0);
			enumAttr.addField("SPHERE", HairMeshModifier::MT_SPHERE);
			enumAttr.addField("CYLINDER", HairMeshModifier::MT_CYLINDER);
			::addAttribute(i_type, atInput);

			i_directionType = enumAttr.create ("directionType", "mdt", 0);
			enumAttr.addField("NONE", HairMeshModifier::DT_NONE);
			enumAttr.addField("TO_CENTER", HairMeshModifier::DT_TO_CENTER);
			enumAttr.addField("X_AXIS", HairMeshModifier::DT_X_AXIS);
			enumAttr.addField("Y_AXIS", HairMeshModifier::DT_Y_AXIS);
			enumAttr.addField("Z_AXIS", HairMeshModifier::DT_Z_AXIS);
			::addAttribute(i_directionType, atInput);
		}

		i_affect_ramp = CreateRampAttr("affectRamp", "afr");
		::addAttribute(i_affect_ramp, atInput);

		{
			if( !CreateAttrNTextureCacheNFlag("BaseWidth","bW", i_HairBaseWidth, 0, 1, 1.0, bStorableAttributes))
				return MS::kFailure;
			if( !CreateAttrNTextureCacheNFlag("TipWidth","tW", i_HairTipWidth, 0, 1, 0.0, bStorableAttributes))
				return MS::kFailure;

			if( !CreateAttrNTextureCacheNFlag("BaseClump","bCl", i_HairBaseClump, 0, 1, 0.0, bStorableAttributes))
				return MS::kFailure;
			if( !CreateAttrNTextureCacheNFlag("TipClump","tCl", i_HairTipClump, 0, 1, 1.0, bStorableAttributes))
				return MS::kFailure;

		}

		// Deforming
		{
			i_PaintAttrResX = numAttr.create("PaintAttrResX","parx", MFnNumericData::kInt, 16, &stat);
			::addAttribute(i_PaintAttrResX, atInput);

			i_PaintAttrResY = numAttr.create("PaintAttrResY","pary", MFnNumericData::kInt, 16, &stat);
			::addAttribute(i_PaintAttrResY, atInput);

			i_bRelative = numAttr.create( "bRelative", "br", MFnNumericData::kBoolean, 1, &stat);
			::addAttribute(i_bRelative, atInput|atKeyable);

			if( !CreateAttrNTextureCacheNFlag("Length", "l", i_HairLength, 0.0001, 1, 1.0, bStorableAttributes))
				return MS::kFailure;
			if( !CreateAttrNTextureCacheNFlag("Inclination", "incl", i_HairInclination, 0, 1, 0.0, bStorableAttributes))
				return MS::kFailure;
			if( !CreateAttrNTextureCacheNFlag("Polar", "plr", i_HairPolar, 0, 1, 1.0, bStorableAttributes))
				return MS::kFailure;
			if( !CreateAttrNTextureCacheNFlag("BaseCurl", "bC", i_HairBaseCurl, 0, 1, 0.5, bStorableAttributes))
				return MS::kFailure;
			if( !CreateAttrNTextureCacheNFlag("TipCurl", "tC", i_HairTipCurl, 0, 1, 0.5, bStorableAttributes))
				return MS::kFailure;
			if( !CreateAttrNTextureCacheNFlag("Scraggle", "s", i_HairScraggle, 0, 1, 0.0, bStorableAttributes))
				return MS::kFailure;
			if( !CreateAttrNTextureCacheNFlag("scraggleFreq", "sF", i_HairScraggleFreq, 0, 1, 0.0, bStorableAttributes))
				return MS::kFailure;
			if( !CreateAttrNTextureCacheNFlag("twist","twi", i_HairTwist, 0, 1, 0.5, bStorableAttributes))
				return MS::kFailure;
			if( !CreateAttrNTextureCacheNFlag("displace","dis", i_HairDisplace, 0, 1, 0, bStorableAttributes))
				return MS::kFailure;

			i_HairDeformers = typedAttr.create( "hairDeformers", "hdefs", HairDeformerData::id, MObject::kNullObj, &stat);
			typedAttr.setWritable(true);
			typedAttr.setReadable(false);
			typedAttr.setArray(true);
			typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
			typedAttr.setIndexMatters(false);
			::addAttribute(i_HairDeformers, 
				atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atHidden|atArray);
		}
		{
			i_testWM = typedAttr.create( "testWM", "twm", MFnData::kMatrix, &stat);
			::addAttribute(i_testWM, atInput);
		}
		{
			o_output = typedAttr.create( "o_output", "out", HairModifierData::id);
			::addAttribute(o_output, atOutput|atHidden);

			MObject isAffected = o_output;

			HairShape::attributeAffects( i_type, isAffected);
			HairShape::attributeAffects( i_directionType, isAffected);
			HairShape::attributeAffects( i_affect_ramp, isAffected);
			HairShape::attributeAffects( i_testWM, isAffected);
//			HairShape::attributeAffects( worldPosition, isAffected);
//			HairShape::attributeAffects( localScale, isAffected);
//			HairShape::attributeAffects( localPosition, isAffected);
//			HairShape::attributeAffects( worldMatrix, isAffected);
//			HairShape::attributeAffects( parentMatrix, isAffected);

			HairShape::attributeAffectsT4( i_HairBaseWidth, isAffected);
			HairShape::attributeAffectsT4( i_HairTipWidth, isAffected);
			HairShape::attributeAffectsT4( i_HairBaseClump, isAffected);
			HairShape::attributeAffectsT4( i_HairTipClump, isAffected);

			attributeAffects( i_bRelative, isAffected);
			HairShape::attributeAffectsT4( i_HairLength, isAffected);
			HairShape::attributeAffectsT4( i_HairInclination, isAffected);
			HairShape::attributeAffectsT4( i_HairPolar, isAffected);
			HairShape::attributeAffectsT4( i_HairBaseCurl, isAffected);
			HairShape::attributeAffectsT4( i_HairTipCurl, isAffected);
			HairShape::attributeAffectsT4( i_HairScraggle, isAffected);
			HairShape::attributeAffectsT4( i_HairScraggleFreq, isAffected);
			HairShape::attributeAffectsT4( i_HairTwist, isAffected);
			HairShape::attributeAffectsT4( i_HairDisplace, isAffected);
			attributeAffects( i_HairDeformers, isAffected);

		}

		if( !SourceMelFromResource(MhInstPlugin, "AEHairModifierTemplate", "MEL", defines))
		{
			displayString("error source AEHairModifierTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}
	return MS::kSuccess;
}

MStatus HairModifier::CreateAttrNTextureCacheNFlag( 
	std::string longname, std::string shortname, 
	MObject* i_HairAttr, double vmin, double vmax, double def, bool bStorableAttributes)
{
	MStatus stat = CreateAttrNTextureCache(
		longname, shortname, 
		i_HairAttr, vmin, vmax, def, bStorableAttributes);
	if( !stat) return stat;

	longname += "Flag";
	shortname+= "f";
	MFnNumericAttribute numAttr;
	i_HairAttr[3] = numAttr.create(longname.c_str(), shortname.c_str(), MFnNumericData::kBoolean, 0, &stat);
	::addAttribute(i_HairAttr[3], atInput);
	attributeAffects(i_HairAttr[3], o_output);
	return stat;
}

MStatus HairModifier::CreateAttrNTextureCache( 
	std::string longname, std::string shortname, 
	MObject* i_HairAttr, double vmin, double vmax, double def, bool bStorableAttributes)
{
	MStatus stat;

	// Value
	MFnNumericAttribute numAttr;
	i_HairAttr[0] = numAttr.create(longname.c_str(), shortname.c_str(), MFnNumericData::kDouble, def, &stat);
	if (!stat) return stat;
	numAttr.setStorable(true);
	numAttr.setHidden(false);
	numAttr.setSoftMin(vmin);	
	numAttr.setSoftMax(vmax);	
	stat = addAttribute (i_HairAttr[0]);	
	if (!stat) return stat;
	attributeAffects(i_HairAttr[0], o_output);

	// Cache
	CreateTextureCacheAttr( longname+"Cache", shortname+"ch", i_HairAttr[0], i_HairAttr[1], bStorableAttributes);

	// jitter
	longname += "Jitter";
	shortname+= "jt";
	i_HairAttr[2] = numAttr.create(longname.c_str(), shortname.c_str(), MFnNumericData::kDouble, 0, &stat);
	numAttr.setSoftMin(0);	
	numAttr.setSoftMax(1);	
	::addAttribute(i_HairAttr[2], atInput);	
	attributeAffects(i_HairAttr[2], o_output);
	attributeAffects(i_HairAttr[2], i_HairAttr[1]);
	if (!stat) return stat;

	return MS::kSuccess;
}

void HairModifier::CreateTextureCacheAttr(
	std::string longName, std::string shortName, MObject& src, MObject& attr, bool bStorableAttributes)
{
	MStatus stat;
	MFnTypedAttribute	typedAttr;

	attr = typedAttr.create( longName.c_str(), shortName.c_str(), textureCacheData::id);
	typedAttr.setStorable(false);
	typedAttr.setReadable(true);
	typedAttr.setWritable(true);
	typedAttr.setHidden(true);
	stat = addAttribute(attr);
	if (!stat) 
	{
		displayString("cand create attr %s", longName);
		return;
	}
	attributeAffects(src,				attr);
	attributeAffects(i_PaintAttrResX,	attr);
	attributeAffects(i_PaintAttrResY,	attr);
	attributeAffects(attr, o_output);
}

MObject HairModifier::CreateRampAttr(
	std::string fullname, 
	std::string shortname)
{
	MObject rampchilds[3];
	MObject i_RampAttr;
	MStatus stat;
	MFnNumericAttribute nAttr;
	MFnEnumAttribute eAttr;
	MFnCompoundAttribute cAttr;
	std::string fn, sn;

	fn = fullname + "_Position";
	sn = shortname + std::string("p");
	rampchilds[0] = nAttr.create(fn.c_str(), sn.c_str(), MFnNumericData::kFloat, 0.0);
	float vmin = -1;
	float vmax = 1;
	
	nAttr.setSoftMin(vmin);
	nAttr.setSoftMax(vmax);
//	stat = addAttribute(i_HairAttr[1]);

	fn = fullname + std::string("_FloatValue");
	sn = shortname + std::string("fv");
	rampchilds[1] = nAttr.create(fn.c_str(), sn.c_str(), MFnNumericData::kFloat, 0.0);
	nAttr.setSoftMin(vmin);
	nAttr.setSoftMax(vmax);
//	stat = addAttribute(fv);

	fn = std::string(fullname) + std::string("_Interp");
	sn = std::string(shortname) + std::string("i");
	rampchilds[2] = eAttr.create(fn.c_str(), sn.c_str());
	eAttr.addField("None",   0);
	eAttr.addField("Linear", 1);
	eAttr.addField("Smooth", 2);
	eAttr.addField("Spline", 3);
//	stat = addAttribute(in);

	i_RampAttr = cAttr.create(fullname.c_str(), shortname.c_str());
	cAttr.addChild(rampchilds[0]);
	cAttr.addChild(rampchilds[1]);
	cAttr.addChild(rampchilds[2]);
	cAttr.setWritable(true);
	cAttr.setStorable(true);
	cAttr.setKeyable(false);
	cAttr.setArray(true);
	return i_RampAttr;
}