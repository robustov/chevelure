#include "dl_HairExport.h"
#include "HairRFMexport.h"

#include "MathNMaya/MathNMaya.h"

#include "HairShape.h"
#include "Util/FileStream.h"
#include "Util/currentHostName.h"
#include "Util/misc_create_directory.h"

// ���� � �������� ��������
extern std::string chevelurebinpath;

HairRFMexport::HairRFMexport(void)
{
}

HairRFMexport::~HairRFMexport(void)
{
}

MSyntax HairRFMexport::newSyntax()			// ����������� ����� �������������� �����������
{
	MStatus stat;
	MSyntax syntax;

	stat = syntax.addArg(MSyntax::kString);	// object
	stat = syntax.addFlag("f", "filename", MSyntax::kString); // 

	return syntax;
}

MStatus HairRFMexport::doIt( const MArgList& args)
{
	MStatus stat = MS::kSuccess;
	MArgDatabase argData(syntax(), args);

	MString objname;
	argData.getCommandArgument(0, objname);

	MString filename;
	if( !argData.getFlagArgument("f", 0, filename))
	{
		std::string host = Util::currentHostName();
		displayStringError("MayaHair: -f must be specified. Host %s!", host.c_str());
		return MS::kFailure;
	}


	MObject obj;
	nodeFromName(objname, obj);
	MFnDependencyNode dn(obj);

	MDagPath path;
	MDagPath::getAPathTo(obj, path);
	Math::Matrix4f thisTransform;
	::copy( thisTransform, path.inclusiveMatrix());

	HairShape* shape = NULL;
	bool bStandAlonePass = false;
	HairRenderPass standAlonePass;

	MTypeId tid = dn.typeId();
	if( tid == HairShape::id)
	{
		shape = (HairShape*)dn.userNode();
	}
	if(!shape)
	{
		displayStringError("HairExport shape is not HairShape");
		return MS::kFailure;
	}

	// HairSystem
	HairSystem hairsystem;
	{
		MDGContext context;
		shape->LoadHairSystem(context, hairsystem, "all");
		if( bStandAlonePass)
		{
			hairsystem.passes.clear();
			hairsystem.passes.push_back(standAlonePass);
		}
		else
		{
			if(hairsystem.passes.empty())
				return MS::kFailure;
		}
	}
	Math::Matrix4f mview;
	hairsystem.camera_worldpos = mview;
	hairsystem.this_worldpos = thisTransform;
	hairsystem.passforocs = "";
	hairsystem.currentpassname = "";

	int timestamp;
	MPlug(obj, HairShape::i_HairTimeStamp).getValue(timestamp);

	std::string hairrootfilename = shape->GetHairRootPositionFile(timestamp);
	if( hairrootfilename.empty()) 
	{
		displayStringError("HairExport shape failed: Hair Root File");
		return MS::kFailure;
	}

	displayString("Hair Root Position File = \"%s\"", hairrootfilename.c_str());

	Util::create_directory_for_file(filename.asChar());

	Util::FileStream file;
	if( !file.open(filename.asChar(), true))
	{
		std::string host = Util::currentHostName();
		displayStringError("MayaHair: cant write file %s Host %s!", filename, host.c_str());
		return MS::kFailure;
	}
	file >> hairrootfilename;
	file >> timestamp;

	// HairSystem
	hairsystem.serialize(file);

	Math::Box3f box;

	MDGContext context;//(time);
	shape->Save(file, box, context, hairsystem);

	MDoubleArray res(6);
	res[0] = box.min.x;
	res[1] = box.max.x;
	res[2] = box.min.y;
	res[3] = box.max.y;
	res[4] = box.min.z;
	res[5] = box.max.z;
	setResult(res);

	return stat;
}

void* HairRFMexport::creator()
{
	return new HairRFMexport();
}
