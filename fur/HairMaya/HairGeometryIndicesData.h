#pragma once

#include "pre.h"
#include <maya/MPxData.h>
#include <maya/MTypeId.h>
#include <maya/MString.h>
#include <maya/MArgList.h>
#include "Util/Stream.h"
#include "HairGeometryIndices.h"

/*/
	USING:
	{
		MFnTypedAttribute fnAttr;
		MObject newAttr = fnAttr.create( 
			"fullName", "briefName", HairGeometryIndicesData::id );
	}
/*/


class HairGeometryIndicesData : public MPxData
{
public:
	HairGeometryIndices hgh;
public:
	// �������������
	bool Init(
		MObject obj, 
		int seed, 
		int nSubdiv, 
		MIntArray& brokenEdges, 
		MString uvSetName);
	// �������������
	static bool Init(
		HairGeometryIndices& hgh, 
		MObject obj, 
		int seed, 
		int nSubdiv, 
		MIntArray& brokenEdges, 
		MString uvSetName);

public:
	HairGeometryIndicesData();
	virtual ~HairGeometryIndicesData(); 

	// Override methods in MPxData.
    virtual MStatus readASCII( const MArgList&, unsigned& lastElement );
    virtual MStatus readBinary( std::istream& in, unsigned length );
    virtual MStatus writeASCII( std::ostream& out );
    virtual MStatus writeBinary( std::ostream& out );

	// Custom methods.
    virtual void copy( const MPxData& ); 

	MTypeId typeId() const; 
	MString name() const;
	static void* creator();

public:
	static MTypeId id;
	static const MString typeName;

protected:
	bool serialize(Util::Stream& stream);
};
