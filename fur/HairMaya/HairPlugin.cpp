//#include "Hair.h"
#include "HairShape.h"
#include "Util/misc_create_directory.h"
#include "HairShapeUI.h"
#include "DrawCache.h"
#include "dl_HairExport.h"
#include "getsetFurCmd.h"
#include "UlHairSetNullSnShape.h"
#include "textureCacheData.h"
#include "HairGeometryIndicesData.h"
#include "HairGeometryData.h"
#include "HairRootPositionsData.h"
#include "HairClumpPositionsData.h"
#include "HairFixedFileData.h"
//#include "UlHairConnectCVCurves.h"
#include "HairCvData.h"
#include "HairEmitter.h"
#include "HairModifier.h"
#include "HairModifierData.h"
#include "HairDeformerData.h"
#include "HairDeformer.h"
#include "HairDeformer_Wind.h"
#include "HairDeformer_Leaf.h"
#include "HairDeformer_ClumpRamp.h"
#include "HairStartBrowserCmd.h"
#include "HairRenderPassNode.h"
#include "HairRenderPassData.h"
#include "HairOcsGenerator.h"
#include "HairCombTool.h"
#include "HairBrushProxy.h"
#include "HairUtilCmd.h"
#include "HairControlsShape.h"
#include "HairControlsUI.h"
#include "HairCvBasesData.h"
#include "HairControlsAnim.h"
#include "ocHairTestShader.h"
#include "HairControlsAnimData.h"
#include "HairArrayMapper.h"
#include "HairDeformer_LenghtRamp.h"
#include "HairRFMexport.h"

#include "ocHairShader.h"


//#ifndef SAS
#include "ocellaris/OcellarisExport.h"
#pragma comment( lib, "OcellarisExport" )
//#endif

HairOcsGenerator hairOcsGenerator;

bool hair_suppressProgress = false;

#ifdef OCELLARIS_INSTANCE
#pragma comment( lib, "OcellarisExport" )
#endif//OCELLARIS_INSTANCE

#include <maya/MFnPlugin.h>   

// ������� ��� �������� ��������
std::map<std::string, std::string> defines;

// ���� � �������� ��������
std::string chevelurebinpath;

MStatus uninitializePlugin(MObject obj);

MStatus initializePlugin(MObject obj)
{
// ������� ��� �������� ��������
#ifndef SAS
	defines["#HAIR#"] = "Hair";
#else
	defines["#HAIR#"] = "chevelure";
#endif

// ������� ��� �������� ��������
#ifndef CHEVELUREDEMO
	defines["#DEMO#"] = "0";
#else
	defines["#DEMO#"] = "1";
#endif

#ifndef CHEVELURELIGHT
	defines["#LIGHT#"] = "0";
#else
	defines["#LIGHT#"] = "1";
#endif

	int buildN; 
	std::string buildDate, versionstring;
	HairGetVersion( buildN, buildDate, versionstring);

#ifdef SAS

	displayString("CHEVELURE. Build #%d. %s", buildN, buildDate.c_str());
#endif// SAS

	// ��� �������� �����
	char filename[512]; 
	GetModuleFileName( (HMODULE)MhInstPlugin, filename, 512); 
	chevelurebinpath = Util::extract_foldername(filename);
#ifdef HAIR_TRACELOG
	fprintf(stderr, "%s\n", filename);
#endif

#ifdef SAS
#ifndef CHEVELUREDEMO
	HairLicenseStart();
#endif
#endif


	MStatus _Status, stat;
	MFnPlugin plugin(obj, "Mitka", versionstring.c_str(), "Any");



	if( !MGlobal::executeCommand("loadPlugin -qt ocellarisMaya.mll", true))
	{
		_Status.perror("Hair: Error loadPlugin ocellarisMaya.mll");
		uninitializePlugin(obj);
		return _Status;
	}

// #include "HairArrayMapper.h"
	stat = plugin.registerNode( 
		HairArrayMapper::typeName, 
		HairArrayMapper::id, 
		HairArrayMapper::creator,
		HairArrayMapper::initialize, 
		MPxNode::kParticleAttributeMapperNode);
	if (!stat) 
	{
		displayString("cant register %s", HairArrayMapper::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	// #include "ocHairTestShader.h"
	{
//		const MString UserClassify( "texture/2d" );
		const MString UserClassify( "shader/surface" );
		stat = plugin.registerNode( 
			ocHairTestShader::typeName, 
			ocHairTestShader::id, 
			ocHairTestShader::creator,
			ocHairTestShader::initialize,
			MPxNode::kDependNode,
			&UserClassify );
		if (!stat) 
		{
			displayString("cant register %s", ocHairTestShader::typeName.asChar());
			stat.perror("registerNode");
			uninitializePlugin(obj);
			return stat;
		}
		registerGenerator(ocHairTestShader::id, &ocHairTestShader::generator);
	}

// #include "ocHairShader.h"
	{
//		const MString UserClassify( "texture/2d" );
		const MString UserClassify( "shader/surface" );
		stat = plugin.registerNode( 
			ocHairShader::typeName, 
			ocHairShader::id, 
			ocHairShader::creator,
			ocHairShader::initialize,
			MPxNode::kDependNode,
			&UserClassify );
		if (!stat) 
		{
			displayString("cant register %s", ocHairShader::typeName.asChar());
			stat.perror("registerNode");
			uninitializePlugin(obj);
			return stat;
		}
		registerGenerator(ocHairShader::id, &ocHairShader::generator);
	}

// #include "HairCvBasesData.h"
	stat = plugin.registerData( 
		HairCvBasesData::typeName, 
		HairCvBasesData::id, 
		HairCvBasesData::creator);
	if (!stat) 
	{
		displayString("cant register %s", HairCvBasesData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "HairCombTool.h"
// #include "HairBrushProxy.h"
	{
		stat = plugin.registerContextCommand(
			HairCombContextCmd::typeName.asChar(), 
			HairCombContextCmd::creator,
			HairCombTool::typeName,
			HairCombTool::creator,
			HairCombTool::newSyntax);
		if (!stat) 
		{
			displayString("cant register %s", HairCombContextCmd::typeName.asChar());
			stat.perror("registerCommand");
			uninitializePlugin(obj);
			return stat;
		}

		stat = plugin.registerNode(
			HairBrushProxy::typeName, 
			HairBrushProxy::id, 
			HairBrushProxy::creator,
			HairBrushProxy::initialize, 
			MPxNode::kLocatorNode );
		if (!stat) 
		{
			displayString("Can't register %s", HairBrushProxy::typeName.asChar());
			stat.perror("registerNode");
			uninitializePlugin(obj);
			return stat;
		}

		stat = HairCombContextCmd::initShelf();
	}

// #include "HairUtilCmd.h"
	stat = plugin.registerCommand( 
		HairUtilCmd::typeName.asChar(), 
		HairUtilCmd::creator, 
		HairUtilCmd::newSyntax );
	if (!stat) 
	{
		displayString("cant register %s", HairUtilCmd::typeName.asChar());
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerCommand( 
		HairRFMexport::typeName.asChar(), 
		HairRFMexport::creator, 
		HairRFMexport::newSyntax );
	if (!stat) 
	{
		displayString("cant register %s", HairRFMexport::typeName.asChar());
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

// #include "HairStartBrowserCmd.h"
	stat = plugin.registerCommand( 
		HairStartBrowserCmd::typeName.asChar(), 
		HairStartBrowserCmd::creator, 
		HairStartBrowserCmd::newSyntax );
	if (!stat) 
	{
		displayString("cant register %s", HairStartBrowserCmd::typeName.asChar());
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerData( 
		HairModifierData::typeName, 
		HairModifierData::id, 
		HairModifierData::creator);
	if (!stat) 
	{
		displayString("cant register %s", HairModifierData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerData( 
		HairRenderPassData::typeName, 
		HairRenderPassData::id, 
		HairRenderPassData::creator);
	if (!stat) 
	{
		displayString("cant register %s", HairRenderPassData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerData( 
		HairAliveData::typeName, 
		HairAliveData::id, 
		HairAliveData::creator);
	if (!stat) 
	{
		displayString("cant register node %s", HairAliveData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerData( 
		HairCVData::typeName, 
		HairCVData::id, 
		HairCVData::creator);
	if (!stat) 
	{
		displayString("cant register node %s", HairCVData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	/*/
	stat = plugin.registerCommand( UlHairConnectCVCurves::typeName, UlHairConnectCVCurves::creator, UlHairConnectCVCurves::newSyntax );
	if (!stat) 
	{
		displayString("cant register node %s", UlHairConnectCVCurves::typeName.asChar());
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}
	/*/

	stat = plugin.registerData( 
		HairCVPositionsData::typeName, 
		HairCVPositionsData::id, 
		HairCVPositionsData::creator);
	if (!stat) 
	{
		displayString("cant register node %s", HairCVPositionsData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerData( 
		HairFixedFileData::typeName, 
		HairFixedFileData::id, 
		HairFixedFileData::creator);
	if (!stat) 
	{
		displayString("cant register node %s", HairFixedFileData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerData( 
		HairGeometryData::typeName, 
		HairGeometryData::id, 
		HairGeometryData::creator);
	if (!stat) 
	{
		displayString("cant register node %s", HairGeometryData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerData( 
		HairClumpPositionsData::typeName, 
		HairClumpPositionsData::id, 
		HairClumpPositionsData::creator);
	if (!stat) 
	{
		displayString("cant register node %s", HairClumpPositionsData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerData( 
		HairGeometryIndicesData::typeName, 
		HairGeometryIndicesData::id, 
		HairGeometryIndicesData::creator);
	if (!stat) 
	{
		displayString("cant register node %s", HairGeometryIndicesData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerData( 
		HairRootPositionsData::typeName, 
		HairRootPositionsData::id, 
		HairRootPositionsData::creator);
	if (!stat) 
	{
		displayString("cant register node %s", HairRootPositionsData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerData( 
		textureCacheData::typeName, 
		textureCacheData::id, 
		textureCacheData::creator);
	if (!stat) 
	{
		displayString("cant register node %s", textureCacheData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerCommand( UlHairSetNullSnShape::typeName, UlHairSetNullSnShape::creator, UlHairSetNullSnShape::newSyntax );
	if (!stat) 
	{
		displayString("cant register node %s", UlHairSetNullSnShape::typeName.asChar());
		stat.perror("registerCommand");
		uninitializePlugin(obj);
		return stat;
	}

#ifndef NO_SN
	if( !MGlobal::executeCommand("loadPlugin -qt SnMaya.mll", true))
	{
		_Status.perror("Hair: Error loadPlugin SnMaya.mll");
		uninitializePlugin(obj);
		return _Status;
	}
#endif

// #include "HairDeformerData.h"
	stat = plugin.registerData( 
		HairDeformerData::typeName, 
		HairDeformerData::id, 
		HairDeformerData::creator);
	if (!stat) 
	{
		displayString("cant register %s", HairDeformerData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
// #include "HairDeformer.h"
	stat = plugin.registerNode( 
		HairDeformer::typeName, 
		HairDeformer::id, 
		HairDeformer::creator,
		HairDeformer::initialize );
	if (!stat) 
	{
		displayString("cant register %s", HairDeformer::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

//	#include "HairDeformer_LenghtRamp.h"
	stat = plugin.registerNode( 
		HairDeformer_LenghtRamp::typeName, 
		HairDeformer_LenghtRamp::id, 
		HairDeformer_LenghtRamp::creator,
		HairDeformer_LenghtRamp::initialize );
	if (!stat) 
	{
		displayString("cant register %s", HairDeformer_LenghtRamp::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "HairDeformer_Wind.h"
	stat = plugin.registerNode( 
		HairDeformer_Wind::typeName, 
		HairDeformer_Wind::id, 
		HairDeformer_Wind::creator,
		HairDeformer_Wind::initialize );
	if (!stat) 
	{
		displayString("cant register %s", HairDeformer_Wind::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "HairDeformer_ClumpRamp.h"
	stat = plugin.registerNode( 
		HairDeformer_ClumpRamp::typeName, 
		HairDeformer_ClumpRamp::id, 
		HairDeformer_ClumpRamp::creator,
		HairDeformer_ClumpRamp::initialize );
	if (!stat) 
	{
		displayString("cant register %s", HairDeformer_ClumpRamp::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "HairDeformer_Leaf.h"
	stat = plugin.registerNode( 
		HairDeformer_Leaf::typeName, 
		HairDeformer_Leaf::id, 
		HairDeformer_Leaf::creator,
		HairDeformer_Leaf::initialize );
	if (!stat) 
	{
		displayString("cant register %s", HairDeformer_Leaf::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "HairModifier.h"
	stat = plugin.registerNode( 
		HairModifier::typeName, 
		HairModifier::id, 
		HairModifier::creator,
		HairModifier::initialize, 
		MPxNode::kLocatorNode );
	if (!stat) 
	{
		displayString("cant register %s", HairModifier::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

// #include "HairRenderPassNode.h"
	stat = plugin.registerNode( 
		HairRenderPassNode::typeName, 
		HairRenderPassNode::id, 
		HairRenderPassNode::creator,
		HairRenderPassNode::initialize);//, 
//		MPxNode::kLocatorNode);
	if (!stat) 
	{
		displayString("cant register %s", HairRenderPassNode::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	plugin.registerCommand(dl_HairExport::typeName, dl_HairExport::creator, dl_HairExport::newSyntax);
	plugin.registerCommand(HairWorkingFacesCmd::typeName, HairWorkingFacesCmd::creator, HairWorkingFacesCmd::newSyntax);

// #include "HairControlsAnimData.h"
	stat = plugin.registerData( 
		HairControlsAnimData::typeName, 
		HairControlsAnimData::id, 
		HairControlsAnimData::creator);
	if (!stat) 
	{
		displayString("cant register %s", HairControlsAnimData::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerNode( 
		HairControlsAnim::typeName, 
		HairControlsAnim::id, 
		HairControlsAnim::creator,
		HairControlsAnim::initialize );
	if (!stat) 
	{
		displayString("cant register %s", HairControlsAnim::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}

	stat = plugin.registerShape( 
		HairControlsShape::typeName, 
		HairControlsShape::id,
		&HairControlsShape::creator,
		&HairControlsShape::initialize,
		&HairControlsUI::creator  
		);
	if (!stat)
	{
		displayString("cant register node %s", HairControlsShape::typeName.asChar());
		uninitializePlugin(obj);
		return stat;
	}

//	plugin.registerCommand(cmd_setHairValue::typeName, cmd_setHairValue::creator, cmd_setHairValue::newSyntax);

	_Status = plugin.registerShape( 
		HairShape::typeName, 
		HairShape::id, 
		HairShape::creator,
		HairShape::initialize, 
		&HairShapeUI::creator);
	if (!_Status)
	{
		displayString("cant register node %s", HairShape::typeName.asChar());
		uninitializePlugin(obj);
		return _Status;
	}

	registerGenerator(HairShape::id, &hairOcsGenerator);

#ifndef SAS
	stat = plugin.registerNode( 
		HairEmitter::typeName, 
		HairEmitter::id, 
		HairEmitter::creator,
		HairEmitter::initialize, 
		MPxNode::kEmitterNode );
	if (!stat) 
	{
		displayString("cant register node %s", HairEmitter::typeName.asChar());
		stat.perror("registerNode");
		uninitializePlugin(obj);
		return stat;
	}
#endif

	if( !SourceMelFromResource(MhInstPlugin, "HAIR", "MEL", defines))
	{
		displayString("error source HAIR.mel");
	}
	if( !SourceMelFromResource(MhInstPlugin, "HairForms", "MEL", defines))
	{
		displayString("error source HairForms.mel");
	}

#ifndef SAS
#else
	bool sourced = false;
	if( SourceMelFromResource(MhInstPlugin, "CHEVELUREMENU", "MEL", defines))
		sourced = true;
	else
		displayString("error source CHEVELUREMENU");

	if( MGlobal::sourceFile("chevelureMenu.mel"))
		sourced = true;
	else
		displayString("error source chevelureMenu.mel");

	if( sourced && !MGlobal::executeCommand("chevelureCreateMainMenu"))
	{
		displayString("Dont found chevelureMainMenu command.mel");
	}
#endif

	return _Status;
}

MStatus uninitializePlugin(MObject obj)
{
	MStatus status, stat;
	MFnPlugin plugin(obj);

#ifdef SAS
#ifndef CHEVELUREDEMO
	HairLicenseStop();
#endif
#endif

	// ��� ������� ���������
	HairCombContext::Unhook();

#ifdef SAS
	MGlobal::executeCommand("chevelureDeleteMainMenu");
#endif

	stat = plugin.deregisterNode( HairArrayMapper::id );
	if (!stat) displayString("cant deregister %s", HairArrayMapper::typeName.asChar());

	unregisterGenerator(ocHairTestShader::id, &ocHairTestShader::generator);
	stat = plugin.deregisterNode( ocHairTestShader::id );
	if (!stat) displayString("cant deregister %s", ocHairTestShader::typeName.asChar());

	unregisterGenerator(ocHairShader::id, &ocHairShader::generator);
	stat = plugin.deregisterNode( ocHairShader::id );
	if (!stat) displayString("cant deregister %s", ocHairShader::typeName.asChar());

	stat = plugin.deregisterData( HairCvBasesData::id );
	if (!stat) displayString("cant deregister %s", HairCvBasesData::typeName.asChar());

	stat = plugin.deregisterNode( HairBrushProxy::id );
	if (!stat) displayString("cant deregister %s", HairBrushProxy::typeName.asChar());

	stat = plugin.deregisterContextCommand(HairCombContextCmd::typeName.asChar(), HairCombTool::typeName.asChar());
	if (!stat) displayString("cant deregister %s", HairCombContextCmd::typeName.asChar());

	stat = plugin.deregisterCommand( HairUtilCmd::typeName );
	if (!stat) displayString("cant deregister %s", HairUtilCmd::typeName.asChar());

	stat = plugin.deregisterCommand( HairRFMexport::typeName );
	if (!stat) displayString("cant deregister %s", HairRFMexport::typeName.asChar());

	stat = plugin.deregisterCommand( HairStartBrowserCmd::typeName );
	if (!stat) displayString("cant deregister %s", HairStartBrowserCmd::typeName.asChar());

	stat = plugin.deregisterData( HairAliveData::id );
	if (!stat) displayString("cant deregister %s", HairAliveData::typeName.asChar());

#ifndef SAS
	stat = plugin.deregisterNode( HairEmitter::id );
	if (!stat) displayString("cant deregister %s", HairEmitter::typeName.asChar());
#endif

	stat = plugin.deregisterNode( HairDeformer::id );
	if (!stat) displayString("cant deregister %s", HairDeformer::typeName.asChar());

	stat = plugin.deregisterNode( HairDeformer_Wind::id );
	if (!stat) displayString("cant deregister %s", HairDeformer_Wind::typeName.asChar());

	stat = plugin.deregisterNode( HairDeformer_LenghtRamp::id );
	if (!stat) displayString("cant deregister %s", HairDeformer_LenghtRamp::typeName.asChar());

	stat = plugin.deregisterNode( HairDeformer_Leaf::id );
	if (!stat) displayString("cant deregister %s", HairDeformer_Leaf::typeName.asChar());

	stat = plugin.deregisterNode( HairDeformer_ClumpRamp::id );
	if (!stat) displayString("cant deregister %s", HairDeformer_ClumpRamp::typeName.asChar());

	stat = plugin.deregisterData( HairDeformerData::id );
	if (!stat) 
	{
		if (!stat) displayString("cant deregister %s", HairDeformerData::typeName.asChar());
		return stat;
	}

	stat = plugin.deregisterNode( HairModifier::id );
	if (!stat) displayString("cant deregister %s", HairModifier::typeName.asChar());

	stat = plugin.deregisterNode( HairRenderPassNode::id );
	if (!stat) displayString("cant deregister %s", HairRenderPassNode::typeName.asChar());

	stat = plugin.deregisterData( HairCVData::id );
	if (!stat) 
	{
		stat.perror("deregisterData");
		return stat;
	}

	/*/
	stat = plugin.deregisterCommand( UlHairConnectCVCurves::typeName );
	if (!stat) 
	{
		stat.perror("deregisterCommand");
		return stat;
	}
	/*/

	stat = plugin.deregisterData( HairCVPositionsData::id );
	if (!stat) 
	{
		stat.perror("deregisterData");
		return stat;
	}

	stat = plugin.deregisterData( HairFixedFileData::id );
	if (!stat) 
	{
		stat.perror("deregisterData");
		return stat;
	}

	stat = plugin.deregisterData( HairGeometryData::id );
	if (!stat) 
	{
		stat.perror("deregisterData");
		return stat;
	}

	stat = plugin.deregisterData( HairModifierData::id );
	if (!stat) 
	{
		stat.perror("deregisterData");
		return stat;
	}

	stat = plugin.deregisterData( HairRenderPassData::id );
	if (!stat) 
	{
		stat.perror("deregisterData");
		return stat;
	}

	stat = plugin.deregisterData( HairClumpPositionsData::id );
	if (!stat) 
	{
		stat.perror("deregisterData");
		return stat;
	}

	stat = plugin.deregisterData( HairGeometryIndicesData::id );
	if (!stat) 
	{
		stat.perror("deregisterData");
		return stat;
	}

	stat = plugin.deregisterData( HairRootPositionsData::id );
	if (!stat) 
	{
		stat.perror("deregisterData");
		return stat;
	}

	stat = plugin.deregisterData( textureCacheData::id );
	if (!stat) 
	{
		stat.perror("deregisterData");
		return stat;
	}

	stat = plugin.deregisterCommand( UlHairSetNullSnShape::typeName );
	if (!stat) 
	{
		stat.perror("deregisterCommand");
		return stat;
	}

	plugin.deregisterCommand(dl_HairExport::typeName);
	plugin.deregisterCommand(HairWorkingFacesCmd::typeName);
//	plugin.deregisterCommand(cmd_setHairValue::typeName);

	stat = plugin.deregisterNode( HairControlsShape::id );
	if ( ! stat ) 
	{
		stat.perror("deregisterCommand");
		return stat;
	}

	stat = plugin.deregisterNode( HairControlsAnim::id );
	if (!stat) displayString("cant deregister %s", HairControlsAnim::typeName.asChar());

	stat = plugin.deregisterData( HairControlsAnimData::id );
	if (!stat) displayString("cant deregister %s", HairControlsAnimData::typeName.asChar());

	status = plugin.deregisterNode( HairShape::id );
//#ifndef SAS
	unregisterGenerator(HairShape::id, &hairOcsGenerator);
//#endif

	return status;
}


