#include "HairShape.h"
#include "CVIterator.h"



bool HairShape::getInternalValue( const MPlug& plug, MDataHandle& result )
{
	bool isOk = true;

	if( (plug == mControlPoints) ||
		(plug == mControlValueX) ||
		(plug == mControlValueY) ||
		(plug == mControlValueZ) )
	{
		double val = 0.0;
		if ( (plug == mControlPoints) && !plug.isArray() ) 
		{
			MPoint pnt;
			int index = plug.logicalIndex();
			pnt = this->getValue(index);
			result.set( pnt[0], pnt[1], pnt[2] );
		}
		else if ( plug == mControlValueX ) 
		{
			MPlug parentPlug = plug.parent();
			int index = parentPlug.logicalIndex();
			val = this->getValue(index)[0];
			result.set( val );
		}
		else if ( plug == mControlValueY ) 
		{
			MPlug parentPlug = plug.parent();
			int index = parentPlug.logicalIndex();
			val = this->getValue(index)[1];
			result.set( val );
		}
		else if ( plug == mControlValueZ ) 
		{
			MPlug parentPlug = plug.parent();
			int index = parentPlug.logicalIndex();
			val = this->getValue(index)[2];
			result.set( val );
		}
	}
	else
	{
		isOk = MPxSurfaceShape::getInternalValue( plug, result );
	}
	return isOk;
}
bool HairShape::setInternalValue( const MPlug& plug, const MDataHandle& handle )
{
	bool isOk = true;

	if( (plug == mControlPoints) ||
		(plug == mControlValueX) ||
		(plug == mControlValueY) ||
		(plug == mControlValueZ) )
	{
		DrawCache* geometry = &this->geometry;
		if( plug == mControlPoints && !plug.isArray()) 
		{
			int index = plug.logicalIndex();
			MPoint point;
			double3& ptData = handle.asDouble3();
			point.x = ptData[0];
			point.y = ptData[1];
			point.z = ptData[2];
			this->setValue(index, point );
			verticesUpdated();
		}
		else if( plug == mControlValueX ) 
		{
			MPlug parentPlug = plug.parent();
			int index = parentPlug.logicalIndex();
			this->setValue(index, 0, handle.asDouble());
			verticesUpdated();
		}
		else if( plug == mControlValueY ) 
		{
			MPlug parentPlug = plug.parent();
			int index = parentPlug.logicalIndex();
			this->setValue(index, 1, handle.asDouble());
			verticesUpdated();
		}
		else if( plug == mControlValueZ ) 
		{
			MPlug parentPlug = plug.parent();
			int index = parentPlug.logicalIndex();
			this->setValue(index, 2, handle.asDouble());
			verticesUpdated();
		}
	}
	else 
	{
		isOk = MPxSurfaceShape::setInternalValue( plug, handle );
	}

	return isOk;
}

MStatus HairShape::shouldSave( const MPlug& plug, bool& result )
{
	MStatus status = MS::kSuccess;

	if( plug == mControlPoints || plug == mControlValueX ||
		plug == mControlValueY || plug == mControlValueZ )
	{
		result = false;
	}
	else
	{
		status = MPxNode::shouldSave( plug, result );
	}

	return status;
}







// Attribute to component (components)
void HairShape::componentToPlugs( 
	MObject & component,
	MSelectionList & list ) const
{
	if ( component.hasFn(MFn::kSingleIndexedComponent) ) 
	{
		MFnSingleIndexedComponent fnVtxComp( component );
    	MObject thisNode = thisMObject();
		MPlug plug( thisNode, mControlPoints );
		// If this node is connected to a tweak node, reset the
		// plug to point at the tweak node.
		//
		convertToTweakNodePlug(plug);

		int len = fnVtxComp.elementCount();

		for ( int i = 0; i < len; i++ )
		{
			plug.selectAncestorLogicalIndex(fnVtxComp.element(i),
											plug.attribute());
			list.add(plug);
		}
	}
}
MPxSurfaceShape::MatchResult HairShape::matchComponent( 
	const MSelectionList& item,
	const MAttributeSpecArray& spec,
	MSelectionList& list )
{
	MPxSurfaceShape::MatchResult result = MPxSurfaceShape::kMatchOk;
	return result;
	/*/
	MPxSurfaceShape::MatchResult result = MPxSurfaceShape::kMatchOk;
	MAttributeSpec attrSpec = spec[0];
	int dim = attrSpec.dimensions();

	// Look for attributes specifications of the form :
	//     vtx[ index ]
	//     vtx[ lower:upper ]
	//
	if ( (1 == spec.length()) && (dim > 0) && (attrSpec.name() == "vtx") ) 
	{
		HairProcessor* geometry = getCachedGeometry();
		int numVertices = geometry->getCVCount();
		MAttributeIndex attrIndex = attrSpec[0];

		int upper = 0;
		int lower = 0;
		if ( attrIndex.hasLowerBound() ) 
		{
			attrIndex.getLower( lower );
		}
		if ( attrIndex.hasUpperBound() ) 
		{
			attrIndex.getUpper( upper );
		}

		// Check the attribute index range is valid
		//
		if ( (lower > upper) || (upper >= numVertices) ) 
		{
			result = MPxSurfaceShape::kMatchInvalidAttributeRange;	
		}
		else 
		{
			MDagPath path;
			item.getDagPath( 0, path );
			MFnSingleIndexedComponent fnVtxComp;
			MObject vtxComp = fnVtxComp.create( MFn::kMeshVertComponent );

			for ( int i=lower; i<=upper; i++ )
			{
				fnVtxComp.addElement( i );
			}
			list.add( path, vtxComp );
		}
	}
	else 
	{
		// Pass this to the parent class
		return MPxSurfaceShape::matchComponent( item, spec, list );
	}
	return result;
	/*/
}
bool HairShape::match(	
	const MSelectionMask & mask,
	const MObjectArray& componentList ) const
{
	bool result = false;

	if( componentList.length() == 0 ) 
	{
		result = mask.intersects( MSelectionMask::kSelectMeshes );
	}
	else 
	{
		for ( int i=0; i<(int)componentList.length(); i++ ) 
		{
			if ( (componentList[i].apiType() == MFn::kMeshVertComponent) &&
				 (mask.intersects(MSelectionMask::kSelectMeshVerts))) 
			{
				result = true;
				break;
			}
		}
	}
	return result;
}



void HairShape::transformUsing( const MMatrix & mat,
										const MObjectArray & componentList,
										MPxSurfaceShape::MVertexCachingMode cachingMode,
										MPointArray* pointCache)
{
    MStatus stat;
//	DrawCache* geomPtr = getCachedGeometry();

	bool savePoints  = (cachingMode == MPxSurfaceShape::kSavePoints);
	unsigned int i=0,j=0;
	unsigned int len = componentList.length();
	if (cachingMode == MPxSurfaceShape::kRestorePoints) 
	{
		unsigned int cacheLen = pointCache->length();
		if (len > 0) 
		{
			// traverse the component list
			//
			for ( i = 0; i < len && j < cacheLen; i++ )
			{
				MObject comp = componentList[i];
				MFnSingleIndexedComponent fnComp( comp );
				int elemCount = fnComp.elementCount();
				for ( int idx=0; idx<elemCount && j < cacheLen; idx++, ++j ) 
				{
					int elemIndex = fnComp.element( idx );
					MPoint pt = (*pointCache)[j];
					this->setValue(elemIndex, pt);
				}
			}
		} 
	}

	if (cachingMode == MPxSurfaceShape::kSavePoints || 
		cachingMode == MPxSurfaceShape::kUpdatePoints ) 
	{
		// Traverse the componentList 
		//
		for ( i=0; i<len; i++ )
		{
			MObject comp = componentList[i];
			MFnSingleIndexedComponent fnComp( comp );
			int elemCount = fnComp.elementCount();

			if (savePoints && 0 == i) 
			{
				pointCache->setSizeIncrement(elemCount);
			}
			for ( int idx=0; idx<elemCount; idx++ )
			{
				int elemIndex = fnComp.element( idx );
				MPoint pt = this->getValue(elemIndex);
				if (savePoints) 
					pointCache->append(pt);

				pt = pt*mat;
				this->setValue(elemIndex, pt);
			}
		}
	}

	MDataBlock datablock = forceCache();

	MDataHandle dHandle = datablock.outputValue( mControlPoints, &stat );
	MPlug pCPs(thisMObject(),mControlPoints);
	pCPs.setValue(dHandle);
}

bool HairShape::deleteComponents( 
	const MObjectArray& componentList,
	MDoubleArray& undoInfo )
{
	return false;
}
bool HairShape::undeleteComponents( 
	const MObjectArray& componentList,
	MDoubleArray& undoInfo )
{
	return false;
}

MPxGeometryIterator* HairShape::geometryIteratorSetup( 
	MObjectArray& componentList, 
	MObject& components,
	bool forReadOnly )
{
	CVIterator* result = NULL;
	/*/
	if ( components.isNull() ) 
	{
		result = new CVIterator( getCachedGeometry(), componentList );
	}
	else 
	{
		result = new CVIterator( getCachedGeometry(), components );
	}
	/*/
	return result;
}
bool HairShape::acceptsGeometryIterator( bool  writeable )
{
	return true;
}
bool HairShape::acceptsGeometryIterator( 
	MObject&, 
	bool writeable,
	bool forReadOnly )
{
	return true;
}


void HairShape::verticesUpdated()
//
// Description
//
//    Helper function to tell maya that this shape's
//    vertices have updated and that the bbox needs
//    to be recalculated and the shape redrawn.
//
{
	childChanged( MPxSurfaceShape::kBoundingBoxChanged );
	childChanged( MPxSurfaceShape::kObjectChanged );
}


// cache
MPoint HairShape::getValue(int index)
{
	/*/

//displayString("getValue %d", index);
	{
		MDataBlock datablock = forceCache();
		MDataHandle handle = datablock.outputValue( io_cacheCvVerticies );
		MFnVectorArrayData vectorArrayData( handle.data());

		if( index>=(int)vectorArrayData.length())
			return MPoint(0, 0, 0, 1);

		MVector pt = vectorArrayData[index];
		Math::Vec3f v((float)pt.x, (float)pt.y, (float)pt.z);
		Math::Matrix4f m = hairprocessor.getCVbasis(index);
		v = m*v;
		return MPoint(v.x, v.y, v.z);
	}

//	if( index>=(int)cache.size())
//		return MPoint(0, 0, 0, 1);
//	return cache[index];
	/*/
	return MPoint(0, 0, 0, 1);
}
void HairShape::setValue(int index, MPoint& pt)
{
	/*/
//displayString("setValue %d %f, %f, %f", index, pt.x, pt.y, pt.z);
	{
		MDataBlock datablock = forceCache();
		MDataHandle handle = datablock.outputValue( io_cacheCvVerticies );
		MFnVectorArrayData vectorArrayData( handle.data());

		if( index>=(int)vectorArrayData.length())
			return;
		pt.homogenize();
		Math::Vec3f v((float)pt.x, (float)pt.y, (float)pt.z);
		Math::Matrix4f m = hairprocessor.getCVbasis(index);
		m.invert();

		v = m*v;
		vectorArrayData[index] = MVector(v.x, v.y, v.z);
	}

//	if( index>=(int)cache.size())
//		return;
//	cache[index] = pt;
	/*/
}
void HairShape::setValue(int index, int el, double val)
{
	/*/
//displayString("setValue %d %d %f", index, el, val);
	{
		MDataBlock datablock = forceCache();
		MDataHandle handle = datablock.outputValue( io_cacheCvVerticies );
		MFnVectorArrayData vectorArrayData( handle.data());

		if( index>=(int)vectorArrayData.length())
			return;
		MPoint pt = vectorArrayData[index];
		Math::Vec3f v((float)pt.x, (float)pt.y, (float)pt.z);
		Math::Matrix4f m = hairprocessor.getCVbasis(index);
		v = m*v;
		v[el] = (float)val;
		m.invert();
		v = m*v;
		vectorArrayData[index] = MVector(v.x, v.y, v.z);
	}
//	if( index>=(int)cache.size())
//		return;
//	cache[index][el] = val;
	/*/
}

