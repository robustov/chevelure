#pragma once

#include <maya/MTypeId.h> 
#include <maya/MPxComponentShape.h> 
#include "HairGeometryIndices.h"
#include "HairGeometry.h"
#include "HairCVPositions.h"
#include "HairCV.h"
#include "HairCvBasesData.h"
#include "HairCVData.h"

class MPxGeometryIterator;

struct HairCVandGeometry
{
	HairCV* cv;
	HairCvBasesData* bases;
};

class HairControlsShape : public MPxComponentShape
{
public:
//	static MObject i_bUseBake;				//!< ������������ ����
//	static MObject i_time;					//!< ����� (��� �����)
	static MObject i_message;				//!< ��� ������ ��������
	static MObject i_importfrom;			//!< ������� � HairSystem ��� Shave ��� ������ �� ���� �������������
//	static MObject i_keepLength;			//!< ���� ���������� �����
	static MObject i_CVcount;				// ����� ���������
	static MObject i_HairGeometryIndices;	//!< ���������� ���������� ���������
	static MObject i_HairGeometry;			//!< ��������� ���������
	static MObject o_HairCvBases;			//!< ������ ��� CH
	static MObject o_HairCVPositions;		//!< ��������� ����������� �����
	static MObject o_Guides;				//!< ����� ����������� �����
	static MObject o_HairCV;				//!< ����������� ������

	static MObject o_HairCVPositionsOut;	//!< ��������� ����������� ����� (�����������)
	static MObject o_HairCVout;				//!< ����������� ������ (�����������)

	// ��� �������� � maya hair
	static MObject o_CVTransform;		//!< ��������� ��� ����������� ������
		static MObject o_CVtranslate;
		static MObject o_CVrotate;

	// scale
	static MObject i_scale[3];
	static MObject i_PaintResXY[2];

public:
	HairControlsShape();
	virtual ~HairControlsShape(); 

public:
	static int current_file_version;
	bool saveFrame(const char* filename);
	bool loadFrame(const char* filename);

	// ������ �� �������� ���������
	bool importCV(
		std::vector< std::vector<Math::Vec3f> >& shavepoints
		);
	// �������
	bool exportCV(
		std::vector< std::vector<Math::Vec3f> >& shavepoints,
		int cvinhair
		);
	// �������/�������� �������� � ��������� �� � ���� ����
	bool connectMayaHair(
		MObject hairSystem, 
		MObject follicleroottransform,
		MObject outputCurvesTransform);

	// �������� � ������
	bool meshOp(
		const char* op, 
		MMatrix shapemat, 
		MObject meshobj, 
		MMatrix meshmat,
		MObject meshobj2, 
		MMatrix _meshmat2, 
		const char* texturename
		);

///////////////////////////////////////////////
public:
	MStatus computeHairCVPositions( const MPlug& plug, MDataBlock& dataBlock );
//	MStatus computeHairCV( const MPlug& plug, MDataBlock& dataBlock );
	MStatus ComputeCVTransform(const MPlug& plug, MDataBlock& data);
	MStatus ComputeCvBases(const MPlug& plug, MDataBlock& data);
	MStatus computeHairCVPositionsOut( const MPlug& plug, MDataBlock& data);
	MStatus computeHairCVout( const MPlug& plug, MDataBlock& data);

public:
	HairGeometryIndices*			LoadHairGeometryIndices(MDataBlock& data);
	HairGeometry*					LoadHairGeometry(MDataBlock& data);
	HairCVPositions*				LoadHairCVPositions(MDataBlock& data);
	HairCVPositions*				LoadHairCVPositionsOut(MDataBlock& data);
	HairCV*							LoadHairCV(MDataBlock& data);
	HairCvBasesData*				LoadHairCvBases(MDataBlock& data);

	MDataBlock forceCache( MDGContext& ctx=MDGContext::fsNormal )
	{
		MDataBlock data = MPxComponentShape::forceCache(ctx);
		return data;
	}

	static MStatus CreateAttrNTextureCache( 
		std::string longname, std::string shortname, 
		MObject* i_HairAttr, double vmin, double vmax, double def, bool bStorableAttributes, MObject i_PaintResXY[2]);
	static void CreateTextureCacheAttr(
		std::string longName, std::string shortName, MObject& src, MObject& attr, bool bStorableAttributes, MObject i_PaintResXY[2]);
	MStatus ComputeTextureCache3(const MPlug& plug, MObject attr[3], MObject i_PaintResXY[2], MDataBlock& data);

// ������
public:
	HairCVandGeometry* getPointsInOS();
	HairCVandGeometry haircvandgeometry;
	Math::Vec3f* vertex(int index, int* hairIndex=NULL);
	Math::Vec3f vertexOS(int index);

///////////////////////////////////////////////
// MPxNode  
public:
	virtual MStatus compute( 
		const MPlug& plug,
		MDataBlock& dataBlock );

	MStatus	setDependentsDirty( const MPlug& plug, MPlugArray & affected);

	///
	virtual bool getInternalValue( 
		const MPlug& plug,
		MDataHandle& dataHandle);
	///
    virtual bool setInternalValue( 
		const MPlug& plug,
		const MDataHandle& dataHandle);
	#if MAYA_API_VERSION >= 200800
	///
    virtual int internalArrayCount( 
		const MPlug& plug,
		const MDGContext& ctx) const;
	#endif

///////////////////////////////////////////////
// MPxSurfaceShape
public:
	/// 
	virtual bool		    isBounded() const;
	///
	virtual MBoundingBox    boundingBox() const; 

	virtual void componentToPlugs( 
		MObject& component,
		MSelectionList& list) const;
	virtual MatchResult matchComponent( 
		const MSelectionList& item,
		const MAttributeSpecArray& spec,
		MSelectionList& list 
		);
	virtual bool match(	
		const MSelectionMask & mask,
		const MObjectArray& componentList 
		) const;

	// Move tool support for components
	virtual void transformUsing( 
		const MMatrix& mat,
		const MObjectArray& componentList );
	virtual void transformUsing( 
		const MMatrix& mat,
		const MObjectArray& componentList,
		MVertexCachingMode cachingMode,
		MPointArray* pointCache);
	virtual void tweakUsing( 
		const MMatrix& mat,
		const MObjectArray& componentList,
		MVertexCachingMode cachingMode,
		MPointArray* pointCache,
		MArrayDataHandle& handle );

	// Associates a user defined iterator with the shape (components)
	//
	virtual MPxGeometryIterator*	geometryIteratorSetup( MObjectArray&, MObject&, bool forReadOnly = false );
	virtual bool                    acceptsGeometryIterator( bool  writeable=true );
	virtual bool                    acceptsGeometryIterator( MObject&, bool writeable=true, bool forReadOnly = false);

///////////////////////////////////////////////
// MPxComponentShape
public:



///////////////////////////////////////////////
public:
	static  void *          creator();
	static  MStatus         initialize();

	static MTypeId id;
	static const MString typeName;
};

inline Math::Vec3f* HairControlsShape::vertex(int index, int* hairIndex)
{
	MStatus stat;
	MDataBlock data = forceCache();
	MDataHandle dHandle = data.outputValue( o_HairCV, &stat );
	MPxData* pxdata = dHandle.asPluginData();

	if( !pxdata) return NULL;
	HairCVData* hc = (HairCVData*)pxdata;
	return hc->cv.getSingleIndexedVertex(index, hairIndex);
}

inline Math::Vec3f HairControlsShape::vertexOS(int index)
{
	MStatus stat;
	MDataBlock data = forceCache();
	MDataHandle dHandle = data.outputValue( o_HairCV, &stat );

	MPxData* pxdata = dHandle.asPluginData();
	if( !pxdata) return Math::Vec3f(0);
	HairCVData* hc = (HairCVData*)pxdata;

	MDataHandle inputData1 = data.inputValue(o_HairCvBases, &stat);
	MPxData* pxdata1 = inputData1.asPluginData();
	if( !pxdata1) return Math::Vec3f(0);
	HairCvBasesData* bases = (HairCvBasesData*)pxdata1;

	int hairIndex;
	Math::Vec3f* v = hc->cv.getSingleIndexedVertex(index, &hairIndex);
	if( !v) return Math::Vec3f(0);

	Math::Vec3f os = bases->bases[hairIndex]*(*v);
	return os;
}
