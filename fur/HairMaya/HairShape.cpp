#include "HairShape.h"
#include "CVIterator.h"
#include "MeshSurfaceSynk.h"
#include "Math/interpolation.h"
#include "Math/Subdiv.h"
#include "Util/EvalTime.h"
#include "textureCacheData.h"
#include <maya/MFileIO.h>
#include "Util/FileStream.h"
#include "HairPersonage.h"
#include "HairModifierData.h"
#include "Util/misc_create_directory.h"
#include "Util/misc_switch_string.h"
#include "HairOcsGenerator.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include "maya/MFnIntArrayData.h"
#include "MathNMaya/MathNMaya.h"
#include "HairRootPositionsData.h"
#include <maya/MRenderUtil.h>
#include <maya/MFloatMatrix.h>
#include <maya/MProgressWindow.h>
#include <maya/MFnNurbsCurveData.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MEulerRotation.h>
#include <maya/MFnMeshData.h>
#include <maya/MDGModifier.h>
#include <maya/MFnPointArrayData.h>
//#include "deformers//HairCombDeformer.h"
#include "HairDeformer.h"
#include <time.h>

#include <maya/MGlobal.h>
#pragma warning(disable : 4101)

#include "HairArtistShape2.h"
#include "HairDeformerData.h"
#include "HairRenderPassData.h"
#include "Math/RayTriIntersection.h"

// Output
MObject HairShape::o_buildMessage;
MObject HairShape::i_HairRootPositionFile;
MObject HairShape::i_ocsFrameFile;

// Geometry
MObject HairShape::i_geometry;				//!< ������� ���������
MObject HairShape::i_HairGeometryIndices;	//!< ���������� ���������� ���������
MObject HairShape::i_HairGeometry;			//!< ��������� ���������
MObject HairShape::i_HairCVPositions;		//!< ��������� ����������� �����
MObject HairShape::i_HairCV;				//!< ����������� ������
MObject HairShape::i_HairClumpPositions;	//!< ��������� ������ �������
//MObject HairShape::i_HairRootPositions;		//!< ��������� ����� (��������� � ����, ��� ���� �������� � ���� i_HairGeometryIndices � i_hairClumpPlacement)
MObject HairShape::i_HairTimeStamp;			//!< ����� ��������� ������ ��� ������� RootPositions
MObject HairShape::i_hairDrawPlacement;		// ��������� ����� � ������ i_maxHairDrawable � i_HairDensityArray
MObject HairShape::i_HairAlive;
MObject HairShape::i_HairRenderPasses;

// Hair painting & caching
MObject HairShape::i_PaintAttrResX;
MObject HairShape::i_PaintAttrResY;

// �����������
MObject HairShape::i_preSubdivSurface;
MObject HairShape::i_uvSetName;				//!< ��� ������������� uvSet�

// ��������� �����
MObject HairShape::i_generateType;
MObject HairShape::i_HairDensity, HairShape::i_HairDensityUvSet;
	// �������������� ��� ���������
	MObject HairShape::i_count;
	MObject HairShape::i_Seed;
	// ���������� ��� ���������
	MObject HairShape::i_regularDim;
	MObject HairShape::i_regularOffset;
	MObject HairShape::i_regularShear;
	MObject HairShape::i_jitter;
//MObject HairShape::i_HairDensityVersion;

// Width
MObject HairShape::i_HairRenderWidth;
//MObject HairShape::i_HairWidthRamp[5];
MObject HairShape::i_HairBaseWidth[4];
//	MObject HairShape::i_HairBaseWidthCache;
MObject HairShape::i_HairTipWidth[4];
MObject HairShape::i_mulWidthByLength;
//	MObject HairShape::i_HairTipWidthCache;

// Hair controllers
MObject HairShape::i_CVcountU;
MObject HairShape::i_CVcountV;
//MObject HairShape::i_CVsegmCount;
MObject HairShape::i_CVtailU; 
MObject HairShape::i_CVtailV;
MObject HairShape::o_CVCurveCreate;
MObject HairShape::o_CVTransform;		//!< ��������� ��� ����������� ������
	MObject HairShape::o_CVtranslate;
	MObject HairShape::o_CVrotate;
	MObject HairShape::o_CVscale;
	MObject HairShape::o_CVshear;
MObject HairShape::i_CVCurves;			//!< ����������� ������
MObject HairShape::i_CVDynArrays;		//!< ����������� ������ ����� ��������
MObject HairShape::i_CVDisplay;			//!< �������� ����������� ������
MObject HairShape::i_useDynamic;		//!< ������������ ��������

// CLUMPing
MObject HairShape::i_bCLUMP;
MObject HairShape::i_CLUMPradius;
MObject HairShape::i_CLUMPalgorithm;
MObject HairShape::i_CLUMPcount;
MObject HairShape::i_CLUMPblending;
MObject HairShape::i_CLUMPdensity;
MObject HairShape::i_CLUMPdensityVersion;
MObject HairShape::i_HairBaseClump[4];
MObject HairShape::i_HairTipClump[4];
MObject HairShape::i_clumpPositionFile;

// Deforming
//MObject HairShape::i_time;				//!< �����, ������������ ��� ���������
MObject HairShape::i_orthogonalize;
MObject HairShape::i_SegmCount;
MObject HairShape::i_HairLength[4];
MObject HairShape::i_HairInclination[4];
MObject HairShape::i_HairPolar[4];
MObject HairShape::i_HairBaseCurl[4];
MObject HairShape::i_HairTipCurl[4];
MObject HairShape::i_HairScraggle[4];
MObject HairShape::i_HairScraggleFreq[4];
MObject HairShape::i_HairTwist[4];
MObject HairShape::i_HairDisplace[4];
MObject HairShape::i_HairTwistFromPolar[4];
MObject HairShape::i_blendCVfactor[4];
MObject HairShape::i_blendDeformfactor[4];
//MObject HairShape::i_Deformer[2];
MObject HairShape::i_HairModifiers;
MObject HairShape::i_HairDeformers;

// Hair display
MObject HairShape::i_Colorize;
MObject HairShape::i_colorUvSetName;
MObject HairShape::i_maxHairDrawable;
MObject HairShape::i_drawMode;
MObject HairShape::i_WorkingFaces;

//MObject HairShape::i_drawBox;
//MObject HairShape::i_drawClumps;
//MObject HairShape::i_drawCV;
MObject HairShape::i_BaseColor;
	MObject HairShape::i_BaseColorCache;
MObject HairShape::i_TipColor;
	MObject HairShape::i_TipColorCache;

// Rendering
MObject HairShape::i_interpolation;
MObject HairShape::i_diceHair;
MObject HairShape::i_sigmaHiding;
MObject HairShape::i_renderDoubleSide;
MObject HairShape::i_renderNormalSource;
MObject HairShape::i_reverseOrientation;
MObject HairShape::i_renderRayTraceEnable, HairShape::i_renderRayCamera, HairShape::i_renderRayTrace, HairShape::i_renderRayPhoton;
MObject HairShape::i_RenderResX;
MObject HairShape::i_RenderResY;
//MObject HairShape::i_MotionBlur;
MObject HairShape::i_MotionSamples;
MObject HairShape::i_bUseWorkingFaces;
MObject HairShape::i_splitGroups;
MObject HairShape::i_bRenderBoxes;
MObject HairShape::i_bAccurateBoxes;
MObject HairShape::i_FreezeFile;
MObject HairShape::i_dumpInformation;
MObject HairShape::i_pruningScaleFactor;


// ��� ��������
MObject HairShape::i_snShape;
MObject HairShape::i_ocellarisShape;
MObject HairShape::i_instanceweights;

// ������
MObject HairShape::o_positions;			// ������� �����
MObject HairShape::o_normals, HairShape::o_tangentUs, HairShape::o_tangentVs;
MObject HairShape::o_hairVector;

// �����
//MObject HairShape::o_outputMesh;
//MObject i_test;


/*/
MStringArray HairShape::ConnectCVCurves(MObject obj, int segmCount)
{
	MStringArray result;
	MStatus stat;
	MFnDependencyNode dn(obj);
	MFnDagNode dag(obj);
	MPxNode* pxnode = dn.userNode(&stat);
	HairShape* pHairShape = (HairShape*)pxnode;

	MDataBlock data = pHairShape->forceCache();

//	pHairShape
	HairGeometryIndices* geometryIndices = pHairShape->LoadHairGeometryIndices(data);
	if( !geometryIndices) return result;
	HairGeometry* geometry = pHairShape->LoadHairGeometry(data);
	if( !geometry) return result;
	HairCVPositions* hairCVPositions = pHairShape->LoadHairCVPositions(data);
	if( !hairCVPositions) return result;
	HairSystem hs;
	pHairShape->LoadHairSystem(data, hs);

	HairClumpPositions clumpPositions;
	HairRootPositions rootPositions;
	HairCVPositions _cvposition;
	HairCV cvs;

displayStringD("ConnectCVCurves");

	MeshSurfaceSynk meshsurface;
	meshsurface.init(geometryIndices, geometry, &clumpPositions, &rootPositions, &_cvposition, &cvs, &hs);
	if( !HairShape::LoadMeshSurface(meshsurface, data, obj))
		return result;

	int count = hairCVPositions->getCount();

//	if( !count) return result;

	char cmd[1024];

	//!< �������� ��� ����������� ������
	if( segmCount)
	{
		int vcount = segmCount+1;
		MPointArray controlVertices(vcount);
		MDoubleArray knotSequences(vcount);
		for(int k=0; k<vcount; k++)
		{
			knotSequences[k] = k;
			double x = k/(vcount-1.0);
			controlVertices[k] = MPoint(x, 0, 0);
		}

		MFnNurbsCurveData dataCreator;
		MObject newOutputData = dataCreator.create(&stat);
		MFnNurbsCurve curve;
		curve.create(controlVertices, knotSequences, 1, MFnNurbsCurve::kOpen, false, false, newOutputData);

		MDataHandle outputData = data.outputValue( o_CVCurveCreate, &stat );
		outputData.set(newOutputData);
		outputData.setClean();
	}

	// ������� ��� ������
	{
		MPlug plug(obj, o_CVTransform);
		for( unsigned i=0; i<plug.numElements(); i++)
		{
			MPlug pel = plug[i];
			MPlug pl = pel.child(o_CVtranslate);

			MPlugArray ar;
			pl.connectedTo(ar, false, true);
			if(ar.length()!=1) continue;

			MString del = MFnDependencyNode( ar[0].node()).name();
			sprintf(cmd, "delete %s", del.asChar());
			MGlobal::executeCommand(cmd);
		}
	}

	// hairSystem
	MString hairSystem;
	{
		MPlug plug(obj, i_CVDynArrays);
		MPlug plug0 = plug[0];
		MPlugArray ar;
		plug0.connectedTo(ar, true, false);
		if(ar.length()!=1)
		{
			sprintf(cmd, "createNode hairSystem");
			MGlobal::executeCommand(cmd, hairSystem);
			// time1 -> hairSystem
			sprintf(cmd, "connectAttr -f time1.outTime %s.currentTime", hairSystem.asChar());
			MGlobal::executeCommand(cmd);
			// hairSystem -> i_CVDynArrays
			sprintf(cmd, "connectAttr -f %s.outputHair[0] %s.i_CVDynArrays[0]", 
				hairSystem.asChar(), dn.name().asChar());
			MGlobal::executeCommand(cmd);
		}
		else
		{
			hairSystem = MFnDependencyNode( ar[0].node()).name();
		}
	}

	meshsurface.InitDeformers();
	meshsurface.initParamsUVsetIndices();

	HairProcessor processor(segmCount, meshsurface, hs.snshapes.size(), false, hs.orthogonalize, false);
	for(int i=0; i<count; i++)
	{
		Math::Matrix4f matrix;
		hairCVPositions->getBasis(i, geometryIndices, geometry, matrix);
		Math::Vec2f uv = hairCVPositions->getUV(i, geometryIndices, geometry);

//		displayStringD("OrthoBasis %d {%f, %f}", i, uv.x, uv.y);
//		displayMatrix(matrix);

		MFnTransform dntransform;
		MObject transform = dntransform.create(dag.parent(0));

		// 
		MString curve;
		{
			HAIRPARAMS hairparams;
			HAIRVERTS hair;
			processor.ParseHair(hairparams, hairCVPositions->cvs[i].faceindex, hairCVPositions->cvs[i].b, true);
			processor.BuildStandAloneHair(hairparams, hair, true);

			// ������ �����!
//			float l = meshsurface.getLength(hairparams.u, hairparams.v);
			float l = meshsurface.length.getValueSource(hairparams.u, hairparams.v);
			matrix[0] *= l;
			matrix[1] *= l;
			matrix[2] *= l;
			Math::Matrix4f matrix_inv = matrix.inverted();

			int vcount = hair.vc();
			MPointArray controlVertices(vcount);
			MDoubleArray knotSequences(vcount);
			for(int k=0; k<vcount; k++)
			{
				// ��������� � TS
				Math::Vec3f pt = hair.cvint[k];
//				pt = Math::Vec3f( k/(float)(vcount-1), 0, 0);
				pt = matrix_inv*pt;
				knotSequences[k] = k;
				controlVertices[k] = MPoint(pt.x, pt.y, pt.z);
			}

			MFnNurbsCurve _curve;
			MObject dncurve = _curve.create(controlVertices, knotSequences, 1, MFnNurbsCurve::kOpen, false, false, transform);
			curve = _curve.name();
		}


		// o_CVTransform
		{
			sprintf(cmd, "connectAttr -f %s.o_CVTransform[%d].o_CVtranslate %s.t", 
				dn.name().asChar(), i, dntransform.name().asChar());
			MGlobal::executeCommand(cmd);

			sprintf(cmd, "connectAttr -f %s.o_CVTransform[%d].o_CVrotate %s.r", 
				dn.name().asChar(), i, dntransform.name().asChar());
			MGlobal::executeCommand(cmd);

			sprintf(cmd, "connectAttr -f %s.o_CVTransform[%d].o_CVscale %s.s", 
				dn.name().asChar(), i, dntransform.name().asChar());
			MGlobal::executeCommand(cmd);

			sprintf(cmd, "connectAttr -f %s.o_CVTransform[%d].o_CVshear %s.sh", 
				dn.name().asChar(), i, dntransform.name().asChar());
			MGlobal::executeCommand(cmd);
		}
		

		sprintf(cmd, "connectAttr -f %s.CvDisplay %s.visibility", 
			dn.name().asChar(), dntransform.name().asChar());
		MGlobal::executeCommand(cmd);

		// to i_CVCurves
		sprintf(cmd, "connectAttr -f %s.worldSpace[0] %s.i_CVCurves[%d]", 
			curve.asChar(), dn.name().asChar(), i);
		MGlobal::executeCommand(cmd);

		// follicle
		MString follicle;
		sprintf(cmd, "createNode follicle -p %s", dntransform.name().asChar());
		MGlobal::executeCommand(cmd, follicle);

		sprintf(cmd, "setAttr %s.restPose 1", follicle.asChar());
		MGlobal::executeCommand(cmd);

		// curve -> follicle
		sprintf(cmd, "connectAttr -f %s.worldSpace[0] %s.startPosition", 
			curve.asChar(), follicle.asChar());
		MGlobal::executeCommand(cmd);

		// follicle -> hairSystem
		sprintf(cmd, "connectAttr -f %s.outHair %s.inputHair[%d]", 
			follicle.asChar(), hairSystem.asChar(), i);
		MGlobal::executeCommand(cmd);

		// hairSystem -> i_CVDynArrays
		sprintf(cmd, "connectAttr -f %s.outputHair[%d] %s.i_CVDynArrays[%d]", 
			hairSystem.asChar(), i, dn.name().asChar(), i);
		MGlobal::executeCommand(cmd);

		result.append(follicle);
	}
	return result;
}
/*/


HairShape::HairShape()
{
	bRebuildClumpData = bRebuildRootData = bRebuildDrawData = bRebuildCVPositions = false;
	bValidBox = false;
}

void HairShape::postConstructor()
{
	MStatus stat;
	bValidBox = false;
	setRenderable( true );

	// for RFM
	{
		MObject obj = thisMObject();
		MFnDependencyNode dn(obj);
		MFnStringData stringData;
		MFnTypedAttribute typedAttr;
		MString hairRfmProcedure;
		#ifndef SAS
		hairRfmProcedure = "HairRFMprocedure";
		#else
		hairRfmProcedure = "chevelureRFMprocedure";
		#endif
		MObject attr = typedAttr.create( "rman__torattr___preShapeScript", "", MFnData::kString, stringData.create(hairRfmProcedure+" export", &stat), &stat );
		typedAttr.setConnectable(true);
		typedAttr.setStorable(true);
		typedAttr.setWritable(true);
		typedAttr.setReadable(true);
		typedAttr.setHidden(false);
		typedAttr.setKeyable(true);
		dn.addAttribute(attr);

		{
			MFnTypedAttribute cacheTypedAttr;
			MObject cacheScriptAttr = cacheTypedAttr.create( "rman__torattr___cacheShapeScript", "", MFnData::kString, stringData.create(hairRfmProcedure+" cache", &stat), &stat );
		
			cacheTypedAttr.setConnectable(true);
			cacheTypedAttr.setStorable(true);
			cacheTypedAttr.setWritable(true);
			cacheTypedAttr.setReadable(true);
			cacheTypedAttr.setHidden(false);
			cacheTypedAttr.setKeyable(true);
			dn.addAttribute(cacheScriptAttr);
		}

		{
			MFnNumericAttribute numAttr;
			// 1 = every frame
			MObject attr = numAttr.create( "rman__torattr___evaluationFrequency", "", MFnNumericData::kLong, 1, &stat );
			numAttr.setConnectable(true);
			numAttr.setStorable(true);
			numAttr.setWritable(true);
			numAttr.setReadable(true);
			numAttr.setHidden(false);
			numAttr.setKeyable(true);
			dn.addAttribute(attr);
		}
	}

//	lastCVcountU = lastCVcountV = lastCVsegmCount = 0;
}
HairShape::~HairShape() 
{
	displayStringD("delete HairShape");
}
void* HairShape::creator()
{
	return new HairShape();
}


MPxData* HairShape::LoadAttr(MDGContext& context, MObject attr, MObject& outval)
{
	MObject obj = thisMObject();
	MPlug(obj, attr).getValue(outval, context);
	MPxData* pxdata = MFnPluginData(outval).data();
	return pxdata;
}

HairGeometryIndices* HairShape::LoadHairGeometryIndices(MDGContext& context, MObject& outval)
{
	MPxData* pxdata = LoadAttr(context, i_HairGeometryIndices, outval);
	if( !pxdata) return NULL;
	HairGeometryIndicesData* hgh = (HairGeometryIndicesData*)pxdata;
//	if( !hgh->hgh.isValid())
//		ComputeHairGeometryIndices( MPlug(thisMObject(), i_HairGeometryIndices), data);
	return &hgh->hgh;
}
HairGeometry* HairShape::LoadHairGeometry(MDGContext& context, MObject& outval)
{
	MPxData* pxdata = LoadAttr(context, i_HairGeometry, outval);
	if( !pxdata) return NULL;
	HairGeometryData* gd = (HairGeometryData*)pxdata;
	return &gd->geometry;
}
HairCVPositions* HairShape::LoadHairCVPositions(MDGContext& context, MObject& outval)
{
	/*/
	if( bRebuildCVPositions)
	{
		MDataBlock data = this->forceCache();
		ComputeHairCVPositions(MPlug(thisMObject(), i_HairCVPositions), data);
	}
	/*/

	MPxData* pxdata = LoadAttr(context, i_HairCVPositions, outval);
	if( !pxdata)
	{
		static HairCVPositions shcp;
		return &shcp;
	}
	HairCVPositionsData* hc = (HairCVPositionsData*)pxdata;
	return &hc->cvpos;
}
HairCV* HairShape::LoadHairCV(MDGContext& context, MObject& outval)
{
	MPxData* pxdata = LoadAttr(context, i_HairCV, outval);
	if( !pxdata)
	{
		static HairCV shc;
		return &shc;
	}
	HairCVData* hc = (HairCVData*)pxdata;
	return &hc->cv;
}
HairClumpPositions* HairShape::LoadHairClumpPositions(MDGContext& context, MObject& outval)
{
	if( bRebuildClumpData)
	{
		MDataBlock data = this->forceCache();
		ComputeHairClumpPositions(MPlug(thisMObject(), i_HairClumpPositions), data);
	}

	MPxData* pxdata = LoadAttr(context, i_HairClumpPositions, outval);
	if( !pxdata) return NULL;
	HairClumpPositionsData* hc = (HairClumpPositionsData*)pxdata;
	return &hc->hc;
}
void HairShape::LoadHairSystem(MDGContext& context, HairSystem& hairsystem, const char* passname)
{
	MStatus stat;

	MObject obj = thisMObject();
	MDataBlock data = this->forceCache();

	int orthogonalize;
	MPlug(obj, i_orthogonalize).getValue(orthogonalize, context);
	hairsystem.orthogonalize = (enOrthogonalizeType)orthogonalize;
	MPlug(obj, i_SegmCount).getValue(hairsystem.SegmCount, context);

	MPlug(obj, i_CVtailU).getValue(hairsystem.CVtailU, context);
	MPlug(obj, i_CVtailV).getValue(hairsystem.CVtailV, context);

	hairsystem.mulWidthByLength = data.inputValue( i_mulWidthByLength).asBool();

	hairsystem.ocsfiles.clear();
	hairsystem.snshapes.clear();
	hairsystem.snshapes_weight.clear();

	// snshapes
	MArrayDataHandle arh = data.inputArrayValue(i_snShape, &stat);
	if( stat)
	{
		for( unsigned i=0; i<arh.elementCount(); i++)
		{
			if( arh.jumpToArrayElement(i))
			{
				int index = arh.elementIndex();

				MPxData* pxdata = arh.inputValue(&stat).asPluginData();
				if( stat && pxdata && pxdata->typeId() == MTypeId(SN_DATAID))
				{
					SnData* sndata = (SnData*)pxdata;
					hairsystem.snshapes[ index ] = sndata->ssp;
					Math::ParamTex2d<unsigned char, Math::EJT_MUL>& w = hairsystem.snshapes_weight[index];
					w = sndata->weight;
				}
			}
		}
	}

	LoadInstanceWeights(i_instanceweights, data, hairsystem.snshapes_weight);

	LoadOcellarisShapes(i_ocellarisShape, data, hairsystem.ocsfiles);

	// LoadRenderPasses
	bool bres = LoadRenderPasses(context, hairsystem.passes, passname);
	// default pass
	if( !bres)
	{
		HairRenderPass pass;
		pass.source = HairRenderPass::RT_DEFAULT;
		pass.passName = "defaultPass";
		pass.shader = "";
		pass.SegmCount = hairsystem.SegmCount;
		MPlug(obj, i_HairRenderWidth).getValue(pass.RenderWidth, context);
		pass.bRenderClumps = false;
		MPlug(obj, i_mulWidthByLength).getValue(pass.mulWidthByLength, context);

		MPlug(obj, i_interpolation).getValue(pass.interpolation, context);
		MPlug(obj, i_diceHair).getValue(pass.diceHair, context);
		MPlug(obj, i_sigmaHiding).getValue(pass.sigmaHiding, context);
		MPlug(obj, i_renderDoubleSide).getValue(pass.renderDoubleSide, context);
		MPlug(obj, i_reverseOrientation).getValue(pass.reverseOrientation, context);

		int renderNormalSource;
		MPlug(obj, i_renderNormalSource).getValue(renderNormalSource, context);
		pass.renderNormalSource = (enRenderNormalSource)renderNormalSource;

		MPlug(obj, i_renderRayTraceEnable).getValue(pass.renderRayTraceEnable, context);
		MPlug(obj, i_renderRayCamera).getValue(pass.renderRayCamera, context);
		MPlug(obj, i_renderRayTrace).getValue(pass.renderRayTrace, context);
		MPlug(obj, i_renderRayPhoton).getValue(pass.renderRayPhoton, context);
		pass.bUseDetails = false;

		MPlug plug;
		bool bSet = false;

		MFnDependencyNode dn(thisMObject());
		{
			plug = dn.findPlug( "primaryVisibility");
			plug.getValue(bSet, context);
			if( bSet) pass.passes.push_back("final");
		}
		{
			plug = dn.findPlug( "castsShadows");
			plug.getValue(bSet, context);
			if( bSet) pass.passes.push_back("shadow");
		}

		for( int x=0; x<(int)pass.passes.size(); x++)
		{
			if( strcmp(passname, "all")==0 || pass.passes[x]==passname)
			{
				hairsystem.passes.push_back(pass);
				break;
			}
		}

	}

	////////////////////////////////////////////
	// ���� shadow - ��������� � ���� diceHair
	/*/
	if( strcmp(passname, "shadow")==0)
	{
		for( int x=0; x<(int)hairsystem.passes.size(); x++)
		{
			HairRenderPass& renderPass = hairsystem.passes[x];
			renderPass.diceHair = false;
		}
	}
	/*/

	////////////////////////////////////////////
	//
	hairsystem.splitGroups = data.inputValue( i_splitGroups).asBool();
	hairsystem.bRenderBoxes = data.inputValue( i_bRenderBoxes).asBool();
	hairsystem.bAccurateBoxes = data.inputValue( i_bAccurateBoxes).asBool();

	hairsystem.workingfaces.clear();
	bool bUseWorkingFaces = data.inputValue( i_bUseWorkingFaces).asBool();

	if( bUseWorkingFaces)
		LoadHairWorkingFaces(data, hairsystem.workingfaces);

	// FREEZE
	{
		MString freezefile;
		freezefile = data.inputValue( i_FreezeFile).asString();
		if( freezefile.length()!=0)
		{
			FILE* f = fopen(freezefile.asChar(), "rb");
			if(f)
			{
				fclose( f);
			}
			else
				freezefile = "";
		}
		hairsystem.freezefile = freezefile.asChar();
	}
	hairsystem.dumpInformation = data.inputValue( i_dumpInformation).asBool();
	hairsystem.pruningScaleFactor = (float)data.inputValue( i_pruningScaleFactor).asDouble();
	
	
}

bool HairShape::LoadRenderPasses(MDGContext& context, std::vector< HairRenderPass>& passes, const char* pass)
{
	// modifiers
	bool bres = false;
	MStatus stat;
	MDataBlock data = this->forceCache();
	passes.clear();
	MArrayDataHandle arh = data.inputArrayValue(i_HairRenderPasses, &stat);
	if( stat)
	{
		passes.reserve(arh.elementCount());
		for( unsigned i=0; i<arh.elementCount(); i++)
		{
			if( arh.jumpToArrayElement(i))
			{
				MPxData* pxdata = arh.inputValue(&stat).asPluginData();
				if( stat && pxdata && pxdata->typeId() == HairRenderPassData::id)
				{
					HairRenderPassData* sndata = (HairRenderPassData*)pxdata;
					bres = true;

					if( !sndata->data.bEnable || sndata->data.bStandAlone)
						continue;

					for( int x=0; x<(int)sndata->data.passes.size(); x++)
					{
						if( strcmp(pass, "all")==0 || sndata->data.passes[x]==pass)
						{
							passes.push_back(sndata->data);
							break;
						}
					}
				}
			}
		}
	}
	return bres;
}
bool HairShape::LoadInstanceWeights(MObject i_attribute, MDataBlock& data, std::map< int, HairSystem::weight_t>& snshapes_weight)
{
	MStatus stat;

	MArrayDataHandle arh = data.inputArrayValue(i_attribute, &stat);
	if( stat)
	{
		MPlug arrayplug(thisMObject(), i_attribute);
		for( unsigned i=0; i<arh.elementCount(); i++)
		{
			if( arh.jumpToArrayElement(i))
			{
				int index = arh.elementIndex();

				MDataHandle dh = arh.inputValue();
				double skip = dh.asDouble();

				Math::ParamTex2d<unsigned char, Math::EJT_MUL>& weight = snshapes_weight[index];
				weight.defValue = (float)skip;

				MPlug plug = arrayplug.elementByLogicalIndex(index);
				if( plug.isNull())
					continue;
				
				MPlugArray connects;
				plug.connectedTo(connects, true, false);
				if(connects.length()!=1)
					continue;

				plug = connects[0];
				weight.load(plug, 128, 128, "");
			}
		}
	}
	return true;
}

bool HairShape::LoadOcellarisShapes(MObject i_attribute, MDataBlock& data, std::map< int, cls::MInstance>& shapes)
{
	MStatus stat;
	shapes.clear();
	MArrayDataHandle arh = data.inputArrayValue(i_attribute, &stat);
	if( stat)
	{
		for( unsigned i=0; i<arh.elementCount(); i++)
		{
			if( arh.jumpToArrayElement(i))
			{
				int index = arh.elementIndex();
				MPxData* pxdata = arh.inputValue(&stat).asPluginData();
				if( stat && pxdata && pxdata->typeId() == cls::MInstancePxData::id)
				{
					cls::MInstancePxData* sndata = (cls::MInstancePxData*)pxdata;
					shapes[ index ] = sndata->drawcache;
//					cls::MInstance& inst = shapes[ shapes.size()-1 ];
//					inst.getCache().storage.Dump();

//					Math::ParamTex2d<unsigned char, Math::EJT_MUL>& w = hairsystem.snshapes_weight[index];
//					w = sndata->weight;
				}
			}
		}
	}
	return true;
}

bool HairShape::LoadMeshSurface( MeshSurfaceSynk& meshsurface, MDGContext& context)
{
	MStatus stat;
	MObject val;
	meshsurface.setArray2d( MeshSurfaceSynk::T_LENGTH,			LoadAttr(context, i_HairLength[1], val));
	meshsurface.setArray2d( MeshSurfaceSynk::T_INCLINATION,		LoadAttr(context, i_HairInclination[1], val));
	meshsurface.setArray2d( MeshSurfaceSynk::T_POLAR,			LoadAttr(context, i_HairPolar[1], val));
	meshsurface.setArray2d( MeshSurfaceSynk::T_BASECURL,		LoadAttr(context, i_HairBaseCurl[1], val));
	meshsurface.setArray2d( MeshSurfaceSynk::T_TIPCURL,			LoadAttr(context, i_HairTipCurl[1], val));
	meshsurface.setArray2d( MeshSurfaceSynk::T_BASEWIDTH,		LoadAttr(context, i_HairBaseWidth[1], val));
	meshsurface.setArray2d( MeshSurfaceSynk::T_TIPWIDTH,		LoadAttr(context, i_HairTipWidth[1], val));
	meshsurface.setArray2d( MeshSurfaceSynk::T_SCRAGGLE,		LoadAttr(context, i_HairScraggle[1], val));
	meshsurface.setArray2d( MeshSurfaceSynk::T_SCRAGGLEFREQ,	LoadAttr(context, i_HairScraggleFreq[1], val));
	meshsurface.setArray2d( MeshSurfaceSynk::T_TWIST,			LoadAttr(context, i_HairTwist[1], val));
	meshsurface.setArray2d( MeshSurfaceSynk::T_DISPLACE,		LoadAttr(context, i_HairDisplace[1], val));
	meshsurface.setArray2d( MeshSurfaceSynk::T_TWISTFROMPOLAR,	LoadAttr(context, i_HairTwistFromPolar[1], val));

	meshsurface.setArray2d( MeshSurfaceSynk::T_BLENDCVFACTOR,	LoadAttr(context, i_blendCVfactor[1], val));
	meshsurface.setArray2d( MeshSurfaceSynk::T_BLENDDEFORMFACTOR,	LoadAttr(context, i_blendDeformfactor[1], val));
	
	meshsurface.setArray2d( MeshSurfaceSynk::T_BASECLUMP,		LoadAttr(context, i_HairBaseClump[1], val));
	meshsurface.setArray2d( MeshSurfaceSynk::T_TIPCLUMP,		LoadAttr(context, i_HairTipClump[1], val));

	meshsurface.setVectorArray( MeshSurfaceSynk::T_BASECOLOR,	LoadAttr(context, i_BaseColorCache, val));
	meshsurface.setVectorArray( MeshSurfaceSynk::T_TIPCOLOR,	LoadAttr(context, i_TipColorCache, val));

	// modifiers
	MDataBlock data = this->forceCache();
	meshsurface.modifiers.clear();
	MArrayDataHandle arh = data.inputArrayValue(i_HairModifiers, &stat);
	if( stat)
	{
		MDagPath path;
		MDagPath::getAPathTo(thisMObject(), path);
		Math::Matrix4f thisTransform;
		copy( thisTransform, path.inclusiveMatrix());

		meshsurface.modifiers.reserve(arh.elementCount());
		for( unsigned i=0; i<arh.elementCount(); i++)
		{
			if( arh.jumpToArrayElement(i))
			{
				MPxData* pxdata = arh.inputValue(&stat).asPluginData();
				if( stat && pxdata && pxdata->typeId() == HairModifierData::id)
				{
displayStringD("Modifier %d", i);
					HairModifierData* sndata = (HairModifierData*)pxdata;
					meshsurface.modifiers.push_back(sndata->data);

					HairMeshModifier& hmm = meshsurface.modifiers.back();
					hmm.transform = thisTransform*hmm.transform;
					hmm.transform_inv = hmm.transform.inverted();
				}
			}
		}
	}

	// deformers
	{
		meshsurface.deformers.clear();
		MArrayDataHandle arh = data.inputArrayValue(i_HairDeformers, &stat);
		if( stat)
		{
			meshsurface.deformers.reserve(arh.elementCount());
			for( unsigned i=0; i<arh.elementCount(); i++)
			{
				if( arh.jumpToArrayElement(i))
				{
					MPxData* pxdata = arh.inputValue(&stat).asPluginData();
					if( stat && pxdata && pxdata->typeId() == HairDeformerData::id)
					{
						HairDeformerData* sndata = (HairDeformerData*)pxdata;
						meshsurface.deformers.push_back(HairDeformerDecl());
						HairDeformerDecl& decl = meshsurface.deformers.back();
						decl.deformerproc = sndata->deformerproc;
						decl.deformerstream = sndata->stream;
						decl.Load();
					}
				}
			}
		}
	}

	return true;
};




HairAlive* HairShape::LoadHairAlive(MDGContext& context, MObject& outval)
{
	MPxData* pxdata = LoadAttr(context, i_HairAlive, outval);
	if( !pxdata) return NULL;
	HairAliveData* hc = (HairAliveData*)pxdata;
	return &hc->data;
}


// LOAD
HairGeometryIndices* HairShape::LoadHairGeometryIndices(MDataBlock& data, bool forceCompute)
{
	MStatus stat;
	MDataHandle inputData = data.inputValue(i_HairGeometryIndices, &stat);
	MPxData* pxdata = inputData.asPluginData();
	if( !pxdata)
	{
		ComputeHairGeometryIndices( MPlug(thisMObject(), i_HairGeometryIndices), data);
		inputData = data.inputValue(i_HairGeometryIndices, &stat);
		pxdata = inputData.asPluginData();
		return NULL;
	}
	HairGeometryIndicesData* hgh = (HairGeometryIndicesData*)pxdata;
	if( !hgh->hgh.isValid() || forceCompute)
		ComputeHairGeometryIndices( MPlug(thisMObject(), i_HairGeometryIndices), data);
	return &hgh->hgh;
}
HairGeometry* HairShape::LoadHairGeometry(MDataBlock& data)
{
	MStatus stat;
	MDataHandle inputData = data.inputValue(i_HairGeometry, &stat);
	MPxData* pxdata = inputData.asPluginData();
	if( !pxdata) return NULL;
	HairGeometryData* gd = (HairGeometryData*)pxdata;
	if( !gd->geometry.isValid())
		ComputeHairGeometry( MPlug(thisMObject(), i_HairGeometry), data);
	return &gd->geometry;
}
HairCVPositions* HairShape::LoadHairCVPositions(MDataBlock& data)
{
//	if( bRebuildCVPositions)
//		ComputeHairCVPositions(MPlug(thisMObject(), i_HairCVPositions), data);

	MStatus stat;
	MDataHandle inputData = data.inputValue(i_HairCVPositions, &stat);
	MPxData* pxdata = inputData.asPluginData();
	if( !pxdata)
	{
		static HairCVPositions shcp;
		return &shcp;
	}
	HairCVPositionsData* hc = (HairCVPositionsData*)pxdata;
	return &hc->cvpos;
}
HairCV* HairShape::LoadHairCV(MDataBlock& data)
{
//	if( bRebuildCVPositions)
//		ComputeHairCV(MPlug(thisMObject(), i_HairCV), data);

	MStatus stat;
	MDataHandle inputData = data.inputValue(i_HairCV, &stat);
	MPxData* pxdata = inputData.asPluginData();
	if( !pxdata)
	{
		static HairCV shc;
		return &shc;
	}
	HairCVData* hc = (HairCVData*)pxdata;
	return &hc->cv;
}
HairClumpPositions* HairShape::LoadHairClumpPositions(MDataBlock& data)
{
	if( bRebuildClumpData)
		ComputeHairClumpPositions(MPlug(thisMObject(), i_HairClumpPositions), data);

	MStatus stat;
	MDataHandle inputData = data.inputValue(i_HairClumpPositions, &stat);
	MPxData* pxdata = inputData.asPluginData();
	if( !pxdata) return NULL;
	HairClumpPositionsData* hc = (HairClumpPositionsData*)pxdata;
	return &hc->hc;
}

std::string HairShape::GetHairRootPositionFile(int& timeStamp, bool bAlways, bool bDump)
{
	displayStringD("GetHairRootPositionFile");

	MStatus stat;
	MDataBlock data = this->forceCache();

	MFnDependencyNode dn(thisMObject());
	bool isReference = dn.isFromReferencedFile();

	std::string fullobjname = MFnDagNode(thisMObject()).fullPathName().asChar();

	MPlug timeStampPlug(thisMObject(), i_HairTimeStamp);
	timeStampPlug.getValue(timeStamp);
	MString currentFile = data.inputValue(i_HairRootPositionFile, &stat).asString();
	MPlug hairRootPositionFilePlug(thisMObject(), i_HairRootPositionFile);

//	timeStampPlug
	bool bLocked = false;
	if( hairRootPositionFilePlug.isLocked())
		bLocked = true;
	if( timeStampPlug.isLocked())
		bLocked = true;

	// ��������� ����� � ��� �������
	int currenttime;
	std::string currentname;
	if( !bAlways)
	{
		Util::FileStream file;
		if( file.open( currentFile.asChar(), false))
		{
			file >> currenttime;
			file >> currentname;
			displayStringD("GetHairRootPositionFile: timeStamp=%d && fullobjname==%s", timeStamp, fullobjname.c_str());
			displayStringD("File %s: timeStamp=%d && fullobjname==%s", currentFile.asChar(), currenttime, currentname.c_str());
			if(timeStamp==currenttime)
			{
				if( isReference || fullobjname==currentname)
				{
					if( bDump)
					{
						if( isReference)
							displayString( "%s: cache file is valid (from reference).", fullobjname.c_str());
						else
							displayString( "%s: cache file is valid.", fullobjname.c_str());
					}
					return currentFile.asChar();
				}
			}
		}
	}
	if( !bAlways)
	{
		if( (!::hair_suppressProgress) && MGlobal::mayaState()!=MGlobal::kInteractive)
		{
//			displayStringError("Fur: %s:\n You MAST Build Cache before render throw DISTRENDER or DISTRIBUTED!!!", fullobjname.c_str());
			fprintf(stderr, "MayaHair: %s:\n You MUST build Cache before render throw DISTRENDER or DISTRIBUTED!!!\n", fullobjname.c_str());
			fprintf(stderr, "my TimeStamp=%d file timeStamp = %d\n", timeStamp, currenttime);
			fprintf(stderr, "my name = %s name in file = %s\n", fullobjname.c_str(), currentname.c_str());
			timeStamp = currenttime;
			return currentFile.asChar();
		}
	}
	if( bLocked)
	{
		displayString("%s: Cache file must be updated but attribute is LOCKED!!!", fullobjname.c_str());
		timeStamp = currenttime;
		return currentFile.asChar();
	}

	displayStringD("Update HairRootPositionFile");
	// ����� ����
	HairGeometryIndices* geometryIndices = LoadHairGeometryIndices(data);
	if( !geometryIndices) return "";
	HairGeometry* geometry = LoadHairGeometry(data);
	if( !geometry) return "";
	HairClumpPositions* clumpPositions = LoadHairClumpPositions(data);
	if( !clumpPositions) return "";
	HairCVPositions* cvPositions = LoadHairCVPositions(data);
	if( !cvPositions) return "";

	int genType = data.inputValue(i_generateType).asShort();
	int Count = data.inputValue(i_count).asInt();
	int seed = data.inputValue(i_Seed).asInt();
	int2& regdims = data.inputValue(i_regularDim).asInt2();
	double2& regoffset = data.inputValue(i_regularOffset).asDouble2();
	double2& regshear = data.inputValue(i_regularShear).asDouble2();
	double2& jitter = data.inputValue(i_jitter).asDouble2();
	double HairDensity = data.inputValue(i_HairDensity).asDouble();
	data.inputValue(i_HairDensityUvSet).asString();
	double CLUMPblending = data.inputValue(i_CLUMPblending).asDouble();
	double CLUMPradius = data.inputValue(i_CLUMPradius).asDouble();
	HairRootPositionsData::enClumpAlgorithm algorithm = (HairRootPositionsData::enClumpAlgorithm)data.inputValue(i_CLUMPalgorithm).asShort();

	int resX = data.inputValue(i_RenderResX, &stat).asInt();
	int resY = data.inputValue(i_RenderResY, &stat).asInt();

	// File name
	std::string filename = MFileIO::currentFile().asChar();
	size_t n = filename.rfind('/');
	if( n!=filename.npos)
	{
		filename.insert(n+1, ".HairCache//");
	}
	filename += ".";
//	std::string dgNodeName = MFnDagNode(thisMObject()).name().asChar();
//	correctFileName( dgNodeName);
	filename += fullobjname;
	filename += ".hair";
	Util::correctFileName(filename);
	Util::create_directory_for_file(filename.c_str());

	displayStringD(filename.c_str());

	HairRootPositions hp;
	std::set<int> workingfaces;
	if( genType==0)
	{
		bool bEqualize = false;
		HairRootPositionsData::Init(hp, thisMObject(), 
			clumpPositions, geometryIndices, geometry, 
			Count, bEqualize, 
			0, seed, 
			resX, resY, (float)CLUMPblending, true, (float)CLUMPradius, algorithm, workingfaces);
	}
	else
	{
		HairRootPositionsData::Init(hp, thisMObject(), 
			clumpPositions, geometryIndices, geometry, 
			Math::Vec2i(regdims[0], regdims[1]), 
			Math::Vec2f((float)regoffset[0], (float)regoffset[1]),
			Math::Vec2f((float)regshear[0], (float)regshear[1]),
			Math::Vec2f((float)jitter[0], (float)jitter[1]), seed, -1, 
			resX, resY, (float)CLUMPblending, false, (float)CLUMPradius, algorithm, workingfaces);
	}

	HairClumpPositionsData::CalcClumpRadius(&hp, clumpPositions, geometryIndices, geometry);

	// save 
	{
		bool bProgress = (!::hair_suppressProgress) && (MGlobal::mayaState()==MGlobal::kInteractive);

		if( bProgress)
		{
			MProgressWindow::reserve();
			MProgressWindow::setInterruptable(false);
			MProgressWindow::setTitle("Hair file cache");
			MProgressWindow::startProgress();
			MProgressWindow::setProgressRange(0, 100);
			MProgressWindow::setProgressStatus("save to file");
			MProgressWindow::setProgress(100);
		}

		std::string logfile = filename+".log";
		FILE* log = fopen(logfile.c_str(), "at");
		int place = 0;
		try
		{
			Util::FileStream file;
			if( !file.open( filename.c_str(), true))
			{
				if( log)
					fputs("Cant create Hair Cache file\n", log);
				throw "Cant create Hair Cache file";
			}
			place = 1;
			file >> timeStamp;
			file >> fullobjname;

			if( log)
			{
				fprintf(log, "timestamp=%d\nfullobjname=%s\n", 
					timeStamp,
					fullobjname.c_str());
			}
			place = 2;

			// save geometryIndices & clumpPositions & rootPositions
			geometryIndices->serialize(file);
			place = 3;
			cvPositions->serialize(file);
			place = 4;
			clumpPositions->serialize(file);
			place = 5;
			hp.serialize(file);
			place = 6;
		}
		catch(...)
		{
			if( log)
				fputs("HairRootPositionsData: Exception when save file\n", log);
			displayStringError("HairRootPositionsData: Exception when save file %s place %d", filename.c_str(), place);
			throw;
		}
		if( log)
			fclose(log);
		if( bProgress)
			MProgressWindow::endProgress();
	}

	if(bDump)
	{
		if( isReference)
			displayStringError( "%s: cache file is updated, but hair shape from reference file", fullobjname.c_str());
		else
			displayString( "%s: cache file is updated. Please save scene now.", fullobjname.c_str());
	}

	// 
	MPlug(thisMObject(), i_HairRootPositionFile).setValue(MString(filename.c_str()));

	return filename;
}

/*/
std::string HairShape::LoadHairRootPositions(time_t& timestamp)
{
	MDataBlock data = this->forceCache();
	return LoadHairRootPositions(data, timestamp);
}

std::string HairShape::LoadHairRootPositions(MDataBlock& data, time_t& timestamp)
{
	if( bRebuildRootData)
	{
		ComputeHairRootPositions(MPlug(thisMObject(), i_HairRootPositions), data);
	}

	MStatus stat;
	{
		MDataHandle inputData = data.inputValue(i_HairRootPositions, &stat);
		MPxData* pxdata = inputData.asPluginData();
		if( !pxdata) return "";
		HairFixedFileData* fixedFileData = (HairFixedFileData*)pxdata;
		timestamp = fixedFileData->timestamp;
		if( fixedFileData->IsValid()) return fixedFileData->filename;
	}

	{
		// REBUILD
		ComputeHairRootPositions(MPlug(thisMObject(), i_HairRootPositions), data);
		MDataHandle inputData = data.inputValue(i_HairRootPositions, &stat);
		MPxData* pxdata = inputData.asPluginData();
		if( !pxdata) return "";
		HairFixedFileData* fixedFileData = (HairFixedFileData*)pxdata;
		timestamp = fixedFileData->timestamp;
		return fixedFileData->filename;
	}
}
/*/

HairRootPositions* HairShape::LoadHairDrawPlacement(MDataBlock& data)
{
	if( bRebuildDrawData)
		ComputeHairDrawPlacement(MPlug(thisMObject(), i_hairDrawPlacement), data);
	
	MStatus stat;
	MDataHandle inputData = data.inputValue(i_hairDrawPlacement, &stat);
	MPxData* pxdata = inputData.asPluginData();
	if( !pxdata) return NULL;
	HairRootPositionsData* placement = (HairRootPositionsData*)pxdata;
	return &placement->hp;
}

bool HairShape::LoadMeshSurface(MeshSurfaceSynk& meshsurface, MDataBlock& data, MObject thisMObject)
{
	MStatus stat;

	meshsurface.setArray2d( MeshSurfaceSynk::T_LENGTH,			data.inputValue( i_HairLength[1]).asPluginData());
	meshsurface.setArray2d( MeshSurfaceSynk::T_INCLINATION,		data.inputValue( i_HairInclination[1]).asPluginData());
	meshsurface.setArray2d( MeshSurfaceSynk::T_POLAR,			data.inputValue( i_HairPolar[1]).asPluginData());
	meshsurface.setArray2d( MeshSurfaceSynk::T_BASECURL,		data.inputValue( i_HairBaseCurl[1]).asPluginData());
	meshsurface.setArray2d( MeshSurfaceSynk::T_TIPCURL,			data.inputValue( i_HairTipCurl[1]).asPluginData());
	meshsurface.setArray2d( MeshSurfaceSynk::T_BASEWIDTH,		data.inputValue( i_HairBaseWidth[1]).asPluginData());
	meshsurface.setArray2d( MeshSurfaceSynk::T_TIPWIDTH,		data.inputValue( i_HairTipWidth[1]).asPluginData());
	meshsurface.setArray2d( MeshSurfaceSynk::T_SCRAGGLE,		data.inputValue( i_HairScraggle[1]).asPluginData());
	meshsurface.setArray2d( MeshSurfaceSynk::T_SCRAGGLEFREQ,	data.inputValue( i_HairScraggleFreq[1]).asPluginData());
	meshsurface.setArray2d( MeshSurfaceSynk::T_TWIST,			data.inputValue( i_HairTwist[1]).asPluginData());
	meshsurface.setArray2d( MeshSurfaceSynk::T_DISPLACE,		data.inputValue( i_HairDisplace[1]).asPluginData());
	meshsurface.setArray2d( MeshSurfaceSynk::T_TWISTFROMPOLAR,	data.inputValue( i_HairTwistFromPolar[1]).asPluginData());

	meshsurface.setArray2d( MeshSurfaceSynk::T_BLENDCVFACTOR,	data.inputValue( i_blendCVfactor[1]).asPluginData());
	meshsurface.setArray2d( MeshSurfaceSynk::T_BLENDDEFORMFACTOR,	data.inputValue( i_blendDeformfactor[1]).asPluginData());

	meshsurface.setArray2d( MeshSurfaceSynk::T_BASECLUMP,		data.inputValue( i_HairBaseClump[1]).asPluginData());
	meshsurface.setArray2d( MeshSurfaceSynk::T_TIPCLUMP,		data.inputValue( i_HairTipClump[1]).asPluginData());

	meshsurface.setVectorArray( MeshSurfaceSynk::T_BASECOLOR,		data.inputValue( i_BaseColorCache).asPluginData());
	meshsurface.setVectorArray( MeshSurfaceSynk::T_TIPCOLOR,		data.inputValue( i_TipColorCache).asPluginData());

	// modifiers
	meshsurface.modifiers.clear();
	MArrayDataHandle arh = data.inputArrayValue(i_HairModifiers, &stat);
	if( stat)
	{
		MDagPath path;
		MDagPath::getAPathTo(thisMObject, path);
		Math::Matrix4f thisTransform;
		copy( thisTransform, path.inclusiveMatrix());

		meshsurface.modifiers.reserve(arh.elementCount());
		for( unsigned i=0; i<arh.elementCount(); i++)
		{
			if( arh.jumpToArrayElement(i))
			{
				MPxData* pxdata = arh.inputValue(&stat).asPluginData();
				if( stat && pxdata && pxdata->typeId() == HairModifierData::id)
				{
displayStringD("Modifier %d", i);
					HairModifierData* sndata = (HairModifierData*)pxdata;
					meshsurface.modifiers.push_back(sndata->data);

					HairMeshModifier& hmm = meshsurface.modifiers.back();
					hmm.transform = thisTransform*hmm.transform;
					hmm.transform_inv = hmm.transform.inverted();
				}
			}
		}
	}

	// deformers
	{
		meshsurface.deformers.clear();
		MArrayDataHandle arh = data.inputArrayValue(i_HairDeformers, &stat);
		if( stat)
		{
			meshsurface.deformers.reserve(arh.elementCount());
			for( unsigned i=0; i<arh.elementCount(); i++)
			{
				if( arh.jumpToArrayElement(i))
				{
					MPxData* pxdata = arh.inputValue(&stat).asPluginData();
					if( stat && pxdata && pxdata->typeId() == HairDeformerData::id)
					{
						HairDeformerData* sndata = (HairDeformerData*)pxdata;
						meshsurface.deformers.push_back(HairDeformerDecl());
						HairDeformerDecl& decl = meshsurface.deformers.back();
						decl.deformerproc = sndata->deformerproc;
						decl.deformerstream = sndata->stream;
						decl.Load();
					}
					arh.next();
				}
			}
		}
	}


	return true;
}

void HairShape::LoadHairSystem(MDataBlock& data, HairSystem& hairsystem)
{
	MStatus stat;

	int orthogonalize = data.inputValue(i_orthogonalize).asShort();
	hairsystem.orthogonalize = (enOrthogonalizeType)orthogonalize;

	hairsystem.SegmCount = data.inputValue(i_SegmCount).asInt();
	hairsystem.CVtailU = data.inputValue( i_CVtailU).asBool();
	hairsystem.CVtailV = data.inputValue( i_CVtailV).asBool();

	hairsystem.mulWidthByLength = data.inputValue( i_mulWidthByLength).asBool();

	// snshapes
	hairsystem.ocsfiles.clear();
	hairsystem.snshapes.clear();
	hairsystem.snshapes_weight.clear();
	MArrayDataHandle arh = data.inputArrayValue(i_snShape, &stat);
	if( stat)
	{
		for( unsigned i=0; i<arh.elementCount(); i++)
		{
			if( arh.jumpToArrayElement(i))
			{
				int index = arh.elementIndex();
				MDataHandle dh = arh.inputValue(&stat);
				if(stat)
				{
					MObject obj = dh.data();
					MFnPluginData pldata(obj);
					MTypeId tid = pldata.typeId();

					if( tid == MTypeId(SN_DATAID))
					{
						MPxData* pxdata = dh.asPluginData();
						if( stat && pxdata && pxdata->typeId() == MTypeId(SN_DATAID))
						{
							SnData* sndata = (SnData*)pxdata;
							hairsystem.snshapes[ index ] = sndata->ssp;
//							hairsystem.snshapes_weight.push_back(sndata->weight);

							Math::ParamTex2d<unsigned char, Math::EJT_MUL>& w = hairsystem.snshapes_weight[index];
							w = sndata->weight;
						}
					}
				}
			}
		}
	}

	LoadInstanceWeights(i_instanceweights, data, hairsystem.snshapes_weight);

	LoadOcellarisShapes(i_ocellarisShape, data, hairsystem.ocsfiles);


	hairsystem.splitGroups = data.inputValue( i_splitGroups).asBool();
	hairsystem.bRenderBoxes = data.inputValue( i_bRenderBoxes).asBool();
	hairsystem.bAccurateBoxes = data.inputValue( i_bAccurateBoxes).asBool();
	

	hairsystem.workingfaces.clear();
	bool bUseWorkingFaces = data.inputValue( i_bUseWorkingFaces).asBool();
	if( bUseWorkingFaces)
		LoadHairWorkingFaces(data, hairsystem.workingfaces);

	// FREEZE
	{
		MString freezefile;
		freezefile = data.inputValue( i_FreezeFile).asString();
		if( freezefile.length()!=0)
		{
			FILE* f = fopen(freezefile.asChar(), "rb");
			if(f)
			{
				fclose( f);
			}
			else
				freezefile = "";
		}
		hairsystem.freezefile = freezefile.asChar();
	}

	hairsystem.dumpInformation = data.inputValue( i_dumpInformation).asBool();
	hairsystem.pruningScaleFactor = (float)data.inputValue( i_pruningScaleFactor).asDouble();
}

HairAlive* HairShape::LoadHairAlive(MDataBlock& data)
{
	MStatus stat;
	MDataHandle inputData = data.inputValue(i_HairAlive, &stat);
	MPxData* pxdata = inputData.asPluginData();
	if( !pxdata) return NULL;
	HairAliveData* hc = (HairAliveData*)pxdata;
	return &hc->data;
}

void HairShape::LoadHairWorkingFaces(MDataBlock& data, std::set<int>& workingfaces)
{
	workingfaces.clear();

	MObject obj = data.inputValue(i_WorkingFaces).data();
	if( obj.isNull()) return;

	MFnIntArrayData intdata(obj);
	for(int i=0; i<(int)intdata.length(); i++)
	{
		int n = intdata[i];
		workingfaces.insert(n);
	}
}


// COMPUTE
MStatus HairShape::ComputeBuildMessage(const MPlug& plug, MDataBlock& data)
{
	/*/
	{
		MPlug plug(thisMObject(), i_test);
		MPlugArray ar;
		plug.connectedTo(ar, true, false);
		plug = ar[0];

		MObject obj;
		plug.getValue(obj);

		MArrayDataHandle testarray = data.inputArrayValue(i_test);
		for( unsigned i=0; i<testarray.elementCount(); i++)
		{
			if( testarray.jumpToArrayElement(i))
			{
				MDataHandle dh = testarray.inputValue();
				double3& pt = dh.asDouble3();
				printf("%d: %f, %f, %f\n", i, pt[0], pt[1], pt[2]);
			}
		}
	}
	/*/
	

	MStatus stat;
	displayStringD("ComputeBuildMessage %s", plug.name().asChar());

	enDrawMode drawMode = (enDrawMode)data.inputValue(i_drawMode).asShort();
//	this->drawBox = data.inputValue(i_drawBox, &stat).asBool();
//	data.inputValue(i_drawClumps, &stat).asBool();
//	bool drawCV = data.inputValue(i_drawCV, &stat).asBool();
//	bool drawHairs = !drawCV;
	int maxHairDrawable = data.inputValue(i_maxHairDrawable, &stat).asInt();

	HairGeometryIndices* geometryIndices = LoadHairGeometryIndices(data);
	if( !geometryIndices) return MS::kFailure;
	HairGeometry* hairgeometry = LoadHairGeometry(data);
	if( !hairgeometry) return MS::kFailure;
	HairClumpPositions* clumpPositions = LoadHairClumpPositions(data);
	if( !clumpPositions) return MS::kFailure;
	HairRootPositions* rootPositions = LoadHairDrawPlacement(data);
	if( !rootPositions) return MS::kFailure;
	HairCVPositions* cvposition = LoadHairCVPositions(data);
	if( !cvposition) return MS::kFailure;
	HairCV* cvs = LoadHairCV(data);
	if( !cvs) return MS::kFailure;
	HairAlive* ha = LoadHairAlive(data);

	bool useDynamic = cvposition->controlhairCount()!=0;

	HairSystem hairsystem;
	HairShape::LoadHairSystem(data, hairsystem);

	int resX = data.inputValue(i_PaintAttrResX, &stat).asInt();
	int resY = data.inputValue(i_PaintAttrResY, &stat).asInt();
	MeshSurfaceSynk meshsurface;
	meshsurface.init(geometryIndices, hairgeometry, clumpPositions, rootPositions, cvposition, cvs, &hairsystem);
	if( !HairShape::LoadMeshSurface(meshsurface, data, thisMObject()))
		return MS::kFailure;

	// deformer
	meshsurface.InitDeformers();
	meshsurface.initParamsUVsetIndices();

	// ������ ���������� INCREASELENGTH
	std::vector<IHairDeformer*> deformers_INCREASELENGTH;
	int deformercount = meshsurface.getDeformerCount();
	deformers_INCREASELENGTH.reserve(deformercount);
	for(int i=0; i<(int)deformercount; i++)
	{
		IHairDeformer* deformer = meshsurface.getDeformer(i);
		if( !deformer) continue;
		int type = deformer->getType();
		if( type&IHairDeformer::INCREASELENGTH)
			deformers_INCREASELENGTH.push_back(deformer);
	}

	float ml = meshsurface.getMaxLength(deformers_INCREASELENGTH);

	bool bInstanced = false;
	bInstanced |= hairsystem.snshapes.size()!=0;
	bInstanced |= hairsystem.ocsfiles.size()!=0;
	#ifdef CHEVELURELIGHT
		bInstanced = false;
	#endif CHEVELURELIGHT
	if( drawMode!=DM_INSTANCER && drawMode!=DM_INSTANCERBOX)
		bInstanced = false;
	Math::Box3f box = meshsurface.getBox();

	if( !bInstanced)
	{
		box.expand(ml);
	}
	else
	{
		Math::Box3f snbox;
		{
			std::map< int, sn::ShapeShortcutPtr >::iterator it = hairsystem.snshapes.begin();
			for( ; it != hairsystem.snshapes.end(); it++)
			{
				Math::Box3f _snbox;
				sn::ShapeShortcutPtr& snInst = it->second;
				snInst->shaper->getDirtyBox(snInst->shape, Math::Matrix4f::id, _snbox);
				snbox.insert(_snbox);
			}
		}
		{
			std::map< int, cls::MInstance>::iterator it = hairsystem.ocsfiles.begin();
			for( ; it != hairsystem.ocsfiles.end(); it++)
			{
				cls::MInstance& snInst = it->second;
				Math::Box3f _snbox = snInst.getBox();
				snbox.insert(_snbox);
			}
		}
		box.min += snbox.min*ml;
		box.max += snbox.max*ml;
	}

	geometry.ClearInstances();
	geometry.Init(0, 0, HI_LINIAR, false, NULL);
	geometry.InitSn(0);

	bool mulWidthByLength = hairsystem.mulWidthByLength;
	if( !bInstanced)
	{
		if(drawMode==DM_INSTANCER||drawMode==DM_INSTANCERBOX) 
			drawMode = DM_HAIRS;
	}
	else
	{
		mulWidthByLength = false;
	}

	int uvset = -1;
	std::string colorUvSet = data.inputValue(i_colorUvSetName).asString().asChar();
	if( !colorUvSet.empty())
	{
		for( unsigned long s=0; s<geometryIndices->uvSetCount(); s++)
			if( strcmp( geometryIndices->uvSetName(s), colorUvSet.c_str())==0)
				uvset = s;
		if(uvset==-1)
			displayString("cant find uvset: %s", colorUvSet.c_str());
	}

	switch(drawMode)
	{
		case DM_NONE:
			break;
		case DM_HAIRS:
		case DM_CLUMPS:
		{
			bool bHairIsClump = drawMode==DM_CLUMPS;
			HairProcessor processor(
				hairsystem.SegmCount, meshsurface, 
				hairsystem.snshapes, 
				hairsystem.ocsfiles, 
				hairsystem.snshapes_weight, 
				mulWidthByLength, 
				hairsystem.orthogonalize, 
				false, 
				bHairIsClump);
			int count = rootPositions->getCount();

			HAIRPARAMS params;
			HAIRVERTS hair;
			geometry.Init(count, hairsystem.SegmCount+1, HI_LINIAR, false, NULL);
			for(int i=0; i<count; i++)
			{
				if( ha && !(*ha)[i]) continue;
				params.clear(0);
				processor.BuildHair(i, params, hair);
				if( !hair.vc()) continue;

				float colorU=params.u, colorV=params.v;
				if(uvset>=0)
				{
					Math::Vec2f uv = rootPositions->getUVset(i, uvset, geometryIndices, hairgeometry);
					colorU = uv.x;
					colorV = uv.y;
				}

				Math::Vec3f BaseColor = meshsurface.getVectorValue(MeshSurfaceSynk::T_BASECOLOR, colorU, colorV);
				Math::Vec3f TipColor  = meshsurface.getVectorValue(MeshSurfaceSynk::T_TIPCOLOR, colorU, colorV);

				int id = i;
				geometry.sethair(i, hair.vc(), params.u, params.v, id, params.position, params.normal);
				for(int v=0; v<hair.vc(); v++)
				{
					geometry.sethairvertex(i, v, hair.cvint[v]);
					float factor = hair.vfactor[v];
					Math::Vec3f color = (1-factor)*BaseColor + factor*TipColor;
					geometry.sethaircolor(i, v, color);
				}
			}
			break;
		}
		case DM_DYNAMIC_CURVES:
		{
			int count = cvs->getCount();
			geometry.Init(count, 100, HI_LINIAR, false, NULL);
			for(int i=0; i<count; i++)
			{
				HairCV::CV& cv = cvs->cvs[i];
				Math::Matrix4f m = Math::Matrix4f::id;
				HairCV::getControlHairBasis(i, cvposition, geometryIndices, hairgeometry, m);
				if( !cv.vc()) continue;
				if( !cv.bValid) continue;
				int id = i;
				geometry.sethair(i, cv.vc(), 0, 0, id, Math::Vec3f(0, 0, 0), Math::Vec3f(1, 0, 0));
				for(int v=0; v<cv.vc(); v++)
				{
					Math::Vec3f vert = cv.ts[v];
					vert = m*vert;
					geometry.sethairvertex(i, v, vert);
				}
			}
			break;
		}
		case DM_INSTANCER:
		case DM_INSTANCERBOX:
		{
			if( drawMode==DM_INSTANCERBOX)
				geometry.bDrawAsBox = true;
			else
				geometry.bDrawAsBox = false;

			HairProcessor processor(
				hairsystem.SegmCount, 
				meshsurface, 
				hairsystem.snshapes, 
				hairsystem.ocsfiles, 
				hairsystem.snshapes_weight, 
				mulWidthByLength, 
				hairsystem.orthogonalize, 
				false);
			int count = rootPositions->getCount();

			HAIRPARAMS params;
			HAIRVERTS hair;
			geometry.InitSn(count);

			{
				std::map< int, sn::ShapeShortcutPtr >::iterator it = hairsystem.snshapes.begin();
				for(;it != hairsystem.snshapes.end(); it++)
					geometry.setSnShape(it->first, it->second);
			}

			{
				std::map< int, cls::MInstance>::iterator it = hairsystem.ocsfiles.begin();
				for(;it != hairsystem.ocsfiles.end(); it++)
					geometry.setOcsShape(it->first, it->second);
			}

			for(int i=0; i<count; i++)
			{
//				processor.ParseHair(params, i, false);
				processor.BuildHair(i, params, hair);

				geometry.SetHair(
					i, 
					i,
					params, 
					hair,
					RNS_NONE,	// hairsystem.renderNormalSource, 
					1,			// hairsystem.RenderWidth, 
					0			// hairsystem.widthBias
					);

				/*/
				HairPersonageData hairPersonageData;
				Math::Matrix4f matr;
				matr[0] = Math::Vec4f(params.tangentU,	0);
				matr[1] = Math::Vec4f(params.normal,	0);
				matr[2] = Math::Vec4f(params.tangentV,	0);
				matr[3] = Math::Vec4f(params.position,	1);
				if(false)
				{
					for(int x=0; x<(int)hair.basas.size(); x++)
					{
						Math::Matrix4f& matr = hair.basas[x];
						printf("matrix %d:\n", x);
						printf("%f %f %f %f\n", matr[0][0], matr[0][1], matr[0][2], matr[0][3]);
						printf("%f %f %f %f\n", matr[1][0], matr[1][1], matr[1][2], matr[1][3]);
						printf("%f %f %f %f\n", matr[2][0], matr[2][1], matr[2][2], matr[2][3]);
						printf("%f %f %f %f\n", matr[3][0], matr[3][1], matr[3][2], matr[3][3]);
					}
					fflush(stdout);
				}
				hairPersonageData.transforms	= hair.basas;
				hairPersonageData.parameters	= hair.vfactor;
				hairPersonageData.transform		= matr;
				hairPersonageData.polar			= params.polar;
				hairPersonageData.inclination	= params.inclination;
				hairPersonageData.baseCurl		= params.baseCurl;
				hairPersonageData.tipCurl		= params.tipCurl;
				float scalelen = params.length*params.lengthjitter;

				hairPersonageData.length		= scalelen;
				hairPersonageData.baseWidth		= meshsurface.getWidth(params.u, params.v, 0);
				hairPersonageData.tipWidth		= meshsurface.getWidth(params.u, params.v, 1);
				hairPersonageData.twist			= params.twist;
				hairPersonageData.u				= params.u;
				hairPersonageData.v				= params.v;
				hairPersonageData.id			= params.seed;
				hairPersonageData.animation		= params.scraggleFreq;

				geometry.setSnHair(
					i, params.instance, hairPersonageData);
				/*/
			}
			break;
		}
		case DM_BOX:
			// ���� �� ������
			break;
	}
	copy( this->box, box);
	bValidBox = true;

	MDataHandle outputData = data.outputValue(o_buildMessage, &stat);
	outputData.setClean();

	/*/
	for(i=0; i<(int)deformers.size(); i++)
	{
		IHairDeformer* deformer = deformers[i];
		if(deformer)deformer->release();
	}
	/*/

	displayStringD("ComputeBuildMessage Finished %s", plug.name().asChar());
	return MS::kSuccess;
}

// ������� �������� ���������� � CV
MStatus HairShape::exportDeformationToCV(
	MDataBlock& data, 
	const HairCVPositions* dstCVpos, 
	HairCV* dstCV
	)
{
	MStatus stat;
	displayStringD("exportDeformationToCV");

	HairGeometryIndices* geometryIndices = LoadHairGeometryIndices(data);
	if( !geometryIndices) return MS::kFailure;
	HairGeometry* hairgeometry = LoadHairGeometry(data);
	if( !hairgeometry) return MS::kFailure;
	HairClumpPositions* clumpPositions = LoadHairClumpPositions(data);
	if( !clumpPositions) return MS::kFailure;
	HairRootPositions* rootPositions = LoadHairDrawPlacement(data);
	if( !rootPositions) return MS::kFailure;
	HairCVPositions* cvposition = LoadHairCVPositions(data);
	if( !cvposition) return MS::kFailure;
	HairCV* cvs = LoadHairCV(data);
	if( !cvs) return MS::kFailure;

	HairSystem hairsystem;
	HairShape::LoadHairSystem(data, hairsystem);

	MeshSurfaceSynk meshsurface;
	meshsurface.init(geometryIndices, hairgeometry, clumpPositions, rootPositions, cvposition, cvs, &hairsystem);
	if( !HairShape::LoadMeshSurface(meshsurface, data, thisMObject()))
		return MS::kFailure;

	// deformer
	meshsurface.InitDeformers();
	meshsurface.initParamsUVsetIndices();

	bool res = dstCV->init(dstCVpos->controlVertexCount(), (HairCVPositions*)dstCVpos, meshsurface);
	if(!res)
		return MS::kFailure;
	return MS::kSuccess;
}

MStatus HairShape::Save(
	Util::Stream& file, 
	Math::Box3f& box, 
	MDGContext& context, 
	HairSystem& hairsystem, 
	cls::IRender* render)
{
	MStatus stat;

	{
		MObject obj = thisMObject();
		int timestamp;
		MPlug(obj, i_HairTimeStamp).getValue(timestamp, context);

		displayStringD("timestamp = %d", timestamp);
	}
	MDataBlock data = forceCache();

	MObject val_geometryIndices;
	HairGeometryIndices* geometryIndices = LoadHairGeometryIndices(context, val_geometryIndices);
	if( !geometryIndices) return MS::kFailure;
	MObject val_geometry;
	HairGeometry* geometry = LoadHairGeometry(context, val_geometry);
	if( !geometry) return MS::kFailure;
	MObject val_clumpPositions;
	HairClumpPositions* clumpPositions = LoadHairClumpPositions(context, val_clumpPositions);
	if( !clumpPositions) return MS::kFailure;
	MObject val_cvpositions;
	HairCVPositions* cvpositions = LoadHairCVPositions(data);
	if( !cvpositions) return MS::kFailure;
	MObject val_cv;
	HairCV* cv = LoadHairCV(data);
	if( !cv) return MS::kFailure;

	// MeshSurfaceSynk
	MeshSurfaceSynk meshsurface;
	meshsurface.init(geometryIndices, geometry, clumpPositions, NULL, cvpositions, cv, &hairsystem);
	if( !HairShape::LoadMeshSurface(meshsurface, context))
		return MS::kFailure;

	// deformer
	meshsurface.InitDeformers();
	meshsurface.initParamsUVsetIndices();

	// ������ ���������� INCREASELENGTH
	std::vector<IHairDeformer*> deformers_INCREASELENGTH;
	int deformercount = meshsurface.getDeformerCount();
	deformers_INCREASELENGTH.reserve(deformercount);
	for(int i=0; i<(int)deformercount; i++)
	{
		IHairDeformer* deformer = meshsurface.getDeformer(i);
		if( !deformer) continue;
		int type = deformer->getType();
		if( type&IHairDeformer::INCREASELENGTH)
			deformers_INCREASELENGTH.push_back(deformer);
	}

	// box
	box = meshsurface.getBox();
	float ml = meshsurface.getMaxLength(deformers_INCREASELENGTH);

#ifndef OCELLARIS_INSTANCE
	bool bInstanced = hairsystem.snshapes.size()!=0;
	#ifdef CHEVELURELIGHT
		bInstanced = false;
	#endif CHEVELURELIGHT
	if( !bInstanced)
	{
		box.expand(ml);
	}
	else
	{
		Math::Box3f snbox;
		std::map< int, sn::ShapeShortcutPtr >::iterator it = hairsystem.snshapes.begin();
		for( ; it != hairsystem.snshapes.end(); it++)
		{
			Math::Box3f _snbox;
			sn::ShapeShortcutPtr& snInst = it->second;
			snInst->shaper->getDirtyBox(snInst->shape, Math::Matrix4f::id, _snbox);
			snbox.insert(_snbox);
		}
		box.min += snbox.min*ml;
		box.max += snbox.max*ml;
		// �� ����� ����� �����?
		box.expand(0.5);
	}
#else
	bool bInstanced = hairsystem.ocsfiles.size()!=0;
	#ifdef CHEVELURELIGHT
		bInstanced = false;
	#endif CHEVELURELIGHT
	if( !bInstanced)
		box.expand(ml);
	else
	{
		Math::Box3f snbox;
		std::map< int, cls::MInstance>::iterator it = hairsystem.ocsfiles.begin();
		for( ; it != hairsystem.ocsfiles.end(); it++)
		{
			cls::MInstance& snInst = it->second;
			Math::Box3f _snbox = snInst.getBox();
			snbox.insert(_snbox);
		}
		box.min += snbox.min*ml;
		box.max += snbox.max*ml;
		box.expand(0.5);
	}
#endif// OCELLARIS_INSTANCE

	if( !render)
	{
		// save hairrootfilename & hairprocessor & meshsurface & geometry
		meshsurface.serialize(file);
		geometry->serialize(file);
		cv->serialize(file);
	}

	if( render)
	{
		meshsurface.serializeOcs("HairMesh", render, true);
		geometry->serializeOcs("geometry", render, true);
		cv->serializeOcs("cv", render, true);
		

		MString uvSetName = data.inputValue(i_uvSetName).asString();
		MDataHandle inputData = data.inputValue(i_geometry, &stat);
		MObject obj = inputData.data();

		if(obj.hasFn(MFn::kMesh))
		{
			MFnMesh surface(obj);

			MFloatPointArray vertexArray;
			MFloatVectorArray normals;
			surface.getPoints(vertexArray);
			surface.getNormals(normals);

			MFloatArray uArray, vArray;
			if( uvSetName.length()==0)
				surface.getCurrentUVSetName(uvSetName);
			surface.getUVs(uArray, vArray, &uvSetName);
			int uvcount = uArray.length();
			if( uvcount==0)
			{
				// ������ UVSet
				uvcount = vertexArray.length();
				uArray.setLength(uvcount);
				vArray.setLength(uvcount);
				for(int x=0; x<uvcount; x++)
				{
					uArray[x] = vertexArray[ x ].x;
					vArray[x] = vertexArray[ x ].y;
				}
			}
			std::vector<MFloatArray> uvSet_uArray(geometryIndices->uvSetCount());
			std::vector<MFloatArray> uvSet_vArray(geometryIndices->uvSetCount());
			std::vector<int> uvSet_counts(geometryIndices->uvSetCount());
			unsigned i;
			for( i=0; i<geometryIndices->uvSetCount(); i++)
			{
				MString name = geometryIndices->uvSetName(i);
				surface.getUVs(uvSet_uArray[i], uvSet_vArray[i], &name);
				uvSet_counts[i] = uvSet_uArray[i].length();
			}

//			cls::PA< Math::Vec3f> geometry_vertex(cls::PI_VERTEX, vertexArray.length(), cls::PT_POINT);
//			cls::PA< Math::Vec3f> geometry_normal(cls::PI_PRIMITIVEVERTEX, normals.length(), cls::PT_NORMAL);
			cls::PA< float> geometry_s(cls::PI_PRIMITIVEVERTEX, uvcount);
			cls::PA< float> geometry_t(cls::PI_PRIMITIVEVERTEX, uvcount);

			unsigned x;
			/*/
			for(unsigned x=0; x<vertexArray.length(); x++)
				::copy( geometry_vertex[x], vertexArray[x]);

			for(x=0; x<normals.length(); x++)
				::copy( geometry_normal[x], normals[x]);
			/*/

			for(x=0; x<uArray.length(); x++)
			{
				geometry_s[x] = uArray[ x ];
				geometry_t[x] = vArray[ x ];
			}

			{
				// ������ ��������
				int z;
//				cls::PA<int> verts(cls::PI_PRIMITIVEVERTEX, normals.length());
				cls::PA<int> normalindexByfaceindex(cls::PI_PRIMITIVEVERTEX, normals.length());
				MItMeshPolygon polyIter(obj);
				int faceVertex=0;
				for(polyIter.reset(); !polyIter.isDone(); polyIter.next())
				{
					int polyIndex = polyIter.index();

					MIntArray vertices, nindicies;
					polyIter.getVertices( vertices );
					// normal
					nindicies.setLength(vertices.length());
					for(z=0; z<(int)vertices.length(); z++)
						nindicies[z] = polyIter.normalIndex(z);

					for(z=0; z<(int)vertices.length(); z++)
					{
						int ind = vertices[z];
						int nind = nindicies[z];
						if(nind >= (int)normals.length())
						{
							displayString("normal %d out of data (polygon[%d].%d)\n", nind, polyIndex, z);
							continue;
						}
						if(ind >= (int)vertexArray.length())
						{
							displayString("vertex %d out of data (polygon[%d].%d)\n", ind, polyIndex, z);
							continue;
						}
//						verts[nind] = ind;
						normalindexByfaceindex[nind] = faceVertex++;
					}
				}
//				render->Parameter("mesh::verts", verts);
				render->Parameter("Hair::normalindexByfaceindex", normalindexByfaceindex);
				render->Parameter("Hair::NormalsIsFacevertex", true);
			}

//			render->Parameter("P", geometry_vertex);
//			render->Parameter("N", geometry_normal);
			render->Parameter("s", geometry_s);
			render->Parameter("t", geometry_t);
			for( i=0; i<uvSet_counts.size(); i++)
			{
				MString name = geometryIndices->uvSetName(i);
				cls::PA< float> uv_s(cls::PI_PRIMITIVEVERTEX, uvSet_counts[i]);
				cls::PA< float> uv_t(cls::PI_PRIMITIVEVERTEX, uvSet_counts[i]);
				MFloatArray& uArray = uvSet_uArray[i];
				MFloatArray& vArray = uvSet_vArray[i];
				for(x=0; x<uArray.length(); x++)
				{
					uv_s[x] = uArray[ x ];
					uv_t[x] = vArray[ x ];
				}
				std::string pname;
				pname = name.asChar();pname+="_s";
				render->Parameter(pname.c_str(), uv_s);
				pname = name.asChar();pname+="_t";
				render->Parameter(pname.c_str(), uv_t);
			}

		}
		else 
			return MS::kSuccess;
	}
	return MS::kSuccess;
}

// ������� CV � ��������
MStatus HairShape::saveCVtoTexture(
	const char* what, 
	const char* filename, 
	int res)
{
	MDataBlock data = forceCache();

	HairGeometryIndices* geometryIndices = LoadHairGeometryIndices(data);
	if( !geometryIndices) return MS::kFailure;
	HairGeometry* geometry = LoadHairGeometry(data);
	if( !geometry) return MS::kFailure;
	HairCVPositions* cvpositions = LoadHairCVPositions(data);
	if( !cvpositions) return MS::kFailure;
	HairCV* cv = LoadHairCV(data);
	if( !cv) return MS::kFailure;

	Math::matrixMxN<Util::rgb> texture;
	// ������� � ��������
	cv->export_(
		texture, 
		what,			// ����� ��������
		res,					// ����������
		geometryIndices, 
		geometry,
		cvpositions
		);

	saveBmp(filename, texture);
//	saveTiff( (std::string(filename)+".tiff").c_str(), texture);

	return MS::kSuccess;
}

// ����������� � ����� (��������� � from)
bool HairShape::intersection(
	const Math::Vec3f& from, const Math::Vec3f& direction, 
	Math::Vec3f& closest, Math::Vec3f& closestbary, int& closestface)
{
	MDataBlock data = forceCache();
	HairGeometryIndices* geometryindex = LoadHairGeometryIndices(data);
	if( !geometryindex) return false;
	HairGeometry* geometry = LoadHairGeometry(data);
	if( !geometry) return false;

	// ������� ���������
	float minDistance = std::numeric_limits<float>::max();
	for( int f=0; f<(int)geometryindex->faceCount(); f++)
	{
		const Math::Face32& face = geometryindex->face(f);
		Math::Vec3f& v0 = geometry->vertex(face[0]);
		Math::Vec3f& v1 = geometry->vertex(face[1]);
		Math::Vec3f& v2 = geometry->vertex(face[2]);
	
		// ����� ������ ��� ��� � �����������. ���������� � ��-��
		Math::Vec3f bary;
		float mDistance;
		bool res = Math::RayTriIntersection<false>(
			v0, v1, v2, 
			from,
			direction, 
			bary, mDistance);
		if(!res) continue;

		if( mDistance<minDistance)
		{
			closestbary = bary;
			closestface = f;
			closest = v0*bary.x + v1*bary.y + v2*bary.z;
			minDistance = mDistance;
		}
	}
	if( minDistance != std::numeric_limits<float>::max())
	{
		return true;
	}
	return false;
}

/*/
// ��������
// �������� �������� (����� ��� ������ �������)
bool HairShape::updateTexture(
	const char* streamname,
	enPaintOperation operation, 
	float value,
	const Math::Vec3f& from1, const Math::Vec3f& direction1, 
	const Math::Vec3f& bary1, int face1,
	const Math::Vec3f& from2, const Math::Vec3f& direction2
	)
{
	// ���������
	MDataBlock data = forceCache();
	HairGeometryIndices* geometryindex = LoadHairGeometryIndices(data);
	if( !geometryindex) return false;
	HairGeometry* geometry = LoadHairGeometry(data);
	if( !geometry) return false;

	HairDeformerData* pCombDeformerData;
	HairCombDeformer* combdeformer = this->getCombDeformer(pCombDeformerData);
	if( !combdeformer) return false;

	// ���������...
	{
		int resX = 32, resY = 32;
		if(combdeformer->length.empty())
			combdeformer->length.init(resX, resY);

		// ����������� 1-�� � 2-�� ���� � ���������� ��-��
		const Math::Face32& face = geometryindex->face(face1);
		const Math::Vec3f& v0 = geometry->vertex(face[0]);
		const Math::Vec3f& v1 = geometry->vertex(face[1]);
		const Math::Vec3f& v2 = geometry->vertex(face[2]);

		Math::Vec3f inter1 = v0*bary1.x + v1*bary1.y + v2*bary1.z;

		Math::Vec3f inter2;
		if( !Math::RayTriPlaneIntersection( v0, v1, v2, 
			from2, direction2, inter2))
			return false;

		// ��������� ������ inter2-inter1 � ��������� ������������ � � �����
		Math::Vec3f combdir = inter2-inter1;
displayStringD("local = {%f, %f, %f}", combdir.x, combdir.y, combdir.z);
		Math::Matrix4f basis;
		geometry->getBasis(face1, bary1, geometryindex, basis);
		Math::Matrix4f basisinv = basis.inverted();
		combdir = Math::Matrix4As3f( basisinv)*combdir;

//		polarvector = combdir;// out
		Math::Vec2f vpolar = tangentSpace2_2d(combdir);
		float polar = vector2polar(vpolar);
Math::Vec2f t = polar2vector(polar);
displayStringD("{%f, %f} = %f", vpolar.x, vpolar.y, polar);
		
		// �������� � ��������
		Math::Vec2f uv = geometry->getUV(face1, bary1, geometryindex);
		uv.x = uv.x - floor(uv.x);
		uv.y = uv.y - floor(uv.y);
		int x = (int)(uv.x*combdeformer->length.sizeX());
		if(x>=combdeformer->length.sizeX()) x = 0;
		int y = (int)(uv.y*combdeformer->length.sizeY());
		if(y>=combdeformer->length.sizeY()) y = 0;
		
		combdeformer->length[y][x] = value;
//		displayStringD("HairShape::updateTexture {%d, %d}=%f", x, y, combdeformer->polarmap[y][x]);

	}

	setCombDeformer(pCombDeformerData, combdeformer);

	return false;
}

// �������� �������� ������
bool HairShape::updatePolarTexture(
	const Math::Vec3f& from1, const Math::Vec3f& direction1, 
	const Math::Vec3f& bary1, int face1,
	const Math::Vec3f& from2, const Math::Vec3f& direction2, 
	Math::Vec3f& polarvector)
{
	// ���������
	MDataBlock data = forceCache();
	HairGeometryIndices* geometryindex = LoadHairGeometryIndices(data);
	if( !geometryindex) return false;
	HairGeometry* geometry = LoadHairGeometry(data);
	if( !geometry) return false;

	HairDeformerData* pCombDeformerData;
	HairCombDeformer* combdeformer = this->getCombDeformer(pCombDeformerData);
	if( !combdeformer) return false;

	// ���������...
	{
		if(combdeformer->polarmap.empty())
			combdeformer->polarmap.init(32, 32);

		// ����������� 1-�� � 2-�� ���� � ���������� ��-��
		const Math::Face32& face = geometryindex->face(face1);
		const Math::Vec3f& v0 = geometry->vertex(face[0]);
		const Math::Vec3f& v1 = geometry->vertex(face[1]);
		const Math::Vec3f& v2 = geometry->vertex(face[2]);

		Math::Vec3f inter1 = v0*bary1.x + v1*bary1.y + v2*bary1.z;

		Math::Vec3f inter2;
		if( !Math::RayTriPlaneIntersection( v0, v1, v2, 
			from2, direction2, inter2))
			return false;

		// ��������� ������ inter2-inter1 � ��������� ������������ � � �����
		Math::Vec3f combdir = inter2-inter1;
displayStringD("local = {%f, %f, %f}", combdir.x, combdir.y, combdir.z);
		Math::Matrix4f basis;
		geometry->getBasis(face1, bary1, geometryindex, basis);
		Math::Matrix4f basisinv = basis.inverted();
		combdir = Math::Matrix4As3f( basisinv)*combdir;
		polarvector = combdir;// out
		Math::Vec2f vpolar = tangentSpace2_2d(combdir);
		float polar = vector2polar(vpolar);
Math::Vec2f t = polar2vector(polar);
displayStringD("{%f, %f} = %f", vpolar.x, vpolar.y, polar);
		
		// �������� � ��������
		Math::Vec2f uv = geometry->getUV(face1, bary1, geometryindex);
		uv.x = uv.x - floor(uv.x);
		uv.y = uv.y - floor(uv.y);
		int x = (int)(uv.x*combdeformer->polarmap.sizeX());
		if(x>=combdeformer->polarmap.sizeX()) x = 0;
		int y = (int)(uv.y*combdeformer->polarmap.sizeY());
		if(y>=combdeformer->polarmap.sizeY()) y = 0;
		
		combdeformer->polarmap[y][x] = polar;
		displayStringD("HairShape::updateTexture {%d, %d}=%f", x, y, combdeformer->polarmap[y][x]);

	}

	setCombDeformer(pCombDeformerData, combdeformer);

	return false;
}

// ������� CombDeformer (������� ���� ����)
HairCombDeformer* HairShape::getCombDeformer(HairDeformerData* &pCombDeformerData)
{
	// ����� ��� ��������� combDeformer
	pCombDeformerData = findCombDeformer();
	if( !pCombDeformerData)
	{ 
		pCombDeformerData = attachCombDeformer();
		if( !pCombDeformerData)
			return NULL;
	}

	IHairDeformer* hd = (*pCombDeformerData->deformerproc)();
	HairCombDeformer* combdeformer = (HairCombDeformer*)hd;
	if( !combdeformer) return NULL;

	pCombDeformerData->stream.startReading();
	combdeformer->serialise(pCombDeformerData->stream);
	combdeformer->init();
	return combdeformer;
}

// �������� ������ CombDeformer
void HairShape::setCombDeformer(HairDeformerData* pCombDeformerData, HairCombDeformer* combdeformer)
{
	// ������
	pCombDeformerData->stream.open();
	combdeformer->serialise(pCombDeformerData->stream);

	// ��������� ��������������
	{
		MPlug plug(thisMObject(), i_Colorize);
		bool val;
		plug.getValue(val);
		plug.setValue(val);
	}
}

// ��������� combDeformer
HairDeformerData* HairShape::attachCombDeformer()
{
	MDGModifier modifer;
	MObject obj = modifer.createNode(HairDeformer::id);
	modifer.connect(obj, HairDeformer::o_output, thisMObject(), i_HairDeformers);
	modifer.doIt();

	MPlug(obj, HairDeformer::i_Deformer[0]).setValue("HairMath.dll");
	MPlug(obj, HairDeformer::i_Deformer[1]).setValue("getCombDeformer");

//	MGlobal::executeCommand("Hair_AttachCombDeformer "+dn.name());

	return findCombDeformer();
}

// ����� combDeformer
HairDeformerData* HairShape::findCombDeformer()
{
	MStatus stat;
	MDataBlock data = forceCache();
	MArrayDataHandle arh = data.inputArrayValue(i_HairDeformers, &stat);
	if( stat)
	{
		for( unsigned i=0; i<arh.elementCount(); i++)
		{
			if( arh.jumpToArrayElement(i))
			{
				MPxData* pxdata = arh.inputValue(&stat).asPluginData();
				if( stat && pxdata && pxdata->typeId() == HairDeformerData::id)
				{
					HairDeformerData* sndata = (HairDeformerData*)pxdata;
					if( sndata->deformerproc.dll == "HairMath.dll" &&
						sndata->deformerproc.entrypoint == "getCombDeformer")
					{
						return sndata;
					}
				}
			}
		}
	}

	//
	return NULL;
}
/*/

// ��������� TimeStamp
MStatus HairShape::ComputeHairGeometryIndices(const MPlug& plug, MDataBlock& data)
{
	MStatus stat;

	HairGeometryIndicesData* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( i_HairGeometryIndices, &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<HairGeometryIndicesData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( HairGeometryIndicesData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<HairGeometryIndicesData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	int Seed = data.inputValue(i_Seed).asInt();
	int nSubdiv = data.inputValue(i_preSubdivSurface).asInt();
	MString uvSet = data.inputValue(i_uvSetName).asString();
	
	MDataHandle inputData = data.inputValue(i_geometry, &stat);
	MObject component = inputData.data();

	MIntArray brokenEdges;
	if( !pData->Init(component, Seed, nSubdiv, brokenEdges, uvSet))
		return MS::kSuccess;

	this->timeStamp_internal = (int)time(NULL);

	bRebuildRootData = bRebuildClumpData = bRebuildDrawData = bRebuildCVPositions = true;

displayStringD( "ComputeHairGeometryIndices");

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();

	data.outputValue(i_HairTimeStamp).asInt() = this->timeStamp_internal;
	data.setClean(i_HairTimeStamp);

	if( (!::hair_suppressProgress) && MGlobal::mayaState()!=MGlobal::kInteractive)
	{
		fprintf(stderr, "Fur: update TimeStamp in ComputeHairGeometryIndices\n");
		fprintf(stderr, "   new time stamp = %d\n", this->timeStamp_internal);
	}

	return MS::kSuccess;
}

MStatus HairShape::ComputeHairGeometry(const MPlug& plug, MDataBlock& data)
{
	MStatus stat;

	HairGeometryData* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( i_HairGeometry, &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<HairGeometryData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( HairGeometryData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<HairGeometryData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	HairGeometryIndices* hgh = LoadHairGeometryIndices(data);
	if( !hgh) return MS::kFailure;

	MString uvSet = data.inputValue(i_uvSetName).asString();
	MDataHandle inputData = data.inputValue(i_geometry, &stat);
	MObject component = inputData.data();
	pData->Init(component, hgh, uvSet);

displayStringD( "ComputeHairGeometry");

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();

	return MS::kSuccess;
}
/*/
MStatus HairShape::ComputeHairCVPositions(const MPlug& plug, MDataBlock& data)
{
	MStatus stat;
	bRebuildCVPositions = false;

	HairCVPositionsData* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( i_HairCVPositions, &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<HairCVPositionsData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( HairCVPositionsData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<HairCVPositionsData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	HairGeometryIndices* geometryIndices = LoadHairGeometryIndices(data);
	if( !geometryIndices) return MS::kFailure;
	HairGeometry* geometry = LoadHairGeometry(data);
	if( !geometry) return MS::kFailure;

	int resX = data.inputValue(i_CVcountU, &stat).asInt();
	int resY = data.inputValue(i_CVcountV, &stat).asInt();

//	int segs = data.inputValue(i_CVsegmCount, &stat).asInt();
//	if( !segs)
//		resX = resY = 0;	

	pData->Init(Math::Vec2i(resX, resY), *geometry, *geometryIndices);

displayStringD( "ComputeHairCVPositions");

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();

	return MS::kSuccess;
}
/*/
/*/
MStatus HairShape::ComputeHairCV(const MPlug& plug, MDataBlock& data)
{
	MStatus stat;

	HairCVData* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( i_HairCV, &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<HairCVData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( HairCVData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<HairCVData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	HairGeometryIndices* geometryIndices = LoadHairGeometryIndices(data);
	if( !geometryIndices) return MS::kFailure;
	HairGeometry* geometry = LoadHairGeometry(data);
	if( !geometry) return MS::kFailure;
	HairCVPositions* hairCVPositions = LoadHairCVPositions(data);
	if( !hairCVPositions) return MS::kFailure;

	int count = hairCVPositions->getCount();

	Math::Matrix4f wm, wmi;
	{
		data.inputValue(worldMatrix).asMatrix();
		MPlug wmplug(thisMObject(), worldMatrix);
		MObject wmobj;
		wmplug.elementByLogicalIndex(0).getValue(wmobj);
		MMatrix mwm = MFnMatrixData(wmobj).matrix();
		copy(wm, mwm);
		wmi = wm.inverted();
	}

	bool bDynamic = data.inputValue( i_useDynamic).asBool();
	MArrayDataHandle CVCurves = data.inputArrayValue(i_CVCurves, &stat);
	MArrayDataHandle CVDynArrays = data.inputArrayValue(i_CVDynArrays, &stat);

	HairCV& hcv = pData->cv;
	hcv.cvs.clear();
	hcv.cvs.resize(count);

	for(int i=0; i<count; i++)
	{
		// ��������� ������� � TS space
		Math::Matrix4f basis;
		hairCVPositions->getBasis(
			i, geometryIndices, geometry, basis);
		basis.invert();

		HairCV::CV& cv = hcv.cvs[i];
		if( bDynamic)
		{
			if( !CVDynArrays.jumpToElement(i))
				// ����! ��� ���������
				continue;

			MObject obj_va = CVDynArrays.inputValue().data();
			MFnVectorArrayData va(obj_va);
			
			cv.os.resize(va.length());
			for(unsigned v=0; v<va.length(); v++)
			{
				Math::Vec3f pt;
				copy( pt, va[v]);
				pt = wmi*pt;
				cv.os[v] = pt;
			}
		}
		else
		{
			if( !CVCurves.jumpToElement(i))
			{
				// ����! ��� ���������
				continue;
			}
			MObject obj_nurb = CVCurves.inputValue().asNurbsCurve();
			MFnNurbsCurve nurb(obj_nurb);

			MPointArray va;
			nurb.getCVs( va, MSpace::kWorld);
			cv.os.resize(va.length());
			for(unsigned v=0; v<va.length(); v++)
			{
				Math::Vec3f pt;
				copy( pt, va[v]);
				cv.os[v] = wmi*pt;
			}
		}
		cv.ts.resize(cv.os.size());
		cv.vfactor.resize(cv.os.size());
		for(unsigned v=0; v<cv.os.size(); v++)
		{
			// ��� ��������!!!
			cv.ts[v] = basis*cv.os[v];
			cv.vfactor[v] = 0;
			if(v!=0)
				cv.vfactor[v] = (cv.ts[v]-cv.ts[v-1]).length() + cv.vfactor[v-1];
		}
		// �����!!! ��� ������! ������!
		Math::SubdivCurve(2, cv.os);
		Math::SubdivCurve(2, cv.ts);
		Math::SubdivCurve(2, cv.vfactor);
	}

displayStringD( "ComputeHairCV");

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();

	return MS::kSuccess;
}
/*/

MStatus HairShape::ComputeHairClumpPositions(const MPlug& plug, MDataBlock& data)
{
	MStatus stat;
	bRebuildClumpData = false;

	HairClumpPositionsData* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( i_HairClumpPositions, &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<HairClumpPositionsData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( HairClumpPositionsData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<HairClumpPositionsData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}

	HairGeometryIndices* geometryIndices = LoadHairGeometryIndices(data);
	if( !geometryIndices) return MS::kFailure;
	HairGeometry* geometry = LoadHairGeometry(data);
	if( !geometry) return MS::kFailure;

	int resX = data.inputValue(i_RenderResX, &stat).asInt();
	int resY = data.inputValue(i_RenderResY, &stat).asInt();

	MString clumpPositionFile = data.inputValue(i_clumpPositionFile, &stat).asString();
	int CLUMPcount = data.inputValue(i_CLUMPcount, &stat).asInt();
	int CLUMPdensityVersion = data.inputValue(i_CLUMPdensityVersion).asInt();
	double CLUMPdensity = data.inputValue(i_CLUMPdensity).asDouble();
	bool bCLUMP = data.inputValue(i_bCLUMP, &stat).asBool();

	if( !bCLUMP) 
	{
		pData->Init(thisMObject(), geometryIndices, geometry, 0, 1, 1);
	}
	else
	{
		Math::matrixMxN<unsigned char> matrix;
		if( LoadMatrixFromIFF(clumpPositionFile.asChar(), matrix))
		{
			displayStringD("load matrix %dx%d", matrix.sizeX(), matrix.sizeY());
			pData->Init(geometryIndices, geometry, matrix);
		}
		else
		{
			pData->Init(thisMObject(), geometryIndices, geometry, CLUMPcount, resX, resY);
		}
	}

displayStringD( "ComputeHairClumpPositions");

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();

	return MS::kSuccess;

}

// ��������� TimeStamp
MStatus HairShape::ComputeHairTimeStamp(const MPlug& plug, MDataBlock& data)
{
	displayStringD("ComputeHairTimeStamp:");
	MStatus stat;

	HairGeometryIndices* geometryIndices = LoadHairGeometryIndices(data);
	if( !geometryIndices) return MS::kFailure;
	HairGeometry* geometry = LoadHairGeometry(data);
	if( !geometry) return MS::kFailure;
	HairClumpPositions* clumpPositions = LoadHairClumpPositions(data);
	if( !clumpPositions) return MS::kFailure;
	HairCVPositions* cvPositions = LoadHairCVPositions(data);
	if( !cvPositions) return MS::kFailure;

	int genType = data.inputValue(i_generateType).asShort();
	int Count = data.inputValue(i_count).asInt();
	int seed = data.inputValue(i_Seed).asInt();
	int2& regdims = data.inputValue(i_regularDim).asInt2();
	double2& regoffset = data.inputValue(i_regularOffset).asDouble2();
	double2& regshear = data.inputValue(i_regularShear).asDouble2();
	double2& jitter = data.inputValue(i_jitter).asDouble2();
	double HairDensity = data.inputValue(i_HairDensity).asDouble();
	double CLUMPblending = data.inputValue(i_CLUMPblending).asDouble();
	double CLUMPradius = data.inputValue(i_CLUMPradius).asDouble();
	HairRootPositionsData::enClumpAlgorithm algorithm = (HairRootPositionsData::enClumpAlgorithm)data.inputValue(i_CLUMPalgorithm).asShort();
	
	int resX = data.inputValue(i_RenderResX, &stat).asInt();
	int resY = data.inputValue(i_RenderResY, &stat).asInt();

	MDataHandle outputData = data.outputValue( plug, &stat );
	time_t currenttime = time(NULL);
	outputData.set( (int)currenttime );
	outputData.setClean();
	displayStringD("ComputeHairTimeStamp: %d", (int)currenttime);

	if( (!::hair_suppressProgress) && MGlobal::mayaState()!=MGlobal::kInteractive)
	{
		fprintf(stderr, "Fur: update TimeStamp in ComputeHairTimeStamp\n");
		fprintf(stderr, "   new time stamp = %d\n", currenttime);
	}

	return MS::kSuccess;
}

/*/
MStatus HairShape::ComputeHairRootPositions(const MPlug& plug, MDataBlock& data)
{
	MStatus stat;
	bRebuildRootData = false;

	MDataHandle outputData = data.outputValue( i_HairRootPositions, &stat );
	outputData.set(MString(""));

	HairGeometryIndices* geometryIndices = LoadHairGeometryIndices(data);
	if( !geometryIndices) return MS::kFailure;
	HairGeometry* geometry = LoadHairGeometry(data);
	if( !geometry) return MS::kFailure;
	HairClumpPositions* clumpPositions = LoadHairClumpPositions(data);
	if( !clumpPositions) return MS::kFailure;
	HairCVPositions* cvPositions = LoadHairCVPositions(data);
	if( !cvPositions) return MS::kFailure;

	int genType = data.inputValue(i_generateType).asShort();
	int Count = data.inputValue(i_count).asInt();
	int seed = data.inputValue(i_Seed).asInt();
	int2& regdims = data.inputValue(i_regularDim).asInt2();
	double2& regoffset = data.inputValue(i_regularOffset).asDouble2();
	double2& regshear = data.inputValue(i_regularShear).asDouble2();
	double2& jitter = data.inputValue(i_jitter).asDouble2();
//	int HairDensityVersion = data.inputValue(i_HairDensityVersion).asInt();
	double HairDensity = data.inputValue(i_HairDensity).asDouble();
	double CLUMPblending = data.inputValue(i_CLUMPblending).asDouble();
	
	int resX = data.inputValue(i_RenderResX, &stat).asInt();
	int resY = data.inputValue(i_RenderResY, &stat).asInt();

	// File name
	std::string filename = MFileIO::currentFile().asChar();
	filename += ".";
	filename += MFnDagNode(thisMObject()).name().asChar();
	filename += ".hair";

	HairRootPositions hp;
	if( genType==0)
	{
		HairRootPositionsData::Init(hp, thisMObject(), 
			clumpPositions, geometryIndices, geometry, 
			Count, seed, 
			resX, resY, (float)CLUMPblending, true);
	}
	else
	{
		HairRootPositionsData::Init(hp, thisMObject(), 
			clumpPositions, geometryIndices, geometry, 
			Math::Vec2i(regdims[0], regdims[1]), 
			Math::Vec2f((float)regoffset[0], (float)regoffset[1]),
			Math::Vec2f((float)regshear[0], (float)regshear[1]),
			Math::Vec2f((float)jitter[0], (float)jitter[1]), seed, -1, 
			resX, resY, (float)CLUMPblending, false);
	}
	time_t currenttime = time(NULL);

	// save 
	{
		bool bProgress = MGlobal::mayaState()==MGlobal::kInteractive;

		if( bProgress)
		{
			MProgressWindow::reserve();
			MProgressWindow::setInterruptable(false);
			MProgressWindow::setTitle("Hair file cache");
			MProgressWindow::startProgress();
			MProgressWindow::setProgressRange(0, 100);
			MProgressWindow::setProgressStatus("save to file");
			MProgressWindow::setProgress(100);
		}

		try
		{
			Util::FileStream file;
			file.open( filename.c_str(), true);
			file >> currenttime;

			// save geometryIndices & clumpPositions & rootPositions
			geometryIndices->serialize(file);
			cvPositions->serialize(file);
			clumpPositions->serialize(file);
			hp.serialize(file);
		}
		catch(...)
		{
			displayStringError("HairRootPositionsData: Exception when save file");
			throw;
		}
		if( bProgress)
			MProgressWindow::endProgress();
	}


	// ser
	MTypeId tmpid( HairFixedFileData::id );
	MFnPluginData fnDataCreator;
	MObject newDataObject = fnDataCreator.create( tmpid, &stat );
	HairFixedFileData* pData = static_cast<HairFixedFileData*>(fnDataCreator.data( &stat ));
	pData->filename = filename;
	pData->timestamp = currenttime;
	outputData.set( pData);
	outputData.setClean();

	return MS::kSuccess;
}
/*/

MStatus HairShape::ComputeHairDrawPlacement(const MPlug& plug, MDataBlock& data)
{
	MStatus stat;

	bRebuildDrawData = false;
	// i_hairDrawPlacement
	HairRootPositionsData* pData = NULL;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( plug, &stat );
	MPxData* userdata = outputData.asPluginData();
	pData = static_cast<HairRootPositionsData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( HairRootPositionsData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<HairRootPositionsData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}


	HairGeometryIndices* geometryIndices = LoadHairGeometryIndices(data);
	if( !geometryIndices) return MS::kFailure;
	HairGeometry* geometry = LoadHairGeometry(data);
	if( !geometry) return MS::kFailure;
	HairClumpPositions* clumpPositions = LoadHairClumpPositions(data);
	if( !clumpPositions) return MS::kFailure;

	std::set<int> workingfaces;
	LoadHairWorkingFaces(data, workingfaces);

	int genType = data.inputValue(i_generateType).asShort();
	double HairDensity = data.inputValue(i_HairDensity).asDouble();
	data.inputValue(i_HairDensityUvSet).asString();
	int Count = data.inputValue(i_count).asInt();
	int seed = data.inputValue(i_Seed).asInt();
	int2& regdims = data.inputValue(i_regularDim).asInt2();
	double2& regoffset = data.inputValue(i_regularOffset).asDouble2();
	double2& regshear = data.inputValue(i_regularShear).asDouble2();
	double2& jitter = data.inputValue(i_jitter).asDouble2();
	int resX = data.inputValue(i_RenderResX, &stat).asInt();
	int resY = data.inputValue(i_RenderResY, &stat).asInt();
	double CLUMPblending = data.inputValue(i_CLUMPblending).asDouble();
	double CLUMPradius = data.inputValue(i_CLUMPradius).asDouble();
	HairRootPositionsData::enClumpAlgorithm algorithm = (HairRootPositionsData::enClumpAlgorithm)data.inputValue(i_CLUMPalgorithm).asShort();

	enDrawMode drawMode = (enDrawMode)data.inputValue(i_drawMode).asShort();
	int maxHairDrawable = data.inputValue(i_maxHairDrawable, &stat).asInt();

	pData->hp.clear();
	switch(drawMode)
	{
		case DM_HAIRS:
		case DM_INSTANCER:
		case DM_INSTANCERBOX:
			{
				if( genType==0)
				{
//					if( maxHairDrawable<=Count)
//						Count = maxHairDrawable;
					bool bEqualize = false;
					pData->Init(pData->hp, thisMObject(), clumpPositions, geometryIndices, geometry, 
						maxHairDrawable, bEqualize, Count, seed, 
						resX, resY, (float)CLUMPblending, true, (float)CLUMPradius, algorithm, workingfaces);
				}
				else
				{
					pData->Init(pData->hp, thisMObject(), clumpPositions, geometryIndices, geometry, 
						Math::Vec2i(regdims[0], regdims[1]), 
						Math::Vec2f((float)regoffset[0], (float)regoffset[1]),
						Math::Vec2f((float)regshear[0], (float)regshear[1]),
						Math::Vec2f((float)jitter[0], (float)jitter[1]), seed, maxHairDrawable, 
						resX, resY, (float)CLUMPblending, true, (float)CLUMPradius, algorithm, workingfaces);
//					displayStringD( "COUNT=%d", pData->hp.getCount());
				}
				break;
			}
		case DM_CLUMPS:
			{
				pData->InitFromClumps(
					pData->hp, clumpPositions, 
					geometryIndices, geometry, 
					maxHairDrawable, workingfaces);
				break;
			}
		case DM_NONE:
		case DM_DYNAMIC_CURVES:
		case DM_BOX:
			// ���� �� ������
			break;
	}

	displayStringD( "ComputeHairDrawPlacement");

	if( bNewData)
		outputData.set(pData);
	outputData.setClean();

	return MS::kSuccess;
}

MStatus HairShape::ComputeTextureCacheV(const MPlug& dst, const MPlug& src, MDataBlock& data)
{
	displayStringD("HairShape::ComputeTextureCache %s", dst.name().asChar());
	MStatus stat;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( dst, &stat );
	MPxData* userdata = outputData.asPluginData();
	textureCacheData* pData = static_cast<textureCacheData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( textureCacheData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<textureCacheData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}
	int resX = data.inputValue(i_PaintAttrResX, &stat).asInt();
	int resY = data.inputValue(i_PaintAttrResY, &stat).asInt();

	MFnNumericAttribute attr( src.attribute());
	if( attr.unitType()==MFnNumericData::kDouble)
		pData->setFloats(src, data, resX, resY, 0, "");
	else
		pData->setVectors(src, data, resX, resY);


	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}

MStatus HairShape::ComputeTextureCache(MObject attr[3], MDataBlock& data)
{
	MStatus stat;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( attr[1], &stat );
	MPxData* userdata = outputData.asPluginData();
	textureCacheData* pData = static_cast<textureCacheData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( textureCacheData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<textureCacheData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}
	int resX = data.inputValue(i_PaintAttrResX, &stat).asInt();
	int resY = data.inputValue(i_PaintAttrResY, &stat).asInt();

	MPlug src(thisMObject(), attr[0]);
	displayStringD("HairShape::ComputeTextureCache for %s", src.name().asChar());

	MFnNumericAttribute numattr( attr[0]);
	if( numattr.unitType()==MFnNumericData::kDouble)
	{
		double jitter = data.inputValue(attr[2]).asDouble();
		pData->setFloats(src, data, resX, resY, jitter, "");
	}
	else
		pData->setVectors(src, data, resX, resY);


	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}
MStatus HairShape::ComputeTextureCache4(MObject attr[4], MDataBlock& data)
{
	MStatus stat;
	bool bNewData = false;
	MDataHandle outputData = data.outputValue( attr[1], &stat );
	MPxData* userdata = outputData.asPluginData();
	textureCacheData* pData = static_cast<textureCacheData*>(userdata);
	MFnPluginData fnDataCreator;
	if( !pData)
	{
		MTypeId tmpid( textureCacheData::id );
		MObject newDataObject = fnDataCreator.create( tmpid, &stat );
		pData = static_cast<textureCacheData*>(fnDataCreator.data( &stat ));
		bNewData = true;
	}
	int resX = data.inputValue(i_PaintAttrResX, &stat).asInt();
	int resY = data.inputValue(i_PaintAttrResY, &stat).asInt();

	MPlug src(thisMObject(), attr[0]);
	displayStringD("HairShape::ComputeTextureCache4 for %s", src.name().asChar());

	MFnNumericAttribute numattr( attr[0]);
	if( numattr.unitType()==MFnNumericData::kDouble)
	{
		double jitter = data.inputValue(attr[2]).asDouble();
		MString uvsetname = data.inputValue(attr[3]).asString();
		pData->setFloats(src, data, resX, resY, jitter, uvsetname.asChar());
	}
	else
		pData->setVectors(src, data, resX, resY);


	if( bNewData)
		outputData.set(pData);
	outputData.setClean();
	return MS::kSuccess;
}

/*/
MStatus HairShape::ComputeCVTransform(const MPlug& plug, MDataBlock& data)
{
	MStatus stat;
displayStringD("ComputeCVTransform");
	HairGeometryIndices* geometryIndices = LoadHairGeometryIndices(data);
	if( !geometryIndices) return MS::kFailure;
	HairGeometry* geometry = LoadHairGeometry(data);
	if( !geometry) return MS::kFailure;
	HairCVPositions* hairCVPositions = LoadHairCVPositions(data);
	if( !hairCVPositions) return MS::kFailure;

	textureCacheData* length = (textureCacheData*)data.inputValue( i_HairLength[1]).asPluginData();
	if( !length) return MS::kFailure;

	int count = hairCVPositions->getCount();
	MArrayDataHandle dh = data.outputArrayValue(o_CVTransform);

	MArrayDataBuilder oldbuilder = dh.builder(&stat);
	MArrayDataBuilder builder( oldbuilder);
	for(int i=0; i<count; i++)
	{
		MDataHandle rec = builder.addElement(i);
		Math::Matrix4f matrix;
		hairCVPositions->getBasis(i, geometryIndices, geometry, matrix);
		Math::Vec2f uv = hairCVPositions->getUV(i, geometryIndices, geometry);

//		displayStringD("+OrthoBasis %d {%f, %f}", i, uv.x, uv.y);
//		displayMatrix(matrix);

		// ������ �����!
		float l = length->paramTex2d.getValueSource(uv.x, uv.y);

		MMatrix m; copy(m, matrix);
		{
			MTransformationMatrix mtm(m);
			double sc[3], sh[3];
			stat = mtm.getScale(sc, MSpace::kObject);
			MEulerRotation rot = mtm.eulerRotation();
			stat = mtm.getShear(sh, MSpace::kObject);

			MVector tr = mtm.translation(MSpace::kTransform, &stat);

			rec.child(o_CVtranslate).set(MFloatVector(tr));

			float factor = (float)MAngle( 1, MAngle::uiUnit()).asRadians();
			MFloatVector r( (float)rot.x/factor, (float)rot.y/factor, (float)rot.z/factor); 
			rec.child(o_CVrotate).set(r);

			// ������ �����!
			MFloatVector s( (float)sc[0]*l, (float)sc[1]*l, (float)sc[2]*l);
			rec.child(o_CVscale).set(s);

			MFloatVector shi( (float)sh[0], (float)sh[1], (float)sh[2]);
			rec.child(o_CVshear).set(shi);
		}
	}
	dh.set(builder);
	dh.setAllClean();
	return MS::kSuccess;
}
/*/

MStatus HairShape::ComputeOutput( const MPlug& plug, MDataBlock& data, const char* type)
{
	MArrayDataHandle dah = data.inputArrayValue( this->worldMatrix);
	dah.jumpToElement(0);
	dah.inputValue().asMatrix();

	MPlug wmplug(thisMObject(), this->worldMatrix);
	MObject wmobj;
	wmplug.elementByLogicalIndex(0).getValue(wmobj);
	MMatrix mwm = MFnMatrixData(wmobj).matrix();
	Math::Matrix4f wm;
	copy(wm, mwm);
	Math::Matrix4As3f wm3(wm);

	static misc::switch_string::Token semslist[] = {
		{"position",			0}, 
		{"normal",				1}, 
		{"tangentU",			2}, 
		{"tangentV",			3},
		{"hairvector",			4},
		{NULL, 		-1}};
	static misc::switch_string sems(semslist);
	int code = sems.match(type);
	if(code==-1)
		return MS::kFailure;
	
	MStatus stat;
	displayStringD("ComputeOutput %s type+%s", plug.name().asChar(), type);

	int maxHairDrawable = data.inputValue(i_maxHairDrawable, &stat).asInt();

	HairGeometryIndices* geometryIndices = LoadHairGeometryIndices(data);
	if( !geometryIndices) return MS::kFailure;
	HairGeometry* hairgeometry = LoadHairGeometry(data);
	if( !hairgeometry) return MS::kFailure;
	HairClumpPositions* clumpPositions = LoadHairClumpPositions(data);
	if( !clumpPositions) return MS::kFailure;
	HairRootPositions* rootPositions = LoadHairDrawPlacement(data);
	if( !rootPositions) return MS::kFailure;
	HairCVPositions* cvposition = LoadHairCVPositions(data);
	if( !cvposition) return MS::kFailure;
	HairCV* cvs = LoadHairCV(data);
	if( !cvs) return MS::kFailure;
	HairAlive* ha = LoadHairAlive(data);

	HairSystem hairsystem;
	HairShape::LoadHairSystem(data, hairsystem);

	int resX = data.inputValue(i_PaintAttrResX, &stat).asInt();
	int resY = data.inputValue(i_PaintAttrResY, &stat).asInt();
	MeshSurfaceSynk meshsurface;
	meshsurface.init(geometryIndices, hairgeometry, clumpPositions, rootPositions, cvposition, cvs, &hairsystem);
	if( !HairShape::LoadMeshSurface(meshsurface, data, thisMObject()))
		return MS::kFailure;

	// deformer
	meshsurface.InitDeformers();
	meshsurface.initParamsUVsetIndices();


	HairProcessor processor(
		hairsystem.SegmCount, 
		meshsurface, 
		hairsystem.snshapes, 
		hairsystem.ocsfiles, 
		hairsystem.snshapes_weight, 
		hairsystem.mulWidthByLength, hairsystem.orthogonalize, false);
	int count = rootPositions->getCount();

	HAIRPARAMS params;
	HAIRVERTS hair;
	std::vector<Math::Vec3f> out;
	out.reserve(count);
	for(int i=0; i<count; i++)
	{
		if( ha && !(*ha)[i]) continue;
		processor.BuildHair(i, params, hair);
		if( !hair.vc()) continue;

		Math::Matrix4f toos;
		params.getBasis(toos);

		Math::Matrix4f basis = hair.basas[0];
		basis = basis*toos;
		Math::Vec3f v(0);

		switch(code)
		{
		case 0:
			v = wm*basis[3].as<3>();break;
		case 1:
			v = wm3*basis[1].as<3>();break;
		case 2:
			v = wm3*basis[0].as<3>();break;
		case 3:
			v = wm3*basis[2].as<3>();break;
		case 4:
			{
				// ����������� �� ������� �������� � ����������
				Math::Vec3f dir = hair.cvint.back() - hair.cvint.front();
				v = wm3*dir;
				break;
			}
		}
		out.push_back(Math::Vec3f(v.x, v.y, v.z));
	}
	MObject obj = MObject::kNullObj;
	switch(code)
	{
	case 0:
/*/
		{
			MPointArray pa(out.size());
			for(i=0; i<(int)out.size(); i++)
			{
				::copy( pa[i], out[i]);
			}
			MFnPointArrayData pad;
			obj = pad.create(pa);
		}
		break;
/*/
	case 1:
	case 2:
	case 3:
	case 4:
		{
			int i;
			MVectorArray pa( (int)out.size());
			for(i=0; i<(int)out.size(); i++)
			{
				::copy( pa[i], out[i]);
			}
			MFnVectorArrayData pad;
			obj = pad.create(pa);
		}
		break;
	}

	MDataHandle outputData = data.outputValue(plug, &stat);
	outputData.set(obj);
	outputData.setClean();
	return MS::kSuccess;
}

MStatus HairShape::ComputeOcsFrameFile( const MPlug& plug, MDataBlock& data)
{
	MStatus stat;
	displayStringD("HairShape::ComputeOcsFrameFile %s", plug.name().asChar());

	MDataHandle outputData = data.outputValue(plug, &stat);

	std::string filename="ooooo.ocs";
	bool bAscii = true;

	{
		double time = MAnimControl::currentTime().as(MTime::uiUnit());
		char buf[24];
		sprintf(buf, "%.2g", time);
		std::string relative_filename;
		relative_filename += ".HairMR//";
		std::string scenefilename = MFileIO::currentFile().asChar();
		std::string mbname = Util::extract_filename(scenefilename.c_str());
		relative_filename += mbname;
		relative_filename += ".";
		std::string dgNodeName = MFnDependencyNode(thisMObject()).name().asChar();
		Util::correctFileName( (char*)dgNodeName.c_str());
		relative_filename += dgNodeName;
		relative_filename += ".";
		relative_filename += buf;
		relative_filename += ".ocs";
		filename = scenefoldername()+relative_filename;
	}

	Util::create_directory_for_file(filename.c_str());
	cls::renderFileImpl filerender;
	if( !filerender.openForWrite(filename.c_str(), bAscii))
	{
		displayStringError("Export: renderFileImpl cant create \"%s\" file!!!", filename.c_str());
		filename = "";
		outputData.set(MString(filename.c_str()));
		outputData.setClean();
		return MS::kSuccess;
	}
	cls::IRender* render = &filerender;

	Math::Box3f box;
	cls::IExportContext* context = ocNewExportContext();
	cls::INode* pnode = context->addNode(thisMObject());
	hairOcsGenerator.OnGeometry(
		*pnode,				//!< ������ 
		render, 
		box, 
		context,
		MObject::kNullObj
		);
	ocDeleteExportContext(context);

	outputData.set(MString(filename.c_str()));
	outputData.setClean();
	return MS::kSuccess;
}


MStatus HairShape::compute( const MPlug& plug, MDataBlock& data )
{
	displayStringD("HairShape::compute %s", plug.name().asChar());
	MStatus stat = MS::kUnknownParameter;

	try
	{
//		if(plug == o_CVTransform || plug.parent()==o_CVTransform)
//			return ComputeCVTransform(plug, data);
		if(plug == o_buildMessage)
			return ComputeBuildMessage(plug, data);
		if( plug == i_HairGeometryIndices)
			return ComputeHairGeometryIndices(plug, data);
		if( plug == i_HairGeometry)
			return ComputeHairGeometry(plug, data);
//		if( plug == i_HairCVPositions)
//			return ComputeHairCVPositions(plug, data);
//		if( plug == i_HairCV)
//			return ComputeHairCV(plug, data);
		if( plug == i_HairClumpPositions)
			return ComputeHairClumpPositions(plug, data);
		if( plug == i_HairTimeStamp)
			return ComputeHairTimeStamp(plug, data);
//		if( plug == i_HairRootPositions)
//			return ComputeHairRootPositions(plug, data);
		if( plug == i_ocsFrameFile)
			return ComputeOcsFrameFile(plug, data);
		if( plug == i_hairDrawPlacement)
			return ComputeHairDrawPlacement(plug, data);
		if( plug == i_CLUMPdensityVersion)
		{
			int cur = data.outputValue(plug).asInt();
			data.inputValue(i_CLUMPdensity).asDouble();
			data.outputValue(plug).set(cur+1);
			return MS::kSuccess;
		}
		if( plug == i_HairLength[1])
			return ComputeTextureCache4(i_HairLength, data);
		if( plug == i_HairInclination[1])
			return ComputeTextureCache4(i_HairInclination, data);
		if( plug == i_HairPolar[1])
			return ComputeTextureCache4(i_HairPolar, data);
		if( plug == i_HairBaseCurl[1])
			return ComputeTextureCache4(i_HairBaseCurl, data);
		if( plug == i_HairTipCurl[1])
			return ComputeTextureCache4(i_HairTipCurl, data);
		if( plug == i_HairScraggle[1])
			return ComputeTextureCache4(i_HairScraggle, data);
		if( plug == i_HairScraggleFreq[1])
			return ComputeTextureCache4(i_HairScraggleFreq, data);
		if( plug == i_HairTwist[1])
			return ComputeTextureCache4(i_HairTwist, data);
		if( plug == i_HairDisplace[1])
			return ComputeTextureCache4(i_HairDisplace, data);
		if( plug == i_HairTwistFromPolar[1])
			return ComputeTextureCache4(i_HairTwistFromPolar, data);

		if( plug == i_blendCVfactor[1])
			return ComputeTextureCache4(i_blendCVfactor, data);
		if( plug == i_blendDeformfactor[1])
			return ComputeTextureCache4(i_blendDeformfactor, data);

		if( plug == i_HairBaseWidth[1])
			return ComputeTextureCache4(i_HairBaseWidth, data);
		if( plug == i_HairTipWidth[1])
			return ComputeTextureCache4(i_HairTipWidth, data);
		if( plug == i_HairBaseClump[1])
			return ComputeTextureCache4(i_HairBaseClump, data);
		if( plug == i_HairTipClump[1])
			return ComputeTextureCache4(i_HairTipClump, data);

		if( plug == i_BaseColorCache)
			return ComputeTextureCacheV(plug, MPlug(thisMObject(), i_BaseColor), data);
		if( plug == i_TipColorCache)
			return ComputeTextureCacheV(plug, MPlug(thisMObject(), i_TipColor), data);

		if( plug.array() == o_positions)
			return ComputeOutput( plug, data, "position");
		if( plug.array() == o_hairVector)
			return ComputeOutput( plug, data, "hairvector");
		if( plug.array() == o_normals)
			return ComputeOutput( plug, data, "normal");
		if( plug.array() == o_tangentUs)
			return ComputeOutput( plug, data, "tangentU");
		if( plug.array() == o_tangentVs)
			return ComputeOutput( plug, data, "tangentV");
	}
	catch(std::string err)
	{
		MGlobal::displayError(plug.name().asChar());
		MGlobal::displayError(err.c_str());
	}
	catch(...)
	{
		MGlobal::displayError(plug.name().asChar());
		MGlobal::displayError("v0.0 HairShape::compute. Exception!!!");
	}
	return stat;
}
MStatus HairShape::connectionBroken( 
	const MPlug& plug,
	const MPlug& otherPlug,
	bool asSrc )
{
	MStatus stat;
	if( !asSrc && plug==i_HairAlive)
	{
		MDataBlock data = forceCache();
		MDataHandle outputData = data.outputValue( i_HairCV, &stat );
		outputData.set(NULL);
		outputData.setClean();
	}
	return MPxSurfaceShape::connectionBroken( plug, otherPlug, asSrc );
}

bool HairShape::getInternalValue( const MPlug& plug, MDataHandle& data)
{
	if( plug==i_HairTimeStamp)
	{
		data.asInt() = timeStamp_internal;
		return true;
	}
	return MPxSurfaceShape::getInternalValue( plug, data);
}
bool HairShape::setInternalValue( const MPlug& plug, const MDataHandle& data)
{
	if( plug==i_HairTimeStamp)
	{
		timeStamp_internal = data.asInt();
		return true;
	}
	return MPxSurfaceShape::setInternalValue( plug, data);
}

MStatus	HairShape::setDependentsDirty( const MPlug& plug, MPlugArray & affected)
{
	displayStringD("setDependentsDirty %s", plug.name().asChar());

	/*/
	MDataBlock db = this->forceCache();
	MPlug pl(thisMObject(), i_Colorize);
	if( db.isClean(pl))
	{
		bool b;
		pl.getValue(b);
		pl.setValue(b);
	}
	/*/
	return MS::kSuccess;
}

MStatus HairShape::shouldSave( const MPlug& plug, bool& result )
{
//	displayString("HairShape::shouldSave %s", plug.name().asChar());

	if( plug==i_HairTimeStamp)
	{
		displayStringD("HairShape::shouldSave %s", plug.name().asChar());
		int val;
		plug.getValue(val);
		result = true;
		return MS::kSuccess;
	}
	return MPxSurfaceShape::shouldSave( plug, result );
}


// BOX
bool HairShape::isBounded() const 
{
	return bValidBox;
}
MBoundingBox HairShape::boundingBox() const
{
	return box;
}




// INITIALIZE
MStatus HairShape::initialize()
{ 
	MStatus stat;	
	MFnTypedAttribute _Input;
	MFnNumericAttribute numAttr;
	MFnGenericAttribute genAttr;
	MFnTypedAttribute typedAttr;
	MFnCompoundAttribute comAttr;
	MFnEnumAttribute enumAttr;
	MFnUnitAttribute unitAttr;
	MFnStringData stringData;

	try
	{
	// Output
	{
		o_buildMessage = numAttr.create ("buildMessage","bm", MFnNumericData::kDouble, 0.0,&stat);
		if (!stat) return stat;
		numAttr.setStorable(false);
		numAttr.setCached(false);
		numAttr.setHidden(true);
		numAttr.setReadable(false);
		numAttr.setWritable(false);
		stat = addAttribute (o_buildMessage);	
		if (!stat) return stat;

		i_HairRootPositionFile = typedAttr.create( "fileName", "fn",
			MFnData::kString, stringData.create(&stat), &stat );
		typedAttr.setWritable(true);
		typedAttr.setStorable(true);
		typedAttr.setHidden(false);
//		typedAttr.setInternal(true);
		stat = addAttribute(i_HairRootPositionFile);

		i_ocsFrameFile = typedAttr.create( "ocsFrameFile", "ocsff",
			MFnData::kString, stringData.create(&stat), &stat );
		::addAttribute(i_ocsFrameFile, atReadable|atUnWritable|atConnectable|atUnCached|atHidden|atUnStorable);
	}

	// Geometry
	bool bStorableAttributes = true;
	{
		i_geometry = genAttr.create( "Input", "in", &stat);
		genAttr.addAccept( MFnData::kDynSweptGeometry);
		genAttr.addAccept( MFnData::kMesh);
		genAttr.setStorable(false);
		genAttr.setConnectable(true);
		genAttr.setHidden(false);
		genAttr.setReadable(true);
		stat = addAttribute (i_geometry);

		i_HairGeometryIndices = typedAttr.create( "HairGeometryIndices", "hgh", HairGeometryIndicesData::id);
		typedAttr.setStorable(bStorableAttributes);
		typedAttr.setReadable(true);
		typedAttr.setWritable(true);
		typedAttr.setHidden(true);
		typedAttr.setConnectable(true);
		stat = addAttribute (i_HairGeometryIndices);
		if (!stat) return stat;

		i_HairGeometry = typedAttr.create( "HairGeometry", "hge", HairGeometryData::id);
		typedAttr.setStorable(bStorableAttributes);
		typedAttr.setReadable(true);
		typedAttr.setWritable(true);
		typedAttr.setHidden(true);
		typedAttr.setConnectable(true);
		stat = addAttribute (i_HairGeometry);
		if (!stat) return stat;

		i_HairCVPositions = typedAttr.create( "hairCVPositions", "hcvp", HairCVPositionsData::id);
		typedAttr.setStorable(false);
		typedAttr.setReadable(true);
		typedAttr.setWritable(true);
		typedAttr.setHidden(true);
		typedAttr.setConnectable(true);
		typedAttr.setDisconnectBehavior(  MFnAttribute::kReset);
		stat = addAttribute (i_HairCVPositions);
		if (!stat) return stat;

		i_HairCV = typedAttr.create( "hairCV", "hcv", HairCVData::id);
		typedAttr.setStorable(false);
		typedAttr.setReadable(true);
		typedAttr.setWritable(true);
		typedAttr.setHidden(true);
		typedAttr.setConnectable(true);
		typedAttr.setDisconnectBehavior( MFnAttribute::kReset);
		stat = addAttribute (i_HairCV);
		if (!stat) return stat;

		i_HairClumpPositions = typedAttr.create( "hairClumps", "hcs", HairClumpPositionsData::id);
		typedAttr.setStorable(bStorableAttributes);
		typedAttr.setReadable(true);
		typedAttr.setWritable(true);
		typedAttr.setHidden(true);
		typedAttr.setConnectable(false);
		stat = addAttribute (i_HairClumpPositions);
		if (!stat) return stat;
		
		i_HairTimeStamp = numAttr.create("HairTimeStamp", "htst", MFnNumericData::kInt, 0, &stat);
//		numAttr.setInternal(true);
		::addAttribute(i_HairTimeStamp, atReadable|atStorable|atCached|atUnConnectable);

		/*/
		i_HairRootPositions = typedAttr.create( "HairRootPositions", "hpl", HairFixedFileData::id);
		typedAttr.setStorable(false);
		typedAttr.setReadable(true);
		typedAttr.setWritable(true);
		typedAttr.setHidden(true);
		typedAttr.setConnectable(false);
		stat = addAttribute (i_HairRootPositions);
		if (!stat) return stat;
		/*/

		i_hairDrawPlacement = typedAttr.create( "hairDrawPlacement", "hdpl", HairRootPositionsData::id);
		typedAttr.setReadable(true);
		typedAttr.setWritable(true);
		typedAttr.setCached(true);
		typedAttr.setStorable(bStorableAttributes);
		typedAttr.setHidden(true);
		typedAttr.setConnectable(true);
		stat = addAttribute(i_hairDrawPlacement);

		i_HairAlive = typedAttr.create( "HairAliveData", "ha", HairAliveData::id);
		::addAttribute( i_HairAlive, atReadable|atUnStorable|atUnCached|atHidden|atConnectable);


		i_HairRenderPasses = typedAttr.create( "HairRenderPasses", "hrpss", HairRenderPassData::id, MObject::kNullObj, &stat);
		typedAttr.setWritable(true);
		typedAttr.setReadable(false);
		typedAttr.setArray(true);
		typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
		typedAttr.setIndexMatters(false);
		::addAttribute(i_HairRenderPasses, 
			atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atHidden|atArray);
	}


	// Hair painting & caching
	{
		i_PaintAttrResX = numAttr.create("PaintAttrResX","parx", MFnNumericData::kInt, 16, &stat);
		if (!stat) return stat;
		numAttr.setReadable(true);
		numAttr.setWritable(true);
		numAttr.setStorable (true);
		numAttr.setHidden(false);
		numAttr.setMin(2.0);
		numAttr.setMax(1024.0);
		stat = addAttribute (i_PaintAttrResX);	
		if (!stat) return stat;

		i_PaintAttrResY = numAttr.create("PaintAttrResY","pary", MFnNumericData::kInt, 16, &stat);
		if (!stat) return stat;
		numAttr.setReadable(true);
		numAttr.setWritable(true);
		numAttr.setStorable (true);
		numAttr.setHidden(false);
		numAttr.setMin(2.0);
		numAttr.setMax(1024.0);
		stat = addAttribute (i_PaintAttrResY);
		if (!stat) return stat;
	}

	// �����������
	{
		i_preSubdivSurface = numAttr.create ("preSubdivSurface","prsds", MFnNumericData::kInt, 0, &stat);
		numAttr.setMin(0.0);
		numAttr.setSoftMax(2);	
		numAttr.setMax(10);	
		::addAttribute(i_preSubdivSurface, atInput);

		i_uvSetName = typedAttr.create ("hairUVSetName", "huvsn", MFnData::kString, &stat);
		::addAttribute(i_uvSetName, atInput);

	}

	// ��������� �����
	{
		i_generateType = enumAttr.create ("hairGenerateType", "hgt");
		enumAttr.addField("CASUALS", 0);
		enumAttr.addField("UV REGULAR", 1);
		::addAttribute(i_generateType, atInput);

		i_HairDensity = numAttr.create ("Density","dns", MFnNumericData::kDouble, 1,&stat);
		numAttr.setMin(0.0);	
		numAttr.setSoftMax(1);	
		::addAttribute(i_HairDensity, atInput);

		i_HairDensityUvSet = typedAttr.create ("densityUVset", "dnsuv", MFnData::kString, &stat);
		::addAttribute(i_HairDensityUvSet, atInput);

		// �������������� ��� ���������
		{
			i_count = numAttr.create ("count","hc", MFnNumericData::kInt, 100,&stat);
			numAttr.setSoftMin(1);	
			numAttr.setSoftMax(1000000);	
			::addAttribute(i_count, atInput|atKeyable);	

			i_Seed = numAttr.create ("globalSeed","gS",MFnNumericData::kInt, 0,&stat);
			numAttr.setMin(0.0);
			numAttr.setMax(1000.0);
			::addAttribute(i_Seed, atInput|atKeyable);	
		}
		// ���������� ��� ���������
		{
			i_regularDim = numAttr.create("regularDim","hrd",MFnNumericData::k2Int, 20, &stat);
			numAttr.setMin(1, 1);
			numAttr.setDefault(10, 10);
			::addAttribute(i_regularDim, atInput|atKeyable);	

			i_regularOffset = numAttr.create ("regularOffset","hrof",MFnNumericData::k2Double, 0.0,&stat);
			numAttr.setDefault(0.0, 0.0);
			numAttr.setMin(0.0, 0.0);
			numAttr.setMax(1.0, 1.0);
			::addAttribute(i_regularOffset, atInput|atKeyable);	
			
			i_regularShear = numAttr.create ("regularShear","hrsh",MFnNumericData::k2Double, 0.0,&stat);
			numAttr.setDefault(0.0, 0.0);
			numAttr.setMin(0.0, 0.0);
			numAttr.setMax(1.0, 1.0);
			::addAttribute(i_regularShear, atInput|atKeyable);	
				

			i_jitter = numAttr.create ("jitter","jt",MFnNumericData::k2Double, 0.0,&stat);
			numAttr.setDefault(0.0, 0.0);
			numAttr.setMin(0.0, 0.0);
			numAttr.setMax(1.0, 1.0);
			::addAttribute(i_jitter, atInput|atKeyable);
		}
	}



	// Width
	{
		i_HairRenderWidth = numAttr.create ("RenderWidth","rW",MFnNumericData::kDouble, 0.02,&stat);
		if (!stat) return stat;
		numAttr.setStorable (true);
		numAttr.setKeyable(true);
		numAttr.setHidden(false);
		numAttr.setSoftMin(0);	
		numAttr.setSoftMax(0.1);	
		stat = addAttribute(i_HairRenderWidth);	
		if (!stat) return stat;

		if( !CreateAttrNTextureCache4("BaseWidth","bW", i_HairBaseWidth, 0, 1, 1.0, bStorableAttributes))
			return MS::kFailure;

		if( !CreateAttrNTextureCache4("TipWidth","tW", i_HairTipWidth, 0, 1, 0.0, bStorableAttributes))
			return MS::kFailure;

		i_mulWidthByLength = numAttr.create( "mulWidthByLength", "mwbl", MFnNumericData::kBoolean, 0, &stat);
		::addAttribute(i_mulWidthByLength, atInput|atKeyable);
		
		// TEST
//		CreateRampAttrNTextureCache(i_HairWidthRamp, "width", "hwr", 0, 1, 0, bStorableAttributes);
	}
	// Hair controllers
	{
		i_CVcountU = numAttr.create ("CVcountU","cvcU",MFnNumericData::kInt, 0,&stat);
		if (!stat) return stat;
		numAttr.setStorable (true);
		numAttr.setReadable(true);
		numAttr.setHidden(false);
		numAttr.setSoftMin(1);	
		numAttr.setSoftMax(25);	
		stat = addAttribute (i_CVcountU);
		if (!stat) return stat;

		i_CVcountV = numAttr.create ("CVcountV","cvcV",MFnNumericData::kInt, 0,&stat);
		if (!stat) return stat;
		numAttr.setStorable (true);
		numAttr.setReadable(true);
		numAttr.setHidden(false);
		numAttr.setSoftMin(1);	
		numAttr.setSoftMax(25);	
		stat = addAttribute (i_CVcountV);	
		if (!stat) return stat;

		/*/
		i_CVsegmCount = numAttr.create ("CVsegmCount","cvsC",MFnNumericData::kInt, 0, &stat);
		if (!stat) return stat;
		numAttr.setStorable (true);
		numAttr.setHidden(false);
		numAttr.setSoftMin(0);	
		numAttr.setSoftMax(10);	
		stat = addAttribute (i_CVsegmCount);
		if (!stat) return stat;
		/*/

		i_CVtailU = numAttr.create ("CVtailU","cvtu",MFnNumericData::kBoolean, 0, &stat);
		if (!stat) return stat;
		numAttr.setStorable (true);
		numAttr.setReadable(true);
		numAttr.setHidden(false);
		stat = addAttribute( i_CVtailU);
		if (!stat) return stat;

		i_CVtailV = numAttr.create ("CVtailV","cvtv",MFnNumericData::kBoolean, 0, &stat);
		if (!stat) return stat; 
		numAttr.setStorable (true);
		numAttr.setReadable(true);
		numAttr.setHidden(false);
		stat = addAttribute( i_CVtailV);
		if (!stat) return stat;

		o_CVCurveCreate = typedAttr.create("o_CVCurveCreate", "ocvcc", MFnData::kNurbsCurve);
		::addAttribute(o_CVCurveCreate, atReadable|atWritable|atStorable|atCached);

		//!< ��������� ��� ����������� ������
		o_CVTransform = comAttr.create("o_CVTransform", "ocvt");
		{
			o_CVtranslate = numAttr.createPoint("o_CVtranslate", "ocvtt");
			comAttr.addChild(o_CVtranslate);
		}
		{
			o_CVrotate = numAttr.createPoint("o_CVrotate", "ocvtr");
			comAttr.addChild(o_CVrotate);
		}
		{
			o_CVscale = numAttr.createPoint("o_CVscale", "ocvts");
			comAttr.addChild(o_CVscale);
		}
		{
			o_CVshear = numAttr.createPoint("o_CVshear", "ocvtsh");
			comAttr.addChild(o_CVshear);
		}
		comAttr.setArray(true);
		comAttr.setUsesArrayDataBuilder(true);
		::addAttribute(o_CVTransform, atOutput);

		//!< ����������� ������
		i_CVCurves = typedAttr.create("i_CVCurves", "icvc", MFnData::kNurbsCurve);
		typedAttr.setArray(true);
		::addAttribute(i_CVCurves, atInput);

		//!< ����������� ������ ����� ��������
		i_CVDynArrays = typedAttr.create("i_CVDynArrays", "icvda", MFnData::kVectorArray);
		typedAttr.setArray(true);
		::addAttribute(i_CVDynArrays, atInput);

		i_CVDisplay = numAttr.create ("CvDisplay","cvdi",MFnNumericData::kBoolean, 1, &stat);
		::addAttribute(i_CVDisplay, atInput|atKeyable);

		i_useDynamic = numAttr.create ("useDynamic","ud",MFnNumericData::kBoolean, 0, &stat);
		::addAttribute(i_useDynamic, atInput|atKeyable);

	
	}
	// CLUMPing
	{
		i_CLUMPcount = numAttr.create ("CLUMPcount","cc",MFnNumericData::kInt, 25,&stat);
		if (!stat) return stat;
		numAttr.setStorable (true);
		numAttr.setReadable(true);
		numAttr.setHidden(false);
		numAttr.setSoftMin(0);	
		numAttr.setSoftMax(1000);	
		stat = addAttribute (i_CLUMPcount);
		if (!stat) return stat;

		i_bCLUMP = numAttr.create ("bCLUMP","bclu",MFnNumericData::kBoolean, 0, &stat);
		if (!stat) return stat;
		numAttr.setStorable (true);
		numAttr.setReadable(true);
		numAttr.setHidden(false);
		stat = addAttribute( i_bCLUMP);
		if (!stat) return stat;

		i_CLUMPblending = numAttr.create ("CLUMPblending","clbl",MFnNumericData::kDouble, 0.0, &stat);
		if (!stat) return stat;
		numAttr.setStorable (true);
		numAttr.setHidden(false);
		numAttr.setSoftMin(0);	
		numAttr.setSoftMax(1);	
		stat = addAttribute(i_CLUMPblending);
		if (!stat) return stat;

		i_CLUMPradius = numAttr.create ("CLUMPradius","clr",MFnNumericData::kDouble, 1.0, &stat);
		numAttr.setMin(0);
		numAttr.setSoftMax(1);	
		::addAttribute(i_CLUMPradius, atInput);

		i_CLUMPalgorithm = enumAttr.create ("CLUMPalgorithm", "clal", HairRootPositionsData::QUICK);
		enumAttr.addField("AUTO",	HairRootPositionsData::AUTO);
		enumAttr.addField("QUICK",	HairRootPositionsData::QUICK);
		enumAttr.addField("STUPID",	HairRootPositionsData::STUPID);
		::addAttribute(i_CLUMPalgorithm, atInput);

		i_CLUMPdensity = numAttr.create ("CLUMPdensity","cld",MFnNumericData::kDouble, 1, &stat);
		if (!stat) return stat;
		numAttr.setStorable (true);
		numAttr.setHidden(false);
		numAttr.setSoftMin(0);	
		numAttr.setSoftMax(1);	
		stat = addAttribute(i_CLUMPdensity);
		if (!stat) return stat;

		i_CLUMPdensityVersion = numAttr.create ("CLUMPdensityVersion","cldv", MFnNumericData::kInt, 0,&stat);
		if (!stat) return stat;
		numAttr.setStorable(true);
		numAttr.setHidden(true);
		stat = addAttribute( i_CLUMPdensityVersion);	
		attributeAffects(i_CLUMPdensity, i_CLUMPdensityVersion);

		if( !CreateAttrNTextureCache4("BaseClump","bCl", i_HairBaseClump, 0, 1, 0.0, bStorableAttributes))
			return MS::kFailure;
		if( !CreateAttrNTextureCache4("TipClump","tCl", i_HairTipClump, 0, 1, 1.0, bStorableAttributes))
			return MS::kFailure;

		i_clumpPositionFile = typedAttr.create( "clumpPositionFile", "clpf",
			MFnData::kString, stringData.create(&stat), &stat );
		::addAttribute(i_clumpPositionFile, atInput);
	}



	// Deforming
	{
//		i_time = unitAttr.create( "time", "t", MFnUnitAttribute::kTime);
//		::addAttribute(i_time, atInput);

		i_orthogonalize = enumAttr.create ("orthogonalize", "orth", ORT_NONE);
		enumAttr.addField("NONE",			ORT_NONE);
		enumAttr.addField("tangent U",		ORT_TANGENTU);
		enumAttr.addField("tangent V",		ORT_TANGENTV);
		enumAttr.addField("tangent U(1)",	ORT_TANGENTU_V1);
		enumAttr.addField("tangent V(1)",	ORT_TANGENTV_V1);
		::addAttribute(i_orthogonalize, atInput);

		i_SegmCount = numAttr.create ("segmentCount","sC",MFnNumericData::kInt, 2,&stat);
		if (!stat) return stat;
		numAttr.setKeyable(true);
		numAttr.setStorable (true);
		numAttr.setHidden(false);
		numAttr.setMin(1);	
		numAttr.setSoftMax(50);	
		stat = addAttribute (i_SegmCount);	
		if (!stat) return stat;

		if( !CreateAttrNTextureCache4("Length", "l", i_HairLength, 0.0001, 1, 1.0, bStorableAttributes))
			return MS::kFailure;
		if( !CreateAttrNTextureCache4("Inclination", "incl", i_HairInclination, 0, 1, 0.0, bStorableAttributes))
			return MS::kFailure;
		if( !CreateAttrNTextureCache4("Polar", "plr", i_HairPolar, 0, 1, 1.0, bStorableAttributes))
			return MS::kFailure;
		if( !CreateAttrNTextureCache4("BaseCurl", "bC", i_HairBaseCurl, 0, 1, 0.5, bStorableAttributes))
			return MS::kFailure;
		if( !CreateAttrNTextureCache4("TipCurl", "tC", i_HairTipCurl, 0, 1, 0.5, bStorableAttributes))
			return MS::kFailure;
		if( !CreateAttrNTextureCache4("Scraggle", "s", i_HairScraggle, 0, 1, 0.0, bStorableAttributes))
			return MS::kFailure;
		if( !CreateAttrNTextureCache4("scraggleFreq", "sF", i_HairScraggleFreq, 0, 1, 0.0, bStorableAttributes))
			return MS::kFailure;
		if( !CreateAttrNTextureCache4("twist","twi", i_HairTwist, 0, 1, 0.5, bStorableAttributes))
			return MS::kFailure;
		if( !CreateAttrNTextureCache4("twistFromPolar", "twfp", i_HairTwistFromPolar, 0, 1, 0, bStorableAttributes))
			return MS::kFailure;
		if( !CreateAttrNTextureCache4("blendCV", "blcv", i_blendCVfactor, 0, 1, 1, bStorableAttributes))
			return MS::kFailure;
		if( !CreateAttrNTextureCache4("blendDeform", "blde", i_blendDeformfactor, 0, 1, 1, bStorableAttributes))
			return MS::kFailure;
		if( !CreateAttrNTextureCache4("displace","dis", i_HairDisplace, 0, 1, 0, bStorableAttributes))
			return MS::kFailure;

/*/		
		i_Deformer[0] = typedAttr.create( "deformerDll", "ddll",
			MFnData::kString, stringData.create(&stat), &stat );
		::addAttribute(i_Deformer[0], atInput);

		i_Deformer[1] = typedAttr.create( "deformerProc", "dproc",
			MFnData::kString, stringData.create(&stat), &stat );
		::addAttribute(i_Deformer[1], atInput);
/*/		


		MFnTypedAttribute typedAttr;
		i_HairModifiers = typedAttr.create( "hairModifiers", "hmods", HairModifierData::id, MObject::kNullObj, &stat);
		typedAttr.setWritable(true);
		typedAttr.setReadable(false);
		typedAttr.setArray(true);
		typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
		typedAttr.setIndexMatters(false);
		::addAttribute(i_HairModifiers, 
			atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atHidden|atArray);

		i_HairDeformers = typedAttr.create( "hairDeformers", "hdefs", HairDeformerData::id, MObject::kNullObj, &stat);
		typedAttr.setWritable(true);
		typedAttr.setReadable(false);
		typedAttr.setArray(true);
		typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
		typedAttr.setIndexMatters(false);
		::addAttribute(i_HairDeformers, 
			atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atHidden|atArray);
//			atReadable|atUnWritable|atUnStorable|atUnCached|atConnectable|atHidden|atArray);
	}
	// Hair display
	{
		i_Colorize = numAttr.create ("Colorize","clz",MFnNumericData::kBoolean, 0, &stat);
		if (!stat) return stat;
		numAttr.setStorable(true);
		numAttr.setHidden(false);
		numAttr.setReadable(true);
		stat = addAttribute (i_Colorize);	
		if (!stat) return stat;

		i_maxHairDrawable = numAttr.create("maxHairDrawable", "mhd", MFnNumericData::kInt, 500, &stat);
		if (!stat) return stat;
		numAttr.setStorable (true);
		numAttr.setKeyable (true);
		numAttr.setReadable(true);
		numAttr.setHidden(false);
		numAttr.setMin(1);
		numAttr.setSoftMax(2000);
		numAttr.setMax(1000000);
		stat = addAttribute( i_maxHairDrawable);	
		if (!stat) return stat;

		i_WorkingFaces = typedAttr.create("woFa", "workingFaces", MFnData::kIntArray);
		::addAttribute(i_WorkingFaces, atInput|atHidden);

		i_drawMode = enumAttr.create("drawMode", "dm", 1);
		enumAttr.addField("NONE",			DM_NONE);
		enumAttr.addField("HAIRS",			DM_HAIRS);
		enumAttr.addField("CLUMPS",			DM_CLUMPS);
		enumAttr.addField("CONTROL CURVES", DM_DYNAMIC_CURVES);
#ifndef NO_SN
		enumAttr.addField("INSTANCER",		DM_INSTANCER);
		enumAttr.addField("INSTANCER BOXES",DM_INSTANCERBOX);
#endif
		enumAttr.addField("BOX",			DM_BOX);
		::addAttribute(i_drawMode, atInput);

		i_colorUvSetName = typedAttr.create ("colorUvSet", "hcuvs", MFnData::kString, &stat);
		::addAttribute(i_colorUvSetName, atInput);

		i_BaseColor = numAttr.createColor("BaseColor", "bc");
		if (!stat) return stat;
		numAttr.setStorable (true);
		numAttr.setDefault(1.f, 1.f, 1.f);
		numAttr.setHidden(false);
		numAttr.setReadable(true);
		stat = addAttribute (i_BaseColor);	
		if (!stat) return stat;
		CreateTextureCacheAttr("i_BaseColorCache","bcc", i_BaseColor, i_BaseColorCache, bStorableAttributes);

		i_TipColor = numAttr.createColor("TipColor", "tc");
		if (!stat) return stat;
		numAttr.setStorable (true);
		numAttr.setDefault(1, 1, 1);
		numAttr.setHidden(false);
		numAttr.setReadable(true);
		stat = addAttribute (i_TipColor);	
		if (!stat) return stat;
		CreateTextureCacheAttr("i_TipColorCache","tcc", i_TipColor, i_TipColorCache, bStorableAttributes);
	}

	// Rendering
	{
		MFnEnumAttribute enumAttr;
		i_interpolation = enumAttr.create( "interpolation", "intr", 0, &stat );
		enumAttr.addField( "Linear", 0 );
		enumAttr.addField( "Catmul-Rom", 1 );
		enumAttr.setDefault(0);
		addAttribute( i_interpolation); 

		i_diceHair = numAttr.create( "diceHair", "bdc", MFnNumericData::kBoolean, 1, &stat);
		if (!stat) return stat;
		numAttr.setStorable (true);
		numAttr.setHidden(false);
		stat = addAttribute (i_diceHair);
		if (!stat) return stat;

		i_sigmaHiding = numAttr.create( "sigmaHiding", "shd", MFnNumericData::kBoolean, 0, &stat);
		::addAttribute( i_sigmaHiding, atInput);

		i_renderDoubleSide = numAttr.create( "renderDoubleSide", "rds", MFnNumericData::kBoolean, 1, &stat);
		::addAttribute( i_renderDoubleSide, atInput);

		i_renderNormalSource = enumAttr.create("renderNormalSource", "rns", 0);
		enumAttr.addField("none (on camera)",	0);
		enumAttr.addField("tangentU",			1);
		enumAttr.addField("clumpVector",		2);
		::addAttribute( i_renderNormalSource, atInput);

		i_reverseOrientation = numAttr.create( "reverseOrientation", "rro", MFnNumericData::kBoolean, 0, &stat);
		::addAttribute (i_reverseOrientation, atInput);

		i_renderRayTraceEnable = numAttr.create( "renderRayTraceEnable", "rrte", MFnNumericData::kBoolean, 0, &stat);
		::addAttribute (i_renderRayTraceEnable, atInput);
		i_renderRayCamera = numAttr.create( "renderRayCamera", "rrtc", MFnNumericData::kBoolean, 1, &stat);
		::addAttribute (i_renderRayCamera, atInput);
		i_renderRayTrace = numAttr.create( "renderRayTrace", "rrt", MFnNumericData::kBoolean, 1, &stat);
		::addAttribute (i_renderRayTrace, atInput);
		i_renderRayPhoton = numAttr.create( "renderRayPhoton", "rrtph", MFnNumericData::kBoolean, 1, &stat);
		::addAttribute (i_renderRayPhoton, atInput);

		i_RenderResX = numAttr.create("RenderResX","rrx", MFnNumericData::kInt, 512, &stat);
		if (!stat) return stat;
		numAttr.setReadable(true);
		numAttr.setWritable(true);
		numAttr.setStorable (true);
		numAttr.setHidden(false);
		numAttr.setMin(128.0);
		numAttr.setMax(2048.0);
		stat = addAttribute (i_RenderResX);
		if (!stat) return stat;
		
		i_RenderResY = numAttr.create("RenderResY","rry", MFnNumericData::kInt, 512, &stat);
		if (!stat) return stat;
		numAttr.setReadable(true);
		numAttr.setWritable(true);
		numAttr.setStorable (true);
		numAttr.setHidden(false);
		numAttr.setMin(128.0);
		numAttr.setMax(2048.0);
		stat = addAttribute (i_RenderResY);
		if (!stat) return stat;

//		i_MotionBlur = numAttr.create( "motionBlur", "rmb", MFnNumericData::kBoolean, 1, &stat);
//		::addAttribute(i_MotionBlur, atInput);

		i_bUseWorkingFaces = numAttr.create( "bUseWorkingFaces", "ruwf", MFnNumericData::kBoolean, 0, &stat);
		::addAttribute(i_bUseWorkingFaces, atInput);

		i_splitGroups = numAttr.create( "splitGroups", "rnsg", MFnNumericData::kBoolean, 1, &stat);
		::addAttribute(i_splitGroups, atInput);

		i_bRenderBoxes = numAttr.create( "bRenderBoxes", "rnbo", MFnNumericData::kBoolean, 0, &stat);
		::addAttribute(i_bRenderBoxes, atInput);

		i_bAccurateBoxes = numAttr.create( "accurateBoxes", "rnacb", MFnNumericData::kBoolean, 0, &stat);
		::addAttribute(i_bAccurateBoxes, atInput);
		
		MFnStringData stringData;
		i_FreezeFile = typedAttr.create( "freezeFile", "frfl", MFnData::kString, stringData.create(&stat), &stat );
		::addAttribute(i_FreezeFile, atInput);

		i_dumpInformation = numAttr.create( "dumpInformation", "duin", MFnNumericData::kBoolean, 0, &stat);
		::addAttribute(i_dumpInformation, atInput);
		
		i_pruningScaleFactor = numAttr.create( "pruningScaleFactor", "prsf", MFnNumericData::kDouble, 1, &stat);
		numAttr.setMin(0.0001);
		numAttr.setSoftMax(2);
		::addAttribute(i_pruningScaleFactor, atInput);

		i_MotionSamples = numAttr.create( "motionSamples", "rms", MFnNumericData::kInt, 2, &stat);
		numAttr.setMin(2);
		numAttr.setMax(6);
		::addAttribute(i_MotionSamples, atInput|atKeyable);

		/*/
		i_useDetails = numAttr.create( "useDetails", "usde", MFnNumericData::kBoolean, 0, &stat);
		::addAttribute(i_useDetails, atInput|atKeyable);

		CreateDetailRangeAttr("clump", i_DetailRange_Clump);
		CreateDetailRangeAttr("hair", i_DetailRange_Hair);
		CreateDetailRangeAttr("instance", i_DetailRange_Instance);

		CreateRenderPass("ShadowPass", "sp", i_shadowPass);
		/*/
	}

	// ��� ��������
	{
		MFnTypedAttribute typedAttr;
		MTypeId sndataid(SN_DATAID);
		i_snShape = typedAttr.create( "snShape", "snsh", sndataid, MObject::kNullObj, &stat);
		typedAttr.setReadable(false);
		typedAttr.setArray(true);
		typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
		typedAttr.setIndexMatters(false);
		::addAttribute(i_snShape, atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atHidden|atArray);
		attributeAffects( i_snShape, o_buildMessage);
	}
	{
		MFnTypedAttribute typedAttr;
		i_ocellarisShape = typedAttr.create( "ocellarisShapes", "ocshs", cls::MInstancePxData::id, MObject::kNullObj, &stat);
		typedAttr.setWritable(true);
		typedAttr.setReadable(false);
		typedAttr.setArray(true);
		typedAttr.setIndexMatters(false);
		typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
		::addAttribute(i_ocellarisShape, atUnReadable|atWritable|atUnStorable|atUnCached|atConnectable|atHidden|atArray);
		attributeAffects( i_ocellarisShape, o_buildMessage);
	}
	{
		i_instanceweights = numAttr.create( "instanceweights", "inwe", MFnNumericData::kDouble, 1, &stat);
		numAttr.setMin(0);
		numAttr.setSoftMax(1);
		numAttr.setWritable(true);
		numAttr.setReadable(true);
		numAttr.setArray(true);
		numAttr.setIndexMatters(false);
		numAttr.setDisconnectBehavior( MFnAttribute::kDelete );
		::addAttribute(i_instanceweights, atReadable|atWritable|atStorable|atCached|atConnectable|atArray);
		attributeAffects( i_instanceweights, o_buildMessage);
	}

	// ������
	{
		o_positions = typedAttr.create( "outputPosition", "outpos", MFnData::kVectorArray, &stat);
		typedAttr.setReadable(false);
		typedAttr.setArray(true);
		typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
		typedAttr.setIndexMatters(false);
		stat = typedAttr.setWorldSpace(true);
//		typedAttr.setAffectsWorldSpace(true);
		::addAttribute(o_positions, atOutput);
		AffectHairBuildMessage(o_positions);

		o_hairVector = typedAttr.create( "outputHairVector", "outhv", MFnData::kVectorArray, &stat);
		typedAttr.setReadable(false);
		typedAttr.setArray(true);
		typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
		typedAttr.setIndexMatters(false);
		stat = typedAttr.setWorldSpace(true);
		::addAttribute(o_hairVector, atOutput);
		AffectHairBuildMessage(o_hairVector);

		o_normals = typedAttr.create( "outputNormal", "outnor", MFnData::kVectorArray, &stat);
		typedAttr.setReadable(false);
		typedAttr.setArray(true);
		typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
		typedAttr.setIndexMatters(false);
		typedAttr.setWorldSpace(true);
		::addAttribute(o_normals, atOutput);
		AffectHairBuildMessage(o_normals);

		o_tangentUs = typedAttr.create( "outputTangentU", "outtnu", MFnData::kVectorArray, &stat);
		typedAttr.setReadable(false);
		typedAttr.setArray(true);
		typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
		typedAttr.setIndexMatters(false);
		typedAttr.setWorldSpace(true);
		::addAttribute(o_tangentUs, atOutput);
		AffectHairBuildMessage(o_tangentUs);

		o_tangentVs = typedAttr.create( "outputTangentV", "outtnv", MFnData::kVectorArray, &stat);
		typedAttr.setReadable(false);
		typedAttr.setArray(true);
		typedAttr.setDisconnectBehavior( MFnAttribute::kDelete );
		typedAttr.setIndexMatters(false);
		typedAttr.setWorldSpace(true);
		::addAttribute(o_tangentVs, atOutput);
		AffectHairBuildMessage(o_tangentVs);
	}

	// �����
	{
//		i_test = numAttr.createPoint("test", "ttes", &stat);
//		numAttr.setDisconnectBehavior( MFnAttribute::kDelete);
//		::addAttribute(i_test, atInput|atArray|atUnCached|atStorable);

//		AffectHairGeometryIndices(o_outputMesh);
//		AffectHairGeometry(o_outputMesh);
	}


	// i_HairGeometryIndices
	AffectHairGeometryIndices(i_HairGeometryIndices);

	// i_HairGeometryIndices
	AffectHairGeometry(i_HairGeometry);

	// i_HairClumpPositions 
	AffectHairCVPositions(i_HairCVPositions);

	// i_HairCV
	AffectHairCV(i_HairCV);

	AffectHairCVTransform(o_CVTransform);

	// i_HairClumpPositions 
	AffectHairClumpPositions(i_HairClumpPositions);

	// i_HairRootPositions
//	AffectHairRootPositions(i_HairRootPositionFile);
//	AffectHairClumpPositions(i_HairRootPositionFile);
//	AffectHairCVPositions(i_HairRootPositionFile);

	// i_HairTimeStamp
	AffectHairRootPositions(i_HairTimeStamp);
	AffectHairClumpPositions(i_HairTimeStamp);
	AffectHairCVPositions(i_HairTimeStamp);

	// i_hairDrawPlacement
	AffectHairDrawPlacement(i_hairDrawPlacement);
	AffectHairClumpPositions(i_hairDrawPlacement);

	// o_buildMessage
	AffectHairBuildMessage(o_buildMessage);
	AffectHairBuildMessage(i_ocsFrameFile);

		if( !SourceMelFromResource(MhInstPlugin, "AEHairShapeTemplate", "MEL", defines))
		{
			displayString("error source AEHairShapeTemplate.mel");
		}
	}
	catch(MString err)
	{
		return MS::kFailure;
	}

	return MStatus::kSuccess;
}

void HairShape::AffectByMain(const MObject& isAffected)
{
	attributeAffects(i_generateType,	isAffected);
	attributeAffects(i_HairDensity,		isAffected);
	attributeAffects(i_HairDensityUvSet,isAffected);
	attributeAffects(i_count,			isAffected);
	attributeAffects(i_Seed,			isAffected);
	attributeAffects(i_regularDim,		isAffected);
	attributeAffects(i_regularOffset,	isAffected);
	attributeAffects(i_regularShear,	isAffected);
	attributeAffects(i_jitter,			isAffected);
}
void HairShape::AffectHairGeometryIndices(const MObject& isAffected)
{
	attributeAffects(i_preSubdivSurface,	isAffected);
	attributeAffects(i_uvSetName,			isAffected);
	attributeAffects(i_geometry,			isAffected);
	attributeAffects(i_Seed,				isAffected);
	attributeAffects(i_RenderResX,			isAffected);
	attributeAffects(i_RenderResY,			isAffected);
}
void HairShape::AffectHairGeometry(const MObject& isAffected)
{
	attributeAffects(i_preSubdivSurface,	isAffected);
	attributeAffects(i_uvSetName,			isAffected);
	attributeAffects(i_geometry,	isAffected);
}
void HairShape::AffectHairCVTransform(const MObject& isAffected)
{
	AffectHairCVPositions(isAffected);
	attributeAffects(i_HairCVPositions,		isAffected);
	attributeAffects(i_geometry,			isAffected);
	attributeAffects(i_preSubdivSurface,	isAffected);
	attributeAffects(i_uvSetName,			isAffected);
	attributeAffectsT(i_HairLength,			isAffected);
	attributeAffects(i_HairDeformers,		isAffected);
}

void HairShape::AffectHairCV(const MObject& isAffected)
{
//	AffectHairCVPositions(isAffected);
//	attributeAffects(i_HairCVPositions,	isAffected);
//	attributeAffects(i_useDynamic,		isAffected);
//	attributeAffects(i_CVCurves,		isAffected);
//	attributeAffects(i_CVDynArrays,		isAffected);
}

void HairShape::AffectHairCVPositions(const MObject& isAffected)
{
//	attributeAffects(i_HairGeometryIndices,	isAffected);
//	attributeAffects(i_CVcountU,			isAffected);
//	attributeAffects(i_CVcountV,			isAffected);
}
void HairShape::AffectHairClumpPositions(const MObject& isAffected)
{
	attributeAffects(i_HairGeometryIndices,	isAffected);
	attributeAffects(i_CLUMPcount,			isAffected);
	attributeAffects(i_clumpPositionFile,	isAffected);
	attributeAffects(i_bCLUMP,				isAffected);
	attributeAffects(i_CLUMPblending,		isAffected);
	attributeAffects(i_CLUMPradius,			isAffected);
	attributeAffects(i_CLUMPalgorithm,		isAffected);
	attributeAffects(i_CLUMPdensity,		isAffected);
	attributeAffects(i_CLUMPdensityVersion, isAffected);
	attributeAffects(i_RenderResX,			isAffected);
	attributeAffects(i_RenderResY,			isAffected);
}
void HairShape::AffectHairRootPositions(const MObject& isAffected)
{
	MStatus stat;
	AffectByMain(isAffected);
	attributeAffects( i_HairGeometryIndices,	isAffected);
	attributeAffects( i_HairGeometry,			isAffected);
	attributeAffects( i_HairClumpPositions,		isAffected);

	attributeAffects(i_generateType,			isAffected);
	attributeAffects(i_count,					isAffected);
	attributeAffects(i_Seed,					isAffected);
	attributeAffects(i_regularDim,				isAffected);
	attributeAffects(i_regularOffset,			isAffected);
	attributeAffects(i_regularShear,			isAffected);
	attributeAffects(i_jitter,					isAffected);
	attributeAffects(i_HairDensity,				isAffected);
	attributeAffects(i_HairDensityUvSet,		isAffected);
}
void HairShape::AffectHairDrawPlacement(const MObject& isAffected)
{
	AffectByMain(isAffected);
	attributeAffects( i_HairGeometryIndices,	isAffected);
	attributeAffects( i_HairClumpPositions,		isAffected);
	attributeAffects( i_count,					isAffected);
	attributeAffects( i_jitter,					isAffected);
	attributeAffects( i_HairDensity,			isAffected);
	attributeAffects( i_HairDensityUvSet,		isAffected);
	attributeAffects( i_drawMode,				isAffected);
	attributeAffects( i_maxHairDrawable,		isAffected);
	attributeAffects( i_WorkingFaces,			isAffected);
}

void HairShape::AffectHairBuildMessage(const MObject& isAffected)
{
//	attributeAffects(i_test, isAffected);

	AffectHairGeometryIndices(isAffected);
//	AffectHairGeometry(isAffected);
	AffectHairCVPositions(isAffected);
	AffectHairClumpPositions(isAffected);
	AffectByMain(isAffected);
	AffectHairCV(isAffected);
	attributeAffects(i_HairAlive, isAffected);

	attributeAffects(i_HairGeometryIndices, isAffected);
	attributeAffects(i_HairGeometry,	isAffected);
	attributeAffects(i_HairClumpPositions, isAffected);
	attributeAffects(i_HairCVPositions, isAffected);
	attributeAffects(i_HairCV, isAffected);
//	attributeAffects(i_HairRootPositions, isAffected);
	attributeAffects(i_HairTimeStamp, isAffected);
	attributeAffects(i_hairDrawPlacement, isAffected);

	attributeAffects(i_geometry, isAffected);
	attributeAffects(i_preSubdivSurface,	isAffected);
	attributeAffects(i_uvSetName,			isAffected);
//	attributeAffects(i_displaceSurfaceVer,	isAffected);

	attributeAffects(i_Colorize, isAffected);
	attributeAffects(i_colorUvSetName, isAffected);
	attributeAffects(i_BaseColor, isAffected);
	attributeAffects(i_TipColor, isAffected);
	attributeAffects(i_drawMode,					isAffected);
	attributeAffects( i_Colorize,				isAffected);
	attributeAffects(i_useDynamic,					isAffected);

	attributeAffects(i_count, isAffected);
	attributeAffects(i_jitter, isAffected);

	attributeAffects( i_PaintAttrResX, isAffected);
	attributeAffects( i_PaintAttrResY, isAffected);

	attributeAffectsT(i_HairLength, isAffected);
	attributeAffects(i_HairDeformers, isAffected);
	attributeAffects(i_HairDensity, isAffected);
	attributeAffects( i_HairDensityUvSet,		isAffected);

	attributeAffectsT(i_HairInclination, isAffected);
	attributeAffectsT(i_HairPolar, isAffected);
	attributeAffects(i_HairRenderWidth, isAffected);
	attributeAffectsT(i_HairBaseWidth, isAffected);
	attributeAffectsT(i_HairTipWidth, isAffected);
	attributeAffectsT(i_HairBaseCurl, isAffected);
	attributeAffectsT(i_HairTipCurl, isAffected);
	attributeAffectsT(i_HairScraggle, isAffected);
	attributeAffectsT(i_HairScraggleFreq, isAffected);
	attributeAffectsT(i_HairTwist,				isAffected);
	attributeAffectsT(i_HairDisplace,			isAffected);
	attributeAffectsT(i_HairTwistFromPolar,			isAffected);

	attributeAffectsT(i_blendCVfactor,			isAffected);
	attributeAffectsT(i_blendDeformfactor,			isAffected);

	attributeAffectsT(i_HairBaseClump, isAffected);
	attributeAffectsT(i_HairTipClump, isAffected);
	attributeAffects(i_SegmCount, isAffected);
	attributeAffects(i_orthogonalize, isAffected);
//	attributeAffects(i_time, isAffected);
	attributeAffects(i_Seed, isAffected);
	attributeAffects(i_maxHairDrawable, isAffected);
	attributeAffects( i_WorkingFaces,			isAffected);

	attributeAffects(i_CVcountU, isAffected);
	attributeAffects(i_CVcountV, isAffected);
//	attributeAffects(i_CVsegmCount, isAffected);
	attributeAffects(i_CVtailU, isAffected);
	attributeAffects(i_CVtailV, isAffected);

	attributeAffects(i_CLUMPcount,			isAffected);
	attributeAffects(i_clumpPositionFile,	isAffected);
	attributeAffects(i_bCLUMP,				isAffected);
	attributeAffects(i_CLUMPblending,		isAffected);
	attributeAffects(i_CLUMPradius,			isAffected);
	attributeAffects(i_CLUMPalgorithm,		isAffected);
	attributeAffects(i_CLUMPdensity,		isAffected);
	attributeAffects(i_CLUMPdensityVersion, isAffected);

	attributeAffects(i_interpolation,	isAffected);
	attributeAffects(i_RenderResX,		isAffected);
	attributeAffects(i_RenderResY,		isAffected);

	attributeAffects( mControlPoints, isAffected);
	attributeAffects( mControlValueX, isAffected);
	attributeAffects( mControlValueY, isAffected);
	attributeAffects( mControlValueZ, isAffected);

	attributeAffects( i_HairModifiers, isAffected);
	attributeAffects( i_HairDeformers, isAffected);

//	attributeAffects(io_cacheCvVerticies, isAffected);
}

// �����
void HairShape::CreateArrayAttrib(const char* longName, const char* shortName, int size, float val, MObject& attr)
{
	MStatus stat;
	MFnTypedAttribute	typedAttr;
	MFnDoubleArrayData	doubleArrayData;
	MDoubleArray array(size, val);

	attr = typedAttr.create( longName, shortName,
		MFnData::kDoubleArray, doubleArrayData.create(array, &stat), &stat );
	typedAttr.setReadable(true);
	typedAttr.setWritable(true);
	typedAttr.setCached(true);
	typedAttr.setStorable(true);
	typedAttr.setHidden(true);
	stat = addAttribute(attr);
	attributeAffects( attr, o_buildMessage);

	attributeAffects( i_PaintAttrResX, attr);
	attributeAffects( i_PaintAttrResY, attr);
}

MStatus HairShape::ComputeArrayAttrib(const MPlug& plug, MDataBlock& data)
{
	MStatus stat;
	int resX = data.inputValue(i_PaintAttrResX, &stat).asInt();
	int resY = data.inputValue(i_PaintAttrResY, &stat).asInt();
	MDataHandle handle = data.outputValue( plug );
	MFnDoubleArrayData doubleArrayData( handle.data());
	double val = doubleArrayData[0];

	MDoubleArray array(resX*resY, val);
	doubleArrayData.set(array);
	handle.setClean();
	return MS::kSuccess;
}

void HairShape::CreateVectorArrayAttrib(const char* longName, const char* shortName, int size, const MVector& val, MObject& attr)
{
	MStatus stat;
	MFnTypedAttribute	typedAttr;
	MFnVectorArrayData	vectorArrayData;
	MVectorArray array(size, val);

	attr = typedAttr.create( longName, shortName,
		MFnData::kVectorArray, vectorArrayData.create(array, &stat), &stat );
	typedAttr.setReadable(true);
	typedAttr.setWritable(true);
	typedAttr.setCached(true);
	typedAttr.setStorable(true);
	typedAttr.setHidden(true);
	stat = addAttribute(attr);
	attributeAffects( attr, o_buildMessage);

	attributeAffects( i_PaintAttrResX, attr);
	attributeAffects( i_PaintAttrResY, attr);
}
MStatus HairShape::ComputeVectorArrayAttrib(const MPlug& plug, MDataBlock& data)
{
	MStatus stat;
	int resX = data.inputValue(i_PaintAttrResX, &stat).asInt();
	int resY = data.inputValue(i_PaintAttrResY, &stat).asInt();
	MDataHandle handle = data.outputValue( plug );
	MFnVectorArrayData vectorArrayData( handle.data());
	MVector val = vectorArrayData[0];

	MVectorArray array(resX*resY, val);
	vectorArrayData.set(array);
	handle.setClean();
	return MS::kSuccess;
}
void HairShape::CreateTextureCacheAttr(
	std::string longName, std::string shortName, MObject& src, MObject& attr, bool bStorableAttributes)
{
	MStatus stat;
	MFnTypedAttribute	typedAttr;

	attr = typedAttr.create( longName.c_str(), shortName.c_str(), textureCacheData::id);
	typedAttr.setStorable(false);
	typedAttr.setReadable(true);
	typedAttr.setWritable(true);
	typedAttr.setHidden(true);
	stat = addAttribute(attr);
	if (!stat) 
	{
		displayString("cand create attr %s", longName);
		return;
	}
	attributeAffects(src,				attr);
	attributeAffects(i_PaintAttrResX,	attr);
	attributeAffects(i_PaintAttrResY,	attr);
	attributeAffects(attr, o_buildMessage);
}

MStatus HairShape::CreateAttrNTextureCache( 
	std::string longname, std::string shortname, 
	MObject* i_HairAttr, double vmin, double vmax, double def, bool bStorableAttributes)
{
	MStatus stat;

	// Value
	MFnNumericAttribute numAttr;
	i_HairAttr[0] = numAttr.create(longname.c_str(), shortname.c_str(), MFnNumericData::kDouble, def, &stat);
	if (!stat) return stat;
	numAttr.setKeyable(true);
	numAttr.setStorable(true);
	numAttr.setHidden(false);
	numAttr.setSoftMin(vmin);	
	numAttr.setSoftMax(vmax);	
	stat = addAttribute (i_HairAttr[0]);	
	if (!stat) return stat;
	attributeAffects(i_HairAttr[0], o_buildMessage);

	// Cache
	CreateTextureCacheAttr( longname+"Cache", shortname+"ch", i_HairAttr[0], i_HairAttr[1], bStorableAttributes);

	// jitter
	longname += "Jitter";
	shortname+= "jt";
	i_HairAttr[2] = numAttr.create(longname.c_str(), shortname.c_str(), MFnNumericData::kDouble, 0, &stat);
	numAttr.setSoftMin(0);	
	numAttr.setSoftMax(1);	
	::addAttribute(i_HairAttr[2], atInput);	
	attributeAffects(i_HairAttr[2], o_buildMessage);
	attributeAffects(i_HairAttr[2], i_HairAttr[1]);
	if (!stat) return stat;

	return MS::kSuccess;
}

MStatus HairShape::CreateAttrNTextureCache4( 
	std::string longname, std::string shortname, 
	MObject* i_HairAttr, double vmin, double vmax, double def, bool bStorableAttributes)
{
	MStatus stat;

	// Value
	MFnNumericAttribute numAttr;
	i_HairAttr[0] = numAttr.create(longname.c_str(), shortname.c_str(), MFnNumericData::kDouble, def, &stat);
	if (!stat) return stat;
	numAttr.setKeyable(true);
	numAttr.setStorable(true);
	numAttr.setHidden(false);
	numAttr.setSoftMin(vmin);	
	numAttr.setSoftMax(vmax);	
	stat = addAttribute (i_HairAttr[0]);	
	if (!stat) return stat;
	attributeAffects(i_HairAttr[0], o_buildMessage);

	// Cache
	CreateTextureCacheAttr( longname+"Cache", shortname+"ch", i_HairAttr[0], i_HairAttr[1], bStorableAttributes);

	// jitter
	std::string ln = longname + "Jitter";
	std::string sn = shortname+ "jt";
	i_HairAttr[2] = numAttr.create(ln.c_str(), sn.c_str(), MFnNumericData::kDouble, 0, &stat);
	numAttr.setSoftMin(0);	
	numAttr.setSoftMax(1);	
	::addAttribute(i_HairAttr[2], atInput);	
	attributeAffects(i_HairAttr[2], o_buildMessage);
	attributeAffects(i_HairAttr[2], i_HairAttr[1]);
	if (!stat) return stat;

	// uvsetname
	ln = longname + "UVset";
	sn = shortname+ "uvs";
	MFnTypedAttribute typedAttr;
	MFnStringData stringData;
	i_HairAttr[3] = typedAttr.create( ln.c_str(), sn.c_str(), 
		MFnData::kString, stringData.create(&stat), &stat );
	::addAttribute(i_HairAttr[3], atInput);	
	attributeAffects(i_HairAttr[3], o_buildMessage);
	attributeAffects(i_HairAttr[3], i_HairAttr[1]);
	if (!stat) return stat;

	return MS::kSuccess;
}

MStatus HairShape::CreateRampAttrNTextureCache(
	MObject i_HairAttr[5], std::string fullname, std::string shortname, 
	double vmin, double vmax, double def, bool bStorableAttributes)
{
	MStatus stat;
	MFnNumericAttribute nAttr;
	MFnEnumAttribute eAttr;
	MFnCompoundAttribute cAttr;
	std::string fn, sn;

	fn = fullname + "_Position";
	sn = shortname + std::string("p");
	i_HairAttr[1] = nAttr.create(fn.c_str(), sn.c_str(), MFnNumericData::kFloat, 0.0);
	nAttr.setSoftMin(vmin);
	nAttr.setSoftMax(vmax);
//	stat = addAttribute(i_HairAttr[1]);

	fn = fullname + std::string("_FloatValue");
	sn = shortname + std::string("fv");
	i_HairAttr[2] = nAttr.create(fn.c_str(), sn.c_str(), MFnNumericData::kFloat, 0.0);
//	stat = addAttribute(fv);

	fn = std::string(fullname) + std::string("_Interp");
	sn = std::string(shortname) + std::string("i");
	i_HairAttr[3] = eAttr.create(fn.c_str(), sn.c_str());
	eAttr.addField("None",   0);
	eAttr.addField("Linear", 1);
	eAttr.addField("Smooth", 2);
	eAttr.addField("Spline", 3);
//	stat = addAttribute(in);

	{
		MFnTypedAttribute	typedAttr;
		fn = std::string(fullname) + std::string("_Cache");
		sn = std::string(shortname) + std::string("ch");
		i_HairAttr[4] = typedAttr.create( fn.c_str(), sn.c_str(), textureCacheData::id);
		typedAttr.setStorable(false);
		typedAttr.setReadable(true);
		typedAttr.setWritable(true);
		typedAttr.setHidden(true);
//		stat = addAttribute(attr);
	}

	i_HairAttr[0] = cAttr.create(fullname.c_str(), shortname.c_str());
	cAttr.addChild(i_HairAttr[1]);
	cAttr.addChild(i_HairAttr[2]);
	cAttr.addChild(i_HairAttr[3]);
	cAttr.addChild(i_HairAttr[4]);
	cAttr.setWritable(true);
	cAttr.setStorable(true);
	cAttr.setKeyable(false);
	cAttr.setArray(true);
	stat = addAttribute(i_HairAttr[0]);

	attributeAffects(i_HairAttr[2], i_HairAttr[4]);

//	CreateTextureCacheAttr( fullname+"Cache", shortname+"ch", i_HairAttr[0], i_HairAttr[1]);
	return MS::kSuccess;
}

/*/
void HairShape::CreateDetailRangeAttr(const char* name, MObject i_DetailRange[4])
{
	MStatus stat;
	std::string an = name;
	std::string longname, shortname;
	MFnNumericAttribute numAttr;

	longname = an+"minvisible"; shortname = an+"minvis";
	i_DetailRange[0] = numAttr.create( longname.c_str(), shortname.c_str(), MFnNumericData::kDouble, 0, &stat);
	numAttr.setMin(0);numAttr.setSoftMax(1000);
	::addAttribute(i_DetailRange[0], atInput|atKeyable);

	longname = an+"lowertransition"; shortname = an+"lowtran";
	i_DetailRange[1] = numAttr.create( longname.c_str(), shortname.c_str(), MFnNumericData::kDouble, 0, &stat);
	numAttr.setMin(0);numAttr.setSoftMax(1000);
	::addAttribute(i_DetailRange[1], atInput|atKeyable);

	longname = an+"uppertransition"; shortname = an+"uptran";
	i_DetailRange[2] = numAttr.create( longname.c_str(), shortname.c_str(), MFnNumericData::kDouble, 10000, &stat);
	numAttr.setMin(0);numAttr.setSoftMax(1000);
	::addAttribute(i_DetailRange[2], atInput|atKeyable);

	longname = an+"maxvisible"; shortname = an+"maxvis";
	i_DetailRange[3] = numAttr.create( longname.c_str(), shortname.c_str(), MFnNumericData::kDouble, 10000, &stat);
	numAttr.setMin(0);numAttr.setSoftMax(1000);
	::addAttribute(i_DetailRange[3], atInput|atKeyable);
}

void HairShape::LoadDetailRangeAttr( MDataBlock& data, MObject i_DetailRange[4], HairSystem::DetailRange& v)
{
	v.minvisible		= (float)data.inputValue( i_DetailRange[0]).asDouble();
	v.lowertransition	= (float)data.inputValue( i_DetailRange[1]).asDouble();
	v.uppertransition	= (float)data.inputValue( i_DetailRange[2]).asDouble();
	v.maxvisible		= (float)data.inputValue( i_DetailRange[3]).asDouble();
}


void HairShape::CreateRenderPass(const char* passname, const char* passnameshort, MObject i_pass[2])
{
	MStatus stat;
	std::string an = passname;
	std::string ans = passnameshort;

	std::string longname, shortname;
	MFnNumericAttribute numAttr;

	longname = an+"Overwrite"; shortname = ans+"ov";
	i_pass[0] = numAttr.create( longname.c_str(), shortname.c_str(), MFnNumericData::kBoolean, 0, &stat);
	::addAttribute(i_pass[0], atInput);

	longname = an+"Width"; shortname = ans+"w";
	i_pass[1] = numAttr.create( longname.c_str(), shortname.c_str(), MFnNumericData::kDouble, 0.02, &stat);
	::addAttribute(i_pass[1], atInput);
}
/*/

void HairShape::correctFileName(std::string& str)
{
	for( unsigned int i=0; i<str.size(); i++)
	{
		char& c = str[i];
//		if(i==1 && c==':') continue;
		if( c=='|' || 
			c=='?' || 
			c=='*' || 
			c=='"' || 
			c==':' || 
			c=='<' || 
			c=='>' //|| 
			)
			c = '#';
	}
}


//#include "Util/math_bmpMatrixMN.h"
//#include "Util/FileStream.h"

// HairExport HairShape1 x x 1 0 0 0 0 0 -bmp 1024
//
bool HairShape::SaveHairPositionToFile(int resolution)
{
	/*/
	MDGContext context;//(time);

	MObject val_geometry;
	HairGeometry* geometry = LoadHairGeometry(context, val_geometry);
	if( !geometry) 
		return false;

	std::string hairRootPositionsFileName = this->GetHairRootPositionFile();
	if( hairRootPositionsFileName.empty()) 
		return false;

	HairGeometryIndices geometryIndices;
	HairCVPositions		cvPositions;
	HairClumpPositions	clumpPositions;
	HairRootPositions	rootPositions;

	{
		Util::FileStream file;
		if( !file.open( hairRootPositionsFileName.c_str(), false))
		{
			fprintf(stderr, "Fur: cant open %s for read\n", hairRootPositionsFileName.c_str());
			return false;
		}
		time_t timestamp;
		std::string fullobjname;
		file >> timestamp;
		file >> fullobjname;
		geometryIndices.serialize(file);
		cvPositions.serialize(file);
		clumpPositions.serialize(file);
		rootPositions.serialize(file);
	}

	math::bmpMatrix bmp;
	bmp.init(resolution, resolution);
	
	for( int i=0; i<(int)rootPositions.hairs.size(); i++)
	{
		HairGeometryIndices::pointonsurface& pon = rootPositions.hairs[i];

		Math::Vec2f uv = rootPositions.getUV(i, &geometryIndices, geometry);
		uv.x = (uv.x-floor(uv.x))*resolution;
		uv.y = (uv.y-floor(uv.y))*resolution;
		int x = uv.x<resolution?uv.x:0;
		int y = uv.y<resolution?uv.y:0;

		bmp[y][x] = math::rgb(255, 255, 255);
	}


	std::string out = hairRootPositionsFileName+".bmp";
	math::saveBmp(out.c_str(), bmp);
	/*/

	return true;
}
