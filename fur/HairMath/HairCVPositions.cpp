#include "stdafx.h"
#include "HairCVPositions.h"
#include "HairGeometry.h"
#include "Math/interpolation.h"

/*/
Math::Vec2f HairCVPositions::getUV(
	int cv, 
	HairGeometryIndices* geometryIndices, 
	HairGeometry* geometry
	)
{
geometryIndices->vertexCount();
	HairGeometryIndices::pointonsurface& pons = cvs[cv];
	return geometry->getUV(pons.faceindex, pons.b, geometryIndices);
}

// �����
void HairCVPositions::getBasis(int cv, 
	HairGeometryIndices* geometryIndices, 
	HairGeometry* geometry, 
	Math::Matrix4f& basis
	)
{
	HairGeometryIndices::pointonsurface& pons = cvs[cv];
	geometry->getBasis(pons.faceindex, pons.b, geometryIndices, basis);
}


bool HairCVPositions::getWeidths(
	float u, float v, 
	HairGeometryIndices& hgi,
	std::vector< WEIGHT>& weights,
	bool CVtailU, bool CVtailV
	)
{
	weights.clear();
	if(regular.empty())
		return false;
	weights.reserve(4);
	int X[2], Y[2];
	float w[2][2];
	u -= 0.5f/regular.sizeX();
	v -= 0.5f/regular.sizeY();
	Math::bilinear_interpolation(u, v, regular.sizeX(), regular.sizeY(), 
		X[0], X[1], Y[0], Y[1], 
		w[0][0], w[0][1], w[1][0], w[1][1], 
		CVtailU, CVtailV);
	double sum = 0;
	for(int x=0; x<2; x++)
	{
		for(int y=0; y<2; y++)
		{
			int index = regular[Y[y]][X[x]];
			if( index<0) continue;

			sum += w[x][y];
			weights.push_back(WEIGHT(index, w[x][y]));

		}
	}
	if( weights.empty()) return false;

	for(int i=0; i<(int)weights.size(); i++)
	{
		weights[i].w = (float)(weights[i].w/sum);
	}
	return true;
}
/*/

void HairCVPositions::copy(const HairCVPositions& arg)
{
//	this->cvs	  =	arg.cvs;
//	this->regular =	arg.regular;
	this->guideCount = arg.guideCount;
//	this->cv_binding = arg.cv_binding;
	this->cvcount = arg.cvcount;
//	this->cvfv_binding = arg.cvfv_binding;
	this->facevertsByGuide = arg.facevertsByGuide;
}

HairCVPositions& HairCVPositions::operator=(const HairCVPositions& arg)
{
	HairCVPositions::copy(arg);
	return *this;
}

void HairCVPositions::serialize(Util::Stream& stream)
{
	int version = 2;
	stream >> version;
	if(version>=2)
	{
		stream >> guideCount;
		stream >> cvcount;
		stream >> facevertsByGuide;
	}
}

void HairCVPositions::clear()
{
//	cvs.clear();
//	regular.init(0, 0);

	cvcount = 0;
	guideCount = 0;
	facevertsByGuide.clear();
//	cvfv_binding.clear();
}
