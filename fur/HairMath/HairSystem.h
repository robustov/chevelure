#pragma once

#include "HairMath.h"
#include "Math/Math.h"
#include "Math/Box.h"
#include <Util/Stream.h>
#include <Util/STLStream.h>
#include <vector>
#include <map>
#include "supernova/ISnShaper.h"
#include "IHairDeformer.h"
#include "HairRenderPass.h"

#include "ocellaris/renderCacheImpl.h"
#include "ocellaris/MInstance.h"

#pragma warning( disable : 4251)

// ������ ��� ���� ������� �����
struct HAIRMATH_API HairSystem
{
	// ������������� 
	int	SegmCount;		// ������������ ������ ��� ��������� � ����!
	// Render
	bool mulWidthByLength;		// ������������ ������ ��� ��������� � ����!

	// ��������������� ������
	enOrthogonalizeType orthogonalize;

	// �� �������� ������� ����������!!!
	std::set<int> workingfaces;

	// ������� ����������� �����
	bool CVtailU, CVtailV;

	// ��������� �� ������ ��� �������
	bool splitGroups;
	// ������� ����� ����� ������� RiProcedure
	bool bAccurateBoxes;
	// ��������� ������ ����� (��� �������)
	bool bRenderBoxes;

	// dump
	bool dumpInformation;

	// SN
	std::map< int, sn::ShapeShortcutPtr> snshapes;
	// ocs files or objects
	std::map< int, cls::MInstance> ocsfiles;
	// ����
	typedef Math::ParamTex2d<unsigned char, Math::EJT_MUL> weight_t;
	std::map< int, weight_t> snshapes_weight;

	// world ��������� ������
	Math::Matrix4f camera_worldpos;
	// world ��������� �����
	Math::Matrix4f this_worldpos;
	// ��� �������� ������� ���������� (��� �������� � ocs)
	std::string passforocs;

	// HairRenderPass
//	HairRenderPass defaultPass;
	std::vector< HairRenderPass> passes;

	// ��� �������� ������� (��� �������� ������)
	std::string currentpassname;


	// freezefile
	std::string freezefile;

	// ����� ScaleFactor ��� �������� 
	float pruningScaleFactor;

public:
	HairSystem();
	void serialize(Util::Stream& stream);
	void serializeOcs(
		const char* _name, 
		cls::IRender* render, 
		bool bSave, 
		const char* dir=NULL, 
		const char* shapename=NULL);
	void clear();
};