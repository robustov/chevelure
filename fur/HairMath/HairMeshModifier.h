#pragma once

#include "HairMath.h"
//#include "HairParam.h"
#include <Math/Math.h>
#include <Math/Ramp.h>
#include <Util/Stream.h>
#include <Util/STLStream.h>
#include "HairDeformerDecl.h"

#pragma warning( disable : 4251)

//! HairMesh
class HAIRMATH_API HairMeshModifier
{
public:
	void LoadParams(HAIRPARAMS& c_hairs, const Math::Vec2f& v, float U, float V, bool bClump);

	// common 
	bool isRelative();	// ������������ ����������� �� �������
	float getLength(float u, float v);
	float getLengthJitter(float u, float v);
	float getInclination(float u, float v);
	float getPolar(float u, float v);
	float getCurl(float u, float v, float param);
	float getWidth(float u, float v, float param);
	float getScraggle(float u, float v);
	float getScraggleFreq(float u, float v);
	float getClump(float u, float v, float param);
	float getTwist(float u, float v);
	float getDisplace(float u, float v, bool bNoJitter=false);

	// ����� ���������� (������������ ����)
	bool CallBuildHair(
		float weight, 
		const Math::Vec3f& dirToModifier,
		const HAIRPARAMS& c_hairs, 			// ��������� ������
		int SegmCount,					// ����� ���������
		HAIRVERTS& hair					// ���������
		);
	void CallPostBuildHair(
		float weight, 
		const Math::Vec3f& dirToModifier,
		const HAIRPARAMS& c_hairs, 			// ��������� ������
		int SegmCount,					// ����� ���������
		HAIRVERTS& hair					// ���������
		);
	bool CallMergeWithClump(
		float weight, 
		const Math::Vec3f& dirToModifier,
		const HAIRPARAMS& c_hairs, 			// ��������� ������
		const HAIRVERTS& orighair,		// �������� �����
		const HAIRPARAMS& c_clumphair, 	// ��������� ������ - ������
		const HAIRVERTS& clumphair,		// ����� � ������
		HAIRVERTS& hair					// ���������
		);

public:
	Math::Matrix4f transform;		// �� ������������ HairShape � ������������ ��������
	Math::Matrix4f transform_inv;	// �� ������������ �������� � ������������ HairShape
	// ��������
	Math::Ramp affectRamp;
	// ���������
	Math::ParamTex2d<unsigned char, Math::EJT_MUL> length;
	Math::ParamTex2d<unsigned char, Math::EJT_MIDDLE> inclination, polar;
	Math::ParamTex2d<unsigned char, Math::EJT_MIDDLE> baseCurl, tipCurl;
	Math::ParamTex2d<unsigned char> baseWidth, tipWidth;
	Math::ParamTex2d<unsigned char> scragle;
	Math::ParamTex2d<unsigned char> scragleFreq;
	Math::ParamTex2d<unsigned char> baseClump, tipClump;
	Math::ParamTex2d<unsigned char, Math::EJT_MIDDLE> twist;
	Math::ParamTex2d<unsigned char, Math::EJT_MUL> displace;

	int hairParamBit;	// ���� ����������
	bool bRelative;

	// ��� �����
	enum enType
	{
		MT_SPHERE = 0,
		MT_CYLINDER = 1
	};
	enType modiferType;
	// �� ���� ����������� �����
	enum enDirectionType
	{
		DT_NONE = 0,
		DT_TO_CENTER = 1,
		DT_X_AXIS = 2,
		DT_Y_AXIS = 3,
		DT_Z_AXIS = 4,
	};
	enDirectionType directionType;
	

	// ���������
	std::vector<HairDeformerDecl> deformers;
public:
	HairMeshModifier();
	void serialize(Util::Stream& stream);
	#ifdef OCELLARIS
	void serializeOcs(const char* _name, cls::IRender* render, bool bSave);
	#endif
};

template <class STREAM>
STREAM& operator>>(STREAM& serializer, HairMeshModifier &v)
{
	v.serialize(serializer);
	return serializer;
}


//common 
inline float HairMeshModifier::getLength(float u, float v)
{
	return length.getValueSource(u, v);
}
inline float HairMeshModifier::getLengthJitter(float u, float v)
{
	return length.getValueJitter();
}

inline float HairMeshModifier::getInclination(float u, float v)
{
	return inclination.getValue( u, v);
}

inline float HairMeshModifier::getPolar(float u, float v)
{
	Math::ParamTex2d<unsigned char, Math::EJT_MIDDLE>& param = polar;

	/*/
	if( !param.filename.empty() && !param.matrix->empty())
	{
		unsigned char c = param.matrix->angle_cubic_interpolation((float)u, (float)v, false, false);
		float d = (c/255.f)*(param.factor)+param.offset;
		return d;
	}
	/*/
	float j = 0;
	if(param.jitter!=0)
	{
		j = param.jitter*(rand()/(float)RAND_MAX - 0.5f);
	}
	if(param.matrix && !param.matrix->empty())
	{
		unsigned char c = (unsigned char)(param.matrix->angle_cubic_interpolation((float)u, (float)v, false, false));
		float d = (c/255.f)*(param.max-param.min)+param.min;
		return d+j;
	}
	return param.defValue+j;
}
inline float HairMeshModifier::getCurl(float u, float v, float param)
{
	return baseCurl.getValue(u, v)*(1.f-param) + tipCurl.getValue( u, v)*param;
}
inline float HairMeshModifier::getWidth(float u, float v, float param)
{
	return baseWidth.getValue(u, v)*(1.f-param) + tipWidth.getValue(u, v)*param;
}

inline float HairMeshModifier::getScraggle(float u, float v)
{
	return scragle.getValue(u, v);
}
inline float HairMeshModifier::getScraggleFreq(float u, float v)
{
	return scragleFreq.getValue(u, v);
}

inline float HairMeshModifier::getClump(float u, float v, float param)
{
	return baseClump.getValue(u, v)*(1.f-param) + tipClump.getValue(u, v)*param;
}
inline float HairMeshModifier::getTwist(float u, float v)
{
	return twist.getValue(u, v);
}
inline float HairMeshModifier::getDisplace(float u, float v, bool bNoJitter)
{
	if( bNoJitter)
		return displace.getValueSource(u, v);
	return displace.getValue(u, v);
}


