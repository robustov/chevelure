#pragma once

#include "HairProcessor.h"
#include "mathNpixar\IPrman.h"
#ifdef OCELLARIS_INSTANCE
#include "ocellaris/IRender.h"
#include "ocellaris/renderCacheImpl.h"
#endif// OCELLARIS_INSTANCE

#pragma warning ( disable:4275)

//! HairProceduralCurve
class HAIRMATH_API HairProceduralCurve : public DrawSynk
{
public:
//	HairProcessor* hairs;
public:
    HairProceduralCurve();
    ~HairProceduralCurve();

	void setBaseUvSet( const char* baseUvSet);

	static void RenderInstanceMotion( 
		std::vector<HairProceduralCurve>& procedurals, 
		std::vector<float>& times, 
		IPrman* prman);

public:
	// ������������� �������
	virtual void Init(
		int hairCount, 
		int vertexInHair, 
		enHairInterpolation interpolation, 
		bool bNormals, 
		std::vector<const char*>* adduv,
		int flags);
	// ���������� ��� ������ ������
	virtual void SetHair(
		int hairindex, 
		int hairid,
		HAIRPARAMS& params, 
		HAIRVERTS& hair,
		enRenderNormalSource renderNormalSource, 
		float RenderWidth,
		float widthBias
		);


	// Render
	void Render(
		IPrman* prman,
		cls::IRender* render,
		float motionPhase, 
		std::map< std::string, cls::Param>& extraParameters, 
		DrawSynk* freeze
		);
	// Render BLUR
	virtual void RenderBlur(
		IPrman* prman,
		cls::IRender* render,
		std::vector<float> times,
		std::vector<DrawSynk*>& procedurals,
		std::map< std::string, cls::Param>& extraParameters, 
		DrawSynk* freeze
		);



	/////////////////////////////////////////////
	// ��� ��� ��������!!!
	virtual void sethair(int cvhair, int vertcount, float u, float v, int id, const Math::Vec3f& p, const Math::Vec3f& n, Math::Vec2f* adduv=NULL);
	virtual void sethairclumpdata(int cvhair, const Math::Vec3f& clump_norm, const Math::Vec3f& clump_vect);
	virtual void sethairvertex(int cvhair, int vertindex, const Math::Vec3f& pt);
	virtual void sethaircolor(int cvhair, int vertindex, const Math::Vec3f& pt);
	virtual void sethairwidht(int cvhair, int vertindex, float val);
	virtual void sethairnormal(int cvhair, int vertindex, const Math::Vec3f& normal);
	virtual void sethairclump_vectorV(int cvhair, int vertindex, float clump);

private:
	std::string baseUvSet;
	bool bCubic;
//	bool diceHair;
	int perVertexCacheSise;

	int* hairoffset;
	std::vector<int> dataoffset;
	RtInt *nverts;		// ����� ����. � ������ ������ nverts[haircount];

	int currentOffset;
	int currentDataOffset;

	// PER VERTEX
	RtPoint* cvs;
	RtFloat* widths;
	bool bNormals;
	std::vector< Math::Vec3f> normals;
	// PER HAIR
	RtFloat* s, *t;
	RtNormal* surface_normals;
//	RtFloat* seed;
	RtInt* hairIds;

	int vertexInHair;
	int hairCount;

	bool bClump;
	std::vector<Math::Vec3f> clump_normal;		// per hair
	std::vector<Math::Vec3f> clump_vector;		// per hair

	std::vector<float> vclump;		// per hair vertex
	std::vector<Math::Vec3f> tangent_vector;		// PI_VARYING

	struct uvSet
	{
		std::string name;
		std::vector<float> u;
		std::vector<float> v;
	};
	std::vector< uvSet > uvSets;
};

