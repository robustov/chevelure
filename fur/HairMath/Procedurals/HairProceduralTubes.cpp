#include "stdafx.h"
	#include "ocellaris/IRender.h"
#include "HairProceduralTubes.h"
#include "HairPersonage.h"
#include "ocellaris/renderPrmanImpl.h"
#include "ocellaris/renderFileImpl.h"

const float minHairLen = 0.00001f;

HairProceduralTubes::HairProceduralTubes():
	nverts(cls::PI_PRIMITIVE),
	iverts(cls::PI_PRIMITIVEVERTEX),
	P(cls::PI_VERTEX, 0, cls::PT_POINT),
	N(cls::PI_VERTEX, 0, cls::PT_NORMAL), 
	s(cls::PI_PRIMITIVEVERTEX),
	t(cls::PI_PRIMITIVEVERTEX)
{
	bCubic = false;
}
HairProceduralTubes::HairProceduralTubes(const HairProceduralTubes& arg):
	nverts(cls::PI_PRIMITIVE),
	iverts(cls::PI_PRIMITIVEVERTEX),
	P(cls::PI_VERTEX, 0, cls::PT_POINT),
	N(cls::PI_VERTEX, 0, cls::PT_NORMAL), 
	s(cls::PI_PRIMITIVEVERTEX),
	t(cls::PI_PRIMITIVEVERTEX)
{
	bCubic = false;
}

void HairProceduralTubes::setBaseUvSet( const char* baseUvSet)
{
	this->baseUvSet = baseUvSet;
}

HairProceduralTubes::~HairProceduralTubes() 
{
}

void HairProceduralTubes::setSubdiv(bool bSubdiv)
{
	this->bCubic = bSubdiv;
}

void HairProceduralTubes::Init(
	int hairCount, int vertexInHair, enHairInterpolation interpolation, bool bNormals, std::vector<const char*>* adduv,
	int flags
	)
{
	nverts.clear();
	iverts.clear();
	N.clear();
	P.clear();
	s.clear();
	t.clear();
	clump_normal.clear();		// per hair
	clump_vector.clear();		// per hair
	clump_vectorV.clear();		// per hair vertex
	uvSets.clear();



	this->vertexInHair = vertexInHair;

	int fc = 4*(vertexInHair-1)*hairCount;
	int vc = 4*vertexInHair*hairCount;
	this->nverts.reserve(fc);
	this->iverts.reserve(fc*4);
	this->P.reserve(vc);
	if( !bCubic)
	{
		this->N.reserve(vc);
	}
	this->s.reserve(fc*4);
	this->t.reserve(fc*4);

	bClump = true;
	if(bClump)
	{
		clump_normal.resize(hairCount);
		clump_vector.resize(hairCount);
	}
	if( adduv)
	{
		uvSets.resize(adduv->size());
		for(unsigned i=0; i<uvSets.size(); i++)
		{
			uvSets[i].name = (*adduv)[i];
			uvSets[i].u.resize(hairCount);
			uvSets[i].v.resize(hairCount);
		}
	}
}
// ���������� ��� ������ ������
void HairProceduralTubes::SetHair(
	int hairindex, 
	int hairid,
	HAIRPARAMS& params, 
	HAIRVERTS& hair,
	enRenderNormalSource renderNormalSource, 
	float RenderWidth,
	float widthBias
	)
{
	if(bCubic)
		RenderWidth = RenderWidth*1.361f;

	int lastvert = P.size();
	if( (int)hair.cvint.size()*4+lastvert > P.reserve_size())
		return;	// �� ������
	int vc = (int)hair.cvint.size();
	Math::Vec3f toMe = params.tangentU;
	for(int v=0; v<vc; v++ )
	{
		Math::Vec3f tangent = Math::Vec3f(0, 0, 0);
		Math::Vec3f pt = hair.cvint[v];
		if(v!=0)
		{
			Math::Vec3f dir = hair.cvint[v]-hair.cvint[v-1];
//			length += dir.length();
//			lparam[v] = length;
			tangent += (dir).normalized();
		}
		if(v!=vc-1)
		{
			Math::Vec3f dir = hair.cvint[v+1]-hair.cvint[v];
			tangent += dir.normalized();
		}

		Math::Vec3f normal1 = Math::cross( tangent, toMe).normalized();
		Math::Vec3f normal2 = Math::cross( tangent, normal1).normalized();
		toMe = -normal2;

		float w = RenderWidth*hair.vwidht[v]*0.5f;
		w += widthBias;
		if(w<0.000001f) w=0.000001f;
		normal1 *= w;
		normal2 *= w;

		P.push_back(pt + normal1);
		P.push_back(pt + normal2);
		P.push_back(pt - normal1);
		P.push_back(pt - normal2);

		if( N.reserve_size()>0)
		{
			N.push_back( normal1);
			N.push_back( normal2);
			N.push_back(-normal1);
			N.push_back(-normal2);
		}
	}
	int nfaces = 4*(vc-1);
	for(int f=0; f<nfaces; f++ )
	{
		nverts.push_back(4);
		if( (f&0x3) != 0x3)
		{
			iverts.push_back( lastvert+f+0);
			iverts.push_back( lastvert+f+1);
			iverts.push_back( lastvert+f+5);
			iverts.push_back( lastvert+f+4);
		}
		else
		{
			iverts.push_back( lastvert+f+0);
			iverts.push_back( lastvert+f+1-4);
			iverts.push_back( lastvert+f+5-4);
			iverts.push_back( lastvert+f+4);
		}
		if( s.reserve_size()>0 && t.reserve_size()>0)
		{
			int v = f/4;
			float t0 = v/(float)(vc-1);
			float t1 = (v+1)/(float)(vc-1);
			float s0 = (f&0x3)/(float)4;
			float s1 = s0 + 0.25f;
			s.push_back(s0);
			s.push_back(s1);
			s.push_back(s1);
			s.push_back(s0);
			t.push_back(1-t0);
			t.push_back(1-t0);
			t.push_back(1-t1);
			t.push_back(1-t1);
		}
	}
}

void HairProceduralTubes::Render(
	IPrman* prman,
	cls::IRender* render,
	float motionPhase, 
	std::map< std::string, cls::Param>& extraParameters, 
	DrawSynk* freeze
	)
{
//	cls::renderFileImpl render;
//	render.openForWrite("m:/xxx.ocs", true);

	render->Parameter("P", P);

	if( freeze)
	{
		HairProceduralTubes* _freeze = (HairProceduralTubes*)freeze;
		render->Parameter("__Pref", _freeze->P);
	}

	if( !N.empty())
		render->Parameter("N", N);
	if( !s.empty())
		render->Parameter("s", s);
	if( !t.empty())
		render->Parameter("t", t);

}

void HairProceduralTubes::RenderCall(
	cls::IRender* render, 
	int motionBlurSamples, 
	float* motionBlurTimes
	)
{
	cls::PA<int> loops(cls::PI_PRIMITIVE, nverts.size());
	for(int i=0; i<loops.size(); i++)
		loops[i] = 1;

	if( bCubic)
	{
		render->Parameter("#subdiv::interpolateBoundary", true);
	}

	render->Mesh(bCubic?"catmull-clark":"linear", 
		loops, nverts, iverts);
}


// Render BLUR
void HairProceduralTubes::RenderBlur(
	IPrman* prman,
	cls::IRender* render,
	std::vector<float> times,
	std::vector<DrawSynk*>& procedurals,
	std::map< std::string, cls::Param>& extraParameters, 
	DrawSynk* freeze
	)
{
	prman->RiMotionBeginV( (int)times.size(), &times[0]);
	for(int p=0; p<(int)times.size(); p++)
	{
		procedurals[p]->Render(prman, render, 0, extraParameters, freeze);
	}
	prman->RiMotionEnd();
}
