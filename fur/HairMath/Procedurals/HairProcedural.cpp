#include "stdafx.h"
#include "HairProcedural.h"
#include "HairPersonage.h"

const float minHairLen = 0.00001f;
void CompleteTokenList(
	std::vector<RtToken>& tokens, 
	const std::vector<std::string>& tokennames
	);


HairProcedural::HairProcedural()
{
//	this->interpolation = interpolation;
//	this->diceHair = diceHair;
	perVertexCacheSise = 0;
	currentOffset = 0;
	currentDataOffset = 0;
	vertexInHair = 0;
	hairCount = 0;
	bClump= false;
	bCubic= false;

	hairoffset = 0;
	cvs = NULL;
	widths = NULL;
	nverts = NULL;
	s = t = 0;
	surface_normals = NULL;
//	seed = 0;
	hairIds = 0;
	bNormals = false;
}
void HairProcedural::setBaseUvSet( const char* baseUvSet)
{
	this->baseUvSet = baseUvSet;
}

HairProcedural::~HairProcedural() 
{
	delete widths;
	delete[] cvs;
	delete nverts;
	delete hairoffset;
	delete s; 
	delete t;
	delete[] surface_normals;
//	delete seed;
	delete hairIds;
}

void HairProcedural::Init(int hairCount, int vertexInHair, enHairInterpolation interpolation, bool bNormals, std::vector<const char*>* adduv)
{
	this->bCubic = false;
	if( interpolation != HI_LINIAR)
		this->bCubic = true;
	this->bNormals = bNormals;

//this->bCubic = true;
//fprintf(stderr, "HairProcedural::Init hairs.interpolation=%d bCubic = %d\n", hairs.interpolation, (int)this->bCubic);

	this->vertexInHair = vertexInHair;
	if( bCubic)
		this->vertexInHair += 2;
	this->perVertexCacheSise = this->vertexInHair*hairCount+10;

	this->currentOffset = 0;
	this->currentDataOffset = 0;
	this->hairoffset = new int[hairCount];
	this->nverts = new RtInt[hairCount];
	this->dataoffset.resize(hairCount);

	this->s = new RtFloat[hairCount];
	this->t = new RtFloat[hairCount];
	this->surface_normals = new RtNormal[hairCount];
//	this->seed = new RtFloat[hairCount];
	this->hairIds = new RtInt[hairCount];

	cvs = new RtPoint[perVertexCacheSise];
	widths = new RtFloat[perVertexCacheSise];

	this->hairCount = 0;

	bClump = true;
	if(bClump)
	{
		clump_normal.resize(hairCount);
		clump_vector.resize(hairCount);
		clump_vectorV.resize(perVertexCacheSise, Math::Vec3f(0, 0, 0));
	}
	if(this->bNormals)
	{
		normals.resize(perVertexCacheSise);
	}
	if( adduv)
	{
		uvSets.resize(adduv->size());
		for(unsigned i=0; i<uvSets.size(); i++)
		{
			uvSets[i].name = (*adduv)[i];
			uvSets[i].u.resize(hairCount);
			uvSets[i].v.resize(hairCount);
		}
	}
//fprintf(stderr, ": %x HairProcedural::Init h=%d vc=%d sum=%d\n", this, hairCount, vertexInHair, perVertexCacheSise);
}
void HairProcedural::sethair(
	int cvhair, int vertcount, 
	float u, float v, 
	int id, 
	const Math::Vec3f& p, 
	const Math::Vec3f& n, 
	Math::Vec2f* adduv
	)
{
	hairCount = cvhair+1;
	s[cvhair] = u;
	t[cvhair] = 1-v;
//	seed[cvhair] = u+v;
	this->hairIds[cvhair] = id;
	IPrman::copy( surface_normals[cvhair], n);

	dataoffset[cvhair] = currentDataOffset;
	currentDataOffset += vertcount;

	if(bCubic)
		vertcount+=2;

//fprintf(stderr, "> %x HairProcedural::sethair %d %d ofs=%d uv{%f %f}\n", this, cvhair, vertcount, currentOffset, u, v);

	if( currentOffset+vertcount >= perVertexCacheSise)
	{
		fprintf(stderr, "PER VERTEX BUFFER OVERFLOW cvhair=%d cv=%d| %d+%d > %d\n", cvhair, vertcount, currentOffset, vertcount, perVertexCacheSise);
		fflush(stderr);
		currentOffset = 0;
	}
	hairoffset[cvhair] = currentOffset;

	nverts[cvhair] = vertcount;
	currentOffset += vertcount;

	if(bCubic)
	{
		int i = hairoffset[cvhair];
		Math::Vec3f& pt = -n.normalized();
		RtPoint* dst = cvs + i;
		IPrman::copy(*dst, pt);
	}
	if( adduv)
	{
		for(unsigned i=0; i<uvSets.size(); i++)
		{
			uvSets[i].u[cvhair] = adduv[i].x;
			uvSets[i].v[cvhair] = adduv[i].y;
		}
	}
}
void HairProcedural::sethairclumpdata(int cvhair, const Math::Vec3f& clump_norm, const Math::Vec3f& clump_vect)
{
	if(bClump)
	{
		clump_normal[cvhair] = clump_norm;
		clump_vector[cvhair] = clump_vect;
	}
}
void HairProcedural::sethairclump_vectorV(int cvhair, int vertindex, const Math::Vec3f& clump_vector)
{
	if(bClump)
	{
		int i = dataoffset[cvhair] + vertindex;
		clump_vectorV[i] = clump_vector;
	}
}

void HairProcedural::sethairvertex(int cvhair, int vertindex, const Math::Vec3f& pt)
{
	int i = hairoffset[cvhair] + vertindex + (bCubic?1:0);
//fprintf(stderr, "%x HairProcedural::sethairvertex %d %d (%f, %f, %f) ind=%d\n", this, cvhair, vertindex, pt.x, pt.y, pt.z, i);
	RtPoint* dst = cvs + i;
	IPrman::copy(*dst, pt);
	if( bCubic && vertindex == (nverts[cvhair]-3))
	{
		// last point
		IPrman::copy(dst[1], pt);
		// first point
		RtPoint* first = cvs + hairoffset[cvhair];
		Math::Vec3f pt1, pt2, n;
		IPrman::copy(  n, first[0]);
		IPrman::copy(pt1, first[1]);
		IPrman::copy(pt2, first[2]);
		float l = (pt1-pt2).length();
		pt1 += n*l;
		IPrman::copy(first[0], pt1);
	}
}

void HairProcedural::sethairnormal(int cvhair, int vertindex, const Math::Vec3f& normal)
{
	if( bNormals)
	{
		int i = dataoffset[cvhair] + vertindex;
		normals[i] = normal;
//		normals[i] = Math::Vec3f(1, 0, 0);
	}
}

void HairProcedural::sethairwidht(int cvhair, int vertindex, float val)
{
	int i = dataoffset[cvhair] + vertindex;
//fprintf(stderr, "%x HairProcedural::sethairwidht %d %d (%f) ind=%d\n", this, cvhair, vertindex, val, i);

	RtFloat* dst = widths + i;
	*dst = val;
}
void HairProcedural::sethaircolor(int cvhair, int vertindex, const Math::Vec3f& pt)
{
}


// INSTANCE
void HairProcedural::InitSn(int hairCount)
{
	instHairs.resize(hairCount);
#ifdef OCELLARIS_INSTANCE
	ocsshapes.resize(hairCount);
#endif// OCELLARIS_INSTANCE
}
void HairProcedural::setSnShape(int index, sn::ShapeShortcutPtr& ptr)
{
	snshapes[index] = ptr;
}
#ifdef OCELLARIS_INSTANCE
void HairProcedural::setOcsShape(int index, cls::MInstance& cache)
{
	ocsshapes[index] = &cache;
}
#endif// OCELLARIS_INSTANCE
void HairProcedural::setSnHair(int cvhair, int instance, HairPersonageData& data)
{
	instHairs[cvhair] = data;
	instHairs[cvhair].instance = instance;
}

RtVoid fur_subdivfunc(RtPointer ptr, RtFloat detail)
{
	HairPersonage* personage = (HairPersonage*)ptr;
	try
	{
		// detail
		sn::ShapeShortcut& ss = *personage->ptr;
		ss.shaper->render(
			ss.shape, personage, personage->prman);
	}
	catch(...)
	{
		fprintf(stderr, "Fur: fur_subdivfunc exception\n");
		fflush(stderr);
	}
}
RtVoid fur_freefunc(RtPointer ptr)
{
	try
	{
		HairPersonage* personage = (HairPersonage*)ptr;
		delete personage;
	}
	catch(...)
	{
		fprintf(stderr, "Fur: fur_freefunc exception\n");
		fflush(stderr);
	}
}


void HairProcedural::RenderInstanceMotion( 
	std::vector<HairProcedural>& procedurals, 
	std::vector<float>& times,
	IPrman* prman)
{
	// ��� ������ �����
	std::vector<HairPersonage> personages;
	HairProcedural& startProcedureal = procedurals[0];
	
	// �� ������� �������
	for(unsigned i=0; i<startProcedureal.instHairs.size(); ++i)
	{
		const HairPersonageData& snhair = startProcedureal.instHairs[i];
		sn::ShapeShortcutPtr snInst = startProcedureal.snshapes[snhair.instance];

		if( !snInst->shaper)
			continue;

		bool bValid = true;
		personages.resize(procedurals.size());
		for(unsigned z=0; z<personages.size(); z++)
		{
			personages[z].data = procedurals[z].instHairs[i];
			personages[z].ptr  = snInst;
			personages[z].prman = prman;
//			printf("%d(%d) length=%f:\n", i, z, personages[z].data.length);
			if( personages[z].data.length < minHairLen)
				bValid = false;
		}
		if( !bValid) 
			continue;

		prman->RiAttributeBegin();

		// ������
		prman->setRenderMode(IPrman::RENDERMODE_SHADER);
		snInst->shaper->render(snInst->shape, &personages[0], prman);
		
		// ���������
		prman->RiTransformBegin();
		prman->RiMotionBeginV(times.size(), &times[0]);
		for(z=0; z<personages.size(); z++)
		{
			const HairPersonageData& snhair = procedurals[z].instHairs[i];
			Math::Matrix4f m = snhair.transform;
			m[0] *= snhair.length;
			m[1] *= snhair.length;
			m[2] *= snhair.length;
			prman->RiConcatTransform(m);
		}
		prman->RiMotionEnd();

		// ���������
		prman->setRenderMode(IPrman::RENDERMODE_GEOMETRY);
		prman->RiMotionBeginV(times.size(), &times[0]);
		for(z=0; z<personages.size(); z++)
			snInst->shaper->render(snInst->shape, &personages[z], prman);
		prman->RiMotionEnd();

		prman->RiTransformEnd();
		prman->RiAttributeEnd();
		prman->setRenderMode(IPrman::RENDERMODE_ALL);

	}
}

void HairProcedural::Render(
	IPrman* prman
#ifdef OCELLARIS_INSTANCE
	,cls::IRender* render
#endif// OCELLARIS_INSTANCE
	)
{
	if( hairCount)
	{
		/*/
		RtInt dicehair = 1;
		prman->RiAttributeBegin();
		prman->RiAttribute("dice", "uniform int hair", &dicehair, RI_NULL);

		RtBasis catmullRom = {
		{ -0.5,  1.5, -1.5,  0.5 },
		{  1.0, -2.5,  2.0, -0.5 },
		{ -0.5,  0.0,  0.5,  0.0 },
		{  0.0,  1.0,  0.0,  0.0 }
		};
		prman->RiBasis(catmullRom, 1, catmullRom, 1);
		/*/

		if( true)
		{
			std::vector<RtToken> tokens;
			std::vector<std::string> tokennames;
			std::vector<RtPointer> parms;

			tokennames.push_back( "P");
			parms.push_back( cvs);

			if( bNormals)
			{
//				tokennames.push_back( "normal N");
//				tokennames.push_back( "N");
				tokennames.push_back( "varying normal N");
				parms.push_back( &normals[0]);
			}

			tokennames.push_back( "uniform normal SN");
			parms.push_back( surface_normals);

			tokennames.push_back( "width");
			parms.push_back( widths);

			tokennames.push_back( "uniform float s");
			parms.push_back( s);

			tokennames.push_back( "uniform float t");
			parms.push_back( t);

			tokennames.push_back( "uniform int hairId");
			parms.push_back( hairIds);

			if( bClump)
			{
				tokennames.push_back( "uniform vector clump_normal");
				parms.push_back( &clump_normal[0]);

				tokennames.push_back( "uniform vector clump_vector");
				parms.push_back( &clump_vector[0]);

				tokennames.push_back( "varying vector clump_vectorV");
				parms.push_back( &clump_vectorV[0]);
			}
			{
				for(unsigned i=0; i<uvSets.size(); i++)
				{
					std::string name_u = "uniform float u_" + uvSets[i].name;
					std::string name_v = "uniform float v_" + uvSets[i].name;
					std::vector<float>& u = uvSets[i].u;
					std::vector<float>& v = uvSets[i].v;
					tokennames.push_back( name_u);
					parms.push_back( &u[0]);
					tokennames.push_back( name_v);
					parms.push_back( &v[0]);
				}
				// baseUvSet
				{
					std::string name_u = "uniform float u_" + baseUvSet;
					std::string name_v = "uniform float v_" + baseUvSet;
					tokennames.push_back( name_u);
					parms.push_back( s);
					tokennames.push_back( name_v);
					parms.push_back( t);
				}
			}
			CompleteTokenList(tokens, tokennames);
			/*/
			if( this->diceHair)
			{
				RtInt flag = 1;
				prman->RiAttribute("dice", "hair", (RtPointer)&flag, NULL);
			}
			/*/
			if(bCubic)
				prman->RiCurvesV("cubic", hairCount, nverts, "nonperiodic", tokens.size(), &tokens[0], &parms[0]);
			else
				prman->RiCurvesV("linear", hairCount, nverts, "nonperiodic", tokens.size(), &tokens[0], &parms[0]);
		}

		/*/

		// TEST
		if(false)
		{
			hairCount = 1;
			vertexInHair = 10;
			nverts = new RtInt[hairCount];

			cvs = new RtPoint[vertexInHair*hairCount];
			widths = new RtFloat[vertexInHair*hairCount];

			nverts[0] = vertexInHair;
			for(int i=0; i<vertexInHair; i++)
			{
				cvs[i][0] = 0.f;
				cvs[i][1] = 0.1f*i;
				cvs[i][2] = 0.f;
				widths[i] = 0.1f;
			}
		}
	//		fprintf(stderr, "%x Build\n", this);

		// DUMP
		if(false)
		{
			for(int h=0; h<hairCount; h++)
			{
				if( (h%500)!=0 )
					continue;
				fprintf(stderr, "h %d count %d ofs %d\n", h, nverts[h], hairoffset[h]);
				for(int v=0; v<nverts[h]; v++)
				{
					int i  = hairoffset[h]+v;
					RtPoint* pt = cvs+i;
					fprintf(stderr, "  %d: (%f, %f, %f)\n", i, (*pt)[0], (*pt)[1], (*pt)[2]);
				}
			}
		}

		if(false)
		{
			for(int h=0; h<hairCount; h++)
			{
				try
				{
					int ih = hairoffset[h];
					int id = dataoffset[h];

					RtInt locnverts[1];
					locnverts[0] = nverts[h];

					RtToken names[] = {
						"P",
						"uniform normal SN",
						"width",
						"uniform float s",
						"uniform float t",
	//					"uniform float SEED",
						"uniform int hairId",
					};
					RtPointer values[] = 
					{
						cvs+ih,
						surface_normals+h,
						widths+id,
						s+h,
						t+h,
	//					seed+h,
						hairIds+h,
					};

					if( this->diceHair)
					{
						RtInt flag = 1;
						prman->RiAttribute("dice", "hair", (RtPointer)&flag, NULL);
					}
					if(bCubic)
						prman->RiCurvesV("cubic", 1, locnverts, "nonperiodic", sizeof(values)/sizeof(RtPointer), names, values);
					else
						prman->RiCurvesV("linear", 1, locnverts, "nonperiodic", sizeof(values)/sizeof(RtPointer), names, values);
				}
				catch(...)
				{
					fprintf(stderr, "Exception h=%d\n", h);
				}
			}
		}
		/*/
//		prman->RiAttributeEnd();
	}
	// 
#ifndef OCELLARIS_INSTANCE
	if(instHairs.size())
	{
		for(unsigned i=0; i<instHairs.size(); ++i)
		{
			const HairPersonageData& snhair = instHairs[i];
			sn::ShapeShortcutPtr snInst = snshapes[snhair.instance];

			if( !snInst->shaper)
				continue;

			HairPersonage* personage = new HairPersonage();

			personage->data = snhair;
			personage->ptr = snInst;
			personage->prman = prman;

			if( personage->data.length < minHairLen)
				continue;

			Math::Matrix4f m = snhair.transform;
			m[0] *= snhair.length;
			m[1] *= snhair.length;
			m[2] *= snhair.length;

			prman->RiTransformBegin();
			prman->RiConcatTransform(m);

			RtBound bound;
			Math::Box3f box;
			snInst->shaper->getDirtyBox( snInst->shape, box);

			IPrman::copy(bound, box);
			prman->RiProcedural(personage, bound, fur_subdivfunc, fur_freefunc);
			prman->RiTransformEnd();
		}
	}
#else
	if(instHairs.size())
	{
		for(unsigned i=0; i<instHairs.size(); ++i)
		{
			const HairPersonageData& snhair = instHairs[i];
			cls::MInstance* inst = ocsshapes[snhair.instance];
			if( inst && inst->isValid())
			{
				Math::Matrix4f m = snhair.transform;
				m[0] *= snhair.length;
				m[1] *= snhair.length;
				m[2] *= snhair.length;

				render->PushAttributes();
				render->PushTransform();
				render->AppendTransform(m);

					cls::PA<Math::Matrix4f> transforms(cls::PI_CONSTANT, snhair.transforms);
					cls::PA<float> parameters(cls::PI_CONSTANT, snhair.parameters);
					
					Math::Box3f box = inst->getBox();
					float maxDot = box.max.y;
					render->Parameter("HairFilter::u", snhair.u);
					render->Parameter("HairFilter::v", snhair.v);
					render->Parameter("HairFilter::id", snhair.id);
					render->Parameter("HairFilter::transforms", transforms);
					render->Parameter("HairFilter::parameters", parameters);
					render->Parameter("HairFilter::maxDot", maxDot);
					render->Filter("HairFilter");

					render->Attribute("global::phase", snhair.animation);
					inst->Render(render);

				render->PopTransform();
				render->PopAttributes();
			}
		}
	}
#endif// OCELLARIS_INSTANCE
}

void CompleteTokenList(
	std::vector<RtToken>& tokens, 
	const std::vector<std::string>& tokennames
	)
{
	tokens.resize(tokennames.size());
	for(unsigned i=0; i<tokens.size(); i++)
	{
		tokens[i] = (RtToken)tokennames[i].c_str();
	}
}
