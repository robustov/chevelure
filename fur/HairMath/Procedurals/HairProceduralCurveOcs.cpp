#include "stdafx.h"
	#include "ocellaris/IRender.h"
#include "HairProceduralCurveOcs.h"
#include "ocellaris/renderCacheImpl.h"
#include "ocellaris/renderBlurParser.h"

const float minHairLen = 0.00001f;

// test NAN values
//#define TEST_NAN

HairProceduralCurveOcs::uvSet::uvSet():
	u(cls::PI_PRIMITIVE),
	v(cls::PI_PRIMITIVE)
{
};
HairProceduralCurveOcs::uvSet::uvSet(const uvSet& arg):
	u(cls::PI_PRIMITIVE),
	v(cls::PI_PRIMITIVE)
{
	name = arg.name;
}

HairProceduralCurveOcs::HairProceduralCurveOcs():
	nverts(cls::PI_CONSTANT),
	P(cls::PI_VERTEX, 0, cls::PT_POINT),
	P_nonoise(cls::PI_VERTEX, 0, cls::PT_POINT),
	widths(cls::PI_VARYING),
	normals(cls::PI_VARYING),
	hairIds(cls::PI_PRIMITIVE),
	clumpIds(cls::PI_PRIMITIVE),
	surface_normals(cls::PI_PRIMITIVE, 0, cls::PT_NORMAL),
	s(cls::PI_PRIMITIVE),
	t(cls::PI_PRIMITIVE),
	lengthjitter(cls::PI_PRIMITIVE),
	clump_radius(cls::PI_PRIMITIVE),
	clump_normal(cls::PI_PRIMITIVE),
	clump_vector(cls::PI_PRIMITIVE),
	clump_vectorV(cls::PI_VARYING), 
	vclump(cls::PI_VARYING), 
	tangent_vector(cls::PI_PRIMITIVE)
{
	bClump= false;
	bCubic= false;
	bNormals = false;
}
HairProceduralCurveOcs::HairProceduralCurveOcs(const HairProceduralCurveOcs& arg):
	nverts(cls::PI_CONSTANT),
	P(cls::PI_VERTEX, 0, cls::PT_POINT),
	P_nonoise(cls::PI_VERTEX, 0, cls::PT_POINT),
	widths(cls::PI_VARYING),
	normals(cls::PI_VARYING),
	hairIds(cls::PI_PRIMITIVE),
	clumpIds(cls::PI_PRIMITIVE),
	surface_normals(cls::PI_PRIMITIVE, 0, cls::PT_NORMAL),
	s(cls::PI_PRIMITIVE),
	t(cls::PI_PRIMITIVE),
	lengthjitter(cls::PI_PRIMITIVE),
	clump_radius(cls::PI_PRIMITIVE),
	clump_normal(cls::PI_PRIMITIVE),
	clump_vector(cls::PI_PRIMITIVE),
	clump_vectorV(cls::PI_VARYING), 
	vclump(cls::PI_VARYING), 
	tangent_vector(cls::PI_PRIMITIVE)
{
	bClump= false;
	bCubic= false;
	bNormals = false;
}

void HairProceduralCurveOcs::setBaseUvSet( const char* baseUvSet)
{
	this->baseUvSet = baseUvSet;
}

HairProceduralCurveOcs::~HairProceduralCurveOcs() 
{
}

void HairProceduralCurveOcs::Init(
	int hairCount, 
	int vertexInHair, 
	enHairInterpolation interpolation, 
	bool bNormals, 
	std::vector<const char*>* adduv,
	int flags
	)
{
	nverts.clear();						// HAIRS
	P.clear();						// �����
	P_nonoise.clear();				// nonoise
	widths.clear();						// PI_CONSTANT
	normals.clear();				// PI_VARYING
	hairIds.clear();						// PI_CONSTANT
	clumpIds.clear();						// PI_CONSTANT
	surface_normals.clear();		// PI_CONSTANT
	s.clear(), t.clear();						// PI_CONSTANT
	clump_radius.clear();			// PI_CONSTANT
	lengthjitter.clear();
	clump_normal.clear();			// PI_CONSTANT
	clump_vector.clear();			// PI_CONSTANT
	clump_vectorV.clear();			// PI_VARYING
	vclump.clear();						// PI_VARYING
	tangent_vector.clear();		// PI_VARYING
	uvSets.clear();



	this->bCubic = false;
	if( interpolation != HI_LINIAR)
		this->bCubic = true;
	this->bNormals = bNormals;

	if( bCubic)
		vertexInHair += 2;

	int perVertexCacheSise = vertexInHair*hairCount;

	this->nverts.reserve(hairCount);

	if( flags&DC_ST)
	{
		this->s.reserve(hairCount);
		this->t.reserve(hairCount);
	}
	if( flags&DC_SURFACE_NORMALS)
		this->surface_normals.reserve(hairCount);

	if( flags&DC_HAIRIDS)
		this->hairIds.reserve(hairCount);

	if( flags&DC_CLUMPID)
		this->clumpIds.reserve(hairCount);

	P.reserve(perVertexCacheSise);
	widths.reserve(perVertexCacheSise);

	if( flags&DC_NONOISE)
		P_nonoise.reserve(perVertexCacheSise);

	if( flags&DC_TANGENT_VECTOR)
		tangent_vector.reserve(perVertexCacheSise);

	bClump = true;
	if(bClump)
	{
		
		if( flags&DC_CLUMP_RADIUS)
			clump_radius.reserve(hairCount);
		if( flags&DC_CLUMP_NORMAL)
			clump_normal.reserve(hairCount);
		if( flags&DC_CLUMP_VECTOR)
			clump_vector.reserve(hairCount);
		if( flags&DC_VCLUMP)
			vclump.reserve(perVertexCacheSise);
		if( flags&DC_CLUMP_VECTORV)
			clump_vectorV.reserve(perVertexCacheSise);
	}

	if( flags&DC_HAIRLENGTHSCALE)
	{
		lengthjitter.reserve(hairCount);
	}

	if(this->bNormals)
	{
		normals.reserve(perVertexCacheSise);
	}
	if( adduv)
	{
		uvSets.resize(adduv->size());
		for(unsigned i=0; i<uvSets.size(); i++)
		{
			uvSet& uvs = uvSets[i];
			uvs.name = (*adduv)[i];
			uvs.u.reserve(hairCount);
			uvs.v.reserve(hairCount);

//			printf("%d: %s, %d, {%x, %x}\n", i, 
//				uvs.name.c_str(), 
//				hairCount, &uvs.u, &uvs.v);
		}
	}
}

void HairProceduralCurveOcs::SetHair(
	int i,	// hair index
	int id,	// hair id
	HAIRPARAMS& params, 
	HAIRVERTS& hair, 
	enRenderNormalSource renderNormalSource, 
	float RenderWidth,
	float widthBias
	)
{
	int vc = hair.vc();
	int vcvar = bCubic?(vc+2):vc;

	int needcc = nverts.size()+1;
	int needvc = P.size()+vc;
	int needvar = P.size()+vc;

	if( needcc > nverts.reserve_size()) return;
	if( needvar > P.reserve_size()) return;
	if( needvar > widths.reserve_size()) return;

	nverts.push_back(vcvar);

	if( hairIds.reserve_size())
		hairIds.push_back(id);
	if( clumpIds.reserve_size())
		clumpIds.push_back( params.clumpIndex);
	if( clump_radius.reserve_size())
		clump_radius.push_back( hair.clumpradius);
	if( clump_normal.reserve_size())
		clump_normal.push_back( hair.clump_normal);
	if( clump_vector.reserve_size())
		clump_vector.push_back( hair.clump_vector);
	if( lengthjitter.reserve_size())
		lengthjitter.push_back( params.lengthjitter);
	if( s.reserve_size())
		s.push_back( params.u);
	if( t.reserve_size())
		t.push_back( 1-params.v);
	if( surface_normals.reserve_size())
		surface_normals.push_back( params.normal);
#ifdef TEST_NAN
		if( _isnan(params.normal.x) || _isnan(params.normal.y) ||_isnan(params.normal.z))
			fprintf(stderr, "\nN is NAN!!! id = %d\n", id);
		if( _isnan(params.u) || _isnan(params.v))
			fprintf(stderr, "\nUV is NAN!!! id = %d\n", id);
		if( _isnan(hair.clump_normal.x) || _isnan(hair.clump_normal.y) ||_isnan(hair.clump_normal.z))
			fprintf(stderr, "\nclump_normal is NAN!!! id = %d\n", id);
		if( _isnan(hair.clump_vector.x) || _isnan(hair.clump_vector.y) ||_isnan(hair.clump_vector.z))
			fprintf(stderr, "\nclump_vector is NAN!!! id = %d\n", id);
#endif

//printf("%d N%d ", s.size(), this->uvSets.size());

	// adduv
	for(unsigned i=0; i<uvSets.size(); i++)
	{
		if( i>=params.uvSets.size()) continue;
		uvSet& uvs = this->uvSets[i];
		uvs.u.push_back( params.uvSets[i].x);
		uvs.v.push_back( 1-params.uvSets[i].y);
#ifdef TEST_NAN
		if( _isnan(params.uvSets[i].x) || _isnan(params.uvSets[i].y))
			fprintf(stderr, "\nUV is NAN!!! id = %d\n", id);
#endif
//		printf("%d:{%x, %x}(%d %d),   ", i, &uvs.u, &uvs.v, uvs.u.size(), uvs.v.size());
	}
//printf("\n");

	if(bCubic)
	{
		Math::Vec3f pt = hair.cvint.front();
		P.push_back(pt);
		if( P_nonoise.reserve_size())
			P_nonoise.push_back( hair.pt_nonoise.front());

#ifdef TEST_NAN
		if( _isnan(pt.x) || _isnan(pt.y) ||_isnan(pt.z))
			fprintf(stderr, "\nP is NAN!!! id = %d\n", id);
#endif
	}
	for(int v=0; v<vc; v++)
	{
		Math::Vec3f pt = hair.cvint[v];
		P.push_back(pt);
		if( P_nonoise.reserve_size())
			P_nonoise.push_back( hair.pt_nonoise[v]);
		#ifdef TEST_NAN
			if( _isnan(pt.x) || _isnan(pt.y) ||_isnan(pt.z))
				fprintf(stderr, "\nP is NAN!!! id = %d\n", id);
		#endif

		// widths
		float w = hair.vwidht[v];
		w = w*(float)RenderWidth;
		w += widthBias;
		if(w<0) w=0;
		widths.push_back(w);
		#ifdef TEST_NAN
			if( _isnan(w))
				fprintf(stderr, "\nW is NAN!!! id = %d\n", id);
		#endif

		// tangent_vector
		if( tangent_vector.reserve_size())
		{
			Math::Vec3f tv = tangentUfromBasis(hair.basas[v]);
			tangent_vector.push_back(tv);
		}
		#ifdef TEST_NAN
			if( _isnan(tv.x) || _isnan(tv.y) ||_isnan(tv.z))
				fprintf(stderr, "\ntv is NAN!!! id = %d\n", id);
		#endif
		// normal
		if( renderNormalSource==RNS_TANGENTU)
		{
			Math::Matrix4f& basis = hair.basas[v];
			normals.push_back(Math::Vec3f(basis[0].x, basis[0].y, basis[0].z));
			#ifdef TEST_NAN
				if( _isnan(normals.back().x) || _isnan(normals.back().y) ||_isnan(normals.back().z))
					fprintf(stderr, "\ntu is NAN!!! id = %d\n", id);
			#endif
		}
		// ������ ���� ���� ����� ��� ������!!!
		else if(renderNormalSource==RNS_CLUMPVECTOR && params.clumpIndex>=0)
		{
			normals.push_back(hair.clump_vector);
			#ifdef TEST_NAN
				if( _isnan(normals.back().x) || _isnan(normals.back().y) ||_isnan(normals.back().z))
					fprintf(stderr, "\nclump_vector is NAN!!! id = %d\n", id);
			#endif
		}

		if( vclump.reserve_size())
			vclump.push_back(hair.vclump[v]);

		#ifdef TEST_NAN
			if( _isnan(vclump.back()))
				fprintf(stderr, "\nvclump is NAN!!! id = %d\n", id);
		#endif

		// clump_vectorV
		if( vclump.reserve_size())
		{
			if( v<(int)hair.clump_vectorV.size())
				clump_vectorV.push_back( hair.clump_vectorV[v]);
			else
				clump_vectorV.push_back( hair.clump_vector);
		}
	}
	if( bCubic)
	{
		Math::Vec3f pt = hair.cvint.back();
		P.push_back(pt);
		if( P_nonoise.reserve_size())
			P_nonoise.push_back( hair.pt_nonoise.back());
		#ifdef TEST_NAN
			if( _isnan(pt.x) || _isnan(pt.y) ||_isnan(pt.z))
				fprintf(stderr, "\nP is NAN!!! id = %d\n", id);
		#endif
	}
}

int curvind = 0;
int curvecount = 0;
int curvertcount = 0;
int curmem = 0;

float catmullRomM[16] = {
	-0.5,  1.5, -1.5,  0.5 ,
	 1.0, -2.5,  2.0, -0.5 ,
	-0.5,  0.0,  0.5,  0.0 ,
	 0.0,  1.0,  0.0,  0.0 
	};
Math::Matrix4f catmullRom(catmullRomM);

void HairProceduralCurveOcs::Render(
	IPrman* prman,
	cls::IRender* render,
	float motionPhase, 
	std::map< std::string, cls::Param>& extraParameters, 
	DrawSynk* freeze
	)
{
//	printf("procedural=%x\n", this);

	if( !nverts.empty())
	{
		int mem = 0;
		render->Parameter("P", P);	mem += P.size()*P->stride;

		if( P_nonoise.reserve_size())
			render->Parameter("P_nonoise", P_nonoise);	mem += P_nonoise.size()*P_nonoise->stride;

		if( freeze)
		{
			HairProceduralCurveOcs* _freeze = (HairProceduralCurveOcs*)freeze;

			render->Parameter("__Pref", _freeze->P); mem += _freeze->P.size()*_freeze->P->stride;
		}

		bool bMemTest = true;
		if( bNormals)
		{
			render->Parameter("N", normals);	mem += normals.size()*normals->stride;
		}

		render->Parameter("SN", surface_normals);	mem += surface_normals.size()*surface_normals->stride;
		render->Parameter( "#width", widths);		mem += widths.size()*widths->stride;
		if( s.reserve_size())
			render->Parameter( "s", s),	mem += s.size()*s->stride;
		if( t.reserve_size())
			render->Parameter( "t", t), mem += t.size()*t->stride;
//printf("st: %d %d\n", s.size(), t.size());
//if( !bMemTest)
		if( hairIds.reserve_size())
			render->Parameter( "hairId", hairIds), mem += hairIds.size()*hairIds->stride;

		if( clumpIds.reserve_size())
			render->Parameter( "clumpId", clumpIds), mem += clumpIds.size()*clumpIds->stride;

		if( clump_radius.reserve_size())
			render->Parameter("clump_radius", cls::PT_FLOAT, clump_radius), mem += clump_radius.size()*clump_radius->stride;

//		if( bClump)
//		{
//if( !bMemTest)
			if( clump_normal.reserve_size())
				render->Parameter("clump_normal", cls::PT_VECTOR, clump_normal), mem += clump_normal.size()*clump_normal->stride;
//if( !bMemTest)
			if( clump_vector.reserve_size())
				render->Parameter("clump_vector", cls::PT_VECTOR, clump_vector), mem += clump_vector.size()*clump_vector->stride;
//if( !bMemTest)
			if( vclump.reserve_size())
				render->Parameter("clump", vclump), mem += vclump.size()*vclump->stride;
			if( clump_vectorV.reserve_size())
				render->Parameter("clump_vectorV", clump_vectorV), mem += clump_vectorV.size()*clump_vectorV->stride;
//		}

		if( lengthjitter.reserve_size())
			render->Parameter("hairlengthscale", lengthjitter), mem += lengthjitter.size()*lengthjitter->stride;
			

		// uvSets
		for(unsigned i=0; i<uvSets.size(); i++)
		{
			uvSet& uvs = uvSets[i];
//if( !bMemTest)
			if( uvs.u.reserve_size())
				render->Parameter("u_"+uvs.name, uvs.u), mem += uvs.u.size()*uvs.u->stride;
//if( !bMemTest)
			if( uvs.v.reserve_size())
				render->Parameter("v_"+uvs.name, uvs.v), mem += uvs.v.size()*uvs.v->stride;
//printf("%s: %d %d\n", uvs.name.c_str(), uvs.u.size(), uvs.v.size());
		}
		// baseUvSet
//if( !bMemTest)
		if( s.reserve_size())
			render->Parameter("u_"+baseUvSet, s), mem += s.size()*s->stride;
//if( !bMemTest)
		if( t.reserve_size())
			render->Parameter("v_"+baseUvSet, t), mem += t.size()*t->stride;

		// tangent_vector
//if( !bMemTest)
		if( tangent_vector.reserve_size())
			render->Parameter("tangentvector", tangent_vector), mem += tangent_vector.size()*tangent_vector->stride;

		// extraParameters
		std::map< std::string, cls::Param>::iterator it = extraParameters.begin();
		for( ; it != extraParameters.end(); it++)
		{
			std::string name = it->first;
			cls::Param& param = it->second;
			render->Parameter(name.c_str(), param);
		}

		// ��������

try
{
//		render->SystemCall(cls::SC_CURVES);
//printf( "curve %d: nhair: %d\n", curvind++, nverts.size());
		curvind++; curvecount+=nverts.size(); curvertcount+=P.size(); curmem += mem;


		/*/

void* mh = ::LoadLibrary("NTDll.dll");
00215     if ( mh )  {
00216       NtApi::NtQueryInformationProcess = (NtApi::__NtQueryInformationProcess)
00217         ::GetProcAddress((HINSTANCE)mh, "NtQueryInformationProcess");
00218     }

		::NtQueryInformationProcess(
			GetCurrentProcessHandle(), 

			ProcessBasicInformation,

			ProcessPooledUsageAndLimits, 

			);
		/*/



//fprintf(stderr, "{hs:%d c:%d v:%d m:%d(Kb) %s} \n", curvind, curvecount, curvertcount, (int)(curmem/(1024)), bCubic?"CUBIC":"lin");
#ifdef OCS_MEMTEST
fprintf(stderr, "cls::memory: %d\n", (int)(cls::IParameter::memory(0)/1024));
#endif
fflush(stderr);
}
catch(...)
{
fprintf( stderr, "BREAKDOWN HERE:!!!\ncurve %d: nhair: %d\n", curvind, nverts.size());
fflush(stderr);
}
	}
}

void HairProceduralCurveOcs::RenderCall(
	cls::IRender* render, 
	int motionBlurSamples, 
	float* motionBlurTimes
	)
{
	render->Parameter("#curves::interpolation", bCubic?"cubic":"linear");
	render->Parameter("#curves::wrap", "nonperiodic");
	render->Parameter("#curves::nverts", nverts);

	render->Parameter("@basis", "catmull-rom");

	render->SystemCall(cls::SC_CURVES);
};

// Render BLUR
void HairProceduralCurveOcs::RenderBlur(
	IPrman* prman,
	cls::IRender* render,
	std::vector<float> times,
	std::vector<DrawSynk*>& procedurals,
	std::map< std::string, cls::Param>& extraParameters, 
	DrawSynk* freeze
	)
{
	cls::renderBlurParser<> blurParser;
	for(int p=0; p<(int)times.size(); p++)
	{
//		render->SetCurrentMotionPhase(motionBlurTimes[i]);
		blurParser.SetCurrentPhase(times[p]);
		procedurals[p]->Render(NULL, &blurParser, 0, extraParameters, freeze);

//cls::PA< Math::Vec3f> geometry_vertex;
//blurParser.GetParameter("P", geometry_vertex);
//printf("%f: param=%x, {%f, %f, %f}\n", 
//times[p], geometry_vertex->data, 
//geometry_vertex[0].x, geometry_vertex[0].y, geometry_vertex[0].z);

	}
	render->SetCurrentMotionPhase(cls::nonBlurValue);
	blurParser.Render(render);

}
