#include "stdafx.h"
	#include "ocellaris/IRender.h"
#include "ocellaris/renderCacheImpl.h"
#include "ocellaris/renderBlurParser.h"
#include "ocellaris/renderExecutiveCacheImpl.h"
#include "HairProceduralSnOcs.h"
#include "HairPersonage.h"

//const float minHairLen = 0.00001f;
extern float min_hairLen;

struct HairRenderSnPersonageProc : public cls::IProcedural
{
	// ������ ����������
	virtual void Render(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		);

};
// ������ ����������
void HairRenderSnPersonageProc::Render(
	cls::IRender* render, 
	int motionBlurSamples, 
	float* motionBlurTimes
	)
{
}


HairProceduralSnOcs::HairProceduralSnOcs()
{
}
void HairProceduralSnOcs::setBaseUvSet( const char* baseUvSet)
{
	this->baseUvSet = baseUvSet;
}

HairProceduralSnOcs::~HairProceduralSnOcs() 
{
}

// INSTANCE
void HairProceduralSnOcs::setSnShape(int index, sn::ShapeShortcutPtr& ptr)
{
	snshapes[index] = ptr;
}
void HairProceduralSnOcs::setOcsShape(int index, cls::MInstance& cache)
{
	ocsshapes[index] = &cache;
}

void HairProceduralSnOcs::Init(
	int hairCount, int vertexInHair, enHairInterpolation interpolation, bool bNormals, std::vector<const char*>* adduv,
	int flags
	)
{
	instHairs.resize(hairCount);
}


void HairProceduralSnOcs::SetHair(
	int i,	// hair index
	int id,	// hair id
	HAIRPARAMS& params, 
	HAIRVERTS& hair, 
	enRenderNormalSource renderNormalSource, 
	float RenderWidth,
	float widthBias
	)
{
	HairPersonageData hairPersonageData;
	hairPersonageData.transforms	= hair.basas;
	hairPersonageData.parameters	= hair.vfactor;
	Math::Matrix4f matr;
	matr[0] = Math::Vec4f(params.tangentU,	0);
	matr[1] = Math::Vec4f(params.normal,	0);
	matr[2] = Math::Vec4f(params.tangentV,	0);
	matr[3] = Math::Vec4f(params.position,	1);
	if(false)
	{
		for(unsigned x=0; x<hair.basas.size(); x++)
		{
			Math::Matrix4f& matr = hair.basas[x];
			printf("matrix %d length=%f:\n", x, params.length);
			printf("%f %f %f %f\n", matr[0][0], matr[0][1], matr[0][2], matr[0][3]);
			printf("%f %f %f %f\n", matr[1][0], matr[1][1], matr[1][2], matr[1][3]);
			printf("%f %f %f %f\n", matr[2][0], matr[2][1], matr[2][2], matr[2][3]);
			printf("%f %f %f %f\n", matr[3][0], matr[3][1], matr[3][2], matr[3][3]);
		}
	}
	float scalelen = params.length*params.lengthjitter;
	if(scalelen<min_hairLen) scalelen=min_hairLen;

	hairPersonageData.transform		= matr;
	hairPersonageData.polar			= params.polar;
	hairPersonageData.inclination	= params.inclination;
	hairPersonageData.baseCurl		= params.baseCurl;
	hairPersonageData.tipCurl		= params.tipCurl;
	hairPersonageData.length		= scalelen;
	hairPersonageData.baseWidth		= params.baseWidth;
	hairPersonageData.tipWidth		= params.tipWidth;
	hairPersonageData.twist			= params.twist;
	hairPersonageData.u				= params.u;
	hairPersonageData.v				= params.v;
	hairPersonageData.id			= params.seed;
	hairPersonageData.animation		= params.scraggleFreq;

	this->setSnHair(
		i, params.instance, hairPersonageData);

}

void HairProceduralSnOcs::setSnHair(int cvhair, int instance, HairPersonageData& data)
{
	instHairs[cvhair] = data;
	instHairs[cvhair].instance = instance;
}

/*/
RtVoid fur_subdivfunc(RtPointer ptr, RtFloat detail)
{
	HairPersonage* personage = (HairPersonage*)ptr;
	try
	{
		// detail
		sn::ShapeShortcut& ss = *personage->ptr;
		ss.shaper->render(
			ss.shape, personage, personage->transform, personage->prman, &personage->cache);
	}
	catch(...)
	{
		fprintf(stderr, "Fur: fur_subdivfunc exception\n");
	}
}
RtVoid fur_freefunc(RtPointer ptr)
{
	try
	{
		HairPersonage* personage = (HairPersonage*)ptr;
		delete personage;
	}
	catch(...)
	{
		fprintf(stderr, "Fur: fur_freefunc exception\n");
	}
}
/*/



void HairProceduralSnOcs::Render(
	IPrman* prman,
	cls::IRender* _render,
	float motionPhase, 
	std::map< std::string, cls::Param>& extraParameters, 
	DrawSynk* freeze
	)
{
//	this->instHairsBlur[motionPhase] = this->instHairs;
	
	if(instHairs.size())
	{
		for(unsigned i=0; i<instHairs.size(); ++i)
		{
			const HairPersonageData& snhair = instHairs[i];

			Math::Matrix4f m = snhair.transform;
			m[0] *= snhair.length;
			m[1] *= snhair.length;
			m[2] *= snhair.length;


			if( snshapes.find(snhair.instance)!=snshapes.end())
			{
				sn::ShapeShortcutPtr snInst = snshapes[snhair.instance];

				if( !snInst->shaper)
					continue;

				HairPersonage* personage = new HairPersonage();

				personage->data = snhair;
				personage->ptr = snInst;
				personage->prman = prman;
				personage->transform = m;

				// ����������� __Pref!!!
				if( freeze)
				{
					HairProceduralSnOcs* _freeze = (HairProceduralSnOcs*)freeze;
					HairPersonage persfreeze;
					persfreeze.data = _freeze->instHairs[i];
					persfreeze.ptr  = snInst;
					persfreeze.prman = prman;

					Math::Matrix4f m = persfreeze.data.transform;
					m[0] *= persfreeze.data.length;
					m[1] *= persfreeze.data.length;
					m[2] *= persfreeze.data.length;
					persfreeze.transform = m;
			
					snInst->shaper->render(snInst->shape, &persfreeze, persfreeze.transform, NULL, &personage->cache);
				}

				if( personage->data.length < min_hairLen)
					continue;
			}

			if(ocsshapes.find(snhair.instance)!=ocsshapes.end())
			{
				const cls::MInstance* cache = this->ocsshapes[snhair.instance];
				if( cache && cache->isValid())
				{
					cls::IRender* render = _render;
					Math::Box3f box = cache->getBox();
					float maxDot = box.max.y;
					// scale �� maxDot
					float boxScale = 1/maxDot;
					if( maxDot<0.00001)
						boxScale = 1;
					Math::Matrix4f Tscale = Math::Matrix4f::id;
					Tscale.scale(Math::Vec4f(boxScale, boxScale, boxScale, 1));
					maxDot = 1;
//					m[0] *= boxScale;
//					m[1] *= boxScale;
//					m[2] *= boxScale;

					if( motionPhase!=cls::nonBlurValue)
					{
						bool bBegin = this->hairs.find(i)==this->hairs.end();

						HairRenderData& hairrenderdata = this->hairs[i];
						hairrenderdata.blurParser.SetCurrentPhase(motionPhase);
						render = &hairrenderdata.blurParser;
//						if( bBegin)
//							render->MotionBegin();
//						render->MotionPhaseBegin(motionPhase);
					}

					render->PushTransform();
					render->PushAttributes();
					render->AppendTransform(m);
					render->PopAttributes();

					render->PushAttributes();

					{
						cls::renderExecutiveCacheImpl execache;
						execache.init_transform(render);
						execache.init_attributes(_render);

						cls::PA<Math::Matrix4f> transforms(cls::PI_CONSTANT, snhair.transforms);
						cls::PA<float> parameters(cls::PI_CONSTANT, snhair.parameters);
						
						execache.Parameter("HairFilter::u", snhair.u);
						execache.Parameter("HairFilter::v", snhair.v);
						execache.Parameter("HairFilter::id", snhair.id);

						execache.Parameter("HairFilter::transforms", transforms);
						execache.Parameter("HairFilter::parameters", parameters);
						execache.Parameter("HairFilter::maxDot", maxDot);
						execache.Filter("HairFilter");

						execache.Attribute("global::phase", snhair.animation);

						{
							execache.PushTransform();
							execache.PushAttributes();
							((cls::IRender*)&execache)->AppendTransform(Tscale);
							execache.PopAttributes();
								cache->Render(&execache);
							execache.PopTransform();
						}

						execache.Render(render);
					}

					render->PopAttributes();
					render->PopTransform();

//					if( motionPhase!=cls::nonBlurValue)
//						render->MotionPhaseEnd();

//					hairrenderdata.blurParser.SetCurrentMotionPhase(cls::nonBlurValue);
				}
			}
		
		}
	}
}

void HairProceduralSnOcs::RenderCall(
	cls::IRender* render, 
	int motionBlurSamples, 
	float* motionBlurTimes
	)
{
	std::map< int, HairRenderData>::iterator it = hairs.begin();
	for( ; it != hairs.end(); it++)
	{
		HairRenderData& hairrenderdata = it->second;

//		hairrenderdata.blurParser.MotionEnd();
//		hairrenderdata.blurParser.SetCurrentPhase(cls::nonBlurValue);
		hairrenderdata.blurParser.Render(render);
	}
};

// Render BLUR
void HairProceduralSnOcs::RenderBlur(
	IPrman* prman,
	cls::IRender* render,
	std::vector<float> times,
	std::vector<DrawSynk*>& procedurals,
	std::map< std::string, cls::Param>& extraParameters, 
	DrawSynk* freeze
	)
{
	std::map< int, HairRenderData>::iterator it = hairs.begin();
	for( ; it != hairs.end(); it++)
	{
		HairRenderData& hairrenderdata = it->second;

		hairrenderdata.blurParser.SetCurrentMotionPhase(cls::nonBlurValue);
		hairrenderdata.blurParser.Render(render);
	}

	/*/
	cls::renderBlurParser<> blurParser;
	for(int p=0; p<(int)times.size(); p++)
	{
		blurParser.SetCurrentPhase(times[p]);
		procedurals[p]->Render(NULL, &blurParser, 0, extraParameters, freeze);

	}
	render->SetCurrentMotionPhase(cls::nonBlurValue);
	blurParser.Render(render);
	/*/
}
