#pragma once

#include "HairProcessor.h"
#include "mathNpixar\IPrman.h"
#ifdef OCELLARIS_INSTANCE
#include "ocellaris/IRender.h"
#include "ocellaris/renderCacheImpl.h"
#endif// OCELLARIS_INSTANCE

#pragma warning ( disable:4275)

//! HairProceduralSn
class HAIRMATH_API HairProceduralSn : public DrawSynk
{
public:
//	HairProcessor* hairs;
public:
    HairProceduralSn();
    ~HairProceduralSn();

	void setBaseUvSet( const char* baseUvSet);

	static void RenderInstanceMotion( 
		std::vector<HairProceduralSn*>& procedurals, 
		std::vector<float>& times, 
		IPrman* prman, 
		cls::IRender* render,
		HairProceduralSn* freeze
		);

	// INSTANCE
	virtual void setSnShape(int index, sn::ShapeShortcutPtr& ptr);
	virtual void setOcsShape(int index, cls::MInstance& cache);

	// ������������� �������
	virtual void Init(
		int hairCount, 
		int vertexInHair, 
		enHairInterpolation interpolation, 
		bool bNormals, 
		std::vector<const char*>* adduv, 
		int flags
		);
	// ���������� ��� ������ ������
	virtual void SetHair(
		int hairindex, 
		int hairid,
		HAIRPARAMS& params, 
		HAIRVERTS& hair,
		enRenderNormalSource renderNormalSource, 
		float RenderWidth, 
		float widthBias
		);


	// Render
	void Render(
		IPrman* prman,
		cls::IRender* render,
		float motionPhase, 
		std::map< std::string, cls::Param>& extraParameters, 
		DrawSynk* freeze
		);
	// Render BLUR
	virtual void RenderBlur(
		IPrman* prman,
		cls::IRender* render,
		std::vector<float> times,
		std::vector<DrawSynk*>& procedurals,
		std::map< std::string, cls::Param>& extraParameters, 
		DrawSynk* freeze
		);

	/////////////////////////////////////////////
	// ��� ��� ��������!!!
	/*/
	virtual void sethair(int cvhair, int vertcount, float u, float v, int id, const Math::Vec3f& p, const Math::Vec3f& n, Math::Vec2f* adduv=NULL);
	virtual void sethairclumpdata(int cvhair, const Math::Vec3f& clump_norm, const Math::Vec3f& clump_vect);
	virtual void sethairvertex(int cvhair, int vertindex, const Math::Vec3f& pt);
	virtual void sethaircolor(int cvhair, int vertindex, const Math::Vec3f& pt);
	virtual void sethairwidht(int cvhair, int vertindex, float val);
	virtual void sethairnormal(int cvhair, int vertindex, const Math::Vec3f& normal);
	virtual void sethairclump_vectorV(int cvhair, int vertindex, const Math::Vec3f& clump_vectorV);
//	virtual void InitSn(int hairCount);
	/*/

	virtual void setSnHair(int cvhair, int instance, HairPersonageData& data);
//	const Math::Vec3f& p, const Math::Vec3f& n, const Math::Vec3f& tangentu, const Math::Vec3f& tangentv,
//		float polar, float inclination, float baseCurl, float tipCurl, float length, 
//		float baseWidth, float tipWidth, float twist, float u, float v);

private:
	std::string baseUvSet;
//	bool bCubic;
//	bool diceHair;
//	int perVertexCacheSise;

	/*/
	int* hairoffset;
	std::vector<int> dataoffset;
	RtInt *nverts;		// ����� ����. � ������ ������ nverts[haircount];

	int currentOffset;
	int currentDataOffset;

	// PER VERTEX
	RtPoint* cvs;
	RtFloat* widths;
	bool bNormals;
	std::vector< Math::Vec3f> normals;
	// PER HAIR
	RtFloat* s, *t;
	RtNormal* surface_normals;
//	RtFloat* seed;
	RtInt* hairIds;

	int vertexInHair;
	int hairCount;

	bool bClump;
	std::vector<Math::Vec3f> clump_normal;		// per hair
	std::vector<Math::Vec3f> clump_vector;		// per hair

	std::vector<Math::Vec3f> clump_vectorV;		// per hair vertex

	struct uvSet
	{
		std::string name;
		std::vector<float> u;
		std::vector<float> v;
	};
	std::vector< uvSet > uvSets;
	/*/

	// INSTANCE
	/*/
	struct SnHair
	{
		Math::Matrix4f transform;
		float polar, inclination, baseCurl, tipCurl, length;
		float baseWidth, tipWidth, twist;
		float u, v;
	};
	/*/
	mutable std::map< int, sn::ShapeShortcutPtr > snshapes;
//	sn::ShapeShortcutPtr snInst;
	std::vector<HairPersonageData> instHairs;

	std::map<int, cls::MInstance*> ocsshapes;

};

