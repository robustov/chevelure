#pragma once

#include "HairProcessor.h"
#include "mathNpixar\IPrman.h"

#include "ocellaris/IRender.h"

#pragma warning ( disable:4275)

//! HairProceduralTubes
class HAIRMATH_API HairProceduralTubes : public DrawSynk
{
public:
//	HairProcessor* hairs;
public:
    HairProceduralTubes();
	HairProceduralTubes(const HairProceduralTubes& arg);
    ~HairProceduralTubes();

	void setBaseUvSet( const char* baseUvSet);

	void setSubdiv(bool bSubdiv);
public:
	// ������������� �������
	virtual void Init(
		int hairCount, 
		int vertexInHair, 
		enHairInterpolation interpolation, 
		bool bNormals, 
		std::vector<const char*>* adduv, 
		int flags
		);
	// ���������� ��� ������ ������
	virtual void SetHair(
		int hairindex, 
		int hairid,
		HAIRPARAMS& params, 
		HAIRVERTS& hair,
		enRenderNormalSource renderNormalSource, 
		float RenderWidth, 
		float widthBias
		);
	// Render
	virtual void Render(
		IPrman* prman,
		cls::IRender* render,
		float motionPhase, 
		std::map< std::string, cls::Param>& extraParameters, 
		DrawSynk* freeze
		);
	void RenderCall(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		);


	// Render BLUR
	virtual void RenderBlur(
		IPrman* prman,
		cls::IRender* render,
		std::vector<float> times,
		std::vector<DrawSynk*>& procedurals,
		std::map< std::string, cls::Param>& extraParameters, 
		DrawSynk* freeze
		);

private:
	bool bCubic;
	int vertexInHair;

	std::string baseUvSet;

	cls::PA<int> nverts;
	cls::PA<int> iverts;
	cls::PA<Math::Vec3f> N;
	cls::PA<Math::Vec3f> P;
	cls::PA<float> s;
	cls::PA<float> t;

	bool bClump;
	std::vector<Math::Vec3f> clump_normal;		// per hair
	std::vector<Math::Vec3f> clump_vector;		// per hair
	std::vector<Math::Vec3f> clump_vectorV;		// per hair vertex

	struct uvSet
	{
		std::string name;
		std::vector<float> u;
		std::vector<float> v;
	};
	std::vector< uvSet > uvSets;

};

