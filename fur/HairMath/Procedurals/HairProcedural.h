#pragma once

#include "HairProcessor.h"
#include "mathNpixar\IPrman.h"
#ifdef OCELLARIS_INSTANCE
#include "ocellaris/IRender.h"
#include "ocellaris/renderCacheImpl.h"
#endif// OCELLARIS_INSTANCE

#pragma warning ( disable:4275)

//! HairProcedural
class HAIRMATH_API HairProcedural : public DrawSynk
{
public:
//	HairProcessor* hairs;
public:
    HairProcedural();
    ~HairProcedural();

	void setBaseUvSet( const char* baseUvSet);

	static void RenderInstanceMotion( 
		std::vector<HairProcedural>& procedurals, 
		std::vector<float>& times, 
		IPrman* prman);

	void Render(
		IPrman* prman
#ifdef OCELLARIS_INSTANCE
		,cls::IRender* render
#endif// OCELLARIS_INSTANCE
		);
public:
	virtual void Init(int hairCount, int vertexInHair, enHairInterpolation interpolation, bool bNormals, std::vector<const char*>* adduv);

	// ���������� ��� ������ ������
	virtual void SetHair(
		int hairindex, 
		int hairid,
		HAIRPARAMS& params, 
		HAIRVERTS& hair,
		enRenderNormalSource renderNormalSource, 
		float RenderWidth){};

	virtual void sethair(int cvhair, int vertcount, float u, float v, int id, const Math::Vec3f& p, const Math::Vec3f& n, Math::Vec2f* adduv=NULL);
	virtual void sethairclumpdata(int cvhair, const Math::Vec3f& clump_norm, const Math::Vec3f& clump_vect);
	virtual void sethairvertex(int cvhair, int vertindex, const Math::Vec3f& pt);
	virtual void sethaircolor(int cvhair, int vertindex, const Math::Vec3f& pt);
	virtual void sethairwidht(int cvhair, int vertindex, float val);
	virtual void sethairnormal(int cvhair, int vertindex, const Math::Vec3f& normal);
	virtual void sethairclump_vectorV(int cvhair, int vertindex, const Math::Vec3f& clump_vectorV);

	// INSTANCE
	virtual void InitSn(int hairCount);
	virtual void setSnShape(int index, sn::ShapeShortcutPtr& ptr);
	#ifdef OCELLARIS_INSTANCE
	virtual void setOcsShape(int index, cls::MInstance& cache);
	#endif// OCELLARIS_INSTANCE
	virtual void setSnHair(int cvhair, int instance, HairPersonageData& data);
//	const Math::Vec3f& p, const Math::Vec3f& n, const Math::Vec3f& tangentu, const Math::Vec3f& tangentv,
//		float polar, float inclination, float baseCurl, float tipCurl, float length, 
//		float baseWidth, float tipWidth, float twist, float u, float v);

private:
	std::string baseUvSet;
	bool bCubic;
//	bool diceHair;
	int perVertexCacheSise;

	int* hairoffset;
	std::vector<int> dataoffset;
	RtInt *nverts;		// ����� ����. � ������ ������ nverts[haircount];

	int currentOffset;
	int currentDataOffset;

	// PER VERTEX
	RtPoint* cvs;
	RtFloat* widths;
	bool bNormals;
	std::vector< Math::Vec3f> normals;
	// PER HAIR
	RtFloat* s, *t;
	RtNormal* surface_normals;
//	RtFloat* seed;
	RtInt* hairIds;

	int vertexInHair;
	int hairCount;

	bool bClump;
	std::vector<Math::Vec3f> clump_normal;		// per hair
	std::vector<Math::Vec3f> clump_vector;		// per hair

	std::vector<Math::Vec3f> clump_vectorV;		// per hair vertex

	struct uvSet
	{
		std::string name;
		std::vector<float> u;
		std::vector<float> v;
	};
	std::vector< uvSet > uvSets;

	// INSTANCE
	/*/
	struct SnHair
	{
		Math::Matrix4f transform;
		float polar, inclination, baseCurl, tipCurl, length;
		float baseWidth, tipWidth, twist;
		float u, v;
	};
	/*/
	mutable std::map< int, sn::ShapeShortcutPtr > snshapes;
//	sn::ShapeShortcutPtr snInst;
	std::vector<HairPersonageData> instHairs;

#ifdef OCELLARIS_INSTANCE
	std::vector<cls::MInstance*> ocsshapes;
#endif// OCELLARIS_INSTANCE

};

