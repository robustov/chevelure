#include "stdafx.h"
#include "HairProceduralSn.h"
#include "HairPersonage.h"
#include "ocellaris/IRender.h"
#include "ocellaris/renderExecutiveCacheImpl.h"
#include "ocellaris/renderBlurParser.h"

//const float minHairLen = 0.00001f;
extern float min_hairLen;


HairProceduralSn::HairProceduralSn()
{
//	this->interpolation = interpolation;
//	this->diceHair = diceHair;
	/*/
	perVertexCacheSise = 0;
	currentOffset = 0;
	currentDataOffset = 0;
	vertexInHair = 0;
	hairCount = 0;
	bClump= false;
	bCubic= false;

	hairoffset = 0;
	cvs = NULL;
	widths = NULL;
	nverts = NULL;
	s = t = 0;
	surface_normals = NULL;
//	seed = 0;
	hairIds = 0;
	bNormals = false;
	/*/
}
void HairProceduralSn::setBaseUvSet( const char* baseUvSet)
{
	this->baseUvSet = baseUvSet;
}

HairProceduralSn::~HairProceduralSn() 
{
	/*/
	delete widths;
	delete[] cvs;
	delete nverts;
	delete hairoffset;
	delete s; 
	delete t;
	delete[] surface_normals;
//	delete seed;
	delete hairIds;
	/*/
}

// INSTANCE
void HairProceduralSn::setSnShape(int index, sn::ShapeShortcutPtr& ptr)
{
	snshapes[index] = ptr;
}
void HairProceduralSn::setOcsShape(int index, cls::MInstance& cache)
{
	ocsshapes[index] = &cache;
}

void HairProceduralSn::Init(
	int hairCount, int vertexInHair, enHairInterpolation interpolation, bool bNormals, std::vector<const char*>* adduv,
	int flags
	)
{
	instHairs.resize(hairCount);

	/*/
	this->bCubic = false;
	if( interpolation != HI_LINIAR)
		this->bCubic = true;
	this->bNormals = bNormals;

//this->bCubic = true;
//fprintf(stderr, "HairProceduralSn::Init hairs.interpolation=%d bCubic = %d\n", hairs.interpolation, (int)this->bCubic);

	this->vertexInHair = vertexInHair;
	if( bCubic)
		this->vertexInHair += 2;
	this->perVertexCacheSise = this->vertexInHair*hairCount+10;

	this->currentOffset = 0;
	this->currentDataOffset = 0;
	this->hairoffset = new int[hairCount];
	this->nverts = new RtInt[hairCount];
	this->dataoffset.resize(hairCount);

	this->s = new RtFloat[hairCount];
	this->t = new RtFloat[hairCount];
	this->surface_normals = new RtNormal[hairCount];
//	this->seed = new RtFloat[hairCount];
	this->hairIds = new RtInt[hairCount];

	cvs = new RtPoint[perVertexCacheSise];
	widths = new RtFloat[perVertexCacheSise];

	this->hairCount = 0;

	bClump = true;
	if(bClump)
	{
		clump_normal.resize(hairCount);
		clump_vector.resize(hairCount);
		clump_vectorV.resize(perVertexCacheSise, Math::Vec3f(0, 0, 0));
	}
	if(this->bNormals)
	{
		normals.resize(perVertexCacheSise);
	}
	if( adduv)
	{
		uvSets.resize(adduv->size());
		for(unsigned i=0; i<uvSets.size(); i++)
		{
			uvSets[i].name = (*adduv)[i];
			uvSets[i].u.resize(hairCount);
			uvSets[i].v.resize(hairCount);
		}
	}
//fprintf(stderr, ": %x HairProceduralSn::Init h=%d vc=%d sum=%d\n", this, hairCount, vertexInHair, perVertexCacheSise);
	/*/
}


void HairProceduralSn::SetHair(
	int i,	// hair index
	int id,	// hair id
	HAIRPARAMS& params, 
	HAIRVERTS& hair, 
	enRenderNormalSource renderNormalSource, 
	float RenderWidth,
	float widthBias
	)
{
	HairPersonageData hairPersonageData;
	hairPersonageData.transforms	= hair.basas;
	hairPersonageData.parameters	= hair.vfactor;
	Math::Matrix4f matr;
	matr[0] = Math::Vec4f(params.tangentU,	0);
	matr[1] = Math::Vec4f(params.normal,	0);
	matr[2] = Math::Vec4f(params.tangentV,	0);
	matr[3] = Math::Vec4f(params.position,	1);
	if(false)
	{
		for(unsigned x=0; x<hair.basas.size(); x++)
		{
			Math::Matrix4f& matr = hair.basas[x];
			printf("matrix %d length=%f:\n", x, params.length);
			printf("%f %f %f %f\n", matr[0][0], matr[0][1], matr[0][2], matr[0][3]);
			printf("%f %f %f %f\n", matr[1][0], matr[1][1], matr[1][2], matr[1][3]);
			printf("%f %f %f %f\n", matr[2][0], matr[2][1], matr[2][2], matr[2][3]);
			printf("%f %f %f %f\n", matr[3][0], matr[3][1], matr[3][2], matr[3][3]);
		}
	}
	float scalelen = params.length*params.lengthjitter;
	if(scalelen<min_hairLen) scalelen=min_hairLen;

	hairPersonageData.transform		= matr;
	hairPersonageData.polar			= params.polar;
	hairPersonageData.inclination	= params.inclination;
	hairPersonageData.baseCurl		= params.baseCurl;
	hairPersonageData.tipCurl		= params.tipCurl;
	hairPersonageData.length		= scalelen;
	hairPersonageData.baseWidth		= params.baseWidth;
	hairPersonageData.tipWidth		= params.tipWidth;
	hairPersonageData.twist			= params.twist;
	hairPersonageData.u				= params.u;
	hairPersonageData.v				= params.v;
	hairPersonageData.id			= params.seed;
	hairPersonageData.animation		= params.scraggleFreq;

	this->setSnHair(
		i, params.instance, hairPersonageData);
}

void HairProceduralSn::setSnHair(int cvhair, int instance, HairPersonageData& data)
{
	instHairs[cvhair] = data;
	instHairs[cvhair].instance = instance;
}

RtVoid fur_subdivfunc(RtPointer ptr, RtFloat detail)
{
	HairPersonage* personage = (HairPersonage*)ptr;
	try
	{
		// detail
		sn::ShapeShortcut& ss = *personage->ptr;
		ss.shaper->render(
			ss.shape, personage, personage->transform, personage->prman, &personage->cache);
	}
	catch(...)
	{
		fprintf(stderr, "Fur: fur_subdivfunc exception\n");
	}
}
RtVoid fur_freefunc(RtPointer ptr)
{
	try
	{
		HairPersonage* personage = (HairPersonage*)ptr;
		delete personage;
	}
	catch(...)
	{
		fprintf(stderr, "Fur: fur_freefunc exception\n");
	}
}



void HairProceduralSn::Render(
	IPrman* prman,
	cls::IRender* render,
	float motionPhase, 
	std::map< std::string, cls::Param>& extraParameters, 
	DrawSynk* freeze
	)
{
	if(instHairs.size())
	{
		for(unsigned i=0; i<instHairs.size(); ++i)
		{
			const HairPersonageData& snhair = instHairs[i];
			if( snshapes.find( snhair.instance)!=snshapes.end())
			{
				sn::ShapeShortcutPtr snInst = snshapes[snhair.instance];

				if( !snInst->shaper)
					continue;

				HairPersonage* personage = new HairPersonage();

				personage->data = snhair;
				personage->ptr = snInst;
				personage->prman = prman;

				Math::Matrix4f m = snhair.transform;
				m[0] *= personage->data.length;
				m[1] *= personage->data.length;
				m[2] *= personage->data.length;
				personage->transform = m;

				// ����������� __Pref!!!
				if( freeze)
				{
					HairProceduralSn* _freeze = (HairProceduralSn*)freeze;
					HairPersonage persfreeze;
					persfreeze.data = _freeze->instHairs[i];
					persfreeze.ptr  = snInst;
					persfreeze.prman = prman;

					Math::Matrix4f m = persfreeze.data.transform;
					m[0] *= persfreeze.data.length;
					m[1] *= persfreeze.data.length;
					m[2] *= persfreeze.data.length;
					persfreeze.transform = m;
			
					snInst->shaper->render(snInst->shape, &persfreeze, persfreeze.transform, NULL, &personage->cache);
				}

				if( personage->data.length < min_hairLen)
					continue;

	//			prman->RiTransformBegin();
	//			prman->RiConcatTransform(m);

				RtBound bound;
				Math::Box3f box;
				snInst->shaper->getDirtyBox( snInst->shape, personage->transform, box);

				IPrman::copy(bound, box);
				prman->RiProcedural(personage, bound, fur_subdivfunc, fur_freefunc);
	//			prman->RiTransformEnd();
			}
		}

		for(unsigned i=0; i<instHairs.size(); ++i)
		{
			const HairPersonageData& snhair = instHairs[i];
			if( ocsshapes.find( snhair.instance)!=ocsshapes.end())
			{
				cls::MInstance* inst = ocsshapes[snhair.instance];
				if( inst && inst->isValid())
				{
					Math::Matrix4f m = snhair.transform;
					m[0] *= snhair.length;
					m[1] *= snhair.length;
					m[2] *= snhair.length;
					Math::Box3f box = inst->getBox();
					float maxDot = box.max.y;
					// scale �� maxDot
					float boxScale = 1/maxDot;
					if( maxDot<0.00001)
						boxScale = 1;
					Math::Matrix4f Tscale = Math::Matrix4f::id;
					Tscale.scale(Math::Vec4f(boxScale, boxScale, boxScale, 1));
					maxDot = 1;
//					m[0] *= boxScale;
//					m[1] *= boxScale;
//					m[2] *= boxScale;

					render->PushTransform();
						render->PushAttributes();
						render->AppendTransform(m);
						render->PopAttributes();

					render->PushAttributes();
					{
						cls::renderExecutiveCacheImpl execache(render);
						cls::PA<Math::Matrix4f> transforms(cls::PI_CONSTANT, snhair.transforms);
						cls::PA<float> parameters(cls::PI_CONSTANT, snhair.parameters);
						
						execache.Parameter("HairFilter::u", snhair.u);
						execache.Parameter("HairFilter::v", snhair.v);
						execache.Parameter("HairFilter::id", snhair.id);
						execache.Parameter("HairFilter::transforms", transforms);
						execache.Parameter("HairFilter::parameters", parameters);
						execache.Parameter("HairFilter::maxDot", maxDot);
						execache.Filter("HairFilter");

						execache.Attribute("global::phase", snhair.animation);
						{
							execache.PushTransform();
							execache.PushAttributes();
							((cls::IRender*)&execache)->AppendTransform(Tscale);
							execache.PopAttributes();
								inst->Render(&execache);
							execache.PopTransform();
						}
						execache.Render(render);
					}
					render->PopAttributes();

					render->PopTransform();
				}
			}
		}
	}
}

// Render BLUR
void HairProceduralSn::RenderBlur(
	IPrman* prman,
	cls::IRender* render,
	std::vector<float> times,
	std::vector<DrawSynk*>& procedurals,
	std::map< std::string, cls::Param>& extraParameters, 
	DrawSynk* freeze
	)
{
	std::vector<HairProceduralSn*> proceduralsSn(times.size());
	for(int p=0; p<(int)times.size(); p++)
	{
		proceduralsSn[p] = (HairProceduralSn*)procedurals[p];
	}
	HairProceduralSn* _freeze = NULL;
	if(freeze) _freeze = (HairProceduralSn*)freeze;

	// �������!!!
	HairProceduralSn::RenderInstanceMotion(
		proceduralsSn, times, prman, render, _freeze);
}

void HairProceduralSn::RenderInstanceMotion( 
	std::vector<HairProceduralSn*>& procedurals, 
	std::vector<float>& times,
	IPrman* prman, 
	cls::IRender* _render, 
	HairProceduralSn* freeze
	)
{
	// ��� ������ �����
	std::vector<HairPersonage> personages;
	HairProceduralSn& startProcedureal = *procedurals[0];

	{
		// �� ������� �������
		for(unsigned i=0; i<startProcedureal.instHairs.size(); ++i)
		{
			const HairPersonageData& snhair = startProcedureal.instHairs[i];
			if( startProcedureal.snshapes.find( snhair.instance)==startProcedureal.snshapes.end())
				continue;

			sn::ShapeShortcutPtr snInst = startProcedureal.snshapes[snhair.instance];

			if( !snInst->shaper)
				continue;

			cls::renderCacheImpl<> cache;
			// ����������� __Pref!!!
			if( freeze)
			{
				HairPersonage persfreeze;
				persfreeze.data = freeze->instHairs[i];
				persfreeze.ptr  = snInst;
				persfreeze.prman = prman;

				Math::Matrix4f m = persfreeze.data.transform;
				m[0] *= persfreeze.data.length;
				m[1] *= persfreeze.data.length;
				m[2] *= persfreeze.data.length;
				persfreeze.transform = m;

				snInst->shaper->render(snInst->shape, &persfreeze, persfreeze.transform, NULL, &cache);
			}

			bool bValid = true;
			personages.resize(procedurals.size());
			unsigned z;
			for(z=0; z<personages.size(); z++)
			{
				personages[z].data = procedurals[z]->instHairs[i];
				personages[z].ptr  = snInst;
				personages[z].prman = prman;
	//			printf("%d(%d) length=%f:\n", i, z, personages[z].data.length);
				if( personages[z].data.length < min_hairLen)
					bValid = false;
			}
			if( !bValid) 
				continue;

			prman->RiAttributeBegin();

			// ������
			prman->setRenderMode(IPrman::RENDERMODE_SHADER);
			snInst->shaper->render(snInst->shape, &personages[0], Math::Matrix4f::id, prman, NULL);
			
			// ���������
	//		prman->RiTransformBegin();
	//		prman->RiMotionBeginV(times.size(), &times[0]);
			for(z=0; z<personages.size(); z++)
			{
				const HairPersonageData& snhair = procedurals[z]->instHairs[i];
				Math::Matrix4f m = snhair.transform;
				m[0] *= snhair.length;
				m[1] *= snhair.length;
				m[2] *= snhair.length;
				personages[z].transform = m;
	//			prman->RiConcatTransform(m);
			}
	//		prman->RiMotionEnd();

			// ���������
			prman->setRenderMode(IPrman::RENDERMODE_GEOMETRY);
			prman->RiMotionBeginV((int)times.size(), &times[0]);
			for(z=0; z<personages.size(); z++)
				snInst->shaper->render(snInst->shape, &personages[z], personages[z].transform, prman, &cache);
			prman->RiMotionEnd();

	//		prman->RiTransformEnd();
			prman->RiAttributeEnd();
			prman->setRenderMode(IPrman::RENDERMODE_ALL);
		}
	}



	// OCS
	{
		for(unsigned i=0; i<startProcedureal.instHairs.size(); ++i)
		{
			const HairPersonageData& start_snhair = startProcedureal.instHairs[i];
			if( startProcedureal.ocsshapes.find( start_snhair.instance)==startProcedureal.ocsshapes.end())
				continue;

			cls::MInstance* inst = startProcedureal.ocsshapes[start_snhair.instance];
			if( inst && inst->isValid())
			{

				cls::renderBlurParser<cls::TAttributeStack_forblur, cls::TTransformStack_minimal> blurParser;
				Math::Matrix4f nonblurworldtransform;

				for(int z=0; z<(int)times.size(); z++)
				{
					const HairPersonageData& snhair = procedurals[z]->instHairs[i];

					float motionPhase = times[z];
					blurParser.SetCurrentPhase(motionPhase);
					cls::IRender* render = &blurParser;

					Math::Matrix4f m = snhair.transform;
					m[0] *= snhair.length;
					m[1] *= snhair.length;
					m[2] *= snhair.length;
					Math::Box3f box = inst->getBox();
					float maxDot = box.max.y;
					// scale �� maxDot
					float boxScale = 1/maxDot;
					if( maxDot<0.00001)
						boxScale = 1;
					Math::Matrix4f Tscale = Math::Matrix4f::id;
					Tscale.scale(Math::Vec4f(boxScale, boxScale, boxScale, 1));
					maxDot = 1;

					render->PushTransform();
						render->PushAttributes();
						render->AppendTransform(m);
						render->PopAttributes();
					if(z==0)
					{
						render->GetParameter("worldtransform", nonblurworldtransform);
					}

					render->PushAttributes();
					{
						cls::renderExecutiveCacheImpl execache(render);
printf("phase %d:\n", z);
						execache.init_transform(nonblurworldtransform);
						execache.init_attributes(_render);
						cls::PA<Math::Matrix4f> transforms(cls::PI_CONSTANT, snhair.transforms);
						cls::PA<float> parameters(cls::PI_CONSTANT, snhair.parameters);
						
						execache.Parameter("HairFilter::u", snhair.u);
						execache.Parameter("HairFilter::v", snhair.v);
						execache.Parameter("HairFilter::id", snhair.id);
						execache.Parameter("HairFilter::transforms", transforms);
						execache.Parameter("HairFilter::parameters", parameters);
						execache.Parameter("HairFilter::maxDot", maxDot);
						execache.Filter("HairFilter");

						execache.Attribute("global::phase", snhair.animation);
						{
							execache.PushTransform();
							execache.PushAttributes();
							((cls::IRender*)&execache)->AppendTransform(Tscale);
							execache.PopAttributes();
								inst->Render(&execache);
							execache.PopTransform();
						}
						execache.Render(render);
					}
					render->PopAttributes();

					render->PopTransform();
				}

				// ���������
				blurParser.Render(_render);
			}
		}
	}

}
