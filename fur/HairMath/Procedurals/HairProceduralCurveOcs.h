#pragma once

#include "HairProcessor.h"
#include "mathNpixar\IPrman.h"
#include "ocellaris/IRender.h"

#pragma warning ( disable:4275)

//! HairProceduralCurve
class HAIRMATH_API HairProceduralCurveOcs : public DrawSynk
{
public:
//	HairProcessor* hairs;
public:
    HairProceduralCurveOcs();
	HairProceduralCurveOcs(const HairProceduralCurveOcs& arg);
    ~HairProceduralCurveOcs();

	void setBaseUvSet( const char* baseUvSet);

public:
	// ������������� �������
	virtual void Init(
		int hairCount, 
		int vertexInHair, 
		enHairInterpolation interpolation, 
		bool bNormals, 
		std::vector<const char*>* adduv, 
		int flags
		);
	// ���������� ��� ������ ������
	virtual void SetHair(
		int hairindex, 
		int hairid,
		HAIRPARAMS& params, 
		HAIRVERTS& hair,
		enRenderNormalSource renderNormalSource, 
		float RenderWidth, 
		float widthBias
		);


	// Render
	void Render(
		IPrman* prman,
		cls::IRender* render,
		float motionPhase, 
		std::map< std::string, cls::Param>& extraParameters, 
		DrawSynk* freeze
		);
	void RenderCall(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		);


	// Render BLUR
	virtual void RenderBlur(
		IPrman* prman,
		cls::IRender* render,
		std::vector<float> times,
		std::vector<DrawSynk*>& procedurals,
		std::map< std::string, cls::Param>& extraParameters, 
		DrawSynk* freeze
		);


private:
	bool bCubic;
	// nverts
	cls::PA<int> nverts;						// HAIRS

	cls::PA<Math::Vec3f> P;						// �����
	cls::PA<Math::Vec3f> P_nonoise;				// nonoise
	cls::PA<float> widths;						// PI_CONSTANT

	bool bNormals;
	cls::PA<Math::Vec3f> normals;				// PI_VARYING

	cls::PA<int> hairIds;						// PI_CONSTANT
	cls::PA<int> clumpIds;						// PI_CONSTANT

	cls::PA<Math::Vec3f> surface_normals;		// PI_CONSTANT

	cls::PA<float> lengthjitter;				// PI_CONSTANT

	// baseUvSet
	std::string baseUvSet;
	cls::PA<float> s, t;						// PI_CONSTANT

	// Clump
	bool bClump;
	cls::PA<float> clump_radius;				// PI_CONSTANT
	cls::PA<Math::Vec3f> clump_normal;			// PI_CONSTANT
	cls::PA<Math::Vec3f> clump_vector;			// PI_CONSTANT
	cls::PA<Math::Vec3f> clump_vectorV;			// PI_VARYING
	cls::PA<float> vclump;						// PI_VARYING
	cls::PA<Math::Vec3f> tangent_vector;		// PI_VARYING
	
	// uvSets
	struct uvSet
	{
		std::string name;
		cls::PA<float> u, v;
		uvSet();
		uvSet(const uvSet& arg);
	};
	std::vector< uvSet > uvSets;
};

