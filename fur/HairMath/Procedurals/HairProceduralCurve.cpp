#include "stdafx.h"
	#include "ocellaris/IRender.h"
#include "HairProceduralCurve.h"
#include "HairPersonage.h"
#include "ocellaris/renderPrmanImpl.h"

const float minHairLen = 0.00001f;
void CompleteTokenList(
	std::vector<RtToken>& tokens, 
	const std::vector<std::string>& tokennames
	);


HairProceduralCurve::HairProceduralCurve()
{
//	this->interpolation = interpolation;
//	this->diceHair = diceHair;
	perVertexCacheSise = 0;
	currentOffset = 0;
	currentDataOffset = 0;
	vertexInHair = 0;
	hairCount = 0;
	bClump= false;
	bCubic= false;

	hairoffset = 0;
	cvs = NULL;
	widths = NULL;
	nverts = NULL;
	s = t = 0;
	surface_normals = NULL;
//	seed = 0;
	hairIds = 0;
	bNormals = false;
}
void HairProceduralCurve::setBaseUvSet( const char* baseUvSet)
{
	this->baseUvSet = baseUvSet;
}

HairProceduralCurve::~HairProceduralCurve() 
{
	delete widths;
	delete[] cvs;
	delete nverts;
	delete hairoffset;
	delete s; 
	delete t;
	delete[] surface_normals;
//	delete seed;
	delete hairIds;
}

void HairProceduralCurve::Init(
	int hairCount, int vertexInHair, enHairInterpolation interpolation, bool bNormals, std::vector<const char*>* adduv,
	int flags
	)
{
	try
	{
		this->bCubic = false;
		if( interpolation != HI_LINIAR)
			this->bCubic = true;
		this->bNormals = bNormals;

	//this->bCubic = true;
	//fprintf(stderr, "HairProceduralCurve::Init hairs.interpolation=%d bCubic = %d\n", hairs.interpolation, (int)this->bCubic);

		this->vertexInHair = vertexInHair;
		if( bCubic)
			this->vertexInHair += 2;
		this->perVertexCacheSise = this->vertexInHair*hairCount+10;

		this->currentOffset = 0;
		this->currentDataOffset = 0;
		this->hairoffset = new int[hairCount];
		if( !this->hairoffset) throw "hairoffset";
		this->nverts = new RtInt[hairCount];
		if( !this->nverts) throw "nverts";
		this->dataoffset.resize(hairCount);
		if( this->dataoffset.empty()) throw "dataoffset";

		this->s = new RtFloat[hairCount];
		if( !this->s) throw "s";
		this->t = new RtFloat[hairCount];
		if( !this->t) throw "t";
		this->surface_normals = new RtNormal[hairCount];
		if( !this->surface_normals) throw "surface_normals";
	//	this->seed = new RtFloat[hairCount];
		this->hairIds = new RtInt[hairCount];
		if( !this->hairIds) throw "hairIds";

		cvs = new RtPoint[perVertexCacheSise];
		if( !this->cvs) throw "cvs";
		widths = new RtFloat[perVertexCacheSise];
		if( !this->widths) throw "widths";

		this->hairCount = 0;

		tangent_vector.resize(perVertexCacheSise, Math::Vec3f(0, 0, 0));
		if( tangent_vector.empty()) throw "tangent_vector";

		bClump = true;
		if(bClump)
		{
			clump_normal.resize(hairCount);
			if( clump_normal.empty()) throw "clump_normal";
			clump_vector.resize(hairCount);
			if( clump_vector.empty()) throw "clump_vector";
#ifndef SAS
//			clump_vectorV.resize(perVertexCacheSise, Math::Vec3f(0, 0, 0));
//			if( clump_vectorV.empty()) throw "clump_vectorV";
			vclump.resize(perVertexCacheSise, 0);
			if( vclump.empty()) throw "vclump";
#endif
		}
		if(this->bNormals)
		{
			normals.resize(perVertexCacheSise);
			if( normals.empty()) throw "normals";
		}
		if( adduv && adduv->size())
		{
			uvSets.resize(adduv->size());
			if( uvSets.empty()) throw "uvSets";
			for(unsigned i=0; i<uvSets.size(); i++)
			{
				uvSets[i].name = (*adduv)[i];
				uvSets[i].u.resize(hairCount);
				if( uvSets[i].u.empty()) throw "uvSets[i].u";
				uvSets[i].v.resize(hairCount);
				if( uvSets[i].v.empty()) throw "uvSets[i].v";
			}
		}
	}
	catch(const char* what)
	{
		fprintf(stderr, "HairMath: out of memory then allocate %s (hairs=%d verties=%d sum=%d)\n", what, hairCount, vertexInHair, perVertexCacheSise);
		fflush(stderr);
		throw;
	}
	catch(...)
	{
		fprintf(stderr, "HairMath: HairProceduralCurve::Init\n");
		fflush(stderr);
		throw;
	}
}

void HairProceduralCurve::SetHair(
	int i,	// hair index
	int id,	// hair id
	HAIRPARAMS& params, 
	HAIRVERTS& hair, 
	enRenderNormalSource renderNormalSource, 
	float RenderWidth, 
	float widthBias
	)
{
	// render
	Math::Vec2f* addUv = NULL;
	if( params.uvSets.size())
		addUv = &params.uvSets[0];
	this->sethair(i, hair.vc(), params.u, params.v, id, params.position, params.normal, addUv);
	this->sethairclumpdata(i, hair.clump_normal, hair.clump_vector);
	for(int v=0; v<hair.vc(); v++)
	{
		this->sethairvertex(i, v, hair.cvint[v]);
		float factor = hair.vfactor[v];
		float w = hair.vwidht[v];
		w = w*(float)RenderWidth;
		w += widthBias;
		if(w<0) w=0;
		this->sethairwidht(i, v, w);
		if( renderNormalSource==RNS_TANGENTU)
		{
			Math::Matrix4f& basis = hair.basas[v];
			this->sethairnormal(i, v, tangentUfromBasis(basis));
		}
		// ������ ���� ���� ����� ��� ������!!!
		else if(renderNormalSource==RNS_CLUMPVECTOR 
			&& params.clumpIndex>=0)
		{
			this->sethairnormal(i, v, hair.clump_vector);
		}

		if( v<(int)hair.vclump.size())
			this->sethairclump_vectorV(i, v, hair.vclump[v]);

		// tangent_vector
		{
			int xi = dataoffset[i] + v;
			Math::Matrix4f basis;
			params.getBasis(basis);
			Math::Vec3f tu = tangentUfromBasis(hair.basas[v]);
			tu *= Math::Matrix4As3f(basis);
			tangent_vector[xi] = tu;
		}
	}
}

void HairProceduralCurve::sethair(
	int cvhair, int vertcount, 
	float u, float v, 
	int id, 
	const Math::Vec3f& p, 
	const Math::Vec3f& n, 
	Math::Vec2f* adduv
	)
{
	hairCount = cvhair+1;
	s[cvhair] = u;
	t[cvhair] = 1-v;
//	seed[cvhair] = u+v;
	this->hairIds[cvhair] = id;
	IPrman::copy( surface_normals[cvhair], n);

	dataoffset[cvhair] = currentDataOffset;
	currentDataOffset += vertcount;

	if(bCubic)
		vertcount+=2;

//fprintf(stderr, "> %x HairProceduralCurve::sethair %d %d ofs=%d uv{%f %f}\n", this, cvhair, vertcount, currentOffset, u, v);

	if( currentOffset+vertcount >= perVertexCacheSise)
	{
		fprintf(stderr, "PER VERTEX BUFFER OVERFLOW cvhair=%d cv=%d| %d+%d > %d\n", cvhair, vertcount, currentOffset, vertcount, perVertexCacheSise);
		fflush(stderr);
		currentOffset = 0;
	}
	hairoffset[cvhair] = currentOffset;

	nverts[cvhair] = vertcount;
	currentOffset += vertcount;

	if(bCubic)
	{
		int i = hairoffset[cvhair];
		Math::Vec3f& pt = -n.normalized();
		RtPoint* dst = cvs + i;
		IPrman::copy(*dst, pt);
	}
	if( adduv)
	{
		for(unsigned i=0; i<uvSets.size(); i++)
		{
			uvSets[i].u[cvhair] = adduv[i].x;
			uvSets[i].v[cvhair] = 1-adduv[i].y;
		}
	}
}
void HairProceduralCurve::sethairclumpdata(int cvhair, const Math::Vec3f& clump_norm, const Math::Vec3f& clump_vect)
{
	if(bClump)
	{
		clump_normal[cvhair] = clump_norm;
		clump_vector[cvhair] = clump_vect;
	}
}
void HairProceduralCurve::sethairclump_vectorV(int cvhair, int vertindex, float clump)
{
	if(bClump)
	{
		int i = dataoffset[cvhair] + vertindex;
#ifndef SAS
		vclump[i] = clump;
#endif
	}
}

void HairProceduralCurve::sethairvertex(int cvhair, int vertindex, const Math::Vec3f& pt)
{
	int i = hairoffset[cvhair] + vertindex + (bCubic?1:0);
//fprintf(stderr, "%x HairProceduralCurve::sethairvertex %d %d (%f, %f, %f) ind=%d\n", this, cvhair, vertindex, pt.x, pt.y, pt.z, i);
	RtPoint* dst = cvs + i;
	IPrman::copy(*dst, pt);
	if( bCubic && vertindex == (nverts[cvhair]-3))
	{
		// last point
		IPrman::copy(dst[1], pt);
		// first point
		RtPoint* first = cvs + hairoffset[cvhair];
		Math::Vec3f pt1, pt2, n;
		IPrman::copy(  n, first[0]);
		IPrman::copy(pt1, first[1]);
		IPrman::copy(pt2, first[2]);
		float l = (pt1-pt2).length();
		pt1 += n*l;
		IPrman::copy(first[0], pt1);
	}
}

void HairProceduralCurve::sethairnormal(int cvhair, int vertindex, const Math::Vec3f& normal)
{
	if( bNormals)
	{
		int i = dataoffset[cvhair] + vertindex;
		normals[i] = normal;
//		normals[i] = Math::Vec3f(1, 0, 0);
	}
}

void HairProceduralCurve::sethairwidht(int cvhair, int vertindex, float val)
{
	int i = dataoffset[cvhair] + vertindex;
//fprintf(stderr, "%x HairProceduralCurve::sethairwidht %d %d (%f) ind=%d\n", this, cvhair, vertindex, val, i);

	RtFloat* dst = widths + i;
	*dst = val;
}
void HairProceduralCurve::sethaircolor(int cvhair, int vertindex, const Math::Vec3f& pt)
{
}

extern int curvind;
extern int curvecount;
extern int curvertcount;
extern int curmem;

void HairProceduralCurve::Render(
	IPrman* prman,
	cls::IRender* render,
	float motionPhase, 
	std::map< std::string, cls::Param>& extraParameters, 
	DrawSynk* freeze
	)
{
	if( hairCount)
	{
		/*/
		RtInt dicehair = 1;
		prman->RiAttributeBegin();
		prman->RiAttribute("dice", "uniform int hair", &dicehair, RI_NULL);

		RtBasis catmullRom = {
		{ -0.5,  1.5, -1.5,  0.5 },
		{  1.0, -2.5,  2.0, -0.5 },
		{ -0.5,  0.0,  0.5,  0.0 },
		{  0.0,  1.0,  0.0,  0.0 }
		};
		prman->RiBasis(catmullRom, 1, catmullRom, 1);
		/*/

		if( true)
		{
			std::vector<RtToken> tokens;
			std::vector<std::string> tokennames;
			std::vector<RtPointer> parms;
			std::list<RtString> strings;

			tokennames.push_back( "P");
			parms.push_back( cvs);

			if( freeze)
			{
				HairProceduralCurve* _freeze = (HairProceduralCurve*)freeze;
				tokennames.push_back( "vertex point __Pref");
				parms.push_back( _freeze->cvs);
			}

			if( bNormals)
			{
//				tokennames.push_back( "normal N");
//				tokennames.push_back( "N");
				tokennames.push_back( "varying normal N");
				parms.push_back( &normals[0]);
			}

			tokennames.push_back( "uniform normal SN");
			parms.push_back( surface_normals);

			tokennames.push_back( "width");
			parms.push_back( widths);

			tokennames.push_back( "uniform float s");
			parms.push_back( s);

			tokennames.push_back( "uniform float t");
			parms.push_back( t);

			tokennames.push_back( "uniform int hairId");
			parms.push_back( hairIds);

			if( bClump)
			{
				tokennames.push_back( "uniform vector clump_normal");
				parms.push_back( &clump_normal[0]);

				tokennames.push_back( "uniform vector clump_vector");
				parms.push_back( &clump_vector[0]);

#ifndef SAS
				tokennames.push_back( "varying float clump");
				parms.push_back( &vclump[0]);
#endif
			}
			// tangent_vector
			tokennames.push_back( "varying vector tangentvector");
			parms.push_back( &tangent_vector[0]);

			{
				for(unsigned i=0; i<uvSets.size(); i++)
				{
					std::string name_u = "uniform float u_" + uvSets[i].name;
					std::string name_v = "uniform float v_" + uvSets[i].name;
					std::vector<float>& u = uvSets[i].u;
					std::vector<float>& v = uvSets[i].v;
					tokennames.push_back( name_u);
					parms.push_back( &u[0]);
					tokennames.push_back( name_v);
					parms.push_back( &v[0]);
				}
				// baseUvSet
				{
					std::string name_u = "uniform float u_" + baseUvSet;
					std::string name_v = "uniform float v_" + baseUvSet;
					tokennames.push_back( name_u);
					parms.push_back( s);
					tokennames.push_back( name_v);
					parms.push_back( t);
				}
			}

			// extraParameters
			std::map< std::string, cls::Param>::iterator it = extraParameters.begin();
			for( ; it != extraParameters.end(); it++)
			{
				static char* interpolationTable[] =
				{
					"", // PI_UNKNOWN
					"constant", // PI_CONSTANT
					"vertex", // PI_VERTEX
					"uniform", // PI_PRIMITIVE
					"facevarying", // PI_PRIMITIVEVERTEX
					"varying", // PI_VARYING
					"", //PI_LAST
				};

				std::string name;
				RtPointer data = cls::renderPrmanImpl<>::BuildSingleParam(
					it->first.c_str(), 
					it->second, 
					interpolationTable, 
					name,
					strings, 
					false);
				if( !data)
					continue;

				tokennames.push_back( name);
				parms.push_back( data);
			}

//			tokennames.push_back( "constant float prune");
//			parms.push_back( &prune);

			CompleteTokenList(tokens, tokennames);
			/*/
			if( this->diceHair)
			{
				RtInt flag = 1;
				prman->RiAttribute("dice", "hair", (RtPointer)&flag, NULL);
			}
			/*/
			/*/
			int vcount = 0;
			for(int i=0; i<hairCount; i++)
				vcount += nverts[i];
			printf("hairCount=%d vcount=%d\n", hairCount, vcount);
			/*/

			/*/
{
	printf("count: %d ", hairCount);
	for(int i=0; i<tokennames.size(); i++)
	{
		printf("%s, ", tokennames[i].c_str());
	}
	printf("\n");
}
			/*/
			if(bCubic)
				prman->RiCurvesV("cubic", hairCount, nverts, "nonperiodic", (int)tokens.size(), &tokens[0], &parms[0]);
			else
				prman->RiCurvesV("linear", hairCount, nverts, "nonperiodic", (int)tokens.size(), &tokens[0], &parms[0]);

			/*/
			int mem = 0;
			curvind++; curvecount+=hairCount; curvertcount+=perVertexCacheSise; curmem += mem;
			fprintf(stderr, "{hs:%d c:%d v:%d m:%d(Kb) %s} ", curvind, curvecount, curvertcount, (int)(curmem/(1024)), bCubic?"CUBIC":"lin");
			fflush(stderr);
			/*/
		}
	}
}

RtVoid ErrorHandler(RtInt i, RtInt j, char * xhar)
{
	printf("%s\n", xhar);
}

std::list<std::string> test;
// Render BLUR
void HairProceduralCurve::RenderBlur(
	IPrman* prman,
	cls::IRender* render,
	std::vector<float> times,
	std::vector<DrawSynk*>& procedurals,
	std::map< std::string, cls::Param>& extraParameters, 
	DrawSynk* freeze
	)
{
	prman->RiErrorHandler(&ErrorHandler);

//	char buf[64];
//	sprintf(buf, "gr_%d", test.size());
//	test.push_back( buf);
//	char* rrr = (char*)test.back().c_str();
//	prman->RiAttribute( "identifier", "string name", &rrr, NULL);
	
	prman->RiMotionBeginV( (int)times.size(), &times[0]);
	for(int p=0; p<(int)times.size(); p++)
	{
		procedurals[p]->Render(prman, render, 0, extraParameters, freeze);
	}
	prman->RiMotionEnd();

	prman->RiErrorHandler(0);
}

void CompleteTokenList(
	std::vector<RtToken>& tokens, 
	const std::vector<std::string>& tokennames
	)
{
	tokens.resize(tokennames.size());
	for(unsigned i=0; i<tokens.size(); i++)
	{
		tokens[i] = (RtToken)tokennames[i].c_str();
	}
}
