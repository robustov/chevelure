#pragma once

#include "HairProcessor.h"
#include "mathNpixar\IPrman.h"

#include "ocellaris/IRender.h"
#include "ocellaris/renderCacheImpl.h"
#include "ocellaris/renderBlurParser.h"

#pragma warning ( disable:4275)

//! HairProceduralSnOcs
class HAIRMATH_API HairProceduralSnOcs : public DrawSynk
{
public:
//	HairProcessor* hairs;
public:
    HairProceduralSnOcs();
    ~HairProceduralSnOcs();

	void setBaseUvSet( const char* baseUvSet);

	// INSTANCE
	virtual void setSnShape(int index, sn::ShapeShortcutPtr& ptr);
	virtual void setOcsShape(int index, cls::MInstance& cache);

	// ������������� �������
	virtual void Init(
		int hairCount, 
		int vertexInHair, 
		enHairInterpolation interpolation, 
		bool bNormals, 
		std::vector<const char*>* adduv, 
		int flags
		);
	// ���������� ��� ������ ������
	virtual void SetHair(
		int hairindex, 
		int hairid,
		HAIRPARAMS& params, 
		HAIRVERTS& hair,
		enRenderNormalSource renderNormalSource, 
		float RenderWidth, 
		float widthBias
		);


	// Render
	void Render(
		IPrman* prman,
		cls::IRender* render,
		float motionPhase, 
		std::map< std::string, cls::Param>& extraParameters, 
		DrawSynk* freeze
		);
	void RenderCall(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		);


	// ��������!!!
	// Render BLUR
	virtual void RenderBlur(
		IPrman* prman,
		cls::IRender* render,
		std::vector<float> times,
		std::vector<DrawSynk*>& procedurals,
		std::map< std::string, cls::Param>& extraParameters, 
		DrawSynk* freeze
		);


	virtual void setSnHair(int cvhair, int instance, HairPersonageData& data);


private:
	std::string baseUvSet;

	mutable std::map< int, sn::ShapeShortcutPtr > snshapes;

	typedef std::vector<HairPersonageData> instHairs_t;

	std::vector<HairPersonageData> instHairs;

	std::map<int, cls::MInstance*> ocsshapes;

// ��� ����������
private:
	struct HairRenderData
	{
		cls::renderBlurParser<cls::TAttributeStack_forblur, cls::TTransformStack_minimal> blurParser;
	};
	// ������������ �� �������
	std::map< int, HairRenderData> hairs;

};

