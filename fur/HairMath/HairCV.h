#pragma once

#include "HairMath.h"
#include "ocellaris/IRender.h"
#include "Math/Math.h"
#include <vector>
#include <Util/Stream.h>
#include <Util\STLStream.h>
#include "HairControl.h"
#include "HairCVPositions.h"
#include <Math/matrixMxN.h>
#include <Util/math_bmpMatrixMN.h>

#pragma warning( disable : 4251)

struct IHairCVmanipulator;

class HairMesh;
//! ����������� ������
class HAIRMATH_API HairCV
{
public:
	struct CV
	{
		bool bValid;
		// ������� � tangent space
		std::vector< Math::Vec3f> ts;

		CV(){clear();}
		void clear(){ts.clear();bValid=false;};
		int vc()const{return (int)ts.size();}
		void init(int vc);
		float getLength() const;

		// scale
		void scale(float);

	};
//	typedef HAIRCONTROL CV;
public:
	//! copy
	void copy(const HairCV& arg);
	//! operator=
	HairCV& operator=(const HairCV& arg);
	
	//! �������� ������������
	void linear_interpolation(const HairCV& arg1, const HairCV& arg2, float t1, float t2);

	// ����� �����
	int getCount()const{return (int)cvs.size();}
	// ����� ��������� � ����������� ������
	int controlVertexCount() const {return cvcount;};
	// �����
	CV& getControlHair(int i){return cvs[i];};
	const CV& getControlHair(int i)const{return cvs[i];};
	// 1-�� ��������������� �������
	Math::Vec3f* getSingleIndexedVertex(
		int index, int* hairindex=NULL);
	// 1-�� ��������������� �������
	bool getSingleIndexedVertex(
		int index, int& hairindex, int& vertindex);

public:
	// ����� ����� � ����������� ������
	int cvcount;

	// ����������� ������
	std::vector< CV> cvs;

public:
	// ��� ����� ����������
	bool init(
		int CVcount, 
		HairCVPositions* pos);
	// � ������ ����������
	bool init(
		int CVcount, 
		HairCVPositions* dst_cvpos, 
		HairMesh& meshsurface
		);
	

	// �������� �� ������ �� �����
	bool import(
		std::vector< std::vector<Math::Vec3f> >& shavepoints, 
		HairGeometryIndices* gi, 
		HairGeometry* g,
		HairCVPositions* cvp
		);
	// �������� �� ������ �� �����
	bool export_(
		std::vector< std::vector<Math::Vec3f> >& shavepoints, 
		int cvinhair,
		HairGeometryIndices* gi, 
		HairGeometry* g,
		HairCVPositions* cvp
		);
	
	// ������� � ��������
	bool export_(
		Math::matrixMxN<Util::rgb>& texture, 
		const char* what,			// ����� ��������
		int resolution,				// ����������
		HairGeometryIndices* gi, 
		HairGeometry* g,
		HairCVPositions* cvp
		)const;
	// �����
	static bool getControlHairBasis(
		int h, 
		const HairCVPositions* cvp, 
		const HairGeometryIndices* gi,
		const HairGeometry* g,
		Math::Matrix4f& m
		);
	// �����
	static bool getControlHairBasisSrc(
		int h, 
		const HairCVPositions* cvp, 
		const HairGeometryIndices* gi,
		const HairGeometry* g,
		Math::Matrix4f& m
		);
	// �����
	static Math::Vec2f getControlHairUV(
		int h, 
		const HairCVPositions* cvp, 
		const HairGeometryIndices* gi,
		const HairGeometry* g
		);
	// ����������� ����� �� ���������������� �����������
	bool buildControlHair( 
		const HairGeometryIndices::pointonsurface& pos,
		const HairCVPositions* cvPositions, 
		const HairGeometryIndices* geometryIndices,
		const HairGeometry* geometry,
		HAIRCONTROL& CV
		) const;

	// select
	// ��������� � ��������
	bool selectVertecies(
		std::vector< Math::Matrix4f>& bases,
		const Math::Matrix4f& worldpos,		// ������� ���������� ����� � WS
		const Math::Matrix4f& viewMatrix,	// view 
		const Math::Matrix4f& projMatrix,	// projection
		Math::Vec2f& combPosPS,				// ������� �������� (projection space)
		float radiusPS,						// ������ �������� (projection space)
		std::list<int>& selectedverts		// ���������
		);

public:
	void serialize(Util::Stream& stream);
	void serializeOcs(const char* _name, cls::IRender* render, bool bSave);
	void clear();
};

template<class Stream> inline
Stream& operator >> (Stream& out, HairCV::CV& v)
{
	int version = 0;
	out >> version;
	if( version>=0)
	{
		out >> v.bValid;
		out >> v.ts;
	}
	return out;
}

template<class Stream> inline
Stream& operator >> (Stream& out, HairCV& v)
{
	v.serialize(out);
	return out;
}

inline void HairCV::CV::init(int vc)
{
	ts.resize(vc);
	for(int v=0; v<vc; v++)
	{
		float y = v/(float)(vc-1);
		ts[v] = Math::Vec3f(0, y, 0);
	}
};
inline float HairCV::CV::getLength() const
{
	float l=0;
	for(int i=1; i<(int)ts.size(); i++)
	{
		l += (ts[i-1]-ts[i]).length();
	}
	return l;
}

// scale
inline void HairCV::CV::scale(float s)
{
	Math::Vec3f ofs(0);
	for(int i=1; i<(int)ts.size(); i++)
	{
		Math::Vec3f seg = ts[i]-ts[i-1];
		ts[i-1] += ofs;
		// ����� �������
		seg = seg*(s-1);
		
		ofs += seg;
	}
	ts[ts.size()-1] += ofs;
}
