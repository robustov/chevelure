#pragma once
//#include "hairmath.h"
//#include "Util/Stream.h"
#include "Math/Math.h"
#include "HAIRCONTROL.h"
#include <vector>

struct HAIRPARAMS 
{
	// ���������� ���������� ��������������� ������� ������ �� �����������
	float u,v;			
	// �������, ������� � �������� "������" ����� - �������������� �����
	Math::Vec3f position, normal, tangentU, tangentV;
	// ������� dP/du � pP/dv, �����������������
	Math::Vec3f dPdU, dPdV;
	// ������� �������� �� object space � UV space
	Math::Matrix4f basis_inv;
	// ���. uv 	
	std::vector<Math::Vec2f> uvSets;

	float displace;				// ��������
	float length;				// ����� ������ (��� jitter)
	float lengthjitter;			// ������ ���������� ����� lengthjitter = (<=1) (���� jitter ����� �� ����� �� �����������)
	float polar;				// ���� ��������
	int clumpIndex;				// ������ ������
	Math::Vec3f toclump;		// ����������� �� ����� ������ 
	Math::Vec2f toclumpUV;		// ����������� �� ����� ������ � UV space
	Math::Vec2f inclination;	// �������
	Math::Vec2f baseCurl;		// ����������
	Math::Vec2f tipCurl;		
	float baseWidth;			// ������ �����
	float tipWidth;				// ������ ������
	float scraggle;				// ����
	float scraggleFreq;			// �� ������������
	float BaseClumpValue;		// ���������� �����������
	float TipClumpValue;	
	float twist;				// �������
	float twistFromPolar;		// ������� ��������� ������ �� twist - ������ ���������� �� ����������� ������

	// ��������� ��� ���������� CV � ������� ����������
	float blendCVfactor, blendDeformfactor;		

	int seed;					// ��� �������
	int instance;				// ����� ��������
	HAIRCONTROL	cv;				// ����������� ����� (����� ���� null)

	std::vector<float> deformers_data;	// ������ ��� ����������

	void getBasis(Math::Matrix4f& basis) const;
	void orthogonalize(bool bU);

	Math::Vec2f uv(float U, float V, int uvnumber);
	HAIRPARAMS();

	void clear(int sizein_HAIRPARAMS)
	{
		deformers_data.clear();
		if( sizein_HAIRPARAMS)
			deformers_data.resize(sizein_HAIRPARAMS);
	}

};
inline HAIRPARAMS::HAIRPARAMS()
{
	// ���������� ���������� ��������������� ������� ������ �� �����������
	u=v=0;			

	displace=0;				// ��������
	length=0;				// ����� ������ (��� jitter)
	lengthjitter=0;			// ������ ���������� ����� lengthjitter = (<=1) (���� jitter ����� �� ����� �� �����������)
	polar=0;				// ���� ��������
	clumpIndex=0;				// ������ ������
	baseWidth=0;			// ������ �����
	tipWidth=0;				// ������ ������
	scraggle=0;				// ����
	scraggleFreq=0;			// �� ������������
	BaseClumpValue=0;		// ���������� �����������
	TipClumpValue=0;	
	twist=0;				// �������
	twistFromPolar=0;		// ������� ��������� ������ �� twist - ������ ���������� �� ����������� ������

	// ��������� ��� ���������� CV � ������� ����������
	blendCVfactor = blendDeformfactor = 1;		

	seed=0;					// ��� �������
	instance=0;				// ����� ��������

}

inline void HAIRPARAMS::getBasis(Math::Matrix4f& basis) const
{
	basis[0] = Math::Vec4f(tangentU,	0);
	basis[1] = Math::Vec4f(normal,		0);
	basis[2] = Math::Vec4f(tangentV,	0);
	basis[3] = Math::Vec4f(position,	1);

//	basis = buildBasis4f(position, normal, tangentU, tangentV);

//	basis[0] = Math::Vec4f( tangentU, 0);
//	basis[1] = Math::Vec4f( normal, 0);
//	basis[2] = Math::Vec4f( tangentV, 0);
//	basis[3] = Math::Vec4f( position, 1);
}

inline void HAIRPARAMS::orthogonalize(bool bU)
{
	Math::Vec3f n = Math::cross( tangentU, tangentV);
	if( bU)
	{
		Math::Vec3f tu = Math::cross( n, tangentV);
		float tul = tu.length();
		float tul_ = tangentU.length();
		tangentU = -tu*tul_/tul;
	}
	else
	{
		Math::Vec3f tu = Math::cross( tangentU, n);
		float tul = tu.length();
		float tul_ = tangentV.length();
		tangentV = -tu*tul_/tul;
	}
}

inline Math::Vec2f HAIRPARAMS::uv(float U, float V, int uvnumber)
{
	if( uvnumber>=0 && 
		uvnumber<(int)uvSets.size())
	{
		return uvSets[uvnumber];
	}
	return Math::Vec2f(U, V);
}

