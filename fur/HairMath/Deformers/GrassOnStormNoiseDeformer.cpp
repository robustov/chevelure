#include "StdAfx.h"
#include "HairMath.h"
#include "IHairDeformer.h"
#include "Math/PerlinNoise.h"

class GrassOnStormNoiseDeformer : public IHairDeformer
{
	int seed;
	float t;
	Math::Vec3f freq;
	Math::Vec3f amp, amplen;
	Math::Vec3f timefreq;
	float time;
	float angle_min, angle_max;

	Math::PerlinNoise noise;

public:
	virtual int getType(
		)
	{
		return POSTBUILDHAIR;
	}
	virtual void release()
	{
		delete this;
	};

public:
	// ��� ���� ���������� ������� ������� ���������, � ����� ����� ��������� ����� ��������������� ������
	virtual void PostBuildHair(
		float affect, 
		const HAIRPARAMS& c_hairs, 			// ��������� ������
		int SegmCount,					// ����� ���������
		HAIRVERTS& hair					// ���������
		);

public:
	// �������������
	virtual void init(
		);
	// ������ �� UI
	virtual void fromUI(
		artist::IShape* shape
		);
	// ������������
	virtual void serialise(
		Util::Stream& out
		);

public:
	GrassOnStormNoiseDeformer(void);
	~GrassOnStormNoiseDeformer(void);
};

extern "C"
{
	HAIRMATH_API IHairDeformer* getGrassOnStormNoiseDeformer();
}

HAIRMATH_API IHairDeformer* getGrassOnStormNoiseDeformer()
{
	return new GrassOnStormNoiseDeformer();
}






// -------------------------------------------------------

GrassOnStormNoiseDeformer::GrassOnStormNoiseDeformer(void)
{
}

GrassOnStormNoiseDeformer::~GrassOnStormNoiseDeformer(void)
{
}

// ��� ���� ���������� ������� ������� ���������, � ����� ����� ��������� ����� ��������������� ������
void GrassOnStormNoiseDeformer::PostBuildHair(
	float affect, 
	const HAIRPARAMS& c_hairs, 			// ��������� ������
	int SegmCount,					// ����� ���������
	HAIRVERTS& hair					// ���������
	)
{
	affect = __max(0, affect);
	affect = __min(1, affect);

	srand(c_hairs.seed+56);
	float dx = rand()/(float)RAND_MAX;
	float dy = rand()/(float)RAND_MAX;
	dx = 0.2f;
	dy = (float)M_PI;

	Math::Vec3f lastts(0);
	for(int i=0; i<(int)hair.vwidht.size(); i++)
	{
		Math::Vec3f ts = c_hairs.basis_inv*hair.cvint[i];

		// ������ ����� �� �������
		if(i!=0)
		{
			Math::Vec3f difts = ts-lastts;
			// ���� �������
			float cos = difts.y/difts.length();
			cos = __max(0, cos);
			float sin = sqrt(1-cos*cos);

			float y = hair.vfactor[i];
			Math::Vec3f v( c_hairs.u, y, c_hairs.v);
			v *= freq;
			Math::Vec3f vt( time*timefreq.x, time*timefreq.y, time*timefreq.z);
			v += vt;

			Math::Vec3f vx = v + Math::Vec3f(dx, 0, dx);
			Math::Vec3f vy = v + Math::Vec3f(dy, 0, dy);
			float a = noise.noise(vx.x, vx.y, vx.z);
			float b = noise.noise(vy.x, vy.y, vy.z);
			a *= amp.x;
			b *= amp.z;

			// object space
			Math::Vec3f of(a, 0, b);
			of = Math::Matrix4As3f(hair.basas[i])*of;

			float factor1 = (1-y)*amplen.x + y*amplen.z;
			float factor2 = 1;
			if(fabs(angle_max-angle_min)>0.0001)
			{
				factor2 = (sin-angle_min)/(angle_max-angle_min);
				factor2 = __min(1, factor2);
				factor2 = __max(0, factor2);
			}

			of *= factor1*factor2;
			of *= 1/hair.vwidht[i];		// ����������� scale 
			of *= affect;
			hair.cvint[i] += of;
		}
		lastts = ts;
	}
}

void GrassOnStormNoiseDeformer::init(		
	)
{
	noise.Init(seed);
}
// ������ �� UI
void GrassOnStormNoiseDeformer::fromUI(
	artist::IShape* shape
	)
{
	shape->getValue(time, "time", sn::UNIFORM_SHAPE, 0, 0, 1);
	shape->getValue(seed, "seed", sn::UNIFORM_SHAPE, 0, 0, 1000);
	shape->getValue(freq.x, "freq_x", sn::UNIFORM_SHAPE, 1, 0, 1);
	shape->getValue(freq.y, "freq_y", sn::UNIFORM_SHAPE, 0.01f, 0, 1);
	shape->getValue(freq.z, "freq_z", sn::UNIFORM_SHAPE, 1, 0, 1);

	shape->getValue(timefreq.x, "timefreq_x", sn::UNIFORM_SHAPE, 1, 0, 1);
	shape->getValue(timefreq.y, "timefreq_y", sn::UNIFORM_SHAPE, 1, 0, 1);
	shape->getValue(timefreq.z, "timefreq_z", sn::UNIFORM_SHAPE, 1, 0, 1);

	shape->getValue(amp.x, "amp_x", sn::UNIFORM_SHAPE, 0.1f, 0, 1);
	shape->getValue(amp.z, "amp_z", sn::UNIFORM_SHAPE, 0.1f, 0, 1);

	shape->getValue(amplen.x, "amp_base", sn::UNIFORM_SHAPE, 0, 0, 1);
	shape->getValue(amplen.z, "amp_tip", sn::UNIFORM_SHAPE, 1, 0, 1);

	shape->getValue(angle_min, "angle_min", sn::UNIFORM_SHAPE, 0.2f, 0, 1);
	shape->getValue(angle_max, "angle_max", sn::UNIFORM_SHAPE, 0.5f, 0, 1);
}

// ������������
void GrassOnStormNoiseDeformer::serialise(
	Util::Stream& out
	)
{
	int version = 0;
	out >> version;
	if( version>=0)
	{
		out>>seed;
		out>>freq;
		out>>amp;
		out>>amplen;
		out>>time;
		out>>timefreq;
		out>>angle_min;
		out>>angle_max;
	}
}
