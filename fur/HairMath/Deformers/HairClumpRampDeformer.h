#pragma once

#include "IHairDeformer.h"
//#include "MathNMaya\MayaRamp.h"

class HairClumpRampDeformer : public IHairDeformer
{
	enum enType
	{
		IGNORE_TIP_BASE=0,		// ������������ ������� ��������
		PRODUCT_TIP_BASE=1,		// �������� �� ������� ��������
	};
	Math::Ramp clumpRamp;
	enType type;

	// ������������ ��� ��� (��� ����!!!)
	Math::ParamTex2d<unsigned char, Math::EJT_MUL> affect;

public:
	virtual int getType(
		)
	{
		return POSTBUILDHAIR;
	}
	virtual void release()
	{
		delete this;
	};

public:
	// ��� ���� ���������� ������� ������� ���������, � ����� ����� ��������� ����� ��������������� ������
	virtual void PostBuildHair(
		float affect, 
		const HAIRPARAMS& c_hairs, 			// ��������� ������
		int SegmCount,					// ����� ���������
		HAIRVERTS& hair					// ���������
		);

public:
	// �������������
	virtual void init(
		);
	// ������ �� UI
	virtual void fromUI(
		artist::IShape* shape
		);
	// ������������
	virtual void serialise(
		Util::Stream& out
		);

public:
	HairClumpRampDeformer(void);
	~HairClumpRampDeformer(void);
};
