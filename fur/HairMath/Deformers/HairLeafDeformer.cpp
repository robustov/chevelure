#include "StdAfx.h"
#include "HairMath.h"
#include ".\HairLeafDeformer.h"

// ������� ������ ������ ������ �� �����
// 

extern "C"
{
	HAIRMATH_API IHairDeformer* getLeafDeformer();
}

HAIRMATH_API IHairDeformer* getLeafDeformer()
{
	return new HairLeafDeformer();
}

HairLeafDeformer::HairLeafDeformer(void)
{
}

HairLeafDeformer::~HairLeafDeformer(void)
{
}

// ��������� ������
void HairLeafDeformer::ParseHair(
	float affect, 
	HAIRPARAMS& c_hairs,
	bool bClump
	)
{
	switch( type)
	{
		case USE_IDENTITY:
			c_hairs.baseWidth = 1;
			c_hairs.tipWidth = 1;
			break;
		case USE_BASEWIDTH:
			c_hairs.tipWidth = c_hairs.baseWidth;
			break;
		case USE_TIPWIDTH:
			c_hairs.baseWidth = c_hairs.tipWidth;
			break;
		case USE_BOTH:
			break;
	}
}
// ��� ���� ���������� ������� ������� ���������, � ����� ����� ��������� ����� ��������������� ������
void HairLeafDeformer::PostBuildHair(
	float affect, 
	const HAIRPARAMS& c_hairs, 			// ��������� ������
	int SegmCount,					// ����� ���������
	HAIRVERTS& hair					// ���������
	)
{
	for(int i=0; i<(int)hair.vwidht.size(); i++)
	{
		float f = hair.vfactor[i];
		float v = rampWidth.getValue(f);
		hair.vwidht[i] *= v;
	}
}

void HairLeafDeformer::init(		
	)
{
}
// ������ �� UI
void HairLeafDeformer::fromUI(
	artist::IShape* shape
	)
{
	shape->getValue(*(int*)&type, "sourceWidth", sn::UNIFORM_SHAPE);
	shape->getValue(rampWidth, "rampWidth");
}

// ������������
void HairLeafDeformer::serialise(
	Util::Stream& out
	)
{
	int version = 0;
	out >> version;
	if( version>=0)
	{
		out>>*(int*)&type;
		out>>rampWidth;
	}
}
