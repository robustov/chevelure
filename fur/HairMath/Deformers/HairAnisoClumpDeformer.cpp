// HairCustomDeforms.dll
// getAnisoClumpDeformer

#include "stdafx.h"
#include "HairMath.h"

#include "IHairDeformer.h"

class HairAnisoClumpDeformer : public IHairDeformer
{
	Math::ParamTex2d<unsigned char, Math::EJT_MIDDLE> clumppolar;
	Math::ParamTex2d<unsigned char, Math::EJT_MUL> anisotropic;

public:
	virtual int getType(
		)
	{
		return MERGEWITHCLUMP;
	}
	virtual void release()
	{
		delete this;
	};

public:
	// ������������� ������, ������ true ���� ������� � ������� ����������� �� �����
	virtual bool MergeWithClump(
		float affect, 
		const HAIRPARAMS& c_hairs, 			// ��������� ������
		const HAIRVERTS& orighair,		// �������� �����
		const HAIRPARAMS& c_clumphair, 	// ��������� ������ - ������
		const HAIRVERTS& clumphair,		// ����� � ������
		HAIRVERTS& hair					// ���������
		);

public:
	// �������������
	virtual void init(
		);
	// ������ �� UI
	virtual void fromUI(
		artist::IShape* shape
		);
	// ������������
	virtual void serialise(
		Util::Stream& out
		);

public:
	HairAnisoClumpDeformer(void);
	~HairAnisoClumpDeformer(void);
};


extern "C"
{
	HAIRMATH_API IHairDeformer* getAnisoClumpDeformer();
}

HAIRMATH_API IHairDeformer* getAnisoClumpDeformer()
{
	return new HairAnisoClumpDeformer();
}


HairAnisoClumpDeformer::HairAnisoClumpDeformer(void)
{
}

HairAnisoClumpDeformer::~HairAnisoClumpDeformer(void)
{
}

// ������������� ������, ������ true ���� ������� � ������� ����������� �� �����
bool HairAnisoClumpDeformer::MergeWithClump(
	float affect, 
	const HAIRPARAMS& c_hairs, 			// ��������� ������
	const HAIRVERTS& orighair,		// �������� �����
	const HAIRPARAMS& c_clumphair, 	// ��������� ������ - ������
	const HAIRVERTS& clumphair,		// ����� � ������
	HAIRVERTS& hair					// ���������
	)
{
	Math::Matrix4f basis = buildBasis4f(c_hairs.position, c_hairs.normal, c_hairs.tangentU, c_hairs.tangentV);
	basis.scale(Math::Vec4f(c_hairs.length,c_hairs.length,c_hairs.length,1));
	Math::Matrix4f basis_inv = basis.inverted();

	float anifactor = anisotropic.getValue(c_hairs.u, c_hairs.v);

	float polar = clumppolar.getValueAngle(c_hairs.u, c_hairs.v);

	Math::Vec2f vpolar_src = polar2vector(polar);
	Math::Vec3f cpolv(vpolar_src.x, 0, vpolar_src.y);
//	Math::Vec3f cpolv(1, 0, 0);

	// �����������
	hair.cvint.clear();
	hair.vfactor.clear();
	hair.vwidht.clear();
	hair.basas.clear();
	hair.vclump.clear();
	hair.cvint.reserve(orighair.cvint.size());
	hair.vfactor.reserve(orighair.cvint.size());
	hair.vwidht.reserve(orighair.cvint.size());
	hair.basas.reserve(orighair.cvint.size());
	hair.vclump.reserve(orighair.cvint.size());

	int csize = (int)orighair.cvint.size();
	for(int v=0; v<csize; v++)
	{
		float f = orighair.vfactor[v];
//		float cf = 1-clumpRamp.getValue(f);
		float cf = orighair.vclump[v];
		float w = orighair.vwidht[v];
		if( v>=(int)clumphair.cvint.size())
			continue;

		Math::Vec3f shift = clumphair.cvint[v] - orighair.cvint[v];
		Math::Vec3f shift_ani = Math::dot(shift, cpolv)*cpolv;
		shift = shift_ani*anifactor + shift*(1-anifactor);

		Math::Vec3f pt = orighair.cvint[v] + shift*cf;
		Math::Matrix4f m = orighair.basas[v];
		{
			Math::Vec3f pt0 = basis_inv*pt;
			m[3] = Math::Vec4f(pt0, 1);
		}
		hair.cvint.push_back(pt);
		hair.vfactor.push_back(f);
		hair.vwidht.push_back(w);
		hair.basas.push_back(m);
		hair.vclump.push_back(cf);
	}
	return !hair.cvint.empty();
}

void HairAnisoClumpDeformer::init(		
	)
{
}
// ������ �� UI
void HairAnisoClumpDeformer::fromUI(
	artist::IShape* shape
	)
{
	Math::ParamTex<unsigned char>* p = (Math::ParamTex<unsigned char>*)&clumppolar;
	shape->getValue(*p, "clumpPolar", 1.f);

	Math::ParamTex<unsigned char>* p2 = (Math::ParamTex<unsigned char>*)&anisotropic;
	shape->getValue(*p2, "anisotropicPower", 1.f);
}

// ������������
void HairAnisoClumpDeformer::serialise(
	Util::Stream& out
	)
{
	int version = 1;
	out >> version;
	if( version>=0)
	{
		out>>clumppolar;
	}
	if( version>=1)
	{
		out>>anisotropic;
	}
}
