#include "StdAfx.h"
#include "HairMath.h"
#include "IHairDeformer.h"

// ����� ����� �� ������������ ������
// ������������ ��� ��������������� ���� ������� ��������������� �������� �������
// 
class PolarFromControlsDeformer : public IHairDeformer
{
public:
	virtual int getType(
		)
	{
		return PREPARSEHAIR;
	}
	virtual void release()
	{
		delete this;
	};

public:
	// �������� ��������� ������, ���������� �� ��������� ���������� � 2d �������
	virtual void PreParseHair(
		float affect, 
		HAIRPARAMS& c_hairs,
		bool bClump
		);

public:
	// �������������
	virtual void init(
		);
	// ������ �� UI
	virtual void fromUI(
		artist::IShape* shape
		);
	// ������������
	virtual void serialise(
		Util::Stream& out
		);

public:
	PolarFromControlsDeformer(void);
	~PolarFromControlsDeformer(void);
};

extern "C"
{
	HAIRMATH_API IHairDeformer* getPolarFromControlsDeformer();
}

HAIRMATH_API IHairDeformer* getPolarFromControlsDeformer()
{
	return new PolarFromControlsDeformer();
}

PolarFromControlsDeformer::PolarFromControlsDeformer(void)
{
}

PolarFromControlsDeformer::~PolarFromControlsDeformer(void)
{
}

// ��� ���� ���������� ������� ������� ���������, � ����� ����� ��������� ����� ��������������� ������
void PolarFromControlsDeformer::PreParseHair(
	float affect, 
	HAIRPARAMS& c_hairs,
	bool bClump
	)
{
	affect = __max(0, affect);
	affect = __min(1, affect);

	if( !c_hairs.cv.bValid)
		return;

	// ���� �� 0� ������ � ts
	Math::Vec2f vpolar(0);
	for( int i=1; i<(int)c_hairs.cv.ts.size(); i++)
	{
		Math::Vec3f v = c_hairs.cv.ts[i] - c_hairs.cv.ts[0];
		vpolar = Math::Vec2f(v.x, v.z);
		if( vpolar.length()<0.0001) continue;
		break;
	}
	if( vpolar.length()<0.0001) 
		return;
	c_hairs.polar = vector2polar(-vpolar);
}

void PolarFromControlsDeformer::init(		
	)
{
}
// ������ �� UI
void PolarFromControlsDeformer::fromUI(
	artist::IShape* shape
	)
{
}

// ������������
void PolarFromControlsDeformer::serialise(
	Util::Stream& out
	)
{
	int version = 0;
	out >> version;
	if( version>=0)
	{
	}
}
