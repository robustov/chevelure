#pragma once

#include "IHairDeformer.h"

class HairLeafDeformer : public IHairDeformer
{
	enum enType
	{
		USE_IDENTITY=0,		// ������������ ������ ����� 1 �� ������
		USE_BASEWIDTH=1,		// ������������ ������ ������ � �����
		USE_TIPWIDTH=2,		// ������������ ������ ������ �� �����
		USE_BOTH=3,			// ��������������� ������ ����������� ��������, ����� ��������
	};
	enType type;

	Math::Ramp rampWidth;

public:
	virtual int getType(
		)
	{
		return PARSEHAIR|POSTBUILDHAIR;
	}
	virtual void release()
	{
		delete this;
	};

public:
	// ��������� ������
	virtual void ParseHair(
		float affect, 
		HAIRPARAMS& c_hairs,
		bool bClump
		);
	// ��� ���� ���������� ������� ������� ���������, � ����� ����� ��������� ����� ��������������� ������
	virtual void PostBuildHair(
		float affect, 
		const HAIRPARAMS& c_hairs, 			// ��������� ������
		int SegmCount,					// ����� ���������
		HAIRVERTS& hair					// ���������
		);

public:
	// �������������
	virtual void init(
		);
	// ������ �� UI
	virtual void fromUI(
		artist::IShape* shape
		);
	// ������������
	virtual void serialise(
		Util::Stream& out
		);

public:
	HairLeafDeformer(void);
	~HairLeafDeformer(void);
};
