#include "StdAfx.h"
#include "HairMath.h"
#include "IHairDeformer.h"
#include "Math/PerlinNoise.h"

// ��������� � �����������
// ���������!!!
class StickySurfaceDeformer : public IHairDeformer
{
public:
	virtual int getType(
		)
	{
		return POSTBUILDHAIR;
	}
	virtual void release()
	{
		delete this;
	};

public:
	// ��� ���� ���������� ������� ������� ���������, � ����� ����� ��������� ����� ��������������� ������
	virtual void PostBuildHair(
		float affect, 
		const HAIRPARAMS& c_hairs, 			// ��������� ������
		int SegmCount,					// ����� ���������
		HAIRVERTS& hair					// ���������
		);

public:
	// �������������
	virtual void init(
		);
	// ������ �� UI
	virtual void fromUI(
		artist::IShape* shape
		);
	// ������������
	virtual void serialise(
		Util::Stream& out
		);

public:
	StickySurfaceDeformer(void);
	~StickySurfaceDeformer(void);
};

extern "C"
{
	HAIRMATH_API IHairDeformer* getStickySurfaceDeformer();
}

HAIRMATH_API IHairDeformer* getStickySurfaceDeformer()
{
	return new StickySurfaceDeformer();
}

StickySurfaceDeformer::StickySurfaceDeformer(void)
{
}

StickySurfaceDeformer::~StickySurfaceDeformer(void)
{
}

// ��� ���� ���������� ������� ������� ���������, � ����� ����� ��������� ����� ��������������� ������
void StickySurfaceDeformer::PostBuildHair(
	float affect, 
	const HAIRPARAMS& c_hairs, 			// ��������� ������
	int SegmCount,					// ����� ���������
	HAIRVERTS& hair					// ���������
	)
{
	affect = __max(0, affect);
	affect = __min(1, affect);

	// ���������
	Math::Vec3f pp = c_hairs.position;
	Math::Vec3f pn = c_hairs.normal;
	pn.normalize();

	srand(c_hairs.seed+56);
	float dx = rand()/(float)RAND_MAX;
	float dy = rand()/(float)RAND_MAX;
	dx = 0.2f;
	dy = (float)M_PI;
	std::vector<Math::Vec3f> cvintnew(hair.cvint.size());
	cvintnew[0] = hair.cvint[0];
	int i;
	for(i=1; i<(int)hair.cvint.size(); i++)
	{
		Math::Vec3f dir = hair.cvint[i] - hair.cvint[i-1];
		float l = dir.length();
		dir = dir-pn*Math::dot( dir, pn);
		dir.normalize();
		dir*=l;

		cvintnew[i] = cvintnew[i-1]+dir;
	}
	for(i=0; i<(int)hair.cvint.size(); i++)
	{
		hair.cvint[i] = (1-affect)*hair.cvint[i] + cvintnew[i]*affect;
	}
}

void StickySurfaceDeformer::init(		
	)
{
}
// ������ �� UI
void StickySurfaceDeformer::fromUI(
	artist::IShape* shape
	)
{
}

// ������������
void StickySurfaceDeformer::serialise(
	Util::Stream& out
	)
{
	int version = 0;
	out >> version;
	if( version>=0)
	{
	}
}
