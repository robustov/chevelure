#include "StdAfx.h"
#include "IHairDeformer.h"
#include "Math/PerlinNoise.h"

// ��������� � �����������
// ���������!!!
class RenderRootPosDeformer : public IHairDeformer
{
public:
	virtual int getType(
		)
	{
		return RENDERATTRIBUTES;
	}
	virtual void release()
	{
		delete this;
	};

public:
	// �������� �� ������, ���� ����� ���� ��������� ��������
	// ���� ����� RENDERATTRIBUTES
	virtual bool renderGroupStart(
		int hairCount,					// ���������
		int SegmCount,					// ...
		bool bCubic,					// ...
		cls::IRender* render			// ���� ������ ������
		);
	virtual bool renderHair(
		int index, 
		const HAIRPARAMS& c_hairs, 		// ��������� ������
		const HAIRVERTS& hair,			// �������� ������
		bool bCubic						// ���� ������������ ����������
		);

public:
	// �������������
	virtual void init(
		);
	// ������ �� UI
	virtual void fromUI(
		artist::IShape* shape
		);
	// ������������
	virtual void serialise(
		Util::Stream& out
		);

public:
	RenderRootPosDeformer(void);
	~RenderRootPosDeformer(void);
};

extern "C"
{
	HAIRMATH_API IHairDeformer* getRenderRootPosDeformer();
}

HAIRMATH_API IHairDeformer* getRenderRootPosDeformer()
{
	return new RenderRootPosDeformer();
}

RenderRootPosDeformer::RenderRootPosDeformer(void)
{
}

RenderRootPosDeformer::~RenderRootPosDeformer(void)
{
}

// �������� �� ������, ���� ����� ���� ��������� ��������
// ���� ����� RENDERATTRIBUTES
bool RenderRootPosDeformer::renderGroupStart(
	int hairCount,					// ���������
	int SegmCount,					// ...
	bool bCubic,					// ...
	cls::IRender* render			// ���� ������ ������
	)
{
	return false;
}
bool RenderRootPosDeformer::renderHair(
	int index, 
	const HAIRPARAMS& c_hairs, 		// ��������� ������
	const HAIRVERTS& hair,			// �������� ������
	bool bCubic						// ���� ������������ ����������
	)
{
	return false;
}

void RenderRootPosDeformer::init(		
	)
{
}
// ������ �� UI
void RenderRootPosDeformer::fromUI(
	artist::IShape* shape
	)
{
}

// ������������
void RenderRootPosDeformer::serialise(
	Util::Stream& out
	)
{
	int version = 0;
	out >> version;
	if( version>=0)
	{
	}
}
