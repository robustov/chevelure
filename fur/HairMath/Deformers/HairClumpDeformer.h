#pragma once

#include "IHairDeformer.h"

class HairClumpDeformer : public IHairDeformer
{
//	float clumpRadius;
	int clumpNoLerpType;

//	float baseGlue, tipGlue;

	Math::ParamTex2d<unsigned char, Math::EJT_MUL> val_baseGlue;
	Math::ParamTex2d<unsigned char, Math::EJT_MUL> val_tipGlue;

//	float clumpPercent,clumpRate;

//	bool bAnisotropic;
//	Math::ParamTex<unsigned char, Math::EJT_MUL> anisotropic;
//	Math::ParamTex<unsigned char, Math::EJT_MIDDLE> clumppolar;

public:
	virtual int getType(
		)
	{
		return POSTBUILDHAIR;
		//|MERGEWITHCLUMP|PARSEHAIR|
	}
	virtual void release();
public:

	/*/
	// ��������� ������
	virtual void ParseHair(
		HAIRPARAMS& c_hairs,
		bool bClump
		);
	/*/
	// ��� ���� ���������� ������� ������� ���������, � ����� ����� ��������� ����� ��������������� ������
	virtual void PostBuildHair(
		float affect, 
		const HAIRPARAMS& c_hairs, 			// ��������� ������
		int SegmCount,					// ����� ���������
		HAIRVERTS& hair					// ���������
		);

public:
	// �������������
	virtual void init(
		);
	// ������ �� UI
	virtual void fromUI(
		artist::IShape* shape
		);
	// ������������
	virtual void serialise(
		Util::Stream& out
		);

public:
	HairClumpDeformer(void);
	~HairClumpDeformer(void);
};
