#pragma once

#include "IHairDeformer.h"
#include "Math/PerlinNoise.h"

class HairNoiseDeformer : public IHairDeformer
{
	int seed;
	float t;
	Math::Vec3f freq;
	Math::Vec3f amp, amplen;
	Math::Vec3f timefreq;
	float time;

	Math::PerlinNoise noise;

public:
	virtual int getType(
		)
	{
		return POSTBUILDHAIR;
	}
	virtual void release()
	{
		delete this;
	};

public:
	// ��� ���� ���������� ������� ������� ���������, � ����� ����� ��������� ����� ��������������� ������
	virtual void PostBuildHair(
		float affect, 
		const HAIRPARAMS& c_hairs, 			// ��������� ������
		int SegmCount,					// ����� ���������
		HAIRVERTS& hair					// ���������
		);

public:
	// �������������
	virtual void init(
		);
	// ������ �� UI
	virtual void fromUI(
		artist::IShape* shape
		);
	// ������������
	virtual void serialise(
		Util::Stream& out
		);

public:
	HairNoiseDeformer(void);
	~HairNoiseDeformer(void);
};
