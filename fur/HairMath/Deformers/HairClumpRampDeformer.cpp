#include "StdAfx.h"
#include "HairMath.h"
#include ".\HairClumpRampDeformer.h"

// ������� ����� ������� ������

extern "C"
{
	HAIRMATH_API IHairDeformer* getClumpRampDeformer();
}

HAIRMATH_API IHairDeformer* getClumpRampDeformer()
{
	return new HairClumpRampDeformer();
}


HairClumpRampDeformer::HairClumpRampDeformer(void)
{
	type = IGNORE_TIP_BASE;
}

HairClumpRampDeformer::~HairClumpRampDeformer(void)
{
}

// ��� ���� ���������� ������� ������� ���������, � ����� ����� ��������� ����� ��������������� ������
void HairClumpRampDeformer::PostBuildHair(
	float affect, 
	const HAIRPARAMS& c_hairs, 			// ��������� ������
	int SegmCount,					// ����� ���������
	HAIRVERTS& hair					// ���������
	)
{
	float faffect = affect;
	faffect = __max(0, faffect);
	faffect = __min(1, faffect);

	int csize = (int)hair.vfactor.size();
	for(int v=0; v<csize; v++)
	{
		float f = hair.vfactor[v];
		float cf = 1-clumpRamp.getValue(f);
		float oldcf = hair.vclump[v];
		float newc = oldcf;

		switch( type)
		{
			case IGNORE_TIP_BASE:
			{
				newc = cf;
				break;
			}
			case PRODUCT_TIP_BASE:
			{
				newc = cf*oldcf;
				break;
			}
		}
		hair.vclump[v] = faffect*newc + (1-faffect)*oldcf;
	}
}

/*/
// ������������� ������, ������ true ���� ������� � ������� ����������� �� �����
bool HairClumpRampDeformer::MergeWithClump(
	const HAIRPARAMS& c_hairs, 			// ��������� ������
	const HAIRVERTS& orighair,		// �������� �����
	const HAIRPARAMS& c_clumphair, 	// ��������� ������ - ������
	const HAIRVERTS& clumphair,		// ����� � ������
	HAIRVERTS& hair					// ���������
	)
{
	Math::Matrix4f basis = buildBasis4f(c_hairs.position, c_hairs.normal, c_hairs.tangentU, c_hairs.tangentV);
	basis.scale(Math::Vec4f(c_hairs.length,c_hairs.length,c_hairs.length,1));
	Math::Matrix4f basis_inv = basis.inverted();

//	float baseCLUMP = c_hairs.BaseClumpValue;
//	float tipCLUMP  = c_hairs.TipClumpValue;
//	float clumpSlope = tipCLUMP-baseCLUMP;

	// �����������
	hair.cvint.clear();
	hair.vfactor.clear();
	hair.vwidht.clear();
	hair.basas.clear();
	hair.cvint.reserve(orighair.cvint.size());
	hair.vfactor.reserve(orighair.cvint.size());
	hair.vwidht.reserve(orighair.cvint.size());
	hair.basas.reserve(orighair.cvint.size());

	int csize = orighair.cvint.size();
	hair.clump_vectorV.reserve(csize);
	for(int v=0; v<csize; v++)
	{
		float f = orighair.vfactor[v];
		float cf = 1-clumpRamp.getValue(f);
		float w = orighair.vwidht[v];
		if( v>=(int)clumphair.cvint.size())
			continue;

		Math::Vec3f pt = (cf)*clumphair.cvint[v] + (1-cf)*orighair.cvint[v];
		Math::Matrix4f m = orighair.basas[v];
		{
			Math::Vec3f pt0 = basis_inv*pt;
			m[3] = Math::Vec4f(pt0, 1);
		}
		hair.cvint.push_back(pt);
		hair.vfactor.push_back(f);
		hair.vwidht.push_back(w);
		hair.basas.push_back(m);
		hair.clump_vectorV.push_back(clumphair.cvint[v]-orighair.cvint[v]);
	}
	return !hair.cvint.empty();
}
/*/

void HairClumpRampDeformer::init(		
	)
{
}
// ������ �� UI
void HairClumpRampDeformer::fromUI(
	artist::IShape* shape
	)
{
	shape->getValue(clumpRamp, "rampClump", 0, 1, 0);
	shape->getValue(*(int*)&type, "sourceClump", sn::UNIFORM_SHAPE);

	Math::ParamTex<unsigned char>* p2 = (Math::ParamTex<unsigned char>*)&affect;
	shape->getValue(*p2, "envelope", 1.f);
}

// ������������
void HairClumpRampDeformer::serialise(
	Util::Stream& out
	)
{
	int version = 2;
	out >> version;
	if( version>=0)
	{
		out>>clumpRamp;
	}
	if( version>=1)
	{
		out>>*(int*)&type;
	}
	if( version>=2)
	{
		out>>affect;
	}
	else
	{
		affect.defValue = 1;
	}
	
}
