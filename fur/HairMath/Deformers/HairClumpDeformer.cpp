#include "StdAfx.h"
#include "HairMath.h"
#include ".\HairClumpDeformer.h"

// ������������� ����� �������
// ������ ������������

extern "C"
{
	HAIRMATH_API IHairDeformer* getClumpDeformer();
}

HAIRMATH_API IHairDeformer* getClumpDeformer()
{
	return new HairClumpDeformer();
}

void HairClumpDeformer::release()
{
	delete this;
};


HairClumpDeformer::HairClumpDeformer(void)
{
}

HairClumpDeformer::~HairClumpDeformer(void)
{
}

/*/
// ��������� ������
void HairClumpDeformer::ParseHair(
	HAIRPARAMS& c_hairs,
	bool bClump
	)
{
	if( bClump) return;

	if(	c_hairs.clumpIndex==-1)
		return;

	if( c_hairs.toclump.length()<clumpRadius)
		return;

	c_hairs.clumpIndex = -1;
	c_hairs.toclump = Math::Vec3f(0);
	c_hairs.toclumpUV = Math::Vec2f(0);
}
/*/
inline float sinproc(float p)
{
	if( p>M_PI) return 1;
	if( p<0) return -1;

	return -cos(p);
}
inline float finproc(float p, float p0, float p1, float f0, float f1)
{
	p = p*(p1-p0)+p0;
	if( p>M_PI) p=(float)M_PI;
	if( p<0) p=0;
	float f = -cos(p);
	f = (f-f0)/(f1-f0);
	return f;
}

// ��� ���� ���������� ������� ������� ���������, � ����� ����� ��������� ����� ��������������� ������
void HairClumpDeformer::PostBuildHair(
	float affect, 
	const HAIRPARAMS& c_hairs, 			// ��������� ������
	int SegmCount,					// ����� ���������
	HAIRVERTS& hair					// ���������
	)
{
	affect = __max(0, affect);
	affect = __min(1, affect);

	float baseGlue = val_baseGlue.getValue(c_hairs.u, c_hairs.v);
	float tipGlue  = val_tipGlue.getValue(c_hairs.u, c_hairs.v);

	if( baseGlue<0) baseGlue = 0;
	if( tipGlue<0) tipGlue = 0;
	float p0 = (float) M_PI * (1-baseGlue)*0.499f;
	float p1 = (float) M_PI*( 1-(1-tipGlue)*0.499f);
	float f0 = sinproc(p0);
	float f1 = sinproc(p1);

	float baseCLUMP = c_hairs.BaseClumpValue;// * this->BaseClumpValue;
	float tipCLUMP  = c_hairs.TipClumpValue;// * this->TipClumpValue;

	float clumpSlope = tipCLUMP-baseCLUMP;

	int csize = (int)hair.cvint.size();
	for(int v=0; v<csize; v++)
	{
		float f = hair.vfactor[v];
		float oldcf = hair.vclump[v];
		float cf = f;
		switch( clumpNoLerpType)
		{
		case 1:
			cf = (sin(f*(float)M_PI*0.5f));
			break;
		case 2:
			cf = (-cos(f*(float)M_PI)+1)*0.5f;
			break;
		case 3:
			cf = (-cos(f*(float)M_PI*0.5f)+1);
			break;
		case 4:
			cf = finproc(f, p0, p1, f0, f1);
			break;
//		case 5:
//			cf = clumpPercent*(f + clumpRate * (1 - f));
//			break;
		}
		if( clumpNoLerpType!=5)
		{
			cf = baseCLUMP*(1-cf)+cf*tipCLUMP;
		}
		hair.vclump[v] = affect*cf + (1-affect)*oldcf;
	}
}
/*/
// ������������� ������, ������ true ���� ������� � ������� ����������� �� �����
bool HairClumpDeformer::MergeWithClump(
	const HAIRPARAMS& c_hairs, 			// ��������� ������
	const HAIRVERTS& orighair,		// �������� �����
	const HAIRPARAMS& c_clumphair, 	// ��������� ������ - ������
	const HAIRVERTS& clumphair,		// ����� � ������
	HAIRVERTS& hair					// ���������
	)
{
	if( baseGlue<0) baseGlue = 0;
	if( tipGlue<0) tipGlue = 0;
	float p0 = (float) M_PI * (1-baseGlue)*0.499f;
	float p1 = (float) M_PI*( 1-(1-tipGlue)*0.499f);
	float f0 = sinproc(p0);
	float f1 = sinproc(p1);

	Math::Matrix4f basis = buildBasis4f(c_hairs.position, c_hairs.normal, c_hairs.tangentU, c_hairs.tangentV);
	basis.scale(Math::Vec4f(c_hairs.length,c_hairs.length,c_hairs.length,1));
	Math::Matrix4f basis_inv = basis.inverted();

	float baseCLUMP = c_hairs.BaseClumpValue;// * this->BaseClumpValue;
	float tipCLUMP  = c_hairs.TipClumpValue;// * this->TipClumpValue;

	float clumpSlope = tipCLUMP-baseCLUMP;
//	normal = normal*baseCLUMP + clumphairc_hairs.normal*(1-baseCLUMP);

	float anifactor = 0;
	if(bAnisotropic)
		anifactor = anisotropic.getValue(c_hairs.u, c_hairs.v);

	float polar = clumppolar.getValueAngle(c_hairs.u, c_hairs.v);
	Math::Vec2f vpolar_src = polar2vector(polar);
	Math::Vec3f cpolv(vpolar_src.x, 0, vpolar_src.y);

	// �����������
	hair.cvint.clear();
	hair.vfactor.clear();
	hair.vwidht.clear();
	hair.vclump.clear();
	hair.basas.clear();
	hair.cvint.reserve(orighair.cvint.size());
	hair.vfactor.reserve(orighair.cvint.size());
	hair.vwidht.reserve(orighair.cvint.size());
	hair.vclump.reserve(orighair.cvint.size());
	hair.basas.reserve(orighair.cvint.size());

	int csize = orighair.cvint.size();
	for(int v=0; v<csize; v++)
	{
		float f = orighair.vfactor[v];
		float cf = f;
		switch( clumpNoLerpType)
		{
		case 1:
			cf = (sin(f*(float)M_PI*0.5f));
			break;
		case 2:
			cf = (-cos(f*(float)M_PI)+1)*0.5f;
			break;
		case 3:
			cf = (-cos(f*(float)M_PI*0.5f)+1);
			break;
		case 4:
			cf = finproc(f, p0, p1, f0, f1);
			break;
		case 5:
			cf = clumpPercent*(f + clumpRate * (1 - f));
			break;
		}
		float w = orighair.vwidht[v];
		if( clumpNoLerpType!=5)
		{
			cf = baseCLUMP*(1-cf)+cf*tipCLUMP;
		}
		if( v>=(int)clumphair.cvint.size())
			continue;

		Math::Vec3f shift = clumphair.cvint[v] - orighair.cvint[v];
		if( anifactor!=0)
		{
			Math::Vec3f shift_ani = Math::dot(shift, cpolv)*cpolv;
			shift = shift_ani*anifactor + shift*(1-anifactor);
		}
		Math::Vec3f pt = orighair.cvint[v] + shift*cf;

//		Math::Vec3f pt = (cf)*clumphair.cvint[v] + (1-cf)*orighair.cvint[v];
		Math::Matrix4f m = orighair.basas[v];
		{
			Math::Vec3f pt0 = basis_inv*pt;
			m[3] = Math::Vec4f(pt0, 1);
		}
		hair.cvint.push_back(pt);
		hair.vfactor.push_back(f);
		hair.vwidht.push_back(w);
		hair.basas.push_back(m);
		hair.vclump.push_back(cf);
		
//		hair.clump_vectorV.push_back(clumphair.cvint[v]-orighair.cvint[v]);
	}
	return !hair.cvint.empty();
}
/*/

void HairClumpDeformer::init(		
	)
{
}
// ������ �� UI
void HairClumpDeformer::fromUI(
	artist::IShape* shape
	)
{
//	shape->getValue(clumpRadius, "clumpRadius", sn::UNIFORM_SHAPE, 1, 0, 5);
	shape->getValue(clumpNoLerpType, "clumpNoLerpType", sn::UNIFORM_SHAPE, 0, 0, 6);

	Math::ParamTex<unsigned char>* p = (Math::ParamTex<unsigned char>*)&val_baseGlue;
	shape->getValue(*p, "baseGlue", 0.f);

	Math::ParamTex<unsigned char>* p2 = (Math::ParamTex<unsigned char>*)&val_tipGlue;
	shape->getValue(*p2, "tipGlue", 0.f);

//	shape->getValue(baseGlue, "baseGlue", sn::UNIFORM_SHAPE, 0, 0, 1);
//	shape->getValue(tipGlue, "tipGlue", sn::UNIFORM_SHAPE, 0, 0, 1);

//	shape->getValue(clumpPercent, "clumpPercent", sn::UNIFORM_SHAPE, 0, -5, 5);
//	shape->getValue(clumpRate, "clumpRate", sn::UNIFORM_SHAPE, 0, -5, 5);
/*/
	{
		shape->getValue(bAnisotropic, "anisotropic", sn::UNIFORM_SHAPE, false);
		Math::ParamTex<unsigned char>* p = (Math::ParamTex<unsigned char>*)&clumppolar;
		shape->getValue(*p, "clumpPolar", 1.f);

		Math::ParamTex<unsigned char>* p2 = (Math::ParamTex<unsigned char>*)&anisotropic;
		shape->getValue(*p2, "anisotropicPower", 1.f);
	}
/*/
}

// ������������
void HairClumpDeformer::serialise(
	Util::Stream& out
	)
{
	int version = 6;
	out >> version;
	if( version==6)
	{
		out>>clumpNoLerpType;
		out>>val_baseGlue;
		out>>val_tipGlue;
	}	
}
