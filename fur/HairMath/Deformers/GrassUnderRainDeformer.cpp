#include "StdAfx.h"
#include "HairMath.h"
#include "IHairDeformer.h"
#include "Math/PerlinNoise.h"

// ��������� ������� ������ �� �����
// ������ ����������� � ��������� ������ � �������� ��������������

class GrassUnderRainDeformer : public IHairDeformer
{
	float time;
	// ��������� ������
	int seed;
	float drop_frequency;
	Math::ParamTex2d<unsigned char, Math::EJT_MUL> drop_dencity;
	// ���������
	float frequency;
	float dump;
	float amplitude;

//	Math::PerlinNoise noise;

public:
	virtual int getType(
		)
	{
		return PARSEHAIR;
	}
	virtual void release()
	{
		delete this;
	};

public:
	// ��� ���� ���������� ������� ������� ���������, � ����� ����� ��������� ����� ��������������� ������
	virtual void ParseHair(
		float affect, 
		HAIRPARAMS& c_hairs,
		bool bClump
		);

public:
	// �������������
	virtual void init(
		);
	// ������ �� UI
	virtual void fromUI(
		artist::IShape* shape
		);
	// ������������
	virtual void serialise(
		Util::Stream& out
		);

public:
	GrassUnderRainDeformer(void);
	~GrassUnderRainDeformer(void);
};

extern "C"
{
	HAIRMATH_API IHairDeformer* getGrassUnderRainDeformer();
}

HAIRMATH_API IHairDeformer* getGrassUnderRainDeformer()
{
	return new GrassUnderRainDeformer();
}






// -------------------------------------------------------

GrassUnderRainDeformer::GrassUnderRainDeformer(void)
{
}

GrassUnderRainDeformer::~GrassUnderRainDeformer(void)
{
}

// ��� ���� ���������� ������� ������� ���������, � ����� ����� ��������� ����� ��������������� ������
void GrassUnderRainDeformer::ParseHair(
	float affect, 
	HAIRPARAMS& c_hairs,
	bool bClump
	)
{
	affect = __max(0, affect);
	affect = __min(1, affect);

	int seed = this->seed + c_hairs.seed;
	srand(seed);

	// ������� 
	float p = this->time * this->drop_frequency;
	p = p + rand()/(float)RAND_MAX;
	int number = (int)floor(p);
	p = p - number;

	// ��������� (0-1)
	srand(seed+number+100);
	float d = this->drop_dencity.getValue(c_hairs.u, c_hairs.v);
	if( rand()>d*RAND_MAX)
		return;

	// ��������� �� ����������
	float time = p/this->drop_frequency;
	float a = amplitude*( exp(-time*this->dump) - exp(-this->dump/this->drop_frequency));
	float val = sin(time*this->frequency)*a;

	Math::Vec2f polarv = polar2vector(c_hairs.polar);
	c_hairs.tipCurl += polarv*val;
	/*/
	srand(c_hairs.seed+56);
	float dx = rand()/(float)RAND_MAX;
	float dy = rand()/(float)RAND_MAX;
	dx = 0.2f;
	dy = (float)M_PI;
	for(int i=0; i<(int)hair.vwidht.size(); i++)
	{
		float f = hair.vfactor[i];
//		float f = i/(hair.vwidht.size()-1.f);
		Math::Vec3f v( c_hairs.u, f, c_hairs.v);
		v *= freq;
		Math::Vec3f vt( time*timefreq.x, time*timefreq.y, time*timefreq.z);
		v += vt;

		Math::Vec3f vx = v + Math::Vec3f(dx, 0, dx);
		Math::Vec3f vy = v + Math::Vec3f(dy, 0, dy);
		float a = noise.noise(vx.x, vx.y, vx.z);
		float b = noise.noise(vy.x, vy.y, vy.z);
		a *= amp.x;
		b *= amp.z;

		Math::Vec3f of(a, 0, b);
		of = Math::Matrix4As3f(hair.basas[i])*of;

		float factor = (1-f)*amplen.x + f*amplen.z;
		of *= factor;
		of *= 1/hair.vwidht[i];
		of *= affect;
		hair.cvint[i] += of;
	}
	/*/
}

void GrassUnderRainDeformer::init(		
	)
{
//	noise.Init(seed);
}
// ������ �� UI
void GrassUnderRainDeformer::fromUI(
	artist::IShape* shape
	)
{
	shape->getValue(seed, "seed", sn::UNIFORM_SHAPE, 0, 0, 1000);
	shape->getValue(time, "time", sn::UNIFORM_SHAPE, 0, 0, 1);
	shape->getValue(drop_frequency, "dropFrequency", sn::UNIFORM_SHAPE, 1, 0, 1);

	Math::ParamTex<unsigned char>* p2 = (Math::ParamTex<unsigned char>*)&drop_dencity;
	shape->getValue(*p2, "dropDencity", 1.f);

	shape->getValue(frequency, "frequency", sn::UNIFORM_SHAPE, 1, 0, 1);
	shape->getValue(dump, "dump", sn::UNIFORM_SHAPE, 1, 0, 1);
	shape->getValue(amplitude, "amplitude", sn::UNIFORM_SHAPE, 1, 0, 1);
}

// ������������
void GrassUnderRainDeformer::serialise(
	Util::Stream& out
	)
{
	int version = 0;
	out >> version;
	if( version>=0)
	{
		out>>seed;
		out>>time;
		out>>drop_frequency;
		out>>drop_dencity;
		out>>frequency;
		out>>dump;
		out>>amplitude;
	}
}
