#pragma once

#include "IHairDeformer.h"
#include "Math/matrixMxN.h"

class HairCombDeformer : public IHairDeformer
{

public:
	virtual int getType(
		)
	{
		return PREPARSEHAIR|PARSEHAIR;
	}
	virtual void release()
	{
		delete this;
	};

	Math::matrixMxN<float> polarmap;

	Math::matrixMxN<float> length;
public:
	// �������� ��������� ������, ���������� �� ��������� ���������� � 2d �������
	virtual void PreParseHair(
		float affect, 
		HAIRPARAMS& c_hairs,
		bool bClump
		);
	// �������� ��������� ������
	virtual void ParseHair(
		float affect, 
		HAIRPARAMS& c_hairs,
		bool bClump
		);

public:
	// �������������
	virtual void init(
		);
	// ������ �� UI
	virtual void fromUI(
		artist::IShape* shape
		);
	// ������������
	virtual void serialise(
		Util::Stream& out
		);

public:
	HairCombDeformer(void);
	~HairCombDeformer(void);
};
