#include "StdAfx.h"
#include "HairMath.h"
#include ".\hairgrassdeformer.h"

// ���������� ������� �� �����
// ����
// base clump ��������� � 1
// tip clump � 0 ����� ����� ��� ��������

extern "C"
{
	HAIRMATH_API IHairDeformer* getGrassDeformer();
}
HAIRMATH_API IHairDeformer* getGrassDeformer()
{
	return new HairGrassDeformer();
}

HairGrassDeformer::HairGrassDeformer(void)
{
}

HairGrassDeformer::~HairGrassDeformer(void)
{
}

// ��������� ������
void HairGrassDeformer::ParseHair(
	float affect, 
	HAIRPARAMS& c_hairs,
	bool bClump
	)
{
	float f1=1, f2=1, f3=1;
	if( c_hairs.clumpIndex != -1)
	{
		float l = c_hairs.toclumpUV.length();
		float f = exp(-l*affectLength);
		f = (float)sin( f*M_PI*0.5);
		f = (float)sqrt( f);
		c_hairs.length = c_hairs.length*f;
		c_hairs.inclination += c_hairs.toclumpUV*affectInclination;
		c_hairs.baseCurl += c_hairs.toclumpUV*affectBaseCurl;
		c_hairs.tipCurl += c_hairs.toclumpUV*affectTipCurl;
	}
}

void HairGrassDeformer::init(		
	)
{
}
// ������ �� UI
void HairGrassDeformer::fromUI(
	artist::IShape* shape
	)
{
	shape->getValue(affectLength, "affectLength", sn::UNIFORM_SHAPE, 0.f, 0, 30);
	shape->getValue(affectTipCurl, "affectTipCurl", sn::UNIFORM_SHAPE, 1.f, 0, 30);
	shape->getValue(affectBaseCurl, "affectBaseCurl", sn::UNIFORM_SHAPE, 1.f, 0, 30);
	shape->getValue(affectInclination, "affectInclination", sn::UNIFORM_SHAPE, 1.f, 0.0f, 10);
}

// ������������
void HairGrassDeformer::serialise(
	Util::Stream& out
	)
{
	int version = 1;
	out >> version;
	if( version>=0)
	{
		out>>affectLength;
		out>>affectTipCurl;
		out>>affectBaseCurl;
		out>>affectInclination;
	}
}