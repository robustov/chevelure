#include "StdAfx.h"
#include "HairMath.h"

#include "IHairDeformer.h"

// ���� ����� ���� ������ �������� �� ������� � ��������� � ����� 

class HairDeformer_Lenght : public IHairDeformer
{
	Math::ParamTex2d<unsigned char, Math::EJT_MUL> mul;
	Math::ParamTex2d<unsigned char, Math::EJT_MUL> add;

public:
	virtual int getType(
		)
	{
		return INCREASELENGTH;
	}
	virtual void release()
	{
		delete this;
	};
	// ���� ���� ��������� ����� ������ 
	// ��������� � ������� �����!!! ����� �������� �����!!!
	virtual void IncreaseHairLength( 
		float u, float v, float& length
		);
	virtual void IncreaseMaxLength(
		float& length
		);
public:
	// �������������
	virtual void init(
		);
	// ������ �� UI
	virtual void fromUI(
		artist::IShape* shape
		);
	// ������������
	virtual void serialise(
		Util::Stream& out
		);
public:
	HairDeformer_Lenght(void){};
	~HairDeformer_Lenght(void){};
};


extern "C"
{
	HAIRMATH_API IHairDeformer* getLenghtDeformer();
}

HAIRMATH_API IHairDeformer* getLenghtDeformer()
{
	return new HairDeformer_Lenght();
}

// ���� ���� ��������� ����� ������ 
// ��������� � ������� �����!!! ����� �������� �����!!!
void HairDeformer_Lenght::IncreaseHairLength( 
	float u, float v, float& length
	)
{
	float prod = this->mul.getValueSource(u, v);
	float plus = this->add.getValueSource(u, v);

	length = prod*length + plus;
};
void HairDeformer_Lenght::IncreaseMaxLength(
	float& length
	)
{
	float prod = this->mul.getMax();
	float plus = this->mul.getMax();
	length = length*prod + plus;
};


void HairDeformer_Lenght::init(		
	)
{
}
// ������ �� UI
void HairDeformer_Lenght::fromUI(
	artist::IShape* shape
	)
{
	{
		Math::ParamTex<unsigned char>* p2 = (Math::ParamTex<unsigned char>*)&mul;
		shape->getValue(*p2, "mul", 1.f);
	}
	{
		Math::ParamTex<unsigned char>* p2 = (Math::ParamTex<unsigned char>*)&add;
		shape->getValue(*p2, "add", 0.f);
	}
}

// ������������
void HairDeformer_Lenght::serialise(
	Util::Stream& out
	)
{
	int version = 0;
	out >> version;
	if( version>=0)
	{
		out>>mul;
		out>>add;
	}
}
