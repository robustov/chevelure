#include "StdAfx.h"
#include "HairMath.h"
#include ".\HairCombDeformer.h"

extern "C"
{
	HAIRMATH_API IHairDeformer* getCombDeformer();
}

HAIRMATH_API IHairDeformer* getCombDeformer()
{
	return new HairCombDeformer();
}

HairCombDeformer::HairCombDeformer(void)
{
}

HairCombDeformer::~HairCombDeformer(void)
{
}

// �������� ��������� ������, ���������� �� ��������� ���������� � 2d �������
void HairCombDeformer::PreParseHair(
	float affect, 
	HAIRPARAMS& c_hairs,
	bool bClump
	)
{
	if( !polarmap.empty())
	{
		float val = polarmap.interpolation(c_hairs.u, c_hairs.v, false, false);
		c_hairs.polar = val;
	}
}

// ��������� ������
void HairCombDeformer::ParseHair(
	float affect, 
	HAIRPARAMS& c_hairs,
	bool bClump
	)
{
	if( !length.empty())
	{
		float val = length.interpolation(c_hairs.u, c_hairs.v, false, false);
		c_hairs.length = val;
	}
}

void HairCombDeformer::init(		
	)
{
}
// ������ �� UI
void HairCombDeformer::fromUI(
	artist::IShape* shape
	)
{
}

// ������������
void HairCombDeformer::serialise(
	Util::Stream& out
	)
{
	int version = 1;
	out >> version;
	if( version>=1)
	{
		out>>polarmap;
		out>>length;
	}
}
