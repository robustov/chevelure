#include "StdAfx.h"
#include "HairMath.h"
#include "IHairDeformer.h"
#include <Math/Rotation3.h>

// ��� ���� �������������� baseCurl tipCurl � polar

class addDeformDeformer : public IHairDeformer
{
	Math::ParamTex2d<unsigned char, Math::EJT_MIDDLE> polar;
	Math::ParamTex2d<unsigned char, Math::EJT_MIDDLE> val_baseCurl;
	Math::ParamTex2d<unsigned char, Math::EJT_MIDDLE> val_tipCurl;

public:
	virtual int getType(
		)
	{
		return DEFORMHAIRVERTEX;
	}
	virtual void release()
	{
		delete this;
	}

	addDeformDeformer();	

	// ���������� �� ������ ���� ���������� ��������, ����� ������������� m
	virtual void deformHairVertex(
		float affect, 
		const HAIRPARAMS& c_hairs, 			// ��������� ������
		int vertIndex,						// ������ ��������
		int segCount,						// ����� ���������
		Math::Matrix4f& m,					// ������� �������� ��������
		Math::Vec3f pt						// ������� � TS
		);

public:
	// �������������
	virtual void init(
		);
	// ������ �� UI
	virtual void fromUI(
		artist::IShape* shape
		);
	// ������������
	virtual void serialise(
		Util::Stream& out
		);
};

extern "C"
{
	HAIRMATH_API IHairDeformer* getAddDeformDeformer();
}

HAIRMATH_API IHairDeformer* getAddDeformDeformer()
{
	return new addDeformDeformer();
}

addDeformDeformer::addDeformDeformer()
{
}

// ���������� �� ������ ���� ���������� ��������, ����� ������������� m
void addDeformDeformer::deformHairVertex(
	float affect, 
	const HAIRPARAMS& c_hairs, 			// ��������� ������
	int vertIndex,						// ������ ��������
	int segCount,						// ����� ���������
	Math::Matrix4f& mi,					// ������� �������� ��������
	Math::Vec3f pt						// ������� � TS
	)
{
	float polar = this->polar.getValueAngle(c_hairs.u, c_hairs.v);
	Math::Vec2f v = polar2vector(polar);

	// ������� �����
	Math::Matrix4f maintransform = buildBasis4f(
		Math::Vec3f(0), 
		Math::Vec3f(0, 1, 0), 
		Math::Vec3f(1, 0, 0), 
		Math::Vec3f(0, 0, 1));

	float param = (float)vertIndex/((float)segCount);

	Math::Vec2f baseCurl = v*( this->val_baseCurl.getValue(c_hairs.u, c_hairs.v)-0.5f)*(float)M_PI;
	Math::Vec2f tipCurl  = v*( this->val_tipCurl.getValue(c_hairs.u, c_hairs.v)-0.5f)*(float)M_PI;
	float cvBlendFactor = 1;
	{
		
		Math::Vec2f curl = (1-param)*baseCurl + param*tipCurl;

		curl = (curl)/((float)segCount);
		float curlangle = curl.length();
		if(curlangle>0.000001)
		{
			if( vertIndex==0) 
				curlangle*=0.5f; 
			// ������� ������������ ��� ���������������� curl
			Math::Vec3f axis = maintransform*Math::Vec3f(-curl.y, 0, curl.x);
			Math::Rot3f CurlRot( curlangle*cvBlendFactor, axis );
			Math::Matrix4f obj = Math::Matrix4f::id;
			CurlRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());
			mi = obj*mi;
			// ����������� �����!!!
//			sl = 2*sin(curlangle*0.5f)*sl/curlangle;
		}
	}
}


// �������������
void addDeformDeformer::init(
	)
{
}
// ������ �� UI
void addDeformDeformer::fromUI(
	artist::IShape* shape
	)
{
	{
		Math::ParamTex<unsigned char>* p = (Math::ParamTex<unsigned char>*)&polar;
		shape->getValue(*p, "polar", 0.0f);
	}

	{
		Math::ParamTex<unsigned char>* p = (Math::ParamTex<unsigned char>*)&val_baseCurl;
		shape->getValue(*p, "baseCurl", 0.5f);
	}

	{
		Math::ParamTex<unsigned char>* p = (Math::ParamTex<unsigned char>*)&val_tipCurl;
		shape->getValue(*p, "tipCurl", 0.5f);
	}
}
// ������������
void addDeformDeformer::serialise(
	Util::Stream& out
	)
{
	int version = 0;
	out >> version;
	if( version<=0)
	{
		out>>val_baseCurl;
		out>>val_tipCurl;
		out>>polar;
	}	
}

