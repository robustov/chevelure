#include "StdAfx.h"
#include "HairMath.h"
#include ".\HairNoiseDeformer.h"

extern "C"
{
	HAIRMATH_API IHairDeformer* getNoiseDeformer();
}

HAIRMATH_API IHairDeformer* getNoiseDeformer()
{
	return new HairNoiseDeformer();
}

HairNoiseDeformer::HairNoiseDeformer(void)
{
}

HairNoiseDeformer::~HairNoiseDeformer(void)
{
}

// ��� ���� ���������� ������� ������� ���������, � ����� ����� ��������� ����� ��������������� ������
void HairNoiseDeformer::PostBuildHair(
	float affect, 
	const HAIRPARAMS& c_hairs, 			// ��������� ������
	int SegmCount,					// ����� ���������
	HAIRVERTS& hair					// ���������
	)
{
	affect = __max(0, affect);
	affect = __min(1, affect);

	srand(c_hairs.seed+56);
	float dx = rand()/(float)RAND_MAX;
	float dy = rand()/(float)RAND_MAX;
	dx = 0.2f;
	dy = (float)M_PI;
	for(int i=0; i<(int)hair.vwidht.size(); i++)
	{
		float f = hair.vfactor[i];
//		float f = i/(hair.vwidht.size()-1.f);
		Math::Vec3f v( c_hairs.u, f, c_hairs.v);
		v *= freq;
		Math::Vec3f vt( time*timefreq.x, time*timefreq.y, time*timefreq.z);
		v += vt;

		Math::Vec3f vx = v + Math::Vec3f(dx, 0, dx);
		Math::Vec3f vy = v + Math::Vec3f(dy, 0, dy);
		float a = noise.noise(vx.x, vx.y, vx.z);
		float b = noise.noise(vy.x, vy.y, vy.z);
		a *= amp.x;
		b *= amp.z;

		Math::Vec3f of(a, 0, b);
		of = Math::Matrix4As3f(hair.basas[i])*of;

		float factor = (1-f)*amplen.x + f*amplen.z;
		of *= factor;
		of *= 1/hair.vwidht[i];
		of *= affect;
		hair.cvint[i] += of;
	}
}

void HairNoiseDeformer::init(		
	)
{
	noise.Init(seed);
}
// ������ �� UI
void HairNoiseDeformer::fromUI(
	artist::IShape* shape
	)
{
	shape->getValue(seed, "seed", sn::UNIFORM_SHAPE, 0, 0, 1000);
	shape->getValue(freq.x, "freq_x", sn::UNIFORM_SHAPE, 1, 0, 1);
	shape->getValue(freq.y, "freq_y", sn::UNIFORM_SHAPE, 1, 0, 1);
	shape->getValue(freq.z, "freq_z", sn::UNIFORM_SHAPE, 1, 0, 1);
	shape->getValue(amp.x, "amp_x", sn::UNIFORM_SHAPE, 0.1f, 0, 1);
	shape->getValue(amp.z, "amp_z", sn::UNIFORM_SHAPE, 0.1f, 0, 1);

	shape->getValue(amplen.x, "amp_base", sn::UNIFORM_SHAPE, 0, 0, 1);
	shape->getValue(amplen.z, "amp_tip", sn::UNIFORM_SHAPE, 1, 0, 1);
	shape->getValue(time, "time", sn::UNIFORM_SHAPE, 0, 0, 1);

	shape->getValue(timefreq.x, "timefreq_x", sn::UNIFORM_SHAPE, 1, 0, 1);
	shape->getValue(timefreq.y, "timefreq_y", sn::UNIFORM_SHAPE, 1, 0, 1);
	shape->getValue(timefreq.z, "timefreq_z", sn::UNIFORM_SHAPE, 1, 0, 1);

}

// ������������
void HairNoiseDeformer::serialise(
	Util::Stream& out
	)
{
	int version = 0;
	out >> version;
	if( version>=0)
	{
		out>>seed;
		out>>freq;
		out>>amp;
		out>>amplen;
		out>>time;
		out>>timefreq;
	}
}
