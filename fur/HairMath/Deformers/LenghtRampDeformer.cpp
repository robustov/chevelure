#include "StdAfx.h"
#include "HairMath.h"

#include "IHairDeformer.h"

// ��� ������ �������� ���� �� �������� �������� � ��������� 
// ������ ������ ������ �� ����� �� ����

class HairDeformer_LenghtRamp : public IHairDeformer
{
	bool bUseU;
	Math::Ramp lenghtRamp;

public:
	virtual int getType(
		)
	{
		return PARSEHAIR;
	}
	virtual void release()
	{
		delete this;
	};

public:
	// �������� ��������� ������
	virtual void ParseHair(
		float affect, 
		HAIRPARAMS& c_hairs,
		bool bClump
		);

public:
	// �������������
	virtual void init(
		);
	// ������ �� UI
	virtual void fromUI(
		artist::IShape* shape
		);
	// ������������
	virtual void serialise(
		Util::Stream& out
		);

public:
	HairDeformer_LenghtRamp(void);
	~HairDeformer_LenghtRamp(void);
};





extern "C"
{
	HAIRMATH_API IHairDeformer* getLenghtRampDeformer();
}

HAIRMATH_API IHairDeformer* getLenghtRampDeformer()
{
	return new HairDeformer_LenghtRamp();
}


HairDeformer_LenghtRamp::HairDeformer_LenghtRamp(void)
{
	bUseU = true;
}

HairDeformer_LenghtRamp::~HairDeformer_LenghtRamp(void)
{
}

// �������� ��������� ������
void HairDeformer_LenghtRamp::ParseHair(
	float affect, 
	HAIRPARAMS& c_hairs,
	bool bClump
	)
{
	float faffect = affect;
	faffect = __max(0, faffect);
	faffect = __min(1, faffect);

	float value = 1;
	if( bUseU)
	{
		value = lenghtRamp.getValue(c_hairs.u);
	}
	else
	{
		value = lenghtRamp.getValue(c_hairs.v);
	}
	c_hairs.length = faffect*value*c_hairs.length + (1-faffect)*c_hairs.length;
}


void HairDeformer_LenghtRamp::init(		
	)
{
}
// ������ �� UI
void HairDeformer_LenghtRamp::fromUI(
	artist::IShape* shape
	)
{
	shape->getValue(lenghtRamp, "lenghtClump", 0, 1, 0);
	shape->getValue(bUseU, "useU", sn::UNIFORM_SHAPE);

}

// ������������
void HairDeformer_LenghtRamp::serialise(
	Util::Stream& out
	)
{
	int version = 0;
	out >> version;
	if( version>=0)
	{
		out>>lenghtRamp;
		out>>bUseU;
	}
}
