#pragma once

#include "IHairDeformer.h"

class HairGrassDeformer : public IHairDeformer
{
	float affectLength;
	float affectTipCurl;
	float affectBaseCurl;
	float affectInclination;

public:
	virtual int getType(
		)
	{
		return PARSEHAIR;
	}
	virtual void release()
	{
		delete this;
	};

public:
	// ��������� ������
	virtual void ParseHair(
		float affect, 
		HAIRPARAMS& c_hairs,
		bool bClump
		);

public:
	// �������������
	virtual void init(
		);
	// ������ �� UI
	virtual void fromUI(
		artist::IShape* shape
		);
	// ������������
	virtual void serialise(
		Util::Stream& out
		);

public:
	HairGrassDeformer(void);
	~HairGrassDeformer(void);
};
