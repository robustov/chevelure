#include "stdafx.h"

#include "IHairCVmanipulator.h"

class HairCVManipulator_Transform : public IHairCVmanipulator
{
	Math::Matrix4f transform;
public:
	// ������ �����������
	virtual bool Start(
		cls::IRender* attributes
		);
	// ��� ����������� (���������� "cursorPos")
	virtual bool Step(
		cls::IRender* attributes
		);
	// ������ ����������� �� ��������� �����
	virtual void ComputeSingleHair(
		HairCV::CV& hair,				// �����
		std::vector<float> affects,		// ������� ����������� �� ������ ������� ������
		Math::Matrix4f& basis,			// ������� ��� ��������� �� TS -> OS
		Math::Matrix4f& basis_inv		// ������� ��� ��������� �� OS -> TS
		);
	// ��������� �����������
	virtual void Finish(
		);
};


extern "C"
{
	HAIRMATH_API IHairCVmanipulator* getTransformManipulator();
}

HairCVManipulator_Transform theHairCVManipulator_Transform;

HAIRMATH_API IHairCVmanipulator* getTransformManipulator()
{
	return &theHairCVManipulator_Transform;
}


// ������������� ������������
bool HairCVManipulator_Transform::Start(
	cls::IRender* attributes
	)
{
	bool res = attributes->GetAttribute("transform", transform);
	return res;
}
// ��� ����������� (���������� "cursorPos")
bool HairCVManipulator_Transform::Step(
	cls::IRender* attributes
	)
{
	return true;
}

// ������ ����������� �� ��������� �����
void HairCVManipulator_Transform::ComputeSingleHair(
	HairCV::CV& hc,					// �����
	std::vector<float> affects,		// ������� ����������� �� ������ ������� ������
	Math::Matrix4f& basis,			// ������� ��� ��������� �� TS -> OS
	Math::Matrix4f& basis_inv		// ������� ��� ��������� �� OS -> TS
	)
{
	bool bKeepLen = true;

	std::vector<float> lenghts(hc.ts.size()-1);
	for(int v=0; v<(int)hc.ts.size()-1; v++)
	{
		float len = (hc.ts[v+1]-hc.ts[v]).length();
		lenghts[v] = len;
	}

	if( hc.ts.empty())
		return;
	Math::Vec3f h0 = hc.ts[0];
	for(int v=0; v<(int)hc.ts.size(); v++)
	{
		float affect = 0;
		if(v<(int)affects.size())
			affect = affects[v];

		Math::Vec3f hvert = hc.ts[v];

		Math::Vec3f os = basis*hvert;
		os = transform*os;
		hvert = basis_inv*os;

		hc.ts[v] = hc.ts[v]*(1-affect) + hvert*affect;
	}

	// �����!!!!

	// ������� 0� ������� �����
	hc.ts[0] = h0;

	if( bKeepLen)
	{
		for(int v=1; v<(int)hc.ts.size(); v++)
		{
			Math::Vec3f dir = hc.ts[v-1]-hc.ts[v];
			float len = dir.length();
			float factor = (lenghts[v-1]-len)/len;
			dir *= factor;

			for(int dv=v; dv<(int)hc.ts.size(); dv++)
			{
				hc.ts[dv] -= dir;
			}
		}
	}
}

// ��������� �����������
void HairCVManipulator_Transform::Finish(
	)
{
}

