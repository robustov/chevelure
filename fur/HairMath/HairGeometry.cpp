#include "stdafx.h"
#include "HairGeometry.h"
#include "Math\MeshUtil.h"

HairGeometry::HairGeometry()
{
	bValid = true;
}

void HairGeometry::copy(const HairGeometry& arg)
{
	this->box		= arg.box;
	this->minUV		= arg.minUV; 
	this->maxUV		= arg.maxUV;
	this->vertexArray = arg.vertexArray;
	this->normals	= arg.normals;
	this->uvArray	= arg.uvArray;
	this->tangentUs = arg.tangentUs;
	this->tangentVs = arg.tangentVs;
	this->uvSets    = arg.uvSets;

	// 13.03.2008
	this->src_vertexArray	= arg.src_vertexArray;
	this->src_normals		= arg.src_normals;
	this->src_uvArray		= arg.src_uvArray;
	this->src_tangentUs		= arg.src_tangentUs;
	this->src_tangentVs		= arg.src_tangentVs;
}

//! findUvFace
//! ������ -1 ���� �-� �� ������
int HairGeometry::findUvFace(const Math::Vec2f& uv, const HairGeometryIndices* hgi, Math::Vec3f& bary)
{
	for(unsigned f=0; f<hgi->faceCount(); f++)
	{
		const Math::Face32& face_uv = hgi->faceUV(f);

		Math::Vec2f uvs[3];
		uvs[0] = this->uvArray[ face_uv.v[0] ];
		uvs[1] = this->uvArray[ face_uv.v[1] ];
		uvs[2] = this->uvArray[ face_uv.v[2] ];

		if( !Math::IsInFace(uvs, uv))
			continue;

		bary = Math::CalcBaryCoords(uvs, uv);
		return f;
	}
	return -1;
}

HairGeometry& HairGeometry::operator=(const HairGeometry& arg)
{
	HairGeometry::copy(arg);
	return *this;
}

void HairGeometry::serialize(Util::Stream& stream)
{
	int version = 4;
	stream >> version;
	if( version>=2)
	{
		stream >> box;
		stream >> minUV; 
		stream >> maxUV;

		stream >> vertexArray;
		stream >> normals;
		stream >> uvArray;
		stream >> tangentUs;
		stream >> tangentVs;
		if( version>=3)
		{
			stream >> uvSets;
		}
		// 13.03.2008
		if( version>=4)
		{
			stream >> src_vertexArray;
			stream >> src_normals;
			stream >> src_uvArray;
			stream >> src_tangentUs;
			stream >> src_tangentVs;
		}
		bValid = true;
	}
	else
		bValid = false;
}
void HairGeometry::serializeOcs(const char* _name, cls::IRender* render, bool bSave)
{
	std::string name = _name;

	SERIALIZEOCS(box);
	SERIALIZEOCS(minUV);
	SERIALIZEOCS(maxUV);

	SERIALIZEOCS(vertexArray);
	SERIALIZEOCS(normals);
	SERIALIZEOCS(uvArray);
	SERIALIZEOCS(tangentUs);
	SERIALIZEOCS(tangentVs);
//	SERIALIZEOCS(uvSets);
	if(bSave)
	{
		render->Parameter( (name+"::uvsetcount").c_str(), (int)uvSets.size());
		for(int i=0; i<(int)uvSets.size(); i++)
		{
			std::string attrname = name+"::uvset";
			char buf[24];
			attrname += _itoa(i, buf, 10);
			render->Parameter( (attrname).c_str(), uvSets[i]);
		}
	}
	else
	{
		int uvSetscount=0;
		render->GetParameter( (name+"::uvsetcount").c_str(), uvSetscount);
		uvSets.resize(uvSetscount);
		for(int i=0; i<uvSetscount; i++)
		{
			std::string attrname = name+"::uvset";
			char buf[24];
			attrname += _itoa(i, buf, 10);
			render->GetParameter( (attrname).c_str(), uvSets[i]);
		}
	}
	SERIALIZEOCS(src_vertexArray);
	SERIALIZEOCS(src_normals);
	SERIALIZEOCS(src_uvArray);
	SERIALIZEOCS(src_tangentUs);
	SERIALIZEOCS(src_tangentVs);
}


void HairGeometry::clear()
{
	minUV = Math::Vec2f();
	maxUV = Math::Vec2f();
	vertexArray.clear();
	normals.clear();
	uvArray.clear();
	tangentUs.clear();
	tangentVs.clear();
	uvSets.clear();
	// 13.03.2008
	src_vertexArray.clear();
	src_normals.clear();
	src_uvArray.clear();
	src_tangentUs.clear();
	src_tangentVs.clear();
}

void HairGeometry::calcBounds()
{
	Math::Box3f b;
	unsigned v;
	for(v=0; v<vertexArray.size(); v++)
	{
		b.insert(vertexArray[v]);
	}
	box = b;

	for(v=0; v<uvArray.size(); v++)
	{
		Math::Vec2f uv = uvArray[v];
		if(v==0)
			minUV = maxUV = uv; 

		minUV.x = __min(minUV.x, uv.x);
		minUV.y = __min(minUV.y, uv.y);
		maxUV.x = __max(maxUV.x, uv.x);
		maxUV.y = __max(maxUV.y, uv.y);
	}
}

// Tangent space
void HairGeometry::calcTangentSpace(HairGeometryIndices* geometryIndices)
{
	int vc  = geometryIndices->vertexCount();
	int nuv = geometryIndices->uvCount();  
//	int nuv = geometryIndices->normalCount();  

	tangentUs.resize(nuv, Math::Vec3f(0.000001f, 0, 0));
	tangentVs.resize(nuv, Math::Vec3f(0, 0.000001f, 0));
	
	std::vector<int> counts(nuv, 0);
	for( unsigned i=0; i<geometryIndices->faceCount(); i++)
	{
		Math::Face32& face = geometryIndices->face(i);
		Math::Face32& face_uv = geometryIndices->faceUV(i);
//		Math::Face32& face_n = geometryIndices->faceN(i);

		Math::Vec3f V[3];
		V[0] = vertex(face.v[0]);
		V[1] = vertex(face.v[1]);
		V[2] = vertex(face.v[2]);

		Math::Vec2f uvs[3];
		uvs[0] = uv( face_uv.v[0] );
		uvs[1] = uv( face_uv.v[1] );
		uvs[2] = uv( face_uv.v[2] );

		Math::Vec3f tangentU, tangentV;
		Math::CalcTangentSpace(V, uvs, tangentU, tangentV);

		for(int g=0; g<3; g++)
		{
			int v = face_uv.v[g];
			this->tangentU(v) += tangentU;
			this->tangentV(v) += tangentV;
//			this->tangentU(face_n.v[g]) += tangentU;
//			this->tangentV(face_n.v[g]) += tangentV;
			counts[v] += 1;
		}
	}
	for(int v=0; v<nuv; v++)
	{
		float p1 = 1.f/counts[v];
		this->tangentU(v) = this->tangentU(v)*p1;
		this->tangentV(v) = this->tangentV(v)*p1;
	}

	///////////////////////////////////
	// SRC �������:
	if( geometryIndices->isSubdived())
	{
		int vc  = geometryIndices->src_vertexCount();
		int nuv = geometryIndices->src_uvCount();  

		this->src_tangentUs.resize(nuv, Math::Vec3f(0.000001f, 0, 0));
		this->src_tangentVs.resize(nuv, Math::Vec3f(0, 0.000001f, 0));
		
		std::vector<int> counts(nuv, 0);
		for( int i=0; i<geometryIndices->src_faceCount(); i++)
		{
			const Math::Face32& face = geometryIndices->src_face(i);
			const Math::Face32& face_uv = geometryIndices->src_faceUV(i);

			Math::Vec3f V[3];
			V[0] = src_vert(face.v[0]);
			V[1] = src_vert(face.v[1]);
			V[2] = src_vert(face.v[2]);

			Math::Vec2f uvs[3];
			uvs[0] = src_uv( face_uv.v[0] );
			uvs[1] = src_uv( face_uv.v[1] );
			uvs[2] = src_uv( face_uv.v[2] );

			Math::Vec3f tangentU, tangentV;
			Math::CalcTangentSpace(V, uvs, tangentU, tangentV);

			for(int g=0; g<3; g++)
			{
				int v = face_uv.v[g];
				this->src_tangentUs[v] += tangentU;
				this->src_tangentVs[v] += tangentV;
				counts[v] += 1;
			}
		}
		for(int v=0; v<nuv; v++)
		{
			float p1 = 1.f/counts[v];
			this->src_tangentUs[v] = this->src_tangentUs[v]*p1;
			this->src_tangentVs[v] = this->src_tangentVs[v]*p1;
		}
	}
}

//! ��������� subdivide ����� � ������ � �������� vertexArray, normals, uvArray
void HairGeometry::subdivide(HairGeometryIndices* geometryIndices)
{
	if( geometryIndices->isSubdived())
	{
		this->src_vertexArray = this->vertexArray;
		this->src_uvArray = this->uvArray;
		this->src_normals = this->normals;
	}

	geometryIndices->subdivide_faces(this->vertexArray, 0);
	geometryIndices->subdivide_faces(this->uvArray, 1);
	geometryIndices->subdivide_faces(this->normals, 2);

	for(unsigned i=0; i<uvSets.size(); i++)
	{
		geometryIndices->subdivide_uvSetfaces(i, uvSets[i]);
	}
}
