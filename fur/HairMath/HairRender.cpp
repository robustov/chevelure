#include "stdafx.h"
#include "HairMath.h"
#include "HairRender.h"
#include "Util/FileStream.h"
#include "HairPersonage.h"
#include "Procedurals/HairProceduralCurve.h"
#include "Procedurals/HairProceduralSn.h"
#include "Procedurals/HairProceduralTubes.h"
#include "Procedurals/HairProceduralCurveOcs.h"
#include "Util/currentHostName.h"
#include ".\hairlicensethread.h"
#include <time.h>

#ifndef SAS
#include "ulLicense/UlLClientThread.h"
extern UlLClientThread clientThread;
#endif


// ��� ���������� ������ (��� ���������������) (������ ��������)
std::map<std::string, HairRender::StaticData> staticDataCache;
// ��� ���������� ������ (��� ���������������) (������ ��������)
std::map<std::string, HairRender::Dynamic> dynamicDataCache;

bool HairRender::StaticData::Load(const char* filename, bool bDump)
{
	// Static data file
	Util::FileStream file;
	if( !file.open( filename, false))
	{
		std::string host = Util::currentHostName();
		fprintf(stderr, "HairMath: cant open %s for read. Host=%s\n", filename, host.c_str());
		fflush(stderr);
		return false;
	}
	int _timestamp = (int)timestamp;
	file >> _timestamp;
	timestamp = _timestamp;
	file >> fullobjname;
	geometryIndices.serialize(file);
	cvPositions.serialize(file);
	clumpPositions.serialize(file);
	rootPositions.serialize(file);

	rootPositions.buildPolygonGroups(hairPolygonGroups, geometryIndices);
	clumpPositions.buildPolygonGroups(clumpPolygonGroups, geometryIndices);
	isValid = true;
	if( bDump)
	{
		printf("Hair: Load file %s\n", filename), fflush(stdout);
	}
	return true;
}

bool HairRender::Dynamic::Load( Util::Stream& file)
{
	surface.serialize(file);
	geometry.serialize(file);
	cv.serialize(file);
	isValid = true;
	return true;
}
bool HairRender::Dynamic::Init( HairRender* hairRender)
{
	surface.init(
		&hairRender->staticData->geometryIndices, &geometry, 
		&hairRender->staticData->clumpPositions, 
		&hairRender->staticData->rootPositions, 
		&hairRender->staticData->cvPositions, 
		&cv, &hairRender->hairSystem);
	surface.initParamsUVsetIndices();
	return true;
}
 




HairRender* HairRender::creator()
{
	return new HairRender;
}
HairRender::HairRender()
//	#ifdef _DEBUG
//		prmanproc("IPrmanDSOD.dll", "getIPrman")
//	#else
//		prmanproc("IPrmanDSO.dll", "getIPrman")
//	#endif
{
	InitializeCriticalSection(&this->mutex);

	prman = NULL;
//	if( prmanproc.isValid())
//		prman = (*prmanproc)();

#ifdef SAS
#ifndef CHEVELUREDEMO
	HairLicenseStart();
#endif
#endif

// ��������� ��������
#ifndef SAS
	clientThread.WaitForStart(UlHair_licflag!=0x1);
#endif

	render = 0;
	nGroups = nHairs = 0;
	refCount = 1;
	bDeleteStaticData = true;
	staticData = NULL;
	bDeleteDeformblur = true;
	deformfreeze = NULL;
}
HairRender::~HairRender()
{
	if(bDeleteStaticData && staticData)
		delete staticData;

	if( bDeleteDeformblur)
	{
		std::map<float, HairRender::Dynamic*>::iterator it = deformblur.begin();
		for(;it != deformblur.end(); it++)
		{
			if( it->second)
				delete it->second;
			it->second = NULL;
		}
		delete deformfreeze;
	}

	std::string host = Util::currentHostName();
//	fprintf(stderr, "HairMath: Hair render finished (%d groups, %d hairs). Host=%s, %s\n", nGroups, nHairs, host.c_str(), hairRootPositionsFileName.c_str());
//fprintf(stderr, "Hair(%d) ", nHairs);

	if( hairSystem.dumpInformation)
		fprintf(stderr, "Hair(%d) ", nHairs), fflush(stderr);

#ifdef SAS
#ifndef CHEVELUREDEMO
	HairLicenseStop();
#endif
#endif

}


void HairRender::addRef()
{
	EnterCriticalSection(&this->mutex);
	++refCount;
	LeaveCriticalSection(&this->mutex);
}
void HairRender::release()
{
	EnterCriticalSection(&this->mutex);
	try
	{
		--refCount;
		if( refCount<=0)
		{
			if( hairSystem.dumpInformation)
				fprintf(stderr, "Fur: release HairRender %s!!!\n", hairRootPositionsFileName.c_str());
//			fprintf(stderr, "HairRender: rendered %d groups from XX\n", nGroups);//, blurless.surface.getGroupCount());
			fflush(stderr);
			delete this;
		}
	}
	catch(...)
	{
		fprintf(stderr, "HairMath: HairRender::release exception\n");
		fflush(stderr);
	}
	LeaveCriticalSection(&this->mutex);
}

bool HairRender::Load2(std::vector<std::string> files, std::vector<float>& motion)
{
	std::string hairRootPositionsFileName;
	int currenttimestamp = 0;

	for(int f=0; f<files.size(); f++)
	{
		auto& filename = files[f];
		float time = f<motion.size()?motion[f]:0;

		Util::FileStream file;
		if( !file.open( filename.c_str(), false))
		{
			std::string host = Util::currentHostName();
			fprintf(stderr, "HairMath: cant open \"%s\" for read. Host=%s\n", filename.c_str(), host.c_str());
			fflush(stderr);
			return false;
		}

		std::string _hairRootPositionsFileName;
		file >> _hairRootPositionsFileName;
		int _currenttimestamp;
		file >> _currenttimestamp;
		hairSystem.serialize(file);

		// check
		if( hairRootPositionsFileName.empty())
			hairRootPositionsFileName = _hairRootPositionsFileName;
		if( currenttimestamp==0)
			currenttimestamp = _currenttimestamp;

		if( hairRootPositionsFileName!=_hairRootPositionsFileName ||
			currenttimestamp != _currenttimestamp)
		{
			std::string host = Util::currentHostName();
			fprintf(stderr, "HairMath: render \"%s\"\nDifferent RootPositionsFile \"%s\". Host=%s", 
				filename, 
				hairRootPositionsFileName.c_str(), 
				host.c_str()
				);
			fflush(stderr);
			return false;
		}

		deformblur[time] = new Dynamic();
		Dynamic& dynamic = *deformblur[time];
		dynamic.Load(file);
	}

	if( !LoadRootPositionsFile(hairRootPositionsFileName.c_str(), currenttimestamp, false))
	{
		std::string host = Util::currentHostName();
		fprintf(stderr, "HairMath: \ncant load RootPositionsFile \"%s\". Host=%s", 
			hairRootPositionsFileName.c_str(), 
			host.c_str()
			);
		fflush(stderr);
		return false;
	}

	for(auto it=deformblur.begin(); it!=deformblur.end(); it++)
	{
		auto& dynamic = *it->second;
		dynamic.Init(this);
		dynamic.surface.InitDeformers();
	}
	return true;
}

bool HairRender::Load(const char* filename)
{
	time_t currenttimestamp = 0;
	// Dynamic data file
	Util::FileStream file;
	if( !file.open( filename, false))
	{
		std::string host = Util::currentHostName();
		fprintf(stderr, "HairMath: cant open %s for read. Host=%s\n", filename, host.c_str());
		fflush(stderr);
		return false;
	}

	file >> hairRootPositionsFileName;
	int _currenttimestamp = (int)currenttimestamp;
	file >> _currenttimestamp;
	currenttimestamp = _currenttimestamp;
	hairSystem.serialize(file);
	unsigned int blurcount;
	file >> blurcount;

	if( !LoadRootPositionsFile(hairRootPositionsFileName.c_str(), currenttimestamp, false))
	{
		std::string host = Util::currentHostName();
		fprintf(stderr, "HairMath: render %s\ncant load RootPositionsFile %s. Host=%s", 
			filename, 
			hairRootPositionsFileName.c_str(), 
			host.c_str()
			);
		fflush(stderr);
		return false;
	}

	for(int i=0; i<(int)blurcount; i++)
	{
		float time;
		file >> time;
		deformblur[time] = new Dynamic();
		Dynamic& dynamic = *deformblur[time];
		dynamic.Load(file);
		dynamic.Init(this);
		dynamic.surface.InitDeformers();
//		blurless.Load(this, file);
	}

	if( !hairSystem.freezefile.empty())
	{
		Util::FileStream freezefile;
		if( !freezefile.open( hairSystem.freezefile.c_str(), false))
		{
			std::string host = Util::currentHostName();
			fprintf(stderr, "HairMath: cant open FREEZE file %s for read. Host=%s\n", hairSystem.freezefile.c_str(), host.c_str());
			fflush(stderr);
			return false;
		}

		deformfreeze = new Dynamic();
		deformfreeze->Load(freezefile);
		deformfreeze->Init(this);
	}

	return true;
}
//! ��������� Root Positions File
bool HairRender::LoadRootPositionsFile(
	const char* hairRootPositionsFileName, 
	time_t currenttimestamp, 
	bool bUseCache, 
	bool bAllowNoFatalError
	)
{
	if( bUseCache)
	{
		bDeleteStaticData = false;

		std::map<std::string, HairRender::StaticData>::iterator it = 
			staticDataCache.find(hairRootPositionsFileName);
		StaticData* sd = NULL;
		if(it!=staticDataCache.end())
		{
			sd = &it->second;
			if( sd && !sd->isValid)
				sd = NULL;
			if( sd->timestamp != currenttimestamp)
				sd = NULL;
			if(!sd)
				staticDataCache.erase(it);
		}
		if(!sd)
		{
			sd = &staticDataCache[hairRootPositionsFileName];
			sd->Load(hairRootPositionsFileName, hairSystem.dumpInformation);
		}
		if( !sd->isValid)
			return false;
		staticData = sd;
		bDeleteStaticData = false;
	}
	else
	{
		staticData = new StaticData();
		if( !staticData->Load(hairRootPositionsFileName, hairSystem.dumpInformation))
			return false;
		bDeleteStaticData = true;
	}
	if(staticData->timestamp != currenttimestamp)
	{
		std::string host = Util::currentHostName();
		fprintf(stderr, "HairMath: invalid timestamp!!!\ntimestamp:%d needtimestamp=%d. Host=%s\n", 
			(int)staticData->timestamp, (int)currenttimestamp, host.c_str());
		fflush(stderr);
		if( !bAllowNoFatalError)
			return false;
		// ����� ���� �� ��������� ������
//		return false;
	}
	return true;
}
bool HairRender::LoadDeformBlurFromOcs(
	cls::IRender* render,
	float motionBlurTime
	)
{
	std::string framefile;
	if( !render->GetParameter("Hair::filename", framefile)) return false;

	int currenttimestamp;
	std::string hairsystemfilename;
	if( !render->GetParameter("@Hair::positionfilename", hairRootPositionsFileName)) return false;
	if( !render->GetParameter("@Hair::timestamp", currenttimestamp)) return false;
	if( !render->GetParameter("Hair::systemfilename", hairsystemfilename)) return false;

	// hairSystem
	{
		Util::FileStream file;
		if( !file.open( hairsystemfilename.c_str(), false))
		{
			std::string host = Util::currentHostName();
			fprintf(stderr, "HairMath: cant open %s for read. Host=%s\n", hairsystemfilename.c_str(), host.c_str());
			fflush(stderr);
			return false;
		}
		hairSystem.serialize(file);
	}
	if(!staticData)
	{
		if( !LoadRootPositionsFile(hairRootPositionsFileName.c_str(), currenttimestamp, true))
			return false;
	}

	// ������� �����
	bool bLoadNew = false;
	if(bLoadNew)
	{
		const char* filename = framefile.c_str();
		Util::FileStream file;
		if( !file.open( filename, false))
		{
			std::string host = Util::currentHostName();
			fprintf(stderr, "HairMath: cant open %s for read. Host=%s\n", filename, host.c_str());
			fflush(stderr);
			return false;
		}
		bDeleteDeformblur = true;
		Dynamic* dynamic = new Dynamic();
		if( !dynamic->Load(file))
			return false;
		if( !dynamic->isValid)
			return false;
		deformblur[motionBlurTime] = dynamic;
	}
	else
	// ���������� ������
	{
		const char* filename = framefile.c_str();
		std::map<std::string, HairRender::Dynamic>::iterator itf = dynamicDataCache.find(filename);
		if( itf != dynamicDataCache.end())
		{
			if( !itf->second.isValid)
				return false;
			Dynamic* dynamic = new Dynamic();
			*dynamic = itf->second;
			dynamic->surface.InitDeformers();
			deformblur[motionBlurTime] = dynamic;
		}
		else
		{
			// Dynamic data file
			Util::FileStream file;
			if( !file.open( filename, false))
			{
				std::string host = Util::currentHostName();
				fprintf(stderr, "HairMath: cant open %s for read. Host=%s\n", filename, host.c_str());
				fflush(stderr);
				return false;
			}
			Dynamic& dynamic = dynamicDataCache[filename];
			if( !dynamic.Load(file))
				return false;
			if( !dynamic.isValid)
				return false;
			Dynamic* localdynamic = new Dynamic();
			*localdynamic = dynamic;
			localdynamic->surface.InitDeformers();
			deformblur[motionBlurTime] = localdynamic;
		}
	}

	Dynamic& dynamic = *deformblur[motionBlurTime];
	dynamic.Init(this);

	// ������������ �����
	float lengthfactor = 1;
	if( render->GetParameter("Hair::lengthfactor", lengthfactor))
	{
		srand(*(int*)&lengthfactor);
		int seedbias = rand();
		dynamic.surface.Randomize( lengthfactor, seedbias);
	}

	bool updateGeometry = true;
	if(updateGeometry)
	{
	//	HairGeometry geometry;
		HairGeometry& geometry = dynamic.geometry;
		cls::PA< Math::Vec3f> geometry_vertex, geometry_normal;
		cls::PA< float> geometry_s, geometry_t;

		if(!render->GetParameter("P", geometry_vertex)) return false;
		if(!render->GetParameter("N", geometry_normal))
			if(!render->GetParameter("#N", geometry_normal)) return false;
		if(!render->GetParameter("s", geometry_s)) return false;
		if(!render->GetParameter("t", geometry_t)) return false;
		if( geometry_s.size()!=geometry_t.size()) return false;

		bool NormalsIsFacevertex = false;
		render->GetParameter("Hair::NormalsIsFacevertex", NormalsIsFacevertex);
		if(NormalsIsFacevertex)
		{
			cls::PA< int> normalindexByfaceindex;
			if(!render->GetParameter("Hair::normalindexByfaceindex", normalindexByfaceindex)) return false;

			cls::PA< Math::Vec3f> true_geometry_normal( cls::PI_PRIMITIVEVERTEX, normalindexByfaceindex.size(), cls::PT_NORMAL);
			for(int i=0; i<true_geometry_normal.size(); i++)
			{
				int index = normalindexByfaceindex[i];
				true_geometry_normal[i] = geometry_normal[index];
			}
			geometry_normal = true_geometry_normal;
		}
//printf("%f: {%f, %f, %f}\n", motionBlurTime, 
//	   geometry_vertex[0].x, geometry_vertex[0].y, geometry_vertex[0].z);
//printf("%f: dynamic=%x, param=%x, {%f, %f, %f}\n", 
//motionBlurTime, &dynamic, geometry_vertex->data, 
//geometry_vertex[0].x, geometry_vertex[0].y, geometry_vertex[0].z);

		geometry_vertex.data( geometry.vertexArray);
		geometry_normal.data( geometry.normals);
		geometry.uvArray.resize(geometry_s.size());
		for(int i=0; i<geometry_s.size(); i++)
		{
			geometry.uvArray[i].x = geometry_s[i];
			geometry.uvArray[i].y = geometry_t[i];
		}
		geometry.uvSets.resize(staticData->geometryIndices.uvSetCount());
		for( int x=0; x<(int)staticData->geometryIndices.uvSetCount(); x++)
		{
			std::string pname;
			cls::PA< float> geometry_s, geometry_t;
			pname = staticData->geometryIndices.uvSetName(x);pname+="_s";
			if(!render->GetParameter(pname.c_str(), geometry_s)) return false;
			pname = staticData->geometryIndices.uvSetName(x);pname+="_t";
			if(!render->GetParameter(pname.c_str(), geometry_t)) return false;

			geometry.uvSets[x].resize(geometry_s.size());
			for(int i=0; i<geometry_s.size(); i++)
			{
				geometry.uvSets[x][i].x = geometry_s[i];
				geometry.uvSets[x][i].y = geometry_t[i];
			}
		}

		// ���������
		geometry.subdivide(&staticData->geometryIndices);
		geometry.calcTangentSpace(&staticData->geometryIndices);
		geometry.calcBounds();

	/*/
	{
		// ��������
		Dynamic& dynamic = deformblur[motionBlurTime];
		for(int i=0; i<(int)geometry.vertexArray.size(); i++)
		{
			Math::Vec3f v0 = geometry.vertexArray[i];
			Math::Vec3f v1 = dynamic.geometry.vertexArray[i];
			if((v0-v1).length()>0.0001)
				printf(".");
		}
		for(int i=0; i<(int)geometry.normals.size(); i++)
		{
			Math::Vec3f v0 = geometry.normals[i];
			Math::Vec3f v1 = dynamic.geometry.normals[i];
			if((v0-v1).length()>0.0001)
				printf(".");
		}
		for(int i=0; i<(int)geometry.tangentUs.size(); i++)
		{
			Math::Vec3f v0 = geometry.tangentUs[i];
			Math::Vec3f v1 = dynamic.geometry.tangentUs[i];
			if((v0-v1).length()>0.0001)
				printf(".");
		}
		for(int i=0; i<(int)geometry.tangentVs.size(); i++)
		{
			Math::Vec3f v0 = geometry.tangentVs[i];
			Math::Vec3f v1 = dynamic.geometry.tangentVs[i];
			if((v0-v1).length()>0.0001)
				printf(".");
		}
		printf(".");
	}
	/*/
	}
	return true;
}

/*/
bool HairRender::LoadOcs(
	std::vector< std::string>& framefiles,
	float* motionBlurTimes
	)
{
	for(int f=0; f<(int)framefiles.size(); f++)
	{
		const char* filename = framefiles[f].c_str();
		time_t currenttimestamp;
		// Dynamic data file
		Util::FileStream file;
		if( !file.open( filename, false))
		{
			std::string host = Util::currentHostName();
			fprintf(stderr, "HairMath: cant open %s for read. Host=%s\n", filename, host.c_str());
			fflush(stderr);
			return false;
		}

		file >> hairRootPositionsFileName;
		file >> currenttimestamp;
		hairSystem.serialize(file);
		
		if(f==0)
		{
			if( !LoadRootPositionsFile(hairRootPositionsFileName.c_str(), currenttimestamp, true))
				return false;
		}

		float time = 0;
		if( motionBlurTimes)
			time = motionBlurTimes[f];
		deformblur[time].Load(this, file);
	}

	return true;
}
/*/


bool HairRender::GenRIB(IPrman* prman) 
{
	if(!prman)
		return false;
	LoadLibrary("ocellarisProc.dll");

//char buf[64];
//sprintf(buf, "gr_sdadasdaas");
//char* sss = buf;
//prman->RiAttribute( "identifier", "string name", &sss, NULL);

	EnterCriticalSection(&this->mutex);

	this->render = &renderimpl;
	renderimpl.setBlurParams(0, 0, 1);
	this->render->SetTransform(hairSystem.this_worldpos);
	this->render->Attribute( "@camera::worldpos", hairSystem.camera_worldpos);
	this->render->Attribute( "@pass", hairSystem.passforocs);

	// ������
	this->prman = prman;
	renderimpl.setPrman(this->prman);

	bool checkWorkingFaces = !hairSystem.workingfaces.empty();

	bool bNonGroupedRender = false;
	if( checkWorkingFaces) 
		bNonGroupedRender = false;

	if( !hairSystem.splitGroups)
		bNonGroupedRender = true;

	// ������ � ����������
	std::string currentpassname=hairSystem.currentpassname, currentpassid;
	RtString str=NULL;
	RxInfoType_t restype;
	int rescount;
	prman->RxOption("user:pass_class", &str, sizeof(str), &restype, &rescount);
	if( rescount && str)
		currentpassname = str;
	else
		fprintf(stderr, "Option user:pass_class not found. Let's it be \"final\"\n");
	_strlwr( (char*)currentpassname.c_str());

	str=NULL;
	prman->RxOption("user:pass_id", &str, sizeof(str), &restype, &rescount);
	if( rescount && str)
		currentpassid = str;
	_strlwr( (char*)currentpassid.c_str());

	if(hairSystem.dumpInformation)
	{
		fprintf(stderr, "pass_class=%s pass_id=%s\n", currentpassname.c_str(), currentpassid.c_str());
		fflush(stderr);
	}

	bool bInstanced = 
		(hairSystem.snshapes.size()!=0) || 
		(hairSystem.ocsfiles.size()!=0);

	if(hairSystem.dumpInformation)
	{
//		fprintf(stderr, "bInstanced=%d\n", bInstanced?1:0);
//		fflush(stderr);
	}

	///////////////////////////////////////
	// ��������� ��� �����
	std::vector<HairRenderPass*> aloned_passes, grouped_passes;
	aloned_passes.reserve(hairSystem.passes.size());
	grouped_passes.reserve(hairSystem.passes.size());
	for(int x=0; x<(int)hairSystem.passes.size(); x++)
	{
		HairRenderPass& pass = hairSystem.passes[x];

		// ��������� ����� �� ��������� ���� ������
		// passes
		bool bValid = false;
		for( int pp=0; pp<(int)pass.passes.size(); pp++)
			if( strcmpi(currentpassname.c_str(), pass.passes[pp].c_str())==0)
				bValid = true;
		if( !bValid)
			continue;		// �� ���������

		// passIds
		if(!pass.passIds.empty())
		{
			bValid = false;
			for( int pp=0; pp<(int)pass.passIds.size(); pp++)
				if( strcmpi(currentpassname.c_str(), pass.passIds[pp].c_str())==0)
					bValid = true;
			if( !bValid)
				continue;		// �� ���������
		}

		if(hairSystem.dumpInformation)
		{
			fprintf(stderr, "%s is valid\n", pass.passName.c_str());
			fflush(stderr);
		}

		if(currentpassname == "shadow")
			pass.diceHair = false;

		if( pass.bPrune)
		{
			pass.prune = CalcPruning(pass, true);
		}

		if( pass.renderNormalSource == RNS_CLUMPVECTOR && 
			staticData->clumpPositions.getCount()==0)
		{
			fprintf(stderr, "HairMath: Hair pass %s: curve normal set to \"clumpVector\" but clumps is OFF!\n", pass.passName.c_str());
			fflush(stderr);
			pass.renderNormalSource = RNS_NONE;
			continue;
		}

		// RiDetailRange
		if( pass.bUseDetails)
		{
			grouped_passes.push_back(&pass);
			continue;
		}

		if( bInstanced && (pass.source == HairRenderPass::RT_DEFAULT || pass.source == HairRenderPass::RT_INSTANCES) )
		{
			grouped_passes.push_back(&pass);
			continue;
		}

		int count = pass.CalcCount(staticData->rootPositions.getCount(), staticData->clumpPositions.getCount());

		// ���� ������ 10 ����� � ������ - ��������� �������
		if( count < this->getPolygonGroupCount()*10)
		{
			aloned_passes.push_back(&pass);
			continue;
		}

		if( bNonGroupedRender)
		{
			aloned_passes.push_back(&pass);
		}
		else
		{
			grouped_passes.push_back(&pass);
		}
	}

	///////////////////////////////////////
	// Box ��� ����������
	Math::Box3f snbox(0);
	{
		std::map< int, sn::ShapeShortcutPtr >::iterator it = hairSystem.snshapes.begin();
		for( ; it != hairSystem.snshapes.end(); it++)
		{
			Math::Box3f _snbox;
			sn::ShapeShortcutPtr& snInst = it->second;
			snInst->shaper->getDirtyBox(snInst->shape, Math::Matrix4f::id, _snbox);
			snbox.insert(_snbox);
		}
	}

	///////////////////////////////////////
	// ��������� ��� �������
	if( !aloned_passes.empty())
	{
		this->GenGroupRIB(NULL, -1, snbox, aloned_passes);
	}

	///////////////////////////////////////
	// ��������� ������
	if( !grouped_passes.empty())
	{
		int gc = this->getPolygonGroupCount();
		for(int g=0; g<gc; g++)
		{
			//////////////////////////////////////////////
			// render
			if( checkWorkingFaces)
				if( hairSystem.workingfaces.find(g)==hairSystem.workingfaces.end())
					continue;

			this->GenGroupRIB(NULL, g, snbox, grouped_passes);
		}
	}

	if( hairSystem.dumpInformation)
		fprintf(stderr, "\nend gen\n"), fflush(stderr);

	LeaveCriticalSection(&this->mutex);

	return true;
}

//! GenPointClouds
bool HairRender::GenPointClouds(
	IPointCloudFile* pointcloudfile
	)
{
	bool bClump = 0;
	HAIRVERTS hair;
	HAIRPARAMS params;

	for(int x=0; x<(int)hairSystem.passes.size(); x++)
	{
		HairRenderPass& pass = hairSystem.passes[x];

		// ������
		std::map<float, HairRender::Dynamic*>::iterator it = this->deformblur.begin();
		for(int p=0; it != this->deformblur.end(); it++, p++)
		{
			HairRender::Dynamic& dynamicRecord = *it->second;

			HairMesh* surface = &dynamicRecord.surface;
			HairProcessor processor(pass.SegmCount, *surface, 
				hairSystem.snshapes, 
				hairSystem.ocsfiles, 
				hairSystem.snshapes_weight, 

				hairSystem.mulWidthByLength, hairSystem.orthogonalize, false, bClump);

			int count = surface->rootPositions->getCount();
			for(int i=0; i<count; i++)
			{
				processor.BuildHair(i, params, hair);

				float W = hairSystem.passes[0].RenderWidth;

				float L = hair.getLength();
				for(float l=0; l<L; l+=W)
				{
					float f = l/L;
					float w=1;
					Math::Vec3f pt = hair.getPointByParam(f, &w);
					pointcloudfile->setPoint(pt, Math::Vec3f(0, 0, 0), w*W);
				}
			}
		}
	}

	return true;
}

float HairRender::CalcPruning(HairRenderPass& pass, bool bDump)
{
	Dynamic* dynamic = deformblur.begin()->second;

	Math::Matrix4f camera_worldpos = hairSystem.camera_worldpos;
	Math::Matrix4f this_worldpos = hairSystem.this_worldpos;

	float pruningrate = pass.pruningrate;

	float prunedistmin = pass.prunedistmin; 
	float prunemin_value=pass.prunemin_value;
	float prunemin_width=pass.prunemin_width;

	
	{
		render->GetAttribute("@fur::pruningRate", pruningrate);
		render->GetAttribute("@fur::pruningScale", prunedistmin);
		render->GetAttribute("@fur::pruningMin", prunemin_value);
		render->GetAttribute("@fur::pruningWidth", pass.degradationWidth);
	}
	prunedistmin *= hairSystem.pruningScaleFactor;

	Math::Vec3f campos;
	bool bUseObjectSpace = true;
	float z = 0;
	if( bUseObjectSpace)
	{
		campos = camera_worldpos*Math::Vec3f(0);
		this_worldpos.invert();
		campos = this_worldpos*campos;
		z = campos.length();
		if( bDump)
			printf("campos_inobjectspace={%f, %f, %f} scale factor = %f\n", 
				campos.x, campos.y, campos.z, hairSystem.pruningScaleFactor);
	}
	else
	{
		campos = camera_worldpos*Math::Vec3f(0);
		Math::Vec3f thispos = this_worldpos*Math::Vec3f(0);
		z = (thispos-campos).length();
		if( bDump)
			printf("campos={%f, %f, %f} thispos={%f, %f, %f}\n", 
				campos.x, campos.y, campos.z, thispos.x, thispos.y, thispos.z);
	}

	// ����� ����������� ��������� (�����������)
	if( dynamic)
	{
		z = (float)1e30;
		int vc = staticData->geometryIndices.vertexCount();
		for( int v=0; v<vc; v++)
		{
			Math::Vec3f vert = dynamic->geometry.vertex(v);
			if( bUseObjectSpace)
			{
				float _z = (vert-campos).length2();
				z = __min(z, _z);
			}
			else
			{
				Math::Vec3f thispos = this_worldpos*Math::Vec3f(vert);
				float _z = (thispos-campos).length2();
				z = __min(z, _z);
			}
		}
		z = sqrt(z);
	}

	// �������� ���������
	if( pass.pruneTestDistance > 0.00001)
	{
		z = pass.pruneTestDistance;
	}

	// DEBUG
	z = z/prunedistmin;

	float p = log(2.f)/log(pruningrate);
	float u = (z<=1) ? 1 : pow( z, -p);
	

//		z = (z-prunedistmin)/(prunedistmax-prunedistmin);
	float prune = u;
	if(prune<prunemin_value) prune = prunemin_value;

	pass.degradation *= prune;

	pass.RenderWidth = pass.RenderWidth*(1/prune);
	// ��� ����
//	pass.RenderWidth = 
//		pass.RenderWidth*u + 
//		(prunemin_width)*(1-u);

	if( bDump)
		printf("prune %f width = %f dist=%f{%f}\n", prune, pass.RenderWidth, z*prunedistmin, z);
	return prune;
}

//! per hair seed
int HairRender::getSeed(int index, bool bClump)
{
	if( bClump) 
		return staticData->clumpPositions.getSeed(index);
	return staticData->rootPositions.getSeed(index);
}

void HairRender::getPolygonGroup(int group, bool bClump, int& startindex, int& endindex)
{
	if(!bClump)
	{
		startindex = 0;
		endindex = staticData->rootPositions.getCount();

		if( group<0 || group>=(int)staticData->hairPolygonGroups.size()) 
			return;

		startindex = staticData->hairPolygonGroups[group].startindex;
		endindex   = staticData->hairPolygonGroups[group].endindex;
	}
	else
	{
		startindex = 0;
		endindex = staticData->clumpPositions.getCount();

		if( group<0 || group>=(int)staticData->clumpPolygonGroups.size()) 
			return;

		startindex = staticData->clumpPolygonGroups[group].startindex;
		endindex   = staticData->clumpPolygonGroups[group].endindex;
	}
}



