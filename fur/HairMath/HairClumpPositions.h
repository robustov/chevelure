#pragma once

#include <Math/Vector.h>
#include <Math/Matrix.h>
#include <Math/Rotation3.h>
#include <Math/Face.h>
#include "Math\MeshUtil.h"
#include <Util/Stream.h>
#include <Util\STLStream.h>
#include <vector>

#include "HairMath.h"
#include "HairGeometryIndices.h"
#include "HairGeometry.h"

#pragma warning( disable : 4251)

//! Clumps
//!			����������� ����������!!!
class HAIRMATH_API HairClumpPositions
{
public:
	//! copy
	void copy(const HairClumpPositions& arg);
	//! operator=
	HairClumpPositions& operator=(const HairClumpPositions& arg);
public:
	//! getCount
	int getCount();
	//! ����� �� �����������
	const HairGeometryIndices::pointonsurface& getPointOnSurface( int index) const;

	//! getUV
	Math::Vec2f getUV(int index, HairGeometryIndices* geometryIndices, HairGeometry* geometry);
	//! getUVset
	Math::Vec2f getUVset(int index, int uvset, HairGeometryIndices* geometryIndices, HairGeometry* geometry);
	//! getPOS
	Math::Vec3f getPOS(int index, HairGeometryIndices* geometryIndices, HairGeometry* geometry);
	//! ������ ������
	float getRadius(int index) const;
	//! getBasis
	void getBasis(
		int index, HairGeometryIndices* geometryIndices, HairGeometry* geometry, 
		Math::Vec3f& normal, Math::Vec3f& tangentU, Math::Vec3f& tangentV);
	void getBasis(
		int index, HairGeometryIndices* geometryIndices, HairGeometry* geometry, 
		Math::Vec3f& normal, Math::Vec3f& tangentU, Math::Vec3f& tangentV,
		Math::Vec3f& dpdU, Math::Vec3f& dpdV);
	//! per hair seed
	int getSeed(int index);
	//! get polygon index
	int getPolygonIndex(int index, const HairGeometryIndices& geometryIndices);

	//! getGroupCount
	int getGroupCount();
	//! group
	void getGroup(int group, int& startindex, int& endindex);

public:
	// �������
	std::vector< HairGeometryIndices::pointonsurface> clumps;
	// ������ �� ��-���
	std::vector< HairGeometryIndices::facegroup> clumpgroups;
	// seed
	std::vector< int> clumpseeds;
	// ������ ������
	std::vector< float> radius;

public:
	void serialize(Util::Stream& stream);
	void clear();
	void buildPolygonGroups(
		std::vector<HairGeometryIndices::facegroup>& clumpPolygonGroups, 
		const HairGeometryIndices& hgi);

public:
	/*/
	//! ����� ������ � ������� ��� ������
	void findClumpGroupNPolygon(
		int h, 
		HairGeometryIndices& geometryIndices,
		int& group,
		int& polygon);
	/*/
};

inline int HairClumpPositions::getCount()
{
	return (int)clumps.size();
}

//! ����� �� �����������
inline const HairGeometryIndices::pointonsurface& HairClumpPositions::getPointOnSurface( int index) const
{
	if( index>=(int)clumps.size())
		return HairGeometryIndices::badpointonsurface;
	else
		return clumps[index];
}

inline Math::Vec2f HairClumpPositions::getUV(int index, HairGeometryIndices* geometryIndices, HairGeometry* geometry)
{
	if( index>=(int)clumps.size())
		return Math::Vec2f(0, 0);
	HairGeometryIndices::pointonsurface& pos = clumps[index];
	return geometry->getUV(pos.faceindex, pos.b, geometryIndices);
}
//! getUVset
inline Math::Vec2f HairClumpPositions::getUVset(int index, int uvset, HairGeometryIndices* geometryIndices, HairGeometry* geometry)
{
	if( index>=(int)clumps.size())
		return Math::Vec2f(0, 0);
	HairGeometryIndices::pointonsurface& pos = clumps[index];
	return geometry->getUVset(pos.faceindex, pos.b, geometryIndices, uvset);
}

inline Math::Vec3f HairClumpPositions::getPOS(int index, HairGeometryIndices* geometryIndices, HairGeometry* geometry)
{
	if( index >= (int)clumps.size())
		return Math::Vec3f(0, 0, 0);
	HairGeometryIndices::pointonsurface& pos = clumps[index];
	return geometry->getPOS(pos.faceindex, pos.b, geometryIndices);
}
//! ������ ������
inline float HairClumpPositions::getRadius(int index) const
{
	if( index<0) 
		return 0;
	if( index >= (int)radius.size())
		return 0;
	return radius[index];
}

inline void HairClumpPositions::getBasis(
	int index, HairGeometryIndices* geometryIndices, HairGeometry* geometry, 
	Math::Vec3f& normal,
	Math::Vec3f& tangentU,
	Math::Vec3f& tangentV)
{
	if( index>=(int)clumps.size())
		return;
	HairGeometryIndices::pointonsurface& pos = clumps[index];
	geometry->getBasis(pos.faceindex, pos.b, geometryIndices, normal, tangentU, tangentV);
}
inline void HairClumpPositions::getBasis(
	int index, HairGeometryIndices* geometryIndices, HairGeometry* geometry, 
	Math::Vec3f& normal,
	Math::Vec3f& tangentU,
	Math::Vec3f& tangentV,
	Math::Vec3f& dpdU, Math::Vec3f& dpdV)
{
	if( index>=(int)clumps.size())
		return;
	HairGeometryIndices::pointonsurface& pos = clumps[index];
	geometry->getBasis(pos.faceindex, pos.b, geometryIndices, normal, tangentU, tangentV, dpdU, dpdV);
}

//! per hair seed
inline int HairClumpPositions::getSeed(int index)
{
	if(index<0 || index>=(int)clumpseeds.size())
		return 0;
	return clumpseeds[index];
}
inline int HairClumpPositions::getGroupCount()
{
	if(clumpgroups.empty())
		return 1;
	return (int)clumpgroups.size();
}
inline void HairClumpPositions::getGroup(int group, int& startindex, int& endindex)
{
	startindex = 0;
	endindex = (int)clumps.size();
	if( group<0 || group>=(int)clumpgroups.size()) 
		return;

	startindex = clumpgroups[group].startindex;
	endindex   = clumpgroups[group].endindex;
	return;
}
inline int HairClumpPositions::getPolygonIndex(int index, const HairGeometryIndices& gi)
{
	if( index>=(int)clumps.size())
		return -1;
	return gi.face_polygonIndex(clumps[index].faceindex);
}
