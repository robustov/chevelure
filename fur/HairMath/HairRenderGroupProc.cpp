#include "stdafx.h"
#include "HairMath.h"
#include "HairRender.h"

#include "Procedurals/HairProceduralSnOcs.h"
#include "Procedurals/HairProceduralTubes.h"
#include "Procedurals/HairProceduralCurveOcs.h"
#include ".\hairlicensethread.h"

struct HairRenderGroupProc : public cls::IProcedural
{
	// ������ ����������
	virtual void Render(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		);
protected:

	bool setUpHairs(
		HairRender* hairRender, 
		int startindex, 
		int count, 
		bool bClump, 
		HairRenderPass::enSource rendertype,
		float degradation,
		float degradationWidth,
		int renderNormalSource,
		float RenderWidth, 
		bool useClumpRadiusAsWidth, 
		float widthBias,
		HairProcessor& processor, 
		DrawSynk& procedural
		);

};

HairRenderGroupProc hairrendergroupproc;

extern "C"
{
	// Dump
	__declspec(dllexport) cls::IProcedural* __cdecl HairRenderGroup(cls::IRender* prman)
	{
		return &hairrendergroupproc;
	}
}


bool HairRenderGroupProc::setUpHairs(
	HairRender* hairRender, 
	int startindex, 
	int count, 
	bool bClump, 
	HairRenderPass::enSource rendertype,
	float degradation,
	float degradationWidth,
	int renderNormalSource,
	float RenderWidth, 
	bool useClumpRadiusAsWidth, 
	float widthBias,
	HairProcessor& processor, 
	DrawSynk& procedural
	)
{
	HAIRVERTS hair;
	HAIRPARAMS params;
	int dg=0;

	for(int i=0; i<count; i++)
	{
		float degparam;
		bool deg = hairRender->IsHairDegradate(startindex+i, bClump, degradation, degradationWidth, degparam);
		if( deg) continue;

		if( !processor.BuildHair(i+startindex, params, hair))
			return false;

		if( !hair.vc()) 
			continue;

		// ��������� degradationWindow
		if( degradation<1 && degradationWidth>0.001)
		{
			if( degparam > degradation)
			{
				float resDegradation = degradation + degradationWidth;
				float wfactor = (degparam - resDegradation)/( -degradationWidth);
				for(int v=0; v<(int)hair.vwidht.size(); v++)
					hair.vwidht[v] *= wfactor;
			}
		}

		float rw = RenderWidth;
		if(useClumpRadiusAsWidth)
		{
			rw = hair.clumpradius;
			if(hair.vwidht.size()==hair.vclump.size())
			{
				hair.vwidht = hair.vclump;
				for(int v=0; v<(int)hair.vwidht.size(); v++)
					hair.vwidht[v] = 1-hair.vwidht[v];
			}
		}

		procedural.SetHair(
			dg, 
			i+startindex, 
			params, 
			hair, 
			(enRenderNormalSource)renderNormalSource, 
			rw, 
			widthBias
			);
		dg++;
	}
	return true;
}



// ������ ����������
void HairRenderGroupProc::Render(
	cls::IRender* render, 
	int motionBlurSamples, 
	float* motionBlurTimes
	)
{
//	if( motionBlurSamples)
//		render->SetCurrentMotionPhase(motionBlurTimes[0]);

//	#ifdef SAS
//	#ifndef CHEVELUREDEMO
//		printf("lf: %x\n", licenseflag);
//	#endif
//	#endif

	// ��������� ���������� ������ �����
	int startindex = 0;
	int endindex = 0;
	bool bClump = false;
	float prune = 0;
	int rendertype = HairRenderPass::RT_HAIRS;
	bool mulWidthByLength=false;

	float degradation=1, degradationWidth=0;
	float opacity=-1;

	int renderNormalSource = RNS_NONE;
	int SegmCount = 2;
	int interpolation = HI_LINIAR;
	int orthogonalize = ORT_NONE;
	float RenderWidth = 1; 
	bool useClumpRadiusAsWidth = false;
	float widthBias = 0;

	int hairRenderindex=0;
	int renderflags = DrawSynk::DC_DEFAULT;

	render->GetParameter("@hairRender", hairRenderindex);
	render->GetParameter("@startindex", startindex);
	render->GetParameter("@endindex", endindex);
	render->GetParameter("@bClump", bClump);
	render->GetParameter("@prune", prune);
	render->GetParameter("@rendertype", rendertype);
	render->GetParameter("@mulWidthByLength", mulWidthByLength);
	render->GetParameter("@degradation", degradation);
	render->GetParameter("@degradationWidth", degradationWidth);
	render->GetParameter("@opacity", opacity);
	render->GetParameter("@renderNormalSource", renderNormalSource);
	render->GetParameter("@SegmCount", SegmCount);
	render->GetParameter("@interpolation", interpolation);
	render->GetParameter("@orthogonalize", orthogonalize);
	render->GetParameter("@RenderWidth", RenderWidth);
	render->GetParameter("@useClumpRadiusAsWidth", useClumpRadiusAsWidth);
	render->GetParameter("@widthBias", widthBias);
	render->GetParameter("@renderflags", renderflags);

	///////////////////////////////////
	// ������
	HairRender* hairRender = hairRenderPool[hairRenderindex];

//	hairRender->release();

	int count = endindex-startindex;
	if( count<=0)
		return;
	HairGeometryIndices* hgi = &hairRender->staticData->geometryIndices;
	HairSystem& hairSystem = hairRender->hairSystem;
//	if( hairSystem.dumpInformation)
//		fprintf(stderr, "."), fflush(stderr);

	std::vector<const char*> adduv(hgi->uvSetCount(), NULL);
	for(unsigned i=0; i<adduv.size(); i++)
		adduv[i] = hgi->uvSetName(i);

	bool bNormals = !(renderNormalSource==RNS_NONE);

//printf("HairRenderGroupProc::setUpHairs %d %d\n", startindex, count);

	////////////////////////////////////
	// ��� ��������� �������
	DrawSynk* procedure, *freezeprocedural;
	HairProceduralCurveOcs	proceduralsCurve[2];
	HairProceduralSnOcs		proceduralsSn[2];
	HairProceduralTubes		proceduralsTubes[2];
	int snshapecount = 0;
	if( rendertype==HairRenderPass::RT_HAIRS)
	{
		procedure = &proceduralsCurve[0];
		freezeprocedural = &proceduralsCurve[1];
	}
	else if( rendertype==HairRenderPass::RT_INSTANCES)
	{
		procedure = &proceduralsSn[0];
		freezeprocedural = &proceduralsSn[1];

//		fprintf(stderr, "HairMath: rendertype==HairRenderPass::RT_INSTANCES not supported!\n");
//		fflush(stderr);
//		return;
		{
			std::map< int, sn::ShapeShortcutPtr >::iterator it = hairSystem.snshapes.begin();
			for(;it != hairSystem.snshapes.end(); it++)
			{
				procedure->setSnShape(it->first, it->second);
				freezeprocedural->setSnShape(it->first, it->second);
			}
			snshapecount = (int)hairSystem.snshapes.size();
		}
		{
			std::map< int, cls::MInstance>::iterator it = hairSystem.ocsfiles.begin();
			for(;it != hairSystem.ocsfiles.end(); it++)
			{
				procedure->setOcsShape(it->first, it->second);
				freezeprocedural->setOcsShape(it->first, it->second);
			}
			snshapecount = (int)hairSystem.ocsfiles.size();
		}
	}
	else if( rendertype==HairRenderPass::RT_TUBES || rendertype==HairRenderPass::RT_SUBDIVTUBES)
	{
		proceduralsTubes[0].setSubdiv(rendertype==HairRenderPass::RT_SUBDIVTUBES);
		proceduralsTubes[1].setSubdiv(rendertype==HairRenderPass::RT_SUBDIVTUBES);

		procedure = &proceduralsTubes[0];
		freezeprocedural = &proceduralsTubes[1];

	}



	///////////////////////////////////
	// mulWidthByLength
	if( rendertype==HairRenderPass::RT_INSTANCES) 
		mulWidthByLength = false;

	///////////////////////////////////
	// degradationHairs
	int countAfterDegradation=count;
	float resDegradation = degradation + degradationWidth;

	if(resDegradation<1)
	{	
		countAfterDegradation = 0;
		for(int i=0; i<count; i++)
		{
			float degparam;
			bool deg = hairRender->IsHairDegradate(startindex+i, bClump, degradation, degradationWidth, degparam);
			if( deg) continue;

			countAfterDegradation++;
		}
	}
	if( countAfterDegradation==0) 
	{
		// �� ��������!
		return;
	}

	// ����������
	hairRender->nGroups++;
	hairRender->nHairs = countAfterDegradation;

	// extraParameters
	std::map< std::string, cls::Param> extraParameters;
	extraParameters["prune"] = cls::P<float>(prune);
	if( opacity>=0) 
	{
		Math::Vec3f Os(opacity);
		extraParameters["Os"] = cls::P<Math::Vec3f>(Os, cls::PT_COLOR);
	}

	///////////////////////////////////////
	// freeze
	if(hairRender->deformfreeze)
	{
		DrawSynk& procedural = *freezeprocedural;
		procedural.setBaseUvSet(hgi->getBaseUVSet());
		int deformflags = 0;
		procedural.Init(countAfterDegradation, SegmCount+1, ((enHairInterpolation)interpolation), bNormals, &adduv, deformflags);

		HairRender::Dynamic& dynamicRecord = *hairRender->deformfreeze;
		HairMesh* surface = &dynamicRecord.surface;
		HairProcessor processor(
			SegmCount, *surface, 
			hairSystem.snshapes, 
			hairSystem.ocsfiles, 
			hairSystem.snshapes_weight, 
			mulWidthByLength, (enOrthogonalizeType)orthogonalize, true, bClump);
		
		if( !this->setUpHairs(
			hairRender, 
			startindex, 
			count, 
			bClump, 
			(HairRenderPass::enSource)rendertype,
			degradation,
			degradationWidth,
			renderNormalSource,
			RenderWidth, 
			useClumpRadiusAsWidth, 
			widthBias,
			processor, 
			procedural
			))
		{
			fprintf(stderr, "stop render group %d-%d\n", startindex, endindex);
			return;
		}
	}
	else
	{
		freezeprocedural = 0;
	}


	///////////////////////////////////////
	// ����
	int c = 1;
	if( motionBlurSamples)
	{
		render->MotionBegin();
		c = motionBlurSamples;
	}
	for(int blur=0; blur<c; blur++)
	{
//		if( motionBlurSamples)
//			render->SetCurrentMotionPhase(motionBlurTimes[blur]);

		DrawSynk& procedural = *procedure;

		// ��������� �����
		procedural.setBaseUvSet( hgi->getBaseUVSet());
		procedural.Init(countAfterDegradation, SegmCount+1, ((enHairInterpolation)interpolation), bNormals, &adduv, renderflags);

//		float dynamicRecordtime=motionBlurTimes[blur];
//		render->GetParameter("dynamicRecordtime", dynamicRecordtime);

//		std::map<float, HairRender::Dynamic*>::iterator it = hairRender->deformblur.find(dynamicRecordtime);
		std::map<float, HairRender::Dynamic*>::iterator it = hairRender->deformblur.begin();
		if(motionBlurTimes)
			it = hairRender->deformblur.find(motionBlurTimes[blur]);
		if( it==hairRender->deformblur.end())
			return;
		HairRender::Dynamic& dynamicRecord = *it->second;
		HairMesh& surface = dynamicRecord.surface;

		HairProcessor processor(
			SegmCount, 
			surface, 
			hairSystem.snshapes, 
			hairSystem.ocsfiles, 
			hairSystem.snshapes_weight, 
			mulWidthByLength, (enOrthogonalizeType)orthogonalize, true, bClump);

		if( !this->setUpHairs(
			hairRender, 
			startindex, 
			count, 
			bClump, 
			(HairRenderPass::enSource)rendertype,
			degradation,
			degradationWidth,
			renderNormalSource,
			RenderWidth, 
			useClumpRadiusAsWidth, 
			widthBias,
			processor, 
			procedural
			))
		{

			fprintf(stderr, "stop render group %d-%d\n", startindex, endindex);
			if( motionBlurSamples)
				render->MotionEnd();
			return;
		}

		float motionPhase = cls::nonBlurValue;

		if( motionBlurSamples)
		{
			render->MotionPhaseBegin(it->first);
			motionPhase = it->first;
		}

		procedural.Render(NULL, render, motionPhase, extraParameters, freezeprocedural);

		if( motionBlurSamples)
			render->MotionPhaseEnd();
	}
	if( motionBlurSamples)
	{
		render->MotionEnd();
	}

//	if( motionBlurSamples)
//		render->SetCurrentMotionPhase(cls::nonBlurValue);
	
	// �������� �� ������
	procedure->RenderCall(render, motionBlurSamples, motionBlurTimes);
}



