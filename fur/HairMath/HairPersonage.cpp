#include "stdafx.h"

#include "HairPersonage.h"
#include <math.h>


HairPersonageData::HairPersonageData()
{
	instance = 0;
}
HairPersonageData::HairPersonageData(const HairPersonageData& arg)
{
	*this = arg;
}
HairPersonageData& HairPersonageData::operator = (const HairPersonageData& arg)
{
	transform	= arg.transform;
	length		= arg.length;

	transforms	= arg.transforms;
	parameters	= arg.parameters;
	polar		= arg.polar;
	inclination	= arg.inclination;
	baseCurl	= arg.baseCurl;
	tipCurl		= arg.tipCurl;
	baseWidth	= arg.baseWidth;
	tipWidth	= arg.tipWidth;
	twist		= arg.twist;
	u			= arg.u;
	v			= arg.v;
	instance	= arg.instance;
	id			= arg.id;
	animation	= arg.animation;
	return *this;
}


// HairPersonage
HairPersonage::HairPersonage()
{
	transform = Math::Matrix4f::id;
}

void HairPersonage::validateAttr(sn::attr_t& attr)
{
	if( attr.id==-1)
	{
		// ����� ������� �� �����
		if( strcmp("tolerance",attr.name.c_str())==0) attr.id = 0;
		else if( strcmp("polar",attr.name.c_str())==0) attr.id = 1;
		else if( strcmp("inclination",attr.name.c_str())==0) attr.id = 2;
		else if( strcmp("baseCurl",attr.name.c_str())==0) attr.id = 3;
		else if( strcmp("tipCurl",attr.name.c_str())==0) attr.id = 4;
//		else if( strcmp("length",attr.name.c_str())==0) attr.id = 5;
		else if( strcmp("twist",attr.name.c_str())==0) attr.id = 6;
		else if( strcmp("baseWidth",attr.name.c_str())==0) attr.id = 7;
		else if( strcmp("tipWidth",attr.name.c_str())==0) attr.id = 8;
		else if( strcmp("U",attr.name.c_str())==0) attr.id = 9;
		else if( strcmp("V",attr.name.c_str())==0) attr.id = 10;
		else if( strcmp("transforms",attr.name.c_str())==0) attr.id = 11;
		else if( strcmp("parameters",attr.name.c_str())==0) attr.id = 12;
		else if( strcmp("id",attr.name.c_str())==0) attr.id = 13;
	}
}
void HairPersonage::getValue(sn::attr_t& attr, float& val)
{
	validateAttr(attr);
	switch(attr.id)
	{
		case 1: val = data.polar; return;
		case 2: val = data.inclination.length(); return;
		case 3: val = data.baseCurl.length(); return;
		case 4: val = data.tipCurl.length(); return;
//		case 5: val = 0; return;
		case 6: val = data.twist; return;
		case 7: val = data.baseWidth; return;
		case 8: val = data.tipWidth; return;
		case 9: val = data.u; return;
		case 10: val = data.v; return;
		case 13: val = (float)data.id; return;
	}
	val = 0;
	return;
};
void HairPersonage::getValue(sn::attr_t& attr, std::vector<float>& val)
{
	val.clear();
	validateAttr(attr);
	if(attr.id==12)
	{
		val = data.parameters;
		return;
	}
}
void HairPersonage::getValue(sn::attr_t& attr, std::vector<Math::Matrix4f>& val)
{
	val.clear();
	validateAttr(attr);
	if(attr.id==11)
	{
		val = data.transforms;
		return;
	}
}

