#include "stdafx.h"
	#include "ocellaris/IRender.h"
#include "HairProcessor.h"
#include <math.h>
#include <fstream>
#include "Math/cellnoise.h"
#include "Math/interpolation.h"
#include "HairMesh.h"
#include ".\hairlicensethread.h"

float min_hairLen = (float)1e-10;

#include "Util/Stream.h"
#include "Util/FileStream.h"
#include "Util/STLStream.h"

HairProcessor::HairProcessor(
	int SegmCount,
	HairMesh& surface, 
	std::map< int, sn::ShapeShortcutPtr>& snshapes,
	std::map< int, cls::MInstance>& ocsfiles,
	std::map< int, HairProcessor::weight_t>& snshapes_weight,
	bool mulWidthByLength, 
	enOrthogonalizeType orthogonalize, 
	bool bUseAddUV,
	// �������� ������
	bool bHairIsClump
	)
{
	this->SegmCount = SegmCount;
	this->surface = &surface;
	this->mulWidthByLength = mulWidthByLength;
	this->orthogonalize = orthogonalize;
	this->bUseAddUV = bUseAddUV;
	this->bHairIsClump = bHairIsClump;

	{
		std::set<int> instindxs;
		{
			std::map< int, sn::ShapeShortcutPtr>::iterator it = snshapes.begin();
			for(;it != snshapes.end(); it++)
				instindxs.insert(it->first);
		}
		{
			std::map< int, cls::MInstance>::iterator it = ocsfiles.begin();
			for(;it != ocsfiles.end(); it++)
				instindxs.insert(it->first);
		}
		this->instanceDatas.clear();
		this->instanceDatas.resize(instindxs.size());
		std::set<int>::iterator it = instindxs.begin();
		for(int i=0; it!=instindxs.end(); it++, i++)
		{
			InstanceData& instancedata = this->instanceDatas[i];
			instancedata.index = *it;
			instancedata.instanceweights = NULL;

			if( snshapes_weight.find(instancedata.index)!=snshapes_weight.end())
			{
				instancedata.instanceweights = &snshapes_weight[instancedata.index];
			}
		}
	}

	this->sizein_HAIRPARAMS = this->sizein_HAIRVERTS = 0;
	int dc = surface.getDeformerCount();
	deformers_INCREASELENGTH.reserve(dc);
	deformers_PREPARSEHAIR.reserve(dc);
	deformers_PARSEHAIR.reserve(dc);
	deformers_BUILDHAIR.reserve(dc);
	deformers_MERGEWITHCLUMP.reserve(dc);
	deformers_POSTBUILDHAIR.reserve(dc);
	deformers_DEFORMHAIRVERTEX.reserve(dc);
	for(int i=0; i<(int)dc; i++)
	{
		IHairDeformer* deformer = surface.getDeformer(i);
		if( !deformer) continue;

		deformer->requiredMemoryForHair(SegmCount, sizein_HAIRPARAMS, sizein_HAIRVERTS);

		int type = deformer->getType();

		if( type&IHairDeformer::INCREASELENGTH)
			deformers_INCREASELENGTH.push_back(deformer);
		if( type&IHairDeformer::PREPARSEHAIR)
			deformers_PREPARSEHAIR.push_back(deformer);
		if( type&IHairDeformer::PARSEHAIR)
			deformers_PARSEHAIR.push_back(deformer);
		if( type&IHairDeformer::BUILDHAIR)
			deformers_BUILDHAIR.push_back(deformer);
		if( type&IHairDeformer::MERGEWITHCLUMP)
			deformers_MERGEWITHCLUMP.push_back(deformer);
		if( type&IHairDeformer::POSTBUILDHAIR)
			deformers_POSTBUILDHAIR.push_back(deformer);
		if( type&IHairDeformer::DEFORMHAIRVERTEX)
			deformers_DEFORMHAIRVERTEX.push_back(deformer);
	}
}
bool HairProcessor::BuildHair( 
	int index, 
	HAIRPARAMS& params, 
	HAIRVERTS& hair
	)
{
	// ������������� ������ ��� ���������
	params.clear(sizein_HAIRPARAMS);
	hair.clear(sizein_HAIRVERTS);

	// ��������� ������
//	bool useCV = surface->cvPositions->getCount()!=0;
	this->ParseHair(params, index, bHairIsClump);

	HAIRPARAMS paramsclump;
	paramsclump.clear(sizein_HAIRPARAMS);
	if( !bHairIsClump && params.clumpIndex>=0)
	{
		std::map<int, HAIRPARAMS>::iterator it = clumpparamcache.find(params.clumpIndex);
		if(it!=clumpparamcache.end())
		{
			paramsclump = it->second;
		}
		else
		{
			this->ParseHair(paramsclump, params.clumpIndex, true);
			clumpparamcache[params.clumpIndex] = paramsclump;
		}
		// ��������� �� ����� ������
		if( params.length*params.lengthjitter>paramsclump.length)
		{
			params.length = paramsclump.length;
			params.lengthjitter = 1;
		}
	}

	{
	#ifndef CHEVELUREDEMO
		CreateHairFromCV(params, hair, bHairIsClump);
	#else //CHEVELUREDEMO
		CreateHairDefault(params, hair, bHairIsClump);
	#endif
		if( !DeformAloneHair(params, hair, bHairIsClump))
			return false;
		
		if( hair._isnan())
		{
			fprintf(stderr, "Hair %d is NAN build error\n", index);
			return false;
		}
//		if( !DeformAloneHair_test(params, hair, bHairIsClump))
//			return;
	}

	if( !bHairIsClump && params.clumpIndex>=0)
	{
		// ��������	
		HAIRVERTS hairclump;
		hairclump.clear(sizein_HAIRVERTS);
		std::map<int, HAIRVERTS>::iterator it = clumpvertscache.find(params.clumpIndex);
		if(it!=clumpvertscache.end())
		{
			hairclump = it->second;
		}
		else
		{
			CreateHairFromCV(paramsclump, hairclump, true);
			if( !DeformAloneHair(paramsclump, hairclump, true))
				return false;
			if( hairclump._isnan())
			{
				fprintf(stderr, "Clump %d is NAN build error\n", params.clumpIndex);
				return false;
			}

//				if( !DeformAloneHair_test(paramsclump, hairclump, true))
//					return;
		}

		HAIRVERTS orighair = hair;
		this->BuildHairInClump(paramsclump, hairclump, params, orighair, hair);

		if( hair._isnan())
		{
			fprintf(stderr, "Hair %d is NAN BuildHairInClump error\n", index);
			return false;
		}

		hair.clumpradius = surface->clumpPositions->getRadius(params.clumpIndex);
		hair.clump_normal = paramsclump.normal;
		hair.clump_vector = paramsclump.position - params.position;
	}
	else
	{
		// �������� clump
		hair.clumpradius = 0;
		hair.clump_normal = params.normal;
		hair.clump_vector = Math::Vec3f(0);
		if(bHairIsClump)
		{
			hair.clumpradius = surface->clumpPositions->getRadius(index);
		}
		else
		{
			for(int i=0; i<(int)hair.vclump.size(); i++)
				hair.vclump[i] = 0;
		}
	}


	// ��������� ������� �������� � ������������������� ��-��
	Math::Matrix4f toortho = Math::Matrix4f::id;
	switch(orthogonalize)
	{
	case ORT_TANGENTU_V1:
		{
			Math::Matrix4f bsrc; 
			params.getBasis(bsrc);
			params.orthogonalize(true);
			Math::Matrix4f borth;
			params.getBasis(borth);

			borth.invert();
			toortho = bsrc*borth;
		}
		break;
	case ORT_TANGENTV_V1:
		{
			Math::Matrix4f bsrc; 
			params.getBasis(bsrc);
			params.orthogonalize(false);
			Math::Matrix4f borth;
			params.getBasis(borth);

			borth.invert();
			toortho = bsrc*borth;
		}
		break;
	}

	for(int i=0; i<(int)hair.basas.size(); i++)
	{
		Math::Vec3f p = toortho * hair.basas[i][3].as<3>();
		hair.basas[i][3] = Math::Vec4f(p, 1);
	}

	return true;
}

// ��������� ��������
void HairProcessor::ApplyModifiers(
	HairMesh::ModifiersData& modifiers, 
	HAIRPARAMS& c_hairs, 
	bool bClump
	)
{
	if( modifiers.empty()) return;

	float U = c_hairs.u;
	float V = c_hairs.v;

	float length		= c_hairs.length;
	float lengthjitter	= c_hairs.lengthjitter;
//	float polar			= c_hairs.polar;
	Math::Vec2f inclination 	= c_hairs.inclination;
	Math::Vec2f baseCurl 		= c_hairs.baseCurl;
	Math::Vec2f tipCurl 		= c_hairs.tipCurl;
	float scraggle		= c_hairs.scraggle;
	float scraggleFreq	= c_hairs.scraggleFreq;
	float BaseClumpValue= c_hairs.BaseClumpValue;
	float TipClumpValue = c_hairs.TipClumpValue;
	float twist			= c_hairs.twist;
	float twistFromPolar= c_hairs.twistFromPolar;
	float blendCVfactor = c_hairs.blendCVfactor;
	float blendDeformfactor= c_hairs.blendDeformfactor;
	float displace		= c_hairs.displace;
	float baseWidth		= c_hairs.baseWidth;
	float tipWidth		= c_hairs.tipWidth;

	c_hairs.length		*=modifiers.w;
	c_hairs.lengthjitter*=modifiers.w;
	c_hairs.inclination *=modifiers.w;
//	c_hairs.polar		*=modifiers.w;
	c_hairs.baseCurl	*=modifiers.w;
	c_hairs.tipCurl		*=modifiers.w;
	c_hairs.scraggle	*=modifiers.w;
	c_hairs.scraggleFreq *=modifiers.w;
	c_hairs.BaseClumpValue *=modifiers.w;
	c_hairs.TipClumpValue *=modifiers.w;
	c_hairs.twist		*=modifiers.w;
	c_hairs.twistFromPolar*=modifiers.w;
	c_hairs.blendCVfactor*=modifiers.w;
	c_hairs.blendDeformfactor*=modifiers.w;

	c_hairs.displace	*=modifiers.w;
	c_hairs.baseWidth	*=modifiers.w;
	c_hairs.tipWidth	*=modifiers.w;

	/*/
	Math::Vec2f vpolar_src = polar2vector(polar);
	Math::Vec2f vpolar = vpolar_src*modifiers.w;
	/*/

	Math::Matrix3f basis = buildBasis3f(c_hairs.normal, c_hairs.tangentU, c_hairs.tangentV);
	basis.invert();

	for(unsigned x=0; x<modifiers.affectmodifiers.size(); x++)
	{
		srand( c_hairs.seed+45);

		HairMesh::ModifierData& m = modifiers.affectmodifiers[x];
		HairMeshModifier* modifier = m.modifier;
		float lw = m.w;

		Math::Vec2f v = polar2vector(0);
		// ����������� �� �����
		if( modifier->isRelative())
		{
			Math::Vec3f v3 = m.dirToModifier*basis;
			v = tangentSpace2_2d(v3).normalized();
		}

		HAIRPARAMS modiferparam;
		modifier->LoadParams(modiferparam, v, U, V, bClump);

		if(modifier->hairParamBit & HPB_LENGTH)
		{
			c_hairs.length			+= lw*modiferparam.length;
			c_hairs.lengthjitter	+= lw*modiferparam.lengthjitter;
		}
		else
		{
			c_hairs.length			+= lw*length;
			c_hairs.lengthjitter	+= lw*lengthjitter;
		}

		if(modifier->hairParamBit & HPB_INCLINATION)
			c_hairs.inclination		+= lw*modiferparam.inclination;
		else
			c_hairs.inclination		+= lw*inclination;

		/*/
		if(modifier->hairParamBit & HPB_POLAR)
		{
			Math::Vec2f v = polar2vector(modiferparam.polar);
			vpolar += lw*v;
		}
		else
		{
			vpolar += lw*vpolar_src;
		}
		/*/

		if(modifier->hairParamBit & HPB_BASECURL)
			c_hairs.baseCurl		+= lw*modiferparam.baseCurl;
		else
			c_hairs.baseCurl		+= lw*baseCurl;

		if(modifier->hairParamBit & HPB_TIPCURL)
			c_hairs.tipCurl			+= lw*modiferparam.tipCurl;
		else
			c_hairs.tipCurl			+= lw*tipCurl;

		if(modifier->hairParamBit & HPB_SCRAGLE)
			c_hairs.scraggle		+= lw*modiferparam.scraggle;
		else
			c_hairs.scraggle		+= lw*scraggle;

		if(modifier->hairParamBit & HPB_SCRAGLEFREQ)
			c_hairs.scraggleFreq	+= lw*modiferparam.scraggleFreq;
		else
			c_hairs.scraggleFreq	+= lw*scraggleFreq;

		if(modifier->hairParamBit & HPB_BASECLUMP)
			c_hairs.BaseClumpValue	+= lw*modiferparam.BaseClumpValue;
		else
			c_hairs.BaseClumpValue	+= lw*BaseClumpValue;

		if(modifier->hairParamBit & HPB_TIPCLUMP)
			c_hairs.TipClumpValue	+= lw*modiferparam.TipClumpValue;
		else
			c_hairs.TipClumpValue	+= lw*TipClumpValue;

		if(modifier->hairParamBit & HPB_TWIST)
			c_hairs.twist			+= lw*modiferparam.twist;
		else
			c_hairs.twist			+= lw*twist;

		if(modifier->hairParamBit & HPB_TWISTFROMPOLAR)
			c_hairs.twistFromPolar	+= lw*modiferparam.twistFromPolar;
		else
			c_hairs.twistFromPolar	+= lw*twistFromPolar;


		if(modifier->hairParamBit & HPB_BLENDCVFACTOR)
			c_hairs.blendCVfactor	+= lw*modiferparam.blendCVfactor;
		else
			c_hairs.blendCVfactor	+= lw*blendCVfactor;

		if(modifier->hairParamBit & HPB_BLENDDEFORMFACTOR)
			c_hairs.blendDeformfactor	+= lw*modiferparam.blendDeformfactor;
		else
			c_hairs.blendDeformfactor	+= lw*blendDeformfactor;


		if(modifier->hairParamBit & HPB_DISPLACE)
			c_hairs.displace		+= lw*modiferparam.displace;
		else
			c_hairs.displace		+= lw*displace;

		if(modifier->hairParamBit & HPB_BASEWIDTH)
			c_hairs.baseWidth		+= lw*modiferparam.baseWidth;
		else
			c_hairs.baseWidth		+= lw*baseWidth;

		if(modifier->hairParamBit & HPB_TIPWIDTH)
			c_hairs.tipWidth		+= lw*modiferparam.tipWidth;
		else
			c_hairs.tipWidth		+= lw*tipWidth;
	}
//	c_hairs.polar = vector2polar(vpolar);
}
void HairProcessor::ApplyModifiersPolar(
	HairMesh::ModifiersData& modifiers, 
	HAIRPARAMS& c_hairs, 
	bool bClump
	)
{
	if( modifiers.empty()) return;
	float U = c_hairs.u;
	float V = c_hairs.v;

	float polar			= c_hairs.polar;
	c_hairs.polar		*=modifiers.w;

	Math::Vec2f vpolar_src = polar2vector(polar);
	Math::Vec2f vpolar = vpolar_src*modifiers.w;

	Math::Matrix3f basis = buildBasis3f(c_hairs.normal, c_hairs.tangentU, c_hairs.tangentV);
	basis.invert();

	for(unsigned x=0; x<modifiers.affectmodifiers.size(); x++)
	{
		srand( c_hairs.seed+45);

		HairMesh::ModifierData& m = modifiers.affectmodifiers[x];
		HairMeshModifier* modifier = m.modifier;
		float lw = m.w;

		Math::Vec2f v = polar2vector(0);
		// ����������� �� �����
		if( modifier->isRelative())
		{
			Math::Vec3f v3 = m.dirToModifier*basis;
			v = tangentSpace2_2d(v3).normalized();
		}

		HAIRPARAMS modiferparam;
		modifier->LoadParams(modiferparam, v, U, V, bClump);

		if(modifier->hairParamBit & HPB_POLAR)
		{
			Math::Vec2f v = polar2vector(modiferparam.polar);
			vpolar += lw*v;
		}
		else
		{
			vpolar += lw*vpolar_src;
		}
	}
	c_hairs.polar = vector2polar(vpolar);
}

// ������� ��������� ������ ��� ������ �� ������� ������
void HairProcessor::ParseHair(
	HAIRPARAMS& c_hairs, 
	int vert, bool bClump		// ����� �������� ��� ���� ������
	)
{
	c_hairs.uvSets.clear();
	c_hairs.cv.clear();
	c_hairs.seed		= surface->getSeed(vert, bClump);

	surface->getUVAtHair(vert, bClump, c_hairs.u, c_hairs.v);
	float U = c_hairs.u;
	float V = c_hairs.v;

	// ����� ��������
	srand( c_hairs.seed+44);

	int instanceCount = (int)instanceDatas.size();
	if( instanceCount>0)
	{

		float ri = rand()/(float)RAND_MAX;
		std::vector<float> weigths(instanceCount);
		float sum = 0;
		int i=0;
		for(i=0;i<instanceCount; i++)
		{
			InstanceData& id = instanceDatas[i];
			float w = 1;
			if(id.instanceweights)
				w = id.instanceweights->getValue(U, V);
			sum += w;
			weigths[i] = sum;
		}
		if(sum<0.0001f)
			sum = 1;
		for(i=0;i<instanceCount; i++)
		{
			float w = weigths[i]/sum;
			if(ri<w)
				break;
		}
		if( i>=instanceCount)
			i=instanceCount-1;
		c_hairs.instance = instanceDatas[i].index;
	}
	else
	{
		c_hairs.instance = 0;
	}

	srand( c_hairs.seed+22);

	surface->getPointAtHair( vert, bClump, c_hairs.position, c_hairs.normal, c_hairs.tangentU, c_hairs.tangentV, c_hairs.dPdU, c_hairs.dPdV);
	c_hairs.basis_inv = buildBasis4f(c_hairs.position, c_hairs.normal, c_hairs.tangentU, c_hairs.tangentV);
	c_hairs.basis_inv.invert();

	surface->getControlHair( vert, bClump, c_hairs.cv);
	// ����������� TS ����� OS
	if( !c_hairs.cv.empty())
	{
		Math::Vec3f ofs(0);
		for(int v=0; v<(int)c_hairs.cv.ts.size(); v++)
		{
			c_hairs.cv.ts[v] = c_hairs.basis_inv*c_hairs.cv.os[v];
			if( v==0) ofs = -c_hairs.cv.ts[v];
			c_hairs.cv.ts[v] += ofs;
		}
	}

	if(bClump)
	{
		c_hairs.toclump = Math::Vec3f(0);
		c_hairs.toclumpUV = Math::Vec2f(0);
		c_hairs.clumpIndex = -1;
	}
	else
	{
		// ����������� �� �����
		c_hairs.clumpIndex = surface->getClumpIndexAtHair(vert);
		if( c_hairs.clumpIndex>=0)
		{
			Math::Vec3f clump_position;
			surface->getPosAtHair( c_hairs.clumpIndex, true, clump_position);
			Math::Vec3f toclump = c_hairs.position-clump_position;
			c_hairs.toclump = toclump;

			Math::Matrix4As3f basis_inv3f(c_hairs.basis_inv);
			Math::Vec3f toclumpUV = basis_inv3f*toclump;
// ???�����???
			c_hairs.toclumpUV = tangentSpace2_2d( toclumpUV);//Math::Vec2f(-toclumpUV.z, toclumpUV.x);
		}
	}

//	if( bUseAddUV)
//	{
		surface->getUVSetAtHair(vert, bClump, c_hairs.uvSets);
//	}

	// ������ ������ ��� ������
	this->ParseHair(c_hairs, U, V, bClump);

	if( !c_hairs.cv.empty())
	{
		float l = c_hairs.cv.getLength();
		if(l<c_hairs.length) 
			c_hairs.length = l;
	}

	/*/
	#ifdef SAS
	#ifndef CHEVELUREDEMO
		if(!licenseflag)
		{
			int group = surface->getPolygonIndex(vert, bClump);
			srand(group);
			int test = rand();
			float aaa = (test%32)/32.f;
			c_hairs.length *= aaa;
		}
	#endif
	#endif

	#ifndef SAS
		if( UlHair_licflag!=1)
		{
			c_hairs.length = 0.1f*rand()/(float)RAND_MAX;
		}
	#endif
	/*/

	c_hairs.position += c_hairs.displace*c_hairs.normal;

	// ��� �������:
//c_hairs.blendCVfactor = c_hairs.scraggleFreq;
//c_hairs.blendDeformfactor = 1-c_hairs.scraggleFreq;

}

// ������� ��������� ������ �� ����� �� �����������
void HairProcessor::ParseHair(
	HAIRPARAMS& c_hairs, 
	int faceindex, 
	const Math::Vec3f& bary,
	bool bClump
	)
{
	c_hairs.uvSets.clear();
	Math::Vec2f uv = surface->geometry->getUV(
		faceindex, bary, surface->geometryIndices);
	c_hairs.position = surface->geometry->getPOS(
		faceindex, bary, surface->geometryIndices);
	surface->geometry->getBasis(
		faceindex, bary, surface->geometryIndices, c_hairs.normal, c_hairs.tangentU, c_hairs.tangentV);

	float U = c_hairs.u = uv.x;
	float V = c_hairs.v = uv.y;
	
	surface->geometry->getUVsets(
		faceindex, bary, surface->geometryIndices, c_hairs.uvSets);

	ParseHair(c_hairs, U, V, bClump);
}

// ������� ��������� ������ �� U,V
void HairProcessor::ParseHair(
	HAIRPARAMS& c_hairs, 
	float U, float V,
	bool bClump, 
	bool bIgnoreJitters
	)
{
	Math::Vec2f luv;
	surface->getModifiersAtPoint(c_hairs.position, this->modifiers);

	// displace
	luv = c_hairs.uv(U, V, surface->displace.uvsetnumber);
	c_hairs.displace = surface->getDisplace(luv.x, luv.y);

	// length
	c_hairs.length = surface->getLength(U, V, c_hairs.uvSets, deformers_INCREASELENGTH);
	c_hairs.lengthjitter = surface->getLengthJitter();
	if( bClump) 
		c_hairs.lengthjitter = 1;
	if( bIgnoreJitters)
		c_hairs.lengthjitter = 1;

	// ��� ������������� � �������� �����:
	// ��� ���������� ������������ � ����������� -tangentV
	// ������� ������������ ���� �� -PI/2
	luv = c_hairs.uv(U, V, surface->polar.uvsetnumber);
	c_hairs.polar = surface->getPolar(luv.x, luv.y, bIgnoreJitters);

	// ���������� polar �����������
	for(int i=0; i<(int)deformers_PREPARSEHAIR.size(); i++)
	{
		IHairDeformer* deformer = deformers_PREPARSEHAIR[i];
		float affect = deformer->affect.getValue(U, V);
		deformer->PreParseHair(affect, c_hairs, bClump);
	}
	ApplyModifiersPolar(this->modifiers, c_hairs, bClump);
	Math::Vec2f v = polar2vector(c_hairs.polar);

	//inclination
	luv = c_hairs.uv(U, V, surface->inclination.uvsetnumber);
	c_hairs.inclination = v*surface->getInclination(luv.x, luv.y, bIgnoreJitters);

	//baseCurl
	luv = c_hairs.uv(U, V, surface->baseCurl.uvsetnumber);
	c_hairs.baseCurl	= v*(surface->getCurl(luv.x, luv.y, 0, bIgnoreJitters)-0.5f)*(float)M_PI;

	//tipCurl
	luv = c_hairs.uv(U, V, surface->tipCurl.uvsetnumber);
	c_hairs.tipCurl		= v*(surface->getCurl(luv.x, luv.y, 1, bIgnoreJitters)-0.5f)*(float)M_PI;

	//scraggle
	luv = c_hairs.uv(U, V, surface->scragle.uvsetnumber);
	c_hairs.scraggle	= surface->getScraggle(luv.x, luv.y);
	//scraggleFreq
	luv = c_hairs.uv(U, V, surface->scragleFreq.uvsetnumber);
	c_hairs.scraggleFreq= surface->getScraggleFreq(luv.x, luv.y);

	// BaseClumpValue
	luv = c_hairs.uv(U, V, surface->baseClump.uvsetnumber);
	c_hairs.BaseClumpValue = surface->getClump(luv.x, luv.y, 0);
	// TipClumpValue
	luv = c_hairs.uv(U, V, surface->tipClump.uvsetnumber);
	c_hairs.TipClumpValue = surface->getClump(luv.x, luv.y, 1);

	// twist
	luv = c_hairs.uv(U, V, surface->twist.uvsetnumber);
	c_hairs.twist		= surface->getTwist(luv.x, luv.y, bIgnoreJitters);
//	c_hairs.twist *= lendifffactor;

	// twistFromPolar
	luv = c_hairs.uv(U, V, surface->twistFromPolar.uvsetnumber);
	if( bIgnoreJitters)
		c_hairs.twistFromPolar = surface->twistFromPolar.getValueSource(luv.x, luv.y);
	else
		c_hairs.twistFromPolar = surface->twistFromPolar.getValue(luv.x, luv.y);

	// blendCVfactor blendDeformfactor
	c_hairs.blendCVfactor = surface->blendCVfactor.getValue(U, V);
	c_hairs.blendDeformfactor = surface->blendDeformfactor.getValue(U, V);

	// baseWidth
	luv = c_hairs.uv(U, V, surface->baseWidth.uvsetnumber);
	c_hairs.baseWidth	= surface->getWidth(luv.x, luv.y, 0);

	// tipWidth
	luv = c_hairs.uv(U, V, surface->tipWidth.uvsetnumber);
	c_hairs.tipWidth	= surface->getWidth(luv.x, luv.y, 1);

	switch(orthogonalize)
	{
	case ORT_TANGENTU:
		c_hairs.orthogonalize(true);
		break;
	case ORT_TANGENTV:
		c_hairs.orthogonalize(false);
		break;
	}
	for(int i=0; i<(int)deformers_PARSEHAIR.size(); i++)
	{
		IHairDeformer* deformer = deformers_PARSEHAIR[i];
		float affect = deformer->affect.getValue(U, V);
		deformer->ParseHair(affect, c_hairs, bClump);
	}

	ApplyModifiers(this->modifiers, c_hairs, bClump);
}


/*/
// ��������� ����� 
bool HairProcessor::BuildStandAloneHair(
	const HAIRPARAMS& params, 
	HAIRVERTS& hair, 
	bool bClump
	)
{
	srand( params.seed);

	// ������� BuildHair � ����������
	for(int i=0; i<(int)deformers_BUILDHAIR.size(); i++)
	{
		IHairDeformer* deformer = deformers_BUILDHAIR[i];
		float affect = deformer->affect.getValue(params.u, params.v);
		if( deformer->BuildHair(affect, params, SegmCount, hair))
		{
			return !hair.cvint.empty();
		}
	}
	for(unsigned x=0; x<modifiers.affectmodifiers.size(); x++)
	{
		HairMesh::ModifierData& modifier = modifiers.affectmodifiers[x];
		if( modifier.modifier->CallBuildHair(modifier.w, modifier.dirToModifier, params, SegmCount, hair))
			return !hair.cvint.empty();
	}

	Math::Matrix4f basis = buildBasis4f( params.position, params.normal, params.tangentU, params.tangentV);
	float scalelen = params.length * params.lengthjitter;
	if(scalelen<min_hairLen) scalelen=min_hairLen;
	basis.scale( Math::Vec4f( scalelen, scalelen, scalelen, 1.f));

	Math::Vec3f point(0, 0, 0);
	Math::Vec3f normal(0, 1, 0);
	Math::Vec3f tangentU(1, 0, 0);
	Math::Vec3f tangentV(0, 0, 1);

	Math::Matrix4f maintransform = buildBasis4f(Math::Vec3f(0), normal, tangentU, tangentV);

	float polar		= params.polar;
	Math::Vec2f inclination = params.inclination;
	Math::Vec2f basecurl	= params.baseCurl*params.lengthjitter;
	Math::Vec2f tipcurl		= params.tipCurl*params.lengthjitter;
	float scraggle	= params.scraggle*scalelen;
	float baseWidth	= params.baseWidth;
	float tipWidth	= params.tipWidth;
	if( mulWidthByLength)
	{
		baseWidth *= scalelen;
		tipWidth *= scalelen;
	}

	float twist		= params.twist;
	twist = ((float)M_PI)*2*(twist-0.5f);

	// ����� �� ������
	if( params.twistFromPolar!=0)
	{
		float polardirive = surface->getPolarDeriv(params.u, params.v, polar);
		polardirive *= 2/(params.dPdU.length()+params.dPdV.length());
		polardirive *= params.twistFromPolar;
		polardirive *= 1/(scalelen);
		twist += ((float)M_PI)*2*polardirive;
	}

	float baseCLUMP = params.BaseClumpValue;// * this->BaseClumpValue;
	float tipCLUMP  = params.TipClumpValue;// * this->TipClumpValue;

	int tolerance   = SegmCount+1;

	double theta = M_PI * (2 * polar);
	Math::Rot3f PolarRot( (float)theta, normal );
	Math::Matrix4f obj = Math::Matrix4f::id;
	PolarRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());
	tangentU = obj*tangentU;
	normal   = obj*normal;
	tangentV = obj*tangentV;

	// orto tangentV tangentU
	{
		tangentV = Math::cross( tangentU, normal);
		tangentV.normalize();
	}

	// inclination
	{
		float dzeta = inclination.length()*(float)M_PI*0.5f;
		if(dzeta>0.0001)
		{
			// ������� ������������ ��� ���������������� inclination
			Math::Vec3f axis = maintransform*Math::Vec3f(-inclination.y, 0, inclination.x);
//			double dzeta = M_PI * inclination * 0.5;
			Math::Rot3f IncRot( dzeta, axis );
			Math::Matrix4f obj = Math::Matrix4f::id;
			IncRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());
			tangentU = obj*tangentU;
			normal   = obj*normal;
			tangentV = obj*tangentV;
		}
	}

	hair.cvint.resize(SegmCount+1);
	hair.vfactor.resize(SegmCount+1);
	hair.vwidht.resize(SegmCount+1);
	hair.vclump.resize(SegmCount+1);
	hair.basas.resize(SegmCount+1);

	float segLength = 1/((float)tolerance-1);
	for (int _k = 0;_k < tolerance; _k++)
	{
		float sl = segLength;
		float param = (float)_k/((float)tolerance-1);
		float cf = baseCLUMP*(1-param) + param*tipCLUMP;

		float w = (1-param)*baseWidth + param*tipWidth;
		// ��� ���� ����� �� ���������� � 0
		if( w<1e-3f) w = 1e-3f;

		Math::Matrix4f transform;
		transform[0] = Math::Vec4f(tangentU, 0)*w;
		transform[1] = Math::Vec4f(normal, 0);
		transform[2] = Math::Vec4f(tangentV, 0)*w;
		transform[3] = Math::Vec4f(point, 1);

		hair.cvint[_k] = transform*Math::Vec3f(0, 0, 0);
		hair.cvint[_k] = basis*hair.cvint[_k];
		hair.vfactor[_k] = param;
		hair.vwidht[_k] = w;
		hair.basas[_k] = transform;
		hair.vclump[_k] = cf;

		// curl
		Math::Vec2f curl = (1-param)*basecurl + param*tipcurl;
		curl = (curl)/((float)tolerance-1);
		float curlangle = curl.length();
		if(curlangle>0.000001)
		{
			if( _k==0) 
				curlangle*=0.5f; 
			// ������� ������������ ��� ���������������� curl
			Math::Vec3f axis = maintransform*Math::Vec3f(-curl.y, 0, curl.x);
			Math::Rot3f CurlRot( curlangle, axis );
			Math::Matrix4f obj = Math::Matrix4f::id;
			CurlRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());
			tangentU = obj*tangentU;
			normal   = obj*normal;
			tangentV = obj*tangentV;
			sl = 2*sin(curlangle*0.5f)*segLength/curlangle;
		}

		// twist
		float twistAngle = twist/((float)tolerance-1);
		Math::Rot3f TwistRot( (float)(twistAngle), Math::Vec3f(0, 1, 0) );
		TwistRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());
		tangentU = obj*tangentU;
		normal   = obj*normal;
		tangentV = obj*tangentV;

		// scragle
		if (scraggle!= 0)
		{
			Math::Vec3f RandVec;
			RandVec = randomAxis();

			point.x += sl * scraggle * RandVec.x;
			point.y += sl * scraggle * RandVec.y;
			point.z += sl * scraggle * RandVec.z;
		}

		point += normal*sl;
	}
	// ������� PostBuildHair � ����������
	for(int i=0; i<(int)deformers_POSTBUILDHAIR.size(); i++)
	{
		IHairDeformer* deformer = deformers_POSTBUILDHAIR[i];
		float affect = deformer->affect.getValue(params.u, params.v);
		deformer->PostBuildHair(affect, params, SegmCount, hair);
	}
	for(unsigned x=0; x<modifiers.affectmodifiers.size(); x++)
	{
		HairMesh::ModifierData& modifier = modifiers.affectmodifiers[x];
		modifier.modifier->CallPostBuildHair(modifier.w, modifier.dirToModifier, params, SegmCount, hair);
	}

	return !hair.cvint.empty();
}
/*/

// ��������� ����� � �����
bool HairProcessor::BuildHairInClump(
	const HAIRPARAMS& clumphairparams,	// ����� �� ������ �����
	const HAIRVERTS& clumphair,		
	const HAIRPARAMS& params,			// StandAlone �����
	const HAIRVERTS& orighair,				
	HAIRVERTS& hair							// ���������
	)
{
	srand( params.seed);

	// ������� MergeWithClump � ����������
	for(int i=0; i<(int)deformers_MERGEWITHCLUMP.size(); i++)
	{
		IHairDeformer* deformer = deformers_MERGEWITHCLUMP[i];
		float affect = deformer->affect.getValue(params.u, params.v);
		if( deformer->MergeWithClump(affect, params, orighair, clumphairparams, clumphair, hair))
		{
			return !hair.cvint.empty();
		}
	}
	for(unsigned x=0; x<modifiers.affectmodifiers.size(); x++)
	{
		HairMesh::ModifierData& modifier = modifiers.affectmodifiers[x];
		if( modifier.modifier->CallMergeWithClump(modifier.w, modifier.dirToModifier, params, orighair, clumphairparams, clumphair, hair))
			return !hair.cvint.empty();
	}

	Math::Matrix4f basis = buildBasis4f(params.position, params.normal, params.tangentU, params.tangentV);
	float scalelen = params.length*params.lengthjitter;
	if(scalelen<min_hairLen) scalelen=min_hairLen;
	basis.scale(Math::Vec4f(scalelen,scalelen,scalelen,1));
	Math::Matrix4f basis_inv = basis.inverted();

	// �����������
	hair.cvint.clear();
	hair.vfactor.clear();
	hair.vwidht.clear();
	hair.vclump.clear();
	hair.basas.clear();
	hair.pt_nonoise.clear();
	hair.cvint.reserve(orighair.cvint.size());
	hair.vfactor.reserve(orighair.cvint.size());
	hair.vwidht.reserve(orighair.cvint.size());
	hair.vclump.reserve(orighair.cvint.size());
	hair.basas.reserve(orighair.cvint.size());
	hair.pt_nonoise.reserve(orighair.cvint.size());

	int csize = (int)orighair.cvint.size();
	hair.vclump.reserve(csize);
	for(int v=0; v<csize; v++)
	{
		float f = orighair.vfactor[v];
		float w = orighair.vwidht[v];
		float cf = orighair.vclump[v];
		if( v>=(int)clumphair.cvint.size())
			continue;
		Math::Vec3f pt_nonoise = orighair.pt_nonoise[v];

		// �����
		Math::Vec3f pt = (cf)*clumphair.cvint[v] + (1-cf)*orighair.cvint[v];

		hair.cvint.push_back(pt);
		hair.vfactor.push_back(f);
		hair.vwidht.push_back(w);
		hair.vclump.push_back(cf);
		hair.pt_nonoise.push_back(pt_nonoise);
	}

	{
		// ������ ����������� � ������ ����� ��������
		float rest = 0;
		int clumpVert = 0;

		std::vector<Math::Vec3f> newcvint;
		newcvint.reserve(csize);
		newcvint.push_back(hair.cvint[0]);
		for(int v=1; v<csize; v++)
		{
			Math::Vec3f orig = orighair.cvint[v]-orighair.cvint[v-1];
			float origLen = orig.length();
			Math::Vec3f pt;
			if(_isnan(origLen))
			{
				fprintf(stderr, "origLen = NAN v=%d origLen=%f rest=%f\n", v, origLen, rest);
				break;
			}

			if(origLen>=rest)
				clumpVert++;
			else
			{
				if( clumpVert<1 || clumpVert>=(int)hair.cvint.size()) 
					fprintf(stderr, "clumpVert error %d size=%d v=%d origLen=%f rest=%f\n", clumpVert, hair.cvint.size(), v, origLen, rest);
				Math::Vec3f cur = hair.cvint[clumpVert]-hair.cvint[clumpVert-1];
				float curLen = cur.length();
				float len = rest-origLen;

				// ����� ����� �����
				float f = 1 - (len)/curLen;
				pt = (1-f)*hair.cvint[clumpVert-1] + f*hair.cvint[clumpVert];
				rest = len;
				newcvint.push_back(pt);
				continue;
			}

			for( ; clumpVert<csize; )
			{
				Math::Vec3f cur = hair.cvint[clumpVert]-hair.cvint[clumpVert-1];
				float curLen = cur.length();
				float len = rest+curLen;
				if(len>origLen)
				{
					// ����� ����� �����
					float f = 1 - (len-origLen)/curLen;
					pt = (1-f)*hair.cvint[clumpVert-1] + f*hair.cvint[clumpVert];
					rest = len-origLen;
					newcvint.push_back(pt);
					break;
				}
				rest = len;
				clumpVert++;
			}
			if( clumpVert==csize)
			{
				// ��������� ����� �� ����������� ����������
				Math::Vec3f cur = hair.cvint[csize-1]-hair.cvint[csize-2];
				cur.normalize();
				pt = hair.cvint[csize-1] + cur*(origLen-rest);
				newcvint.push_back(pt);
				for(v++; v<csize; v++)
				{
					Math::Vec3f orig = orighair.cvint[v]-orighair.cvint[v-1];
					float origLen = orig.length();

					pt = newcvint.back() + cur*origLen;
					newcvint.push_back(pt);
				}
				break;
			}
		}
		hair.cvint = newcvint;
		// ��������
		/*/
		{
			float l0=0, l1=0;
			for(int v=1; v<csize; v++)
			{
				l0 += (orighair.cvint[v]-orighair.cvint[v-1]).length();
				l1 += (hair.cvint[v]-hair.cvint[v-1]).length();
			}
			if( fabs(l0-l1)>0.01 )
			{
				printf("l0=%f l1=%f\n", l0, l1);
			}
		}
		/*/
	}
	// ��������� ������
	for(int v=0; v<csize; v++)
	{
		Math::Matrix4f m = orighair.basas[v];
		Math::Vec3f pt0 = basis_inv*hair.cvint[v];
		m[3] = Math::Vec4f(pt0, 1);
		hair.basas.push_back(m);
	}
	return !hair.cvint.empty();
}

/*/
// ��������� ����� �� ����������� �������
bool HairProcessor::BuildHairByCV(
	const HAIRPARAMS& params, 
	HAIRVERTS& hair,
	bool bClump			// ���� ������� �����
	)
{
	srand( params.seed);

	std::vector< HairCVPositions::WEIGHT> weights;
	if( !surface->cvPositions->getWeidths(
			params.u, params.v, 
			*surface->geometryIndices, weights, 
			surface->hairSystem->CVtailU, surface->hairSystem->CVtailV))
		return false;

	float baseWidth	= params.baseWidth;
	float tipWidth	= params.tipWidth;
	if( mulWidthByLength)
	{
		baseWidth *= params.length * params.lengthjitter;
		tipWidth  *= params.length * params.lengthjitter;
	}

	float Scraggle	= params.scraggle;
	float segm = (float)params.length*params.lengthjitter/((float)(SegmCount));

	float baseCLUMP = params.BaseClumpValue;// * this->BaseClumpValue;
	float tipCLUMP  = params.TipClumpValue;// * this->TipClumpValue;

	Math::Matrix4f basis, basis_inv;
	params.getBasis(basis);
	basis_inv = basis.inverted();

	{
		Math::Matrix4f basis = buildBasis4f(params.position, params.normal, params.tangentU, params.tangentV);
		float scalelen = params.length*params.lengthjitter;
		if(scalelen<min_hairLen) scalelen=min_hairLen;
		basis.scale(Math::Vec4f(scalelen, scalelen, scalelen, 1));
		basis_inv = basis.inverted();
	}

	hair.cvint.clear();
	hair.vfactor.clear();
	hair.vwidht.clear();
	hair.vclump.clear();
	hair.basas.clear();
	hair.cvint.reserve(SegmCount+1);
	hair.vfactor.reserve(SegmCount+1);
	hair.vwidht.reserve(SegmCount+1);
	hair.vclump.reserve(SegmCount+1);
	hair.basas.reserve(SegmCount+1);

	Math::Vec3f point(0, 0, 0);
	Math::Vec3f normal(0, 1, 0);
	Math::Vec3f tangentU(1, 0, 0);
	Math::Vec3f tangentV(0, 0, 1);

//fprintf(stderr, ">Hair %d\n", params.seed);

	std::vector< Math::Vec3f> lastOsPt(weights.size());
	srand( params.seed);
	Math::Vec3f ptLast(0, 0, 0);
	for(int v=0; v<SegmCount+1; v++)
	{
		float f = v/(float)SegmCount;
		float lf = params.length*params.lengthjitter*f;
		float widht = (1-f)*baseWidth + f*tipWidth;
		float cf = baseCLUMP*(1-f) + f*tipCLUMP;

		float tsFactor = 0;

		Math::Vec3f ptTs(0, 0, 0);
		Math::Vec3f ptOs(0, 0, 0);
		Math::Vec3f deltaTs(0, 0, 0);
		Math::Vec3f deltaOs(0, 0, 0);

		// ������������ � OS
		// ������������ � TS
		for(unsigned wi=0; wi<weights.size(); wi++)
		{
			HairCVPositions::WEIGHT& w = weights[wi];
			HairCV::CV& curve = surface->cv->cvs[w.cvindex];

			Math::Vec3f wpt = curve.getPointByParam(lf, true);
			ptTs = ptTs + wpt*w.w;

			Math::Vec3f wopt = curve.getPointByParam(lf, false);
			if(v!=0)
			{
				Math::Vec3f d = (wopt-lastOsPt[wi]);
				if( d.length2()>0.0001)
					deltaOs += d*w.w;
			}
			lastOsPt[wi] = wopt;
		}
		// ���������
		Math::Vec3f pt;
		if(v==0)
		{
			pt = basis*ptTs;
		}
		else
		{
			if( deltaOs.length2()<0.001)
				tsFactor = 1;
//tsFactor = 1;
			// ���������
			deltaTs = basis*ptTs - ptLast;
			Math::Vec3f delta = (deltaTs)*tsFactor + (1-tsFactor)*deltaOs;
			pt = delta.normalize()*segm;
			pt += ptLast;
//fprintf(stderr, "+{%f, %f, %f}*%f\n", delta.x, delta.y, delta.z, segm);
		}
//fprintf(stderr, "%d: {%f, %f, %f}\n", v, pt.x, pt.y, pt.z);
		Math::Vec3f ptScraggle = pt;
		if (Scraggle!= 0)
		{
			Math::Vec3f RandVec;
			RandVec = randomAxis();
			ptScraggle += segm*RandVec*Scraggle;
		}


		// matrix
		{
			// ��������� � ts
			Math::Vec3f ptos = basis_inv*ptScraggle;
			if(v!=0)
			{
				Math::Vec3f newnormal;
				newnormal = (ptos-point).normalized();
				Math::Rot3f rot(normal, newnormal);
				Math::Matrix3f rm;
				rot.getBasis(rm[0].data(), rm[1].data(), rm[2].data());
				normal   = rm*normal;
				tangentU = rm*tangentU;
				tangentV = rm*tangentV;
			}
			point = ptos;

			float w = (1-f)*baseWidth + f*tipWidth;
			// ��� ���� ����� �� ���������� � 0
			if( w<1e-3f) w = 1e-3f;

			Math::Matrix4f matr = buildBasis4f(ptos, normal, tangentU*w, tangentV*w);
//			matr[0] = Math::Vec4f(tangentU*w, 0);
//			matr[1] = Math::Vec4f(normal,   0);
//			matr[2] = Math::Vec4f(tangentV*w, 0);
//			matr[3] = Math::Vec4f(ptos.x, ptos.y, ptos.z, 1);
			hair.basas.push_back(matr);
		}

		ptLast = pt;

		hair.cvint.push_back(ptScraggle);
		hair.vfactor.push_back(f);
		hair.vwidht.push_back(widht);
		hair.vclump.push_back(cf);
	}

	// ������� PostBuildHair � ����������
	for(int i=0; i<(int)deformers_POSTBUILDHAIR.size(); i++)
	{
		IHairDeformer* deformer = deformers_POSTBUILDHAIR[i];
		float affect = deformer->affect.getValue(params.u, params.v);
		deformer->PostBuildHair(affect, params, SegmCount, hair);
	}

	return hair.vc()!=0;

}
/*/

// ��������� ��������� �� ���������
void HairProcessor::CreateHairDefault(
//	int index, 
	HAIRPARAMS& hairparam, 
	HAIRVERTS& hair,	// �����
	bool bClump			// ���� ������� �����
	)
{
	// �������� ���� hair.basas
	Math::Vec3f point(0, 0, 0);
	Math::Vec3f normal(0, 1, 0);
	Math::Vec3f tangentU(1, 0, 0);
	Math::Vec3f tangentV(0, 0, 1);

	Math::Matrix4f maintransform = buildBasis4f(Math::Vec3f(0), normal, tangentU, tangentV);

	hair.basas.resize(SegmCount+1);
	float segLength = 1/((float)SegmCount);
	for( int k = 0; k < SegmCount+1; k++)
	{
		Math::Matrix4f transform;
		transform[0] = Math::Vec4f(tangentU, 0);
		transform[1] = Math::Vec4f(normal, 0);
		transform[2] = Math::Vec4f(tangentV, 0);
		transform[3] = Math::Vec4f(point, 1);
		hair.basas[k] = transform;

		point += Math::Vec3f(0, segLength, 0);
	}
}
// ��������� ��������� �� CV
bool HairProcessor::CreateHairFromCV(
//	int index, 
	HAIRPARAMS& hairparam, 
	HAIRVERTS& hair,		// �����
	bool bClump				// ���� ������� �����
	)
{
	float cvBlendFactor = hairparam.blendCVfactor;

	// ������ hairparam.cv
	if( hairparam.cv.empty())
	{
		CreateHairDefault(
//			index,
			hairparam, 
			hair,	// �����
			bClump			// ���� ������� �����
			);
		return true;
	}

	int k;
	std::vector<Math::Vec3f> op;

	// ������
	float len = hairparam.length;
	op.resize(SegmCount+1);
	for( k = 0; k < SegmCount+1; k++)
	{
		float f = len*(float)k/((float)SegmCount);

		Math::Vec3f v;
		v = hairparam.cv.getPointByParam(f*hairparam.lengthjitter, true, true);
		op[k] = v;
	}

	// ��������� �������
	hair.basas.resize(SegmCount+1);
	for( k = 0; k < SegmCount+1; k++)
	{
		// �������
		Math::Matrix4f m = Math::Matrix4f::id;
		if(k!=0)
		{
			Math::Vec3f v0(0, 1, 0);
			Math::Vec3f v1 = (op[k]-op[k-1]).normalized();
			v1 = v0*(1-cvBlendFactor) + v1*cvBlendFactor;
			Math::Rot3f rot(v0, v1);
			rot.getMatrix(m);
		}
		m[3] = Math::Vec4f(op[k], 1);
		hair.basas[k] = m;
	}
	return true;
}

// ����������� HAIRVERTS �� HAIRPARAMS
// ��� ����� �������
// ����� ������ ��������� �� ��������� ��������� ������
bool HairProcessor::DeformAloneHair(
	const HAIRPARAMS& params, 
	HAIRVERTS& hair,		// �� ����� - �������� �����
	bool bClump			// ���� ������� �����
	)
{
	int SegmCount = (int)hair.basas.size()-1;

	srand( params.seed);

	// ������� BuildHair � ����������
	for(int i=0; i<(int)deformers_BUILDHAIR.size(); i++)
	{
		IHairDeformer* deformer = deformers_BUILDHAIR[i];
		float affect = deformer->affect.getValue(params.u, params.v);
		if( deformer->BuildHair(affect, params, SegmCount, hair))
		{
			return !hair.cvint.empty();
		}
	}
	for(unsigned x=0; x<modifiers.affectmodifiers.size(); x++)
	{
		HairMesh::ModifierData& modifier = modifiers.affectmodifiers[x];
		if( modifier.modifier->CallBuildHair(modifier.w, modifier.dirToModifier, params, SegmCount, hair))
			return !hair.cvint.empty();
	}

	// ����� ������
	Math::Matrix4f basis = buildBasis4f( params.position, params.normal, params.tangentU, params.tangentV);

	float scalelen = params.length*params.lengthjitter;
	if(scalelen<min_hairLen) scalelen=min_hairLen;
	basis.scale( Math::Vec4f( scalelen, scalelen, scalelen, 1.f));


	// ����������� ���������
	float polar	= params.polar;
	Math::Vec2f inclination = params.inclination;
	Math::Vec2f basecurl	= params.baseCurl*params.lengthjitter;
	Math::Vec2f tipcurl		= params.tipCurl*params.lengthjitter;
	float scraggle	= params.scraggle*scalelen;
	float baseWidth	= params.baseWidth;
	float tipWidth	= params.tipWidth;
	if( mulWidthByLength)
	{
		baseWidth *= scalelen;
		tipWidth *= scalelen;
	}
	float twist		= params.twist;
	twist = ((float)M_PI)*2*(twist-0.5f);
	// ����� �� ������
	if( params.twistFromPolar!=0)
	{
		float polardirive = surface->getPolarDeriv(params.u, params.v, polar);
		polardirive *= 2/(params.dPdU.length()+params.dPdV.length());
		polardirive *= params.twistFromPolar;
		polardirive *= 1/(scalelen);
		twist += ((float)M_PI)*2*polardirive;
	}
	float baseCLUMP = params.BaseClumpValue;// * this->BaseClumpValue;
	float tipCLUMP  = params.TipClumpValue;// * this->TipClumpValue;

	// ������� �����
	Math::Matrix4f maintransform = buildBasis4f(
		Math::Vec3f(0), 
		Math::Vec3f(0, 1, 0), 
		Math::Vec3f(1, 0, 0), 
		Math::Vec3f(0, 0, 1));


	Math::Vec3f point(0, 0, 0);
	Math::Vec3f point_nonoise(0, 0, 0);
	Math::Vec3f normal(0, 1, 0);
	Math::Vec3f tangentU(1, 0, 0);
	Math::Vec3f tangentV(0, 0, 1);

	float cvBlendFactor = params.blendDeformfactor;

	// ������ ������� = inclination*polar
	Math::Matrix4f obj = Math::Matrix4f::id;
	Math::Matrix4f m0;
	{
		// POLAR
		float theta = (float)( M_PI * (2 * polar));
		Math::Rot3f PolarRot( (float)theta, normal );
		PolarRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());
		m0 = obj;

		// INCLINATION
		float dzeta = inclination.length()*(float)M_PI*0.5f;
		if(dzeta>0.0001)
		{
			// ������� ������������ ��� ���������������� inclination
			Math::Vec3f axis = maintransform*Math::Vec3f(-inclination.y, 0, inclination.x);
			Math::Rot3f IncRot( dzeta*cvBlendFactor, axis );
			Math::Matrix4f obj = Math::Matrix4f::id;
			IncRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());

			m0 = m0*obj;
		}
	}
	Math::Matrix4As3f m0_3(m0);
	tangentU = m0_3*tangentU;
	normal   = m0_3*normal;
	tangentV = m0_3*tangentV;

	// orto tangentV tangentU
	{
		tangentV = Math::cross( tangentU, normal);
		tangentV.normalize();
	}

	int tolerance   = SegmCount+1;

	hair.cvint.resize(SegmCount+1);
	hair.vfactor.resize(SegmCount+1);
	hair.vwidht.resize(SegmCount+1);
	hair.vclump.resize(SegmCount+1);
	hair.pt_nonoise.resize(SegmCount+1);
//	hair.basas.resize(SegmCount+1);

	float segLength = 1/((float)SegmCount);
	for (int _k = 0;_k < tolerance; _k++)
	{
		float sl = segLength;
		float param = (float)_k/((float)tolerance-1);
		float cf = baseCLUMP*(1-param) + param*tipCLUMP;

		float w = (1-param)*baseWidth + param*tipWidth;
		// ��� ���� ����� �� ���������� � 0
		if( w<1e-3f) w = 1e-3f;

		// ������� �������������:
		Math::Matrix4f mi = Math::Matrix4f::id;
		if(_k!=SegmCount)
		{
			Math::Matrix4f mi0 = hair.basas[_k];
			Math::Matrix4f mi1 = hair.basas[_k+1];
			mi = mi0.inverted()*mi1;
 		}

		// �������
		Math::Matrix4f transform;
		transform[0] = Math::Vec4f(tangentU, 0)*w;
		transform[1] = Math::Vec4f(normal, 0);
		transform[2] = Math::Vec4f(tangentV, 0)*w;
		transform[3] = Math::Vec4f(point, 1);

		hair.cvint[_k] = transform*Math::Vec3f(0, 0, 0);
		hair.cvint[_k] = basis*hair.cvint[_k];
		hair.vfactor[_k] = param;
		hair.vwidht[_k] = w;
		hair.basas[_k] = transform;
		hair.vclump[_k] = cf;
		hair.pt_nonoise[_k] = basis*point_nonoise;

//		if(_k==SegmCount)
//			break;

		// curl
		{
			Math::Vec2f curl = (1-param)*basecurl + param*tipcurl;
			curl = (curl)/((float)tolerance-1);
			float curlangle = curl.length();
			if(curlangle>0.000001)
			{
				if( _k==0) 
					curlangle*=0.5f; 
				// ������� ������������ ��� ���������������� curl
				Math::Vec3f axis = maintransform*Math::Vec3f(-curl.y, 0, curl.x);
				Math::Rot3f CurlRot( curlangle*cvBlendFactor, axis );
				Math::Matrix4f obj = Math::Matrix4f::id;
				CurlRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());
				mi = obj*mi;
				// ����������� �����!!!
				sl = 2*sin(curlangle*0.5f)*sl/curlangle;
			}
		}

		// twist
		{
			float twistAngle = twist/((float)tolerance-1);
			Math::Rot3f TwistRot( (float)(twistAngle)*cvBlendFactor, Math::Vec3f(0, 1, 0) );
			TwistRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());
			mi = obj*mi;
		}

		// deformers
		for(int i=0; i<(int)deformers_DEFORMHAIRVERTEX.size(); i++)
		{
			IHairDeformer* deformer = deformers_DEFORMHAIRVERTEX[i];
			float affect = deformer->affect.getValue(params.u, params.v);
			affect = affect*cvBlendFactor;
			deformer->deformHairVertex(affect, params, _k, SegmCount, mi, point);
		}
		

		// scragle
		if (scraggle!= 0)
		{
			Math::Vec3f RandVec;
			RandVec = randomAxis();

			point += tangentU * sl * scraggle * RandVec.x;
			point += tangentV * sl * scraggle * RandVec.z;
//			point.x += sl * scraggle * RandVec.x;
//			point.y += sl * scraggle * RandVec.y;
//			point.z += sl * scraggle * RandVec.z;
		}

		Math::Matrix4As3f mi_3(mi);
		tangentU = mi_3*tangentU;
		normal   = mi_3*normal;
		tangentV = mi_3*tangentV;

		point += normal*sl;
		point_nonoise += normal*sl;
	}
	/*/
	{
		Math::Vec3f lastpt = hair.cvint[tolerance-1];
		Math::Vec3f normalX = Math::cross(params.dPdU, params.dPdV);
		Math::Matrix4f basisX = buildBasis4f( params.position, normalX, params.dPdU, params.dPdV);
		Math::Matrix4f basisX_inv = basisX.inverted();
		Math::Vec3f uvX = basisX_inv*lastpt;

		Math::Vec2f uv = tangentSpace2_2d(uvX);
		uv.x += params.u;
		uv.y += params.v;
		if(uv.x>0&&uv.x<1&&uv.y>0&&uv.y<1)
		{
			float polartip = surface->getPolar(uv.x, uv.y);

			double theta = M_PI * (2 * (polartip-polar))*params.scraggleFreq;
			for (int _k = 0;_k < tolerance; _k++)
			{
				float param = (float)_k/((float)tolerance-1);
				Math::Rot3f PolarRot( (float)param*theta/(tolerance-1), params.normal );
				Math::Matrix4f obj = Math::Matrix4f::id;
				PolarRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());

				hair.cvint[_k] = obj*hair.cvint[_k];
			}
		}
	}
	//*/
	// ������� PostBuildHair � ����������
	for(int i=0; i<(int)deformers_POSTBUILDHAIR.size(); i++)
	{
		IHairDeformer* deformer = deformers_POSTBUILDHAIR[i];
		float affect = deformer->affect.getValue(params.u, params.v);
		deformer->PostBuildHair(affect, params, SegmCount, hair);
	}
	for(unsigned x=0; x<modifiers.affectmodifiers.size(); x++)
	{
		HairMesh::ModifierData& modifier = modifiers.affectmodifiers[x];
		modifier.modifier->CallPostBuildHair(modifier.w, modifier.dirToModifier, params, SegmCount, hair);
	}

	return !hair.cvint.empty();

}

	// ����������� HAIRVERTS �� HAIRPARAMS
	// ��� ����� �������
	// ����� ������ ��������� �� ��������� ��������� ������
	bool HairProcessor::DeformAloneHair_test(
		const HAIRPARAMS& params, 
		HAIRVERTS& hair,		// �� ����� - �������� �����
		bool bClump			// ���� ������� �����
		)
	{
		int SegmCount = (int)hair.basas.size()-1;

		srand( params.seed);

		// ������� BuildHair � ����������
		for(int i=0; i<(int)deformers_BUILDHAIR.size(); i++)
		{
			IHairDeformer* deformer = deformers_BUILDHAIR[i];
			float affect = deformer->affect.getValue(params.u, params.v);
			if( deformer->BuildHair(affect, params, SegmCount, hair))
			{
				return !hair.cvint.empty();
			}
		}
		for(unsigned x=0; x<modifiers.affectmodifiers.size(); x++)
		{
			HairMesh::ModifierData& modifier = modifiers.affectmodifiers[x];
			if( modifier.modifier->CallBuildHair(modifier.w, modifier.dirToModifier, params, SegmCount, hair))
				return !hair.cvint.empty();
		}

		// ����� ������
		Math::Matrix4f basis = buildBasis4f( params.position, params.normal, params.tangentU, params.tangentV);
		float scalelen = params.length*params.lengthjitter;
		if(scalelen<min_hairLen) scalelen=min_hairLen;
		basis.scale( Math::Vec4f( scalelen, scalelen, scalelen, 1.f));


		// ����������� ���������
		float polar	= params.polar;
		Math::Vec2f inclination = params.inclination;
		Math::Vec2f basecurl	= params.baseCurl*params.lengthjitter;
		Math::Vec2f tipcurl		= params.tipCurl*params.lengthjitter;
		float scraggle	= params.scraggle*scalelen;
		float baseWidth	= params.baseWidth;
		float tipWidth	= params.tipWidth;
		if( mulWidthByLength)
		{
			baseWidth *= scalelen;
			tipWidth *= scalelen;
		}
		float twist		= params.twist;
		twist = ((float)M_PI)*2*(twist-0.5f);
		// ����� �� ������
		if( params.twistFromPolar!=0)
		{
			float polardirive = surface->getPolarDeriv(params.u, params.v, polar);
			polardirive *= 2/(params.dPdU.length()+params.dPdV.length());
			polardirive *= params.twistFromPolar;
			polardirive *= 1/(scalelen);
			twist += ((float)M_PI)*2*polardirive;
		}
		float baseCLUMP = params.BaseClumpValue;// * this->BaseClumpValue;
		float tipCLUMP  = params.TipClumpValue;// * this->TipClumpValue;

		// ������� �����
		Math::Matrix4f maintransform = buildBasis4f(
			Math::Vec3f(0), 
			Math::Vec3f(0, 1, 0), 
			Math::Vec3f(1, 0, 0), 
			Math::Vec3f(0, 0, 1));


		Math::Vec3f point(0, 0, 0);
		Math::Vec3f normal(0, 1, 0);
		Math::Vec3f tangentU(1, 0, 0);
		Math::Vec3f tangentV(0, 0, 1);

		// ������ ������� = inclination*polar
		Math::Matrix4f obj = Math::Matrix4f::id;
		Math::Matrix4f m0;
		{
			// POLAR
			float theta = (float)( M_PI * (2 * polar));
			Math::Rot3f PolarRot( (float)theta, normal );
			PolarRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());
			m0 = obj;

			// INCLINATION
			float dzeta = inclination.length()*(float)M_PI*0.5f;
			if(dzeta>0.0001)
			{
				// ������� ������������ ��� ���������������� inclination
				Math::Vec3f axis = maintransform*Math::Vec3f(-inclination.y, 0, inclination.x);
				Math::Rot3f IncRot( dzeta, axis );
				Math::Matrix4f obj = Math::Matrix4f::id;
				IncRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());

				m0 = obj*m0;
			}
		}
		Math::Matrix4As3f m0_3(m0);
		tangentU = m0_3*tangentU;
		normal   = m0_3*normal;
		tangentV = m0_3*tangentV;

		// orto tangentV tangentU
		{
			tangentV = Math::cross( tangentU, normal);
			tangentV.normalize();
		}

		int tolerance   = SegmCount+1;

		hair.cvint.resize(SegmCount+1);
		hair.vfactor.resize(SegmCount+1);
		hair.vwidht.resize(SegmCount+1);
		hair.vclump.resize(SegmCount+1);
	//	hair.basas.resize(SegmCount+1);

		float segLength = 1/((float)SegmCount);
		for (int _k = 0;_k < tolerance; _k++)
		{
			float sl = segLength;
			float param = (float)_k/((float)tolerance-1);
			float cf = baseCLUMP*(1-param) + param*tipCLUMP;

			float w = (1-param)*baseWidth + param*tipWidth;
			// ��� ���� ����� �� ���������� � 0
			if( w<1e-3f) w = 1e-3f;

			// ������� �������������:
			Math::Matrix4f mi = Math::Matrix4f::id;
			if(_k!=SegmCount)
			{
				Math::Matrix4f mi0 = hair.basas[_k];
				Math::Matrix4f mi1 = hair.basas[_k+1];
//				mi = mi1*mi0.inverted();
				mi = mi0.inverted()*mi1;
 			}

			// �������
			Math::Matrix4f transform;
			transform[0] = Math::Vec4f(tangentU, 0)*w;
			transform[1] = Math::Vec4f(normal, 0);
			transform[2] = Math::Vec4f(tangentV, 0)*w;
			transform[3] = Math::Vec4f(point, 1);

			hair.cvint[_k] = transform*Math::Vec3f(0, 0, 0);
			hair.cvint[_k] = basis*hair.cvint[_k];
			hair.vfactor[_k] = param;
			hair.vwidht[_k] = w;
			hair.basas[_k] = transform;
			hair.vclump[_k] = cf;

	//		if(_k==SegmCount)
	//			break;

			// curl
			{
				Math::Vec2f curl = (1-param)*basecurl + param*tipcurl;
				curl = (curl)/((float)tolerance-1);
				float curlangle = curl.length();
				if(curlangle>0.000001)
				{
					if( _k==0) 
						curlangle*=0.5f; 
					// ������� ������������ ��� ���������������� curl
					Math::Vec3f axis = maintransform*Math::Vec3f(-curl.y, 0, curl.x);
					Math::Rot3f CurlRot( curlangle, axis );
					Math::Matrix4f obj = Math::Matrix4f::id;
					CurlRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());
					mi = obj*mi;
					// ����������� �����!!!
					sl = 2*sin(curlangle*0.5f)*sl/curlangle;
				}
			}

			// twist
			{
				float twistAngle = twist/((float)tolerance-1);
				Math::Rot3f TwistRot( (float)(twistAngle), Math::Vec3f(0, 1, 0) );
				TwistRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());
				mi = obj*mi;
			}

			// scragle
			if (scraggle!= 0)
			{
				Math::Vec3f RandVec;
				RandVec = randomAxis();

				point += tangentU * sl * scraggle * RandVec.x;
				point += tangentV * sl * scraggle * RandVec.z;
	//			point.x += sl * scraggle * RandVec.x;
	//			point.y += sl * scraggle * RandVec.y;
	//			point.z += sl * scraggle * RandVec.z;
			}

			Math::Matrix4As3f mi_3(mi);
			tangentU = mi_3*tangentU;
			normal   = mi_3*normal;
			tangentV = mi_3*tangentV;

			point += normal*sl;
			if(_k!=SegmCount)
			{
//				point = hair.basas[_k+1][3].as<3>();
			}
		}
		/*/
		{
			Math::Vec3f lastpt = hair.cvint[tolerance-1];
			Math::Vec3f normalX = Math::cross(params.dPdU, params.dPdV);
			Math::Matrix4f basisX = buildBasis4f( params.position, normalX, params.dPdU, params.dPdV);
			Math::Matrix4f basisX_inv = basisX.inverted();
			Math::Vec3f uvX = basisX_inv*lastpt;

			Math::Vec2f uv = tangentSpace2_2d(uvX);
			uv.x += params.u;
			uv.y += params.v;
			if(uv.x>0&&uv.x<1&&uv.y>0&&uv.y<1)
			{
				float polartip = surface->getPolar(uv.x, uv.y);

				double theta = M_PI * (2 * (polartip-polar))*params.scraggleFreq;
				for (int _k = 0;_k < tolerance; _k++)
				{
					float param = (float)_k/((float)tolerance-1);
					Math::Rot3f PolarRot( (float)param*theta/(tolerance-1), params.normal );
					Math::Matrix4f obj = Math::Matrix4f::id;
					PolarRot.getBasis(obj[0].data(), obj[1].data(), obj[2].data());

					hair.cvint[_k] = obj*hair.cvint[_k];
				}
			}
		}
		//*/
		// ������� PostBuildHair � ����������
		for(int i=0; i<(int)deformers_POSTBUILDHAIR.size(); i++)
		{
			IHairDeformer* deformer = deformers_POSTBUILDHAIR[i];
			float affect = deformer->affect.getValue(params.u, params.v);
			deformer->PostBuildHair(affect, params, SegmCount, hair);
		}
		for(unsigned x=0; x<modifiers.affectmodifiers.size(); x++)
		{
			HairMesh::ModifierData& modifier = modifiers.affectmodifiers[x];
			modifier.modifier->CallPostBuildHair(modifier.w, modifier.dirToModifier, params, SegmCount, hair);
		}

		return !hair.cvint.empty();

	}


double HairProcessor::randomize(void)
{
    return (double) rand() / (double) RAND_MAX;
}

Math::Vec3f HairProcessor::randomAxis()
{
	Math::Vec3f axis;
	float axisX = (float)( 2.0f * randomize() - 1.0f);
    float t = (float)( 2.0f * M_PI * randomize());
    float w = sqrt(1 - axisX * axisX);
	
    float axisY = w * cos(t);
    float axisZ = w * sin(t);

	axis.x = axisX;
	axis.y = axisY;
	axis.z = axisZ;
	axis.normalize();
	return axis;
}

