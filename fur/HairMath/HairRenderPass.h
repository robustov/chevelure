#pragma once

#include "HairMath.h"
#include "Math/Math.h"
#include "Math/Box.h"
#include <Util/Stream.h>
#include <Util/STLStream.h>
#include <vector>
#include <map>
#include "supernova/ISnShaper.h"
#include "IHairDeformer.h"

#pragma warning( disable : 4251)

// ������ ��� ������� ���������
struct HAIRMATH_API HairRenderPass
{
	// StandAlone
	bool bStandAlone;
	// �������
	bool bEnable;
	// ��� �������
	std::string passName;
	// ����� ���������
	int	SegmCount;
	// ������ ������
	std::string shader;
	// bMotionBlur
	bool bMotionBlur;	// ���� false �� ����� �� ����� �����

	enum enSource
	{
		RT_DEFAULT=0,		// RT_INSTANCE ���� ���� ��������, ����� RT_HAIRS
		RT_HAIRS=1,			// �������� ������ 
		RT_TUBES=2,			// �������� ������������� ������
		RT_INSTANCES=3,		// �������� �������
		RT_SUBDIVTUBES=4	// �������� ��������� ������
	};
	// ��� ���������
	enSource source;
	// ��������� ������
	bool bRenderClumps;
	// ������������ [0, 1]
	// �������� �� ������ � ������� �������� ������������ (DP) < degradation
	// ������ � ������� DP > degradationWidth �� ��������
	// ������ � ������� degradation < DP < degradationWidth  �������� � ����������� ����������� ������ (� degradationWidth width=0)
	float degradation;
	float degradationWidth; 

	// ������ ��� ��������� �����
	float RenderWidth;
	// �������� ������ �� �����?
	bool mulWidthByLength;
	// ������������ ������ ������ ��� ������
	bool useClumpRadiusAsWidth;

	// ������������
	int interpolation;	// enInterpolation 
	bool diceHair, renderDoubleSide, reverseOrientation;
	// sigma
	bool sigmaHiding;

	// �� ���� ������ �������
	enRenderNormalSource renderNormalSource;

	// Raytrace
	bool renderRayTraceEnable, renderRayCamera, renderRayTrace, renderRayPhoton;


	struct DetailRange
	{
		float minvisible, lowertransition, uppertransition, maxvisible;
		bool isVisible() const
		{
			return (minvisible!=lowertransition || lowertransition!=uppertransition || uppertransition!=maxvisible);
		}
	};
	bool bUseDetails;
	DetailRange detailrange;

	// ����� ����� ��������
	std::vector< std::string> passes;

	// 08.11.2009
	// ����� ����� ��������
	std::vector< std::string> passIds;


	// prune
	bool bPrune;
	float pruningrate;			// �������
	float prunedistmin;			// ��������� ������ ������������
	float prunemin_value;		// ����������� ��������
	float prunemin_width;		// ������ ��� ����������� ��������
	float pruneTestDistance;	// �������� ��������� (���� 0 ��� )

	// ����������� ����. �������� (��� �������� � ������)
	float prune;

	// 13.03.2008
	float widthBias;			// ������� � ������

	// 22.08.2008
	float opacity;
public:
	HairRenderPass();
	void serialize(Util::Stream& stream);
	void serializeOcs(const char* _name, cls::IRender* render, bool bSave);
	void clear();

	// ��������� ���������� ���������� �� �������
	int CalcCount(int haircount, int clumpcount);
};

template <class STREAM>
STREAM& operator>>(STREAM& serializer, HairRenderPass &v)
{
	v.serialize( serializer);
	return serializer;
}
