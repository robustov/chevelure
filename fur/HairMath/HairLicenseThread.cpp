#include "StdAfx.h"
#include ".\HairMath.h"
#include ".\hairlicensethread.h"

#ifdef SAS
#ifndef CHEVELUREDEMO

// ������ ��������� ����������
//#define CHEVELURE_DUMP

#include "../HairLicense/HLClient.h"
#include "../HairLicense/HLUtils.h"
DWORD licenseflag = 0;
DWORD threadId = 0;

HANDLE hThread = INVALID_HANDLE_VALUE;
HANDLE hEvent = INVALID_HANDLE_VALUE;
const DWORD timeout = 1000;

DWORD WINAPI ThreadProc(
	LPVOID lpParameter
)
{
//fprintf(stderr, "Chevelure: ThreadProc\n");

	HANDLE hEvent = (HANDLE)lpParameter;

	char filename[512];
	GetModuleFileName((HMODULE)hairMathModule, filename, 512);

	std::string licensefile = HLsearchLicenseFile(filename);
	if( !licensefile.empty())
	{
		HLClient client;
		if( !client.Start(licensefile.c_str()))
			return -1;

		client.GetLicense(&licenseflag, sizeof(licenseflag));

		PulseEvent(hEvent);

		while(1)
		{
			DWORD res = MsgWaitForMultipleObjectsEx(0, NULL, 10000, QS_ALLEVENTS, 0);
			if( res != WAIT_TIMEOUT)
//			if( WaitForSingleObject(hEvent, 1000)==WAIT_OBJECT_0)
				break;
			client.KeepAliveLoop();
		}
		client.SendImDead();
//		fprintf(stderr, "Chevelure: exit ThreadProc\n");
	}
	else
	{
		fprintf(stderr, "Chevelure: cant find license file for %s\n", filename);
	}
	PulseEvent(hEvent);
	CloseHandle(hEvent);
	return 0;
}


HAIRMATH_API void HairLicenseStart()
{
#ifndef CHEVELUREDEMO
	if(hThread == INVALID_HANDLE_VALUE)
	{
		hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
		hThread = CreateThread(NULL, 0, &ThreadProc, (LPVOID)hEvent, 0, &threadId);

		DWORD ret = WaitForSingleObject(hEvent, timeout);
		if( ret!=WAIT_OBJECT_0)
		{
			fprintf(stderr, "Chevelure: start timeout\n");
		}
		else
		{
#ifdef CHEVELURE_DUMP
			fprintf(stderr, "Chevelure: HairLicenseStart lf=%x\n", licenseflag);
#endif 
		}
	}
#endif
}
HAIRMATH_API void HairLicenseStop()
{
#ifndef CHEVELUREDEMO
//fprintf(stderr, "Chevelure: HairLicenseStop\n");
	if(hThread != INVALID_HANDLE_VALUE)
	{
		PostThreadMessage(threadId, WM_QUIT, 0, 0);

//		PulseEvent(hEvent);
		DWORD ret = WaitForSingleObject(hThread, timeout);
		if( ret!=WAIT_OBJECT_0)
			fprintf(stderr, "Chevelure: stop timeout\n");
		hThread = INVALID_HANDLE_VALUE;
	}

#endif
}

#endif //CHEVELUREDEMO
#endif //SAS
