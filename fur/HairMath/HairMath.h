// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the HAIRMATH_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// HAIRMATH_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#pragma once

#include "HairMathVersion.h"
#include "ocellaris/IRender.h"

// �������� ��������� ��������� ��� ���������
//#define OCELLARIS_INSTANCE

#ifdef HAIRMATH_EXPORTS
#define HAIRMATH_API __declspec(dllexport)
#else
#define HAIRMATH_API __declspec(dllimport)
#endif

extern HANDLE hairMathModule;


#ifndef SAS
extern HAIRMATH_API DWORD UlHair_licflag;
#endif

#include "Math/Math.h"
#include <string>

#ifdef SAS
extern "C"
{
	HAIRMATH_API void HairLicenseStart();
	HAIRMATH_API void HairLicenseStop();
}
#endif

extern "C"
{
	HAIRMATH_API void HairGetVersion(int& buildN, std::string& buildDate, std::string& versionstring);
}

// �������������� polar -> Vec2f
inline Math::Vec2f polar2vector(float polar)
{
// ��������!!! ��� ������������� � ����. ��������!!!
polar = polar-0.25f;
	double theta = M_PI * (2 * polar);
	Math::Vec2f v = Math::rotate( Math::Vec2f(0, -1), (float)theta);
	return v;
}
// �������������� Vec2f -> polar
inline float vector2polar(const Math::Vec2f& vpolar)
{
	const float oneover2pi = (float)(1.f/(2.f*M_PI));
	float polar = Math::angle(vpolar)*oneover2pi;
// ��������!!!
polar = polar+0.25f;
	return polar+0.25f;
}
// �������������� �� tangentspace -> Vec2f
inline Math::Vec2f tangentSpace2_2d(const Math::Vec3f& arg) 
{
	return Math::Vec2f(arg.x, arg.z);
}
// �������������� �� Vec2f -> tangentspace
inline Math::Vec3f _2d2tangentSpace(const Math::Vec2f& arg) 
{
	return Math::Vec3f(arg.x, 0, arg.y);
}

// ��������������� ������ �� U ��� �� V
inline void orthogonalizeBasis( Math::Matrix4f& basis, bool bU)
{
	Math::Vec3f tangentU = basis[0].as<3>();
	Math::Vec3f normal   = basis[1].as<3>();
	Math::Vec3f tangentV = basis[2].as<3>();

	Math::Vec3f n = Math::cross( tangentU, tangentV);
	if( bU)
	{
		Math::Vec3f tu = Math::cross( n, tangentV);
		float tul = tu.length();
		float tul_ = tangentU.length();
		tangentU = -tu*tul_/tul;
		basis[0] = Math::Vec4f(tangentU,	0);
	}
	else
	{
		Math::Vec3f tu = Math::cross( tangentU, n);
		float tul = tu.length();
		float tul_ = tangentV.length();
		tangentV = -tu*tul_/tul;
		basis[2] = Math::Vec4f(tangentV,	0);
	}
}

// ���������� ������ �� ������� ������� � ���������
inline Math::Matrix4f buildBasis4f(
	const Math::Vec3f& position, 
	const Math::Vec3f& normal, 
	const Math::Vec3f& tangentU, 
	const Math::Vec3f& tangentV)
{
	Math::Matrix4f basis;
	basis[0] = Math::Vec4f(tangentU,	0);
	basis[1] = Math::Vec4f(normal,		0);
	basis[2] = Math::Vec4f(tangentV,	0);
	basis[3] = Math::Vec4f(position,	1);
	return basis;
}

// ���������� ������ �� ������� � ���������
inline Math::Matrix3f buildBasis3f(
	const Math::Vec3f& normal, 
	const Math::Vec3f& tangentU, 
	const Math::Vec3f& tangentV)
{
	Math::Matrix3f basis;
	basis[0] = tangentU;
	basis[1] = normal;
	basis[2] = tangentV;
	return basis;
}


// ������� tangentU �� ������
inline Math::Vec3f tangentUfromBasis(
	const Math::Matrix4f& basis)
{
	return Math::Vec3f(basis[0].x, basis[0].y, basis[0].z);
}
// ������� ������� �� ������
inline Math::Vec3f normalFromBasis(
	const Math::Matrix4f& basis)
{
	return Math::Vec3f(basis[1].x, basis[1].y, basis[1].z);
}

enum enHairInterpolation
{
	HI_LINIAR	 = 0, 
	HI_CATMULROM	 = 1
};

enum enOrthogonalizeType
{
	ORT_NONE = 0,
	ORT_TANGENTU = 1,
	ORT_TANGENTV = 2,
	ORT_TANGENTU_V1 = 3,
	ORT_TANGENTV_V1 = 4,
};

enum enRenderNormalSource
{
	RNS_NONE = 0,
	RNS_TANGENTU = 1,
	RNS_CLUMPVECTOR = 2,
};

enum enHairParamBit
{
	HPB_LENGTH		= 0x1, 
	HPB_INCLINATION = 0x2, 
	HPB_POLAR		= 0x4, 
	HPB_BASECURL	= 0x8, 
	HPB_TIPCURL		= 0x10, 
	HPB_BASEWIDTH	= 0x20, 
	HPB_TIPWIDTH	= 0x40, 
	HPB_SCRAGLE		= 0x80, 
	HPB_SCRAGLEFREQ	= 0x100, 
	HPB_BASECLUMP	= 0x200, 
	HPB_TIPCLUMP	= 0x400, 
	HPB_TWIST		= 0x1000, 
	HPB_DISPLACE	= 0x2000, 
	HPB_TWISTFROMPOLAR		= 0x4000, 
	HPB_BLENDCVFACTOR		= 0x8000, 
	HPB_BLENDDEFORMFACTOR	= 0x10000, 
};

