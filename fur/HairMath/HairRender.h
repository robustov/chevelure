#pragma once

#include "HairProcessor.h"
#include "HairRootPositions.h"
#include "HairMesh.h"
#include "HairSystem.h"
#include "HairClumps.h"
#include "IHairDeformer.h"
#include "mathNpixar\IPrman.h"

#include "ocellaris\IRender.h"
#include "ocellaris/renderPrmanImpl.h"

struct IPointCloudFile
{
	virtual void setPoint(
		const Math::Vec3f& pt, 
		const Math::Vec3f& n, 
		float radius
		)=0;
};

struct hairGroup;
//! HairRender
//! ��������� ��� ����������
//! ��������� ��� ������ ��� ����� 
//! HairGeometryIndices, HairGeometry, HairClumpPositions, HairRootPositions, HairMesh, HairProcessor
class HAIRMATH_API HairRender
{
public:
	IPrman* prman;
	cls::renderPrmanImpl<cls::TTransformStack_minimal> renderimpl;
	cls::IRender* render;

	struct StaticData
	{
		bool isValid;
		time_t timestamp;
		std::string fullobjname;
		// STATIC DATA
		HairGeometryIndices geometryIndices;
		HairCVPositions		cvPositions;
		HairClumpPositions	clumpPositions;
		HairRootPositions	rootPositions;

		// ������� ��� ��������
		std::vector<HairGeometryIndices::facegroup> hairPolygonGroups;
		std::vector<HairGeometryIndices::facegroup> clumpPolygonGroups;

		StaticData(){isValid=false;}

		bool Load(const char* filename, bool bDump);
	};
	std::string hairRootPositionsFileName;
	StaticData* staticData;
	bool bDeleteStaticData;

	HairSystem			hairSystem;

public:
	// DYNAMIC DATA
	struct HAIRMATH_API Dynamic
	{
		bool isValid;
		HairGeometry	geometry;
		HairMesh		surface;
		HairCV			cv;

		Dynamic(){isValid=false;};
		~Dynamic()
		{
		};
		//! ���������! 
		bool Load( Util::Stream& file);
		//! ���������! 
		bool Init( HairRender* hairRender);
	};
	// ���� ��� �����
//	Dynamic blurless;
	// � ������ ������ time -> Dynamic
	std::map<float, Dynamic*> deformblur;
	bool bDeleteDeformblur;
	// freeze
	Dynamic* deformfreeze;


/////////////////////////////////////////////
// polygonGroups
public:
	void getPolygonGroup(int group, bool bClump, int& startindex, int& endindex);
	int getPolygonGroupCount(){return (int)std::min(staticData->hairPolygonGroups.size(), staticData->clumpPolygonGroups.size());};

	//! get hair or clump polygon index
	int getPolygonIndex(int index, bool bClump);

	//! per hair seed
	int getSeed(int index, bool bClump);

	// IsHairDegradate
	bool IsHairDegradate( int hair, bool bClump, float degradation, float degradationWindow, float& degparam);

/////////////////////////////////////////////
// CACHE DATA
public:
	// ����������
	int nGroups, nHairs;

public:
    HairRender();
    ~HairRender();
	
	//! ���������! 
	bool Load(
		const char* filename
		);
	bool Load2(
		std::vector<std::string> files, 
		std::vector<float>& motion
		);

	//! ���������! �� ocs (��������)
//	bool LoadOcs(
//		std::vector< std::string>& framefiles,
//		float* motionBlurTimes
//		);
	//! ���������! �� ocs
	bool LoadDeformBlurFromOcs(
		cls::IRender* render,
		float motionBlurTime
		);

	//! ��������� Root Positions File
	bool LoadRootPositionsFile(
		const char* hairRootPositionsFileName, 
		time_t currenttimestamp, 
		bool bUseCache,
		bool bAllowNoFatalError=true
		);

	//! GenRIB
    bool GenRIB(
		IPrman* prman
		);
	//! GenPointClouds
    bool GenPointClouds(
		IPointCloudFile* pointcloudfile
		);
	//! ���������� �� HairProcessor::Render ��� ������ ������
	void GenGroupRIB(
		HairProcessor* processor, 
		int group, 
		const Math::Box3f& snbox, 
		std::vector<HairRenderPass*>& passes
		);

	//! GenOCS
    bool GenOCS(
		cls::IRender* render, 
		int hairRenderIndex
		);
	void GenGroupRIBOCS(
		cls::IRender* render, 
		int hairRenderIndex, 
		int group, 
		const Math::Box3f& snbox, 
		std::vector<HairRenderPass*>& passes
		);
//    bool RenderHairGroupOcs(
//		hairGroup* hg
//		);

	//��������� RenderWidth � degradation �� ��������
	float CalcPruning(
		HairRenderPass& pass, bool bDump
		);

	// �������� ����� visibility � prman
	void RayTraceOptions( 
		HairRenderPass* pPass, 
		IPrman* prman);

public:
	CRITICAL_SECTION mutex;

	int refCount;
	void addRef();
	void release();
	static HairRender* creator();
};
// ������ ���� ���������� HairRender
extern std::map<int, HairRender*> hairRenderPool;



//! get hair or clump polygon index
inline int HairRender::getPolygonIndex(int index, bool bClump)
{
	if( bClump) 
		return staticData->clumpPositions.getPolygonIndex(index, staticData->geometryIndices);
	return staticData->rootPositions.getPolygonIndex(index, staticData->geometryIndices);
}

// ����������� �����?
inline bool HairRender::IsHairDegradate( int hair, bool bClump, float degradation, float degradationWindow, float& degparam)
{
	float resDegradation = degradation + degradationWindow;

	int seed = this->getSeed( hair, bClump);
	srand(seed+0xdada);

	int rnd = rand();
	degparam = rnd/(float)RAND_MAX;
	if( degparam > resDegradation)
		return true;

	return false;
}
