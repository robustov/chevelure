#pragma once

#include <Math/Vector.h>
#include <Math/Matrix.h>
#include <Math/Rotation3.h>
#include <Math/Face.h>
#include <Math/Quad.h>
#include <Math/Polygon.h>
#include <Math/Subdiv.h>
#include <Math/Edge.h>
#include <Util/Stream.h>
#include <Util\STLStream.h>
#include <vector>
#include "HairMath.h"
#pragma warning( disable : 4251)

class HairGeometry;

class HairGeometryIndicesData;
//! ���������� ���������� ���������
//!			����������� ����������!!!
class HairGeometryIndices
{
public:
	friend class HairGeometryIndicesData;
	//! �������
	struct pointonsurface
	{
		int faceindex;
		int clumpindex;
		float decimationValue;	// �������� �� 0 �� 1 ��� ������������
		Math::Vec3f b; // � ���������������� �����.
		pointonsurface();
		bool isValid() const {return faceindex>=0;}
	};
	static pointonsurface badpointonsurface;
	//! ������ �� ��-���
	struct facegroup
	{
		int startindex;
		int endindex;
		facegroup(int startindex, int endindex);
	};

	// �������������� uvset
	struct uvSet
	{
		std::string name;
		int src_uvcount;
		int uvcount;
		std::vector< Math::Face32> faces_uv;
		std::vector< Math::Quad32> sd_uvquads;
		std::vector< Math::subdivWeight> sd_uvverts;

		uvSet();
		uvSet(const uvSet& arg);
		uvSet& operator=(const uvSet& arg);
		void copy(const uvSet& arg);

		void subdivide(std::vector<Math::Vec2f>& verts, std::vector< Math::subdivWeight>& sd_verts);
	};

public:
	HAIRMATH_API HairGeometryIndices();
	//! copy
	HAIRMATH_API void copy(const HairGeometryIndices& arg);
	//! operator=
	HAIRMATH_API HairGeometryIndices& operator=(const HairGeometryIndices& arg);

	HAIRMATH_API void init(int vertcount, int poligoncount, int normalcount, int uvcount, int nSubdiv, const char* baseUvSet, int uvSetCount, std::string* uvsetnames, int* uvsetcounts);
	HAIRMATH_API bool isValid(int vertcount, int poligoncount, int normalcount, int uvcount, int nSubdiv, const char* baseUvSet, int uvSetCount, std::string* uvsetnames, int* uvsetcounts);

//	unsigned long sourcePolygonCount() const{return src_poligoncount;};
//	unsigned long sourceVertexCount() const{return src_vertcount;}
//	unsigned long sourceNormalCount() const{return src_normalcount;};
public:
	bool isSubdived() const{return subdivideSteps!=0;}
	unsigned long faceCount() const{return (unsigned long)faces.size();};
	unsigned long vertexCount() const{return vertcount;}
	unsigned long normalCount() const{return normalcount;};
	unsigned long uvCount() const{return uvcount;};
	unsigned long uvSetCount() const{return (unsigned long)uvSets.size();};
	const char*   uvSetName(unsigned i)const{return uvSets[i].name.c_str();}
	unsigned long facevertexCount() const {return (unsigned long)faces.size()*3;};
	// ����� ������ uvSet �� �����. ���� ��� ������ -1
	HAIRMATH_API unsigned long finduvSetByName(const char*)const;

	const Math::Face32& face(unsigned i) const;
	const Math::Face32& faceUV(unsigned i) const;
	const Math::Face32& faceUVset(unsigned i, unsigned uvset) const;
	const Math::Face32& faceN(unsigned i) const{return faces_n[i];}
	Math::Face32	    fvface(unsigned i) const{return Math::Face32(i*3, i*3+1, i*3+2);};
	int faceSeed(unsigned i) const{return faceseeds[i];}
	int face_polygonIndex(unsigned i)const{return faces_polygonIndex[i];}
	Math::Face32& face(unsigned i);
	Math::Face32& faceUV(unsigned i);
	Math::Face32& faceUVset(unsigned i, unsigned uvset);
	Math::Face32& faceN(unsigned i){return faces_n[i];}
	bool isValid(){return bValid;}
	void setValid(){bValid=true;}
	const char* getBaseUVSet()const{return baseUvSet.c_str();};

	// facevertex
	int FV_vertexindex(int fv) const;
	int FV_normalindex(int fv) const;
	int FV_uvindex(int fv) const;

	// 13.03.2008
	// SRC:
	int src_vertexCount() const{ if(!isSubdived()) return vertexCount(); return src_vertcount;}
	int src_uvCount() const{ if(!isSubdived()) return uvCount(); return src_uvcount;}
	int src_faceCount()const{ if(!isSubdived()) return faceCount(); return (int)src_faces.size();}
	const Math::Face32& src_face(int i) const{if(!isSubdived()) return face(i);return src_faces[i];}
	const Math::Face32& src_faceUV(int i) const{if(!isSubdived()) return faceUV(i);return src_faces_uv[i];}
	int src_facevertexCount()const{if(!isSubdived()) return facevertexCount();return (int)src_faces.size()*3;}
	int src_FV_vertexindex(int fv)const;
	int src_FV_normalindex(int fv)const;
	int src_FV_uvindex(int fv)const;

protected:
	bool bValid;
	// �������� �������� (�� subdivide)
	int src_vertcount, src_poligoncount, src_normalcount, src_uvcount;
	// ��� �������� �� ����
	std::string baseUvSet;
	// ��������� �������� (����� subdivide)
	int vertcount, normalcount, uvcount;
	// ��-��
	std::vector< Math::Face32> faces;
	std::vector< Math::Face32> faces_uv;
	std::vector< Math::Face32> faces_n;
	std::vector< int> faceseeds;
	std::vector< int> faces_polygonIndex;	// ������ �������� �� �������� ������ face (��� ������ woking face set)
	//! ����� subdivide (������ ���.��������->{������ ������, ���})
	int subdivideSteps;	//! ����� ����� subdivide 
	std::vector< Math::Quad32> sd_quads, sd_uvquads, sd_nquads;
	std::vector< Math::subdivWeight> sd_verts, sd_uvverts, sd_nverts;

	// 13.03.2008
	std::vector< Math::Face32> src_faces;
	std::vector< Math::Face32> src_faces_uv;
	std::vector< Math::Face32> src_faces_n;

	// �������������� uvset (����� ��������� �� �������)
	std::vector< uvSet> uvSets;

	//! ����� ������� 
	//! �� ���� ������ �� �������� ��������� ������������� - ��� ������ ������ �������������� �������������
	std::set< Math::Edge32> brokenEdges;

	//! ��������� ��-��� ()
	//! ��������������� ��� ����� 0-1, 1-2, 2-0
	//! 0xFFFFFFFF - ����������� �����
	std::vector< Math::Face32> associatedFaces;
	//! �������� ��������� � ��-���
	//! associatedFacesToVetrexIndices �������� ������ � ������ associatedFacesToVetrex
	std::vector< int> associatedFacesToVetrexIndices; //!< ������ = vertcount+1
	std::vector< int> associatedFacesToVetrex;

protected:
	// ��������� ����� subdivide 
	HAIRMATH_API void buildSubdivisionScheme(
		int level, 
		int facevertexcount, 
		std::vector<Math::Polygon32>& sd_faces, 
		std::vector<Math::Polygon32>& sd_uvfaces, 
		std::vector<Math::Polygon32>& sd_nfaces);
	// ��������� ����� subdivide ��� ���������� uv ������
	HAIRMATH_API void buildSubdivisionSchemeForUv(
		int level, 
		int facevertexcount, 
		int set,
		std::vector<Math::Polygon32>& sd_uvfaces
		);

public:
	//! ����� ������������ � ������� ������ �������
	HAIRMATH_API void findVertexAssociatedFaces(
		int v,						//!< vertex
		int*& faces,				//!< ��������� ������
		int& count
		);
	//! ����� ��� facevertex ��� ������� ��������
	HAIRMATH_API void findFacevertexForVertex(
		int v,						//!< vertex
		std::vector<int>& fvs		//!< ��������� ������
		);

	//! ����� ��-�� ����������� ����� � ��-���
	HAIRMATH_API void findAssociatedFaces(
		HairGeometry* geometry,		//!< ���������
		int f,						//!< face
		float radius,				//!< ������ ������
		std::vector<int>& faces,	//!< ��������� ������
		int& callCount				//!< ����. ������� ������ (��� ������������ facecount). ������ ����� �������
		);
public:
	HAIRMATH_API void serialize(Util::Stream& stream);
	HAIRMATH_API void clear();
	HAIRMATH_API void buildAssociated();

	// subdivide ���
	template <class T>
	void subdivide_faces(std::vector<T>& verts, int type);
	// subdivide �������������� uv
	void subdivide_uvSetfaces(int set, std::vector<Math::Vec2f>& verts);
	// subdivide ���� ��� ��������
	const Math::subdivWeight& getSubdiveShemeForVertex(int v) const;
protected:
	// ����� �� ��������� (�� ����� ��������� brokenEdges)
	void findAssociatedFaces_rec( 
		HairGeometry* geometry,
		unsigned vertex, 
		float radius, 
		std::vector<unsigned char>& selectedfaces, 
		int& callCount,
		int maxCallCount);
	// ����� �� ��-��� (��� ������ brokenEdges)
	void findAssociatedFaces_rec2( 
		HairGeometry* geometry,
		unsigned face, 
		float radius, 
		std::vector<unsigned char>& selectedfaces, 
		int& callCount, 
		int maxCallCount);
};





inline HairGeometryIndices::pointonsurface::pointonsurface()
{
	faceindex = -1;
	clumpindex = -1;
	decimationValue = 0;
}
inline HairGeometryIndices::facegroup::facegroup(int startindex=0, int endindex=0)
{
	this->startindex = startindex;
	this->endindex = endindex;
}

template <class STREAM>
STREAM& operator>>(STREAM& serializer, HairGeometryIndices::pointonsurface &v)
{
	serializer >> v.faceindex;
	serializer >> v.clumpindex;
	serializer >> v.decimationValue;
	serializer >> v.b;
	return serializer;
}
template <class STREAM>
STREAM& operator>>(STREAM& serializer, HairGeometryIndices::facegroup &v)
{
	serializer >> v.startindex;
	serializer >> v.endindex;
	return serializer;
}

inline const Math::Face32& HairGeometryIndices::face(unsigned i) const
{
	return faces[i];
}
inline const Math::Face32& HairGeometryIndices::faceUV(unsigned i) const
{
	return faces_uv[i];
}
inline const Math::Face32& HairGeometryIndices::faceUVset(unsigned i, unsigned uvset) const
{
	const std::vector< Math::Face32>& faces_uv = this->uvSets[uvset].faces_uv;
	return faces_uv[i];
}

inline Math::Face32& HairGeometryIndices::face(unsigned i)
{
	return faces[i];
}
inline Math::Face32& HairGeometryIndices::faceUV(unsigned i)
{
	return faces_uv[i];
}
inline Math::Face32& HairGeometryIndices::faceUVset(unsigned i, unsigned uvset)
{
	uvSet& set = this->uvSets[uvset];
	std::vector< Math::Face32>& faces_uv = set.faces_uv;
	return faces_uv[i];
}

inline int HairGeometryIndices::FV_vertexindex(int fv)const
{
	int face = fv/3;
	int vinface = fv-face*3;
	int index = this->face(face).v[vinface];
	return index;
}
inline int HairGeometryIndices::FV_normalindex(int fv)const
{
	int face = fv/3;
	int vinface = fv-face*3;
	int index = this->faceN(face).v[vinface];
	return index;
}
inline int HairGeometryIndices::FV_uvindex(int fv)const
{
	int face = fv/3;
	int vinface = fv-face*3;
	int index = this->faceUV(face).v[vinface];
	return index;
}

///////////////////////////////////////
// 13.03.2008
inline int HairGeometryIndices::src_FV_vertexindex(int fv)const
{
	if( !isSubdived())
		return FV_vertexindex(fv);
	int face = fv/3;
	int vinface = fv-face*3;
	int index = this->src_faces[face].v[vinface];
	return index;
}
inline int HairGeometryIndices::src_FV_normalindex(int fv)const
{
	if( !isSubdived())
		return FV_normalindex(fv);
	int face = fv/3;
	int vinface = fv-face*3;
	int index = this->src_faces_n[face].v[vinface];
	return index;
}
inline int HairGeometryIndices::src_FV_uvindex(int fv)const
{
	if( !isSubdived())
		return FV_uvindex(fv);
	int face = fv/3;
	int vinface = fv-face*3;
	int index = this->src_faces_uv[face].v[vinface];
	return index;
}

template <class T>
void HairGeometryIndices::subdivide_faces(std::vector<T>& verts, int type)
{
	if( subdivideSteps<=0) return;	// ������ �� ���� ������
	int count=0;
	std::vector< Math::subdivWeight>* weigths = NULL;
	switch( type)
	{
	case 0:
		count = vertexCount();
		weigths = &sd_verts;
		break;
	case 1:
		count = uvCount();
		weigths = &sd_uvverts;
		if( weigths->empty()) weigths = &sd_verts;
		break;
	case 2:
		count = normalCount();
		weigths = &sd_nverts;
		if( weigths->empty()) weigths = &sd_verts;
		break;
	}
	std::vector<T> oldverts = verts;
	verts.resize(count);
	for(int v=0; v<count; v++)
	{
		T vert(0);
		Math::subdivWeight& w = (*weigths)[v];
		for(unsigned x=0; x<w.data.size(); x++)
		{
			float we = w.data[x].second;
			int vi = w.data[x].first;
			vert += we * oldverts[vi];
		}
		verts[v] = vert;
	}
}

inline void HairGeometryIndices::subdivide_uvSetfaces(int set, std::vector<Math::Vec2f>& verts)
{
	if( subdivideSteps<=0) return;	// ������ �� ���� ������
	uvSets[set].subdivide(verts, sd_verts);
}

// subdivide ���� ��� ��������
inline const Math::subdivWeight& HairGeometryIndices::getSubdiveShemeForVertex(int v) const
{
	return sd_verts[v];
}

inline void HairGeometryIndices::uvSet::subdivide(std::vector<Math::Vec2f>& verts, std::vector< Math::subdivWeight>& sd_verts)
{
	std::vector< Math::subdivWeight>* weigths = &sd_uvverts;
	if( weigths->empty()) 
		weigths = &sd_verts;
	
	std::vector<Math::Vec2f> oldverts = verts;
	verts.resize(uvcount);
	for(int v=0; v<uvcount; v++)
	{
		Math::Vec2f vert(0);
		Math::subdivWeight& w = (*weigths)[v];
		for(unsigned x=0; x<w.data.size(); x++)
		{
			float we = w.data[x].second;
			int vi = w.data[x].first;
			vert += we * oldverts[vi];
		}
		verts[v] = vert;
	}
}
