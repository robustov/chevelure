#pragma once

#include "HairMath.h"
#include "ocellaris/IRender.h"

#include "HairGeometryIndices.h"

#include <Math/Vector.h>
#include <Math/Matrix.h>
#include <Math/Rotation3.h>
#include <Math/Face.h>
#include <Math/Box.h>
#include "Math/matrixMxN.h"
#include <Util/Stream.h>
#include <Util\STLStream.h>
#include "Math\MeshUtil.h"
#include <vector>

//! ���������. ��������� �� HairGeometryIndices
//! ������ ������, ��������, UV, tangentU, tangentV
class HairGeometry
{
	friend class HairGeometryData;
	friend class HairRender;
public:
	//! copy
	HAIRMATH_API void copy(const HairGeometry& arg);
	//! operator=
	HAIRMATH_API HairGeometry& operator=(const HairGeometry& arg);
public:
	//! vert
	Math::Vec3f& vert(int i);
	const Math::Vec3f& vert(int i)const ;
	//! findUvFace
	//! ������ -1 ���� �-� �� ������
	HAIRMATH_API int findUvFace(const Math::Vec2f& uv, const HairGeometryIndices* hgi, Math::Vec3f& bary);

	Math::Vec2f getUV(int faceindex, const Math::Vec3f& bary, const HairGeometryIndices* geometryIndices)const;
	Math::Vec2f getUVset(int faceindex, const Math::Vec3f& bary, HairGeometryIndices* geometryIndices, int uvset);
	void getUVsets(int faceindex, const Math::Vec3f& bary, HairGeometryIndices* geometryIndices, std::vector<Math::Vec2f>& uvSets);
	Math::Vec3f getPOS(int faceindex, const Math::Vec3f& bary, HairGeometryIndices* geometryIndices);
	void getBasis(
		int faceindex, 
		const Math::Vec3f& bary, 
		HairGeometryIndices* geometryIndices, 
		Math::Vec3f& normal,
		Math::Vec3f& tangentU,
		Math::Vec3f& tangentV);

	void getBasis(
		int faceindex, 
		const Math::Vec3f& bary, 
		HairGeometryIndices* geometryIndices, 
		Math::Vec3f& normal,
		Math::Vec3f& tangentU,
		Math::Vec3f& tangentV, 
		Math::Vec3f& dpdU, 
		Math::Vec3f& dpdV 
		);

	void getBasis(
		int faceindex, const Math::Vec3f& bary, 
		HairGeometryIndices* geometryIndices, 
		Math::Matrix4f& basis);

	// data
	Math::Vec3f& vertex(int v){return vertexArray[v];}
	Math::Vec3f& normal(int i){return normals[i];}
	Math::Vec2f& uv(int i){return uvArray[i];}
	Math::Vec2f& uvSet(int i, int set){return uvSets[set][i];}
	Math::Vec3f& tangentU(int i){return tangentUs[i];}
	Math::Vec3f& tangentV(int i){return tangentVs[i];}

	// ��� facevertex
	const Math::Vec3f& FV_vertex(int fv, const HairGeometryIndices* gi) const;
	const Math::Vec3f& FV_normal(int fv, const HairGeometryIndices* gi) const;
	const Math::Vec2f& FV_UV(int fv, const HairGeometryIndices* gi) const;
	const Math::Vec3f& FV_tangentU(int fv, const HairGeometryIndices* gi) const;
	const Math::Vec3f& FV_tangentV(int fv, const HairGeometryIndices* gi) const;

	// 13.03.2008
	Math::Vec2f src_getUV(int faceindex, const Math::Vec3f& bary, const HairGeometryIndices* geometryIndices)const;
	const Math::Vec3f& src_vert(int v)const;
	const Math::Vec2f& src_uv(int i)const;
	const Math::Vec3f& src_FV_vertex(int fv, const HairGeometryIndices* gi) const;
	const Math::Vec2f& src_FV_UV(int fv, const HairGeometryIndices* gi) const;
	const Math::Vec3f& src_FV_normal(int fv, const HairGeometryIndices* gi) const;
	const Math::Vec3f& src_FV_tangentU(int fv, const HairGeometryIndices* gi) const;
	const Math::Vec3f& src_FV_tangentV(int fv, const HairGeometryIndices* gi) const;

	void resize(int vc, int nc, int uvc, std::vector<int>* uvSet_counts)
	{
		vertexArray.resize(vc);
		normals.resize(nc);
		uvArray.resize(uvc);
		if( uvSet_counts)
		{
			uvSets.resize(uvSet_counts->size());
			for(unsigned i=0; i<uvSet_counts->size(); i++)
				uvSets[i].resize((*uvSet_counts)[i]);
		}
	}

	//! ��������� subdivide ����� � ������ � �������� vertexArray, normals, uvArray
	HAIRMATH_API void subdivide(HairGeometryIndices* geometryIndices);

	//! ��������� Tangent space
	HAIRMATH_API void calcTangentSpace(HairGeometryIndices* geometryIndices);

	//! ��������� Bounds
	HAIRMATH_API void calcBounds();

	bool isValid(){return bValid;}
	void setValid(){bValid=true;}
public:
	Math::Box3f box;
	Math::Vec2f minUV, maxUV;				// range
protected:
	bool bValid;
	std::vector<Math::Vec3f> vertexArray;
	std::vector<Math::Vec3f> normals;
	std::vector<Math::Vec2f> uvArray;
	std::vector<Math::Vec3f> tangentUs;
	std::vector<Math::Vec3f> tangentVs;
	std::vector< std::vector<Math::Vec2f> > uvSets;

	// 13.03.2008
	std::vector<Math::Vec3f> src_vertexArray;
	std::vector<Math::Vec3f> src_normals;
	std::vector<Math::Vec2f> src_uvArray;
	std::vector<Math::Vec3f> src_tangentUs;
	std::vector<Math::Vec3f> src_tangentVs;


public:
	HAIRMATH_API HairGeometry();
	HAIRMATH_API void clear();
	HAIRMATH_API void serialize(Util::Stream& stream);
	HAIRMATH_API void serializeOcs(const char* _name, cls::IRender* render, bool bSave);
};

inline Math::Vec3f& HairGeometry::vert(int i)
{
	return vertexArray[i];
}
inline const Math::Vec3f& HairGeometry::vert(int i)const 
{
	return vertexArray[i];
}

inline Math::Vec2f HairGeometry::getUV(int faceindex, const Math::Vec3f& bary, const HairGeometryIndices* geometryIndices) const
{
	return Math::FromBary(bary, geometryIndices->faceUV(faceindex), uvArray);
}
inline Math::Vec2f HairGeometry::getUVset(int faceindex, const Math::Vec3f& bary, HairGeometryIndices* geometryIndices, int uvset)
{
	if( uvset>=(int)uvSets.size())
		return Math::Vec2f(0);
	std::vector<Math::Vec2f>& uvArray = uvSets[uvset];
	return Math::FromBary(bary, geometryIndices->faceUVset(faceindex, uvset), uvArray);
}
inline void HairGeometry::getUVsets(int faceindex, const Math::Vec3f& bary, HairGeometryIndices* geometryIndices, std::vector<Math::Vec2f>& dst)
{
	dst.resize( uvSets.size());
	for( int uvset=0; uvset<(int)uvSets.size(); uvset++)
	{
		dst[uvset] = Math::FromBary(bary, geometryIndices->faceUVset(faceindex, uvset), uvArray);
	}
}

inline Math::Vec3f HairGeometry::getPOS(int faceindex, const Math::Vec3f& bary, HairGeometryIndices* geometryIndices)
{
	return Math::FromBary(bary, geometryIndices->face(faceindex), vertexArray);
}

inline void HairGeometry::getBasis(
	int faceindex, 
	const Math::Vec3f& bary, 
	HairGeometryIndices* geometryIndices, 
	Math::Vec3f& normal,
	Math::Vec3f& tangentU,
	Math::Vec3f& tangentV)
{
	normal   = Math::FromBary(bary, geometryIndices->faceN(faceindex), normals);
	tangentU = Math::FromBary(bary, geometryIndices->faceUV(faceindex), tangentUs);
	tangentV = Math::FromBary(bary, geometryIndices->faceUV(faceindex), tangentVs);
	normal.normalize();

	float du = Math::dot(tangentU, normal);
	tangentU -= du*normal;
	float dv = Math::dot(tangentV, normal);
	tangentV -= dv*normal;

	tangentU.normalize();
	tangentV.normalize();
}
inline void HairGeometry::getBasis(
	int faceindex, 
	const Math::Vec3f& bary, 
	HairGeometryIndices* geometryIndices, 
	Math::Vec3f& normal,
	Math::Vec3f& tangentU,
	Math::Vec3f& tangentV, 
	Math::Vec3f& dpdU, 
	Math::Vec3f& dpdV 
	)
{
	normal   = Math::FromBary(bary, geometryIndices->faceN(faceindex), normals);
	tangentU = Math::FromBary(bary, geometryIndices->faceUV(faceindex), tangentUs);
	tangentV = Math::FromBary(bary, geometryIndices->faceUV(faceindex), tangentVs);
	dpdU = tangentU;
	dpdV = tangentV;
	normal.normalize();

	float du = Math::dot(tangentU, normal);
	tangentU -= du*normal;
	float dv = Math::dot(tangentV, normal);
	tangentV -= dv*normal;

	tangentU.normalize();
	tangentV.normalize();
}

inline void HairGeometry::getBasis(
	int faceindex, 
	const Math::Vec3f& bary, 
	HairGeometryIndices* geometryIndices, 
	Math::Matrix4f& basis)
{
	this->getBasis(
		faceindex, bary, geometryIndices, 
		*(Math::Vec3f*)&basis[1],
		*(Math::Vec3f*)&basis[0],
		*(Math::Vec3f*)&basis[2]
		);
	basis[0][3] = basis[1][3] = basis[2][3] = 0;
	Math::Vec3f pos = Math::FromBary(bary, geometryIndices->face(faceindex), vertexArray);
	basis[3] = Math::Vec4f(pos, 1);
}

// ��� facevertex
inline const Math::Vec3f& HairGeometry::FV_vertex(int fv, const HairGeometryIndices* gi) const
{
	int index = gi->FV_vertexindex(fv);
	return vertexArray[index];
}
inline const Math::Vec3f& HairGeometry::FV_normal(int fv, const HairGeometryIndices* gi) const
{
	int index = gi->FV_normalindex(fv);
	return normals[index];
}
inline const Math::Vec2f& HairGeometry::FV_UV(int fv, const HairGeometryIndices* gi) const
{
	int index = gi->FV_uvindex(fv);
	return uvArray[index];
}
inline const Math::Vec3f& HairGeometry::FV_tangentU(int fv, const HairGeometryIndices* gi) const
{
	int index = gi->FV_uvindex(fv);
	return tangentUs[index];
}
inline const Math::Vec3f& HairGeometry::FV_tangentV(int fv, const HairGeometryIndices* gi) const
{
	int index = gi->FV_uvindex(fv);
	return tangentVs[index];
}

// 13.03.2008
inline Math::Vec2f HairGeometry::src_getUV(int faceindex, const Math::Vec3f& bary, const HairGeometryIndices* geometryIndices) const
{
	if(!geometryIndices->isSubdived())
		return this->getUV(faceindex, bary, geometryIndices);
	return Math::FromBary(bary, geometryIndices->src_faceUV(faceindex), src_uvArray);
}

inline const Math::Vec3f& HairGeometry::src_vert(int v)const
{
	if( v>=(int)src_vertexArray.size())
		return vert(v);
	return src_vertexArray[v];
};
inline const Math::Vec2f& HairGeometry::src_uv(int i)const
{
	return src_uvArray[i];
}

inline const Math::Vec3f& HairGeometry::src_FV_vertex(int fv, const HairGeometryIndices* gi) const
{
	if(!gi->isSubdived())
		return FV_vertex(fv, gi);
	int index = gi->src_FV_vertexindex(fv);
	return src_vertexArray[index];
}
inline const Math::Vec2f& HairGeometry::src_FV_UV(int fv, const HairGeometryIndices* gi) const
{
	if(!gi->isSubdived())
		return FV_UV(fv, gi);
	int index = gi->src_FV_uvindex(fv);
	return src_uvArray[index];
}

inline const Math::Vec3f& HairGeometry::src_FV_normal(int fv, const HairGeometryIndices* gi) const
{
	if(!gi->isSubdived())
		return FV_normal(fv, gi);
	int index = gi->src_FV_normalindex(fv);
	return src_normals[index];
}
inline const Math::Vec3f& HairGeometry::src_FV_tangentU(int fv, const HairGeometryIndices* gi) const
{
	if(!gi->isSubdived())
		return FV_tangentU(fv, gi);
	int index = gi->src_FV_uvindex(fv);
	return src_tangentUs[index];
}
inline const Math::Vec3f& HairGeometry::src_FV_tangentV(int fv, const HairGeometryIndices* gi) const
{
	if(!gi->isSubdived())
		return FV_tangentV(fv, gi);
	int index = gi->src_FV_uvindex(fv);
	return src_tangentVs[index];
}
