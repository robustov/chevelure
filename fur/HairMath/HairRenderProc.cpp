#include "stdafx.h"
#include "HairMath.h"
#include "HairRender.h"
#include "Util\misc_evaltime.h"

// ������ ���� ���������� HairRender
std::map<int, HairRender*> hairRenderPool;

//! @ingroup implement_group
//! \brief ��������� ������ ��������� �������� ��������
//! 
struct HairRenderProc : public cls::IProcedural
{
//	HairRender hairRender;
public:
	// ������ ����������
	virtual void Render(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		);
};


HairRenderProc hairRenderProc;
extern "C" 
{
    __declspec(dllexport) cls::IProcedural* __cdecl HairRenderProc(cls::IRender* prman)
	{
		return &hairRenderProc;
	}
}
Util::FunEvalTimeCounter HairRenderProc_Render_Counter;

// ������ ����������
void HairRenderProc::Render(
	cls::IRender* render, 
	int motionBlurSamples, 
	float* motionBlurTimes
	)
{
	fprintf(stderr, "HairRenderProc Render\n");
	fflush(stderr);

	Util::FunEvalTime fet(HairRenderProc_Render_Counter);
	{
		bool bRenderFiles = false;
		render->GetAttribute("viewport::bRenderFiles", bRenderFiles);
		if( !bRenderFiles)
		{
			const char* renderType = render->renderType();
			if( strcmp(renderType, "opengl")==0)
			{
				return;
			}
		}
	}

	try
	{
		HairRender* hairRender = new HairRender();

		std::string hairrootfilename;
		int timestamp;
		if( !render->GetParameter("@Hair::positionfilename", hairrootfilename)) 
			return;
		if( !render->GetParameter("@Hair::timestamp", timestamp)) 
			return;

		hairRender->hairSystem.serializeOcs("@hairsystem", render, false);

//printf("HairRenderProc::Render %s\n", hairRender->hairSystem.dumpInformation?"1":"0");

		if( !hairRender->LoadRootPositionsFile(hairrootfilename.c_str(), timestamp, true, false))
			return;

		if( !motionBlurSamples)
		{
			HairRender::Dynamic* dynamic = new HairRender::Dynamic();

			dynamic->geometry.serializeOcs("geometry", render, false);
			dynamic->surface.serializeOcs("HairMesh", render, false);
			dynamic->cv.serializeOcs("cv", render, false);

			dynamic->Init( hairRender);
			dynamic->surface.InitDeformers();
			dynamic->isValid = true;

			hairRender->deformblur[cls::nonBlurValue] = dynamic;

//			hairRender.LoadDeformBlurFromOcs(render, 0);
		}
		else
		{
			for(int i=0; i<motionBlurSamples; i++)
			{
				render->SetCurrentMotionPhase(motionBlurTimes[i]);

				HairRender::Dynamic* dynamic = new HairRender::Dynamic();

				dynamic->geometry.serializeOcs("geometry", render, false);
				dynamic->surface.serializeOcs("HairMesh", render, false);
				dynamic->cv.serializeOcs("cv", render, false);

				dynamic->Init( hairRender);
				dynamic->surface.InitDeformers();
				dynamic->isValid = true;

				hairRender->deformblur[motionBlurTimes[i] ] = dynamic;
			}
			render->SetCurrentMotionPhase(cls::nonBlurValue);
		}
		int hairRenderIndex = (int)hairRenderPool.size();
		hairRenderPool[hairRenderIndex] = hairRender;

		hairRender->GenOCS(render, hairRenderIndex);
	}
	catch(...)
	{
		fprintf(stderr, "HAIR EXCEPTION!!!\n");
		fflush(stderr);
	}
	fet.stop();

	fprintf(stderr, "HairRenderProc Render: %d = %fsec \n", HairRenderProc_Render_Counter.evalcnt, HairRenderProc_Render_Counter.sumtime);
	fflush(stderr);
	
}

//////////////////////////////////////////////////
// ocellaris


//! GenOCS
bool HairRender::GenOCS(
	cls::IRender* render, 
	int hairRenderIndex
	)
{
	bool bDump = true;
	render->GetAttribute("@fur::dump", bDump);

	float widhtFactor = 1;
	render->GetAttribute("@fur::widhtFactor", widhtFactor);
//printf("widhtFactor=%f\n", widhtFactor);

	// ���� ������� �������
	bool bPruningFailed = false;
	bPruningFailed |= !render->GetAttribute("@camera::worldpos", hairSystem.camera_worldpos);
	bPruningFailed |= !render->GetParameter("worldtransform", hairSystem.this_worldpos);

	hairSystem.pruningScaleFactor = 1;
	render->GetAttribute("@pruningScaleFactor", hairSystem.pruningScaleFactor);
	float parentpruningScaleFactor = 1;
	render->GetAttribute("@parentpruningScaleFactor", parentpruningScaleFactor);
	hairSystem.pruningScaleFactor *= parentpruningScaleFactor;

	std::string currentpassname;
	render->GetAttribute("@pass", currentpassname);
	if( currentpassname.empty())
	{
		fprintf(stderr, "Attribute @pass is absent. Let's it be \"final\"\n");
		currentpassname = "final";
	}
	_strlwr( (char*)currentpassname.c_str());

	this->render = render;

	// �����
	bool checkWorkingFaces = !hairSystem.workingfaces.empty();

	bool bNonGroupedRender = false;
	if( checkWorkingFaces) 
		bNonGroupedRender = false;

	if( !hairSystem.splitGroups)
		bNonGroupedRender = true;

	///////////////////////////////////////
	// ��������� ��� �����
	std::vector<HairRenderPass*> aloned_passes, grouped_passes;
	aloned_passes.reserve(hairSystem.passes.size());
	grouped_passes.reserve(hairSystem.passes.size());
	for(int x=0; x<(int)hairSystem.passes.size(); x++)
	{
		HairRenderPass& pass = hairSystem.passes[x];

		// ��������� ����� �� ��������� ���� ������
		int pp;
		for(pp=0; pp<(int)pass.passes.size(); pp++)
			if( currentpassname == pass.passes[pp])
				break;
		if( pp>=(int)pass.passes.size())
			continue;

		if( pass.bPrune)
		{
			if( bPruningFailed)
			{
				fprintf(stderr, "\nfor pruning nesessery attributes @camera::worldpos and worldtransform");
				pass.bPrune = false;
			}
			else
			{
				pass.prune = CalcPruning(pass, true);
			}
		}

		if( pass.renderNormalSource == RNS_CLUMPVECTOR && 
			staticData->clumpPositions.getCount()==0)
		{
			fprintf(stderr, "HairMath: Hair pass %s: curve normal set to \"clumpVector\" but clumps is OFF!\n", pass.passName.c_str());
			fflush(stderr);
			pass.renderNormalSource = RNS_NONE;
			continue;
		}

		// RiDetailRange
		if( pass.bUseDetails)
		{
			grouped_passes.push_back(&pass);
			continue;
		}


		int count = pass.CalcCount(staticData->rootPositions.getCount(), staticData->clumpPositions.getCount());

		// ���� ������ 10 ����� � ������ � �������� ������� - ��������� �������
		if( (!pass.bPrune) && (count < this->getPolygonGroupCount()*10))
		{
			aloned_passes.push_back(&pass);
			continue;
		}

		if( (!pass.bPrune) && bNonGroupedRender)
		{
			aloned_passes.push_back(&pass);
		}
		else
		{
			grouped_passes.push_back(&pass);
		}
	}

	///////////////////////////////////////
	// Box ��� ����������
	Math::Box3f snbox(0);
	{
		std::map< int, sn::ShapeShortcutPtr >::iterator it = hairSystem.snshapes.begin();
		for( ; it != hairSystem.snshapes.end(); it++)
		{
			Math::Box3f _snbox;
			sn::ShapeShortcutPtr& snInst = it->second;
			snInst->shaper->getDirtyBox(snInst->shape, Math::Matrix4f::id, _snbox);
			snbox.insert(_snbox);
		}
	}

	///////////////////////////////////////
	// ��������� ��� �������
	if( !aloned_passes.empty())
	{
		this->GenGroupRIBOCS(render, hairRenderIndex, -1, snbox, aloned_passes);
	}
	///////////////////////////////////////
	// ��������� ������
	if( !grouped_passes.empty())
	{
		int gc = this->getPolygonGroupCount();
		for(int g=0; g<gc; g++)
		{
			//////////////////////////////////////////////
			// render
			if( checkWorkingFaces)
				if( hairSystem.workingfaces.find(g)==hairSystem.workingfaces.end())
					continue;

			this->GenGroupRIBOCS(render, hairRenderIndex, g, snbox, grouped_passes);
		}
	}
	return true;
}

void HairRender::GenGroupRIBOCS(
	cls::IRender* render, 
	int hairRenderIndex, 
	int group, 
	const Math::Box3f& snbox, 
	std::vector<HairRenderPass*>& passes
	)
{
	bool bDump = true;
	render->GetAttribute("@fur::dump", bDump);

	bool bRender = true;
	bool bRenderGroupBox = hairSystem.bRenderBoxes;

	Math::Box3f groupbox;
	float grMaxLen = 0;
	float grMaxDisplace = 0;

	// ������� �����
	int hairstartindex, hairendindex;
	this->getPolygonGroup(group, false, hairstartindex, hairendindex);

	int clumpstartindex, clumpendindex;
	this->getPolygonGroup(group, true, clumpstartindex, clumpendindex);

	if(hairstartindex==hairendindex && clumpstartindex==clumpendindex) 
		return;


	HairSystem& hairSystem = this->hairSystem;
	bool bInstanced = false;
	bInstanced |= hairSystem.snshapes.size()!=0;
	bInstanced |= hairSystem.ocsfiles.size()!=0;


	//////////////////////////////////////////////
	// ������ �����
	std::map<float, Dynamic*>::iterator it = this->deformblur.begin();
	for(;it != this->deformblur.end(); it++)
	{
		Dynamic& dynamic = *it->second;
		HairMesh* surface = &dynamic.surface;

		// ������ ���������� INCREASELENGTH
		std::vector<IHairDeformer*> deformers_INCREASELENGTH;
		int deformercount = surface->getDeformerCount();
		deformers_INCREASELENGTH.reserve(deformercount);
		for(int i=0; i<(int)deformercount; i++)
		{
			IHairDeformer* deformer = surface->getDeformer(i);
			if( !deformer) continue;
			int type = deformer->getType();
			if( type&IHairDeformer::INCREASELENGTH)
				deformers_INCREASELENGTH.push_back(deformer);
		}

		// DEBUG!!!!
		// ��� �������� ���� ����� ����������� �����

		// ������ ����������� ������
		for(int vert=hairstartindex; vert<hairendindex; vert++)
		{
			Math::Vec3f pos;
			float u, v;
			surface->getUVAtHair(vert, false, u, v);
			surface->getPosAtHair(vert, false, pos);

			float length = surface->getLengthHair(vert, false, deformers_INCREASELENGTH);
			float disp = surface->getDisplaceHair(vert, false);

			groupbox.insert(pos);
			grMaxDisplace = __max(grMaxDisplace, disp);
			grMaxLen = __max(grMaxLen, length);
		}
		for(int vert=clumpstartindex; vert<clumpendindex; vert++)
		{
			Math::Vec3f pos;
			float u, v;
			surface->getUVAtHair(vert, true, u, v);
			surface->getPosAtHair(vert, true, pos);
			float length = surface->getLengthHair(vert, true, deformers_INCREASELENGTH);
			float disp = surface->getDisplaceHair(vert, true);

			groupbox.insert(pos);
			grMaxDisplace = __max(grMaxDisplace, disp);
			grMaxLen = __max(grMaxLen, length);
		}
	}
	groupbox.expand(grMaxLen);
	groupbox.expand(grMaxDisplace);
	groupbox.min += snbox.min*grMaxLen;
	groupbox.max += snbox.max*grMaxLen;

	// ������ �����
	if( bRenderGroupBox && !hairSystem.bAccurateBoxes)
	{
		// ������ ����� � ������� �����
		float w = 0.01f;
		if( !passes.empty())
			w = passes[0]->RenderWidth;

		render->Parameter("#width", w);
		render->Parameter("#box", groupbox);
		render->RenderCall("Box");
		return;
	}


	////////////////////////////////////
	// RENDER
	if( bRender)
	{
		for(int x=0; x<(int)passes.size(); x++)
		{
			HairRenderPass& pass = *passes[x];

			// hairGroup
			bool bClump = pass.bRenderClumps;
			int startindex, endindex;
			if(bClump)
				startindex = clumpstartindex, endindex = clumpendindex;
			else
				startindex = hairstartindex, endindex = hairendindex;
			if( startindex==endindex)
				continue;

			HairRenderPass::enSource rendertype = bInstanced?HairRenderPass::RT_INSTANCES:HairRenderPass::RT_HAIRS;
			if(pass.source != HairRenderPass::RT_DEFAULT)
				rendertype = pass.source;
			if( rendertype==HairRenderPass::RT_INSTANCES && !bInstanced) 
				continue;

			render->PushRenderAttributes();

			/*/
				// RiDetailRange
				if( pass.bUseDetails)
				{
	//				prman->RiAttributeBegin();
					prman->RiDetail(bound);

					HairRenderPass::DetailRange& dr = pass.detailrange;
					if( dr.isVisible())
					{
						prman->RiDetailRange(
							dr.minvisible*dr.minvisible, 
							dr.lowertransition*dr.lowertransition, 
							dr.uppertransition*dr.uppertransition, 
							dr.maxvisible*dr.maxvisible);
					}
				}
			/*/

			if( pass.renderRayTraceEnable)
			{
				render->Attribute("visibility::camera", pass.renderRayCamera?1:0);
				render->Attribute("visibility::trace", pass.renderRayTrace?1:0);
				render->Attribute("visibility::photon", pass.renderRayPhoton?1:0);
			}

			int dicehair = pass.diceHair?1:0;
			if( pass.renderNormalSource!=RNS_NONE)
				dicehair = 0;
			render->Attribute("dice::hair", dicehair);

			int sigmaHiding = pass.sigmaHiding?1:0;
			if( sigmaHiding)
				render->Attribute("stochastic::sigma", sigmaHiding);

			// RiSides
			render->Attribute("sides", pass.renderDoubleSide?2:1);
			
			// reverseOrientation
			int reverseOrientation = pass.reverseOrientation?1:0;
			render->Attribute("reverseOrientation", reverseOrientation);

			render->PushAttributes();
			render->Parameter("@hairRender", hairRenderIndex);
			render->Parameter("@startindex", startindex);
			render->Parameter("@endindex", endindex);
			render->Parameter("@bClump", bClump);
			render->Parameter("@prune", pass.prune);
			render->Parameter("@rendertype", rendertype);
			render->Parameter("@mulWidthByLength", pass.mulWidthByLength);
			render->Parameter("@degradation", pass.degradation);
			render->Parameter("@degradationWidth", pass.degradationWidth);
			render->Parameter("@opacity", pass.opacity);
			render->Parameter("@renderNormalSource", pass.renderNormalSource);
			render->Parameter("@SegmCount", pass.SegmCount);
			render->Parameter("@interpolation", pass.interpolation);
			render->Parameter("@orthogonalize", hairSystem.orthogonalize);
			render->Parameter("@RenderWidth", pass.RenderWidth);
			render->Parameter("@useClumpRadiusAsWidth", pass.useClumpRadiusAsWidth);
			render->Parameter("@widthBias", pass.widthBias);
			render->Parameter("#bound", groupbox);
			render->Parameter("@delay", true);
#ifndef SAS
			render->RenderCall("HairMath.dll@HairRenderGroup");
#else
			render->RenderCall("chevelureMath.dll@HairRenderGroup");
#endif
			render->PopAttributes();

			render->PopRenderAttributes();

			this->addRef();
		}
	}

#ifdef OCS_MEMTEST
	fprintf(stderr, "cls::memory on exit: %d kb\n", (int)(cls::IParameter::memory(0)/1024));
#endif
}

