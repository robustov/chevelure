#pragma once

#include "hairmath.h"
#include "Util/Stream.h"
#include "mathNpixar/IPrman.h"
#include "supernova/ISnShaper.h"
#include "ocellaris/renderCacheImpl.h"

#pragma warning ( disable:4275)
#pragma warning ( disable:4251)

struct HAIRMATH_API HairPersonageData
{
	Math::Matrix4f transform;
	float length;

	std::vector<Math::Matrix4f> transforms;
	std::vector<float> parameters;
	float polar;
	Math::Vec2f inclination, baseCurl, tipCurl;
	float baseWidth, tipWidth, twist;
	float u, v;
	int instance;
	int id;
	float animation;

	HairPersonageData();
	HairPersonageData(const HairPersonageData& arg);
	HairPersonageData& operator = (const HairPersonageData& arg);
};
//! HairPersonage
struct HAIRMATH_API HairPersonage : public sn::IPersonage
{
	Math::Matrix4f transform;
	HairPersonageData data;
	sn::ShapeShortcutPtr ptr;
	IPrman* prman;
	cls::renderCacheImpl<> cache;
public:
	HairPersonage();
	void validateAttr(sn::attr_t& attr);
	virtual void getValue(sn::attr_t& attr, float& val);
	virtual void getValue(sn::attr_t& attr, std::vector<float>& val);
	virtual void getValue(sn::attr_t& attr, std::vector<Math::Matrix4f>& val);
};
