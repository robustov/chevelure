#include "stdafx.h"
#include "HAIRVERTS.h"

void HAIRVERTS::clear(int sizein_HAIRVERTS)
{
	clumpradius = 0;
	clump_normal = Math::Vec3f(0);
	clump_vector = Math::Vec3f(0);
	cvint.clear();
	vfactor.clear();
	basas.clear();
	vclump.clear();
	clump_vectorV.clear();
	vwidht.clear();
	pt_nonoise.clear();
	deformers_data.clear();

	if( sizein_HAIRVERTS)
		deformers_data.resize(sizein_HAIRVERTS);
};

bool HAIRVERTS::_isnan()
{
	if( ::_isnan(clumpradius))
		return true;
	if( ::_isnan( clump_normal))
		return true;
	if( ::_isnan( clump_vector))
		return true;
	unsigned int i=0;
	for(i=0; i<clump_vectorV.size(); i++)
		if( ::_isnan( clump_vectorV[i]))
			return true;
	for(i=0; i<vclump.size(); i++)
		if( ::_isnan( vclump[i]))
			return true;
	for(i=0; i<cvint.size(); i++)
		if( ::_isnan( cvint[i]))
			return true;
	for(i=0; i<vfactor.size(); i++)
		if( ::_isnan( vfactor[i]))
			return true;
	for(i=0; i<vwidht.size(); i++)
		if( ::_isnan( vwidht[i]))
			return true;
	for(i=0; i<basas.size(); i++)
		if( ::_isnan( basas[i]))
			return true;
	for(i=0; i<pt_nonoise.size(); i++)
		if( ::_isnan( pt_nonoise[i]))
			return true;
	for(i=0; i<deformers_data.size(); i++)
		if( ::_isnan( deformers_data[i]))
			return true;
	return false;
}
