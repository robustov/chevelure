#pragma once
#include "IHairDeformer.h"
#include <Util/Stream.h>
#include <Util\STLStream.h>
#include <Util\MemStream.h>

struct HairDeformerDecl
{
	Util::DllProcedureTmpl<getIHairDeformer> deformerproc;
	Util::MemStream deformerstream;
	IHairDeformer* deformer;
public:
	HairDeformerDecl()
	{
		deformer = NULL;
	}
	HairDeformerDecl(const HairDeformerDecl& arg)
	{
		deformer = NULL;
		*this = arg;
	}
	HairDeformerDecl& operator=(const HairDeformerDecl& arg)
	{
		this->deformerproc = arg.deformerproc;
		this->deformerstream = arg.deformerstream;
		if(arg.deformer)
			Load();
		return *this;
	}
	bool Load()
	{
		if( deformer)
			deformer->release();
		deformer = NULL;

		if( !deformerproc.isValid())
			return false;
		deformer = (*deformerproc)();
		if( deformer)
		{
			deformerstream.startReading();
			// �������
			deformerstream >> deformer->affect;
			deformer->serialise(deformerstream);
//			deformer->init();
			return true;
		}
		return false;
	}
	~HairDeformerDecl()
	{
		if( deformer)
			deformer->release();
		deformer = NULL;
	}
	#ifdef OCELLARIS
	void serializeOcs(const char* _name, cls::IRender* render, bool bSave);
	#endif
};


template <class STREAM>
STREAM& operator>>(STREAM& serializer, HairDeformerDecl &v)
{
	serializer >> v.deformerproc;
	v.deformerstream.serialize(serializer);
	if(serializer.isLoading())
	{
		v.Load();
	}
	return serializer;
}

#ifdef OCELLARIS
inline void HairDeformerDecl::serializeOcs(const char* _name, cls::IRender* render, bool bSave)
{
	std::string name = _name;

	SERIALIZEOCSPROC( deformerproc);
	SERIALIZEOCSPROC( deformerstream);
	if(!bSave)
	{
		this->Load();
	}
}
#endif
