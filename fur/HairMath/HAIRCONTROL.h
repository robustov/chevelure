#pragma once

//#include "HairMath.h"
#include "Math/Math.h"
#include "Math/Spline.h"
#include <vector>
//#include <Util/Stream.h>
//#include <Util\STLStream.h>

struct HAIRCONTROL
{
	bool bValid;
	// ������� � tangent space
	std::vector< Math::Vec3f> ts;
	// ������� � object space
	std::vector< Math::Vec3f> os;
	// ���������
	std::vector<float> vfactor;

	HAIRCONTROL(){clear();}
	void clear(){ts.clear();bValid=false;};
	// init
	void init(int vc);
	// empty
	bool empty() const {return !bValid;};
	// ����� ���������
	int vc()const {return (int)ts.size();}
	// �� ���������
	Math::Vec3f getPointByParam(float f, bool bTs=true, bool bSpline=true);

	// ��������� vfactor �� TS ��� OS
	bool calcVFactor(bool bTs=true);

	// ����� ������������ ������
	float getLength(bool bTs=false) const;

	// build TS from OS
	void buildTSfromOS(const Math::Matrix4f& basis_inv);

	void buildOSfromTS(const Math::Matrix4f& basis);
};

template<class Stream> inline
Stream& operator >> (Stream& out, HAIRCONTROL& v)
{
	int version = 0;
	out >> version;
	if( version>=0)
	{
		out >> v.bValid;
		out >> v.ts >> v.os >> v.vfactor;
	}
	return out;
}

inline float HAIRCONTROL::getLength(bool bTs) const
{
	// ������� � object space
//	std::vector< Math::Vec3f> os;
	float l=0;
	if( !bTs)
	{
		for(int i=1; i<(int)os.size(); i++)
		{
			l += (os[i-1]-os[i]).length();
		}
	}
	else
	{
		for(int i=1; i<(int)os.size(); i++)
		{
			l += (ts[i-1]-ts[i]).length();
		}
	}
	return l;
}

inline Math::Vec3f HAIRCONTROL::getPointByParam(float f, bool bTs, bool bSpline)
{
	if( this->vc()==0)
		return Math::Vec3f(0, 0, 0);

	if(bSpline)
	{
		if( bTs) 
			return Math::Spline3(f, &this->ts[0], &this->vfactor[0], (int)this->ts.size(), Math::Spline3f::BSpline);//CatmullRom);
		else
			return Math::Spline3(f, &this->os[0], &this->vfactor[0], (int)this->os.size(), Math::Spline3f::BSpline);//CatmullRom);
	}

	if(f<this->vfactor[0])
	{
		if( bTs) 
			return this->ts[0];
		else
			return this->os[0];
	}
	if(f>=this->vfactor[this->vc()-1])
	{
		if( bTs) 
			return this->ts[this->vc()-1];
		else
			return this->os[this->vc()-1];
	}

	for(int g=1; g<this->vc(); g++)
	{
		if( this->vfactor[g-1] <= f && f < this->vfactor[g])
		{
			float factor = (f-this->vfactor[g-1])/(this->vfactor[g]-this->vfactor[g-1]);
//			if( factor<0) factor=0;
//			if( factor>1) factor=1;
			if( bTs)
			{
				Math::Vec3f pt = (1-factor)*this->ts[g-1] + factor*this->ts[g];
				return pt;
			}
			else
			{
				Math::Vec3f pt = (1-factor)*this->os[g-1] + factor*this->os[g];
				return pt;
			}
		}
	}
	return Math::Vec3f(0, 0, 0);
}

inline void HAIRCONTROL::init(int vc)
{
	ts.resize(vc);
	os.resize(vc);
	vfactor.resize(vc);

	for(int v=0; v<vc; v++)
	{
		float y = v/(float)(vc-1);
		vfactor[v] = y;
		ts[v] = Math::Vec3f(0, y, 0);
		os[v] = Math::Vec3f(0, y, 0);
	}
}

// build TS from OS
inline void HAIRCONTROL::buildTSfromOS(const Math::Matrix4f& basis_inv)
{
	int vc = (int)os.size();
	ts.resize(vc);
	for(int i=0; i<vc; i++)
	{
		ts[i] = basis_inv*os[i];
		ts[i] = ts[i] - ts[0];
	}
}
// build OS from TS
inline void HAIRCONTROL::buildOSfromTS(const Math::Matrix4f& basis)
{
	int vc = (int)ts.size();
	os.resize(vc);
	for(int i=0; i<vc; i++)
	{
		os[i] = basis*os[i];
	}
}

// vfactor - �����
inline bool HAIRCONTROL::calcVFactor(bool bTs)
{
	float L=0;
	int s = (int)ts.size();
	Math::Vec3f* data = &ts[0];
	if(!bTs)
	{
		s = (int)os.size();
		data = &os[0];
	}

	vfactor[0] = 0;
	for(int i=1; i<(int)s; i++)
	{
		L += (data[i-1]-data[i]).length();
		vfactor[i] = L;
	}
	if( L<1e-8)
		return false;
	return true;
	/*/
	if( L<1e-8)
	{
		for(i=0; i<s; i++)
		{
			float f = i/(float)(s-1);
			vfactor[i] = f;
		}
		return false;
	}
	else
	{
		float l=0;
		for(i=0; i<s; i++)
		{
			if(i!=0)
				l += (data[i-1]-data[i]).length();
			float f = l/L;
			vfactor[i] = f;
		}
		return true;
	}
	/*/
}

