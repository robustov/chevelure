#include "stdafx.h"
#include "HairClumpPositions.h"

void HairClumpPositions::copy(const HairClumpPositions& arg)
{
	this->clumps =		arg.clumps;
	this->clumpgroups =	arg.clumpgroups;
	this->clumpseeds =	arg.clumpseeds;
}

HairClumpPositions& HairClumpPositions::operator=(const HairClumpPositions& arg)
{
	HairClumpPositions::copy(arg);
	return *this;
}

void HairClumpPositions::serialize(Util::Stream& stream)
{
	int version = 2;
	stream >> version;
	if( version>=0)
	{
		stream >> clumps;
		stream >> clumpgroups;
		if(version>=1)
		{
			stream >> clumpseeds;
		}
	}
	if( version>=2)
	{
		stream >> radius;
	}
}


void HairClumpPositions::clear()
{
	clumps.clear();
	clumpgroups.clear();
	clumpseeds.clear();
}

void HairClumpPositions::buildPolygonGroups(
	std::vector<HairGeometryIndices::facegroup>& clumpPolygonGroups, 
	const HairGeometryIndices& hgi)
{
	int c=0;
	int i;
	for(i=0; i<(int)hgi.faceCount(); i++)
		c = std::max(c, hgi.face_polygonIndex(i)+1);
	
	clumpPolygonGroups.resize(c);
	for(i=0; i<(int)hgi.faceCount(); i++)
	{
		if(i>=(int)clumpgroups.size()) break;
		HairGeometryIndices::facegroup& src = clumpgroups[i];
		int p = hgi.face_polygonIndex(i);
		HairGeometryIndices::facegroup& dst = clumpPolygonGroups[ p];
		if( dst.endindex==dst.startindex)
			dst = src;
		else
		{
			dst.startindex = std::min(dst.startindex, src.startindex);
			dst.endindex = std::max(dst.endindex, src.endindex);
		}
	}
}
/*/
//! ����� ������ � ������� ��� ������
void HairClumpPositions::findClumpGroupNPolygon(
	int h, 
	HairGeometryIndices& geometryIndices,
	int& group,
	int& polygon)
{
	for( int i=0; i<getGroupCount(); i++)
	{
		int start, end;
		getGroup(i, start, end);
		if(h<start || h>=end) 
			continue;
		group = i;
		polygon = geometryIndices.face_polygonIndex(group);
		return;
	}
}
/*/

