#include "stdafx.h"
#include "HairSystem.h"
#include "Util/FileStream.h"
#include "Util/misc_create_directory.h"

HairSystem::HairSystem()
{
	clear();
}

void HairSystem::clear()
{
	snshapes.clear();
	passes.clear();
	workingfaces.clear();
	splitGroups = true;
	bRenderBoxes = false;
	bAccurateBoxes = false;

	Math::Matrix4f camera_worldpos = Math::Matrix4f::id;
	Math::Matrix4f this_worldpos = Math::Matrix4f::id;

	dumpInformation = true;
	pruningScaleFactor = 1;
}

void HairSystem::serialize(Util::Stream& out)
{
	int version = 19;
	out >> version;
	if( version>=7)
	{
		// распределение
		out >> SegmCount;// >> Length >> Jitter;

		// SN
		out >> snshapes;

		out >> CVtailU >> CVtailV;

		out >> mulWidthByLength;

		out >> *(int*)&orthogonalize;

		out >> passes;
	}
	if(version>=8)
	{
		// те полигоны которые рендерятся!!!
		out >> workingfaces;
	}
	if(version>=9)
	{
		out >> splitGroups;
	}
	if(version>=10)
	{
		out >> camera_worldpos;
		out >> this_worldpos;
	}
	if(version>=11)
	{
		out >> bRenderBoxes;
		out >> bAccurateBoxes;
	}
	if(version>=12)
	{
		out >> freezefile;
	}
	if(version>=13)
	{
		out >> dumpInformation;
	}
	if(version>=14)
	{
		std::vector< weight_t> _snshapes_weight;
		out >> _snshapes_weight;
	}
	if(version>=15)
	{
		out >> ocsfiles;

		// Загрузить
		if(out.isLoading())
		{
			std::map< int, cls::MInstance>::iterator it = ocsfiles.begin();
			for(; it != ocsfiles.end(); it++)
			{
				cls::MInstance& inst = it->second;
				inst.LoadForRender(inst.getFile());
			}
		}
	}
	if(version>=16)
	{
		out >> snshapes_weight;
	}
	if(version>=17)
	{
		out >> passforocs;
	}
	if(version>=18)
	{
		out >> currentpassname;
	}
	if(version>=19)
	{
		out >> pruningScaleFactor;
	}
	
}
void HairSystem::serializeOcs(
	const char* _name, 
	cls::IRender* render, 
	bool bSave,
	const char* dir,
	const char* shapename
	)
{
	std::string name = _name;

	SERIALIZEOCS( SegmCount);
	if( !snshapes.empty())
		fprintf(stderr, "\n\n!!!ocs dont support snshapes!!!\n\n");
//	SERIALIZEOCSPROCARRAY( snshapes);
	SERIALIZEOCS( CVtailU);
	SERIALIZEOCS( CVtailV);
	SERIALIZEOCS( mulWidthByLength);
	SERIALIZEOCSENUM( orthogonalize);

/*/
#ifdef OCELLARIS_INSTANCE
			out >> ocsfiles;
			// Загрузить
			if(out.isLoading())
			{
				std::map< int, cls::MInstance>::iterator it = ocsfiles.begin();
				for(; it != ocsfiles.end(); it++)
				{
					cls::MInstance& inst = it->second;
					inst.LoadForRender();
				}
			}
#endif//OCELLARIS_INSTANCE
/*/
	SERIALIZEOCSPROCARRAY(passes);
	SERIALIZEOCS( workingfaces);
	SERIALIZEOCS( splitGroups);
	SERIALIZEOCS( camera_worldpos);
	SERIALIZEOCS( this_worldpos);
	SERIALIZEOCS( bRenderBoxes);
	SERIALIZEOCS( bAccurateBoxes);
	SERIALIZEOCS( freezefile);
	SERIALIZEOCS( dumpInformation);

	if(bSave)
	{
		if( dir && shapename)
		{
			std::vector< int> sn_index(this->snshapes.size() );
			std::vector< std::string > sn_files(this->snshapes.size() );
			std::map< int, sn::ShapeShortcutPtr>::iterator itsn = this->snshapes.begin();
			for( int sn=0; itsn != this->snshapes.end(); itsn++, sn++)
			{
				sn_index[sn] = itsn->first;
	//			sn_files[sn] = itsn->second;

				std::string filename = dir;
				filename += "\\";
				filename += shapename;
				char buf[24];
				filename += _itoa(itsn->first, buf, 10);
				filename += ".sn";

				Util::correctFileName(filename);
				Util::changeSlashToUnixSlash(filename);
				Util::create_directory_for_file(filename.c_str());
				Util::FileStream file;
				if( !file.open(filename.c_str(), true))
					continue;

				file >> itsn->second;
				sn_files[sn] = filename;
			}
			render->Parameter( (name+"::sn_index").c_str(), sn_index);
			render->Parameter( (name+"::sn_files").c_str(), sn_files);
		}
	}
	else
	{
		std::vector< int> sn_index;
		std::vector< std::string > sn_files;
		render->GetParameter( (name+"::sn_index").c_str(), sn_index);
		render->GetParameter( (name+"::sn_files").c_str(), sn_files);

		for( int sn=0; sn<(int)sn_index.size(); sn++)
		{
			Util::FileStream file;
			if( !file.open(sn_files[sn].c_str(), false))
				continue;
			file >> this->snshapes[sn_index[sn]];
		}
	}

	if(bSave)
	{
		std::vector< int> ocs_index(this->ocsfiles.size() );
		std::vector< std::string > ocs_files(this->ocsfiles.size() );
		std::map< int, cls::MInstance>::iterator itsn = this->ocsfiles.begin();
		for( int sn=0; itsn != this->ocsfiles.end(); itsn++, sn++)
		{
			cls::MInstance& inst = itsn->second;
			const char* filename = inst.getFile();
			ocs_index[sn] = itsn->first;
			ocs_files[sn] = filename?filename:"";
		}
		render->Parameter( (name+"::ocs_index").c_str(), ocs_index);
		render->Parameter( (name+"::ocs_files").c_str(), ocs_files);
	}
	else
	{
		std::vector< int> ocs_index;
		std::vector< std::string > ocs_files;
		render->GetParameter( (name+"::ocs_index").c_str(), ocs_index);
		render->GetParameter( (name+"::ocs_files").c_str(), ocs_files);

		for( int sn=0; sn<(int)ocs_index.size(); sn++)
		{
			cls::MInstance& inst = this->ocsfiles[ ocs_index[sn]];
			if( !inst.LoadForRender( ocs_files[sn].c_str()))
				fprintf(stderr, "\nCant load osc %s\n", ocs_files[sn].c_str());
		}
	}

	{
		if( bSave)
		{
			std::vector< int> indecies;
			indecies.reserve(snshapes_weight.size() );
			std::map< int, weight_t>::iterator it = snshapes_weight.begin();
			for( ; it != snshapes_weight.end(); it++)
			{
				indecies.push_back( it->first);
				char attrname[1024];
				_snprintf(attrname, 1024, "%s::snshapes_weight_%d_", name.c_str(), it->first);

				it->second.serializeOcs( attrname, render, bSave);
			}
			render->Parameter( (name+"::snshapes_weight_index").c_str(), indecies);
		}
		else
		{
			std::vector< int> indecies;
			render->GetParameter( (name+"::snshapes_weight_index").c_str(), indecies);
			for(int i=0; i<(int)indecies.size(); i++)
			{
				int ind = indecies[i];
				char attrname[1024];
				_snprintf(attrname, 1024, "%s::snshapes_weight_%d_", name.c_str(), ind);

				weight_t& w = snshapes_weight[ind];
				w.serializeOcs( attrname, render, bSave);
			}
		}
	}

}

