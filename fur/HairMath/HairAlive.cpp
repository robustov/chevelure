#include "stdafx.h"
#include "HairAlive.h"

void HairAlive::resize(int count)
{
	if(this->size != count)
	{
		this->size = count;
		int sz = (count+7)/8; 
		bitMask.resize(sz, 0xFF);
	}
}

HairAlive::HairAlive()
{
	size = 0;
}
HairAlive::HairAlive(const HairAlive& arg)
{
	size	= arg.size;
	bitMask = arg.bitMask;
}
HairAlive& HairAlive::operator =(const HairAlive& arg)
{
	size	= arg.size;
	bitMask = arg.bitMask;
	return *this;
}
void HairAlive::serialize(Util::Stream& stream)
{
	int version = 0;
	if( version>=0)
	{
		stream >> size;
		stream >> bitMask;
	}
}

