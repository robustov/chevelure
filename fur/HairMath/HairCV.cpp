#include "stdafx.h"
#include "HairCV.h"
#include "HairGeometry.h"
#include "HairMesh.h"
#include "HairProcessor.h"

extern float min_hairLen;

// � ������ ����������
bool HairCV::init(
	int CVcount, 
	HairCVPositions* dst_cvpos, 
	HairMesh& meshsurface
	)
{
	HairCVPositions* cvp = dst_cvpos;
	HairGeometry* g = meshsurface.geometry;
	HairGeometryIndices* gi = meshsurface.geometryIndices;

	this->cvcount = CVcount;
	this->cvs.resize(cvp->controlhairCount());

	std::map< int, sn::ShapeShortcutPtr> snshapes;
	std::map< int, cls::MInstance> ocsfiles;
	std::map< int, HairProcessor::weight_t> snshapes_weight;

	HairProcessor processor( CVcount-1, meshsurface, snshapes, ocsfiles, snshapes_weight, false, ORT_NONE, false, false);
	HAIRPARAMS params;
	HAIRVERTS hair;

	int chc = cvp->controlhairCount();
	for(int h=0; h<chc; h++)
	{
		Math::Matrix4f m;
		this->getControlHairBasisSrc(
			h, cvp, gi, g, m);
		Math::Matrix4f m_inv = m.inverted();

		// Bary ��� �����
		const int* faceverts; 
		int facevertcount;
		cvp->getFacevertsListByGuide(h, faceverts, facevertcount, gi);
		if( facevertcount<0) continue;
		int fv = faceverts[0];
		int faceindex = fv/3; 
		int vinface = fv-faceindex*3;
		Math::Vec3f bary(0);
		bary[vinface] = 1.f;

		// ������ ������ ��� ������
		params.uvSets.clear();
		Math::Vec2f uv = g->src_getUV(faceindex, bary, gi);

		params.normal = m[1].as<3>();
		params.tangentU = m[0].as<3>();
		params.tangentV = m[2].as<3>();
		params.position = m[3].as<3>();


		processor.ParseHair(params, uv.x, uv.y, false, true);
		params.blendCVfactor = 0;
		params.blendDeformfactor = 1;
		params.displace = 0;
		params.scraggle = 0;

		processor.CreateHairDefault(params, hair, false);
		if( !processor.DeformAloneHair(params, hair, false))
			continue;

		// �������� hair � ����������� �����
		HairCV::CV& outch = this->getControlHair(h);
		outch.clear();
		outch.ts.resize(CVcount);
		for(int v=0; v<CVcount; v++)
		{
			Math::Vec3f os = hair.cvint[v];
			outch.ts[v] = m_inv*os;
		}
		outch.bValid = true;
	}

	return true;
}

// ��� ����� ����������
bool HairCV::init(
	int CVcount, 
	HairCVPositions* pos
	)
{
	this->cvcount = CVcount;
	this->cvs.resize(pos->controlhairCount());

	int chc = pos->controlhairCount();
	for(int i=0; i<(int)chc; i++)
	{
		HairCV::CV& outch = this->getControlHair(i);
		outch.clear();
		outch.ts.resize(CVcount);
		for(int v=0; v<CVcount; v++)
		{
			int ind = i*CVcount + v;
			float y = v/(float)(CVcount-1);
			outch.ts[v] = Math::Vec3f(0, y, 0);
		}
		outch.bValid = true;
	}

	return true;
}

//! �������� ������������
void HairCV::linear_interpolation(const HairCV& cv1, const HairCV& cv2, float f1, float f2)
{
	int hcount = __max(cv1.getCount(), cv2.getCount());
	this->cvcount = __max(cv1.cvcount, cv2.cvcount);

	cvs.resize(hcount);

	for(int h=0; h<this->getCount(); h++)
	{
		HairCV::CV& ch = this->getControlHair(h);
		ch.clear();
		ch.ts.resize(this->cvcount);
		for(int v=0; v<this->cvcount; v++)
		{
			float y = v/(float)(this->cvcount-1);
			ch.ts[v] = Math::Vec3f(0, y, 0);
		}
		ch.bValid = true;

		if(h>=cv2.getCount())
		{
			ch = cv1.getControlHair(h);
			continue;
		}
		if(h>=cv1.getCount())
		{
			ch = cv2.getControlHair(h);
			continue;
		}

		const HairCV::CV& ch1 = cv1.getControlHair(h);
		const HairCV::CV& ch2 = cv2.getControlHair(h);
		if( ch2.vc()!=ch1.vc())
			continue;

		for( int v=0; v<this->cvcount; v++)
		{
			if(v>=ch1.vc() || v>=ch2.vc())
				continue;
			ch.ts[v] = ch1.ts[v]*f1 + ch2.ts[v]*f2;
		}
	}
}

// �������� �� ������ �� �����
bool HairCV::import(
	std::vector< std::vector<Math::Vec3f> >& shavepoints, 
	HairGeometryIndices* gi, 
	HairGeometry* g, 
	HairCVPositions* cvp
	)
{
	// � HairCV::import
	for( int h=0; h< (int)cvp->controlhairCount(); h++)
	{
		int vert = cvp->getVertexByGuide(h);
		Math::Vec3f v = g->src_vert(vert);

		// ����� ��������� �� shavepoints[.][0];
		float dmin = (float)1e32;
		int find = -1;
		for(int i=0; i<(int)shavepoints.size(); i++)
		{
			std::vector<Math::Vec3f>& shaveguide = shavepoints[i];
			if(shaveguide.empty())
				continue;
			Math::Vec3f gv = shavepoints[i][0];
			float d = (gv-v).length();
			if(d<dmin)
			{
				dmin = d;
				find = i;
			}
		}
		if(find<0) 
			continue;
		if(dmin>0.0001) 
			continue;
		std::vector<Math::Vec3f>& array = shavepoints[find];

		// �����
		Math::Matrix4f m;
		bool res = HairCV::getControlHairBasisSrc(h, cvp, gi, g, m);
		if( !res) 
			continue;
		m.invert();

		HAIRCONTROL srchc;
		srchc.init( (int)array.size());
		srchc.os = array;
		float len = srchc.getLength(false);
		bool bValid = srchc.calcVFactor(false);

		HairCV::CV& dsth = this->getControlHair(h);

		// ������� � controlPoints
		for(int v=0; v<cvcount; v++)
		{
			Math::Vec3f vert(0);
			if( bValid)
			{
				float f = len*v/(float)(cvcount-1);
				vert = srchc.getPointByParam(f, false);
				vert = m*vert;
				if(v==0)
					vert = Math::Vec3f(0);
			}
			dsth.ts[v] = vert;
		}
	}
	return true;
}
// �������� �� ������ �� �����
bool HairCV::export_(
	std::vector< std::vector<Math::Vec3f> >& shavepoints, 
	int cvinhair,
	HairGeometryIndices* gi, 
	HairGeometry* g,
	HairCVPositions* cvp
	)
{
	shavepoints.resize(cvp->controlhairCount());
	
	// � HairCV::import
	for( int h=0; h<(int)cvp->controlhairCount(); h++)
	{
		int vert = cvp->getVertexByGuide(h);
		Math::Vec3f first = g->src_vert(vert);

		std::vector<Math::Vec3f>& dst = shavepoints[h];
		dst.resize(cvinhair);

		// �����
		Math::Matrix4f m;
		bool res = HairCV::getControlHairBasisSrc(h, cvp, gi, g, m);
		if( !res) 
			continue;
//		m.invert();

		HairCV::CV& src = this->getControlHair(h);

		HAIRCONTROL srchc;
		srchc.init( (int)src.ts.size() );
		srchc.ts = src.ts;
		srchc.buildOSfromTS(m);
		float len = srchc.getLength(false);
		bool bValid = srchc.calcVFactor(false);

		for(int i=0; i<cvinhair; i++)
		{
			float f = i/(float)(cvinhair-1);
			Math::Vec3f v = srchc.getPointByParam(f, false, true);
			dst[i] = v;
		}
		dst[0] = first;
	}
	return true;
}

// ������� � ��������
bool HairCV::export_(
	Math::matrixMxN<Util::rgb>& rgb, 
	const char* what,			// ����� ��������
	int res,					// ����������
	HairGeometryIndices* geometryIndices, 
	HairGeometry* geometry,
	HairCVPositions* cvpositions
	)const
{
	rgb.init(res, res);

	int attr = -1;
	if( _strcmpi(what, "polar")==0) attr = 0;
	if( _strcmpi(what, "lenght")==0) attr = 1;
	if( _strcmpi(what, "tipcurl")==0) attr = 2;
	if( _strcmpi(what, "basecurl")==0) attr = 3;
	if(attr==2 || attr==3)
	{
		fprintf(stderr, "HairCV::export not implemented for tipcurl and basecurl!!!\n");
		return false;
	}

	float maxvalue = 0;
	Math::matrixMxN<float> texture;
	texture.init(res, res);
	for(int x=0; x<res; x++)
	{
		for(int y=0; y<res; y++)
		{
			Math::Vec2f uv((x+0.5f)/res, (y+0.5f)/res);
			HairGeometryIndices::pointonsurface pos;
			pos.faceindex = geometry->findUvFace(uv, geometryIndices, pos.b);
			if(pos.faceindex<0)
				continue;

			float value = 0;

			// CV
			HAIRCONTROL CV;
			this->buildControlHair(pos, cvpositions, geometryIndices, geometry, CV);

			// ��������� � ts
			Math::Matrix4f basis;
			geometry->getBasis(pos.faceindex, pos.b, geometryIndices, basis);
			basis.invert();
//			CV.buildTSfromOS(basis);

			switch(attr)
			{
				// polar
				case 0:
				{
					// ���� �� 0� ������ � ts
					Math::Vec2f vpolar(0);
					for( int i=1; i<(int)CV.ts.size(); i++)
					{
						Math::Vec3f v = CV.ts[i] - CV.ts[0];
						vpolar = tangentSpace2_2d(v);
						if( vpolar.length()<0.0001) continue;
						break;
					}

					// ������������� � Math::Vec2f
					float angle = vector2polar(vpolar);

					value = angle; // ��� -angle
					break;
				}
				// lenght
				case 1:
				{
					float l = CV.getLength(false);
					value = l;
					maxvalue = __max(maxvalue, value);
					break;
				}
				// tipcurl
				case 2:
				{
					// ���� ������� � �������� ������
					value = 1;
					maxvalue = __max(maxvalue, value);
					break;
				}
				// basecurl
				case 3:
				{
					value = 1;
					maxvalue = __max(maxvalue, value);
					break;
				}
			}
			texture[y][x] = value;
		}
	}
	if( attr==0)
		maxvalue = 1;
	float normalfactor = 1/maxvalue;

	for(int x=0; x<res; x++)
	{
		for(int y=0; y<res; y++)
		{
			float value = texture[y][x]*normalfactor;
			unsigned char c = (unsigned char)(__max( __min(value*255, 255), 0));
			rgb[y][x] = Util::rgb(c, c, c);
		}
	}
	return true;
}

// ����������� ����� �� ���������������� �����������
bool HairCV::buildControlHair( 
	const HairGeometryIndices::pointonsurface& pos,
	const HairCVPositions* cvPositions, 
	const HairGeometryIndices* geometryIndices,
	const HairGeometry* geometry,
	HAIRCONTROL& CV
	) const
{
	// DEBUG!!! ���� ��� - � ������ ���������� ����!!!!
	{
		const Math::Vec3f& bary = pos.b;

		Math::Face32& face = geometryIndices->fvface(pos.faceindex);
		Math::Face<int> chindex;
		chindex.v[0] = cvPositions->getGuideByFacevertex(face.v[0], geometryIndices);
		chindex.v[1] = cvPositions->getGuideByFacevertex(face.v[1], geometryIndices);
		chindex.v[2] = cvPositions->getGuideByFacevertex(face.v[2], geometryIndices);
		if(chindex.v[0]<0 || chindex.v[1]<0 || chindex.v[2]<0)
			return false;
		int chc = this->getCount();
		if(chindex.v[0]>=chc || chindex.v[1]>=chc || chindex.v[2]>=chc)
			return false;

		const HairCV::CV* hc[3];
		hc[0] = &this->getControlHair(chindex.v[0]);
		hc[1] = &this->getControlHair(chindex.v[1]);
		hc[2] = &this->getControlHair(chindex.v[2]);

		Math::Matrix4f tm[3];
		HairCV::getControlHairBasis(chindex.v[0], cvPositions, geometryIndices, geometry, tm[0]);
		HairCV::getControlHairBasis(chindex.v[1], cvPositions, geometryIndices, geometry, tm[1]);
		HairCV::getControlHairBasis(chindex.v[2], cvPositions, geometryIndices, geometry, tm[2]);
		

		// ������������!!!
		bool bInterpolation = true;
		if(bInterpolation)
		{
			int vc = hc[0]->vc();
			if(hc[1]->vc()!=vc || hc[2]->vc()!=vc)
				return false;
			CV.init( vc );
			for(int i=0; i<vc; i++)
			{
				CV.ts[i] = bary.x*hc[0]->ts[i] + bary.y*hc[1]->ts[i] + bary.z*hc[2]->ts[i];
				Math::Vec3f os[3];
				os[0] = tm[0] * hc[0]->ts[i];
				os[1] = tm[1] * hc[1]->ts[i];
				os[2] = tm[2] * hc[2]->ts[i];
				CV.os[i] = bary.x*os[0] + bary.y*os[1] + bary.z*os[2];
			}
			CV.bValid = true;
		}
		else
		{
			// ���� ����� ���������
			int closest = 0;
			if(bary.v[1]>bary.v[closest])
				closest = 1;
			if(bary.v[2]>bary.v[closest])
				closest = 2;
			
			int vc = hc[closest]->vc();
			CV.init( vc );
			for(int i=0; i<vc; i++)
			{
				CV.ts[i] = hc[closest]->ts[i];
				CV.os[i] = tm[closest]*CV.ts[i];
			}
			CV.bValid = true;
		}
		CV.calcVFactor(false);
		return true;
	}


	// ��������� �������������� �����
//	bool res = cv->getControlHair(pos->faceindex, pos->b, CV, cvPositions, geometryIndices);
//	if( !res) 
//		return false;
	return true;
}

// ��������� � ��������
bool HairCV::selectVertecies(
	std::vector< Math::Matrix4f>& bases,
	const Math::Matrix4f& worldpos,		// ������� ���������� ����� � WS
	const Math::Matrix4f& viewMatrix,	// view 
	const Math::Matrix4f& projMatrix,	// projection
	Math::Vec2f& combPosPS,				// ������� �������� (projection space)
	float radiusPS,						// ������ �������� (projection space)
	std::list<int>& selectedverts		// ���������
	)
{
	selectedverts.clear();

	// ������������� ��� � projection space � ��������� �� ��������� � ��������
	for( int i=0; i<(int)cvs.size(); i++)
	{
		CV& cv = cvs[i];
		Math::Matrix4f& basis = bases[i];

		for( int v=0; v<(int)cv.ts.size(); v++)
		{
			Math::Vec3f vert = cv.ts[v];

			// to OS
			vert = basis*vert;
			// to WS
			vert = worldpos*vert;
			// to View Space
			vert = viewMatrix*vert;
			Math::Vec4f vert4(vert.x, vert.y, vert.z, 1);
			// to projection space
			vert4 = vert4*projMatrix;
			vert4 *= 1/vert4.w;

			Math::Vec2f vert2(vert4.x, vert4.y);

			float len = (vert2-combPosPS).length();

			if(len<radiusPS)
			{
				// ������
				int cvcount = controlVertexCount();
				int vindex = cvcount*i + v;
				selectedverts.push_back(vindex);
			}
		}
	}
	return !selectedverts.empty();
}


// ��������������� �������
Math::Vec3f* HairCV::getSingleIndexedVertex(
	int v, 
	int* hairindex)
{
	if( v<0) return NULL;
	int cvcount = controlVertexCount();
	int h = v/cvcount;
	v = v-h*cvcount;
	if( h>=getCount())
		return NULL;
	HairCV::CV& hc = getControlHair(h);
	if( v>=hc.vc())
		return NULL;
	if( hairindex) 
		*hairindex = h;
	return &hc.ts[v];
}
// 1-�� ��������������� �������
bool HairCV::getSingleIndexedVertex(
	int v, int& hairindex, int& vertindex)
{
	if( v<0) return false;
	int cvcount = controlVertexCount();
	int h = v/cvcount;
	v = v-h*cvcount;
	if( h>=getCount())
		return false;
	HairCV::CV& hc = getControlHair(h);
	if( v>=hc.vc())
		return false;

	hairindex = h;
	vertindex = v;
	return true;
}


bool HairCV::getControlHairBasis(
	int h, 
	const HairCVPositions* cvp, 
	const HairGeometryIndices* gi,
	const HairGeometry* g,
	Math::Matrix4f& m
	)
{
	if(h>=cvp->controlhairCount())
		return false;

	m = Math::Matrix4f::id;

	int v = cvp->getVertexByGuide(h);
	const int* faceverts; 
	int facevertcount;
	cvp->getFacevertsListByGuide(h, faceverts, facevertcount, gi);

	Math::Vec3f P = g->vert(v);
	Math::Vec3f normal(0), tangentU(0), tangentV(0);
	for(int f=0; f<facevertcount; f++)
	{
		int fv = faceverts[f];
		if(fv>=(int)gi->facevertexCount())
			return false;
		normal += g->FV_normal(fv, gi);
		tangentU += g->FV_tangentU(fv, gi);
		tangentV += g->FV_tangentV(fv, gi);
	}

	normal.normalize();
	float du = Math::dot(tangentU, normal);
	tangentU -= du*normal;
	tangentU.normalize();

	float dv = Math::dot(tangentV, normal);
	tangentV -= dv*normal;
	float dvByu = Math::dot(tangentV, tangentU);
	tangentV -= dvByu*tangentU;
	tangentV.normalize();

	m = Math::Matrix4f::id;
	m[0] = tangentU.as<4>();
	m[1] = normal.as<4>();
	m[2] = tangentV.as<4>();
	m[3] = Math::Vec4f(P, 1);
	return true;
}

bool HairCV::getControlHairBasisSrc(
	int h, 
	const HairCVPositions* cvp, 
	const HairGeometryIndices* gi,
	const HairGeometry* g,
	Math::Matrix4f& m
	)
{
	if(h>=cvp->controlhairCount())
		return false;

	m = Math::Matrix4f::id;

	int v = cvp->getVertexByGuide(h);
	const int* faceverts; 
	int facevertcount;
	cvp->getFacevertsListByGuide(h, faceverts, facevertcount, gi);

	Math::Vec3f P = g->src_vert(v);
	Math::Vec3f normal(0), tangentU(0), tangentV(0);
	for(int f=0; f<facevertcount; f++)
	{
		int fv = faceverts[f];
		if(fv>=(int)gi->src_facevertexCount())
			return false;
		normal += g->src_FV_normal(fv, gi);
		tangentU += g->src_FV_tangentU(fv, gi);
		tangentV += g->src_FV_tangentV(fv, gi);
	}

	normal.normalize();
	float du = Math::dot(tangentU, normal);
	tangentU -= du*normal;
	tangentU.normalize();

	float dv = Math::dot(tangentV, normal);
	tangentV -= dv*normal;
	float dvByu = Math::dot(tangentV, tangentU);
	tangentV -= dvByu*tangentU;
	tangentV.normalize();

	m = Math::Matrix4f::id;
	m[0] = tangentU.as<4>();
	m[1] = normal.as<4>();
	m[2] = tangentV.as<4>();
	m[3] = Math::Vec4f(P, 1);
	return true;
}
// �����
Math::Vec2f HairCV::getControlHairUV(
	int h, 
	const HairCVPositions* cvp, 
	const HairGeometryIndices* gi,
	const HairGeometry* g
	)
{
	if(h>=cvp->controlhairCount())
		return Math::Vec2f(0);

	int v = cvp->getVertexByGuide(h);
	const int* faceverts; 
	int facevertcount;
	cvp->getFacevertsListByGuide(h, faceverts, facevertcount, gi);

	Math::Vec3f P = g->src_vert(v);
	Math::Vec3f normal(0), tangentU(0), tangentV(0);
	for(int f=0; f<facevertcount; f++)
	{
		int fv = faceverts[f];
		if(fv>=(int)gi->src_facevertexCount())
			continue;
		Math::Vec2f uv = g->src_FV_UV(fv, gi);
		return uv;
	}
	return Math::Vec2f(0);
}



void HairCV::copy(const HairCV& arg)
{
	this->cvs =	    arg.cvs;
	this->cvcount = arg.cvcount;
//	this->clumpgroups =	    arg.clumpgroups;
}

HairCV& HairCV::operator=(const HairCV& arg)
{
	HairCV::copy(arg);
	return *this;
}

void HairCV::serialize(Util::Stream& stream)
{
	int version = 0;
	stream >> version;
	if( version>=0)
	{
		stream >> cvs;
		stream >> cvcount;
//		stream >> clumpgroups;
	}
}
void HairCV::serializeOcs(const char* _name, cls::IRender* render, bool bSave)
{
	std::string name = _name;
	SERIALIZEOCS( cvcount);

	if(bSave)
	{
		cls::PA<int> vertsincv(cls::PI_CONSTANT, (int)cvs.size());
		int i=0; 
		int sum=0;
		for( i=0; i<(int)cvs.size(); i++)
		{
			vertsincv[i] = (int)cvs[i].ts.size();
			sum += vertsincv[i];
		}
		cls::PA<Math::Vec3f> P(cls::PI_CONSTANT);
		P.reserve(sum);
		for( i=0; i<(int)cvs.size(); i++)
		{
			for(int v=0; v<(int)cvs[i].ts.size(); v++)
			{
				P.push_back( cvs[i].ts[v]);
			}
		}
		render->Parameter( (name+"::vertsincv").c_str(),vertsincv);
		render->Parameter( (name+"::P").c_str(), P);
	}
	else
	{
		cls::PA<int> vertsincv = render->GetParameter( (name+"::vertsincv").c_str());
		cls::PA<Math::Vec3f> P = render->GetParameter( (name+"::P").c_str());
		cvs.resize(vertsincv.size());

		int i=0; 
		int sum=0;
		for( i=0; i<(int)cvs.size(); i++)
		{
			cvs[i].bValid = true;
			cvs[i].ts.resize(vertsincv[i]);
			for(int v=0; v<vertsincv[i]; v++)
			{
				if(sum>=P.size())
					break;
				cvs[i].ts[v] = P[sum];
				sum++;
			}
		}
	}
}

void HairCV::clear()
{
	cvs.clear();
	cvcount = 0;
//	clumpgroups.clear();
}
