#pragma once

#pragma warning( disable : 4251)

//#include "pre.h"
#include <map>
#include "hairmath.h"
#include "Util/Stream.h"
#include "supernova/ISnShaper.h"
#include "DrawSynk.h"
#include "IHairDeformer.h"
#include "HAIRPARAMS.h"
#include "HAIRVERTS.h"
#include "HairMesh.h"

class HairMesh;
class HairProcessor;
class Stream;

//! HairProcessor
//! ����� ��������� ����� ��� ������� ������!
class HAIRMATH_API HairProcessor
{
public:
	HairMesh* surface;
	int SegmCount;
	bool mulWidthByLength;
	enOrthogonalizeType orthogonalize;
//	std::vector<IHairDeformer*> deformers;
	bool bUseAddUV;
	bool bHairIsClump;

	std::vector<IHairDeformer*> deformers_INCREASELENGTH;
	std::vector<IHairDeformer*> deformers_PREPARSEHAIR;
	std::vector<IHairDeformer*> deformers_PARSEHAIR;
	std::vector<IHairDeformer*> deformers_BUILDHAIR;
	std::vector<IHairDeformer*> deformers_MERGEWITHCLUMP;
	std::vector<IHairDeformer*> deformers_POSTBUILDHAIR;
	std::vector<IHairDeformer*> deformers_DEFORMHAIRVERTEX;
	int sizein_HAIRPARAMS;
	int sizein_HAIRVERTS;

	// ��������
	struct InstanceData
	{
		// ����� ��������
		int index;	
		// ��� (�� NULL)
		Math::ParamTex2d<unsigned char, Math::EJT_MUL>* instanceweights;
	};
	std::vector< InstanceData> instanceDatas;

public:
	typedef Math::ParamTex2d<unsigned char, Math::EJT_MUL> weight_t;

	HairProcessor(
		// ����� ���������
		int SegmCount,
		// �����������
		HairMesh& surface, 
		// ��������
		std::map< int, sn::ShapeShortcutPtr>& snshapes,
		std::map< int, cls::MInstance>& ocsfiles,
		std::map< int, weight_t>& snshapes_weight,
		// �������� �� �����?
		bool mulWidthByLength, 
		// ��������������� ������
		enOrthogonalizeType orthogonalize,
		// ����������� �������������� UV
		bool bUseAddUV = true, 
		// �������� ������
		bool bHairIsClump = false
		);

public:
	// ������ HairProcessor::HAIR �� HAIRPARAMS
	// � ������ �������
	bool BuildHair(
		int index, 
		HAIRPARAMS& hairparam, 
		HAIRVERTS& hair
		);

public:
	// ��������� ��������� �� ���������
	void CreateHairDefault(
//		int index, 
		HAIRPARAMS& hairparam, 
		HAIRVERTS& hairsrc,	// �����
		bool bClump			// ���� ������� �����
		);
	// ��������� ��������� �� CV
	bool CreateHairFromCV(
//		int index, 
		HAIRPARAMS& hairparam, 
		HAIRVERTS& hairsrc,		// �����
		bool bClump				// ���� ������� �����
		);
public:
	// ����������� HAIRVERTS �� HAIRPARAMS
	// ��� ����� �������
	// ����� ������ ��������� �� ��������� ��������� ������
	bool DeformAloneHair(
		const HAIRPARAMS& params, 
		HAIRVERTS& hair,		// �� ����� - �������� �����
		bool bClump			// ���� ������� �����
		);
		// ��� ������
		bool DeformAloneHair_test(
			const HAIRPARAMS& params, 
			HAIRVERTS& hair,		// �� ����� - �������� �����
			bool bClump			// ���� ������� �����
			);
/*/
	// ������ HAIRVERTS �� HAIRPARAMS
	// ��� ����� �������
	bool BuildStandAloneHair(
		const HAIRPARAMS& params, 
		HAIRVERTS& hair,
		bool bClump			// ���� ������� �����
		);
/*/
public:
	// ������� ��������� ������ ��� ������ �� ������� ������
	void ParseHair(
		HAIRPARAMS& c_hairs, 
		int vert, bool bClump		// ����� �������� ��� ���� ������
		);
	// ������� ��������� ������ �� ����� �� �����������
	void ParseHair(
		HAIRPARAMS& c_hairs, 
		int faceindex, const Math::Vec3f& bary,		// ����� �������� ��� ���� ������
		bool bClump
		);
public:
	// ������� ��������� ������ �� U,V
	void ParseHair(
		HAIRPARAMS& c_hairs, 
		float U, float V,
		bool bClump, 
		bool bIgnoreJitters=false
		);

protected:
	// ��������� ����� � �����
	bool BuildHairInClump(
		const HAIRPARAMS& clumphairparams,  // ����� �� ������ �����
		const HAIRVERTS& clumphair,				
		const HAIRPARAMS& params,			// StandAlone �����
		const HAIRVERTS& orighair,		
		HAIRVERTS& hair					// ���������
		);
/*/
	// ��������� ����� �� ����������� �������
	// �������� (��.BuildStandAloneHair2 � BuildSrcHairFromCV)
	bool BuildHairByCV(
		const HAIRPARAMS& params, 
		HAIRVERTS& hair,
		bool bClump			// ���� ������� �����
		);
/*/

	// ��������� ��������
	void ApplyModifiers(
		HairMesh::ModifiersData& modifiers, 
		HAIRPARAMS& c_hairs, 
		bool bClump
		);
	// ��������� �������� � ������
	void ApplyModifiersPolar(
		HairMesh::ModifiersData& modifiers, 
		HAIRPARAMS& c_hairs, 
		bool bClump
		);

protected:
	Math::Vec3f randomAxis();
	double randomize();

	// ��� �������
	std::map<int, HAIRPARAMS> clumpparamcache; 
	std::map<int, HAIRVERTS> clumpvertscache; 

	// ��� ������, ���� �� ��������� �� 100 ���
	HairMesh::ModifiersData modifiers;

};




