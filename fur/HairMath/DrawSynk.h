#pragma once
class HairProcessor;
struct HairPersonageData;

#include "ocellaris/IRender.h"
#include "ocellaris/renderCacheImpl.h"
#include "ocellaris/MInstance.h"
#include "HAIRPARAMS.h"
#include "HAIRVERTS.h"
#include "HairPersonage.h"

//! ��������� ��� ���������
struct DrawSynk
{
	enum enDataChannels
	{
		DC_NONOISE					 = 0x0001,
		DC_HAIRIDS					 = 0x0002,
		DC_SURFACE_NORMALS			 = 0x0004,
		DC_ST						 = 0x0008,
		DC_CLUMP_RADIUS				 = 0x0010,
		DC_CLUMP_NORMAL				 = 0x0020,
		DC_CLUMP_VECTOR				 = 0x0040,
		DC_CLUMP_VECTORV			 = 0x0080,
		DC_VCLUMP					 = 0x0100,
		DC_TANGENT_VECTOR			 = 0x0200,
		DC_CLUMPID					 = 0x0400,
		DC_HAIRLENGTHSCALE			 = 0x0800,		// length jitter
		DC_DEFAULT					 = 0xFFFFFFFF
	};

	virtual void ClearBox(){};

	// INSTANCE
	virtual void setSnShape(int index, sn::ShapeShortcutPtr& ptr){};
	virtual void setOcsShape(int index, cls::MInstance& cache){};

	// ������������� �������
	virtual void Init(
		int hairCount, 
		int vertexInHair, 
		enHairInterpolation interpolation, 
		bool bNormals, 
		std::vector<const char*>* adduv, 
		int flags=DC_DEFAULT)=0;
	// setBaseUvSet
	virtual void setBaseUvSet( const char* baseUvSet){};

	// ���������� ��� ������ ������
	virtual void SetHair(
		int hairindex, 
		int hairid,
		HAIRPARAMS& params, 
		HAIRVERTS& hair,
		enRenderNormalSource renderNormalSource, 
		float RenderWidth, 
		float widthBias
		)=0;

	// Render
	virtual void Render(
		IPrman* prman,
		cls::IRender* render, 
		float motionPhase, 
		std::map< std::string, cls::Param>& extraParameters, 
		DrawSynk* freeze
		){};
	virtual void RenderCall(
		cls::IRender* render, 
		int motionBlurSamples, 
		float* motionBlurTimes
		){};

	// ��������!!!
	// Render BLUR
	virtual void RenderBlur(
		IPrman* prman,
		cls::IRender* render,
		std::vector<float> times,
		std::vector<DrawSynk*>& procedurals,
		std::map< std::string, cls::Param>& extraParameters, 
		DrawSynk* freeze
		){};

};

