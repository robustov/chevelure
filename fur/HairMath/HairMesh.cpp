#include "stdafx.h"
	#include "ocellaris/IRender.h"

#include "HairMesh.h"
#include "Math/Face.h"
#include "Math\MeshUtil.h"

/*/
template <class STREAM>
STREAM& operator>>(STREAM& serializer, HairMesh::DeformerDecl &v)
{
	serializer >> v.deformerproc;
	v.deformerstream.serialize(serializer);
	return serializer;
}
/*/
void HairMesh::init(
	HairGeometryIndices* geometryIndices, 
	HairGeometry*		geometry, 
	HairClumpPositions*	clumpPositions,
	HairRootPositions*	rootPositions,
	HairCVPositions*	cvPositions,
	HairCV*				cv,
	HairSystem*			hairSystem
	)
{
	this->geometryIndices = geometryIndices;
	this->geometry		= geometry;
	this->clumpPositions = clumpPositions;
	this->rootPositions = rootPositions;
	this->cvPositions	= cvPositions;
	this->cv			= cv;
	this->hairSystem	= hairSystem;
	seedbias = 0;
}

// ���������������� ������ ���������� ������� � ����������
void HairMesh::initParamsUVsetIndices()
{
	int ind;
	// length
	ind = geometryIndices->finduvSetByName(length.uvsetname.c_str());
	length.uvsetnumber = ind;
	// inclination
	ind = geometryIndices->finduvSetByName(inclination.uvsetname.c_str());
	inclination.uvsetnumber = ind;
	// polar
	ind = geometryIndices->finduvSetByName(polar.uvsetname.c_str());
	polar.uvsetnumber = ind;
	// baseCurl
	ind = geometryIndices->finduvSetByName(baseCurl.uvsetname.c_str());
	baseCurl.uvsetnumber = ind;
	// tipCurl
	ind = geometryIndices->finduvSetByName(tipCurl.uvsetname.c_str());
	tipCurl.uvsetnumber = ind;
	// baseWidth
	ind = geometryIndices->finduvSetByName(baseWidth.uvsetname.c_str());
	baseWidth.uvsetnumber = ind;
	// tipWidth
	ind = geometryIndices->finduvSetByName(tipWidth.uvsetname.c_str());
	tipWidth.uvsetnumber = ind;
	// scragle
	ind = geometryIndices->finduvSetByName(scragle.uvsetname.c_str());
	scragle.uvsetnumber = ind;
	// scragleFreq
	ind = geometryIndices->finduvSetByName(scragleFreq.uvsetname.c_str());
	scragleFreq.uvsetnumber = ind;
	// baseClump
	ind = geometryIndices->finduvSetByName(baseClump.uvsetname.c_str());
	baseClump.uvsetnumber = ind;
	// length
	ind = geometryIndices->finduvSetByName(tipClump.uvsetname.c_str());
	tipClump.uvsetnumber = ind;
	// length
	ind = geometryIndices->finduvSetByName(twist.uvsetname.c_str());
	twist.uvsetnumber = ind;
	// length
	ind = geometryIndices->finduvSetByName(displace.uvsetname.c_str());
	displace.uvsetnumber = ind;
}

void HairMesh::serialize(Util::Stream& stream)
{
	seedbias = 0;

	int version = 4;
	stream >> version;
	if( version>=0)
	{
		stream >> length	>> inclination >> polar;
		stream >> baseCurl	>> tipCurl;
		stream >> baseWidth >> tipWidth;
		stream >> scragle	>> scragleFreq;
		stream >> baseClump >> tipClump;
		stream >> twist;
		stream >> displace;
	}
	if(version>=1)
	{
		stream >> modifiers;
	}
	if(version>=2)
	{
		stream >> deformers;
	}
	if(version>=3)
	{
		stream >> twistFromPolar;
	}
	if(version>=4)
	{
		stream >> blendCVfactor >> blendDeformfactor;
	}
}
void HairMesh::serializeOcs(const char* _name, cls::IRender* render, bool bSave)
{
	seedbias = 0;

	std::string name = _name;

	length.serializeOcs( (name+"::length").c_str(), render, bSave);
	inclination.serializeOcs( (name+"::inclination").c_str(), render, bSave);
	polar.serializeOcs( (name+"::polar").c_str(), render, bSave);

	SERIALIZEOCSPROC( baseCurl);
	SERIALIZEOCSPROC( tipCurl);
	SERIALIZEOCSPROC( baseWidth);
	SERIALIZEOCSPROC( tipWidth);
	SERIALIZEOCSPROC( scragle);
	SERIALIZEOCSPROC( scragleFreq);
	SERIALIZEOCSPROC( baseClump);
	SERIALIZEOCSPROC( tipClump);
	SERIALIZEOCSPROC( twist);
	SERIALIZEOCSPROC( displace);
	SERIALIZEOCSPROC( twistFromPolar);
	SERIALIZEOCSPROC( blendCVfactor);
	SERIALIZEOCSPROC( blendDeformfactor);

	if(bSave)
	{
		render->Parameter( (name+"::modiferscount").c_str(), (int)modifiers.size());
		render->Parameter( (name+"::deformerscount").c_str(), (int)deformers.size());
	}
	else
	{
		{
			int size=0;
			render->GetParameter( (name+"::modiferscount").c_str(), size);
			modifiers.resize(size);
		}
		{
			int size=0;
			render->GetParameter( (name+"::deformerscount").c_str(), size);
			deformers.resize(size);
		}
	}
	int i;
	for(i=0; i<(int)modifiers.size(); i++)
	{
		char buf[256];
		sprintf(buf, "%s::modifer%d", _name, i);
		modifiers[i].serializeOcs(buf, render, bSave);
	}
	for(i=0; i<(int)deformers.size(); i++)
	{
		char buf[256];
		sprintf(buf, "%s::deformer%d", _name, i);
		deformers[i].serializeOcs(buf, render, bSave);
	}
}

Math::Box3f HairMesh::getBox()
{
	Math::Box3f box;
	
	for(unsigned f=0; f<geometryIndices->faceCount(); f++)
	{
		const Math::Face32& face = geometryIndices->face(f);
		for(int i=0; i<3; i++)
			box.insert( geometry->vert( face.v[i] ));
	}
	box.expand(displace.getMax());
//	box.expand(length.getMax());
	return box;
}

float HairMesh::getMaxLength(
	const std::vector<IHairDeformer*>& deformers_INCREASELENGTH
	)
{
	float len = length.getMax();
	for( int d=0; d<(int)deformers_INCREASELENGTH.size(); d++)
	{
		deformers_INCREASELENGTH[d]->IncreaseMaxLength(len);
	}
	// �� ����������� �����
	for(int h=0; h<cv->getCount(); h++)
	{
		HairCV::CV& ch = cv->getControlHair(h);
		float l=0;
		l += ch.getLength();
		len = __max(len, l);
	}
	return len;
}


void HairMesh::getParamRange( 
	double& startU, double& endU, double& startV, double& endV)
{
	startU = geometry->minUV.x;
	endU   = geometry->maxUV.x;
	startV = geometry->minUV.y;
	endV   = geometry->maxUV.y;
}
bool HairMesh::getPointAtParam(
	float u, float v, 
	Math::Vec3f& pos, 
	Math::Vec3f& normal,
	Math::Vec3f& tangentU,
	Math::Vec3f& tangentV, 
	Math::Vec3f& dpdU,
	Math::Vec3f& dpdV
	)
{
	Math::Vec2f uv(u, v);
	Math::Vec3f bary;
	int f = geometry->findUvFace(uv, geometryIndices, bary);
	if( f<0)
		return false;

	pos = geometry->getPOS(f, bary, geometryIndices);
	geometry->getBasis(f, bary, geometryIndices, normal, tangentU, tangentV, dpdU, dpdV);
	return true;
}
// ������ ��������� ��� �����
bool HairMesh::getModifiersAtPoint(
	const Math::Vec3f& pos, 
	ModifiersData& md)
{
	md.affectmodifiers.clear();
	md.w = 0;
	md.hpb = 0;
	for(unsigned i=0; i<modifiers.size(); i++)
	{
		HairMeshModifier& hmm = modifiers[i];
		Math::Vec3f p = hmm.transform*pos;
		float l = 0;
		switch( hmm.modiferType)
		{
		case HairMeshModifier::MT_SPHERE:
			l = p.length()*2;
			break;
		case HairMeshModifier::MT_CYLINDER:
			l = Math::Vec3f( p.x, 0, p.z).length()*2;
			if(l>1) continue;
			l = fabs(p.y*2);
			break;
		}
		if(l>1) continue;

		float lw = hmm.affectRamp.getValue(l);
		Math::Vec3f dirToModifier = Math::Vec3f(0, 1, 0);
		Math::Matrix4As3f mforVec(hmm.transform_inv);
		switch( hmm.directionType)
		{
		case HairMeshModifier::DT_NONE:
		case HairMeshModifier::DT_TO_CENTER:
			dirToModifier = hmm.transform_inv*Math::Vec3f(0, 0, 0)-pos;
			break;
		case HairMeshModifier::DT_X_AXIS:
			dirToModifier = mforVec*Math::Vec3f(1, 0, 0);
			break;
		case HairMeshModifier::DT_Y_AXIS:
			dirToModifier = mforVec*Math::Vec3f(0, 1, 0);
			break;
		case HairMeshModifier::DT_Z_AXIS:
			dirToModifier = mforVec*Math::Vec3f(0, 0, 1);
			break;
		}
		
//		float lw = 1-l;
		md.affectmodifiers.push_back(ModifierData(&hmm, lw, dirToModifier));
		md.w += lw;

		md.hpb |= hmm.hairParamBit;
	}
	if( md.affectmodifiers.empty())
		return false;
	if(md.w>1)
	{
		// �������������
		for(unsigned i=0; i<md.affectmodifiers.size(); i++)
		{
			ModifierData& am = md.affectmodifiers[i];
			am.w = am.w/md.w;
		}
		md.w = 0;
	}
	else
	{
		md.w = 1-md.w;
	}
	return true;
}



/*/
// ������ ����� �����
std::pair<int, int> HairMesh::generateHairs()
{
	return std::pair<int, int>(
		rootPositions->getCount(), 
		clumpPositions->getCount());
}
/*/

// ������
int HairMesh::getGroupCount()
{
	return rootPositions->getGroupCount();
}
void HairMesh::getGroup(int group, int& startindex, int& endindex)
{
	rootPositions->getGroup(group, startindex, endindex);
	return;
}


// ������ ���������������� (0-1) {u, v} ��� ������ 
void HairMesh::getUVAtHair(
	int index, bool bClump, 
	float& u, float& v)
{
	int i = index;
	Math::Vec2f uv;
	if( !bClump)
		uv = rootPositions->getUV(i, geometryIndices, geometry);
	else
		uv = clumpPositions->getUV(i, geometryIndices, geometry);

	u = uv.x;
	v = uv.y;
}
void HairMesh::getUVSetAtHair(
	int index, bool bClump, 
	std::vector<Math::Vec2f>& res)
{
	int i = index;
	res.resize(geometryIndices->uvSetCount());
	if( !bClump)
	{
		for(unsigned s=0; s<geometryIndices->uvSetCount(); s++)
		{
			Math::Vec2f uv = rootPositions->getUVset(i, s, geometryIndices, geometry);
			res[s] = uv;
		}
	}
	else
	{
		for(unsigned s=0; s<geometryIndices->uvSetCount(); s++)
		{
			Math::Vec2f uv = clumpPositions->getUVset(i, s, geometryIndices, geometry);
			res[s] = uv;
		}
	}
}

void HairMesh::getPosAtHair(
	int index, bool bClump, 
	Math::Vec3f& pos)
{
	if( !bClump)
		pos = rootPositions->getPOS(index, geometryIndices, geometry);
	else
		pos = clumpPositions->getPOS(index, geometryIndices, geometry);
}

// ���������� �� �������� ����� 
void HairMesh::getPointAtHair(
	int index, bool bClump, 
	Math::Vec3f& pos, 
	Math::Vec3f& normal,
	Math::Vec3f& tangentU,
	Math::Vec3f& tangentV, 
	Math::Vec3f& dpdU,
	Math::Vec3f& dpdV)
{
	if( !bClump)
	{
		pos = rootPositions->getPOS(index, geometryIndices, geometry);
		rootPositions->getBasis(index, geometryIndices, geometry, normal, tangentU, tangentV, dpdU, dpdV);
	}
	else
	{
		pos = clumpPositions->getPOS(index, geometryIndices, geometry);
		clumpPositions->getBasis(index, geometryIndices, geometry, normal, tangentU, tangentV, dpdU, dpdV);
	}
}
// ���������� �� �������� ����� 
int HairMesh::getClumpIndexAtHair(
	int index)
{
	return rootPositions->getClumpIndex(index);
}

//! get control hair for hair
bool HairMesh::getControlHair( int index, bool bClump, HAIRCONTROL& CV) const
{
	// ����� face
	const HairGeometryIndices::pointonsurface* pos = NULL;
	if( !bClump)
	{
		pos = &rootPositions->getPointOnSurface(index);
	}
	else
	{
		pos = &clumpPositions->getPointOnSurface(index);
	}
	if(!pos->isValid()) 
		return false;

	return getControlHair( *pos, CV);
}

//! get control hair for hair
bool HairMesh::getControlHair( const HairGeometryIndices::pointonsurface& pos, HAIRCONTROL& CV) const
{
	// ����������� ����� �� ���������������� �����������
	return cv->buildControlHair( 
		pos,
		cvPositions, 
		geometryIndices,
		geometry,
		CV
		);
}


// ��������� � ����������
int HairMesh::getDeformerCount()
{
	return (int)deformers.size();
}
IHairDeformer* HairMesh::getDeformer(int i)
{
	return deformers[i].deformer;
}
void HairMesh::InitDeformers()
{
	for(int i=0; i<(int)deformers.size(); i++)
	{
		IHairDeformer* deformer = deformers[i].deformer;
		if( deformer)
			deformer->init();
	}
}
void HairMesh::Randomize(float lengthfactor, int seedbias)
{
	this->length.scale(lengthfactor);
	this->seedbias = seedbias;
}

//common 
float HairMesh::getLength(
	float u, float v, 
	const std::vector<Math::Vec2f>& uvSets, 
	const std::vector<IHairDeformer*>& deformers_INCREASELENGTH
	)
{
	int uvnumber = length.uvsetnumber;
	if( uvnumber>=0 && 
		uvnumber<(int)uvSets.size())
	{
		const Math::Vec2f& uv = uvSets[uvnumber];
		u = uv.x;
		v = uv.y;
	}
	float len = length.getValueSource(u, v);

	for( int d=0; d<(int)deformers_INCREASELENGTH.size(); d++)
	{
		deformers_INCREASELENGTH[d]->IncreaseHairLength(u, v, len);
	}
	return len;
}


float HairMesh::getLengthHair(
	int vert, 
	bool bClump, 
	const std::vector<IHairDeformer*>& deformers_INCREASELENGTH
	)
{
	Math::Vec2f uv;
	int uvsetindex = this->length.uvsetnumber;
	this->getUVSetAtHair(vert, bClump, uvsetindex, uv);

	float len = length.getValueSource(uv.x, uv.y);

	for( int d=0; d<(int)deformers_INCREASELENGTH.size(); d++)
	{
		deformers_INCREASELENGTH[d]->IncreaseHairLength(uv.x, uv.y, len);
	}
	return len;
}
float HairMesh::getDisplaceHair(int vert, bool bClump)
{
	Math::Vec2f uv;
	int uvsetindex = this->displace.uvsetnumber;
	this->getUVSetAtHair(vert, bClump, uvsetindex, uv);
	float length = this->getDisplace(uv.x, uv.y);
	return length;
}


/*/
float HairMesh::getValue(HairParam<unsigned char>& param, float u, float v)
{
	if( !param.filename.empty() && !param.matrix->empty())
	{
		unsigned char c = param.matrix->interpolation((float)u, (float)v, false, false);
		float d = (c/255.f)*(param.factor)+param.offset;
		return d;
	}
	if(param.matrix && !param.matrix->empty())
	{
		unsigned char c = param.matrix->interpolation((float)u, (float)v, false, false);
		float d = (c/255.f)*(param.max-param.min)+param.min;
		return d;
	}
	return param.defValue;
}
/*/

