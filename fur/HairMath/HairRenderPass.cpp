#include "stdafx.h"
#include "HairRenderPass.h"

template <class STREAM>
STREAM& operator>>(STREAM& serializer, HairRenderPass::DetailRange &v)
{
	serializer >> v.minvisible;
	serializer >> v.lowertransition;
	serializer >> v.uppertransition;
	serializer >> v.maxvisible;
	return serializer;
}

HairRenderPass::HairRenderPass()
{
	clear();
}
void HairRenderPass::clear()
{
	degradation = 1.f;
	degradationWidth = 0.f;
	bMotionBlur = true;
	reverseOrientation = false;

	bPrune = false;
	pruningrate = 2;			// �������
	prunedistmin = 1;			// ��������� ������ ������������
	prunemin_value = 0.01f;		// ����������� ��������
	prunemin_width = 10;		// ������ ��� ����������� ��������
	pruneTestDistance = 0;

	// ����������� ����. �������� (��� �������� � ������)
	prune = 1;

	// 13.03.2008
	widthBias = 0;
	// 22.08.2008
	opacity = -1;

	sigmaHiding = false;

	useClumpRadiusAsWidth = false;

	passes.clear();
	passIds.clear();
}
void HairRenderPass::serialize(Util::Stream& out)
{
	int version = 9;
	out >> version;
	if( version>=0)
	{
		// StandAlone
		out >> bStandAlone;
		// �������
		out >> bEnable;
		// ��� �������
		out >> passName;
		// ����� ���������
		out >> SegmCount;
		// ������ ������
		out >> shader;

		out >> *(int*)&source;
		out >> bRenderClumps;

		out >> RenderWidth;
		out >> mulWidthByLength;

		out >> interpolation;
		out >> diceHair;
		out >> renderDoubleSide >> reverseOrientation;

		out >> *(int*)&renderNormalSource;

		out >> renderRayTraceEnable;
		out >> renderRayCamera;
		out >> renderRayTrace;
		out >> renderRayPhoton;

		out >> bUseDetails;
		out >> detailrange;

		out >> passes;
	}
	if(version>=1)
	{
		out >> bMotionBlur;
	}
	if(version>=2)
	{
		out >> degradation;
	}
	if(version>=3)
	{
		out >> bPrune;
		Math::Matrix4f camera_worldpos = Math::Matrix4f::id;
		out >> camera_worldpos;
		Math::Matrix4f this_worldpos = Math::Matrix4f::id;
		out >> this_worldpos;
		out >> pruningrate;			// �������
		out >> prunedistmin;			// ��������� ������ ������������
		out >> prunemin_value;		// ����������� ��������
		out >> prunemin_width;		// ������ ��� ����������� ��������
	}
	if(version>=4)
	{
		out >> degradationWidth;
		out >> pruneTestDistance;
	}
	if(version>=5)
	{
		// 13.03.2008
		out >> widthBias;
	}
	if(version>=6)
	{
		// 22.08.2008
		out >> opacity;
	}
	if(version>=7)
	{
		// 22.08.2008
		out >> sigmaHiding;
	}
	if(version>=8)
	{
		// 16.12.2008
		out >> useClumpRadiusAsWidth;
	}
	if(version>=9)
	{
		// 08.11.2009
		out >> passIds;
	}
}
void HairRenderPass::serializeOcs(const char* _name, cls::IRender* render, bool bSave)
{
	std::string name = _name;

	SERIALIZEOCS( bStandAlone);
	SERIALIZEOCS( bEnable);
	SERIALIZEOCS( passName);
	SERIALIZEOCS( SegmCount);
	SERIALIZEOCS( shader);
	SERIALIZEOCS( bMotionBlur);
	SERIALIZEOCSENUM( source);
	SERIALIZEOCS( bRenderClumps);
	SERIALIZEOCS( degradation);
	SERIALIZEOCS( degradationWidth);
	SERIALIZEOCS( RenderWidth);
	SERIALIZEOCS( mulWidthByLength);
	SERIALIZEOCS( interpolation);
	SERIALIZEOCS( diceHair);
	SERIALIZEOCS( renderDoubleSide);
	SERIALIZEOCS( reverseOrientation);
	SERIALIZEOCS( sigmaHiding);
	SERIALIZEOCSENUM( renderNormalSource);
	SERIALIZEOCS( renderRayTraceEnable);
	SERIALIZEOCS( renderRayCamera);
	SERIALIZEOCS( renderRayTrace);
	SERIALIZEOCS( renderRayPhoton);
	SERIALIZEOCS( bUseDetails);
//	SERIALIZEOCS( detailrange);
	SERIALIZEOCS( passes);
	SERIALIZEOCS( bPrune);
	SERIALIZEOCS( pruningrate);			// �������
	SERIALIZEOCS( prunedistmin);			// ��������� ������ ������������
	SERIALIZEOCS( prunemin_value);		// ����������� ��������
	SERIALIZEOCS( prunemin_width);		// ������ ��� ����������� ��������
	SERIALIZEOCS( pruneTestDistance);	// �������� ��������� (���� 0 ��� )
	SERIALIZEOCS( prune);
	SERIALIZEOCS( widthBias);			// ������� � ������
	SERIALIZEOCS( opacity);

	// 16.12.2008
	SERIALIZEOCS( useClumpRadiusAsWidth);
}

// ��������� ���������� ���������� �� �������
int HairRenderPass::CalcCount(int haircount, int clumpcount)
{
	if(bRenderClumps)
	{
		return (int)(clumpcount*degradation);
	}
	else
	{
		return (int)(haircount*degradation);
	}
}
