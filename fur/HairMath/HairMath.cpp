// HairMath.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "HairMath.h"
#include "stdio.h"
#include ".\hairlicensethread.h"

//#include "DrawSynk.h"

#ifndef SAS
	#include "ulLicense/UlLClientThread.h"
#endif

// ��������� ��������
#ifndef SAS
//	__declspec(dllexport) DWORD UlHair_licflag=0x2994f;
	__declspec(dllexport) DWORD UlHair_licflag=0x1;
	UlLClientThread clientThread;
#endif

CRITICAL_SECTION globalmutex;

void HairGetVersion(int& buildN, std::string& buildDate, std::string& versionstring)
{
	buildN = hair_buildN;
	buildDate = hair_buildDate;
	versionstring = hair_versionstring;
}

HANDLE hairMathModule = 0;

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
//fprintf(stderr, "HairMath %d\n", ul_reason_for_call);
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		{
			hairMathModule = hModule;

			InitializeCriticalSection(&globalmutex);

// ��������� ��������
#ifndef SAS
			clientThread.StartThread(hairMathModule, "UlHair", "", &UlHair_licflag);
#endif

//#ifdef HAIR_TRACELOG
			int buildN; 
			std::string buildDate, versionstring;
			HairGetVersion( buildN, buildDate, versionstring);

			// ��� �������� �����
			char filename[512]; 
			GetModuleFileName( (HMODULE)hModule, filename, 512); 
//			fprintf(stderr, "%s build %d(%s) version %s\n", filename, buildN, buildDate.c_str(), versionstring.c_str());
//#endif
#ifdef SAS
#ifdef CHEVELUREDEMO
			fprintf(stderr, "Caution! This is FREE version. Textured parameters aren't supported.\nEmail to robustov@mail.ru if you need full version.\n");
#else
//			fprintf(stderr, "Full version.\n");
#endif
#endif
			fflush(stderr);

			break;
		}
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
// ��������� ��������
#ifndef SAS
		clientThread.StopThread();
#endif
		break;
	}
    return TRUE;
}

