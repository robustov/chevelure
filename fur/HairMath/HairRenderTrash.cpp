#include "stdafx.h"
#include "HairMath.h"
#include "HairRender.h"
#include "Util/FileStream.h"
#include "HairPersonage.h"
#include "Procedurals/HairProceduralCurve.h"
#include "Procedurals/HairProceduralSn.h"
#include "Procedurals/HairProceduralTubes.h"
#include "Procedurals/HairProceduralCurveOcs.h"
#include "Util/currentHostName.h"
#include ".\hairlicensethread.h"
#include <time.h>

extern CRITICAL_SECTION globalmutex;

//! ������ �����, ��� ���������� ����� ������
struct hairGroup
{
	hairGroup(
		HairRender* hairRender, 
		int startindex, 
		int endindex,
		bool bClump, 
		HairRenderPass* pPass, 
		float prune
		)
	{
		this->hairRender = hairRender;
		this->startindex = startindex;
		this->endindex = endindex;
		this->bClump = bClump;
		this->pPass = pPass;
		this->prune = prune;
		this->hairRender->addRef();
	}
	hairGroup(
		const hairGroup& arg
		)
	{
		this->hairRender = arg.hairRender;
		this->startindex = arg.startindex;
		this->endindex = arg.endindex;
		this->bClump = arg.bClump;
		this->pPass = arg.pPass;
		this->prune = arg.prune;
		this->hairRender->addRef();
	}
	~hairGroup()
	{
		hairRender->release();
	}
	HairRender* hairRender;
	int startindex, endindex;
	bool bClump;
	HairRenderPass* pPass;
	float prune;
};


// ���������� ������ �����
void HairRenderProcessGroup(
	int startindex, 
	int count, 
	bool bClump, 
//	std::vector<bool>& degradationHairs, 
	HairRender* hairRender, 
	HairRenderPass::enSource rendertype,
	HairProcessor& processor, 
	HairRenderPass* pPass, 
	DrawSynk& procedural, 
	cls::IRender* testrender
	)
{
	HAIRVERTS hair;
	HAIRPARAMS params;

	int dg=0;

//cls::PA<Math::Vec3f> P(cls::PI_VERTEX, count, cls::PT_POINT);

	for(int i=0; i<count; i++)
	{
//		if( !degradationHairs.empty())
//			if( !degradationHairs[i])
//				continue;
		float degparam;
		bool deg = hairRender->IsHairDegradate(startindex+i, bClump, pPass->degradation, pPass->degradationWidth, degparam);
		if( deg) continue;


		processor.BuildHair(i+startindex, params, hair);
//P[i] = params.position;

		if( !hair.vc()) 
			continue;

		// ��������� degradationWindow
		if( pPass->degradation<1 && pPass->degradationWidth>0.001)
		{
			if( degparam > pPass->degradation)
			{
				float resDegradation = pPass->degradation + pPass->degradationWidth;
				float wfactor = (degparam - resDegradation)/( -pPass->degradationWidth);
				for(int v=0; v<(int)hair.vwidht.size(); v++)
					hair.vwidht[v] *= wfactor;
			}
		}

//printf("%f, %f, %f    ", params.position.x, params.position.y, params.position.z);

		procedural.SetHair(
			dg, 
			i+startindex, 
			params, 
			hair, 
			pPass->renderNormalSource, 
			pPass->RenderWidth, 
			pPass->widthBias
			);
		dg++;
	}
//printf("\n");

//testrender->Parameter("#width", 0.1f);
//testrender->Points(P);
}

// �������� ����� visibility � prman
void HairRender::RayTraceOptions( HairRenderPass* pPass, IPrman* prman)
{
	if( pPass->renderRayTraceEnable)
	{
		RtInt visibility[3];
		visibility[0] = pPass->renderRayCamera;
		visibility[1] = pPass->renderRayTrace;
		visibility[2] = pPass->renderRayPhoton;

		prman->RiAttribute("visibility", 
			"int camera", &visibility[0], 
			"int trace", &visibility[1], 
			"int photon", &visibility[2], 
			RI_NULL);
	}
}


RtVoid FUR_SUBDIVFUNC_GROUP(RtPointer ptr, RtFloat detail)
{
	// detail
	hairGroup* hg = (hairGroup*)ptr;
	HairRender* hairRender = hg->hairRender;
	HairRenderPass* pPass = hg->pPass;

	try
	{
		// add uv
		HairGeometryIndices* hgi = &hairRender->staticData->geometryIndices;
		std::vector<const char*> adduv(hgi->uvSetCount(), NULL);
		for(unsigned i=0; i<adduv.size(); i++)
			adduv[i] = hgi->uvSetName(i);

		// ��� ��������?
		int startindex = hg->startindex;
		int endindex = hg->endindex;
		bool bClump = hg->bClump;
		int count = endindex-startindex;

		///////////////////////////////////
		// render type
		HairSystem& hairSystem = hairRender->hairSystem;
		bool bInstanced = 
			(hairSystem.snshapes.size()!=0) || 
			(hairSystem.ocsfiles.size()!=0);

	#ifdef CHEVELURELIGHT
		bInstanced = false;
	#endif CHEVELURELIGHT

		HairRenderPass::enSource rendertype = bInstanced?HairRenderPass::RT_INSTANCES:HairRenderPass::RT_HAIRS;
		if(pPass->source != HairRenderPass::RT_DEFAULT)
			rendertype = pPass->source;
		if( rendertype==HairRenderPass::RT_INSTANCES && !bInstanced) 
			return;

		///////////////////////////////////
		// mulWidthByLength
		bool mulWidthByLength = pPass->mulWidthByLength;
		if( rendertype==HairRenderPass::RT_INSTANCES) mulWidthByLength = false;

		///////////////////////////////////
		// ������ HairProcedural �� ����� (hairRender->deformblur)
		int dbs = hairRender->deformblur.size();
		if( !pPass->bMotionBlur) dbs = 1;
		std::vector<float> times;
		times.resize(dbs);

		// ��������� �������
		std::vector<DrawSynk*> procedurals(dbs+1);
		std::vector<HairProceduralCurve> proceduralsCurve;
		std::vector<HairProceduralSn> proceduralsSn;
		std::vector<HairProceduralTubes> proceduralsTubes;

		if( rendertype==HairRenderPass::RT_HAIRS)
		{
			proceduralsCurve.resize(dbs+1);
			for( int i=0; i<dbs+1; i++) procedurals[i] = &proceduralsCurve[i];
		}
		else if( rendertype==HairRenderPass::RT_INSTANCES)
		{
			proceduralsSn.resize(dbs+1);
			for( int i=0; i<dbs+1; i++)
			{
				procedurals[i] = &proceduralsSn[i];
				{
					std::map< int, sn::ShapeShortcutPtr >::iterator it = hairSystem.snshapes.begin();
					for(;it != hairSystem.snshapes.end(); it++)
						procedurals[i]->setSnShape(it->first, it->second);
				}
				{
					std::map< int, cls::MInstance>::iterator it = hairSystem.ocsfiles.begin();
					for(;it != hairSystem.ocsfiles.end(); it++)
						procedurals[i]->setOcsShape(it->first, it->second);
				}
			}
		}
		else if(rendertype==HairRenderPass::RT_TUBES || rendertype==HairRenderPass::RT_SUBDIVTUBES)
		{
			proceduralsTubes.resize(dbs+1);
			for( int i=0; i<dbs+1; i++)
			{
				procedurals[i] = &proceduralsTubes[i];
				proceduralsTubes[i].setSubdiv(rendertype==HairRenderPass::RT_SUBDIVTUBES);
			}
		}
//		std::vector<bool> degradationHairs;
		int countAfterDegradation=count;
		float resDegradation = pPass->degradation + pPass->degradationWidth;
		if(resDegradation<1)
		{	
			countAfterDegradation = 0;
//			degradationHairs.resize(count, false);
			for(int i=0; i<count; i++)
			{
				float degparam;
				bool deg = hairRender->IsHairDegradate(hg->startindex+i, hg->bClump, pPass->degradation, pPass->degradationWidth, degparam);
				if( deg) continue;

//				degradationHairs[i] = true;
				countAfterDegradation++;
			}
		}
		if( countAfterDegradation==0) 
		{
			// �� ��������!
			return;
		}

		// ����������
		hairRender->nGroups++;
		hairRender->nHairs += countAfterDegradation;

		// extraParameters
		std::map< std::string, cls::Param> extraParameters;
		extraParameters["prune"] = cls::P<float>(hg->prune);

		if( pPass->opacity>=0) 
		{
			Math::Vec3f Os(pPass->opacity);
			extraParameters["Os"] = cls::P<Math::Vec3f>(Os, cls::PT_COLOR);
		}

		// ����
		std::map<float, HairRender::Dynamic*>::iterator it = hairRender->deformblur.begin();
		for(int p=0; it != hairRender->deformblur.end(); it++, p++)
		{
			DrawSynk& procedural = *procedurals[p];
			procedural.setBaseUvSet(hgi->getBaseUVSet());
			bool bNormals = !(pPass->renderNormalSource==RNS_NONE);
			procedural.Init(countAfterDegradation, pPass->SegmCount+1, ((enHairInterpolation)pPass->interpolation), bNormals, &adduv);

			HairRender::Dynamic& dynamicRecord = *it->second;
			times[p] = it->first;

			HairMesh* surface = &dynamicRecord.surface;
			HairProcessor processor(pPass->SegmCount, *surface, 
				hairSystem.snshapes, 
				hairSystem.ocsfiles, 
				hairSystem.snshapes_weight, 
				mulWidthByLength, hairSystem.orthogonalize, true, bClump);

			HairRenderProcessGroup(
				startindex, 
				count, 
				hg->bClump, 
				hairRender, 
				rendertype, 
				processor, 
				pPass,
				procedural, 
				NULL
				);
			if( dbs == 1)
				break;
		}
		// Freeze
		DrawSynk* freezeprocedural = NULL;
		if(hairRender->deformfreeze)
		{
			DrawSynk& procedural = *procedurals[dbs];
			procedural.setBaseUvSet(hgi->getBaseUVSet());
			bool bNormals = !(pPass->renderNormalSource==RNS_NONE);
			procedural.Init(countAfterDegradation, pPass->SegmCount+1, ((enHairInterpolation)pPass->interpolation), bNormals, &adduv);

			HairRender::Dynamic& dynamicRecord = *hairRender->deformfreeze;

			HairMesh* surface = &dynamicRecord.surface;
			HairProcessor processor(pPass->SegmCount, *surface, 
				hairSystem.snshapes, 
				hairSystem.ocsfiles, 
				hairSystem.snshapes_weight, 
				mulWidthByLength, hairSystem.orthogonalize, true, bClump);

			HairRenderProcessGroup(
				startindex, 
				count, 
				hg->bClump, 
				hairRender, 
				rendertype, 
				processor, 
				pPass,
				procedural, 
				NULL
				);
			freezeprocedural = &procedural;
		}
//LeaveCriticalSection(&hairRender->mutex);

		////////////////////////////////////////
		// ��������� �� ������
		// -> PrMan
		IPrman* prman = hairRender->prman;

		prman->RiAttributeBegin();

		// Raytrace
		hairRender->RayTraceOptions(pPass, prman);

		// dicehair
		RtInt dicehair = 0;
		if( pPass->diceHair) dicehair = 1;
		if( pPass->renderNormalSource!=RNS_NONE)
			dicehair = 0;
		prman->RiAttribute("dice", "uniform int hair", &dicehair, RI_NULL);

		// sigma hiding
		RtInt sigma = 0;
		if( pPass->sigmaHiding) sigma = 1;
		prman->RiAttribute("stochastic", "uniform int sigma", &sigma, RI_NULL);


		// RiSides
		int sides = pPass->renderDoubleSide?2:1;
		prman->RiSides(sides);

		// reverseOrientation
		if(pPass->reverseOrientation)
			prman->RiReverseOrientation();
		
		RtBasis catmullRom = {
		{ -0.5,  1.5, -1.5,  0.5 },
		{  1.0, -2.5,  2.0, -0.5 },
		{ -0.5,  0.0,  0.5,  0.0 },
		{  0.0,  1.0,  0.0,  0.0 }
		};
		prman->RiBasis(catmullRom, 1, catmullRom, 1);

			/*/
printf("Group %d[%d]\n", hg->startindex, hg->endindex-hg->startindex);
			/*/

	#ifndef SAS
		if( UlHair_licflag!=1)
		{
			goto licflag;
		}
	#endif

		cls::IRender* render = hairRender->render;

// DEBUG MULTITHREAD
//render = 0;

		EnterCriticalSection(&globalmutex);

		if( render)
			render->Attribute("@parentpruningScaleFactor", hairSystem.pruningScaleFactor);
		if( dbs==1)
		{
			procedurals[0]->Render(prman, render, 0, extraParameters, freezeprocedural);
		}
		else
		{
			procedurals[0]->RenderBlur(prman, render, times, procedurals, extraParameters, freezeprocedural);
		}
		LeaveCriticalSection(&globalmutex);

licflag:
		prman->RiAttributeEnd();
	}
	catch(...)
	{
		std::string host = Util::currentHostName();
		fprintf(stderr, "HairMath: fur_subdivfunc_group exception. host=%s file=%s\n", host.c_str(), hairRender->hairRootPositionsFileName.c_str());
		int startPoly, endPoly;
//		int endPoly, endGroup;
		startPoly = hairRender->getPolygonIndex( hg->startindex, hg->bClump);
		endPoly = hairRender->getPolygonIndex( hg->endindex-1, hg->bClump);

		fprintf(stderr, "DUMP: hairs[%d-%d] poly[%d, %d] %s deg=%f\n", 
			hg->startindex, hg->endindex, 
			startPoly, endPoly,
			hg->bClump?"Clump":"", 
			pPass->degradation
			);
		fflush(stderr);
	}
}

RtVoid fur_subdivfunc_group(RtPointer ptr, RtFloat detail)
{
	// detail
	hairGroup* hg = (hairGroup*)ptr;
	HairRender* hairRender = hg->hairRender;
	HairRenderPass* pPass = hg->pPass;

	EnterCriticalSection(&hairRender->mutex);

	if( hairRender->hairSystem.dumpInformation)
		fprintf(stderr, "<", hg->startindex), fflush(stderr);

	FUR_SUBDIVFUNC_GROUP(ptr, detail);

	if( hairRender->hairSystem.dumpInformation)
		fprintf(stderr, ">", hg->startindex), fflush(stderr);

	LeaveCriticalSection(&hairRender->mutex);
}

RtVoid fur_freefunc_group(RtPointer ptr)
{
	try
	{
		hairGroup* hg = (hairGroup*)ptr;

		HairSystem& hairSystem = hg->hairRender->hairSystem;
		if( hairSystem.dumpInformation)
			fprintf(stderr, "-"), fflush(stderr);

		delete hg;
	}
	catch(...)
	{
		std::string host = Util::currentHostName();
		fprintf(stderr, "HairMath: fur_freefunc_group exception. Host=%s\n", host.c_str());
		fflush(stderr);
	}
}

// ���������� ���� ������� ���������� �����
RtVoid fur_subdivfunc_group_accurate(RtPointer ptr, RtFloat detail)
{
	hairGroup* hg = (hairGroup*)ptr;
	HairRender* hairRender = hg->hairRender;
	HairRenderPass* pPass = hg->pPass;

	HairSystem& hairSystem = hairRender->hairSystem;
	bool bInstanced = 
		(hairSystem.snshapes.size()!=0) || 
		(hairSystem.ocsfiles.size()!=0);

	Math::Box3f groupbox;
	// ����� ����������� ������
	// ��������� ��� ������
	std::map<float, HairRender::Dynamic*>::iterator it = hairRender->deformblur.begin();
	for(int p=0; it != hairRender->deformblur.end(); it++, p++)
	{
		HairRenderPass& pass = *pPass;
		HairRender::Dynamic& dynamicRecord = *it->second;
		HairMesh* surface = &dynamicRecord.surface;

		// hairGroup
		bool bClump = hg->bClump;
		int startindex = hg->startindex;
		int endindex = hg->endindex;

		HairProcessor processor( pass.SegmCount, *surface, 
			hairRender->hairSystem.snshapes, 
			hairRender->hairSystem.ocsfiles, 
			hairRender->hairSystem.snshapes_weight, 
			pass.mulWidthByLength, hairRender->hairSystem.orthogonalize, false, bClump);
		HAIRVERTS hair;
		HAIRPARAMS params;

		for(int h=startindex; h<endindex; h++)
		{
			float degparam;
			bool deg = hairRender->IsHairDegradate(h, hg->bClump, pPass->degradation, pPass->degradationWidth, degparam);
			if( deg) continue;

			processor.BuildHair(h, params, hair);

			float winstance = 0;
			if(bInstanced)
			{
				if( hairSystem.snshapes.find(params.instance)!=hairSystem.snshapes.end())
				{
					sn::ShapeShortcutPtr snInst = hairSystem.snshapes[params.instance];
					Math::Box3f _snbox;
					snInst->shaper->getDirtyBox(snInst->shape, Math::Matrix4f::id, _snbox);

					Math::Vec3f dir = _snbox.min - _snbox.max;
					dir.y = 0;
					winstance = __max(dir.length()*0.5f, winstance);
				}
				
				if( hairSystem.ocsfiles.find(params.instance)!=hairSystem.ocsfiles.end())
				{
					cls::MInstance& inst = hairSystem.ocsfiles[params.instance];
					if( inst.isValid())
					{
						Math::Box3f _snbox = inst.getBox();
						Math::Vec3f dir = _snbox.min - _snbox.max;
						dir.y = 0;
						winstance = __max(dir.length()*0.5f, winstance);
					}
				}
			}

			for(int v=0; v<(int)hair.cvint.size(); v++)
			{
				float w = hair.vwidht[v];
				if( bInstanced) 
					w *= winstance;
				else
					w *= pPass->RenderWidth;
				w *= 0.5f;
				
				groupbox.insert(hair.cvint[v]+Math::Vec3f(w));
				groupbox.insert(hair.cvint[v]-Math::Vec3f(w));
			}
		}
	}
	// ������ �����
	bool bRenderGroupBox = hairRender->hairSystem.bRenderBoxes;
	cls::IRender* render = hairRender->render;
	if( render && bRenderGroupBox )
	{
		// ������ ����� � ������� �����
		float w = pPass->RenderWidth;
		

		render->Parameter("#width", w);
		render->Parameter("#box", groupbox);
		render->RenderCall("Box");
		return;
	}

	RtBound bound;
	IPrman::copy(bound, groupbox);

	hairGroup* newhg = new hairGroup(*hg);
	IPrman* prman = hairRender->prman;
	prman->RiProcedural(newhg, bound, fur_subdivfunc_group, fur_freefunc_group);
}

/*/
bool HairRender::RenderHairGroupOcs(
	hairGroup* hg
	)
{
	if(false)
	{
		HairRender* hairRender = this;
		std::map<float, HairRender::Dynamic*>::iterator it = hairRender->deformblur.begin();
		for(int p=0; it != hairRender->deformblur.end(); it++, p++)
		{
			HairRender::Dynamic& dynamicRecord = *it->second;
			cls::PA<Math::Vec3f> P(cls::PI_VERTEX, staticData->geometryIndices.vertexCount(), cls::PT_POINT);
			for(int i=0; i<P.size();i++)
				P[i] = dynamicRecord.geometry.vertex(i);
			render->Parameter("#width", 0.1f);
			render->Points(P);
		}
	}
	HairRender* hairRender = this;
	HairRenderPass* pPass = hg->pPass;

	// add uv
	HairGeometryIndices* hgi = &hairRender->staticData->geometryIndices;
	std::vector<const char*> adduv(hgi->uvSetCount(), NULL);
	for(unsigned i=0; i<adduv.size(); i++)
		adduv[i] = hgi->uvSetName(i);

	// ��� ��������?
	int startindex = hg->startindex;
	int endindex = hg->endindex;
	bool bClump = hg->bClump;
	int count = endindex-startindex;

	///////////////////////////////////
	// render type
	HairSystem& hairSystem = hairRender->hairSystem;
#ifndef OCELLARIS_INSTANCE
	bool bInstanced = hairSystem.snshapes.size()!=0;
#else
	bool bInstanced = hairSystem.ocsfiles.size()!=0;
#endif// OCELLARIS_INSTANCE

	#ifdef CHEVELURELIGHT
		bInstanced = false;
	#endif CHEVELURELIGHT

	HairRenderPass::enSource rendertype = bInstanced?HairRenderPass::RT_INSTANCES:HairRenderPass::RT_HAIRS;
	if(pPass->source != HairRenderPass::RT_DEFAULT)
		rendertype = pPass->source;
	if( rendertype==HairRenderPass::RT_INSTANCES && !bInstanced) 
		return false;

	if( rendertype==HairRenderPass::RT_INSTANCES)
	{
		fprintf(stderr, "HairMath: rendertype==HairRenderPass::RT_INSTANCES not supported!\n");
		fflush(stderr);
		return false;
	}
	///////////////////////////////////
	// mulWidthByLength
	bool mulWidthByLength = pPass->mulWidthByLength;
	if( rendertype==HairRenderPass::RT_INSTANCES) mulWidthByLength = false;

	///////////////////////////////////
	// ������ HairProcedural �� ����� (hairRender->deformblur)
	int dbs = hairRender->deformblur.size();
	std::vector<float> times;
	times.resize(dbs);

	// ��������� �������
	std::vector<DrawSynk*> procedurals(dbs);
	std::vector<HairProceduralCurveOcs> proceduralsCurve;
	std::vector<HairProceduralSn> proceduralsSn;
	std::vector<HairProceduralTubes> proceduralsTubes;

	if( rendertype==HairRenderPass::RT_HAIRS)
	{
		proceduralsCurve.resize(dbs);
		for( int i=0; i<dbs; i++) procedurals[i] = &proceduralsCurve[i];
	}
	else if( rendertype==HairRenderPass::RT_INSTANCES)
	{
		proceduralsSn.resize(dbs);
		for( int i=0; i<dbs; i++)
		{
			procedurals[i] = &proceduralsSn[i];
			#ifndef OCELLARIS_INSTANCE
			{
				std::map< int, sn::ShapeShortcutPtr >::iterator it = hairSystem.snshapes.begin();
				for(;it != hairSystem.snshapes.end(); it++)
					procedurals[i]->setSnShape(it->first, it->second);
			}
			#else
			{
				std::map< int, cls::MInstance>::iterator it = hairSystem.ocsfiles.begin();
				for(;it != hairSystem.ocsfiles.end(); it++)
					procedurals[i]->setOcsShape(it->first, it->second);
			}
			#endif// OCELLARIS_INSTANCE
		}
	}
	else if(rendertype==HairRenderPass::RT_TUBES || rendertype==HairRenderPass::RT_SUBDIVTUBES)
	{
		proceduralsTubes.resize(dbs);
		for( int i=0; i<dbs; i++)
		{
			procedurals[i] = &proceduralsTubes[i];
			proceduralsTubes[i].setSubdiv(rendertype==HairRenderPass::RT_SUBDIVTUBES);
		}
	}

	// degradationHairs
//	std::vector<bool> degradationHairs;
	int countAfterDegradation=count;
	float resDegradation = pPass->degradation + pPass->degradationWidth;
//printf("degradation:%f degradationWidth:%f\n", pPass->degradation, pPass->degradationWidth);

	if(resDegradation<1)
	{	
		countAfterDegradation = 0;
//		degradationHairs.resize(count, false);
		for(int i=0; i<count; i++)
		{
			float degparam;
			bool deg = hairRender->IsHairDegradate(hg->startindex+i, hg->bClump, pPass->degradation, pPass->degradationWidth, degparam);
			if( deg) continue;

//			degradationHairs[i] = true;
			countAfterDegradation++;
		}
	}
	if( countAfterDegradation==0) 
	{
		// �� ��������!
		return true;
	}

	// ����������
	hairRender->nGroups++;
	hairRender->nHairs = countAfterDegradation;

	// extraParameters
	std::map< std::string, cls::Param> extraParameters;
	extraParameters["prune"] = cls::P<float>(hg->prune);
	if( pPass->opacity>=0) 
	{
		Math::Vec3f Os(pPass->opacity);
		extraParameters["Os"] = cls::P<Math::Vec3f>(Os, cls::PT_COLOR);
	}

	// ����
	std::map<float, HairRender::Dynamic*>::iterator it = hairRender->deformblur.begin();
	for(int p=0; it != hairRender->deformblur.end(); it++, p++)
	{
		DrawSynk& procedural = *procedurals[p];
		procedural.setBaseUvSet(hgi->getBaseUVSet());
		bool bNormals = !(pPass->renderNormalSource==RNS_NONE);
		procedural.Init(countAfterDegradation, pPass->SegmCount+1, ((enHairInterpolation)pPass->interpolation), bNormals, &adduv);

		HairRender::Dynamic& dynamicRecord = *it->second;
		times[p] = it->first;

//		HairMesh* surface = &it->second.surface;
		HairMesh& surface = dynamicRecord.surface;

		HairProcessor processor(pPass->SegmCount, surface, hairSystem.snshapes.size(), mulWidthByLength, hairSystem.orthogonalize, true, bClump);

		HairRenderProcessGroup(
			startindex, 
			count, 
			bClump, 
			this, 
			rendertype, 
			processor, 
			pPass,
			procedural, 
			render
			);

	}

	////////////////////////////////////////
	// ��������� �� ������

	render->PushAttributes();

	// dicehair
	int dicehair = 0;
	if( pPass->diceHair) dicehair = 1;
	if( pPass->renderNormalSource!=RNS_NONE)
		dicehair = 0;
	render->Attribute("dice::hair", dicehair);

	// RiSides
	int sides = pPass->renderDoubleSide?2:1;
	render->Attribute("sides", sides);

	// reverseOrientation
	if(pPass->reverseOrientation)
		render->Attribute("orientation::reverse", pPass->reverseOrientation?1:0);
	
	// Raytrace
	if( pPass->renderRayTraceEnable)
	{
		render->Attribute("visibility::camera", pPass->renderRayCamera?1:0);
		render->Attribute("visibility::trace", pPass->renderRayTrace?1:0);
		render->Attribute("visibility::photon", pPass->renderRayPhoton?1:0);
	}

	DrawSynk* freezeprocedural = NULL;

#ifndef SAS
	if( UlHair_licflag!=1)
	{
		goto licflag;
	}
#endif

	if( dbs==1)
	{
		procedurals[0]->Render(NULL, render, extraParameters, freezeprocedural);
	}
	else
	{
		procedurals[0]->RenderBlur(NULL, render, times, procedurals, extraParameters, freezeprocedural);
	}
licflag:
	render->PopAttributes();
	return true;
}
/*/


// This code was written as a compact example, not for speed or generality.
// For example, these are in-memory routines, but in practice the elements
// would be streamed in from a file.
// The "Prune" routine takes an array "eIn" of elements in reverse pruning order
// at distance "z", and returns a pruned array of elements "eOut" to be rendered.
// These graphs illustrate how s and t change near u=0 and u=1:
//
// (when u-trans<0) (when u+trans>1)
// sMax-> s ...sss <- sMax ...sss <- sMax
// |s s . s
// | s s . s
// | s<t> or s<- t -> or .<-t->s <- sMin
// | s . s . |
// | s . s . |
// sMin=0-> +-----s- x ---+------sss... -----+-----+ x
// 0 u u u 1
/*/
Prune( element *eIn, element *eOut, double z, double k1, int nIn, int *nOut ) 
{
	double h=3, kmax=121, k=k1*z*z;
	double u = (z<=1) ? 1 : pow(z,-log(2,h));
	double trans = 0.1; // halfsize of transition region
	double t = (u-trans<0) ? u : (u+trans>1) 1-u : trans; // t is trans adjusted for the ends
	double sMax = (u+trans<1) ? 1/u : 1/(1-t*t/trans); // max area scaling for this u
	double sMin = ( u + trans < 1 ) ? 0 : sMax*(1.0-t/trans); // min area scaling for this u
	*nOut = (u+t) * nIn; // # unpruned, including transition
	for (i=0; i<*nOut; i++) {
		double x = (i+0.5)/nIn; // position in pruning order
		double sLerp = (x<u-t) ? 1 : (x<u+t) ? (u+t-x)/(2*t) : 0;
		double s = sMin + (sMax-sMin) sLerp; // area scaling for this element
		double alpha = sqrt(min(kmax,k*u)/min(kmax,k)); // contrast correction
		eOut[i] = eIn[i];
		eOut[i]->scaleAreaBy(s); // scales area of element
		eOut[i]->scaleContrastBy(alpha); // scales contrast of element
	}
}
/*/


// BYGROUP
void HairRender::GenGroupRIB(
	HairProcessor* processor, 
	int group, 
	const Math::Box3f& snbox, 
	std::vector<HairRenderPass*>& passes
	)
{
//	printf("render group %d\n", group);

	// ��� ��������� ������
	bool bRender = true;
	bool bRenderGroupBox = hairSystem.bRenderBoxes;

	Math::Box3f groupbox;
	float grMaxLen = 0;
	float grMaxDisplace = 0;

	int hairstartindex, hairendindex;
//	rootPositions.getGroup(group, hairstartindex, hairendindex);
	this->getPolygonGroup(group, false, hairstartindex, hairendindex);

	int clumpstartindex, clumpendindex;
//	clumpPositions.getGroup(group, clumpstartindex, clumpendindex);
	this->getPolygonGroup(group, true, clumpstartindex, clumpendindex);
	if(hairstartindex==hairendindex && clumpstartindex==clumpendindex) 
		return;

//printf("group %d = %d-%d\n", group, hairstartindex, hairendindex);

	//////////////////////////////////////////////
	// ������ �����
	std::map<float, Dynamic*>::iterator it = this->deformblur.begin();
	for(;it != this->deformblur.end(); it++)
	{
		Dynamic& dynamic = *it->second;
		HairMesh* surface = &dynamic.surface;

		// ������ ���������� INCREASELENGTH
		std::vector<IHairDeformer*> deformers_INCREASELENGTH;
		int deformercount = surface->getDeformerCount();
		deformers_INCREASELENGTH.reserve(deformercount);
		for(int i=0; i<(int)deformercount; i++)
		{
			IHairDeformer* deformer = surface->getDeformer(i);
			if( !deformer) continue;
			int type = deformer->getType();
			if( type&IHairDeformer::INCREASELENGTH)
				deformers_INCREASELENGTH.push_back(deformer);
		}

		// DEBUG!!!!
		// ��� �������� ���� ����� ����������� �����
		// !!!!!!!!!!!!!!!!!!!!!!!

		// ������ ����������� ������
		for(int vert=hairstartindex; vert<hairendindex; vert++)
		{
			Math::Vec3f pos;
			float u, v;
			surface->getUVAtHair(vert, false, u, v);
			surface->getPosAtHair(vert, false, pos);
			float length = surface->getLengthHair(vert, false, deformers_INCREASELENGTH);
			float disp = surface->getDisplaceHair(vert, false);

			groupbox.insert(pos);
			grMaxDisplace = __max(grMaxDisplace, disp);
			grMaxLen = __max(grMaxLen, length);
		}
		for(int vert=clumpstartindex; vert<clumpendindex; vert++)
		{
			Math::Vec3f pos;
			float u, v;
			surface->getUVAtHair(vert, true, u, v);
			surface->getPosAtHair(vert, true, pos);
			float length = surface->getLengthHair(vert, true, deformers_INCREASELENGTH);
			float disp = surface->getDisplaceHair(vert, true);

			groupbox.insert(pos);
			grMaxDisplace = __max(grMaxDisplace, disp);
			grMaxLen = __max(grMaxLen, length);
		}
	}
	groupbox.expand(grMaxLen);
	groupbox.expand(grMaxDisplace);
	groupbox.min += snbox.min*grMaxLen;
	groupbox.max += snbox.max*grMaxLen;

	// ������ �����
	if( bRenderGroupBox && !hairSystem.bAccurateBoxes)
	{
		// ������ ����� � ������� �����
		float w = 0.01f;
		if( !passes.empty())
			w = passes[0]->RenderWidth;

		render->Parameter("#width", w);
		render->Parameter("#box", groupbox);
		render->RenderCall("Box");
		return;
	}

	////////////////////////////////////
	// RENDER
	if( bRender)
	{
		RtBound bound;
		IPrman::copy(bound, groupbox);

		for(int x=0; x<(int)passes.size(); x++)
		{
			HairRenderPass& pass = *passes[x];

			// hairGroup
			bool bClump = pass.bRenderClumps;
			int startindex, endindex;
			if(bClump)
				startindex = clumpstartindex, endindex = clumpendindex;
			else
				startindex = hairstartindex, endindex = hairendindex;
			if( startindex==endindex)
				continue;
			hairGroup* hg = new hairGroup(this, startindex, endindex, bClump, &pass, pass.prune);

			prman->RiAttributeBegin();

{
char buf[512];
_snprintf(buf, 512, "%s_pass%s_st%d_en%d", this->hairRootPositionsFileName.c_str(), pass.passName.c_str(), startindex, endindex);
char* sss = buf;
prman->RiAttribute( "identifier", "string name", &sss, NULL);
}

			// Raytrace
			this->RayTraceOptions(&pass, prman);

			// RiDetailRange
			if( pass.bUseDetails)
			{
//				prman->RiAttributeBegin();
				prman->RiDetail(bound);

				HairRenderPass::DetailRange& dr = pass.detailrange;
				if( dr.isVisible())
				{
					prman->RiDetailRange(
						dr.minvisible*dr.minvisible, 
						dr.lowertransition*dr.lowertransition, 
						dr.uppertransition*dr.uppertransition, 
						dr.maxvisible*dr.maxvisible);
				}
			}
			// �����
			if( !hairSystem.bAccurateBoxes)
			{
				RtInt reentrant = 1;
				prman->RiAttribute("procedural", "int reentrant", &reentrant, RI_NULL);

//				if( hairSystem.dumpInformation)
//					fprintf(stderr, "hg %d {\n", hg->startindex), fflush(stderr);

				prman->RiProcedural(hg, bound, fur_subdivfunc_group, fur_freefunc_group);
			}
			else
			{
				prman->RiProcedural(hg, bound, fur_subdivfunc_group_accurate, fur_freefunc_group);
			}

//			if( pass.bUseDetails)
//				prman->RiAttributeEnd();
			prman->RiAttributeEnd();
		}
	}
}

