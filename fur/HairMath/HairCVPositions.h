#pragma once

#include "Math/matrixMxN.h"
#include "HAIRCONTROL.h"

#include "HairGeometryIndices.h"

class HairCVPositionsData;
//! CV positions
//!			����������� ����������!!!
struct HAIRMATH_API HairCVPositions
{
	friend class HairCVPositionsData;
public:
	//! copy
	void copy(const HairCVPositions& arg);
	//! operator=
	HairCVPositions& operator=(const HairCVPositions& arg);

protected:
	// �������� facevertex -> cvindex
//	std::map<int, int> cv_binding;
	// �������� cvindex -> facevertex
//	std::vector< std::vector<int> > cvfv_binding;

	// ��������: �� ������ ���� -> ����� ��������
	//...
	// ��������: �� ������ �������� -> ����� ����
	//...
	// ��������� ������ ������������
	int guideCount;

	// ����� ����� � ����������� ������
	int cvcount;

	// �������� � faceverts
	std::vector< std::vector<int> > facevertsByGuide;

public:
	// ����� ��������� � ����������� ������
	int controlVertexCount() const {return cvcount;};
	// ����� ����������� �����
	int controlhairCount() const {return guideCount;};

	// �� ������ ���� - ����� ��������
	int getVertexByGuide(int g)const {return g;}
	// �� ������ �������� - ����� ����
	int getGuideByVertex(int v)const {return v;}
	// �� ������ facevertex - ����� ����
	int getGuideByFacevertex(int fv, const HairGeometryIndices* gi)const 
	{
		return gi->FV_vertexindex(fv);
	}

	// ������ ��-��� ��� ����
	bool getFacevertsListByGuide(int g, const int* &faceverts, int& facevertscount, const HairGeometryIndices* gi)const
	{
		const std::vector<int>& fcs = facevertsByGuide[g];
		faceverts = &fcs[0];
		facevertscount = (int)fcs.size();
		return true;
	}

	// ������ ������������ ������ �� facevertex. ������ -1 ���� ��� ������
//	int facevertexhairindex(int facevertex) const;


	/*/
// ��������!!!
public:
	// �������
	std::vector< HairGeometryIndices::pointonsurface> cvs;
	// ��� ����������� �������������
	Math::matrixMxN<int> regular;

// ��������!!!
public:
	struct WEIGHT
	{
		float w;
		int cvindex;
		WEIGHT(int cvindex=0, float w=0){this->cvindex=cvindex; this->w=w;}
	};
// ��������!!!
public:
	int getCount(){return (int)cvs.size();}
	//! ���� ����������� ����� ��� �������� �����
	//! ���� ���
	bool getWeidths(
		float u, float v, 
		HairGeometryIndices& hgi,
		std::vector< WEIGHT>& weights, 
		bool CVtailU, bool CVtailV
		);
	Math::Vec2f getUV(
		int cv, 
		HairGeometryIndices* geometryIndices, 
		HairGeometry* geometry); 
	// �����
	void getBasis(int cv, 
		HairGeometryIndices* geometryIndices, 
		HairGeometry* geometry, 
		Math::Matrix4f& basis
		);
	/*/

public:
	void serialize(Util::Stream& stream);
	void clear();
};

/*/
// ������ ������������ ������ �� facevertex
inline int HairCVPositions::facevertexhairindex(int facevertex) const
{
	std::map<int, int>::const_iterator it = cv_binding.find(facevertex);
	if(it==cv_binding.end())
		return -1;
	return it->second;
}

/*/
