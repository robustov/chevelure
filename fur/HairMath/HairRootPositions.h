#pragma once

#include <Math/Vector.h>
#include <Math/Matrix.h>
#include <Math/Rotation3.h>
#include <Math/Face.h>
#include <Math/Box.h>
#include <Util/Stream.h>
#include <Util\STLStream.h>
#include <vector>
#include "HairMath.h"
#include "HairGeometryIndices.h"
#include "HairClumpPositions.h"
#pragma warning( disable : 4251)

//! HairRootPositions 
//!			����������� ����������!!!
class HAIRMATH_API HairRootPositions
{
public:
	// copy
	void copy(const HairRootPositions& arg);
	// operator=
	HairRootPositions& operator=(const HairRootPositions& arg);

public:
	//! getCount
	int getCount();
	//! getGroupCount
	int getGroupCount();
	//! HairRootPositions
	void getGroup(int group, int& startindex, int& endindex);
	//! getGroupBox
	Math::Box3f getGroupBox(int group, HairGeometryIndices* geometryIndices, HairGeometry* geometry);
	//! ����� �� �����������
	const HairGeometryIndices::pointonsurface& getPointOnSurface( int index) const;

	//! getUV
	Math::Vec2f getUV(int index, HairGeometryIndices* geometryIndices, HairGeometry* geometry);
	//! getUVset
	Math::Vec2f getUVset(int index, int uvset, HairGeometryIndices* geometryIndices, HairGeometry* geometry);
	//! getPOS
	Math::Vec3f getPOS(int index, HairGeometryIndices* geometryIndices, HairGeometry* geometry);
	//! getBasis
	void getBasis(
		int index, HairGeometryIndices* geometryIndices, HairGeometry* geometry, 
		Math::Vec3f& normal, Math::Vec3f& tangentU, Math::Vec3f& tangentV);
	void getBasis(
		int index, HairGeometryIndices* geometryIndices, HairGeometry* geometry, 
		Math::Vec3f& normal, Math::Vec3f& tangentU, Math::Vec3f& tangentV, 
		Math::Vec3f& dpdU, Math::Vec3f& dpdV);
	//! per hair seed
	int getSeed(int index);
	//! getClumpIndex
	int getClumpIndex(int index);
	//! get polygon index
	int getPolygonIndex(int index, const HairGeometryIndices& geometryIndices);
public:
	std::vector< HairGeometryIndices::pointonsurface> hairs;
	std::vector< int> hairseeds;
	std::vector< HairGeometryIndices::facegroup> hairgroups;
public:
	void serialize(Util::Stream& stream);
	void clear();
	void buildPolygonGroups(
		std::vector<HairGeometryIndices::facegroup>& hairPolygonGroups, 
		const HairGeometryIndices& hgi);
public:
	/*/
	//! ����� ������ � ������� ��� ������
	void findHairGroupNPolygon(
		int h, 
		HairGeometryIndices& geometryIndices,
		int& group,
		int& polygon);
	/*/
	//! testGroup
	void testGroup(int index, int count, int group);
};

inline int HairRootPositions::getCount()
{
	return (int)hairs.size();
}
inline int HairRootPositions::getGroupCount()
{
	if(hairgroups.empty())
		return 1;
	return (int)hairgroups.size();
}
inline void HairRootPositions::getGroup(int group, int& startindex, int& endindex)
{
	startindex = 0;
	endindex = (int)hairs.size();
	if( group<0 || group>=(int)hairgroups.size()) 
		return;

	startindex = hairgroups[group].startindex;
	endindex   = hairgroups[group].endindex;
	return;
}
//! ����� �� �����������
inline const HairGeometryIndices::pointonsurface& HairRootPositions::getPointOnSurface( int index) const
{
	if( index>=(int)hairs.size())
		return HairGeometryIndices::badpointonsurface;
	else
		return hairs[index];
}

inline Math::Vec2f HairRootPositions::getUV(int index, HairGeometryIndices* geometryIndices, HairGeometry* geometry)
{
	if( index>=(int)hairs.size())
		return Math::Vec2f(0, 0);
	HairGeometryIndices::pointonsurface& pos = hairs[index];
	return geometry->getUV(pos.faceindex, pos.b, geometryIndices);
}
//! getUVset
inline Math::Vec2f HairRootPositions::getUVset(int index, int uvset, HairGeometryIndices* geometryIndices, HairGeometry* geometry)
{
	if( index>=(int)hairs.size())
		return Math::Vec2f(0, 0);
	HairGeometryIndices::pointonsurface& pos = hairs[index];
	return geometry->getUVset(pos.faceindex, pos.b, geometryIndices, uvset);
}

inline Math::Vec3f HairRootPositions::getPOS(int index, HairGeometryIndices* geometryIndices, HairGeometry* geometry)
{
	if( index>=(int)hairs.size())
		return Math::Vec3f(0, 0, 0);
	HairGeometryIndices::pointonsurface& pos = hairs[index];
	return geometry->getPOS(pos.faceindex, pos.b, geometryIndices);
}

inline void HairRootPositions::getBasis(
	int index, HairGeometryIndices* geometryIndices, HairGeometry* geometry, 
	Math::Vec3f& normal,
	Math::Vec3f& tangentU,
	Math::Vec3f& tangentV)
{
	if( index>=(int)hairs.size())
		return;
	HairGeometryIndices::pointonsurface& pos = hairs[index];
	geometry->getBasis(pos.faceindex, pos.b, geometryIndices, normal, tangentU, tangentV);
}
inline void HairRootPositions::getBasis(
	int index, HairGeometryIndices* geometryIndices, HairGeometry* geometry, 
	Math::Vec3f& normal,
	Math::Vec3f& tangentU,
	Math::Vec3f& tangentV, 
	Math::Vec3f& dpdU, Math::Vec3f& dpdV)
{
	if( index>=(int)hairs.size())
		return;
	HairGeometryIndices::pointonsurface& pos = hairs[index];
	geometry->getBasis(pos.faceindex, pos.b, geometryIndices, normal, tangentU, tangentV, dpdU, dpdV);
}

inline int HairRootPositions::getClumpIndex(int index)
{
	if( index>=(int)hairs.size())
		return -1;
	HairGeometryIndices::pointonsurface& hr = hairs[ index ];
	return hr.clumpindex;
}

//! per hair seed
inline int HairRootPositions::getSeed(int index)
{
	if( index>=(int)hairs.size())
		return 0;
	return hairseeds[index];
}
inline int HairRootPositions::getPolygonIndex(int index, const HairGeometryIndices& gi)
{
	if( index>=(int)hairs.size())
		return -1;
	return gi.face_polygonIndex(hairs[index].faceindex);
}
