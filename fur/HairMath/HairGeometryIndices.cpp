#include "stdafx.h"
#include "HairGeometryIndices.h"
#include "HairGeometry.h"
#include <Math/Edge.h>
#include <string>

// �����! 
// �������� breakedEdges

HairGeometryIndices::pointonsurface HairGeometryIndices::badpointonsurface;

HairGeometryIndices::uvSet::uvSet()
{
	this->name = "";
}
HairGeometryIndices::uvSet::uvSet(const HairGeometryIndices::uvSet& arg)
{
	copy(arg);
}
HairGeometryIndices::uvSet& HairGeometryIndices::uvSet::operator=(const HairGeometryIndices::uvSet& arg)
{
	copy(arg);
	return *this;
}
void HairGeometryIndices::uvSet::copy(const HairGeometryIndices::uvSet& arg)
{
	this->name			= arg.name;
	this->src_uvcount	= arg.src_uvcount;
	this->uvcount		= arg.uvcount;
	this->faces_uv		= arg.faces_uv;
	this->sd_uvquads	= arg.sd_uvquads;
	this->sd_uvverts	= arg.sd_uvverts;
}
template<class Stream> inline
Stream& operator >> (Stream& out, HairGeometryIndices::uvSet& v)
{
	out >> v.name;
	out >> v.src_uvcount;
	out >> v.uvcount;
	out >> v.faces_uv;
	out >> v.sd_uvquads;
	out >> v.sd_uvverts;
	return out;
}


HairGeometryIndices::HairGeometryIndices()
{
	bValid = true;

	baseUvSet = "";
	src_vertcount = src_poligoncount = src_normalcount = src_uvcount = -1;
	vertcount = normalcount = uvcount = -1;

}
bool HairGeometryIndices::isValid(
	int vertcount, int poligoncount, int normalcount, int uvcount, int nSubdiv, const char* baseUvSet, int uvSetCount, std::string* uvsetnames, int* uvsetcounts)
{
	if( vertcount != src_vertcount ||
		poligoncount != src_poligoncount || 
		normalcount != src_normalcount ||
		uvcount != src_uvcount ||
		nSubdiv != subdivideSteps || 
		uvSetCount != uvSets.size() ||
		this->baseUvSet	!= baseUvSet)
	{
		clear();
		return false;
	}
	if(uvSetCount)
	{
		for(int i=0; i<uvSetCount; i++)
		{
			if( uvsetnames[i]!=uvSets[i].name ||
				uvsetcounts[i]!=uvSets[i].src_uvcount)
			{
				clear();
				return false;
			}
		}
	}
	return true;
}
void HairGeometryIndices::init(int vertcount, int poligoncount, int normalcount, int uvcount, int nSubdiv, const char* baseUvSet, int uvSetCount, std::string* uvsetnames, int* uvsetcounts)
{
	this->src_poligoncount	= poligoncount;
	this->src_vertcount		= vertcount;
	this->src_normalcount	= normalcount;
	this->src_uvcount		= uvcount;
	this->subdivideSteps	= nSubdiv;	//! ����� ����� subdivide 

	this->vertcount		= vertcount;
	this->normalcount	= normalcount;
	this->uvcount		= uvcount;
	this->baseUvSet		= baseUvSet;

	this->uvSets.resize(uvSetCount);
	for(unsigned i=0; i<this->uvSets.size(); i++)
	{
		this->uvSets[i].name = uvsetnames[i];
		this->uvSets[i].src_uvcount = uvsetcounts[i];
		this->uvSets[i].uvcount		= uvsetcounts[i];
	}
}

void HairGeometryIndices::copy(const HairGeometryIndices& arg)
{
	this->src_poligoncount	= arg.src_poligoncount;
	this->src_vertcount		= arg.src_vertcount;
	this->src_normalcount	= arg.src_normalcount;
	this->src_uvcount		= arg.src_uvcount;
	this->baseUvSet			= arg.baseUvSet;

	this->vertcount =    arg.vertcount;
	this->normalcount  = arg.normalcount;
	this->uvcount		= arg.uvcount;

	this->faces =       arg.faces;
	this->faces_uv =	arg.faces_uv;
	this->faces_n  =	arg.faces_n;
	this->associatedFaces = arg.associatedFaces;
	this->associatedFacesToVetrexIndices = arg.associatedFacesToVetrexIndices;
	this->associatedFacesToVetrex = arg.associatedFacesToVetrex;

	this->faceseeds =	arg.faceseeds;

	this->subdivideSteps = arg.subdivideSteps;	//! ����� ����� subdivide 
	this->sd_quads		= arg.sd_quads;			
	this->sd_uvquads	= arg.sd_uvquads;
	this->sd_nquads		= arg.sd_nquads;			
	this->sd_verts		= arg.sd_verts;
	this->sd_uvverts	= arg.sd_uvverts;			
	this->sd_nverts		= arg.sd_nverts;
	this->uvSets		= arg.uvSets;
	this->faces_polygonIndex  = arg.faces_polygonIndex;
	this->brokenEdges = arg.brokenEdges;

	// 13.03.2008
	this->src_faces = arg.src_faces;
	this->src_faces_uv = arg.src_faces_uv;
	this->src_faces_n = arg.src_faces_n;
}

HairGeometryIndices& HairGeometryIndices::operator=(const HairGeometryIndices& arg)
{
	HairGeometryIndices::copy(arg);
	return *this;
}

void HairGeometryIndices::serialize(Util::Stream& stream)
{
	int version = 6;
	stream >> version;
	if( version>=5)
	{
		stream >> src_vertcount >> src_poligoncount >> src_normalcount >> src_uvcount;

		stream >> vertcount >> normalcount >> uvcount;

		stream >> faces;
		stream >> faces_uv;
		stream >> faces_n;
		stream >> associatedFaces;
		stream >> associatedFacesToVetrexIndices; //!< ������ = vertcount+1
		stream >> associatedFacesToVetrex;

		stream >> faceseeds;

		stream >> subdivideSteps;
		stream >> sd_quads >> sd_uvquads >> sd_nquads;
		stream >> sd_verts >> sd_uvverts >> sd_nverts;

		stream >> uvSets;
		stream >> baseUvSet;

		stream >> faces_polygonIndex;
		if(faces_polygonIndex.size()!=faces.size())
			faces_polygonIndex.resize(faces.size());

		stream >> brokenEdges;

		// 13.03.2008
		if(version>=6)
		{
			stream >> this->src_faces;
			stream >> this->src_faces_uv;
			stream >> this->src_faces_n;
		}

		bValid = true;
	}
	else
		bValid = false;

	bool bDump = false;
	if( bDump)
	{
		FILE* file = fopen("d:/hair.log", "wt");
		for(int s=0; s<(int)uvSets.size(); s++)
		{
			uvSet& set = uvSets[s];
			fprintf( file, "\n\nUVSET:%d\n%s %d(%d)\n", s, set.name.c_str(), set.src_uvcount, set.uvcount);
			for(int f=0; f<(int)set.faces_uv.size(); f++)
			{
				Math::Face32& f32 = set.faces_uv[f];
				fprintf( file, "%d, %d, %d\n", f32.v[0], f32.v[1], f32.v[2]);
			}
		}
		fclose(file);
	}
}
void HairGeometryIndices::clear()
{
	src_vertcount = src_poligoncount = src_normalcount = src_uvcount = -1;
	vertcount = normalcount = uvcount = 0;
	faces.clear();
	faces_uv.clear();
	faces_n.clear();
	faceseeds.clear();
	associatedFaces.clear();
	associatedFacesToVetrexIndices.clear();
	associatedFacesToVetrex.clear();
	uvSets.clear();
	baseUvSet = "";

	subdivideSteps = 0;
	sd_quads.clear(); sd_uvquads.clear(); sd_nquads.clear();
	sd_verts.clear(); sd_uvverts.clear(); sd_nverts.clear();
	faces_polygonIndex.clear();
	brokenEdges.clear();

	// 13.03.2008
	this->src_faces.clear();
	this->src_faces_uv.clear();
	this->src_faces_n.clear();
}

unsigned long HairGeometryIndices::finduvSetByName(const char* name)const
{
	for( int i=0; i<(int)uvSets.size(); i++)
	{
		if(uvSets[i].name==name)
			return i;
	}
	return -1;
}

void HairGeometryIndices::buildAssociated()
{
	// associatedFacesToVetrexIndices & associatedFacesToVetrex
	int fullsize = 0;
	std::vector< std::vector<int> > faceVertexList(vertcount);
	for(unsigned f=0; f<faces.size(); f++)
	{
		Math::Face32& face = faces[f];
		for(int v=0; v<3; v++)
		{
			unsigned vert = face.v[v];
			faceVertexList[vert].push_back(f);
			fullsize++;
		}
	}
	this->associatedFacesToVetrexIndices.resize(vertcount+1);
	this->associatedFacesToVetrex.clear();
	this->associatedFacesToVetrex.reserve(fullsize);
	unsigned v;
	for(v=0; v<faceVertexList.size(); v++)
	{
		this->associatedFacesToVetrexIndices[v] = (int)this->associatedFacesToVetrex.size();
		for(unsigned fv=0; fv<faceVertexList[v].size(); fv++)
			this->associatedFacesToVetrex.push_back(faceVertexList[v][fv]);
	}
	this->associatedFacesToVetrexIndices[v] = this->associatedFacesToVetrex.size();

	// associatedFaces
	Math::tag_edge_list edges;
	GetMeshEdgeList(&faces[0], (int)faces.size(), edges);

	associatedFaces.resize(faces.size(), Math::Face32(0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF));
	Math::tag_edge_list::iterator it = edges.begin();
	for(;it != edges.end(); it++)
	{
//		const Math::Edge32& edge = it->first;
		Math::EdgeBind32& edgebind = it->second;
		if( edgebind.facecount()!=2) 
			continue;
		// � ������ ����� �������!, �� ��������!
		if( brokenEdges.find(it->first)!=brokenEdges.end())
			continue;

		Math::Face32& f1 = associatedFaces[edgebind.face1];
		Math::Face32& f2 = associatedFaces[edgebind.face2];
		f1.v[edgebind.face1edge] = edgebind.face2;
		f2.v[edgebind.face2edge] = edgebind.face1;
	}
}

//! ����� ������������ � ������� ������ �������
void HairGeometryIndices::findVertexAssociatedFaces(
	int vertex,					//!< vertex
	int*& faces,				//!< ��������� ������
	int& count
	)
{
	if(vertex>=(int)associatedFacesToVetrexIndices.size())
	{
		faces = 0;
		count = 0;
		return;
	}
	int startFV = associatedFacesToVetrexIndices[vertex];
	int endFV = associatedFacesToVetrexIndices[vertex+1];

	faces = &associatedFacesToVetrex[0]+startFV;
	count = endFV-startFV;
}
//! ����� ��� facevertex ��� ������� ��������
void HairGeometryIndices::findFacevertexForVertex(
	int vertex,					//!< vertex
	std::vector<int>& fvs		//!< ��������� ������
	)
{
	int* faces;
	int count;
	findVertexAssociatedFaces(
		vertex, faces, count);
	fvs.clear();
	fvs.reserve(count);
	for( int i=0; i<count; i++)
	{
		Math::Face32& f = face( faces[i]);
		Math::Face32& ffv = fvface( faces[i]);
		for(int v=0; v<3; v++)
			if(f[v]==vertex)
				fvs.push_back(ffv[v]);
	}
}

//! ����� ��-�� ����������� ����� � ��-���
void HairGeometryIndices::findAssociatedFaces(
	HairGeometry* geometry,
	int f, 
	float radius, 
	std::vector<int>& wantedfaces, 
	int& callCount)
{
	wantedfaces.clear();

	static std::vector<unsigned char> selectedfaces;
	selectedfaces.resize(faces.size(), 0);
	selectedfaces.assign(faces.size(), 0);
	selectedfaces[f] = 1;

	int maxCallCount = callCount;
	callCount = 0;

	if(brokenEdges.empty())
	{
		Math::Face32& face = faces[f];
		for(int v=0; v<3; v++)
		{
			int cc=0;
			findAssociatedFaces_rec( geometry, face.v[v], radius, selectedfaces, cc, maxCallCount);
			callCount+=cc;
		}
	}
	else
	{
		findAssociatedFaces_rec2(geometry, f, radius, selectedfaces, callCount, maxCallCount);
	}
	for(unsigned sf=0; sf<selectedfaces.size(); sf++)
		if( selectedfaces[sf])
			wantedfaces.push_back(sf);
}
void HairGeometryIndices::findAssociatedFaces_rec2( 
	HairGeometry* geometry,
	unsigned face, 
	float radius, 
	std::vector<unsigned char>& selectedfaces, 
	int& callCount, 
	int maxCallCount)
{
	if( callCount>=maxCallCount)
		return;
	callCount++;

	const Math::Face32& aface = associatedFaces[face];
	for(int v=0; v<3; v++)
	{
		unsigned long f = aface[v];
		if( f==0xFFFFFFFF) 
			continue;
		if( selectedfaces[f])
			continue;

		// �����!!! ��� � ����������� ����, ��� ������� �� ����
		// ����������
		float dist = 0.01f;
		if( dist>radius)
			continue;
		// ����� �����
		dist = radius-dist;

		findAssociatedFaces_rec( geometry, f, dist, selectedfaces, callCount, maxCallCount);
	}
}

void HairGeometryIndices::findAssociatedFaces_rec( 
	HairGeometry* geometry,
	unsigned vertex, 
	float radius, 
	std::vector<unsigned char>& selectedfaces, 
	int& callCount, 
	int maxCallCount)
{
	if( callCount>=maxCallCount)
		return;
	callCount++;
	Math::Vec3f& point = geometry->vertex(vertex);
	// for each face associated to vertex
	int startFV = associatedFacesToVetrexIndices[vertex];
	int endFV = associatedFacesToVetrexIndices[vertex+1];
	for(int fv=startFV; fv<endFV; fv++)
	{
		unsigned f = associatedFacesToVetrex[fv];

		if( selectedfaces[f])
			continue;

		selectedfaces[f] = true;

		Math::Face32& face = faces[f];
		for(int v=0; v<3; v++)
		{
			if(face.v[v]==vertex) continue;
			// ����������
			float dist = (geometry->vertex(face.v[v])-point).length();
			if( dist>radius)
				continue;
			// ����� �����
			dist = radius-dist;
			findAssociatedFaces_rec( geometry, face.v[v], dist, selectedfaces, callCount, maxCallCount);
		}
	}
}
// ����� ������������
void stuped_triangulation(Math::Quad32& quad, Math::Face32* dst)
{
	dst[0] = Math::Face32(quad[0], quad[1], quad[2]);
	dst[1] = Math::Face32(quad[2], quad[3], quad[0]);
}

// ��������� ����� subdivide 
void HairGeometryIndices::buildSubdivisionScheme(
	int level, 
	int facevertexcount, 
	std::vector<Math::Polygon32>& sd_faces, 
	std::vector<Math::Polygon32>& sd_uvfaces, 
	std::vector<Math::Polygon32>& sd_nfaces)
{
	this->subdivideSteps = level;
	if(this->subdivideSteps<=0) return;

	// 13.03.2008
	this->src_faces = this->faces;
	this->src_faces_uv = this->faces_uv;
	this->src_faces_n = this->faces_n;
	

	// �����! brokenEdges
	// brokenEdges;

	std::vector<int> quads_polygonIndex;
	Subdiv(level, &sd_faces[0], (int)sd_faces.size(), this->src_vertcount, facevertexcount, sd_quads, sd_verts, &quads_polygonIndex);
	vertcount = uvcount = normalcount = (int)sd_verts.size();
	if(!sd_uvfaces.empty())
	{
		Subdiv(level, &sd_uvfaces[0], sd_uvfaces.size(), this->src_uvcount, facevertexcount, sd_uvquads, sd_uvverts);
		uvcount = sd_uvverts.size();
	}
	if(!sd_nfaces.empty())
	{
		Subdiv(level, &sd_nfaces[0], sd_nfaces.size(), this->src_normalcount, facevertexcount, sd_nquads, sd_nverts);
		normalcount = sd_nverts.size();
	}

	// ��� ���� ������������������� � ����������� �������:
	int facecount = sd_quads.size()*2;
	faces.resize(facecount);
	faces_uv.resize(facecount);
	faces_n.resize(facecount);
	faceseeds.resize(facecount);
	faces_polygonIndex.resize(facecount);
	for(unsigned f=0; f<sd_quads.size(); f++)
	{
		stuped_triangulation(sd_quads[f], &faces[f*2]);
		if( !sd_uvquads.empty())
			stuped_triangulation(sd_uvquads[f], &faces_uv[f*2]);
		else
			stuped_triangulation(sd_quads[f], &faces_uv[f*2]);

		if( !sd_nquads.empty())
			stuped_triangulation(sd_nquads[f],	&faces_n[f*2]);
		else
			stuped_triangulation(sd_quads[f], &faces_n[f*2]);

		faceseeds[f*2+0] = rand();
		faceseeds[f*2+1] = rand();

		faces_polygonIndex[f*2+0] = quads_polygonIndex[f];
		faces_polygonIndex[f*2+1] = quads_polygonIndex[f];
	}
}
// ����� �������� ������ ����� buildSubdivisionScheme
void HairGeometryIndices::buildSubdivisionSchemeForUv(
	int level, 
	int facevertexcount, 
	int set,
	std::vector<Math::Polygon32>& sd_uvfaces
	)
{
	uvSet& uvset = uvSets[set];
	uvset.uvcount = vertcount;

	// Subdiv
	if( !sd_uvfaces.empty())
	{
		Subdiv(level, &sd_uvfaces[0], sd_uvfaces.size(), uvset.src_uvcount, facevertexcount, uvset.sd_uvquads, uvset.sd_uvverts);
		uvset.uvcount = uvset.sd_uvverts.size();
	}

	// ��� ���� ������������������� � ����������� �������:
	int facecount = this->sd_quads.size()*2;
	uvset.faces_uv.resize(facecount);
	for(unsigned f=0; f<uvset.sd_uvquads.size(); f++)
	{
		stuped_triangulation(uvset.sd_uvquads[f], &uvset.faces_uv[f*2]);
	}
}

