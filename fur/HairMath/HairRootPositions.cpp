#include "stdafx.h"
#include "HairRootPositions.h"

//! getGroupBox
Math::Box3f HairRootPositions::getGroupBox(int group, HairGeometryIndices* geometryIndices, HairGeometry* geometry)
{
	Math::Face32& face = geometryIndices->face(group);
	Math::Vec3f pos;
	pos = geometry->vertex(face.v[0]);
	pos += geometry->vertex(face.v[1]);
	pos += geometry->vertex(face.v[2]);
	pos = pos/3;

	float s = (geometry->box.dx()+geometry->box.dy()+geometry->box.dz())/3;
	float div = (float)geometryIndices->faceCount();
	s = s/sqrt( div);
	return Math::Box3f(pos, s);
}


// ������:

void HairRootPositions::copy(const HairRootPositions& arg)
{
	this->hairs = arg.hairs;
	this->hairseeds = arg.hairseeds;
	this->hairgroups = arg.hairgroups;
}

HairRootPositions& HairRootPositions::operator=(const HairRootPositions& arg)
{
	HairRootPositions::copy(arg);
	return *this;
}

void HairRootPositions::serialize(Util::Stream& stream)
{
	int version = 2;
	stream >> version;
	if( version==2)
	{
		stream >> hairs;
		stream >> hairgroups;
		stream >> hairseeds;
	}
}
void HairRootPositions::clear()
{
	hairs.clear();
	hairseeds.clear();
	hairgroups.clear();
}
void HairRootPositions::buildPolygonGroups(
	std::vector<HairGeometryIndices::facegroup>& hairPolygonGroup, 
	const HairGeometryIndices& hgi)
{
	int c=0;
	int i;
	for(i=0; i<(int)hgi.faceCount(); i++)
		c = std::max(c, hgi.face_polygonIndex(i)+1);
	
	hairPolygonGroup.resize(c);
	for(i=0; i<(int)hgi.faceCount(); i++)
	{
		if(i>=(int)hairgroups.size()) break;
		HairGeometryIndices::facegroup& src = hairgroups[i];
		int p = hgi.face_polygonIndex(i);
		HairGeometryIndices::facegroup& dst = hairPolygonGroup[ p];
		if( dst.endindex==dst.startindex)
			dst = src;
		else
		{
			dst.startindex = std::min(dst.startindex, src.startindex);
			dst.endindex = std::max(dst.endindex, src.endindex);
		}
	}
	/*/
	for( i=0; i<hairPolygonGroup.size(); i++)
	{
		printf("%d: %d-%d\n", i, hairPolygonGroup[i].startindex, hairPolygonGroup[i].endindex);
	}
	/*/
}

void HairRootPositions::testGroup(int index, int count, int group)
{
	int err = 0;
	for(int i=0; i<count; i++)
	{
		HairGeometryIndices::pointonsurface& pos = hairs[index];
		if(pos.faceindex!=group)
			err++;
	}
	if( err)
	{
		fprintf(stderr, "group %d, err %d(%d)\n", group, err, count);
	}
}
/*/
//! ����� ������ � ������� ��� ������
void HairRootPositions::findHairGroupNPolygon(
	int h, 
	HairGeometryIndices& geometryIndices,
	int& group,
	int& polygon)
{
	for( int i=0; i<getGroupCount(); i++)
	{
		int start, end;
		getGroup(i, start, end);
		if(h<start || h>=end) 
			continue;
		group = i;
		polygon = geometryIndices.face_polygonIndex(group);
		return;
	}
}
/*/
