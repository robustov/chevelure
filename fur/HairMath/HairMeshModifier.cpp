#include "stdafx.h"
	#include "ocellaris/IRender.h"
#include "HairMeshModifier.h"

extern float min_hairLen;

HairMeshModifier::HairMeshModifier()
{
	modiferType = MT_SPHERE;
	directionType = DT_NONE;
}

void HairMeshModifier::LoadParams(
	HAIRPARAMS& c_hairs, const Math::Vec2f& _v, float U, float V, bool bClump)
{
	c_hairs.displace = this->getDisplace(U, V);

	c_hairs.length		 = this->getLength(U, V);
	c_hairs.lengthjitter = this->getLengthJitter(U, V);
	if( bClump)
		c_hairs.lengthjitter = 1;

	c_hairs.polar       = this->getPolar(U, V);	// ��� �������������

	int i;
	for(i=0; i<(int)deformers.size(); i++)
	{
		IHairDeformer* deformer = deformers[i].deformer;
		if( deformer && (deformer->getType()&IHairDeformer::PREPARSEHAIR))
		{
			float affect = deformer->affect.getValue(U, V);
			deformer->PreParseHair(affect, c_hairs, bClump);
		}
	}

	float theta = (float)(M_PI * (2 * c_hairs.polar));
	Math::Vec2f v = Math::rotate( _v, theta);
	c_hairs.polar = vector2polar(v);

	c_hairs.inclination = v*this->getInclination(U, V);
	c_hairs.baseCurl	= v*(this->getCurl(U, V, 0)-0.5f)*(float)M_PI;
	c_hairs.tipCurl		= v*(this->getCurl(U, V, 1)-0.5f)*(float)M_PI;

	c_hairs.scraggle	= this->getScraggle(U, V);
	c_hairs.scraggleFreq= this->getScraggleFreq(U, V);

	c_hairs.BaseClumpValue = this->getClump(U, V, 0);
	c_hairs.TipClumpValue = this->getClump(U, V, 1);

	c_hairs.twist		= this->getTwist(U, V);

	c_hairs.baseWidth	= this->getWidth(U, V, 0);
	c_hairs.tipWidth	= this->getWidth(U, V, 1);

	for(i=0; i<(int)deformers.size(); i++)
	{
		IHairDeformer* deformer = deformers[i].deformer;
		if( deformer && (deformer->getType()&IHairDeformer::PARSEHAIR))
		{
			float affect = deformer->affect.getValue(U, V);
			deformer->ParseHair(affect, c_hairs, bClump);
		}
	}
}

// ����� ���������� (������������ ����)
bool HairMeshModifier::CallBuildHair(
	float weight, 
	const Math::Vec3f& dirToModifier,
	const HAIRPARAMS& c_hairs, 			// ��������� ������
	int SegmCount,					// ����� ���������
	HAIRVERTS& hair					// ���������
	)
{
	for(int i=0; i<(int)deformers.size(); i++)
	{
		IHairDeformer* deformer = deformers[i].deformer;
		if( deformer && (deformer->getType()&IHairDeformer::BUILDHAIR))
		{
			float affect = deformer->affect.getValue(c_hairs.u, c_hairs.v);
			if( deformer->BuildHair(weight*affect, c_hairs, SegmCount, hair))
				return true;
		}
	}
	return false;
}
void HairMeshModifier::CallPostBuildHair(
	float weight, 
	const Math::Vec3f& dirToModifier,
	const HAIRPARAMS& c_hairs, 			// ��������� ������
	int SegmCount,					// ����� ���������
	HAIRVERTS& hair					// ���������
	)
{
	for(int i=0; i<(int)deformers.size(); i++)
	{
		IHairDeformer* deformer = deformers[i].deformer;
		if( deformer && (deformer->getType()&IHairDeformer::POSTBUILDHAIR))
		{
			float affect = deformer->affect.getValue(c_hairs.u, c_hairs.v);
			deformer->PostBuildHair(weight*affect, c_hairs, SegmCount, hair);
		}
	}
}
bool HairMeshModifier::CallMergeWithClump(
	float weight, 
	const Math::Vec3f& dirToModifier,
	const HAIRPARAMS& c_hairs, 			// ��������� ������
	const HAIRVERTS& orighair,		// �������� �����
	const HAIRPARAMS& c_clumphair, 	// ��������� ������ - ������
	const HAIRVERTS& clumphair,		// ����� � ������
	HAIRVERTS& hair					// ���������
	)
{
	for(int i=0; i<(int)deformers.size(); i++)
	{
		IHairDeformer* deformer = deformers[i].deformer;
		if( deformer && (deformer->getType()&IHairDeformer::MERGEWITHCLUMP))
		{
			float affect = deformer->affect.getValue(c_hairs.u, c_hairs.v);
			if( deformer->MergeWithClump(weight*affect, c_hairs, orighair, c_clumphair, clumphair, hair))
				return true;
		}
	}
	return false;
}

void HairMeshModifier::serialize(Util::Stream& stream)
{
	int version = 3;
	stream >> version;
	if( version>=0)
	{
		stream >> transform;		// � ������������ HairShape
		stream >> affectRamp;

		stream >> length	>> inclination >> polar;
		stream >> baseCurl	>> tipCurl;
		stream >> baseWidth >> tipWidth;
		stream >> scragle	>> scragleFreq;
		stream >> baseClump >> tipClump;
		stream >> twist;
		stream >> displace;
		if(stream.isLoading())
			transform_inv = transform.inverted();
	}
	if( version>=1)
	{
		stream >> hairParamBit;
	}
	if( version>=2)
	{
		stream >> bRelative;
		stream >> deformers;
	}
	if( version>=3)
	{
		stream >> *(int*)&modiferType;
		stream >> *(int*)&directionType;
	}
}
// ������������ ����������� �� �������
bool HairMeshModifier::isRelative()
{
//	return bRelative;
	return directionType!=DT_NONE;
}




#ifdef OCELLARIS
void HairMeshModifier::serializeOcs(const char* _name, cls::IRender* render, bool bSave)
{
	std::string name = _name;

	SERIALIZEOCS( transform);		// �� ������������ HairShape � ������������ ��������
	SERIALIZEOCS( transform_inv);	// �� ������������ �������� � ������������ HairShape
	// ��������
	SERIALIZEOCSPROC( affectRamp);
	// ���������
	SERIALIZEOCSPROC( length);
	SERIALIZEOCSPROC( inclination);
	SERIALIZEOCSPROC( polar);
	SERIALIZEOCSPROC( baseCurl);
	SERIALIZEOCSPROC( tipCurl);
	SERIALIZEOCSPROC( baseWidth);
	SERIALIZEOCSPROC( tipWidth);
	SERIALIZEOCSPROC( scragle);
	SERIALIZEOCSPROC( scragleFreq);
	SERIALIZEOCSPROC( baseClump);
	SERIALIZEOCSPROC( tipClump);
	SERIALIZEOCSPROC( twist);
	SERIALIZEOCSPROC( displace);

	SERIALIZEOCS( hairParamBit);	// ���� ����������
	SERIALIZEOCS( bRelative);

	SERIALIZEOCSENUM( modiferType);
	SERIALIZEOCSENUM( directionType);
	
	int i;
	for(i=0; i<(int)deformers.size(); i++)
	{
		char buf[256];
		sprintf(buf, "%s::deformer%d", _name, i);
		deformers[i].serializeOcs(buf, render, bSave);
	}
}
#endif
