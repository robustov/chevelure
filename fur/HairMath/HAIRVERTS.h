#pragma once
//#include "hairmath.h"
#include "Math/Math.h"
#include <vector>

inline bool _isnan( const Math::Vec3f& v)
{
	return _isnan( v.x) || _isnan( v.y) || _isnan( v.z);
}
inline bool _isnan( const Math::Matrix4f& v)
{
	const float* d = v.data();
	for(int i=0; i<16; i++)
		if( _isnan( d[i]))
			return true;
	return false;
}

#pragma warning( disable : 4251)

struct HAIRVERTS
{
	float clumpradius;				// ������ ������ � ������� ������ ����� ��� ��� �����
	Math::Vec3f clump_normal;		// ������� � ����� ������
	Math::Vec3f clump_vector;		// ����������� �� ����� ������ � ������ ������
	std::vector<Math::Vec3f> clump_vectorV;		// ����������� �� �������� ������ � ������� ������
	std::vector<float> vclump;		// �������� ������ � ��������� (0-�� ������������ 1-������������)

	std::vector<Math::Vec3f> cvint;		// ����� � object space
	std::vector<float> vfactor;			// �������� ��� ������������/�� �����
	std::vector<float> vwidht;			// ������ � ������
	std::vector<Math::Matrix4f> basas;	// ������ � tangent space
	std::vector<Math::Vec3f> pt_nonoise;	// ����� ��� ����� ����

	std::vector<float> deformers_data;	// ������ ��� ����������

	int vc(){return (int)cvint.size();}

	void clear(int sizein_HAIRVERTS);

	Math::Vec3f getPointByParam(float f, float* w=NULL) const;
	float getLength() const;

	bool _isnan();
};

inline float HAIRVERTS::getLength() const
{
	float l=0;
	for(int i=1; i<(int)cvint.size(); i++)
	{
		l += (cvint[i-1]-cvint[i]).length();
	}
	return l;
}
inline Math::Vec3f HAIRVERTS::getPointByParam(float f, float* w) const
{
	if(f<this->vfactor[0])
	{
		if(w) *w = vwidht[0];
		return this->cvint[0];
	}
	if(f>=this->vfactor[this->cvint.size()-1])
	{
		if(w) *w = vwidht[this->vwidht.size()-1];
		return this->cvint[this->cvint.size()-1];
	}

	for(int g=1; g<(int)this->cvint.size(); g++)
	{
		if( this->vfactor[g-1] <= f && f < this->vfactor[g])
		{
			float factor = (f-this->vfactor[g-1])/(this->vfactor[g]-this->vfactor[g-1]);
			Math::Vec3f pt = (1-factor)*this->cvint[g-1] + factor*this->cvint[g];

			if(w) *w = (1-factor)*vwidht[g-1] + factor*vwidht[g];
			return pt;
		}
	}

	if(w) *w = 0;
	return Math::Vec3f(0, 0, 0);
}
