#pragma once

#include "HairMath.h"
//#include "HairParam.h"

#include <Math/Vector.h>
#include <Math/Matrix.h>
#include <Math/Rotation3.h>
#include <Math/Face.h>
#include "Math/matrixMxN.h"
#include "MathNMaya/ParamTex.h"
#include <Util/Stream.h>
#include <Util\STLStream.h>
#include <vector>

#include "HairRootPositions.h"
#include "HairClumpPositions.h"
#include "HairGeometryIndices.h"
#include "HairCVPositions.h"
#include "HairGeometry.h"
//#include "HairProcessor.h"
#include "HairCV.h"
#include "HairSystem.h"
#include "HairMeshModifier.h"
#include "HairDeformerDecl.h"
#include "HairParameters.h"

#pragma warning( disable : 4251)

//! HairMesh
// ������ ������ � ���������� ����� (�����, ����� � ������)
class HAIRMATH_API HairMesh : public HairParameters
{
public:
	virtual void getParamRange( 
		double& startU, double& endU, double& startV, double& endV);
	virtual bool getPointAtParam(
		float u, float v, 
		Math::Vec3f& pos, 
		Math::Vec3f& normal,
		Math::Vec3f& tangentU,
		Math::Vec3f& tangentV, 
		Math::Vec3f& dpdU,
		Math::Vec3f& dpdV);

	// ���������
	struct ModifierData
	{
		float w;							// ��� ��������
		Math::Vec3f dirToModifier;			// ����������� �� �������
		HairMeshModifier* modifier;			// �������
		ModifierData(HairMeshModifier* modifier=0, float w=0, Math::Vec3f dirToModifier=Math::Vec3f(0)){this->modifier=modifier; this->w=w; this->dirToModifier=dirToModifier;}
	};
	struct ModifiersData
	{
		float w;				// ��� ������� ����������
		unsigned int hpb;		// ���� ����������
		std::vector<ModifierData> affectmodifiers;
		ModifiersData(){hpb = 0;}
		bool empty(){return affectmodifiers.empty();}
	};
	// ������ ��������� ��� �����
	bool getModifiersAtPoint(
		const Math::Vec3f& pos, 
		ModifiersData& md);

	// ��� ������� �����
	// ������������ �����
	float getMaxLength(
		const std::vector<IHairDeformer*>& deformers_INCREASELENGTH
		);
	// common 
	// ����� ��� �������
	float getLength(
		float u, float v, 
		const std::vector<Math::Vec2f>& uvSets, 
		const std::vector<IHairDeformer*>& deformers_INCREASELENGTH
		);
	float getLengthJitter();	// ������ �� ����� (Math::EJT_MUL)
	float getInclination(float u, float v, bool bNoJitter=false);
	float getPolar(float u, float v, bool bNoJitter=false);
	float getPolarDeriv(float u, float v, float polar);
	float getCurl(float u, float v, float param, bool bNoJitter=false);
	float getWidth(float u, float v, float param);
	float getScraggle(float u, float v);
	float getScraggleFreq(float u, float v);
	float getClump(float u, float v, float param);
	float getTwist(float u, float v, bool bNoJitter=false);
	float getDisplace(float u, float v, bool bNoJitter=false);

	// ������
	virtual int getGroupCount();
	virtual void getGroup(int group, int& startindex, int& endindex);

	// ������ ���������������� (0-1) {u, v} ��� ������ 
	virtual void getUVAtHair(
		int index, bool bClump, 
		float& u, float& v);
	virtual void getUVSetAtHair(
		int index, bool bClump, int uvsetindex, 
		Math::Vec2f& res);
	virtual void getUVSetAtHair(
		int index, bool bClump, 
		std::vector<Math::Vec2f>& res);
	virtual void getPosAtHair(
		int index, bool bClump, 
		Math::Vec3f& pos);
	// ���������� �� �������� ����� 
	virtual void getPointAtHair(
		int index, bool bClump, 
		Math::Vec3f& pos, 
		Math::Vec3f& normal,
		Math::Vec3f& tangentU,
		Math::Vec3f& tangentV, 
		Math::Vec3f& dpdU,
		Math::Vec3f& dpdV);
	// ���������� �� �������� ����� 
	virtual int getClumpIndexAtHair(
		int index);
	//! per hair seed
	int getSeed(int index, bool bClump);
	//! get hair or clump polygon index
	int getPolygonIndex(int index, bool bClump);
	//! get control hair for hair
	bool getControlHair( int index, bool bClump, HAIRCONTROL& CV)const;
	bool getControlHair( const HairGeometryIndices::pointonsurface& pos, HAIRCONTROL& CV) const;

	// ��������� � ����������
	int getDeformerCount();
	IHairDeformer* getDeformer(int i);
	// ������������� ����������
	void InitDeformers();
	// ������������. ������������ ����� �������� "Hair::lengthfactor"
	void Randomize(float lengthfactor, int seedbias);
public:
	float getLengthHair(
		int vert, bool bClump, 
		const std::vector<IHairDeformer*>& deformers_INCREASELENGTH
		);
	float getDisplaceHair(int vert, bool bClump);

public:
	HairGeometryIndices* geometryIndices;
	HairCVPositions*	cvPositions;
	HairGeometry*		geometry;
	HairClumpPositions*	clumpPositions;
	HairRootPositions*	rootPositions;
	HairCV*				cv;
	HairSystem*			hairSystem;

protected:
	Math::ParamTex2d<unsigned char, Math::EJT_MUL> length;
public:
	Math::ParamTex2d<unsigned char, Math::EJT_MIDDLE> inclination, polar;
	Math::ParamTex2d<unsigned char, Math::EJT_MIDDLE> baseCurl, tipCurl;
	Math::ParamTex2d<unsigned char> baseWidth, tipWidth;
	Math::ParamTex2d<unsigned char> scragle;
	Math::ParamTex2d<unsigned char> scragleFreq;
	Math::ParamTex2d<unsigned char> baseClump, tipClump;
	Math::ParamTex2d<unsigned char, Math::EJT_MIDDLE> twist;
	Math::ParamTex2d<unsigned char, Math::EJT_MUL> displace;
	Math::ParamTex2d<unsigned char> twistFromPolar;
	Math::ParamTex2d<unsigned char, Math::EJT_MUL> blendCVfactor;
	Math::ParamTex2d<unsigned char, Math::EJT_MUL> blendDeformfactor;

	// ��� OCS ONLY
	int seedbias;

	// �������������� ���������� ���������
	//...

	// ������������
	std::vector<HairMeshModifier> modifiers;

	// ���������
	/*/
	struct DeformerDecl
	{
		Util::DllProcedureTmpl<getIHairDeformer> deformerproc;
		Util::MemStream deformerstream;
	};
	/*/
	std::vector<HairDeformerDecl> deformers;

public:
	Math::Box3f getBox();

	void init(
		HairGeometryIndices* geometryIndices, 
		HairGeometry* geometry, 
		HairClumpPositions*	clumpPositions,
		HairRootPositions* rootPositions,
		HairCVPositions*	cvPositions,
		HairCV*				cv,
		HairSystem*			hairSystem
		);
	// ���������������� ������ ���������� ������� � ����������
	void initParamsUVsetIndices();
	// ������������
	void serialize(Util::Stream& stream);
	void serializeOcs(const char* _name, cls::IRender* render, bool bSave);
};

inline void HairMesh::getUVSetAtHair(
	int index, bool bClump, int uvsetindex, 
	Math::Vec2f& uv)
{
	if( uvsetindex<0)
	{
		this->getUVAtHair(index, bClump, uv.x, uv.y);
		return;
	}

	int i = index;
	if( !bClump)
		uv = rootPositions->getUVset(i, uvsetindex, geometryIndices, geometry);
	else
		uv = clumpPositions->getUVset(i, uvsetindex, geometryIndices, geometry);
}

inline int HairMesh::getSeed(int index, bool bClump)
{
	if( bClump) 
		return clumpPositions->getSeed(index)+seedbias;
	return rootPositions->getSeed(index)+seedbias;
}
//! get hair or clump polygon index
inline int HairMesh::getPolygonIndex(int index, bool bClump)
{
	if( bClump) 
		return clumpPositions->getPolygonIndex(index, *geometryIndices);
	return rootPositions->getPolygonIndex(index, *geometryIndices);
}

inline float HairMesh::getLengthJitter()
{
	return length.getValueJitter();
}

inline float HairMesh::getInclination(float u, float v, bool bNoJitter)
{
	if( bNoJitter)
		return inclination.getValueSource(u, v);
	return inclination.getValue( u, v);
}

inline float HairMesh::getPolar(float u, float v, bool bNoJitter)
{
	return polar.getValueAngle(u, v, bNoJitter);
}
inline float HairMesh::getPolarDeriv(float u, float v, float p)
{
	Math::ParamTex2d<unsigned char, Math::EJT_MIDDLE>& param = polar;
	if(param.matrix && !param.matrix->empty())
	{
		float nu = (float)(-cos(2*M_PI*p)/param.matrix->Xsize);
		float nv = (float)(-sin(2*M_PI*p)/param.matrix->Ysize);
		nu += u;
		nv += v;
		float c = (float)(param.matrix->angle_cubic_interpolation(nu, nv, false, false));
		float d = (c/255.f)*(param.max-param.min)+param.min;
		float z = (d - p);
		return z*param.matrix->Xsize;
	}
	return 0;
}
inline float HairMesh::getCurl(float u, float v, float param, bool bNoJitter)
{
	if( bNoJitter)
		return baseCurl.getValueSource(u, v)*(1.f-param) + tipCurl.getValueSource( u, v)*param;
	return baseCurl.getValue(u, v)*(1.f-param) + tipCurl.getValue( u, v)*param;
}
inline float HairMesh::getWidth(float u, float v, float param)
{
	return baseWidth.getValue(u, v)*(1.f-param) + tipWidth.getValue(u, v)*param;
}

inline float HairMesh::getScraggle(float u, float v)
{
	return scragle.getValue(u, v);
}
inline float HairMesh::getScraggleFreq(float u, float v)
{
	return scragleFreq.getValue(u, v);
}

inline float HairMesh::getClump(float u, float v, float param)
{
	return baseClump.getValue(u, v)*(1.f-param) + tipClump.getValue(u, v)*param;
}
inline float HairMesh::getTwist(float u, float v, bool bNoJitter)
{
	if( bNoJitter)
		return twist.getValueSource(u, v);
	return twist.getValue(u, v);
}
inline float HairMesh::getDisplace(float u, float v, bool bNoJitter)
{
	if( bNoJitter)
		return displace.getValueSource(u, v);
	return displace.getValue(u, v);
}
