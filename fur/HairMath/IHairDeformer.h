#pragma once

#include "HAIRPARAMS.h"
#include "HAIRVERTS.h"
#include "supernova/iartistshape.h"

// ����������:
// serialise ��� fromUI
// init
// ... ParseHair
class IHairDeformer
{
public:
	enum enType
	{
		INCREASELENGTH =	0x80,		// ���������� IncreaseHairLength() � IncreaseMaxLength()
		PREPARSEHAIR =		0x10,		// ���������� PreParseHair()
		PARSEHAIR =			0x1,		// ���������� ParseHair()
		BUILDHAIR =			0x2,		// ���������� BuildHair() - ��� ���� ���� ��� ������ true ������� ��������� �� ����������
		DEFORMHAIRVERTEX =	0x20,		// ���������� deformHairVertex() - ������ �������������� �� curl � ������ ��������
		POSTBUILDHAIR =		0x8,		// ���������� PostBuildHair() - ��� ���� ���������� ������� ������� ���������, � ����� ����� ��������� ����� ��������������� ������
		MERGEWITHCLUMP =	0x4,		// ���������� MergeWithClump()
		RENDERATTRIBUTES =	0x40,		// ����������� renderGroupStart() � renderHair()
	};
	// ��������� ����������, ������ ����� ������ �� enType
	virtual int getType(
		)=0;
	virtual void release()=0;
public:
	// ���� ���� ��������� ����� ������ 
	// ��������� � ������� �����!!! ����� �������� �����!!!
	virtual void IncreaseHairLength( 
		float u, float v, float& length
		){};
	virtual void IncreaseMaxLength(
		float& length
		){};

	// ��������� ��������� ������������� ������ � ���������� HAIRPARAMS.
	// ���������� ������ � HairProcessor.HairProcessor()
	virtual void requiredMemoryForHair(
		int SegmCount,					// ����� ���������
		int& sizein_HAIRPARAMS, 		// (in/out) ������ ������� ����������, ������ ����� ����� float � HAIRPARAMS
		int& sizein_HAIRVERTS			// (in/out) ������ ������� ����������, ������ ����� ����� float � HAIRVERTS
		){};
	// ���������� �� ������ ���� ���������� ��������, ����� ������������� m
	virtual void deformHairVertex(
		float affect, 
		const HAIRPARAMS& c_hairs, 			// ��������� ������
		int vertIndex,						// ������ ��������
		int segCount,						// ����� ���������
		Math::Matrix4f& m,					// ������� �������� ��������
		Math::Vec3f pt						// ������� � TS
		){};
	// �������� ��������� ������, ���������� �� ��������� ���������� � 2d �������
	virtual void PreParseHair(
		float affect, 
		HAIRPARAMS& c_hairs,
		bool bClump
		){};
	// �������� ��������� ������
	virtual void ParseHair(
		float affect, 
		HAIRPARAMS& c_hairs,
		bool bClump
		){};
	// ���������� ������ �� ����������, ������ true ���� ������� � ������� ����������� �� �����
	// ��� ���� ���� ��� ������ true ������� ��������� �� ����������
	virtual bool BuildHair(
		float affect, 
		const HAIRPARAMS& c_hairs, 			// ��������� ������
		int SegmCount,					// ����� ���������
		HAIRVERTS& hair					// ���������
		){return false;};
	// ��� ���� ���������� ������� ������� ���������, � ����� ����� ��������� ����� ��������������� ������
	virtual void PostBuildHair(
		float affect, 
		const HAIRPARAMS& c_hairs, 			// ��������� ������
		int SegmCount,					// ����� ���������
		HAIRVERTS& hair					// ���������
		){};
	// ������������� ������, ������ true ���� ������� � ������� ����������� �� �����
	virtual bool MergeWithClump(
		float affect, 
		const HAIRPARAMS& c_hairs, 			// ��������� ������
		const HAIRVERTS& orighair,		// �������� �����
		const HAIRPARAMS& c_clumphair, 	// ��������� ������ - ������
		const HAIRVERTS& clumphair,		// ����� � ������
		HAIRVERTS& hair					// ���������
		){return false;};

	// �������� �� ������, ���� ����� ���� ��������� ��������
	// ���� ����� RENDERATTRIBUTES
	virtual bool renderGroupStart(
		int hairCount,					// ���������
		int SegmCount,					// ...
		bool bCubic,					// ...
		cls::IRender* render			// ���� ������ ������
		){return false;};
	virtual bool renderHair(
		int index, 
		const HAIRPARAMS& c_hairs, 		// ��������� ������
		const HAIRVERTS& hair,			// �������� ������
		bool bCubic						// ���� ������������ ����������
		){return false;};


public:
	// �������������
	virtual void init(
		)=0;
	// ������ �� UI
	virtual void fromUI(
		artist::IShape* shape
		)=0;
	// ������������
	virtual void serialise(
		Util::Stream& out
		)=0;
public:
	IHairDeformer()
	{
		affect.defValue = 1;
	}
	// affect
	Math::ParamTex2d<unsigned char, Math::EJT_MUL> affect;

};

typedef IHairDeformer* (*getIHairDeformer)();