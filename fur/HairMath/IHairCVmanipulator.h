#pragma once

#include "HairCV.h"

namespace cls
{
	struct IRender;
};
// ��������� ��� ���������� ����������� �� ����������� ������
// 
struct IHairCVmanipulator
{
	// ������ �����������
	virtual bool Start(
		cls::IRender* attributes
		)=0;

	// ��� ����������� (���������� "cursorPos")
	virtual bool Step(
		cls::IRender* attributes
		)=0;

	// ������ ����������� �� ��������� �����
	virtual void ComputeSingleHair(
		HairCV::CV& hair,				// �����
		std::vector<float> affects,		// ������� ����������� �� ������ ������� ������
		Math::Matrix4f& basis,			// ������� ��� ��������� �� TS -> OS
		Math::Matrix4f& basis_inv		// ������� ��� ��������� �� OS -> TS
		)=0;

	// ��������� �����������
	virtual void Finish(
		)=0;
};

typedef IHairCVmanipulator* (*getIHairCVmanipulator_t)();
