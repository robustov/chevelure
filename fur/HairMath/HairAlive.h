#pragma once

#include "HairMath.h"
#include "Math/Math.h"
#include <vector>
#include <Util/Stream.h>
#include <Util\STLStream.h>

#pragma warning( disable : 4251)

struct HAIRMATH_API HairAlive
{
	int size;
	std::vector<unsigned char> bitMask;
	
public:
	void resize(int count);
	bool operator[] (int ind) const;
	void set(int ind, bool value);
	int findTrue(int ind) const;
public:
	HairAlive();
	HairAlive(const HairAlive& arg);
	HairAlive& operator =(const HairAlive& arg);
	void serialize(Util::Stream& stream);
};

inline bool HairAlive::operator[](int ind) const
{
	if( ind>=size) return false;
	int s = ind/8; 
	int b = ind&0x7;
	char v = bitMask[s];
	unsigned bit = (0x1)<<b;
	return (v&bit) != 0;
}
inline void HairAlive::set(int ind, bool value)
{
	if( ind>=size) return;
	int s = ind/8; 
	int b = ind&0x7;
	unsigned char& v = bitMask[s];
	unsigned bit = (0x1)<<b;
	v &= ~bit;
	if( value)
		v |= bit;
}

inline int HairAlive::findTrue(int ind) const
{
	int s0 = ind/8; 
	int b0 = ind&0x7;
	bool overStart = false;
	for( int s = s0, b = b0; ; s++)
	{
		if( overStart && s>=s0+1)
			return -1;
		if( s>=(int)bitMask.size())
		{
			s = -1;
			b = 0;
			overStart = true;
			continue;
		}

		unsigned char v = bitMask[s];
		if( v==0) 
		{
			b = 0;
			continue;
		}

		for(;b<8; b++)
		{
			if( b+(s<<3) >= size) break;

			unsigned bit = (0x1)<<b;
			if( (v&bit) != 0)
				return b+(s<<3);
		}
		b = 0;
	}
}
