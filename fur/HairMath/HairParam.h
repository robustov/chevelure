#pragma once

#include <vector>
#include <string>
#include <Math/Vector.h>
#include <Math/Matrix.h>
#include "Math/matrixMxN.h"

#pragma warning( disable : 4251)

enum enJittingType
{
	EJT_NONE	= 0, 
	EJT_MIDDLE	= 1, 
	EJT_MAX		= 2,
	EJT_MUL		= 3,

	EJT_EGZ			= 0x1000,
	EJT_MIDDLE_EGZ	= 0x1001,
	EJT_MAX_EGZ		= 0x1002,
	EJT_MULEGZ		= 0x1003,
};

//! �������� �� ����� (per hair) 2d �.�. ����������� ����� {u, v}
template <class T, enJittingType JT = EJT_MIDDLE_EGZ>
struct HairParam2d
{
	float defValue;
	// �������
	float min, max;
	Math::matrixMxN<T>* matrix;
	// ����
	std::string filename;
	float factor, offset;
	// noise
	float jitter;				// 0 - ��������� 1 - ???
	// ��������� ������
	bool bNeedDelmatrix;

	HairParam2d(){bNeedDelmatrix=false; matrix = NULL;reset();}
	~HairParam2d(){reset();}

	float getMax();
	float getValue(float u, float v);
	// ��� jittera
	float getValueSource(float u, float v);
	void reset()
	{
		if( bNeedDelmatrix) delete matrix; 
		matrix=0;
		min=max=defValue=factor=offset=0; 
		filename="";
		jitter=0;
	}
	HairParam2d<T, JT>& operator=(const HairParam2d<T, JT>& arg)
	{
		this->defValue			= arg.defValue;
		this->min				= arg.min;
		this->max				= arg.max;
		this->matrix			= NULL;
		if( arg.matrix)
			this->matrix = new Math::matrixMxN<T>( *arg.matrix);
		this->filename			= arg.filename;
		this->factor			= arg.factor;
		this->offset			= arg.offset;
		this->bNeedDelmatrix	= true;
		this->jitter			= arg.jitter;
		return *this;
	}
};

template <class T, enJittingType JT = EJT_MIDDLE_EGZ>
struct HairParam : public HairParam2d<T, JT>
{
	std::string name;

	template <class T1, enJittingType JT1>
	HairParam<T, JT>& operator=(const HairParam2d<T1, JT1>& arg)
	{
		this->HairParam2d<T, JT>::operator=(arg);
//		this->name = "";
		return *this;
	}
	HairParam<T, JT>& operator=(const HairParam<T, JT>& arg)
	{
		this->HairParam2d<T, JT>::operator=(arg);
		this->name = arg.name;
		return *this;
	}
};

template <class T, enJittingType JT>
struct HairParam3d
{
	std::string name;

	struct key
	{
		float param;
		int interpolation;
		HairParam2d<T, JT> value;
	};
	std::vector<key> ramp;

	HairParam3d(){};
	~HairParam3d(){};

	float getValue(float u, float v, float p);
};





template <class T, enJittingType JT>
float HairParam2d<T, JT>::getMax()
{
	if(matrix && !matrix->empty())
		return max;
	return defValue;
}

// ��� jittera
template <class T, enJittingType JT>
float HairParam2d<T, JT>::getValueSource(float u, float v)
{
	float d;
	if(this->matrix && !this->matrix->empty())
	{
		unsigned char c = this->matrix->interpolation((float)u, (float)v, false, false);
		d = (c/255.f)*(this->max-this->min)+this->min;
	}
	else
		d = this->defValue;
	return d;
}

template <class T, enJittingType JT>
float HairParam2d<T, JT>::getValue(float u, float v)
{
	enJittingType type = (enJittingType)( JT&(~EJT_EGZ));
	/*/
	if( !this->filename.empty() && !this->matrix->empty())
	{
		unsigned char c = this->matrix->interpolation((float)u, (float)v, false, false);
		float d = (c/255.f)*(this->factor)+this->offset;
		return d;
	}
	/*/

	float d = getValueSource(u, v);

	if(this->jitter!=0 && type!=EJT_NONE)
	{
		// j = [0,1]
		float j = rand()/(float)RAND_MAX;
		if( type==EJT_MIDDLE)
			// j = [-1, 1]
			j = 2*(j - 0.5f);
		else if( type==EJT_MAX)
			// j = [-1, 0]
			j = -j;

		if( type==EJT_MUL)
		{
			j = 1-this->jitter*j;
			d = d*j;
		}
		else
		{
			j = this->jitter*j;
			d = d+j;
		}
		if( (JT&EJT_EGZ) != 0)
			if( d<0) d = 0.00001f;
	}
	return d;
}

template <class T, enJittingType JT>
float HairParam3d<T, JT>::getValue(float u, float v, float p)
{
	unsigned vc = ramp.size();
	for(unsigned g=1; g<vc; g++)
	{
		if( (ramp[g-1].param <= f || g==1)&&
			(ramp[g].param >= f   || g==vc-1)
			)
		{
			float factor = (f-ramp[g-1].param)/(ramp[g].param-ramp[g-1].param);
			if( factor<0) factor=0;
			if( factor>1) factor=1;
			float res = (1-factor)*ramp[g-1].value.getValue(u, v) + factor*ramp[g].value.getValue(u, v);
			return pt;
		}
	}
}

#include "mathNmaya/IFFMatrix.h"

template <class STREAM, class T, enJittingType JT>
STREAM& operator>>(STREAM& serializer, HairParam2d<T, JT> &v)
{
	int version = 1;
	serializer >> version;
	if(version==1)
	{
		serializer >> v.defValue;
		serializer >> v.min >> v.max;
		serializer >> v.filename;
		serializer >> v.factor >> v.offset;
		serializer >> v.jitter;

		if(serializer.isLoading())
		{
			// load texture
			bool bMatrix;
			serializer >> bMatrix;
			if(bMatrix)
			{
				v.matrix = new Math::matrixMxN<T>;
				serializer >> *v.matrix;
				v.bNeedDelmatrix = true;
			}
			else if(!v.filename.empty())
			{
				// �� �����
				v.matrix = new Math::matrixMxN<T>;
				LoadMatrixFromIFF(v.filename.c_str(), *v.matrix);
				v.min = v.offset;
				v.max = v.offset + v.factor;
				v.bNeedDelmatrix = true;
			}
		}
		else
		{
			// save texture
			bool bMatrix = v.matrix && !v.matrix->empty() && v.filename.empty();
			serializer >> bMatrix;
			if(bMatrix)
				serializer >> *v.matrix;
		}
	}
	return serializer;
}

template <class STREAM, class T, enJittingType JT>
STREAM& operator>>(STREAM& serializer, HairParam<T, JT> &v)
{
	serializer >> v.name;
	serializer >> *(HairParam2d<T, JT>*)&v;
	return serializer;
}

template <class STREAM, class T, enJittingType JT>
STREAM& operator>>(STREAM& serializer, HairParam3d<T, JT> &v)
{
	serializer >> v.name;
	serializer >> ramp;
	return serializer;
}
