#pragma once

//#include "Complex.h"
#include <math.h>
//#include <D3DX8.h>
//#include <windows.h>
#include "fftWaves.h"
//#include "../Baikov/Geo/Vector.h"
//using namespace Geo;

/*/
struct D3DXVECTOR3
{
	float x;
	float y;
	float z;
	D3DXVECTOR3()
	{
		x = y= z = 0;
	}
	D3DXVECTOR3(float x, float y, float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}
};
/*/

struct OceanEmitter2 : public WavesEmitter
{
	int num_waves;
	float physSize;

//	D3DXVECTOR3 wind; // wind m/s
	float amp; // Phillips spectrum A-coeff (~0.00005f, depends on cellSize)
	float damp; // wave dampening coeff (10-1000)

	float windVel;
	float windDirX, windDirZ;
	float L;

public:
	OceanEmitter2(int num_waves, float physSize, float WaveDumper, float WindAngle, float WindSpeed);

	virtual bool getSpectrum(
		FftWaves* waves, 
		int x, int y, 
		int srandValue, 
		fftw_complex& h_nul, fftw_complex& conj_h_nul);

	float Phillips(float kx, float ky);

	float getPhillips(int x, int y);
};