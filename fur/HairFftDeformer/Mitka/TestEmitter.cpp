#include "TestEmitter.h"
#include <stdio.h>

const float Pi = 3.14159265f;

TestEmitter::TestEmitter(int size)
{
	this->size = size;

	Math::matrixMxN<fftw_complex> yvalue;
	yvalue.init(size, size);
	nul.init(size, size);
	int x, y;
	for(y=0; y<size; y++)
	{
		for(x=0; x<size; x++)
		{
			float X = ((float)x)/(size-1) - 0.5f;
			float Y = ((float)y)/(size-1) - 0.5f;
			X *= Pi*8;
			Y *= Pi*8;

			float R = (float)sqrt(X*X+Y*Y);
			float h = (float)(cos(R)+1)*0.1f;
			if(R>Pi)
				h = 0;

			yvalue[y][x].re = h;
			yvalue[y][x].im = 0;
		}
	}

	fftwnd_plan plan_for;
	plan_for = fftw2d_create_plan(
		size, size, FFTW_FORWARD, FFTW_ESTIMATE);

	fftwnd_one(plan_for, yvalue.data(), nul.data());

	fftwnd_destroy_plan(plan_for);
}

bool TestEmitter::getSpectrum(
	FftWaves* waves, 
	int x, int y, 
	int srandValue, 
	fftw_complex& h_nul, fftw_complex& conj_h_nul)
{
	conj_h_nul.re = h_nul.re = nul[y][x].re/(size*size*2);
	conj_h_nul.im = h_nul.im = nul[y][x].im/(size*size*2);
	return true;
}
