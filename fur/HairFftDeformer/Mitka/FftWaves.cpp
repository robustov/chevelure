#include "FftWaves.h"

//#include "./SeaWater.h"
#include "Util/Stream.h"
#include "Util/FileStream.h"
#include "Util/STLStream.h"

#include "fftw.h"
//#include "rfftw.h"
#include <math.h>
#include "OceanEmitter.h"
#include "random.h"

#define Pi 3.14159265f
#define g 9.81f


FftWaves::FftWaves()
{
	plan_for = 0;
	plan_back = 0;
	size = 0;
	phisicalSize = 0;
}

FftWaves::~FftWaves()
{
	if( plan_for)
		fftwnd_destroy_plan(plan_for);
	if( plan_back)
		fftwnd_destroy_plan(plan_back);
}

bool FftWaves::init(int size, float phisicalSize, int filtersize, WavesEmitter* emitter)
{
	this->size = size;
	this->phisicalSize = phisicalSize;

	if( plan_for)
		fftwnd_destroy_plan(plan_for);
	if( plan_back)
		fftwnd_destroy_plan(plan_back);

	plan_for = fftw2d_create_plan(
		size, size, FFTW_FORWARD, FFTW_ESTIMATE);
	plan_back = fftw2d_create_plan(
		size, size, FFTW_BACKWARD, FFTW_ESTIMATE);

	bool bDeleteEmitter = false;
	if( !emitter)
	{
		emitter = new OceanEmitter(size, phisicalSize, 1, 0, 5);
		bDeleteEmitter = true;
	}
	filtersize = filtersize/2;

	nul.init(size, size);
	nul_conj.init(size, size);
	w_i.init(size, size);
	k_i.init(size, size);
	temp.init(size, size);
	temp2.init(size, size);
	fft.init(size, size);

	yvalue.init(size, size);

	chopX.init(size, size);
	chopZ.init(size, size);

	normalX.init(size, size);
	normalZ.init(size, size);

	int x, y;

	// ������������� � ������� ������������������
	randoms.init(size, size);
	srand(0);
	for(int ss=2; ss<=size; ss+=2)
	{
		for(y=0; y<ss; y++)
		{
			for(x=0; x<ss; x++)
			{
				if(x!=0 && y!=0 && x!=ss-1 && y!=ss-1 )
					continue;
				int X = ((x-ss/2)+size)%size;
				int Y = ((y-ss/2)+size)%size;
				randoms[Y][X] = rand();
			}
		}
	}

	for(y=0; y<size; y++)
	{
		for(x=0; x<size; x++)
		{
//			srand( y*size + x);
			int srandValue = randoms[y][x];
			emitter->getSpectrum(this, x, y, srandValue, nul[y][x], nul_conj[y][x]);

			// ����������
			if( ((x + filtersize)%size)>=filtersize*2 || 
				((y + filtersize)%size)>=filtersize*2)
			{
				nul[y][x].re = nul_conj[y][x].re = 0;
				nul[y][x].im = nul_conj[y][x].im = 0;
			}
		}
	}

	for(y=0; y<size; y++)
	{
		for(x=0; x<size; x++)
		{
			float pKx=2.0f*Pi*(-(float)size*0.5f+(float)x);
			float pKz=2.0f*Pi*(-(float)size*0.5f+(float)y);
			//storing XZ vector magnitude in Y
			float pKy = (float)sqrt(pKx*pKx+pKz*pKz)/phisicalSize;
			if (pKy==0.0f)
				pKy = (float)sqrt(2.0f);

			int X = (x+size/2)%size;
			int Y = (y+size/2)%size;

			w_i[Y][X] = sqrt( g*pKy);
			k_i[Y][X] = vector2(pKx, pKz);
		}
	}

	if( bDeleteEmitter)
		delete emitter;
	return true;
}

bool FftWaves::update(float time)
{
	time += 100.f;
	int i;
	for(i=0; i<nul.fullsize(); i++)
	{
		fftw_real sin_wt = (fftw_real)sin(w_i(i)*time);
		fftw_real cos_wt = (fftw_real)cos(w_i(i)*time);
		fftw_complex plus, minus;
		plus.re  = nul(i).re * cos_wt - nul(i).im * sin_wt;
		plus.im  = nul(i).re * sin_wt + nul(i).im * cos_wt;
		minus.re = nul_conj(i).re * cos_wt - nul_conj(i).im * sin_wt;
		minus.im = -(nul_conj(i).re * sin_wt + nul_conj(i).im * cos_wt);

		temp(i).re = plus.re + minus.re;
		temp(i).im = plus.im + minus.im;
	}

	// Y
	fftwnd_one(plan_back, temp.data(), fft.data());

	for(i=0; i<nul.fullsize(); i++)
		yvalue(i) = fft(i).re;

	// chopX
	{
		for(i=0; i<nul.fullsize(); i++)
		{
			fftw_complex v = temp(i);

			float l = sqrt( k_i(i).x * k_i(i).x + k_i(i).y * k_i(i).y);
			temp2(i).re = -v.im * k_i(i).x/l;
			temp2(i).im = v.re * k_i(i).x/l;
		}
		temp2[0][0].re = 0;
		temp2[0][0].im = 0;
		fftwnd_one(plan_back, temp2.data(), fft.data());
		for(i=0; i<nul.fullsize(); i++)
			chopX(i) = fft(i).re;
	}

	// chopZ
	{
		for(i=0; i<nul.fullsize(); i++)
		{
			fftw_complex v = temp(i);
			float l = sqrt( k_i(i).x * k_i(i).x + k_i(i).y * k_i(i).y);
			temp2(i).re = -v.im * k_i(i).y/l;
			temp2(i).im = v.re * k_i(i).y/l;
		}
		temp2[0][0].re = 0;
		temp2[0][0].im = 0;
		fftwnd_one(plan_back, temp2.data(), fft.data());
		for(i=0; i<nul.fullsize(); i++)
			chopZ(i) = fft(i).re;
	}

	// NormalX
	{
		for(i=0; i<nul.fullsize(); i++)
		{
			fftw_complex v = temp(i);
			temp2(i).re = -v.im * k_i(i).x;
			temp2(i).im = v.re * k_i(i).x;
		}
		fftwnd_one(plan_back, temp2.data(), fft.data());
		for(i=0; i<nul.fullsize(); i++)
			normalX(i) = fft(i).re;
	}
	// NormalZ
	{
		for(i=0; i<nul.fullsize(); i++)
		{
			fftw_complex v = temp(i);
			temp2(i).re = -v.im * k_i(i).y;
			temp2(i).im = v.re * k_i(i).y;
		}
		fftwnd_one(plan_back, temp2.data(), fft.data());
		for(i=0; i<nul.fullsize(); i++)
			normalZ(i) = fft(i).re;
	}

	return true;
}

float FftWaves::getSpectrum(int x, int y)
{
	if(size==0) return 0;
	x = (x+size/2)%size;
	y = (y+size/2)%size;
	fftw_complex v = nul[y][x];
	return (float)sqrt( v.re*v.re + v.im*v.im)*size*2.0f;
}

float FftWaves::getYvalue(int x, int y)
{
	if(size==0) return 0;
	float v = yvalue[y][x];
	return v;
}

float FftWaves::getChopX(int x, int y)
{
	if(size==0) return 0;
	float v = chopX[y][x];
	return v;
}
float FftWaves::getChopZ(int x, int y)
{
	if(size==0) return 0;
	float v = chopZ[y][x];
	return v;
}
float FftWaves::getNormalX(int x, int y)
{
	if(size==0) return 0;
	float v = -normalX[y][x];
	return v;
}
float FftWaves::getNormalZ(int x, int y)
{
	if(size==0) return 0;
	float v = -normalZ[y][x];
	return v;
}


float FftWaves::getWvalue(int x, int y)
{
	if(size==0) return 0;
	x = (x+size/2)%size;
	y = (y+size/2)%size;
	return w_i[y][x];
}

int FftWaves::getRandom(int x, int y)
{
	if(size==0) return 0;
	x = (x+size/2)%size;
	y = (y+size/2)%size;
	return randoms[y][x];
}

int FftWaves::currentVersion = 0;

float FftWaves::getYvalue(float x, float y)
{
	if(size==0) return 0;
	float v = yvalue.cubic_interpolation(y, x, true, true);
	return v;
}
float FftWaves::getChopX(float x, float y)
{
	if(size==0) return 0;
	float v = chopX.cubic_interpolation(y, x, true, true);
	return v;
}
float FftWaves::getChopZ(float x, float y)
{
	if(size==0) return 0;
	float v = chopZ.cubic_interpolation(y, x, true, true);
	return v;
}
float FftWaves::getNormalX(float x, float y)
{
	if(size==0) return 0;
	float v = -normalX.cubic_interpolation(y, x, true, true);
	return v;
}
float FftWaves::getNormalZ(float x, float y)
{
	if(size==0) return 0;
	float v = -normalZ.cubic_interpolation(y, x, true, true);
	return v;
}

bool FftWaves::Save(const char* filename)
{
	Util::FileStream stream;
	if( !stream.open(filename, true, false))
		return false;
	stream >> currentVersion;
	return Serialize(stream);
}
bool FftWaves::Load(const char* filename)
{
	Util::FileStream stream;
	if( !stream.open(filename, false, true))
		return false;

	int ver;
	stream >> ver;
	if( currentVersion != ver)
		return false;

	Serialize(stream);
	return true;
}
bool FftWaves::Serialize(Util::Stream& stream)
{
	stream >> size;
	stream >> phisicalSize;
//	stream >> nul;
//	stream >> nul_conj;
//	stream >> w_i;
//	stream >> k_i;
//	stream >> temp >> temp2 >> fft;
	stream >> yvalue;
	stream >> chopX >> chopZ;
	stream >> normalX >> normalZ;
//	stream >> randoms;
//	stream >> plan_for;
//	stream >> plan_back;
	return true;
}
