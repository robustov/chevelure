#include "random.h"
#include <stdlib.h>
#include <math.h>

float random(int& srandValue)
{
	srand(srandValue);
	srandValue = rand();
	return 1 - 2*(srandValue/float(RAND_MAX));
}
// utility RNG functions
float randUniform(int& srandValue)
{
	srand(srandValue);
	srandValue = rand();
	return srandValue++ / float(RAND_MAX);
}
float randNormal(int& srandValue)
{
    typedef double T;
    const T s = 0.449871, t = -0.386595, a = 0.19600, b = 0.25472;
    const T r1 = 0.27597, r2 = 0.27846;

    T u, v;

    for (;;) 
	{
      // Generate P = (u,v) uniform in rectangle enclosing
      // acceptance region:
      //   0 < u < 1
      // - sqrt(2/e) < v < sqrt(2/e)
      // The constant below is 2*sqrt(2/e).

      u = randUniform(srandValue);
      v = 1.715527769921413592960379282557544956242L 
          * (randUniform(srandValue) - 0.5);

      // Evaluate the quadratic form
      T x = u - s;
      T y = fabs(v) - t;
      T q = x*x + y*(a*y - b*x);
   
      // Accept P if inside inner ellipse
      if (q < r1)
        break;

      // Reject P if outside outer ellipse
      if (q > r2)
        continue;

      // Between ellipses: perform exact test
      if (v*v <= -4.0 * log(u)*u*u)
        break;
    }

    return float(v/u);
}
