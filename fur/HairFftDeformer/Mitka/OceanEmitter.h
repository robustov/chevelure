#pragma once

//#include "Complex.h"
#include <math.h>
//#include <D3DX8.h>
//#include <windows.h>
#include "fftWaves.h"
//#include "../Baikov/Geo/Vector.h"
//using namespace Geo;

typedef vector3 D3DXVECTOR3;

struct OceanEmitter : public WavesEmitter
{
	float WaveDumper;
	D3DXVECTOR3 WindDirection;
	float WindSpeed;
	int num_waves;
	float physSize;


public:
	OceanEmitter(
		int num_waves, float physSize, 
		float WaveDumper, float WindAngle, float WindSpeed);

	virtual bool getSpectrum(
		FftWaves* waves, 
		int x, int y, 
		int srandValue, 
		fftw_complex& h_nul, fftw_complex& conj_h_nul);

	float Philips(D3DXVECTOR3* pK);

	float getPhillips(int x, int y);
};