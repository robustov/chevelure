#pragma once

#include <math.h>
#include "fftWaves.h"

struct TestEmitter : public WavesEmitter
{
	int size;
	Math::matrixMxN<fftw_complex> nul;
public:
	TestEmitter(int size);

	virtual bool getSpectrum(
		FftWaves* waves, 
		int x, int y, 
		int srandValue, 
		fftw_complex& h_nul, fftw_complex& conj_h_nul);
};