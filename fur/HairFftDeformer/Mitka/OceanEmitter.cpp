#include "OceanEmitter.h"
#include "random.h"

const float g   =  9.81f;
const float Pi = 3.14159265f;

float OceanEmitter::Philips(D3DXVECTOR3* pK)
{
	float k = pK->y;
	float KW2 = pK->x*WindDirection.x+pK->z*WindDirection.z;
	if( KW2<0) KW2=0;
	KW2 = KW2*KW2;

	float k4 = k*k*k*k;
    if (k4==0) return 0;
	float gg=WindSpeed*WindSpeed/g;
	float ee=(float)exp(-1.0f/(k*k*gg*gg));
	return WaveDumper*ee*KW2/k4;
}

OceanEmitter::OceanEmitter(int num_waves, float physSize, float WaveDumper, float WindAngle, float WindSpeed)
{
	this->num_waves = num_waves;
	this->physSize  = physSize;

	this->WaveDumper = WaveDumper*0.000020f;// 0.0000100f;
	this->WindDirection=D3DXVECTOR3(sin(WindAngle),0.0,cos(WindAngle));
	this->WindSpeed = WindSpeed;
}


bool OceanEmitter::getSpectrum(
	FftWaves* waves, 
	int x, int y, 
	int srandValue, 
	fftw_complex& h_nul, fftw_complex& conj_h_nul)
{
	x = (x + num_waves/2)%num_waves;
	y = (y + num_waves/2)%num_waves;
	D3DXVECTOR3 kkk;
	D3DXVECTOR3* pK = &kkk;
	pK->x = 2.0f*Pi*(-(float)num_waves*0.5f+(float)x)/physSize;
	pK->z = 2.0f*Pi*(-(float)num_waves*0.5f+(float)y)/physSize;
	//storing XZ vector magnitude in Y
	pK->y=(float)sqrt(pK->x*pK->x+pK->z*pK->z);
	if (pK->y!=0.0f)
	{
		pK->x/=pK->y;
		pK->z/=pK->y;
	}
	else
	{
		pK->x=1.0f;
		pK->y=(float)sqrt(2.0f);
		pK->z=1.0f;
	}
	
	D3DXVECTOR3 pMK=*pK;
	pMK.x*=-1.0f;
	pMK.z*=-1.0f;
	
	float  eta_r=random( srandValue);
	float  eta_i=random( srandValue);
	float  r = random( srandValue)*Pi;
	eta_r=1;
	eta_i=1;
//	r=Pi;
	float rsq2=1.0f/(float)sqrt(2.0f);
	
	float P=(float)sqrt(Philips(pK));
	float mP=(float)sqrt(Philips(&pMK));

	h_nul.re = rsq2*eta_r*P*cos(r);
	h_nul.im = rsq2*eta_i*P*sin(r);
	conj_h_nul.re = rsq2*eta_r*mP*cos(r);
	conj_h_nul.im = -rsq2*eta_i*mP*sin(r);
//	h_nul.re = pK->y; 
//	h_nul.im = 0;
//	conj_h_nul.re = pK->y; 
//	conj_h_nul.im = 0;

	return true;
}


float OceanEmitter::getPhillips(int x, int y)
{
//	x = (x + num_waves/2)%num_waves;
//	y = (y + num_waves/2)%num_waves;
	D3DXVECTOR3 kkk;
	D3DXVECTOR3* pK = &kkk;
	pK->x = 2.0f*Pi*(-(float)num_waves*0.5f+(float)x)/physSize;
	pK->z = 2.0f*Pi*(-(float)num_waves*0.5f+(float)y)/physSize;
	//storing XZ vector magnitude in Y
	pK->y=(float)sqrt(pK->x*pK->x+pK->z*pK->z);
	if (pK->y!=0.0f)
	{
		pK->x/=pK->y;
		pK->z/=pK->y;
	}
	else
	{
		pK->x=1.0f;
		pK->y=(float)sqrt(2.0f);
		pK->z=1.0f;
	}
	
	D3DXVECTOR3 pMK=*pK;
	float P = Philips(pK);
	return sqrt(P);
}