#include "OceanEmitter2.h"
#include "random.h"

const float g   =  9.81f;
const float Pi = 3.14159265f;

float OceanEmitter2::Phillips(float kx, float ky)
{
	const float A = this->amp;//0.00005f;
	float kl2 = sqrt(kx*kx + ky*ky); // |k|^2
	if( kl2==0 ) return 0;
	float l = L*this->damp;//0.01;
	float damp = float(exp(-kl2 * l * l));
	float KdotW = kx * windDirX + ky*windDirZ;
	float phillips = float(A * exp(-1/(kl2*L*L)) / (kl2*kl2*kl2) * KdotW*KdotW);
	return phillips * damp;
}

OceanEmitter2::OceanEmitter2(int num_waves, float physSize, float WaveDumper, float WindAngle, float WindSpeed)
{
	this->num_waves = num_waves;
	this->physSize  = physSize;

	windDirX = sin(WindAngle);
	windDirZ = cos(WindAngle);
	this->amp		= 0.00005f;
	this->damp		= WaveDumper;
	this->windVel	= WindSpeed;
	this->L = windVel * windVel / g;

	srand(0);
	rand();
	rand();
	rand();

}

bool OceanEmitter2::getSpectrum(
	FftWaves* waves, 
	int x, int y, 
	int srandValue, 
	fftw_complex& h_nul, fftw_complex& conj_h_nul)
{
	x = (x + num_waves/2)%num_waves;
	y = (y + num_waves/2)%num_waves;
	float pKx = 2.0f*Pi*(-(float)num_waves*0.5f+(float)x)/physSize;
	float pKz = 2.0f*Pi*(-(float)num_waves*0.5f+(float)y)/physSize;

	float scale = 0.5f * sqrtf( Phillips(pKx, pKz) );
	float amplitude = randNormal( srandValue);
	float theta		= randUniform( srandValue) * 2 * Pi;
	h_nul.re = fftw_real(scale * amplitude * cos(theta));
	h_nul.im = fftw_real(scale * amplitude * sin(theta));

	conj_h_nul.re = h_nul.re;
	conj_h_nul.im = h_nul.im;
//	h_nul[id] = Complex(rsq2*eta_r*P,rsq2*eta_i*P);//
//	conj_h_nul[id] = Complex(rsq2*eta_r*mP,-rsq2*eta_i*mP);


	return true;
}

float OceanEmitter2::getPhillips(int x, int y)
{
	float pKx = 2.0f*Pi*(-(float)num_waves*0.5f+(float)x)/physSize;
	float pKz = 2.0f*Pi*(-(float)num_waves*0.5f+(float)y)/physSize;

	return sqrtf( Phillips(pKx, pKz) );
}