#pragma once

#include "Math/matrixMxN.h"

#include "fftw3.h"
//#include "rfftw.h"
#include <math.h>
#include "Util/Stream.h"

template<class Stream> inline
Stream& operator >> (Stream& out, fftw_complex& v)
{
	out >> v.re >> v.im;
	return out;
}

class FftWaves;
class WavesEmitter
{
public:
	virtual bool getSpectrum(
		FftWaves* waves, 
		int x, int y, 
		int srandValue, 
		fftw_complex& h_nul, fftw_complex& conj_h_nul)=0;

//	virtual void complete();
};

struct vector2
{
	float x, y;
	vector2(){}
	vector2(float x, float y)
	{
		this->x = x;
		this->y = y;
	}
};

struct vector3
{
	float x;
	float y;
	float z;
	vector3()
	{
		x = y= z = 0;
	}
	vector3(float x, float y, float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}
};

template<class Stream> inline
Stream& operator >> (Stream& out, vector2& v)
{
	out >> v.x >> v.y;
	return out;
}
template<class Stream> inline
Stream& operator >> (Stream& out, vector3& v)
{
	out >> v.x >> v.y >> v.z;
	return out;
}

class FftWaves
{
	static int currentVersion;
	int size;
	float phisicalSize;

	Math::matrixMxN<fftw_complex> nul;
	Math::matrixMxN<fftw_complex> nul_conj;
	Math::matrixMxN<float> w_i;
	Math::matrixMxN<vector2> k_i;
	Math::matrixMxN<fftw_complex> temp, temp2, fft;
	Math::matrixMxN<float> yvalue;
	Math::matrixMxN<float> chopX, chopZ;
	Math::matrixMxN<float> normalX, normalZ;

	// ��������
	Math::matrixMxN<int> randoms;

	fftwnd_plan plan_for;
	fftwnd_plan plan_back;

public:
	FftWaves();
	~FftWaves();
	// size - ������ �����
	// phisicalSize - �����. ������ �������
	bool init(int size=64, float phisicalSize=32, int filtersize=32, WavesEmitter* emitter=NULL);
	bool update(float t);

	float getSpectrum(int x, int y);
	float getYvalue(int x, int y);
	float getWvalue(int x, int y);
	float getChopX(int x, int y);
	float getChopZ(int x, int y);
	float getNormalX(int x, int y);
	float getNormalZ(int x, int y);

	int getRandom(int x, int y);

// ����������������� (������� �������� � ��������� [0...1])
public:
	float getYvalue(float x, float y);
	float getChopX(float x, float y);
	float getChopZ(float x, float y);
	float getNormalX(float x, float y);
	float getNormalZ(float x, float y);

public:
	bool Save(const char* filename);
	bool Load(const char* filename);
public:
	bool Serialize(Util::Stream& stream);
};
