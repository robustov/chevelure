// HairFftDeformer.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "HairFFtDeformer.h"
#include "mitka/OceanEmitter.h"

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    return TRUE;
}
//HairFFtDeformer hairFftDeformer;

IHairDeformer* getDeformer()
{
	return new HairFFtDeformer();
}

// ��������� ������
void HairFFtDeformer::ParseHair(
	float affect, 
	HAIRPARAMS& c_hairs,
	bool bClump
	)
{
	float x = waves.getChopX(c_hairs.u, c_hairs.v);
	float y = waves.getChopZ(c_hairs.u, c_hairs.v);

//	float curl = sqrt(x*x+y*y)*10;
//	float tipCurlAmp = fabs(c_hairs.tipCurl-0.5f);
//	float baseCurlAmp = fabs(c_hairs.baseCurl-0.5f);
	c_hairs.tipCurl += Math::Vec2f(x, y)*10*affectTipCurl;
	c_hairs.baseCurl+= Math::Vec2f(x, y)*10*affectBaseCurl;
	c_hairs.inclination += Math::Vec2f(x, y)*10*affectInclination;

}

void HairFFtDeformer::init(		
	)
{
	OceanEmitter oceanemitter(size, pSize, fWaveDumper, (float)(fWindAngle*2*M_PI), fWindSpeed);
	waves.init(size, pSize, filterSize, &oceanemitter);
	waves.update(time);
}

// ������ �� UI
void HairFFtDeformer::fromUI(
	artist::IShape* shape
	)
{
	shape->getValue(time,		"time", sn::UNIFORM_SHAPE, 0, 0, 1000);
	shape->getValue(size,		"fftDimention", sn::UNIFORM_SHAPE, 32, 16, 1024);
	shape->getValue(pSize,		"fftPhisicalSize", sn::UNIFORM_SHAPE, 20.0f, 1, 100);
	shape->getValue(filterSize, "fftFilterSize", sn::UNIFORM_SHAPE, 32, 4, 512);
	shape->getValue(fWaveDumper, "fftWaveDumper", sn::UNIFORM_SHAPE, 1.f, 0, 10);
	shape->getValue(fWindAngle, "fftWindAngle", sn::UNIFORM_SHAPE, 0, -1, 1);
	shape->getValue(fWindSpeed, "fftWindSpeed", sn::UNIFORM_SHAPE, 5.0f, 0.1f, 100.f);

	shape->getValue(affectTipCurl, "affectTipCurl", sn::UNIFORM_SHAPE, 1.f, 0, 10);
	shape->getValue(affectBaseCurl, "affectBaseCurl", sn::UNIFORM_SHAPE, 1.f, 0, 10);
	shape->getValue(affectInclination, "affectInclination", sn::UNIFORM_SHAPE, 0.0f, 0.0f, 10);

}
// ������������
void HairFFtDeformer::serialise(
	Util::Stream& out
	)
{
	int version = 1;
	out >> version;
	if( version>=0)
	{
		out>>time;
		out>>size;
		out>>pSize;
		out>>filterSize;
		out>>fWaveDumper;
		out>>fWindAngle;
		out>>fWindSpeed;
		out>>affectTipCurl;
		out>>affectBaseCurl;
		out>>affectInclination;
	}
}
