#pragma once

#include "IHairDeformer.h"
#include "mitka/FftWaves.h"

class HairFFtDeformer : public IHairDeformer
{
	float time;
	int size;
	float pSize;
	int filterSize;
	float fWaveDumper;
	float fWindAngle;
	float fWindSpeed;

	float affectTipCurl;
	float affectBaseCurl;
	float affectInclination;

	FftWaves waves;

public:
	virtual int getType(
		)
	{
		return PARSEHAIR;
	}
	virtual void release()
	{
		delete this;
	};

public:
	// ��������� ������
	virtual void ParseHair(
		float affect, 
		HAIRPARAMS& c_hairs,
		bool bClump
		);
public:
	// �������������
	virtual void init(
		);
	// ������ �� UI
	virtual void fromUI(
		artist::IShape* shape
		);
	// ������������
	virtual void serialise(
		Util::Stream& out
		);

};