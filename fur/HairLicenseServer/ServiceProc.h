#pragma once

int Install(const char* pPath);
int UnInstall();
int KillService();
int RunService();
bool ExecuteStartService(const char* license);
