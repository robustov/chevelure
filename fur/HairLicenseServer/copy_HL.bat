set BIN_DIR=.\out\chelicense

mkdir %BIN_DIR%

rem OCELLARIS
copy Bin\chevelureLicenseServer.exe	%BIN_DIR%\

copy Bin\installation.txt			%BIN_DIR%\
copy Bin\HL_uninstall_service.bat	%BIN_DIR%\
copy Bin\HL_as_client.bat			%BIN_DIR%\
copy Bin\HL_as_server.bat			%BIN_DIR%\
copy Bin\HL_install_service.bat		%BIN_DIR%\
copy Bin\HL_start_service.bat		%BIN_DIR%\
copy Bin\HL_stop_service.bat		%BIN_DIR%\

pause