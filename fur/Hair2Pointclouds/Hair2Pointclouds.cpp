// pcvc8.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <windows.h>
#include "hairProcessor.h"
#include "hairRender.h"
#include "mathNpixar/IPointCloud.h"
#include "math/math.h"

float rnd()
{
	return rand()/(float(RAND_MAX));
}

struct PointCloudFile : public IPointCloudFile
{
	PointCloudFile()
	{
		bBoxPass = true;
	}
	bool bBoxPass;

	Math::Box3f box;

	int X, Y, Z;
	std::vector<float> map3d;//X*Y*Z;

	inline float* cell(int x, int y, int z)
	{
		if(x<0 && x>=X) return NULL;
		if(y<0 && y>=Y) return NULL;
		if(z<0 && z>=Z) return NULL;
		int index = x + y*X + z*X*Y;
		return &map3d[index];
	}

	void init()
	{
		X = Y = Z = 256;
		map3d.resize(X*Y*Z);
	}
	virtual void setPoint(
		const Math::Vec3f& pt, 
		const Math::Vec3f& n, 
		float radius
		)
	{
		if( bBoxPass)
		{
			box.insert(pt);
		}
		else
		{
			int x = X*(box.max.x-pt.x)/(box.max.x-box.min.x);
			if(x<0) x = 0;
			if(x>=X) x = X-1;
			int y = Y*(box.max.y-pt.y)/(box.max.y-box.min.y);
			if(y<0) y = 0;
			if(y>=Y) y = Y-1;
			int z = Z*(box.max.z-pt.z)/(box.max.z-box.min.z);
			if(z<0) z = 0;
			if(z>=Z) z = Z-1;

			float* c = cell(x, y, z);
			if(c) *c += radius;
		}
		printf(".");
	}
	void ToPointCloudFile(
		IPointCloud* pPointCloud, 
		void* outptc
		)
	{
		float r = (box.max.x-box.min.x)/(float)X;

		for(int z=0; z<Z; z++)
			for(int y=0; y<Y; y++)
				for(int x=0; x<X; x++)
				{
					Math::Vec3f pt;
					pt.x = (x+0.5f)*(box.max.x-box.min.x)/X + box.min.x;
					pt.y = (y+0.5f)*(box.max.y-box.min.y)/Y + box.min.y;
					pt.z = (z+0.5f)*(box.max.z-box.min.z)/Z + box.min.z;

					float* c = cell(x, y, z);
					if(!c) continue;

					Math::Vec3f normal(0, 0, 0);
//					PtcWriteDataPoint(outptc, (float*)pt.data(), (float*)normal.data(), r, c);

//					float data = rnd();
					float data = *c * 1;
					pPointCloud->PtcWriteDataPoint(outptc, (float*)pt.data(), (float*)normal.data(), r, &data);
				}
//		float data = rnd();
//		Math::Vec3f normal(0, 1, 0);
//		PtcWriteDataPoint(outptc, (float*)pt.data(), (float*)normal.data(), 0.01, &data);
//		PtcWriteDataPoint(outptc, (float*)pt.data(), (float*)n.data(), 0.01, &data);
	}
};

int main(int argc, char* argv[])
{
#ifndef _DEBUG
	getIPointCloudDll PointCloudDll("IPrman.dll", "getIPointCloud");
#else
	getIPointCloudDll PointCloudDll("IPrmanD.dll", "getIPointCloud");
#endif
	if( !PointCloudDll.isValid())
		return -1;
	IPointCloud* pPointCloud = (*PointCloudDll)();

	char* furDataFile = argv[1];
	HairRender furData;
	if( !furData.Load(furDataFile))
	{
		fprintf(stderr, "Fur: Loading %s failed\n", furDataFile);
		return NULL;
	}

	Math::Matrix4f w2e = Math::Matrix4f::id;
	w2e.data()[0]=	-0.0034906501;
	w2e.data()[1]=	-0.50510204;
	w2e.data()[2]=	-0.86305207;
	w2e.data()[3]=	3.2715099e-014;
	w2e.data()[4]=	-2.8595199e-011;
	w2e.data()[5]=	0.86305803;
	w2e.data()[6]=	-0.50510603;
	w2e.data()[7]=	1.0747200e-014;
	w2e.data()[8]=	-0.99999398;
	w2e.data()[9]=	0.0017631500;
	w2e.data()[10]=	0.0030126302;
	w2e.data()[11]=	4.1315501e-012;
	w2e.data()[12]=	-0.010147200;
	w2e.data()[13]=	-0.054375097;
	w2e.data()[14]=	1.9743202;
	w2e.data()[15]=	1.0000000;

	Math::Matrix4f w2n = Math::Matrix4f::id;
	w2n.data()[0]=	-0.0067873788;
	w2n.data()[1]=	-0.98214340;
	w2n.data()[2]=	-0.86305219;
	w2n.data()[3]=	-0.86305207;
	w2n.data()[4]=	-5.5601804e-011;
	w2n.data()[5]=	1.6781693;
	w2n.data()[6]=	-0.50510609;
	w2n.data()[7]=	-0.50510603;
	w2n.data()[8]=	-1.9444338;
	w2n.data()[9]=	0.0034283490;
	w2n.data()[10]=	0.0030126306;
	w2n.data()[11]=	0.0030126302;
	w2n.data()[12]=	-0.019730678;
	w2n.data()[13]=	-0.10572942;
	w2n.data()[14]=	1.9733204;
	w2n.data()[15]=	1.9743202;

	float format[3] = {512, 512, 1};

    const char* outname = argv[argc - 1];
	char* vartypes[] = {"float", NULL};
	char* varnames[] = {"dencity", NULL};

	void* outptc;
	outptc = pPointCloud->PtcCreatePointCloudFile(
		(char*)outname, 1, vartypes, varnames,
		w2e.data(), w2n.data(), format);
    if (!outptc) 
	{
		fprintf(stderr, "Unable to open output file %s.\n", outname);
		exit(1);
    }

	PointCloudFile outfile;

	fprintf(stderr, "Read hairs for box\n");
	furData.GenPointClouds(&outfile);
	outfile.init();
	outfile.bBoxPass = false;
	fprintf(stderr, "Generate points\n");
	furData.GenPointClouds(&outfile);

	outfile.ToPointCloudFile(pPointCloud, outptc);

	pPointCloud->PtcFinishPointCloudFile(outptc);
	fprintf(stderr, "Save file %s.\n", outname);

	/*/
	Math::Matrix4f w2e = Math::Matrix4f::id;
	Math::Matrix4f w2n = Math::Matrix4f::id;
	float format[3] = {512, 512, 1};

    const char* outname = argv[argc - 1];
	char* vartypes[] = {"float"};
	char* varnames[] = {"dencity"};

    void* outptc = NULL;
	
    outptc = PtcCreatePointCloudFile(
		(char*)outname, 1, vartypes, varnames,
		w2e.data(), w2n.data(), format);
    if (!outptc) 
	{
		fprintf(stderr, "Unable to open output file %s.\n", outname);
		exit(1);
    }

	float npoints = 10000;
	for (int p = 0; p < npoints; p++) 
	{
		Math::Vec3f pt(rnd(), rnd(), rnd());
		Math::Vec3f normal(rnd(), rnd(), rnd());
		float radius = 0.01f;
		float data = 1;
		PtcWriteDataPoint(outptc, pt.data(), normal.data(), radius, &data);
	}

	PtcFinishPointCloudFile(outptc);
	/*/


	/*/
    void* *inptcs = NULL; 
    void* outptc = NULL;
    float w2e[16], w2n[16], format[3];
    float point[3], normal[3];
    float radius, *data;
    int nInFiles = argc-2, f, v;
    int datasize, nvars, nv;
    int npoints, p;
    char *vartypes[256], *varnames[256], *vt[256], *vn[256];
    char *inname, *outname;

    if (argc < 3) {
	fprintf(stderr, "ptmerge error: needs at least one input file and an output\n");
	exit(1);
    }

    inptcs = new void*[nInFiles];

    inname = argv[1];
    inptcs[0] = PtcOpenPointCloudFile(inname, &nvars, vartypes, varnames);
    if (!inptcs[0]) {
	fprintf(stderr, "ptmerge error: unable to open input file %s\n",
		inname);
	exit(1);
    }

    for (f = 1; f < nInFiles; f++) {
	inname = argv[f+1];
	inptcs[f] = PtcOpenPointCloudFile(inname, &nv, vt, vn);
	if (!inptcs[f]) {
	    fprintf(stderr, "ptmerge error: unable to open input file %s, skipping it.\n",
		    inname);
	    continue;
	}
	if (nv != nvars) {
	    fprintf(stderr, "ptmerge error: input files differ number of vars\n");
	    exit(1);
	}
	for (v = 0; v < nvars; v++) {
	    if (strcmp(vartypes[v], vt[v])) {
		fprintf(stderr, "ptmerge error: input files differ in data types: %s vs %s\n",
			vartypes[v], vt[v]);
		exit(1);
	    }
	    if (strcmp(varnames[v], vn[v])) {
		fprintf(stderr, "ptmerge error: input files differ in data names: %s vs %s\n",
			varnames[v], vn[v]);
		exit(1);
	    }
	}
    }

    PtcGetPointCloudInfo(inptcs[0], "world2eye", w2e);
    PtcGetPointCloudInfo(inptcs[0], "world2ndc", w2n);
    PtcGetPointCloudInfo(inptcs[0], "format", format);

    outname = argv[argc - 1];
    outptc = PtcCreatePointCloudFile(outname, nvars, vartypes, varnames,
				     w2e, w2n, format);
    if (!outptc) {
	fprintf(stderr, "Unable to open output file %s.\n", outname);
	exit(1);
    }

    PtcGetPointCloudInfo(inptcs[0], "datasize", &datasize);
    data = (float *) malloc(datasize * sizeof(float));

    for (f = 0; f < nInFiles; ++f) {
	PtcGetPointCloudInfo(inptcs[f], "npoints", &npoints);
	inname = argv[f+1];
	printf("input file '%s' has %i points\n", inname, npoints);
	for (p = 0; p < npoints; p++) {
	    PtcReadDataPoint(inptcs[f], point, normal, &radius, data);
	    PtcWriteDataPoint(outptc, point, normal, radius, data);
	}
	PtcClosePointCloudFile(inptcs[f]);
    }

    free(inptcs);
    free(data);

    PtcFinishPointCloudFile(outptc);

    printf("merged file '%s' written\n", outname);

	{
		const char* br = "C:/M/public/untitled.perspShape.ocf.0001.bkm";
		float c[3];

		RtPoint p={0, 0, 0};
		RtNormal n={0, 0, 0};
		RtFloat r = 0;
		RxTexture3d( (char*)br, p, n, r, "color c", *c, NULL);
	}
	/*/


    return 0;
}