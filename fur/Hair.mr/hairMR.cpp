/*-----------------------------------------------------------------------------
 * hairMR.c
 *
 * demonstrates how to build a feather object in a geometry shader using hair
 * primitives, with a callback.
 *
 * (C) mental images 2003
 *---------------------------------------------------------------------------*/

#include <mr/shader.h>
#include <mr/geoshader.h>

#include "ocellaris\renderFileImpl.h"
#include "ocellaris\renderMentalRayImpl.h"


typedef struct
{
	miTag file_name;
}hairMR_t;

extern "C"
{

// выкинуть!!!
DLLEXPORT miBoolean hairMR_cb(miTag tag, void *args);

DLLEXPORT miInteger chevelureGeoMR_version(miState *state, hairMR_t *paras, miBoolean *inst_init_req)
{
    return 1;
}


DLLEXPORT miBoolean chevelureGeoMR(miTag *result, miState *state, hairMR_t *paras)
{
	fprintf(stdout, "chevelureGeoMR\n");
	fflush(stdout);

	bool bTest = false;

	if( !bTest)
	{
		/* Get filename */
		miTag tag;
		char* file_name="";
		if( paras->file_name) 
		{
			tag = *mi_eval_tag( &paras->file_name);
			file_name = (char*)mi_db_access(tag);
		} 

		fprintf(stdout, "file_name=%s\n", file_name);
		fflush(stdout);

		// open ocs
		cls::renderFileImpl fileimpl;
		cls::renderCacheImpl<cls::CacheStorageSimple> cache;
		if( !fileimpl.openForRead(file_name, &cache))
		{
			fprintf(stderr, "Some error during opening %s\n", file_name);
			return miFALSE;
		}
		fileimpl.Render(&cache);

		// рендерить
		cls::renderMentalRayImpl<> mental(result);
		cache.Render(&mental);
	}
	
	if( bTest)
	{
		miTag tag;
		miObject *obj = mi_api_object_begin(mi_mem_strdup("hairMR"));


		mi_api_object_callback(hairMR_cb, (void *)paras);
		obj->visible = obj->reflection = obj->refraction = miTRUE;
		obj->shadow  = 0x7F;
		obj->shadowmap = miTRUE;

		obj->bbox_min.x = -0.5f;
		obj->bbox_min.y =  0.0f;
		obj->bbox_min.z = -0.1f;
		obj->bbox_max.x =  0.5f;
		obj->bbox_max.y =  2.0f;
		obj->bbox_max.z =  0.1f;

		tag = mi_api_object_end();
		mi_geoshader_add_result(result, tag);
		obj = (miObject *)mi_scene_edit(tag);
		obj->geo.placeholder_list.type = miOBJECT_HAIR;
		mi_scene_edit_end(tag);
	}

    return miTRUE;
}




DLLEXPORT miBoolean hairMR_cb(miTag tag, void *args)
{
	fprintf(stdout, "hairMR_cb\n");
	fflush(stdout);

    const char      *name;
    miObject        *obj;
    miHair_list     *hair;
    miScalar        *sarray;
    miGeoIndex      *harray;
    hairMR_t *paras = (hairMR_t *)args;
    int i = 0;
    int h;
//   int hcount = 41;
    /* when using the nbarbs parameter, instead of default here of 41 ... */
	int hcount = 41;

    name = mi_api_tag_lookup(tag);
    mi_api_incremental(miTRUE);

    obj = mi_api_object_begin(mi_mem_strdup(name));

	obj->visible = obj->reflection = obj->refraction = miTRUE;
	obj->shadow  = 0x7F;
	obj->shadowmap = miTRUE;

    hair = mi_api_hair_begin();
 //  hair->approx = 5;				//need edit here for mr 3.7
    hair->degree = 3;               /* cubic                    */

    mi_api_hair_info(1, 'r', 1);    /* per-hair radius          */
    mi_api_hair_info(1, 't', 4);    /* per-vertex textures (4)  */
    mi_api_hair_info(1, 'm', 3);    /* per-vertex motion vector */

    /* here we add the geometry data. in a real program, this would be a
       very clever part. i just use numbers from a sample mi file i have. */
    sarray = mi_api_hair_scalars_begin(44*(hcount*2+1));
    i = 0;
    /* hair 1 is spine of feather */
    /* per-hair data for hair 1, here just the radius. */
    /* hair 1 control points 1-4, incuding per-vertex data. */
    sarray[i++] =  0.0f; sarray[i++] =  0.0f; sarray[i++] =  0.0f; /* vertex  */
	sarray[i++] =  0.01f;
    sarray[i++] =  1.0f; sarray[i++] =  1.0f; sarray[i++] =  1.0f; sarray[i++] =  1.0f; /* texture */
    sarray[i++] =  0.0f; sarray[i++] =  0.0f; sarray[i++] =  0.0f; /* motion  */

	
    sarray[i++] =  0.0f; sarray[i++] =  0.33f; sarray[i++] =  0.0f; /* vertex  */
	sarray[i++] =  0.02f;
    sarray[i++] =  1.0f; sarray[i++] =  1.0f; sarray[i++] =  1.0f; sarray[i++] =  0.9f; /* texture */
    sarray[i++] =  0.0f; sarray[i++] =  0.33f; sarray[i++] =  0.0f; /* motion  */

	
    sarray[i++] =  0.0f; sarray[i++] =  0.67f; sarray[i++] =  0.0f; /* vertex  */
	sarray[i++] =  0.01f;
    sarray[i++] =  1.0f; sarray[i++] =  1.0f; sarray[i++] =  1.0f; sarray[i++] =  0.8f; /* texture */
    sarray[i++] =  0.0f; sarray[i++] =  0.67f; sarray[i++] =  0.0f; /* motion  */

	 sarray[i++] =  0.0f; sarray[i++] =  1.0f; sarray[i++] =  0.0f; /* vertex  */
	sarray[i++] =  0.005f;
    sarray[i++] =  1.0f; sarray[i++] =  1.0f; sarray[i++] =  1.0f; sarray[i++] =  0.7f; /* texture */
    sarray[i++] =  0.0f; sarray[i++] =  1.0f; sarray[i++] =  0.0f; /* motion  */

    /* iterate for hairs along spine */

    for (h=0; h < hcount; h++) 
	{
      /* per-hair data for hair 2, ie. radius. */
      
      /* hair 2, 3, 4, ... control points 1-4, incuding per-vertex data. */
      sarray[i++] =  0.01f; sarray[i++] =  0.2f + h * 0.025f; sarray[i++] =  0.0f; /* vertex  */
	  sarray[i++] =  0.003f;
      sarray[i++] =  0.0f;  sarray[i++] =  0.6f;            sarray[i++] =  0.8f; sarray[i++] =  0.4f; /* texture */
      sarray[i++] =  0.01f; sarray[i++] =  0.2f + h * 0.025f; sarray[i++] =  0.0f; /* motion  */

		

      sarray[i++] =  0.11f; sarray[i++] =  0.3f + h * 0.025f; sarray[i++] =  0.0f; /* vertex  */
	  sarray[i++] =  0.01f;
      sarray[i++] =  0.0f;  sarray[i++] =  0.6f;            sarray[i++] =  0.8f; sarray[i++] =  0.34f; /* texture */
      sarray[i++] =  0.11f; sarray[i++] =  0.3f + h * 0.025f; sarray[i++] =  0.0f; /* motion  */

		

      sarray[i++] =  0.21f; sarray[i++] =  0.38f + h * 0.025f; sarray[i++] =  0.0f; /* vertex  */
	  sarray[i++] =  0.006f;
      sarray[i++] =  0.0f;  sarray[i++] =  0.6f;             sarray[i++] =  0.8f; sarray[i++] =  0.26f; /* texture */
      sarray[i++] =  0.21f; sarray[i++] =  0.38f + h * 0.025f; sarray[i++] =  0.0f; /* motion  */

		

      sarray[i++] =  0.31f; sarray[i++] =  0.46f + h * 0.025f; sarray[i++] =  0.0f; /* vertex  */
	  sarray[i++] =  0.001f;
      sarray[i++] =  0.0f;  sarray[i++] =  0.4f;             sarray[i++] =  0.8f; sarray[i++] =  0.0f; /* texture */
      sarray[i++] =  0.31f; sarray[i++] =  0.46f + h * 0.025f; sarray[i++] =  0.0f; /* motion  */

      /* mirrored per-hair data for hair 2, ie. radius. */
      
      /* mirror hair 2, 3, 4, ... control points 1-4, incuding per-vertex data. */
      sarray[i++] = -0.01f; sarray[i++] =  0.2f + h * 0.025f; sarray[i++] =  0.0f; /* vertex  */
	  sarray[i++] =  0.003f;
      sarray[i++] =  0.0f;  sarray[i++] =  0.6f;            sarray[i++] =  0.8f; sarray[i++] =  0.4f; /* texture */
      sarray[i++] = -0.01f; sarray[i++] =  0.2f + h * 0.025f; sarray[i++] =  0.0f; /* motion  */


      sarray[i++] = -0.11f; sarray[i++] =  0.3f + h * 0.025f; sarray[i++] =  0.0f; /* vertex  */
	  sarray[i++] =  0.01f;
      sarray[i++] =  0.0f;  sarray[i++] =  0.6f;            sarray[i++] =  0.8f; sarray[i++] =  0.34f; /* texture */
      sarray[i++] = -0.11f; sarray[i++] =  0.3f + h * 0.025f; sarray[i++] =  0.0f; /* motion  */

		
      sarray[i++] = -0.21f; sarray[i++] =  0.38f + h * 0.025f; sarray[i++] =  0.0f; /* vertex  */
	  sarray[i++] =  0.006f;
      sarray[i++] =  0.0f;  sarray[i++] =  0.6f;             sarray[i++] =  0.8f; sarray[i++] =  0.26f; /* texture */
      sarray[i++] = -0.21f; sarray[i++] =  0.38f + h * 0.025f; sarray[i++] =  0.0f; /* motion  */

		
      sarray[i++] = -0.31f; sarray[i++] =  0.46f + h * 0.025f; sarray[i++] =  0.0f; /* vertex  */
	  sarray[i++] =  0.001f;
      sarray[i++] =  0.0f;  sarray[i++] =  0.4f;             sarray[i++] =  0.8f; sarray[i++] =  0.20f; /* texture */
      sarray[i++] = -0.31f; sarray[i++] =  0.46f + h * 0.025f; sarray[i++] =  0.0f; /* motion  */
    }
    mi_api_hair_scalars_end( 44 * (hcount*2 + 1) );

    /* there are just the two hairs, ie. three numbers describe the sizes. */
    harray = mi_api_hair_hairs_begin(hcount*2 + 1 + 1);
    for (h=0; h < hcount*2 + 1 + 1; h++) {
      harray[h] = h * 44;
    }
    mi_api_hair_hairs_end();

    mi_api_hair_end();
    mi_api_object_end();

    return miTRUE;
}
}
