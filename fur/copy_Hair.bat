set BIN_DIR=.\out\hair
set BIN_DIR=\\server.ulitka.home\bin70

mkdir %BIN_DIR%
mkdir %BIN_DIR%\bin
mkdir %BIN_DIR%\mel
mkdir %BIN_DIR%\slim
mkdir %BIN_DIR%\docs

rem OCELLARIS
copy BinRelease\OcellarisExport.dll		%BIN_DIR%\bin

rem SN
copy BinRelease\SnHairShaper.dll			%BIN_DIR%\bin
copy BinRelease\SnMaya.mll					%BIN_DIR%\bin
copy BinRelease\SnDelayMaya.dll			%BIN_DIR%\bin
copy BinRelease\SnMath.dll					%BIN_DIR%\bin

rem copy Mel\AESnInstanceTemplate.mel	%BIN_DIR%\mel
rem copy Mel\AESnInstancerTemplate.mel	%BIN_DIR%\mel
rem copy Mel\AESnShapeTemplate.mel		%BIN_DIR%\mel
rem copy Mel\AESnMotherTemplate.mel		%BIN_DIR%\mel
rem copy Mel\sn.mel						%BIN_DIR%\mel

rem HAIR
copy BinRelease\HairCustomDeforms.dll		%BIN_DIR%\bin
copy BinRelease\HairDSO.dll				%BIN_DIR%\bin
copy BinRelease\HairDSOTest.exe			%BIN_DIR%\bin
copy BinRelease\HairMath.dll				%BIN_DIR%\bin
copy BinRelease\HairMaya.mll				%BIN_DIR%\bin
copy BinRelease\HairRibGen.dll				%BIN_DIR%\bin
copy BinRelease\HairRibGen.slim			%BIN_DIR%\bin
copy BinRelease\HairFftDeformer.dll		%BIN_DIR%\bin

copy Mel\UlHairMenu.mel				%BIN_DIR%\mel
rem copy Mel\AEHairEmitterTemplate.mel	%BIN_DIR%\mel
rem copy Mel\AEHairModifierTemplate.mel	%BIN_DIR%\mel
rem copy Mel\AEHairShapeTemplate.mel	%BIN_DIR%\mel
rem copy Mel\HairAttrPaint.mel			%BIN_DIR%\mel
rem copy Mel\UlHair.mel					%BIN_DIR%\mel
rem copy Mel\UlHairForms.mel			%BIN_DIR%\mel
rem copy Mel\AEHairShapeTemplate.mel	%BIN_DIR%\mel

copy slim\ulHairSurfaceUVmanifold.slim %BIN_DIR%\slim\

rem copy docs\HairMaya.doc			%BIN_DIR%\docs
xcopy /E /I /Y docs\Hair			%BIN_DIR%\docs\Hair

pause