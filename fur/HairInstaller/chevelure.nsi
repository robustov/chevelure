; example2.nsi
;
; This script is based on example1.nsi, but it remember the directory, 
; has uninstall support and (optionally) installs start menu shortcuts.
;
; It will install example2.nsi into a directory that the user selects,

;--------------------------------
!include "WordFunc.nsh"
!include "TextFunc.nsh"

!insertmacro WordAdd
!insertmacro WordReplace
!insertmacro un.WordAdd
!insertmacro LineFind

!ifdef ZLIB
	SetCompressor zlib
!else
	SetCompressor /FINAL /SOLID lzma
	SetCompressorDictSize 64
!endif

!ifndef BINDIR
    #define BINDIR "$%SASBIN%\x86_64.vc90.release.maya2012\"
!endif 

;!define PRMAN14
;!define PRMAN15
;!define PRMAN16
;!define PRMAN17
;!define PRMAN18
;!define PRMAN19
!define PRMAN20

!ifdef CHEVELUREFULL
	!ifdef MAYA70
		Name "Chevelure70"
		OutFile "$%SASBIN%\Chevelure70.exe"
	!endif
	!ifdef MAYA85
		!ifndef W64
			Name "Chevelure85"
			OutFile "$%SASBIN%\Chevelure2008x32.exe"
		!else
			Name "Chevelure85x64"
			OutFile "$%SASBIN%\Chevelure2008x64.exe"
		!endif
	!endif
	!ifdef MAYA2009
		!ifndef W64
			Name "Chevelure2009"
			OutFile "$%SASBIN%\Chevelure2009x32.exe"
		!else
			Name "Chevelure2009x64"
			OutFile "$%SASBIN%\Chevelure2009x64.exe"
		!endif
	!endif
	!ifdef MAYA2010
		!ifndef W64
			Name "Chevelure2010"
			OutFile "$%SASBIN%\Chevelure2010x32.exe"
		!else
			Name "Chevelure2010x64"
			OutFile "$%SASBIN%\Chevelure2010x64.exe"
		!endif
	!endif
	!ifdef MAYA2011
		!ifndef W64
			Name "Chevelure2011"
			OutFile "$%SASBIN%\Chevelure2011x32.exe"
		!else
			Name "Chevelure2011x64"
			OutFile "$%SASBIN%\Chevelure2011x64.exe"
		!endif
	!endif
	!ifdef MAYA2012
		!ifndef W64
			Name "Chevelure2012"
			OutFile "$%SASBIN%\Chevelure2012x32.exe"
		!else
			Name "Chevelure2012x64"
			OutFile "$%SASBIN%\Chevelure2012x64.exe"
		!endif
	!endif
	!ifdef MAYA2013
		!ifndef W64
			Name "Chevelure2013"
			OutFile "$%SASBIN%\Chevelure2013x32.exe"
		!else
			Name "Chevelure2013x64"
			OutFile "$%SASBIN%\Chevelure2013x64.exe"
		!endif
	!endif
	!ifdef MAYA2014
		!ifndef W64
			Name "Chevelure2014"
			OutFile "$%SASBIN%\Chevelure2014x32.exe"
		!else
			Name "Chevelure2014x64"
			OutFile "$%SASBIN%\Chevelure2014x64.exe"
		!endif
	!endif
	!ifdef MAYA2015
		!ifndef W64
			Name "Chevelure2015"
			OutFile "$%SASBIN%\Chevelure2015x32.exe"
		!else
			Name "Chevelure2015x64"
			OutFile "$%SASBIN%\Chevelure2015x64.exe"
		!endif
	!endif
	!ifdef MAYA2016
		!ifndef W64
			Name "Chevelure2016"
			OutFile "$%SASBIN%\Chevelure2016x32.exe"
		!else
			Name "Chevelure2016x64"
			OutFile "$%SASBIN%\Chevelure2016x64.exe"
		!endif
	!endif
	
!endif

!ifdef CHEVELURELIGHT
	; The name of the installer
	Name "ChevelureLight"
	; The file to write
	OutFile "$%SASBIN%\ChevelureLight.exe"
!endif

!ifdef DEMO 
	!ifdef MAYA70
		Name "ChevelureFree70"
		OutFile "$%SASBIN%\ChevelureFree70.exe"
	!endif
	!ifdef MAYA85
		Name "ChevelureFree85"
		OutFile "$%SASBIN%\ChevelureFree2008.exe"
	!endif
!endif


; The default installation directory
InstallDir $PROGRAMFILES\Chevelure

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\Chevelure" "Install_Dir"


;--------------------------------
; Pages
!define TEMP1 $R0 ;Temp variable
ReserveFile "${NSISDIR}\Plugins\InstallOptions.dll"
ReserveFile "installoptions.ini"

Page directory
Page custom INSTALLOPTIONS
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

; The stuff to install
Section "Chevelure"

  SectionIn RO
  
  ; Set output path to the installation directory.
  SetOutPath $INSTDIR

  ;File /oname=chevelure.doc "$%SASBIN%\bin\chevelureMitka.doc"
  
  ; Put file there
!ifndef W64
	File "${BINDIR}\ocellarisProc.dll"
	File "${BINDIR}\ocellarisPrmanDSO.dll"

;	File "${BINDIR}\IPrmanDSO16.0.dll"
;	File "${BINDIR}\IPrman16.0.dll"

;	File "${BINDIR}\IPrmanDSO15.0.dll"
;	File "${BINDIR}\IPrman15.0.dll"

	File "${BINDIR}\IPrmanDSO14.0.dll"
	File "${BINDIR}\IPrman14.0.dll"

	File "${BINDIR}\IPrmanDSO13.5.dll"
	File "${BINDIR}\IPrman13.5.dll"
	
!ifdef PRMAN14
	File /oname=IPrmanDSO.dll		"${BINDIR}\IPrmanDSO14.0.dll"
	File /oname=IPrman.dll			"${BINDIR}\IPrman14.0.dll"
!endif
!ifdef PRMAN15
	File /oname=IPrmanDSO.dll		"${BINDIR}\IPrmanDSO15.0.dll"
	File /oname=IPrman.dll			"${BINDIR}\IPrman15.0.dll"
!endif

	File "${BINDIR}\chevelureDSO.dll"
	File "${BINDIR}\chevelureDSOTest.exe"
	File "${BINDIR}\chevelureMath.dll"
	;File "${BINDIR}\SnHairShaper.dll"
	File "${BINDIR}\chevelureRibGen.dll"
    File "$%SASBIN%\slim\chevelureRibGen.slim"
    File "${BINDIR}\ocellarisRibGen.dll"
    File "$%SASBIN%\bin\OcellarisRibGen.slim"

    File "$%SASBIN%\bin\Changes.txt"
    File "$%SASBIN%\bin\Chevelure_installation.txt"
    File "$%SASBIN%\bin\ChevelureLicenseServer_installation.txt"

	!ifdef MAYA85
		File "${BINDIR}\chevelureMaya.mll"
		File "${BINDIR}\ocellarisExport.dll"
		File "${BINDIR}\ocellarisMaya.mll"
			
		File "${BINDIR}\SnDelayMaya.dll"
		File "${BINDIR}\SnMath.dll"
		File "${BINDIR}\SnMaya.mll"

		File /oname=IPrmanRMS.dll		"${BINDIR}\IPrmanRMS2008.dll"
	!endif
	
	!ifdef MAYA2009
		File "${BINDIR}\chevelureMaya.mll"
		File "${BINDIR}\ocellarisExport.dll"
		File "${BINDIR}\ocellarisMaya.mll"
			
		File "${BINDIR}\SnDelayMaya.dll"
		File "${BINDIR}\SnMath.dll"
		File "${BINDIR}\SnMaya.mll"
		
		File /oname=IPrmanRMS.dll		"${BINDIR}\IPrmanRMS2009.dll"
	!endif

	!ifdef MAYA2010
		File "${BINDIR}\chevelureMaya.mll"
		File "${BINDIR}\ocellarisExport.dll"
		File "${BINDIR}\ocellarisMaya.mll"
			
		File "${BINDIR}\SnDelayMaya.dll"
		File "${BINDIR}\SnMath.dll"
		File "${BINDIR}\SnMaya.mll"
		
		File /oname=IPrmanRMS.dll		"${BINDIR}\IPrmanRMS2010.dll"
	!endif

	!ifdef MAYA2011
		File "${BINDIR}\chevelureMaya.mll"
		File "${BINDIR}\ocellarisExport.dll"
		File "${BINDIR}\ocellarisMaya.mll"
			
		File "${BINDIR}\SnDelayMaya.dll"
		File "${BINDIR}\SnMath.dll"
		File "${BINDIR}\SnMaya.mll"
		
		File /oname=IPrmanRMS.dll		"${BINDIR}\IPrmanRMS2011.dll"
	!endif

	!ifdef MAYA2012
		File "${BINDIR}\chevelureMaya.mll"
		File "${BINDIR}\ocellarisExport.dll"
		File "${BINDIR}\ocellarisMaya.mll"
			
		File "${BINDIR}\SnDelayMaya.dll"
		File "${BINDIR}\SnMath.dll"
		File "${BINDIR}\SnMaya.mll"
		
		File /oname=IPrmanRMS.dll		"${BINDIR}\IPrmanRMS2012.dll"
	!endif
	
	!ifdef MAYA2013
		File "${BINDIR}\chevelureMaya.mll"
		File "${BINDIR}\ocellarisExport.dll"
		File "${BINDIR}\ocellarisMaya.mll"
			
		File "${BINDIR}\SnDelayMaya.dll"
		File "${BINDIR}\SnMath.dll"
		File "${BINDIR}\SnMaya.mll"
		
		File /oname=IPrmanRMS.dll		"${BINDIR}\IPrmanRMS2013.dll"
	!endif
	
	!ifdef MAYA2014
		File "${BINDIR}\chevelureMaya.mll"
		File "${BINDIR}\ocellarisExport.dll"
		File "${BINDIR}\ocellarisMaya.mll"
			
		File "${BINDIR}\SnDelayMaya.dll"
		File "${BINDIR}\SnMath.dll"
		File "${BINDIR}\SnMaya.mll"
		
		File /oname=IPrmanRMS.dll		"${BINDIR}\IPrmanRMS2014.dll"
	!endif
	
	!ifdef MAYA2015
		File "${BINDIR}\chevelureMaya.mll"
		File "${BINDIR}\ocellarisExport.dll"
		File "${BINDIR}\ocellarisMaya.mll"
			
		File "${BINDIR}\SnDelayMaya.dll"
		File "${BINDIR}\SnMath.dll"
		File "${BINDIR}\SnMaya.mll"
		
		File /oname=IPrmanRMS.dll		"${BINDIR}\IPrmanRMS2015.dll"
	!endif
	
	!ifdef MAYA2016
		File "${BINDIR}\chevelureMaya.mll"
		File "${BINDIR}\ocellarisExport.dll"
		File "${BINDIR}\ocellarisMaya.mll"
			
		File "${BINDIR}\SnDelayMaya.dll"
		File "${BINDIR}\SnMath.dll"
		File "${BINDIR}\SnMaya.mll"
		
		File /oname=IPrmanRMS.dll		"${BINDIR}\IPrmanRMS2016.dll"
	!endif
	
	SetOutPath $INSTDIR\mentalray\include
	File /oname=chevelureGeoMR.mi "..\Hair.mr\hairMR.mi"
	SetOutPath $INSTDIR\mentalray\lib
	File "${BINDIR}\chevelureGeoMR.dll"

  
!else
	;
	;W64
	;
	File "${BINDIR}\ocellarisProc.dll"
	File "${BINDIR}\ocellarisPrmanDSO.dll"
	
	File "${BINDIR}\IPrmanDSO14.0.dll"
	File "${BINDIR}\IPrman14.0.dll"
	
	File "${BINDIR}\IPrmanDSO15.0.dll"
	File "${BINDIR}\IPrman15.0.dll"
	
	File "${BINDIR}\IPrmanDSO16.0.dll"
	File "${BINDIR}\IPrman16.0.dll"
	
	File "${BINDIR}\IPrmanDSO17.0.dll"
	File "${BINDIR}\IPrman17.0.dll"
	
	File "${BINDIR}\IPrmanDSO18.0.dll"
	File "${BINDIR}\IPrman18.0.dll"
	
	File "${BINDIR}\IPrmanDSO19.0.dll"
	File "${BINDIR}\IPrman19.0.dll"
	
	File "${BINDIR}\IPrmanDSO20.0.dll"
	File "${BINDIR}\IPrman20.0.dll"
	
!ifdef PRMAN14
	File /oname=IPrmanDSO.dll		"${BINDIR}\IPrmanDSO14.0.dll"
	File /oname=IPrman.dll			"${BINDIR}\IPrman14.0.dll"
!endif
!ifdef PRMAN15
	File /oname=IPrmanDSO.dll		"${BINDIR}\IPrmanDSO15.0.dll"
	File /oname=IPrman.dll			"${BINDIR}\IPrman15.0.dll"
!endif
!ifdef PRMAN16
	File /oname=IPrmanDSO.dll		"${BINDIR}\IPrmanDSO16.0.dll"
	File /oname=IPrman.dll			"${BINDIR}\IPrman16.0.dll"
!endif
!ifdef PRMAN17
	File /oname=IPrmanDSO.dll		"${BINDIR}\IPrmanDSO17.0.dll"
	File /oname=IPrman.dll			"${BINDIR}\IPrman17.0.dll"
!endif
!ifdef PRMAN18
	File /oname=IPrmanDSO.dll		"${BINDIR}\IPrmanDSO18.0.dll"
	File /oname=IPrman.dll			"${BINDIR}\IPrman18.0.dll"
!endif
!ifdef PRMAN19
	File /oname=IPrmanDSO.dll		"${BINDIR}\IPrmanDSO19.0.dll"
	File /oname=IPrman.dll			"${BINDIR}\IPrman19.0.dll"
!endif
!ifdef PRMAN19
	File /oname=IPrmanDSO.dll		"${BINDIR}\IPrmanDSO20.0.dll"
	File /oname=IPrman.dll			"${BINDIR}\IPrman20.0.dll"
!endif

	File "${BINDIR}\chevelureDSO.dll"
	File "${BINDIR}\chevelureDSOTest.exe"
	File "${BINDIR}\chevelureMath.dll"
	File "${BINDIR}\SnHairShaper.dll"
	File "${BINDIR}\chevelureRibGen.dll"
    File "$%SASBIN%\slim\chevelureRibGen.slim"
    File "${BINDIR}\ocellarisRibGen.dll"
    File "$%SASBIN%\bin\OcellarisRibGen.slim"
    
    File "$%SASBIN%\bin\Changes.txt"
    File "$%SASBIN%\bin\Chevelure_installation.txt"
    File "$%SASBIN%\bin\ChevelureLicenseServer_installation.txt"

	!ifdef MAYA85
		File "${BINDIR}\chevelureMaya.mll"
		File "${BINDIR}\ocellarisExport.dll"
		File "${BINDIR}\ocellarisMaya.mll"
			
		File "${BINDIR}\SnDelayMaya.dll"
		File "${BINDIR}\SnMath.dll"
		File "${BINDIR}\SnMaya.mll"

		File /oname=IPrmanRMS.dll		"${BINDIR}\IPrmanRMS2008.dll"
		
		SetOutPath $INSTDIR\python
	    File /oname=ocsCommands.py		"$%SASBIN%\python\ocsCommands.py"
	    File /oname=furRFMExportHelper.py		"$%SASBIN%\python\furRFMExportHelper.py"
		SetOutPath $INSTDIR
	
	!endif
	
	!ifdef MAYA2009
		File "${BINDIR}\chevelureMaya.mll"
		File "${BINDIR}\ocellarisExport.dll"
		File "${BINDIR}\ocellarisMaya.mll"
			
		File "${BINDIR}\SnDelayMaya.dll"
		File "${BINDIR}\SnMath.dll"
		File "${BINDIR}\SnMaya.mll"
		
		File /oname=IPrmanRMS.dll		"${BINDIR}\IPrmanRMS2009.dll"

		SetOutPath $INSTDIR\python
	    File /oname=ocsCommands.py		"$%SASBIN%\python\ocsCommands.py"
	    File /oname=furRFMExportHelper.py		"$%SASBIN%\python\furRFMExportHelper.py"
		SetOutPath $INSTDIR
	!endif

	!ifdef MAYA2010
		File "${BINDIR}\chevelureMaya.mll"
		File "${BINDIR}\ocellarisExport.dll"
		File "${BINDIR}\ocellarisMaya.mll"
			
		File "${BINDIR}\SnDelayMaya.dll"
		File "${BINDIR}\SnMath.dll"
		File "${BINDIR}\SnMaya.mll"
		
		File /oname=IPrmanRMS.dll		"${BINDIR}\IPrmanRMS2010.dll"

		SetOutPath $INSTDIR\python
	    File /oname=ocsCommands.py		"$%SASBIN%\python\ocsCommands.py"
	    File /oname=furRFMExportHelper.py		"$%SASBIN%\python\furRFMExportHelper.py"
		SetOutPath $INSTDIR
	!endif

	!ifdef MAYA2011
		File "${BINDIR}\chevelureMaya.mll"
		File "${BINDIR}\ocellarisExport.dll"
		File "${BINDIR}\ocellarisMaya.mll"
			
		File "${BINDIR}\SnDelayMaya.dll"
		File "${BINDIR}\SnMath.dll"
		File "${BINDIR}\SnMaya.mll"
		
		File /oname=IPrmanRMS.dll		"${BINDIR}\IPrmanRMS2011.dll"

		SetOutPath $INSTDIR\python
	    File /oname=ocsCommands.py		"$%SASBIN%\python\ocsCommands.py"
	    File /oname=furRFMExportHelper.py		"$%SASBIN%\python\furRFMExportHelper.py"
		SetOutPath $INSTDIR
	!endif
	
	!ifdef MAYA2012
		File "${BINDIR}\chevelureMaya.mll"
		File "${BINDIR}\ocellarisExport.dll"
		File "${BINDIR}\ocellarisMaya.mll"
			
		File "${BINDIR}\SnDelayMaya.dll"
		File "${BINDIR}\SnMath.dll"
		File "${BINDIR}\SnMaya.mll"
		
		File /oname=IPrmanRMS.dll		"${BINDIR}\IPrmanRMS2012.dll"

		SetOutPath $INSTDIR\python
	    File /oname=ocsCommands.py		"$%SASBIN%\python\ocsCommands.py"
	    File /oname=furRFMExportHelper.py		"$%SASBIN%\python\furRFMExportHelper.py"
		SetOutPath $INSTDIR
	!endif

	!ifdef MAYA2013
		File "${BINDIR}\chevelureMaya.mll"
		File "${BINDIR}\ocellarisExport.dll"
		File "${BINDIR}\ocellarisMaya.mll"
			
		File "${BINDIR}\SnDelayMaya.dll"
		File "${BINDIR}\SnMath.dll"
		File "${BINDIR}\SnMaya.mll"
		
		File /oname=IPrmanRMS.dll		"${BINDIR}\IPrmanRMS2013.dll"

		SetOutPath $INSTDIR\python
	    File /oname=ocsCommands.py		"$%SASBIN%\python\ocsCommands.py"
	    File /oname=furRFMExportHelper.py		"$%SASBIN%\python\furRFMExportHelper.py"
		SetOutPath $INSTDIR
	!endif

	!ifdef MAYA2014
		File "${BINDIR}\chevelureMaya.mll"
		File "${BINDIR}\ocellarisExport.dll"
		File "${BINDIR}\ocellarisMaya.mll"
			
		File "${BINDIR}\SnDelayMaya.dll"
		File "${BINDIR}\SnMath.dll"
		File "${BINDIR}\SnMaya.mll"
		
		File /oname=IPrmanRMS.dll		"${BINDIR}\IPrmanRMS2014.dll"

		SetOutPath $INSTDIR\python
	    File /oname=ocsCommands.py		"$%SASBIN%\python\ocsCommands.py"
	    File /oname=furRFMExportHelper.py		"$%SASBIN%\python\furRFMExportHelper.py"
		SetOutPath $INSTDIR
	!endif

	!ifdef MAYA2015
		File "${BINDIR}\chevelureMaya.mll"
		File "${BINDIR}\ocellarisExport.dll"
		File "${BINDIR}\ocellarisMaya.mll"
			
		File "${BINDIR}\SnDelayMaya.dll"
		File "${BINDIR}\SnMath.dll"
		File "${BINDIR}\SnMaya.mll"
		
		File /oname=IPrmanRMS.dll		"${BINDIR}\IPrmanRMS2015.dll"

		SetOutPath $INSTDIR\python
	    File /oname=ocsCommands.py		"$%SASBIN%\python\ocsCommands.py"
	    File /oname=furRFMExportHelper.py		"$%SASBIN%\python\furRFMExportHelper.py"
		SetOutPath $INSTDIR
	!endif

	!ifdef MAYA2016
		File "${BINDIR}\chevelureMaya.mll"
		File "${BINDIR}\ocellarisExport.dll"
		File "${BINDIR}\ocellarisMaya.mll"
			
		File "${BINDIR}\SnDelayMaya.dll"
		File "${BINDIR}\SnMath.dll"
		File "${BINDIR}\SnMaya.mll"
		
		File /oname=IPrmanRMS.dll		"${BINDIR}\IPrmanRMS2016.dll"

		SetOutPath $INSTDIR\python
	    File /oname=ocsCommands.py		"$%SASBIN%\python\ocsCommands.py"
	    File /oname=furRFMExportHelper.py		"$%SASBIN%\python\furRFMExportHelper.py"
		SetOutPath $INSTDIR
	!endif
	
	SetOutPath $INSTDIR\mentalray\include
	File /oname=chevelureGeoMR.mi "..\Hair.mr\hairMR.mi"
	;SetOutPath $INSTDIR\mentalray\lib
	;File "${BINDIR}\chevelureGeoMR.dll"
	
!endif
  
	
  
  File "$%SASBIN%\slim\slim.ini" 
  ${LineFind} "$INSTDIR\slim.ini" "$INSTDIR\slim.ini" "1:-1" "Example5"
 
	SetOutPath $INSTDIR\mel
	File "$%SASBIN%\mel\chevelureMenu.mel"
	File "$%SASBIN%\mel\chevelureRFMprocedure.mel"
	
	File "$%SASBIN%\mel\AEocRenderPassTemplate.mel"
	File "$%SASBIN%\mel\AEocRootTransformTemplate.mel"
	File "$%SASBIN%\mel\AEocRSLshaderTemplate.mel"
	File "$%SASBIN%\mel\AEocSequenceFileTemplate.mel"
	File "$%SASBIN%\mel\AEocShaderParameterTemplate.mel"
	File "$%SASBIN%\mel\AEocSurfaceShaderTemplate.mel"
	File "$%SASBIN%\mel\AEshapeTemplate.mel"
	File "$%SASBIN%\mel\ocellarisMayaMenu.mel"
	File "$%SASBIN%\mel\ocellarisPassesMenu.mel"
	File "$%SASBIN%\mel\ocellarisShadersMenu.mel"
	File "$%SASBIN%\mel\ocTranslatorOptions.mel"
	File "$%SASBIN%\mel\sloTranslatorOptions.mel"
	File "$%SASBIN%\mel\AEocCustomPassTemplate.mel"
	File "$%SASBIN%\mel\AEocellarisShapeHook.mel"
	File "$%SASBIN%\mel\AEocFileTemplate.mel"
	File "$%SASBIN%\mel\AEocGeneratorProxyTemplate.mel"
	File "$%SASBIN%\mel\AEocGeneratorSetTemplate.mel"
	File "$%SASBIN%\mel\AEocInstance2CreatorTemplate.mel"
	File "$%SASBIN%\mel\AEocInstance2ShapeTemplate.mel"
	File "$%SASBIN%\mel\AEocInstanceTemplate.mel"
	File "$%SASBIN%\mel\AEocJointVisTemplate.mel"
	File "$%SASBIN%\mel\AEocLightTemplate.mel"
	File "$%SASBIN%\mel\AEocOcclusionPassTemplate.mel"
	File "$%SASBIN%\mel\AEocParticleAttributeTemplate.mel"
	File "$%SASBIN%\mel\AEocParticleInstancerTemplate.mel"	

	SetOutPath $INSTDIR\python
	File "$%SASBIN%\python\ocsCommands.py"
	File "$%SASBIN%\python\ocsExport.py"
	File "$%SASBIN%\python\ocsRender.py"

	SetOutPath $INSTDIR\include
	File "$%SASBIN%\include\utilsOcs.h"
	File "$%SASBIN%\include\LambertOcs.h"
	File "$%SASBIN%\include\PhongOcs.h"
	File "$%SASBIN%\include\OcclusionOcs.h"
	File "$%SASBIN%\include\RampOcs.h"
	File "$%SASBIN%\include\SpotLigthOcs.h"
	File "$%SASBIN%\include\ul_filterwidth.h"
	File "$%SASBIN%\include\ul_noises.h"
	File "$%SASBIN%\include\ul_patterns.h"
	File "$%SASBIN%\include\ul_pxslRemap.h"
	File "$%SASBIN%\include\ul_pxslUtil.h"
	File "$%SASBIN%\include\ul_pxslRayUtil.h"
	File "$%SASBIN%\include\hairShaderUtils.h"

	SetOutPath $INSTDIR\sdk\include\chevelure
	File "..\HairMath\IHairDeformer.h"
	File "..\HairMath\HAIRPARAMS.h"
	File "..\HairMath\HAIRVERTS.h"
	File "..\HairMath\HAIRCONTROL.h"
	SetOutPath $INSTDIR\sdk\hairMR
	File "..\Hair.mr\Hair.mr80.vcproj"
	File "..\Hair.mr\hairMR.cpp"
	File "..\Hair.mr\hairMR.mi"
	SetOutPath $INSTDIR\sdk\deformers
	File "..\HairMath\Deformers\LenghtDeformer.cpp"
	File "..\HairMath\Deformers\LenghtRampDeformer.cpp"
	File "..\HairMath\Deformers\PolarFromControlsDeformer.cpp"
	File "..\HairMath\Deformers\renderRootPosDeformer.cpp"
	File "..\HairMath\Deformers\StickySurfaceDeformer.cpp"
	File "..\HairMath\Deformers\addDeformDeformer.cpp"
	File "..\HairMath\Deformers\GrassOnStormNoiseDeformer.cpp"
	File "..\HairMath\Deformers\GrassUnderRainDeformer.cpp"
	File "..\HairMath\Deformers\HairClumpDeformer.cpp"
	File "..\HairMath\Deformers\HairClumpDeformer.h"
	File "..\HairMath\Deformers\HairClumpRampDeformer.cpp"
	File "..\HairMath\Deformers\HairClumpRampDeformer.h"
	File "..\HairMath\Deformers\HairGrassDeformer.cpp"
	File "..\HairMath\Deformers\HairGrassDeformer.h"
	File "..\HairMath\Deformers\HairLeafDeformer.cpp"
	File "..\HairMath\Deformers\HairLeafDeformer.h"
	File "..\HairMath\Deformers\HairNoiseDeformer.cpp"
	File "..\HairMath\Deformers\HairNoiseDeformer.h"
	
	SetOutPath $INSTDIR
	
 
  ;ExpandEnvStrings $R0 "CHEVELURE=$INSTDIR"
 

  ReadINIStr ${TEMP1} "$PLUGINSDIR\installoptions.ini" "Field 1" "State"
  StrCmp ${TEMP1} "0" dontsetenvs
 
	; PATH
	ReadRegStr $R0 HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "Path" 
	IfErrors error0
		${WordAdd} $R0 ";" "+$INSTDIR" $R0
		WriteRegStr HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "Path" $R0 
	error0:
	  	
	; MAYA_PLUG_IN_PATH
	ReadRegStr $R0 HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "MAYA_PLUG_IN_PATH" 
	IfErrors error1
		${WordAdd} $R0 ";" "+$INSTDIR" $R0
		WriteRegStr HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "MAYA_PLUG_IN_PATH" $R0
	error1:

	; MAYA_SCRIPT_PATH
	ReadRegStr $R0 HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "MAYA_SCRIPT_PATH" 
	IfErrors error2
		${WordAdd} $R0 ";" "+$INSTDIR" $R0
		WriteRegStr HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "MAYA_SCRIPT_PATH" $R0
	error2:
	  
	; RAT_SCRIPT_PATHS
	ReadRegStr $R0 HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "RAT_SCRIPT_PATHS" 
	IfErrors error3
		${WordAdd} $R0 ";" "+$INSTDIR" $R0
		WriteRegStr HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "RAT_SCRIPT_PATHS" $R0
	error3:

	; PYTHONPATH
	ReadRegStr $R0 HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "PYTHONPATH" 
	IfErrors error4
		${WordAdd} $R0 ";" "+$INSTDIR" $R0
		WriteRegStr HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "PYTHONPATH" $R0
	error4:
  
  dontsetenvs:

  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\Chevelure "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Chevelure" "DisplayName" "Chevelure"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Chevelure" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Chevelure" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Chevelure" "NoRepair" 1
  WriteUninstaller "uninstall.exe"
  
SectionEnd

;--------------------------------
; Uninstaller
Section "Uninstall"

	
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Chevelure"
  DeleteRegKey HKLM SOFTWARE\Chevelure

  ; Remove files and uninstaller
  Delete $INSTDIR\*.*
;  Delete $INSTDIR\uninstall.exe

  ; Remove directories used
;  RMDir "$SMPROGRAMS\Chevelure"
  RMDir "$INSTDIR"

  ; PATH
  ReadRegStr $R0 HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "Path" 
  IfErrors error0
    ${un.WordAdd} $R0 ";" "-$INSTDIR" $R0
    WriteRegStr HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "Path" $R0 
  error0:

  ; MAYA_PLUG_IN_PATH
  ReadRegStr $R0 HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "MAYA_PLUG_IN_PATH" 
  IfErrors error1
    ${un.WordAdd} $R0 ";" "-$INSTDIR" $R0
    WriteRegStr HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "MAYA_PLUG_IN_PATH" $R0 
  error1:
  
  ; RAT_SCRIPT_PATHS
  ReadRegStr $R0 HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "RAT_SCRIPT_PATHS" 
  IfErrors error2
    ${un.WordAdd} $R0 ";" "-$INSTDIR" $R0
    WriteRegStr HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "RAT_SCRIPT_PATHS" $R0 
  error2:

  ; MAYA_SCRIPT_PATH
  ReadRegStr $R0 HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "MAYA_SCRIPT_PATH" 
  IfErrors error3
    ${un.WordAdd} $R0 ";" "-$INSTDIR\mel" $R0
    WriteRegStr HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "MAYA_SCRIPT_PATH" $R0 
  error3:
  
SectionEnd

Function Example5
    StrCpy $R0 $INSTDIR 
	${WordReplace} $R0 '\' '/' '+*' $R0
	${WordReplace} '$R9' 'CHEVELUREPATH' $R0 '+*' $R9
	Push $0
FunctionEnd

Function .onInit
  InitPluginsDir
  File /oname=$PLUGINSDIR\installoptions.ini "installoptions.ini"
FunctionEnd

Function INSTALLOPTIONS

  ;Display the InstallOptions dialog

  Push ${TEMP1}

    InstallOptions::dialog "$PLUGINSDIR\installoptions.ini"
    Pop ${TEMP1}
  
  Pop ${TEMP1}
	
FunctionEnd
