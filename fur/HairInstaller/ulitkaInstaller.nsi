; example2.nsi
;
; This script is based on example1.nsi, but it remember the directory, 
; has uninstall support and (optionally) installs start menu shortcuts.
;
; It will install example2.nsi into a directory that the user selects,

;--------------------------------
!include "WordFunc.nsh"
!include "TextFunc.nsh"

!insertmacro un.WordAdd


!ifdef MAYA70
	Name "ulitka hair70"
	OutFile "$%ULITKABIN%\hair70.exe"
!endif
!ifdef MAYA2008
	Name "ulitka hair2008"
	OutFile "$%ULITKABIN%\hair2008.exe"
!endif


; The default installation directory
InstallDir ""

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
!ifdef MAYA70
	InstallDirRegKey HKLM "Software\ulitkabin70" "Install_Dir"
!endif
!ifdef MAYA2008
	InstallDirRegKey HKLM "Software\ulitkabin2008" "Install_Dir"
!endif


;--------------------------------
; Pages

Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

; The stuff to install
Section "Ocllaris"

  SectionIn RO
  
	;-----------------------------------------
	; BIN
	SetOutPath $INSTDIR\bin
	  
	; �� ������� �� ������ ����
	File "$%ULITKABIN%\binrelease\HairCustomDeforms.dll"
	File "$%ULITKABIN%\binrelease\HairDSO.dll"
	File "$%ULITKABIN%\binrelease\HairDSOTest.exe"
	File "$%ULITKABIN%\binrelease\HairMath.dll"
	File "$%ULITKABIN%\binrelease\HairFftDeformer.dll"

	; ������
	File "$%ULITKABIN%\binrelease\HairRibGen.dll"
	File "$%ULITKABIN%\slim\HairRibGen.slim"
  
	!ifdef MAYA70
		File "$%ULITKABIN%\binrelease\SnHairShaper.dll" 
		File "$%ULITKABIN%\binrelease\SnMaya.mll" 	
		File "$%ULITKABIN%\binrelease\SnDelayMaya.dll" 
		File "$%ULITKABIN%\binrelease\SnMath.dll" 		
		
		File "$%ULITKABIN%\binrelease\HairMaya.mll"
	!endif
	!ifdef MAYA2008
		File "$%ULITKABIN%\binrelease85\SnHairShaper.dll" 
		File "$%ULITKABIN%\binrelease85\SnMaya.mll" 	
		File "$%ULITKABIN%\binrelease85\SnDelayMaya.dll" 
		File "$%ULITKABIN%\binrelease85\SnMath.dll" 		

		File "$%ULITKABIN%\binrelease85\HairMaya.mll"
	!endif
	
	;-----------------------------------------
	; MEL
	SetOutPath $INSTDIR\mel

	File "$%ULITKABIN%\mel\UlHairMenu.mel"
	
	;-----------------------------------------
	; SLIM
	SetOutPath $INSTDIR\slim
	File "$%ULITKABIN%\slim\ulHairSurfaceUVmanifold.slim"
	 
	;-----------------------------------------
	; DOC	 
	SetOutPath $INSTDIR\docs
	File "$%ULITKABIN%\docs\hair.htm"
	File /r "$%ULITKABIN%\docs\Hair"
	 
	;-----------------------------------------
	; Write the installation path into the registry
	!ifdef MAYA70
		WriteRegStr HKLM SOFTWARE\ulitkabin70 "Install_Dir" "$INSTDIR"
	!endif
	!ifdef MAYA2008
		WriteRegStr HKLM SOFTWARE\ulitkabin2008 "Install_Dir" "$INSTDIR"
	!endif
    
SectionEnd

;--------------------------------
; Uninstaller
Section "Uninstall"

SectionEnd
