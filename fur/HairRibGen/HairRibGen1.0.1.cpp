#include "stdafx.h"

#include "RIBGen.h"
#include "RIBContext.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#ifndef _WIN32
#include <unistd.h>
#endif
#include <math.h>
#include <sys/stat.h>

#if defined (_WIN32)
#define stat _stat
#define unlink _unlink
#endif

#include "HairMathVersion.h"

HANDLE hDllModule;
BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		{
			hDllModule = hModule;
			char filename[512]; 
			GetModuleFileName( (HMODULE)hModule, filename, 512); 
			fprintf(stderr, "%s build %d(%s) version %s\n", filename, hair_buildN, hair_buildDate, hair_versionstring);
			fflush(stderr);
			break;
		}
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
    return TRUE;
}

#include <string>

class HairRibGen : public RIBGen {
public:
    HairRibGen();
    virtual ~HairRibGen();

public:
    virtual void Bound(RIBContext * c, RtBound b);
    virtual int GenRIB(RIBContext * c);

    // Optionally cache internal motion-blur state at FrameOpen.
    //  This call allows certain types of RIB Generator to
    //  handle deforming motion blur.  Ostensibly the cached
    //  state will be used to compose the RIB representation
    //  of your deforming object. 
    virtual int CacheFrameOpenState( RIBContext * );

    virtual int SetArgs( RIBContext *c,	int n, RtToken tokens[], RtPointer vals[]);

 
protected:			

	std::string 	furObject;
	std::string 	fileName;

	// �������
	bool useMotionBlur;
	bool castShadows;
	
	/*/
	bool cleanupFile;
	bool lazyEvaluate;
	bool useMotionBlur;
	bool binaryData;
	bool lengthCorrect;
	int furInU, furInV;
	RtFloat furPlacementJitter;
	RtFloat ShadingRate;
	int furJitterSeed;
	bool castShadows;
	bool flipNormals;
	int furSegments;    
	RtFloat furKa, furKd, furKs, roughness1, roughness2, SPEC1, SPEC2, furBound;
	RtFloat furBaseClump, furTipClump, furClumpSize, furClumpRate, furRandClumpSize, furRandClumpMember;
	int furClumpDensity, typeMethod, areaDensity, densityType;

	bool useSpecularFade;
	RtFloat SpecularFadeRate, StartSpec, EndSpec;
	RtFloat SpecSizeFade, IllumWidth, VarFadeStart, VarFadeEnd;
	RtColor StaticAmbient;
	RtFloat ClumpDarkStrength, ClumpSpecLightening, ClumpDiffDarkening;

	/*/
    bool fileExists(const std::string & filename) const; 
	static void correctFileName(std::string& str);
};


/*---------------------------------------------------------*/
extern "C"
{
	__declspec(dllexport) RIBGen * __cdecl RIBGenCreate()
	{
		return new HairRibGen();
	}

	__declspec(dllexport) void __cdecl RIBGenDestroy(RIBGen * g)
	{
		delete (HairRibGen *) g;
	}
}

/*---------------------------------------------------------*/
HairRibGen::HairRibGen()
{
	useMotionBlur = true;
	castShadows = true;
}

//	������� ���������� �� ��������� ����������

HairRibGen::~HairRibGen() {
}
#ifndef RIBGEN_API
int
HairRibGen::SetArgs(int n, RtToken args[], RtPointer vals[])
#else
int
HairRibGen::SetArgs(RIBContext *, int n, RtToken args[], RtPointer vals[])
#endif
{
    int err = 0, i = 0;
    for (i = 0; i < n; i++)
	{
		if (!strcmp(args[i], "FileName") || !strcmp(args[i], "string FileName"))
		{
			fileName = *(static_cast<char **>(vals[i]));
			correctFileName(fileName);
		}
	/*/
		else if (!strcmp(args[i], "ShadingRate") || !strcmp(args[i], "float ShadingRate"))
		{
			ShadingRate = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "CleanupFile") || !strcmp(args[i], "float CleanupFile"))
		{
			cleanupFile = *(RtFloat *) (vals[i]) != 0;
		}
		else if (!strcmp(args[i], "LazyEvaluate") || !strcmp(args[i], "float LazyEvaluate"))
		{
			lazyEvaluate = *(RtFloat *) (vals[i]) != 0;
		}
		else if (!strcmp(args[i], "BinaryData") || !strcmp(args[i], "float BinaryData"))
		{
			binaryData = *(RtFloat *) (vals[i]) != 0;
		}
		else if (!strcmp(args[i], "MotionBlur") || !strcmp(args[i], "float MotionBlur"))
		{
			useMotionBlur = *(RtFloat *) (vals[i]) != 0;
		}
		else if (!strcmp(args[i], "LengthCorrect") || !strcmp(args[i], "float LengthCorrect"))
		{
			lengthCorrect = *(RtFloat *) (vals[i]) != 0;
		}
		else if (!strcmp(args[i], "DistrType") || !strcmp(args[i], "float DistrType"))
		{
			typeMethod = (int) *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "DensityType") || !strcmp(args[i], "float DensityType"))
		{
			densityType = (int) *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "AreaDensity") || !strcmp(args[i], "float AreaDensity"))
		{
			areaDensity = (int) *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "FurInU") || !strcmp(args[i], "float FurInU"))
		{
			furInU = (int) *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "FurInV") || !strcmp(args[i], "float FurInV"))
		{
			furInV = (int) *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "FurBound") || !strcmp(args[i], "float FurBound"))
		{
			furBound = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "FurPlacementJitter") || !strcmp(args[i], "float FurPlacementJitter"))
		{
			furPlacementJitter = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "FurJitterSeed") || !strcmp(args[i], "float FurJitterSeed"))
		{
			furJitterSeed = (int) *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "CastShadows") || !strcmp(args[i], "float CastShadows"))
		{
			castShadows = *(RtFloat *) (vals[i]) != 0;
		}
		else if (!strcmp(args[i], "FlipNormals") || !strcmp(args[i], "float FlipNormals"))
		{
			flipNormals = *(RtFloat *) (vals[i]) != 0;
		}
		else if (!strcmp(args[i], "FurKa") ||!strcmp(args[i], "float FurKa"))
		{
			furKa = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "FurKd") || !strcmp(args[i], "float FurKd"))
		{
			furKd = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "FurKs") || !strcmp(args[i], "float FurKs"))
		{
			furKs = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "UseSpecularFade") || !strcmp(args[i], "float UseSpecularFade"))
		{
			useSpecularFade = *(RtFloat *) (vals[i]) != 0;
		}
		else if (!strcmp(args[i], "SpecularFadeRate") || !strcmp(args[i], "float SpecularFadeRate"))
		{
			SpecularFadeRate = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "StartSpec") || !strcmp(args[i], "float StartSpec"))
		{
			StartSpec = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "EndSpec") || !strcmp(args[i], "float EndSpec"))
		{
			EndSpec = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "Roughness1") || !strcmp(args[i], "float Roughness1"))
		{
			roughness1 = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "SPEC1") || !strcmp(args[i], "float SPEC1"))
		{
			SPEC1 = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "Roughness2") || !strcmp(args[i], "float Roughness2"))
		{
			roughness2 = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "SPEC2") || !strcmp(args[i], "float SPEC2"))
		{
			SPEC2 = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "SpecSizeFade") || !strcmp(args[i], "float SpecSizeFade"))
		{
			SpecSizeFade = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "IllumWidth") || !strcmp(args[i], "float IllumWidth"))
		{
			IllumWidth = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "VarFadeStart") || !strcmp(args[i], "float VarFadeStart"))
		{
			VarFadeStart = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "VarFadeEnd") || !strcmp(args[i], "float VarFadeEnd"))
		{
			VarFadeEnd = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "ClumpDarkStrength") || !strcmp(args[i], "float ClumpDarkStrength"))
		{
			ClumpDarkStrength = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "ClumpSpecLightening") || !strcmp(args[i], "float ClumpSpecLightening"))
		{
			ClumpSpecLightening = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "ClumpDiffDarkening") || !strcmp(args[i], "float ClumpDiffDarkening"))
		{
			ClumpDiffDarkening = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "FurSegments")|| !strcmp(args[i], "float FurSegments"))
		{
			furSegments = (int) *(RtFloat *) (vals[i]);
			if (furSegments <= 0) furSegments = 1;
		}
		else if (!strcmp(args[i], "StaticAmbient") || !strcmp(args[i], "color StaticAmbient"))
		{
			StaticAmbient[0] = ((RtFloat *) (vals[i]))[0];
			StaticAmbient[1] = ((RtFloat *) (vals[i]))[1];
			StaticAmbient[2] = ((RtFloat *) (vals[i]))[2];
		}
		else if (!strcmp(args[i], "FurClumpDensity") ||!strcmp(args[i], "float FurClumpDensity"))
		{
			furClumpDensity = (int) *(RtFloat *) (vals[i]);
		} 
		else if (!strcmp(args[i], "FurRandClumpSize") ||!strcmp(args[i], "float FurRandClumpSize"))
		{
			furRandClumpSize = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "FurRandClumpMember") ||!strcmp(args[i], "float FurRandClumpMember"))
		{
			furRandClumpMember = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "FurBaseClump") ||!strcmp(args[i], "float FurBaseClump"))
		{
			furBaseClump = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "FurTipClump") ||!strcmp(args[i], "float FurTipClump"))
		{
			furTipClump = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "FurClumpSize") ||!strcmp(args[i], "float FurClumpSize"))
		{
			furClumpSize = *(RtFloat *) (vals[i]);
		}
		else if (!strcmp(args[i], "FurClumpRate") ||!strcmp(args[i], "float FurClumpRate"))
		{
			furClumpRate = *(RtFloat *) (vals[i]);
		}
	/*/
		else
		{
			err++;
		}
    }
    return err;
}

extern "C" {
    RtVoid dl_FurProceduralFree(RtPointer data)
	{
		RtString* sData = (RtString*) data;
		free(sData[0]);
		free(sData[1]);
		free(sData);
    }
}

typedef bool (*HAIREXPORT)(
	const char* argsl0,
	const char* filename,
	const char* pass,
	const float* camera, 
	int useMotionBlur,
	double frametime, 
	double shutterOpen, 
	double shutterClose, 
	double shutterOpenSec, 
	double shutterCloseSec,
	float outbox[6]
	);

int HairRibGen::GenRIB(RIBContext * c)
{
    furObject = c->GetObjName();  
   	int frame = c->GetFrame(); // �������� �������� �����
    std::string errorString;

    if (furObject.empty())
	{
		return 0;
    }

char filename[512]; GetModuleFileName( (HMODULE)hDllModule, filename, 512); 
//c->ReportError(RIBContext::reInfo, "\nStart RibExport %s", filename);

//c->ReportError(RIBContext::reInfo, "\nStart RibExport HairRibGen");

    RIBContext::RenderingPass p;
    RtBoolean decl;

    // ������ ���� �������� ������ � ��������� ��� motion blur'�    
    useMotionBlur = c->GetMotionBlur(); 
    RtFloat shutterOpen, shutterClose;
    RtFloat shutterAngle, fps;
    RtBoolean subframeMotion;
    RtBoolean blurCamera;
    RIBContext::ShutterTiming shutterTiming;
    RIBContext::ShutterConfig shutterConfig;
    
    c->GetShutter(&shutterOpen, &shutterClose);

	// shutterOpen==shutterClose
	if( shutterOpen==shutterClose)
		useMotionBlur = 0;

    c->GetMotionInfo(&shutterAngle, &fps, &subframeMotion, &blurCamera, &shutterTiming, &shutterConfig);

c->ReportError(RIBContext::reInfo, "\nhair ribgen: useMotionBlur=%d shutterOpen=%f shutterClose = %f", 
			   useMotionBlur, shutterOpen, shutterClose );

c->ReportError(RIBContext::reInfo, "\nhair ribgen: shutterAngle=%f fps=%f subframeMotion = %d, blurCamera = %d shutterTiming=%d shutterConfig=%d", 
			   shutterAngle, fps, subframeMotion, blurCamera, shutterTiming, shutterConfig);

	RtFloat shutterOpenFrame = shutterOpen*fps;
	RtFloat shutterCloseFrame = shutterClose*fps;

	shutterOpen -= 0.0001;

	// ��� ��� �� ���?
	if( !subframeMotion)
	{
		shutterClose = (float)(shutterOpen + (1.0 / fps));
		shutterCloseFrame = shutterOpenFrame + 1;
	}

    c->GetRenderingPass(&p, &decl);
    if (decl)
	{
		return 0;
    }
	else if (p == RIBContext::rpShadow)
	{
		if (!castShadows) return 0;		// �� ������� ������������ ����, ���� �������� castShadow �� off
    }

    char cmd[512];
    
    const RIBContextResult* result;


	std::string pass = "";
	switch(p)
	{
		case RIBContext::rpTraverseOnly:pass = "traverseonly"; break;
		case RIBContext::rpFinal:		pass = "final";		break;
		case RIBContext::rpShadow:		pass = "shadow";	break;
		case RIBContext::rpReflection:	pass = "reflection"; break;
		case RIBContext::rpEnvironment: pass = "environment"; break;
		case RIBContext::rpDepth:		pass = "depth";		break;
		case RIBContext::rpReference:	pass = "reference"; break;
		case RIBContext::rpArchive:		pass = "archive";	break;
		case RIBContext::rpPhoton:		pass = "photon";	break;
	}

    if (fileName.empty())
	{
		c->ReportError(RIBContext::reError, "HairRibGen: must specify a filename to output to file.");
		return -1;
    }

	//	��������� ��������� � ��� ����� ���� ��� ����������
	if( p!=RIBContext::rpFinal)
		fileName += "."+pass;

	float time = frame;
	float bound[6];

	RtMatrix camera;
	c->GetCamBasis( camera, 1);
	const float* mview = &camera[0][0];

#ifndef SAS
	char* dllname = "HairMaya.mll";
#else
	char* dllname = "chevelureMaya.mll";
#endif
	char* procname = "HairExport";
	HMODULE module = LoadLibrary(dllname);
	if( module)
	{
		FARPROC proc = GetProcAddress(module, procname);
		if( proc)
		{
			HAIREXPORT exportproc = (HAIREXPORT)proc;
			bool res = (*exportproc)( 
				furObject.c_str(), 
				fileName.c_str(), 
				pass.c_str(), 
				mview, 
				useMotionBlur?1:0, 
				time, shutterOpen, shutterClose, shutterOpenFrame, shutterCloseFrame,
				bound);
			if(!res)
			{
				FreeLibrary(module);
				c->ReportError(RIBContext::reInfo, "\nError then execute HairExport");
				return -1;
			}
		}
		else
		{
			FreeLibrary(module);
			c->ReportError(RIBContext::reInfo, "\nCant Find HairExport");
			return -1;
		}
		FreeLibrary(module);
	}
	else
	{
		c->ReportError(RIBContext::reInfo, "\nCant Load HairMaya.mll");
		return -1;
	}

/*/
#ifndef SAS
	std::string commandName = "HairExport";
#else
	std::string commandName = "chevelureExport";
#endif

	float time = frame;
    sprintf(cmd, "%s \"%s\" \"%s\" \"%s\" %d %f %f %f %f %f", commandName.c_str(), furObject.c_str(), fileName.c_str(), pass.c_str(), useMotionBlur?1:0, time, shutterOpen, shutterClose, shutterOpen*fps, shutterClose*fps);
    result = c->ExecuteHostCmd(cmd, errorString);
    if (result->ResultType() != RIBContextResult::kFloatArray)
	{
		c->ReportError(RIBContext::reError, "\nHairRibGenX: unexpected return type on dl_hairExport (kFloatArray parameter) (%s)", errorString.c_str());
		c->ReportError(RIBContext::reError, "\nrun:\n%s", cmd);
		return -1;
	}
//c->ReportError(RIBContext::reError, "\nrun:\n%s", cmd);
    std::vector<RtFloat> box;
	box.resize(6);
	int resu = result->GetResult(box, &errorString);
//c->ReportError(RIBContext::reInfo, "\nHair Ribgen : resu=%d; box.size()=%d", resu, box.size());
    if (box.size()!=6)
	{
		c->ReportError(RIBContext::reError, "\nHairExport return %d", box.size());
		c->ReportError(RIBContext::reError, "\nrun:\n%s", cmd);
		return -1;
	}
	if(box[0]==0.f && box[1]==0.f && box[2]==0.f && box[3]==0.f && box[4]==0.f && box[5]==0.f)
	{
		c->ReportError(RIBContext::reError, "\nFAILED");
		return -1;
	}
/*/


// ��������� ������ � ����������� ������� ��� ������ � ���  
    RtString *data = static_cast<RtString*>(malloc(sizeof(RtString) * 2));
#if defined (_WIN32)
#ifndef SAS
    data[0] = strdup("HairDSO.dll");
#else
    data[0] = strdup("chevelureDSO.dll");
#endif
#else
    data[0] = strdup("HairDSO.so");
#endif
    data[1] = static_cast<char*>(malloc(sizeof(char)*4096));
    sprintf(data[1], "%s", fileName.c_str());

/*/
	float* min = &box[0];
	float* max = &box[3];
/*/

	/*/
	//////////////////////////////////////
	//	�������� Bound ���������� �� ���� ����� �������� ���������
    sprintf(cmd, "getAttr %s.boundingBoxMin", furObject.c_str());
    result = c->ExecuteHostCmd(cmd, errorString);    
    if (result->ResultType() != RIBContextResult::kFloatArray)
	{
		c->ReportError(RIBContext::reError, "HairRibGen: unexpected return type on getAttr boundingBoxMin (%s)", errorString.c_str());
		return -1;
    }
    vector<float> min;  
    result->GetResult(min, &errorString);

    sprintf(cmd, "getAttr %s.boundingBoxMax", furObject.c_str());
    result = c->ExecuteHostCmd(cmd, errorString);    
    if (result->ResultType() != RIBContextResult::kFloatArray)
	{
		c->ReportError(RIBContext::reError, "HairRibGen: unexpected return type on getAttr boundingBoxMax (%s)", errorString.c_str());
		return -1;
    }
    vector<float> max;
    result->GetResult(max, &errorString);

	float furBound=0;
    RtBound bound = {
		min[0] - furBound, 
		max[0] + furBound, 
		min[1] - furBound, 
		max[1] + furBound, 
		min[2] - furBound, 
		max[2] + furBound};
	/*/


//	����� ����������� ����� � ���
#if RIBCONTEXT_API == 2
    c->Procedural(data, bound, c->ProcDynamicLoad, dl_FurProceduralFree);
#elif RIBCONTEXT_API > 2
    c->Procedural(data, bound, c->GetProcSubdivFunc(RIBContext::kDynamicLoad), dl_FurProceduralFree);
#else
    Error - HairRibGen requires a more modern implementation of the RIB Context interface.
#endif


	c->ReportError( RIBContext::reInfo, "\nHair Ribgen for %s successful: pass=%s frame=%d shutterOpen = %f, shutterClose = %f", furObject.c_str(), pass.c_str(), frame, shutterOpen, shutterClose);

    return 1;
}

void HairRibGen::Bound(RIBContext * c, RtBound)
{
//	c->ReportError(RIBContext::reInfo, "HairRibGen: Bound");
}

int HairRibGen::CacheFrameOpenState( RIBContext * c)
{
//	c->ReportError(RIBContext::reInfo, "HairRibGen: CacheFrameOpenState");
	return 0;
}

bool HairRibGen::fileExists(const std::string & filename) const
{
    struct stat sbuf;
    return (stat(filename.c_str(), &sbuf) == 0);
}

void HairRibGen::correctFileName(std::string& str)
{
	for( unsigned int i=0; i<str.size(); i++)
	{
		char& c = str[i];
		if(i==1 && c==':') continue;
		if( c=='|' || 
			c=='?' || 
			c=='*' || 
			c=='"' || 
			c==':' || 
			c=='<' || 
			c=='>' //|| 
			)
			c = '#';
	}
}
