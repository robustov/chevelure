# Microsoft Developer Studio Project File - Name="HairRibGen" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=HairRibGen - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "HairRibGen.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "HairRibGen.mak" CFG="HairRibGen - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "HairRibGen - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "HairRibGen - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "HairRibGen - Win32 Chevelure" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Ulitka/FUR", GAAAAAAA"
# PROP Scc_LocalPath ".."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "HairRibGen - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\..\obj\Release\dlUltraFur"
# PROP Intermediate_Dir "..\..\..\obj\Release\dlUltraFur"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "DL_ULTRAFUR_EXPORTS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GR /GX /O2 /I "$(SDK)/RAT70/include" /I "$(SDK)/RAT70/STLport-4.5.3" /I "$(SDK)/RAT70/STLport-4.5.3/stlport" /I "$(SDK)/PRMAN12.5/include" /D "DNT_PLUGIN" /D "DNDEBUG" /D "DSGI_STL" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "DL_ULTRAFUR_EXPORTS" /U "NT_PLUGIN" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x419 /d "NDEBUG"
# ADD RSC /l 0x419 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386 /out:"$(ULITKABIN)\binrelease/HairRibGen.dll" /libpath:"$(SDK)/PRMAN12.5/lib" /EXPORT:RIBGenCreate /EXPORT:RIBGenDestroy
# SUBTRACT LINK32 /pdb:none
# Begin Special Build Tool
SOURCE="$(InputPath)"
PostBuild_Cmds=copy HairRibGen.slim "$(ULITKABIN)\bin/HairRibGen.slim"
# End Special Build Tool

!ELSEIF  "$(CFG)" == "HairRibGen - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\..\obj\Debug\dlUltraFur"
# PROP Intermediate_Dir "..\..\..\obj\Debug\dlUltraFur"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "DL_ULTRAFUR_EXPORTS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MD /W3 /Gm /GX /ZI /Od /I "$(SDK)/RAT70/include" /I "$(SDK)/RAT70/STLport-4.5.3" /I "$(SDK)/RAT70/STLport-4.5.3/stlport" /I "$(SDK)/PRMAN12.5/include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "DLL" /D "DL_ULTRAFUR_EXPORTS" /U "NT_PLUGIN" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x419 /d "_DEBUG"
# ADD RSC /l 0x419 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib prman.lib /nologo /dll /incremental:no /machine:I386 /out:"$(ULITKABIN)\bindebug/HairRibGen.dll" /pdbtype:sept /libpath:"$(SDK)/PRMAN12.5/lib" /EXPORT:RIBGenCreate /EXPORT:RIBGenDestroy
# SUBTRACT LINK32 /pdb:none /debug
# Begin Special Build Tool
SOURCE="$(InputPath)"
PostBuild_Cmds=copy HairRibGen.slim "$(ULITKABIN)\bin/HairRibGen.slim"
# End Special Build Tool

!ELSEIF  "$(CFG)" == "HairRibGen - Win32 Chevelure"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "HairRibGen___Win32_Chevelure"
# PROP BASE Intermediate_Dir "HairRibGen___Win32_Chevelure"
# PROP BASE Ignore_Export_Lib 1
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\..\obj\Chevelure\RibGen"
# PROP Intermediate_Dir "..\..\..\obj\Chevelure\RibGen"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GR /GX /O2 /I "$(SDK)/RAT6/include" /I "$(SDK)/RAT6/STLport-4.5.3" /I "$(SDK)/RAT6/STLport-4.5.3/stlport" /I "$(SDK)/PRMAN6/include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "DL_ULTRAFUR_EXPORTS" /U "NT_PLUGIN" /FR /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GR /GX /O2 /I "$(SDK)/RAT70/include" /I "$(SDK)/RAT70/STLport-4.5.3" /I "$(SDK)/RAT70/STLport-4.5.3/stlport" /I "$(SDK)/PRMAN12.5/include" /D "SAS" /D "DNDEBUG" /D "DSGI_STL" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "DL_ULTRAFUR_EXPORTS" /U "NT_PLUGIN" /FR /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x419 /d "NDEBUG"
# ADD RSC /l 0x419 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386 /out:"$(ULITKABIN)\bin/HairRibGen.dll" /libpath:"$(SDK)/PRMAN6/lib" /EXPORT:RIBGenCreate /EXPORT:RIBGenDestroy
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386 /out:"$(SASBIN)\bin/chevelureRibGen.dll" /libpath:"$(SDK)/PRMAN12.5/lib" /EXPORT:RIBGenCreate /EXPORT:RIBGenDestroy
# SUBTRACT LINK32 /pdb:none
# Begin Special Build Tool
SOURCE="$(InputPath)"
PostBuild_Cmds=copy chevelureRibGen.slim "$(SASBIN)\bin/chevelureRibGen.slim"	copy HairShader.slim "$(SASBIN)\bin/HairShader.slim"	copy slim.ini "$(SASBIN)\bin/slim.ini"
# End Special Build Tool

!ENDIF 

# Begin Target

# Name "HairRibGen - Win32 Release"
# Name "HairRibGen - Win32 Debug"
# Name "HairRibGen - Win32 Chevelure"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\chevelureRibGen.slim
# End Source File
# Begin Source File

SOURCE=.\HairRibGen.cpp
# End Source File
# Begin Source File

SOURCE=.\HairRibGen.slim
# End Source File
# Begin Source File

SOURCE=.\HairShader.slim
# End Source File
# Begin Source File

SOURCE=.\slim.ini
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
