import os
import platform
import SCons
from Make.EDBuildEnvironment import EDBuildEnvironment

class ChevelureBuildEnvironment(EDBuildEnvironment):
    
    PLATFORMS_ALIASES = {'Windows': 'win', 'Darwin': 'mac', 'Linux': 'linux'}

    def __init__(self, arguments):

        self.mProgram = self.edProgram
        self.mSharedLibrary = self.edSharedLibrary
        self.mStaticLibrary = self.edStaticLibrary
        self.mLoadableModule = self.edLoadableModule
        
        EDBuildEnvironment.__init__(self, arguments)
        
        self['TOP_DIR']         = os.getcwd() + '/' # we need this this slash because too many people ignore existing of os.path.join()
        self['BIN_INCLUDE_DIR'] = os.path.join(self['TOP_DIR'], 'bin', 'include')
        self['BIN_MEL_DIR'] = os.path.join(self['TOP_DIR'], 'bin', 'mel')
        self['BIN_PYTHON_DIR'] = os.path.join(self['TOP_DIR'], 'bin', 'python')
        self['BIN_SLIM_DIR'] = os.path.join(self['TOP_DIR'], 'bin', 'slim')
        self['BIN_BIN_DIR'] = os.path.join(self['TOP_DIR'], 'bin', 'bin')
        self.init()
        
        self['ULITKASDK'] = os.environ['ULITKASDK'].replace('\\', '/')
        self['ULITKASDK_INC'] = os.path.join(self['ULITKASDK'], 'inc')
        
        self['MITKASDK'] = os.environ['MITKASDK'].replace('\\', '/')
        self['MITKASDK_INC'] = os.path.join(self['MITKASDK'], 'inc')
        
        self['OCELLARIS'] = os.environ['OCELLARIS'].replace('\\', '/')
        self['OCELLARIS_INC'] = os.path.join(self['OCELLARIS'], 'inc')

        self['SUPERNOVA'] = os.environ['SUPERNOVA'].replace('\\', '/')
        self['SUPERNOVA_INC'] = os.path.join(self['SUPERNOVA'], 'Inc')

        self['SDK'] = os.environ['SDK'].replace('\\', '/')
        
        self['LINKCOM'] = [self['LINKCOM'], 'mt.exe -nologo -manifest ${TARGET}.manifest -outputresource:$TARGET;1']
        self['SHLINKCOM'] = [self['SHLINKCOM'], 'mt.exe -nologo -manifest ${TARGET}.manifest -outputresource:$TARGET;2']

    def init(self):
        self['BUILD_DIR']       = os.path.join(self['TOP_DIR'], self.get_build_dir())
        self['INSTALL_BIN_DIR'] = os.path.join(self['TOP_DIR'], self.get_install_bin_dir())
        self['INSTALL_LIB_DIR'] = os.path.join(self['TOP_DIR'], self.get_install_lib_dir())
        
    # Replace builder methods with corresponding project generation methods
    def project_generation_mode(self):
        self.msvsProjects = {}

        self.edBuild = self.msvsBuild
        self.mProgram = self.msvsProgram
        self.mSharedLibrary = self.msvsSharedLibrary
        self.mStaticLibrary = self.msvsStaticLibrary
        self.mLoadableModule = self.msvsLoadableModule

    def edBuild(self, target_type, builder, **params):
        params['LIBPATH'] = [self['INSTALL_LIB_DIR']]
        return super(ChevelureBuildEnvironment, self).edBuild(target_type, builder, **params)

    # Returns build directory path relative to top dir. Override if needed
    def get_build_dir(self):
        return os.path.join('.build', self['ARCH'] + '.' + self.get_configuration_name())

    def get_configuration_name(self):
        if not self.environ.has_key('MAYA_VERSION') or self.environ['MAYA_VERSION'] == '':
            self.environ['MAYA_VERSION'] = ''
            mayasuffix = ''
        else:
            mayasuffix = '.maya' + self.environ['MAYA_VERSION']

        if self['TOOL'] == 'msvc':
            return 'vc' + self['MSVC_VERSION'].replace('.', '') + '.' + self['MODE'] + mayasuffix

        return self['TOOL'] + '.' + platform.system() + '.' + self['MODE'] + mayasuffix

    # Returns path to bin directory relative to top dir. Override if needed     
    def get_install_bin_dir(self):
        return os.path.join('bin', self['ARCH'] + '.' + self.get_configuration_name())
        
    # Returns path to lib directory relative to top dir. Override if needed
    def get_install_lib_dir(self):
        return os.path.join('lib', self['ARCH'] + '.' + self.get_configuration_name())
        
    # Return current suffix for sdk paths:
    #  - for x86_64 returns 'x64'
    #  - for x86 returns ''
    def get_sdk_suffix(self):
        if self['ARCH'] == 'x86_64':
            return 'x64'
        elif self['ARCH'] == 'x86':
            return ''
        raise SCons.Errors.UserError('unknown ARCH %s, specify x86 or x86_64' % (self['ARCH']))
    

env1 = ChevelureBuildEnvironment(ARGUMENTS)

# separate bin dir for each mayasdk
for mayasdk in env1.getUserVariable('MAYASDK'):
    env = env1.Clone()
    env1.environ['MAYA_VERSION'] = mayasdk
    env1.init()
    env.environ['MAYA_VERSION'] = mayasdk
    env.init()
    
    dir = env['BUILD_DIR']
    Export('env')
    env.SConscript('SConscript', variant_dir = dir, duplicate = 0)

    # installer
    nsis_cmd = '\"%NSIS%\\makensis.exe\"'
    nsis_cmd += ' /DBINDIR={1}'
    nsis_cmd += ' /DMAYA{0}'
    nsis_cmd += ' /DCHEVELUREFULL'
    nsis_cmd += ' /DW64'
    nsis_cmd += ' \"fur/HairInstaller/chevelure.nsi\"'
    nsis_cmd = nsis_cmd.format(mayasdk, os.path.abspath(env.get_install_bin_dir()))
    installer = env.Command('installer_target'+mayasdk, None, nsis_cmd, ENV=os.environ)
    env.AlwaysBuild(installer)
    env.Alias('installer', installer)
    env.Depends(installer, 'install')

env1.generateProjects()

if platform.system() == 'Windows':
    env1.Default('install') 

def install_to_env_dir(install_dict):
    for (k,v) in install_dict:
        copy = lambda(x): env1.Depends('install', env1.Install(env1[k], v))
        if isinstance(v, (list, tuple)):
            for f in v:
                copy(f)
        else:
            copy(v)

install_to_env_dir({
#    ('BIN_MEL_DIR', tuple(env1.RGlob('fur/HairMaya/*.mel'))),
    ('BIN_INCLUDE_DIR', 'ocellaris/src/shaders/bake3dNormal.h'),
    ('BIN_INCLUDE_DIR', 'ocellaris/src/shaders/LambertOcs.h'),
    ('BIN_INCLUDE_DIR', 'ocellaris/src/shaders/noiseOcs.h'),
    ('BIN_INCLUDE_DIR', 'ocellaris/src/shaders/OcclusionOcs.h'),
    ('BIN_INCLUDE_DIR', 'ocellaris/src/shaders/PhongOcs.h'),
    ('BIN_INCLUDE_DIR', 'ocellaris/src/shaders/RampOcs.h'),
    ('BIN_INCLUDE_DIR', 'ocellaris/src/shaders/SpotLigthOcs.h'),
    ('BIN_INCLUDE_DIR', 'ocellaris/src/shaders/ul_filterwidth.h'),
    ('BIN_INCLUDE_DIR', 'ocellaris/src/shaders/ul_noises.h'),
    ('BIN_INCLUDE_DIR', 'ocellaris/src/shaders/ul_patterns.h'),
    ('BIN_INCLUDE_DIR', 'ocellaris/src/shaders/ul_pxslRayUtil.h'),
    ('BIN_INCLUDE_DIR', 'ocellaris/src/shaders/ul_pxslRemap.h'),
    ('BIN_INCLUDE_DIR', 'ocellaris/src/shaders/ul_pxslUtil.h'),
    ('BIN_INCLUDE_DIR', 'ocellaris/src/ocellarisExport/utilsOcs.h'),
    ('BIN_INCLUDE_DIR', 'fur/HairMaya/hairShaderUtils.h'),

    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/AEocCustomPassTemplate.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/AEocellarisShapeHook.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/AEocFileTemplate.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/AEocGeneratorProxyTemplate.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/AEocGeneratorSetTemplate.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/AEocInstance2CreatorTemplate.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/AEocInstance2ShapeTemplate.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/AEocInstanceTemplate.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/AEocJointVisTemplate.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/AEocLightTemplate.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/AEocOcclusionPassTemplate.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/AEocParticleAttributeTemplate.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/AEocParticleInstancerTemplate.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/AEocRenderPassTemplate.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/AEocRootTransformTemplate.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/AEocRSLshaderTemplate.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/AEocSequenceFileTemplate.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/AEocShaderParameterTemplate.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/AEocSurfaceShaderTemplate.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/AEshapeTemplate.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/ocellarisMayaMenu.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/ocellarisPassesMenu.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/ocellarisRFMprocedure.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/ocellarisShadersMenu.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/ocTranslatorOptions.mel'),
    ('BIN_MEL_DIR', 'ocellaris/src/ocellarisMaya/sloTranslatorOptions.mel'),
    ('BIN_MEL_DIR', 'fur/HairMaya/AEHairArrayMapperTemplate.mel'),
    ('BIN_MEL_DIR', 'fur/HairMaya/chevelureMenu.mel'),
    ('BIN_MEL_DIR', 'fur/HairMaya/chevelureRFMprocedure.mel'),
    
    ('BIN_PYTHON_DIR', 'fur/HairMaya/furRFMExportHelper.py'),
    ('BIN_PYTHON_DIR', 'ocellaris/src/ocellarisMaya/ocsCommands.py'),
    ('BIN_PYTHON_DIR', 'ocellaris/src/ocellarisMaya/ocsExport.py'),
    ('BIN_PYTHON_DIR', 'ocellaris/src/ocellarisMaya/ocsRender.py'),

    ('BIN_SLIM_DIR', 'fur/HairRibGen/slim.ini'),
    ('BIN_SLIM_DIR', 'fur/HairRibGen/chevelureRibGen.slim'),
    ('BIN_SLIM_DIR', 'fur/HairRibGen/HairRibGen.slim'),
    ('BIN_SLIM_DIR', 'fur/HairMaya/chevelureSurfaceUVmanifold.slim'),
    ('BIN_SLIM_DIR', 'fur/HairMaya/ulHairSurfaceUVmanifold.slim'),

    ('BIN_BIN_DIR', 'ocellaris\src\OcellarisRibGen\OcellarisRibGen.slim'),
    ('BIN_BIN_DIR', 'fur/Changes.txt'),
    ('BIN_BIN_DIR', 'fur/Chevelure_installation.txt'),
    ('BIN_BIN_DIR', 'fur/HairLicenseServer/ChevelureLicenseServer_installation.txt'),
})

#    ('BIN_INCLUDE_DIR', ''),
#    ('', ''),
#    ('', ''),
#    (),
