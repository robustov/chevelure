/* $Id: //depot/branches/rmanprod/rman-17.0/cmd/prman/setup/shield.sl#1 $  (Pixar - RenderMan Division)  $Date: 2012/09/17 $ */
/*
** Copyright (c) 1999 PIXAR.  All rights reserved.  This program or
** documentation contains proprietary confidential information and trade
** secrets of PIXAR.  Reverse engineering of object code is prohibited.
** Use of copyright notice is precautionary and does not imply
** publication.
**
**                      RESTRICTED RIGHTS NOTICE
**
** Use, duplication, or disclosure by the Government is subject to the
** following restrictions:  For civilian agencies, subparagraphs (a) through
** (d) of the Commercial Computer Software--Restricted Rights clause at
** 52.227-19 of the FAR; and, for units of the Department of Defense, DoD
** Supplement to the FAR, clause 52.227-7013 (c)(1)(ii), Rights in
** Technical Data and Computer Software.
**
** Pixar
** 1001 West Cutting Blvd.
** Richmond, CA  94804
*/
surface
shield ( float a=1, d=1, metal_s=1, plastic_s=1, roughness=.1;
	 color red = color (.8,0,0), gold = color (.8,.6,0); )
{
	varying normal Nf;
	varying vector V;
	varying float mask, letter;
	varying color Fcolor, Mcolor, Bcolor;
	varying color amb, diff, spec;

	mask = texture ("./logo_t" [0]);
	letter = texture ("./logo_t" [1]);

	Nf = faceforward( normalize(N), I);
	V = normalize(-I);

	amb = a*ambient();
	diff = d*diffuse(Nf);
	spec = specular(Nf, V, roughness);

	Fcolor = red * (amb + diff) + plastic_s*spec;	/* plastic */
	Mcolor = gold * (amb + metal_s * spec);		/* metal */
	Bcolor = Cs * (amb + diff);			/* matte */

	Ci = mix(Mcolor, Fcolor, letter);
	Ci = mix(Bcolor, Ci, mask);
	Oi = 1.0;
}
