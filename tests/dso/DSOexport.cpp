#include "stdafx.h"
#include <ri.h>
#include <string>

/**************************************************/
#ifdef WIN32
#define export __declspec(dllexport)
#else
#define export
#endif

extern "C" 
{
	export RtVoid Subdivide2(RtContextHandle, RtFloat detail, RtInt n, RtToken toks[], RtPointer vals[]);
	export RtVoid Bound(RtInt n, RtToken const toks[], RtPointer const vals[], RtBound result[2]);
}
RtVoid SubSubdivide(RtContextHandle ctx, RtFloat detail, RtInt n, RtToken toks[], RtPointer vals[]);

RtVoid Subdivide2(RtContextHandle ctx, RtFloat detail, RtInt n, RtToken toks[], RtPointer vals[])
{
	fprintf(stderr, "%d: Subdivide2\n", GetCurrentThreadId());
	for(int i=0; i<n; i++)
		fprintf(stderr, "    %s:%s\n", toks[i], *(char**)vals[i]);
	fprintf(stderr, "\n");

	for(int x=-10; x<10; x++)
	{
		for(int y=-10; y<10; y++)
		{
			for(int z=-10; z<10; z++)
			{
				RtBound result;
				result[0] = x;
				result[1] = x+1;
				result[2] = y;
				result[3] = y+1;
				result[4] = z;
				result[5] = z+1;
				RtToken names[] = {"bound", RI_NULL};
				RtPointer values[] = {result, RI_NULL};

				RiProcedural2(&SubSubdivide, RiSimpleBound, "float[6] bound", result, RI_NULL);
			}
		}
	}
}

RtVoid Bound(RtInt n, RtToken const toks[], RtPointer const vals[], RtBound result[2])
{
	fprintf(stderr, "Bound\n");
	for(int i=0; i<n; i++)
		fprintf(stderr, "    %s:%s\n", toks[i], *(char**)vals[i]);
	fprintf(stderr, "\n");

	result[0][0] = -10;
	result[0][1] =  10;
	result[0][2] = -10;
	result[0][3] =  10;
	result[0][4] = -10;
	result[0][5] =  10;

	result[1][0] = -10;
	result[1][1] =  10;
	result[1][2] = -10;
	result[1][3] =  10;
	result[1][4] = -10;
	result[1][5] =  10;
}

RtVoid SubSubdivide(RtContextHandle ctx, RtFloat detail, RtInt n, RtToken toks[], RtPointer vals[])
{
	fprintf(stderr, "    %d: Subdivide2\n", GetCurrentThreadId());
}

