#!/usr/bin/python
# encoding: utf-8

import shutil
import glob
import os

targets = [
	'.build',
	'bin/x86*',
	'bin/x86_64*',
	'bin/*.exe',
	'bin/include',
	'bin/mel',
	'bin/python',
	'bin/bin',
	'bin/slim',
	'lib',
	'Make/.sconsign*'
]

if __name__ == "__main__":
	# remove files

	for i in targets:
		for j in glob.glob(i):
			if os.path.isdir(j):
				shutil.rmtree(path = j, ignore_errors = True)
			else:
				os.remove(j)
			print 'Removed ', j
