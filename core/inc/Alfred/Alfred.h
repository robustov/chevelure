#pragma once

#include <list>
#include <string>
#include <stdio.h>
#include <stdarg.h>
#include <map>

// 
// ������ ��� �������� � ������ ����� ����� �������
// 
namespace Alfred
{

	// Cmd {launch_expr} [options]
	struct Cmd
	{
		bool bRemote;			// Cmd ��� RemoteCmd
		std::string launch_expr;
		std::string msg;
		std::string service;
		int atmost;
		int atleast;
		std::string tags;
		std::string expand;
	public:
		std::string ToString(int tabs);
		Cmd();
		Cmd(const char* cmd, bool bRemote);
	};
	struct Assign
	{
		std::string name;
		std::string text;
	public:
		Assign();
		Assign(const char* name, const char* text);
		std::string ToString(int tabs);
	};
	// ������ Cmd
	inline Cmd LocalCmd(const char* cmdstr);
	// ������� RemoteCmd
	inline Cmd RemoteCmd(const char* cmd, ...);
	inline Cmd RemoteCmd(const std::string& cmd);
	// ������� �������� ������� ����
	inline Cmd CmdDeleteFile(const char* filename);


	// Iterate varname -from n -to m -by i -template {script} [options]
	struct Iterate
	{
	};

	// Assign  varname {value_string}


	// Task {title} [options]
	struct Task
	{
		bool bInstance;
		std::string title;
		std::list<Task> subtasks;
//		std::list<std::string> instance;

		std::list<Cmd> cmds;
		std::list<Assign> init;

		std::list<Cmd> cleanup;			// Cmd {Alfred} -msg {File delete \"D:/public/New_Project/rib/untitled.0002.rib\"}
		std::string chaser;				// ???
		std::string preview;			// sho \"D:/public/New_Project/rmanpix/untitled.0002.tif\"

		Task(const char* title, ...);

		void AddCmd(Cmd& cmd);
		void AddTask(Task& task);
		void AddInstance(Task& task);
	public:
		std::string ToString(int tabs);
	};

	Alfred::Task Instance(const char* name);
	Alfred::Task Instance(const Alfred::Task& src);

	// �������: ������ (���������� ������)
	// Job [options]
	struct Job
	{
		std::string title;		// -title {untitled (mtor job)}
		std::string comment;	// -comment {#Created for mitka by mtor 6.5.1, RAT 6.5.1 (Sep  8 2005 11:54:01)}
		std::string envkey;
		std::string dirmaps;
		std::string service;
		std::string tags;
		int atleast;
		int atmost;

		// ???
		std::string whendone;
		std::string whenerror;
		std::string crews;
		std::string metadata;
		int pbias;

		std::list<Assign> init;
		std::list<Task> subtasks;
		std::list<Cmd> cleanup;

		std::map<std::string, std::string> additionaloptions;

		Job(const char* title="");

		void AddSubtask(Task& task);
		void AddCleanup(Cmd& cmd);
	public:
		void clear();
		std::string ToString();
	};

	// ����� ��� ����� ������
	std::string unequalJobName(const char* dir);

	// �������� Alfred ����
	bool saveAlfredFile(const char* filename, Job& job);
	// ��������� Alfred ����
	bool startAlfred(const char* filename, bool bPaused);

}

#include "inl/Alfred.hpp"
//std::string out;