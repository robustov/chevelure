#pragma once

#ifndef __QTMOVIEFILE_H
#define __QTMOVIEFILE_H

#include <qtml.h>
#include <movies.h>
#include <tchar.h>
#include <Gestalt.h>
#include <wtypes.h>
#include <FixMath.h>
#include <Script.h>
#include <stdlib.h>
#include <TextUtils.h>
#include <NumberFormatting.h>

#define		kVideoTimeScale 	600
#define		kNumVideoFrames 	70
#define		kPixelDepth 		32	/* use 32-bit depth */
#define		kNoOffset 			0
#define		kMgrChoose			0
#define		kSyncSample 		0
#define		kAddOneVideoSample	1
#define		kSampleDuration 	25	/* frame duration = 1/10 sec */
#define		kTrackStart			0
#define		kMediaStart			0

#ifndef		VIDEO_CODEC_TYPE
//#define		VIDEO_CODEC_TYPE	kJPEGCodecType
#define		VIDEO_CODEC_TYPE	kH264CodecType
#endif
#ifndef		VIDEO_CODEC_QUALITY
#define		VIDEO_CODEC_QUALITY	codecNormalQuality
#endif

#include "Util/math_bmpMatrixMN.h"

class QTMovieFile
{
	CGrafPtr	m_savedPort;
	GDHandle	m_savedGD;
	GWorldPtr	m_gWorld;
	Track		m_pTrack;
	Media		m_pMedia;
	Movie		m_pMovie;
	Str255		m_FileName;
	short		m_resRefNum;
	Rect		m_trackFrame ;
	Handle		m_compressedData;
	Ptr			m_compressedDataPtr;
	ImageDescriptionHandle m_imageDesc;

	TCHAR	m_szErrMsg[256];

	void	ReleaseMemory();
	int		nAppendFuncSelector;		//0=Dummy	1=FirstTime	2=Usual
	HRESULT	AppendFrameFirstTime( );
	HRESULT	AppendFrameUsual();
	HRESULT	AppendDummy();
	HRESULT	(QTMovieFile::*pAppendFrame[3])();

public:
	QTMovieFile();
	HRESULT InitGraphicsWorld(int h, int w, LPCTSTR lpszFileName);
	~QTMovieFile(void);
	HRESULT	AppendNewFrame(Math::matrixMxN<Util::rgb>& bmp, int sizeY=-1, int sizeX=-1);
	HRESULT	AppendNewFrame(Math::matrixMxN<Util::frgba>& bmp, int sizeY=-1, int sizeX=-1);
};


#endif 
