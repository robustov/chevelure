/**
 */
#ifndef _ED_Spline_h_
#define _ED_Spline_h_
#pragma once

#include "./_config.h"
#include "./Vector.h"
#include "./Matrix.h"
#include "./_math.h"
#include "../Util/CTAssert.h"
#include <vector>
#include "../Util/STLStream.h"


namespace Math 
{

inline Math::Vec3f Spline3(float t, Math::Vec3f* data, float* knots, int count, const Math::Matrix4f& basis)
{
	typedef Math::Vec3f point_t;
	typedef float param_t;

	if(count<2) return Math::Vec3f(0);

	int index;
	float p;
	if(t<=knots[0])
	{
		index = 1;
		p = 0;
	}
	else if(t>=knots[count-1])
	{
		index = count-1;
		p = 1;
	}
	{
		for(index=1; index<count && t>=knots[index]; index++);
		if(index>=count)
		{
			index = count-1;
			p = 1;
		}
		else
		{
			float d = knots[index]-knots[index-1];
			if( d!=0) 
				p = (t-knots[index-1])/d;
			else
				p = 0;

		}
	}
	index = index-2;
//	point_t* four_points = &knots[0] + index;

	Vector<4,param_t> u__( p*p*p, p*p , p , 1 );

	u__ *= basis;
	point_t result(0);
	for( int i = 0 ; i < 4 ; ++i )
	{
		int n = index+i;
		if(n<0) n=0;
		if(n>=count) n=count-1;
		result += u__[i] * data[n];
	}
	return result;
}


/**
 */

template<int N, class T, class POINT, class PARAM>
class Spline
{
public:
	typedef POINT point_t;
	typedef PARAM param_t;
	typedef T scalar_t;
	typedef Matrix< N+1, scalar_t> matrix_t;
	typedef Matrix< 4, param_t> basis_t;

	typedef Spline<N,T, POINT, PARAM> self_t;

	enum { Size = N };

	basis_t basis;
	std::vector< point_t> knots;

	/**
	*/
	Spline() {}

	/**
	*/
	explicit Spline(basis_t basis, point_t* data, int size, bool bPeriodic) 
	{ 
		setBasis(basis);
		setKnots(data, size, bPeriodic); 
	}

	/**
	*/
	void setKnots(point_t* data, int size, bool bPeriodic)
	{ 
		knots.clear();
		if(data && size)
		{
			if( !bPeriodic)
				knots.assign(data, data+size);
			else
			{
				// ��� ���������!
				knots.reserve(size+3);
				knots.push_back(data[size-1]);
				knots.insert(knots.end(), data, data+size);
				knots.push_back(data[0]);
				knots.push_back(data[1]);
			}
		}
	}

	/**
	 */
	void setBasis(basis_t basis) 
	{ 
		this->basis = basis;
	}


	point_t getValue(param_t p)
	{
		int index = param2index(p);
		point_t* four_points = &knots[0] + index;
		
		Vector<4,param_t> u__( p*p*p, p*p , p , 1 );

		u__ *= basis;
		point_t result(0);
		for( int i = 0 ; i < 4 ; ++i )
		{
			result += u__[i] * four_points[i];
		}
		return result;
	}
	point_t getTangent(param_t p)
	{
		int index = param2index(p);
		point_t* four_points = &knots[0] + index;
		
		Vector<4,param_t> u__( 3*p*p, 2*p , 1 , 0 );

		u__ *= basis;
		point_t result(0);
		for( int i = 0 ; i < 4 ; ++i )
		{
			result += u__[i] * four_points[i];
		}
		return result;
	}

	int param2index(param_t& p)
	{
		int n_segments = (int)knots.size()-3;
		param_t temp = p * scalar_t( n_segments );
		int n_segment = int( temp );	
		if(n_segment>=n_segments)
			n_segment = n_segments-1;
		p = temp - param_t(n_segment);
		return n_segment;
	}

	static ED_MATH_EXTERN basis_t BSpline;
	static ED_MATH_EXTERN basis_t CatmullRom;
	static ED_MATH_EXTERN basis_t Bezier;
	static ED_MATH_EXTERN basis_t Linear;

}; // class Vector<N, T>

/**
 * Write/Load components to/from a stream.
template<int N, class T, class Stream> inline
Stream& operator >> (Stream& out, Math::Vector<N, T>& v)
{
	for( int i=0; i<N; ++i )
		out >> v[i];
	return out;
}
 */

typedef Spline<1, float, float, float > Spline1f;
typedef Spline<2, float, Math::Vector<2, float>, float > Spline2f;
typedef Spline<3, float, Math::Vector<3, float>, float > Spline3f;

typedef Spline<1, double, double, double> Spline1d;
typedef Spline<2, double, Math::Vector<2, double>, double> Spline2d;
typedef Spline<3, double, Math::Vector<3, double>, double> Spline3d;

}

/**
 * Write/Load components to/from a stream.
 */
template<int N, class T, class POINT, class PARAM, class Stream> inline
Stream& operator >> (Stream& out, Math::Spline<N, T, POINT, PARAM>& v)
{
//	unsigned int sz = v.knots.size();
//	for(unsigned int i=0; i<sz; i++)
	out >> v.knots;
	return out;
}

#include "_Spline.inl.h"

#endif /* _ED_Spline_h_ */
