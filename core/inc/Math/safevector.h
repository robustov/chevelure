#pragma once

namespace std
{

template <class CLASS>
CLASS* safevector(vector<CLASS>& arg)
{
	if(arg.empty())
		return 0;
	return &arg[0];
}

}