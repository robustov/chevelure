#pragma once

#include "Math/Math.h"
#include "Math/Polygon.h"
#include "Math/Quad.h"
#include "Math/Edge.h"
#include <vector>
#include <map>

namespace Math 
{
	struct subdivWeight
	{
		std::vector< std::pair<int, float> > data;
	};

	// ������������ ����� �������
	ED_MATH_EXTERN void ED_MATH_API Subdiv(
		int level,									// ����� ������������
		Polygon32* faces,							// ����: ��������
		int poligoncount,							// ����� ���������
		int vertexcount,							// ����� ���������
		int facevertexcount,						// ����� fv
		std::vector< Quad32>& quads,				// �����: �����
		std::vector< subdivWeight>& neoverts,		// ���� ��� ������� ��������
		std::vector<int>* faces_polygonIndex=NULL,	// ���� ������� ����� ��������� �������� �� �������� ��������� ����
		std::map<Math::Edge32, Math::Edge32>* edges_srcedge=NULL // �������� ���������� -> ������ ����� (� ������ ���� ������ ����� ��������� �������� �������� �����)
		);
	// ������� ��� �������� Subdiv
	ED_MATH_EXTERN void ED_MATH_API test_Subdiv(
		int facecount,  // ����� ��������� (1 ��� 2)
		int verts,		// ��������� � ���. ��������
		int level		// ������� �������
		);

	// ������ ��� ������
	template <class T>
	void SubdivCurve(
		int level, 
		T* verts, 
		int count, 
		std::vector< T>& res
		);
	// ������ ��� ������
	template <class T>
	void SubdivCurve(
		int level, 
		std::vector< T>& res
		);

}

/**
 * Write/Load components to/from a stream.
 */
template<class Stream> inline
Stream& operator >> (Stream& out, Math::subdivWeight& v)
{
	out >> v.data;
	return out;
}


namespace Math 
{
	template <class T>
	void SubdivCurve(
		int level, 
		T* verts, 
		int count, 
		std::vector< T>& res
		)
	{
		if(level==0)
			res.assign(verts, verts+count);

		std::vector< T> temp;
		temp.reserve(count*(2^level));
		for( ; level>0; level--)
		{
			temp.clear();
			for(int i=0; i<count; i++)
			{
				// ������
				if(i==0)
				{
					T v = verts[i];
					temp.push_back(v);
					v = (verts[i+1]+verts[i])*0.5f;
					temp.push_back(v);
					continue;
				}
				// ���������
				if(i==count-1)
				{
					T v = verts[i];
					temp.push_back(v);
					continue;
				}
				// �����������
				float cbound=0.25f, ckernel=0.5f;
				T v = verts[i-1]*cbound+verts[i+1]*cbound+verts[i]*ckernel;
				temp.push_back(v);

				v = (verts[i+1]+verts[i])*0.5f;
				temp.push_back(v);
			}
			res.assign(temp.begin(), temp.end());
			verts = &res[0];
			count = res.size();
		}
	}
	template <class T>
	void SubdivCurve(
		int level, 
		std::vector< T>& res
		)
	{
		std::vector< T> in = res;
		SubdivCurve(level, &in[0], in.size(), res);
	}
}
