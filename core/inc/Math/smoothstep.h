#pragma once

namespace Math
{
	// smoothstep
	// �������� �������� � ��������� [0, 1]
	template< class T>
	inline T smoothstep(T min, T max, T x)
	{
		if(x>=max) return 1;
		if(x<=min) return 0;

		x = (x-min)/(max-min);
		T y = -2*x*x*x + 3*x*x;
		return y;
	}
	// smoothstep_inv - �������� � smoothstep
	// y � ��������� �� 0-1
	template< class T>
	inline T smoothstep_inv(T min, T max, T y)
	{
		if(y>=T(1))
			return max;
		if(y<=T(0))
			return min;

		float x = smoothstep<T>( T(0), T(1), y);
		x = min + x*(max-min);
		return x;
	}
}


