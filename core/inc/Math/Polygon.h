#pragma once

#include <vector>
#include "Util/STLStream.h"

namespace Math 
{
	template <class T>
	struct Polygon
	{
		std::vector<T> verts;
	public:
		void resize(int vertcount)
		{
			verts.resize(vertcount);
		}
		const T& operator[](int i)const
		{
			return verts[i];
		}
		T& operator[](int i)
		{
			return verts[i];
		}
		const T& cycle(int i) const
		{
			if(i<0) i += (int)verts.size()*(-i/(int)verts.size() + 2);
			return verts[i % ((int)verts.size())];
		}
		int size()const
		{
			return (int)verts.size();
		}
		bool operator==(const Polygon<T>& arg) const 
		{
			return verts==arg.verts;
		}
		bool operator!=(const Polygon<T>& arg) const 
		{
			return verts!=arg.verts;
		}
	};
	typedef Polygon<unsigned long> Polygon32;
	typedef Polygon<unsigned short> Polygon16;
};

template<class STREAM, class T> inline
STREAM& operator >> (STREAM& out, Math::Polygon<T>& v)
{
	out >> v.verts;
	return out;
}


