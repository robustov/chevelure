/**
 * Based on
 * EasyGL library v0.1
 * (C)opyright 2002 by Dmitry S. Baikov
 *
 * @file Rotation3.h
 * @author Dmitry S. Baikov
 */
#ifndef _ED_Math_Rotation3_h_
#define _ED_Math_Rotation3_h_


#include "./_config.h"
#include "./Vector.h"
#include "./Matrix.h"


namespace Math {

template<typename T> class Rotation3;

/**
 * Do spherical linear interpolation of rotations.
 */
template<typename T> ED_MATH_EXTERN
Rotation3<T> ED_MATH_API
slerp(const Rotation3<T>& a, const Rotation3<T>& b, const T t);

/**
 * Order of 3d rotations (self-explanatory).
 */
enum RotationOrder {
	XYZ=0, 
	YZX, 
	ZXY, 
	XZY, 
	YXZ, 
	ZYX
};


/**
 * 3-dimensional rotation (unit quaternion).
 */
template<typename T>
class Rotation3 {
private:
	Vector<3,T> v;
	T w;

public:
	typedef Rotation3<T> self_t;
	typedef T scalar_t;
	typedef Vector<3,T> vector_t;
	
		/**
		 * Create uninitialized rotation.
		 */
		Rotation3() {}

		/**
		 * Create unit rotation.
		 */
//		Rotation3(T w_) : v(0), w(w_) {}

		/**
		 * Create rotation from quaternion components.
		 */
		Rotation3(T x_, T y_, T z_, T w_)
			: v(x_, y_, z_), w(w_) {}

		/**
		 * Generic copy contructor.
		 */
		template<class F>
		Rotation3(const Rotation3<F>& r)
			: v(r.im()), w(r.re()) {}

		/**
		 * Create rotation around 'axis' by 'angle'.
		 * @param angle rotation angle in radians
		 * @param axis rotation axis
		 */
		Rotation3(scalar_t angle, const vector_t& axis)
			{ setAngleAxis(angle, axis); }

		/**
		 * Create rotation from one vector to another.
		 */
		Rotation3(const vector_t& from, const vector_t& to)
			{ setFromTo(from, to); }

		/**
		 * Create rotation from Euler anlges.
		 */
		Rotation3(RotationOrder order, const vector_t& angles)
			{ setEulerAngles(order, angles); }

	/**
	 * Get real quaternion part.
	 */
	scalar_t re() const { return w; }
	scalar_t& re() { return w; }

	/**
	 * Get imaginary quaternion part.
	 */
	const vector_t& im() const { return v; }
	vector_t& im() { return v; }
		
	/**
	 * Get basis vectors (rotation matrix).
	 */
	ED_MATH_EXTERN void ED_MATH_API getBasis(T* axis0, T* axis1, T* axis2) const;
	
	void getMatrix(Math::Matrix<4, T>& m) const
	{
		m = Math::Matrix<4, T>::id;
		getBasis(m[0].data(), m[1].data(), m[2].data());
	}

	/**
	 * Set rotation by angle-axis pair.
	 */
	ED_MATH_EXTERN self_t& ED_MATH_API setAngleAxis(scalar_t angle, const vector_t& axis);

	/**
	 * Get rotation by angle-axis pair.
	 */
	ED_MATH_EXTERN void ED_MATH_API getAngleAxis(scalar_t& angle, vector_t& axis) const;

	/**
	 * Set rotation as euler angles.
	 */
	ED_MATH_EXTERN self_t& ED_MATH_API setEulerAngles(RotationOrder order, const vector_t& angles);

	/**
	 * Set rotation as yaw-pitch-roll.
	 */
	ED_MATH_EXTERN self_t& ED_MATH_API setYawPitchRoll(scalar_t yaw, scalar_t pitch, scalar_t roll);

	/**
	 * Get rotation as yaw-pitch-roll.
	 */
	ED_MATH_EXTERN void ED_MATH_API getYawPitchRoll(scalar_t& yaw, scalar_t& pitch, scalar_t& roll) const;
	
	/**
	 * Set rotation as transition from one vector to another.
	 */
	inline self_t& setFromTo(const vector_t& from, const vector_t& to);

	/**
	 * Add rotations.
	 */
	inline self_t operator + (const self_t& arg) const
	{
		self_t res;
		res.v = this->v + arg.v;
		res.w = this->w + arg.w;
		return res;
	}
	inline self_t& operator += (const self_t& arg)
	{
		this->v = this->v + arg.v;
		this->w = this->w + arg.w;
		return *this;
	}

	/**
	 * Concatenate rotations.
	 */
	ED_MATH_EXTERN self_t ED_MATH_API operator * (const self_t&) const;

	/**
	 * Concatenate rotations.
	 */
	self_t& operator *= (const self_t&);

	self_t operator * (const scalar_t& arg) const
	{
		self_t res;
		res.v[0] = arg*this->v[0];
		res.v[1] = arg*this->v[1];
		res.v[2] = arg*this->v[2];
		res.w    = arg*this->w;
		return res;
	}
	self_t& operator *= (const scalar_t& arg)
	{
		this->v[0] = arg*this->v[0];
		this->v[1] = arg*this->v[1];
		this->v[2] = arg*this->v[2];
		this->w    = arg*this->w;
		return *this;
	}

	/**
	 * Reset rotation.
	 */
	self_t& identity();

	/**
	 * Invert rotation (in-place).
	 */
	self_t& invert();

	/**
	 * Get inverted copy of rotation.
	 */
	self_t inverted() const;
	
	/**
	 * Normalize quaternion (in-place).
	 */
	ED_MATH_EXTERN self_t& ED_MATH_API normalize();

	/**
	 * Get normalized copy of quaternion.
	 */
	ED_MATH_EXTERN self_t ED_MATH_API normalized() const;

	friend ED_MATH_EXTERN Rotation3<T> ED_MATH_API
	slerp<>(const Rotation3<T>& a, const Rotation3<T>& b, const T t);
	
}; // class Rotation3<T>


// --- implementation

template<class T> inline
Rotation3<T>& Rotation3<T>::identity()
{
	w = 1;
	v[2] = v[1] = v[0] = 0;
	return *this;
}

template<class T> inline
Rotation3<T>& Rotation3<T>::invert()
{
	v.negate();
	return *this;
}

template<class T> inline
Rotation3<T> Rotation3<T>::inverted() const
{
	return Rotation3<T>(-v[0], -v[1], -v[2], w);
}


template<class T> inline
Rotation3<T>& Rotation3<T>::operator *= (const Rotation3<T>& op)
{
	Rotation3<T> res = *this * op;
	return *this = res;
}

template<class T> inline
Rotation3<T>& Rotation3<T>::setFromTo(const Vector<3,T>& from, const Vector<3,T>& to)
{
	vector_t axis = cross(from, to);
	
	T cos2Alfa = dot(from, to);
	T sin2Alfa = sqrt(1 - cos2Alfa * cos2Alfa);
	
	T sinAlfa = sqrt( (1-cos2Alfa) / 2 );
	T cosAlfa = sqrt( (1+cos2Alfa) / 2 );
	
	if (sin2Alfa > 0.000001 )				// MITKA: sin2Alfa != 0
		axis *= sinAlfa / sin2Alfa;
	
	*this = Rotation3<T>(-axis[0], -axis[1], -axis[2], cosAlfa);
	return *this;
}


#ifdef ED_MATH_IMPLEMENT_ROTATION3

template<class T> ED_MATH_EXTERN
Rotation3<T>& ED_MATH_API Rotation3<T>::normalize()
{
	T scale = sqrt(w*w + length2(v));
	if( scale > 0 ) {
		w /= scale;
		v /= scale;
	} else {
		identity();
	}
	return *this;
}

template<class T> ED_MATH_EXTERN
Rotation3<T> ED_MATH_API Rotation3<T>::normalized() const
{
	T scale = sqrt(w*w + length2(v));
	if( scale > 0 ) {
		return self_t(v[0]/scale, v[1]/scale, v[2]/scale, w/scale);
	} else {
		return self_t(0, 0, 0, 1);
	}
}

template<class T> ED_MATH_EXTERN
Rotation3<T> ED_MATH_API Rotation3<T>::operator * (const Rotation3<T>& op) const
{
	Rotation3<T> res;
	res.w = (w*op.w) - dot(v, op.v);
	res.v = (op.v*w) + (v*op.w) + cross(v, op.v);
	return res;
}

template<class T> ED_MATH_EXTERN
void ED_MATH_API Rotation3<T>::getBasis(T* axis0, T* axis1, T* axis2) const
{
	if( axis0 ) {
		axis0[0] = 1 - 2 * (v[1] * v[1] + v[2] * v[2]);
		axis0[1] = 2 * (v[0] * v[1] - v[2] * w);
		axis0[2] = 2 * (v[2] * v[0] + v[1] * w);
	}
	
	if( axis1 ) {
		axis1[0] = 2 * (v[0] * v[1] + v[2] * w);
		axis1[1] = 1 - 2 * (v[2] * v[2] + v[0] * v[0]);
		axis1[2] = 2 * (v[1] * v[2] - v[0] * w);
	}
	
	if( axis2 ) {
		axis2[0] = 2 * (v[2] * v[0] - v[1] * w);
		axis2[1] = 2 * (v[1] * v[2] + v[0] * w);
		axis2[2] = 1 - 2 * (v[1] * v[1] + v[0] * v[0]);
	}
}

template<class T> ED_MATH_EXTERN
Rotation3<T>& ED_MATH_API Rotation3<T>::setAngleAxis(T angle, const Vector<3,T>& axis)
{
	w = cos(angle/2);
	v = Math::normalized(axis) * sin(angle/2);
	return *this;
}

template<class T> ED_MATH_EXTERN
void ED_MATH_API Rotation3<T>::getAngleAxis(T& angle, Vector<3,T>& axis) const
{
	angle = 2*acos( w );
	axis = Math::normalized( v );
}
	
template<class T> ED_MATH_EXTERN
Rotation3<T>& ED_MATH_API Rotation3<T>::setEulerAngles(RotationOrder order, const Vector<3,T>& angle)
{
	static int axes[6][3] = {
		{0,1,2}, //XYZ
		{1,2,0}, //YZX
		{2,0,1}, //ZXY
		{0,2,1}, //XZY
		{1,0,2}, //YXZ
		{2,1,0}  //ZYX
	};
	identity();
	for(int i=0; i<3; ++i)
	{
		Rotation3<T> rot(0,0,0,cos(angle[i]/2));
		rot.v[ axes[order][i] ] = sin(angle[i]/2);
		*this *= rot;
	}
	return *this;
}

template<class T> ED_MATH_EXTERN
void ED_MATH_API Rotation3<T>::getYawPitchRoll(T &yaw, T &pitch, T &roll) const
{
	yaw = atan(2.f * (v[0]*v[1] + v[2]*w) / (v[0]*v[0] - v[1]*v[1] - v[2]*v[2] + w*w) );
	roll = atan(2.f * (v[1]*v[2] + v[0]*w) / (-v[0]*v[0] - v[1]*v[1] + v[2]*v[2] + w*w) );
	pitch = asin(-2.f * (v[0]*v[2] - v[1]*w) );
}

template<class T> ED_MATH_EXTERN
Rotation3<T>& ED_MATH_API Rotation3<T>::setYawPitchRoll(T yaw, T pitch, T roll)
{
	Rotation3<T> qyaw  (0, 0, sin(yaw/2), cos(yaw/2));
	Rotation3<T> qpitch(0, sin(pitch/2), 0, cos(pitch/2));
	Rotation3<T> qroll (sin(roll/2), 0, 0, cos(roll/2));
	return *this = qyaw * qpitch * qroll;
}

template<class T> ED_MATH_EXTERN
Rotation3<T> ED_MATH_API slerp(const Rotation3<T>& from_, const Rotation3<T>& to, const T t)
{
	// compute dot product, aka cos(theta):
	T cosom = from_.w * to.w + dot(from_.v, to.v);

	Rotation3<T> from;
	if(cosom < 0) {
		// flip start quaternion
		cosom = -cosom;
		from.w = -from_.w;
		from.v = -from_.v;
	} else {
		from.w = from_.w;
		from.v = from_.v;
	}
	// calculate coefficients
	T scale0, scale1;
	if ( (1 - cosom) > T(0.05f) ) {
		// standard case (slerp)
		T omega = acos(cosom);
		T sinom = sin(omega);
		scale0 = sin((1 - t) * omega) / sinom;
		scale1 = sin(t * omega) / sinom;
	} else {       
		// close case (lerp)
		scale0 = 1 - t;
		scale1 = t;
	}
	// calculate final values
	return Rotation3<T>(
		/*x*/scale0 * from.v[0] + scale1 * to.v[0],
		/*y*/scale0 * from.v[1] + scale1 * to.v[1],
		/*z*/scale0 * from.v[2] + scale1 * to.v[2],
		/*w*/scale0 * from.w + scale1 * to.w
		);
}

#endif /* ED_MATH_IMPLEMENT_ROTATION3 */


template<class T>
Math::Vec3f rotate(const Rotation3<T>& q, const Math::Vec3f& v)
{
	Math::Matrix3f m;
	q.getBasis(m[0].data(), m[1].data(), m[2].data());
	return m*v;
}


typedef Rotation3<float> Rot3f;
typedef Rotation3<double> Rot3d;

template<class T> inline
Vector<3, T> rotate(Vector<3, T> src, float ang, const Vector<3, T>& axis)
{
	Math::Matrix<4, T> basis;
	Rotation3<T>(ang, axis).getMatrix(basis);
	src = basis*src;
	return src;
}

} // namespace Math

/**
 * Write/Load components to/from a stream.
 */
template<class T, class Stream> inline
Stream& operator >> (Stream& out, Math::Rotation3<T>& v)
{
	out >> v.im() >> v.re();
	return out;
}

#endif /* _ED_Math_Rotation3_h_ */
