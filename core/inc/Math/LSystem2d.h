#pragma once
#include <vector>
#include <set>
#include <map>
#include "Math/Math.h"
#include "Math/MatrixMxN.h"
#include <conio.h>

// L system 2d
// ����� ������� 2d
//
namespace Math
{
	// �������� ������ CELL
	/*/
	struct cell_proto
	{
		// ������������ � �������
		void Init(
			const Math::Vec2i& id,
			const LSystem2d<cell_proto>& system,
			std::vector<Math::Vec2i>& add_to_alives	// ����� ������� ���� � �����
			);
		// ���������� �� ����� ����� � �������
		void Frame(
			const Math::Vec2i& id,
			const LSystem2d<cell_proto>& system,
			std::vector<Math::Vec2i>& add_to_alives	// ����� ������� ���� � �����
			);
		// ��� - ���������� ������ � �����
		// ���� ������ true - ������ �������� ����
		// �����- ����������� (��������)
		bool Step(
			const Math::Vec2i& id,					// �� ���� ������
			const LSystem2d<cell_proto>& system,	// ��� �������
			std::vector<Math::Vec2i>& add_to_alives	// ������ ������ ������� ���� �������� � �����
			);
	};
	/*/

	// L system 2d
	template <class CELL, class THIS>
	struct LSystem2d
	{
		// ������������
		// � ���� ������ �������� ����� Init
		virtual void Init();
		// ����� ����
		// � ���� ������ �������� ����� Frame
		virtual void Frame();
		// ��� ���������
		// ���������� ��� ����� ������ � �������� � ��� ����� step
		virtual void Step();

		// ����
		bool Loop(int sizeX, int sizeY, int stepperframe);

	public:
		// �����
		Math::Vec2i neibour(const Math::Vec2i& pt, int dir) const;
		// ������
		const CELL& getCell(const Math::Vec2i& pt) const;
		CELL& getCell(const Math::Vec2i& pt);

	public:
		int frame;
		// ��� ������
		Math::matrixMxN<CELL> work;
		// ������ ����� ������ 
		std::set<Math::Vec2i> alives;
		
		// ������ ������ ������� ���� �������� � �����
		std::vector<Math::Vec2i> add_to_alives;
		// ������ ������ ������� ���� ������� �� �����
		std::vector<Math::Vec2i> remove_from_alives;

	protected:
		// �������� add_to_alives � alives ������� remove_from_alives �� alives
		void AddRemoveAlives();

	};

}

#include "LSystem2d.hpp"
