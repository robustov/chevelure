/**
 * Based on
 * EasyGL library v0.1
 * (C)opyright 2002 by Dmitry S. Baikov
 *
 * @file Vector.h
 * @author Dmitry S. Baikov
 *
 * @todo expression templates
 * @todo ET + SSE
 */
#ifndef _ED_Math_Vector_h_
#define _ED_Math_Vector_h_
#pragma once


#include "./_config.h"
#include "./_VectorBase.h"
#include "./_math.h"
#include "../Util/CTAssert.h"


namespace Math {


/**
 * N-dimensional Vector.
 * \note typedef naming convention: \n
 *	typedef Vector<N,T> VecNt; \n
 *	where t is selected by the following rules: \n
 *	char -> b \n
 *	short -> s \n
 *	int -> i \n
 *	unsigned XXX -> uX \n
 *	\n
 *	float -> f \n
 *	double -> d \n
 *	\n
 *	typedefs should be sorted by type (integer types first) and then by size.
 */
template<int N, class T>
class Vector : public _VectorBase<N,T> {
public:
	typedef Vector<N,T> self_t;
	typedef T scalar_t;
	enum { Size = N };

		/**
		 * Create uninitialized vector.
		 */
		Vector() {}

		/**
		 * Create vector with all components set to s.
		 */
		explicit Vector(T s) { set(s); }

		Vector(const Vector<N-1,T>& src, T d) 
		{ 
			for(int i=0; i<N-1; ++i) 
				this->v[i]=src.v[i];
			this->v[N-1] = d;
		}

		/**
		 * Create Vector<2,T>.
		 */
		Vector(T a, T b) { Util::CTAssert<N==2>(); this->v[0]=a; this->v[1]=b; }

		/**
		 * Create Vector<3,T>.
		 */
		Vector(T a, T b, T c) { Util::CTAssert<N==3>(); this->v[0]=a; this->v[1]=b; this->v[2]=c; }

		/**
		 * Create Vector<4,T>.
		 */
		Vector(T a, T b, T c, T d) { Util::CTAssert<N==4>(); this->v[0]=a; this->v[1]=b; this->v[2]=c; this->v[3]=d; }

		/**
		 * Generic copy constructor.
		 */
		template<class F>
		Vector(const Vector<N, F>& u) { for(int i=0; i<N; ++i) this->v[i]=(T)(u[i]); }

	bool operator ==(const self_t& arg) const { for(int i=0; i<N; ++i) if( this->v[i] != arg.v[i]) return false; return true;}

	bool operator !=(const self_t& arg) const { for(int i=0; i<N; ++i) if( this->v[i] != arg.v[i]) return true; return false;}

	/**
	 * Set all components to specified value.
	 */
	self_t& set(T val) { for(int i=0; i<N; ++i) this->v[i] = val; return *this; }

	/**
	 * Set Vector<2,T> components.
	 */
	self_t& set(T v0, T v1) { this->x = v0; this->y = v1; return *this; }

	/**
	 * Set Vector<3,T> components.
	 */
	self_t& set(T v0, T v1, T v2) { this->x=v0; this->y=v1; this->z=v2; return *this; }

	/**
	 * Set Vector<4,T> components.
	 */
	self_t& set(T v0, T v1, T v2, T v3) { this->x=v0; this->y=v1; this->z=v2; this->w=v3; return *this; }

	/**
	 * Get as vector of different size.
	 */
	template <int K>
	Vector<K, T> as(T u = T(0)) const
	{
		Vector<K, T> res;
		for(int i=0; i<K; i++)
		{
			if(i < N) res[i] = this->v[i];
			else res[i] = u;
		}
		return res;
	}

	/**
	 * Get components array.
	 */
	T* data() { return this->v; }

	/**
	 * Get components array.
	 */
	const T* data() const { return this->v; }

	/**
	 * Get number of components.
	 */
	static int size() { return N; }

	/**
	 * Array-style component access.
	 */
	T& operator [] (int i) { return this->v[i]; }

	/**
	 * Array-style component access.
	 */
	const T& operator [] (int i) const { return this->v[i]; }

	bool operator<(const self_t& arg) const
	{
		for(int i=0; i<N; ++i)
		{
			if( this->v[i]<arg[i])
				return true;
			if( this->v[i]>arg[i])
				return false;
		}
		return false;
	}
	/**
	 * Negate in-place.
	 */
	self_t& negate()
	{
		for(int i=0; i<N; ++i)
			this->v[i] = -this->v[i];
		return *this;
	}

	/**
	 * Get negative copy.
	 */
	self_t operator - () const
	{
		self_t res;
		for(int i=0; i<N; ++i)
			res[i] = -this->v[i];
		return res;
	}

	/**
	 * Get squared euclidian length (sum of squared components).
	 */
	T length2() const
	{
		T res = this->v[0] * this->v[0];
		for( int i=1; i<N; ++i )
			res += this->v[i] * this->v[i];
		return res;
	}

	/**
	 * Get euclidian length.
	 */
	T length() const { return T(sqrt( length2() )); }

	/**
	 * Normalize in-place.
	 */
	self_t& normalize()
	{
		T l = length();
		if(l==0) return *this;
		return (*this /= l);
	}

	/**
	 * Get normalized copy.
	 */
	self_t normalized() const
	{
		T l = length();
		if(l==0) return *this;
		return (*this / l);
	}


	// operators
#define _GEN_ASSIGN_OPERATOR( op ) \
	inline \
	self_t& operator op##= (const self_t& u) \
	{ for(int i=0; i<N; ++i) this->v[i] op##= u[i]; return *this; } \
	\
	inline \
	self_t& operator op##= (scalar_t s) \
	{ for(int i=0; i<size(); ++i) this->v[i] op##= s; return *this; }

	_GEN_ASSIGN_OPERATOR( + )
	_GEN_ASSIGN_OPERATOR( - )
	_GEN_ASSIGN_OPERATOR( * )
	_GEN_ASSIGN_OPERATOR( / )

#undef _GEN_ASSIGN_OPERATOR


#define _GEN_BINARY_OPERATOR( op ) \
	inline \
	self_t operator op (const self_t& u) const \
	{ self_t r; for(int i=0; i<N; ++i) r[i] = this->v[i] op u[i]; return r; } \
	\
	inline \
	self_t operator op (scalar_t s) const \
	{ self_t r; for(int i=0; i<N; ++i) r[i] = this->v[i] op s; return r; }

	_GEN_BINARY_OPERATOR( + )
	_GEN_BINARY_OPERATOR( - )
	_GEN_BINARY_OPERATOR( * )
	_GEN_BINARY_OPERATOR( / )

#undef _GEN_BINARY_OPERATOR


#define _GEN_SCALAR_OPERATOR( op ) \
	friend inline \
	self_t operator op (scalar_t s, const self_t& v) \
	{ self_t r; for(int i=0; i<N; ++i) r[i] = s op v[i]; return r; }

	_GEN_SCALAR_OPERATOR( + )
	_GEN_SCALAR_OPERATOR( - )
	_GEN_SCALAR_OPERATOR( * )
	_GEN_SCALAR_OPERATOR( / )

#undef _GEN_SCALAR_OPERATOR


}; // class Vector<N, T>


#define _GEN_SCALAR_OPERATOR( op ) \
template<int N, class T> inline \
Vector<N,T> operator op (T s, const Vector<N,T>& v) \
{ \
	Vector<N,T> res; \
	for(int i=0; i<N; ++i) res[i] = s*v[i]; \
	return res; \
}

_GEN_SCALAR_OPERATOR( + )
_GEN_SCALAR_OPERATOR( - )
_GEN_SCALAR_OPERATOR( * )
_GEN_SCALAR_OPERATOR( / )

#undef _GEN_SCALAR_OPERATOR


/**
 * Get squared euclidian length (sum of squared components).
 */
template<int N, class T>
T length2(const Vector<N, T>& v)
{
	return v.length2();
}

/**
 * Get euclidian length sqrt(sum of squared components).
 */
template<int N, class T>
T length(const Vector<N, T>& v)
{
	return v.length();
}

/**
 * Get normalized copy.
 */
template<int N, class T>
Vector<N, T> normalized(const Vector<N, T>& v)
{
	return v.normalized();
}

/**
 * Calculate dot product.
 */
template<int N, class T>
T dot(const Vector<N, T>& u, const Vector<N, T>& v)
{
	T res = u[0] * v[0];
	for(int i=1; i<N; ++i)
		res += u[i] * v[i];
	return res;
}

/**
 * Calculate cross product.
 */
template<class T>
Vector<3, T> cross(const Vector<3, T>& a, const Vector<3, T>& b)
{
	Vector<3, T> res;
	res[0] = a[1] * b[2] - a[2] * b[1];
	res[1] = a[2] * b[0] - a[0] * b[2];
	res[2] = a[0] * b[1] - a[1] * b[0];
	return res;
}

/**
 * Get homogenized copy.
 */
template<int N, class T>
Vector<N-1, T> homogenized(const Vector<N, T>& v)
{
	Vector<N-1, T> res;
	for(int i=0; i<N-1; ++i)
		res[i] = v[i]/v[N-1];
	return res;
}


/**
 * Linear interpolation.
 */
template<int N, class T> inline
Vector<N, T> lerp(const Vector<N,T>& a, const Vector<N,T>& b, T factor)
{
//	return a*(1-factor) + b*factor;
	Vector<N,T> res;
	for(int i=0; i<N; ++i)
		res[i] = a[i]*(1-factor) + b[i]*factor;
	return res;
}

/**
 * Swizzle
 */
template<class T> inline
Vector<3, T> xyz(const Vector<4,T>& src)
{
	Vector<3,T> res;
	res.x = src.x;
	res.y = src.y;
	res.z = src.z;
	return res;
}

#ifdef _MSC_VER

// �������� 2� �������
template <class T>
Vector<2, T> rotate(Vector<2, T> v, T angle)
{
	T s = T( sin(-angle) );
	T c = T( cos(-angle) );

	Math::Matrix<2, T> op;
	op[0].set(c, -s);
	op[1].set(s, c);

	Vector<2, T> r = op*v;
	return r;
}
#endif

// ���� 2� ������� ������������ Math::Vec2f(1, 0)
template <class T>
T angle(Vector<2, T> v)
{
	T a = atan2(v.y, v.x);
	return a;
}


// common typedefs
typedef Vector<3, unsigned char> Vec3ub;

typedef Vector<4, char> Vec4b;

typedef Vector<2, int> Vec2i;
typedef Vector<3, int> Vec3i;

typedef Vector<2, float> Vec2f;
typedef Vector<3, float> Vec3f;
typedef Vector<4, float> Vec4f;

typedef Vector<2, double> Vec2d;
typedef Vector<3, double> Vec3d;
typedef Vector<4, double> Vec4d;

}

/**
 * Write/Load components to/from a stream.
 */
template<int N, class T, class Stream> inline
Stream& operator >> (Stream& out, Math::Vector<N, T>& v)
{
	for( int i=0; i<N; ++i )
		out >> v[i];
	return out;
}

#endif /* _ED_Math_Vector_h_ */
