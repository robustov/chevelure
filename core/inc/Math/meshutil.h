#pragma once

#include "Math/Math.h"
#include <vector>

namespace Math
{
	// ���������������� ����������
	ED_MATH_EXTERN Vec3f ED_MATH_API CalcBaryCoords(Vec3f* pts, Vec3f p);
	ED_MATH_EXTERN Vec3f ED_MATH_API CalcBaryCoords(Vec2f* pts, Vec2f p);
	ED_MATH_EXTERN Vec3f ED_MATH_API CalcBaryCoords(
		const Math::Vec3f& p0, const Math::Vec3f& p1, const Math::Vec3f& p2, 
		const Math::Vec3f& p);

	// � ���������
	ED_MATH_EXTERN Vec4f ED_MATH_API CalcBaryCoords3d(
		const Vec3f* pts,			// Vec3f[4]
		Vec3f p);

	// � ���� (����� �������)
	ED_MATH_EXTERN bool ED_MATH_API isPointInCube(
		const Math::Vec3f& p, 
		const Math::Vec3f* pts,		// Vec3f[8]
		Math::Vec3f* posInCube);


	// ����� � ��-��?
	ED_MATH_EXTERN bool ED_MATH_API IsInFace(Vec2f* pts, const Vec2f& p);

	// Tangent space for triangle
	ED_MATH_EXTERN void ED_MATH_API CalcTangentSpace(
		Vec3f V[3], 
		Vec2f uv[3],
		Vec3f& S, Vec3f& T);

	// ������� ��-��
	ED_MATH_EXTERN float ED_MATH_API facearea(
		const Vec3f& a,
		const Vec3f& b, 
		const Vec3f& c );

	template <int N, class CV, class CF>
	Math::Vector<N, CV> FromBary(
		int faceindex, 
		const Math::Vec3f& bary, 
		const std::vector< Math::Face<CF> >& faces, 
		const std::vector< Math::Vector<N, CV> >& verts)
	{
		const Math::Face<CF>& face = faces[faceindex];
		const Math::Vector<N, CV> &t0 = verts[face.v[0]];
		const Math::Vector<N, CV> &t1 = verts[face.v[1]];
		const Math::Vector<N, CV> &t2 = verts[face.v[2]];
		Math::Vector<N, CV> v = t0*bary.x + t1*bary.y + t2*bary.z;
		return v;
	}

	template <int N, class CV, class CF>
	Math::Vector<N, CV> FromBary(
		const Math::Vec3f& bary, 
		const Math::Face<CF>& face, 
		const std::vector< Math::Vector<N, CV> >& verts)
	{
		const Math::Vector<N, CV> &t0 = verts[face.v[0]];
		const Math::Vector<N, CV> &t1 = verts[face.v[1]];
		const Math::Vector<N, CV> &t2 = verts[face.v[2]];
		Math::Vector<N, CV> v = t0*bary.x + t1*bary.y + t2*bary.z;
		return v;
	}

	inline Math::Vec3f CalcBaryCoords2d( Math::Vec2f* vt, const Math::Vec2f& p)
	{
		Math::Vec2f a(vt[1].x - vt[0].x, vt[1].y - vt[0].y);
		Math::Vec2f b(vt[2].x - vt[0].x, vt[2].y - vt[0].y);

		float x = p.x - vt[0].x;
		float y = p.y - vt[0].y;

		float d = b.x*a.y - b.y*a.x;
		if(fabs(d) < 0.00001) 
			return Math::Vec3f(1, 0, 0);

		float k = (x*a.y - y*a.x)/d;

		float t;
		if(fabs(a.x) > fabs(a.y)) 
			t = (x - k*b.x)/a.x;
		else 
			t = (y - k*b.y)/a.y;

		return Math::Vec3f(1-t-k, t, k);
//		return t*(vt[1].y - vt[0].y) + k*(vt[2].y - vt[0].y) + vt[0].y;
	}

}

