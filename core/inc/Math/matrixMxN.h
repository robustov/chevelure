#pragma once

#include <memory.h>
#include "Math/interpolation.h"

namespace Math
{
	//! ������� ������� MxN
	//! 
	//! 
	template <class T> class matrixMxN
	{
	public:
		void init(int x, int y)
		{
			int s = x*y;
			if( (Xsize*Ysize)==s && _data!=0)
			{
			}
			else
			{
				delete _data;
				_data = 0;
				if( s)
					_data = new T[ s ];
			}
			if( _data)
				memset(_data, 0, s*sizeof(T));

			Xsize = x;
			Ysize = y;
		}

	public:
		int Xsize, Ysize;
		T* _data;
		typedef T* pointer_t;

	public:
		matrixMxN()
		{
			Xsize=Ysize=0;
			_data = 0;
		}
		matrixMxN(int x, int y)
		{
			Xsize=Ysize=0;
			_data = 0;
			init(x, y);
		}
		matrixMxN(const matrixMxN<T>& arg)
		{
			Xsize=Ysize=0;
			_data = 0;
			init(arg.Xsize, arg.Ysize);
			int s = arg.Xsize*arg.Ysize;
			memcpy(_data, arg._data, s*sizeof(T));
		}
		~matrixMxN(){delete _data;}

		matrixMxN<T>& operator=(const matrixMxN<T>& arg)
		{
			init(arg.Xsize, arg.Ysize);
			int s = arg.Xsize*arg.Ysize;
			memcpy(_data, arg._data, s*sizeof(T));
			return *this;
		}

		void clear()
		{
			delete _data;
			Xsize=Ysize=0;
			_data = 0;
		}
		bool empty()const
		{
			return (Xsize*Ysize) == 0;
		}
		pointer_t operator [](int j)
		{
			return _data + Xsize*j;
		}
		const pointer_t operator [](int j)const
		{
			return _data + Xsize*j;
		}
		T& operator()(int i)
		{
			return _data[i];
		}
		int sizeX()const{return Xsize;}
		int sizeY()const{return Ysize;}
		int index(int y, int x) const
		{
			int s = y*Xsize + x;
			return s;
		}
		int fullsize()const{return Xsize*Ysize;}
		T* data(){return _data;}
		const T* data()const{return _data;}

		T interpolation(float x, float y, bool bUtile=true, bool bVtile=true)
		{
			x -= 0.5f/Xsize;
			y -= 0.5f/Ysize;
			int iu0, iu1, iv0, iv1;
			float w00, w01, w10, w11;
			Math::interpolation(x, y, Xsize, Ysize, 
				iu0, iu1, iv0, iv1, 
				w00, w01, w10, w11,
				bUtile, bVtile);
			T res = (T)(
				(*this)[iv0][iu0]*w00 + 
				(*this)[iv1][iu0]*w01 + 
				(*this)[iv0][iu1]*w10 + 				
				(*this)[iv1][iu1]*w11);
			return res;
		}
		T bilinear_interpolation(float x, float y, bool bUtile=true, bool bVtile=true)
		{
			x -= 0.5f/Xsize;
			y -= 0.5f/Ysize;
			int iu0, iu1, iv0, iv1;
			float w00, w01, w10, w11;
			Math::bilinear_interpolation(x, y, Xsize, Ysize, 
				iu0, iu1, iv0, iv1, 
				w00, w01, w10, w11,
				bUtile, bVtile);
			T res = 
				(*this)[iv0][iu0]*w00 + 
				(*this)[iv1][iu0]*w01 + 
				(*this)[iv0][iu1]*w10 + 				
				(*this)[iv1][iu1]*w11;
			return res;
		}
		T cubic_interpolation(float x, float y, bool bUtile=true, bool bVtile=true)
		{
			x -= 0.5f/Xsize;
			y -= 0.5f/Ysize;

			int iu[4], iv[4];
			float w[4][4];
			Math::cubic_interpolation(
				"catmull-rom", 
				x, y, Xsize, Ysize, 
				iu, iv,
				w, bUtile, bVtile);
			T res;
			for(int x=0; x<4; x++)
			{
				for(int y=0; y<4; y++)
				{
					int U = iu[x];
					int V = iv[y];
					float val = (*this)[V][U];

					if(x==0 && y==0)
						res = w[x][y]*val;
					else
						res += w[x][y]*val;
				}
			}
			return res;
		}
		// ������������ ��� ����� 0=0 1=2*Pi
		float angle_cubic_interpolation(float x, float y, bool bUtile=true, bool bVtile=true)
		{
			x -= 0.5f/Xsize;
			y -= 0.5f/Ysize;

			int iu[4], iv[4];
			float w[4][4];
			Math::cubic_interpolation(
				"catmull-rom", 
				x, y, Xsize, Ysize, 
				iu, iv,
				w, bUtile, bVtile);
			float avr = 0;
			for(int x=0; x<4; x++)
			{
				for(int y=0; y<4; y++)
				{
					int U = iu[x];
					int V = iv[y];
					float val = (*this)[V][U];
					avr += val;
				}
			}
			avr = avr/16;

			float res = 0;
			for(int x=0; x<4; x++)
			{
				for(int y=0; y<4; y++)
				{
					int U = iu[x];
					int V = iv[y];
					float val = (*this)[V][U];
					if( val < avr-T(128)) val += T(1);
					if( val > avr+T(128)) val -= T(1);
					res = (T)(res + w[x][y]*val);
				}
			}
			return res;
		}
		// 
		T* down(int iy, int ix)
		{
			if(iy>=Ysize-1) return NULL;
			return &(*this)[iy+1][ix];
		}
		T* top(int iy, int ix)
		{
			if(iy==0) return NULL;
			return &(*this)[iy-1][ix];
		}
		T* rigth(int iy, int ix)
		{
			if(ix>=Xsize-1) return NULL;
			return &(*this)[iy][ix+1];
		}
		T* left(int iy, int ix)
		{
			if(ix==0) return NULL;
			return &(*this)[iy][ix-1];
		}

		//
		T* toprigth(int iy, int ix)
		{
			if(iy==0) return NULL;
			if(ix>=Xsize-1) return NULL;
			return &(*this)[iy-1][ix+1];
		}
		T* downrigth(int iy, int ix)
		{
			if(iy>=Ysize-1) return NULL;
			if(ix>=Xsize-1) return NULL;
			return &(*this)[iy+1][ix+1];
		}
		T* topleft(int iy, int ix)
		{
			if(iy==0) return NULL;
			if(ix==0) return NULL;
			return &(*this)[iy-1][ix-1];
		}
		T* downleft(int iy, int ix)
		{
			if(iy>=Ysize-1) return NULL;
			if(ix==0) return NULL;
			return &(*this)[iy+1][ix-1];
		}
		float average()
		{
			double sum = 0;
			int resX = this->sizeX(); 
			int resY = this->sizeY();
			for(int x=0; x<resX; x++)
			{
				for(int y=0; y<resY; y++)
				{
					float d = (*this)[y][x]/255.f;
					sum += d;
				}
			}
			sum = sum/(resX*resY);
			return sum;
		}

		#ifdef OCELLARIS
		void serializeOcs(
			const char* _name, 
			cls::IRender* render, 
			bool bSave
			);
		#endif
	};
}

/**
 * Write/Load components to/from a stream.
 */
template<class T, class Stream> inline
Stream& operator >> (Stream& out, Math::matrixMxN<T>& v)
{
	
	if( out.isSaving())
	{
		out >> v.Xsize >> v.Ysize;
	}
	else
	{
		int Xsize, Ysize;
		out >> Xsize >> Ysize;
		v.init(Xsize, Ysize);
	}
	int s = v.Xsize*v.Ysize;
	for(int i=0; i<s; i++)
		out >> v._data[i];
	return out;
}

#ifdef OCELLARIS
template <class T> inline 
void Math::matrixMxN<T>::serializeOcs(
	const char* _name, 
	cls::IRender* render, 
	bool bSave
	)
{
	std::string name = _name;
	if(bSave)
	{
		cls::PA<int> resulution(cls::PI_CONSTANT, 2);
		resulution[0] = this->Xsize;
		resulution[1] = this->Ysize;
		int s = this->Xsize*this->Ysize;
		cls::PA<T> value(cls::PI_CONSTANT, this->_data, s);
		render->Parameter( (name+"::resolution").c_str(), resulution);
		render->Parameter( (name+"::value").c_str(), value);
	}
	else
	{
		cls::PA<int> resulution = render->GetParameter( (name+"::resolution").c_str());
		cls::PA<T> value = render->GetParameter( (name+"::value").c_str());
		if( resulution.size()!=2)
			return;
		int X = resulution[0];
		int Y = resulution[1];
		if(value.size()!=X*Y)
			return;

		this->init(X, Y);
		int s = this->Xsize*this->Ysize;
		for(int i=0; i<s; i++)
			this->_data[i] = value[i];
	}
}
#endif
