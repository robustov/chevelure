#pragma once 

namespace Math
{
	inline bool Bresenham2(Math::Vec2i v1, Math::Vec2i v2, std::vector<Math::Vec2i>& res) 
	{
		bool bSwap = false;
		Math::Vec2i Delta = v2 - v1;
		if( Delta == Math::Vec2i(0) )
		{
			res.push_back(v1);
			return false;
		}
		// ������������� x>y
		if(abs(Delta.x)<abs(Delta.y))
		{
			bSwap = true;
			std::swap(v1.x, v1.y);
			std::swap(v2.x, v2.y);
			std::swap(Delta.x, Delta.y);
		}
		int XDirection = 1;
		if(Delta.x<0) 
		{
			XDirection=-1;
			Delta.x = -Delta.x;
		}
		int YDirection = 1;
		if(Delta.y<0) 
		{
			Delta.y = -Delta.y;
			YDirection=-1;
		}

		Math::Vec2i XY = v1;
		int DYx2; 
		int DYx2MinusDXx2; 
		int ErrorTerm; 

		// ��������� ��������� ������ ���������� � ��������, 
		// ������������ �� ���������� ����� 
		DYx2=Delta.y*2; 
		DYx2MinusDXx2= DYx2 - Delta.x*2; 
		ErrorTerm = DYx2 - Delta.x;// ������ 

		res.reserve(Delta.x+1);
		res.push_back(XY);

		while(Delta.x--) 
		{ 
			if(ErrorTerm >=0) 
			{
 				// ������ �� ��������������� ��� 
				// � ����������� ������ ���������� 
				XY.y += YDirection; 
				ErrorTerm +=DYx2MinusDXx2; 
			} 
			else 
			{ 
				// ��������� ������ ���������� 
				ErrorTerm += DYx2; 
			} 
			// ��������� �� �������� ���
			XY.x += XDirection; 
			// ������ ����� 
			res.push_back( XY);
		}

		if(bSwap)
		{
			for(int i=0; i<(int)res.size(); i++)
				std::swap( res[i].x, res[i].y);
		}
		return true;
	}
	inline bool Bresenham3(Math::Vec3i v1, Math::Vec3i v2, std::vector<Math::Vec3i>& res) 
	{
		Math::Vec3i Delta = v2 - v1;
		Delta.x = abs(Delta.x);
		Delta.y = abs(Delta.y);
		Delta.z = abs(Delta.z);
		if(Delta.x>=Delta.y && Delta.x>=Delta.z)
		{
			// X max
			std::vector<Math::Vec2i> resxy, resxz;
			Bresenham2(Math::Vec2i( v1.x, v1.y), Math::Vec2i( v2.x, v2.y), resxy);
			Bresenham2(Math::Vec2i( v1.x, v1.z), Math::Vec2i( v2.x, v2.z), resxz);
			res.resize(resxy.size());
			for(int i=0; i<(int)res.size(); i++)
			{
				res[i].x = resxy[i].x;
				res[i].y = resxy[i].y;
				res[i].z = resxz[i].y;
			}
			return true;
		}
		if(Delta.y>=Delta.x && Delta.y>=Delta.z)
		{
			// Y max
			std::vector<Math::Vec2i> resyx, resyz;
			Bresenham2(Math::Vec2i( v1.y, v1.x), Math::Vec2i( v2.y, v2.x), resyx);
			Bresenham2(Math::Vec2i( v1.y, v1.z), Math::Vec2i( v2.y, v2.z), resyz);
			res.resize(resyx.size());
			for(int i=0; i<(int)res.size(); i++)
			{
				res[i].x = resyx[i].y;
				res[i].y = resyx[i].x;
				res[i].z = resyz[i].y;
			}
			return true;
		}
		else
		{
			// Z max
			std::vector<Math::Vec2i> reszx, reszy;
			Bresenham2(Math::Vec2i( v1.z, v1.x), Math::Vec2i( v2.z, v2.x), reszx);
			Bresenham2(Math::Vec2i( v1.z, v1.y), Math::Vec2i( v2.z, v2.y), reszy);
			res.resize(reszx.size());
			for(int i=0; i<(int)res.size(); i++)
			{
				res[i].x = reszx[i].y;
				res[i].y = reszy[i].y;
				res[i].z = reszx[i].x;
			}
			return true;
		}
		return true;
	}

	/*/
	inline void Octant0(int X0,int Y0,int DeltaX,int DeltaY,int XDirection) 
	{ 
		 DeltaX -- ������� �� ��� X 
			DeltaY -- �������� �� ��� Y 
			XDirection -- ����������� �������� 

		��� ������ ������� |DeltaX|>DeltaY 
		������������� �������� ��� X, a 
		�������a������� Y 

		int DYx2; 
		int DYx2MinusDXx2; 
		int ErrorTerm; 
		/* ��������� ��������� ������ ���������� � ��������, 
			������������ �� ���������� ����� 

		DYx2=DY*2; 
		DYx2MinusDXx2= DYx2 - DX*2; 
		ErrorTerm = DYx2 - DX;// ������ 

		/* ������ ������ ����� 
		setZPixel(X0,Y0,getZPixel(DeltaX),Color); 

		while(DeltaX--) 
		{ 
			if(ErrorTerm >=0) 
			{
 			/* ������ �� ��������������� ��� 
				� ����������� ������ ���������� 
				Y0++; 
				ErrorTerm+=DeltaYx2MinusDeltaXx2; 
			} 
			else 
			{ 
			/* ��������� ������ ���������� 
				ErrorTerm+= DeltaYx2; 
			} 
		/* ��������� �� �������� ��� 
			X0+= XDirection; 
		/* ������ ����� 
			setZPixel(X0,Y0,getZPixel(DeltaX),Color); 
		}
	} 
	/*/
}
