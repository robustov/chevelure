#ifndef _ED_Math_Plane_h_
#define _ED_Math_Plane_h_
#pragma once


#include "./_config.h"
#include "./_math.h"
#include "./Matrix.h"
#include "./Box.h"

namespace Math {

/**
 * N-dimensional Plane (Line).
 */
template<int N, class T>
class Plane {
public:
	typedef Plane<N,T> self_t;
	typedef Vector<N,T> vector_t;
	typedef T scalar_t;

	vector_t n;
	scalar_t d;

	enum { Size = N };

		/**
		* Create uninitialized plane.
		*/
		Plane() {}

		/**
		 * Create plane (_n, _d)
		 */
		Plane(const vector_t& _n, const scalar_t& _d) : n(_n), d(_d) {} 

		/**
		 * Create Plane<2,T>.
		 */
		Plane(T x, T y, T _d) : n(x,y), d(_d) {}

		/**
		 * Create Plane<3,T>.
		 */
		Plane(T x, T y, T z, T _d) : n(x,y,z), d(_d) {}

		/**
		 * Create by point and normal.
		 */
		Plane(const vector_t& p, const vector_t& _n) : n(_n) {d = -dot(n,p);}

		/**
		 * Generic copy constructor.
		 */
		template<class F>
		Plane(const Plane<N, F>& u) : n(u.n), d(u.d) {}

	/**
	* Set plane to (_n, _d)
	*/
	self_t& set(const vector_t& _n, const scalar_t& _d) {n = _n; d = _d; return *this;} 

	/**
	 * Set Plane<2,T> components.
	 */
	self_t& set(T x, T y, T _d) { n.set(x,y); d = _d; return *this; }

	/**
	 * Set Plane<3,T> components.
	 */
	self_t& set(T x, T y, T z, T _d) { n.set(x,y,z); d = _d; return *this; }

	/**
	 * Set by point and normal.
	 */
	self_t& set(const vector_t& p, const vector_t& _n) {n = _n; d = -dot(n*p); return *this;}

	/**
	 * Get number of components.
	 */
	static int size() { return N; }

	/**
	 * Get distance to point.
	 */
	scalar_t distance(const vector_t& p) const {return dot(n,p) + d;}

	/**
	 * Get distance to box.
	 */
	scalar_t distance(const Box<N,T>& b) const
	{
		scalar_t m = d, M = d;
		for(int i=0; i<N; i++)
		{
			if(n[i] > 0) {M += b.max[i]*n[i]; m += b.min[i]*n[i];}
			else {M += b.min[i]*n[i]; m += b.max[i]*n[i];}
		}
		if(m > 0) return m;
		if(M < 0) return M;
		return 0;
	}

	/** 
	 * Normalize.
	 */
	self_t& normalize() {T l = 1.f/n.length(); n /= l; d /= l; return *this;}

	// operators
	self_t operator -() const {return self_t(-n, -d);}

	self_t& operator *= (const Matrix<N,T>& M) {n *= M; return *this;}
	self_t& operator *= (const Matrix<N+1, T>& M) {n *= asMatrix<N>(M); d -= dot(n, M[N].as<3>()); return *this;}

	self_t operator * (const Matrix<N,T>& M) const {self_t P = *this; P *= M; return P;}
	self_t operator * (const Matrix<N+1, T>& M) const {self_t p = *this; P *= M; return P;}

}; // class Plane<N, T>

// common typedefs
typedef Plane<2, float> Line2f;
typedef Plane<3, float> Plane3f;

typedef Plane<2, double> Line2d;
typedef Plane<3, double> Plane3d;

}

/**
 * Write/Load components to/from a stream.
 */
template<int N, class T, class Stream> inline
Stream& operator >> (Stream& out, Math::Plane<N, T>& v)
{
	out >> v.n >> v.d;
	return out;
}

#endif /* _ED_Math_Plane_h_ */
