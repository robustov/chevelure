#pragma once


#include "./_config.h"
#include "./_VectorBase.h"
#include "./_math.h"
#include "../Util/CTAssert.h"

namespace Math 
{

	template <class T>
	struct Quad
	{
		T verts[4];
	public:
		void resize(int vertcount){}
		const T& operator[](int i)const
		{
			return verts[i];
		}
		T& operator[](int i)
		{
			return verts[i];
		}
		const T& cycle(int i) const
		{
			if(i<0) i += size()*(-i/size() + 2);
			return verts[i % (size())];
		}
		int size() const {return 4;}

		// ����� ������� � ��������
		int getVertexIndex(int v)
		{
			if(verts[0]==v) return 0;
			if(verts[1]==v) return 1;
			if(verts[2]==v) return 2;
			return 3;
		}
	};
	typedef Quad<unsigned long>  Quad32;
	typedef Quad<unsigned short> Quad16;

}

/**
 * Write/Load components to/from a stream.
 */
template<class T, class Stream> inline
Stream& operator >> (Stream& out, Math::Quad<T>& v)
{
	out >> v.verts[0] >> v.verts[1] >> v.verts[2] >> v.verts[3];
	return out;
}

