#pragma once
#include <stdlib.h>
#include <math.h>
namespace Math
{

inline void align(float& u, float& du, int& iu0, int& iu1, int countU, bool bUtile)
{
	int iu;
	if( bUtile)
	{
		u+=-floor(u);
		iu = (int)floor(u * countU);
		du = u*countU - iu;
		iu = iu % countU;
		iu0 = iu;
		iu1 = (iu+1)%countU;
	}
	else
	{
		u = __max( __min(u, (float)1), (float)0);
		iu = (int)floor(u * countU);
		iu = __max( __min(iu, countU-1), 0);
		du = u*countU - iu;
		iu0 = iu;
		iu1 = __min(iu+1, countU-1);
	}
}
inline int align_norma(int u, int countU)
{
	if(u<0) 
		u = countU - (-u%countU);
	return u%countU;
}
inline void align(float& u, float& du, int indices[4], int countU, bool bUtile)
{
	int iu;
	if( bUtile)
	{
		u+=-floor(u);
		iu = (int)floor(u * countU);
		du = u*countU - iu;
		iu = iu % countU;

		indices[0] = (iu-1+countU)%countU;
		indices[1] = iu;
		indices[2] = (iu+1)%countU;
		indices[3] = (iu+2)%countU;
	}
	else
	{
		u = __max( __min(u, (float)1), (float)0);
		iu = (int)floor(u * countU);
		iu = __max( __min(iu, countU-1), 0);
		du = u*countU - iu;
		indices[0] = __max(iu-1, 0);
		indices[1] = iu;
		indices[2] = __min(iu+1, countU-1);
		indices[3] = __min(iu+2, countU-1);
	}
}

// ���������� ������������
inline void bilinear_interpolation(
	float u, float v,
	int countU, int countV,
	int& iu0, int& iu1, int& iv0, int& iv1,
	float& w00, float& w01, float& w10, float& w11, 
	bool bUtile=true, bool bVtile=true)
{ 
	float du, dv;
	align(u, du, iu0, iu1, countU, bUtile);
	align(v, dv, iv0, iv1, countV, bVtile);

	w00 = (1-du)*(1-dv);
	w01 = (1-du)*(dv);
	w10 = (du)*(1-dv);
	w11 = (du)*(dv);
}

// �������� ������������
inline void interpolation(
	float u, float v,
	int countU, int countV,
	int& iu0, int& iu1, int& iv0, int& iv1,
	float& w00, float& w01, float& w10, float& w11, 
	bool bUtile=true, bool bVtile=true)
{
	float du, dv;
	align(u, du, iu0, iu1, countU, bUtile);
	align(v, dv, iv0, iv1, countV, bVtile);

	w00 = w01 = w10 = w11 = 0;
	if(du>dv)
	{
		w00 = 1-du;
		w10 = du-dv;
		w11 = dv;
	}
	else
	{
		w00 = 1-dv;
		w01 = dv-du;
		w11 = du;
	}
}

inline void catmull_rom(float t, float c[4])
{
	float s = 1-t;
	float t2 = t*t;
	float t3 = t*t2;
	c[0] = 0.5f * (-t*s*s);
	c[1] = 0.5f * (2 - 5*t2 + 3*t3);
	c[2] = 0.5f * (1 + 4*t - 3*t2) * t;
	c[3] = 0.5f * (-t2*s);
}

// ������ ����. ���������� ������������ 1D
inline void cubic_interpolation(
	const char* basis, 
	float x, int Xsize,
	int iu[4],
	float w0[4], 
	bool bUtile)
{
	float du;
	align(x, du, iu, Xsize, bUtile);

	catmull_rom(du, w0);
}

// ������ ����. ���������� ������������ 2D
inline void cubic_interpolation(
	const char* basis, 
	float x, float y, int Xsize, int Ysize, 
	int iu[4], int iv[4],
	float w[4][4], 
	bool bUtile, bool bVtile)
{
	float du, dv;
	align(x, du, iu, Xsize, bUtile);
	align(y, dv, iv, Ysize, bVtile);

	float cu[4], cv[4];
	catmull_rom(du, cu);
	catmull_rom(dv, cv);

	for(int ix=0; ix<4; ix++)
		for(int iy=0; iy<4; iy++)
			w[ix][iy] = cu[ix]*cv[iy];

}

}

