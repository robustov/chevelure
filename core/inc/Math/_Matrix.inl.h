#ifndef _ED_Math_Matrix_h_
#error Matrix.inl.h should not be included directly
#endif


// implementation
#ifdef ED_MATH_IMPLEMENT_MATRIX

template<int N, typename T>
Matrix<N,T>& Matrix<N,T>::set(T val)
{
	for(int i=0; i<N; ++i)
		for(int j=0; j<N; ++j)
			m[i][j] = i==j ? val : T(0);
	return *this;
}

template<int N, class T>
Matrix<N,T>& Matrix<N,T>::transpose()
{
	for(int i=1; i<N; ++i)
	{
		for(int j=0; j<i; ++j)
		{
			T tmp = m[i][j];
			m[i][j] = m[j][i];
			m[j][i] = tmp;
		}
	}
	return *this;
}

template<int N, typename T>
ED_MATH_EXTERN void ED_MATH_API transpose(Matrix<N,T>& r, const Matrix<N,T>& m)
{
	for(int i=0; i<N; ++i)
		for(int j=0; j<N; ++j)
			r[j][i] = m[i][j];
}

// matrix by matrix
template<int N, typename T>
ED_MATH_EXTERN void ED_MATH_API multiply(Matrix<N,T>& r, const Matrix<N,T>& m1, const Matrix<N,T>& m2)
{
	for(int i=0; i<N; ++i)
	{
		for(int j=0; j<N; ++j)
		{
			r[i][j] = m1[i][0] * m2[0][j];
			for(int k=1; k<N; ++k)
				r[i][j] += m1[i][k] * m2[k][j];
		}
	}
}

/*/
template<typename T>
ED_MATH_EXTERN void ED_MATH_API invert(Matrix<2,T>& r, const Matrix<2,T>& m)
{
	// TBI
}

template<typename T>
ED_MATH_EXTERN void ED_MATH_API invert(Matrix<3,T>& r, const Matrix<3,T>& m)
{
	// TBI
}
/*/

template<typename T>
ED_MATH_EXTERN void ED_MATH_API invert(Matrix<2,T>& r, const Matrix<2,T>& m)
{
}
template<typename T>
ED_MATH_EXTERN void ED_MATH_API invert(Matrix<3,T>& r, const Matrix<3,T>& m)
{
	r[0][0] =  (m[1][1]*m[2][2] - m[1][2]*m[2][1]);
	r[1][0] = -(m[1][0]*m[2][2] - m[1][2]*m[2][0]);
	r[2][0] =  (m[1][0]*m[2][1] - m[1][1]*m[2][0]);

	r[0][1] = -(m[0][1]*m[2][2] - m[0][2]*m[2][1]);
	r[1][1] =  (m[0][0]*m[2][2] - m[0][2]*m[2][0]);
	r[2][1] = -(m[0][0]*m[2][1] - m[0][1]*m[2][0]);

	r[0][2] =  (m[0][1]*m[1][2] - m[0][2]*m[1][1]);
	r[1][2] = -(m[0][0]*m[1][2] - m[0][2]*m[1][0]);
	r[2][2] =  (m[0][0]*m[1][1] - m[0][1]*m[1][0]);

	T d = (T)(1/determinant(m));
	r[0] *= d;
	r[1] *= d;
	r[2] *= d;
}
template<typename T>
ED_MATH_EXTERN void ED_MATH_API invert(Matrix<4,T>& r, const Matrix<4,T>& m)
{
	r[0][0] = m[1][2]*m[2][3]*m[3][1] - m[1][3]*m[2][2]*m[3][1] + m[1][3]*m[2][1]*m[3][2] - m[1][1]*m[2][3]*m[3][2] - m[1][2]*m[2][1]*m[3][3] + m[1][1]*m[2][2]*m[3][3];    
	r[0][1] = m[0][3]*m[2][2]*m[3][1] - m[0][2]*m[2][3]*m[3][1] - m[0][3]*m[2][1]*m[3][2] + m[0][1]*m[2][3]*m[3][2] + m[0][2]*m[2][1]*m[3][3] - m[0][1]*m[2][2]*m[3][3];    
	r[0][2] = m[0][2]*m[1][3]*m[3][1] - m[0][3]*m[1][2]*m[3][1] + m[0][3]*m[1][1]*m[3][2] - m[0][1]*m[1][3]*m[3][2] - m[0][2]*m[1][1]*m[3][3] + m[0][1]*m[1][2]*m[3][3];    
	r[0][3] = m[0][3]*m[1][2]*m[2][1] - m[0][2]*m[1][3]*m[2][1] - m[0][3]*m[1][1]*m[2][2] + m[0][1]*m[1][3]*m[2][2] + m[0][2]*m[1][1]*m[2][3] - m[0][1]*m[1][2]*m[2][3];
	r[1][0] = m[1][3]*m[2][2]*m[3][0] - m[1][2]*m[2][3]*m[3][0] - m[1][3]*m[2][0]*m[3][2] + m[1][0]*m[2][3]*m[3][2] + m[1][2]*m[2][0]*m[3][3] - m[1][0]*m[2][2]*m[3][3];
	r[1][1] = m[0][2]*m[2][3]*m[3][0] - m[0][3]*m[2][2]*m[3][0] + m[0][3]*m[2][0]*m[3][2] - m[0][0]*m[2][3]*m[3][2] - m[0][2]*m[2][0]*m[3][3] + m[0][0]*m[2][2]*m[3][3];
	r[1][2] = m[0][3]*m[1][2]*m[3][0] - m[0][2]*m[1][3]*m[3][0] - m[0][3]*m[1][0]*m[3][2] + m[0][0]*m[1][3]*m[3][2] + m[0][2]*m[1][0]*m[3][3] - m[0][0]*m[1][2]*m[3][3];
	r[1][3] = m[0][2]*m[1][3]*m[2][0] - m[0][3]*m[1][2]*m[2][0] + m[0][3]*m[1][0]*m[2][2] - m[0][0]*m[1][3]*m[2][2] - m[0][2]*m[1][0]*m[2][3] + m[0][0]*m[1][2]*m[2][3];
	r[2][0] = m[1][1]*m[2][3]*m[3][0] - m[1][3]*m[2][1]*m[3][0] + m[1][3]*m[2][0]*m[3][1] - m[1][0]*m[2][3]*m[3][1] - m[1][1]*m[2][0]*m[3][3] + m[1][0]*m[2][1]*m[3][3];
	r[2][1] = m[0][3]*m[2][1]*m[3][0] - m[0][1]*m[2][3]*m[3][0] - m[0][3]*m[2][0]*m[3][1] + m[0][0]*m[2][3]*m[3][1] + m[0][1]*m[2][0]*m[3][3] - m[0][0]*m[2][1]*m[3][3];
	r[2][2] = m[0][1]*m[1][3]*m[3][0] - m[0][3]*m[1][1]*m[3][0] + m[0][3]*m[1][0]*m[3][1] - m[0][0]*m[1][3]*m[3][1] - m[0][1]*m[1][0]*m[3][3] + m[0][0]*m[1][1]*m[3][3];
	r[2][3] = m[0][3]*m[1][1]*m[2][0] - m[0][1]*m[1][3]*m[2][0] - m[0][3]*m[1][0]*m[2][1] + m[0][0]*m[1][3]*m[2][1] + m[0][1]*m[1][0]*m[2][3] - m[0][0]*m[1][1]*m[2][3];
	r[3][0] = m[1][2]*m[2][1]*m[3][0] - m[1][1]*m[2][2]*m[3][0] - m[1][2]*m[2][0]*m[3][1] + m[1][0]*m[2][2]*m[3][1] + m[1][1]*m[2][0]*m[3][2] - m[1][0]*m[2][1]*m[3][2];
	r[3][1] = m[0][1]*m[2][2]*m[3][0] - m[0][2]*m[2][1]*m[3][0] + m[0][2]*m[2][0]*m[3][1] - m[0][0]*m[2][2]*m[3][1] - m[0][1]*m[2][0]*m[3][2] + m[0][0]*m[2][1]*m[3][2];
	r[3][2] = m[0][2]*m[1][1]*m[3][0] - m[0][1]*m[1][2]*m[3][0] - m[0][2]*m[1][0]*m[3][1] + m[0][0]*m[1][2]*m[3][1] + m[0][1]*m[1][0]*m[3][2] - m[0][0]*m[1][1]*m[3][2];
	r[3][3] = m[0][1]*m[1][2]*m[2][0] - m[0][2]*m[1][1]*m[2][0] + m[0][2]*m[1][0]*m[2][1] - m[0][0]*m[1][2]*m[2][1] - m[0][1]*m[1][0]*m[2][2] + m[0][0]*m[1][1]*m[2][2];
//	r = r*(1/determinant(m));

	T d = (T)(1/determinant(m));
	r[0] *= d;
	r[1] *= d;
	r[2] *= d;
	r[3] *= d;
}

// invert orthogonal matrix
template<int N, typename T>
ED_MATH_EXTERN void ED_MATH_API invertOrtho(Matrix<N,T>& r, const Matrix<N,T>& m)
{
	int i;
	for(i=0; i<N-1; ++i)
	{
		for(int j=0; j<N-1; ++j)
			r[j][i] = m[i][j];
		r[i][N-1] = T(0);
		r[N-1][i] = T(0);
	}

	for(i=0; i<N-1; ++i)
	{
		T l2 = length2( r.m[i] );
		for(int j=0; j<N-1; ++j)
			r[i][j] /= l2;
	}

	r.col(N-1) = -(r * Vector<N,T>(m.col(N-1)));
	r.m[N-1][N-1] = T(1);
}

inline double determinant(const Matrix<3,float>& m) 
{
	double value = 
		m[0][0]*(m[1][1]*m[2][2] - m[1][2]*m[2][1]) - 
		m[0][1]*(m[1][0]*m[2][2] - m[1][2]*m[2][0]) + 
		m[0][2]*(m[1][0]*m[2][1] - m[1][1]*m[2][0]);
	return value;
}

inline double determinant(const Matrix<4,float>& m) 
{
	double value = 
		m[0][3] * m[1][2] * m[2][1] * m[3][0]-m[0][2] * m[1][3] * m[2][1] * m[3][0] - m[0][3] * m[1][1] * m[2][2] * m[3][0]+m[0][1] * m[1][3] * m[2][2] * m[3][0]+
		m[0][2] * m[1][1] * m[2][3] * m[3][0]-m[0][1] * m[1][2] * m[2][3] * m[3][0] - m[0][3] * m[1][2] * m[2][0] * m[3][1]+m[0][2] * m[1][3] * m[2][0] * m[3][1]+
		m[0][3] * m[1][0] * m[2][2] * m[3][1]-m[0][0] * m[1][3] * m[2][2] * m[3][1] - m[0][2] * m[1][0] * m[2][3] * m[3][1]+m[0][0] * m[1][2] * m[2][3] * m[3][1]+
		m[0][3] * m[1][1] * m[2][0] * m[3][2]-m[0][1] * m[1][3] * m[2][0] * m[3][2] - m[0][3] * m[1][0] * m[2][1] * m[3][2]+m[0][0] * m[1][3] * m[2][1] * m[3][2]+
		m[0][1] * m[1][0] * m[2][3] * m[3][2]-m[0][0] * m[1][1] * m[2][3] * m[3][2] - m[0][2] * m[1][1] * m[2][0] * m[3][3]+m[0][1] * m[1][2] * m[2][0] * m[3][3]+
		m[0][2] * m[1][0] * m[2][1] * m[3][3]-m[0][0] * m[1][2] * m[2][1] * m[3][3] - m[0][1] * m[1][0] * m[2][2] * m[3][3]+m[0][0] * m[1][1] * m[2][2] * m[3][3];
	return value;
}

#endif
