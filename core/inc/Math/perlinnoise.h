#pragma once

#include "Math/Math.h"

namespace Math
{
	class ED_MATH_EXTERN PerlinNoise
	{
		int* p;
		Math::Vec3f* g;
		int seed;
	public:
		PerlinNoise(int seed=0);
		void Init(int seed);

//		Math::Vec3f noise3(float vx, float vy, float vz);
		// 
		float noise(float vx, float vy, float vz);
	};


}
