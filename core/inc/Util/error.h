#ifndef _ED_Mem_Error_h_
#define _ED_Mem_Error_h_


#include "../_config.h"
#include <exception>
#include <string>
#include <stdarg.h>


namespace Util
{

//! Error - ����� ���������� ������������ � namespace Mem
class Error : public std::exception 
{
	std::string m_reason;
public:
	Error();
	Error(const char* r, ...);
	Error(const Error& arg);
	~Error() throw();

	const char* what() const throw();
	operator const char* () const;
protected:
	void Error::printf(const char* buffer, va_list argList);

};

inline Error::Error()
{
}
inline Error::Error(const Error& arg)
{
	m_reason = arg.m_reason;
}
inline Error::Error(const char* r, ...)
{
	va_list argList; va_start(argList, r);
	this->printf(r, argList);
	va_end(argList);
}
inline Error::~Error() throw()
{
}

inline const char* Error::what() const throw()
{ 
	return m_reason.c_str(); 
}
inline Error::operator const char* () const
{ 
	return m_reason.c_str(); 
}

inline void Error::printf(const char* buffer, va_list argList)
{
	m_reason = "";
	char sbuf[1024];
	_vsnprintf(sbuf, 1023, buffer, argList);
	m_reason = sbuf;
}


}

#endif /* _ED_Mem_Error_h_ */
