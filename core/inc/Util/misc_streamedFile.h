#pragma once

#include <stdio.h>
#include <map>
#include <string>
#include <vector>

namespace misc
{
	struct streamedFile
	{
	protected:
		FILE* file;
		//
		struct record
		{
			int offset;		//!< �������� �� ������
			int size;		//!< � ������
		};
		std::map< std::string, record> streams; //!< only for load

	public:
		streamedFile()
		{
			file = NULL;
		}
		~streamedFile()
		{
			close();
		}


		template <class T>
		void save(const char* token, T& data)
		{
			int strlength = (int)strlen(token)+1;
			fwrite( &strlength, sizeof(strlength), 1, file);
			fwrite( token, strlength, 1, file);
			int vol = sizeof(data);
			fwrite( &vol, sizeof(vol), 1, file);
			fwrite( &data, vol, 1, file);
		}
		template <class T>
		void save(const char* token, std::vector<T>& data)
		{
			save(token, &data[0], (int)data.size());
		}
		template <class T>
		void save(const char* token, T* data, int size)
		{
			int strlength = (int)strlen(token)+1;
			fwrite( &strlength, sizeof(strlength), 1, file);
			fwrite( token, strlength, 1, file);
			int vol = sizeof(T)*size;
			fwrite( &vol, sizeof(vol), 1, file);
			fwrite( data, vol, 1, file);
		}
		template <class T>
		bool load(const char* token, T& data)
		{
			std::map< std::string, record>::iterator it = streams.find(token);
			if( it == streams.end()) return false;
			
			int vol = sizeof(data);
			if( it->second.size != vol)
				return false;

			int pos = it->second.offset;
			int res = fseek( file, pos, SEEK_SET);
			if( res) return false;

			size_t rc = fread( &data, 1, vol, file);
			if( !rc) return false;
			return true;
		}
		template <class T>
		bool load(const char* token, std::vector<T>& data)
		{
			return load(token, &data[0], (int)data.size());
		}
		template <class T>
		bool load(const char* token, T* data, int size)
		{
			std::map< std::string, record>::iterator it = streams.find(token);
			if( it == streams.end()) return false;
			
			int vol = sizeof(T)*size;
			if( it->second.size != vol)
				return false;

			int pos = it->second.offset;
			int res = fseek( file, pos, SEEK_SET);
			if( res) return false;

			size_t rc = fread( data, 1, vol, file);
			if( !rc) return false;
			return true;
		}

		bool open(const char* filename, bool bWtite=false)
		{
			if(!bWtite)
			{
				file = fopen(filename, "rb");
				if(!file) return false;

				char buf[1024];
				while( !feof( file ) )
				{
					size_t rc;
					int strlength;
					rc = fread( &strlength, sizeof(strlength), 1, file);
					if( !rc) break;
					rc = fread( buf, strlength, 1, file);
					if( !rc) break;

					int next;
					rc = fread( &next, sizeof(next), 1, file);
					if( !rc) break;
					int pos = ftell(file);

					record r;
					r.offset = pos;
					r.size   = next;
					streams[buf] = r;

					if( fseek( file, next, SEEK_CUR))
						break;
				}
			}
			else
			{
				file = fopen(filename, "wb");
				if(!file) return false;
			}
			return true;
		}
		void close()
		{
			if(file)
				fclose(file);
			file = NULL;
			streams.clear();
		}
	};
}

#define MISC_SAVE(file, what) ( file.save("" #what "", what))
#define MISC_SAVEA(file, what, SZ) ( file.save("" #what "", what, SZ))
#define MISC_LOAD(file, what) ( file.load("" #what "", what))
#define MISC_LOADA(file, what, SZ) ( file.load("" #what "", what, SZ))
