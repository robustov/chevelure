#pragma once

#pragma warning(disable : 4200 )
#pragma warning(disable : 327 )

#include <windows.h>
#include "../Util/error.h"
#include "../Util/misc_mmf_wr.h"
//#include "misc/misc_ptr.h"
namespace math
{
	// ������� �������
	template <class T> class matrixMN
	{
	public:
		class row
		{
			int rowsize;
			int size;
			T array[];
		public:
			row(int i)
			{
				size=i;
			}
			T& operator [](int i)
			{
				if(i<0 || i>=size) 
					throw Util::Error("matrixMN: i=%d, index<0 or index>=%d", i, size);
				return array[i];
			}
		};
		void init(int i, int j, misc::mmf_write* file=NULL)
		{
			rowsize = i;
			size=j;
			if( data) 
				delete (char*)data;
			data = NULL;
			if( i==0 || j==0) return;
			int s = size*( sizeof(row) + rowsize*sizeof(T));
			if( !file)
				data = new char[ s ];
			else
				data = new (*file) char[ s ];
			memset(data, 0, s);
			for(int ii=0; ii<j; ii++)
				(*this)[ii].row::row(rowsize);
		}

	public:
		int size, rowsize;
		misc::ptr<char> data;
		row blancrow;

	public:
		matrixMN() : blancrow(0){data=0; init(0, 0);}
		matrixMN(const matrixMN<T>& arg) : blancrow(0)
		{
			data=0; 
			init(arg.xSize(), arg.ySize());
		}
		matrixMN(int iX, int jY) : blancrow(0)
		{
			data=0; 
			init(iX, jY);
		}
		~matrixMN()
		{
			delete (char*) data;
		}
		row& operator [](int j)
		{
			if(j<0 || j>=size) 
				throw Util::Error("matrixMN: j=%d, index<0 or index>=%d", j, size);
			row* prow = (row*)( (char*)data + j*( sizeof(row)+rowsize*sizeof(T)));
			return *prow;
		}
		matrixMN<T>& operator=(matrixMN<T>& arg);

		int xSize()const{return rowsize;}
		int ySize()const{return size;}

		// 
		T* down(int iy, int ix)
		{
			if(iy>=size-1) return NULL;
			return &(*this)[iy+1][ix];
		}
		T* top(int iy, int ix)
		{
			if(iy==0) return NULL;
			return &(*this)[iy-1][ix];
		}
		T* rigth(int iy, int ix)
		{
			if(ix>=rowsize-1) return NULL;
			return &(*this)[iy][ix+1];
		}
		T* left(int iy, int ix)
		{
			if(ix==0) return NULL;
			return &(*this)[iy][ix-1];
		}

		//
		T* toprigth(int iy, int ix)
		{
			if(iy==0) return NULL;
			if(ix>=rowsize-1) return NULL;
			return &(*this)[iy-1][ix+1];
		}
		T* downrigth(int iy, int ix)
		{
			if(iy>=size-1) return NULL;
			if(ix>=rowsize-1) return NULL;
			return &(*this)[iy+1][ix+1];
		}
		T* topleft(int iy, int ix)
		{
			if(iy==0) return NULL;
			if(ix==0) return NULL;
			return &(*this)[iy-1][ix-1];
		}
		T* downleft(int iy, int ix)
		{
			if(iy>=size-1) return NULL;
			if(ix==0) return NULL;
			return &(*this)[iy+1][ix-1];
		}

		// ��������� ����������� ����������
		void set(T value);
		// ���. � ����. ��������
		void GetMinMax(
			T& maxheigth, 
			T& minheigth);
		// ������� ��������
		T GetAverage();

		// ������������� � BMP
		void convert_to_grayscale_bmp(
			const char* filename, 
			T maxvalue, T minvalue);
		// ������������� � 16bit row
		void convert_to_grayscale_row(
			const char* filename, 
			T maxvalue, T minvalue);
		// �������� � ����
		bool save_to_file(
			const char* filename);
		// ��������� �� �����
		bool load_from_file(
			const char* filename);
	};
}

namespace math
{
	// ��������� ����������� ����������
	template <class T> 
	void matrixMN<T>::set(T value)
	{
		int xsize = rowsize;
		int ysize = size;
		int iy, ix;
		for(iy = 0; iy<ysize; iy++)
			for(ix = 0; ix<xsize; ix++)
				(*this)[iy][ix] = value;
	}

	template <class T> 
	matrixMN<T>& matrixMN<T>::operator=(matrixMN<T>& arg)
	{
		delete (char*) data;
		data = NULL;
		init( arg.rowsize, arg.size);
		int xsize = rowsize;
		int ysize = size;
		int iy, ix;
		for(iy = 0; iy<ysize; iy++)
			for(ix = 0; ix<xsize; ix++)
				(*this)[iy][ix] = arg[iy][ix];
		return *this;
	}

	// ���. � ����. ��������
	template <class T> 
	void matrixMN<T>::GetMinMax(
		T& maxheigth, 
		T& minheigth)
	{
		int xsize = rowsize;
		int ysize = size;
		maxheigth = (*this)[0][0];
		minheigth = (*this)[0][0];
		int iy, ix;
		for(iy = 0; iy<ysize; iy++)
		{
			for(ix = 0; ix<xsize; ix++)
			{
				T cur = (*this)[iy][ix];
				if( cur>maxheigth) maxheigth = cur;
				if( cur<minheigth) minheigth = cur;
			}
		}
	}
	// ������� ��������
	template <class T> 
	T matrixMN<T>::GetAverage()
	{
		int xsize = rowsize;
		int ysize = size;
		T t = (*this)[0][0];
		int iy, ix;
		for(iy = 0; iy<ysize; iy++)
		{
			for(ix = 0; ix<xsize; ix++)
			{
				if(iy==0 && ix==0) continue;
				T cur = (*this)[iy][ix];
				t += cur;
			}
		}
		t = t/(ysize*xsize);
		return t;
	}

	// ������������� � BMP
	template <class T> 
	void matrixMN<T>::convert_to_grayscale_bmp(const char* filename, T maxvalue, T minvalue)
	{
		FILE *bmpptr = 0;
		unsigned char *BitmapIndexes= 0;
		try
		{
			int NumPointsX = this->rowsize; 
			int NumPointsY = this->size;

			int NumPointsXAligned = NumPointsX;
			if (NumPointsX%4 != 0) NumPointsXAligned = (NumPointsX/4 + 1)*4;

			BITMAPFILEHEADER bHeader;
			bHeader.bfType      = 19778;
			bHeader.bfSize      = sizeof(BITMAPFILEHEADER) + 
								  sizeof(BITMAPINFOHEADER) +
								  sizeof(RGBQUAD)*256 +
								  NumPointsXAligned*NumPointsY;
			bHeader.bfReserved1 = 0;
			bHeader.bfReserved2 = 0;
			bHeader.bfOffBits   = 1078;
			BITMAPINFOHEADER biHeader;
			biHeader.biSize          = 40;
			biHeader.biWidth         = NumPointsX;
			biHeader.biHeight        = NumPointsY;
			biHeader.biPlanes        = 1;
			biHeader.biBitCount      = 8;
			biHeader.biCompression   = 0;
			biHeader.biSizeImage     = 0;
			biHeader.biXPelsPerMeter = 99962;
			biHeader.biYPelsPerMeter = 99962;
			biHeader.biClrUsed       = 0;
			biHeader.biClrImportant  = 0;
			RGBQUAD rgbq[256];
			int i, j;
			for (i = 0; i < 256; i++)
			{
				rgbq[i].rgbBlue     = (BYTE)i;
				rgbq[i].rgbGreen    = (BYTE)i;
				rgbq[i].rgbRed      = (BYTE)i;
				rgbq[i].rgbReserved = 0;
			}
			bmpptr = fopen(filename, "wb");
			if (!bmpptr) 
				throw "Can't open file %s";
				//throw misc::error("Can't open file %s", filename);

			fwrite(&bHeader, sizeof(BITMAPFILEHEADER), 1, bmpptr);
			fwrite(&biHeader, sizeof(BITMAPINFOHEADER), 1, bmpptr);
			fwrite(rgbq, sizeof(RGBQUAD), 256, bmpptr);

			BitmapIndexes = new unsigned char [NumPointsXAligned];
			for (i = 0; i < NumPointsY; i++)
			{
				for (j = 0; j < NumPointsX; j++)
				{
					T res = (*this)[i][j];
					res -= minvalue;
					if( res<0) res = 0;
					res = res/(maxvalue-minvalue);
					int ires = res*256;
					if(ires>=256) ires=255;
					BitmapIndexes[j] = ires;
				}
				for (j = NumPointsX; j < NumPointsXAligned; j++)
				{
					BitmapIndexes[j] = 0;
				}
				fwrite(BitmapIndexes, sizeof(unsigned char), NumPointsXAligned, bmpptr);
			}

			if( BitmapIndexes) delete [] BitmapIndexes;
			if(bmpptr) fclose(bmpptr);
		}
		catch(...)
		{
			if( BitmapIndexes) delete [] BitmapIndexes;
			if(bmpptr) fclose(bmpptr);
			throw;
		}
	}
	// ������������� � 16bit row
	template <class T> 
	void matrixMN<T>::convert_to_grayscale_row(const char* filename, T maxvalue, T minvalue)
	{
		FILE *bmpptr = 0;
		unsigned char *BitmapIndexes= 0;
		try
		{
			int NumPointsX = this->rowsize; 
			int NumPointsY = this->size;
			bmpptr = fopen(filename, "wb");
			if (!bmpptr) throw misc::error("Can't open file %s", filename);
			for (int i = NumPointsY-1; i >= 0; i--)
			{
				for (int j = 0; j < NumPointsX; j++)
				{
					T res = (*this)[i][j];
					res -= minvalue;
					if( res<0) res = 0;
					res = res/(maxvalue-minvalue);
					int ires = res*0x10000;
					unsigned short data = ires;

					fwrite(&data, sizeof(data), 1, bmpptr);
				}
			}
			if(bmpptr) fclose(bmpptr);
		}
		catch(...)
		{
			if( BitmapIndexes) delete [] BitmapIndexes;
			if(bmpptr) fclose(bmpptr);
			throw;
		}
	}

	// �������� � ����
	template <class T> 
	bool matrixMN<T>::save_to_file(
		const char* filename)
	{
		FILE *bmpptr = 0;
		unsigned char *BitmapIndexes= 0;
		try
		{
			int NumPointsX = this->rowsize; 
			int NumPointsY = this->size;
			bmpptr = fopen(filename, "wb");
			if (!bmpptr) throw misc::error("Can't open file %s", filename);

			fwrite(&NumPointsX, sizeof(int), 1, bmpptr);
			fwrite(&NumPointsY, sizeof(int), 1, bmpptr);
			for (int i = 0; i < NumPointsY; i++)
			{
				for (int j = 0; j < NumPointsX; j++)
				{
					T res = (*this)[i][j];
					fwrite(&res, sizeof(T), 1, bmpptr);
				}
			}
			if(bmpptr) fclose(bmpptr);
		}
		catch(...)
		{
			if( BitmapIndexes) delete [] BitmapIndexes;
			if(bmpptr) fclose(bmpptr);
			return false;
		}
		return true;
	}
	// ��������� �� �����
	template <class T> 
	bool matrixMN<T>::load_from_file(
		const char* filename)
	{
		FILE *bmpptr = 0;
		unsigned char *BitmapIndexes= 0;
		try
		{
			int NumPointsX, NumPointsY;
			bmpptr = fopen(filename, "rb");
			if (!bmpptr) throw misc::error("Can't open file %s", filename);

			fread(&NumPointsX, sizeof(int), 1, bmpptr);
			fread(&NumPointsY, sizeof(int), 1, bmpptr);
			init(NumPointsX, NumPointsY);

			for (int i = 0; i < NumPointsY; i++)
			{
				for (int j = 0; j < NumPointsX; j++)
				{
					T& res = (*this)[i][j];
					fread(&res, sizeof(T), 1, bmpptr);
				}
			}
			if(bmpptr) fclose(bmpptr);
		}
		catch(...)
		{
			if( BitmapIndexes) delete [] BitmapIndexes;
			if(bmpptr) fclose(bmpptr);
			return false;
		}
		return true;
	}

}
