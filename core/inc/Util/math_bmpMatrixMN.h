#pragma once

#pragma warning(disable : 4200 )

#include <windows.h>
#include "Util/error.h"
#include "Util/misc_mmf.h"
#include "Math/matrixMxN.h"
#include "Util/misc_mmf_wr.h"
#include <vector>

//#include "Magick++.h"

namespace Util
{
	template <class CHANNEL>
	struct t_rgb
	{
		CHANNEL r;
		CHANNEL g;
		CHANNEL b;
		t_rgb(CHANNEL r=0, CHANNEL g=0, CHANNEL b=0){this->r=r;this->g=g;this->b=b;}
		t_rgb(const t_rgb& arg){memcpy(this, &arg, sizeof(*this));};
		t_rgb& operator =(const t_rgb& arg){memcpy(this, &arg, sizeof(*this));return *this;};
		CHANNEL& operator[](int i)
		{
			if(i==0) return r;
			if(i==1) return g;
			if(i==2) return b;
			return r;
		}
	};
	template <class CHANNEL>
	struct t_rgba
	{
		CHANNEL r;
		CHANNEL g;
		CHANNEL b;
		CHANNEL a;
		t_rgba(CHANNEL r=0, CHANNEL g=0, CHANNEL b=0, CHANNEL a=0){this->r=r;this->g=g;this->b=b;this->a=a;}
		t_rgba(const t_rgba& arg){memcpy(this, &arg, sizeof(*this));};
		t_rgba& operator =(const t_rgba& arg){memcpy(this, &arg, sizeof(*this));return *this;};
		CHANNEL& operator[](int i)
		{
			if(i==0) return r;
			if(i==1) return g;
			if(i==2) return b;
			if(i==3) return a;
			return a;
		}
	};
	struct PALETRE
	{
		RGBQUAD rgbq[256];
	};
	inline bool operator ==(const RGBQUAD& arg1, const RGBQUAD& arg2)
	{
		return 
			arg1.rgbBlue  == arg2.rgbBlue ||
			arg1.rgbGreen == arg2.rgbGreen ||
			arg1.rgbRed   == arg2.rgbRed;
	}
	typedef t_rgb<unsigned char>  rgb;
	typedef t_rgba<unsigned char> rgba;

	typedef t_rgb<float>  frgb;
	typedef t_rgba<float> frgba;

	inline void set_rgb( rgb& dst, unsigned long src)
	{
		dst.r = GetRValue(src);
		dst.g = GetGValue(src);
		dst.b = GetBValue(src);
	}

	// ������� �������
	typedef Math::matrixMxN<rgb>  bmpMatrix;
	typedef Math::matrixMxN<rgba> tgaMatrix;
	typedef Math::matrixMxN<frgb>  fbmpMatrix;
	typedef Math::matrixMxN<frgba> ftgaMatrix;
	typedef Math::matrixMxN<unsigned char> palBmpMatrix;

	inline bool loadTiff(const char* filename, Math::matrixMxN<Util::frgba>& m);
	inline bool saveTiff(const char* filename, const Math::matrixMxN<Util::rgb>& m );

	inline bool loadBmpPalette(const char* filename, palBmpMatrix& bmp, PALETTEENTRY* pal, int size);
	inline bool saveBmpPalette(const char* filename, palBmpMatrix& bmp, PALETTEENTRY* pal, int size);

	inline bool loadBmp(const char* filename, bmpMatrix& bmp, PALETTEENTRY* palette=NULL, bool invX=false, bool invY=false);
	inline bool saveBmp(const char* filename, bmpMatrix& bmp);

	inline bool loadTga(const char* filename, tgaMatrix& tga, bool invX=false, bool invY=false);
	inline bool loadTga(const char* filename, bmpMatrix& bmp, bool invX=false, bool invY=false);
	inline bool saveTga(const char* filename, tgaMatrix& tga);
	inline bool loadTga(const char* filename, ftgaMatrix& tga);
	inline bool saveTga(const char* filename, ftgaMatrix& tga);

	inline bool loadPalette(const char* filename, PALETTEENTRY* pal, int size);
	inline bool savePalette(const char* filename, PALETTEENTRY* pal, int size);
}

#include <tiffio.h>

namespace Util
{

	inline bool saveTiff(const char* filename, const Math::matrixMxN<Util::rgb>& m )
	{
		// Open the output image
		TIFF* output = TIFFOpen(filename, "w");
		if(!output)
			return false;

		int w = m.sizeX();
		int h = m.sizeY();
		std::vector<unsigned char> data(w*h*3, 0);
		for(int y=0; y<h; y++)
		{
			for(int x=0; x<w; x++)
			{
				Util::rgb color = m[h-y-1][x];
				unsigned char* d = &data[0] + (y*w+x)*3;
				d[0] = (unsigned char)(color[0]);
				d[1] = (unsigned char)(color[1]);
				d[2] = (unsigned char)(color[2]);
//				d[3] = (unsigned char)(color[3]*255);
			}
		}

		// Magical stuff for creating the image
		// ...
		// Write the tiff tags to the file
		TIFFSetField(output, TIFFTAG_IMAGEWIDTH, w);
		TIFFSetField(output, TIFFTAG_IMAGELENGTH, h);
		TIFFSetField(output, TIFFTAG_COMPRESSION, COMPRESSION_LZW);
		TIFFSetField(output, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
		TIFFSetField(output, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
		TIFFSetField(output, TIFFTAG_BITSPERSAMPLE, 8);
		TIFFSetField(output, TIFFTAG_SAMPLESPERPIXEL, 3);
		// Actually write the image
		if(TIFFWriteEncodedStrip(
			output, 0, &data[0], (int)data.size()) == 0)
		{
			fprintf(stderr, "Could not write image\n");
			exit(42);
		}
		TIFFClose(output);
		return true;
	}
	inline bool loadTiff(const char* filename, Math::matrixMxN<Util::frgba>& m)
	{
		TIFF *image;
		uint16 bps, spp;
		tsize_t stripSize;

		unsigned long imageOffset, result;
		int stripMax, stripCount;

		// Open the TIFF image
		if((image = TIFFOpen(filename, "r")) == NULL)
		{
			fprintf(stderr, "Could not open incoming image\n");
			return false;
		}

		int comp, planarconf, photometric;
		TIFFGetField(image, TIFFTAG_COMPRESSION, &comp);
		TIFFGetField(image, TIFFTAG_PLANARCONFIG, &planarconf);
		TIFFGetField(image, TIFFTAG_PHOTOMETRIC, &photometric);

		int w, h;
		TIFFGetField(image, TIFFTAG_IMAGEWIDTH, &w);
		TIFFGetField(image, TIFFTAG_IMAGELENGTH, &h);
		m.init(w, h);

		int istiled = TIFFIsTiled(image);

		// Check that it is of a type that we support
		if( TIFFGetField(image, TIFFTAG_BITSPERSAMPLE, &bps) == 0)
		{
			fprintf(stderr, "Either undefined or unsupported number of bits per sample\n");
			return false;
		}
		int byps = bps/8;
		if( TIFFGetField(image, TIFFTAG_SAMPLESPERPIXEL, &spp) == 0)
		{
			fprintf(stderr, "Either undefined or unsupported number of samples per pixel\n");
			return false;
		}
		// Read in the possibly multiple strips
		stripSize = TIFFStripSize (image);
		stripMax = TIFFNumberOfStrips (image);
		imageOffset = 0;
		unsigned char* buffer = (unsigned char*)(new char[stripSize]);
		if(!buffer)
		{
			fprintf(stderr, "Could not allocate enough memory for the uncompressed image\n");
			return false;
		}

		int x=0, y=0;
		for (stripCount = 0; stripCount < stripMax; stripCount++)
		{
			result = TIFFReadEncodedStrip (image, stripCount,
				buffer, stripSize);
			int s = stripSize/(spp*byps);
			for(int i=0; i<s; i++)
			{
				Util::frgba color;
				for( int c=0; c<spp; c++)
				{
					unsigned char* val = buffer + (i*spp+c)*byps;
					float v=0;
					if(bps==8)
					{
						v = (*val)/255.f;
					}
					if(bps==32)
					{
						v = *(float*)val;
					}
					if(c<3)
						color[c]=v;
					else
						color[c]=v;
				}
				m[h-y-1][x] = color;

				// ������
				x++;
				if(x>=w)
				{
					y++;
					if(y>=h) break;
					x=0;
				}
			}

			if(y>=h) break;
			if( result==-1)
			{
				fprintf(stderr, "Read error on input strip number %d\n", stripCount);
				return false;
			}
		}
		return true;
	}

	inline bool loadBmpPalette(const char* filename, palBmpMatrix& bmp, PALETTEENTRY* pal, int size)
	{
		misc::mmf theMMF;
		if( !theMMF.open(filename)) return false;

		unsigned long offset = 0;
		BITMAPFILEHEADER* bHeader;
		BITMAPINFOHEADER* biHeader;

		theMMF.getup(offset, bHeader);
		if( bHeader->bfType != 19778) return false;
		theMMF.getup(offset, biHeader);

		if( biHeader->biBitCount!=8) return false;

		PALETRE* rgbq;
		theMMF.getup(offset, rgbq);
		char* data;
		theMMF.get(offset,   data);

		if( size>256) size = 256;
		for(int i=0; i<size; i++)
		{
			PALETTEENTRY& pe = pal[i];
			RGBQUAD& src = rgbq->rgbq[i];
			pe.peRed = src.rgbRed;
			pe.peGreen = src.rgbGreen;
			pe.peBlue = src.rgbBlue;
			pe.peFlags = 0;
		}

		int NumPointsX = biHeader->biWidth; 
		int NumPointsY = biHeader->biHeight;
		bmp.init(NumPointsX, NumPointsY);

		int NumPointsXAligned = NumPointsX;
		if (NumPointsX%4 != 0) NumPointsXAligned = (NumPointsX/4 + 1)*4;

		for (int i = 0; i < NumPointsY; i++)
		{
			for (int j = 0; j < NumPointsX; j++)
			{
				unsigned char index = (unsigned char)data[j];
				bmp[i][j] = index;
			}
			data+=NumPointsXAligned;
		}

		return true;
	}

	inline bool saveBmpPalette(const char* filename, palBmpMatrix& bmp, PALETTEENTRY* pal, int size)
	{
		FILE *bmpptr = 0;
		unsigned char *BitmapIndexes= 0;
		try
		{
			int NumPointsX = bmp.sizeX(); 
			int NumPointsY = bmp.sizeY();

			int NumPointsXAligned = NumPointsX;
			if (NumPointsX%4 != 0) NumPointsXAligned = (NumPointsX/4 + 1)*4;

			BITMAPFILEHEADER bHeader;
			bHeader.bfType      = 19778;
			bHeader.bfSize      = sizeof(BITMAPFILEHEADER) + 
								  sizeof(BITMAPINFOHEADER) +
								  sizeof(RGBQUAD)*256 +
								  NumPointsXAligned*NumPointsY;
			bHeader.bfReserved1 = 0;
			bHeader.bfReserved2 = 0;
			bHeader.bfOffBits   = 1078;
			BITMAPINFOHEADER biHeader;
			biHeader.biSize          = 40;
			biHeader.biWidth         = NumPointsX;
			biHeader.biHeight        = NumPointsY;
			biHeader.biPlanes        = 1;
			biHeader.biBitCount      = 8;
			biHeader.biCompression   = 0;
			biHeader.biSizeImage     = 0;
			biHeader.biXPelsPerMeter = 99962;
			biHeader.biYPelsPerMeter = 99962;
			biHeader.biClrUsed       = 0;
			biHeader.biClrImportant  = 0;
			RGBQUAD rgbq[256];
			int i, j;
			for (i = 0; i < 256; i++)
			{
				rgbq[i].rgbBlue     = pal[i].peBlue;
				rgbq[i].rgbGreen    = pal[i].peGreen;
				rgbq[i].rgbRed      = pal[i].peRed;
				rgbq[i].rgbReserved = 0;
			}
			bmpptr = fopen(filename, "wb");
			if (!bmpptr) throw Util::Error("Can't open file %s", filename);

			fwrite(&bHeader, sizeof(BITMAPFILEHEADER), 1, bmpptr);
			fwrite(&biHeader, sizeof(BITMAPINFOHEADER), 1, bmpptr);
			fwrite(rgbq, sizeof(RGBQUAD), 256, bmpptr);

			BitmapIndexes = new unsigned char [NumPointsXAligned];
			for (i = 0; i < NumPointsY; i++)
			{
				for (j = 0; j < NumPointsX; j++)
				{
					BitmapIndexes[j] = bmp[i][j];
				}
				for (j = NumPointsX; j < NumPointsXAligned; j++)
				{
					BitmapIndexes[j] = 0;
				}
				fwrite(BitmapIndexes, sizeof(unsigned char), NumPointsXAligned, bmpptr);
			}

			if( BitmapIndexes) delete [] BitmapIndexes;
			if(bmpptr) fclose(bmpptr);
		}
		catch(...)
		{
			if( BitmapIndexes) delete [] BitmapIndexes;
			if(bmpptr) fclose(bmpptr);
			throw;
		}
		return true;
	}



	inline bool loadBmp(const char* filename, bmpMatrix& bmp, PALETTEENTRY* palette, bool invX, bool invY)
	{
		/*/
		Magick::Image i;
		try
		{
			i.read(filename);
		}
		catch(...)
		{
			return false;
		}
		Magick::Geometry sz = i.size();

		int NumPointsX = sz.width(); 
		int NumPointsY = sz.height();
		bmp.init(NumPointsX, NumPointsY);

		Magick::Blob blob; 
		i.magick( "RGB" ); // Set JPEG output format 
		i.write( &blob ); 
		int l = blob.length();
		math::rgb* f = (math::rgb*)blob.data();

		for (int y = 0; y < NumPointsY; y++)
		{
			for (int x = 0; x < NumPointsX; x++)
			{
				math::rgb& pix = f[y*NumPointsX + x];
				int ii = x;
				int jj = y;
				if(invX) ii = NumPointsX-x-1;
				if(invY) jj = NumPointsY-y-1;
				rgb& dst = bmp[jj][ii];
				dst = pix;
			}
		}
		/*/

		//*/
		misc::mmf theMMF;
		if( !theMMF.open(filename)) return false;

		unsigned long offset = 0;
		BITMAPFILEHEADER* bHeader;
		BITMAPINFOHEADER* biHeader;

		theMMF.getup(offset, bHeader);
		if( bHeader->bfType != 19778) return false;
		theMMF.getup(offset, biHeader);

		int NumPointsX = biHeader->biWidth; 
		int NumPointsY = biHeader->biHeight;
		bmp.init(NumPointsX, NumPointsY);

		if( biHeader->biBitCount==8)
		{
			PALETRE* rgbq;
			theMMF.getup(offset, rgbq);
			char* data;
			theMMF.get(offset,   data);

			int NumPointsXAligned = NumPointsX;
			if (NumPointsX%4 != 0) NumPointsXAligned = (NumPointsX/4 + 1)*4;

			for (int i = 0; i < NumPointsY; i++)
			{
				for (int j = 0; j < NumPointsX; j++)
				{
					unsigned char index = (unsigned char)data[j];
					int ii = i;
					int jj = j;
					if(invX) jj = NumPointsX-j-1;
					if(invY) ii = NumPointsY-i-1;
					rgb& dst = bmp[ii][jj];
					if(!palette)
					{
						RGBQUAD& src = rgbq->rgbq[index];
						dst.r = src.rgbRed;
						dst.g = src.rgbGreen;
						dst.b = src.rgbBlue;
					}
					else
					{
						PALETTEENTRY& src = palette[index];
						dst.r = src.peRed;
						dst.g = src.peGreen;
						dst.b = src.peBlue;
					}
				}
				data+=NumPointsXAligned;
			}
		}
		else if( biHeader->biBitCount==32)
		{
			char* data;
			theMMF.get(offset, data);
			data += 64;

			for (int y = 0, ofs=0; y < NumPointsY; y++)
			{
				for (int x = 0; x < NumPointsX; x++)
				{
					int ii = x;
					int jj = y;
					if(invX) ii = NumPointsX-x-1;
					if(invY) jj = NumPointsY-y-1;
					rgb& dst = bmp[jj][ii];
					dst.r = data[ofs+2];
					dst.g = data[ofs+1];
					dst.b = data[ofs+0];
					ofs+=4;
				}
//				ofs = ((ofs-1)&(0xFFFFFFFC)) + 4;
			}

		}
		else if( biHeader->biBitCount==24)
		{
			char* data;
			theMMF.get(offset, data);

			for (int y = 0, ofs=0; y < NumPointsY; y++)
			{
				for (int x = 0; x < NumPointsX; x++)
				{
					int ii = x;
					int jj = y;
					if(invX) ii = NumPointsX-x-1;
					if(invY) jj = NumPointsY-y-1;
					rgb& dst = bmp[jj][ii];
					dst.r = data[ofs+2];
					dst.g = data[ofs+1];
					dst.b = data[ofs+0];
					ofs+=3;
				}
				ofs = ((ofs-1)&(0xFFFFFFFC)) + 4;
			}

		}
		//*/
		return true;
	}

	inline bool saveBmp(const char* filename, bmpMatrix& bmp)
	{
		int NumPointsX = bmp.sizeX(); 
		int NumPointsY = bmp.sizeY();

		misc::mmf_write theMMF;
		int linesize = NumPointsX*3;
		linesize = ((linesize-1)&(0xFFFFFFFC)) + 4;

		if( !theMMF.Create(filename, linesize*NumPointsY+10000)) 
			return false;

		BITMAPFILEHEADER* bHeader;
		BITMAPINFOHEADER* biHeader;
		char* data;
		theMMF.alloc(bHeader);
		theMMF.alloc(biHeader);
		theMMF.allocarray(data, linesize*NumPointsY);

		bHeader->bfType      = 19778;
		bHeader->bfSize      = sizeof(BITMAPFILEHEADER) + 
							  sizeof(BITMAPINFOHEADER) +
							  NumPointsX*NumPointsY*3;
		bHeader->bfReserved1 = 0;
		bHeader->bfReserved2 = 0;
		bHeader->bfOffBits   = 54;

		biHeader->biSize          = 40;
		biHeader->biWidth         = NumPointsX;
		biHeader->biHeight        = NumPointsY;
		biHeader->biPlanes        = 1;
		biHeader->biBitCount      = 24;
		biHeader->biCompression   = 0;
		biHeader->biSizeImage     = 0;
		biHeader->biXPelsPerMeter = 99962;
		biHeader->biYPelsPerMeter = 99962;
		biHeader->biClrUsed       = 0;
		biHeader->biClrImportant  = 0;

		for (int i = 0, ofs=0; i < NumPointsY; i++)
		{
			for (int j = 0; j < NumPointsX; j++)
			{
				rgb& src = bmp[i][j];
				data[ofs+2] = src.r;
				data[ofs+1] = src.g;
				data[ofs+0] = src.b;
				ofs+=3;
			}
			ofs = ((ofs-1)&(0xFFFFFFFC)) + 4;
		}
		return true;
	}

	struct TARGAHEADER
	{
		BYTE	IDlength;
		BYTE	ColorMapType;
		BYTE	ImgType;
		BYTE	FirstColorMapEntry[2];
		BYTE	ColorMapLength[2];
		BYTE	ColorMapEntrySize;
		BYTE	Xorg[2];
		BYTE	Yorg[2];
		WORD	Width;
		WORD	Height;
		BYTE	Bpp;
		BYTE	Bits;
	};

	inline bool loadTga(const char* filename, tgaMatrix& tga, bool invX, bool invY)
	{
		misc::mmf theMMF;
		if( !theMMF.open(filename)) return false;

		unsigned long offset = 0;
		TARGAHEADER* bHeader;

		theMMF.getup(offset, bHeader);
		char* data;
		theMMF.get(offset, data);

		int NumPointsX = bHeader->Width;
		int NumPointsY = bHeader->Height;
		tga.init(NumPointsX, NumPointsY);
		for (int y = 0; y < NumPointsY; y++)
		{
			for (int x = 0; x < NumPointsX; x++)
			{
				int ii = x;
				int jj = y;
				if(invX) ii = NumPointsX-x-1;
				if(invY) jj = NumPointsY-y-1;

				rgba& dst = tga[jj][ii];
				dst.r = data[2];
				dst.g = data[1];
				dst.b = data[0];
				if(bHeader->Bpp==24)
				{
					dst.a = 1;
					data+=3;
				}
				else if(bHeader->Bpp==32)
				{
					dst.a = data[3];
					data+=4;
				}
			}
		}
		return true;
	}
	inline bool loadTga(const char* filename, bmpMatrix& tga, bool invX, bool invY)
	{
		misc::mmf theMMF;
		if( !theMMF.open(filename)) return false;

		unsigned long offset = 0;
		TARGAHEADER* bHeader;

		theMMF.getup(offset, bHeader);
		char* data;
		theMMF.get(offset, data);

		int NumPointsX = bHeader->Width;
		int NumPointsY = bHeader->Height;
		tga.init(NumPointsX, NumPointsY);
		for (int y = 0; y < NumPointsY; y++)
		{
			for (int x = 0; x < NumPointsX; x++)
			{
				int ii = x;
				int jj = y;
				if(invX) ii = NumPointsX-x-1;
				if(invY) jj = NumPointsY-y-1;

				rgb& dst = tga[jj][ii];
				dst.r = data[2];
				dst.g = data[1];
				dst.b = data[0];
				if(bHeader->Bpp==24)
				{
					data+=3;
				}
				else if(bHeader->Bpp==32)
				{
					data+=4;
				}
			}
		}
		return true;
	}
	inline bool saveTga(const char* filename, tgaMatrix& tga)
	{
		int NumPointsX = tga.sizeX(); 
		int NumPointsY = tga.sizeY();

		misc::mmf_write theMMF;
		if( !theMMF.Create(filename, 4*NumPointsX*NumPointsY+10000)) 
			return false;

		TARGAHEADER* bHeader;
		char* data;
		theMMF.alloc(bHeader);
		theMMF.allocarray(data, NumPointsX*NumPointsY*4);

		bHeader->IDlength = 0;
		bHeader->ColorMapType = 0;
		bHeader->ImgType = 2;
		bHeader->FirstColorMapEntry[0] = bHeader->FirstColorMapEntry[1] = 0;
		bHeader->ColorMapLength[0] = bHeader->ColorMapLength[1] = 0;
		bHeader->ColorMapEntrySize = 0;
		bHeader->Xorg[0] = bHeader->Xorg[1] = 0;
		bHeader->Yorg[0] = bHeader->Yorg[1] = 0;
		bHeader->Width = NumPointsX;
		bHeader->Height= NumPointsY;
		bHeader->Bpp = 32;
		bHeader->Bits= 8;
		for (int y = 0; y < NumPointsY; y++)
		{
			for (int x = 0; x < NumPointsX; x++)
			{
				rgba& dst = tga[y][x];
				data[2] = dst.r;
				data[1] = dst.g;
				data[0] = dst.b;
				data[3] = dst.a;
				data+=4;
			}
		}
		return true;
	}

	inline bool loadTga(const char* filename, ftgaMatrix& tga)
	{
		tgaMatrix tgauc;
		if( !loadTga(filename, tgauc)) return false;
		tga.init(tgauc.sizeX(), tgauc.sizeY());
		for(int iy = 0; iy<tga.sizeY(); iy++)
		{
			for(int ix = 0; ix<tga.sizeX(); ix++)
			{
				rgba   cs = tgauc[iy][ix];
				frgba& cd = tga[iy][ix];
				cd.r = cs.r/255.f;
				cd.g = cs.g/255.f;
				cd.b = cs.b/255.f;
				cd.a = cs.a/255.f;
			}
		}
		return true;
	}
	inline bool saveTga(const char* filename, ftgaMatrix& tga)
	{
		tgaMatrix tgauc;
		tgauc.init(tga.sizeX(), tga.sizeY());

		for(int iy = 0; iy<tga.sizeY(); iy++)
		{
			for(int ix = 0; ix<tga.sizeX(); ix++)
			{
				frgba cs = tga[iy][ix];
				rgba& cd = tgauc[iy][ix];
				cd.r = (unsigned char)(__max( 0.f, __min( 1.f, cs.r))*255.f);
				cd.g = (unsigned char)(__max( 0.f, __min( 1.f, cs.g))*255.f);
				cd.b = (unsigned char)(__max( 0.f, __min( 1.f, cs.b))*255.f);
				cd.a = (unsigned char)(__max( 0.f, __min( 1.f, cs.a))*255.f);
			}
		}
		return saveTga(filename, tgauc);
	}

	inline bool loadPalette(const char* filename, PALETTEENTRY* pal, int size)
	{
		// ������� � ��������� ����
		if( !filename || !filename[0])
			return false;
		FILE* palfile = fopen(filename, "rb");
		if( !palfile) 
			return false;
		for(int c=0; c<size; c++)
		{
			if( feof(palfile)) 
				return false;
			BYTE ch[3];
			fread(&ch, 1, 3, palfile);
			pal[c].peRed = ch[0];
			pal[c].peGreen = ch[1];
			pal[c].peBlue = ch[2];
		}
		fclose(palfile);
		return true;
	}
	inline bool savePalette(const char* filename, PALETTEENTRY* pal, int size)
	{
		// ������� � ��������� ����
		FILE* palfile = fopen(filename, "wb");
		if( !palfile) 
			return false;
		for(int c=0; c<size; c++)
		{
			fwrite(&pal[c], 1, 3, palfile);
		}
		fclose(palfile);
		return true;
	}
}

