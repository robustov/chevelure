///////////////////////////////////////////////////////////////////////////////
//  �������� ������.
//
#ifndef _tree_H_
#define _tree_H_

/*/

������� ����: � �������� tree::iterator ���� ��������� �������
begin() � end() ��� ��������� ���������� child ������.

----------------
������� ��������:

tree<> ������ ������������ (������, �����, ����) ������. ������ ���������� ��������� stl
�����������, ��� ����������� ������������� �������� ������, ���� TreeCtrl. ��������� �
������������ ���� std.
��� �������, ������������ �� ������ ������������ bidirectional 
��������� (����������� ��������� ++ � --):
iterator( const_iterator) - ��������� ��������������� ���������� �������� ������ ����. 
branch_iterator( const_branch_iterator) - ��������� ������ ������
����� ������ ��� ����� ���� childo� ��� ��������
���� ��������������� �� reverse ���������.

����� ���� � ��������� ���� ������:
parent() - ���������� �������� - ����������� �������
level() - ���������� ������� �������� � �������� (0 ��� tree.begin())
begin() - ���������� �������� ������� �������� ������ child��
end() - ���������� �������� ����� ������ child��
size() - ���������� ����� child��
� branch_iterator ���� �����:
scipchild() - ��������� ������� ��� ������������ �� child��

������ tree<> �������� ������:
begin() - ���������� �������� ������ ������ �������� ������ ��������
end() - ���������� �������� ����� ������ �������� ������ ��������
insert() - ����������
erase()  - �������� �������� ������ �� ������� childo�
move()   - ����������� �������� ������ �� ������� childo� � ������ �����
������ ��� � ������ ������
copy()  - ����������� �������� ������ �� ������� childo� � ������ �����
������ ��� � ������ ������

����� ���� ���� �������� ��������� ������� �������� � ����������� �������� � �������� ���������:
inithead() - ���������������� �������� ������� ������
insertbefore() - ��������� ������� insertnode �� �������� marknode
insertafter() - ��������� ������� insertnode ����� �������� marknode
swap() - �������� oldnode �� newnode
erase() - ������� ������� �� ������


������ ����� ����������� � ��������� ����:
 A1
  B11
   C111
   C112
  B12
  B13
 A2
  B21
   C211
  B22
 A3
  B31
	�311

A1-A3 - �������� �������� ������
������ � ��������� �������� ������ ���������� ������� � ���������
������. 

// ������� ��������� ��������(�������) ������
tree::iterator it = thetree.begin();
for( ; it != thetree.end(); it++)
{
	...
}

�������� ������� ���� ������-������� ���� ������������ �� ���� 
���������� ������. ������ � ��� ����� ���� ������� 
��������� �������:

tree::iterator it = parent.begin();
for( ; it != parent.end(); it++)
{
	...
}

��� ������ ���� ��������� ������ ������������ branch_iterator.
������������ ����� ����������� � ������� ��������� �� ������� ������.

tag_tree::branch_iterator it = thetree.begin();
for( ; it!=thetree.end(); it++)
{
	...
}


/*/
#include <iterator>

/*/
#include "common_insidelist.h"
/*/
// /SUSPEND
namespace insidelist
{
	// ������ � �������� ���������
	// � ������ node (N), ������ ���� ���������� �-���:
	// N*& next() � N*& prev()

	// ���������������� �������� ������� ������
	template <class N>
		inline void inithead(N& headnode)
	{
		// ������������� ������ ������:
		headnode.next() = &headnode;
		headnode.prev() = &headnode;
	}

	// ��������� ������� insertnode �� �������� marknode
	template <class N>
		inline void insertbefore(N& insertnode, N& marknode)
	{
		insertnode.prev() = marknode.prev();
		insertnode.next() = &marknode;
		marknode.prev() = &insertnode;
		N* prevnode = insertnode.prev();
		prevnode->next() = &insertnode;
	}
	// ��������� ������� ��������� begin-end �� �������� marknode, �� ������� end
	template <class N>
		inline void movebefore(N& begin, N& _end, N& marknode)
	{
		N& end = *_end.prev();
		// ��������
		N* nextnode = end.next();
		N* prevnode = begin.prev();
		nextnode->prev() = prevnode;
		prevnode->next() = nextnode;

		// ��������
		nextnode = &marknode;
		prevnode = marknode.prev();

		begin.prev() = prevnode;
		prevnode->next() = &begin;
		end.next()   = nextnode;
		nextnode->prev() = &end;
	}

	// ��������� ������� insertnode ����� �������� marknode
	template <class N>
		inline void insertafter(N& insertnode, N& marknode)
	{
		insertnode.next() = marknode.next();
		insertnode.prev() = &marknode;
		marknode.next() = &insertnode;
		N* nextnode = insertnode.next();
		nextnode->prev() = &insertnode;
	}

	// �������� oldnode �� newnode
	template <class N>
		inline void swap(N& oldnode, N& newnode)
	{
		if(oldnode.next()==&oldnode || oldnode.prev()==&oldnode)
		{
			inithead( newnode);
			return;
		}
		oldnode.next()->prev() = &newnode;
		newnode.next() = oldnode.next();
		oldnode.prev()->next() = &newnode;
		newnode.prev() = oldnode.prev();
	}

	// ������� ������� �� ������
	template <class N>
		inline bool erase(N& node)
	{
		if(node.next()==&node || node.prev()==&node) return false;
		N* nextnode = node.next();
		N* prevnode = node.prev();
		nextnode->prev() = prevnode;
		prevnode->next() = nextnode;
		return true;
	}
}; 
//*/

namespace std
{
using namespace insidelist;

/*
������������� ���������/BR
������������ � ������������ ���� std./BR
��������� tree<T> ������ ������������� ��������� ��������� ���� T. ������ ������� ������ (��������) ������������ ����� ���������� ������./BR
������ ������������� ���������:/BR
A1				/BR
..B11			/BR
....C111		/BR
....C112		/BR
..B12			/BR
..B13			/BR
A2				/BR
..B21			/BR
....C211		/BR
..B22			/BR
A3				/BR
..B31			/BR
....C311		/BR

�������� A1, A2, A3 ��������� ���������� �������� ������. ���������� � ��� ����� ��� ����� tree ��� ������ (std::list). ��. tree::begin(), tree::end(), tree::rbegin(), tree::rend().
*/
template<class T>
class tree 
{
	struct node;
	
	class listnode
	{
		listnode* pnext;
		listnode* pprev;
	public:
		typedef listnode* plistnode;
		unsigned char index;
		plistnode& next(){return pnext;}
		plistnode& prev(){return pprev;}
		const plistnode& next()const {return pnext;}
		const plistnode& prev()const {return pprev;}
	};
	struct node
	{
		// ������ - ������� � sibling ������
		// ������ - �������� ������� � child ������
		listnode links[2];
		// ��������
		T value;
		node():value()
		{
			links[0].index = 0;
			links[1].index = 1;
			inithead(links[1]);
		}
		node(const T& val):value(val)
		{
			links[0].index = 0;
			links[1].index = 1;
			insidelist::inithead(links[1]);
		}
		static node* getnode(listnode* lnode)
		{
			if( lnode->index>=2) return NULL; // ������
			lnode -= lnode->index;
			return (node*)lnode;
		}
	};
protected:
	// ������� ������
	listnode head;

//////////////////////////////
// ������������ ����
public:
	class const_iterator;
	class iterator;
	class const_branch_iterator;
	class branch_iterator;

	// ������ �� �������
	typedef const T& const_reference;
	// ��������� �� �������
	typedef const T* const_pointer;
	// ������ �� �������
	typedef T& reference;
	// ��������� �� �������
	typedef T* pointer;

	friend class tree::const_iterator;

//////////////////////////////
// ���������
public:
	// �������� ��� �������� ��������� ������ ������ ������
	class const_iterator //: public _Bidit<T, ptrdiff_t, pointer, reference>
	{
	// ����������� ���������
	public:
		const_iterator():plistnode(NULL){}
		const_iterator(const listnode* _plistnode):plistnode((listnode*)_plistnode){}
		const_iterator(const const_iterator& it):plistnode(it.plistnode){}
		const_iterator& operator=(const const_iterator& it){this->const_iterator::const_iterator(it); return *this;}

		reference operator*() const
		{
			const node* n = Node();
			return (reference)n->value;
		}
		pointer operator->() const
		{
			const node* n = Node();
			return (pointer)&n->value;
		}
		const_iterator& operator++()
		{
			plistnode = plistnode->next();
			return (*this); 
		}
		const_iterator operator++(int)
		{
			const_iterator _Tmp = *this;
			plistnode = plistnode->next();
			return (_Tmp); 
		}
		const_iterator& operator--()
		{
			plistnode = plistnode->prev();
			return (*this); 
		}
		const_iterator operator--(int)
		{
			const_iterator _Tmp = *this;
			--(*this);
			return (_Tmp); 
		}
		bool operator==(const const_iterator& it) const
		{
			return (plistnode == it.plistnode);
		}
		bool operator!=(const const_iterator& it) const
		{
			return (plistnode != it.plistnode);
		}
		bool operator<(const const_iterator& it) const
		{
			// ��������� ������
			const_iterator t1 = *this, t2 = it;
			for( ; t2.level()<t1.level(); ) t1 = t1.parent();
			for( ; t1.level()<t2.level(); ) t2 = t2.parent();
			// ���� �� ������ ���������� parent�
			for( ; t1.parent()!=t2.parent(); ) 
				t2 = t2.parent(), t1.parent();
			if(t1 == t2) return false;
			const_iterator tt1 = t1, tt2 = t2;
			// �������� ��� ������
			for( ; ; tt2++, tt1++)
			{
				if( tt1.IsEnd()) return false; // t2<t1
				if( tt2.IsEnd()) return true;  // t1<t2	
				if(tt2 == t1) return false;	// t2<t1
				if(tt1 == t2) return true;	// t1<t2
			}
			return false;
		}
		bool operator<=(const const_iterator& it) const
		{
			if(*this==it) return true;
			return operator<(it);
		}
		bool operator>(const const_iterator& it) const
		{
			return !(operator<=(it));
		}
		bool operator>=(const const_iterator& it) const
		{
			return !(operator<(it));
		}

	// ��������� � ������
	public:
		// �������� ������
		const_iterator parent() const 
		{
			const_iterator it = *this;
			for( ; !it.IsEnd(); it++);
			if(it.IsTreeEnd()) return it;
			node* pnode = node::getnode(it.plistnode);
			return const_iterator( &pnode->links[0]);
		}
		// �������� - ������ ������ ��������
		const_iterator begin() const
		{
			if( IsEnd()) return *this;
			return (++end());
		}
		// �������� - ����� ������ ��������
		const_iterator end() const
		{
			if( IsEnd()) return *this;
			node* pnode = node::getnode(plistnode);
			return iterator( &pnode->links[1]);
		}
	// �������
	public:
		// ����� ����������� ���������
		int size() const
		{
			const_iterator it = begin();
			int _size=0;
			for( ; it!=end(); it++, _size++);
			return _size;
		}
		// ��������� �� ���������� ����������� ���������
		bool empty() const {return (begin() == end());}
		// ������� �������� � ��������
		long level() const
		{
			long level = 0;
			const_iterator par = parent();
			for(; !par.IsTreeEnd() ;level++)
				par = par.parent();
			return level;
		}
		unsigned long ToDWord()
		{
			return (unsigned long)plistnode;
		}
		void FromDWord(unsigned long data)
		{
			plistnode = (listnode*)data;
		}

	// ��������� �������
	protected:
		node* Node(){return node::getnode(plistnode);}
		const node* Node()const{return node::getnode(plistnode);}
		bool IsTreeEnd()const{return (plistnode->index==2);}
	public:
		// �������� ���������� ���������
		bool IsEnd()const{return (plistnode->index!=0);}
	protected:
		friend class tree<T>;
		listnode* plistnode;
	};

	// �������� ��� �������� ��������� ������ ������ ������/BR
	// ������������� ������������� const_iterator.
	class iterator : public const_iterator
	{
	public:
		iterator():const_iterator(){}
		iterator(listnode* _plistnode):const_iterator(_plistnode){}
		iterator(const iterator& it):const_iterator(it){}
		iterator(const branch_iterator& it):const_iterator(it){}
		iterator& operator=(const iterator& it){this->iterator::iterator(it); return *this;}
///		iterator& operator=(const branch_iterator& it){this->iterator::iterator(it); return *this;}

		reference operator*()
		{
			return const_iterator::Node()->value;
		}
		pointer operator->()
		{
			return &const_iterator::Node()->value;
		}
		iterator& operator++()
		{
			const_iterator::plistnode = const_iterator::plistnode->next();
			return (*this);
		}
		iterator operator++(int)
		{
			iterator _Tmp = *this;
			const_iterator::plistnode = const_iterator::plistnode->next();
			return (_Tmp);
		}
		iterator& operator--()
		{
			const_iterator::plistnode = const_iterator::plistnode->prev();
			return (*this);
		}
		iterator operator--(int)
		{
			iterator _Tmp = *this;
			--(*this);
			return (_Tmp);
		}

	public:
		iterator parent() const{const_iterator it = const_iterator::parent(); return *((iterator*)&it);}
		iterator begin() const{const_iterator it = const_iterator::begin(); return *((iterator*)&it);}
		iterator end() const{const_iterator it = const_iterator::end(); return *((iterator*)&it);}
		iterator insert(const T& value=T())
		{
			node* newnode = new node(value);
			insidelist::insertbefore(newnode->links[0], *this->plistnode);
			return iterator(&(newnode->links[0]));
		}
	};

	// �������� ��� ������� ������ ������/BR
	class const_branch_iterator : public const_iterator
	{
	// ����������� ��������� 
	public:
		const_branch_iterator():const_iterator(){}
		const_branch_iterator(listnode* _plistnode):const_iterator(_plistnode){}
		const_branch_iterator(const const_iterator& it):const_iterator(it){}
		const_branch_iterator& operator=(const const_branch_iterator& it){this->const_iterator::const_iterator(it); return *this;}

		const_branch_iterator& operator++()
		{
			if( begin()!=end())
			{
				*this = begin();
				return *this;
			}
			for(;;)
			{
				if( const_iterator::IsEnd())
				{
					// ����� ������ child
					// ������� � �������� ��-��
					if( const_iterator::IsTreeEnd()) return *this;
					const_iterator::plistnode = &const_iterator::Node()->links[0];
				}
				const_iterator::plistnode = const_iterator::plistnode->next();
				if( !const_iterator::IsEnd()) return *this;
			}
			return (*this);
		}
		const_branch_iterator operator++(int)
		{
			const_iterator _Tmp = *this;
			++(*this);
			return (_Tmp);
		}
		const_branch_iterator& operator--()
		{
			const_iterator::plistnode = const_iterator::plistnode->prev();
			if(!const_iterator::IsEnd())
			{
				// ���������� �� ���������� child�
				for(;begin()!=end();)
				{
					*this = end();
					const_iterator::plistnode = const_iterator::plistnode->prev();
				}
			}
			else
			{
				// ��������� � ���� � ������� ���
				if(const_iterator::IsTreeEnd()) return *this;
				const_iterator::plistnode = &const_iterator::Node()->links[0];
				return *this;
			}
			return (*this); 
		}
		const_branch_iterator operator--(int)
		{
			branch_iterator _Tmp = *this;
			--(*this);
			return (_Tmp); 
		}
	// ����������� ��������� 
	public:
		// ��������� ������� ��� ������������ �� child��
		const_branch_iterator scipchild() const
		{
			const_branch_iterator bit = end();
			return (++bit);
		}
	};

	// �������� ��� ������� ������ ������
	// ������������� ������������� const_branch_iterator.
	class branch_iterator : public const_branch_iterator
	{
	public:
		branch_iterator():const_branch_iterator(){}
		branch_iterator(listnode* _plistnode):const_branch_iterator(_plistnode){}
		branch_iterator(const branch_iterator& it):const_branch_iterator(it){}
		branch_iterator(const iterator& it):const_branch_iterator(it){}
		branch_iterator& operator=(const branch_iterator& it){this->const_branch_iterator::const_branch_iterator(it); return *this;}
		branch_iterator& operator=(const iterator& it){this->const_branch_iterator::const_branch_iterator(it); return *this;}

		reference operator*()
		{
			return const_iterator::Node()->value;
		}
		pointer operator->()
		{
			return &const_iterator::Node()->value;
		}
		branch_iterator& operator++()
		{
			const_branch_iterator::operator++();
			return (*this);
		}
		branch_iterator operator++(int)
		{
			iterator _Tmp = *this; ++(*this); return (_Tmp);
		}
		branch_iterator& operator--()
		{
			const_branch_iterator::operator--();
			return (*this); 
		}
		branch_iterator operator--(int)
		{
			branch_iterator _Tmp = *this; --(*this); return (_Tmp); 
		}
		// ��������� ������� ��� ������������ �� child��
		branch_iterator scipchild() const
		{
			return *((branch_iterator*)&const_branch_iterator::scipchild());
		}
	public:
		branch_iterator parent() const{const_iterator it = const_iterator::parent(); return *((branch_iterator*)&it);}
		branch_iterator begin() const{const_iterator it = const_iterator::begin(); return *((branch_iterator*)&it);}
		branch_iterator end() const{const_iterator it = const_iterator::end(); return *((branch_iterator*)&it);}
	};

//////////////////////////////
// �������� (reverse) ���������
public:
	/*/
	// reverse_iterator
	typedef reverse_bidirectional_iterator<iterator, T>
		reverse_iterator;
	// const_reverse_iterator
	typedef reverse_bidirectional_iterator<const_iterator, T> 
		const_reverse_iterator;
	// reverse_branch_iterator
	typedef reverse_bidirectional_iterator<branch_iterator, T>
		reverse_branch_iterator;
	// const_reverse_branch_iterator
	typedef reverse_bidirectional_iterator<const_branch_iterator, T>
		const_reverse_branch_iterator;
	/*/

//////////////////////////////
// ������������
public:
	explicit tree()
	{
		insidelist::inithead(head);
		head.index = 2;
	}
	tree(const tree& src)
	{
		insidelist::inithead(head);
		head.index = 2;
		*this = src;
	}
	~tree(){clear();}
	// ����������� ����������� ����� ������
	tree& operator=(const tree& src)
	{
		clear();
		const_iterator from = src.begin();
		for( ; from != src.end(); from++) 
			copy(from, end());
		return *this;
	}

// �������
public:
	// ��������� �� ���������� ���������
	bool empty()const {return (begin() == end());}
	// ����� ��������� �������� ������
	int size() const
	{
		const_iterator it = begin();
		int _size=0;
		for( ; it!=end(); it++, _size++);
		return _size;
	}

	// ���� ��������� �������� �� ���� ������� ������� �������
	bool ischildof(const_iterator itparent, const_iterator it)
	{
		// ���� ��������� �� �������� �� point_it child�� target_it
		iterator temp(it);
		for( ; !temp.IsTreeEnd(); temp = temp.parent())
			if(temp.Node()==itparent.Node()) return true;
		return false;
	}

// ������ � ��������� ������
public:
	// ������� ���������
	const_iterator begin()const{return const_iterator( head.next());}
	iterator begin(){return iterator( head.next());}
	const_iterator end()const{return const_iterator( &head);}
	iterator end(){return iterator( &head);}

	// �������� ���������
//	const_reverse_iterator rbegin()const{return const_reverse_iterator(end());}
//	reverse_iterator rbegin(){return reverse_iterator(end());}
//	const_reverse_iterator rend()const{return const_reverse_iterator(begin());}
//	reverse_iterator rend(){return reverse_iterator(begin());}


// �������� ��� �������
public:
	// ������� ��� ��������
	void clear(){for( ; begin()!=end(); ) erase(begin());}

	// �������� ������� � ������ /BR
	// ���� �� ������ sibling
	iterator insert(iterator before_it, const T& value=T())
	{
		node* newnode = new node(value);
		insidelist::insertbefore(newnode->links[0], *before_it.plistnode);
		return iterator(&(newnode->links[0]));
	}
	// �������� ����������� �������/BR
	// ��� sibling �������� ������ ��� child���
	iterator insertparent(iterator parent_for_it, const T& value=T())
	{
		return insertchild(parent_for_it.parent(), value);
	}
	// �������� ������ ������� 
	// ��� child �������� ������ child��� ������������ ��������
	iterator insertchild(iterator child_for_it, const T& value=T())
	{
		listnode* pparentnode = &head;
		node* parent = child_for_it.Node();
		if( parent) pparentnode = &parent->links[1];
		node* newnode = new node(value);
		insidelist::swap( *pparentnode, newnode->links[1]);
		insidelist::inithead( *pparentnode);
		insidelist::insertbefore(newnode->links[0], *pparentnode);
		return iterator(&(newnode->links[0]));
	}
	// ������� ������� 
	iterator erase(iterator it)
	{
		if( it.IsEnd()) throw "������ ������� ����� �������";

		// ����� child�� � �������� �������
		branch_iterator bit = it, endbit = it;
		iterator retit = it; retit++;
		bit = bit.scipchild();
		for( bit--; bit!=endbit; )
		{
			// ��������� ������� - �������������, ��� � ���� ��� child��
			branch_iterator erit = bit;
			bit--;
			// �������
			node* pnode = erit.Node();
			if( erit.begin()!=erit.end()) throw "������ ��� ��������";
			if( !insidelist::erase( pnode->links[0])) throw "������ ��� ��������";
			delete pnode;
		}
		node* pnode = it.Node();
		if( !pnode) return retit;
		if( !insidelist::erase( pnode->links[0])) return retit;
		delete pnode;
		return retit;
	}
	// ����������� ������� 
	iterator move(iterator from_it, iterator to_it, iterator* pnewit=NULL)
	{
		if( from_it.IsEnd()) return from_it;
		iterator retit = from_it; retit++;
		// ���� ��������� �� �������� �� point_it child�� target_it
		iterator temp(to_it);
		for( ; !temp.IsTreeEnd(); temp = temp.parent())
			if(temp.Node()==from_it.Node()) return retit;

		node* pnode = from_it.Node();
		// �������� �� �������� ������
		if( !insidelist::erase( pnode->links[0])) return retit;
		// �������� � ����� �����
		insidelist::insertbefore(pnode->links[0], *(to_it.plistnode));

		iterator newit = iterator(&(pnode->links[0]));
		if( pnewit) *pnewit = newit;

		return retit;
	}

	// ����������� ������ ���������
	iterator move(iterator before_it, iterator begin, iterator end)
	{
		/*/
		node* begin_node  = begin.Node();
		node* end_node    = end.Node();
		node* before_node = before_it.Node();

		insidelist::movebefore(begin_node->links[0], end_node->links[0], before_node->links[0]);
		return before_it;
		/*/
		iterator from_it = begin;
		iterator retit = before_it;

		while (from_it != end)
		{
			if( from_it.IsEnd()) 
				return before_it;
			from_it = move(from_it, before_it);
		}
		return from_it;
		//*/
	}

	// ���������� ������� 
	iterator copy(const_iterator from_it, iterator to_it)
	{
		const_iterator srcit = from_it;
		const_iterator endit = from_it; endit++;

		node* newnode = new node(*srcit);
		iterator dstit = iterator(&newnode->links[0]);
		dstit = dstit.begin();
		srcit = srcit.begin();
		for( ; srcit!=endit; )
		{
			if( !srcit.IsEnd())
			{
				dstit = insert(dstit, *srcit);
				if(srcit.end() != srcit.begin())
				{
					srcit = srcit.begin();
					dstit = dstit.begin();
				}
				else
				{
					srcit++;
					dstit++;
				}
				continue;
			}
			// � ����:
			srcit = srcit.parent();
			dstit = dstit.parent();
			srcit++;
			dstit++;
		}
		insidelist::insertbefore(newnode->links[0], *to_it.plistnode);
		return iterator(&(newnode->links[0]));
	}

};

}

#endif
