#pragma once

// ������ �������� � ���������������� ��� stdout � pipe � ������� � ����������� ��������
#include <windows.h>
namespace Util
{
	
	// Create a pipe for the child process's STDOUT. 
	SECURITY_ATTRIBUTES saAttr; 
	saAttr.nLength = sizeof(SECURITY_ATTRIBUTES); 
	saAttr.bInheritHandle = TRUE; 
	saAttr.lpSecurityDescriptor = NULL; 

	HANDLE hFile, hFileError;
	char filename[1024];
	for( i=0; ; i++)
	{
		_snprintf(filename, 1024, "c:/temp/alfspool/ulmtor%04d.tcl", i);
		Util::create_directory_for_file(filename);

		hFile = CreateFile(filename, GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ, &saAttr, CREATE_NEW, FILE_ATTRIBUTE_NORMAL/*|FILE_ATTRIBUTE_TEMPORARY*/, NULL);
		if( hFile!=INVALID_HANDLE_VALUE)
		{
			hFileError = CreateFile((std::string(filename)+".err").c_str(), GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ, &saAttr, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL/*|FILE_ATTRIBUTE_TEMPORARY*/, NULL);
			break;
		}

		if( GetLastError()!=ERROR_ALREADY_EXISTS)
			continue;
	}

	STARTUPINFO si = { sizeof(si) };
	
	if( !bDistRender)
	{
		si.hStdInput = GetStdHandle(STD_INPUT_HANDLE); 
		si.hStdError = hFileError; 
		si.hStdOutput = hFile;
	}
	else
	{
		si.hStdInput = GetStdHandle(STD_INPUT_HANDLE); 
		si.hStdError = GetStdHandle(STD_ERROR_HANDLE); 
		si.hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE); 
	}
	si.dwFlags |= STARTF_USESTDHANDLES;
	PROCESS_INFORMATION pi;
  
	DWORD exitcode=-1;
	BOOL res = CreateProcess( NULL, (LPSTR)startline.c_str(), NULL, NULL, TRUE, 0, NULL, NULL, &si, &pi);
	if ( res)
	{
		WaitForSingleObject( pi.hProcess, INFINITE );
		GetExitCodeProcess(pi.hProcess, &exitcode);
		CloseHandle(pi.hProcess);
	}
	CloseHandle(hFileError);
	if( bDistRender)
	{
		CloseHandle(hFile);
		return exitcode;
	}
	if( !res)
	{
		CloseHandle(hFile);
		return 2;
	}
	if( exitcode!=0) 
		return exitcode;

	SetFilePointer(hFile, 0, 0, FILE_BEGIN);

	std::string data;
	data.reserve(20000);
	for(;;)
	{
		DWORD nRead;
		char c;
		BOOL bResult = ReadFile(hFile, &c, 1, &nRead, NULL);
		if( !bResult || !nRead)
			break;
//		if(c==EOF)
//			break;
		data.push_back(c);
	}
	CloseHandle(hFile);


}
