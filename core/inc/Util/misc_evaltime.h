/// �������� ��.evaltime.h
///
///


#ifndef MISK_EVALTIME_H__
#define MISK_EVALTIME_H__
#include <winbase.h>


namespace Util
{
	// ������� ������� ���������� �������
	struct FunEvalTimeCounter
	{
		double sumtime;		// �������� ����� ����������
		long evalcnt;		// ����� ������� 
		double averagetime;	// ������� ����� 
		double peaktime;	// ������� ����� 
		double mintime;
		FunEvalTimeCounter(){reset();}
		void reset(){sumtime=0; evalcnt=0; peaktime=0; mintime=1.0e100; }
		void calc()
		{
			if( evalcnt)
				averagetime = sumtime/evalcnt;
			else
				averagetime = 0;
		}
	};
	// ����� ���������� �������
	class FunEvalTime
	{
		FunEvalTimeCounter* counter;
		LARGE_INTEGER starttime;
	public:
		FunEvalTime(FunEvalTimeCounter& _counter)
		{
			start(_counter);
		}
		~FunEvalTime()
		{
			stop();
		}

		void start(FunEvalTimeCounter& _counter)
		{
			counter = &_counter;
			if( counter)
				QueryPerformanceCounter(&starttime);
		}
		void stop()
		{
			if( !counter)
				return;
			LARGE_INTEGER endtime;
			QueryPerformanceCounter(&endtime);
			starttime.QuadPart = endtime.QuadPart-starttime.QuadPart;
			if( !QueryPerformanceFrequency(&endtime))
				endtime.QuadPart=1;
			double delta = (double)starttime.QuadPart/(double)endtime.QuadPart;
			counter->sumtime += delta;
			counter->evalcnt++;
			counter->calc();
			if( delta>counter->peaktime) counter->peaktime=delta;
			if( delta<counter->mintime) counter->mintime=delta;
			counter = NULL;
		}
	};
};

#endif
