#pragma once

#include "Util/Stream.h"
//#include <fstream>
#include <stdio.h>

namespace Util
{
/// ��������� �������, ������� ������������� ������
class FileStream : public Stream
{
public:
	FILE* file;
//	std::fstream file;
	bool bSaveMode;
//	bool bAsciiMode;
	int pointer; // ��� �������
	bool bCloseFile;
public:
	FileStream()
	{
		file = NULL;
		bCloseFile = false;
	}
	~FileStream()
	{
		close();
	}
	bool open(FILE* file, bool bSaveMode = false)
	{
		close();
		if(!file) 
			return false;
		bCloseFile = false;
		this->file = file;
		this->bSaveMode = bSaveMode;
		this->pointer = ftell(file);

		return true;
	}
	bool open(const char* filename, bool bSaveMode = false, bool bAsciiMode=false)
	{
		close();
		this->bSaveMode = bSaveMode;
		this->pointer = 0;
//		std::ios_base::openmode mode = bSaveMode ? std::ios_base::out : std::ios_base::in;
//		if(bSaveMode)
//			mode |= std::ios_base::trunc;
//		mode |= std::ios_base::binary;
		
		file = fopen( filename, bSaveMode?"wb":"rb");
//		file.open(filename, mode);
//		if( !file.is_open())
		if( !file)
		{
//			DWORD err = GetLastError();
			return false;
		}
		bCloseFile = true;

		if( bSaveMode)
		{
//			this->bAsciiMode = bAsciiMode;
//			if( bAsciiMode)
//				file << "a";
//			else
//				file << "b";
		}
		else
		{
//			char mode;
//			file >> mode;
//			this->bAsciiMode = mode=='a';
		}
		return true;
	};
	void close()
	{	
//		file.close();
		if( file && bCloseFile) 
			fclose(file); 
		file=NULL;
	}

	/// ����� ������
	virtual bool isLoading(){return !bSaveMode;}
	virtual bool isSaving(){return bSaveMode;}
//	virtual bool is_ascii(){return bAsciiMode;}

	/// ������������
	virtual Stream &serialize(void *data, unsigned size)
	{
		if(bSaveMode)
			fwrite(data, size, 1, file);
//			file.write((char*)data, size);
		else
			fread(data, size, 1, file);
//			file.read((char*)data, size);
		pointer += size;
		return *this;
	}
	int tell()
	{
		return ftell(file);
	}
	void seek(int pointer)
	{
		fseek(file, pointer, SEEK_SET);
	}
	/*/
	template<typename T>
	Stream& ascii(T& data)
	{
		if(bSaveMode)
			file << data << "\n";
		else
			file >> data;
		return *this;
	}
	/*/
};

}
