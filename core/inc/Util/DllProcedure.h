//#ifndef _UTIL_DLLPROCEDURE_H 
//#define _UTIL_DLLPROCEDURE_H
#pragma once

#include <string>
#include "Util/STLStream.h"

#ifndef LINUX
#    include <windows.h>
#else

#endif

//#define DLLPROCEDURE_DUMP

namespace Util
{
struct DllProcedure
{
	std::string dll;
	std::string entrypoint;

#	ifndef LINUX
	HMODULE module;
	FARPROC proc;
#	else

#	endif

	DllProcedure()
	{
#	ifndef LINUX
		module = NULL;
		proc = NULL;
#	else
#	endif
	}
	DllProcedure(const char* dll, const char* entrypoint)
	{
#	ifndef LINUX
		module = NULL;
		proc = NULL;
		Load(dll, entrypoint);
#	else
#	endif
	}
	~DllProcedure()
	{
		Free();
	}
	DllProcedure(const DllProcedure& arg)
	{
#	ifndef LINUX
		module = NULL;
		proc = NULL;
		*this = arg;
#	else
#	endif
	}
	DllProcedure& operator =(const DllProcedure& arg)
	{
		this->Load(arg.dll.c_str(), arg.entrypoint.c_str());
		return *this;
	}

	bool Load(const char* dll, const char* entrypoint)
	{
		this->Free();

		this->dll = dll;
		this->entrypoint = entrypoint;
		return Load();
	}
	// ������: procname@dllname
	bool LoadFormated(const char* dllNproc, const char* defaultdll="")
	{
		std::string buf = dllNproc;
		char *token = strtok( (char*)buf.c_str(), "@");
		if( token == NULL) return false;

		this->dll = token;
		token = strtok( NULL, "@" );
		if( token == NULL)
		{
			this->entrypoint = this->dll;
			this->dll = defaultdll;
		}
		else
		{
			this->entrypoint = token;
		}
		return Load();
	}
	// ������: procname@dllname
	std::string GetFormated()
	{
		std::string name = this->dll + "@" + this->entrypoint;
		return name;
	}
	bool Load()
	{
#	ifndef LINUX
		if( this->dll.empty())
			return false;
		if( this->entrypoint.empty())
			return false;
		module = LoadLibrary(this->dll.c_str());
		if( !module)
		{
			fprintf(stderr, "Module %s not found\n", this->dll.c_str());
			fflush(stderr);
			return false;
		}
		proc = GetProcAddress(module, this->entrypoint.c_str());
		if( !proc)
		{
			FreeLibrary(module);
			module = NULL;
			fprintf(stderr, "Entry point %s in module %s not found\n", this->entrypoint.c_str(), this->dll.c_str());
			fflush(stderr);
			return false;
		}
#ifdef DLLPROCEDURE_DUMP
fprintf(stderr, "%x load %s\n", (int)this, this->dll.c_str());
fflush(stderr);
#endif 
#	else
#	endif
		return true;
	}
	void Free()
	{
#	ifndef LINUX
		if( module)
		{
			FreeLibrary(module);
#ifdef DLLPROCEDURE_DUMP
fprintf(stderr, "%x free %s\n", (int)this, this->dll.c_str());
fflush(stderr);
#endif 
		}
		module = NULL;
		proc = NULL;
#	else
#	endif
	}
	bool isValid()
	{
#	ifndef LINUX
		return proc!=NULL;
#	else
		return false;
#	endif
	}

	#ifdef OCELLARIS
	void serializeOcs(
		const char* _name, 
		cls::IRender* render, 
		bool bSave
		);
	#endif

};
template <class PROCTYPE>
struct DllProcedureTmpl : public DllProcedure
{
	DllProcedureTmpl()
		:DllProcedure()
	{
	}
	DllProcedureTmpl(const char* dll, const char* entrypoint)
		:DllProcedure(dll, entrypoint)
	{
	}
	DllProcedureTmpl& operator =(const DllProcedure& arg)
	{
		this->Load(arg.dll.c_str(), arg.entrypoint.c_str());
		return *this;
	}
	PROCTYPE operator*()
	{
		if(!isValid()) return NULL;
#	ifndef LINUX
		return (PROCTYPE)proc;
#	else
		return NULL;
#	endif
	}
};

}

template<class Stream> inline
Stream& operator >> (Stream& out, Util::DllProcedure& v)
{
	out >> v.dll >> v.entrypoint;
	if(out.isLoading())
		v.Load(v.dll.c_str(), v.entrypoint.c_str());
	return out;
}

#ifdef OCELLARIS
inline void Util::DllProcedure::serializeOcs(
	const char* _name, 
	cls::IRender* render, 
	bool bSave
	)
{
	std::string name = _name;

	SERIALIZEOCS(dll);
	SERIALIZEOCS(entrypoint);
	if(!bSave)
		this->Load(dll.c_str(), entrypoint.c_str());
}
#endif

//#endif

