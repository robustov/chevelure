#ifndef _W32_MMF_WR_H__
#define _W32_MMF_WR_H__

// MMF ���� ��� ������ 
#include "../Util/error.h"
#include <windows.h>
#include "../Util/misc_ptr.h"

namespace misc
{
	// ���� ������ ���������� (��������� ������ �� ���������)
	class mmf_write
	{
	public:
		// ��� ������
		typedef long valuepointer;
	protected:
		struct header
		{
			long realAllocSize;		// ������ ����� �����
			long bookMarkPtr;		// �������� �� ��������
		};

		// �������� ����
		char filename[256];
//		HANDLE hFile;
//		HANDLE hMMF;
		char* map;
		long filesize;
		// ���������:
		header theHeader;
		header* pheader;

		// ����������������� ������
		char* reservMemory;
		long reservSize;
		long commitSize;
		long realUserSize;
		long dwPageSize;

		// ��������������� ��������� ������
		valuepointer predictionpointer;	// ��������������� ���������
		long predictionlen;				// ��������������� ������
		// ������������
		long allignment;

		long align(long value)
		{
			return ((value-1) & (~(dwPageSize-1))) + dwPageSize;
		}
		long alignsize(long value)
		{
			if( allignment==1) return value;
			return ((value-1) & (~(allignment-1))) + allignment;
		}
	// ������� ��������� ������
	public:	
		int partition(const valuepointer* pvaluepointer) const
		{
			if(*pvaluepointer>=filesize) return 1;
			return 0;
		}
		void* get( const valuepointer* pvaluepointer) const
		{
			if(*pvaluepointer>=filesize) 
				return reservMemory + *pvaluepointer - filesize;
			else
				return map + *pvaluepointer;
		};
		template<class T> T* alloc(T*& newobject, unsigned long* offset=NULL)
		{
			long ofs;
			if( !alloc(sizeof(T), &ofs)) return NULL;
			if(offset) *offset = ofs;
			newobject = (T*)get(&ofs);
			return newobject;
		}
		template<class T> T* alloccopy(const T& srcobject, unsigned long* offset=NULL)
		{
			long ofs;
			if( !alloc(sizeof(T), &ofs)) return NULL;
			if(offset) *offset = ofs;
			T* newobject = (T*)get(&ofs);
			*newobject = srcobject;
			return newobject;
		}
		template<class T> T* allocarray(T*& newobject, int size, unsigned long* offset=NULL)
		{
			long ofs;
			if( !alloc(sizeof(T)*size, &ofs)) return NULL;
			if(offset) *offset = ofs;
			newobject = (T*)get(&ofs);
			return newobject;
		}
		bool alloc(long len, valuepointer* pvaluepointer)
		{
			len = alignsize(len);
			// ������������ ���������?
			if( isvalid(&predictionpointer) && len<=predictionlen)
			{
				*pvaluepointer = predictionpointer;
				predictionpointer += len;
				predictionlen -= len;
				return true;
			}

			if( realUserSize + len>reservSize) 
				throw Util::Error("Memory overflow %d", reservSize);
			*pvaluepointer = realUserSize + filesize;

			if( realUserSize + len>commitSize)
			{
				// ���������� ������� ����:
				long newcommitsize = align(realUserSize + len);
				LPVOID ret = VirtualAlloc( reservMemory+commitSize, newcommitsize - commitSize, MEM_COMMIT, PAGE_READWRITE);
				if( ret != reservMemory+commitSize) 
					throw Util::Error("VirtualAlloc() %d", GetLastError());
				commitSize = newcommitsize;
			}
			pheader->realAllocSize += len;
			realUserSize += len;
			return true;
		};
		void free( valuepointer* pvaluepointer)
		{
			*pvaluepointer = 0;
		};
		int GetRollback()
		{
			return realUserSize;
		}
		void Rollback(int rollback)
		{
			realUserSize = rollback;
		}
		static void null(valuepointer* pvaluepointer)
		{
			*pvaluepointer = 0;
		}
		static bool isvalid( valuepointer* pvaluepointer)
		{
			return *pvaluepointer!=0;
		}
	// �������������� ��������:
	public:
		// ��������������� ��������� ������
		// ������������ ��� ��������� ������ � ��� ���������� �������
		void allocprediction( valuepointer valuepointer, long len)
		{
			this->predictionpointer = valuepointer;
			this->predictionlen = len;
		}

	public:
			
		mmf_write(long allignment=1)
		{
			this->allignment=allignment;
			SYSTEM_INFO si;
			GetSystemInfo(&si);
			dwPageSize = si.dwPageSize;

			map = NULL;
			filename[0] = 0;
//			hMMF = NULL;
			reservMemory = NULL;
//			hFile = NULL;

			null( &predictionpointer);	// ��������������� ���������
			predictionlen = 0;			// ��������������� ������
		}
		~mmf_write(){Close();Reset();}
		bool Create(LPCTSTR filename, long maxfilesize=0x1000000)
		{
			try
			{
				strcpy( this->filename, filename);
				// ������� ����
//				hFile = CreateFile(filename, GENERIC_WRITE|GENERIC_READ, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
//				if( hFile==INVALID_HANDLE_VALUE) throw Util::Error( "CreateFile() %d", GetLastError());

				filesize = 0;

				// �������� ���������
				pheader = &theHeader;
				pheader->realAllocSize  = 0;
				pheader->bookMarkPtr	= 0;

				// ������������� ������
				reservSize = maxfilesize;
				reservMemory = (char*)VirtualAlloc(NULL, reservSize, MEM_RESERVE, PAGE_READWRITE);
				if(!reservMemory) 
					throw Util::Error( "VirtualAlloc(MEM_RESERVE) %d byte - %d", reservSize, GetLastError());

				commitSize = 0;
				realUserSize = 0;
			}
			catch(...)
			{
				Close();
				throw;
			}
			return true;
		}
		// ������� ��� ������
		void Reset()
		{
			if( commitSize)
			{
				BOOL res = VirtualFree(reservMemory, commitSize, MEM_DECOMMIT);
				commitSize = 0;
			}
			if( reservMemory)
			{
				BOOL res = VirtualFree(reservMemory, 0, MEM_RELEASE);
				reservMemory = NULL;
			}
			filename[0] = 0;
		}
		// ������� � ��������
		void Close()
		{
			HANDLE hFile = NULL;
			try
			{
				if(filename[0])
				{
					hFile = CreateFile(filename, GENERIC_WRITE|GENERIC_READ, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
					if( hFile==INVALID_HANDLE_VALUE) 
						throw Util::Error( "CreateFile() %d", GetLastError());
					// �������� ����� ������ � ����
					if( hFile&&reservMemory)
					{
						DWORD dwLen;
						SetFilePointer(hFile, 0, 0, FILE_END);
						BOOL bWrite = WriteFile( hFile, reservMemory, realUserSize, &dwLen, NULL);
						long err = GetLastError();
						if( !bWrite) 
							throw Util::Error( "WriteFile(MEM_RESERVE) %d", GetLastError());
					}
				}
			}
			catch(...)
			{
				if( hFile)
					CloseHandle( hFile);
				Reset();
				throw;
			}
			if( hFile)
				CloseHandle( hFile);
			Reset();
		}
		int getSize()
		{
			return commitSize;
		}
	};
}

#pragma warning(disable : 4291 )

// �������� ��������� ������ new
// �������������:
//              misc::mmf_write file;
//              BBB* pbbb = new(file) BBB;
//              ptr<BBB> ptb = new(file) BBB;
#ifdef new
    #undef new
#endif

inline void* __cdecl operator new(
	size_t s, 
	misc::mmf_write& file)
{ 
	long ofs;
	if( !file.alloc((long)s, &ofs)) return NULL;
	char* p = (char*)file.get(&ofs);
	return p;
}

#endif

