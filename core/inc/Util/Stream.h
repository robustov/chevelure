#pragma once

//#include <fstream>

namespace Util
{
/// ��������� �������, ������� ������������� ������
class Stream
{
public:
	Stream(){}
	~Stream(){}

	/// ����� ������
	virtual bool isLoading()=0;
	virtual bool isSaving()=0;
//	virtual bool is_ascii()=0;

	/// ������������
	virtual Stream &serialize(void *data, unsigned size)=0;

		/*/
	template<typename T>
	Stream& ascii(T& data)
	{
		if(bSaveMode)
			file << data << "\n";
		else
			file >> data;
		return *this;
	}
		/*/
};

inline Stream& operator >>(Stream& stream, double& value)
{
//	if( stream.is_ascii())
//		return stream.ascii(value);
	return stream.serialize(&value, sizeof(double));
}

inline Stream &operator >>(Stream &stream, float& value)
{
//	if( stream.is_ascii())
//		return stream.ascii(value);
	return stream.serialize(&value, sizeof(float));
}

inline Stream& operator >>(Stream& stream, int& value)
{
//	if( stream.is_ascii())
//		return stream.ascii(value);
	return stream.serialize(&value, sizeof(int));
}

inline Stream& operator >>(Stream& stream, unsigned int& value)
{
//	if( stream.is_ascii())
//		return stream.ascii(value);
	return stream.serialize(&value, sizeof(unsigned int));
}

inline Stream& operator >>(Stream& stream, long& value)
{
//	if( stream.is_ascii())
//		return stream.ascii(value);
	return stream.serialize(&value, sizeof(long));
}

inline Stream& operator >>(Stream& stream, unsigned long& value)
{
//	if( stream.is_ascii())
//		return stream.ascii(value);
	return stream.serialize(&value, sizeof(unsigned long));
}

inline Stream& operator >>(Stream& stream, short& value)
{
//	if( stream.is_ascii())
//		return stream.ascii(value);
	return stream.serialize(&value, sizeof(short));
}

inline Stream& operator >>(Stream& stream, unsigned short& value)
{
//	if( stream.is_ascii())
//		return stream.ascii(value);
	return stream.serialize(&value, sizeof(unsigned short));
}

inline Stream& operator >>(Stream& stream, char& value)
{
//	if( stream.is_ascii())
//		return stream.ascii(value);
	return stream.serialize(&value, sizeof(char));
}

inline Stream& operator >>(Stream& stream, unsigned char& value)
{
//	if( stream.is_ascii())
//		return stream.ascii(value);
	return stream.serialize(&value, sizeof(unsigned char));
}

inline Stream& operator >>(Stream& stream, bool& value)
{
//	if( stream.is_ascii())
//		return stream.ascii(value);
	return stream.serialize(&value, sizeof(bool));
}

template <class T>
Stream &operator <<(Stream &ser, T object)
{
	return ser >> object;
}

}
