#pragma once
// MayaSerialize.h
#include <maya/MBoundingBox.h>
#include <maya/MMatrix.h>
#include <maya/MFloatPoint.h>
#include "Util/Stream.h"

template<class Stream> inline
Stream& operator >> (Stream& out, MMatrix& v)
{
	for(int x=0; x<4; x++)
		for(int y=0; y<4; y++)
			out >> v.matrix[x][y];
	return out;
}

template<class Stream> inline
Stream& operator >> (Stream& out, MVector& v)
{
	out >> v.x >> v.y >> v.z;
	return out;
}

template<class Stream> inline
Stream& operator >> (Stream& out, MPoint& v)
{
	out >> v.x >> v.y >> v.z >> v.w;
	return out;
}
