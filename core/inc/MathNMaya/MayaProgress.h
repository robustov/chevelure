#pragma once

#include <maya/MProgressWindow.h>
class MayaProgress
{
	bool bProgress;
public:
	MayaProgress(const char* title, const char* status )
	{
		bProgress = MGlobal::mayaState()==MGlobal::kInteractive;

		if( bProgress)
		{
			MProgressWindow::reserve();
			MProgressWindow::setInterruptable(true);
			MProgressWindow::setTitle(title);
			MProgressWindow::startProgress();
			MProgressWindow::setProgressRange(0, 100);
			MProgressWindow::setProgressStatus(status);
		}
	}
	bool OnProgress(float p, char* text=NULL)
	{
		static int lastf=-100;
//		bool bProgress = MGlobal::mayaState()==MGlobal::kInteractive;
		if( bProgress)
		{
			int f = (int)(p*100);
			if( abs(lastf-f)>5) 
			{
				MProgressWindow::setProgress(f);
				lastf=f;
			}
			if( text)
			{
				MProgressWindow::setProgressStatus(text);
			}
			if( MProgressWindow::isCancelled())
				return false;
		}
		else
		{
			fprintf(stderr, "R90000   %02d%%\n", (int)(p*100.f));
			fflush(stderr);
		}
		return true;
	}
	~MayaProgress()
	{
		if( bProgress)
			MProgressWindow::endProgress();
	}
};
