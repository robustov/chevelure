#pragma once

#include "Math/Math.h"
#include "Math/Box.h"
#include <maya/MBoundingBox.h>
#include <maya/MMatrix.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MVector.h>
#include <maya/MPoint.h>

inline void copy( Math::Vec3f& dst, const MVector& src)
{
	dst = Math::Vec3f((float)src.x, (float)src.y, (float)src.z);
}
inline void copy( Math::Vec3f& dst, const MPoint& src)
{
	dst = Math::Vec3f((float)src.x, (float)src.y, (float)src.z);
}
inline void copy( Math::Vec3f& dst, const double3& src)
{
	dst = Math::Vec3f((float)src[0], (float)src[1], (float)src[2]);
}
inline void copy( Math::Vec3f& dst, const MFloatPoint& src)
{
	dst = Math::Vec3f(src.x, src.y, src.z);
}
inline void copy( Math::Vec3f& dst, const MFloatVector& src)
{
	dst = Math::Vec3f(src.x, src.y, src.z);
}
inline void copy( Math::Vec2f& dst, const float2& src)
{
	dst = Math::Vec2f(src[0], src[1]);
}
inline void copy( Math::Vec2f& dst, const MVector& src)
{
	dst = Math::Vec2f((float)src.x, (float)src.y);
}
inline void copy( Math::Matrix4f& dst, const MMatrix& src)
{
	float dest[4][4];
	src.get(dest);
	dst[0] = Math::Vec4f(dest[0][0], dest[0][1], dest[0][2], dest[0][3]);
	dst[1] = Math::Vec4f(dest[1][0], dest[1][1], dest[1][2], dest[1][3]);
	dst[2] = Math::Vec4f(dest[2][0], dest[2][1], dest[2][2], dest[2][3]);
	dst[3] = Math::Vec4f(dest[3][0], dest[3][1], dest[3][2], dest[3][3]);
}
inline void copy( MMatrix& dst, const Math::Matrix4f& src)
{
	float dest[4][4];
	for(int i=0; i<4; i++)
		for(int j=0; j<4; j++)
			dest[i][j] = src[i][j];
	dst = MMatrix(dest);
}
inline void copy( Math::Box3f& dst, const MBoundingBox& src)
{
	Math::Vec3f p1, p2;
	copy(p1, src.min());
	copy(p2, src.max());
	dst = Math::Box3f(p1, p2);
}


inline void copy( MVector& dst, const Math::Vec3f& src)
{
	dst = MPoint(src.x, src.y, src.z);
}
inline void copy( MPoint& dst, const Math::Vec3f& src)
{
	dst = MPoint(src.x, src.y, src.z);
}
inline void copy( MBoundingBox& dst, const Math::Box3f& src)
{
	MPoint p1, p2;
	copy(p1, src.min);
	copy(p2, src.max);
	dst = MBoundingBox(p1, p2);
}