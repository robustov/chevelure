#pragma once

//
// ������� ��� �-��� ������
// #include "pointcloud.h"
// 
struct IPointCloud
{
	virtual void* PtcCreatePointCloudFile(
		char *filename,
		int nvars, 
		char **vartypes, char **varnames,
        float *world2eye, 
		float *world2ndc, 
		float *format)=0;

	virtual int PtcWriteDataPoint(
		void* pointcloud,
		float *point, float *normal, float radius, float *data
		)=0;
	virtual void PtcFinishPointCloudFile(
		void* pointcloud
		)=0;

	virtual void* PtcOpenPointCloudFile(
		char *filename,
		int *nvars, char **vartypes, char **varnames
		)=0; // superceded
	virtual void* PtcSafeOpenPointCloudFile(
		char *filename
		)=0;
	virtual int PtcGetPointCloudInfo(
		void* pointcloud,
        char *request, 
		void *result
		)=0;
	virtual int PtcReadDataPoint(
		void* pointcloud,
		float *point, 
		float *normal, 
		float *radius, 
		float *data)=0;
	virtual void PtcClosePointCloudFile(
		void* pointcloud
		)=0;
};

typedef IPointCloud* (*t_getIPointCloud)(void);

#include "Util/DllProcedure.h"
typedef Util::DllProcedureTmpl< t_getIPointCloud> getIPointCloudDll;

// �������������:
/*/
#ifndef _DEBUG
	getIPointCloudDll PointCloudDll("IPrman.dll", "getIPointCloud");
#else
	getIPointCloudDll PointCloudDll("IPrmanD.dll", "getIPointCloud");
#endif
	if( !PointCloudDll.isValid())
		return -1;
	IPointCloud* pPointCloud = (*PointCloudDll)();

/*/
