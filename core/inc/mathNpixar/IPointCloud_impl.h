#pragma once 

// ������ � 13.5 ����������
#ifdef RI_POINTCLOUD

#include "IPointCloud.h"
#include <pointcloud.h>

struct IPointCloud_impl : public IPointCloud
{
	virtual void* PtcCreatePointCloudFile(
		char *filename,
		int nvars, 
		char **vartypes, char **varnames,
        float *world2eye, 
		float *world2ndc, 
		float *format)
	{
		return ::PtcCreatePointCloudFile(
			filename,
			nvars, 
			vartypes, 
			varnames,
			world2eye, 
			world2ndc, 
			format);
	}

	virtual int PtcWriteDataPoint(
		void* pointcloud,
		float *point, float *normal, float radius, float *data
		)
	{
		return ::PtcWriteDataPoint(
			pointcloud,
			point, normal, radius, data
			);
	}
	virtual void PtcFinishPointCloudFile(
		void* pointcloud
		)
	{
		::PtcFinishPointCloudFile(
			pointcloud);
	}

	virtual void* PtcOpenPointCloudFile(
		char *filename,
		int *nvars, char **vartypes, char **varnames
		)
	{
		return ::PtcOpenPointCloudFile(
			filename,
			nvars, vartypes, varnames
			);
	}
	virtual void* PtcSafeOpenPointCloudFile(
		char *filename
		)
	{
		return ::PtcSafeOpenPointCloudFile(
			filename
			);
	}
	virtual int PtcGetPointCloudInfo(
		void* pointcloud,
        char *request, 
		void *result
		)
	{
		return ::PtcGetPointCloudInfo(
				pointcloud,
				request, 
				result
				);
	}
	virtual int PtcReadDataPoint(
		void* pointcloud,
		float *point, 
		float *normal, 
		float *radius, 
		float *data)
	{
		return ::PtcReadDataPoint(
			pointcloud,
			point, 
			normal, 
			radius, 
			data);
	}
	virtual void PtcClosePointCloudFile(
		void* pointcloud
		)
	{
		::PtcClosePointCloudFile(
			pointcloud
			);
	}
};

#endif
