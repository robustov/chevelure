#ifndef _MATHnPIXAR_IPRMANIMPL_H 
#define _MATHnPIXAR_IPRMANIMPL_H
#pragma once

#include "IPrman.h"
#include "rx.h"
#include <vector>
struct Prman_Impl : public IPrman
{
	Prman_Impl(){renderMode=RENDERMODE_ALL;}
	
	virtual int getRenderPass(){return 1;};

	enRenderMode renderMode;
	virtual enRenderMode getRenderMode(){return renderMode;};
	virtual void setRenderMode(enRenderMode mode){renderMode=mode;};

	virtual RtToken	RI_FRAMEBUFFER(){return ::RI_FRAMEBUFFER;}
	virtual RtToken	RI_FILE(){return ::RI_FILE;}
	virtual RtToken	RI_RGB(){return ::RI_RGB;}
	virtual RtToken	RI_RGBA(){return ::RI_RGBA;}
	virtual RtToken	RI_RGBZ(){return ::RI_RGBZ;}
	virtual RtToken	RI_RGBAZ(){return ::RI_RGBAZ;}
	virtual RtToken	RI_A(){return ::RI_A;}
	virtual RtToken	RI_Z(){return ::RI_Z;}
	virtual RtToken	RI_AZ(){return ::RI_AZ;}

/*/
DLLIMPORT extern RtToken	RI_FRAMEBUFFER, RI_FILE;
DLLIMPORT extern RtToken	RI_RGB, RI_RGBA, RI_RGBZ, RI_RGBAZ, RI_A, RI_Z, RI_AZ;
DLLIMPORT extern RtToken  	RI_DEEPSHADOWERROR;
DLLIMPORT extern RtToken 	RI_ADDCOVERAGE;
DLLIMPORT extern RtToken	RI_I;
DLLIMPORT extern RtToken	RI_RGBI, RI_RGBAI, RI_RGBZI, RI_RGBAZI, RI_AI, RI_ZI, RI_AZI;
/*/

	RtToken	RI_PERSPECTIVE(){return ::RI_PERSPECTIVE;}
	RtToken	RI_ORTHOGRAPHIC(){return ::RI_ORTHOGRAPHIC;}
/*/
DLLIMPORT extern RtToken	RI_HIDDEN, RI_PAINT, RI_DEPTHFILTER, RI_PHOTON;
DLLIMPORT extern RtToken	RI_CONSTANT, RI_SMOOTH;
DLLIMPORT extern RtToken  	RI_ORIGIN;
DLLIMPORT extern RtToken  	RI_FLATNESS, RI_MOTIONFACTOR, RI_FOCUSFACTOR, RI_OLDMOTIONFACTOR, 
				RI_TRIMDEVIATION, RI_FOV;
/*/
	virtual RtToken	RI_FOV(){return ::RI_FOV;}
/*/
DLLIMPORT extern RtToken	RI_AMBIENTLIGHT, RI_POINTLIGHT, RI_DISTANTLIGHT, RI_SPOTLIGHT;
DLLIMPORT extern RtToken	RI_INTENSITY, RI_LIGHTCOLOR, RI_FROM, RI_TO, RI_CONEANGLE,
				RI_CONEDELTAANGLE, RI_BEAMDISTRIBUTION;
DLLIMPORT extern RtToken	_RI_SHADERINSTANCEID;
DLLIMPORT extern RtToken	RI_MATTE, RI_METAL, RI_SHINYMETAL;
DLLIMPORT extern RtToken	RI_PLASTIC, RI_PAINTEDPLASTIC;
DLLIMPORT extern RtToken	RI_KA, RI_KD, RI_KS, RI_ROUGHNESS, RI_SPECULARCOLOR;
DLLIMPORT extern RtToken	RI_KR, RI_TEXTURENAME;
DLLIMPORT extern RtToken	RI_DEPTHCUE, RI_FOG, RI_BUMPY;
DLLIMPORT extern RtToken	RI_MINDISTANCE, RI_MAXDISTANCE, RI_BACKGROUND, RI_DISTANCE;
DLLIMPORT extern RtToken	RI_AMPLITUDE;

DLLIMPORT extern RtToken	RI_RASTER, RI_SCREEN, RI_CAMERA, RI_WORLD, RI_OBJECT;
DLLIMPORT extern RtToken	RI_INSIDE, RI_OUTSIDE, RI_LH, RI_RH;
/*/
	virtual RtToken	RI_P(){return ::RI_P;}

/*/
DLLIMPORT extern RtToken	RI_P, RI_PZ, RI_PW, RI_N, RI_NG, RI_NP, RI_CS, RI_OS, RI_CI, RI_OI,
		RI_S, RI_T, RI_ST;
DLLIMPORT extern RtToken	RI_MAXSPECULARDEPTH, RI_MAXDIFFUSEDEPTH;//ray//
DLLIMPORT extern RtToken	RI_DISPLACEMENTS, RI_BIAS, RI_SAMPLEMOTION; // for ray tracing//
DLLIMPORT extern RtToken	RI_DPDU, RI_DPDV; // for ray tracing //
DLLIMPORT extern RtToken	RI_ESTIMATOR; // for globillum and caustics //
DLLIMPORT extern RtToken	RI_SHADINGMODEL; // for photon tracing //
DLLIMPORT extern RtToken	RI_GLOBALMAP; // for global illum //
DLLIMPORT extern RtToken	RI_CAUSTICMAP; // for caustics //
DLLIMPORT extern RtToken	RI_MAXERROR, RI_MAXPIXELDIST; // irrad cache //
DLLIMPORT extern RtToken	RI_FORCEDSAMPLING; // irrad cache //
DLLIMPORT extern RtToken	RI_HANDLE, RI_FILEMODE; // irradiance cache //
/*/
	virtual RtToken	RI_BILINEAR(){return ::RI_BILINEAR;}
	virtual RtToken	RI_BICUBIC(){return ::RI_BICUBIC;}
	virtual RtToken	RI_LINEAR(){return ::RI_LINEAR;}
	virtual RtToken	RI_CUBIC(){return ::RI_CUBIC;}
/*/
DLLIMPORT extern RtToken	RI_PRIMITIVE, RI_INTERSECTION, RI_UNION, RI_DIFFERENCE;
DLLIMPORT extern RtToken	RI_PERIODIC, RI_NONPERIODIC, RI_CLAMP, RI_BLACK;
DLLIMPORT extern RtToken	RI_IGNORE, RI_PRINT, RI_ABORT, RI_HANDLER;
/*/
	virtual RtToken	RI_COMMENT(){return ::RI_COMMENT;}
	virtual RtToken	RI_STRUCTURE(){return ::RI_STRUCTURE;}
//	virtual RtToken	RI_VERBATIM(){return ::RI_VERBATIM;}
	virtual RtToken	RI_VERBATIM(){return 0;}
/*/
DLLIMPORT extern RtToken	RI_IDENTIFIER, RI_NAME, RI_SHADINGGROUP;
DLLIMPORT extern RtToken	RI_WIDTH, RI_CONSTANTWIDTH;


DLLIMPORT extern RtToken 	RI_MINWIDTH, RI_CLAMPWIDTH;


// Added for 4.0 compile
DLLIMPORT extern RtToken	RI_DEVIATION, RI_RASTER, RI_TESSELATION;
DLLIMPORT extern RtToken  RI_TRIMCURVE, RI_PARAMETRIC;
//

DLLIMPORT extern RtToken 	RI_QUANTIZE, RI_DITHER, RI_FILTER, RI_FILTERWIDTH;
DLLIMPORT extern RtToken 	RI_REPELFILE, RI_REPELPARAMS;
DLLIMPORT extern RtToken 	RI_HANDLEID;
DLLIMPORT extern RtToken	RI_THRESHOLD;
DLLIMPORT extern RtBasis	RiBezierBasis, RiBSplineBasis, RiCatmullRomBasis,
		RiHermiteBasis, RiPowerBasis;

#define RI_BEZIERSTEP		((RtInt)3)
#define RI_BSPLINESTEP		((RtInt)1)
#define RI_CATMULLROMSTEP	((RtInt)1)
#define RI_HERMITESTEP		((RtInt)2)
#define RI_POWERSTEP		((RtInt)4)

DLLIMPORT extern RtInt	RiLastError;


	// Declarations of All the RenderMan Interface Subroutines //

/*/
	virtual RtFilterFunc RiGaussianFilter(){return ::RiGaussianFilter;}
	virtual RtFilterFunc RiBoxFilter(){return ::RiBoxFilter;}
	virtual RtFilterFunc RiTriangleFilter(){return ::RiTriangleFilter;}
	virtual RtFilterFunc RiCatmullRomFilter(){return ::RiCatmullRomFilter;}
	virtual RtFilterFunc RiSeparableCatmullRomFilter(){return NULL;}//return ::RiSeparableCatmullRomFilter;}
	virtual RtFilterFunc RiBlackmanHarrisFilter(){return NULL;}//return ::RiBlackmanHarrisFilter;}
	virtual RtFilterFunc RiLanczosFilter(){return NULL;}//return ::RiLanczosFilter;}
	virtual RtFilterFunc RiMitchellFilter(){return NULL;}//return ::RiMitchellFilter;}
	virtual RtFilterFunc RiSincFilter(){return ::RiSincFilter;}
	virtual RtFilterFunc RiBesselFilter(){return ::RiBesselFilter;}
	virtual RtFilterFunc RiDiskFilter(){return ::RiDiskFilter;}

/*/
DLLIMPORT extern RtFloat	RiGaussianFilter(RtFloat x, RtFloat y,
			RtFloat xwidth, RtFloat ywidth);
DLLIMPORT extern RtFloat	RiBoxFilter(RtFloat x, RtFloat y,
			RtFloat xwidth, RtFloat ywidth);
DLLIMPORT extern RtFloat	RiTriangleFilter(RtFloat x, RtFloat y,
			RtFloat xwidth, RtFloat ywidth);
DLLIMPORT extern RtFloat	RiCatmullRomFilter(RtFloat x, RtFloat y,
			RtFloat xwidth, RtFloat ywidth);
DLLIMPORT extern RtFloat	RiSeparableCatmullRomFilter(RtFloat x, RtFloat y,
			RtFloat xwidth, RtFloat ywidth);
DLLIMPORT extern RtFloat	RiBlackmanHarrisFilter(RtFloat x, RtFloat y,
			RtFloat xwidth, RtFloat ywidth);
DLLIMPORT extern RtFloat	RiLanczosFilter(RtFloat x, RtFloat y,
			RtFloat xwidth, RtFloat ywidth);
DLLIMPORT extern RtFloat	RiMitchellFilter(RtFloat x, RtFloat y,
			RtFloat xwidth, RtFloat ywidth);
DLLIMPORT extern RtFloat	RiSincFilter(RtFloat x, RtFloat y,
			RtFloat xwidth, RtFloat ywidth);
DLLIMPORT extern RtFloat	RiBesselFilter(RtFloat x, RtFloat y,
			RtFloat xwidth, RtFloat ywidth);
DLLIMPORT extern RtFloat	RiDiskFilter(RtFloat x, RtFloat y,
			RtFloat xwidth, RtFloat ywidth);
DLLIMPORT extern RtVoid	RiErrorIgnore(RtInt code, RtInt severity, char *msg);
DLLIMPORT extern RtVoid	RiErrorPrint(RtInt code, RtInt severity, char *msg);
DLLIMPORT extern RtVoid	RiErrorPrintOnce(RtInt code, RtInt severity, char *msg);
DLLIMPORT extern RtVoid	RiErrorCondAbort(RtInt code, RtInt severity, char *msg);
DLLIMPORT extern RtVoid	RiErrorAbort(RtInt code, RtInt severity, char *msg);
DLLIMPORT extern RtVoid   RiErrorCleanup(void);
/*/
	virtual RtSubdivfunc RiProcDelayedReadArchive(){return ::RiProcDelayedReadArchive;}
	virtual RtSubdivfunc RiProcRunProgram(){return ::RiProcRunProgram;}
	virtual RtSubdivfunc RiProcDynamicLoad(){return ::RiProcDynamicLoad;}

/*/
DLLIMPORT extern RtContextHandle RiGetContext(void);
DLLIMPORT extern RtVoid RiContext(RtContextHandle);

DLLIMPORT extern RtToken
	RiDeclare(char *name, char *declaration);

DLLIMPORT extern RtVoid
/*/
	virtual RtVoid RiBegin(RtToken name)
	{
		::RiBegin(name);
	}
	virtual RtVoid RiEnd(void)
	{
		::RiEnd();
	}
	virtual RtVoid RiFrameBegin(RtInt frame)
	{
		::RiFrameBegin(frame);
	}
	virtual RtVoid RiFrameEnd(void)
	{
		::RiFrameEnd();
	}
	virtual RtVoid RiWorldBegin(void)
	{
		::RiWorldBegin();
	}
	virtual RtVoid RiWorldEnd(void)
	{
		::RiWorldEnd();
	}
	RtVoid RiFormat(RtInt xres, RtInt yres, RtFloat aspect)
	{
		::RiFormat(xres, yres, aspect);
	}
	virtual RtVoid RiFrameAspectRatio(RtFloat aspect)
	{
		::RiFrameAspectRatio(aspect);
	}
	virtual RtVoid RiScreenWindow(RtFloat left, RtFloat right, RtFloat bot, RtFloat top)
	{
		::RiScreenWindow(left, right, bot, top);
	}
	virtual RtVoid RiCropWindow(RtFloat xmin, RtFloat xmax, RtFloat ymin, RtFloat ymax)
	{
		::RiCropWindow(xmin, xmax, ymin, ymax);
	}
	virtual RtVoid RiProjection(RtToken name, ...)
	{
		va_list marker;
		va_start( marker, name );     /* Initialize variable arguments. */

		std::vector<RtToken> tokens; std::vector<RtPointer> parms;
		int count = BuildParamList(tokens, parms, marker);
		::RiProjectionV(name, count, &tokens[0], &parms[0]);
		va_end( marker );              /* Reset variable arguments.      */
	}
	virtual RtVoid RiProjectionV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[])
	{
		::RiProjectionV(name, n, tokens, parms);
	}
	virtual RtVoid RiClipping(RtFloat hither, RtFloat yon)
	{
		::RiClipping(hither, yon);
	}
	/*/
	virtual RtVoid RiClippingPlane(RtFloat Nx, RtFloat Ny, RtFloat Nz, RtFloat Px, 
                        RtFloat Py, RtFloat Pz)
	{
		::RiClippingPlane(Nx, Ny, Nz, Px, Py, Pz);
	}
	/*/
	virtual RtVoid RiDepthOfField(RtFloat fstop, RtFloat focallength, RtFloat focaldistance)
	{
		::RiDepthOfField(fstop, focallength, focaldistance);
	}
	virtual RtVoid RiShutter(RtFloat min, RtFloat max)
	{
		::RiShutter(min, max);
	}
/*/

DLLIMPORT extern RtVoid
	RiPixelVariance(RtFloat variation),
	RiPixelFidelity(RtFloat variation),
	/*/
	RtVoid RiPixelSamples(RtFloat xsamples, RtFloat ysamples)
	{
		::RiPixelSamples(xsamples, ysamples);
	}
	RtVoid RiPixelFilter(RtFilterFunc filterfunc, RtFloat xwidth, RtFloat ywidth)
	{
		::RiPixelFilter(filterfunc, xwidth, ywidth);
	}
	RtVoid RiExposure(RtFloat gain, RtFloat gamma)
	{
		::RiExposure(gain, gamma);
	}
	RtVoid RiImager(RtToken name, ...)
	{
		va_list marker;
		va_start( marker, name );     /* Initialize variable arguments. */

		std::vector<RtToken> tokens; std::vector<RtPointer> parms;
		int n = BuildParamList(tokens, parms, marker);
		::RiImagerV(name, n, &tokens[0], &parms[0]);
		va_end( marker );              /* Reset variable arguments.      */
	}
	RtVoid RiImagerV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[])
	{
		::RiImagerV(name, n, tokens, parms);
	}
	RtVoid RiQuantize(RtToken type, RtInt one, RtInt min, RtInt max, RtFloat ampl)
	{
		::RiQuantize(type, one, min, max, ampl);
	}

	RtVoid RiDisplay(char *name, RtToken type, RtToken mode, ...)
	{
		va_list marker;
		va_start( marker, mode );     /* Initialize variable arguments. */

		std::vector<RtToken> tokens; std::vector<RtPointer> parms;
		int count = BuildParamList(tokens, parms, marker);
		if( count==0)
			::RiDisplay(name, type, mode, NULL);
		else
			::RiDisplayV(name, type, mode, count, &tokens[0], &parms[0]);
		va_end( marker );              /* Reset variable arguments.      */
	}
	RtVoid RiDisplayV(char *name, RtToken type, RtToken mode,
			RtInt n, RtToken tokens[], RtPointer parms[])
	{
		::RiDisplayV(name, type, mode, n, tokens, parms);
	}

	virtual RtVoid RiDisplayChannel(RtToken channel, ...)
	{
		#ifdef PRMANVER13
		va_list marker;
		va_start( marker, channel );     /* Initialize variable arguments. */

		std::vector<RtToken> tokens; std::vector<RtPointer> parms;
		int count = BuildParamList(tokens, parms, marker);
		if( count==0)
			::RiDisplayChannel(channel, NULL);
		else
			::RiDisplayChannelV(channel, count, &tokens[0], &parms[0]);
		va_end( marker );              /* Reset variable arguments.      */
		#endif
	}
	virtual RtVoid RiDisplayChannelV(RtToken channel, RtInt n, RtToken tokens[], RtPointer parms[])
	{
		#ifdef PRMANVER13
		::RiDisplayChannelV(channel, n, tokens, parms);
		#endif
	}

	virtual RtVoid RiHider(RtToken type, ...)
	{
		va_list marker;
		va_start( marker, type );     /* Initialize variable arguments. */

		std::vector<RtToken> tokens; std::vector<RtPointer> parms;
		int count = BuildParamList(tokens, parms, marker);
		::RiHiderV(type, count, &tokens[0], &parms[0]);
		va_end( marker );              /* Reset variable arguments.      */
	}
	virtual RtVoid RiHiderV(RtToken type, RtInt n, RtToken tokens[], RtPointer parms[])
	{
		::RiHiderV(type, n, tokens, parms);
	}

	virtual RtVoid RiColorSamples(RtInt n, RtFloat nRGB[], RtFloat RGBn[])
	{
		::RiColorSamples(n, nRGB, RGBn);
	}
	virtual RtVoid RiRelativeDetail(RtFloat relativedetail)
	{
		::RiRelativeDetail(relativedetail);
	}
	virtual RtVoid RiOption(RtToken name, ...)
	{
		va_list marker;
		va_start( marker, name );     /* Initialize variable arguments. */

		std::vector<RtToken> tokens; std::vector<RtPointer> parms;
		int count = BuildParamList(tokens, parms, marker);
		::RiOptionV(name, count, &tokens[0], &parms[0]);
		va_end( marker );              /* Reset variable arguments.      */
	}
	
	/*/
	RiOptionV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[]);

DLLIMPORT extern RtVoid
/*/
	virtual RtVoid RiAttributeBegin(void)
	{
		::RiAttributeBegin();
	}
	virtual RtVoid RiAttributeEnd(void)
	{
		::RiAttributeEnd();
	}
	virtual RtVoid RiColor(RtColor color)
	{
		::RiColor(color);
	}
	virtual RtVoid RiOpacity(RtColor color)
	{
		::RiOpacity(color);
	}
	virtual RtVoid RiTextureCoordinates(RtFloat s1, RtFloat t1, RtFloat s2, RtFloat t2,
				RtFloat s3, RtFloat t3, RtFloat s4, RtFloat t4)
	{
		::RiTextureCoordinates(s1, t1, s2, t2, s3, t3, s4, t4);
	}
/*/

DLLIMPORT extern RtLightHandle
	RiLightSource(RtToken name, ...),
/*/
	virtual RtLightHandle RiLightSourceV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[])
	{
		return ::RiLightSourceV(name, n, tokens, parms);

	}
/*/
	RiAreaLightSource(RtToken name, ...),
	RiAreaLightSourceV(RtToken name,
				RtInt n, RtToken tokens[], RtPointer parms[]);
DLLIMPORT extern RtVoid
	RiIlluminate(RtLightHandle light, RtBoolean onoff),
	/*/

	RtVoid RiSurface(RtToken name, ...)
	{
		va_list marker;
		va_start( marker, name );     /* Initialize variable arguments. */

		std::vector<RtToken> tokens; std::vector<RtPointer> parms;
		int count = BuildParamList(tokens, parms, marker);
		::RiSurfaceV(name, count, &tokens[0], &parms[0]);
		va_end( marker );              /* Reset variable arguments.      */
	}
	RtVoid RiSurfaceV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[])
	{
		::RiSurfaceV(name, n, tokens, parms);
	}

	/*/
	RiSurfaceV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[]),
	RiAtmosphere(RtToken name, ...),
	RiAtmosphereV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[]),
	RiInterior(RtToken name, ...),
	RiInteriorV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[]),
	RiExterior(RtToken name, ...),
	RiExteriorV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[]),
	/*/
	RtVoid RiShadingRate(RtFloat size)
	{
		::RiShadingRate(size);
	}
	RtVoid RiShadingInterpolation(RtToken type)
	{
		::RiShadingInterpolation(type);
	}
	RtVoid RiMatte(RtBoolean onoff)
	{
		::RiMatte(onoff);
	}
	/*/
DLLIMPORT extern RtVoid
/*/
	RtVoid RiBound(RtBound bound)
	{
		::RiBound(bound);
	}
	RtVoid RiDetail(RtBound bound)
	{
		::RiDetail(bound);
	}
	RtVoid RiDetailRange(RtFloat minvis, RtFloat lowtran, RtFloat uptran,
			RtFloat maxvis)
	{
		::RiDetailRange(minvis, lowtran, uptran, maxvis);
	}
/*/
	RiGeometricApproximation(RtToken type, RtFloat value),
	RiOrientation(RtToken orientation),
/*/
	RtVoid RiReverseOrientation(void)
	{
		::RiReverseOrientation();
	}
	RtVoid RiSides(RtInt sides)
	{
		::RiSides(sides);
	}
	RtVoid RiIdentity(void)
	{
		::RiIdentity();
	}
	RtVoid RiTransform(RtMatrix transform)
	{
		::RiTransform(transform);
	}
	RtVoid RiConcatTransform(RtMatrix transform)
	{
		::RiConcatTransform(transform);
	}
	RtVoid RiConcatTransform(Math::Matrix4f& m)
	{
		RtMatrix transform; copy(transform, m);
		::RiConcatTransform(transform);
	}
	RtVoid RiPerspective(RtFloat fov)
	{
		::RiPerspective(fov);
	}
	RtVoid RiTranslate(RtFloat dx, RtFloat dy, RtFloat dz)
	{
		::RiTranslate(dx, dy, dz);
	}
	RtVoid RiRotate(RtFloat angle, RtFloat dx, RtFloat dy, RtFloat dz)
	{
		::RiRotate(angle, dx, dy, dz);
	}
	RtVoid RiScale(RtFloat sx, RtFloat sy, RtFloat sz)
	{
		::RiScale(sx, sy, sz);
	}
	RtVoid RiSkew(RtFloat angle, RtFloat dx1, RtFloat dy1, RtFloat dz1,
		RtFloat dx2, RtFloat dy2, RtFloat dz2)
	{
		::RiSkew( angle, dx1, dy1, dz1, dx2, dy2, dz2);
	}
/*/
	RiDeformation(RtToken name, ...),
	RiDeformationV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[]),
	/*/
	RtVoid RiDisplacement(RtToken name, ...)
	{
		va_list marker;
		va_start( marker, name );     /* Initialize variable arguments. */

		std::vector<RtToken> tokens; std::vector<RtPointer> parms;
		int count = BuildParamList(tokens, parms, marker);
		::RiDisplacementV(name, count, &tokens[0], &parms[0]);
		va_end( marker );              /* Reset variable arguments.      */
	}
	RtVoid RiDisplacementV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[])
	{
		::RiDisplacementV(name, n, tokens, parms);
	}
	RtVoid RiCoordinateSystem(RtToken space)
	{
		::RiCoordinateSystem(space);
	}
	/*/
	RiCoordinateSystem(RtToken space),
	RiScopedCoordinateSystem(RtToken space),
	RiCoordSysTransform(RtToken space);
DLLIMPORT extern RtPoint
	*RiTransformPoints(RtToken fromspace, RtToken tospace, RtInt n,
			   RtPoint points[]);
DLLIMPORT extern RtVoid
/*/

	RtVoid RiTransformBegin(void)
	{
		::RiTransformBegin();
	}
	RtVoid RiTransformEnd(void)
	{
		::RiTransformEnd();
	}

	RtVoid RiAttribute(RtToken name, ...)
	{
		va_list marker;
		va_start( marker, name );     /* Initialize variable arguments. */

		std::vector<RtToken> tokens; std::vector<RtPointer> parms;
		int count = BuildParamList(tokens, parms, marker);
		::RiAttributeV(name, count, &tokens[0], &parms[0]);
		va_end( marker );              /* Reset variable arguments.      */
	}
/*/
	RiAttributeV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[]);

DLLIMPORT extern RtVoid
	RiPolygon(RtInt nverts, ...),
/*/
	RtVoid RiPolygonV(RtInt nverts, RtInt n, RtToken tokens[], RtPointer parms[])
	{
		::RiPolygonV(nverts, n, tokens, parms);
	}
/*/
	RiGeneralPolygon(RtInt nloops, RtInt nverts[], ...),
	RiGeneralPolygonV(RtInt nloops, RtInt nverts[],
				RtInt n, RtToken tokens[], RtPointer parms[]),
	RiPointsPolygons(RtInt npolys, RtInt nverts[], RtInt verts[], ...),
/*/
	RtVoid RiPointsPolygonsV(RtInt npolys, RtInt nverts[], RtInt verts[],
				RtInt n, RtToken tokens[], RtPointer parms[])
	{
		::RiPointsPolygonsV(npolys, nverts,
			verts, n, tokens, parms);
	}
/*/
	RiPointsGeneralPolygons(RtInt npolys, RtInt nloops[], RtInt nverts[],
				RtInt verts[], ...),
/*/
	RtVoid RiPointsGeneralPolygonsV(RtInt npolys, RtInt nloops[], RtInt nverts[],
			RtInt verts[], RtInt n, RtToken tokens[], RtPointer parms[])
	{
		::RiPointsGeneralPolygonsV(npolys, nloops, nverts,
			verts, n, tokens, parms);
	}
	RtVoid RiBasis(RtBasis ubasis, RtInt ustep, RtBasis vbasis, RtInt vstep)
	{
		::RiBasis(ubasis, ustep, vbasis, vstep);
	}
	RtVoid RiPatch(RtToken type, ...)
	{
		va_list marker;
		va_start( marker, type );     /* Initialize variable arguments. */

		std::vector<RtToken> tokens; std::vector<RtPointer> parms;
		int n = BuildParamList(tokens, parms, marker);
		::RiPatchV(type, n, &tokens[0], &parms[0]);
		va_end( marker );              /* Reset variable arguments.      */
	}
	RtVoid RiPatchV(RtToken type, RtInt n, RtToken tokens[], RtPointer parms[])
	{
		::RiPatchV(type, n, &tokens[0], &parms[0]);
	}
	RtVoid RiPatchMesh(RtToken type, RtInt nu, RtToken uwrap,
			RtInt nv, RtToken vwrap, ...)
	{
		va_list marker;
		va_start( marker, vwrap );     /* Initialize variable arguments. */

		std::vector<RtToken> tokens; std::vector<RtPointer> parms;
		int n = BuildParamList(tokens, parms, marker);
		::RiPatchMeshV(type, nu, uwrap, nv, vwrap, n, &tokens[0], &parms[0]);
		va_end( marker );              /* Reset variable arguments.      */
	}
	RtVoid RiPatchMeshV(RtToken type, RtInt nu, RtToken uwrap,
			RtInt nv, RtToken vwrap,
			RtInt n, RtToken tokens[], RtPointer parms[])
	{
		::RiPatchMeshV(type, nu, uwrap, nv, vwrap, n, &tokens[0], &parms[0]);
	}
	RtVoid RiNuPatch(RtInt nu, RtInt uorder, RtFloat uknot[], RtFloat umin, RtFloat umax,
			RtInt nv, RtInt vorder, RtFloat vknot[], RtFloat vmin,
			RtFloat vmax, ...)
	{
		va_list marker;
		va_start( marker, vmax );     /* Initialize variable arguments. */

		std::vector<RtToken> tokens; std::vector<RtPointer> parms;
		int n = BuildParamList(tokens, parms, marker);
		::RiNuPatchV(nu, uorder, uknot, umin, umax, nv, vorder, vknot, vmin,
			vmax, n, &tokens[0], &parms[0]);
		va_end( marker );              /* Reset variable arguments.      */
	}
	RtVoid RiNuPatchV(RtInt nu, RtInt uorder, RtFloat uknot[], RtFloat umin,
			RtFloat umax, RtInt nv, RtInt vorder, RtFloat vknot[],
			RtFloat vmin, RtFloat vmax,
			RtInt n, RtToken tokens[], RtPointer parms[])
	{
		::RiNuPatchV(nu, uorder, uknot, umin, umax, nv, vorder, vknot, vmin,
			vmax, n, &tokens[0], &parms[0]);
	}
/*/

DLLIMPORT extern RtVoid
	RiSphere(RtFloat radius, RtFloat zmin, RtFloat zmax, RtFloat tmax, ...),
/*/
	RtVoid RiSphereV(RtFloat radius, RtFloat zmin, RtFloat zmax, RtFloat tmax,
			RtInt n, RtToken tokens[], RtPointer parms[])
	{
		::RiSphereV(radius, zmin, zmax, tmax, n, tokens, parms);
	}
/*/
	RiCone(RtFloat height, RtFloat radius, RtFloat tmax, ...),
	RiConeV(RtFloat height, RtFloat radius, RtFloat tmax,
			RtInt n, RtToken tokens[], RtPointer parms[]),
	RiCylinder(RtFloat radius, RtFloat zmin, RtFloat zmax, RtFloat tmax, ...),
	RiCylinderV(RtFloat radius, RtFloat zmin, RtFloat zmax, RtFloat tmax,
			RtInt n, RtToken tokens[], RtPointer parms[]),
	RiHyperboloid(RtPoint point1, RtPoint point2, RtFloat tmax, ...),
	RiHyperboloidV(RtPoint point1, RtPoint point2, RtFloat tmax,
			RtInt n, RtToken tokens[], RtPointer parms[]),
	RiParaboloid(RtFloat rmax, RtFloat zmin, RtFloat zmax, RtFloat tmax, ...),
	RiParaboloidV(RtFloat rmax, RtFloat zmin, RtFloat zmax, RtFloat tmax,
			RtInt n, RtToken tokens[], RtPointer parms[]),
	RiDisk(RtFloat height, RtFloat radius, RtFloat tmax, ...),
	RiDiskV(RtFloat height, RtFloat radius, RtFloat tmax,
			RtInt n, RtToken tokens[], RtPointer parms[]),
	RiTorus(RtFloat majrad, RtFloat minrad, RtFloat phimin, RtFloat phimax,
			RtFloat tmax, ...),
	RiTorusV(RtFloat majrad, RtFloat minrad, RtFloat phimin, RtFloat phimax,
			RtFloat tmax, RtInt n, RtToken tokens[], RtPointer parms[]);

DLLIMPORT extern RtVoid RiBlobby(RtInt nleaf, RtInt ncode, RtInt code[], 
		       RtInt nflt, RtFloat flt[],
		       RtInt nstr, RtToken str[], ...);
/*/
	virtual RtVoid RiBlobbyV(RtInt nleaf, RtInt ncode, RtInt  code[], 
				RtInt nflt, RtFloat flt[],
				RtInt nstr, RtToken str[],
				RtInt n , RtToken tokens[], RtPointer parms[])
	{
		::RiBlobbyV(nleaf, ncode, code, 
				nflt, flt,
				nstr, str,
				n , tokens, parms);
	}
/*/


DLLIMPORT extern RtVoid
	RiCurves(RtToken type, RtInt ncurves,
		 RtInt nvertices[], RtToken wrap, ...),
/*/
	RtVoid RiCurvesV(RtToken type, RtInt ncurves, RtInt nvertices[], RtToken wrap,
		  RtInt n, RtToken tokens[], RtPointer parms[])
	{
		::RiCurvesV(type, ncurves, nvertices, wrap, n, tokens, parms);
	}
/*/
	RiPoints(RtInt nverts,...),
/*/
	RtVoid RiPointsV(RtInt nverts, RtInt n, RtToken tokens[], RtPointer parms[])
	{
		::RiPointsV( nverts, n, tokens, parms);
	}
/*/
	RiSubdivisionMesh(RtToken mask, RtInt nf, RtInt nverts[],
			  RtInt verts[],
			  RtInt ntags, RtToken tags[], RtInt numargs[],
			  RtInt intargs[], RtFloat floatargs[], ...),
/*/
	RtVoid RiSubdivisionMeshV(
		RtToken mask, RtInt nf, RtInt nverts[],
		RtInt verts[], RtInt ntags, RtToken tags[],
		RtInt nargs[], RtInt intargs[],
		RtFloat floatargs[], RtInt n,
		RtToken tokens[], RtPointer *parms)
	{
		::RiSubdivisionMeshV(mask, nf, nverts, 
			verts, ntags, tags, 
			nargs, intargs,
			floatargs, n, tokens, parms);
	}
	RtVoid RiHierarchicalSubdivisionMeshV(
				RtToken mask, RtInt nf, RtInt nverts[],
			   RtInt verts[], RtInt ntags, RtToken tags[],
			   RtInt nargs[], RtInt intargs[],
			   RtFloat floatargs[], RtToken stringargs[], RtInt n,
			   RtToken tokens[], RtPointer *parms)
	{
		::RiHierarchicalSubdivisionMeshV(
			mask, nf, nverts, 
			verts, 
			ntags, tags, nargs, 
			intargs,
			floatargs, stringargs, n, tokens, parms);
	}

	RtVoid	RiTrimCurve(RtInt nloops, RtInt ncurves[], RtInt order[], RtFloat knot[],
			RtFloat min[], RtFloat max[], RtInt n[],
			RtFloat u[], RtFloat v[], RtFloat w[])
	{
	    ::RiTrimCurve(nloops, ncurves, order, knot,
			min, max, n, u, v, w);
	}

	RtVoid RiProcedural(RtPointer data, RtBound bound,
			RtVoid (*subdivfunc)(RtPointer, RtFloat),
			RtVoid (*freefunc)(RtPointer))
	{
		if(bExecuteProcedural)
		{
			(*subdivfunc)(data, 1);
			(*freefunc)(data);
			return;
		}
		return ::RiProcedural(data, bound, subdivfunc, freefunc);
	}
	RtVoid RiGeometry(RtToken type, ...)
	{
		va_list marker;
		va_start( marker, type );     /* Initialize variable arguments. */

		std::vector<RtToken> tokens; std::vector<RtPointer> parms;
		int n = BuildParamList(tokens, parms, marker);
		::RiGeometryV(type, n, &tokens[0], &parms[0]);
		va_end( marker );              /* Reset variable arguments.      */
	}
	RtVoid RiGeometryV(RtToken type, RtInt n, RtToken tokens[], RtPointer parms[])
	{
		::RiGeometryV(type, n, &tokens[0], &parms[0]);
	}
	/*/

DLLIMPORT extern RtVoid
	RiSolidBegin(RtToken operation),
	RiSolidEnd(void);
DLLIMPORT extern RtObjectHandle
	RiObjectBegin(void);
DLLIMPORT extern RtVoid
	RiObjectEnd(void),
	RiObjectInstance(RtObjectHandle handle),
	RiMotionBegin(RtInt n, ...),
/*/
	RtVoid RiMotionBeginV(RtInt n, RtFloat times[])
	{
		::RiMotionBeginV(n, times);
	}
	RtVoid RiMotionEnd(void)
	{
		::RiMotionEnd();
	}

/*/
DLLIMPORT extern RtVoid
	RiResource(RtToken handle, RtToken type, ...),
	RiResourceV(RtToken handle, RtToken type,
		RtInt n, RtToken tokens[], RtPointer parms[]);

DLLIMPORT extern RtVoid
	RiMakeTexture(char *pic, char *tex, RtToken swrap, RtToken twrap,
		RtFilterFunc filterfunc, RtFloat swidth, RtFloat twidth, ...),
	RiMakeTextureV(char *pic, char *tex, RtToken swrap, RtToken twrap,
		RtFilterFunc filterfunc, RtFloat swidth, RtFloat twidth,
		RtInt n, RtToken tokens[], RtPointer parms[]),
	RiMakeBump(char *pic, char *tex, RtToken swrap, RtToken twrap,
		RtFilterFunc filterfunc, RtFloat swidth, RtFloat twidth, ...),
	RiMakeBumpV(char *pic, char *tex, RtToken swrap, RtToken twrap,
		RtFilterFunc filterfunc, RtFloat swidth, RtFloat twidth,
		RtInt n, RtToken tokens[], RtPointer parms[]),
	RiMakeLatLongEnvironment(char *pic, char *tex, RtFilterFunc filterfunc,
		RtFloat swidth, RtFloat twidth, ...),
	RiMakeLatLongEnvironmentV(char *pic, char *tex, RtFilterFunc filterfunc,
		RtFloat swidth, RtFloat twidth,
		RtInt n, RtToken tokens[], RtPointer parms[]),
	RiMakeCubeFaceEnvironment(char *px, char *nx, char *py, char *ny,
		char *pz, char *nz, char *tex, RtFloat fov,
		RtFilterFunc filterfunc, RtFloat swidth, RtFloat ywidth, ...),
	RiMakeCubeFaceEnvironmentV(char *px, char *nx, char *py, char *ny,
		char *pz, char *nz, char *tex, RtFloat fov,
		RtFilterFunc filterfunc, RtFloat swidth, RtFloat ywidth,
		RtInt n, RtToken tokens[], RtPointer parms[]),
	RiMakeShadow(char *pic, char *tex, ...),
	RiMakeShadowV(char *pic, char *tex,
		RtInt n, RtToken tokens[], RtPointer parms[]);
/*/

	virtual void RiErrorHandler(RtErrorHandler handler)
	{
		::RiErrorHandler( handler);
	}

/*/
DLLIMPORT extern RtVoid
	RiSynchronize(RtToken);

DLLIMPORT extern RtVoid
/*/
	void RiArchiveRecord(RtToken type, char *format)
	{
		::RiArchiveRecord(type, format);
	}
/*/
	RiReadArchive(RtToken name, RtVoid (*callback)(RtToken,char*,...), ...),
	RiReadArchiveV(RtToken name, RtVoid (*callback)(RtToken,char*,...),
		RtInt n, RtToken tokens[], RtPointer parms[]);

DLLIMPORT extern RtArchiveHandle
	RiArchiveBegin(RtToken name, ...),
    	RiArchiveBeginV(RtToken name, RtInt n, RtToken tokens[], RtPointer parms[]);

DLLIMPORT extern RtVoid
	RiArchiveEnd(void);

DLLIMPORT extern RtVoid
	RiIfBegin(char *expr, ...),
	RiIfBeginV(char *expr, RtInt n, RtToken tokens[], RtPointer parms[]),
	RiElseIf(char *expr, ...),
	RiElseIfV(char *expr, RtInt n, RtToken tokens[], RtPointer parms[]),
	RiElse(void),
	RiIfEnd(void);


// OBSOLETE call: see RiErrorHandler //
DLLIMPORT extern RtVoid
	RiErrorMode(RtToken mode, RtErrorHandler handler);
/*/


	int BuildParamList( std::vector<RtToken>& tokens, std::vector<RtPointer>& parms, va_list argptr)
	{
		for(;;)
		{
			RtToken token = va_arg( argptr, RtToken);
			if(!token) break;
			RtPointer param = va_arg( argptr, RtPointer);
			if(!token) break;

			tokens.push_back(token);
			parms.push_back(param);
		}
		return (int)tokens.size();
	}

	// ��� ������
	virtual void RxOption(const char *name, void *result, int resultlen,
				RxInfoType_t *resulttype, int *resultcount)	
	{
#ifdef USERXLIBRARY
		::RxOption(name, result, resultlen, resulttype, resultcount);
#endif
	};

};
#endif

