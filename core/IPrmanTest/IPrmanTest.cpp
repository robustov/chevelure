// IPrmanTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "mathNpixar/IPrman.h"

int _tmain(int argc, _TCHAR* argv[])
{


	char* filename = "xxx.rib";
#ifdef _DEBUG
  	getIPrmanDll dllproc("IPrmanD.dll", "getIPrman");
#else
  	getIPrmanDll dllproc("IPrman.dll", "getIPrman");
#endif
	if( !dllproc.isValid())
		return 0;

	IPrman* prman = (*dllproc)();

	prman->RiBegin( (char*)filename );
	{
		RtString format = "ascii";
		prman->RiOption(( RtToken ) "rib", ( RtToken ) "format", &format, RI_NULL);
		RtString compression = "none";
		prman->RiOption(( RtToken ) "rib", ( RtToken ) "compression", &compression, RI_NULL);
	}
	std::string displayname = "ocRenderPass1,";
	std::string displaytype = "it,";
	std::string displaymode = "rgba,";

	RtInt origin[2] = { 10, 10 };
	prman->RiDisplay((RtToken)displayname.c_str(), (RtToken)displaytype.c_str(), (RtToken)displaymode.c_str(), "origin,", (int*)origin, RI_NULL);
	prman->RiDisplay((RtToken)displayname.c_str(), (RtToken)displaytype.c_str(), (RtToken)displaymode.c_str(), RI_NULL);

	RtString text = "sdsds";
	prman->RiOption( (RtToken )"searchpath", 
		(RtToken )"shader", ( RtPointer )&text, RI_NULL);

	prman->RiEnd();

	return 0;
}

