// IPrmanDSO.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"

#define USERXLIBRARY
#include "mathNpixar/IPrman.h"
#include "mathNpixar/IPrman_impl.h"
//#include "mathNpixar/ISlo.h"
//#include "mathNpixar/ISlo_impl.h"
//#include "mathNpixar/IPointCloud_impl.h"



#ifdef _MANAGED
#pragma managed(push, off)
#endif

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
    return TRUE;
}

#ifdef _MANAGED
#pragma managed(pop)
#endif


#ifdef WIN32
    #define EXPORT_API __declspec(dllexport)
#else
    #define EXPORT_API
#endif


Prman_Impl prman;
extern "C" EXPORT_API IPrman* getIPrman(void)
{
	return &prman;
}
