// test.cpp : Defines the entry point for the console application.
//

#include "Math/Math.h"

/*/
int main(int argc, char* argv[])
{
	float s = 1, f=3;
	float x = std::min(s, f);
	return 0;
}

/*/
#define __min std::min
#define __max std::max

#include "Math/matrixMxN.h"

int main(int argc, char* argv[])
{
	Math::matrixMxN<float> m;
	m.init(4, 4);
	for(int x=0; x<4; x++)
		for(int y=0; y<4; y++)
			m[x][y] = (float)(x+y);

	float r = m.cubic_interpolation(0, 0.5, false, false);
//	float r = m.cubic_interpolation(1, 0.5, false, false);
	for(float dt=-0.3; dt<=1.301; dt+=0.1)
	{
		float x = dt;
		float y = 0.5f;
		float r = m.cubic_interpolation(x, y, false, false);
		printf("[%f][%f]: %f\n", x, y, r);
	}
	return 0;
}
//*/

