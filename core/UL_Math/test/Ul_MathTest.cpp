// Ul_MathTest.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
	/*/
#include "Math/meshutil.h"
#include "Math/Edge.h"
#include "MathNMaya/IFFMatrix.h"

#include "Math/Subdiv.h"
#include "Math/Spline.h"
#include "Math/Box.h"
//#include "Math/MatrixBoolean.h"
#include "Math/RayTriIntersection.h"
#include "Math/smoothstep.h"
#include "Util/currentHostName.h"
#include "Math/perlinnoise.h"
#include <string>
#include "Util/misc_create_directory.h"
#include "Math/Bresenham.h"
//#include "Math/Grid.h"
#include "Util/DllProcedure.h"
#include "Util/FileStream.h"

#include <fstream>
	/*/

#include "mathNpixar/ISlo.h"

/*/
#include "core_all.h"
#include "core_all.h"

float TriPropRandom(float P[3], float V[3], float p);

float rnd(float _min=-1, float _max=1)
{
	return rand()/((float)RAND_MAX)*(_max-_min)+_min;
}
/*/

int main(int argc, char* argv[])
{
	{
		getISloDll SloDll("IPrman12.5D.dll", "getISlo");
		if( !SloDll.isValid())
			return -2;

		char* shaderName = "M:/public/rib/che.mb/ocRenderPass1/lambert1.slo";
		
//		bool bexist = Util::is_file_exist( shaderName);
		ISlo* slo = (*SloDll)();
		int res = slo->Slo_SetShader(shaderName);
		if(res!=0)
			return -4;

		int n = slo->Slo_GetNArgs();
	//		std::map<std::string, cls::Param> params;
		for(int i=0; i<n; i++)
		{
			SLO_VISSYMDEF* def = slo->Slo_GetArgById(i+1);
			if( def->svd_storage == SLO_STOR_OUTPUTPARAMETER)
			{
				// ���������� �����
				continue;
			}
			printf( "%d\n", def->svd_type);
		}

		return -3;
	}

	/*/
	{
		Math::Vec3f pts[4];
		pts[0] = Math::Vec3f(0, 0, 0);
		pts[1] = Math::Vec3f(1, 0, 0);
		pts[2] = Math::Vec3f(0, 1, 0);
		pts[3] = Math::Vec3f(1, 1, 0);
		pts[4] = Math::Vec3f(0, 0, 1);
		pts[5] = Math::Vec3f(1, 0, 1);
		pts[6] = Math::Vec3f(0, 1, 1);
		pts[7] = Math::Vec3f(1, 1, 1);

		for(int i=0; i<100000; i++)
		{
			Math::Vec3f p(rnd(-0.5f, 1.5f), rnd(-0.5f, 1.5f), rnd(-0.5f, 1.5f));
			bool bII = ( p.x<0 && p.x>1 && 
				p.y<0 && p.y>1 && 
				p.z<0 && p.z>1);

//			Math::Vec4f bary = Math::CalcBaryCoords3d(pts, p);
			Math::Vec3f pinc;
			bool inside = isPointInCube(p, pts, &pinc);

			if(inside)
			{
				float d = (p-pinc).length();
				if( d!=0)
					printf("inside %f\n", d);
			}
			else
			{
				if( bII)
					printf("error: %f %f %f -> offside\n", p.x, p.y, p.z);
			}
		}
		printf("end\n");
		return -1;
	}

	{
		Math::Vec3f verts[]={
			Math::Vec3f(-11),
			Math::Vec3f(0),
			Math::Vec3f(1),
			Math::Vec3f(2)
			};
		float k[] = {
			-1, 
			0, 
			1, 
			2};

		Math::Vec3f v0 = Math::Spline3(0, verts, k, 4, Math::Spline3f::BSpline);
		Math::Vec3f v1 = Math::Spline3(1, verts, k, 4, Math::Spline3f::BSpline);
		

		return -1;
	}
	{
		Util::DllProcedure test;
		Util::FileStream file;
		Util::Stream& stream = file;
		stream >> test;
	}

	{
		Math::Vec3f pts[4];
		pts[0] = Math::Vec3f(rnd(), rnd(), rnd());
		pts[1] = Math::Vec3f(rnd(), rnd(), rnd());
		pts[2] = Math::Vec3f(rnd(), rnd(), rnd());
		pts[3] = Math::Vec3f(rnd(), rnd(), rnd());

		pts[1] += Math::Vec3f(10, 7, 9);

		for(int i=0; i<100; i++)
		{
			Math::Vec3f p(rnd(), rnd(), rnd());
			Math::Vec4f bary = Math::CalcBaryCoords3d(pts, p);

			if(bary[0]>=0 && bary[1]>=0 && bary[2]>=0 && bary[3]>=0)
				printf("inside ");
			Math::Vec3f p2 = bary[0]*pts[0] + bary[1]*pts[1] + bary[2]*pts[2] + bary[3]*pts[3];
			float d = (p2 - p).length();
			printf("%f\n", d);
		}
		return -1;
	}
	{
		{
			std::vector<Math::Vec2i> res;
			Math::Bresenham2(Math::Vec2i(-14, 25), Math::Vec2i(7, 15), res);
			for( int i=0; i<res.size(); i++)
			{
				printf("%d %d\n", res[i].x, res[i].y);
			}
		}

		{
			std::vector<Math::Vec3i> res;
			Math::Bresenham3( Math::Vec3i(-14, 25, 89), Math::Vec3i(7, 15, 56), res);
			for( int i=0; i<(int)res.size(); i++)
			{
				printf("%d %d %d\n", res[i].x, res[i].y, res[i].z);
			}
		}

		return 1;
	}
	/*/
	/*/
	{
		Math::GridCellId v(0, 0, 0, 0);
		Math::GridEdge gride( v, (Math::enNeigbour3d)3);
		for(int i=0; i<6; i++)
		{
			Math::GridCellId v2 = Math::Grid::getNeigbour(v, i);
			Math::GridEdge gride2(v2, v);
			if(gride2 == gride)
				printf("eq%d\n", i);
			if(gride2 != gride)
				printf("neq%d\n", i);
		}
		return 1;
	}
	{
		Math::Grid grid;
		// ����
		Math::GridCell& cell = grid.addCell( Math::GridCellId(0, 1, 0, 1));
		cell.dump(stdout);

		grid.dump(0, stdout);
		grid.dump(1, stdout);
		grid.dump(2, stdout);
	}

	{
		Math::test_Subdiv(2, 3, 1);
		return 0;
	}

	{
		std::vector< Math::Vec3f> data;
		std::vector< float> ks1;
		ks1.push_back(0); data.push_back(Math::Vec3f(0));
		ks1.push_back(1); data.push_back(Math::Vec3f(1));
		ks1.push_back(2); data.push_back(Math::Vec3f(2));
		ks1.push_back(3); data.push_back(Math::Vec3f(3));
//		ks1.push_back(4); data.push_back(Math::Vec3f(4));
//		ks1.push_back(5); data.push_back(Math::Vec3f(5));

		Math::Spline3( 1, &data[0], &ks1[0], (int)ks1.size(), Math::Spline3f::CatmullRom);
		for(float t=-1; t<4; t+=0.1f)
		{
			Math::Vec3f v = Math::Spline3( t, &data[0], &ks1[0], (int)ks1.size(), Math::Spline3f::CatmullRom);
			printf("%f: %f\n", t, v.x);
		}
	}
	return 0;

	{
		Math::matrixMxN<unsigned char> matrix;
		bool res = Math::LoadMatrixFromIFF(
			"\\\\Server\\movies\\Wanted\\Wanted_56\\3D\\PAN-1200\\textures\\PAN-1200_GRND_height.tif", 
			matrix);

	}
	/*/

//	fixDisplayInRib("C:/M/public/rib/binaryC/binaryC.0001.rib");

	return 0;

	/*/
	{
		float frequency = 0.1f;
		int id = 2;

		Math::PerlinNoise pn;
		pn.Init(rand());
		float P[3] = {100, 30, 30};
		float V[3] = {0, 2, 3};

		std::vector<int> res(100, 0);

		int i;
		for(i=0; i<10000; i++)
		{
			float p;
//			p = pn.noise( i*M_PI, i*0.02323, i*0.1);
			p = pn.noise( id*M_PI, 0.02323, i*frequency);
			p = p + 0.5f;
			if(p>1) p=1;
			if(p<0) p=0;

			float z;
//			for(z=0; z<p; z+=0.02)
//				printf(" ");
//			printf("#\n");

//			p = rand()/(float)RAND_MAX;
			float v = TriPropRandom(P, V, p);

			for(z=V[0]; z<v; z+=(V[2]-V[0])/50 )
				printf(" ");
			printf("#\n");

//			printf("%f\n", v);

			float r = 100*(v-V[0])/(V[2]-V[0]);
//r = 100*p;
			int d = r;
			if(d<0) d = 0;
			if(d>=res.size()) d = res.size()-1;
			res[d]++;
		}
		for(i=0; i<res.size(); i++)
		{
			float v = V[0] + (V[2]-V[0])*i/(float)100;
			printf("%f:", v);
			for( int a=0; a<res[i]; a+=6)
				printf(":");
			printf("\n", v);
		}
		return 0;
	}

	float s = Math::smoothstep_inv<float>(0, 1, 0.1);

	Math::Matrix3f b;
	Math::Vec3f v;
	v = v*b;

	// Spline
	{
		bool bPeriodic = true;
		Math::Spline1f spline;
		std::vector< float> ks1;
		ks1.push_back(0);
		ks1.push_back(2);
		ks1.push_back(1);
		ks1.push_back(5);
		ks1.push_back(4);
		ks1.push_back(4);
		spline.setBasis(spline.CatmullRom);
		spline.setKnots(&ks1[0], ks1.size(), bPeriodic);

		std::ofstream points_file("plot/points1.dat");
		for( size_t i = 0 ; i < ks1.size() ; ++i  )
		{
			float pt = ks1[i];
			float time = (i-1.f)/(float)(ks1.size()-3.f);
			if( bPeriodic)
				time = i/(float)(ks1.size());
			points_file<<time<<' '<<pt<<'\n';
		}

		std::ofstream catmulrom_file("plot/catmullrom1.dat");
		std::ofstream bspline_file("plot/bspline1.dat");
		std::ofstream bezier_file("plot/bezier1.dat");
		for( double time = 0.0 ; time <= 1.0 ; time +=0.01 )
		{
			float pt;
			spline.setBasis(spline.CatmullRom);
			pt = spline.getValue( time );
			catmulrom_file<<time<<' '<<pt<<'\n';

			spline.setBasis(spline.BSpline);
			pt = spline.getValue( time );
			bspline_file<<time<<' '<<pt<<'\n';

			spline.setBasis(spline.Bezier);
			pt = spline.getValue( time );
			bezier_file<<time<<' '<<pt<<'\n';
		}

		std::ofstream catmulrom_plt("plot/catmullrom1.plt");
		catmulrom_plt<<"plot 'points1.dat','catmullrom1.dat'\npause -1";
		std::ofstream bspline_plt("plot/bspline1.plt");
		bspline_plt<<"plot 'points1.dat','bspline1.dat'\npause -1";
		std::ofstream bezier_plt("plot/bezier1.plt");
		bezier_plt<<"plot 'points1.dat','bezier1.dat'\npause -1";


		// subdiv
		std::vector< float> ks1subd;
		Math::SubdivCurve(5, &ks1[0], ks1.size(), ks1subd);
		std::ofstream subdiv_file("plot/subdiv1.dat");
		for( size_t i = 0 ; i < ks1subd.size() ; ++i  )
		{
			float pt = ks1subd[i];
			float time = i/(float)(ks1subd.size());
			subdiv_file<<time<<' '<<pt<<'\n';
		}
		std::ofstream subdiv_plt("plot/subdiv1.plt");
		subdiv_plt<<"plot 'points1.dat','subdiv1.dat'\npause -1";
	}


	{
		Math::Spline2f spline;

		std::vector< Math::Vec2f> ks2;
		ks2.push_back( Math::Vec2d( 0,0));
		ks2.push_back( Math::Vec2d( 1,0));
		ks2.push_back( Math::Vec2d( 1,1));
		ks2.push_back( Math::Vec2d( 2,1));
		ks2.push_back( Math::Vec2d( 2,2));
		ks2.push_back( Math::Vec2d( 1,2));
		ks2.push_back( Math::Vec2d( 0,2));
		ks2.push_back( Math::Vec2d( 0,3));
		ks2.push_back( Math::Vec2d( 1,3));

		spline.setBasis(spline.CatmullRom);
		spline.setKnots(&ks2[0], ks2.size(), false);


		std::ofstream points_file("plot/points2.dat");

		for( size_t i = 0 ; i < ks2.size() ; ++i  )
			points_file<<ks2[i][0]<<' '<<ks2[i][1]<<'\n';

		std::ofstream catmulrom_file("plot/catmullrom2.dat");
		std::ofstream bspline_file("plot/bspline2.dat");
		std::ofstream bezier_file("plot/bezier2.dat");
		for( double time = 0.0 ; time < 1.0 ; time +=0.001 )
		{
			Math::Vec2d pt;
			spline.setBasis(spline.CatmullRom);
			pt = spline.getValue( time );
			catmulrom_file<<pt[0]<<' '<<pt[1]<<'\n';

			spline.setBasis(spline.BSpline);
			pt = spline.getValue( time );
			bspline_file<<pt[0]<<' '<<pt[1]<<'\n';

			spline.setBasis(spline.Bezier);
			pt = spline.getValue( time );
			bezier_file<<pt[0]<<' '<<pt[1]<<'\n';
		}
	}
	/*/

	/*/
	{
		Math::Spline3f spline3;

		Math::Matrix4f x = Math::Matrix4f::id;

		Math::Matrix4f m;
		m[0] = Math::Vec4f( 1, 0, 0, 0 );
		m[1] = Math::Vec4f( 0, 1, 0, 0);
		m[2] = Math::Vec4f( 0, 0, 1, 0);
		m[3] = Math::Vec4f( 1, -0.2f, -6, 1);

		Math::Matrix4f mi = m.inverted();


		Math::Polygon32 p[2];
		p[0].verts.push_back(0);
		p[0].verts.push_back(1);
		p[0].verts.push_back(2);
		p[0].verts.push_back(3);
	//	p[1].verts.push_back(0);
	//	p[1].verts.push_back(3);
	//	p[1].verts.push_back(4);
	//	p[1].verts.push_back(5);
		int vertexcount = 4;
		int poligoncount = 1;
		int edgecount = vertexcount*2 + poligoncount*3;
		
		std::vector< Math::Quad32> quads;
		std::vector< Math::subdivWeight> neoverts;
		Subdiv(2, p, poligoncount, vertexcount, edgecount, quads, neoverts);

		// �����
		for( int i=0; i<(int)quads.size(); i++)
		{
			Math::Quad32& face = quads[i];
			printf("Quad %d: {%d %d %d %d}\n", i, 
				face[0], face[1], face[2], face[3]);
		}
		// �������
		for(i=0; i<(int)neoverts.size(); i++)
		{
			printf("v %d: ", i);
			Math::subdivWeight& v = neoverts[i];
			for( int it=0; it != v.data.size(); it++)
			{
				printf("%d(%f) ", v.data[it].first, v.data[it].second);
			}
			printf("\n");
		}
	}
	/*/

	/*/
	std::string host = currentHostName();
	printf( "%s\n", host.c_str());

	Math::Vec3f vert0(0, 0, 0);
	Math::Vec3f vert1(1, 1, 0);
	Math::Vec3f vert2(0, 0, 1);
	Math::Vec3f dir(0, 1, 0);
	for(float x=-2; x<2; x+=0.1f)
	{
		for(float y=-2; y<2; y+=0.1f)
		{
			Math::Vec3f origin(x, 0, y);

			Math::Vec3f bary;
			float mDistance;
			bool res = Math::RayTriIntersection<false>(
				vert0, 
				vert1, 
				vert2, 
				origin,
				dir, 
				bary, mDistance);

			Math::Vec3f inter;
			Math::RayTriPlaneIntersection(
				vert0, 
				vert1, 
				vert2, 
				origin,
				dir, 
				inter);

			if(res)
			{
				Math::Vec3f t = vert0*bary.x + vert1*bary.y + vert2*bary.z;
				float eps = (t - inter).length();

				int s = (int)(mDistance*10);
				if( s>=10) 
					printf(".");
				else if(s<0)
					printf("-");
				else
					printf("%d", s);
			}
			else
				printf(" ");

		}
		printf("\n");
	}
	/*/



	/*/

	// Math::rotate
	Math::Vec3f v1(0.1, 2, 3);
	Math::Vec2f v2(1, 0.2);
	Math::Vec4f v3(1, 2, 0.3, 4);

	Math::Vec3i v4(1, 2, 3);
	Math::Vec2i v5(1, 2);

	Math::Matrix4f m = Math::Matrix4f::id;

	Math::Box2f bb(3);
	Math::Box3f bb2(1);

	for(float a=0; a<10; a+=0.043)
	{
		Math::Vec2f vpolar = Math::rotate(Math::Vec2f(1, 0), a);
		float a2 = Math::angle(vpolar);
		printf("%f -> %f\n", a, a2);
	}
	return 0;
	/*/

	/*/
	// MatrixBoolean
	{
		Math::MatrixBoolean m1, m2, m3;
//		m3.open("D:\\temp\\leopard\\mask.bmp");
		m1.set(80, 80);
		m2.set(40, 40);
		m1.FillRect( Math::Rect2i(Math::Vec2i(10, 10), Math::Vec2i(20, 20)));
		m2.FillRect( Math::Rect2i(Math::Vec2i(5, 5), Math::Vec2i(15, 15)));

		double area = 0;
		if( m1.intersection(Math::Vec2f(0, 0), m2, Math::Vec2f(0, 0), area))
		{
			m1.join(Math::Vec2f(0, 0), m2, Math::Vec2f(0, 0));
		}

		m1.save("D:\\temp\\leopard\\res.bmp");

		math::tgaMatrix resmask(m1.xSize(), m1.ySize());
		int x, y;
		for(y=0; y<m1.ySize(); y++)
		{
			for(x=0; x<m1.xSize(); x++)
			{
				bool a = m1(y, x);
				unsigned char f = 0;
				if( a) f = 255;
				resmask[y][x] = math::rgba(f, f, f, 1);
			}
			printf("\n");
		}
		char buf[256];
//		sprintf(buf, "D:\\temp\\leopard\\res.tga");
//		saveTga(buf, resmask);

//		double area = 0;
//		m1.intersection(Math::Vec2f(0, 0), m2, Math::Vec2f(0, 0), area);
	}
	/*/

	/*/
	/*/

	return 0;
}


float TriPropRandom(float P[3], float V[3], float p)
{
	// 
	float p0, p1;
	{
		float sP = P[0]+P[1]+P[2];
		p0=P[0]/sP;
		p1=P[1]/sP;
		p1 = p0 + p1;
	}
//	printf("p=%f   ", p);

	// �������� ��-���: 0->0, P0->0.33, P1->0.66, 1->1
	float f = 0;
	if(p<p0)
	{
		p = p/p0;
		f = p*0.33;
	}
	else if(p<p1)
	{
		p = (p-p0)/(p1-p0);
		f = p*0.33 + 0.33;
	}
	else
	{
		p = (p-p1)/(1-p1);
		f = p*0.33 + 0.66;
	}
//	printf("f=%f   ", f);
	// p
	float v;
	if(f<0.5f)
	{
		// ����� v[0] v[1]
		v = (V[1]-V[0])*f*2;
		v += V[0];
	}
	else
	{
		// ����� v[1] v[2]
		v = (V[2]-V[1])*(f-0.5f)*2;
		v += V[1];
	}
	return v;
}
