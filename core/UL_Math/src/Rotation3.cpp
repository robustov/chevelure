//#include "stdafx.h"
#define ED_MATH_INTERNALS
#define ED_MATH_IMPLEMENT_ROTATION3
#include "Math/Rotation3.h"


namespace Math {

#define _INSTANTIATE( T ) \
template class Rotation3<T>; \
template ED_MATH_EXTERN Rotation3<T> ED_MATH_API slerp(const Rotation3<T>&, const Rotation3<T>&, const T); \

_INSTANTIATE( float )
_INSTANTIATE( double )
	
}
