// only windows
#ifdef WIN32
#	define NT_APP
#endif

#include "MathNMaya/IFFMatrix.h"

#define REQUIRE_IOSTREAM

#include <maya/flib.h>
#include <maya/ilib.h>

#ifdef _MSC_VER
bool ED_MATH_API Math::LoadMatrixFromIFF(
	const char* filename, 
	Math::matrixMxN<unsigned char>& matrix)
{
    void* mILDSO = NULL;
    ILimage* (*mILopen) (const char *, const char *) = NULL;
    int (*mILgetsize) (ILimage *, int *, int *) = NULL;
    int (*mILload) (ILimage *, void *, float *) = NULL;
    int (*mILclose) (ILimage *) = NULL;
    int (*mILgetbpp) (ILimage *) = NULL;
    int (*mILctrl) (ILimage *, int, int) = NULL;

	mILDSO = LoadLibrary("Image.dll");
	if (!mILDSO) 
	{
	    fprintf(stderr, "IFFImage: unable to load Image.dll\n");
		return false;
	}
	if( !(mILopen = (ILimage *(*)(const char *, const char *)) GetProcAddress((HMODULE) mILDSO, "ILopen")) ||
	    !(mILgetsize = (int (*)(ILimage *, int *, int *)) GetProcAddress((HMODULE) mILDSO, "ILgetsize")) ||
	    !(mILload = (int (*)(ILimage *, void *, float *)) GetProcAddress((HMODULE) mILDSO, "ILload")) ||
	    !(mILclose = (int (*)(ILimage *)) GetProcAddress((HMODULE) mILDSO, "ILclose")) ||
	    !(mILgetbpp = (int (*)(ILimage *)) GetProcAddress((HMODULE) mILDSO, "ILgetbpp")) ||
	    !(mILctrl = (int (*)(ILimage *, int, int)) GetProcAddress((HMODULE) mILDSO, "ILctrl"))
	    ) 
	{
	    fprintf(stderr, "IFFImage: unable to find necessary functions in libImage.so\n");
	    FreeLibrary((HMODULE) mILDSO);
	    mILDSO = 0;
	    return false;
	}

    ILimage* image = mILopen(filename, "r");
    if (!image) 
	{
	    fprintf(stderr, "mILopen: unable to open file %s\n", filename);
//	    FreeLibrary((HMODULE) mILDSO);
		return false;
	}

	mILctrl( image, ILF_Pack, 0);
	mILctrl( image, ILF_Full, 0);
	int bpp = mILgetbpp( image);

	int width, height;
	mILgetsize(image, &width, &height);

    unsigned char* pixels = new unsigned char[width * height * 4];
    mILload(image, pixels, 0);
    mILclose(image);
	matrix.init(width, height);

    for(int i = 0; i < height; i++) 
	{
		for(int j = 0; j < width; j++) 
		{
//		    unsigned offset = 4 * ((height - i - 1) * width + j);
		    unsigned offset = 4 * (i * width + j);
			float r = pixels[offset + 2];
			float g = pixels[offset + 1];
			float b = pixels[offset + 0];
			matrix[i][j] = (unsigned char)( (r+g+b)/3);
		}
	}

    delete(pixels);

//    fprintf(stderr, "LoadMatrixFromIFF file loading succesefull %s\n", filename);
	return true;
}
#endif


