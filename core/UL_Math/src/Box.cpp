//#include "stdafx.h"
#define ED_MATH_INTERNALS
#define ED_MATH_IMPLEMENT_MATRIX
#include "Math/Box.h"

using namespace Math;

namespace Math 
{
template<>
Box<2, int>	Box<2, int>::id(2);
template<>
Box<3, int>	Box<3, int>::id(2);
template<>
Box<2, float>	Box<2, float>::id(2);
template<>
Box<3, float>	Box<3, float>::id(2);
template<>
Box<2, double>	Box<2, double>::id(2);
template<>
Box<3, double>	Box<3, double>::id(2);
}

