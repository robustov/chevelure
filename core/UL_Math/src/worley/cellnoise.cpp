//#include "stdafx.h"

#include <stdio.h>
#include <string.h>
#include <math.h>
#include "tables.h"
#include "Math/cellnoise.h"
//#include <ri.h>
//aler@mila-faq.nsk.ru

float ED_MATH_API Math::cellnoise( float x)
{
   float           f;
   unsigned int    i;

   f = x;
   if (f<0.0)
      f-=1;
   i = P[((unsigned int)(f))&0x7ff];

   return R[i];
}

float ED_MATH_API Math::cellnoise( int narg, float v[])
{
   float           f;
   unsigned int    i;

   if (narg<1) return 0;

	f = v[0];
	if (f<0.0)
		f-=1;
	i = P[(unsigned int)(f)&0x7ff];

	for (int n=1; n<narg; n++){
		f = v[n];
		if (f<0.0)
			f-=1;
		i = P[P[i] + (unsigned int)(f)&0x7ff];
  }

   return R[i];
}


/*/
void cellnoise( float x, RtPoint res)
{
  float           f;
  unsigned int    i;

  f = x;
  if (f<0.0)
		f-=1;
  i = P[((unsigned int)(f))&0x7ff];

  res[0] = R[i];
  i = P[P[i] + i];
  res[1] = R[i];
  i = P[P[i] + i];
  res[2] = R[i];
}
/*/

void ED_MATH_API Math::cellnoise(float v[3], float res[3])
{
   float           f;
   unsigned int    i;

	f = v[0];
	if (f<0.0)
		f-=1;
	i = P[(unsigned int)(f)&0x7ff];

	for (int n=1; n<3; n++){
		f = v[n];
		if (f<0.0)
			f-=1;
		i = P[P[i] + (unsigned int)(f)&0x7ff];
  }

   res[0] = R[i];
   i = P[i];
   res[1] = R[i];
   i = P[i];
   res[2] = R[i];
}

/*/
void cellnoise( int narg, float v[], RtPoint res)
{
   float           f;
   unsigned int    i;

	 if (narg<1){
		 res[0]=res[1]=res[2]=0;
		 return;
	 }

	f = v[0];
	if (f<0.0)
		f-=1;
	i = P[(unsigned int)(f)&0x7ff];

	for (int n=1; n<narg; n++){
		f = v[n];
		if (f<0.0)
			f-=1;
		i = P[P[i] + (unsigned int)(f)&0x7ff];
  }

   res[0] = R[i];
   i = P[i];
   res[1] = R[i];
   i = P[i];
   res[2] = R[i];
}
/*/
