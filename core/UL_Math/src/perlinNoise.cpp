//#include "stdafx.h"

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include "Math/perlinnoise.h"

#define DOT(a,b) (a[0] * b[0] + a[1] * b[1] + a[2] * b[2]) 
#define B 256 
static int start = 1; 

#define setup(i,b0,b1,r0,r1) t = i + 10000.0f; b0 = ((int)t) & (B-1);  b1 = (b0+1) & (B-1);  r0 = t - (int)t;  r1 = r0 - 1.0f; 

Math::PerlinNoise::PerlinNoise(int seed)
{
	p = 0;
	g = 0;
	Init(seed);
}
namespace Math
{
void PerlinNoise::Init(int seed)
{
	if( p!=0 && g!=0 && this->seed == seed)
		return;

	this->seed = seed;
	p = new int[B +B +2];
	g = new Math::Vec3f[B +B +2];

	int i, j, k; 
	float v[3], s; 
 
	// Create an array of random gradient vectors uniformly on the
	// unit sphere

	srand(seed);

	for (i = 0 ; i < B ; i++) 
	{ 
		do 
		{ 
			// Choose uniformly in a cube
			for (j=0 ; j<3 ; j++) 
				v[j] = (float)((rand() % (B + B)) - B) / B; 
			s = DOT(v,v); 
		} while (s > 1.0);					// If not in sphere try again
		s = sqrtf(s); 
		for (j = 0 ; j < 3 ; j++)			// Else normalize
			g[i][j] = v[j] / s; 
	} 

	// Create a pseudorandom permutation of [1..B]
	for (i = 0 ; i < B ; i++) 
		p[i] = i; 
	for(i=B-2 ;i >0 ;i -=2)
	{ 
		k = p[i];
		j = rand() % B;
		p[i] = p[j]; 
		p[j] = k; 
	} 
	// Extend g and p arrays to allow for faster indexing
	for(i=0 ;i <B +2 ;i++) 
	{ 
		p[B + i] = p[i]; 
		for (j = 0 ; j < 3 ; j++) 
			g[B + i][j] = g[i][j]; 
	}

}

// 
float PerlinNoise::noise(float vx, float vy, float vz)
{
	int bx0, bx1, by0, by1, bz0, bz1, b00, b10, b01, b11; 
	float rx0, rx1, ry0, ry1, rz0, rz1, *q, sx, sy, sz, a, b, c, d, t, u, v; 
	register int i, j; 

	if (start) 
	{ 
		start = 0; 
		int x=0;
		Init(x); 
	} 
	setup(vx, bx0,bx1, rx0,rx1); 
	setup(vy, by0,by1, ry0,ry1); 
	setup(vz, bz0,bz1, rz0,rz1); 
	i = p[ bx0 ]; 
	j = p[ bx1 ]; 
	b00 = p[ i + by0 ]; 
	b10 = p[ j + by0 ]; 
	b01 = p[ i + by1 ]; 
	b11 = p[ j + by1 ];

	#define at(rx,ry,rz) ( rx * q[0] + ry * q[1] + rz * q[2] ) 
	#define s_curve(t) ( t * t * (3.0f - 2.0f * t) ) 
	#define lerp(t, a, b) ( a + t * (b - a) ) 

	sx = s_curve(rx0);
	sy = s_curve(ry0);
	sz = s_curve(rz0);
	q = g[ b00 + bz0 ].data();
	u = at(rx0,ry0,rz0);
	q = g[ b10 + bz0 ].data();
	v = at(rx1,ry0,rz0);
	a = lerp(sx, u, v);
	q = g[ b01 + bz0 ].data();
	u = at(rx0,ry1,rz0);
	q = g[ b11 + bz0 ].data();
	v = at(rx1,ry1,rz0);
	b = lerp(sx, u, v); 
	c = lerp(sy, a, b);						// interpolate in y at lo x
	q = g[ b00 + bz1 ].data(); 
	u = at(rx0,ry0,rz1); 
	q = g[ b10 + bz1 ].data(); 
	v = at(rx1,ry0,rz1); 
	a = lerp(sx, u, v); 
	q = g[ b01 + bz1 ].data(); 
	u = at(rx0,ry1,rz1); 
	q = g[ b11 + bz1 ].data(); 
	v = at(rx1,ry1,rz1); 
	b = lerp(sx, u, v); 
	d = lerp(sy, a, b);						// interpolate in y at hi x
	float ddd =  1.5f * lerp(sz, c, d);			// interpolate in z
	return ddd;
}
}
