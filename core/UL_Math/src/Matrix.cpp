//#include "stdafx.h"
#define ED_MATH_INTERNALS
#define ED_MATH_IMPLEMENT_MATRIX
#include "Math/Matrix.h"

using namespace Math;

namespace Math {

#ifdef _MSC_VER

	#define _INSTANTIATE( N, T ) \
	template<> \
		Matrix<N, T> Matrix<N, T>::id( T(1) ); \
	template class Matrix<N, T>; \
	template ED_MATH_EXTERN void ED_MATH_API multiply(Matrix<N,T>& r, const Matrix<N,T>& m1, const Matrix<N,T>& m2); \
	template ED_MATH_EXTERN void ED_MATH_API transpose(Matrix<N,T>& r, const Matrix<N,T>& m); \
	template ED_MATH_EXTERN void ED_MATH_API invertOrtho<N,T>(Matrix<N,T>& r, const Matrix<N,T>& m); \
	template ED_MATH_EXTERN void ED_MATH_API multiply(Vector<N,T>& r, const AsMatrix<N,N,T>& m, const Vector<N,T>& v); \
	template ED_MATH_EXTERN void ED_MATH_API multiply(Vector<N-1,T>& r, const AsMatrix<N-1,N,T>& m, const Vector<N-1,T>& v); 
//	template ED_MATH_EXTERN void ED_MATH_API multiply1<N-1,N,T>(Vector<N-1,T>& r, const AsMatrix<N,N,T>& m, const Vector<N-1,T>& v);
//	template ED_MATH_EXTERN void ED_MATH_API invert<N,T>(Matrix<N,T>& r, const Matrix<N,T>& m); 
#else

	#define _INSTANTIATE( N, T ) \
	template<> \
		Matrix<N, T> Matrix<N, T>::id( T(1) ); \
	template class Matrix<N, T>; \
	template ED_MATH_EXTERN void ED_MATH_API multiply(Matrix<N,T>& r, const Matrix<N,T>& m1, const Matrix<N,T>& m2); \
	template ED_MATH_EXTERN void ED_MATH_API transpose(Matrix<N,T>& r, const Matrix<N,T>& m); \
	template ED_MATH_EXTERN void ED_MATH_API invertOrtho<N,T>(Matrix<N,T>& r, const Matrix<N,T>& m); \
	template ED_MATH_EXTERN void ED_MATH_API multiply(Vector<N,T>& r, const AsMatrix<N,N,T>& m, const Vector<N,T>& v); \
	template ED_MATH_EXTERN void ED_MATH_API multiply(Vector<N-1,T>& r, const AsMatrix<N-1,N,T>& m, const Vector<N-1,T>& v); 
//	template ED_MATH_EXTERN void ED_MATH_API multiply1<N-1,N,T>(Vector<N-1,T>& r, const AsMatrix<N,N,T>& m, const Vector<N-1,T>& v);
//	template ED_MATH_EXTERN void ED_MATH_API invert<N,T>(Matrix<N,T>& r, const Matrix<N,T>& m); 
#endif

_INSTANTIATE( 2, float )
_INSTANTIATE( 3, float )
_INSTANTIATE( 4, float )

_INSTANTIATE( 2, double )
_INSTANTIATE( 3, double )
_INSTANTIATE( 4, double )

//template ED_MATH_EXTERN void ED_MATH_API invert(Matrix<2,float>& r, const Matrix<2,float>& m);
//template ED_MATH_EXTERN void ED_MATH_API invert(Matrix<3,float>& r, const Matrix<3,float>& m);
//template ED_MATH_EXTERN void ED_MATH_API invert(Matrix<4,float>& r, const Matrix<4,float>& m);

//template ED_MATH_EXTERN void ED_MATH_API invert(Matrix<2,double>& r, const Matrix<2,double>& m);
//template ED_MATH_EXTERN void ED_MATH_API invert(Matrix<3,double>& r, const Matrix<3,double>& m);
//template ED_MATH_EXTERN void ED_MATH_API invert(Matrix<4,double>& r, const Matrix<4,double>& m);

}

